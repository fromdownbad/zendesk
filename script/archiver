#!/usr/bin/env ruby
require 'optparse'
require 'time'

options = {}
OptionParser.new do |opts|
  opts.banner = <<BANNER
Archiver for ticket archive V2.
BANNER
  opts.on("--log_level=LEVEL", String, "specify rails log level (debug,info,warn,error)") { |level| options[:log_level] = level }
  opts.on("--shard_ids=[IDS]", String, "list of shard_ids to operate on") { |list| options[:shard_ids] = list.split(",").map(&:to_i) }
  opts.on("--worker_mod=n,nWorkers", String, "tuple of [which_worker,worker_count]") { |list| options[:worker_number], options[:worker_count] = *list.split(",").map(&:to_i) }
  opts.on("--account_ids=[IDS]", String, "list of account_ids to archive tickets, comma separated") {|x| options[:account_ids] = x.split(",") }
  opts.on("--delete", "DEPRECATED: Always enabled")
  opts.on("--copy-only", "DEPRECATED: Always disabled")
  opts.on("--run_once", "only run one round of archiving") { options[:run_once] = true }
  opts.on("--max-size", "max size of tickets to archive in MB") { |sz| options[:max_size_mb] = sz.to_i }
  opts.on("--dry-run", "no side-effects, just enumerate all tickets") { options[:dry_run] = true }
  opts.on("--retry-on-crash", "rescue all errors at the script level to ensure the main process stays running") { options[:retry_on_crash] = true }
  opts.on("-h", "--help","Show this.") { puts opts; exit }
end.parse!

def normalize_options!(options)
  if options[:worker_number]
    options[:shard_ids] = Zendesk::Archive::Archiver.shards_for_worker_info(options[:worker_number], options.fetch(:worker_count))
  end
end

require_relative "../config/environment"
normalize_options!(options)

retry_on_crash = options.delete(:retry_on_crash)

begin
  $archiver = Zendesk::Archive::Archiver.new(options)
  $archiver.run
rescue StandardError => e
  if retry_on_crash
    $statsd.increment('misc.process.crash', tags: ["process:archiver", "error:#{e.class}"])
    warn "Archiver process crashed, retrying in 1 min. #{e.class}\n#{e.message}"
    sleep 60
    retry
  else
    raise e
  end
end
