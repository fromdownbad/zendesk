/* class method file line count, sorted by count */
ruby*:::method-entry
{
  @[copyinstr(arg0), copyinstr(arg1), copyinstr(arg2), arg3] = count();
}
