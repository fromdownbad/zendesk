# Usage:
# ruby -r 'digest/md5' -p -i.bak script/obfuscate_email_addresses.rb test/fixtures/inbound_mailer/**/*.*

gsub!(/[\w\.+-]+@\w+\.[\w\.]+/) do
  email = $&
  if email =~ /localhost|(ag|aghassipour|foo|zendesk|example|missing|mail\.gmail)\.com$/
    email
  else
    "#{Digest::MD5.hexdigest(email.reverse)[0..10]}@example.com"
  end
end
