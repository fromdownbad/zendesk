#!/bin/sh

set -ex

# make tests boot and run faster + simulate production
export FORCE_EAGER_LOAD=true

# do not upload vendor/bundle, misc takes care of that
# see https://github.com/travis-ci/casher/blob/master/bin/casher
rm -f $CASHER_DIR/mtime.yml

# just a debugging message to verify which Gemfile was used to bundle
echo ${BUNDLE_GEMFILE:-Gemfile}

bundle exec rake db:load_config db:test:load zendesk:load_timezones
bundle exec ruby test/integration/cleanliness_test.rb
