#!/bin/sh
#
# This is copied from Help Center and is used to upload assets to
# the s3 buckets during the cloudbuild process.
#
# https://github.com/zendesk/help_center/blob/a5a558e/script/s3_sync.sh

SOURCE_DIR=$1
DESTINATION=$2

# Default assets, cache age set to 10 minutes
aws s3 sync \
  --only-show-errors \
  --exclude manifest.json \
  --exclude *.map \
  --sse AES256 \
  --acl public-read \
  --cache-control "public, max-age=600" \
  $SOURCE_DIR $DESTINATION

# Other asset types with content-type overrides

# aws s3 sync --exclude '*' \
#   --include *.eot \
#   --content-type "application/vnd.ms-fontobject" \
#   --sse AES256 --acl public-read \
#   --cache-control "public, max-age=678" \
#   $SOURCE_DIR $DESTINATION

# aws s3 sync --exclude '*' \
#   --include *.ttf \
#   --content-type "application/octet-stream" \
#   --sse AES256 --acl public-read \
#   --cache-control "public, max-age=678" \
#   $SOURCE_DIR $DESTINATION

# aws s3 sync --exclude '*' \
#   --include *.woff \
#   --content-type "application/font-woff" \
#   --sse AES256 --acl public-read \
#   --cache-control "public, max-age=678" \
#   $SOURCE_DIR $DESTINATION
