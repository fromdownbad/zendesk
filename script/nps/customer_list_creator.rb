#!/usr/bin/env ruby
require_relative '../../config/environment'
require 'optparse'

class CustomerListCreator
  attr_reader :number_of_users, :tag, :account, :email_name

  def self.read_options
    options = {}
    ARGV << '-h' if ARGV.empty?
    ARGV.options do |opts|
      opts.banner = "Usage: cl_creator.rb [options]"
      opts.on("-e", "--email_name=val", "Actual email will be val+{user_id}@zendesk.com") { |email| options[:email_name] = email }
      opts.on("-a", '--account-subdomain=val') { |subdomain| options[:subdomain] = subdomain }
      opts.on("-n", "--num-of-endusers=val", Integer) { |integer| options[:enduser_number] = integer }
      opts.on("-t", "--tag=val") { |source| options[:tag] = source }
      opts.on_tail("-h", "--help") do
        puts opts
        exit
      end
      opts.parse!
    end
    options
  end

  def self.create_cl
    new(read_options).run!
  end

  def initialize(options)
    @account         = ::Account.find_by_subdomain(options[:subdomain])
    @number_of_users = options[:enduser_number]
    @tag             = options[:tag]
    @email_name           = options[:email_name] || 'foo'
  end

  def run!

    puts "Generating #{number_of_users} users for #{account.subdomain}"

    account.on_shard do
      reserve_global_uids do
        number_of_users.times do |i|
          user_id     = User.get_reserved_global_uid
          created_at  = 1.day.ago.to_date
          last_login  = DateTime.now
          external_id = user_id

          ActiveRecord::Base.connection.execute(
            <<-eos
            INSERT INTO `users` (`account_id`, `created_at`, `crypted_password`,
            `delta`, `details`, `external_id`, `id`, `is_active`, `is_moderator`, `is_private_comments_only`, `last_login`,
            `locale_id`, `name`, `notes`, `openid_url`, `organization_id`, `permission_set_id`, `phone`, `restriction_id`,
            `roles`, `salt`, `time_zone`, `updated_at`, `uses_12_hour_clock`)
            VALUES (#{account.id}, '#{created_at}', NULL, 0, NULL, '#{external_id}',#{user_id}, 1, 0, 0, '#{last_login}',
            1, 'PT User #{user_id}', NULL, NULL, NULL, NULL, NULL, 4, 0, NULL, NULL, '2013-11-26 20:36:10', 0);
          eos
          )


          tag_id = Tagging.get_reserved_global_uid
          ActiveRecord::Base.connection.execute(
            <<-eos
            INSERT INTO `taggings` (`account_id`, `created_at`, `id`, `position`, `tag_id`, `tag_name`, `taggable_id`, `taggable_type`)
            VALUES (#{account.id}, '2013-11-26 23:28:06', #{tag_id}, NULL, NULL, '#{tag}', #{user_id}, 'User');
            eos
          )


          user_identity_id = UserEmailIdentity.get_reserved_global_uid
          ActiveRecord::Base.connection.execute(
            <<-eos
            INSERT INTO `user_identities` (`account_id`, `created_at`, `id`, `is_verified`, `priority`, `type`, `undeliverable_count`, `updated_at`, `user_id`, `value`)
            VALUES (#{account.id}, '2013-11-26 23:50:26', #{user_identity_id}, 0, 1, 'UserEmailIdentity', 0, '2013-11-26 23:50:26', #{user_id}, '#{email_name}+#{user_id}@zendesk.com');
            eos
          )
        end
      end
    end
    puts "Generated #{number_of_users} users with tag #{tag}"
  end

  private

  def reserve_global_uids
    # removed bulk uid generation
    yield
  end
end

if __FILE__ == $0
  raise "This script is not intended for production." if Rails.env.production?
  CustomerListCreator.create_cl
end
