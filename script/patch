#!/bin/bash
function usage()
{
  echo "Cherry-pick and create a patch pull request against master and staging"
  echo ""
  echo "./script/patch"
  echo "  -h --help"
  echo "  -s --staging-only"
  echo "  -p --production-only"
  echo ""
}

STAGING_ONLY=0
PROD_ONLY=0

while [ "$1" != "" ]; do
  PARAM=`echo $1 | awk -F= '{print $1}'`
  VALUE=`echo $1 | awk -F= '{print $2}'`
  case $PARAM in
    -h | --help)
      usage
      exit
      ;;
    -s | --staging-only)
      STAGING_ONLY=1
      ;;
    -p | --production-only)
      PROD_ONLY=1
      ;;
    *)
      echo "ERROR: unknown parameter \"$PARAM\""
      usage
      exit 1
      ;;
  esac
  shift
done

if [ "$STAGING_ONLY" == "1" ] && [ "$PROD_ONLY" == "1" ]; then
  echo "You can't use both --staging-only and --production-only"
  exit 1
fi

set -e

echo "This will perform a hard reset of your local staging and production branches. Continue? [y/N]:"
read confirm

if [ "$confirm" != "y" ]; then
  exit 1
fi

# Make sure local repository is up to date with the remote
echo "Updating local repository..."
git fetch origin --quiet

# SHA to patch
echo ""
echo "Enter the merge SHA(s) from PRs against master. This argument will be passed directly to git cherry-pick"
echo "Separate multiple SHAs with a space and provide them in merge order to reduce chances for conflict:"
read sha
echo ""

###
# Staging
###
if [ "$PROD_ONLY" != "1" ]; then
  # Check out the staging branch
  git checkout staging --quiet
  git reset --hard origin/staging --quiet

  # Create a branch with your name and a unique patch number
  counter=1
  staging_branch="$USER/patch-staging-$counter"

  until git checkout --quiet -b $staging_branch 2> /dev/null; do
    let counter+=1
    staging_branch="$USER/patch-staging-${counter}"
  done <<< "$staging_branch"

  echo "Creating staging branch: $(git rev-parse --abbrev-ref HEAD)"

  # Cherry pick the merge commit(s) from master (make sure to use the merge commit SHA):
  git cherry-pick -m1 $sha > /dev/null

  # Push up the branch
  echo "Pushing $staging_branch to origin"
  git push --quiet origin $staging_branch

  staging_pr_url="https://github.com/zendesk/zendesk/compare/staging...$staging_branch?expand=1"

  echo "Proceed to the following URL to open the Pull Request:"
  echo $staging_pr_url

  if [ $(which open) ]; then
    open $staging_pr_url
  fi
fi

###
# Production
###
if [ "$STAGING_ONLY" != "1" ]; then
  echo ""
  # Check out the production branch
  git checkout production --quiet
  git reset --hard origin/production --quiet

  # Create a branch with your name and a unique patch number
  counter=1
  production_branch="$USER/patch-production-$counter"

  until git checkout --quiet -b $production_branch 2> /dev/null; do
    let counter+=1
    production_branch="$USER/patch-production-${counter}"
  done <<< "$production_branch"

  echo "Creating production branch: $(git rev-parse --abbrev-ref HEAD)"

  # Cherry pick the merge commit(s) from master (make sure to use the merge commit SHA):
  git cherry-pick -m1 $sha > /dev/null

  # Push up the branch
  echo "Pushing $production_branch to origin"
  git push --quiet origin $production_branch

  production_pr_url="https://github.com/zendesk/zendesk/compare/production...$production_branch?expand=1"

  echo "Proceed to the following URL to open the Pull Request:"
  echo $production_pr_url

  if [ $(which open) ]; then
    open $production_pr_url
  fi
fi
