#!/usr/bin/env ruby

# Creates fake ("synthetic") Tweets and creates IncomingConversion objects for
# them.  This allows you to rapidly create many Tickets/Comments based on tweets.

require 'optparse'
options = {}

DEFAULT_BREADTH         = 3
DEFAULT_DEPTH           = 3
DEFAULT_TWITTER_USER    = 'kbucklertest2'
MAX_TWEETS              = 10000
TWEETBACK_SLEEP         = 0.1
MAX_TWEETBACK_SLEEPS    = 600

opts_parser = OptionParser.new do |opt|
  opt.banner = "Usage: #{__FILE__} <subdomain> [options]"
  opt.separator ""
  opt.separator "Options"
  opt.on("-b","--breadth [N]", Integer, "Breadth (number of tweets at each level.)  Default is #{DEFAULT_BREADTH}.") { |o| options[:breadth] = o }
  opt.on("-d","--depth [N]", Integer, "Depth (of responses to responses.)  Default is #{DEFAULT_DEPTH}.") { |o| options[:depth] = o }
  opt.on("-u","--user [S]", String, "Screen name of Twitter user that is tweeting.  Default is #{DEFAULT_TWITTER_USER}.") { |o| options[:twitter_user] = o }
  opt.on("-r","--respond [S]", String, "Respond to incoming Tweets with Tweetbacks from indicated user.  Default is nil (no Tweetback.)") { |o| options[:tweetback_user] = o }
  opt.on("-q","--quiet",  "Minimal logging.  Default to false.")  { |o| options[:quiet] = o }
end

options[:subdomain] = ARGV[0]
options[:breadth]       ||= DEFAULT_BREADTH
options[:depth]         ||= DEFAULT_DEPTH
options[:twitter_user]  ||= DEFAULT_TWITTER_USER

begin
  opts_parser.parse!
  mandatory = [:subdomain]
  missing = mandatory.select{ |param| options[param].nil? }
  if not missing.empty?
    puts opts_parser
    abort "Missing options: #{missing.join(', ')}"
  end
rescue OptionParser::InvalidOption, OptionParser::MissingArgument
  puts $!.to_s
  abort opts_parser
end

options[:quiet] = !!options[:quiet]
options[:tweetback] = options.has_key?(:tweetback_user)

total_tweets = 0
(1..options[:depth]).each do |current_depth|
  total_tweets = total_tweets + (options[:breadth] ** (current_depth))
end

total_tweets = total_tweets * 2 if options[:tweetback]

# If the user asked us to make too many tweets, quit
if total_tweets > MAX_TWEETS
  abort "Cannot make #{total_tweets} tweets, max is #{MAX_TWEETS}"
end

# If the user asked us to make a lot of tweets, confirm
if !options[:quiet]
  if total_tweets > 50
    puts "Is it OK to create #{total_tweets} tweets (y/n)?"
    conf = STDIN.gets.chomp
    if !conf.downcase.start_with?('y')
      abort "will not create #{total_tweets} tweets"
    end
  end
end


require_relative '../config/environment.rb'

RESOURCE_TYPE         = ::Channels::Constants::TWITTER_STATUS

class ConversionCreator

  attr_accessor :options, :account, :source, :start, :next_tweet_idx, :external_ids_needing_tweetback

  def initialize(options)
    @options = options
    @account = Account.find_by_subdomain(options[:subdomain])
    ActiveRecord::Base.on_shard(@account.shard_id) do
      @source = MonitoredTwitterHandle.where(:account_id => account.id).first
    end
    @start = Time.now.to_i
    @next_tweet_idx = 1

    ActiveRecord::Base.on_shard(@account.shard_id) do
      @twitter_user_profile = Channels::UserProfile.where(name: options[:twitter_user])
    end

    @external_ids_needing_tweetback = []
  end

  def work
    ActiveRecord::Base.on_shard(account.shard_id) do

      if options[:tweetback]
        # Set an account setting that will prevent the job from actually tweeting
        if 0 != account.settings.suppress_channelback
          abort "Another instance of #{__FILE__} is running; concurrent access is not supported.  If no other instance is running, check the suppress_channelback setting of account '#{options[:subdomain]}'."
        end
      end

      account.settings.suppress_channelback = start
      account.save!

      begin
        create_child_incoming_conversions_recursive
        handle_dangling_tweetbacks if options[:tweetback]

        account.reload
        account.settings.suppress_channelback = 0
        account.save!
      ensure
        account.reload
        account.settings.suppress_channelback = 0
        account.save!
      end
    end
  end

  def create_child_incoming_conversions_recursive(parent_path = [], parent_tweet_idx = nil)
    if options[:depth] > parent_path.length
      (1..options[:breadth]).each do |current_breadth|
        child_path = parent_path + [current_breadth]
        child_tweet_idx = create_incoming_conversion(child_path, parent_tweet_idx)
        create_child_incoming_conversions_recursive(child_path, child_tweet_idx)
      end
    end
  end

  def create_incoming_conversion(path, parent_tweet_idx = nil)
    this_tweet_idx = next_tweet_idx
    @next_tweet_idx = next_tweet_idx + 1
    external_id = tweet_id(this_tweet_idx)

    puts "Creating IncomingConversion for path #{path.join(':')} with external_id #{external_id}" if !options[:quiet]

    payload = {
      'created_at' => Time.now.to_s,
      'id' => external_id,
      'text' => "Run #{start}, location:#{path.join(':')}",
      'user' => {
        'id' => @twitter_user_profile.external_id,
        'name' => @twitter_user_profile.name,
        'screen_name' => @twitter_user_profile.screen_name,
        'profile_image_url' => @twitter_user_profile.profile_image_url
      },
      'entities' => {
        'user_mentions' => [{
          'id' => @source.twitter_user_id
        }]
      },
    }
    payload['in_reply_to_status_id'] = tweet_id(parent_tweet_idx) if parent_tweet_idx.present?

    cf = ::Channels::Converter::ConversionFactory.new(RESOURCE_TYPE, source)
    c = cf.create_incoming_conversion(payload)

    tweetback_external_ids if options[:tweetback]
    external_ids_needing_tweetback << external_id if options[:tweetback]

    this_tweet_idx
  end

  def tweet_id(tweet_idx)
    ((start * MAX_TWEETS) + tweet_idx).to_s
  end

  def tweetback_external_ids
    return if external_ids_needing_tweetback.empty?

    ::Channels::Resource.active.where(:external_id => external_ids_needing_tweetback, :account_id => account.id).each do |resource|
      puts "Creating TweetBack comment for external_id #{resource.external_id}" if !options[:quiet]

      ticket = resource.comment.ticket
      comment = Comment.new(
        :is_public => true,
        :body => "Tweetback response to #{resource.comment.body}",
        :channel_source_id => source.id,
        :via_id => ViaType.WEB_FORM,
        :channel_back => true
      )
      ticket.comment = comment
      ticket.will_be_saved_by(tweetback_user)
      ticket.save!

      del_res = external_ids_needing_tweetback.delete resource.external_id
    end

    !external_ids_needing_tweetback.empty?
  end

  def handle_dangling_tweetbacks
    num_sleeps = 0
    while tweetback_external_ids
      sleep TWEETBACK_SLEEP
      num_sleeps += 1
      if num_sleeps > MAX_TWEETBACK_SLEEPS
        abort "Too much time taken waiting for dangling tweetbacks (#{TWEETBACK_SLEEP * MAX_TWEETBACK_SLEEPS} seconds)"
      end
    end
  end

  def tweetback_user
    @tweetback_user ||= account.agents.where(:name => options[:tweetback_user]).first
  end
end


cc = ConversionCreator.new(options)
cc.work

success_msg = options[:quiet] ? cc.start.to_s : "Created tweets containing token #{cc.start}"
puts success_msg
