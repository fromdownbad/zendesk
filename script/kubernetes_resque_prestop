#!/usr/bin/env ruby

# This script is designed to be run as a kubernetes preStop hook for resque workers. It is intended
# to run infinitely if there are resque workers processing jobs, which is used to stall the
# container termination. This is paired with our configurable `terminationGracePeriodSeconds` value
# which will forcefully kill the container after the given number of seconds if this prestop hook
# has not finished successfully.

require 'datadog/statsd'

class PSResult
  # PS output example:
  #  Thu Jan 23 22:23:52 2020  73957 resque-1.27.4: Processing channels since 1579818243 [Channels::Monitor::Source::FacebookSourceMonitorJob]
  #
  RESQUE_PS_MATCH = /\s+(?<pid>\d+)\s+resque-.+?:\s+processing (?<queue>\w+)\s+since\s+(?<start>\d+)\s+\[(?<job>[\w:]+)\]/i

  attr_reader :pid, :queue, :start, :job_class

  def initialize(process_string)
    process_string.match(RESQUE_PS_MATCH) do |match|
      @pid = match[:pid]
      @queue = match[:queue]
      @start = Time.at(Integer(match[:start]))
      @job_class = match[:job]
    end
  end

  def resque?
    [pid, queue, start, job_class].all?
  end
end

statsd = Datadog::Statsd.new(
  (ENV['STATSD_HOST'] || 'localhost'),
  (ENV['STATSD_PORT'] || 8125),
  namespace: 'classic'
)

metric_name = 'resque.graceful_termination_latency'
prestop_start = Time.now

# Sit here and monitor the resque processes for ones that are actively processing jobs. We only
# want to break out of this loop once there are no active processing jobs.
loop do
  processing_resque_pids = `ps -eo lstart,pid,args`
    .split("\n")
    .map { |ps| PSResult.new(ps) }
    .select(&:resque?)

  if processing_resque_pids.any?
    # Resque appears to be processing a job, record some info about it then check again later
    processing_resque_pids.each do |process|
      statsd.histogram(
        metric_name,
        (prestop_start - process.start).to_i,
        tags: %W[processing:true queue:#{process.queue} job:#{process.job_class}]
      )
    end
  else
    # Resque does not appear to be processing any jobs, exit so the container can be terminated
    statsd.histogram(
      metric_name,
      (Time.now - prestop_start).to_i,
      tags: %w[processing:false]
    )
    exit(0)
  end

  sleep 5
end
