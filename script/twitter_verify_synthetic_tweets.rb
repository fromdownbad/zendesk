#!/usr/bin/env ruby

# Connects to Zendesk and verifies that the objects created by a run of
# twitter_create_synthetic_tweets.rb are correct.
# To use this tool, you'd first call twitter_create_synthetic tweets, noting
# the breadth and depth you specified, as well as the reported token value.
# Then you'd pass those items into this script.  For example:
#   >script/twitter_create_synthetic_tweets.rb support -u MSmedbergTest3 -b 2 -d 4
#   >Created tweets containing token 1407974126
#   >script/twitter_verify_synthetic_tweets.rb support 1407974126 -b 2 -d 4
#   >Tests pass
# NOTE: When you run twitter_create_synthetic_tweets.rb, you'll create a number
# of Resque jobs to create tickets/comments.  You should wait until all of the
# jobs are complete before running this verification script!

require 'optparse'
options = {}

DEFAULT_BREADTH       = 3
DEFAULT_DEPTH         = 3

opts_parser = OptionParser.new do |opt|
  opt.banner = "Usage: #{__FILE__} <subdomain> <create token> [options]"
  opt.separator ""
  opt.separator "Options"
  opt.on("-b","--breadth [N]", Integer, "Breadth (number of tweets at each level.)  Default is #{DEFAULT_BREADTH}.") { |o| options[:breadth] = o }
  opt.on("-d","--depth [N]", Integer, "Depth (of responses to responses.)  Default is #{DEFAULT_DEPTH}.") { |o| options[:depth] = o }
  opt.on("-r","--respond", "Incoming Tweets were responded to with Tweetbacks.  Default is false.") { |o| options[:tweetback] = o }
  opt.on("-u","--user [S]", String, "Screen name of Twitter user that tweeted.  Required if -r option is used.") { |o| options[:twitter_user] = o }
  opt.on("-q","--quiet",  "Minimal logging")  { |o| options[:quiet] = o }
end

options[:subdomain] = ARGV[0]
options[:create_token] = ARGV[1]
options[:breadth]       ||= DEFAULT_BREADTH
options[:depth]         ||= DEFAULT_DEPTH
options[:tweetback]     = !!options[:tweetback]

begin
  opts_parser.parse!
  mandatory = [:subdomain, :create_token]
  missing = mandatory.select{ |param| options[param].nil? }
  if not missing.empty?
    puts opts_parser
    abort "Missing options: #{missing.join(', ')}"
  end

  if options[:tweetback] && (options[:twitter_user].nil? || options[:twitter_user].empty?)
    abort "-r option supplied, but -u option not supplied.  -u is mandatory if -r is supplied"
  end
rescue OptionParser::InvalidOption, OptionParser::MissingArgument
  puts $!.to_s
  abort opts_parser
end

options[:quiet] = !!options[:quiet]

require_relative '../config/environment.rb'

RESOURCE_TYPE         = ::Channels::Constants::TWITTER_STATUS

class ConversionVerifier

  attr_accessor :options, :account, :source, :start, :tweet_idx

  def initialize(options)
    @options = options
    @account = Account.find_by_subdomain(options[:subdomain])
    @errors = 0
  end

  def work
    ActiveRecord::Base.on_shard(@account.shard_id) do
      check_tickets
    end

    @errors
  end

  def check_tickets
    (1..options[:breadth]).each do |current_breadth|
      # There should be a ticket with a description like "Run <token>, location:<current_breadth>"
      expected_desc = "Run #{options[:create_token]}, location:#{current_breadth}"
      report "Checking ticket with description '#{expected_desc}'"
      ticket = @account.tickets.order('created_at DESC').detect do |ticket|
        ticket.description == expected_desc
      end

      if ticket
        check_comments ticket, current_breadth
      else
        report "Failed to find ticket for token #{options[:create_token]}, location #{current_breadth}"
        @errors += 1
      end
    end
  end

  def check_comments(ticket, ticket_breadth)
    paths = calculate_paths_for_breadth(ticket_breadth)
    comments = ticket.comments.order('created_at ASC')

    incoming_comment_descriptions = paths.map { |path| "Run #{options[:create_token]}, location:#{path.join(':')}" }
    incoming_comment_descriptions_idx = 0
    outgoing_comment_descriptions = []
    if options[:tweetback]
      outgoing_comment_descriptions = paths.map { |path| "@#{options[:twitter_user]} Tweetback response to Run #{options[:create_token]}, location:#{path.join(':')}" }
    end
    outgoing_comment_descriptions_idx = 0

    incoming_comment_count = comments.find_all { |comment| !comment.audit.events.any? { |evt| evt.type == 'ChannelBackEvent' } }.length
    if (incoming_comment_count != incoming_comment_descriptions.length)
      report "Expected #{incoming_comment_descriptions.length} incoming comments, but found #{incoming_comment_count}.  This can happen if the MTH needs reauthorization."
      @errors += 1
      return
    end
    outgoing_comment_count = comments.find_all { |comment| comment.audit.events.any? { |evt| evt.type == 'ChannelBackEvent' } }.length
    if (outgoing_comment_count != outgoing_comment_descriptions.length)
      report "Expected #{outgoing_comment_descriptions.length} outgoing comments, but found #{outgoing_comment_count}.  This can happen if the MTH needs reauthorization."
      @errors += 1
      return
    end

    created_at_map = comments.group_by { |comment| comment.created_at }
    created_at_map.keys.sort.each do |created_at|
      incoming_comments = created_at_map[created_at].find_all { |comment| !comment.audit.events.any? { |evt| evt.type == 'ChannelBackEvent' } }
      incoming_comment_descriptions_idx += check_comments_slice(incoming_comments, incoming_comment_descriptions, incoming_comment_descriptions_idx, ticket)

      outgoing_comments = created_at_map[created_at].find_all { |comment| comment.audit.events.any? { |evt| evt.type == 'ChannelBackEvent' } }
      outgoing_comment_descriptions_idx += check_comments_slice(outgoing_comments, outgoing_comment_descriptions, outgoing_comment_descriptions_idx, ticket)
    end

    check_for_missing_comments(incoming_comment_descriptions, incoming_comment_descriptions_idx, ticket)
    check_for_missing_comments(outgoing_comment_descriptions, outgoing_comment_descriptions_idx, ticket)
  end

  def check_comments_slice(comments, expected_descriptions, descriptions_idx, ticket)
    num_comments = comments.length
    if 0 < num_comments
      expected_descriptions_slice = expected_descriptions.slice(descriptions_idx, num_comments)
      comments.each do |comment|
        report "Checking comment '#{comment.value}'"
        if !expected_descriptions_slice.include? comment.value
          report "Comment '#{comment.value}' in ticket '#{ticket.description}' was unexpected"
          @errors += 1
        end
      end
    end

    num_comments
  end

  def check_for_missing_comments(expected_descriptions, descriptions_idx, ticket)
    if descriptions_idx < expected_descriptions.length
      report "Comments missing from ticket '#{ticket.description}':"
      expected_descriptions.drop(descriptions_idx).each do |desc|
        report "  '#{desc}"
        @errors += 1
      end
    end
  end

  def calculate_paths_for_breadth(breadth)
    paths = calculate_paths_recursive.map { |path| [breadth] + path}
    [[breadth]] + paths
  end

  def calculate_paths_recursive(parent_path = [], paths = [], depth = options[:depth] - 1)
    if depth > 0
      (1..options[:breadth]).each do |current_breadth|
        child_path = parent_path + [current_breadth]
        paths << child_path
        calculate_paths_recursive(child_path, paths, depth - 1)
      end
    end

    paths
  end

  def report(info)
    puts info if !options[:quiet]
  end
end

cc = ConversionVerifier.new(options)
errors = cc.work

if errors == 0
  puts 'Tests pass' if !options[:quiet]
else
  abort "#{errors} tests failed"
end
