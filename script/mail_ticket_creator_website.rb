#!/usr/bin/env ruby

require_relative '../config/environment.rb'

STDOUT.sync = true

puts "Starting mail ticket creator website in #{Rails.env} mode. Rails version: #{Rails.version}"

port = ENV.fetch('PORT', '3000')
host = ENV.fetch('HOST', '0.0.0.0')

exec "rackup --port #{port} --host #{host} lib/zendesk/inbound_mail/config.ru"
