module UserIdentityFix

  def self.dry_run?
    return true unless Rails.env.production?

    ENV.key?('DRY_RUN')
  end

  def self.run(file)
    lines = file.readlines

    lines.map do |line|
      begin
        fix_from_line(line)
      rescue => e
        puts "Failed on #{line}..."
        puts "Failed to save: #{e.inspect}" if Rails.env.production?
      end
    end

    run_custom_identity_fixes
  end

  def self.fix_from_line(line)
    identity_id        = line.scan(/ id: (\d+)/).first.first.to_i
    user_id            = line.scan(/user_id: (\d+)/).first.first.to_i
    current_account_id = line.scan(/account_id: (\d+)/).first.first.to_i
    target_account_id  = line.scan(/not on account #(\d+)/).first.first.to_i
    current_account    = Account.find(current_account_id)
    target_account     = Account.find(target_account_id)

    current_account.on_shard do
      user_identity  = UserIdentity.find(identity_id)

      puts "=" * 30

      if current_account == target_account
        puts "User identity ##{identity_id} has already been fixed, ignoring."
        next
      end

      if current_account.shard_id != target_account.shard_id
        puts "NOT ON SAME SHARD! Moving from shard #{current_account.shard_id} to #{target_account.shard_id}"
      end

      target_account.on_shard do
        user_account = user_identity.user.account
        if user_account != target_account
          puts "User account is #{user_account} ##{user_account.id}, but trying to move to #{target_account} ##{target_account.id}, ignoring."
          next
        end

        puts "Fixing user identity ##{identity_id} (#{user_identity.value}) for user ##{user_id}"
        puts "bad identity verified? #{user_identity.is_verified?}"
        puts "Moving from account ##{current_account_id} to ##{target_account_id}"

        user_identity.account = target_account

        if user_identity.valid?
          puts "Identity seems valid"
        else
          puts "Would have failed to save: #{user_identity.errors.full_messages.inspect}"
          user_emails = user_identity.user.identities.email
          good_emails = user_emails.select { |email| email.account_id == user_identity.user.account_id }
          if good_emails.any?
            puts "User with bad identity has good identities #{good_emails.map(&:value)}"
            puts "Deleting bad identity"
            user_identity.destroy unless dry_run?
          else
            puts "No other good identities. Doing nothing..."
          end
        end

        if !dry_run? && !user_identity.frozen?
          puts "Saving user identity..."
          user_identity.save!
        end
      end
    end
  end

  def self.run_custom_identity_fixes
    account_emails = { 3518 => 29601099, 71159  => 17930455, 5726 => 16815323 }
    account_emails.each do |account_id, email_id|
      account = Account.find(account_id)
      account.on_shard do
        email               = UserEmailIdentity.find(email_id)
        wrong_account       = email.account_id != email.user.account_id && email.user.account_id == account.id
        multiple_identities = email.user.identities.email.size > 1

        if wrong_account && multiple_identities
          puts "Identity is on the wrong account and the user has multiple email addresses"
          puts "Deleting bad identity #{email.id}/#{email.value} for user #{email.user.id}"

          email.destroy unless dry_run?
        else
          puts "Unable to delete bad identity #{email.id}"
        end
      end
    end


  end


end

if __FILE__ == $0
  File.open("user_identities.txt", "r") do |file|
    UserIdentityFix.run(file)
  end
end
