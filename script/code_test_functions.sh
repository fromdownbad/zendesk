# Shell functions

code_to_test() {
  sed -e 's/^app\//test\//' -e 's/^lib/test\/lib/' -e 's/\.rb/_test.rb/'
}

test_to_code() {
  sed -e 's/^test\/lib/lib/' -e 's/^test\//app\//' -e 's/_test\.rb/.rb/'
}
