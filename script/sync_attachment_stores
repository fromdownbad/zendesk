#!/usr/bin/env ruby

require 'optparse'
require File.expand_path('../config/environment', File.dirname(__FILE__))

# disable size validation so that we can fix old attachments that were created under a different maximum size
require 'attachment'
class Attachment < ActiveRecord::Base
  def valid_size?
    true
  end
end

Signal.trap("TERM") do
  puts "Terminating due to signal"
  exit(0)
end

def main
  options = {}
  OptionParser.new do |o|
    o.banner = "Usage: #{__FILE__} [options]"

    o.separator "\nSpecific options:"
    o.on('--worker-number N', 'What worker number is this; needs to be in range 1 to worker-count') { |v| options[:worker_number] = v.to_i }
    o.on('--worker-count COUNT', 'How many workers are there -- important for this worker to know its lane') { |v| options[:worker_count] = v.to_i }
    o.on('--[no-]json-logger', 'Log as JSON instead of plain text') { |v| options[:use_json_logger] = v }
    o.on('--log-level LEVEL', 'log level') { |v| options[:log_level] = v.upcase }
    o.on('--[no-]dry-run', 'Dry-run mode; does not update audits or records') { |v| options[:dry_run] = v }
    o.on('--[no-]loop', 'Run once across shards or loop continuously') { |v| options[:loop] = v }
    o.on('--batch-size N', 'Max number of recs we will process for a model (Attachments) during an account\'s turn') { |v| options[:batch_size] = v.to_i }
    o.on('--account-allowance SECONDS', 'Max time to backfill an account before moving forward') { |v| options[:account_allowance] = v.to_f.seconds }
    o.on('--shard-allowance SECONDS', 'Max time to backfill a shard before moving forward') { |v| options[:shard_allowance] = v.to_f.seconds }
    o.on('--priority-weight N', 'Weight (between 0 and 1) for priority (data-locality) accounts for EMEA/FRA pods') { |v| options[:priority_weight] = v.to_f }
    o.on('--retry-on-crash', 'rescue all errors at the script level to ensure the main process stays running') { options[:retry_on_crash] = true }
    o.on('--model-classes CLASS[,CLASS...]', 'A comma-separated list of the model classes to work on') { |v| options[:backfill_classes] = v.split(',') }
    o.on('--weeks X', 'DEPRECATED') {}
    o.on_tail('--help', 'Show this message') do
      puts(<<-HELPMSG)

        Copies objects written through attachment_fu to the correct stores for the pod.

        This will copy objects to the preferred stores, currently based on attachments.yml
        and, if that is successful or unnecessary, remove stores that are not preferred.

        This process works in batches for each account, updating the corresponding audit for the
        account after each batch.  It has a time bound for how long to process one account
        and how long to process one shard.

        This is intended to be run as a permanent background job in each pod. Where it will
        poll shards and audits for work.

      HELPMSG

      puts o

      exit
    end
  end.parse!

  retry_on_crash = options.delete(:retry_on_crash)

  begin
    Zendesk::Stores::Backfill::StoresBackfiller.create_backfiller(options).run
  rescue StandardError => e
    if retry_on_crash
      $statsd.increment('misc.process.crash', tags: ["process:sync_attachment_stores", "error:#{e.class}"])
      warn "Sync attachment stores process crashed, retrying in 1 min. #{e.class}\n#{e.message}"
      sleep 60
      retry
    else
      raise e
    end
  end
end

main
