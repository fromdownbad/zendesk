#!/usr/bin/env ruby

require 'rubygems'
require 'optparse'
options = {}

opts = OptionParser.new do |opt|
  opt.banner = "Usage: script/migrate_attachments <origin> <destination> [options]"
  opt.separator ""
  opt.separator "Specific options"
  opt.on("-r","--range x,y", Array,"Range of directories to be transfered, like 0,4581") do |v|
    options[:range] = v
  end
  opt.on("-b","--batch_size [N]",Integer,"Size of the batches. Default is 50") {|v| options[:batch_size] = v }
  opt.separator ""
  opt.separator "Other options"
  opt.on("-q","--[no-]quiet",  "Minimal logging")  {|v| options[:quiet] = v}
  opt.on("-n","--[no-]dry",  "Dry run")            {|v| options[:dry] = v}
  opt.on("-v","--[no-]verbose","Run verbosely")    {|v| options[:verbose] = v }
end.parse!

options[:origin] = ARGV[0]
options[:destination] = ARGV[1]

begin
  opts.parse!
  mandatory = [:range, :origin, :destination]
  missing = mandatory.select{ |param| options[param].nil? }
  if not missing.empty?
    puts "Missing options: #{missing.join(', ')}"
    puts opts
    exit
  end
rescue OptionParser::InvalidOption, OptionParser::MissingArgument
  puts $!.to_s
  puts opts
  exit
end

RSYNC = '/usr/bin/rsync'
OPTIONS = '-acz'
DESTINATION = options[:destination]
ORIGIN = options[:origin]

INITIAL_DIR = options[:range][0].to_i
FINAL_DIR = options[:range][1].to_i

@quiet     = !!options[:quiet]
@verbose   = !!options[:verbose]
@dry       = !!options[:dry]
batch_size = options[:batch_size] || 50
range      = options[:range]

unless File.exists?(ORIGIN)
  warn("Origin path:#{ORIGIN} does not exists")
  exit(-1)
end

dir_list = INITIAL_DIR.upto(FINAL_DIR).map {|d| "#{ORIGIN}/#{"%04d" % d}"}.select {|dir| File.exists?(dir)}
list_size = dir_list.size
processed = []
batches = 0
retried = false
while dir_list.size > 0
  batch = dir_list.slice(0..batch_size - 1)
  dirs_in_batch = batch.join(' ')
  command_line = "#{RSYNC} #{OPTIONS} #{dirs_in_batch} #{DESTINATION}"
  warn "processing batch:#{batches} size:#{batch.size}" unless @quiet
  warn "#{command_line}" if @verbose && !@quiet
  unless @dry
    output = `#{command_line}`
    exitstatus = $?.exitstatus
    if (exitstatus != 0)
      unless retried
        retried = true
        warn("Retrying...")
        sleep(1)
        redo
      end
      warn("Problem running rsync: \n#{output}") unless @quiet
      warn("Exit code: #{exitstatus}") unless @quiet
      warn("Stopping...") unless @quiet
      exit(-1)
    end
    warn(output) if @verbose && !@quiet
  end
  batches += 1
  processed += batch
  dir_list -= batch
  retried = false
end

warn("list size: #{list_size} processed size #{processed.size} batch size #{batches}")
