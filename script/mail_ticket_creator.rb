#!/usr/bin/env ruby

require_relative '../config/environment.rb'

require 'zendesk_mail'
require 'zendesk/inbound_mail'

STDOUT.sync = true
json_logger = Logger.new(STDOUT)

json_logger.formatter = if Rails.env.development?
  Zendesk::Mail::DevelopmentJsonFormatter.new('mail_ticket_creator')
else
  Zendesk::Mail::JsonFormatter.new(
    'mail_ticket_creator',
    environment: Rails.env,
    pod: Zendesk::Configuration.fetch(:pod_id)
  )
end

# TODO: The KUBERNETES check should be added once this is actually runninng
# within k8s with config loaded the correct way
MAIL_KUBERNETES = !!ENV.fetch('MTC_K8S', false) # && KUBERNETES
MTC_QUEUE_ON_SQS = ENV.fetch('MTC_QUEUE_ON_SQS', false) == 'true' ? true : false

if MAIL_KUBERNETES
  Rails.logger = json_logger
  Zendesk::Mail.logger = json_logger
  puts "Running Mail Ticket Creator in k8s"
else
  # Copied from Zendesk::Mail.logger initialization
  log_file_path = Bundler.root.join('log', "mail_processing_#{Rails.env}.log")
  pid_logger = Zendesk::Mail::PidLogger.new(log_file_path.to_s) do |logger|
    logger.auto_flushing = false
    at_exit { logger.flush }
  end

  Zendesk::Mail.logger = Logcast::Broadcaster.new
  Zendesk::Mail.logger.subscribe(json_logger)
  Zendesk::Mail.logger.subscribe(pid_logger)
end

Zendesk::Mail.raw_remote_files.logger        = Zendesk::Mail.logger
Zendesk::Mail.attachment_remote_files.logger = Zendesk::Mail.logger

# Don't show SQL logs on MTC
ActiveRecord::Base.logger.level = Logger::INFO

puts "Starting mail ticket creator worker in #{Rails.env} mode. Rails version: #{Rails.version}"

dispatcher_class = MTC_QUEUE_ON_SQS ? Zendesk::InboundMail::SQSDispatcher : Zendesk::InboundMail::Dispatcher
config = MAIL_KUBERNETES ? {} : Zendesk::Mail.config
dispatcher = dispatcher_class.new(config)
dispatcher.start
