#!/bin/bash

## docker-images/config.run.yml should contain only 1 group "engineering"
# groups:
#  - engineering
#
## fresh restart
# zdi vm restart && zdi services restart && zdi apps restart
#
### Run benchmark
# ./script/benchmark_memcached_and_dalli.sh
###

set -e

CACHE_CLASSES_OPTS='-e RAILS_CACHE_CLASSES=true'
DALLI_OPTS="-e DALLI=true"
RAILS3_OPTS="${CACHE_CLASSES_OPTS}"
RAILS4_OPTS="${CACHE_CLASSES_OPTS} -e BUNDLE_GEMFILE=Gemfile.rails4"


function benchmark() {
  sleep 30
  zdi mysql reset &> /dev/null
  ruby ./test/load/ticket_creator.rb
  time ruby ./test/load/traffic_creator.rb
}

echo "Rails 3 + memcached:"
zdi zendesk -d restart $RAILS3_OPTS -b > /dev/null
benchmark

echo "Rails 3 + dalli:"
zdi zendesk -d restart $RAILS3_OPTS $DALLI_OPTS -b > /dev/null
benchmark

echo "Rails 3 + memcached:"
zdi zendesk -d restart $RAILS4_OPTS -b > /dev/null
benchmark

echo "Rails 4 + dalli:"
zdi zendesk -d restart $RAILS4_OPTS $DALLI_OPTS -b > /dev/null
benchmark

