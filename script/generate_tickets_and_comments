#!/usr/bin/env ruby

=begin
Usage: generate_tickets_and_comments subdomain
=end

require 'optparse'

class TicketsAndCommentsGenerator

  def self.parse(args)
    {}.tap do |params|
      args.push('-h') if args.empty?

      params[:subdomain] = args.first

      option_parser = OptionParser.new do |options|
        options.banner = 'Usage: generate_tickets_and_comments subdomain [options]'

        options.on_tail('-h', 'Help') do
          puts options

          exit
        end
      end

      option_parser.parse!

      %i(subdomain).each do |param|
        if !params.has_key?(param)
          puts option_parser

          fail OptionParser::MissingArgument, "'#{param}'"
        end
      end
    end
  end

  def initialize(args)
    params = self.class.parse(args)
    @subdomain        = params[:subdomain]
  end

  def generate
    agents = simulator.create(:agent, 32)
    users = simulator.create(:user, 6)

    simulator.create(:ticket, 100, users: users).each do |ticket|
      simulator.create(:comment, rand(1..12), ticket: ticket)
    end

    puts "Finished!"
  end

  private

  attr_reader :subdomain

  def simulator
    @simulator ||= SimZen::Simulator.new(subdomain)
  end
end

generator = TicketsAndCommentsGenerator.new(ARGV)

require File.expand_path('../../config/boot', __FILE__)
require_relative '../config/environment'
require 'sim_zen'

ActiveRecord::Base.default_shard = 1

generator.generate
