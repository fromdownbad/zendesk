# RAILS_ENV=staging ruby-prof script/test.rb -p call_tree -f xxx.trace
# qcachegrind xxx.trace
RubyProf.stop
require 'bundler/setup'
require './config/boot.rb'
ARGV.replace ["c"]
APP_PATH = File.expand_path('../../config/application',  __FILE__)
require 'rails/commands'
RubyProf.start

100.times { puts app.get("http://support.localhost:3000/tickets/new") }
