#!/usr/bin/env ruby

# This script calls twitter_create_synthetic_tweets.rb and twitter_verify_synthetic_tweets.rb
# over and over, in a loop.  The goal is to expose locking bugs that may happen
# infrequently.

# You may want to tweak these constants to match your needs
SUBDOMAIN           = 'support'
TWITTER_USER        = 'MSmedbergTest3'
AGENT_NAME          = 'Agent Extraordinaire'
BREADTH             = 3
DEPTH               = 4
RUBY_CMD            = 'bundle exec ruby'
QUIET               = false

CREATE_TWEETS_FILE  = 'twitter_create_synthetic_tweets.rb'
VERIFY_TWEETS_FILE  = 'twitter_verify_synthetic_tweets.rb'
RESQUE_JOB_NAMES    = ['Channels::IncomingConversionJob', 'Channels::OutgoingConversionJob']

require_relative '../config/environment.rb'

def resque_job_running?
  any_queued = Resque.peek('high', 0, 99999).any? do | job |
    RESQUE_JOB_NAMES.include? job['class']
  end

  any_running = Resque::Worker.working.any? do |wk|
    job = wk.job
    job.present? &&
      job.has_key?('payload') &&
      job['payload'].has_key?('class') &&
      RESQUE_JOB_NAMES.include?(job['payload']['class'])
  end

  any_running || any_queued
end

create_cmd = "#{RUBY_CMD} #{File.join(File.dirname(__FILE__), CREATE_TWEETS_FILE)} #{SUBDOMAIN} -u #{TWITTER_USER} -b #{BREADTH} -d #{DEPTH} -r \"#{AGENT_NAME}\" -q"

loop_idx = 1
while true
  puts "Creating via '#{create_cmd}'" if !QUIET

  token = ''
  time_to_create = Benchmark.ms do
    token = `#{create_cmd}`.chomp
    exit_status = $?.to_i
    if 0 != exit_status
      abort "Error: command '#{create_cmd}' returned #{exit_status}"
    end

    puts "Waiting for jobs" if !QUIET
    while resque_job_running?
      sleep 0.1
    end
  end
  puts "Created tickets, comments, and tweetbacks in #{time_to_create} ms" if !QUIET

  verify_cmd = "#{RUBY_CMD} #{File.join(File.dirname(__FILE__), VERIFY_TWEETS_FILE)} #{SUBDOMAIN} #{token} -u #{TWITTER_USER} -b #{BREADTH} -d #{DEPTH} -r -q"
  puts "Verifying via '#{verify_cmd}'" if !QUIET
  verify_result = `#{verify_cmd}`
  exit_status = $?.to_i
  if 0 != exit_status
    abort "Error: verification failed for '#{verify_cmd}', returned #{exit_status}"
  end

  puts "Verification #{loop_idx} at #{Time.now} successful"

  @account = Account.find_by_subdomain(SUBDOMAIN)
  ActiveRecord::Base.on_shard(@account.shard_id) do
     @account.tickets.order('created_at DESC').each do |ticket|
      if ticket.description.match(token)
        ticket.destroy
      end
    end
  end

  loop_idx += 1
end
