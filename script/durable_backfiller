#!/usr/bin/env ruby

require 'optparse'
require_relative '../config/environment'

options = {}
OptionParser.new do |o|
  o.banner = "Usage: #{__FILE__} [options]"

  o.separator "\nSpecific options:"
  o.on('--retry-on-crash', 'rescue all errors at the script level to ensure the main process stays running') { options[:retry_on_crash] = true }
  o.on_tail('--help', 'Show this message') { puts o; exit }
end.parse!

retry_on_crash = options.delete(:retry_on_crash)

begin
  DurableBackfill::Manager.run
rescue StandardError => e
  if retry_on_crash
    $statsd.increment('misc.process.crash', tags: ["process:durable_backfiller", "error:#{e.class}"])
    warn "Durable backfiller process crashed, retrying in 1 min. #{e.class}\n#{e.message}"
    sleep 60
    retry
  else
    raise e
  end
end
