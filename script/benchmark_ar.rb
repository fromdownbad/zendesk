# used to benchmark activerecord speed between 2.3 / 3.0 / 3.1
require File.expand_path('../../config/boot', __FILE__)
require_relative '../config/environment'
require 'benchmark'

def measure(times, name, &block)
  time = Benchmark.realtime do
    times.times(&block)
  end
  puts "#{times}x#{name}\t = #{time.round(2)}"
end

ActiveRecord::Base.default_shard = 1

puts(t = Ticket.first)
measure(1000, "Traversal"){ Account.first.users.first.tickets.first }
measure(10000, "User find simple"){ User.first || raise("FAIL") }
measure(10000, "Ticket find simple"){ Ticket.where(status_id: [1,2,3,4,5,6]).first || raise("FAIL") }
measure(5000, "Ticket find scopes"){ Ticket.for_user(t.requester).not_closed.first || raise("FAIL") }
