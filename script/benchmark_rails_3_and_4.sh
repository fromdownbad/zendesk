#!/bin/bash

## docker-images/config.run.yml should contain only 1 group "engineering"
# groups:
#  - engineering
#
## fresh restart
# zdi vm restart && zdi services restart && zdi apps restart
#
### Run benchmark
# ./script/benchmark_rails_3_and_4.sh
###

set -e

CACHE_CLASSES_OPTS='-e RAILS_CACHE_CLASSES=true'
RAILS3_OPTS="${CACHE_CLASSES_OPTS}"
RAILS4_OPTS="${CACHE_CLASSES_OPTS} -e BUNDLE_GEMFILE=Gemfile.rails4"
LD_PRELOAD_JEMALLOC='-e LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libjemalloc.so.1'

function stock_ruby() {
  sed -i '' -e 's/FROM.*/FROM ruby\:2\.2\.4/g' Dockerfile  
}

function jemalloc_ruby() {
  sed -i '' -e 's/FROM.*/FROM docker-registry\.zende\.sk\/zendesk\/ruby\:2\.2\.4-2/g' Dockerfile  
}

function benchmark() {
  sleep 30
  zdi mysql reset &> /dev/null
  ruby ./test/load/ticket_creator.rb
  time ruby ./test/load/traffic_creator.rb
}

echo "Rails 3 + malloc:"
stock_ruby
zdi zendesk build -v > /dev/null
zdi zendesk -d restart $RAILS3_OPTS -b > /dev/null
benchmark
echo "Rails 4 + malloc:"
stock_ruby
zdi zendesk build -v > /dev/null
zdi zendesk -d restart $RAILS4_OPTS -b > /dev/null
benchmark
echo "Rails 3 + LD_PRELOAD=libjemalloc.so:"
stock_ruby
zdi zendesk build -v > /dev/null
zdi zendesk -d restart $RAILS3_OPTS $LD_PRELOAD_JEMALLOC -b > /dev/null
benchmark
echo "Rails 4 + LD_PRELOAD=libjemalloc.so:"
stock_ruby
zdi zendesk build -v > /dev/null
zdi zendesk -d restart $RAILS4_OPTS $LD_PRELOAD_JEMALLOC -b > /dev/null
benchmark
echo "Rails 3 + jemalloc compiled in:"
jemalloc_ruby
zdi zendesk build -v > /dev/null
zdi zendesk -d restart $RAILS3_OPTS -b > /dev/null
benchmark
echo "Rails 4 + jemalloc compiled in:"
jemalloc_ruby
zdi zendesk build -v > /dev/null
zdi zendesk -d restart $RAILS4_OPTS -b > /dev/null
benchmark
