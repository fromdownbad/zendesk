#!/bin/bash

set -e
set -xv

# make tests boot and run faster + simulate production
export FORCE_EAGER_LOAD=true

# just a debugging message to verify which Gemfile was used to bundle
echo $BUNDLE_GEMFILE

bundle exec forking-test-runner test \
       --group $1 --groups $2 \
       --parallel $3 \
       --runtime-log test/files/runtime.log \
       --helper test/support/test_helper.rb

# NOTE: to update/record runtime add `--record-runtime amend`
