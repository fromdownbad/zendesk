require 'curb'
require 'json'
require 'csv'

# Change these to the account you want to test
HOST     = 'trial.localhost:3000'
USERNAME = 'agent@zendesk.com'
PASSWORD = '123456'

class UserViewPerformanceTester
  attr_accessor :definitions, :organizations, :locales

  def initialize
    puts "Starting up Customer List Performance tester"
    request_test_data
    run_test
  end

  def request_test_data
    print "Requesting list definitions... "
    response = get_request("user_views/definitions")
    @definitions = response["definitions"]
    puts "DONE"

    print "Requesting Organizations... "
    response = get_request("organizations")
    @organizations = response["organizations"]
    puts "DONE"

    print "Requesting Locales... "
    response = get_request("locales")
    @locales = response["locales"]
    puts "DONE"
  end

  def generate_request(path)
    http = Curl::Easy.new("#{HOST}/api/v2/#{path}.json")
    http.username = USERNAME
    http.password = PASSWORD
    http.headers['Content-Type'] = 'application/json'
    http
  end

  def get_request(path)
    http = generate_request(path)
    http.perform
    JSON.parse http.body_str
  end

  def post_request(path, params)
    http = generate_request(path)
    http.http_post(params.to_json)
    JSON.parse http.body_str
  end

  def get_value(type, definition)
    if type == "dropdown"
      if options = definition['custom_field_options']
        options.first['value']
      elsif definition['key'] == "organization_id"
        organizations.first['id']
      elsif definition['key'] == "locale_id"
        locales.first['id']
      end
    elsif ["text", "textarea", "regexp"].include?(type)
      "ABC"
    elsif type == "tags"
      ["abc", "def"]
    elsif type == "date"
      "2013-09-27"
    elsif type == "integer"
      "300"
    elsif type == "decimal"
      "300.0"
    elsif type == "regexp"
      "ABC"
    elsif type == "checkbox"
      "true"
    end
  end

  def run_test
    CSV.open("customer-lists-#{HOST}.csv", "wb") do |csv|
      execution = {}
      execution["sort"] = definitions.select { |definition| definition['sortable'] }
      execution["group"] = definitions.select { |definition| definition['groupable'] }
      definitions.each do |definition|
        definition['operators'].each do |operator|
          value = ""
          source = definition['key']
          op_key = operator['key']
          unless ["is_present", "is_not_present"].include?(operator['key'])
            value = get_value(operator['type'], definition)
          end
          duration = get_duration(source, op_key, value)
          puts "Duration: #{duration}"
          csv << [source, op_key, value, "none", "none", duration]
          ["group", "sort"].each do |type|
            execution[type].each do |item|
              duration = get_duration(source, op_key, value, {
                type => { "id" => item['key'], "order" => "asc"}
              })
              puts "Duration: #{duration}"
              group = type == "group" ? item['key'] : "none"
              sort = type == "sort" ? item['key'] : "none"
              csv << [source, op_key, value, sort, group, duration]
            end
          end
        end
      end
    end
  end

  def get_duration(source, op_key, value, execution={})
    print "Performing query #{source} #{op_key} #{value}"
    print " #{execution.keys.first} by #{execution.values.first}" if execution.size > 0
    print "... "
    start_time = Time.now
    response = post_request("user_views/preview", "user_view" => {
      "all" => [{
        "field" => source,
        "operator" => op_key,
        "value" => value
      }],
      "execution" => execution
    })
    puts "FAILED!" unless response['rows'].is_a?(Array)
    Time.now - start_time
  end
end

UserViewPerformanceTester.new
