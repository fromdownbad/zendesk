#!/usr/bin/env ruby
# feed with lines from ultragrep that contain the path
# - prints list of visited tickets and exposed emails
# - prints list of visited users and exposed emails
# - prints list of downloaded attachments

require "config/environment"

module SecurityLogAnalyzer
  class << self
    def analyze(account, file)
      lines = File.readlines(file)
      seen_emails = analyze_seen_tickets(account, lines) + analyze_seen_users(account, lines)
      puts "All emails: #{seen_emails.flatten.uniq.sort.join(", ")}"
      analyze_attachments(account, lines)
      nil # clean exit
    end

    def emails(text)
      text.to_s.scan(/[\[\(^\s\=\*<>"':]+@[^\s"':]+\.[^\s><\="':\*\]\)]+/)
    end

    def emails_for_user(user)
      return [] unless user
      [
        user.identities.select { |i| i.is_a?(UserEmailIdentity)}.map(&:value),
        emails(user.organization.try(:notes)),
        emails(user.organization.try(:details)),
        emails(user.try(:notes)),
        emails(user.try(:details)),
      ].flatten.uniq.reject(&:blank?)
    end

    def emails_for_ticket(ticket)
      [
        emails_for_user(ticket.requester),
        emails_for_user(ticket.assignee),
        ticket.collaborators.map { |u| emails_for_user(u) },
        emails(ticket.organization.try(:notes)),
        emails(ticket.organization.try(:details)),
        ticket.assignee.try(:email),
        ticket.collaborators.map { |u| emails_for_user(u) },
        emails(ticket.comments.map(&:body).join(" "))
      ].flatten.uniq.reject(&:blank?)
    end

    def analyze_attachments(account, lines)
      [Attachment, ExpirableAttachment].each do |type|
        puts
        tokens = lines.map { |line| line[%r{/#{type.name.underscore}s/token/(.*?)(/|\?)}, 1] }.compact.uniq
        attachments = type.where(account_id: account.id, token: tokens).to_a
        report_find(tokens, attachments, type.name.underscore)
        attachments.each do |attachment|
          puts "#{type} #{attachment.display_filename} -- #{attachment.s3.authenticated_s3_url}"
        end
      end
    end

    def analyze_seen_users(account, lines)
      user_ids = lines.map { |line| line[%r{/users/(\d+)}, 1] }.uniq
      users = account.users.where(id: user_ids).to_a
      report_find(user_ids, users, "user")
      all = users.map { |user| [user, emails_for_user(user)] }
      all.map { |user, emails| puts "User #{user.id} - #{user.name}: #{emails.join(", ")}" }
      puts
      all.map(&:last).flatten
    end

    def analyze_seen_tickets(account, lines)
      ticket_ids = lines.map { |line| line[%r{/tickets/(\d+)}, 1] }.uniq
      tickets = account.tickets.where(nice_id: ticket_ids).to_a
      report_find(ticket_ids, tickets, "ticket")
      all = tickets.map { |ticket| [ticket, emails_for_ticket(ticket)] }
      puts all.map { |ticket, emails| "Ticket #{ticket.id}: #{emails.join(", ")}" }
      puts
      all.map(&:last).flatten
    end

    def report_find(ids, objects, name)
      puts "Saw #{ids.size} #{name}"
      diff = ids.size - objects.size
      puts "Did not find #{diff} #{name.pluralize}" if diff > 0
    end
  end
end

subdomain = (ARGV[0] || raise("pass me the account subdomain"))
file = (ARGV[1] || raise("pass me the logfile"))
a = Account.find_by_subdomain(subdomain)
a.on_shard do
  SecurityLogAnalyzer.analyze(a, file)
end

# cat tumblr_traffic.log | pru 'self[/GET ([^\s]+)/, 1]' | grep -v '/api/v1/tickets/' | grep -v '/api/v2/users/' | grep -v '/search' | grep -v '/system' | grep -v '/tracker' | grep -v '/tickets/' | grep -v '/users/' | grep -v '/node/radar/presenc' | grep -v '/agent/version' | grep -v '/widgets/' | grep -v '/generated/' | grep -v '/images/' | grep -v '/home' | grep -v '/api/v1/stats/graph_data/'
