#!/usr/bin/env ruby
# Run this like so:
#   ./script/create_suspended_ticket dev Foo foo@zendesk.com
# or:
#   ./script/create_suspended_ticket dev Foo foo@zendesk.com unverified
#
# Available types (see types.rb in zendesk_core for up to date list):
#    spam
#    not_on_whitelist
#    unverified
#    auto_mailer
#    loop
#    unknown_user_update
#    other_user_update
#    auto_delivery_failure
#    auto_vacation
#    system_user
#    unknown_author
#    blank
#    signup_required
#    blacklisted
#    backscatter
#    disabled
#    to_noreply
#    server_lock
#    unserviceable_account
#    unprocessable_email
#    unauthenticated_email_update

raise "This script is intented for local development only." if Rails.env.to_s != "development"

if ARGV.size < 3
  puts "usage: created_suspended_tickets subdomain from_name from_email [type]"
  exit 1
end

require_relative '../config/environment.rb'

subdomain, name, email, type = ARGV

type ||= "spam"

account = Account.find_by_subdomain(subdomain)

account.on_shard do
  s = account.suspended_tickets.create!(
    :subject => "test #{email}",
    :recipient => "support@dev.localhost",
    :from_name => name ,
    :from_mail => email,
    :cause => SuspensionType.find!(type),
    :content => "I am an apple #{email}"
  )

  s.brand = account.default_brand
  s.save!
end
