#!/bin/bash

set -e

# Check if any file exists for an expanded glob.
expanded_glob_exists() {
  [ -e "$1" ]
}
export -f expanded_glob_exists

# Return un-expanded glob if no files match the expanded glob.
glob_exists() {
  expanded_glob_exists $1 || echo "$1"
}
export -f glob_exists

NON_EXISTING_GLOBS=$(grep -v '^#' .github/CODEOWNERS | awk '{ print $1 }' | xargs -I@ bash -c 'glob_exists "@"')

if [ -z "$NON_EXISTING_GLOBS" ]; then
  exit 0
else
  echo Some patterns in .github/CODEOWNERS could not be found:
  echo "$NON_EXISTING_GLOBS"
  exit 1
fi
