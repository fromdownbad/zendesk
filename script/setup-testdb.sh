set -e

# Always at least one DB needed
TEST_ENV_NUMBER='' bundle exec rake db:load_config db:test:load zendesk:load_timezones 2>&1 > /tmp/test_env.log &

# Create and populate more DBs if necessary
for i in `seq 2 $TEST_GROUP_PARALLELISM`
do
  TEST_ENV_NUMBER=$i bundle exec rake db:load_config db:test:load zendesk:load_timezones 2>&1 > /dev/null &
done

wait

echo "Logs for loading TEST_ENV_NUMBER 1"
cat /tmp/test_env.log
