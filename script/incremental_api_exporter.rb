#!/usr/bin/env ruby

# Export script for exporting data as a system user. This can be used to generate sample exports for GoodData.
# Requirements:
# A checkout of a Zendesk project that has the zendesk_configuration and zendesk_internal_api_client gems
# A link to a config/zendesk.yml that contains the correct secret for the environment that you want the extract from
#
# A production export can be obtained from an admin server using Carson
#
# A dev export can be obtained by specifying all parameters
# ruby incremental_api_exporter.rb dev GOODDATA users 0 dev.zendesk.dev http

require 'tmpdir'
require 'fileutils'
require 'bundler/setup'
require 'json'
require 'zendesk_configuration'
require 'zendesk/internal_api/client'

subdomain = ARGV[0]
user = ARGV[1]
object = ARGV[2]
start_time = ARGV[3]
host = ARGV[4] || "#{subdomain}.zendesk.com"
scheme = ARGV[5] || 'https'
environment = ENV['RAILS_ENV'] || 'development'

unless subdomain && user && object && start_time && host && scheme
  $stderr.puts "Usage: ruby safe_export.rb <subdomain> <system_user> <export_object> <start_time> [full domain] [url scheme]"
  exit 1
end

next_page = "/api/v2/incremental/#{object}.json?start_time=#{start_time}"
end_time = -1
@export_dir = File.join(Dir.tmpdir, object, Time.now.to_i.to_s)
puts "Writing files to: #{@export_dir}"
FileUtils.mkdir_p @export_dir

Zendesk.config.setup(:env => environment)

out_file = File.open(File.join(@export_dir, "full_export.json"), 'w+')

loop do
  puts "Getting #{object} with start time #{start_time}"

  begin
    # recreate client every request so that faraday doesn't keep caching and leak memory
    client = Zendesk::InternalApi::Client.new(subdomain, :user => user, :host => host, :scheme => scheme)

    res = client.connection.get next_page
    body = res.body
    end_time = body['end_time']
    next_page = body['next_page']
    puts "COUNT= #{body['count']}"
    puts "END_TIME= #{end_time}"
    body[object].each { |obj| out_file.puts(JSON.dump(obj)) }

    break if start_time == end_time
    start_time = end_time
  rescue => ex
    $stderr.puts
    $stderr.puts "Failed to export:"
    $stderr.puts "Requested url: #{next_page}"
    $stderr.puts "Exception: #{ex.message}"
    $stderr.puts ex.backtrace.join("\n")
    exit 1
  end
end

out_file.close
