#!/usr/bin/env ruby

# https://confluence.atlassian.com/doc/children-display-macro-139501.html
# https://confluence.atlassian.com/doc/page-layouts-columns-and-sections-275188613.html
# https://confluence.atlassian.com/doc/confluence-wiki-markup-251003035.html
# https://jira.atlassian.com/secure/WikiRendererHelpAction.jspa?section=all
# https://zendesk.atlassian.net/wiki/pages/editpage.action?pageId=261563514

# todo:
# implement better verbosity controls
# handle merge commits without merge messages / some kind of fallback mechanism
# add team mapping https://github.com/orgs/zendesk/teams?utf8=%E2%9C%93&query=%40user-name
# handle file renaming
# handle reverts
# QA test session support

require 'open3'
require 'optparse'
require 'octokit'
require 'json'

class String ; def presence ; self.strip.empty? ? nil : self ; end ; end
@verbosity = 3

def system_call!(*args)
  puts "Executing system call -- #{args.join(' ').inspect}" if @verbosity > 1
  stdout, stderr, status = Open3.capture3(*args)
  if (status.exitstatus != 0) || (stderr != "")
    raise StandardError.new("Unexpected errors during execution: #{args.inspect}; #{status.exitstatus}; #{stderr}; PWD: #{Dir.pwd}")
  end
  stdout
end

def change_delta(to, *discards)
  discard_list = discards.map{|d| d.to_s.strip }.select{|d| d != "" }.map{|x| "^#{x}" }
  changes = system_call!("git","rev-list",to,*discard_list,"--first-parent").lines.map(&:chomp!)
  puts "Found #{changes.count} change events" if @verbosity > 0
  changes
end

def parse_change(repo, sha)
  puts "parse_change(#{repo},#{sha})"
  lines = system_call!("git","cat-file","-p",sha).split("\n")
  parent = 0
  change = {repo: repo, sha: sha}
  while line = lines.shift do
    key, value = line.split(" ", 2)
    case key
    when "tree"
      change[:tree] = value
    when "parent"
      if change[:parent].nil?
        change[:parent] = value
        change[:diff] = parse_diff(change[:parent], change[:tree])
      else
        change[:merge] ||= []
        change[:merge] << value
      end
    when "author"
      change[:author] = value
    when "committer"
      change[:committer] = value
    when "gpgsig" # don't really care about gpgsigs
      loop do
        line = lines.shift
        break if line =~ /END.*SIGNATURE/
      end
    when nil
      change[:message] = []
      while line = lines.shift do
        if line =~ /Merge pull request #(\d+)/
          change[:pr_id] = $1
        elsif line.strip != ""
          change[:message] << line
        end
      end
    else
      puts "Unexpected #{key.inspect} #{value.inspect}"
    end
  end
  change
end

def parse_diff(sha1, sha2)
  files = Hash.new{|h,k| h[k] = {} }
  diff_stats = system_call!("git","diff","--numstat","--summary",sha1,sha2)
  diff_stats.split("\n").each do |line|
    case line
    when /(\d+)\s+(\d+)\s+(.*)/
      puts "Adds #{$1}, dels #{$2}, file #{$3}"
      files[$3][:type] = :text
      files[$3][:adds] = $1.to_i
      files[$3][:deletes] = $2.to_i
    when /-\s+-\s+(.*)/
      puts "Change of binary file #{$1}"
      files[$1][:type] = :binary
    when /delete mode \d+ (.*)/
      puts "Deleted file #{$1}"
      files[$1][:deleted] = true
    when /create mode \d+ (.*)/
      puts "Created file #{$1}"
      files[$1][:created] = true
    when /rename (.*)/
      puts "renamed file #{$1}"
    else
      puts "Unexpected line [#{line}]"
    end
  end
  files
end

def add_tag_data(change)
  tag_data = system_call!("git","tag","--points-at",change[:sha])
  puts "tag(s) pointing at #{change[:sha]} -- #{tag_data.inspect}" if @verbosity > 1
  change[:tags] = tag_data.strip.split("\n")
end

def add_cherry_data(change, from)
  cherry_data = system_call!("git","cherry",from,change[:sha],change[:parent])
  puts "cherry data for #{change[:sha]} -- #{cherry_data.inspect}" if @verbosity > 1
  change[:is_change] = cherry_data.strip.split("\n").reject{|x| x.start_with?("- ") }.any?
end

def add_github_data(client, change)
  if change[:pr_id]
    begin
      puts "Fetching https://github.com/#{change[:repo]}/pull/#{change[:pr_id]} for #{change[:sha]}"
      change[:pr] = client.pull_request(change[:repo], change[:pr_id])
      change[:title] = change[:pr].title.presence
      change[:body] = change[:pr].body.presence
    rescue => e
      puts "Unable to retrieve info from github for #{change.inspect} because #{e}"
    end
  else
    puts "No PR found for #{change.inspect}"
  end
  change[:title] ||= change[:message][0].presence || "&#128169;"
  change[:body] ||= change[:message].join("\n").presence || "&#128169;" # entirely missing commit messages are shit
  change[:risk_text]  = (change[:body].match(/^\W*risks?(.*)/im) && $1.strip) || change[:body]
end

def add_risk_scores(change)
  self_identified_risk = SELF_IDENTIFIED_RISK_MAPPING[(change[:risk_text].match(/(high|moderate|medium|low|none)/i) && $1).to_s.downcase]

  important_diff = change[:diff].reject { |file,stats| file.start_with?("test/") && (stats[:adds].to_i >= stats[:deletes].to_i) }
  file_count_risk = case important_diff.size
    when 0 then 0.0
    when 1..4 then 1.0
    when 4..8 then 2.0
    else 3.0
    end

  line_change_risk = 0.0
#  line_change_risk = case (change[:total_adds] + change[:total_deletes])
#    when 0 then 0.0
#    when 1..20 then 1.0
#    when 21..100 then 2.0
#    else 3.0
#    end

  risk_score = [self_identified_risk, file_count_risk, line_change_risk].max
  change[:risk_score] = risk_score
  change[:self_identified_risk] = self_identified_risk
end

SELF_IDENTIFIED_RISK_MAPPING = { "high" => 3.0, "moderate" => 2.0, "medium" => 2.0, "low" => 1.0, "" => 0.0, "none" => 0.0 }.freeze

def jira_escaped(str)
  str.
    gsub(/^\s*\*/, '\\*').
    gsub(/^\s*-/, '\\-').
    gsub(/(\r?\n)/, " \\\\\\\\ ").
    gsub('|', "\\|").
    gsub('#', "\\#").
    gsub('{', "\\{").
    gsub('}', "\\}").
    gsub('[', "\\[").
    gsub(']', "\\]")
end

def change_text(change)
  text = []
  text << if change[:pr]
    "[#{change[:pr_id]}|#{change[:pr].html_url}##{change[:sha][0..8]}]"
  else
    "[#{change[:sha][0..8]}|https://github.com/#{change[:repo]}/commit/#{change[:sha]}]"
  end

  text << change[:tags].join(", ") if change[:tags].any?

  flags = []
  flags << "&#127826;" if !change[:merge]
  flags << "&#127937;" if !change[:is_change]
  text << flags.join(" ") if flags.any?

  text.join(" \\\\")
end

def author_text(change)
  _, author_name, author_email = *change[:author].match(/([^<]*)\s*<?([^>]*)/)
  if change[:pr]
    "[#{author_name}|#{change[:pr].user.html_url}]"
  else
    "[#{author_name}|mailto:#{author_email}]"
  end
end

def risk_text(change)
  human_label = case change[:risk_score]
    when (0.0...1.0) then "None"
    when (1.0...2.0) then "Low"
    when (2.0...3.0) then "Medium"
    else "High"
    end

  risk_summary = [change[:risk_score], human_label]

  discrepancy = (change[:risk_score] - change[:self_identified_risk])
  if discrepancy > 0.0
    risk_summary << ((discrepancy >= 2.0) ? "&#128561;" : "&#128560;")
  end

  risk_summary.join(" \\\\")
end

def stats_text(change)
  summary = [0,0,0]
  change[:diff].each do |file,stats|
    summary[0] += 1
    summary[1] += stats[:adds].to_i
    summary[2] += stats[:deletes].to_i
  end
  change[:diff].
    map{|d| "|#{d[0]}|#{d[1]}|" }.
    join("\n").
    prepend("{expand:#{summary[0]} files, #{summary[1]} adds, #{summary[2]} deletes}||FILE||STATS||\n").
    concat("{expand}")
end

def summary_text(change)
  jira_escaped("#{change[:title].strip}\n\n\n#{change[:risk_text].strip}\n\n")
end

def markup_change_data(changes)
  changes.map do |change|
    [ change_text(change),
      risk_text(change),
      author_text(change),
      summary_text(change) + stats_text(change)
    ].join("|")
  end.join("|\n|").prepend("||CHANGE||RISK||AUTHOR||SUMMARY||\n|").concat("|")
end

def markup_test_session_data(changes)
  changes.select{|c| c[:is_change] && c[:risk_score] >= 2.0 }.map do |change|
    [ change_text(change),
      risk_text(change),
      author_text(change),
      summary_text(change),
      " ",
      " "
    ].join("|")
  end.join("|\n|").prepend("||CHANGE||RISK||AUTHOR||SUMMARY||NOTES||READY FOR RELEASE?||\n|").concat("|")
end

if __FILE__ == $0

  params = {}
  opt_parse = OptionParser.new do |opts|
    opts.banner = "Usage: this [options]"
    opts.separator ""
    opts.separator "Specific options:"
    opts.on("-cFILE",   "--config=FILE", "Configuration file with API key, team information, etc") { |conf| params[:conf] = conf }
    opts.on("-fOBJECT", "--from=OBJECT", "The revision/tag/sha/branch/whatever we're moving from") { |from| params[:from] = from }
    opts.on("-tOBJECT", "--to=OBJECT", "The revision/tag/sha/branch/whatever we're moving to") { |to| params[:to] = to }
    opts.on("-lOBJECT", "--limit=OBJECT", "The revision/tag/sha/branch/whatever to stop at on from") { |limit| params[:limit] = limit }
    opts.on("-kKEY",    "--key=KEY", "Your github access token key") { |token| params[:github_token] = token }
    opts.on("-sOBJECT", "--ts=OBJECT", "Test Session output") { |ts| params[:ts] = ts }
    opts.on_tail("-h",  "--help", "Show this message") { puts(opts) and exit }
  end

  begin
    opt_parse.parse!
    #mandatory = [:from, :to]
    #missing = mandatory.select { |param| params[param].nil? }
    #raise OptionParser::MissingArgument.new(missing.join(', ')) unless missing.empty?
  rescue OptionParser::InvalidOption, OptionParser::MissingArgument
    puts $!.to_s
    puts opt_parse
    exit
  end

  conf_path = File.expand_path(params[:conf] || "~/.changeaudit")
  conf_file = File.open(conf_path, File::CREAT | File::RDWR)
  conf_data = JSON.parse(conf_file.read.presence || "{}")

  github_token = params[:github_token] || conf_data["github_token"]
  github_client = Octokit::Client.new(access_token: github_token)

  remote_origin_url = system_call!("git","config","remote.origin.url").strip
  repo = remote_origin_url.match(%r{^(?:https://github.com|git@github.com:)?/?(.*[^.git])})[1]
  raise StandardError.new("Unknown/unexpected remote #{remote_url}") unless repo

  changes = change_delta(params[:to], params[:from], params[:limit]).
    map { |sha| parse_change(repo, sha) }.
    each { |change| add_tag_data(change) }.
    each { |change| add_cherry_data(change, params[:from]) }.
    each { |change| add_github_data(github_client, change) }.
    each { |change| add_risk_scores(change) }

  markup = markup_change_data(changes)
  if params[:ts]
    markup << "\n\nTEST SESSION MARKUP:\n\n"
    markup << markup_test_session_data(changes)
  end

  puts ">----- BUCKLE UP FOR JIRA MARKDOWN TIME -----<\n\n\n"
  puts "Legend:"
  puts "&#127826; - Change was not a merge (e.g. a cherry-pick)"
  puts "&#127937; - Change detected on target branch (e.g. already in Production)"
  puts "&#128560; - Minor discrepancy detected"
  puts "&#128561; - Major discrepancy detected"
  puts markup
  "#{changes.count} changes" #, #{changes.map{|c| c[:risk_score] }.reduce(&:+)} aggregate risk score"
  puts "\n\n\n"

end
