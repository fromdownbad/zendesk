#!/usr/bin/env ruby
# Ref: https://zendesk.atlassian.net/wiki/spaces/QA/pages/95879584/Creating+an+account+via+API+Dev+Master+Staging
# A simple script to create an account in specified env.
# Usage: ./script/create_account [options]
#     -e, --environment ENV
# Envs: master|staging|new-staging|production|development
#     -s, --subdomain Z3NSUBDOMAIN
#     -n, --name BEN
#     -m, --email EMAIL
#     -p, --pod[=998]
#     -d, --shard[=99801]
# If specifying a shard or a pod, make sure your env matches the shard correctly

require 'optparse'

options = {}

optparse = OptionParser.new do |opts|
    opts.banner = "Usage: #{__FILE__} [options]"
    opts.on("-e", "--environment ENV", String) { |val| options[:environment] = val }
    opts.separator "Envs: master|staging|new-staging|production|development"
    opts.on("-s", "--subdomain Z3NSUBDOMAIN", String) { |s| options[:subdomain] = s }
    opts.on("-n", "--name BEN", String) { |n| options[:name] = n }
    opts.on("-m", "--email EMAIL", String) { |e| options[:email] = e }
    opts.on("-p", "--pod [998]", Integer) { |p| options[:pod] = p }
    opts.on("-d", "--shard [99801]", Integer) { |d| options[:shard] = d }
    opts.separator "If specifying a shard or a pod, make sure your env/pod/shard matches correctly"
    opts.separator "You should not specify a shard without the pod"
end

optparse.parse!
mandatory = [:environment, :subdomain, :name, :email]
missing = mandatory.select { |k| options[k].nil? }
if missing.any?
  puts "Missing params: #{missing.join(",")}"
  puts optparse
  exit 1
end

if options[:shard] && options[:pod].nil?
  puts "Specify a pod and a shard together for selection. You can also just specify a pod and the shard will be picked for you"
  puts optparse
  exit 1
end

options[:signup] = case options[:environment]
                   when "development"
                     "https://support.zd-dev.com"
                   when "staging"
                     "https://signup.zendesk-staging.com"
                   when "production"
                     "https://signup.zendesk.com"
                   end

cmd = "curl -v"

cmd += " -d \"owner[name]=#{options[:name]}\""
cmd += " -d \"owner[email]=#{options[:email]}\""
cmd += " -d \"owner[password]=12345\""
cmd += " -d \"owner[is_verified]=1\""
cmd += " -d \"address[phone]=2125551234\""
cmd += " -d \"account[name]=#{options[:subdomain]}\""
cmd += " -d \"account[subdomain]=#{options[:subdomain]}\""
cmd += " -d \"account[help_desk_size]=Large group\""
cmd += " -d \"account[language]=en\""
cmd += " -d \"force_pod_id=#{options[:pod]}\"" if options[:pod]
cmd += " -d \"account[_zd_choose_shard_udupi]=#{options[:shard]}\"" if options[:shard]
cmd += " -d \"terms=terms_and_things\""
cmd += " #{options[:signup]}/accounts.json"

puts "Executing this curl API: #{cmd}"
system(cmd)
