require 'date'

class Zendesk < FPM::Cookery::Recipe
  description 'Zendesk AKA Classic'

  name     'zendesk'
  version  ENV['VERSION'].to_s
  homepage 'http://github.com/zendesk/zendesk'
  source   File.expand_path("../fpm-src", Dir.pwd).to_s, with: :directory

  fpm_attributes["#{FPM::Cookery::Facts.target}_user".to_sym]   = 'zendesk'
  fpm_attributes["#{FPM::Cookery::Facts.target}_group".to_sym]  = 'zendesk'

  def build
  end

  def install
    %w[config log releases shared unicorn].each { |dir| root("data/zendesk/#{dir}").mkpath }
    # Change into the app directory so it isn't included as a sub-directory
    Dir.chdir('fpm-src')
    dirs = Dir.glob("*", File::FNM_DOTMATCH) - %w[. .. fpm log test .git]

    release_name = "#{DateTime.now.strftime('%Y%m%d-%H%M%S')}-#{version}"
    root("/data/zendesk/releases/#{release_name}/").install dirs

    with_trueprefix do
      create_post_install_hook <<-BASH
        #!/bin/bash

        set -x
        exec 2>/var/log/zendesk-dpkg-post-install.log

        APPLICATION_NAME=zendesk
        RELEASE_NAME="#{release_name}"
        RELEASE_PATH=/data/$APPLICATION_NAME/releases/$RELEASE_NAME
        ENV_FILE_PATH=/data/$APPLICATION_NAME/.env

        # Move the .env file from where the bootstrap script left it
        test -f "$ENV_FILE_PATH" && mv "$ENV_FILE_PATH" "$RELEASE_PATH"

        # Create config symlinks
        for CONFIG_FILE in /data/zendesk/config/*
        do
          FILENAME=$(basename -- "$CONFIG_FILE")
          ln -Tsf $CONFIG_FILE $RELEASE_PATH/config/$FILENAME
        done


        # Create 'log' symlink
        ln -Tsf /data/$APPLICATION_NAME/log $RELEASE_PATH/log


        # Create the 'current' symlink
        CURRENT_PATH=$(readlink /data/$APPLICATION_NAME/current) || true
        # Set $CURRENT_PATH to (none) if no symlink exists yet
        if [ -z $CURRENT_PATH ]; then CURRENT_PATH='(none)'; fi
        echo "Updating the symlink from $CURRENT_PATH to $RELEASE_PATH"
        ln -Tsf $RELEASE_PATH /data/$APPLICATION_NAME/current

        # Bundle install yo
        cd $RELEASE_PATH
        /opt/rbenv/shims/bundle install --jobs 4 --local --deployment --quiet --path vendor/bundle --without development test

        /usr/local/bin/reload_services zendesk
      BASH
    end
  end

  def create_post_install_hook(script)
    File.open(builddir('post-install'), 'w', 0755) do |f|
      f.write script.gsub(/^\s+/, '')
      self.class.post_install(File.expand_path(f.path))
    end
  end

  def create_pre_uninstall_hook(script, interpreter = "/bin/sh")
    File.open(builddir('pre-uninstall'), 'w', 0755) do |f|
      f.write "#!#{interpreter}\n" + script.gsub(/^\s+/, '')
      self.class.pre_uninstall(File.expand_path(f.path))
    end
  end
end
