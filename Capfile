require 'bundler/setup'
require 'zendesk/deployment'
require 'zendesk/deployment/tasks/kubernetes'
require 'datadog/statsd'
require 'octokit'
require_relative 'config/deploy'

set(:application, 'zendesk')
set(:repository, 'git@github.com:zendesk/zendesk.git')
set(:default_env, 'NOKOGIRI_USE_SYSTEM_LIBRARIES' => '1')
set(:kubernetes_service_name, 'classic')
set(:app_host_role, :app)
set(:check_for_pending_migrations?, false)

class ApplicationDiscovery < Zendesk::Deployment::ApplicationDiscovery
  MIGRATION_ROLES_MAPPED = {}

  # interesting hosts
  def role_mapping(n)
    mapping = super

    if mapping[:proxy]
      mapping[:proxy][:no_release] = true
    elsif !MIGRATION_ROLES_MAPPED[n['pod']] && mapping[:app] && n['host'] !~ /dbadmin/
      mapping[:db] = { primary: true }
      MIGRATION_ROLES_MAPPED[n['pod']] = true
    end

    mapping
  end
end

set(:statsd_client, -> { Datadog::Statsd.new(ENV.fetch('STATSD_HOST'), ENV.fetch('STATSD_PORT'), namespace: 'zendesk.classic.deploy') })

set(:application_discovery, -> {
  ApplicationDiscovery.new(
    environment_discovery: fetch(:environment_discovery),
    project_name: fetch(:deploy_project_name)
  )
})

set(
  :app_configs,
  %w[
    attachments.yml
    database.yml
    gooddata.yml
    gooddata_gpg.key
    mail.yml
    networks.yml
    radar.yml
    redis_clusters.yml
    remote_files.yml
    resque-pool.yml
    resque.yml
    stats.yml
    uploader_configuration.yml
    zendesk.yml
    zopim.yml
  ]
)

namespace :zendesk do
  task :notify_datadog_of_restart do
    begin
      event_name = "#{ENV['DEPLOYER_NAME']} is restarting Classic with version #{ENV['REFERENCE']}"
      event_description = "Classic is being restarted with version #{ENV['REFERENCE']} in the following deploy groups: #{ENV['DEPLOY_GROUPS']}\n#{ENV['DEPLOY_URL']}"
      event_tags = ['zendesk', 'deploy_restart'] + Array(ENV['ZD_DEPLOY_RESTART_EVENT_TAGS'].split(','))
      fetch(:statsd_client).event(event_name, event_description, tags: event_tags, alert_type: 'info')
    rescue => e
      warn "Failed to notify datadog of restart: #{e.message}"
    end
  end

  task :coverband_clear do
    on! roles(:dbadmin) do
      within fetch(:current_path) do
        with skip_extra_log_files: 1 do
          execute(:bundle, 'exec rake coverband:clear')
        end
      end
    end
  end

  task :coverband_baseline do
    on! roles(:dbadmin) do
      within fetch(:current_path) do
        with skip_extra_log_files: 1 do
          execute(:bundle, 'exec rake coverband:clear')
          execute(:bundle, 'exec rake coverband:baseline')
        end
      end
    end
  end

  task :coverband_run do
    require_relative 'lib/zendesk/coverband_helpers'
    # Collect coverband reports from each pod
    cb_reports = [{}]
    on! roles(:dbadmin) do
      within fetch(:current_path) do
        with skip_extra_log_files: 1 do
          cb_reports << YAML.load(capture(:bundle, 'exec rake zendesk:coverband'))
        end
      end
    end

    # Aggregate reports from all pods
    aggregated_report = cb_reports.reduce { |total_report, partial_report| Coverband::Reporters::Base.merge_existing_coverage(partial_report, total_report) }

    # TODO: remove this step by adding root path regex to config
    # Sanitize coverband data to have the same absolute path as current host
    sanitized_report = aggregated_report.map { |file, data| [file.sub(%r{^/data/zendesk/releases/.*/}, "#{Zendesk::Coverband.rails_root}/"), data] }.to_h

    # Generate Simplecov Report
    Zendesk::Coverband.simplecov_result(sanitized_report).format!
    logger.info "report is ready and viewable: open #{Zendesk::Coverband.rails_root}/#{SimpleCov.coverage_dir}/index.html"
    S3ReportWriter.new(Coverband.configuration.s3_bucket).persist! if Coverband.configuration.s3_bucket
  end

  namespace :branch do
    github = -> { Octokit::Client.new(access_token: ENV.fetch('GITHUB_TOKEN')) }

    repo = 'zendesk/zendesk'
    media_type = 'application/vnd.github.luke-cage-preview+json'

    %i[staging production].each do |branch|
      namespace branch do
        desc "Protect the #{branch} branch in GitHub"
        task :protect do
          github.call.protect_branch(
            repo,
            branch.to_s,
            enforce_admins: true,
            required_pull_request_reviews: {required_approving_review_count: 1},
            required_status_checks: {
              strict: true,
              contexts: ['Travis CI - Pull Request', 'docker-images-180022'],
            },
            accept: media_type
          )
        end

        desc "Unprotect the #{branch} branch in GitHub"
        task :unprotect do
          begin
            github.call.unprotect_branch(repo, branch.to_s, accept: media_type)
          rescue Octokit::BranchNotProtected
            puts "branch not protected, continuing anyway"
          end
        end
      end
    end
  end
end

coverband_tasks = %w[zendesk:coverband_clear zendesk:coverband_baseline zendesk:coverband_run]
coverband_tasks.each do |task|
  before task, 'deploy:establish_ssh_connection'
  before task, 'deploy:setup'
end

after 'deploy:prepare_project', 'zendesk:update_assets'
after 'deploy:prepare_project', 'zendesk:timestamps_for_release'

after 'deploy:fetch_archive_from_mirror', 'zendesk:update_config'
before 'deploy:create_symlink', 'upload_env_file'
before 'deploy:create_symlink', 'zendesk:upload_assets'

before 'deploy:restart', 'zendesk:notify_datadog_of_restart'

after 'deploy:setup', 'zendesk:setup_cache_folders'
