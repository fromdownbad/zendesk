require_relative '../support/test_helper'

SingleCov.covered!

describe GroupDomainEventObserver do
  fixtures :groups

  let(:group) do
    groups(:minimum_group).tap do |org|
      org.domain_event_publisher = domain_event_publisher
    end
  end

  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  describe 'on_save' do
    describe 'with events to emit' do
      before do
        group.id = 123
      end

      describe_with_arturo_enabled :publish_group_domain_events do
        it 'does create events' do
          group.publish_group_events_to_bus!
          domain_event_publisher.events.length.must_equal 1
        end

        describe 'with a publishing error' do
          it 'logs and supresses exceptions' do
            group.expects(:group_events_encoder).raises(RuntimeError)
            Zendesk::StatsD::Client.any_instance.expects(:increment).once
            group.publish_group_events_to_bus!
          end
        end

        describe 'statsd reporting' do
          it 'records domain event counts' do
            Zendesk::ModelChangeInstrumentation.any_instance.stubs(:report_change_source_metrics)
            Zendesk::StatsD::Client.any_instance.expects(:increment).once
            group.publish_group_events_to_bus!
          end

          it 'records mesage size stats' do
            Zendesk::StatsD::Client.any_instance.expects(:histogram).once
            group.publish_group_events_to_bus!
          end
        end
      end

      describe_with_arturo_disabled :publish_group_domain_events do
        it 'does not create events' do
          group.publish_group_events_to_bus!
          domain_event_publisher.events.length.must_equal 0
        end
      end
    end

    describe 'with no events to emit' do
      describe_with_and_without_arturo_enabled :publish_group_domain_events do
        before do
          GroupEventsProtobufEncoder.any_instance.stubs(:to_a).returns([])
        end

        it 'does not create events' do
          group.publish_group_events_to_bus!
          domain_event_publisher.events.length.must_equal 0
        end
      end
    end
  end
end
