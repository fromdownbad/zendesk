require_relative "../support/test_helper"

SingleCov.covered!

describe CustomFieldValueObserver do
  fixtures :cf_values

  describe "when updating a value with organization owner" do
    let(:field_value) { cf_values(:org_integer1) }
    let(:current) { '-000000000002' }

    it "adds domain event to domain_events of owner" do
      Organization.any_instance.stubs(:publish_organization_events_to_bus!)
      field_value.value = current
      field_value.save!
      assert_equal 1, field_value.owner.domain_events.length
    end
  end

  describe "when destroying a value with organization owner" do
    let(:field_value) { cf_values(:org_integer1) }

    it "adds domain event to domain_events of owner" do
      Organization.any_instance.stubs(:publish_organization_events_to_bus!)
      owner = field_value.owner
      field_value.destroy!
      assert_equal 1, owner.domain_events.length
    end
  end

  describe "when not update the value" do
    let(:field_value) { cf_values(:text1) }

    it "add domain event to domain_events in field_value" do
      field_value.touch
      assert_equal 0, field_value.owner.domain_events.length
    end
  end
end
