require_relative "../support/test_helper"

SingleCov.covered!

describe AccountRestrictionObserver do
  fixtures :activities

  let!(:account) { Account.new }
  let!(:record) { activities(:minimum_activity_1) }
  let(:observer) { AccountRestrictionObserver }

  def find
    record.class.find(record.id)
  end

  before do
    account.stubs(:has_account_restriction_on_read?).returns(true)

    Zendesk.stubs(:current_account).returns(account) # wrong account

    Rails.env.stubs(:production?).returns(true)
  end

  describe "#after_find" do
    it "raises exceptions when account_restriction_on_read is enabled" do
      assert_raises(AccountRestrictionError) { find }
    end

    it "does not raise when account_restriction_on_read is disabled" do
      account.unstub(:has_account_restriction_on_read?)
      find
    end

    it "does not raise when silenced" do
      observer.silence { find }
    end
  end

  describe "#before_save" do
    it "raises" do
      assert_raises(AccountRestrictionError) do
        record.save
      end
    end
  end

  describe "#silence" do
    before do
      observer.silenced = nil
    end

    after do
      # cleanup for other tests
      observer.silenced = nil
    end

    it "silences" do
      silenced = 0
      observer.silence { silenced = observer.silenced }
      assert(silenced)
    end

    it "resets silenced afterwards" do
      observer.silenced = false
      observer.silence {}
      assert_equal false, observer.silenced

      observer.silenced = true
      observer.silence {}
      assert(observer.silenced)
    end

    it "returns block result" do
      assert_equal 1, observer.silence { 1 }
    end
  end
end
