require_relative "../support/test_helper"

SingleCov.covered!

describe UserGuideEntitlementsChangesObserver do
  fixtures :users

  let(:user) { users(:minimum_agent) }
  let(:context) { Object.new }
  let(:instrumentation) { stub }

  before do
    @actor = users(:minimum_admin)
    CIA.stubs(:current_actor).returns(@actor)
    user.stubs(:binding).returns(context)
    user.stubs(:instrument_general_column_changes)
  end

  it 'calls Zendesk::ModelChangeInstrumentation to perform' do
    Zendesk::ModelChangeInstrumentation.expects(:new).with(
      model: user,
      exec_context: context,
      columns_to_instrument: ['is_moderator', 'roles', 'permission_set_id'],
      source_of_instrument: 'guide_user_change'
    ).once.returns(instrumentation)
    instrumentation.expects(:perform).once

    user.save!
  end
end
