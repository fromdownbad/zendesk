require_relative "../support/test_helper"

SingleCov.covered!

describe BrandLogoObserver do
  describe "brand publishing" do
    let(:logo) { FactoryBot.create(:brand_logo, filename: "small.png") }

    describe "without publish_brand_entities settings activated" do
      it "doesn't call brand publisher" do
        logo.account.stubs(:has_publish_brand_entities?).returns(false)

        BrandPublisher.any_instance.expects(:publish_for_brand).never

        logo.update_attributes!(thumbnail: 'small')
      end
    end

    describe "with publish_brand_entities settings activated" do
      it 'calls brand publisher when the logo is updated' do
        logo.account.stubs(:has_publish_brand_entities?).returns(true)

        BrandPublisher.any_instance.expects(:publish_for_brand).with(brand_id: logo.brand_id)

        logo.update_attributes!(thumbnail: 'small')
      end

      it 'calls brand publisher when the logo is deleted' do
        logo.account.stubs(:has_publish_brand_entities?).returns(true)

        BrandPublisher.any_instance.expects(:publish_for_brand).with(brand_id: logo.brand_id)

        logo.destroy
      end
    end
  end
end
