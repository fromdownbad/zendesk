require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe CertificateObserver do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:certificate) { account.certificates.first }

  describe "brand publishing" do
    describe "without publish_brand_entities settings activated" do
      before do
        account.stubs(:has_publish_brand_entities?).returns(false)
      end

      it "doesn't call brand publisher" do
        BrandPublisher.any_instance.expects(:publish_for_account).never

        certificate.autoprovisioned = true
        certificate.save
      end
    end

    describe "with publish_brand_entities settings activated" do
      before do
        account.stubs(:has_publish_brand_entities?).returns(true)
      end

      it 'calls brand publisher when a certificate is updated' do
        BrandPublisher.any_instance.expects(:publish_for_account)

        certificate.autoprovisioned = true
        certificate.save
      end

      it 'calls brand publisher when a certificate is deleted' do
        BrandPublisher.any_instance.expects(:publish_for_account)

        certificate.destroy
      end
    end
  end
end
