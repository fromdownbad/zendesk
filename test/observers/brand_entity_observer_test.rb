require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe BrandEntityObserver do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:brand) do
    creator = Zendesk::BrandCreator.new(account)
    creator.brand.subdomain = 'newexample'
    creator.brand.name = account.name
    creator.brand
  end

  describe "brand publishing" do
    describe "without publish_brand_entities settings activated" do
      it "doesn't call brand publisher" do
        BrandPublisher.expects(:new).never

        brand.name = "new brand on the block"
        brand.save
      end
    end

    describe "with publish_brand_entities settings activated" do
      before do
        Account.any_instance.stubs(:has_publish_brand_entities?).returns(true)
      end

      it 'calls brand publisher when a brand is created' do
        BrandPublisher.any_instance.expects(:publish_for_brand)

        brand.route.save!
      end

      it 'calls brand publisher when a brand is updated' do
        brand.save
        BrandPublisher.any_instance.expects(:publish_for_brand).with(brand_id: brand.id)

        brand.name = "new brand on the block"
        brand.save
      end

      it 'calls brand publisher when a brand is deleted' do
        brand.save
        BrandPublisher.any_instance.expects(:publish_tombstone).with(brand_id: brand.id)

        brand.soft_delete!
      end
    end
  end
end
