require_relative "../support/test_helper"

SingleCov.covered!

describe 'AccountTimeZoneObserver' do
  fixtures :accounts

  describe "account changes time zone" do
    let(:account) { accounts(:minimum) }

    describe "account has a gooddata integration" do
      describe "gooddata integration is v2" do
        before do
          account.stubs(:gooddata_integration).returns(stub)
          account.time_zone = 'Melbourne'
          account.save!
        end

        before_should "update the gooddata project time zone" do
          GooddataConfigurationJob.expects(:enqueue).with(account.id)
        end
      end
    end

    describe "account doesn't have a gooddata integration" do
      before do
        account.stubs(:gooddata_integration)
        account.time_zone = 'Melbourne'
        account.save!
      end

      before_should "do nothing" do
        GooddataConfigurationJob.expects(:enqueue).never
      end
    end
  end

  describe "account changes something other than time zone" do
    let(:account) { accounts(:minimum) }

    before do
      account.stubs(:gooddata_integration).returns(stub)
      account.account_group_id = 2
      account.save!
    end

    before_should "do nothing" do
      GooddataConfigurationJob.expects(:enqueue).never
    end
  end
end
