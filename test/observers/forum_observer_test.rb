require_relative "../support/test_helper"
require 'zendesk_stats'

SingleCov.covered!

describe 'ForumObserver' do
  fixtures :users, :accounts, :forums, :entries

  before do
    @forum = forums(:announcements)
    @entry = @forum.entries.last
    @user = users(:minimum_agent)
    @account = accounts(:minimum)
  end

  describe "creating a forum 'topic'" do
    it "stores a 'entry_create' stat event" do
      forum_events_expects(
        type: "entry_create", account_id: @account.id,
        shard_id: @account.shard_id, category_id: @forum.category_id,
        forum_id: @forum.id, user_id: @user.id
      )

      Entry.create!(
        forum: @forum,
        submitter: @user,
        body: "Foo",
        title: "var",
        account: @account
      ) do |entry|
        entry.current_user = @user
      end
    end

    it "stores a 'vote_create' stat event" do
      forum_events_expects(
        type: "vote_create", account_id: @account.id,
        shard_id: @account.shard_id, category_id: @forum.category_id,
        forum_id: @forum.id, entry_id: @entry.id, user_id: @user.id
      )

      Vote.create! entry: @entry, user: @user
    end

    it "stores a 'post_create' stat event" do
      forum_events_expects(
        type: "post_create", account_id: @account.id,
        shard_id: @account.shard_id, category_id: @forum.category_id,
        forum_id: @forum.id, entry_id: @entry.id, user_id: @user.id
      )

      Post.create! body: "yo", entry_id: @entry.id, user_id: @user.id
    end

    it "stores a 'watching_forum_create' stat event" do
      forum_events_expects(
        type: "watching_forum_create", account_id: @account.id,
        shard_id: @account.shard_id, category_id: @forum.category_id,
        forum_id: @forum.id, user_id: @user.id
      )

      Watching.create! account: @account, user: @user, source: @forum
    end

    it "stores a 'watching_entry_create' stat event" do
      forum_events_expects(
        type: "watching_entry_create", account_id: @account.id,
        shard_id: @account.shard_id, category_id: @forum.category_id,
        forum_id: @forum.id, entry_id: @entry.id, user_id: @user.id
      )

      Watching.create! account: @account, user: @user, source: @entry
    end
  end

  private

  def forum_events_expects(h)
    Zendesk::ClassicStats::ForumEvent.expects(:store).with do |hash|
      if hash[:type] == h[:type]
        h.keys.each do |k|
          assert_equal_with_nil hash[k], h[k]
        end
      end
      true
    end.at_least_once
  end
end
