require_relative "../support/test_helper"

SingleCov.covered!

describe UserOtpSettingObserver do
  fixtures :users

  describe 'after_create' do
    it "does not send an email" do
      assert_emails_sent(0) do
        users(:minimum_admin).create_otp_setting!(phone_based: false)
      end
    end
  end

  describe 'after_update' do
    subject { users(:minimum_admin).create_otp_setting!(phone_based: false) }

    it "does not send an email if not confirmed" do
      assert_emails_sent(0) do
        subject.update_attributes!(confirmed: false)
      end
    end

    it "sends an email if confirmed" do
      assert_emails_sent do
        subject.update_attributes!(confirmed: true)
      end
    end

    it "does not send an email if touched" do
      subject.update_attributes!(confirmed: true)

      assert_emails_sent(0) do
        subject.update_attributes!({})
      end
    end
  end

  describe 'after_destroy' do
    describe 'in a legacy account' do
      subject { users(:minimum_admin).create_otp_setting!(phone_based: false) }

      it "sends an email" do
        assert_emails_sent do
          subject.destroy
        end
      end
    end

    describe 'in a multiproduct account' do
      subject { users(:multiproduct_support_agent).create_otp_setting!(phone_based: false) }

      it 'does not send an email' do
        assert_emails_sent(0) do
          subject.destroy
        end
      end
    end
  end
end
