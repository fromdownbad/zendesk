require_relative "../support/test_helper"

SingleCov.covered!

describe 'PermissionExploreEntitlementsChangesObserver' do
  fixtures :permission_sets

  let(:permission) { PermissionSetPermission.new(name: permission_name, permission_set: permission_set, value: permission_value) }
  let(:permission_set) { permission_sets(:multiproduct_custom_permission_set) }
  let(:context) { Object.new }
  let(:instrumentation) { stub(perform: true) }

  before do
    @actor = users(:minimum_admin)
    CIA.stubs(:current_actor).returns(@actor)
    permission.stubs(:binding).returns(context)
    permission.account = permission_set.account
  end

  describe 'change to explore_access' do
    let(:permission_name) { 'explore_access' }
    let(:permission_value) { 'full' }

    it 'calls Zendesk::ModelChangeInstrumentation to perform' do
      Zendesk::ModelChangeInstrumentation.expects(:new).with(
        model: permission,
        exec_context: context,
        columns_to_instrument: ['value'],
        source_of_instrument: 'explore_permissions_change'
      ).once.returns(instrumentation)
      permission.save!
    end
  end

  describe 'change to something else' do
    let(:permission_name) { 'something_else' }
    let(:permission_value) { 'anything' }

    it 'does not call Zendesk::ModelChangeInstrumentation to perform' do
      Zendesk::ModelChangeInstrumentation.expects(:new).never
      permission.save!
    end
  end
end
