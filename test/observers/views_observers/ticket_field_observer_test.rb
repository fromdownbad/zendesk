require_relative "../../support/test_helper"

SingleCov.covered!

describe ViewsObservers::TicketFieldObserver do
  fixtures :accounts, :tickets, :ticket_fields

  let(:account)      { ticket.account }
  let(:ticket)       { tickets(:minimum_1) }
  let(:ticket_field) { ticket_fields(:field_integer_custom) }

  let(:ticket_field_entry) do
    TicketFieldEntry.new(
      ticket_field: ticket_field,
      ticket: ticket,
      account: account
    )
  end

  describe '#after_save' do
    describe_with_arturo_enabled 'publish_ticket_entities_to_bus' do
      describe 'with changes' do
        before :each do
          ticket_field_entry.save # generate an ID
          ticket_field_entry.value = 1
        end

        it 'calls ViewsObservers::EntityPublishCallback#after_save' do
          ViewsObservers::EntityPublishCallback.any_instance.expects(:after_save)

          ticket_field_entry.save!
        end

        it 'passes the correct params to escape' do
          EscKafkaMessage.expects(:bulk_import!).with(
            includes(
              has_entries(
                account_id: ticket_field_entry.account_id,
                topic: 'support.views.ticket_fields',
                key: "#{ticket_field_entry.account_id}/#{ticket_field_entry.id}",
                value: anything,
                metadata: anything
              )
            )
          ).once
          ticket_field_entry.save!
        end

        it 'sends publish time to statsd' do
          Zendesk::StatsD::Client.any_instance.expects(:time).with('after_save_publish_time', tags: ['entity:TicketFieldProtobufEncoder']).once
          ticket_field_entry.save!
        end

        it 'uses the correct protobuf encoder' do
          ViewsEncoders::TicketFieldProtobufEncoder.any_instance.expects(:to_proto)
          ticket_field_entry.save!
        end
      end

      describe 'does not publish' do
        before :each do
          ticket_field_entry.save # generate an ID
        end

        it 'when model does not have changes' do
          EscKafkaMessage.expects(:create!).never
          ticket_field_entry.save!
        end

        it 'when model has changes we dont care about' do
          ticket_field_entry.updated_at = Time.now
          EscKafkaMessage.expects(:create!).never
          ticket_field_entry.save!
        end
      end
    end
  end
end
