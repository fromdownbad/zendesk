require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 0

describe ViewsObservers::TruthyMatcher do
  fixtures :tickets

  let(:ticket)  { tickets(:minimum_1) }
  let(:matcher) { ViewsObservers::TruthyMatcher.new(ticket) }

  it 'returns true for matched?' do
    assert matcher.matched?
  end
end
