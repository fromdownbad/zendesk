require_relative "../../support/test_helper"

SingleCov.covered!

describe ViewsObservers::TicketObserver do
  fixtures :tickets, :users

  let(:ticket) { tickets(:minimum_1) }
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  before do
    ViewsObservers::EscapeProducer.any_instance.stubs(domain_event_publisher: domain_event_publisher)
  end

  describe '#after_save' do
    describe_with_arturo_enabled 'publish_ticket_entities_to_bus' do
      describe 'publishes' do
        before(:each) do
          ticket.status_id = 1
          ticket.will_be_saved_by(users(:minimum_agent))
        end

        it 'calls ViewsObservers::EntityPublishCallback#after_save' do
          ViewsObservers::EntityPublishCallback.any_instance.expects(:after_save)
          ticket.save!
        end

        it 'passes the correct params to escape' do
          domain_event_publisher.expects(:bulk_import!).
            with(
              includes(
                has_entries(
                  account_id: ticket.account_id,
                  topic: 'support.views.tickets',
                  key: "#{ticket.account_id}/#{ticket.id}",
                  value: anything,
                  metadata: anything
                )
              )
            ).once
          ticket.save!
        end

        it 'sends publish time to statsd' do
          Zendesk::StatsD::Client.any_instance.expects(:time).with('domain_event.publish_time').once
          Zendesk::StatsD::Client.any_instance.expects(:time).with('after_save_publish_time', tags: ['entity:ViewsTicketProtobufEncoder']).once
          ticket.save!
        end

        it 'uses the correct protobuf encoder' do
          ViewsEncoders::ViewsTicketProtobufEncoder.any_instance.expects(:to_proto)
          ticket.save!
        end
      end

      describe 'does not publish' do
        before do
          # Needed to pass validations
          ticket.will_be_saved_by(users(:minimum_agent))
          ticket.save!
        end

        it 'when model does not have changes' do
          domain_event_publisher.expects(:create!).with(includes(has_entries(topic: 'support.views.tickets'))).never

          ticket.save!
        end

        it 'when model has changes we dont care about' do
          domain_event_publisher.expects(:create!).with(includes(has_entries(topic: 'support.views.tickets'))).never

          ticket.base_score = 1
          ticket.save!
        end
      end
    end
  end
end
