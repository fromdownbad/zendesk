require_relative '../../support/test_helper'
require 'zendesk_protobuf_clients/zendesk/protobuf/escape/escape_metadata_pb'

SingleCov.covered! uncovered: 4, file: 'app/observers/views_observers/entity_publisher.rb'
SingleCov.covered! uncovered: 2, file: 'app/protobuf_encoders/views_encoders/views_ticket_protobuf_encoder.rb'
SingleCov.covered! file: 'app/observers/views_observers/escape_producer.rb'

describe ViewsObservers::EntityPublisher do
  fixtures :tickets, :accounts, :users

  let(:ticket) { tickets(:minimum_1) }
  let(:subject) do
    ViewsObservers::EntityPublisher.new(
      'test_topic',
      'test_namespace',
      ViewsEncoders::ViewsTicketProtobufEncoder,
      ViewsObservers::TicketChangedMatcher
    )
  end

  let(:metadata) do
    ::Zendesk::Protobuf::Escape::Metadata.
      new(headers: {'ZENDESK_PUBLICATION_TIMESTAMP' =>
                    ::Google::Protobuf::BytesValue.new(value: DateTime.new(1990).strftime('%Q'))}).to_proto
  end

  describe '#publish' do
    before do
      ticket.status_id = 2
    end

    it 'publishes entites to escape' do
      EscKafkaMessage.expects(:bulk_import!).with(
        includes(
          has_entries(
            account_id: ticket.account.id,
            topic: 'test_topic',
            key: "#{ticket.account_id}/#{ticket.id}",
            value: anything,
            metadata: anything
          )
        )
      )
      subject.publish(ticket)
    end

    it 'sends the publication time in the headers' do
      Timecop.freeze(DateTime.new(1990))

      EscKafkaMessage.expects(:bulk_import!).with(
        includes(
          has_entries(
            metadata: metadata
          )
        )
      )
      subject.publish(ticket)

      Timecop.return
    end

    it 'publishes multiple entities to escape' do
      # bulk_import! is passed an array of hashes
      EscKafkaMessage.expects(:bulk_import!).with { |*args| args.first.count == 2 }.once
      subject.publish([ticket, ticket])
    end

    it 'only publishes entities with relevant changes' do
      irrelevant_ticket = tickets(:minimum_2)
      irrelevant_ticket.base_score = 1

      EscKafkaMessage.expects(:bulk_import!).with { |*args| args.first.count == 1 }.once
      subject.publish([ticket, irrelevant_ticket])
    end

    it 'encodes to protobuf' do
      ViewsEncoders::ViewsTicketProtobufEncoder.any_instance.expects(:to_proto).returns(:protobuf)
      EscKafkaMessage.expects(:bulk_import!).with(includes(has_entries(value: :protobuf)))
      subject.publish(ticket)
    end

    it 'handles errors' do
      EscKafkaMessage.expects(:bulk_import!).raises(StandardError)
      Zendesk::StatsD::Client.any_instance.expects(:increment).
        with('errors', tags: ['entity:ViewsTicketProtobufEncoder', 'exception:StandardError'])
      subject.publish(ticket)
    end
  end

  describe 'publishes tombstone' do
    it 'when the model has been deleted' do
      ticket.will_be_saved_by(users(:minimum_agent), via_id: ViaType.WEB_SERVICE)
      ticket.soft_delete!

      EscKafkaMessage.expects(:bulk_import!).with(
        includes(
          has_entries(
            account_id: ticket.account.id,
            topic: 'test_topic',
            key: "#{ticket.account_id}/#{ticket.id}",
            value: nil,
            metadata: anything
          )
        )
      )
      subject.publish(ticket)
    end

    it 'when forced' do
      EscKafkaMessage.expects(:bulk_import!).with(
        includes(
          has_entries(
            account_id: ticket.account.id,
            topic: 'test_topic',
            key: "#{ticket.account_id}/#{ticket.id}",
            value: nil,
            metadata: anything
          )
        )
      )
      subject.publish(ticket, force_tombstone: true)
    end
  end

  it 'raises when entity cannot provide a key' do
    lame_duck = Struct.new(:id).new(1)
    Zendesk::StatsD::Client.any_instance.expects(:increment).
      with('errors', tags: ['entity:ViewsTicketProtobufEncoder', 'exception:NoMethodError'])
    EscKafkaMessage.expects(:bulk_import!).never
    subject.publish(lame_duck)
  end
end
