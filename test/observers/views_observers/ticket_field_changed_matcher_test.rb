require_relative '../../support/test_helper.rb'

SingleCov.covered! uncovered: 1

describe ViewsObservers::TicketFieldChangedMatcher do
  fixtures :ticket_fields

  let(:ticket_field) { ticket_fields(:field_tagger_custom) }
  let(:ticket_field) { TicketFieldEntry.new }
  let(:subject) { ViewsObservers::TicketFieldChangedMatcher.new(ticket_field) }

  describe 'never tombstones' do
    it 'for no change' do
      assert_not subject.tombstone?
    end

    it 'with change' do
      ticket_field.delete
      assert_not subject.tombstone?
    end
  end
end
