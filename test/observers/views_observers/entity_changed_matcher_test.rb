require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 1

describe ViewsObservers::EntityChangedMatcher do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }

  class TestMatcher < ViewsObservers::EntityChangedMatcher
    def fields
      [:status_id]
    end
  end

  describe 'with fields defined' do
    it 'detects changed fields of interest' do
      ticket.status_id = 2
      matcher = TestMatcher.new(ticket)
      assert matcher.matched?
    end

    it 'ignores changed fields of no interest' do
      ticket.generated_timestamp = Time.now
      matcher = TestMatcher.new(ticket)
      refute matcher.matched?
    end
  end
end
