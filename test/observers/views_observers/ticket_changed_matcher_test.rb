require_relative '../../support/test_helper'

SingleCov.covered!

describe ViewsObservers::TicketChangedMatcher do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }

  before do
    ticket.will_be_saved_by(User.system)
    # We need the audit event to be created to properly perform tests for
    # ticket schedule changes, audits are always guaranteed to be present
    # on ticket save
    event = Create.new(audit: ticket.audit, ticket: ticket)
    ticket.audit.events << event

    ticket.save!
  end

  describe 'when there is change in ticket prediction' do
    before { TicketPrediction.update_satisfaction_probability(ticket, 0.8) }

    it 'detects the change' do
      matcher = ViewsObservers::TicketChangedMatcher.new(ticket)
      assert matcher.matched?
    end
  end

  describe 'when there is change in ticket schedule' do
    let(:schedule) do
      ticket.
        account.
        schedules.
        create(name: 'schedule_1', time_zone: ticket.account.time_zone)
    end

    before do
      ticket.will_be_saved_by(User.system)

      event = ScheduleAssignment.new(
        audit: ticket.audit,
        ticket: ticket,
        value: nil,
        value_previous: schedule.id
      )
      ticket.audit.events << event
      ticket.audit.save!
      ticket.reload
    end

    it 'detects the change' do
      matcher = ViewsObservers::TicketChangedMatcher.new(ticket)
      assert matcher.matched?
    end
  end
end
