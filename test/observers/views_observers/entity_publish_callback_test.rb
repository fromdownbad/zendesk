require_relative '../../support/test_helper'

SingleCov.covered!

describe ViewsObservers::EntityPublishCallback do
  fixtures :tickets, :accounts, :users

  let(:ticket) { tickets(:minimum_1) }
  let(:encoder) { stub('encoder') }
  let(:subject) do
    ViewsObservers::EntityPublishCallback.new(
      topic: 'test_topic',
      namespace: 'test_namespace',
      encoder_klass: encoder,
      matcher_klass: stub('matcher')
    )
  end

  describe '#after_save' do
    describe_with_arturo_enabled 'publish_ticket_entities_to_bus' do
      before do
        encoder.expects(:name).returns('EntityEncoder')
      end

      it 'publishes entites to escape' do
        ViewsObservers::EntityPublisher.any_instance.expects(:publish).with(ticket)
        subject.after_save(ticket)
      end

      it 'sends publish time' do
        Zendesk::StatsD::Client.any_instance.expects(:time).with('after_save_publish_time', tags: ['entity:EntityEncoder']).once
        subject.after_save(ticket)
      end
    end

    describe_with_arturo_disabled 'publish_ticket_entities_to_bus' do
      it 'does not publish' do
        ViewsObservers::EntityPublisher.any_instance.expects(:publish).never
        subject.after_save(ticket)
      end
    end
  end
end
