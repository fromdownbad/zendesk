require_relative "../support/test_helper"

SingleCov.covered!

describe 'PermissionSetObserver' do
  fixtures :users

  before do
    @user = users(:minimum_end_user)
  end

  describe 'role definition is changed' do
    before do
      @permission_set, = PermissionSet.build_default_permission_sets(accounts(:minimum))
      @permission_set.users << @user
      @permission_set.save!
      @permission_set.reload
    end

    it "syncs the user when the permission set's reporting access changes" do
      Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).
        with(@user)
      @permission_set.permissions.
        detect { |perm| perm.name == 'report_access' }.value = 'none'
      @permission_set.save!
    end
    it "does not sync the user when the permission set's reporting access didn't change" do
      Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).
        with(@user).never
      @permission_set.save!
    end
  end
end
