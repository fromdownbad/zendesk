require_relative "../support/test_helper"

SingleCov.covered!

describe 'SubscriptionObserver' do
  fixtures :accounts, :payments

  before do
    @account = accounts(:minimum)
    @subscription = @account.subscription
  end

  describe "the account has been canceled" do
    it "destroy the integration" do
      Zendesk::Gooddata::IntegrationProvisioning.expects(:destroy_for_account).with(@account)

      @account.update_attribute(:is_active, false)
      @account.stubs(:is_serviceable?).returns(false)
      @subscription.canceled_on = Date.today
      @subscription.save!
    end
  end
end
