require_relative "../support/test_helper"

SingleCov.covered! uncovered: 11

describe 'AccountSettingObserver' do
  fixtures :users
  describe "API settings change notification" do
    before do
      @actor = users(:minimum_agent)
      @actor.stubs(:name).returns("Curious George")
      current_transaction = { actor: @actor }
      CIA.stubs(:current_transaction).returns(current_transaction)

      @user    = users(:minimum_admin)
      @account = @user.account
    end

    describe "when the mobile_app_access setting is saved" do
      describe "and the setting is disabled" do
        before do
          assert @account.settings.mobile_app_access
          @account.settings.mobile_app_access = false
        end

        it "deletes all mobile oauth tokens" do
          LogoutAllMobileDevicesJob.stubs(:enqueue).with(@account.id).once
          @account.settings.save!
        end
      end

      describe "and the setting is enabled" do
        before do
          @account.settings.mobile_app_access = false
          @account.settings.save!
          refute @account.settings.mobile_app_access
          @account.settings.mobile_app_access = true
        end

        it "does nothing" do
          Apps::ChatAppInstallation.any_instance.stubs(:handle_disable_installation)
          LogoutAllMobileDevicesJob.stubs(:enqueue).never
          @account.settings.save!
        end
      end
    end

    describe "when the automatic_certificate_provisioning setting is saved" do
      describe "and the setting is enabled" do
        before do
          refute @account.settings.automatic_certificate_provisioning
          @account.settings.automatic_certificate_provisioning = true
        end

        it "enqueues the provisioning job" do
          AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(@account.id).once
          @account.settings.save!
        end

        it "does not enqueue the provisioning job when a random setting is enabled" do
          @account.settings.save!
          @account.settings.mobile_app_access = true
          AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(@account.id).never
          @account.settings.save!
        end
      end

      describe "and the setting is disabled" do
        before do
          @account.settings.automatic_certificate_provisioning = true
          AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(@account.id).once

          @account.settings.save!

          assert @account.settings.automatic_certificate_provisioning
          @account.settings.automatic_certificate_provisioning = false
        end

        it "does not enqueue the provisioning job" do
          AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(@account.id).never
          @account.settings.save!
        end
      end
    end

    describe "when the account_assumption setting is saved" do
      before { Arturo.enable_feature! :account_assumption_control }

      describe "and the setting is enabled" do
        before do
          refute @account.settings.assumption_expiration
          @account.settings.assumption_expiration = 1.week.from_now
        end

        it "sends an email" do
          SecurityMailer.expects(:deliver_assumption_expiration).once
          @account.settings.save!
        end
      end

      describe "and the setting duration is permanent" do
        before do
          SecurityMailer.expects(:deliver_assumption_expiration).twice
          @account.settings.assumption_expiration = 1.week.from_now
          @account.settings.save!
          @account.settings.assumption_expiration = Account::AssumptionSupport::ALWAYS_ASSUMABLE
        end

        it "sends an email" do
          @account.settings.save!
        end
      end

      describe "and the setting is not changing" do
        before do
          @account.settings.mobile_app_access = true
        end

        it "does not send an email when a different setting changes" do
          SecurityMailer.expects(:deliver_assumption_expiration).never
          @account.settings.save!
        end

        describe "previously set" do
          before do
            SecurityMailer.expects(:deliver_assumption_expiration).once
            @account.settings.assumption_expiration = 1.week.from_now
            @account.settings.save!
          end

          it "does not send an email" do
            @account.settings.save!
          end
        end
      end

      describe "and the setting is disabled" do
        before do
          SecurityMailer.expects(:deliver_assumption_expiration).twice
          @account.settings.assumption_expiration = 1.week.from_now
          @account.save!
          @account.settings.assumption_expiration = nil
        end

        it "sends an email" do
          @account.settings.save!
        end
      end

      describe "and the setting changing" do
        describe "reenabling after expiration" do
          before do
            SecurityMailer.expects(:deliver_assumption_expiration).twice
            @account.settings.assumption_expiration = 1.year.ago
            @account.save!
          end

          it "sends an email" do
            @account.settings.assumption_expiration = 1.year.from_now
            @account.settings.save!
          end
        end

        describe "changing when already enabled" do
          before do
            SecurityMailer.expects(:deliver_assumption_expiration).twice
            @account.settings.assumption_expiration = 1.week.from_now
            @account.save!
          end

          it "sends an email" do
            @account.settings.assumption_expiration = 1.year.from_now
            @account.settings.save!
          end
        end

        describe "an unhandled change" do
          before do
            @account.settings.assumption_expiration = 1.week.ago
            @account.save!
          end

          it "does not send an email" do
            SecurityMailer.expects(:deliver_assumption_expiration).never
            @account.settings.assumption_expiration = 1.second.ago
            @account.save!
          end
        end
      end
    end

    describe "when the csat_reason_code setting is changed" do
      describe "when setting is set to enabled" do
        describe "when there are no existing satisfaction reasons" do
          before do
            @account.settings.csat_reason_code = 1
            @account.save!
          end

          it "creates system satisfaction reasons" do
            assert_equal @account.satisfaction_reasons.count(:all), 6
          end
        end

        describe "when there are existing satisfaction_reasons" do
          before do
            [1, 0, 1].each do |value|
              @account.settings.csat_reason_code = value
              @account.save!
            end
          end

          it "does not create additional system satisfaction reasons" do
            assert_equal @account.satisfaction_reasons.count(:all), 6
          end
        end
      end
    end

    describe "with the feature enabled" do
      before do
        Arturo.enable_feature!(:security_email_notifications_for_account_settings)
      end

      describe "api_token" do
        describe "when it hasn't been initialized" do
          before do
            @account.api_tokens.destroy_all
          end

          describe "and set to some value" do
            it "does not send an api token changed notification" do
              SecurityMailer.expects(:deliver_api_token_changed).never
              @account.api_tokens.create!
            end
          end
        end

        describe "when it has been initialized" do
          before do
            @account.api_tokens.create!
          end

          describe "and set to another value" do
            it "does not send an api token changed notification" do
              SecurityMailer.expects(:deliver_api_token_changed).never
              @account.api_tokens.create!
            end
          end
        end
      end

      describe "api_token_access" do
        describe "when it hasn't been initialized" do
          before do
            @account.settings.api_token_access = nil
            @account.save!
          end

          describe "and set to some value" do
            it "does not send an api token access notification" do
              SecurityMailer.expects(:deliver_api_token_access_disabled).never
              @account.settings.api_token_access = '0'
              @account.save!
            end
          end
        end

        describe "when it has been initialized" do
          before do
            @account.settings.api_token_access = '0'
            @account.save!
          end

          describe "and set to the same value" do
            it "does not send an api token access notification" do
              SecurityMailer.expects(:deliver_api_token_access_enabled).never
              @account.settings.api_token_access = '0'
              @account.save!
            end
          end
        end
      end

      describe "api_password_access" do
        describe "when it hasn't been initialized" do
          before do
            @account.settings.api_password_access = nil
            @account.save!
          end

          describe "and set to some value" do
            it "does not send an api token access notification" do
              SecurityMailer.expects(:deliver_api_password_access_disabled).never
              @account.settings.api_password_access = '0'
              @account.save!
            end
          end
        end

        describe "when it has been initialized" do
          before do
            @account.settings.api_password_access = '0'
            @account.save!
          end

          describe "and set to a new value" do
            it "sends an api token access notification" do
              SecurityMailer.expects(:deliver_api_password_access_enabled).once
              @account.settings.api_password_access = '1'
              @account.save!
            end
          end

          describe "and set to the same value" do
            it "does not send an api token access notification" do
              SecurityMailer.expects(:deliver_api_password_access_enabled).never
              @account.settings.api_password_access = '0'
              @account.save!
            end
          end
        end

        describe "when it's currently disabled" do
          before do
            @account.settings.api_password_access = '0'
            @account.save!
          end

          describe "and set to enable" do
            it "sends an api token access enabled notification" do
              SecurityMailer.expects(:deliver_api_password_access_enabled).once
              @account.settings.api_password_access = '1'
              @account.save!
            end
          end
        end

        describe "when it's currently enabled" do
          before do
            @account.settings.api_password_access = '1'
            @account.save!
          end

          describe "and set to disable" do
            it "sends an api token access disabled notification" do
              SecurityMailer.expects(:deliver_api_password_access_disabled).once
              @account.settings.api_password_access = '0'
              @account.save!
            end
          end
        end
      end
    end

    describe "with the feature disabled" do
      before do
        Arturo.disable_feature!(:security_email_notifications_for_account_settings)
      end

      describe "api_token_access" do
        before do
          @account.settings.api_token_access = nil
          @account.save
        end

        describe "when disabled" do
          it "does not send an api token access disabled notification" do
            SecurityMailer.expects(:deliver_api_token_access_disabled).never
            @account.settings.api_token_access = "0"
            @account.save
          end
        end

        describe "when enabled" do
          it "does not send an api token access enabled notification" do
            SecurityMailer.expects(:deliver_api_token_access_enabled).never
            @account.settings.api_token_access = "1"
            @account.save
          end
        end
      end

      describe "api_password_access" do
        before do
          @account.settings.api_password_access = nil
          @account.save
        end

        describe "when disabled" do
          it "does not send an api token access disabled notification" do
            SecurityMailer.expects(:deliver_api_password_access_disabled).never
            @account.settings.api_password_access = "0"
            @account.save
          end
        end

        describe "when enabled" do
          it "does not send an api token access enabled notification" do
            SecurityMailer.expects(:deliver_api_password_access_enabled).never
            @account.settings.api_password_access = "1"
            @account.save
          end
        end
      end
    end

    describe "#track_changes_in_kafka" do
      around { |example| Timecop.freeze { example.call } }

      describe "when end user attachments changes" do
        before do
          @account.settings.end_user_attachments = '0'
          @account.save!
        end

        let(:setting) { @account.settings.find { |setting| setting.name == 'end_user_attachments' } }

        it "creates an escape kafka message record" do
          @account.settings.end_user_attachments = '1'
          EscKafkaMessage.expects(:create!).with(
            account_id: @account.id,
            topic: "support.account_setting_events",
            key: "#{@account.id}/end_user_attachments",
            value: AccountSettingProtobufEncoder.new(setting).to_proto,
            partition_key: @account.id.to_s
          )
          @account.save!
        end
      end

      describe 'when agent workspace changes' do
        before do
          @account.settings.polaris = false
          @account.save!
          stub_request(:get, "https://minimum.zendesk-test.com/api/v2/apps/installations.json").to_return(
            status: 200,
            body: { installations: [] }.to_json,
            headers: {'Content-Type' => 'application/json'}
          )
        end

        let(:setting) { @account.settings.find { |setting| setting.name == 'polaris' } }

        it 'creates an escape kafka message record' do
          @account.settings.polaris = true
          EscKafkaMessage.expects(:create!).with(
            account_id: @account.id,
            topic: "support.account_setting_events",
            key: "#{@account.id}/polaris",
            value: AccountSettingProtobufEncoder.new(setting).to_proto,
            partition_key: @account.id.to_s
          )
          @account.save!
        end
      end

      describe "when end user attachments does not change" do
        it "does not create an escape kafka message record" do
          EscKafkaMessage.expects(:create!).never
          @account.save!
        end
      end
    end

    describe "#handle_polaris_radar_notification" do
      def stub_radar(data)
        ::RadarFactory.expects(:create_radar_notification).with(
          @account,
          "growl/account/#{@account.subdomain}/polaris"
        ).returns(
          mock do |mock_radar_instance|
            mock_radar_instance.expects(:send).with(*data)
          end
        )
      end

      it "does nothing if setting name is not 'polaris'" do
        ::RadarFactory.expects(:create_radar_notification).with(includes("growl/account/#{@account.subdomain}")).never
        @account.save
      end

      it "sends Radar notification when disabled" do
        stub_radar(['polaris_off', ''])

        @account.settings.polaris = false
        @account.save
      end

      it "sends Radar notification when enabled" do
        @account.settings.polaris = false
        @account.save

        stub_radar(['polaris_on', ''])
        Apps::ChatAppInstallation.any_instance.stubs(:handle_disable_installation)

        @account.settings.polaris = true
        @account.save
      end
    end

    describe "#handle_polaris_chat_app_installation" do
      it "does nothing when setting name is not polaris" do
        @account.settings.mobile_app_access = true
        Apps::ChatAppInstallation.expects(:new).never
        @account.save
      end

      it "does nothing when polaris is disabled" do
        @account.settings.polaris = false
        Apps::ChatAppInstallation.expects(:new).never
        @account.save
      end

      it " does nothing when polaris is enabled and not Chat product" do
        Zendesk::Accounts::Client.any_instance.stubs(:chat_product).returns(nil)

        @account.settings.polaris = true
        Apps::ChatAppInstallation.expects(:new).never
        @account.save
      end

      it "creates ChatAppInstallation instance when polaris is enabled and Chat product exist" do
        Zendesk::Accounts::Client.any_instance.stubs(:account_from_account_service).returns(true)
        Zendesk::Accounts::Client.any_instance.stubs(:chat_product).returns(stub(name: :chat))
        Zopim::InternalApiClient.any_instance.stubs(:notify_account_settings_change)

        @account.settings.polaris = true
        @mock_chat_app_installation = MiniTest::Mock.new
        @mock_chat_app_installation.expect(:handle_disable_installation, nil)
        Apps::ChatAppInstallation.expects(:new).returns(@mock_chat_app_installation)
        @account.save
      end
    end

    describe "#notify_chat_of_polaris_settings_changed" do
      before { Apps::ChatAppInstallation.any_instance.stubs(:handle_disable_installation) }

      def stub_zopim_internal_client
        mock_connection = mock
        mock_connection.expects(:notify_account_settings_change).with(
          'polaris_settings_changed'
        ).returns(true)
        Zopim::InternalApiClient.expects(:new).with(@account.id).returns(mock_connection)
      end

      it "does nothing if setting name is not 'polaris'" do
        Zopim::InternalApiClient.expects(:new).with(@account.id).never
        @account.save
      end

      it "does nothing if chat product is missing" do
        Zendesk::Accounts::Client.any_instance.stubs(:chat_product).returns(nil)
        Zopim::InternalApiClient.expects(:new).with(@account.id).never

        @account.settings.polaris = false
        @account.save
      end

      describe "when chat product exists" do
        before do
          stub_zopim_internal_client
          Zendesk::Accounts::Client.any_instance.stubs(:account_from_account_service).returns(true)
          Zendesk::Accounts::Client.any_instance.stubs(:chat_product).returns(stub(name: :chat))
        end

        it "notify Chat when set to false" do
          @account.settings.polaris = false
          @account.save
        end

        it "notify Chat when set to true" do
          @account.settings.polaris = true
          @account.save
        end
      end

      describe "when missing account record" do
        before do
          Zendesk::Accounts::Client.any_instance.stubs(:account_from_account_service).returns(nil)
        end
        it "does not notify chat" do
          @account.settings.polaris = false
          @account.save
        end
      end
    end

    describe "#handle_email_radar_notification" do
      def stub_radar(data)
        ::RadarFactory.expects(:create_radar_notification).with(
          @account,
          "growl/account/#{@account.subdomain}/simplified_email_threading"
        ).returns(
          mock do |mock_radar_instance|
            mock_radar_instance.expects(:send).with(*data)
          end
        )
      end

      it "does nothing if setting name is not 'simplified_email_threading'" do
        ::RadarFactory.expects(:create_radar_notification).with(includes("growl/account/#{@account.subdomain}")).never
        @account.settings.mobile_app_access = false
        @account.save
      end

      it "sends Radar notification when Simplified Email threading disabled" do
        stub_radar(['simplified_email_threading_off', ''])
        @account.settings.simplified_email_threading = false
        @account.save
      end

      it "sends Radar notification when Simplified Email Threading enabled" do
        stub_radar(['simplified_email_threading_on', ''])
        @account.settings.simplified_email_threading = true
        @account.save
      end
    end

    describe "#track_changes_to_default_brand" do
      let(:account) { accounts(:minimum) }

      describe "when the account has_publish_brand_entities? disabled" do
        before do
          Account.any_instance.stubs(:has_publish_brand_entities?).returns(false)
        end

        it "does not publish an update" do
          BrandPublisher.any_instance.expects(:publish_for_brand).never
          account.save
        end
      end

      describe "when the account has_publish_brand_entities? enabled" do
        before do
          Account.any_instance.stubs(:has_publish_brand_entities?).returns(true)
        end

        it "does nothing if setting name is not 'default_brand_id'" do
          BrandPublisher.any_instance.expects(:publish_for_brand).never
          account.save
        end

        describe 'when the setting has changed' do
          before do
            account.settings.default_brand_id = 1
            account.save
          end

          it 'publishes an update for the account' do
            BrandPublisher.any_instance.expects(:publish_for_brand).with(brand_id: "1").once
            BrandPublisher.any_instance.expects(:publish_for_brand).with(brand_id: "2").once
            account.settings.default_brand_id = 2
            account.save
          end
        end
      end
    end
  end
end
