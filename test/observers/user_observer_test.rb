require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'UserObserver' do
  fixtures :users, :translation_locales

  let(:user) { users(:minimum_agent) }
  let(:actor) { users(:minimum_admin) }

  before do
    CIA.stubs(:current_actor).returns(actor)
  end

  describe 'for a shell account without support' do
    let(:account) { accounts(:shell_account_without_support) }
    let(:user) { FactoryBot.create(:user, account: account, skip_verification: false) }
    let(:actor) { user }

    describe 'updating the password' do
      it 'triggers a notification email' do
        SecurityMailer.expects(:deliver_profile_crypted_password_changed).
          with(user, actor, brand_id: nil).once
        user.password = 'shazam!'
        user.save!
      end

      it "skips notification email when password_change_email is 'false' or false" do
        SecurityMailer.expects(:deliver_profile_crypted_password_changed).never
        user.password_change_email = 'false'
        user.password = "shazam!"
        user.save!

        user.password_change_email = false
        user.password = "shazam2!"
        user.save!
      end
    end
  end

  describe 'for a non-shell account with support enabled' do
    let(:account) { accounts(:minimum) }

    describe "updating the password" do
      it "triggers a notification email" do
        SecurityMailer.expects(:deliver_profile_crypted_password_changed).
          with(user, actor, brand_id: nil).once
        user.password = "shazam!"
        user.save!
      end

      it "skips notification email when password_change_email is 'false' or false" do
        SecurityMailer.expects(:deliver_profile_crypted_password_changed).never
        user.password_change_email = 'false'
        user.password = "shazam!"
        user.save!

        user.password_change_email = false
        user.password = "shazam2!"
        user.save!
      end

      describe "when the user is an end-user" do
        let(:user) { users(:minimum_end_user) }

        it "triggers a notification email" do
          SecurityMailer.expects(:deliver_profile_crypted_password_changed)
          user.password = "shazam!"
          user.save!
        end
      end

      describe "adding an admin to the account" do
        # remove the following block when cleaning up arturo email_notification_profile_admin_added_duplicates_fix
        describe "with arturo email_notification_profile_admin_added_duplicates_fix disabled" do
          describe "by promoting an existing user" do
            it "triggers a notification email" do
              admins = user.account.admins.verified
              SecurityMailer.expects(:deliver_profile_admin_user_added_v2).with(
                user,
                admins.map(&:email) - [user.email],
                admins[0].translation_locale,
                actor
              ).once

              user.roles = Role::ADMIN.id
              user.save!
            end

            it "doesn't email prematurely" do
              SecurityMailer.expects(:deliver_profile_admin_user_added_v2).never
              user.roles = Role::ADMIN.id
              with_rollback(user) { user.save! }
            end
          end

          describe "by creating a new user with an administrator role" do
            let(:user) { FactoryBot.create(:user, account: account, skip_verification: false) }

            it "triggers a notification email" do
              SecurityMailer.expects(:deliver_profile_admin_user_added_v2).once
              user.roles      = Role::ADMIN.id
              user.name       = "Rinko Kikuchi"
              user.email      = "rinko.kikuchi@example.com"
              user.save
            end
          end

          describe "and the new admin is also the account owner" do
            it "does not trigger a notification email" do
              SecurityMailer.expects(:deliver_profile_admin_user_added_v2).never
              account = accounts(:minimum).becomes_new(Accounts::Classic)
              account.owner_id = nil
              account.route_id = nil
              account.subdomain = "localminima"
              account.set_address(
                name: 'Rinko, Inc.',
                phone: '012 64 3 917 540 1054',
                country: 'Denmark'
              )
              account.set_owner(
                name: "Rinko Kikuchi",
                email: "rinko.kikuchi@example.com"
              )
              account.save
            end
          end
        end

        describe "with arturo email_notification_profile_admin_added_duplicates_fix enabled" do
          before do
            Account.any_instance.stubs(:has_email_notification_profile_admin_added_duplicates_fix?).returns(true)
          end

          describe "by promoting an existing user" do
            describe "when the admins have only one language between them" do
              it "triggers a notification email" do
                admins = user.account.admins.verified

                SecurityMailer.expects(:deliver_profile_admin_user_added).with(
                  user,
                  user,
                  admins.map(&:email) - [user.email],
                  actor
                ).once

                user.roles = Role::ADMIN.id
                user.save!
              end
            end

            describe "when the admins have more than one language between them" do
              let(:admins) { user.account.admins.verified }

              before do
                admins[1].locale_id = 4
                admins[1].save!
              end

              it "triggers one notification email per admin" do
                assert_equal 3, admins.size + 1
                SecurityMailer.expects(:deliver_profile_admin_user_added).times(3)

                user.roles = Role::ADMIN.id
                user.save!
              end

              it "send only one email per admin, not one per identity" do
                UserPhoneNumberIdentity.create!(user: admins[1], value: "+14155566555")

                assert_equal 3, admins.size + 1
                SecurityMailer.expects(:deliver_profile_admin_user_added).times(3)

                user.roles = Role::ADMIN.id
                user.save!
              end
            end

            it "doesn't email prematurely" do
              SecurityMailer.expects(:deliver_profile_admin_user_added).never
              user.roles = Role::ADMIN.id
              with_rollback(user) { user.save! }
            end
          end

          describe "by creating a new user with an administrator role" do
            let(:user) { FactoryBot.create(:user, account: account, skip_verification: false) }

            it "triggers a notification email" do
              SecurityMailer.expects(:deliver_profile_admin_user_added).once
              user.roles      = Role::ADMIN.id
              user.name       = "Rinko Kikuchi"
              user.email      = "rinko.kikuchi@example.com"
              user.save
            end
          end

          describe "and the new admin is also the account owner" do
            it "does not trigger a notification email" do
              SecurityMailer.expects(:deliver_profile_admin_user_added).never
              account = accounts(:minimum).becomes_new(Accounts::Classic)
              account.owner_id = nil
              account.route_id = nil
              account.subdomain = "localminima"
              account.set_address(
                name: 'Rinko, Inc.',
                phone: '012 64 3 917 540 1054',
                country: 'Denmark'
              )
              account.set_owner(
                name: "Rinko Kikuchi",
                email: "rinko.kikuchi@example.com"
              )
              account.save
            end
          end
        end

        describe "when existing admins have multiple different locales" do
          it "sends multiple security notifications in those locales" do
            other_locale_admin = User.new(user.dup.attributes)
            other_locale_admin.account_id         = user.account.id
            other_locale_admin.roles              = Role::ADMIN.id
            other_locale_admin.name               = "Other Locale"
            other_locale_admin.email              = "otherlocale@example.com"
            other_locale_admin.translation_locale = translation_locales(:spanish)
            other_locale_admin.is_verified        = true
            other_locale_admin.save!

            SecurityMailer.expects(:deliver_profile_admin_user_added_v2).with do |_user, _emails, locale, _actor|
              locale == translation_locales(:english_by_zendesk)
            end
            SecurityMailer.expects(:deliver_profile_admin_user_added_v2).with do |_user, _emails, locale, _actor|
              locale == translation_locales(:spanish)
            end

            new_user = User.new(user.dup.attributes)
            new_user.account_id = user.account.id
            new_user.roles      = Role::ADMIN.id
            new_user.name       = "Rinko Kikuchi"
            new_user.email      = "rinko.kikuchi@example.com"
            new_user.save
          end
        end
      end

      describe "with the account setting email_agent_when_sensitive_fields_changed disabled" do
        let(:user) { users(:minimum_agent) }

        before do
          user.account.settings.email_agent_when_sensitive_fields_changed = false
          user.account.settings.save!
        end

        describe "updating the password" do
          it "does not trigger a notification email" do
            SecurityMailer.expects(:deliver_profile_crypted_password_changed).never
            user.password = "shazam!"
            user.save!
          end
        end
      end
    end
  end
end
