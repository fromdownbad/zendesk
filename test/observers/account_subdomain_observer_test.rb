require_relative "../support/test_helper"

SingleCov.covered!

describe 'AccountSubdomain' do
  fixtures :accounts

  describe "account changes subdomain" do
    let(:account) { accounts(:minimum) }

    before do
      account.stubs(:is_serviceable_by_billing_state?).returns(true)
    end

    describe "account has a gooddata integration" do
      describe "gooddata integration is v2" do
        before do
          account.stubs(:gooddata_integration).returns(stub)
          account.subdomain = 'awesomesupport'
          account.save!
        end

        before_should "update the gooddata project subdomain" do
          GooddataConfigurationJob.expects(:enqueue).with(account.id)
        end

        before_should "kick off a full reload" do
          GooddataFullReloadJob.expects(:enqueue).with(account.id)
        end
      end
    end

    describe "account doesn't have a gooddata integration" do
      before do
        account.stubs(:gooddata_integration)
        account.subdomain = 'awesomesupport'
        account.save!
      end

      before_should "do nothing" do
        GooddataConfigurationJob.expects(:enqueue).never
        GooddataFullReloadJob.expects(:enqueue).never
      end
    end
  end

  describe "account changes something other than time zone" do
    let(:account) { accounts(:minimum) }

    before do
      account.stubs(:gooddata_integration).returns(stub)
      account.stubs(:is_serviceable_by_billing_state?).returns(true)
      account.account_group_id = 2
      account.save!
    end

    before_should "do nothing" do
      GooddataConfigurationJob.expects(:enqueue).never
    end
  end
end
