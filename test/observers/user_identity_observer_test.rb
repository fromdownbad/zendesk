require_relative "../support/test_helper"
require_relative '../support/gooddata_session_helper'

SingleCov.covered! uncovered: 2

describe 'UserIdentityObserver' do
  fixtures :accounts, :users, :user_identities, :role_settings

  include GooddataSessionHelper

  before do
    @actor = users(:minimum_admin)
    CIA.stubs(:current_actor).returns(@actor)

    Account.any_instance.stubs(has_email_suppress_email_identity_update_notifications?: false)
  end

  it "doesn't fire on without a commit" do
    user = users(:minimum_agent)
    id = UserEmailIdentity.new(
      user: user,
      account: user.account,
      value: "example@gmail.com",
      is_verified: true
    )
    SecurityMailer.expects(:deliver_user_auxillary_email_added).never

    with_rollback(id) { id.save! }
  end

  describe "when checking sandbox creation" do
    let (:account) { accounts(:minimum) }
    let (:user) { users(:minimum_agent) }
    let (:identity_params) do
      {
        user: user,
        account: account,
        value: "example@gmail.com",
        is_verified: true
      }
    end

    it "doesn't fire when sandbox is being created" do
      account.settings.sandbox_creation_in_progress = true
      account.settings.save
      SecurityMailer.expects(:deliver_user_auxillary_email_added).never
      UserEmailIdentity.create(identity_params)
    end

    it "fires after sandbox has been created" do
      SecurityMailer.expects(:deliver_user_auxillary_email_added).once
      UserEmailIdentity.create(identity_params)
    end
  end

  describe "external account" do
    describe "using google" do
      before do
        @user = users(:minimum_admin)
      end

      describe "when created and still unverified" do
        it "does not send an external account added notification" do
          SecurityMailer.expects(:deliver_user_external_account_added).never
          UserEmailIdentity.create(
            user: @user,
            account: @user.account,
            value: "example@gmail.com",
            is_verified: false
          )
          @identity = @user.identities.email.last
          @identity.google_profile = GoogleProfile.create(user_identity: @identity)
        end
      end

      describe "when existing" do
        describe "user is an unverified admin on a trial account" do
          before do
            @user = users(:trial_admin)
            @identity = @user.identities.email.first
            @identity.update_attributes(is_verified: false)

            refute @user.is_verified?
          end

          it 'does not send primary email has changed notification on updating primary email' do
            SecurityMailer.expects(:deliver_user_primary_email_changed).never
            @identity.update_attributes!(value: 'updated_primary@example.com')
          end
        end

        describe "user is a verified admin on a trial account" do
          before do
            @user = users(:trial_admin)
            @identity = @user.identities.email.first

            assert @user.is_verified?
          end

          it 'sends primary email has changed notification on updating primary email' do
            SecurityMailer.expects(:deliver_user_primary_email_changed).once
            @identity.update_attributes!(value: 'updated_primary@example.com')
          end
        end

        describe "and the user is either an admin or an agent" do
          before do
            @user = users(:minimum_agent)
            UserEmailIdentity.create(
              user: @user,
              account: @user.account,
              value: "example@gmail.com",
              is_verified: false
            )
            @identity = @user.identities.email.last
            @identity.google_profile = GoogleProfile.create(user_identity: @identity)
          end

          describe "and verified" do
            it "sends an external account added notification" do
              SecurityMailer.expects(:deliver_user_external_account_added).
                with(@user, @actor, identity_type: 'google', identity_value: 'example@gmail.com').once
              @identity.update_attributes(is_verified: true)
            end

            it "doesn't email if something fails" do
              SecurityMailer.expects(:deliver_user_external_account_added).never
              with_rollback(@identity) { @identity.update_attributes(is_verified: true) }
            end
          end

          describe "and removed" do
            it "sends an external account removed notification" do
              SecurityMailer.expects(:deliver_user_external_account_removed).
                with(@user, @actor, identity_type: 'google', identity_value: 'example@gmail.com').once
              @identity.destroy
            end
          end
        end

        describe "and the user is an end-user" do
          before do
            @user = users(:minimum_end_user)
            UserEmailIdentity.create(
              user: @user,
              account: @user.account,
              value: "example@gmail.com",
              is_verified: false
            )
            @identity = @user.identities.email.last
            @identity.google_profile = GoogleProfile.create(user_identity: @identity)
          end

          describe "and verified" do
            it "does not send an external account added notification" do
              SecurityMailer.expects(:deliver_user_external_account_added).never
              @identity.update_attributes(is_verified: true)
            end
          end

          describe "and removed" do
            it "does not send an external account removed notification" do
              SecurityMailer.expects(:deliver_user_external_account_removed).never
              @identity.destroy
            end
          end
        end
      end
    end
  end

  describe "primary email" do
    before do
      @user = users(:minimum_agent)
      @identity = UserEmailIdentity.new(
        user: @user,
        account: @user.account,
        value: "primary@example.com",
        is_verified: true
      )

      @second_identity = UserEmailIdentity.new(
        user: @user,
        account: @user.account,
        value: 'auxilliary@example.com',
        is_verified: true
      )
    end

    describe "when new" do
      it "does not sync the user with GoodData" do
        assert @identity.primary?
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).never
        @identity.save!
      end
    end

    describe "when updating" do
      before do
        @identity.save!
        @identity.make_primary!
      end

      it "syncs the details with GoodData when the value changes" do
        assert @identity.primary?
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).with(@user).once
        @identity.value = "updated_primary@example.com"
        @identity.save!
      end

      it "syncs the details with GoodData when a secondary identity is promoted to primary" do
        @second_identity.save!
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).with(@user).once
        @second_identity.make_primary!
      end

      it "does not sync with GoodData when the identity hasn't changed" do
        assert @identity.primary?
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).never
        @identity.save!
      end

      it "does not sync with GoodData when the secondary identity is changed" do
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).never
        @second_identity.save!
        @second_identity.update_attribute(:value, 'auxilliary2@example.com')
      end
    end

    describe 'when updating email' do
      before do
        @identity = @user.identities.email.first
      end

      it 'sends a primary email has changed notification when only 1 email identity exists' do
        assert @identity.primary?
        assert_equal(@user.identities.length, 1)
        SecurityMailer.expects(:deliver_user_primary_email_changed).
          with(
            @user,
            @actor,
            identity_type: 'email',
            identity_value: 'updated_primary@example.com',
            previous_primary_email: @identity.to_s
          ).once
        @identity.update_attributes!(value: 'updated_primary@example.com')
      end

      it 'sends a primary email has changed notification to correct email when 2 email identities exist' do
        UserEmailIdentity.create(
          user: @user,
          account: @user.account,
          value: 'auxilliary@example.com',
          is_verified: false
        )
        @user.identities.reload
        assert_equal(@user.identities.length, 2)
        SecurityMailer.expects(:deliver_user_primary_email_changed).
          with(
            @user,
            @actor,
            identity_type: 'email',
            identity_value: 'updated_primary@example.com',
            previous_primary_email: @identity.to_s
          ).once
        @identity.update_attributes!(value: 'updated_primary@example.com')
      end
    end
  end

  describe "auxillary email" do
    before do
      @user = users(:minimum_admin)
    end

    describe "when created and still unverified" do
      it "does not send and auxillary email account added notification" do
        SecurityMailer.expects(:deliver_user_auxillary_email_added).never
        @identity = UserEmailIdentity.create(
          user: @user,
          account: @user.account,
          value: "auxilliary@example.com",
          is_verified: false
        )
      end
    end

    describe "when existing" do
      describe "and the user is either an admin or an agent" do
        before do
          @user = users(:minimum_agent)
          UserEmailIdentity.create(
            user: @user,
            account: @user.account,
            value: "auxilliary@example.com",
            is_verified: false
          )
          @identity = @user.identities.email.last
        end

        describe "and verified" do
          it "sends an auxillary email account added notification" do
            SecurityMailer.expects(:deliver_user_auxillary_email_added).
              with(@user, @actor, identity_type: 'email', identity_value: 'auxilliary@example.com').once
            @identity.update_attributes(is_verified: true)
          end
        end

        describe "and removed" do
          it "sends an auxillary email account removed notification" do
            SecurityMailer.expects(:deliver_user_auxillary_email_removed).
              with(@user, @actor, identity_type: 'email', identity_value: 'auxilliary@example.com').once
            @identity.destroy
          end
        end

        describe "and selected as primary" do
          before do
            refute @identity.primary?
          end

          it "sends a primary email has changed notification on make_primary! method call" do
            SecurityMailer.expects(:deliver_user_primary_email_changed).
              with(@user, @actor, identity_type: 'email', identity_value: 'auxilliary@example.com').once
            @identity.make_primary!
          end

          it "syncs the user with GoodData" do
            Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).
              with(@user)
            @identity.make_primary!
          end
        end
      end

      describe "and the user is an end-user" do
        before do
          @user = users(:minimum_end_user)
          UserEmailIdentity.create(
            user: @user,
            account: @user.account,
            value: "auxilliary@example.com",
            is_verified: false
          )
          @identity = @user.identities.email.last
        end

        describe "and verified" do
          it "does not send an auxillary email account added notification" do
            SecurityMailer.expects(:deliver_user_auxillary_email_added).never
            @identity.update_attributes(is_verified: true)
          end
        end

        describe "and removed" do
          it "does not send an auxillary email account removed notification" do
            SecurityMailer.expects(:deliver_user_auxillary_email_removed).never
            @identity.destroy
          end
        end

        describe "and selected as primary" do
          before do
            refute @identity.primary?
          end

          it "does not send an auxillary email account removed notification" do
            SecurityMailer.expects(:deliver_user_primary_email_changed).never
            @identity.make_primary!
          end
        end
      end
    end
  end

  describe "voice forwarding number" do
    describe "and the user is either an admin or an agent" do
      before do
        @user = users(:minimum_agent)
      end

      describe "when added" do
        it "sends a voice forwarding number added notification" do
          SecurityMailer.expects(:deliver_user_voice_forwarding_number_added).
            once
          UserVoiceForwardingIdentity.create(user: @user, value: "14155551234")
        end
      end

      describe "when removed" do
        before do
          stub_request(:put, "https://minimum.zendesk-test.com/api/v2/channels/voice/availabilities/57888.json").to_return(status: 200)
        end

        it "sends a voice forwarding number removed notification" do
          identity = UserVoiceForwardingIdentity.create(user: @user, value: "14155556789")
          SecurityMailer.expects(:deliver_user_voice_forwarding_number_removed).
            with(@user, @actor, identity_type: 'agent_forwarding', identity_value: '+14155556789').once
          identity.destroy
        end
      end
    end

    describe "and the user is an end-user" do
      before do
        @user = users(:minimum_end_user)
      end

      describe "when added" do
        it "does not send a voice forwarding number added notification" do
          SecurityMailer.expects(:deliver_user_voice_forwarding_number_added).never
          UserVoiceForwardingIdentity.create(user: @user, value: "14155558888")
        end
      end

      describe "when removed" do
        it "does not send a voice forwarding number removed notification" do
          UserVoiceForwardingIdentity.create(user: @user, value: "14155559999")
          identity = @user.identities.voice_number.last
          SecurityMailer.expects(:deliver_user_voice_forwarding_number_removed).never
          identity.destroy
        end
      end
    end
  end
end
