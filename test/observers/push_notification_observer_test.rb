require_relative "../support/test_helper"

SingleCov.covered!

describe PushNotificationObserver do
  fixtures :tickets, :accounts, :events, :users, :mobile_apps, :mobile_sdk_apps

  describe "push notifications" do
    let(:ticket) { tickets(:minimum_1) }
    let(:user) { ticket.assignee }

    before do
      PushNotifications::DeviceIdentifier.without_arsi.delete_all

      PushNotificationRegistrationJob.stubs(:enqueue)

      @assignment_on_create = Create.create!(
        account: accounts(:minimum),
        audit: events(:create_audit_for_minimum_ticket_1),
        ticket: ticket,
        value: user.id,
        value_previous: nil,
        value_reference: "assignee_id",
        author: accounts(:minimum).owner
      )
    end

    it "sends notifications when activities are created" do
      PushNotifications::FirstPartyDeviceIdentifier.create!(
        user: user,
        token: "bar",
        device_type: "ipad",
        account: accounts(:minimum),
        mobile_app: mobile_apps(:com_zendesk_agent)
      )

      PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(
        token: "bas",
        device_type: "iphone",
        account: accounts(:minimum),
        user: user,
        mobile_sdk_app: mobile_sdk_apps(:minimum_sdk)
      )

      agent_app_identifiers = user.device_identifiers.first_party.map(&:id).sort

      expected_enqueues = [
        {
          account_id: accounts(:minimum).id,
          device_ids: agent_app_identifiers,
          gateway: 'urban_airship',
          mobile_app_identifier: 'com.zendesk.agent',
          payload: {
            user_id: user.id,
            msg: "#{accounts(:minimum).owner.name} assigned ticket ##{ticket.nice_id} to you.",
            msg_short: "##{ticket.nice_id} is assigned to you.",
            detail: ticket.description,
            ticket_id: ticket.nice_id
          }
        }
      ]

      PushNotificationSendJob.expects(:enqueue).times(expected_enqueues.size).with do |options|
        options[:device_ids].sort!
        expected_enqueues.must_include options
        true
      end

      assert TicketActivity.create_activity_for(user, event: @assignment_on_create, update_type: :create)
    end

    it "does not send notifications if the user doesn't have any registered device tokens" do
      PushNotificationSendJob.expects(:enqueue).never
      TicketActivity.create_activity_for(user, event: @assignment_on_create, update_type: :create)
    end

    describe "for a ticket comment activity" do
      let(:comment_on_create) do
        Comment.create!(
          account: accounts(:minimum),
          audit: events(:create_audit_for_minimum_ticket_1),
          ticket: ticket,
          author: accounts(:minimum).owner
        )
      end

      it "sends notifications when the activity is created" do
        identifier = PushNotifications::FirstPartyDeviceIdentifier.create!(
          user: user,
          token: "baz",
          account: accounts(:minimum),
          device_type: "android",
          mobile_app: mobile_apps(:com_zendesk_agent)
        )

        PushNotificationSendJob.expects(:enqueue).with(
          account_id: accounts(:minimum).id,
          device_ids: [identifier.id],
          gateway: 'urban_airship',
          mobile_app_identifier: 'com.zendesk.agent',
          payload: {
            user_id: user.id,
            msg: "#{accounts(:minimum).owner.name} commented on ticket ##{ticket.nice_id}: .",
            msg_short: "##{ticket.nice_id} has a new comment.",
            detail: nil,
            ticket_id: ticket.nice_id
          }
        )

        assert TicketActivity.create_activity_for(user, event: comment_on_create)
      end
    end
  end
end
