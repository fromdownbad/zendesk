require_relative "../support/test_helper"

SingleCov.covered!

describe 'AccountProductStateObserver' do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:context) { Object.new }
  let(:instrumentation) { stub(perform: true) }

  before do
    @actor = users(:minimum_admin)
    CIA.stubs(:current_actor).returns(@actor)
    account.stubs(:binding).returns(context)
    Zendesk::ModelChangeInstrumentation.expects(:new).with(
      model: account,
      exec_context: context,
      source_pattern: 'lib\/zendesk\/support_accounts|app\/controllers\/api\/v2\/accounts_creation|app\/models\/jobs\/support_creation|lib\/zendesk_billing_core|app\/services\/provisioning',
      prevent_unknown_update_from: [:console],
      columns_to_instrument: ['is_active', 'is_serviceable', 'sandbox_master_id', 'deleted_at'],
      source_of_instrument: 'account_product_state_change'
    ).once.returns(instrumentation)
  end

  it 'calls Zendesk::ModelChangeInstrumentation to perform' do
    instrumentation.expects(:perform).once
    account.save!
  end
end
