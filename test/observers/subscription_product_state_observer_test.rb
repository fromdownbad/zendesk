require_relative "../support/test_helper"

SingleCov.covered!

describe 'SubscriptionProductStateObserver' do
  fixtures :subscriptions

  let(:subscription) { subscriptions(:minimum) }
  let(:context) { Object.new }
  let(:instrumentation) { stub(perform: true) }

  before do
    @actor = users(:minimum_admin)
    CIA.stubs(:current_actor).returns(@actor)
    subscription.stubs(:binding).returns(context)
    Zendesk::ModelChangeInstrumentation.expects(:new).with(
      model: subscription,
      exec_context: context,
      source_pattern: 'lib\/zendesk\/support_accounts|app\/controllers\/api\/v2|app\/models\/jobs|lib\/zendesk_billing_core|app\/services\/provisioning',
      prevent_unknown_update_from: [:console],
      columns_to_instrument: ['plan_type', 'max_agents', 'trial_expires_on', 'is_trial', 'churned_on', 'payment_method_type'],
      source_of_instrument: 'subscription_product_change'
    ).once.returns(instrumentation)
  end

  it 'calls Zendesk::ModelChangeInstrumentation to perform' do
    instrumentation.expects(:perform).once
    subscription.save!
  end
end
