require_relative "../support/test_helper"

SingleCov.covered!

describe 'GooddataSyncObserver' do
  fixtures :users

  before do
    @user = users(:minimum_end_user)
    @gooddata_user = GooddataUser.create! do |u|
      u.account = accounts(:minimum)
      u.user = users(:minimum_agent)
      u.gooddata_project_id = 'gooddata_project_id'
      u.gooddata_user_id = 'gooddata_user_id'
    end
  end

  describe "user is saved" do
    describe "calling observer" do
      before { @user.save! }

      before_should "not sync GooddataUser" do
        GooddataSyncObserver.expects(:sync_gooddata_user).never
      end
    end

    describe "user not changed" do
      before { @user.save! }

      before_should "not sync GooddataUser" do
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).
          with(@user).never
      end
    end

    describe "user changed but not in relevant way" do
      before do
        @user.phone = "04929818201"
        @user.save!
      end

      before_should "not sync GooddataUser" do
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).
          with(@user).never
      end
    end

    describe "user role changed" do
      before do
        @user.roles = Role::AGENT.id
        @user.save!
      end

      before_should "sync" do
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).
          with(@user)
      end
    end

    describe "user permission_set changed" do
      before do
        @user.permission_set = PermissionSet.create(name: 'test permission set', account: @user.account)
        @user.save!
      end

      before_should "sync" do
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).
          with(@user)
      end
    end

    describe "user name changed" do
      let(:agent) { users(:minimum_agent) }

      before do
        agent.name = "hi my name is"
        agent.save!
      end

      before_should "sync" do
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).
          with(agent)
      end
    end
  end

  describe "user is destroyed" do
    before do
      @user.destroy
    end

    before_should "call observer" do
      Zendesk::Gooddata::UserProvisioning.expects(:destroy_for_user).
        with(@user)

      # in order for destroy through relations to work
      [Change, Comment, Notification].each do |klass|
        klass.any_instance.stubs(new_record?: true)
      end
    end
  end

  describe "user is created" do
    before do
      @new_user = User.new(@user.dup.attributes)
      @new_user.account_id = @user.account.id
      @new_user.name = "new user"
      @new_user.email = "new@user.com"
    end

    describe "user is an agent" do
      before do
        @new_user.roles = Role::AGENT.id
        @new_user.save!
      end

      before_should "create a new Gooddata User" do
        Zendesk::Gooddata::UserProvisioning.expects(:create_for_user).with(@new_user).once
      end

      before_should "not sync a new user" do
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).never
      end
    end

    describe "user is not an agent" do
      before do
        @new_user.roles = Role::END_USER.id
        @new_user.save!
      end

      before_should "create a new Gooddata User" do
        Zendesk::Gooddata::UserProvisioning.expects(:create_for_user).never
      end

      before_should "not sync a new user" do
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).never
      end
    end
  end
end
