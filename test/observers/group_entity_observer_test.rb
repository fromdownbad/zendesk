require_relative "../support/test_helper"

SingleCov.covered!

describe GroupEntityObserver do
  fixtures :accounts
  fixtures :groups
  fixtures :users

  let(:account) { accounts(:minimum) }

  describe '#publish_group_or_tombstone' do
    describe 'with publish_group_entities feature flag disabled' do
      before do
        Account.any_instance.stubs(:has_publish_group_entities?).returns(false)
      end

      let(:group) { groups(:minimum_group) }

      it 'should not publish any change' do
        GroupPublisher.expects(:new).never

        group.name = "Nothing happens"
        group.save
      end
    end

    describe 'with publish_group_entities feature flag enabled' do
      before do
        Account.any_instance.stubs(:has_publish_group_entities?).returns(true)
      end

      describe 'when the group is created' do
        let(:group) do
          g = Group.new
          g.account = account
          g.name = 'Test group'
          g.is_active = true

          g
        end

        it 'should publish the group entity' do
          GroupPublisher.any_instance.expects(:publish).with(group: group)

          group.save
        end
      end

      describe 'when the group is updated' do
        let(:group) { groups(:minimum_group) }

        it 'should publish the group entity' do
          GroupPublisher.any_instance.expects(:publish).with(group: group)

          group.name = 'Another name'
          group.save
        end
      end

      describe 'when deleting' do
        let(:agent) { users(:minimum_agent) }

        before do
          stub_request(:put, %r{/api/v2/channels/voice/internal/groups/\d+/remove_from_routing}).
            to_return(status: 200)

          group.current_user = agent

          # in order to delete a group another one must exists
          other = Group.new
          other.account = account
          other.is_active = true
          other.name = "The last one standing"
          other.save!
        end

        describe 'when the group is deactivated' do
          let(:group) { groups(:minimum_group) }

          it 'should publish the tombstone' do
            GroupPublisher.any_instance.expects(:publish_tombstone).with(group_id: group.id)

            group.deactivate!
          end
        end

        describe 'when the group is deleted' do
          let(:group) { groups(:minimum_group) }

          it 'should publish the tombstone' do
            GroupPublisher.any_instance.expects(:publish_tombstone).with(group_id: group.id)

            group.destroy
          end
        end
      end
    end
  end
end
