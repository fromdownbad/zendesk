require_relative "../support/test_helper"
require 'active_support/cache/libmemcached_local_store'

SingleCov.covered! uncovered: 22

describe CachingObserver do
  fixtures :accounts, :rules, :groups, :organizations, :users, :ticket_fields,
    :tickets, :brands, :account_settings, :role_settings, :ticket_forms, :custom_field_options

  def self.settings_expiry_for(setting)
    it "expires when setting is turned off" do
      @account.settings.send("#{setting}=", true)
      @account.save
      expiry_for(instance_of(Account)).expects(:expire_radar_cache_key).with(:settings)
      @account.settings.send("#{setting}=", false)
      @account.save
    end

    it "expires when setting is turned on" do
      @account.settings.send("#{setting}=", false)
      @account.save
      expiry_for(instance_of(Account)).expects(:expire_radar_cache_key).with(:settings)
      @account.settings.send("#{setting}=", true)
      @account.save
    end
  end

  describe Macro do
    it "expires radar cache" do
      macro = rules(:macro_for_minimum_agent)
      expiry_for(macro.account).expects(:expire_radar_cache_key).with(:macros)
      macro.definition.actions.clear
      macro.definition.actions << DefinitionItem.new('comment_value', nil, ['World'])
      macro.save!
    end
  end

  describe View do
    it "expires radar cache for the view" do
      view = rules(:view_assigned_working_tickets)
      expiry = expiry_for(view.account)
      expiry.expects(:expire_radar_cache_key).with(:views)
      expiry.expects(:expire_radar_cache_key).with("views/#{view.id}/tickets")
      view.title = "New title"
      view.save
    end
  end

  describe TicketPrediction do
    it "expires radar cache for a ticket prediction" do
      @ticket = tickets(:minimum_2)
      @ticket.satisfaction_probability = 0.1
      expiry = expiry_for(@ticket.account)
      expiry.expects(:expire_radar_cache_key).with("tickets/#{@ticket.nice_id}/ticket_prediction")
      @ticket.will_be_saved_by(@agent)
      @ticket.save!
    end
  end

  describe "View reordering" do
    let(:account) { accounts(:minimum) }
    let(:expiry)  { expiry_at_least_once_for(instance_of(Account)) }

    before_should "expire radar cache" do
      expiry.stubs(:expire_radar_cache_key)
      expiry.expects(:expire_radar_cache_key).with(:views).at_least_once

      account.views.update_position(account.views.map(&:id).sort)
    end
  end

  describe Group do
    it "expires radar cache" do
      group = groups(:with_groups_group1)
      expiry_for(group.account).expects(:expire_radar_cache_key).with(:groups)
      group.account.expects(:expire_scoped_cache_key).with(:views)
      group.account.expects(:expire_scoped_cache_key).with(:macros)
      group.account.expects(:expire_scoped_cache_key).with(:groups_agents)
      group.name = "New group name"
      group.save
    end
  end

  describe Organization do
    it "expires radar cache" do
      organization = organizations(:minimum_organization1)
      expiry_for(organization.account).expects(:expire_radar_cache_key).with(:organizations)
      organization.name = "New organization name"
      organization.save
    end

    it "does not expire organization_whitelist if non-domain attributes are changed" do
      organization = organizations(:minimum_organization1)
      organization.account.expects(:expire_scoped_cache_key).never
      organization.notes = "The Notebook"
      organization.save
    end
  end

  describe User do
    it "expires radar cache for agent update" do
      user = users(:minimum_agent)
      expiry_for(user.account).expects(:expire_radar_cache_key).with(:users)
      user.name = "New Name"
      user.save
    end

    it "does not expire cache for user update" do
      user = users(:minimum_end_user)
      expiry_never
      user.name = "New Name"
      user.save
    end

    it "does not expire cache for user creation" do
      account = accounts(:minimum)
      expiry_never
      User.create! do |user|
        user.name = "Jim Beam"
        user.roles = Role::AGENT.id
        user.account = account
        user.email = 'agent@zendesk.com'
      end
    end

    it "does not expire cache for only lotus_last_login" do
      user = users(:minimum_agent)

      # The first user save has extra baggage, so save once first
      user.save

      expiry_never

      user.settings.last_lotus_login = Time.now
      user.save
    end

    it "expires groups_agents on agent suspension" do
      user = users(:minimum_agent)
      account = user.account
      user.save! # gotta do this here because Membership gets tweaked during save which also clears groups_agents

      old_key = account.scoped_cache_key(:groups_agents)
      user.current_user = account.owner
      user.suspend!

      Timecop.travel(Time.now + 1.seconds)
      account.scoped_cache_key(:groups_agents).wont_equal old_key
    end

    it "expires groups_agents on agent downgrade" do
      user = users(:minimum_agent)
      account = user.account
      user.save! # gotta do this here because Membership gets tweaked during save which also clears groups_agents

      old_key = account.scoped_cache_key(:groups_agents)
      user.current_user = account.owner
      user.roles = Role::END_USER.id
      user.save!

      Timecop.travel(Time.now + 1.seconds)
      account.scoped_cache_key(:groups_agents).wont_equal old_key
    end

    it "expires groups_agents on changes to agent permission_set_id" do
      user = users(:minimum_agent)
      account = user.account
      user.save! # gotta do this here because Membership gets tweaked during save which also clears groups_agents
      ps = PermissionSet.create_light_agent!(account)

      old_key = account.scoped_cache_key(:groups_agents)
      user.current_user = account.owner
      user.permission_set_id = ps.id
      user.save!

      Timecop.travel(Time.now + 1.seconds)
      account.scoped_cache_key(:groups_agents).wont_equal old_key
    end

    it "expires groups_agents on agent deletion" do
      user = users(:minimum_agent)
      account = user.account
      old_key = account.scoped_cache_key(:groups_agents)

      user.current_user = account.owner
      user.delete!

      Timecop.travel(Time.now + 1.seconds)
      account.scoped_cache_key(:groups_agents).wont_equal old_key
    end
  end

  describe "groups_agents" do
    before do
      @group     = groups(:support_group)
      @user      = users(:minimum_agent)
      @account   = accounts(:minimum)
      @cache_key = @account.scoped_cache_key(:groups_agents)

      refute @user.groups.include?(@group)
    end

    # Using `membership.new` here on purpose to catch instances of `.new`
    # in the wild (vs. `.build` or `<<`)
    describe "Creating a new membership without going through user or group" do
      before do
        @user.memberships.new(user: @user, group: @group).save!
      end

      it "blows the groups_agents cache" do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal @cache_key, @account.scoped_cache_key(:groups_agents)
      end
    end
  end

  describe PermissionSet do
    it "expires radar cache" do
      expiry_for(instance_of(Account)).expects(:expire_radar_cache_key).with(:users)
      PermissionSet.create(account: accounts(:minimum), name: "things")
    end

    it "expires groups_agents cache" do
      account = accounts(:minimum)
      ps = PermissionSet.create(account: account, name: "custom")

      assert ps.permissions.ticket_editing # an example that changes assignables

      cache_key = account.scoped_cache_key(:groups_agents)

      ps.permissions.ticket_editing = false
      ps.save!

      Timecop.travel(Time.now + 1.seconds)
      assert_not_equal cache_key, account.scoped_cache_key(:groups_agents)
    end
  end

  describe TicketFormBrandRestriction do
    let(:account) { accounts(:minimum) }
    let(:brand)   { account.default_brand }

    it "expires brands and ticket_forms cache" do
      ticket_form = TicketForm.init_default_form(account)
      ticket_form.save!

      Account.any_instance.expects(:expire_scoped_cache_key).with(:brands)
      Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_forms)

      TicketFormBrandRestriction.create! do |tfbr|
        tfbr.account = account
        tfbr.brand = brand
        tfbr.ticket_form = ticket_form
      end
    end
  end

  describe TicketFieldCondition do
    let(:account) { accounts(:minimum) }
    let(:ticket_form) { ticket_forms(:minimum_ticket_form) }
    let(:parent_field) { ticket_fields(:field_tagger_custom) }
    let(:child_field) { ticket_fields(:field_textarea_custom) }
    let(:cfo1) { custom_field_options(:field_tagger_custom_option_2) }

    before do
      ticket_form.account_id = account.id
      TicketFormField.create(
        ticket_form: ticket_form,
        account: account,
        ticket_field: parent_field,
        position: 1
      )
      TicketFormField.create(
        ticket_form: ticket_form,
        account: account,
        ticket_field: child_field,
        position: 2
      )
    end

    it "expires ticket_forms cache" do
      Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_forms)
      # This is to stop the callback from saving the account and expiring other caches
      TicketFieldCondition.any_instance.stubs(:mark_account_as_migrated)

      TicketFieldCondition.create! do |c|
        c.account = account
        c.ticket_form = ticket_form
        c.parent_field = parent_field
        c.child_field = child_field
        c.value = cfo1.value
        c.user_type = :agent
        c.is_required = true
      end
    end
  end

  describe CustomField::Field do
    it "expires radar cache for user fields" do
      expiry_for(instance_of(Account)).expects(:expire_radar_cache_key).with(:user_fields)
      cf = accounts(:minimum).custom_fields.build(key: "field", title: "field", owner: "User")
      cf.type = "Integer"
      cf.save!
    end

    it "expires radar cache for organization fields" do
      expiry_for(instance_of(Account)).expects(:expire_radar_cache_key).with(:organization_fields)
      cf = accounts(:minimum).custom_fields.build(key: "field", title: "field", owner: "Organization")
      cf.type = "Integer"
      cf.save!
    end
  end

  describe "ticket forms" do
    before do
      @account = accounts(:minimum)
      assert @ticket_form = TicketForm.init_default_form(@account)
      assert @ticket_form.save!
      @old_cache_key = @account.scoped_cache_key(:ticket_forms)
      @old_brands_cache_key = @account.scoped_cache_key(:brands)
    end

    describe "Saving a ticket form" do
      before do
        @ticket_form.save!
      end

      it "updates the ticket_forms cache key" do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal @old_cache_key, @account.scoped_cache_key(:ticket_forms)
      end

      it "updates the brands cache key" do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal @old_brands_cache_key, @account.scoped_cache_key(:brands)
      end
    end

    describe "Deleting a ticket form" do
      before do
        assert @ticket_form.destroy
      end

      it "updates the ticket_forms cache key" do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal @old_cache_key, @account.scoped_cache_key(:ticket_forms)
      end

      it "updates the brands cache key" do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal @old_brands_cache_key, @account.scoped_cache_key(:brands)
      end
    end

    describe "Removing an existing field from the form" do
      before do
        assert field = @account.ticket_fields.last
        assert @ticket_form.ticket_fields.include?(field)
        assert ids = @ticket_form.ticket_fields.map(&:id) - [field.id]

        initializer = Zendesk::TicketForms::Initializer.new(@account,           id: @ticket_form.id,
                                                                                ticket_form: { ticket_field_ids: ids })

        initializer.updated_ticket_form.save!
      end

      it "updates the cache key" do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal @old_cache_key, @account.scoped_cache_key(:ticket_forms)
      end
    end

    describe "Deleting a ticket field that is currently in the form" do
      before do
        assert field = @account.ticket_fields.custom.last
        assert @ticket_form.ticket_fields.include?(field)
        assert field.destroy, "#{field.class}\n#{field.errors.to_xml}"
      end

      it "updates the cache key" do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal @old_cache_key, @account.scoped_cache_key(:ticket_forms)
      end
    end
  end

  describe Account do
    it "does not expire radar cache for account" do
      account = accounts(:minimum)
      expiry_never
      account.help_desk_size = "99999"
      account.save!
    end

    it "expires radar cache when propagating name changes to the brand" do
      account = accounts(:minimum)
      assert_equal 1, account.brands.size

      # Name changes propagate to the default brand (when there is just the one brand)
      account.name = "Foo"

      expiry_for(account).expects(:expire_radar_cache_key).with(:brands)
      account.save
    end
  end

  describe Brand do
    it "expires radar cache when updated" do
      brand = accounts(:minimum).brands.first
      expiry_for(brand.account).expects(:expire_radar_cache_key).with(:brands)
      brand.name = "Foo"
      brand.save!
    end
  end

  describe AccountSetting do
    before do
      @account = accounts(:minimum)
    end

    describe "radar cache for chat" do
      settings_expiry_for(:chat)
    end

    describe "radar cache for voice" do
      settings_expiry_for(:voice)
    end

    describe "radar cache for voice_outbound_enabled" do
      settings_expiry_for(:voice_outbound_enabled)
    end

    describe "when the `macro_order` setting is updated" do
      before do
        @account.settings.disable(:macro_order)

        @account.save!
      end

      before_should "expire the `macros` radar cache" do
        expiry_for(@account).expects(:expire_radar_cache_key).with(:macros)
      end
    end

    describe "Updating on hold status" do
      before do
        @old_cache_key = @account.scoped_cache_key(:settings)
        @account.field_status.use_status_hold = !@account.field_status.use_status_hold
        @account.field_status.save!
      end

      it "blows the settings cache key" do
        Timecop.travel(Time.now + 1.seconds)
        assert_not_equal @old_cache_key, @account.scoped_cache_key(:settings)
      end
    end
  end

  describe AccountPropertySet do
    before do
      @account = accounts(:minimum)
      @property_set = @account.account_property_set
    end

    it 'expires settings on save' do
      cache_key = @account.scoped_cache_key(:settings)
      @property_set.update_attributes!(ticket_forms_instructions: 'hi')
      Timecop.travel(Time.now + 1.seconds)
      assert_not_equal @account.scoped_cache_key(:settings), cache_key
    end
  end

  describe Activity do
    before do
      @user = users(:minimum_agent)
      @ticket = tickets(:minimum_1)
    end

    it "expires user activity on save" do
      cache_key = @user.scoped_cache_key(:activity)
      @ticket.add_comment(body: "hi", is_public: true)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.save!
      Timecop.travel(Time.now + 1.seconds)
      assert_not_equal @user.scoped_cache_key(:activity), cache_key
    end

    it "expires user activity when the parent ticket is soft deleted" do
      cache_key = @user.scoped_cache_key(:activity)
      @ticket.will_be_saved_by(@ticket.requester)
      @ticket.soft_delete!
      Timecop.travel(Time.now + 1.seconds)
      assert_not_equal @user.scoped_cache_key(:activity), cache_key
    end
  end

  describe Sharing::Agreement do
    before do
      @account = accounts(:minimum)
      @old_size = @account.sharing_agreements.size
      @old_key = @account.scoped_cache_key(:sharing_agreements)

      Timecop.travel(10.seconds.from_now)
    end

    it "expires cache for sharing agreements after creating a new agreement" do
      Sharing::Agreement.any_instance.stubs(:send_agreement_request_to_partner)
      @account.sharing_agreements.create!(subdomain: accounts(:support).subdomain)

      assert_equal(@old_size + 1, @account.sharing_agreements.size)
      assert_not_equal @old_key, @account.scoped_cache_key(:sharing_agreements)
    end
  end

  describe UserIdentity do
    before do
      @user = users(:minimum_end_user)
      assert_equal(8, @user.identities.reload.size)
      @old_key = @user.cache_key
      Timecop.travel(10.seconds.from_now)
    end

    it "expires cache for user after saving identity" do
      @user.identities.last.make_primary!
      assert_not_equal @old_key, @user.reload.cache_key
    end

    it "expires cache for user after destroying identity" do
      @user.identities.last.destroy
      assert_not_equal @old_key, @user.reload.cache_key
    end
  end

  describe 'when there is an exception' do
    let(:exception) { StandardError.new("something went wrong") }

    it 'the exception is logged and raised' do
      assert_raises StandardError, "Max number of fraud scores for account" do
        Account.any_instance.expects(:expire_scoped_cache_key).with(:brands).raises(exception).once
        Rails.logger.expects(:info).with(regexp_matches(/Exception in #expire_fragments_on_save_or_destroy/)).once
        # Update brand to trigger #expire_fragments_on_save_or_destroy
        account = accounts(:minimum)
        brand = account.brands.first
        brand.name = "Foobar"
        brand.save!
      end
    end
  end

  def expiry_never
    mock('Zendesk::RadarExpiry').tap do |_expiry|
      Zendesk::RadarExpiry.expects(:get).never
    end
  end

  def expiry_for(account)
    mock('Zendesk::RadarExpiry').tap do |expiry|
      Zendesk::RadarExpiry.stubs(:get).with(account).returns(expiry)
    end
  end

  def expiry_at_least_once_for(account)
    mock('Zendesk::RadarExpiry').tap do |expiry|
      Zendesk::RadarExpiry.stubs(:get).with(account).returns(expiry).at_least_once
    end
  end
end
