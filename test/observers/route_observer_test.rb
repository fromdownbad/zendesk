require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe RouteObserver do
  fixtures :accounts, :routes, :brands
  let(:account) { accounts(:minimum) }
  let(:account_route) { account.routes.create!(subdomain: 'route') }

  before do
    Route.any_instance.stubs(valid_cname_record?: true)
    Subscription.any_instance.stubs(has_host_mapping?: true)
  end

  describe "cache busting" do
    # The lazy load to route causes one reload so reference it ahead of time
    before do
      account_route.subdomain = 'foo'
      account.url
    end

    it "clears the url generator" do
      assert account.instance_variable_get(:@url_generator), 'Expected url_generator to be defined'
      account_route.save!
      assert_nil account.instance_variable_get(:@url_generator)
    end

    it "manually reloads the route after update" do
      account.route.expects(:reload)
      account_route.save!
    end
  end

  it "does not request a certificate without the setting" do
    AcmeCertificateJobStatus.expects(:enqueue_with_status!).never
    account.routes.create!(subdomain: 'route', host_mapping: 'example.com')
  end

  describe "with automatic_certificate_provisioning" do
    before do
      account.settings.automatic_certificate_provisioning = true
      account.settings.save!
    end

    describe 'create' do
      it "requests a certificate when hostmapped" do
        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id).once
        account.routes.create!(subdomain: 'route', host_mapping: 'example.com')
      end

      it "does not request a certificate without hostmapping" do
        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id).never
        account.routes.create!(subdomain: 'route')
      end
    end

    describe 'update' do
      it "requests a certificate when adding hostmapping" do
        assert_nil account_route.host_mapping

        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id)
        account_route.update_attributes!(host_mapping: 'example.com')
      end

      it "requests a certificate when changing hostmapping" do
        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id)
        account_route.update_attributes!(host_mapping: 'example.com')

        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id).once
        account_route.update_attributes!(host_mapping: 'another-example.com')
      end

      it "does not request a certificate when removing hostmapping" do
        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id)
        account_route.update_attributes!(host_mapping: 'example.com')

        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id).never
        account_route.update_attributes!(host_mapping: nil)
      end

      it "does not request a certificate or error when changing an unrelated attribute" do
        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id)
        account_route.update_attributes!(host_mapping: 'example.com')

        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id).never
        ZendeskExceptions::Logger.expects(:record_or_raise).never
      end
    end

    describe 'delete' do
      it "does not request a certificate" do
        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id)
        account_route.update_attributes!(host_mapping: 'example.com')

        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id).never
        account_route.destroy
      end
    end
  end

  describe "brand publishing" do
    describe "without publish_brand_entities settings activated" do
      it "doesn't call brand publisher" do
        account_route.account.stubs(:has_publish_brand_entities?).returns(false)
        BrandPublisher.any_instance.expects(:publish_for_route).never

        account_route.update_attributes!(host_mapping: 'example.com')
      end
    end

    describe "with publish_brand_entities settings activated" do
      it 'calls brand publisher when a route is updated' do
        account_route.account.stubs(:has_publish_brand_entities?).returns(true)
        BrandPublisher.any_instance.expects(:publish_for_route).with(route_id: account_route.id)

        account_route.update_attributes!(host_mapping: 'newnewexample.com')
      end

      it 'calls brand publisher when a route is updated' do
        account.stubs(:has_publish_brand_entities?).returns(true)
        BrandPublisher.any_instance.expects(:publish_for_route)

        account.routes.create!(subdomain: 'route')
      end
    end
  end
end
