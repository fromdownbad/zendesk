require_relative "../support/test_helper"

SingleCov.covered!

describe UserSeatChangesObserver do
  fixtures :users

  let(:instrumentation) { stub(perform: true) }
  let(:user) { users(:with_paid_voice_and_zopim_subscription_owner) }
  let(:user_seat) { user.user_seats.create(account: user.account, seat_type: 'voice', role: 0) }
  let(:context) { Object.new }

  before do
    user_seat.stubs(:binding).returns(context)
  end

  describe 'when update' do
    it 'calls Zendesk::ModelChangeInstrumentation to perform' do
      Zendesk::ModelChangeInstrumentation.expects(:new).with(
        model: user_seat,
        exec_context: context,
        columns_to_instrument: ['is_active', 'seat_type', 'user_id'],
        source_of_instrument: 'user_seat_change'
      ).once.returns(instrumentation)

      user_seat.is_active = false
      user_seat.save!
    end
  end

  describe 'when destroy' do
    before do
      stub_request(:post, "https://withpaidvoiceandzopimsubscription.zendesk-test.com/api/v2/channels/voice/availabilities/#{user.id}/voice_seat_lost.json")
    end

    it 'calls Zendesk::ModelChangeInstrumentation to perform' do
      Zendesk::ModelChangeInstrumentation.expects(:new).with(
        model: user_seat,
        exec_context: context,
        columns_to_instrument: ['is_active', 'seat_type', 'user_id'],
        source_of_instrument: 'user_seat_change'
      ).once.returns(instrumentation)

      user_seat.destroy
    end
  end
end
