require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'AccountTextObserver' do
  fixtures :users

  describe "account text change notification" do
    before do
      @actor = users(:minimum_agent)
      @actor.stubs(:name).returns("Curious George")
      current_transaction = { actor: @actor }
      CIA.stubs(:current_transaction).returns(current_transaction)

      @user    = users(:minimum_admin)
      @account = @user.account
    end

    describe "with the feature enabled" do
      before do
        Arturo.enable_feature!(:security_email_notifications_for_account_settings)
      end

      describe "ip_restriction" do
        describe "when nil" do
          before do
            assert_nil @account.texts.ip_restriction
          end

          describe "and set to an empty string" do
            it "does not send an ip restriction changed notification" do
              SecurityMailer.expects(:deliver_ip_restriction_changed).never
              @account.texts.ip_restriction = ''
              @account.save!
            end
          end

          describe "and set to a non-empty string" do
            it "sends an ip restriction changed notification" do
              SecurityMailer.expects(:deliver_ip_restriction_changed).once
              @account.texts.ip_restriction = 'localhost'
              @account.save!
            end
          end
        end

        describe "when not blank" do
          before do
            @account.texts.ip_restriction = 'localhost'
            @account.save!
            refute @account.texts.ip_restriction.blank?
          end

          describe "and set to the same value" do
            it "does not send an ip restriction changed notification" do
              SecurityMailer.expects(:deliver_ip_restriction_changed).never
              @account.texts.ip_restriction = 'localhost'
              @account.save!
            end
          end

          describe "and set to a different value" do
            it "sends an ip restriction changed notification" do
              SecurityMailer.expects(:deliver_ip_restriction_changed).once
              @account.texts.ip_restriction = '0.0.0.0'
              @account.save!
            end
          end

          describe "and set to blank" do
            it "sends an ip restriction changed notification" do
              SecurityMailer.expects(:deliver_ip_restriction_changed).once
              @account.texts.ip_restriction = nil
              @account.save!
            end
          end
        end
      end
    end

    describe "with the feature disabled" do
      before do
        Arturo.disable_feature!(:security_email_notifications_for_account_settings)
      end

      describe "ip_restriction" do
        describe "when changed" do
          it "does not send an ip restriction changed notification" do
            SecurityMailer.expects(:deliver_ip_restriction_changed).never
            @account.texts.ip_restriction = "127.0.0.1"
            @account.save
          end
        end
      end
    end
  end
end
