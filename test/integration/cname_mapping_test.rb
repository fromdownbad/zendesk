require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class CnameMappingTest < ActionDispatch::IntegrationTest
  include ZendeskDSL
  fixtures :all

  before do
    @mapped_domain = "hello.example.com"
    @account = accounts(:minimum)

    https!(false)
  end

  it "does not find unmapped domains" do
    refute @account.host_mapping

    self.host = @mapped_domain
    get "/portal"
    # Redirects to marketing page
    assert_response :redirect
    assert_redirected_to "https://www.zendesk.com/app/help-center-closed/?utm_source=helpcenter-closed&utm_medium=poweredbyzendesk&utm_campaign=text&utm_content=#{@mapped_domain}"
  end

  describe "with host mapping" do
    before do
      @account.update_attribute(:host_mapping, @mapped_domain)
      assert @account.host_mapping

      Account.any_instance.stubs(:has_public_forums?).returns(subject)
    end

    describe "with public forums" do
      subject { true }

      it "has access to mapped domains with public forums" do
        self.host = @mapped_domain
        get "/portal"
        assert_response 200
      end

      describe "with SSL" do
        before do
          @account.update_attributes!(is_ssl_enabled: true)
          @account.certificates.first.update_attribute(:state, "active")
          @account.certificates.first.update_attribute(:valid_until, 10.days.from_now.to_date)
          assert @account.certificates.active.any?
        end

        it "redirects to SSLd host-mapped domain" do
          https!

          self.host = @account.host_name(mapped: false)
          get "/"
          assert_response :redirect
          assert_redirected_to "https://#{@mapped_domain}/access"
        end
      end
    end

    describe "without public forums" do
      subject { false }

      it "does not have access to mapped domains without public forums" do
        self.host = @mapped_domain
        get "/portal"
        assert_redirected_to "http://#{@mapped_domain}/access/unauthenticated?return_to=http%3A%2F%2F#{@mapped_domain}%2Fportal"
      end

      describe "when logging in" do
        before do
          @user = users(:minimum_agent)
        end

        it "does not redirect loop" do
          self.host = @mapped_domain

          https!

          Timeout.timeout(10) do # rubocop:disable Lint/Timeout
            logs_in_as(@user, "123456")
            assert true
          end
        end
      end

      describe "with SSL, but no certificate" do
        before do
          @account.update_attributes!(is_ssl_enabled: true)
          assert_empty(@account.certificates.active)
        end

        it "does not redirect loop" do
          self.host = @mapped_domain
          get "/"
          assert_redirected_to "https://#{@account.host_name(mapped: false)}/access"
        end

        it "redirects to https on endpoint that doesn't skip ensure_proper_protocol" do
          self.host = @mapped_domain
          get '/generated/javascripts/user.json'
          assert_redirected_to "https://#{@account.host_name(mapped: false)}/generated/javascripts/user.json"
        end
      end
    end
  end
end
