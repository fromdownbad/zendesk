require_relative "../support/test_helper"
require_relative "../support/attachment_test_helper"
require_relative "../support/archive_helper"

class TicketScrubIntegrationTest < ActionDispatch::IntegrationTest
  fixtures :all

  def create_and_soft_delete_ticket(user)
    account = accounts(:minimum)
    Account.any_instance.stubs(:has_ticket_scrubbing?).returns(true)

    ticket = account.tickets.new(requester: user, description: "I'm going to be scrubbed soon")

    ticket.will_be_saved_by(user)
    ticket.external_id = "yabadabadu"
    ticket.will_be_saved_by(user)

    identifier_eml = "1/msg.eml"
    identifier_json = "1/msg.json"
    ticket.audit.metadata[:system][:raw_email_identifier] = identifier_eml
    audit = ticket.audit
    ticket.save!
    audit.update_attribute :via_id, ViaType.MAIL
    create_remote_file(identifier_eml, content: "e-mail message text", content_type: "message/rfc822")
    create_remote_file(identifier_json, content: '"body": "e-mail message text"', content_type: "application/json")

    ticket.add_comment(body: "some comment also will be scrubbed", is_public: true)
    ticket.will_be_saved_by(user)
    ticket.save!

    CIA::Event.create!(
      account: account,
      actor: account.owner,
      source: ticket,
      action: "create",
      visible: true
    )

    # deprecated but still needs to be scrubbed
    ticket.class.where(id: ticket.id, account_id: ticket.account_id).update_all(latest_recipients: "fred@bedrock.city wilma@bedrock.city")

    ticket.will_be_saved_by(user)
    ticket.soft_delete!

    attachment1 = create_attachment("#{Rails.root}/test/files/normal_1.jpg", ticket.requester)
    attachment1.update_attribute :ticket_id, ticket.id
    attachment1.update_column :source_type, "Comment"
    attachment1.update_column :source_id, ticket.comments.last.id

    attachment2 = create_attachment("#{Rails.root}/test/files/normal_1.jpg", ticket.requester)
    attachment2.update_column :size, ticket.account.subscription.file_upload_cap.megabytes * 2
    attachment2.update_attribute :ticket_id, ticket.id
    attachment2.update_column :source_type, "Comment"
    attachment2.update_column :source_id, ticket.comments.last.id

    ticket.reload
    ticket
  end

  def raw_email(identifier)
    RemoteFiles::File.new(
      identifier,
      configuration: Zendesk::Mail.raw_remote_files.name,
      stored_in: Zendesk::Mail.raw_remote_files.stores
    )
  end

  def create_remote_file(identifier, params = {})
    default_params = {
      configuration: Zendesk::Mail.raw_remote_files.name
    }

    RemoteFiles::File.new(identifier, default_params.merge(params)).store_once!
  end

  def run_deletion_job(ticket)
    job = Zendesk::Maintenance::Jobs::TicketDeletionJob.new '12345', account_id: ticket.account_id, user_id: User.system.id, tickets_ids: Array(ticket.id)
    job.perform
    ticket.reload
  end

  def object_class(record, attribute)
    attr_type = record.type_for_attribute(attribute)
    attr_type.coder.try(:object_class) if attr_type.is_a?(ActiveRecord::Type::Serialized)
  end

  def stringy_column_names(klass)
    if RAILS5
      klass.columns.select { |c| [:text, :string].include? c.type }.map(&:name)
    else
      klass.columns.select(&:text?).map(&:name)
    end
  end

  before do
    @user = users(:minimum_end_user)
    @ticket = create_and_soft_delete_ticket(@user)
    @soft_deletion_event = @ticket.soft_deletion_event
    run_deletion_job(@ticket)
  end

  describe "a hard deleted ticket" do
    it "deletes attachments from ticket" do
      attachments = Attachment.where(account_id: @ticket.account_id, ticket_id: @ticket.id)
      attachments.each do |attachment|
        assert_empty attachment.stores
        assert attachment.size > 0 # rubocop:disable Style/ZeroLengthPredicate
      end

      # thumbnails are also setting ticket_id
      assert attachments.size >= 1
    end

    it "deletes inbound e-mail" do
      audits = @ticket.audits.where via_id: ViaType.MAIL

      audits.each do |audit|
        remote_file = raw_email(audit.raw_email)
        refute remote_file.content, "remote file should be deleted"
      end

      assert_equal 1, audits.size
    end

    describe "ticket columns" do
      it "scrubs infered columns" do
        columns = stringy_column_names(@ticket.class) - ["subject"]
        columns.each do |column|
          attribute = @ticket.send(column)
          assert_equal Zendesk::Scrub::SCRUB_TEXT, attribute unless attribute.blank?
        end
      end

      it "scrub specific columns" do
        columns = %w[description recipient current_tags current_collaborators external_id original_recipient_address latest_recipients]
        columns.each do |column|
          attribute = @ticket.send(column)
          assert_equal Zendesk::Scrub::SCRUB_TEXT, attribute, "#{attribute} expected to be #{Zendesk::Scrub::SCRUB_TEXT}" unless attribute.blank?
        end
      end

      it "scrubs subject with SCRUBBED" do
        assert_equal "SCRUBBED", @ticket.subject
      end
    end

    it "won't scrub the soft deletion event" do
      event = @ticket.soft_deletion_event
      assert_equal event.value, StatusType.DELETED.to_s
      assert_equal event.value_reference, "status_id"
    end

    it "scrubs tickets associations" do
      assoc = Zendesk::Scrub.send(:dependent_associations, @ticket)

      associations = [
        :taggings,
        :ticket_schedule,
        :cia_events,
        :cia_attribute_changes,
        :latest_comment,
        :latest_public_comment,
        :first_comment,
        :ticket_metric_set,
        :audits,
        :collaborations,
        :comments,
        :recent_comments,
        :public_comments,
        :events,
        :redaction_events,
        :tweets,
        :ticket_field_entries,
        :request_token,
        :target_links,
        :source_links,
        :followup_target_links,
        :bookmarks,
        :activities,
        :skips,
        :twitter_actions,
        :voice_comments,
        :satisfaction_rating_comments,
        :satisfaction_rating_scores,
        :satisfaction_ratings
      ]

      assert_empty associations - assoc.map(&:name)

      checked = false
      assoc.each do |azz|
        names = stringy_column_names(azz.klass)
        next unless records = @ticket.send(azz.name)
        Array(records).each do |record|
          # we want to keep the scrub CIA event
          next if record.class == CIA::Event && record.action == "permanently_destroy"
          # we want to keep the soft deletion event
          next if record == @soft_deletion_event
          refute(record.visible, "cia events should not be visible after scrubbing") if record.class == CIA::Event
          record.attributes.slice(*names).each do |key, val|
            next if key == "type" || key.end_with?("_type") || val.nil? || val == ""

            expected = Zendesk::Scrub::SCRUB_TEXT
            if object_class = object_class(record, key)
              expected = if object_class == Hash
                Hash(expected => expected)
              else
                Array(expected)
              end
            end

            assert_equal expected, val, "got #{key}=#{val},for #{record.class.name}, expected #{Zendesk::Scrub::SCRUB_TEXT}"
            checked = true
          end
        end
      end

      assert checked, "Igor was right, nothing was tested indeed"
    end
  end
end
