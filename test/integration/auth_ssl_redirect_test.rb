require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class AuthSSLRedirectTest < ActionDispatch::IntegrationTest
# This test exists to ensure that SSLRequirement runs before Warden's authentication filters. This is so that secure cookies
# set on SSL-enabled accounts are not lost on redirects. Prevents the issue where logging in from a non-SSL URL sends users
# to the login page.

  fixtures :all

  describe "on an SSL-enabled account" do
    before do
      Account.any_instance.stubs(:has_public_forums?).returns(false)
      assert(accounts(:minimum).update_attribute(:is_ssl_enabled, true))
    end

    describe "a request to a protected page on HTTP" do
      before do
        get("http://minimum.zendesk-test.com/users")
        assert_redirected_to "https://minimum.zendesk-test.com/users"
        follow_redirect!
      end

      it "redirect to the failure app on HTTPS" do
        assert_includes headers["Location"], 'https://minimum.zendesk-test.com/access/unauthenticated'
      end

      it "have an https return_to URL" do
        assert_includes headers["Location"], 'return_to=https%3A%2F%2Fminimum.zendesk-test.com%2Fusers'
      end
    end

    describe "a request to a protected page on HTTPS" do
      before do
        get("https://minimum.zendesk-test.com/users")
      end

      it "redirect to the failure app on HTTPS" do
        assert_includes headers["Location"], 'https://minimum.zendesk-test.com/access/unauthenticated'
      end

      it "have an https return_to URL" do
        assert_includes headers["Location"], 'return_to=https%3A%2F%2Fminimum.zendesk-test.com%2Fusers'
      end
    end

    describe "any request to a protected page" do
      it "invoke filters in right order" do
        filter_sequence = sequence("filter_sequence")
        People::UsersController.any_instance.expects(:ensure_proper_protocol).in_sequence(filter_sequence)
        People::UsersController.any_instance.expects(:authenticate_user).in_sequence(filter_sequence)
        visit("https://minimum.zendesk-test.com/users")
      end
    end
  end

  describe "on a SSL-forced account" do
    before do
      Account.any_instance.stubs(has_public_forums?: false, help_center_enabled?: true)
    end

    describe "a request to a protected page on HTTP" do
      before do
        get("http://minimum.zendesk-test.com/users")
        assert_redirected_to "https://minimum.zendesk-test.com/users"
        follow_redirect!
      end

      it "redirect to the failure app on HTTPS" do
        assert_includes headers["Location"], 'https://minimum.zendesk-test.com/access/unauthenticated'
      end

      it "have an https return_to URL" do
        assert_includes headers["Location"], 'return_to=https%3A%2F%2Fminimum.zendesk-test.com%2Fusers'
      end
    end
  end
end
