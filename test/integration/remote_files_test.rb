require_relative "../support/test_helper"

describe 'RemoteFiles' do
  describe "remote_files" do
    before do
      @configuration = RemoteFiles::CONFIGURATIONS[:inbound_email]

      @file = RemoteFiles::File.new(
        'identifier',
        account_id: 1,
        configuration: :inbound_email,
        stored_in: [@configuration.primary_store.identifier]
      )
    end

    it "retrys on failure" do
      assert RemoteFiles::RetryingResqueJob.respond_to?(:on_failure_retry)
    end

    it "is able to retry with unmodified options" do
      options = { 'identifier' => 'my_identifier' }
      RemoteFiles::File.any_instance.stubs(:delete_now!).returns(true)
      RemoteFiles::RetryingResqueJob.work(1, :delete, options)
      assert_equal 'my_identifier', options['identifier']
    end

    it "successfully recreate the file in the resque job on delete" do
      @configuration.primary_store.expects(:delete!).with(@file.identifier)
      @file.delete!
    end

    it "successfully recreate the file in the resque job on store" do
      non_primary_stores = @configuration.stores - [@configuration.primary_store]
      refute non_primary_stores.empty?
      non_primary_stores.each do |store|
        next if store.read_only?
        store.expects(:store!)
      end

      @file.store!
    end
  end
end
