require_relative "../support/test_helper"

ENV['RSPAMD_API_ENDPOINT'] = 'http://127.0.0.1:9001'
ENV['RSPAMD_PASSWORD'] = 'GoodPassword'

describe 'CloudmarkFeedback' do
  fixtures :all

  before do
    @original_perform_deliveries = CloudmarkFeedbackMailer.perform_deliveries
    CloudmarkFeedbackMailer.perform_deliveries = true
    # We don't care about this, stubbing so we can just focus on Cloudmark deliveries
    EventsMailer.stubs(:deliver_rule)
  end

  after do
    CloudmarkFeedbackMailer.perform_deliveries = @original_perform_deliveries
  end

  describe "A suspended ticket misclassified as spam" do
    before do
      @agent  = users(:minimum_agent)
      @author = users(:minimum_end_user)
      @suspended_ticket = create_suspended_ticket
    end

    it "reports feedback to Cloudmark when recovered" do
      assert_difference('ActionMailer::Base.deliveries.size') do
        @suspended_ticket.recover(@agent, @author)
      end
      mail = CloudmarkFeedbackMailer.deliveries.last

      assert_equal ["cloudmark.#{@agent.account_id}.#{@agent.id}@system.zendesk.com"], mail.from
      assert_equal ["zendesk-legit-headers@feedback.cloudmark.com"], mail.to
    end
  end

  describe "A ticket misclassified as ham" do
    before do
      @agent  = users(:minimum_agent)
      @author = users(:minimum_end_user)

      @ticket = @author.account.tickets.new(requester: @author, description: 'hejsa')
      @ticket.will_be_saved_by(@author)
      @ticket.audit.metadata[:system][:raw_email_identifier] = '1/spam.eml'
      @ticket.save!

      create_remote_file("1/spam.json",
        content_type: "application/json",
        content: '{"headers": {"X-CMAE-Score": ["90"], "X-CMAE-Analysis": ["some analysis"]} }')
    end

    it "reports feedback to Cloudmark when marked as spam" do
      assert_difference('ActionMailer::Base.deliveries.size') do
        @ticket.mark_as_spam_by!(@agent)
      end
      mail = CloudmarkFeedbackMailer.deliveries.last

      assert_equal ["cloudmark.#{@agent.account_id}.#{@agent.id}@system.zendesk.com"], mail.from
      assert_equal ["zendesk-spam-headers@feedback.cloudmark.com"], mail.to
    end
  end

  protected

  def create_suspended_ticket
    suspended_ticket = FactoryBot.create(:suspended_ticket,
      account: @author.account,
      author: @author,
      client: "IP address: 255.255.255.255")

    metadata = {
      message_id: "12345@example.com",
      ip_address: "1.2.3.4",
      raw_email_identifier: '1/not_spam.eml'
    }
    suspended_ticket.properties = { metadata: metadata }
    suspended_ticket.cause      = SuspensionType.SPAM
    suspended_ticket.via_id     = ViaType.MAIL

    create_remote_file("1/not_spam.json",
      content_type: "application/json",
      content: '{"headers": {"X-CMAE-Score": ["100"], "X-CMAE-Analysis": ["some analysis"]} }')

    suspended_ticket
  end

  def create_remote_file(identifier, params)
    default_params = {
      configuration: Zendesk::Mail.raw_remote_files.name
    }

    RemoteFiles::File.new(identifier, default_params.merge(params)).store_once!
  end
end
