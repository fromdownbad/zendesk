require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"
require_relative "../support/api_test_helper"
require_relative "../support/mailer_test_helper"
require_relative "../support/signed_auth_helper"

class AuthenticationTest < ActionDispatch::IntegrationTest
  include DomainEventsHelper
  include ApiTestHelper
  include ZendeskDSL
  include MailerTestHelper
  include SignedAuthHelper

  let(:domain_event_publisher) { FakeEscKafkaMessage.new }
  before { User.any_instance.stubs(domain_event_publisher: domain_event_publisher) }

  def assert_unauthorized_redirect(attempted_path)
    assert_redirected_to "/access/unauthenticated?return_to=https%3A%2F%2Fminimum.zendesk-test.com#{CGI.escape attempted_path}"
  end

  fixtures :all

  describe "authentication" do
    before do
      Zendesk::GeoLocation.stubs(:locate).with("127.0.0.1").returns(
        description: "San Francisco, CA, United States",
        latitude: 37.7645,
        longitude: -122.4294
      )
      mock_asset_manifest = Lotus::AssetManifest.new(users(:minimum_agent).account, {})
      Lotus::ManifestManager.any_instance.stubs(:find).returns(mock_asset_manifest)
    end

    it "inserts a <link rel='canonical' ...> tag in head without the params in href" do
      user = users(:minimum_agent)
      visit("#{user.account.url}/access/unauthenticated?return_to=/users/something/something")
      assert_match(/<link href=\"https:\/\/minimum.zendesk-test.com\/access\/unauthenticated\" rel=\"canonical\">/, response.body)
    end

    it "stays on the brand if I go to login" do
      account = accounts(:minimum)
      other_brand = FactoryBot.create(:brand, account_id: account.id)

      visit "#{other_brand.url}/login"

      assert_equal "#{other_brand.url}/access/unauthenticated", current_url
    end

    it "works with the proper password" do
      login(users(:minimum_agent), password: "123456")
      visit "/users"
      assert_response :success
    end

    it "fails on get" do
      user = users(:minimum_agent)
      auth_url = "#{user.account.url(mapped: false, ssl: true)}/access/login"
      get auth_url, params: { user: { email: user.email, password: "123456"} }
      assert_redirected_to "https://minimum.zendesk-test.com/access/unauthenticated"
    end

    it "works with basic authentication" do
      user = users(:minimum_agent)
      auth = Base64.encode64("#{user.email}:123456")
      get "#{user.account.url}/rules/views/123", headers: { 'HTTP_AUTHORIZATION' => "Basic #{auth}" }
      assert_response :not_found
    end

    it "handles throttled password attempts" do
      Account.any_instance.stubs(has_security_email_notifications_for_password_strategy?: true)
      user = users(:minimum_agent)

      threshold = user.security_policy.password_attempt_threshold

      threshold.times do
        Prop.throttle!(:password_attempt, [user.email, '127.0.0.1'])
      end

      assert_emails_sent do
        login(user, password: "123456")
      end

      assert_response :forbidden

      delivery = ActionMailer::Base.deliveries.first
      assert_not_nil delivery
      assert_match /Agent \w+ suspended/, delivery.subject
    end

    describe "via a strategy that should not apply across requests" do
      before do
        user = users(:minimum_agent)
        get "#{user.account.url}/api/v2/users/me.json", headers: api_basic_auth_headers(user.email, '123456')
      end

      it "does not store an authenticated session" do
        assert_nil @response.headers["Set-Cookie"]
      end
    end

    describe "with an account that prefers lotus" do
      before do
        @account = accounts(:minimum)
        @account.settings.prefer_lotus = true
        @account.settings.save!

        @agent = users(:minimum_agent)
      end

      describe "after login" do
        before do
          login(@agent, password: "123456")
        end

        describe "an SSL request to the unhostmapped logout page" do
          before do
            refute @account.is_ssl_enabled?
            assert @account.update_attribute(:host_mapping, "wibble.example.com")

            visit("https://#{@account.host_name(mapped: false)}/access/logout")
            follow_all_redirects!
          end

          it "redirects to the hostmapped login" do
            assert_equal "#{@account.url}/access/unauthenticated", current_url
          end
        end

        describe "a hosted SSL request to the unhostmapped logout page" do
          before do
            skip "this test needs to prove that it works on master"
            Account.any_instance.stubs(is_ssl_enabled?: true, has_public_forums?: false)

            assert @account.certificates.first.update_attributes(state: "active", valid_until: 10.days.from_now.to_date)
            assert @account.update_attribute(:host_mapping, "wibble.example.com")

            # Have to be logged into both sites
            token = @agent.challenge_tokens.lookup_by_ip_address(nil)
            visit("https://#{@account.host_name}/access?challenge=#{token.value}")
            follow_all_redirects!
            assert_response 200, @response.body

            visit("https://#{@account.host_name(mapped: false)}/access/logout")
            follow_all_redirects!
            assert_response 200, @response.body
          end

          it "redirects to the hostmapped login" do
            assert_match /^#{Regexp.escape("https://#{@account.host_name}/access/unauthenticated?flash_digest=")}.+/, current_url
          end
        end
      end

      describe "password login" do
        before do
          post "https://#{@account.host_name}/access/login", params: { user: { email: @agent.email, password: '123456' } }
        end

        it "returns to lotus" do
          assert_redirected_to "/agent"
        end
      end

      describe "password login with a return_to" do
        before do
          post "#{@account.url(ssl: true)}/access/login", params: { user: { email: @agent.email, password: '123456' }, return_to: return_to }
        end

        describe "valid return_to" do
          let(:return_to) { "#{@account.url(ssl: true)}/tickets/1" }

          it "returns to lotus" do
            assert_redirected_to "/agent/tickets/1"
          end
        end

        describe "invalid-ish return_to" do
          let(:return_to) { "nowhereland" }

          it "returns to return_to" do
            assert_redirected_to "#{@account.url}/nowhereland"
          end
        end
      end

      describe "password login with force_unaltered_return_to" do
        before do
          post "https://#{@account.host_name}/access/login", params: { user: { email: @agent.email, password: '123456' }, return_to: "http://#{@account.host_name}/provisioning", force_unaltered_return_to: true }
        end

        it "returns to provisioning" do
          assert_redirected_to "http://#{@account.host_name}/provisioning"
        end
      end

      describe "already logged in" do
        before do
          post "https://#{@account.host_name}/access/login", params: { user: { email: @agent.email, password: '123456'} }
          get("#{@account.url}/access")
        end

        it "returns to lotus" do
          assert_redirected_to "#{@account.url(mapped: false, ssl: true)}/agent"
        end
      end

      describe "then requesting the hostmapped ticket" do
        before do
          assert @account.update_attribute(:host_mapping, "wibble.example.com")
          assert @account.certificates.first.update_attribute(:state, "active")
          assert @account.certificates.first.update_attribute(:valid_until, 10.days.from_now.to_date)

          @account.update_attribute(:is_ssl_enabled, true)
          @account.reload

          @ticket = tickets(:minimum_1)

          logs_in_as @agent, '123456', return_to: "#{@account.url}/ping/host"
          get "#{@account.url}/tickets/#{@ticket.nice_id}"
        end

        it "return_tos lotus ticket" do
          assert_match /challenge=[a-zA-Z0-9]+/, headers["Location"]

          url = "#{@account.url(mapped: false)}/agent/tickets/#{@ticket.nice_id}"
          assert_match /return_to=#{url}/, headers["Location"]

          assert_match /^#{@account.url(mapped: false)}/, headers["Location"]
        end
      end
    end

    it "includes return_to when unauthenticated" do
      account = accounts(:minimum)

      visit "#{account.url}/access/login?return_to=/users"

      assert_equal "https://minimum.zendesk-test.com/access/unauthenticated?return_to=%2Fusers", current_url
    end

    describe "with invalid password" do
      let(:user) { users(:minimum_agent) }

      describe "without arturos for auth v3" do
        before do
          login(user, password: "moooo")
        end

        it "does not work with an invalid password" do
          contain 'password combination is incorrect'

          visit "/users"
          assert_redirected_to "https://minimum.zendesk-test.com/access/unauthenticated?return_to=https%3A%2F%2Fminimum.zendesk-test.com%2Fusers"
        end
      end
    end

    it "does not work without a password" do
      login(users(:minimum_agent), password: "")

      contain 'password combination is incorrect'

      visit "/users"
      assert_redirected_to 'https://minimum.zendesk-test.com/access/unauthenticated?return_to=https%3A%2F%2Fminimum.zendesk-test.com%2Fusers'
    end

    describe "for a user who recently logged in" do
      before do
        Timecop.freeze(Time.at(Time.now.to_i))

        @user = users(:minimum_agent)
        @user.settings.set(location: nil, latitude: nil, longitude: nil)
        assert @user.update_attribute(:last_login, 1.minute.ago)
      end

      it "does not update the last_login" do
        login(@user, password: "123456")
        @user.reload
        assert_equal 1.minute.ago, @user.last_login
      end

      it "does not update the user's location" do
        login(@user, password: "123456")
        @user.reload
        assert_nil @user.settings.location
        assert_nil @user.settings.latitude
        assert_nil @user.settings.longitude
      end

      it "does not emit the last_login_changed event" do
        login(@user, password: "123456")
        assert_equal decode_user_events(domain_event_publisher.events, :last_login_changed).size, 0
      end
    end

    describe "for a user who logged in a long time ago" do
      before do
        Timecop.freeze(Time.at(Time.now.to_i))

        @user = users(:minimum_agent)
        @user.settings.set(location: nil, latitude: nil, longitude: nil)
        assert @user.update_attribute(:last_login, 5.days.ago)
      end

      it "updates the last_login" do
        login(@user, password: "123456")
        @user.reload
        assert_equal Time.now, @user.last_login
      end

      it "emits a last_login_changed event" do
        login(@user, password: "123456")
        assert_equal decode_user_events(domain_event_publisher.events, :last_login_changed).size, 1
      end

      describe "when the time_zone is not utc" do
        before { Time.zone = "Pacific Time (US & Canada)" }

        it "uses utc as the time_zone" do
          login(@user, password: "123456")
          assert @user.last_login.utc?
        end
      end

      it "updates the user's location" do
        login(@user, password: "123456")
        @user.reload
        assert_equal "San Francisco, CA, United States", @user.settings.location
        assert_equal 37.7645, @user.settings.latitude
        assert_equal -122.4294, @user.settings.longitude
      end
    end
  end

  describe "challenge tokens" do
    before do
      @user    = users(:minimum_agent)
      @account = accounts(:minimum)
      @account.challenge_tokens.delete_all
      @token = @user.challenge_tokens.lookup_by_ip_address('127.0.0.1')
      stub_request(:get, %r{/agent/tessaManifest.json\z})
    end

    it "is deleted when used" do
      get "#{@account.url(ssl: true)}/access/return_to?challenge=#{@token.value}"

      assert_redirected_to "#{@account.url(mapped: false, ssl: true)}/agent"
      assert_raise(ActiveRecord::RecordNotFound, 'challenge token not deleted') { @token.reload }
    end

    it "is able to be used" do
      get "#{@account.url}/access/return_to?challenge=#{@token.value}"
      assert_redirected_to "#{@account.url(mapped: false, ssl: true)}/agent"

      get "#{@account.url}/users"
      assert_response :success

      assert_raise(ActiveRecord::RecordNotFound, 'challenge token not deleted') { @token.reload }
    end

    describe "when the user is already logged in" do
      before do
        login(@user, password: "123456")
      end

      it "always be deleted on normal requests" do
        visit "#{@account.url(ssl: true)}/home?challenge=#{@token.value}"

        assert_response :success
        assert_raise(ActiveRecord::RecordNotFound, 'challenge token not deleted') { @token.reload }
      end

      it "does not change csrf_token when used" do
        assert csrf_token = session[:_csrf_token]
        get "#{@account.url(ssl: true)}/access/return_to?challenge=#{@token.value}"
        assert_redirected_to "#{@account.url(mapped: false, ssl: true)}/agent"
        assert_equal csrf_token, request.session[:_csrf_token]
        assert_equal csrf_token, shared_session[:csrf_token]
        assert_raise(ActiveRecord::RecordNotFound, 'challenge token not deleted') { @token.reload }
      end

      describe "with hostmapping" do
        before do
          assert @account.update_attribute(:host_mapping, "wibble.example.com")
        end

        it "does not redirect with an invalid challenge token" do
          assert session[:_csrf_token]
          get "#{@account.url(ssl: true)}/access/return_to?challenge=#{@token.value}&return_to=#{@account.url}"
          assert_redirected_to "#{@account.url(mapped: false, ssl: true)}/agent"
          assert_equal [], @account.challenge_tokens
        end
      end

      describe "with a new user" do
        before do
          @admin = users(:minimum_admin)
          @token = @admin.challenge_tokens.lookup_by_ip_address
          @csrf_token = shared_session[:csrf_token]

          visit "#{@account.url(ssl: true)}/access/return_to?challenge=#{@token.value}"
        end

        it "logins as the new user" do
          assert_equal @admin, warden.user
        end

        it "changes the csrf token" do
          refute_equal @csrf_token, shared_session[:csrf_token]
        end

        it "is deleted when used" do
          assert_raise(ActiveRecord::RecordNotFound, 'challenge token not deleted') { @token.reload }
        end
      end
    end

    describe "with an invalid token" do
      before do
        visit "#{@account.url(ssl: true)}/access/return_to?challenge=123123"
      end

      it "clears the user" do
        assert_nil warden.user
      end

      it "clears the session" do
        assert_equal %w[account flash is_mobile route session_id warden.message], request.session.keys.map(&:to_s).sort
        assert_equal %w[id account locale_id], request.shared_session.keys
      end
    end
  end

  describe "session expiration" do
    it "does not allow access to users with expired sessions on authenticated actions" do
      restricted_url = 'https://minimum.zendesk-test.com/users.html'

      Timecop.travel(15.days.ago) do
        login(users(:minimum_agent))
        visit(restricted_url)
        assert_response :success
      end

      visit(restricted_url)
      assert_equal "https://minimum.zendesk-test.com/access/unauthenticated?return_to=#{CGI.escape(restricted_url)}", current_url

      Timecop.travel(2.days.ago) do
        get(restricted_url)
        assert_unauthorized_redirect "/users.html"
      end

      login(users(:minimum_agent))
      get(restricted_url)
      assert_response :success
    end

    it "does not cache home as though it were logged in" do
      login(users(:minimum_agent))
      home_url = 'https://minimum.zendesk-test.com/home'
      Timecop.travel(15.days.ago) do
        visit(home_url)
        assert_response :success
      end
      visit(home_url)
      refute(@response.body =~ /minimum_agent@aghassipour.com/)
    end

    it "does not clear remember_me sessions" do
      login(users(:minimum_agent), remember_me: true)
      restricted_url = 'https://minimum.zendesk-test.com/users.html'

      Timecop.travel(15.days.ago) do
        visit(restricted_url)
        assert_response :success
      end

      visit(restricted_url)
      assert_response :success
    end
  end

  describe "as a system user" do
    let(:account) { accounts(:minimum) }
    let(:user) { account.owner }
    let(:url) { account.url(ssl: true) + "/api/v2/users/me.json" }

    before do
      user.last_login = 1.month.ago
      user.save!

      request_system_user_oauth(:chat, url, headers: {'HTTP_X_ZENDESK_INTERNAL_USER' => user.id})
    end

    it "authenticates the user without updating last login" do
      assert_response :ok
      assert_equal user.id, JSON.parse(response.body).fetch('user').fetch('id')
      assert_equal user.last_login.to_i, user.reload.last_login.to_i
    end

    it "does not emit the last_login_changed event" do
      assert_equal decode_user_events(domain_event_publisher.events, :last_login_changed).size, 0
    end
  end

  describe "using a master token" do
    let(:account) { accounts(:minimum) }
    let(:admin_recipients) do
      [
        "minimum_admin@aghassipour.com",
        "minimum_admin_not_owner@aghassipour.com"
      ]
    end
    let(:user) { account.owner }
    let(:token) { user.master_tokens.create }
    let(:jwt_claim) do
      JWT.encode(
        {
          token: token.value,
          permission: {read_only: false}
        },
        Zendesk::Configuration["monitor"]["key"]
      )
    end
    let(:url) { account.url(ssl: true) + "/access/master/jwt?jwt=#{jwt_claim}" }

    before do
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries         = []
    end

    it "authenticates the user without updating last login" do
      user.last_login = 1.month.ago
      user.save!

      get(url)
      assert_redirected_to "https://minimum.zendesk-test.com/agent"

      assert_equal user.last_login.to_i, user.reload.last_login.to_i
      assert_nil @controller.shared_session[:auth_assuming_monitor_user_id]
    end

    it "does not emit the last_login_changed event" do
      user.last_login = 1.month.ago
      user.save!

      get(url)
      assert_equal decode_user_events(domain_event_publisher.events, :last_login_changed).size, 0
    end

    it "sets the assuming_monitor_user_id when using a token from monitor" do
      token.update_attribute(:assuming_monitor_user_id, 1234)
      get(url)
      assert_redirected_to "https://minimum.zendesk-test.com/agent"
      assert_equal 1234, @controller.shared_session[:auth_assuming_monitor_user_id]
    end

    describe "when bypassing the account assumption restriction" do
      let(:audit_event) { CIA::Event.last }
      before do
        get(url)
        @email = ActionMailer::Base.deliveries.last
      end

      it "delivers a notification when bypassing" do
        refute_empty ActionMailer::Base.deliveries
      end

      it "creates a CIA audit event with audit action" do
        assert_equal 'audit', audit_event.action
      end

      it "creates a CIA audit event with the proper message" do
        assert_equal 'txt.admin.views.reports.tabs.audits.assumption_bypass', audit_event.message
      end

      it "creates a CIA audit event with the proper user" do
        assert_equal User.system.id, audit_event.actor_id
      end

      it "sends the email to admins on the account" do
        assert_equal admin_recipients, @email.bcc
      end

      it "sends an email with the correct subject" do
        @email.subject.must_include I18n.t('txt.email.account_assumption.assumption_bypass.subject')
      end

      it "sends an email with bypass content" do
        @email.text_part.body.decoded.must_include I18n.t('txt.email.account_assumption.assumption_bypass.content')
      end
    end

    describe "trial account assumption" do
      let(:account) { accounts(:trial) }

      before do
        get(url)
        @email = ActionMailer::Base.deliveries.last
      end

      it "does not deliver an email" do
        assert_empty ActionMailer::Base.deliveries
      end
    end
  end

  describe "#unauthenticated" do
    let(:account)        { accounts(:trial) }
    let(:secure_url)     { account.url(protocol: "https") }
    let(:normal_url)     { account.url }
    let(:mapped_host)    { "support.example.com" }
    let(:zendesk_domain) { "zendesk-test.com" }

    before { Account.any_instance.stubs(:use_status_hold?).returns(false) }

    describe "on an account without SSL" do
      before do
        refute account.is_ssl_enabled?
        refute account.certificates.active.any?
      end

      describe "and without host mapping" do
        it "does not redirect" do
          assert_includes normal_url, zendesk_domain
          assert_includes secure_url, zendesk_domain
          assert_nil account.host_mapping
          visit("#{secure_url}/access/unauthenticated")

          assert_response :success
        end
      end

      describe "and with host mapping" do
        before do
          account.update_attribute(:host_mapping, mapped_host)
          assert_includes normal_url, mapped_host
          assert_includes secure_url, zendesk_domain
        end

        it "redirects to the mapped host with the proper protocol" do
          visit("#{secure_url}/access/unauthenticated")
          assert_redirected_to "http://#{mapped_host}/access/unauthenticated"
        end

        it "preserves flash messages" do
          login(users(:trial_admin), password: "moooo")

          contain 'password combination is incorrect'
        end

        it "preserves params" do
          params = {
            return_to: "http://support.example.com/home",
            user: {"email" => "foo"}
          }

          visit("#{secure_url}/access/unauthenticated?#{params.to_query}")
          assert_includes(response.redirected_to, "http://#{mapped_host}/access/unauthenticated")
          assert_includes(response.redirected_to, "return_to=http%3A%2F%2Fsupport.example.com%2Fhome&user%5Bemail%5D=foo")
        end
      end
    end

    describe "on an account with SSL enabled" do
      before { account.update_attribute(:is_ssl_enabled, true) }

      describe "and with host mapping" do
        before do
          account.update_attribute(:host_mapping, mapped_host)
          assert_includes normal_url, zendesk_domain
          assert_includes secure_url, zendesk_domain
        end

        it "does not redirect" do
          visit("#{secure_url}/access/unauthenticated")
          assert_response :success
        end
      end

      describe "and without host mapping" do
        it "does not redirect" do
          assert_includes normal_url, zendesk_domain
          assert_includes secure_url, zendesk_domain
          assert_nil account.host_mapping
          visit("#{secure_url}/access/unauthenticated")

          assert_response :success
        end
      end
    end

    describe "on an account with hosted SSL" do
      let(:account) { accounts(:minimum) }

      before do
        assert account.certificates.first.update_attribute(:state, "active")
        assert account.certificates.first.update_attribute(:valid_until, 10.days.from_now.to_date)
        account.update_attribute(:host_mapping, mapped_host)
        account.update_attribute(:is_ssl_enabled, true)
        assert_includes normal_url, mapped_host
        assert_includes secure_url, mapped_host
      end

      it "does not redirect" do
        visit("#{account.url(protocol: "https")}/access/unauthenticated")
        assert_response :success
      end
    end
  end

  describe "on an account with a host mapping" do
    before do
      Account.any_instance.stubs(:has_public_forums?).returns(false)
      assert(accounts(:minimum).update_attribute(:host_mapping, 'wibble.example.com'))
    end

    describe "an SSL request to the unhostmapped logout page" do
      before do
        account = accounts(:minimum)
        visit("https://#{account.host_name(mapped: false)}/access/logout")
      end

      it "does not redirect to the hostmapped login" do
        assert_no_match %r{/access/return_to}, headers['Location']
      end
    end

    describe "a request to the logout page" do
      before do
        visit("http://wibble.example.com/access/logout")
        follow_all_redirects!

        assert_nil(session[:user])
      end

      it "shows the login page" do
        assert_template 'auth/sso_v2_index'
      end
    end

    describe "a request to a protected page on HTTP" do
      before do
        get("http://wibble.example.com/users")
      end

      it "redirects to the failure app on HTTP" do
        assert_match(/^http:\/\/wibble.example.com\/access\/unauthenticated/, headers["Location"])
      end
    end

    describe "on an account with SSL" do
      let(:account) { accounts(:minimum) }

      before do
        account.certificates.create! do |c|
          c.state = 'active'
          c.valid_until = 10.days.from_now.to_date
        end
        assert account.certificates.active.any?
        account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
        Zendesk::Features::SubscriptionFeatureService.new(account).execute!
        account.update_attribute(:is_ssl_enabled, true)
      end

      describe "a request to a protected page on HTTPS" do
        before do
          get("https://wibble.example.com/users")
        end

        it "redirects to the failure app on HTTPS" do
          assert_match(/^https:\/\/wibble.example.com\/access\/unauthenticated/, headers["Location"])
        end
      end

      describe "universal_sso_redirection" do
        let(:auth) do
          post "https://#{account.host_mapping}/access/login", params: { user: { email: users(:minimum_end_user).email, password: "123456"}, return_to: return_to }
          follow_all_redirects!
          assert_equal return_to, current_url.sub(/\?flash_digest=\w+/, "")
        end
        let(:return_to) { "https://#{account.host_mapping}/ping/host" }
        let(:login_check) { "#{account.url(mapped: false, ssl: true)}/password" }

        it "forces login onto zendesk.com" do
          auth
          visit! login_check
        end
      end
    end

    describe "a request to a protected page" do
      before do
        visit("http://wibble.example.com/users")

        visit(headers["Location"]) while redirect?

        assert_nil(session[:user])
      end

      it "shows the login page" do
        assert_template 'auth/sso_v2_index'
      end

      it "does not set the :auth_via value in the session" do
        assert_nil @controller.send(:auth_via)
      end

      it "calls the login form" do
        contain("https://minimum.zendesk-test.com/auth/v2/host.js")
      end

      describe "and logging in" do
        before do
          visit("#{accounts(:minimum).url}/requests")
          assert(login(users(:minimum_end_user), remember_me: true))
        end

        it "is logged in on the mapped domain" do
          assert_equal(users(:minimum_end_user), warden.user)
        end

        it "has the :auth_via value set in the session" do
          assert_equal "password", @controller.send(:auth_via)
          assert @controller.send(:auth_via?, :password)
          assert @controller.send(:not_auth_via?, :hello)
        end
      end

      describe "and logging in with bad combo" do
        before do
          Account.any_instance.stubs(translation_locale: translation_locales(:brazilian_portuguese))
          login(users(:minimum_end_user), password: 'BAD', remember_me: true)
        end

        it "shows the login page" do
          assert_template 'zendesk/auth/v2/logins/signin'
        end

        it "populates the login form with the provided email address" do
          assert_select "input[name='user[email]'][value='#{users(:minimum_end_user).email}']"
        end

        it "does not populate the password field" do
          assert_select "input[name='user[password]']", value: nil
        end

        it "renders the error message in the correct translation" do
          @response.body.must_include "THIS IS A BRAZILIAN PORTUGUESE LOGIN INCORRECT CREDENTIALS"
        end
      end
    end

    describe "logging out maliciously" do
      before do
        assert(login(users(:minimum_end_user), remember_me: true))
        get "https://minimum.zendesk-test.com/access/logout", params: { return_to: 'http://evil.com' }
        follow_all_redirects!
      end

      it "does not be redirected to evil.com" do
        current_url.must_include "#{accounts(:minimum).url}/access/unauthenticated"
        assert_response :ok
      end
    end

    describe "logging out" do
      before do
        assert(login(users(:minimum_end_user), remember_me: true))
        accounts(:minimum).certificates.first.update_attributes(state: "active", valid_until: 10.days.from_now.to_date)
        visit("https://minimum.zendesk-test.com/access/logout")
        follow_all_redirects!
      end

      it "is logged out on the mapped domain" do
        visit("http://wibble.example.com/users")
        follow_all_redirects!
        assert_template 'auth/sso_v2_index'
      end

      it "is logged out on the unmapped domain" do
        visit("http://minimum.zendesk-test.com/users")
        follow_all_redirects!
        assert_template 'auth/sso_v2_index'
      end
    end

    describe "getting deleted fool" do
      before do
        @user = users(:minimum_end_user)
        assert(login(@user, remember_me: false))
      end

      it "prevents user from accessing the site" do
        visit("http://minimum.zendesk-test.com/users/#{@user.id}")
        assert_includes response.body, @user.name
        assert_response :success

        @user.update_attribute(:is_active, false)
        Rails.cache.clear

        get("http://#{@user.account.host_mapping}/users/#{@user.id}")

        # this was redirecting to hostmapped domain, which is good, but test architecture screwed it up, next test is enough (prevented user)
        # assert_unauthorized_redirect "/users/#{@user.id}"
        assert_redirected_to "/access/unauthenticated?return_to=http%3A%2F%2F#{@user.account.host_mapping}%2Fusers%2F#{@user.id}"
      end
    end

    describe "assuming another user" do
      let(:japanese)   { translation_locales(:japanese) }
      let(:portuguese) { translation_locales(:brazilian_portuguese) }

      before do
        @account = accounts(:minimum)
        @account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
        @account.reload
        @account.expects(:available_languages).returns([ENGLISH_BY_ZENDESK, portuguese])
        @account.save!

        @assumed_user = users(:minimum_end_user)
        @assumed_user.account = @account
        @assumed_user.locale_id = portuguese.id
        @assumed_user.time_zone = 'Brasilia'
        @assumed_user.save!

        @agent_assuming = users(:minimum_admin)
        @agent_assuming.translation_locale = japanese
        @agent_assuming.save!

        assert_equal portuguese, @assumed_user.translation_locale

        assert login_to_host_mapped(@agent_assuming, 'http://wibble.example.com'), "Login assuming agent must succeed"

        assert_not_equal ActiveSupport::TimeZone['Brasilia'], Time.zone

        visit "http://wibble.example.com/users/#{@assumed_user.id}/assume", :post
        assert_response :success

        assert_equal @assumed_user, warden.user
      end

      it "uses the assumed users locale" do
        assert_equal portuguese, I18n.previous_request_locale
      end

      it "uses the assumed users time zone" do
        assert_equal ActiveSupport::TimeZone['Brasilia'], Time.previous_request_zone
      end
    end
  end

  describe "a GET to index as an agent" do
    before do
      stub_request(:get, %r{/agent/tessaManifest.json\z})
      @account = accounts(:minimum)
      @agent   = users(:minimum_agent)
    end

    it "renders the login page" do
      visit("https://#{@account.host_name}/access/login")
      assert_match /\/access\/unauthenticated$/, current_url
    end

    it "renders remote login page if remote auth is enabled" do
      auth = @account.remote_authentications.first
      auth.auth_mode = RemoteAuthentication::JWT
      auth.is_active = true
      auth.remote_login_url = "https://sso.cpconsulting.dk/zendesk_remote_auth.asp"
      auth.save!

      role_settings = @account.role_settings
      role_settings.agent_zendesk_login    = false
      role_settings.end_user_zendesk_login = false
      role_settings.agent_remote_login     = true
      role_settings.end_user_remote_login  = true
      role_settings.save!

      visit("https://#{@account.host_name}/access/login")
      assert(redirect?)
      assert_match /^#{auth.remote_login_url}/, headers["Location"]
    end

    it "redirects to agent page if you are authenticated" do
      login(@agent, password: "123456")
      assert_equal @agent, warden.user
      visit("https://#{@account.host_name}/access/login")
      assert_match /\/agent$/, current_url
    end
  end

  describe "a GET to /" do
    before do
      @account = accounts(:minimum)
      @agent   = users(:minimum_agent)
      visit("https://#{@account.host_name}?user[email]=#{@agent.email}&user[password]=123456")
    end

    it "renders the access/unauthenticated page" do
      assert_match /\/access\/unauthenticated$/, current_url
    end
  end

  describe "with a return_to with UTF8 characters" do
    before do
      @account = accounts(:minimum)
      @url = URI.encode("https://#{@account.host_name}/access/unauthenticated?return_to=/☃")
    end

    it "responds with 200 OK" do
      get @url
      assert_response 200
    end
  end
end
