require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class ActionViewStatsTest < ActionDispatch::IntegrationTest
  fixtures :accounts

  describe "ActionViewStats" do
    fixtures :all

    before do
      ActionViewStats.subscribe
    end

    after do
      ActionViewStats.unsubscribe
    end

    it "instruments template and partial loads" do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.AccessController.unauthenticated', anything).at_least(1)

      Zendesk::StatsD::Client.any_instance.expects(:increment).with('as_notify.action_view',  tags: ['event_name:render_template.action_view', 'file:auth/sso_v2_index.html.erb', 'top_level_folder:auth', 'prototype_rails:false']).once
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('as_notify.action_view',  tags: ['event_name:render_partial.action_view', 'file:shared/_hc_flash.erb', 'top_level_folder:shared', 'prototype_rails:false']).once
      get "https://minimum.zendesk-test.com/access/unauthenticated"
    end
  end
end
