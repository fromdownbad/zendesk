require File.expand_path('../support/test_helper', __dir__)
require 'zendesk/testing/factories/global_uid'
require "zendesk/o_auth/testing/client_factory"

describe "OAuth password grant Integration" do
  fixtures :accounts, :role_settings, :users

  before do
    @user = users(:minimum_agent)
    @client = FactoryBot.create(:client, account: (@account = accounts(:minimum)), user: @user)

    Zendesk::OAuth.configure do |config|
      config.allowed_strategies = config.valid_strategies
      config.default_expiry = 1.day
    end

    @host = "https://#{@account.host_name}"

    host! @host
    https!
  end

  describe "with a valid username and password" do
    let(:json) { JSON.parse(response.body) }
    let(:params) do
      {
        "grant_type" => "password",
        "client_id" => @client.identifier,
        "client_secret" => @client.secret(:full),
        "scope" => "read",
        "username" => @user.email,
        "password" => "123456"
      }
    end

    it "issues an oauth token" do
      post "#{@host}/oauth/tokens", params: params

      assert_response :ok

      refute_nil json["access_token"]
      refute_nil json["refresh_token"]
      assert_equal 'bearer', json["token_type"]
      assert_equal 'read', json["scope"]
    end

    it "blocks the flow when 2FA is confirmed" do
      enable_2fa!(@user)
      post "#{@host}/oauth/tokens", params: params

      assert_response :bad_request
      assert_equal 'invalid_grant', json["error"]
      assert_nil json["access_token"]
    end
  end

  def enable_2fa!(user)
    user.account.settings.save!

    Zendesk::Auth::Otp::MessageJob.stubs(:work)

    assert Zendesk::Auth::Otp::Phone.new(user, phone: '123-123-1234').setup!
    assert Zendesk::Auth::Otp::Phone.new(user, code: user.one_time_password.time_based_token).confirm!
  end
end
