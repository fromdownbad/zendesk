require "base64"
require_relative "../support/test_helper"
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'

class RemoteAuthenticationSAMLIntegrationTest < ActionDispatch::IntegrationTest
  include AuthenticationIntegrationTestHelper

  def return_to
    location_params['RelayState']
  end

  def saml_response_url_for_file(file_name)
    saml_response = CGI.escape(Base64.encode64(read_test_file(file_name)))
    "https://minimum.zendesk-test.com/access/saml?SAMLResponse=#{saml_response}"
  end

  def get_custom_fields(user)
    user.custom_field_values.as_json(account: user.account)
  end

  describe "RemoteAuthentication::SAML" do
    fixtures :remote_authentications

    before do
      stub_request(:get, "https://minimum.zendesk-test.com/agent/tessaManifest.json")
      stub_request(:get, "http://minimum.zendesk-test.com/agent/tessaManifest.json")

      Account.any_instance.stubs(:has_saml?).returns(true)

      # the saml response in our fixtures is restricted to this audience
      Account.any_instance.stubs(authentication_host: 'example.org')

      @account = accounts(:minimum)
      @account.domain_blacklist = ''
      @account.save!

      @auth = remote_authentications(:minimum_remote_authentication)

      @auth.fingerprint      = "44:D2:9D:98:49:66:27:30:3A:67:A2:5D:97:62:31:65:57:9F:57:D1"
      @auth.is_active        = true
      @auth.remote_login_url = "https://idp.example.org/saml"
      @auth.auth_mode        = RemoteAuthentication::SAML
      @auth.save!

      role_settings = @account.role_settings
      role_settings.agent_zendesk_login    = false
      role_settings.end_user_zendesk_login = false
      role_settings.agent_remote_login     = true
      role_settings.end_user_remote_login  = true
      role_settings.save!
    end

    describe "remote auth redirects" do
      describe "set to handle all IPs" do
        before { assert(@auth.handles_ip?("handles anything..")) }
        should_redirect_to_remote_auth("https://minimum.zendesk-test.com/tickets")

        describe ", but disabled" do
          before { @auth.update_attribute(:is_active, false) }
          should_not_redirect_to_remote_auth("http://minimum.zendesk-test.com/tickets")
        end
      end
    end

    describe "with the mobile client" do
      let(:client) { FactoryBot.create(:global_client, account: @account, redirect_uri: 'zendesk://authenticate') }
      let(:headers) { {'HTTP_USER_AGENT' => 'Zendesk for iPhone', 'HTTP_CLIENT_IDENTIFIER' => client.identifier} }
      let(:saml_response) { CGI.escape(Base64.encode64(read_test_file("sample_saml_response.xml"))) }

      describe "with a valid SAML" do
        before do
          Timecop.freeze('2012-08-09 22:25:00 UTC')
          url = "https://minimum.zendesk-test.com/access/saml?SAMLResponse=#{saml_response}"

          post url, headers: headers
          uri = URI.parse(response.location)
          @redirect_params = Rack::Utils.parse_query(uri.query)
        end

        it "redirects back to the native app" do
          assert_response :redirect
          assert response.location.starts_with?('zendesk://authenticate')
        end

        it "includes the username" do
          assert_equal "someone@example.org", @redirect_params['username']
        end

        it "includes a valid oauth token" do
          oauth_token = Zendesk::OAuth::Token.where(token: @redirect_params['access_token']).first
          assert_equal "someone@example.org", oauth_token.user.email
        end
      end

      describe "with an invalid SAML response" do
        before do
          url = "https://minimum.zendesk-test.com/access/saml?SAMLResponse=broken"
          post url, headers: headers
        end

        it 'should redirect back to the native app error handler' do
          assert_redirected_to "zendesk://authenticate/errors"
        end
      end
    end

    describe "on logout" do
      let(:logout_request_options) { { issuer: "https://sp.example.com/saml2", name_id: "test@test.com" } }
      let(:saml_request) { Samlr::Tools.encode(Samlr::LogoutRequest.new(nil, logout_request_options).body) }
      let(:url) { "https://minimum.zendesk-test.com/access/saml?SAMLResponse=" << CGI.escape(Base64.encode64(read_test_file("sample_saml_response.xml"))) }
      let(:saml_response) { Samlr::LogoutResponse.new(get_query_params_from(response.location)["SAMLResponse"]) }

      before do
        Timecop.freeze('2012-08-09 22:25:00 UTC')
        @auth.update_attributes!(remote_logout_url: "https://example-remote.com")
        visit(url, :post)
        assert logged_in?
        get("/access/logout?SAMLRequest=" << saml_request)
      end

      describe "with valid SAML LogoutRequest" do
        it 'responds with SAMLResponse' do
          assert_includes response.location, "SAMLResponse"
        end

        it 'responds with correct Destination' do
          assert_equal @auth.remote_logout_url, saml_response.get_attribute_or_element("//samlp:LogoutResponse", "Destination")
        end

        it 'responds with correct InResponseTo' do
          saml_request_data = Samlr::LogoutRequest.new(CGI.unescape(saml_request))
          assert_equal saml_request_data.id, saml_response.get_attribute_or_element("//samlp:LogoutResponse", "InResponseTo")
        end
      end

      describe "with invalid SAML LogoutRequest" do
        let(:saml_request) { "hello" }
        it 'responds with SAMLRequest' do
          assert_includes response.location, "SAMLRequest"
        end
      end
    end

    describe "when passing no response parameters" do
      before { visit("http://minimum.zendesk-test.com/tickets") }

      it "initiates a SAMLRequest redirect to the IdP" do
        assert(redirect?)
        assert_match(/^#{@auth.remote_login_url}/, headers["Location"])

        query_params = get_query_params_from(headers["Location"])
        assert_not_nil query_params['SAMLRequest']
      end
    end

    describe "when passing a SAMLResponse parameter" do
      before { @url = "https://minimum.zendesk-test.com/access/saml?SAMLResponse=" }

      describe "that is invalid" do
        before do
          @url << "broken"
          visit(@url, :post)
        end

        should_give_an_error("SAML authentication failure", selector: '.notification-error')
      end

      describe "that is valid" do
        before do
          Timecop.freeze('2012-08-09 22:25:00 UTC')
          @url << CGI.escape(Base64.encode64(read_test_file("sample_saml_response.xml")))
        end

        describe "with a SHA1 fingerprint" do
          it "authenticates" do
            visit(@url, :post)
            assert logged_in?
          end
        end

        describe "with a SHA256 fingerprint" do
          before do
            @auth.update_attributes!(fingerprint: "54:AB:F0:74:D7:66:79:04:15:EC:42:B3:A0:D7:5F:03:E5:85:54:D2:B4:48:92:37:23:8D:CC:42:9B:A1:12:DF")
            @auth.save!
          end

          it "authenticates" do
            visit(@url, :post)
            assert logged_in?
          end
        end

        describe "with an invalid fingerprint" do
          before do
            @auth.update_attributes!(fingerprint: "00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00:00")
            @auth.save!
          end

          it "does not authenticate" do
            visit(@url, :post)
            refute logged_in?
          end
        end

        describe "saml audience validation" do
          it "authenticates with a valid audience" do
            Account.any_instance.stubs(authentication_host: 'example.org')
            visit(@url, :post)
            assert logged_in?
          end

          it "authenticates with a mixed case audience" do
            Account.any_instance.stubs(authentication_host: 'exAMPle.org')
            visit(@url, :post)
            assert logged_in?
          end

          it "rejects an invalid audience" do
            Account.any_instance.stubs(authentication_host: 'attacker.com')
            visit(@url, :post)
            refute logged_in?
          end

          describe "with a response with multiple audience nodes" do
            # This was generated from the samlr gem:
            #
            # now = Time.parse('2012-08-09 22:25:00 UTC')
            # saml_response_document(:certificate => @certificate, audience: [['example.org', 'example.com'], ['foo.com']], issue_instant: Samlr::Tools::Timestamp.stamp(now), not_on_or_after: Samlr::Tools::Timestamp.stamp(now + 60), not_before: Samlr::Tools::Timestamp.stamp(now - 60))
            #
            # This generated a response with these AudienceRestrictions:
            # <AudienceRestriction>
            #   <Audience>example.org</Audience>
            #   <Audience>example.com</Audience>
            # </AudienceRestriction>
            # <AudienceRestriction>
            #   <Audience>foo.com</Audience>
            # </AudienceRestriction>
            #
            let(:saml_response) { CGI.escape(Base64.encode64(read_test_file("sample_saml_response_with_multiple_audiences.xml"))) }

            before do
              @url = "https://minimum.zendesk-test.com/access/saml?SAMLResponse=#{saml_response}"
            end

            it "accepts the first audience value" do
              Account.any_instance.stubs(authentication_host: 'example.org')
              visit(@url, :post)
              assert logged_in?
            end

            it "accepts the second audience value" do
              Account.any_instance.stubs(authentication_host: 'example.com')
              visit(@url, :post)
              assert logged_in?
            end

            it "rejects the second AudienceRestriction node" do
              Account.any_instance.stubs(authentication_host: 'foo.com')
              visit(@url, :post)
              refute logged_in?
            end
          end
        end

        describe "with a response with custom fields" do
          # SAML response generation gist:
          # https://gist.github.com/mathurn/80383c4fc89f037396c4efff8b849bec
          let(:new_user) { @account.find_user_by_email("someone@example.org") }
          let(:user_fields) { {"text1" => "sample", "integer1" => 123, "system::integer2" => 456} }

          before do
            visit(saml_response_url_for_file("saml_response_with_custom_fields.xml"), :post)
          end

          it "logs the user in" do
            assert logged_in?
          end

          it "sets custom fields" do
            user_fields.each do |k, v|
              assert_equal(v, get_custom_fields(new_user)[k])
            end
          end

          describe "changes a custom field value" do
            before do
              custom_field_values_for("integer1", new_user)[0].update_attributes!(value: 789)
              new_user.reload
            end

            it "overwrites an existing custom field" do
              assert_equal 789, get_custom_fields(new_user)["integer1"]

              visit(saml_response_url_for_file("saml_response_with_custom_fields.xml"), :post)
              new_user.reload

              assert_equal 123, get_custom_fields(new_user)["integer1"]
            end
          end

          describe "generates a new response with no custom fields" do
            # SAML response generation gist:
            # https://gist.github.com/mathurn/1e29f753c875ca589f7fca79485a039c
            before do
              refute_empty new_user.custom_field_values
              visit(saml_response_url_for_file("saml_response_with_nil_custom_fields.xml"), :post)
              new_user.reload
            end

            it "deletes custom fields that are empty strings in the SAML response" do
              assert_nil get_custom_fields(new_user)["text1"]
            end

            it "deletes custom fields that are nil in the SAML response" do
              assert_nil get_custom_fields(new_user)["integer1"]
            end

            it "doesn't delete custom fields not in the SAML response" do
              assert_equal 456, get_custom_fields(new_user)["system::integer2"]
            end
          end
        end

        describe "with a RelayState" do
          before do
            @user = users(:minimum_agent)
            @user.email = "someone@example.org"
            @user.save!
            @url << "&RelayState=http://minimum.zendesk-test.com/tickets/123"
            visit(@url, :post)
          end
          it "redirects to original destination" do
            assert_equal "https://minimum.zendesk-test.com/agent/tickets/123", current_url
          end
        end

        describe "with a RelayState and agent already logged in" do
          before do
            @user = users(:minimum_agent)
            login(@user, login_path: "/auth/v2/login/normal")
            @user.email = "someone@example.org"
            @user.save!
            @url << "&RelayState=http://minimum.zendesk-test.com/tickets/123"
            visit(@url, :post)
          end
          it "redirects to original destination" do
            assert_equal "https://minimum.zendesk-test.com/agent/tickets/123", current_url
          end
        end

        describe "with sso redirection and host_mapping" do
          before do
            @url.sub!(/minimum\.zendesk-test\.com/, "wibble.example.com")

            user = users(:minimum_agent)
            user.email = "someone@example.org"
            user.save!

            assert @account.update_attribute(:host_mapping, "wibble.example.com")
          end

          describe "hitting the hostmapped /access/saml" do
            let(:url) { @url }

            before do
              visit(url, :post)
            end

            it "redirects to unmapped domain" do
              assert_match /zendesk-test.*/, headers["Location"]
            end

            it "redirects with a challenge" do
              query_params = get_query_params_from(headers["Location"])
              assert_match /[a-z0-9]+/, query_params['challenge']
            end

            it "passes along return_to for agent" do
              query_params = get_query_params_from(headers["Location"])
              assert_equal "https://minimum.zendesk-test.com/agent", query_params['return_to']
            end

            describe "with a return_to param" do
              let(:url) { @url + '&return_to=http://wibble.example.com/home' }

              it "pass along the return_to" do
                query_params = get_query_params_from(headers["Location"])
                assert_equal @account.url + '/home', query_params['return_to']
              end
            end

            describe "with a RelayState param" do
              let(:url) { @url + '&RelayState=http://wibble.example.com/home' }

              it "pass the RelayState as the return_to" do
                query_params = get_query_params_from(headers["Location"])
                assert_equal @account.url + '/home', query_params['return_to']
              end
            end

            describe "with a session return_to" do
              let(:url) do
                visit('http://wibble.example.com/tickets/1')
                @url
              end

              it "pass along the return_to" do
                query_params = get_query_params_from(headers["Location"])
                assert_equal @account.url(mapped: false, ssl: true) + '/agent/tickets/1', query_params['return_to']
              end
            end

            describe "after following the redirect" do
              before { visit(headers["Location"]) }

              it "is logged in" do
                assert logged_in?
              end
            end
          end
        end

        it "is bad request with invalid url" do
          Account.any_instance.stubs(is_ssl_enabled?: true)
          visit(@url.sub(/https/, 'http'), :post)

          assert_response :not_found
        end

        describe "authentication" do
          before do
            visit(@url, :post)
          end

          should_redirect_to_hc
        end
      end
    end
  end
end
