require_relative "../support/test_helper"
require_relative "../support/password_test_helper"

class PasswordChangeIntegrationTest < ActionDispatch::IntegrationTest
  include PasswordTestHelper

  fixtures :all

  describe "Password change for logged in user. " do
    describe "A logged in user" do
      before do
        @user = users(:minimum_agent)
        login(@user)
      end

      it "access a protected pages (fx: /users)" do
        visit "/users"
        assert_response :success
      end

      describe "when someone else changes their password" do
        before do
          change_password!(@user, 'a-new-password')
          visit "/users"
        end

        it "does not be able to access protected pages (fx: /users)" do
          assert_response :redirect
        end
      end

      describe "when she is deleted" do
        before do
          @user.current_user = users(:minimum_agent) # Needed to allow user deletion (see deletion.rb)
          @user.delete!
        end

        it "does not be able to access protected pages (fx: /users)" do
          visit "/users"
          assert_redirected_to "#{@user.account.url}/access/unauthenticated?return_to=https%3A%2F%2Fminimum.zendesk-test.com%2Fusers"
        end
      end

      describe "when changing their password (via PUT /password/update)" do
        before do
          visit "https://minimum.zendesk-test.com/password/update",
            :put,
            current_password: '123456', new_password: 'my-new-password'
          assert_response :success
        end

        it "access a protected pages (fx: /users)" do
          visit "/users"
          assert_response :success
        end

        it "has a current_current user logged in" do
          assert current_user.present?
        end

        it "is able to login with new password" do
          # First we logout then we go to the login page and test new password
          logout
          visit('/access/unauthenticated')
          assert login(@user, password: "my-new-password")
        end
      end
    end

    describe "A logged in admin with large plan" do
      before do
        # First of all we login once with the user we are going to assume
        # in order to make him move to a Bcrypt password schema
        @assumed_user = users(:minimum_agent)
        login(@assumed_user)
        @assumed_user.reload
        logout

        # Then we are ready to login as admin
        accounts(:minimum).subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
        @original_user = users(:minimum_admin)
        visit('/access/unauthenticated')
        login(@original_user)

        # Reload user because we could have migrated the password from SHA1 to Bcrypt after login
        @original_user.reload
      end

      it "access a protected pages (fx: /users)" do
        visit "/users"
        assert_response :success
      end

      describe "when assuming an other user" do
        before do
          visit "/users/#{@assumed_user.id}/assume", :post
        end

        it "assumes user" do
          assert_equal(@assumed_user, warden.user)
        end

        it "access a protected pages (fx: /users)" do
          visit "/users"
          assert_response :success
        end

        describe "when someone changes their password (assumer password change)" do
          before do
            change_password!(@original_user, 'a-new-password')
          end

          it "does not access a protected pages (fx: /users)" do
            visit "/users"
            assert_response :redirect
          end
        end

        describe "when someone changes the assumed user's password" do
          before do
            change_password!(@assumed_user, 'a-new-password')
          end

          it "access a protected pages (fx: /users)" do
            visit "/users"
            assert_response :success
          end
        end

        describe "when someone delete the assumed user" do
          before do
            @assumed_user.current_user = users(:minimum_admin) # Needed to allow user deletion (see deletion.rb)
            @assumed_user.delete!
          end

          it "does not access a protected pages (fx: /users)" do
            visit "/users"
            assert_redirected_to "#{@assumed_user.account.url}/access/unauthenticated?return_to=https%3A%2F%2Fminimum.zendesk-test.com%2Fusers"
          end
        end

        describe "when revert to original user" do
          before do
            visit "/users/revert"
          end

          it "access a protected pages (fx: /users)" do
            visit "/users"
            assert_response :success
          end
        end
      end
    end
  end
end
