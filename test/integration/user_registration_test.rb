require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class UserRegistrationTest < ActionDispatch::IntegrationTest
  fixtures :plans, :accounts, :account_texts, :account_settings, :users, :tickets, :sequences, :rules, :groups, :memberships, :organizations

  before do
    ActionMailer::Base.deliveries = []
  end

  describe "closed account" do
    before do
      account = Account.find(accounts('minimum').id)
      account.update_attribute('is_open', false)
      account.enable_help_center!
      refute account.is_open?
    end

    it "rejects user registrations" do
      self.host = accounts(:minimum).host_name

      # Goto signup page
      get "/registration"
      assert_response 401
    end
  end

  describe "open account" do
    before do
      account = Account.find(accounts('minimum').id)
      account.update_attribute('is_open', true)
      account.enable_help_center!
      assert account.is_open?
      @num_deliveries = ActionMailer::Base.deliveries.size
    end

    it "accepts user registrations" do
      self.host = accounts(:minimum).host_name

      # Goto signup page
      get "/registration"
      assert_response :success
    end

    describe "registering" do
      before do
        ActionMailer::Base.perform_deliveries = true
        self.host = accounts(:minimum).host_name
        https!
      end

      describe "with invalid email domain" do
        before do
          post_via_redirect '/registration/register', user: {name: 'Joe Doe', email: 'a@aaa.dk', password: '123456'}
          @user = accounts(:minimum).find_user_by_email('a@aaa.dk')
        end

        it "does not create a user" do
          assert_nil(@user)
        end

        it "shows an error message" do
          assert_template 'auth/sso_v2_index'
          assert_select '.notification-error'
        end

        it "does not send a welcome email" do
          assert_equal(@num_deliveries, ActionMailer::Base.deliveries.size)
        end
      end

      describe "with existing email address" do
        before do
          ActionMailer::Base.perform_deliveries = true
          post_via_redirect '/registration/register', user: {name: 'Joe Doe', email: 'minimum_end_user@aghassipour.com', password: '123456'}
          @user = accounts(:minimum).find_user_by_email('minimum_end_user@aghassipour.com')
        end

        it "shows an error message" do
          assert_template 'auth/sso_v2_index'
          assert_select '.notification-error'
        end

        it "does not send a welcome email" do
          assert_equal(@num_deliveries, ActionMailer::Base.deliveries.size)
        end
      end

      describe "valid users" do
        before do
          ActionMailer::Base.perform_deliveries = true
          post_via_redirect '/registration/register', user: {name: 'Joe Doe', email: 'joedoe@aghassipour.com', password: '123456'}
          @user = accounts(:minimum).find_user_by_email('joedoe@aghassipour.com')
        end

        it "creates a user" do
          assert(@user)
        end

        it "renders success" do
          assert_response :success
          assert_template 'auth/sso_v2_index'
          assert_select '#error', 0
        end

        it "sends a welcome email" do
          assert_equal(@num_deliveries + 1, ActionMailer::Base.deliveries.size)
        end

        it "does not log you in" do
          assert_nil(warden.user)
        end
      end
    end
  end
end
