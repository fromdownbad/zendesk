require_relative "../support/test_helper"
class AcmeChallengesTest < ActionDispatch::IntegrationTest
  fixtures :accounts

  describe "show" do
    let(:account) { accounts(:minimum) }
    let(:registration) { account.acme_registrations.first || AcmeRegistration.new(account: account) }
    let(:authorization) { AcmeAuthorization.create(account: account, acme_registration: registration, identifier: account.host_mapping, challenge_content: 'challenge_content', challenge_token: 'abc123') }

    before do
      @account = accounts(:minimum)
      @account.update_attribute(:host_mapping, 'support.localhost.com')
      assert_equal 0, authorization.accesses
    end

    describe "with an invalid token" do
      it "returns a 404" do
        get "http://support.localhost.com/.well-known/acme-challenge/not-a-token"
        assert_response :not_found
      end
    end

    describe "with a valid token" do
      before do
        get "http://support.localhost.com/.well-known/acme-challenge/#{authorization.challenge_token}"
        assert_response :ok
      end

      it "returns the challenge" do
        assert_equal authorization.challenge_content, response.body
      end

      it "increments the access counter" do
        assert_equal 1, authorization.reload.accesses
      end

      it "does not set any cookies" do
        assert_empty response.cookies
      end
    end
  end
end
