require_relative "../support/test_helper"
require_relative "../support/signed_auth_helper"
require_relative "../support/zendesk_dsl"

class ZendeskSystemUserTest < ActionDispatch::IntegrationTest
  include SignedAuthHelper
  fixtures :accounts, :users, :global_oauth_clients

  describe "Showing a user from the subsystem chat user" do
    before do
      @account = accounts(:minimum)
      @agent   = users(:minimum_admin)
    end

    describe "using signed auth" do
      before do
        @account.time_zone = "Pacific Time (US & Canada)"
        @account.save!
        request_system_user_signed(:get, :chat, "#{@account.url(protocol: 'https')}/api/v2/users/#{@agent.id}.json")
      end

      it "renders something" do
        assert_response :success
      end
    end

    describe "using OAuth" do
      let(:url) { "#{@account.url(protocol: 'https')}/api/v2/users/#{@agent.id}.json" }
      let(:params) { {"include" => "bar"} }
      before do
        @account.time_zone = "Pacific Time (US & Canada)"
        @account.save!
      end

      describe 'to a valid account' do
        before do
          request_system_user_oauth(:chat, url, params)
        end

        it "renders something" do
          assert_response :success
        end
      end

      describe "mac key rotation" do
        it "accepts the deprecated mac key" do
          request_system_user_oauth(:zendesk, url, params, use_deprecated_mac_key: true)
          assert_response :success
        end

        it "accepts the deprecated oauth key" do
          request_system_user_oauth(:zendesk, url, params, use_deprecated_key: true)
          assert_response :success
        end

        it "accepts the deprecated mac and oauth key" do
          request_system_user_oauth(:zendesk, url, params, use_deprecated_mac_key: true, use_deprecated_key: true)
          assert_response :success
        end
      end
    end

    describe 'to no account' do
      before do
        request_system_user_oauth(:chat, "https://zendesk-test.com/api/v2/internal/csv_exports/accounts.json")
      end

      it "renders something" do
        assert_response :success
      end
    end
  end
end
