require_relative "../support/test_helper"

class MobileIntegrationTest < ActionDispatch::IntegrationTest
  fixtures :accounts, :users, :remote_authentications

  before do
    @minimum_agent = users(:minimum_agent)
  end

  # TODO: Write intergration tests for new auth: [SECDEV-221]
  describe "#logout" do
    before do
      header "HTTP_USER_AGENT", "Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_3_3 like Mac OS X; en-us) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8J2 Safari/6533.18.5"
      login(@minimum_agent)
    end

    should_eventually "preserves mobile preference after logout" do
      assert session[Schmobile::IS_MOBILE]
      visit "/access/logout"
      assert session[Schmobile::IS_MOBILE]
    end
  end

  describe "mobile_api#create" do
    before do
      login(@minimum_agent)
      Account.any_instance.stubs(:subdomain).returns('voxer')
    end

    should_eventually "corrects voxer client over-escaping errors" do
      header 'X-Zendesk-Mobile-API', '1.0'
      over_escaped_body = StringIO.new("email=hello@example.com&set_tags=dropbox&description%3D%0A%0Ahello&via_id=17")

      post "/requests/mobile_api/create", session: { 'rack.input' => over_escaped_body }
      expected_params = { email: "hello@example.com", set_tags: "dropbox",
                          via_id: "17", description: "\n\nhello" }.with_indifferent_access
      assert_response :success, @response.body
      assert_equal(expected_params, @controller.params.except(:action, :controller, :format))
    end
  end
end
