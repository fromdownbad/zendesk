require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class UserNewPasswordAndVerificationTest < ActionDispatch::IntegrationTest
  include ZendeskDSL

  fixtures :all

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    Account.any_instance.stubs(:has_public_forums?).returns(false)
    Zendesk::UserPortalState.any_instance.stubs(:send_identity_verification_email?).returns(true)
  end

  it "tests new password" do
    open_session do |sess|
      sess.extend(ZendeskDSL)
      sess.host = accounts(:minimum).host_name
      # Goto new password page
      sess.get "/access/help"
      sess.assert_response :success
      # Enter incorrect email
      sess.post "/access/help", email: 'wrong@email.com'
      sess.assert_response :redirect
      sess.response.location.must_include(%(/auth/v2/login/password_reset))
      # Enter existing email
      sess.post "/access/help", email: 'minimum_end_user@aghassipour.com'
      sess.assert_response :redirect
      sess.response.location.must_include('/auth/v2/login/password_reset_end_page')
      # Read email
      body = ActionMailer::Base.deliveries[0].joined_bodies # Get body of sent email
      url = body.match(/\/password\/reset\/(.)+/)[0]
      token = url.split('/').last.split('?locale').first

      sess.get(url)
      assert_includes sess.response.body, "/assets/zendesk/auth/v2/host"

      sess.post '/password/reset', new_password: 'ujhyujhy', token: token
      sess.assert_redirected_to "#{accounts(:minimum).url}/access"

      @user = users(:minimum_end_user).reload
      assert @user.reload.authenticated?('ujhyujhy')
      sess.goes_to_logout

      # Login with new password
      new_session_as(accounts(:minimum).host_name, @user, 'ujhyujhy') do |user|
        user.get '/home'
        user.assert_response :success
        user.goes_to_logout
      end
    end
  end

  describe "with an enabled help center" do
    before do
      Account.any_instance.stubs(:help_center_enabled?).returns(true)
    end

    it "tests new verification" do
      assert_verification_sent
    end
  end

  describe "with a restricted web portal" do
    before do
      Account.any_instance.stubs(:web_portal_restricted?).returns(true)
    end

    it "tests new verification" do
      assert_verification_sent
    end
  end

  describe "with neither web portal or help center" do
    it "does not send a new verification" do
      register_user_new_password do
        # User receives new verify email
        assert_equal 1, ActionMailer::Base.deliveries.count
      end
    end
  end

  def assert_verification_sent
    register_user_new_password do |sess|
      # User receives new verify email
      body = ActionMailer::Base.deliveries[1].joined_bodies # Get body of sent email
      id = body.match(/verification\/email\/(\s*|\s*")(\w+)(\D*)\//)[2] # Get verify token

      # User goes to verify link, is redirected to login
      sess.get "/verification/email/#{id}"
      sess.goes_to_login
    end
  end

  def register_user_new_password
    open_session do |sess|
      sess.extend(ZendeskDSL)
      sess.host = accounts(:minimum).host_name
      # User signs up at web signup form
      sess.post "/registration/register", valid_user_signup_parameters
      sess.assert_response :redirect
      # User goes to directly to login, without verifying
      sess.post '/access/login', user: {email: 'ricky@aghassipour.com', password: 'juletrae88'}
      sess.follow_redirect!
      sess.assert_response :success
      sess.post '/access/help', email: 'ricky@aghassipour.com'
      sess.assert_response :redirect

      yield(sess)
    end
  end
end
