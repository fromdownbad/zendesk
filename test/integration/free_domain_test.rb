require_relative "../support/test_helper"

class FreeDomainTest < ActionDispatch::IntegrationTest
  describe "FreeDomainTest" do
    fixtures :all

    it "redirects to marketing when the subdomain is not found" do
      self.host = "moo.#{Zendesk::Configuration.fetch(:host)}"
      get "/"
      assert_response :redirect
      assert_equal 301, @response.status
      assert_no_match /top-menu-background/, response.body
    end
  end
end
