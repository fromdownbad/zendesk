require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class SharedCsrfTokenTestController < ApplicationController
  skip_before_action :authenticate_user
  ssl_allowed :token

  def token
    render plain: form_authenticity_token
  end
end

describe "SharedCsrfTokenControllerSupport Integration" do
  include ZendeskDSL
  prepend ControllerDefaultParams
  integrate_test_routes SharedCsrfTokenTestController

  fixtures :accounts, :users, :role_settings

  let(:account) { accounts(:minimum) }
  let(:agent) { account.owner }
  let(:url) { "https://#{account.host_name}" }
  let(:other_brand) { FactoryBot.create(:brand, account_id: account.id) }

  it "does not set the shared session csrf_token if the user isn't logged in" do
    get "#{url}/test/route/shared_csrf_token_test/token"

    refute_nil session['_csrf_token']
    assert_nil shared_session['csrf_token']
  end

  it "sets the form_authenticity_token into the shared_session when logged in" do
    logs_in_as agent, '123456'

    assert_equal session['_csrf_token'], shared_session['csrf_token']
  end

  it "update the csrf token in shared_session when the csrf_token changes in the session" do
    logs_in_as agent, '123456'
    old_value = session['_csrf_token']
    cookies.delete('_zendesk_test_Session')

    get "#{url}/test/route/shared_csrf_token_test/token"
    assert_response :success

    refute_equal old_value, session['_csrf_token']
    assert_equal session['_csrf_token'], shared_session['csrf_token']
  end

  it "does not update the csrf token in the shared_session if we are on the host mapped domain" do
    logs_in_as agent, '123456'
    authentication_domain_csrf_value = session['_csrf_token']

    params = {return_to: other_brand.route.url, auth_origin: "#{other_brand.id},false,true"}
    get "#{account.url(mapped: false, ssl: true)}/auth/v2/login/signin", params: params
    assert_response :redirect
    get response.location

    cookies.delete('_zendesk_test_Session')

    get "#{other_brand.route.url}/test/route/shared_csrf_token_test/token"
    assert_response :success

    refute_equal authentication_domain_csrf_value, session['_csrf_token']
    assert_equal authentication_domain_csrf_value, shared_session['csrf_token']
  end
end
