require_relative "../support/test_helper"

describe 'TZInfo::Data' do
  it "reflects major DST changes for Brazil in 2018" do
    # https://blogs.technet.microsoft.com/dst2007/2018/03/30/time-zone-updates-for-morocco-brazil-sao-tome-and-principe/
    assert_equal -10_800, Time.parse('2018-02-18T02:00:00').in_time_zone('Brasilia').utc_offset
    refute_equal -7_200, Time.parse('2018-10-21T03:00:00').in_time_zone('Brasilia').utc_offset
    assert_equal -7_200, Time.parse('2018-11-04T03:00:00').in_time_zone('Brasilia').utc_offset
  end

  it "reflects major DST changes for Brazil in 2019" do
    # https://support.microsoft.com/en-us/help/4507704/dst-changes-in-windows-for-brazil-and-morocco
    assert_equal -10_800, Time.parse('2019-02-17T02:00:00').in_time_zone('Brasilia').utc_offset
    refute_equal -7_200, Time.parse('2019-11-03T03:00:00').in_time_zone('Brasilia').utc_offset
    assert_equal -10_800, Time.parse('2019-11-03T03:00:00').in_time_zone('Brasilia').utc_offset
  end
end
