require_relative "../../../support/test_helper"

describe AnswerBot::RequestSolver do
  fixtures :accounts, :ticket_deflections, :ticket_deflection_articles, :sequences

  let(:account) { accounts(:minimum) }
  let(:deflection_article) { ticket_deflection_articles(:without_ticket) }
  let(:resolution_channel_id) { ViaType.WEB_WIDGET }
  let(:deflection) { ticket_deflections(:without_ticket) }
  let(:subject) { 'Phrase of the day' }
  let(:description) { 'A brown fox jumps over the lazy dog' }
  let(:solver) do
    AnswerBot::RequestSolver.new(
      deflection_id: deflection.id,
      article_id: deflection_article.article_id,
      resolution_channel_id: resolution_channel_id
    )
  end

  describe '#create_and_solve_ticket' do
    let(:hc_articles) do
      [
        {
          id: 51,
          title: 'title',
          url: 'http://example.com',
          html_url: 'http://example.com',
          body: 'A brown fox jumps over the lazy dog',
          locale: 'en-us'
        }
      ]
    end

    before do
      account.answer_bot_users.destroy_all
      Zendesk::HelpCenter::InternalApiClient.any_instance.
        stubs(:fetch_articles_for_ids_and_locales).
        returns(hc_articles)
    end

    it 'ends up with an anonymous answer bot user' do
      assert_difference 'account.users.count', 1 do
        solver.create_and_solve_ticket
      end
    end

    it 'creates a ticket' do
      assert_difference 'account.tickets.count', 1 do
        solver.create_and_solve_ticket
      end
    end

    it 'increments ticket nice id' do
      assert_difference 'account.nice_id_sequence.value', 1 do
        solver.create_and_solve_ticket
        account.reload
      end
    end

    it 'disables the answer bot trigger' do
      ticket = solver.create_and_solve_ticket

      assert ticket.disable_answer_bot_trigger
    end

    it 'the created ticket has the right details' do
      ticket = solver.create_and_solve_ticket

      assert_equal 'Toast', ticket.subject
      assert_equal 'A complete and unabridged history.', ticket.description
      assert_equal deflection.deflection_channel_id, ticket.via_id
    end

    it 'the created ticket is solved' do
      ticket = solver.create_and_solve_ticket
      assert_equal StatusType.SOLVED, ticket.status_id
    end

    it 'associates deflection to the anonymous user' do
      solver.create_and_solve_ticket

      expected_requester = account.answer_bot_users.first
      assert_equal expected_requester, deflection.reload.user
    end

    it 'associates deflection to the created ticket' do
      ticket = solver.create_and_solve_ticket

      assert_equal ticket, deflection.reload.ticket
    end

    it 'adds Answer Bot audit events to ticket' do
      ticket = solver.create_and_solve_ticket

      audit_event_types = ticket.audits.map { |a| a.events.map(&:type) }.flatten
      assert_includes audit_event_types, 'AutomaticAnswerSend'
      assert_includes audit_event_types, 'AutomaticAnswerSolve'
    end

    it 'updates deflection record to solved' do
      solver.create_and_solve_ticket
      assert_equal TicketDeflection::STATE_SOLVED, deflection.reload.state
    end

    it 'updates solved deflection article' do
      solver.create_and_solve_ticket
      assert_not_nil deflection_article.reload.solved_at
    end

    describe 'when deflected on non-default brand' do
      let!(:another_brand) do
        Brand.create! do |b|
          b.account_id = account.id
          b.name = 'Another'
          b.route = Route.create!(account: account, subdomain: 'another')
        end
      end
      let!(:deflection) do
        deflection = ticket_deflections(:without_ticket)
        deflection.update_attributes!(brand: another_brand)
        deflection
      end

      it 'creates a ticket of that brand' do
        ticket = solver.create_and_solve_ticket
        assert_equal another_brand, ticket.brand
      end
    end

    describe 'when one of the update failed' do
      before do
        TicketDeflectionArticle.any_instance.
          stubs(:update_attributes).
          raises('Fire fire!')
      end

      it 'does not create ticket' do
        assert_no_difference 'account.tickets.count' do
          assert_raises(RuntimeError) { solver.create_and_solve_ticket }
        end
      end

      it 'does not update deflection' do
        assert_raises(RuntimeError) do
          solver.create_and_solve_ticket
        end

        deflection.reload
        assert_nil deflection.user
        assert_nil deflection.ticket_id
      end

      it 'ticket nice id will still be incremented' do
        assert_difference 'account.nice_id_sequence.value', 1 do
          assert_raises(RuntimeError) { solver.create_and_solve_ticket }
          account.reload
        end
      end
    end
  end
end
