require_relative "../support/test_helper"

describe 'Booting without account master database connection' do
  it 'does not abort when connecting to an unavailable server' do
    # ENABLE_ZENDESK_APM forces config/initializers/datadog_tracer.rb to
    # autoloads several models (e.g. Ticket). We are not using
    # FORCE_EAGER_LOAD, because production doesn't use it.
    system(%q(RAILS_ENV=test ACCOUNT_MASTER_UNAVAILABLE=true ENABLE_ZENDESK_APM=true rails runner 'puts "Classic successfully booted with ACCOUNT_MASTER_UNAVAILABLE=true"'))
    $?.success?.must_equal true
  end
end
