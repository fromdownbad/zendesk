require_relative "../support/test_helper"

class TargetsControllerTest < ActionDispatch::IntegrationTest
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:minimum_admin) { users(:minimum_admin) }
  let(:url) { "#{account.url}/targets/test/email_target" }
  let(:params) do
    { format: 'js', id: 'email_target',
      target: { title: 'Email Target', email: 'admin@zendesk.com' } }
  end

  describe "a request to test_target" do
    before do
      login(minimum_admin)
    end

    it "as a GET returns 404" do
      get url, params: params
      assert_response :not_found
    end

    it "as a POST returns 200" do
      post url, params: params
      assert_response :success
    end

    it "as a PUT returns 200" do
      put url, params: params
      assert_response :success
    end
  end
end
