require_relative "../support/test_helper"

class RulesCountTest < ActionDispatch::IntegrationTest
  fixtures :all

  describe "/rules/count" do
    before do
      login(users(:minimum_agent))
      stub_occam_count(2)

      visit("https://minimum.zendesk-test.com/rules/count", :post, rule_ids: [rules(:view_my_working_tickets).id, rules(:view_assigned_working_tickets).id])
    end

    it "renders a json response with all rules counts" do
      assert_response :success
      json = JSON.parse(body)

      assert(json.is_a?(Array))
      assert_equal(2, json.size)

      assert(json.all? { |item| item.key?('rule_id') })
      assert(json.all? { |item| item.key?('value') })
      assert(json.all? { |item| item.key?('fresh') })

      assert(json.find { |item| item['rule_id'] == rules(:view_my_working_tickets).id })
      assert(json.find { |item| item['rule_id'] == rules(:view_assigned_working_tickets).id })
    end

    before_should "enqueue rule count updates" do
      Resque.expects(:enqueue).twice
    end
  end
end
