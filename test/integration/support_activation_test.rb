require_relative "../support/test_helper"
require_relative "../support/suite_test_helper"
require_relative "../support/multiproduct_test_helper"

describe 'When ShellAccount activates Support' do
  include MultiproductTestHelper
  include SuiteTestHelper
  let(:shell_account) { setup_shell_account }
  let(:support_account) { FactoryBot.create(:account) }

  before do
    stub_account_service_account(shell_account)
    ZBC::Billing::Zuora::Ping.stubs(:request)
    Omnichannel::EntitlementSyncJob.stubs(:enqueue)
    Zendesk::TrialActivation.activate_support_trial!(shell_account)
    support_account.reload
  end

  it 'should have the same settings as a Support first account' do
    whitelisted_different_settings = %w[export_whitelisted_domain default_brand_id]
    # export_whitelisted_domain is generated from owner's email, default_brand_id is different per account
    support_account.settings.each do |setting|
      unless whitelisted_different_settings.include?(setting.name)
        shell_account_setting = shell_account.settings.find_by_name(setting.name)
        assert_equal_with_nil shell_account_setting.value, setting.value
      end
    end
  end
end
