require_relative '../integration_test_helper'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/token_factory'

describe "Provisioning Magento Integration" do
  it 'creates a new zendesk account for magento' do
    AccountCreationShard.stubs(:pluck).returns([1])
    account_params = {
      "confirm_origin" => "zendesk_internal_origin",
      "include" => "magento",
      "account" => {
        "subdomain"        => "xyz",
        "name"             => "Magento Store",
        "help_desk_size"   => "4",
        "language"         => "en",
        "source"           => "Magento",
        "remote_login_url" => "https://www.example.com/login",
        "utc_offset"       => "Australia/Melbourne"
      },
      "owner" => {
        "name"  => "John Smith",
        "email" => "john@example.com"
      },
      "address" => {
        "phone" => "+1-123-456-7890"
      },
      "agents" => [
        {
          "name"  => "John Smith",
          "email" => "john@example.com",
          "role"  => "Admin"
        }
      ]
    }
    account = FactoryBot.create(:account, subdomain: 'zendeskaccounts')
    client = FactoryBot.create(:client, account: account, user: account.users.first)
    token = FactoryBot.create(:token, scopes: ["accounts:write"], account: account, client: client)

    origin_local = ENV["LOCAL"]
    ENV["LOCAL"] = "true"
    options = { 'CONTENT_TYPE' => 'application/json', 'HTTP_AUTHORIZATION' => "Bearer #{token.token(:full)}" }
    post "#{account.url}/api/v2/internal/new_account.json", params: account_params.to_json, headers: options
    ENV["LOCAL"] = origin_local

    response_json = JSON.parse(response.body)

    assert response_json["success"]
    assert_equal "https://#{account_params["account"]["subdomain"]}.zendesk-test.com", response_json["account"]["url"]
    assert_equal account_params["account"]["subdomain"], response_json["account"]["subdomain"]
    assert_not_empty response_json["magento"]["api_token"]
    assert_not_empty response_json["magento"]["remote_authentication_token"]
    assert response_json["magento"]["magento_order_number_ticket_field_id"] > 0
  end
end
