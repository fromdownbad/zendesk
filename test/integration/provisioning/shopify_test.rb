require_relative '../integration_test_helper'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/token_factory'

describe "Shopify Integration" do
  fixtures :all

  def get_account_params # rubocop:disable Naming/AccessorMethodName
    {
      "confirm_origin" => "zendesk_internal_origin",
      "account" => {
        "subdomain"      => "myshop#{(rand * 10000).round}",
        "name"           => "My Shop",
        "help_desk_size" => "1",
        "source"         => "Shopify",
        "language"       => "en",
        "utc_offset"     => "Eastern Time (US & Canada)"
      },
      "owner" => {
        "name"  => "John Smith",
        "email" => "john@example.com"
      },
      "address" => {
        "phone"    => "+1-123-456-7890",
        "street"   => "123 King Street",
        "city"     => "Ottawa",
        "zip"      => "K1N 5X8",
        "province" => "Ontario"
      },
      "agents" => [
        {
          "name"  => "John Smith",
          "email" => "john@example.com",
          "role"  => 'Admin'
        }
      ]
    }
  end

  let(:account) { accounts(:minimum) }

  before do
    Zendesk::Configuration::PodConfig.stubs(pod_ids: [1])

    # The only valid oauth account creator as the subdomain 'zendeskaccounts'.
    # This updates a fixture to satisfy that requirement so we don't have to
    # incur the overhead of creating an account.
    account.subdomain = 'zendeskaccounts'
    account.save!
  end

  it 'creates a new zendesk account for shopify without dropbox' do
    client = FactoryBot.create(:client, account: account, user: account.users.first)
    token = FactoryBot.create(:token, scopes: ["accounts:write"], account: account, client: client)

    account_params = get_account_params
    origin_local = ENV["LOCAL"]
    ENV["LOCAL"] = "true"
    options = { 'CONTENT_TYPE' => 'application/json', 'HTTP_AUTHORIZATION' => "Bearer #{token.token(:full)}" }
    post "#{account.url}/api/v2/internal/new_account.json", params: account_params.to_json, headers: options
    ENV["LOCAL"] = origin_local

    response_json = JSON.parse(response.body)

    assert response_json["success"]
    assert_equal "https://#{account_params["account"]["subdomain"]}.zendesk-test.com", response_json["account"]["url"]
    assert_equal account_params["account"]["subdomain"], response_json["account"]["subdomain"]
  end
end
