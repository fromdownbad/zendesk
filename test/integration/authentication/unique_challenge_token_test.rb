require_relative "../../support/test_helper"
require_relative "../../support/zendesk_dsl"

describe "Unique Challenge Token Integration", :integration do
  include ZendeskDSL
  fixtures :accounts, :users, :role_settings

  let(:account) { accounts(:minimum).tap { |a| a.update_attribute(:host_mapping, 'wibble.example.com') } }
  let(:agent) { account.owner }
  let(:url) { "#{account.authentication_domain}/access/login" }
  let(:params) { { user: {email: agent.email, password: '123456'}, return_to: "#{account.url}/ping/host" } }

  def parse_challenge(url)
    Rack::Utils.parse_query(URI.parse(url).query)["challenge"]
  end

  describe "after a login that redirects with a challenge token" do
    before do
      post url, params: params
      refute_nil @first_token = parse_challenge(response.location)
    end

    describe "and the user logs in again" do
      before do
        post url, params: params
        refute_nil @second_token = parse_challenge(response.location)
      end

      it "should persist the first token" do
        assert_equal 1, agent.challenge_tokens.where(value: @first_token).count(:all)
      end

      it "should persist the second token" do
        assert_equal 1, agent.challenge_tokens.where(value: @second_token).count(:all)
      end

      it "should create unqiue tokens" do
        refute_equal @first_token, @second_token
      end
    end
  end
end
