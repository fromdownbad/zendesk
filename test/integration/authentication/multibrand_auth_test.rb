require_relative "../../support/test_helper"
require_relative "../../support/zendesk_dsl"

describe "Zendesk::Auth::V2::LoginsController Integration", :integration do
  include ZendeskDSL
  fixtures :all

  before do
    @account = accounts(:minimum)
    @account.update_attribute('is_open', true)
    @admin = users(:minimum_admin)
    @other_brand = FactoryBot.create(:brand, account_id: @account.id)
  end

  describe "after login" do
    before do
      logs_in_as @admin, '123456'
      params = {return_to: @other_brand.route.url, auth_origin: "#{@other_brand.id},false,true"}
      get "#{@account.url(mapped: false, ssl: true)}/auth/v2/login/signin", params: params
      assert_response :redirect
    end

    it "should redirect with a challenge token that is tied to the session" do
      params = Rack::Utils.parse_query(URI.parse(response.location).query)
      token = @admin.challenge_tokens.where(value: params["challenge"]).first

      assert_equal shared_session.session_key, token.shared_session_key
    end

    it "should not issue a new session when consuming the challenge token" do
      current_session_key = shared_session.session_key
      get response.location

      assert_response :ok
      assert_equal current_session_key, shared_session.session_key
    end

    it "should not update the auth_created_at when consuming the challenge token" do
      auth_created_at = shared_session['auth_authenticated_at']

      get response.location

      assert_response :ok
      assert auth_created_at
      assert_equal auth_created_at, shared_session['auth_authenticated_at']
    end

    it "the auth_duration should not change after consuming the challenge token" do
      shared_session['auth_duration'] = 1000
      shared_session.save

      get response.location

      assert_response :ok
      assert_equal 1000, shared_session['auth_duration']
    end
  end

  describe "registration/register" do
    before do
      @user_params = { email: 'mentor@gmail.com', name: 'men tor', password: 'mentor' }
    end
    it "should set x-frame-options if the referer is invalid" do
      post "#{@account.url(mapped: false, ssl: true)}/registration/register", params: { user: @user_params }, headers: { "HTTP_REFERER" => "https://attacker.com/foobar" }

      assert_response :redirect
      assert_equal 'SAMEORIGIN', response.headers['X-Frame-Options']
    end

    it "should set x-frame-options if the referer is malformed" do
      post "#{@account.url(mapped: false, ssl: true)}/registration/register", params: { user: @user_params }, headers: { "HTTP_REFERER" => "://attacker.com/foobar" }

      assert_response :redirect
      assert_equal 'SAMEORIGIN', response.headers['X-Frame-Options']
    end

    it "should set x-frame-options if the referer is empty" do
      post "#{@account.url(mapped: false, ssl: true)}/registration/register", params: { user: @user_params }, headers: { "HTTP_REFERER" => '' }

      assert_equal 'SAMEORIGIN', response.headers['X-Frame-Options']
    end

    it "should not set x-frame-options if the referer is valid" do
      post "#{@account.url(mapped: false, ssl: true)}/registration/register", params: { user: @user_params }, headers: { "HTTP_REFERER" => "#{@other_brand.url}/foobar" }

      assert_response :redirect
      assert_nil response.headers['X-Frame-Options']
    end
  end

  describe "with an expired password" do
    before do
      User.any_instance.stubs(password_expired?: true)
      auth_url = "#{@account.url(mapped: false, ssl: true)}/access/login"
      post auth_url, params: { user: { email: @admin.email, password: '123456' } }
    end

    it "grants the user a restricted session" do
      assert_equal @admin.id, shared_session.user_id
    end

    it "redirects to /auth/v2/login/password_change" do
      assert_redirected_to '/auth/v2/login/password_change'
    end

    it "restricts the session" do
      get "#{@account.url(mapped: false, ssl: true)}/settings/account"
      assert_redirected_to '/password'
    end

    it "can fetch the host javascript" do
      get "#{@account.url(mapped: false, ssl: true)}/auth/v2/host.js"
      assert_response :success
    end

    it "can access the password_change action" do
      get "#{@account.url(mapped: false, ssl: true)}/auth/v2/login/password_change"
      assert_response :success
    end
  end

  describe "with an suspended user" do
    before do
      User.any_instance.stubs(suspended?: true)
      auth_url = "#{@account.url(mapped: false, ssl: true)}/access/login"
      post auth_url, params: { user: { email: @admin.email, password: '123456' } }
    end

    it "should redirect to /access/unauthenticated" do
      assert_match /\/access\/unauthenticated\?flash_digest=.{40}/, response.location
    end
  end

  describe "access/help" do
    before do
      @user_params = { email: 'mentor@gmail.com'}
    end
    it "should set x-frame-options if the referer is invalid" do
      post "#{@account.url(mapped: false, ssl: true)}/access/help", params: { user: @user_params }, headers: { "HTTP_REFERER" => "https://attacker.com/foobar" }

      assert_redirected_to zendesk_auth.password_reset_end_page_v2_login_url
      assert_equal 'SAMEORIGIN', response.headers['X-Frame-Options']
    end

    it "should not set x-frame-options if the referer is valid" do
      post "#{@account.url(mapped: false, ssl: true)}/access/help", params: { user: @user_params }, headers: { "HTTP_REFERER" => "#{@other_brand.url}/foobar" }

      assert_redirected_to zendesk_auth.password_reset_end_page_v2_login_url
      assert_nil response.headers['X-Frame-Options']
    end
  end
end
