require_relative "../../support/test_helper"
require_relative "../../support/mailer_test_helper"
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

describe "Mobile Authentication Integration", :integration do
  include MailerTestHelper
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:user) { account.owner }
  let(:client) do
    c = FactoryBot.create(:global_client, user: user, redirect_uri: 'zendesk://authenticate')
    c.update_columns(identifier: 'zendesk_mobile_ios')
    c
  end
  let(:device_info) { {identifier: SecureRandom.hex(32), name: "Morten's iPhone"} }

  describe "JWT" do
    before do
      @auth = remote_authentications(:minimum_remote_authentication)
      @auth.auth_mode = RemoteAuthentication::JWT
      @auth.is_active = true
      @auth.remote_login_url = "https://sso.cpconsulting.dk/zendesk_remote_auth.asp"
      @auth.save!

      role_settings = account.role_settings
      role_settings.agent_zendesk_login    = false
      role_settings.end_user_zendesk_login = false
      role_settings.agent_remote_login     = true
      role_settings.end_user_remote_login  = true
      role_settings.save!
    end

    let(:headers) do
      {
        'HTTP_USER_AGENT' => 'Zendesk for iPhone',
        'HTTP_COOKIE' => "_zendesk_client_identifier=#{client.identifier}; _zendesk_mobile_device=#{Base64.urlsafe_encode64(device_info.to_json)}"
      }
    end

    describe "when the mobile apps are disabled" do
      before do
        account.settings.mobile_app_access = false
        account.settings.save!
      end

      it "blocks mobile password logins" do
        get(account.authentication_domain + "/access/jwt?#{valid_jwt.to_param}&return_to=/home", headers: headers)
        assert_response :forbidden
        assert_equal({"error" => "MobileAccessDisabled", "description" => "The administrator for this Zendesk has disabled mobile app access."}, JSON.parse(response.body))
      end
    end

    describe "with using cookies to pass device info" do
      before do
        get(account.authentication_domain + "/access/jwt?#{valid_jwt.to_param}&return_to=/home", headers: headers)

        assert_response :redirect
        assert response.location.starts_with?('zendesk://authenticate'), "expected redirect to zendesk://authenticate was #{response.location}"

        uri = URI.parse(response.location)
        @redirect_params = Rack::Utils.parse_query(uri.query)
      end

      it "creates a mobile device" do
        assert_equal 1, user.mobile_devices.count(:all)
      end

      it "issues a oauth token tied to the device" do
        token = client.tokens.first
        assert_equal @redirect_params["access_token"], token.token(true)
        assert_equal token, user.mobile_devices.first.oauth_token
      end

      it "sets the device identifier" do
        assert_equal device_info[:name], user.mobile_devices.first.name
      end

      it "sets the device name" do
        assert_equal device_info[:identifier], user.mobile_devices.first.token
      end

      it "doesn't issue a _zendesk_shared_session cookie" do
        assert_empty user.shared_sessions
      end
    end
  end

  describe "with device information" do
    let(:headers) do
      {
        'HTTP_CLIENT_IDENTIFIER' => client.identifier,
        'HTTP_USER_AGENT' => 'Zendesk for iPhone',
        'HTTP_ACCEPT' => 'application/json'
      }
    end
    let(:params) do
      {
        native_mobile: true,
        user: {email: user.email, password: '123456'},
        device: device_info
      }
    end

    before { assert_empty user.mobile_devices }

    describe "when the mobile apps are disabled" do
      before do
        account.settings.mobile_app_access = false
        account.settings.save!
      end

      it "blocks mobile password logins" do
        post account.url(ssl: true) + "/access/oauth_mobile", params: params, headers: headers
        assert_response :forbidden
        assert_equal({"error" => "MobileAccessDisabled", "description" => "The administrator for this Zendesk has disabled mobile app access."}, JSON.parse(response.body))
      end
    end

    describe "a normal mobile login" do
      before do
        post account.url(ssl: true) + "/access/oauth_mobile", params: params, headers: headers
        assert_response :ok

        @json = JSON.parse(response.body)
        user.mobile_devices(true)
      end

      it "creates a mobile device" do
        assert_equal 1, user.mobile_devices.count(:all)
      end

      it "issues a oauth token tied to the device" do
        token = client.tokens.first
        assert_equal @json["authentication"]["access_token"], token.token(true)
        assert_equal token, user.mobile_devices.first.oauth_token
      end

      it "sets the device name" do
        assert_equal device_info[:name], user.mobile_devices.first.name
      end

      it "sets the device identifier" do
        assert_equal device_info[:identifier], user.mobile_devices.first.token
      end

      it "doesn't create a new device when using the device scoped token" do
        token = @json["authentication"]["access_token"]
        cookies.delete("_zendesk_shared_session")

        get(account.url(ssl: true) + "/api/v2/users/me.json", headers: { "HTTP_AUTHORIZATION" => "Bearer #{token}" })
        json = JSON.parse(response.body)
        assert_equal user.id, json["user"]["id"]
        assert_equal 1, user.devices(true).count(:all)

        assert_equal device_info[:identifier], user.devices.first.token
      end

      it "doesn't issue a _zendesk_shared_session cookie" do
        assert_empty user.shared_sessions
      end

      describe "#logout" do
        before do
          @mobile_device = user.mobile_devices.first
          @token = client.tokens.where(token: @json["authentication"]["access_token"]).first
          delete "#{account.url(ssl: true)}/api/mobile/devices/logout.json", headers: headers.merge('HTTP_AUTHORIZATION' => "Bearer #{@token.token(true)}")

          assert_response :ok
        end

        it "deletes the token" do
          assert_raises(ActiveRecord::RecordNotFound) { @token.reload }
        end

        it "does not remove the device" do
          @mobile_device.reload
        end

        it "removes the token from the mobile device" do
          assert_nil @mobile_device.reload.oauth_token_id
        end

        describe "and then on the next login" do
          before do
            post account.url(ssl: true) + "/access/oauth_mobile", params: params, headers: headers
            assert_response :ok

            @json = JSON.parse(response.body)
            user.mobile_devices(true)
          end

          it "uses the existing mobile device" do
            assert_equal 1, user.mobile_devices.count(:all)
            assert_equal @mobile_device, user.mobile_devices.first
          end
        end
      end
    end

    describe "with a 2FA enabled user" do
      before do
        Zendesk::Auth::Otp::Phone.any_instance.stubs(send_message: true)
        user.one_time_password(time_based: false, phone: '555-555-5555').setup!
        user.otp_setting.update_attributes!(confirmed: true)

        post account.url(ssl: true) + "/access/oauth_mobile", params: params, headers: headers
        assert_response :accepted

        @json = JSON.parse(response.body)
        user.mobile_devices(true)
      end

      it "requires a otp" do
        assert_equal({ "message" => "Enter a one time password to continue", "authentication_channel" => "sms" }, @json)
      end

      it "returns a token" do
        assert_equal 1, user.otp_tokens.count
        assert_equal user.otp_tokens[0].value, response.headers['User-Id']
      end

      it "did not create a mobile device" do
        assert_empty user.mobile_devices
      end

      it "did not create a token" do
        assert_empty client.tokens
      end

      describe "when submitting a valid OTP" do
        before do
          params = { password: user.reload.one_time_password.time_based_token, device: device_info }
          headers['HTTP_USER_ID'] = user.otp_tokens[0].value
          post account.url(ssl: true) + "/auth/one_time_password", params: params, headers: headers

          assert_response :ok
          @json = JSON.parse(response.body)
          user.mobile_devices(true)
        end

        it "creates a mobile device" do
          assert_equal 1, user.mobile_devices.count(:all)
        end

        it "issues a oauth token tied to the device" do
          token = client.tokens.first
          assert_equal @json["authentication"]["access_token"], token.token(true)
          assert_equal token, user.mobile_devices.first.oauth_token
        end

        it "sets the device identifier" do
          assert_equal device_info[:name], user.mobile_devices.first.name
        end

        it "sets the device name" do
          assert_equal device_info[:identifier], user.mobile_devices.first.token
        end

        it "doesn't issue a _zendesk_shared_session cookie" do
          assert_empty user.shared_sessions
        end
      end

      describe "when submitting an invalid OTP" do
        before do
          params = { password: 'invalid', device: device_info }
          headers['HTTP_USER_ID'] = user.otp_tokens[0].value
          post account.url(ssl: true) + "/auth/one_time_password", params: params, headers: headers

          assert_response :accepted
          @json = JSON.parse(response.body)
          user.mobile_devices(true)
        end

        it "requires a otp" do
          assert_equal({ "message" => "Enter a one time password to continue", "authentication_channel" => "sms" }, @json)
        end

        it "did not create a mobile device" do
          assert_empty user.mobile_devices
        end
      end
    end

    describe "when the device identifier is already in use" do
      before do
        post account.url(ssl: true) + "/access/oauth_mobile", params: params, headers: headers
        assert_response :ok
        user.mobile_devices(true)

        @old_token = client.tokens.first
        @device = user.mobile_devices.first

        post account.url(ssl: true) + "/access/oauth_mobile", params: params.merge(device: device_info.merge(name: "Mick's iPhone")), headers: headers
        assert_response :ok
        @device.reload
      end

      it "updates the old device" do
        assert_equal 1, user.mobile_devices.count(:all)
        assert_equal "Mick's iPhone", @device.name
      end

      it "creates a new token" do
        json = JSON.parse(response.body)
        refute_equal json["authentication"]["access_token"], @old_token.token(true)
      end

      it "deletes the old token" do
        assert_raises(ActiveRecord::RecordNotFound) { @old_token.reload }
      end
    end

    describe "using a master token to assume into an account" do
      before do
        ActionMailer::Base.perform_deliveries = true
        ActionMailer::Base.deliveries         = []
        @admin_recipients = [
          "minimum_admin@aghassipour.com",
          "minimum_admin_not_owner@aghassipour.com"
        ]
        token = user.master_tokens.create(assuming_monitor_user_id: 1234)
        jwt_claim = JWT.encode({token: token.value, permission: {read_only: false}}, Zendesk::Configuration["monitor"]["key"])

        params[:user][:password] = jwt_claim
        post account.url(ssl: true) + "/access/oauth_mobile", params: params, headers: headers
        assert_response :ok

        cookies.delete("_zendesk_shared_session")
        cookies.delete("_zendesk_cookie")

        @json = JSON.parse(response.body)
        @oauth_token = client.tokens.where(token: @json["authentication"]["access_token"]).first
        @email = ActionMailer::Base.deliveries.last
      end

      it "issues an oauth token that isn't tied to a device" do
        assert_empty user.mobile_devices.where(oauth_token_id: @oauth_token.id)
      end

      it "the oauth token is short lived" do
        assert_in_delta 0, (@oauth_token.expires_at - @oauth_token.created_at) - (60 * 60), 1
      end

      it "the token is tied to the monitor user" do
        assert_equal 1234, @oauth_token.monitor_user_id
      end

      it "doesn't create a device when using the token" do
        user.devices.clear
        get(account.url(ssl: true) + "/api/v2/users/me.json", headers: { "HTTP_AUTHORIZATION" => "Bearer #{@json["authentication"]["access_token"]}" })
        assert_response :ok

        json = JSON.parse(response.body)
        assert_equal user.id, json["user"]["id"]

        assert_empty user.devices(true)
      end

      it "doesn't issue a _zendesk_shared_session cookie" do
        assert_empty user.shared_sessions
      end

      it "delivers a notification when bypassing" do
        refute_empty ActionMailer::Base.deliveries
      end

      it "sends the email to admins on the account" do
        assert_equal @admin_recipients, @email.bcc
      end

      it "sends an email with the correct subject" do
        @email.subject.must_include I18n.t('txt.email.account_assumption.assumption_bypass.subject')
      end

      it "sends an email with bypass content" do
        @email.text_part.body.decoded.must_include I18n.t('txt.email.account_assumption.assumption_bypass.content')
      end
    end
  end

  describe 'Mobile apps SSO authentication' do
    describe 'account does not support multiple products' do
      let(:app_account) { accounts(:minimum) }
      let(:app_token) { authentication_token(app_account) }

      it 'authenticates succefully' do
        post_and_validate_mobile_sso(app_account, app_token)
      end
    end

    describe 'account supports multiple products and has support enabled' do
      let(:app_account) { accounts(:multiproduct) }
      let(:app_token) { authentication_token(app_account) }

      before do
        add_products_to_current_account(
          [
            {name: :support, active: true},
            {name: :chat, active: true}
          ]
        )
      end

      it 'authenticates succefully' do
        post_and_validate_mobile_sso(app_account, app_token)
      end
    end

    describe 'account supports multiple products and has support disabled' do
      let(:app_account) { accounts(:multiproduct) }
      let(:app_token) { authentication_token(app_account) }

      before do
        add_products_to_current_account(
          [
            {name: :support, active: false},
            {name: :chat, active: true}
          ]
        )
      end

      it 'authenticates succefully' do
        post_and_validate_mobile_sso(app_account, app_token)
      end
    end

    describe 'account supports multiple products and does not have support' do
      let(:app_account) { accounts(:multiproduct) }
      let(:app_token) { authentication_token(app_account) }

      before do
        add_products_to_current_account(
          [
            {name: :chat, active: true}
          ]
        )
      end

      it 'authenticates succefully' do
        post_and_validate_mobile_sso(app_account, app_token)
      end
    end
  end

  private

  def valid_jwt(options = {})
    {
      jwt: JWT.encode({
        jti: "unique.request",
        iat: Time.now.to_i,
        name: user.name,
        email: user.email,
      }.merge(options), @auth.token)
    }
  end

  def add_products_to_current_account(products)
    def create_product(name, active)
      product = OpenStruct.new(
        'id' => name,
        'account_id' => '123456',
        'state' => :subscribed,
        'name' => name,
        'active' => active
      )

      def product.active?
        active
      end

      product
    end

    Zendesk::Auth::Api::MobileZopimSsoController.any_instance.
      stubs(:current_account_products).
      returns(products.map { |p| create_product(p[:name], p[:active]) })
  end

  def login_body(account)
    {
      native_mobile: true,
      user: {email: account.owner.email, password: '123456'},
      device: device_info
    }
  end

  def default_headers(token = nil)
    {}.tap do |h|
      h['HTTP_CLIENT_IDENTIFIER'] = client.identifier
      h['HTTP_USER_AGENT'] = 'Zendesk for Android/2.8.12 Chat'
      h['HTTP_ACCEPT'] = 'application/json'
      h['HTTP_AUTHORIZATION'] = "Bearer #{token}" unless token.nil?
    end
  end

  def authentication_token(account)
    post(account.url(ssl: true) + "/access/oauth_mobile", params: login_body(account), headers: default_headers)

    assert_response :ok

    JSON.parse(response.body)["authentication"]["access_token"]
  end

  def post_and_validate_mobile_sso(account, token)
    post(account.url(ssl: true) + '/auth/api/mobile/sso?client_id=zendesk_chat_android', headers: default_headers(token))

    assert_response :ok
  end
end
