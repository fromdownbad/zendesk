require_relative "../../support/test_helper"
require_relative "../../support/zendesk_dsl"

describe "Login and Logout events", :integration do
  include ZendeskDSL
  fixtures :accounts, :users, :role_settings

  let(:account) { accounts(:minimum) }
  let(:agent) { account.owner }
  let(:login_url) { "#{account.authentication_domain}/access/login" }
  let(:logout_url) { "#{account.authentication_domain}/access/logout" }
  let(:params) { { user: {email: agent.email, password: '123456'} } }

  describe "logging in" do
    it "calls #publish_login_event!" do
      Zendesk::Auth::AuthenticationEventPublisher.any_instance.expects(:publish_login_event!)
      post login_url, params: params
    end
  end

  describe "logging out" do
    before do
      post login_url, params: params
    end

    it "calls #publish_logout_event!" do
      Zendesk::Auth::AuthenticationEventPublisher.any_instance.expects(:publish_logout_event!).once
      get logout_url
    end
  end
end
