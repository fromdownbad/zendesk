require_relative "../../support/test_helper"
require_relative "../../support/api_test_helper"

describe "Basic Auth Integration", :integration do
  include ApiTestHelper

  fixtures :accounts, :users, :role_settings

  let(:account) { accounts(:minimum) }
  let(:user) { account.owner }
  let(:email) { user.email }
  let(:password) { "123456" }
  let(:url) { "https://#{account.host_name}" }
  let(:json) { JSON.parse(@response.body) }

  describe "with an expired password" do
    before do
      account.settings.api_token_access = true
      @api_token = account.api_tokens.create!.temp_full_value
      account.save!
      User.any_instance.stubs(password_expired?: true)
    end

    it "allows basic auth with the grandfathered feature" do
      Arturo.enable_feature! :allow_expired_passwords_for_api
      get "#{url}/api/v2/users/me.json", headers: api_basic_auth_headers(email, password)
      assert_equal user.id, json.fetch('user').fetch('id')
    end

    it "blocks access" do
      get "#{url}/api/v2/users/me.json", headers: api_basic_auth_headers(email, password)
      assert_response :unauthorized
      assert_equal 'PasswordExpired', json.fetch('error')
      assert_equal 'Your password has expired. Change your password to continue.', json.fetch('description')
    end

    it "allows basic auth via an api token" do
      get "#{url}/api/v2/users/me.json", headers: api_basic_auth_headers("#{email}/token", @api_token)
      assert_equal user.id, json.fetch('user').fetch('id')
    end
  end

  describe 'with a password not changed since 2016-11-01' do
    before do
      user.update_attribute(:created_at, Time.parse('2016-10-30'))
    end

    describe 'no password changes, but user created before 2016-11-01' do
      it 'blocks access' do
        get "#{url}/api/v2/users/me.json", headers: api_basic_auth_headers(email, password)
        assert_response :unauthorized
        assert_equal "Couldn't authenticate you", json["error"]
      end
    end

    describe 'last password change before 2016-11-01' do
      before do
        password_change = user.password_changes.create
        password_change.update_attribute(:created_at, Time.parse('2016-10-30'))
      end

      it 'blocks access' do
        get "#{url}/api/v2/users/me.json", headers: api_basic_auth_headers(email, password)
        assert_response :unauthorized
        assert_equal "Couldn't authenticate you", json["error"]
      end
    end

    describe 'last password change after 2016-11-01' do
      before do
        password_change = user.password_changes.create
        password_change.update_attribute(:created_at, Time.parse('2016-11-02'))
      end

      it 'allows access' do
        get "#{url}/api/v2/users/me.json", headers: api_basic_auth_headers(email, password)
        assert_equal user.id, json.fetch('user').fetch('id')
      end
    end
  end

  describe "with a valid password" do
    it "blocks authentication when 2FA is configured" do
      Zendesk::Auth::Otp::Phone.any_instance.stubs(send_message: true)
      user.one_time_password(time_based: false, phone: '555-555-5555').setup!
      user.otp_setting.update_attributes!(confirmed: true)

      get "#{url}/api/v2/tickets.json", headers: api_basic_auth_headers(email, password)
      assert_response :unauthorized
      assert_equal "Couldn't authenticate you", json["error"]
    end

    it "blocks authentication when 2FA is required" do
      account.settings.two_factor_enforce = true
      account.settings.save!
      refute user.otp_configured?

      get "#{url}/api/v2/tickets.json", headers: api_basic_auth_headers(email, password)
      assert_response :unauthorized
      assert_equal "Couldn't authenticate you", json["error"]
    end

    describe "when passwords are disabled" do
      before do
        account.role_settings.update_attributes!(agent_password_allowed: false, agent_zendesk_login: false)
      end

      it "blocks authentication" do
        get "#{url}/api/v2/tickets.json", headers: api_basic_auth_headers(email, password)
        assert_response :unauthorized
        assert_equal "Couldn't authenticate you", json["error"]
      end

      it "allows authentication with access_token_sso_bypass" do
        Arturo.enable_feature! :access_token_sso_bypass
        get "#{url}/api/v2/tickets.json", headers: api_basic_auth_headers(email, password)
        assert_response :success
      end
    end
  end
end
