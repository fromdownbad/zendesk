require_relative "../../support/test_helper"
require_relative "../../support/zendesk_dsl"

describe "Access SSO Bypass Integration Test", :integration do
  include ZendeskDSL
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:user) { account.owner }

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
  end

  describe "with access_token_sso_bypass" do
    before do
      account.role_settings.update_attributes!(agent_zendesk_login: false)
      Arturo.enable_feature! :access_token_sso_bypass
    end

    it "logs in the admin" do
      post "#{account.url(mapped: false, ssl: true)}/access/token", params: { email: user.email }

      body = ActionMailer::Base.deliveries.last.joined_bodies
      url  = body.match(/https:\/\/minimum.zendesk-test.com\/access\/access_token\/(.)+/)[0]

      get url

      visit "/api/v2/users/me.json"
      assert_equal user.id, JSON.parse(@response.body)['user']['id']
    end
  end

  describe "when agents can't use passwords" do
    before do
      account.role_settings.update_attributes!(agent_zendesk_login: false)
    end

    it "logs in the admin" do
      post "#{account.url(mapped: false, ssl: true)}/access/token", params: { email: user.email }

      body = ActionMailer::Base.deliveries.last.joined_bodies
      url  = body.match(/https:\/\/minimum.zendesk-test.com\/access\/access_token\/(.)+/)[0]

      get url

      visit "/api/v2/users/me.json"
      assert_equal user.id, JSON.parse(@response.body)['user']['id']
    end

    it "does not send an email if sso_bypass is disabled" do
      account.role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_DISABLED)
      post "#{account.url(mapped: false, ssl: true)}/access/token", params: { email: user.email }

      assert_empty ActionMailer::Base.deliveries
      assert_empty account.otp_tokens
    end

    it "does not send an email for an admin if sso_bypass is owner only" do
      account.role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ACCOUNT_OWNER)
      post "#{account.url(mapped: false, ssl: true)}/access/token", params: { email: users(:minimum_admin_not_owner).email }
      assert_empty ActionMailer::Base.deliveries
      assert_empty account.otp_tokens
    end

    it "does not send an email for an agent" do
      minimum_agent = users(:minimum_agent)
      stub_request(:get, "http://metropolis:8888/api/services/staff/#{minimum_agent.id}/entitlements").to_return(
        status: 200,
        body: { entitlements: { support: 'agent' } }.to_json,
        headers: {'Content-Type' => 'application/json'}
      )

      account.role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ADMINS)
      post "#{account.url(mapped: false, ssl: true)}/access/token", params: { email: minimum_agent.email }

      assert_empty ActionMailer::Base.deliveries
      assert_empty account.otp_tokens
    end
  end
end
