require_relative "../../support/test_helper"
require "zendesk/o_auth/testing/client_factory"
require "zendesk/testing/factories/global_uid"

class SetLocaleAfterAnonymousMobileSdkLoginTest < ActionDispatch::IntegrationTest
  fixtures :accounts

  describe "Set Locale after anonymous Mobile SDK login" do
    let(:account) { accounts(:minimum) }
    let!(:oauth_client) { FactoryBot.create(:client, account: account) }
    let!(:mobile_sdk_auth) { FactoryBot.create(:mobile_sdk_auth, account: account, client: oauth_client) }
    let!(:mobile_sdk_app) { FactoryBot.create(:mobile_sdk_app, account: account, mobile_sdk_auth: mobile_sdk_auth) }
    let(:access_url) { "#{account.url(mapped: false, ssl: true)}/access/sdk/anonymous" }
    let(:payload) { { user: { sdk_guid: SecureRandom.uuid, email: '', name: '' } }.to_json }
    let(:type) { Mime[:json].to_s }
    let(:headers) do
      {
        "HTTP_ACCEPT" => type,
        "CONTENT_TYPE" => type,
        "HTTP_REMOTE_ADDR" => "123.123.123.123",
        "HTTP_USER_AGENT" => "Zendesk SDK for iOS",
        "HTTP_CLIENT_IDENTIFIER" => oauth_client.identifier
      }
    end

    describe_with_arturo_disabled :sdk_auth_synchronous_user_locale do
      it "sets the user locale" do
        SetUserLocaleJob.expects(:enqueue)
        post access_url, params: payload, headers: headers
      end
    end
  end
end
