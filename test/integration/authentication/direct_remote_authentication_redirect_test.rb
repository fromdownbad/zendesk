require_relative "../../support/test_helper"

describe "Direct Remote Redirection Integration", :integration do
  fixtures :accounts, :users, :role_settings, :remote_authentications

  let(:account) { accounts(:minimum) }
  let!(:jwt_auth) do
    remote_authentications(:minimum_remote_authentication).tap do |auth|
      auth.auth_mode = RemoteAuthentication::JWT
      auth.is_active = true
      auth.remote_login_url = "https://sso.cpconsulting.dk/zendesk_remote_auth.asp"
      auth.save!
    end
  end

  let(:invalid_jwt_request) do
    {
      jwt: JSON::JWT.new(
        jti: "unique.request",
        iat: Time.now.to_i,
        name: 'bob smith',
        email: 'smith@example.com'
      ).sign('incorrect-key' * 30).to_s
    }.to_param
  end

  def set_sso_role_settings(agent: false, end_user: false)
    r = account.role_settings
    r.agent_zendesk_login = !agent
    r.agent_remote_login = agent

    r.end_user_zendesk_login = !end_user
    r.end_user_remote_login = end_user
    r.save!
  end

  def assert_redirected_to_sso
    assert_response :redirect
    assert response.location.start_with?(jwt_auth.remote_login_url), "was redirected #{response.location} instead of JWT endpoint"
  end

  def assert_redirected_to_google
    assert_response :redirect
    assert response.location.start_with?("https://accounts.google.com/o/oauth2/auth"), "was redirected #{response.location} instead of to Google"
  end

  def assert_redirected_to_microsoft
    assert_response :redirect
    assert response.location.start_with?("https://login.microsoftonline.com/common/oauth2"), "was redirected #{response.location} instead of to microsoft"
  end

  def assert_redirected_to_sign_in
    assert_response :redirect
    assert response.location.start_with?(account.url + '/access/unauthenticated'), "was redirected #{response.location} instead of the sign in page"
  end

  describe "when agents can use google" do
    before do
      account.role_settings.update_attributes!(agent_zendesk_login: false, agent_google_login: true)
    end

    it "redirects agent urls to google" do
      get account.authentication_domain + "/access/login?" + {return_to: '/agent'}.to_query
      assert_redirected_to_google
    end

    describe "when agents can use both zendesk and google auth" do
      before do
        account.role_settings.update_attributes!(agent_zendesk_login: true, agent_google_login: true)
      end

      it "does not redirect to Google when accessing an agent urls" do
        get account.authentication_domain + "/access/login?" + {return_to: '/agent'}.to_query
        assert_redirected_to_sign_in
      end
    end
  end

  describe "when end users can use google" do
    before do
      account.role_settings.update_attributes!(end_user_zendesk_login: false, end_user_google_login: true)
    end

    it "redirects unauthenticated requests to login" do
      get account.authentication_domain + "/users"
      assert_redirected_to_sign_in
    end

    describe "when end users can use both zendesk and google auth" do
      before do
        account.role_settings.update_attributes!(end_user_zendesk_login: true, end_user_google_login: true)
      end

      it "redirects unauthenticated requests to login" do
        get account.authentication_domain + "/users"
        assert_redirected_to_sign_in
      end
    end
  end

  describe "when agents can use office 365" do
    before do
      account.role_settings.update_attributes!(agent_zendesk_login: false, agent_office_365_login: true)
    end

    it "redirects agent urls to microsoft" do
      get account.authentication_domain + "/access/login?" + {return_to: '/agent'}.to_query
      assert_redirected_to_microsoft
    end

    describe "when agents can use both zendesk and microsoft auth" do
      before do
        account.role_settings.update_attributes!(agent_zendesk_login: true, agent_office_365_login: true)
      end

      it "does not redirect to microsoft when accessing an agent urls" do
        get account.authentication_domain + "/access/login?" + {return_to: '/agent'}.to_query
        assert_redirected_to_sign_in
      end
    end
  end

  describe "when end users can use microsoft" do
    before do
      account.role_settings.update_attributes!(end_user_zendesk_login: false, end_user_office_365_login: true)
    end

    it "redirects unauthenticated requests to login" do
      get account.authentication_domain + "/users"
      assert_redirected_to_sign_in
    end

    describe "when end users can use both zendesk and microsoft auth" do
      before do
        account.role_settings.update_attributes!(end_user_zendesk_login: true, end_user_office_365_login: true)
      end

      it "redirects unauthenticated requests to login" do
        get account.authentication_domain + "/users"
        assert_redirected_to_sign_in
      end
    end
  end

  describe "when only agents can use remote auth" do
    before { set_sso_role_settings(agent: true, end_user: false) }

    it "redirects to SSO when accessing an agent url" do
      # CRUFT: because lotus doens't use real urls
      get account.authentication_domain + "/access/login?" + {return_to: '/agent'}.to_query
      assert_redirected_to_sso
    end

    it "redirects /access/sso to SSO" do
      get account.authentication_domain + "/access/sso"
      assert_redirected_to_sso
    end

    it "redirect unauthenticated requests to sign in page" do
      get account.authentication_domain + "/users"
      assert_redirected_to_sign_in
    end

    it "does not redirect back to JWT after an invalid JWT attempt" do
      get account.authentication_domain + '/access/jwt?' + invalid_jwt_request
      assert_redirected_to_sign_in
    end

    describe "when agents can use both zendesk and remote auth" do
      before do
        account.role_settings.update_attributes!(agent_zendesk_login: true, agent_remote_login: true)
      end

      it "does not redirect to SSO when accessing an agent url" do
        get account.authentication_domain + "/access/login?" + {return_to: '/agent'}.to_query
        assert_redirected_to_sign_in
      end
    end
  end

  describe "when only end users can use remote auth" do
    before { set_sso_role_settings(agent: false, end_user: true) }

    it "does not redirect to SSO when accessing an agent url" do
      get account.authentication_domain + "/access/login?" + {return_to: '/agent'}.to_query
      assert_redirected_to_sign_in
    end

    it "redirects /access/sso to SSO" do
      get account.authentication_domain + "/access/sso"
      assert_redirected_to_sso
    end

    it "redirects unauthenticated requests to SSO" do
      get account.authentication_domain + "/users"
      assert_redirected_to_sso
    end

    it "does not redirect back to JWT after an invalid JWT attempt" do
      get account.authentication_domain + '/access/jwt?' + invalid_jwt_request
      assert_redirected_to_sign_in
    end

    describe "when end users can use both zendesk and remote auth" do
      before do
        account.role_settings.update_attributes!(end_user_zendesk_login: true, end_user_remote_login: true)
      end

      it "redirects unauthenticated requests to SSO" do
        get account.authentication_domain + "/users"
        assert_redirected_to_sso
      end
    end
  end

  describe "when both agents and end users can use remote auth" do
    before { set_sso_role_settings(agent: true, end_user: true) }

    it "redirects to SSO when accessing an agent url" do
      get account.authentication_domain + "/access/login?" + {return_to: '/agent'}.to_query
      assert_redirected_to_sso
    end

    it "redirects /access/sso to SSO" do
      get account.authentication_domain + "/access/sso"
      assert_redirected_to_sso
    end

    it "redirects unauthenticated requests to SSO" do
      get account.authentication_domain + "/users"
      assert_redirected_to_sso
    end

    it "does not redirect back to JWT after an invalid JWT attempt" do
      get account.authentication_domain + '/access/jwt?' + invalid_jwt_request
      assert_redirected_to_sign_in
    end

    describe "when both agents and end users can use both zendesk and remote auth" do
      before do
        account.role_settings.update_attributes!(agent_zendesk_login: true, agent_remote_login: true, end_user_zendesk_login: true, end_user_remote_login: true)
      end

      it "redirects to SSO when accessing an agent url" do
        get account.authentication_domain + "/access/login?" + {return_to: '/agent'}.to_query
        assert_redirected_to_sso
      end

      it "redirects unauthenticated requests to SSO" do
        get account.authentication_domain + "/users"
        assert_redirected_to_sso
      end
    end
  end
end
