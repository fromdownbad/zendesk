require_relative "../../support/test_helper"
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

describe "Mobile Login Controller Integration", :integration do
  fixtures :accounts, :brands, :users, :role_settings, :remote_authentications

  describe "LoginController" do
    let(:account) { accounts(:minimum) }
    let(:user) { account.owner }

    let(:params) do
      {
        device: {
          name: "Chris's iPhone",
          identifier: '123456789'
        },
        client_id: 'zendesk_iphone'
      }
    end

    before do
      account.is_ssl_enabled = true
      account.default_brand = account.brands.first
      account.save!
    end

    def assert_device_cookies
      cookies = response.headers["Set-Cookie"].lines.map(&:strip)

      assert cookies.include?("_zendesk_client_identifier=zendesk_iphone; path=/; secure; HttpOnly; SameSite=None"), "expected _zendesk_client_identifier to have been set"
      assert cookies.include?("_zendesk_mobile_device=eyJuYW1lIjoiQ2hyaXMncyBpUGhvbmUiLCJpZGVudGlmaWVyIjoiMTIzNDU2Nzg5In0%3D; path=/; secure; HttpOnly; SameSite=None"), "expected _zendesk_mobile_device to have been set"
      assert cookies.detect { |c| c.start_with?("_zendesk_test_Session=") }.present?, "expected rails session to have been set"
    end

    def refute_device_cookies
      cookies = response.headers["Set-Cookie"].lines.map(&:strip)

      refute cookies.include?("_zendesk_client_identifier="), "expected _zendesk_client_identifier to not have have been set"
      refute cookies.include?("_zendesk_mobile_device="), "expected _zendesk_mobile_device to not have have been set"
    end

    def enable_jwt!
      auth                  = account.remote_authentications.build
      auth.auth_mode        = RemoteAuthentication::JWT
      auth.is_active        = true
      auth.remote_login_url = "https://sso.cpconsulting.dk/zendesk_remote_auth.asp"
      auth.token            = SecureRandom.hex(32)
      auth.save!

      account.role_settings.agent_remote_login = true
      account.role_settings.end_user_remote_login = true

      account.role_settings.save!

      auth
    end

    def enable_saml!
      Account.any_instance.stubs(has_saml?: true)
      auth                  = account.remote_authentications.build
      auth.fingerprint      = "44:D2:9D:98:49:66:27:30:3A:67:A2:5D:97:62:31:65:57:9F:57:D1"
      auth.is_active        = true
      auth.remote_login_url = "https://idp.example.org/saml"
      auth.auth_mode        = RemoteAuthentication::SAML
      auth.save!

      account.role_settings.agent_remote_login = true
      account.role_settings.end_user_remote_login = true

      account.role_settings.save!

      auth
    end

    def enable_google!
      account.role_settings.agent_google_login = true
      account.role_settings.save!
    end

    def enable_microsoft!
      account.role_settings.agent_office_365_login = true
      account.role_settings.save!
    end

    describe "#google" do
      describe "when not allowed" do
        before do
          get account.url + "/auth/api/mobile/login/google", params: params
          assert_response :bad_request
        end

        it "does set any cookies" do
          refute_device_cookies
        end
      end

      describe "when configured" do
        before do
          enable_google!
          get account.url + "/auth/api/mobile/login/google", params: params
          assert_response :redirect
        end

        it "redirects to google" do
          assert response.location.start_with?("https://accounts.google.com/o/oauth2/auth")
        end

        it "sets the cookies" do
          assert_device_cookies
        end
      end
    end

    describe "#office_365" do
      describe "when not allowed" do
        before do
          get account.url + "/auth/api/mobile/login/office_365", params: params
          assert_response :bad_request
        end

        it "does set any cookies" do
          refute_device_cookies
        end
      end

      describe "when configured" do
        before do
          enable_microsoft!
          get account.url + "/auth/api/mobile/login/office_365", params: params
          assert_response :redirect
        end

        it "redirects to microsoft" do
          assert response.location.start_with?("https://login.microsoftonline.com/common/oauth2/v2.0/authorize"), response.location
        end

        it "sets the cookies" do
          assert_device_cookies
        end
      end
    end

    describe "#sso" do
      describe "when SSO is not allowed" do
        before do
          get account.url + "/auth/api/mobile/login/sso", params: params
          assert_response :bad_request
        end

        it "does set any cookies" do
          refute_device_cookies
        end
      end

      describe "with a JWT server" do
        before do
          enable_jwt!
          get account.url + "/auth/api/mobile/login/sso", params: params
          assert_response :redirect
        end

        it "redirects to the JWT server" do
          assert response.location.starts_with?(account.remote_authentications.active.first.remote_login_url)
        end

        it "sets the cookies" do
          assert_device_cookies
        end
      end

      describe "with a SAML server" do
        before do
          enable_saml!
          get account.url + "/auth/api/mobile/login/sso", params: params
          assert_response :redirect
        end

        it "redirects to the SAML server" do
          assert response.location.starts_with?(account.remote_authentications.active.first.remote_login_url), 'expected redirect to SAML IDP'
        end

        it "sets the cookies" do
          assert_device_cookies
        end
      end

      describe "with both" do
        before do
          enable_saml!
          enable_jwt!
          get account.url + "/auth/api/mobile/login/sso", params: params
          assert_response :redirect
        end

        it "redirects to the SAML server" do
          assert response.location.starts_with?(account.remote_authentications.active.first.remote_login_url), 'expected redirect to SAML IDP'
        end

        it "sets the cookies" do
          assert_device_cookies
        end
      end
    end
  end
end
