require_relative "../support/test_helper"

ActionDispatch::IntegrationTest.class_eval do
  def assert_user_sees(content)
    if content.is_a?(Regexp)
      assert_match content, response.body
    else
      assert_includes response.body, content.to_s, "Expected body to include #{content}"
    end
  end
end
