require_relative "../support/test_helper"
require_relative "../support/authentication_integration_test_helper"

class GoogleLoginTest < ActionDispatch::IntegrationTest
  include AuthenticationIntegrationTestHelper

  fixtures :all

  before do
    @account = accounts(:minimum)
    @role_settings = @account.build_role_settings
    @role_settings.end_user_google_login = true
    @role_settings.agent_facebook_login = true
    @role_settings.save!
  end

  describe 'with an expired session' do
    before do
      # Establish a session
      login(users(:minimum_agent))

      # Expire the session
      Zendesk::AuthenticatedSession.any_instance.stubs(stale?: true)
      get @account.url(mapped: false, ssl: true) + '/access/google?google_domain=whatever.com&profile=google'
    end

    it "properly set state" do
      state = Rack::Utils.parse_query(headers['Location'].split('?', 2).last)['state']
      decoded_state = JWT.decode(state, Zendesk::Configuration.dig!(:google, :key)).first
      assert_not_equal decoded_state['state'], Zendesk::Auth::Warden::GoogleOAuth2Strategy.state({}, Zendesk::Configuration.dig!(:google, :key))
    end
  end

  describe "without host mapping" do
    before do
      get @account.url(mapped: false, ssl: true) + '/auth/v2/login/signin'
    end

    it "is redirecting to google oauth2" do
      assert_select('a.service.google') do |elements|
        assert_equal 1, elements.count
        href = elements.first.attributes['href'].value
        assert href.start_with?(Zendesk::Configuration.dig!(:google, :site)),
          "link to #{href}, not #{Zendesk::Configuration.dig!(:google, :site)}"
      end
    end
  end
end
