require_relative "../support/test_helper"
require_relative "../support/attachment_test_helper"

require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

class Zendesk::OAuth::V1Test < ActionDispatch::IntegrationTest
  fixtures :all

  before do
    Arturo.enable_feature! :attachments_on_separate_domain
    @account = accounts(:minimum)
    @user = users(:minimum_agent)
    @client = FactoryBot.create(:client, account: @account, user: @user)

    @host = "https://#{@account.host_name}"

    host! @host
    https!
  end

  let(:json) { JSON.parse(response.body) }

  it "serves v1" do
    token = FactoryBot.create(:token, user: @user, account: @account, client: @client)
    authorization = { "HTTP_AUTHORIZATION" => "Bearer #{token.token(:full)}" }
    get "#{@host}/api/v1/stats/graph_data/account/0/ticket_stats_by_account/solve_count", params: { duration: 30.days.to_i }, headers: authorization

    assert_response :ok
  end

  it "allows tokens in a non-standard format" do
    # https://zendesk.atlassian.net/browse/SECDEV-1418
    token = FactoryBot.create(:token, user: @user, account: @account, client: @client).token(:full)
    encoded = Base64.strict_encode64("x_oauth_via_basic:#{token}")
    authorization = { "HTTP_AUTHORIZATION" => "Basic #{encoded}" }

    get "#{@host}/api/v2/users/me.json", headers: authorization
    assert_response :ok
    assert_equal json.fetch('user').fetch('id'), @user.id
  end

  it "serves attachments" do
    @account.settings.private_attachments = true
    @account.settings.save!

    token = FactoryBot.create(:token, user: @user, account: @account, client: @client)
    authorization = { "HTTP_AUTHORIZATION" => "Bearer #{token.token(:full)}" }

    attachment = create_attachment(Rails.root.join('test', 'files', 'normal_1.jpg'), @user)
    attachment.source = events(:create_comment_for_minimum_ticket_1)
    attachment.save!

    get "#{@host}/attachments/token/#{attachment.token}", headers: authorization

    assert_response :redirect
    assert_valid_attachment_domain_url(response.location)
  end

  it "does not find assets" do
    token = FactoryBot.create(:token, user: @user, account: @account, client: @client)
    authorization = { "HTTP_AUTHORIZATION" => "Bearer #{token.token(:full)}" }

    get "#{@host}/cassets/system/image.png", headers: authorization
    # Just needs to not be a 302 to /access/unauthenticated
    assert_response :not_found
  end

  it "rates limit invalid authentication" do
    Timecop.freeze
    Prop.throttle!(:api_oauth_attempt, [@account.id, '127.0.0.1'], increment: 10)
    authorization = { "HTTP_AUTHORIZATION" => "Bearer invalid-token" }
    get "#{@host}/api/v2/tickets.json", headers: authorization

    assert_response 429
    assert_not_nil @response.headers['Retry-After']
  end
end
