require_relative "../support/test_helper"

describe 'Helper' do
  class Global
    class << self
      attr_reader :setting
    end

    class << self
      attr_writer :setting
    end
  end

  describe "revert" do
    it "always reset a value changed within a block to its original state" do
      original_state = Global.setting = Object.new

      revert('Global.setting') { Global.setting = 'changed' }
      assert_equal original_state, Global.setting

      assert_raise(RuntimeError) do
        revert('Global.setting') { Global.setting = 'changed' && raise }
      end
      assert_equal original_state, Global.setting
    end
  end

  describe "fake_translation_for_key" do
    it "returns the specified string for I18n.t" do
      desired_key = 'txt.foo.bar.baz.stuff'
      desired_string = 'This is the value that should be returned'

      with_fake_translation(desired_key, desired_string) do
        I18n.t(desired_key).must_equal desired_string
      end
    end

    it "only modifies the key within the block" do
      desired_key = 'txt.foo.bar.baz.stuff'
      desired_string = 'This is the value that should be returned'

      with_fake_translation(desired_key, desired_string) do
        I18n.t(desired_key).must_equal desired_string
      end

      I18n.t(desired_key).wont_equal desired_string
    end
  end
end
