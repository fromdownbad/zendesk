require_relative "../support/test_helper"

describe '.sanitize' do
  describe 'sanitized_allowed_attributes' do
    let(:input) { '<a href="/users/5" data-method="delete" rel="nofollow" target="_blank" style="width: 720px;" data-confirm="Are you sure?" disallowed="asdf">Delete</a>' }

    it 'allows desired attributes' do
      expected = '<a href="/users/5" data-method="delete" rel="nofollow" target="_blank" style="width: 720px;" data-confirm="Are you sure?">Delete</a>'

      assert_equal expected, input.sanitize
    end
  end

  describe 'sanitized_allowed_tags' do
    let(:input) { '<disallowed><table><thead><tr>The table header</tr></thead><tbody><tr><td>The table body</td><td>with two columns</td></tr></tbody></table>' }

    it 'allows desired tags' do
      expected = '<table><thead><tr>The table header</tr></thead><tbody><tr><td>The table body</td><td>with two columns</td></tr></tbody></table>'

      assert_equal expected, input.sanitize
    end
  end
end
