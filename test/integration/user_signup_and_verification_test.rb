require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"
require "zendesk/inbound_mail"

class UserSignupAndVerification < ActionDispatch::IntegrationTest
  include MailTestHelper
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:mail) { FixtureHelper::Mail.read("minimum_ticket.json") }

  before do
    https!
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    Account.any_instance.stubs(:has_public_forums?).returns(false)
    InboundEmail.without_arsi.delete_all
  end

  it "tests reject" do
    account.restrict_help_center!
    account.update_attribute(:is_signup_required, true)
    mail = FixtureHelper::Mail.read("reject_empty_1.json")
    assert_difference('account.suspended_tickets.count(:all)', 1) do
      process_mail(mail, account: account)
    end
    assert_equal 0, ActionMailer::Base.deliveries.length
  end

  it "tests email signup and verify" do
    account.restrict_help_center!
    account.settings.web_portal_state = :restricted
    account.save!
    account.update_attribute(:is_signup_required, true)
    valid_data = get_valid_verify_ticket_parameters
    suspended_count = account.suspended_tickets.size

    # Submit ticket from unregistered user
    stub_mail_from Zendesk::Mail::Address.new(name: "joe newuser", address: "new_user@joe.com")
    process_mail(mail, account: account)

    assert_equal 0, ActionMailer::Base.deliveries.length
    assert_equal (suspended_count + 1), account.suspended_tickets.size # Blacklisted

    # Email from user in whitelist with account that does not allow anonymous requests
    assert accounts(:minimum).is_signup_required
    stub_mail_from Zendesk::Mail::Address.new(name: "joe newuser", address: "new_user@aghassipour.com")
    mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id
    process_mail(mail, account: account)

    body = ActionMailer::Base.deliveries[0].joined_bodies # Get body of sent email
    id = body.match(/verification\/ticket\/(\s*|\s*")(\w+)(\D*)\//)[2] # Get verify token

    open_session do |sess|
      sess.extend(ZendeskDSL)
      sess.host = accounts(:minimum).host_name
      # Get wrong token
      sess.get "/verification/ticket/123"
      sess.assert_redirected_to '/access/help'
      # Get correct token
      sess.get "/verification/ticket/#{id}"
      sess.assert_response :success
      # Put correct token but invalid email
      valid_data[:user][:password] = 'a'
      user = account.find_user_by_email('new_user@aghassipour.com')
      sess.post "/verification/details", valid_data.merge(token: user.identities.email.first.verification_tokens.first.value)
      sess.assert_response :success
      sess.assert_select '.notification-error' # Incorrect password
      # Finally, put correct token
      valid_data[:user][:password] = 'juletrae88'
      sess.post "/verification/details", valid_data.merge(token: user.identities.email.first.verification_tokens.first.value)
      sess.redirected_to_hc?
    end

    new_user = account.find_user_by_email('new_user@aghassipour.com')
    assert new_user.is_verified?
    assert_equal organizations(:minimum_organization1), new_user.organization # Routed to organization according to default mapping

    # Mail has been sent - to alert the user that the ticket has been received, via trigger
    # 1 welcome mail + 2 ticket notifications
    assert_equal 3, ActionMailer::Base.deliveries.length

    # Set whitelist to empty
    account.domain_whitelist = ''
    account.domain_blacklist = nil
    account.save

    # Email from arbitrary user...
    stub_mail_from Zendesk::Mail::Address.new(name: "joe newuser", address: "new_user@joe.com")
    mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id
    process_mail(mail, account: account)

    body = ActionMailer::Base.deliveries.last.joined_bodies # Get body of sent email
    id = body.match(/verification\/ticket\/(\s*|\s*")(\w+)(\D*)\//)[2] # Get verify token

    open_session do |sess|
      sess.extend(ZendeskDSL)
      sess.host = accounts(:minimum).host_name
      # Get token
      sess.get "/verification/ticket/#{id}"
      sess.assert_response :success
      # Put token
      user = account.find_user_by_email('new_user@joe.com')
      sess.post "/verification/details", valid_data.merge(token: user.identities.email.first.verification_tokens.first.value)
      sess.redirected_to_hc?
    end
  end

  it "tests web signup and verify" do
    account.update_attribute(:domain_blacklist, '*')
    account.update_attribute(:domain_whitelist, 'hogwarts.edu')
    account.enable_help_center!

    # Signup user with email in whitelist
    open_session do |sess|
      sess.extend(ZendeskDSL)
      sess.host = accounts(:minimum).host_name

      # Goto signup page
      sess.get "https://minimum.zendesk-test.com/auth/v2/login/successful_registration"
      sess.assert_response :success

      # Fill out form with invalid data
      invalid_data = { user: { name: 'harry', email: 'cornelius@magic.gov' } }
      sess.post "/registration/register", invalid_data
      sess.is_redirected_to 'auth/sso_v2_index'
      sess.assert_select '.notification-error' # Incorrect email - domain not in whitelist

      # Fill out form with valid data
      valid_data = { user: { name: 'harry', email: 'harry@hogwarts.edu' } }
      sess.post "/registration/register", valid_data
      sess.headers["Location"].must_include('/registration/success')
      sess.is_redirected_to 'auth/sso_v2_index'

      # Read email
      body = ActionMailer::Base.deliveries[0].joined_bodies # Get body of sent email
      id = body.match(/verification\/email\/(\s*|\s*")(\w+)(\D*)\//)[2] # Get verify token

      # Goto verify link, should be shown create password page but not verified yet
      sess.get "/verification/email/#{id}"
      sess.assert_template 'auth/sso_v2_index'
      refute account.find_user_by_email('harry@hogwarts.edu').is_verified?

      # Set password, should be logged in and verified
      sess.post "/verification/details", token: id, user: { password: 'anewpassword' }
      sess.redirected_to_hc?
      assert account.find_user_by_email('harry@hogwarts.edu').is_verified?
    end

    # Set whitelist to empty
    account.domain_whitelist = ''
    account.domain_blacklist = ''
    account.save

    # Signup user with arbitrary email...
    open_session do |sess|
      sess.extend(ZendeskDSL)
      sess.host = accounts(:minimum).host_name

      # Fill out form
      valid_data = { user: { name: 'joe', email: 'newuser@joe.com' } }
      sess.post "/registration/register", valid_data
      sess.headers["Location"].must_include('/registration/success')
      sess.is_redirected_to 'auth/sso_v2_index'

      # Read email
      body = ActionMailer::Base.deliveries[1].joined_bodies # Get body of sent email
      id = body.match(/verification\/email\/(\s*|\s*")(\w+)(\D*)\//)[2] # Get verify token

      # Goto verify link, should be shown create password page but not verified yet
      sess.get "/verification/email/#{id}"
      sess.assert_template 'auth/sso_v2_index'
      refute account.find_user_by_email('newuser@joe.com').is_verified?

      # Set password, should be logged in and verified
      sess.post "/verification/details", token: id, user: { password: 'anewpassword' }
      sess.redirected_to_hc?
      assert account.find_user_by_email('newuser@joe.com').is_verified?
    end
  end

  it "tests suspended attachments" do
    stub_request(:put, %r{\.amazonaws\.com/data/attachments/.*})

    # Signup is required for this account
    account.restrict_help_center!
    account.update_attribute(:is_signup_required, true)

    # A new user sends an email with attachments
    suspended_count = SuspendedTicket.count(:all)
    ticket_count = Ticket.count(:all)
    valid_data = get_valid_verify_ticket_parameters

    mail = FixtureHelper::Mail.read('html_text_in_body_email_with_attachments.json')
    mail.subject = 'No ticket id'
    stub_mail_from Zendesk::Mail::Address.new(name: "joe newuser", address: "new_user@gmail.com")

    process_mail(mail, account: account)

    # A suspended ticket because of signup required must be created
    assert_equal (suspended_count + 1), SuspendedTicket.count(:all)
    assert_equal ticket_count, Ticket.count(:all)
    suspended = SuspendedTicket.last
    assert_equal 'new_user@gmail.com', suspended.from_mail
    assert_equal SuspensionType.SIGNUP_REQUIRED, suspended.cause
    assert_equal 2, suspended.attachments.size

    open_session do |sess|
      sess.extend(ZendeskDSL)
      sess.host = accounts(:minimum).host_name

      # New user goes to verify link
      sess.get "/verification/ticket/#{suspended.token}"
      sess.assert_response :success

      valid_data[:user][:password] = 'juletrae88'
      user = account.find_user_by_email('new_user@gmail.com')
      sess.post "/verification/details", valid_data.merge(token: user.identities.email.first.verification_tokens.first.value)
      sess.redirected_to_hc?
    end

    # should create a new ticket from the suspended ticket
    assert_equal (ticket_count + 1), Ticket.count(:all)
    assert_equal suspended.send(:full_content), Ticket.last.description
    assert_equal suspended.send(:full_content), Ticket.last.comments.first.body
    assert_equal 2, Ticket.last.comments.first.attachments.size

    # and delete the suspended ticket
    assert_equal suspended_count, SuspendedTicket.count(:all)
    assert_nil SuspendedTicket.where(id: suspended.id).first
  end

  describe "email verification" do
    describe "on account with remote authentication" do
      let(:integration) { remote_authentications(:minimum_remote_authentication) }
      before do
        integration.is_active = true
        integration.auth_mode = RemoteAuthentication::JWT
        integration.remote_login_url = "https://sso.cpconsulting.dk/zendesk_remote_auth.asp"
        integration.save!

        role_settings = account.role_settings
        role_settings.agent_zendesk_login = false
        role_settings.end_user_zendesk_login = false
        role_settings.agent_remote_login = true
        role_settings.end_user_remote_login = true
        role_settings.save!

        valid_data = valid_user_signup_parameters
        valid_data[:user].delete(:password)

        user = account.all_users.create!(valid_data[:user])
        @token = user.identities.email.first.verification_tokens.create!(source: user.identities.email.first)

        assert(@token)
      end

      describe "enabled on the current IP" do
        before do
          visit("http://minimum.zendesk-test.com/verification/email/#{@token.value}")
        end

        it "redirects to remote auth as that will verify the user" do
          assert(redirect?)
          assert_match(/^#{Regexp.escape(integration.remote_login_url)}/, headers["Location"])
        end
      end

      describe "disabled on the current IP" do
        before do
          integration.ip_ranges = '123.123.876.876'
          integration.save!

          visit("http://minimum.zendesk-test.com/verification/email/#{@token.value}")
        end

        it "asks for a password" do
          contain 'https://minimum.zendesk-test.com/auth/v2/host.js'
        end
      end
    end
  end

  private

  def get_valid_verify_ticket_parameters # rubocop:disable Naming/AccessorMethodName
    {
      user: { name: 'Ricky Gervais', password: 'juletrae88' }
    }
  end
end
