require_relative "../support/test_helper"

class SettingsTest < ActionDispatch::IntegrationTest
  fixtures :all

  let(:account) { accounts(:minimum) }

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:products).returns([])
    Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
    assert ActionController::Base.perform_caching
    login(account.owner)
  end

  it 'works for settings/api' do
    setting_set(api_token_access: true)

    put "#{account.url(ssl: true)}/settings/api/update_settings", params: { account: {
      settings: { api_token_access: true }
    } }

    # 200 = error
    assert_no_match /Name has already been taken/, flash[:error].to_s.strip_tags
    assert_response :redirect
  end

  it 'works for api/v2/account/settings' do
    setting_set(ticket_delete_for_agents: true)

    put "#{account.url(ssl: true)}/api/v2/account/settings", params: { settings: {
      tickets: { agent_ticket_deletion: true }
    } }

    # 422 = error
    assert_response :ok
  end

  it 'works for api/v2/users/settings' do
    setting_set(model: account.owner, show_user_assume_tutorial: false)
    # cache is now invalid

    put "#{account.url(ssl: true)}/api/v2/users/me/settings", params: { settings: {
      lotus: { show_user_assume_tutorial: false }
    } }

    # 422 = error
    assert_response :ok
  end

  private

  def setting_set(model: account, **settings)
    Kasket.cache.clear

    # Force a cache
    model.settings.to_a

    original_cache = Kasket.cache
    Kasket.cache_store = TestMemoryStore.new

    model.settings.set(settings)
    model.settings.save!

    Kasket.cache_store = original_cache
  end
end
