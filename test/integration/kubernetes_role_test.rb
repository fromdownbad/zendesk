require_relative '../support/test_helper'

describe 'Kubernetes configuration' do
  let(:ignored_files) do
    %w[
      kubernetes/config
      kubernetes/app-server-hpa.yml
    ]
  end

  let(:output_files) do
    Dir['kubernetes/*.yml'] - ignored_files
  end

  it 'includes updated generation results' do
    actual_checksums = output_files.each_with_object({}) do |f, h|
      h[f] = Digest::SHA256.digest(File.read(f))
    end

    Zendesk::KubernetesFileGeneration.run

    expected_checksums = output_files.each_with_object({}) do |f, h|
      h[f] = Digest::SHA256.digest(File.read(f))
    end

    assert_equal expected_checksums.keys, actual_checksums.keys

    mismatches = actual_checksums.reject { |file, cs| cs == expected_checksums[file] }.keys

    assert_empty(mismatches, "Checksum mismatch(es) for #{mismatches.join(', ')}. Run `rake k8s` to update the output files.")
  end
end
