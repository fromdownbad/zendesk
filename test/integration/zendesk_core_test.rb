require_relative "../support/test_helper"

describe 'ZendeskCore' do
  describe "Zendesk::Core" do
    it "uses all models from core" do
      core_lib = $LOAD_PATH.grep(/zendesk_core-/).first
      files_in_core = Dir["#{core_lib}/zendesk/models/**/**.rb"]
      models_in_core = files_in_core.select { |f| File.read(f) =~ /^class .* < / } # real models and inherited models
      models_in_core.map! { |m| m.split("/models/").last.sub(".rb", "") }

      # ignore models we do not have in classic, but check first
      models_in_core.delete("country") unless File.exist?("app/models/country.rb")
      models_in_core.delete("channels_brand") unless File.exist?("app/models/channels_brand.rb")

      # found all ?
      assert_includes models_in_core, "user"
      assert_includes models_in_core, "user_twitter_identity"

      # all required and loadable ?
      models_in_core.each do |model|
        assert_match %r{require ['"]zendesk/models/#{model}['"]}, File.read("app/models/#{model}.rb")[0..200]
        require model
      end
    end
  end
end
