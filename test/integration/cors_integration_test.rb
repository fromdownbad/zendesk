require_relative "../support/test_helper"

class CORSIntegrationTest < ActionDispatch::IntegrationTest
  describe "CORS" do
    before do
      @account = accounts(:minimum)
    end

    describe "enabled API endpoint" do
      before do
        options "#{@account.url}/api/v2/tickets.json",
          headers: {
            'HTTP_ORIGIN' => "http://www.example.com",
            'HTTP_ACCESS_CONTROL_REQUEST_METHOD' => 'GET'
          }
      end

      it "is successful" do
        assert_response :success
      end

      it "is allowed" do
        assert_equal "*", response.headers["Access-Control-Allow-Origin"]
      end
    end

    it "raises a routing error" do
      options "#{@account.url}/api/v1/tickets.json",
        headers: {
          'HTTP_ORIGIN' => "http://www.example.com",
          'HTTP_ACCESS_CONTROL_REQUEST_METHOD' => 'GET'
        }

      assert_response :not_found
    end
  end
end
