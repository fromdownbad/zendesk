#!/usr/bin/env ruby

# This is a funky hack to prevent this test from running as part of
# the normal suite but run locally without env vars.
#
# https://docs.github.com/en/free-pro-team@latest/actions/reference/environment-variables

if ENV["CI"] && ENV["GITHUB_WORKFLOW"] !~ /validate/i
  warn "CI environment detected. Skipping cleanliness test."
  exit 0
end

require_relative "../support/test_helper"
require './config/pre-commit'

describe 'Cleanliness' do
  let_all(:test_files) { Dir["test/**/*_test.rb"].map { |file| [file, File.read(file)] } }
  let_all(:controllers) { Dir["app/controllers/**/*.rb"].map { |f| [f, File.read(f)] } }
  let_all(:models) { Dir["app/models/**/*.rb"].map { |f| [f, File.read(f)] } }

  def load_all_api_v2_controllers
    Dir["app/controllers/api/v2*/**/*.rb"].each { |f| require f.sub('app/controllers/', '').sub('.rb', '') }
  end

  def load_all_controllers
    Dir["app/controllers/**/*.rb"].each { |f| require f.sub('app/controllers/', '').sub('.rb', '') }
  end

  def actions(controller)
    actions = controller.action_methods.to_a.reject { |action| action =~ /^(_conditional_callback_around_|_callback_before_|_callback_around_|url_options)/ }
    actions.delete("flash")
    actions.map(&:to_sym)
  end

  it "avoids to when defining routes" do
    route_files = Dir["config/{routes,api_routes}.rb"].map { |f| [f, File.read(f)] }
    failures = route_files.map { |f, c| f if c =~ /(:to ?=>)|([^\S]to:)/i }.compact
    assert_equal [], failures, "Please use :controller and :action rather than :to => 'controller#action' for routes"
  end

  it "calls JobWithStatus.perform_now instead of JobWithStatus.perform" do
    failures = controllers.map { |f, c| f if c =~ /\.perform\(nil,.*?\)/ }.compact
    assert_equal [], failures
  end

  it "does not use def setup/teardown since they will not be pure-minitest compatible" do
    failures = test_files.map { |f, c| f if c =~ /^\s+def (setup|teardown)\s*$/ }.compact
    assert_equal [], failures
  end

  it "does not extend from ActiveSupport::TestCase" do
    failures = test_files.map { |f, c| f if c =~ / < ActiveSupport::TestCase$/ }.compact
    assert_equal [], failures
  end

  it "does not def on the top-level" do
    failures = test_files.map { |f, c| f if c =~ /^def / }.compact
    failures.must_equal []
  end

  it "does not use def test_ since they are heard to read" do
    failures = test_files.map { |f, c| f if c =~ /^\s+def test[a-zA-Z_\d]$/ }.compact
    assert_equal [], failures
  end

  it "uses assert_raise instead of inline rescue" do
    failures = test_files.map { |f, c| f if c =~ /\w rescue \w+/ }.compact - ['test/models/jobs/restore_default_content_job_test.rb']
    assert_equal [], failures
  end

  it "does not use set constants or @@ since they will be global once converted to minitest" do
    # constants can be converted to self::XXX = 1 and read via self.class::XXX
    failures = test_files.
      select { |_f, c| c =~ /^\s+([A-Z][A-Za-z_\d]+|@@\S+) = / }. # defines constants
      reject { |_f, c| c =~ /^\s*class / }.map(&:first) # not inside a class (not perfect, could still be somewhere outside a class)
    assert_equal [], failures
  end

  it 'does not use deprecated matchers in tests' do
    failures = test_files.map do |file, content|
      if file == "test/integration/cleanliness_test.rb"
        nil
      elsif %w[assert_raise_with_message assert_not_match].any? { |m| content.include?(m) }
        file
      elsif /assert_include[^s]/.match?(content)
        file
      end
    end.compact
    assert_equal [], failures
  end

  it 'does not use assert_tag and assert_no_tag in tests' do
    failures = test_files.map { |f, c| f if c =~ /assert_tag/ && f != "test/integration/cleanliness_test.rb" }.compact
    assert_equal failures, [], 'Please replace assert_tag with assert_select.'

    failures = test_files.map { |f, c| f if c =~ /assert_no_tag/ && f != "test/integration/cleanliness_test.rb" }.compact
    assert_equal failures, [], 'Please replace assert_no_tag with assert_select(count: 0).'
  end

  describe "fixture integrity" do
    let_all(:fixtures) do
      Dir["test/fixtures/*.yml"].map do |file|
        [file, File.basename(file, '.yml').singularize, YAML.load(ERB.new(File.read(file)).result)]
      end
    end

    it "does not be empty" do
      failures = fixtures.reject(&:last).map { |file, *| "#{file} is empty!" }
      assert_equal [], failures
    end

    it "has properly referenced models" do
      failures = fixtures.flat_map do |_, klass, content|
        content.select { |_, fixture| fixture["id"] }.flat_map do |name, _|
          fixtures.flat_map do |file, _, c|
            c.select { |_, fixture| fixture[klass] == name }.map do |_, _fixture|
              "#{file} uses #{klass}: #{name}, but should use the id!"
            end
          end
        end
      end.compact

      assert_equal [], failures
    end

    it "does not have defaults as fixtures" do
      failures = fixtures.flat_map do |file, _, content|
        (content.map(&:first).grep(/\A[A-Z\d_]+\Z/) - ["DEFAULTS"]).map do |name|
          "#{file} has invalid uppercase fixture #{name}"
        end
      end
      assert_equal [], failures
    end

    it "does not have invalid dates" do
      failures = fixtures.flat_map do |file, _, content|
        content.flat_map do |name, fixture|
          fixture.flat_map do |k, v|
            v = v.is_a?(String) ? v.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '') : v
            if v =~ /\A\d\d\d\d-\d\d-\d\d.\d\d:\d\d:\d\d/ && v !~ /\A\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d\Z/
              "Invalid date on fixture #{file} #{name} #{k}"
            end
          end
        end
      end.compact
      assert_equal [], failures
    end

    it "is not include boilerplate" do
      failures = fixtures.map(&:first).select { |file, *| File.read(file).include?("# Read about fixtures ") }
      assert_equal [], failures
    end

    it "has an account for everything" do
      non_account_fixtures = [
        "accounts.yml", "plans.yml", "integers.yml", "billing_cycles.yml",
        "certificate_authorities.yml", "certificate_ips.yml", "coupons.yml", "mobile_apps.yml",
        "zuora_coupons.yml", "translation_locales.yml", "currencies.yml", "twitter_user_profiles.yml",
        "app_links.yml", "reserved_subdomains.yml"
      ]

      failures = fixtures.flat_map do |file, _, content|
        failed = content.select do |name, fixture|
          !fixture['account'] && !fixture['account_id'] && name != "DEFAULTS" && !non_account_fixtures.include?(File.basename(file))
        end
        failed.map do |name, _|
          "#{file} does not have account for #{name}"
        end
      end
      assert_equal [], failures
    end
  end

  it "does not define duplicate classes" do
    bad = PreCommitChecks.check_duplicate_test_classes Dir["test/**/*_test.rb"]
    assert_empty(bad, bad.join("\n"))
  end

  it "does not use @options since it will be used by minitest" do
    bad = PreCommitChecks.check_options_usage Dir["test/**/*_test.rb"]
    assert_empty(bad, bad.join("\n"))
  end

  it "does not pull in git gems over ssh" do
    assert_empty PreCommitChecks.check_ssh_git_gems
  end

  it "ensures unowned files list is sorted alphabetically" do
    lines = File.readlines("test/files/unowned.txt")

    unsorted_lines = lines.join
    sorted_lines = lines.sort.join

    if sorted_lines != unsorted_lines
      File.write("test/files/unowned-sorted.txt", sorted_lines)
    end

    assert_equal(sorted_lines, unsorted_lines, "test/files/unowned.txt is not alphabetically sorted. Run locally to generate a sorted version.")
  end

  it "does not introduce unowned files" do
    bad = PreCommitChecks.check_file_ownerships
    assert bad.empty?, bad.join("\n") # rubocop:disable Minitest/AssertEmpty
  end

  it "does not modify unowned files" do
    cmd = `git diff --name-only master...`
    files = cmd.split("\n")
    puts "\e[31m#{PreCommitChecks.check_file_ownership_establishment(files).join}\e[0m"
    assert true
  end

  it "only has _test.rb files in test folders" do
    bad = (
      Dir["test/**/*.rb"] -
      Dir["test/**/*_test.rb"] -
      Dir["test/{factories,support}/**/*.rb"] -
      Dir["test/pact/**/*.rb"] -
      [
        "test/controllers/rules/rules_controller_tests.rb", # superclass for other tests
        "test/integration/integration_test_helper.rb", # test helper
        "test/load/ticket_creator.rb",
        "test/load/traffic_creator.rb",
        "test/rubo_cop/cop/lint/lint_test_helper.rb"
      ]
    )
    bad.must_equal []
  end

  it "only uses rspec to verify pact contracts" do
    bad = Dir["test/**/*.rb"].select do |path|
      File.read(path).include?("rspec")
    end
    bad -= ['test/rubo_cop/cop/lint/lint_test_helper.rb', 'test/integration/cleanliness_test.rb', 'test/models/events/ticket_notifier_test.rb', 'test/pact/service_consumers/pact_helper.rb']
    bad.must_equal([])
  end

  it 'does not define observers' do
    bad = Dir["app/observers/**/*.rb"].select do |path|
      File.read(path).include?("ActiveRecord::Observer")
    end
    bad.must_equal([])
  end

  it 'does not use wrong case headers' do
    failures = controllers.map do |f, c|
      if (name = c[/\.headers\[['"](.*?)['"]\]/, 1]) && name !~ /^([A-Z][a-z]*-?)+$/
        [f, name]
      end
    end.compact
    assert_equal [], failures
  end

  it 'only uses string load paths' do
    bad = $LOAD_PATH.reject { |l| l.is_a?(String) }
    assert_equal [], bad
  end

  it 'does not use test/unit/lib' do
    refute File.exist?('test/unit/lib')
  end

  it "requires oauth scopes for all non-crud actions" do
    load_all_api_v2_controllers
    api_controllers = ActionController::Base.descendants.select { |d| d.name =~ /^Api\:\:V2\:\:(?!Internal|BaseController)/ }
    bad = api_controllers.flat_map do |controller|
      crud_actions = Api::V2::BaseController::READ_ACTIONS + Api::V2::BaseController::WRITE_ACTIONS
      filtered_actions = actions(controller) - crud_actions
      next if filtered_actions.blank?
      controller if filtered_actions.any? do |method| # all non-crud methods ought to require a specific oauth scope
        next unless controller.respond_to?(:required_oauth_scopes)
        # ignore methods that where included from other classes and do not represent endpoints
        next unless controller.new.method(controller.instance_methods.first).source_location.first == controller.new.method(method).source_location.first
        !controller.required_oauth_scopes(method)
      end
    end.compact
    assert_equal [], bad, "Require oauth scopes in all non-crud actions"
  end

  it "declares allowed parameters for all actions on controllers that include AllowedParameters" do
    load_all_controllers
    all = ActionController::Base.descendants.select { |d| d.include?(AllowedParameters) }

    bad = all.flat_map do |controller|
      next if controller.name.end_with?("BaseController") # children are responsible for verification
      next if [ # these controllers use stronger parameters directly
        "Api::V2::Internal::Ipm::FeatureNotificationsController",
        "Api::V2::Internal::Ipm::AlertsController"
      ].include?(controller.name)
      actions = controller.action_methods.to_a.reject { |a| a =~ /^(_conditional_callback_around_|_callback_before_|_callback_around_|url_options)/ } - ["flash", "paginate_with_cursor"] # fine if before_action
      actions.reject! { |a| controller.allowed_parameters[a] } # fine if whitelisted
      actions.select! do |action| # fine if not in our app
        f = controller.instance_method(action).source_location.first
        f.start_with?(Rails.root.join("app").to_s) || f.start_with?(Rails.root.join("lib").to_s)
      end
      actions.flat_map do |action|
        f, n = controller.instance_method(action).source_location
        "#{controller}##{action} needs allowed_parameters (#{f.sub(Rails.root.to_s << '/', '')}:#{n})"
      end
    end.compact
    assert_equal 0, bad.size, bad.join("\n")
  end

  it "does not use enqueue in hooks that can be rolled back" do
    # matching here is not very intuitive, but other approaches like
    # - def.*?\.enqueue and
    # - scan .*?\.enqueue
    # - scan .*\.enqueue
    # do not work.
    # If you refactor it, make sure it catches
    # - multiple .enqueue per file
    # - .enqueue in a method after an innocent method
    # - not .enqueue in a commented out line
    method_pattern = "\s+def ([a-z_\\d\\!\\?]+)"
    enqueue_pattern = "\\.enqueue\\b"

    bad = models.flat_map do |f, content|
      enqueue_methods = content.scan(/(#{method_pattern}.*?#{enqueue_pattern})/m).
        map { |m| m.first !~ /#.*#{enqueue_pattern}/ && m.first.rindex(/#{method_pattern}/) && $1 }.
        compact
      # This enqueues RevereSubscriberUpdateJob, which is harmless.
      enqueue_methods.delete("remove_revere_subscription")
      enqueue_methods.map do |m|
        if /(\s|\.)(after|before|around)_(save|update|create|destroy)\s+.*:#{m}/.match?(content) # after_update :xxx or after_update :yyy, :xxx
          "#{f} method #{m} calls enqueue, but is used inside a hook that might be rolled back"
        end
      end
    end.compact
    bad.must_equal []
  end

  it "does not create new top-level test folders" do
    static = [
      "test/factories", "test/javascript", "test/shoulda_macros",
      'test/lib', 'test/pact', 'test/support', 'test/integration', 'test/fixtures', 'test/files',
      'test/load', 'test/rubo_cop'
    ]
    allowed = Dir['app/*'].map { |d| d.sub('app/', 'test/') } + static
    actual = Dir['test/*']
    bad = actual - allowed
    bad.map! { |b| "#{b} is not allowed, find an existing folder for your new tests" }
    bad.must_equal([], bad)
  end

  it "does not add new files to deprecated test/unit" do
    Dir['test/unit/**/*_test.rb'].size.must_equal 0
  end

  it "matches all tests to files" do
    # Add exceptions to this list. For e.g, a spec has been split into two files, thus one of the spec
    # filenames does not match its implementation.
    exceptions = [
      'test/controllers/api/v2/requests/ccd_test.rb',
      'test/controllers/api/v2/requests/collaborators_test.rb',
      'test/controllers/api/v2/requests/custom_fields_test.rb',
      'test/controllers/api/v2/requests/search_test.rb',
      'test/models/accounts/precreation2_test.rb',
      'test/lib/zendesk/accounts/security/api_settings_update/all_params_test.rb',
      'test/lib/zendesk/accounts/security/api_settings_update/agent_logins_test.rb',
      'test/lib/zendesk/accounts/security/api_settings_update/remote_authentications_test.rb',
      'test/lib/zendesk/accounts/security/api_settings_update/custom_security_policy_and_session_timeout_test.rb',
      'test/lib/zendesk/accounts/security/api_settings_update/assumption_expiration_test.rb'
    ]
    [
      ['test/lib', 'lib'],
      ['test/mailers', 'app/mailers'],
      ['test/observers', 'app/observers'],
      ['test/controllers', 'app/controllers'],
      ['test/backfills', 'app/backfills'],
      ['test/middleware', 'app/middleware'],
      ['test/presenters', 'app/presenters'],
      ['test/models', 'app/models'],
      ['test/rubo_cop/cop/lint', 'test/support/rubocop']
    ].each do |test, real|
      mapping = Dir["#{test}/**/*_test.rb"].map { |f| [f.sub(test, real).sub('_test.rb', '.rb'), f] }.to_h
      Dir["#{real}/**/*.rb"].each { |f| mapping.delete(f) }
      mapping.delete_if { |_f, t| File.read(t).include?('SingleCov.not_covered') }
      mapping.delete_if { |_f, t| exceptions.include? t }
      mapping.size.must_equal(0, mapping.map { |f, t| "#{t} has no matching file #{f}" }.join("\n"))
    end
  end

  # Because of the way that Rails 3 resolves templates if you have a both a
  # mobile_v2 template, a plain .erb template and no .html.erb template,
  # XHR requests that don't specify the content-type properly will be rendered
  # using the mobile_v2 template even when what you wanted was the content in the
  # plain .erb file. See the "a XHR GET with no content-type specified" context in
  # test/functional/entries_controller_test.rb for a distillation of this failure
  it "ensures .mobile_v2.erb template files have corresponding html.erb versions" do
    views = File.join(Rails.root, "app", "views")
    missing_html_templates = [].tap do |missing|
      Dir["#{views}/**/*.mobile_v2.erb"].each do |file|
        if File.exist?(file.gsub("mobile_v2.", "")) && !File.exist?(file.gsub("mobile_v2", "html"))
          missing << file.gsub("mobile_v2.", "")
        end
      end
    end

    error_message = "The following templates have a .mobile_v2.erb version but no corresponding .html version:\n\n" <<
        missing_html_templates.join("\n") <<
        "\n\nPlease copy, symlink or rename the .erb version to .html.erb. See this test example for more detail"
    assert_empty missing_html_templates, error_message
  end

  it "does not have any symlinks in public go to folders not present on proxy servers" do
    links = `find public -maxdepth 10 -type l -print | xargs ls -al`.split("\n")
    links.select { |l| l.include?("../..") }.must_equal []
  end

  it "necessary extensions when importing has all extensions" do
    files = Dir["app/assets/stylesheets/**/*.css*"].map { |file| [file, File.read(file)] }
    files.size.must_be :>, 20
    bad = files.select { |_f, c| c =~ /^@import ['"](\.\.\/)*[^\.]*$/ }
    assert_empty bad,  "use imports with full extensions in #{bad.map(&:first).join(", ")}"
  end

  it 'does not use Rails 2 style of routes' do
    route_files = Dir['config/*routes*.rb'].map { |file| [file, File.read(file)] }
    bad = route_files.select { |_f, c| c =~ %r{\w+(/\w+)*#\w+} }
    assert_empty bad, "Do not use \"get 'my/path' => 'controller#action'\" but \"get 'my/path', controller: 'controller', action: :action\"\nFound in #{bad.map(&:first).join(', ')}"
  end

  it "has single cov for everything" do
    SingleCov.assert_used tests: Dir['test/**/*_test.rb'].reject { |t| t =~ %r{test/(integration|javascript)/} }
  end

  it "has tests for all files" do
    SingleCov.assert_tested untested: File.read("test/files/untested.txt").split("\n")
  end

  it "ensures code owners file is sorted alphabetically" do
    headers, lines = File.readlines(".github/CODEOWNERS").partition { |line| line.start_with?('#') }

    unsorted_lines = lines.join
    sorted_lines = lines.sort.join

    if sorted_lines != unsorted_lines
      File.write(".github/CODEOWNERS-sorted", headers.join + sorted_lines)
    end

    assert_equal(sorted_lines, unsorted_lines, ".github/CODEOWNERS is not alphabetically sorted. Run locally to generate a sorted version.")
  end

  it "ensures code owners file does not contain globs or paths that have no matching files" do
    bad = `script/check_codeowners_globs.sh`.chomp
    assert bad.empty?, bad # rubocop:disable Minitest/AssertEmpty
  end

  it "ensures code owners file has consistent indentation" do
    file = File.read(".github/CODEOWNERS")

    lines = file.lines.
      reject { |l| l.start_with?("#") }.
      reject { |l| l.start_with?("\n") }

    # Count the occurrences of different indentation widths
    indentation_counts = lines.each_with_object(Hash.new(0)) do |line, agg|
      indentation = line[/\A\S+\s+/, 0].size
      agg[indentation] += 1
    end

    # Find the most often occurring indentation
    consensus_indentation = indentation_counts.max_by(&:last).first

    fixed_lines = lines.map do |line|
      file, owners = line.split(/\s+/, 2)
      file.ljust(consensus_indentation, " ") + owners
    end

    assert_equal(fixed_lines.join, lines.join, ".github/CODEOWNERS has inconsistent indentation")
  end

  it "avoids DateTime.now in tests" do
    bad = test_files.select do |name, content|
      name != "test/integration/cleanliness_test.rb" && content.include?('DateTime.now')
    end.map(&:first)
    assert_empty bad
  end

  describe "when bundling rails 5" do
    let(:usage) { "Use ./script/bundle <command> to execute <command> using both gemfiles" }

    it "does not have accidental differences to rails 5" do
      diff = PreCommitChecks.check_lockfile_diff(PreCommitChecks::LOCK_FILES.keys)
      assert diff == [], "You need to bundle for rails 4 and 5. Found differences: #{diff} #{usage}"
    end

    it "does not miss cached files in vendor/cache-rails5" do
      missing = PreCommitChecks.check_package(PreCommitChecks::LOCK_FILES.keys)
      assert missing == [], "You have missing gem files in vendor/cache*. #{missing} #{usage}"
    end
  end
end
