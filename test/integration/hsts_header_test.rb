require_relative "../support/test_helper"

describe "HSTS Header Integration" do
  fixtures :accounts

  def assert_hsts_header(seconds: 365 * 24 * 60 * 60)
    assert_equal "max-age=#{seconds};", response.headers['Strict-Transport-Security']
  end

  describe "HSTS" do
    let(:account) { accounts(:minimum).tap { |a| a.is_ssl_enabled = true; a.save! } }

    it "enables HSTS" do
      get "#{account.url}/ping/host"
      assert_response :ok
      assert_hsts_header
    end

    it "does not set a header on HTTP requests" do
      get "#{account.url(ssl: false)}/ping/host"
      assert_nil response.headers['Strict-Transport-Security']
    end

    it "enables HSTS on the subdomain even when ssl is disabled" do
      account.is_ssl_enabled = false
      account.save!

      get "#{account.url(ssl: true)}/ping/host"

      assert_hsts_header
    end

    describe "with ssl hostmapping" do
      before do
        account.update_attribute(:host_mapping, "example.com")
        Zendesk::Routes::UrlGenerator.any_instance.stubs(hosted_ssl?: true)
        refute_equal account.url, account.url(mapped: false)
      end

      it "enables HSTS on the subdomain" do
        get "#{account.authentication_domain}/ping/host"

        assert_hsts_header
      end

      it "enables HSTS on the hostmapped domain" do
        get "#{account.url}/ping/host"

        assert_hsts_header(seconds: 60 * 60 * 24 * 3)
      end

      it "extends the HSTS header to 1 year on the hostmapped domain with the setting" do
        account.settings.set_long_hsts_header_on_host_mapping = true
        account.save!

        get "#{account.url}/ping/host"

        assert_hsts_header
      end
    end

    describe "with http hostmapping" do
      before do
        account.is_ssl_enabled = false
        account.save!
        account.update_attribute(:host_mapping, "example.com")
        refute_equal account.url, account.url(mapped: false)
      end

      it "enables HSTS on the subdomain" do
        get "#{account.authentication_domain}/ping/host"

        assert_hsts_header
      end

      it "disables HSTS on the hostmapped domain if accessed over HTTPS" do
        get "https://#{account.host_mapping}/ping/host"

        assert_hsts_header(seconds: 0)
      end

      it "does not set a header on the hostmapped domain if accessed over HTTP" do
        get "http://#{account.host_mapping}/ping/host"

        assert_nil response.headers['Strict-Transport-Security']
      end
    end

    describe "on a HSTS protected domain" do
      it "enables HSTS" do
        get "https://zdusercontent.com/ping/host"
        assert_hsts_header
      end

      it "enables HSTS on subdomains" do
        get "https://p1.zdusercontent.com/ping/host"
        assert_hsts_header
      end

      it "doesn't enable HSTS on random domains" do
        get "https://example.com/ping/host"
        assert_hsts_header(seconds: 0)
      end
    end
  end
end
