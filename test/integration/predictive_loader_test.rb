require_relative "../support/test_helper"

describe 'PredictiveLoad::Loader' do
  fixtures :certificates, :accounts, :brands

  it "doesn't preload a sharded association of a unsharded collection" do
    account = Certificate.all.to_a.first.account
    queries = sql_queries { account.brands.to_a }

    assert_equal 1, queries.size
    assert_includes queries[0], "AND `brands`.`account_id` = #{account.id}"
  end

  it "preloads a sharded association of a sharded collection" do
    account = accounts(:minimum)
    queries = sql_queries { account.tickets.map(&:requester) }

    assert_equal 2, queries.size
  end
end
