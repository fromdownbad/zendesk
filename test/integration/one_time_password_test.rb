require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

class OneTimePasswordTest < ActionDispatch::IntegrationTest
  describe 'OTP setup' do
    before do
      @account = accounts(:minimum)

      @account.settings.ssl_enabled = true
      @account.settings.save!

      @agent = users(:minimum_agent)

      Zendesk::Auth::Otp::MessageJob.stubs(:work)

      assert Zendesk::Auth::Otp::Phone.new(@agent, phone: '123-123-1234').setup!
      assert Zendesk::Auth::Otp::Phone.new(@agent, code: @agent.one_time_password.time_based_token).confirm!

      https!
    end

    describe '/access/normal' do
      [true, false].each do |val|
        describe "when mobile_v2=#{val}" do
          let(:mobile_v2) { val }

          should_eventually 'logs the user in' do
            visit "#{@account.authentication_domain}/access/normal?is_mobile=#{val}"

            fill_in 'user[email]', with: @agent.email
            fill_in 'user[password]', with: '123456'
            click_button

            fill_in 'return_to', with: '/api/v2/users/me.json'
            fill_in 'password', with: @agent.reload.one_time_password.time_based_token
            click_button

            assert_equal @agent.id, JSON.parse(@response.body)['user']['id']
          end
        end
      end
    end

    describe '/access/oauth_mobile' do
      before do
        @client = FactoryBot.create(:global_client, account: @account)
      end

      it 'logs the user in' do
        headers = {
          'HTTP_USER_AGENT' => 'Zendesk for iPhone',
          'HTTP_ACCEPT' => 'application/json',
          'HTTP_CLIENT_IDENTIFIER' => @client.identifier
        }

        post "#{@account.authentication_domain}/access/oauth_mobile", params: { user: { email: @agent.email, password: '123456' }, native_mobile: true }, headers: headers

        assert_equal 202, response.status
        assert_equal @agent.otp_tokens.last.value, response.headers['User-Id']

        post "#{@account.authentication_domain}/auth/one_time_password", params: { password: @agent.reload.one_time_password.time_based_token }, headers: headers.merge('HTTP_USER_ID' => @agent.otp_tokens.create.value)

        assert_equal 200, response.status

        token = JSON.parse(response.body)['authentication']['access_token']
        oauth_token = Zendesk::OAuth::Token.where(token: token).first
        assert_equal @agent, oauth_token.user
      end
    end
  end
end
