require_relative "../support/test_helper"

describe Nokogiri do
  it "should not read behind the input" do
    output = Array.new(500) do
      Nokogiri::HTML.fragment("<h1>foo <!--").to_s
    end

    assert_equal ['<h1>foo </h1>'], output.uniq
  end
end
