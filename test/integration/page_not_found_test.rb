require_relative "../support/test_helper"

class PageNotFoundTest < ActionDispatch::IntegrationTest
  fixtures :accounts, :account_property_sets

  describe "when current brand exists" do
    before do
      @account = accounts(:minimum)
      @account.stubs(:default_brand).returns(brands(:minimum))
    end

    describe "when current_brand has help center in use" do
      before do
        HelpCenter.
          stubs(:find_by_brand_id).
          with(@account.default_brand.id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))
      end

      describe "with the classic_help_center_redirect_on_non_body_only Arturo enabled" do
        before do
          Arturo.enable_feature! :classic_help_center_redirect_on_non_body_only
        end

        it "internally redirects a GET request" do
          get "#{@account.url}/foobar"

          assert_response :not_found
          assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
        end

        it "internally redirects a DELETE request" do
          delete "#{@account.url}/foobar"

          assert_response :not_found
          assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
        end

        it "internally redirects a HEAD request" do
          head "#{@account.url}/foobar"

          assert_response :not_found
          assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
        end

        it "redirects a POST request" do
          post "#{@account.url}/foobar"

          assert_response :not_found
          assert_nil @response.headers["X-Accel-Redirect"]
          assert_template(file: "#{Rails.root}/public/404.html")
        end

        it "redirects a PUT request" do
          put "#{@account.url}/foobar"

          assert_response :not_found
          assert_nil @response.headers["X-Accel-Redirect"]
          assert_template(file: "#{Rails.root}/public/404.html")
        end

        it "redirects a PATCH request" do
          patch "#{@account.url}/foobar"

          assert_response :not_found
          assert_nil @response.headers["X-Accel-Redirect"]
          assert_template(file: "#{Rails.root}/public/404.html")
        end
      end

      it "internally redirects a GET request" do
        get "#{@account.url}/foobar"

        assert_response :not_found
        assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
      end

      it "internally redirects a DELETE request" do
        delete "#{@account.url}/foobar"

        assert_response :not_found
        assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
      end

      it "internally redirects a HEAD request" do
        head "#{@account.url}/foobar"

        assert_response :not_found
        assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
      end

      it "internally redirects a POST request" do
        post "#{@account.url}/foobar"

        assert_response :not_found
        assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
      end

      it "internally redirects a PUT request" do
        put "#{@account.url}/foobar"

        assert_response :not_found
        assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
      end

      it "internally redirects a PATCH request" do
        patch "#{@account.url}/foobar"

        assert_response :not_found
        assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
      end
    end

    describe "when current_brand doesn't have an help center in use" do
      before do
        HelpCenter.
          stubs(:find_by_brand_id).
          with(@account.default_brand.id).
          returns(HelpCenter.new(id: 42, state: 'archived'))
      end

      it "renders a static file and return 404" do
        get "#{@account.url}/foobar"

        assert_response :not_found
        assert_template(file: "#{Rails.root}/public/404.html")
      end
    end

    describe "when requesting a .js-file that doesn't exist" do
      before do
        HelpCenter.
          stubs(:find_by_brand_id).
          with(@account.default_brand.id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))
      end

      it 'renders a 404' do
        # After upgrading to Rails 4.2, this would raise an
        # exception instead of returning a 404
        get "#{@account.url}/foobar.js"
        assert_response :not_found
      end
    end
  end

  describe "when no current_brand exists" do
    it 'redirects to marketing site' do
      get '/foobar'
      assert_response :redirect
      assert_equal 301, @response.status
    end

    it "includes utm_content parameter" do
      get '/foobar'

      assert_includes response.redirect_url, "utm_content=#{request.host}"
    end
  end
end
