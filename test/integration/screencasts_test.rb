require_relative "../support/test_helper"
require 'arturo/test_support'

class ScreencastsTest < ActionDispatch::IntegrationTest
  fixtures :all

  before do
    @account = accounts(:minimum)
    @host    = @account.url
  end

  describe "When screencasts screencasts_for_tickets are available to subscription," do
    before do
      Subscription.any_instance.stubs(:has_screencasts_for_tickets?).returns(true)
    end

    describe "as an Admin, going to the ticket settings" do
      before do
        login users(:minimum_admin)
      end

      describe "when Screenr has been provisioned" do
        before do
          @screenr_integration = Screenr::Integration.new(@account)
          @domain = "minimum"
          params_from_api = {
            domain: "minimum",
            host: "minimum.sfssdev.com",
            upgraded_account: false,
            upgrade_url: "https://screenr-upgrade.com/test",
            recorders: {
              tickets_recorder_id: "TICKETS-ID",
              forums_recorder_id: "FORUMS-ID"
            }
          }

          @client = stub("client", create_tenant: params_from_api)
          Screenr::BusinessPartnerClient.stubs(:new).returns(@client)
          @account.screenr_integration.provide_tenant(@domain)

          visit "#{@host}/settings/tickets"
        end

        it "sees the screencasts access settings" do
          assert_response :success
          refute response_body.include?("Screencasts")
        end
      end

      describe "when Screenr has not been provisioned" do
        before do
          visit "#{@host}/settings/tickets"
        end

        it "hides the screencasts access settings, because Screenr is deprecated" do
          assert_response :success
          refute response_body.include?("Screencasts")
        end
      end
    end
  end

  describe "When screencasts_for_tickets are not available to subscription" do
    before do
      Subscription.any_instance.stubs(:has_screencasts_for_tickets?).returns(false)
    end

    describe "as an Admin, going to the ticket settings" do
      before do
        login users(:minimum_admin)
        visit "#{@host}/settings/tickets"
      end

      it "does not see the screencasts access settings" do
        assert_response :success
        refute response_body.include?("Screencasts")
      end
    end
  end
end
