require_relative "../../../support/test_helper"
require_relative "../../../support/zendesk_dsl"
require_relative "../../../support/sessions_test_helper"
require 'zendesk_shared_session/database_store'

class Api::V2::SessionApiTest < ActionDispatch::IntegrationTest
  include ZendeskDSL
  include SessionsTestHelper

  fixtures :accounts, :role_settings

  describe "session_api" do
    let(:password) { "123456" }

    before do
      Zendesk::SharedSession::SessionRecord.without_arsi.delete_all
      @account = accounts(:minimum)
      @admin   = @account.owner
      @domain  = URI(@account.url).host
    end

    describe "after login as an admin" do
      before do
        logs_in_as(@admin, password)
        assert_response :success
        assert_equal @admin.id, shared_session["warden.user.default.key"]
        @admin_cookie = cookies["_zendesk_shared_session"]
        @admin_session_key = get_session_key(@admin_cookie)
      end

      describe "GET to :index" do
        before do
          visit("https://#{@domain}/api/v2/users/#{@admin.id}/sessions")
          assert_response :success
        end

        it "returns the session" do
          sessions = JSON.parse(response.body)["sessions"]
          assert_equal 1, sessions.length
          assert_equal get_session_record(@admin_session_key).id, sessions[0]['id']
        end
      end

      describe "GET to :show_current" do
        before do
          visit("https://#{@domain}/api/v2/users/me/session.json")
          assert_response :success
        end

        it "returns the session" do
          session = JSON.parse(response.body)["session"]
          assert_equal get_session_record(@admin_session_key).id, session['id']
        end
      end

      describe "GET to :renew" do
        before do
          @shared_session = @admin.shared_sessions.last
          @old_expire_at = @shared_session.expire_at
        end

        describe "Arturo is ON without HTTP_X_ZENDESK_RENEW_SESSION" do
          before do
            Arturo.enable_feature! :client_session_renewal_via_explicit_api

            Timecop.travel(6.minutes.from_now) do
              get "https://#{@domain}/api/v2/users/me/session/renew.json"
            end
          end

          it "returns 200" do
            assert_response :success
          end

          it "renews the session" do
            @shared_session.reload
            assert @shared_session.expire_at > @old_expire_at
          end

          it 'returns a header with the renewed session' do
            @shared_session.reload
            assert_equal @shared_session.expire_at, response.headers['X-Zendesk-User-Session-Expires-At']
          end
        end

        describe "Arturo is ON with HTTP_X_ZENDESK_RENEW_SESSION" do
          before do
            Arturo.enable_feature! :client_session_renewal_via_explicit_api

            Timecop.travel(6.minutes.from_now) do
              url = "https://#{@domain}/api/v2/users/me/session/renew.json"
              get(url, headers: { 'HTTP_X_ZENDESK_RENEW_SESSION' => 'true' })
            end
          end

          it "returns 200" do
            assert_response :success
          end

          it "renew the session" do
            @shared_session.reload
            assert @shared_session.expire_at > @old_expire_at
          end

          it 'returns a header with the renewed session' do
            @shared_session.reload
            assert_equal @shared_session.expire_at, response.headers['X-Zendesk-User-Session-Expires-At']
          end
        end

        describe "Arturo is OFF without HTTP_X_ZENDESK_RENEW_SESSION" do
          before do
            Arturo.disable_feature! :client_session_renewal_via_explicit_api

            Timecop.travel(6.minutes.from_now) do
              get "https://#{@domain}/api/v2/users/me/session/renew.json"
            end
          end

          it "returns 200" do
            assert_response :success
          end

          it "do not renew the session" do
            @shared_session.reload
            assert_equal @shared_session.expire_at, @old_expire_at
          end

          it 'returns a header with the renewed session' do
            @shared_session.reload
            assert_equal @shared_session.expire_at, response.headers['X-Zendesk-User-Session-Expires-At']
          end
        end

        describe "Arturo is OFF with HTTP_X_ZENDESK_RENEW_SESSION" do
          before do
            Arturo.disable_feature! :client_session_renewal_via_explicit_api

            Timecop.travel(6.minutes.from_now) do
              url = "https://#{@domain}/api/v2/users/me/session/renew.json"
              get(url, headers: { 'HTTP_X_ZENDESK_RENEW_SESSION' => 'true' })
            end
          end

          it "returns 200" do
            assert_response :success
          end

          it "renew the session" do
            @shared_session.reload
            assert @shared_session.expire_at > @old_expire_at
          end

          it 'returns a header with the renewed session' do
            @shared_session.reload
            assert_equal @shared_session.expire_at, response.headers['X-Zendesk-User-Session-Expires-At']
          end
        end
      end

      describe "DELETE to logout" do
        before do
          delete "https://#{@domain}/api/v2/users/me/logout"
          @new_cookie = response.cookies['_zendesk_shared_session']
          assert_response :success
        end

        it "deletes the session from the session store" do
          assert_nil get_session_record(@admin_session_key)
          assert_equal({}, Zendesk::SharedSession::Session.load(decode_cookie(@admin_cookie)))
        end

        it "logouts the user" do
          url = "https://#{@domain}/api/v2/users/me.json"
          get(url, headers: { 'HTTP_COOKIE' => "_zendesk_shared_session=#{@admin_cookie}" })

          assert_response :unauthorized
        end

        describe_with_arturo_disabled :restrict_unauthenticated_user do
          it 'will return empty user' do
            url = "https://#{@domain}/api/v2/users/me.json"
            get(url, headers: { 'HTTP_COOKIE' => "_zendesk_shared_session=#{@admin_cookie}" })

            assert_response :success

            json = JSON.parse(response.body)
            assert_nil json["user"]["id"]
          end
        end
      end
    end

    describe 'after login as an end user' do
      before do
        @end_user = @account.end_users.first
        logs_in_as(@end_user, password)
        @shared_session = @end_user.shared_sessions.last
        @old_expire_at = @shared_session.expire_at
      end

      describe "GET to :renew" do
        describe "Arturo is ON without HTTP_X_ZENDESK_RENEW_SESSION" do
          before do
            Arturo.enable_feature! :client_session_renewal_via_explicit_api

            Timecop.travel(6.minutes.from_now) do
              get "https://#{@domain}/api/v2/users/me/session/renew.json"
            end
          end

          it "returns 200" do
            assert_response :success
          end

          it "renews the session" do
            @shared_session.reload
            assert @shared_session.expire_at > @old_expire_at
          end

          it 'returns a header with the renewed session' do
            @shared_session.reload
            assert_equal @shared_session.expire_at, response.headers['X-Zendesk-User-Session-Expires-At']
          end
        end
        describe "Arturo is ON with HTTP_X_ZENDESK_RENEW_SESSION" do
          before do
            Arturo.enable_feature! :client_session_renewal_via_explicit_api

            Timecop.travel(6.minutes.from_now) do
              url = "https://#{@domain}/api/v2/users/me/session/renew.json"
              get(url, headers: { 'HTTP_X_ZENDESK_RENEW_SESSION' => 'true' })
            end
          end

          it "returns 200" do
            assert_response :success
          end

          it "renew the session" do
            @shared_session.reload
            assert @shared_session.expire_at > @old_expire_at
          end

          it 'returns a header with the renewed session' do
            @shared_session.reload
            assert_equal @shared_session.expire_at, response.headers['X-Zendesk-User-Session-Expires-At']
          end
        end

        describe "Arturo is OFF without HTTP_X_ZENDESK_RENEW_SESSION" do
          before do
            Arturo.disable_feature! :client_session_renewal_via_explicit_api

            Timecop.travel(6.minutes.from_now) do
              get "https://#{@domain}/api/v2/users/me/session/renew.json"
            end
          end

          it "returns 200" do
            assert_response :success
          end

          it "do not renew the session" do
            @shared_session.reload
            assert_equal @shared_session.expire_at, @old_expire_at
          end

          it 'returns a header with the renewed session' do
            @shared_session.reload
            assert_equal @shared_session.expire_at, response.headers['X-Zendesk-User-Session-Expires-At']
          end
        end

        describe "Arturo is OFF with HTTP_X_ZENDESK_RENEW_SESSION" do
          before do
            Arturo.disable_feature! :client_session_renewal_via_explicit_api

            Timecop.travel(6.minutes.from_now) do
              url = "https://#{@domain}/api/v2/users/me/session/renew.json"
              get(url, headers: { 'HTTP_X_ZENDESK_RENEW_SESSION' => 'true' })
            end
          end

          it "returns 200" do
            assert_response :success
          end

          it "renew the session" do
            @shared_session.reload
            assert @shared_session.expire_at > @old_expire_at
          end

          it 'returns a header with the renewed session' do
            @shared_session.reload
            assert_equal @shared_session.expire_at, response.headers['X-Zendesk-User-Session-Expires-At']
          end
        end
      end
    end

    describe "not authenticated request to renew" do
      before do
        url = "https://#{@domain}/api/v2/users/me/session/renew.json"
        get(url, headers: { 'HTTP_X_ZENDESK_RENEW_SESSION' => true })
      end

      it "returns a 401" do
        assert_response 401
      end
    end

    describe "a basic auth request to show_current" do
      before do
        url = "https://#{@domain}/api/v2/users/me/session.json"
        auth = ActionController::HttpAuthentication::Basic.encode_credentials(@admin.email, password)
        get(url, headers: { 'HTTP_AUTHORIZATION' => auth })
      end

      it "returns not found" do
        assert_response :not_found
      end
    end
  end

  def get_session_record(session_key)
    Zendesk::SharedSession::SessionRecord.unexpired.find_by_session_id(session_key)
  end

  def get_session_key(cookie)
    decode_cookie(cookie).split(':').last
  end

  def decode_cookie(cookie)
    session_decryptor.load(cookie)
  end

  def session_decryptor
    @session_decryptor ||= Zendesk::SharedSession::CookieValue.new(Zendesk::SharedSession::SecretManager.new(@domain))
  end
end
