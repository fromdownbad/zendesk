require_relative "../../../../support/test_helper"
require_relative "../../../../support/gooddata_test_helper"

class IncrementalUsersExportTest < ActionDispatch::IntegrationTest
  include GooddataTestHelper

  fixtures :all

  describe "incremental users export" do
    before do
      @account = accounts(:minimum)
      @account.subscription.plan_type = SubscriptionPlanType.ExtraLarge
      @account.subscription.save!
      @admin = users(:minimum_admin)

      host! "#{@account.subdomain}.zendesk-test.com"
      https!
    end

    describe "with gooddata_oauth" do
      before do
        host = @account.host_name(ssl: true, mapped: false)

        @gooddata_token.update_attributes!(scopes: 'read')

        get "https://#{host}/api/v2/incremental/users.json?start_time=0'", session: { 'HTTP_AUTHORIZATION' => "Bearer #{@gooddata_token.token(:full)}" }
      end

      it "is unauthorized without proper key" do
        assert_response :unauthorized
      end
    end

    describe 'time zones' do
      it 'uses UTC for regular users' do
        login(@admin)
        host = @account.host_name(ssl: true, mapped: false)
        path = '/api/v2/incremental/users.json?start_time=0'
        get "https://#{host}#{path}"
        parsed_response = JSON.parse(@response.body)
        updated_at = parsed_response['users'].first['updated_at']
        response_time_offset = DateTime.parse(updated_at).utc_offset

        assert_equal 0, response_time_offset
      end

      it 'uses the account time zone for GoodData' do
        get_as_gooddata '/api/v2/incremental/users.json?start_time=0'
        parsed_response = JSON.parse(@response.body)
        updated_at = parsed_response['users'].first['updated_at']
        response_time_offset = DateTime.parse(updated_at).zone
        account_time_offset = ActiveSupport::TimeZone[@account.time_zone].now.formatted_offset
        offset_in_seconds = ActiveSupport::TimeZone[@account.time_zone].now.utc_offset

        assert_equal account_time_offset, response_time_offset
        refute offset_in_seconds.zero?
      end
    end

    it "exports updated users" do
      get_as_gooddata "/api/v2/incremental/users.json?start_time=0"
      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_not_empty parsed_response['users']
      assert_not_nil parsed_response['end_time']
      assert_not_nil parsed_response['next_page']
    end

    it "exports inactive users" do
      user = @account.users.first
      user.current_user = user
      user.tickets.update_all(status_id: StatusType.CLOSED)
      user.delete!

      get_as_gooddata '/api/v2/incremental/users.json?start_time=0'
      parsed_response = JSON.parse(@response.body)
      exported_inactive_user = parsed_response['users'].find { |r| r['id'] == user.id }
      assert_equal false, exported_inactive_user['active']
    end

    it "does not fall over when no updates" do
      get_as_gooddata "/api/v2/incremental/users.json?start_time=99999999999"
      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_empty parsed_response['users']
      assert_nil parsed_response['end_time']
      assert_nil parsed_response['next_page']
    end

    it 'responds with sample end point' do
      get_as_gooddata "/api/v2/incremental/users/sample.json?start_time=0"

      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_not_empty parsed_response['users']
      assert_not_nil parsed_response['end_time']
      assert_not_nil parsed_response['next_page']
    end
  end
end
