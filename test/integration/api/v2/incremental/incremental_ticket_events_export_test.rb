require_relative '../../../../support/test_helper'
require_relative '../../../../support/api_test_helper'
require_relative '../../../../support/gooddata_test_helper'

# integration tests for incremental ticket events
class IncrementalTicketEventsExportTest < ActionDispatch::IntegrationTest
  include GooddataTestHelper
  include ApiTestHelper

  fixtures :accounts, :users

  describe 'incremental ticket events export' do
    before do
      @account = accounts(:minimum)
      @account.subscription.plan_type = SubscriptionPlanType.ExtraLarge
      @account.subscription.save!
      @admin = users(:minimum_admin)

      host! "#{@account.subdomain}.zendesk-test.com"
      https!

      Event.connection.execute('delete from events')
      ticket = @account.tickets.find { |t| t.status_id != StatusType.CLOSED }
      4.times do |_i|
        ticket.add_comment(body: 'a comment to generate some events', is_public: true)
        ticket.will_be_saved_by(@admin)
        ticket.save!
      end

      @first_event_time = Event.connection.select_all(
        'select min(created_at) min_time from events'
      ).first['min_time'].to_i
    end

    describe "with gooddata_oauth" do
      before do
        host = @account.host_name(ssl: true, mapped: false)

        @gooddata_token.update_attributes!(scopes: 'read')

        get "https://#{host}/api/v2/incremental/ticket_events.json?start_time=0'", session: { 'HTTP_AUTHORIZATION' => "Bearer #{@gooddata_token.token(:full)}" }
      end

      it "is unauthorized without proper key" do
        assert_response :unauthorized
      end
    end

    it 'allows admin authentication' do
      login(@admin)
      host = @account.host_name(ssl: true, mapped: false)
      path = '/api/v2/incremental/ticket_events.json?start_time=0'
      get "https://#{host}#{path}"

      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_not_empty parsed_response['ticket_events']
      assert_not_nil parsed_response['end_time']
      assert_not_nil parsed_response['next_page']
    end

    it 'exports new ticket events users' do
      get_as_gooddata "/api/v2/incremental/ticket_events.json?start_time=#{@first_event_time}"
      expected_audit_ids = Audit.where(account_id: @account.id).where("created_at >= #{@first_event_time}").order(:created_at).pluck(:id).sort

      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_not_empty parsed_response['ticket_events']
      assert_not_nil parsed_response['end_time']
      assert_not_nil parsed_response['next_page']
      assert_equal expected_audit_ids, parsed_response['ticket_events'].map { |e| e['id'] }.sort.uniq
    end

    it 'can sideload comments' do
      get_as_gooddata "/api/v2/incremental/ticket_events.json?start_time=#{@first_event_time}&include=comment_events"

      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      comment_json = parsed_response['ticket_events'][0]['child_events'][0]
      expected_keys = %w[author_id body html_body]
      assert_equal expected_keys.size, (comment_json.keys & expected_keys).size
    end

    it 'justs skip events that have a broken ticket link' do
      Event.connection.execute('update events set ticket_id = -10 order by id limit 1')
      expected_audit_ids = Audit.where(account_id: @account.id).where('ticket_id != -10').order(:created_at).pluck(:id).sort
      get_as_gooddata "/api/v2/incremental/ticket_events.json?start_time=#{@first_event_time}"

      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_not_empty parsed_response['ticket_events']
      assert_not_nil parsed_response['end_time']
      assert_not_nil parsed_response['next_page']
      assert_equal expected_audit_ids, parsed_response['ticket_events'].map { |e| e['id'] }.sort.uniq
    end

    it 'does not fall over when no updates' do
      get_as_gooddata '/api/v2/incremental/ticket_events.json?start_time=99999999999'

      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_empty parsed_response['ticket_events']
      assert_nil parsed_response['end_time']
      assert_nil parsed_response['next_page']
      assert_equal 0, parsed_response['count']
    end

    it 'responds with sample end point' do
      get_as_gooddata '/api/v2/incremental/ticket_events/sample.json?start_time=0'

      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_not_empty parsed_response['ticket_events']
      assert_not_nil parsed_response['end_time']
      assert_not_nil parsed_response['next_page']
    end

    it 'gets the events from the archive' do
      archive_and_delete(*Ticket.all)
      assert_equal 0, Ticket.count(:all)
      assert_equal 0, Event.count(:all)
      get_as_gooddata "/api/v2/incremental/ticket_events.json?start_time=#{@first_event_time}"

      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_not_empty parsed_response['ticket_events']
      assert_not_nil parsed_response['end_time']
      assert_not_nil parsed_response['next_page']
      assert_equal 4, parsed_response['count']
    end

    it 'combines results from archive and live and give same result' do
      assert_equal 0, TicketArchiveStub.count(:all)
      get_as_gooddata "/api/v2/incremental/ticket_events.json?start_time=#{@first_event_time}"

      pre_archive_response = JSON.parse(@response.body)
      Ticket.all.each_with_index { |t, i| archive_and_delete(t) if i.odd? }
      get_as_gooddata "/api/v2/incremental/ticket_events.json?start_time=#{@first_event_time}"

      expected = pre_archive_response['ticket_events']
      response = JSON.parse(@response.body)['ticket_events']

      [expected, response].each do |items|
        items.each { |x| x["created_at"] = x["created_at"].to_time }
      end

      assert_equal expected.to_set, response.to_set
    end

    it 'combines results from archive and live and give same result with comments' do
      assert_equal 0, TicketArchiveStub.count(:all)
      get_as_gooddata "/api/v2/incremental/ticket_events.json?start_time=#{@first_event_time}&include=comment_events"

      pre_archive_response = JSON.parse(@response.body)
      Ticket.all.each_with_index { |t, i| archive_and_delete(t) if i.odd? }
      get_as_gooddata "/api/v2/incremental/ticket_events.json?start_time=#{@first_event_time}&include=comment_events"

      expected = pre_archive_response['ticket_events']
      response = JSON.parse(@response.body)['ticket_events']

      [expected, response].each do |items|
        items.each do |x|
          x["created_at"] = x["created_at"].to_time
          x["child_events"].each { |e| e.delete("audit_id") } # audits are already deleted so they do not get returned
        end
      end

      assert_equal expected.to_set, response.to_set
    end
  end
end
