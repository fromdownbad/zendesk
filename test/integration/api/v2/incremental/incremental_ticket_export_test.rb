require_relative "../../../../support/test_helper"
require_relative "../../../../support/gooddata_test_helper"

class IncrementalTicketsExportTest < ActionDispatch::IntegrationTest
  include GooddataTestHelper

  fixtures :all

  describe "incremental tickets export" do
    before do
      @account = accounts(:minimum)
      @account.subscription.plan_type = SubscriptionPlanType.ExtraLarge
      @account.subscription.save!
      @admin = users(:minimum_admin)
    end

    describe "with gooddata_oauth" do
      before do
        host = @account.host_name(ssl: true, mapped: false)

        @gooddata_token.update_attributes!(scopes: 'read')

        get "https://#{host}/api/v2/incremental/tickets.json?start_time=0'", session: { 'HTTP_AUTHORIZATION' => "Bearer #{@gooddata_token.token(:full)}" }
      end

      it "is unauthorized without proper key" do
        assert_response :unauthorized
      end
    end

    describe 'time zones' do
      it 'uses UTC for regular users' do
        login(@admin)
        host = @account.host_name(ssl: true, mapped: false)
        path = '/api/v2/incremental/tickets.json?start_time=0'
        get "https://#{host}#{path}"
        parsed_response = JSON.parse(@response.body)
        updated_at = parsed_response['tickets'].first['updated_at']
        response_time_offset = DateTime.parse(updated_at).utc_offset

        assert_equal 0, response_time_offset
      end

      it 'uses the account time zone for GoodData' do
        ticket = @account.tickets.build subject: 'new', description: 'stuff'
        ticket.will_be_saved_by @account.owner
        ticket.save!

        get_as_gooddata '/api/v2/incremental/tickets.json?start_time=0'
        parsed_response = JSON.parse(@response.body)
        updated_at = parsed_response['tickets'].last['updated_at']
        response_time_offset = DateTime.parse(updated_at).zone
        account_time_offset = ActiveSupport::TimeZone[@account.time_zone].now.formatted_offset
        offset_in_seconds = ActiveSupport::TimeZone[@account.time_zone].now.utc_offset

        assert_equal account_time_offset, response_time_offset
        refute offset_in_seconds.zero?
      end
    end

    it "exports updated tickets" do
      get_as_gooddata '/api/v2/incremental/tickets.json?start_time=0'
      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_not_empty parsed_response['tickets']
      assert_not_nil parsed_response['end_time']
      assert_not_nil parsed_response['next_page']
    end

    it "does not fall over when no updates" do
      start_time = Time.now.advance(minutes: 5).to_i
      get_as_gooddata "/api/v2/incremental/tickets.json?start_time=#{start_time}"
      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_empty parsed_response['tickets']
      assert_nil parsed_response['end_time']
      assert_nil parsed_response['next_page']
    end

    it 'uses the account time zone when exporting for Good Data' do
      get_as_gooddata "/api/v2/incremental/tickets.json?start_time=0"
      parsed_response = JSON.parse(@response.body)
      response_ticket = parsed_response['tickets'].first
      response_updated_at = response_ticket['updated_at']
      ticket_updated_at = Time.use_zone(@account.time_zone) do
        @account.tickets.find_by_nice_id(response_ticket['id']).updated_at
      end
      expected_updated_at = ticket_updated_at.iso8601
      assert_equal expected_updated_at, response_updated_at
    end

    it 'responds with sample end point' do
      get_as_gooddata '/api/v2/incremental/tickets/sample.json?start_time=0'

      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      assert_not_empty parsed_response['tickets']
      assert_not_nil parsed_response['end_time']
      assert_not_nil parsed_response['next_page']
    end

    it 'includes deleted tickets' do
      ticket_count = Ticket.with_deleted do
        @account.tickets.where('status_id != ?', StatusType.ARCHIVED).
          count(:all)
      end
      ticket = @account.tickets.last
      ticket.will_be_saved_by(@admin)
      ticket.soft_delete(validate: false)

      get_as_gooddata '/api/v2/incremental/tickets.json?start_time=0'
      assert_response :ok
      parsed_response = JSON.parse(@response.body)
      api_deleted_ticket = parsed_response['tickets'].detect { |t| t['id'] == ticket.nice_id }
      assert_equal ticket_count, parsed_response['count']
      assert_equal 'deleted', api_deleted_ticket['status']
    end

    it 'ignores last_audits side-load' do
      get_as_gooddata '/api/v2/incremental/tickets.json?start_time=0&include=last_audits'
      parsed_response = JSON.parse(@response.body)
      assert_nil parsed_response['last_audits']
    end

    describe 'exporting ticket fields' do
      before do
        @ticket_field = ticket_fields(:field_text_custom)
        @ticket_field.is_exportable = true
        @ticket_field.save!

        @non_export_ticket_field = ticket_fields(:field_tagger_custom)
        @custom_field_option = custom_field_options(:field_tagger_custom_option_1)
        @non_export_ticket_field.is_exportable = false
        @non_export_ticket_field.save!
      end

      it "stills export ticket fields given the presenter uses raw export" do
        get_as_gooddata "/api/v2/incremental/tickets.json?start_time=0"
        result = JSON.parse(@response.body)
        assert result['tickets'].all? { |ticket| ticket['custom_fields'].present? && ticket['fields'].present? }
      end

      it 'does not include fields excluded from export' do
        get_as_gooddata "/api/v2/incremental/tickets.json?start_time=0"
        result = JSON.parse(@response.body)

        exported_custom_field_keys = result['tickets'].first['custom_fields'].map { |field| field["id"] }
        exported_ticket_field_keys = result['tickets'].first['fields'].map { |field| field["id"] }

        refute_includes exported_custom_field_keys, @non_export_ticket_field.id
        refute_includes exported_ticket_field_keys, @non_export_ticket_field.id
        assert_includes exported_custom_field_keys, @ticket_field.id
        assert_includes exported_ticket_field_keys, @ticket_field.id
      end
    end
  end
end

class IncrementalTicketsExportNonGoodDataTest < ActionController::TestCase
  tests Api::V2::Incremental::TicketsController

  fixtures :accounts, :users, :ticket_fields, :custom_field_options

  as_an_admin do
    describe 'when the request is not from gooddata' do
      before do
        @ticket_field = ticket_fields(:field_text_custom)
        @ticket_field.is_exportable = true
        @ticket_field.save!

        @non_export_ticket_field = ticket_fields(:field_tagger_custom)
        @custom_field_option = custom_field_options(:field_tagger_custom_option_1)
        @non_export_ticket_field.is_exportable = false
        @non_export_ticket_field.save!
      end

      it 'includes fields that are marked with is_exportable: false' do
        get :index, params: { format: :json, start_time: 0 }
        result = JSON.parse(@response.body)

        exported_custom_field_keys = result['tickets'].first['custom_fields'].map { |field| field["id"] }
        exported_ticket_field_keys = result['tickets'].first['fields'].map { |field| field["id"] }

        assert_includes exported_custom_field_keys, @non_export_ticket_field.id
        assert_includes exported_ticket_field_keys, @non_export_ticket_field.id
        assert_includes exported_custom_field_keys, @ticket_field.id
        assert_includes exported_ticket_field_keys, @ticket_field.id
      end
    end
  end
end
