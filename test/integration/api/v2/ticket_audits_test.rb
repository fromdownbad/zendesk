require_relative "../../../support/test_helper"

class Api::V2::TicketAuditsTest < ActionDispatch::IntegrationTest
  extend Api::V2::TestHelper

  let(:host) { @account.host_name(ssl: true, mapped: false) }

  before do
    @account = accounts(:minimum)
    @admin = users(:minimum_admin)

    login @admin
  end

  # This exception is caught by zendesk_api_controller.
  # This test just makes sure it's actually working from within Classic.
  it 'catches invalid cursor error' do
    Api::V2::TicketAuditsController.any_instance.stubs(:index).raises(Zendesk::CursorPagination::InvalidCursor)
    get "https://#{host}/api/v2/ticket_audits"
    assert_response :bad_request
  end

  describe "index with cursor pagination params when ids are not sequential" do
    # eg. [5, 4, 3, 2]
    let(:ids) { @account.ticket_audits.order('id desc').pluck(:id) }

    before do
      seq_timestamps = @account.ticket_audits.order('created_at desc').pluck(:created_at).map(&:to_f)

      # Takes the event with the smallest id (ie. created_first) and gives
      # it the highest created_at
      Event.find(ids[-1]).update_column :created_at, Time.at(seq_timestamps[0]) + 1.year
    end

    it "does not have duplicate ids when paginating" do
      assert ids.size > 2
      assert ids.uniq.size >= 2

      cursor = nil
      ids_fetched = []
      max_iterations = 10
      current_iterations = 0

      loop do
        if current_iterations > max_iterations
          raise "Reached max number of iterations expected (#{max_iterations})"
        end

        get "https://#{host}/api/v2/ticket_audits?limit=2&cursor=#{cursor}"

        assert_response :ok
        json = JSON.parse(@response.body)
        audit_json = json.fetch('audits')
        cursor = json.fetch('before_cursor')

        break unless cursor

        ids_fetched += audit_json.map { |e| e['id'] }
        current_iterations += 1
      end

      assert_equal ids_fetched.size, ids_fetched.uniq.size
    end
  end
end
