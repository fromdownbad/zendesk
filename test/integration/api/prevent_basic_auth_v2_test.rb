require_relative "../../support/test_helper"
require_relative "../../support/api_test_helper"

class PreventBasicAuthV2Test < ActionDispatch::IntegrationTest
  include ApiTestHelper

  fixtures :accounts, :users, :role_settings

  describe "test allowing and preventing basic auth in API v2" do
    before do
      @account  = accounts(:minimum)
      @user     = users(:minimum_agent)
    end

    let(:email) { @user.email }
    let(:password) { "123456" }
    let(:url) { "https://#{@account.host_name}" }

    describe "allow v2 basic auth access no Arturo" do
      it "allows basic auth to v2 endpoint" do
        get "#{url}/api/v2/users/#{@user.id}.json", headers: api_basic_auth_headers(email, password)
        assert_response :success
      end
    end

    describe "preventing v2 basic auth access with Arturo feature" do
      before do
        Arturo.enable_feature!(:prevent_v2_basic_auth)
      end

      describe "prevent basic auth to v2" do
        it "allows non basic auth" do
          post "#{url}/access/login", params: { user: { email: email, password: password} }
          get "#{url}/api/v2/users/#{@user.id}.json"
          assert_response :success
        end

        it "prevents basic auth" do
          get "#{url}/api/v2/users/#{@user.id}.json", headers: api_basic_auth_headers(email, password)
          assert_response :bad_request
        end

        it "does not blow up without account and basic auth" do
          params = { account: { name: 'auth test', subdomain: "anewdomain#{(rand * 1000).round}" } }
          post "https://signup.#{Zendesk::Configuration.fetch(:host)}/api/v2/accounts.json", params: params, headers: api_basic_auth_headers(email, password)
          assert_response :unprocessable_entity
        end
      end
    end
  end
end
