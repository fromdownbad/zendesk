require_relative "../../../support/test_helper"

require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

class Api::Mobile::ScopingTest < ActionDispatch::IntegrationTest
  fixtures :all

  describe 'mobile API scoping' do
    let(:request_headers) do
      {
      "HTTP_AUTHORIZATION" => "Bearer #{@token.token(:full)}",
      "HTTP_USER_AGENT" => "Zendesk SDK for iOS"
    }
    end

    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)

      @client = FactoryBot.create(:client, account: @account)
      @auth = FactoryBot.create(:mobile_sdk_auth, account: @account, client: @client)
      mobile_sdk_app = FactoryBot.create(:mobile_sdk_app, account: @account, mobile_sdk_auth: @auth)
      @auth.mobile_sdk_app = mobile_sdk_app
      @token = FactoryBot.create(:token, user: @user, account: @account, client: @client)

      @ticket = FactoryBot.create(:ticket, account: @account, requester: @user)
      @req_token = RequestToken.create(ticket: @ticket)

      @host = "https://#{@account.host_name}"

      host! @host
      https!
    end

    it 'allows :read access to :sdk endpoints' do
      @token.scopes = %w[sdk]
      @token.save!

      get "#{@host}/api/mobile/requests/#{@req_token.value}.json", headers: request_headers
      assert_response :ok
    end

    describe ':sdk scope' do
      before do
        @token.scopes = %w[sdk]
        @token.save!
      end

      it 'allows :sdk access to :sdk endpoints' do
        get "#{@host}/api/mobile/requests/#{@req_token.value}.json", headers: request_headers
        assert_response :ok
      end

      it 'does not allow :sdk access to :read endpoints' do
        get "#{@host}/api/v2/users/me.json", headers: request_headers
        assert_response :forbidden
      end
    end
  end
end
