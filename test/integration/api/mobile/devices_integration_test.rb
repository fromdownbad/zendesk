require_relative "../../../support/test_helper"

require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

class Api::Mobile::DevicesTest < ActionDispatch::IntegrationTest
  describe "#register" do
    let(:account) { accounts(:minimum) }
    let(:user) { account.owner }
    let(:client) { FactoryBot.create(:client, user: user) }
    let(:token) { FactoryBot.create(:token, client: client, user: user) }
    let(:legacy_device) do
      user.mobile_devices.create!(
        account: account,
        user: user,
        name: 'Zendesk for iPhone',
        token: token.id.to_s,
        ip: '1.2.3.4',
        user_agent: 'Zendesk for iPhone'
      )
    end

    it "requires an oauth token" do
      post account.url(ssl: true) + "/api/mobile/devices/register.json"
      assert_response :unauthorized
    end

    describe "with a legacy device" do
      it "can not be logged out" do
        delete_with_oauth "/api/mobile/devices/logout.json", params: {}, token: token
        assert_response :forbidden
        assert token.reload
      end

      describe "#register with bad params" do
        it "does not blow up" do
          assert legacy_device
          post_with_oauth "/api/mobile/devices/register.json", params: {}, token: token
          assert_response :unprocessable_entity
        end
      end

      describe "#register" do
        before do
          assert legacy_device
          @device_info = { identifier: SecureRandom.hex(32), name: "Morten's iPhone" }
          post_with_oauth "/api/mobile/devices/register.json", params: { device: @device_info }, token: token
          assert_response :ok
        end

        it "does not delete the existing token or legacy device" do
          token_value = token.token(true)
          assert legacy_device.reload
          assert token.reload
          assert_equal token_value, token.token(true)
        end

        describe "it creates an new oauth token + mobile device" do
          before do
            new_token = JSON.parse(response.body).fetch("token")
            refute_equal token.token(true), new_token

            @new_token = account.tokens.where(token: new_token).first
            assert @new_token, "did not create a new token"

            @new_mobile_device = account.mobile_devices.where(oauth_token_id: @new_token.id).first
            assert @new_mobile_device, "did not create a new mobile device"
          end

          it "sets the device attributes from the request" do
            assert_equal @device_info[:name], @new_mobile_device.name
            assert_equal @device_info[:identifier], @new_mobile_device.token
          end

          it "copies the token details to the new token" do
            assert_equal token.account_id, @new_token.account_id
            assert_equal token.user_id, @new_token.user_id
            assert_equal token.client_id, @new_token.client_id
            assert_equal token.scopes, @new_token.scopes
          end

          it "does not copy token specific fields" do
            refute_equal token.expires_at, @new_token.expires_at
            refute_equal token.created_at, @new_token.created_at
            refute_equal(token.last_used_at, @new_token.last_used_at) if @new_token.last_used_at
          end

          describe "with the new per device token" do
            it "can not be registered" do
              post_with_oauth "/api/mobile/devices/register.json", params: { device: @device_info }, token: @new_token
              assert_response :forbidden
            end

            describe "#logout" do
              before do
                delete_with_oauth "/api/mobile/devices/logout.json", params: {}, token: @new_token
                assert_response :ok
              end

              it "deletes the oauth token" do
                assert_raises(ActiveRecord::RecordNotFound) { @new_token.reload }
              end

              it "doesn't delete the mobile device" do
                assert @new_mobile_device.reload
              end

              it "removes the oauth token from the mobile device" do
                assert_nil @new_mobile_device.reload.oauth_token_id
              end
            end
          end
        end
      end
    end
  end

  def request_with_oauth(http_verb, path:, params:, token:)
    headers = {
      'HTTP_CLIENT_IDENTIFIER' => token.client.identifier,
      'HTTP_AUTHORIZATION' => "Bearer #{token.token(true)}",
      'HTTP_USER_AGENT' => 'Zendesk for iPhone'
    }
    send(http_verb, account.url(ssl: true) + path, params, headers)
  end

  def post_with_oauth(path, params:, token:)
    request_with_oauth :post, path: path, params: params, token: token
  end

  def delete_with_oauth(path, params:, token:)
    request_with_oauth :delete, path: path, params: params, token: token
  end
end
