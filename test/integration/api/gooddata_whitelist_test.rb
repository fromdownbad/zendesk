require_relative "../../support/test_helper"
require_relative "../../support/gooddata_test_helper"

class Api::GooddataWhitelistTest < ActionDispatch::IntegrationTest
  include GooddataTestHelper

  fixtures :all

  before do
    Account.any_instance.stubs(has_ip_restriction?: true)
    Api::V2::Exports::GooddataController.any_instance.stubs(check_if_allowed: true)
  end

  describe 'accounts with ip restriction' do
    before do
      @account = accounts(:minimum)
      @account.settings.ip_restriction_enabled = true
      @account.texts.ip_restriction = '1.1.1.1'
      @account.save!

      get_as_gooddata "/api/v2/gooddata/tickets_export.json?start_time=0", ip: ip
    end

    describe "gooddata ip" do
      let(:ip) { '107.22.195.240' }

      it "does not deny access for GoodData" do
        assert_response 200
      end
    end

    describe "non-goddata ip" do
      let(:ip) { '2.2.2.2' }

      it "denys access if not on whitelist and not GoodData" do
        assert_response 403
      end
    end
  end
end
