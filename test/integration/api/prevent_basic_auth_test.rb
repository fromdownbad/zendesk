require_relative "../../support/test_helper"
require_relative "../../support/api_test_helper"

class PreventBasicAuthTest < ActionDispatch::IntegrationTest
  include ApiTestHelper

  fixtures :accounts, :users, :role_settings, '../files/stat_rollup/ticket_stats'

  describe "preventing basic auth access" do
    before do
      @account  = accounts(:minimum)
      @host     = @account.host_name
      @user     = users(:minimum_agent)
    end

    let(:email) { @user.email }
    let(:password) { "123456" }

    describe "v1" do
      it "allows non basic auth" do
        post "https://#{@host}/access/login?user[email]=#{email}&user[password]=#{password}"
        get "https://#{@host}/api/v1/stats/graph_data/account/0/ticket_stats_by_account/solve_count"
        assert_response :success
      end

      it "allows salesforce" do
        request_with_basic_auth "/api/v1/stats/graph_data/account/0/ticket_stats_by_account/solve_count", "HTTP_USER_AGENT" => "foo SalesforceApp"
        assert_response :success
      end

      it "allows WordPress" do
        request_with_basic_auth "/api/v1/stats/graph_data/account/0/ticket_stats_by_account/solve_count", "HTTP_USER_AGENT" => "foo WordPress;"
        assert_response :success
      end

      it "should prevent basic auth" do
        request_with_basic_auth "/api/v1/stats/graph_data/account/0/ticket_stats_by_account/solve_count", "HTTP_USER_AGENT" => nil
        # this request should assert_response :bad_request
        # but it cannot because stats has explicitly skip_before_action :prevent_auth
        # so instead we're stuck with:
        assert_response :success
      end
    end

    describe "v2" do
      it "allows basic auth" do
        request_with_basic_auth "/api/v2/users/me.json"
        assert_response :success
      end
    end

    def request_with_basic_auth(path, headers = {})
      get "https://#{@host}#{path}", headers: api_basic_auth_headers(email, password).merge(headers)
    end
  end
end
