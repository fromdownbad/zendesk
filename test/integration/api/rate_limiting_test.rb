require_relative "../../support/test_helper"
require_relative "../../support/api_test_helper"
require_relative "../../support/signed_auth_helper"

class Api::RateLimitingTest < ActionDispatch::IntegrationTest
  include ApiTestHelper
  include SignedAuthHelper

  fixtures :all

  describe "API Rate Limiting" do
    before do
      start = Time.at(1333847684)
      Timecop.freeze start

      @account = accounts(:minimum)
      @admin   = users(:minimum_admin)

      host! "#{@account.subdomain}.zendesk-test.com"
      https!
    end

    describe "When the rate limit threshold has not been reached" do
      describe "and a new API request is sent to a v1 end point" do
        before do
          get "/api/v1/stats/graph_data/account/0/ticket_stats_by_account/solve_count", headers: rest_headers
        end

        it "responds with ok" do
          assert_response :ok
        end
      end

      describe "and a new API request is sent to a v2 end point" do
        before do
          get "/api/v2/users.json", headers: rest_headers
        end

        it "responds with ok with the correct headers" do
          assert_equal @account.api_rate_limit.to_s, response.headers["X-Rate-Limit"]
          assert_equal (@account.api_rate_limit - 1).to_s, response.headers["X-Rate-Limit-Remaining"]
          assert_response :ok
        end
      end
    end

    describe "When the rate limit threshold has been reached" do
      before do
        assert Prop.throttle(:api_requests, @account.id, increment: 1000)
      end

      describe "and a new API request is sent to a v1 end point" do
        before do
          get "/api/v1/stats/graph_data/account/search_account_stats/searches", headers: rest_headers
        end

        it "responds with 429" do
          assert_response 429
        end
      end

      describe "and a new API request is sent to a v2 end point" do
        before do
          get "/api/v2/users.json", headers: rest_headers
        end

        it "responds with 429" do
          @response.body.must_include "Number of allowed API requests per minute exceeded"
          assert_equal "16", @response.headers["Retry-After"]
          assert_equal @account.api_rate_limit.to_s, response.headers["X-Rate-Limit"]
          assert_equal 0, response.headers["X-Rate-Limit-Remaining"]
          assert_response 429
          assert_equal 'Too Many Requests', @response.message
        end
      end

      describe "and a new API request is sent with system user authentication" do
        before do
          request_system_user_oauth(:zendesk, "https://#{@account.subdomain}.zendesk-test.com/api/v2/users.json")
        end

        it "responds ok" do
          assert_response :ok
        end
      end

      describe "and a new API request is sent with system user header" do
        before do
          get "/api/v2/users.json", headers: { Zendesk::SystemUser::SignedWardenStrategy::RACK_SYSTEM_API_HEADER => 'stub' }
        end

        it "responds unauthorized" do
          # Not 429
          assert_response :unauthorized
        end
      end
    end

    describe "When the time threshold has been reached" do
      before do
        assert Prop.throttle(:api_time, @account.id, increment: 20.minutes.to_i)
      end

      describe "and a new API request is sent to a v1 end point" do
        before do
          get "/api/v1/stats/graph_data/account/search_account_stats/searches", headers: rest_headers
        end

        it "responds with 429" do
          assert_response 429
        end
      end

      describe "and a new API request is sent to a v2 end point" do
        before do
          get "/api/v2/users.json", headers: rest_headers
        end

        it "responds with 429" do
          @response.body.must_include "Allotted query time exceeded"
          assert_equal "16", @response.headers["Retry-After"]
          assert_response 429
          assert_equal 'Too Many Requests', @response.message
        end
      end

      it 'does not set the session cookie on rate limited requests' do
        Prop.throttle(:api_time, @account.id, increment: 20.minutes.to_i)
        get "/api/v1/stats/graph_data/account/search_account_stats/searches", headers: rest_headers
        assert_response 429

        assert_no_match /shared_session/, response.headers['Set-Cookie']
        assert_no_match /zendesk_authenticated/, response.headers['Set-Cookie']
        assert_equal 0, response.headers["X-Rate-Limit-Remaining"]
        assert_equal "700", response.headers["X-Rate-Limit"]
        assert_match /public/, response.headers["Cache-Control"]
      end

      describe "and a new API request is sent with system user authentication" do
        before do
          request_system_user_oauth(:zendesk, "https://#{@account.subdomain}.zendesk-test.com/api/v2/users.json")
        end

        it "responds ok" do
          assert_response :ok
        end
      end

      describe "and a new API request is sent with system user header" do
        before do
          get "/api/v2/users.json", headers: { Zendesk::SystemUser::SignedWardenStrategy::RACK_SYSTEM_API_HEADER => 'stub' }
        end

        it "responds unauthorized" do
          # Not 429
          assert_response :unauthorized
        end
      end
    end

    describe 'for unauthorized requests' do
      before do
        Arturo.enable_feature!(:rate_limit_unauthorized_api_requests)

        @fake_ip1 = '1.1.1.1'
        @fake_ip2 = '1.1.1.2'

        Prop.reset(:api_unauthorized_requests, @fake_ip1)
        Prop.reset(:api_unauthorized_requests, @fake_ip2)
      end

      describe 'to account creation endpoint' do
        describe "when the rate limit threshold has been reached" do
          before do
            assert Prop.throttle(:api_unauthorized_requests, @account.id, increment: 1000)
          end

          it 'does not get rate limited' do
            post "/api/v2/accounts.json", headers: rest_headers
            # Default requests for that endpoint
            assert_response :forbidden
          end
        end
      end

      it 'does nothing if authenticated' do
        get "/api/v2/users/me.json", headers: rest_headers.merge('REMOTE_ADDR' => @fake_ip1)
        assert_response :ok
        assert_equal 0, Prop.count(:api_unauthorized_requests, @fake_ip1)
      end

      it 'increments counter for correct ip' do
        get "/api/v2/users/me.json", headers: { 'REMOTE_ADDR' => @fake_ip1 }

        assert_equal 1, Prop.count(:api_unauthorized_requests, @fake_ip1)
        assert_equal 0, Prop.count(:api_unauthorized_requests, @fake_ip2)

        assert_response :unauthorized
      end

      describe_with_arturo_disabled :restrict_unauthenticated_user do
        it 'returns a response' do
          get "/api/v2/users/me.json", headers: { 'REMOTE_ADDR' => @fake_ip1 }

          assert_response :ok
        end
      end

      describe 'when threshold is exceeded' do
        before do
          Prop.throttle(:api_unauthorized_requests, @fake_ip1, increment: 10_000)
        end

        it 'returns 429' do
          get "/api/v2/users/me.json", headers: { 'REMOTE_ADDR' => @fake_ip1 }
          assert_response 429
        end

        it 'works if a valid request is done after being rate limited' do
          get "/api/v2/users/me.json", headers: { 'REMOTE_ADDR' => @fake_ip1 }
          assert_response 429

          assert_equal 10_001, Prop.count(:api_unauthorized_requests, @fake_ip1)

          get "/api/v2/users/me.json", headers: rest_headers.merge('REMOTE_ADDR' => @fake_ip1)
          assert_response :ok

          assert_equal 0, Prop.count(:api_unauthorized_requests, @fake_ip1)
        end

        # Tests that the filter order is fine, ie. we don't have 429 when the
        # endpoint requires authentication.
        it 'does not overide restriction access' do
          get "/api/v2/users.json", headers: { 'REMOTE_ADDR' => @fake_ip1 }
          assert_response 401
        end
      end
    end
  end

  def rest_headers
    api_basic_auth_headers(@admin.email, "123456")
  end
end
