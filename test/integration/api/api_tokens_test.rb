require_relative "../../support/test_helper"
require_relative "../../support/zendesk_dsl"
require_relative "../../support/api_test_helper"

class ApiTokensTest < ActionDispatch::IntegrationTest
  include ApiTestHelper
  include ZendeskDSL

  fixtures :all

  describe "token auth" do
    before do
      @account = accounts(:minimum)
      @owner   = @account.owner
      @tokens_api_token = @account.api_tokens.create!
      @tokens_api_token_value = @tokens_api_token.temp_full_value
    end

    def auth(token)
      ActionController::HttpAuthentication::Basic.encode_credentials("#{@owner.email}/token", token)
    end

    def whoami(token)
      url = "https://minimum.zendesk-test.com/api/v2/users/me.json"
      get(url, headers: { 'HTTP_AUTHORIZATION' => auth(token) })
      assert_response :success
      JSON.parse(response.body)
    end

    def assert_valid_token(token)
      json = whoami(token)
      assert_equal @owner.id, json['user']['id']
    end

    def refute_valid_token(token)
      url = "https://minimum.zendesk-test.com/api/v2/users/me.json"
      get(url, headers: { 'HTTP_AUTHORIZATION' => auth(token) })
      assert_response :unauthorized
    end

    describe 'switching token active' do
      before do
        @account.settings.api_token_access = true
        @account.save!
      end

      describe 'to false' do
        before do
          @tokens_api_token.active = false
          @tokens_api_token.save!
        end

        describe_with_arturo_disabled :restrict_unauthenticated_user do
          it 'will not return a valid user object' do
            json = whoami(@tokens_api_token_value)
            refute_equal @owner.id, json['user']['id']
          end
        end

        it 'should not authenticate' do
          refute_valid_token(@tokens_api_token_value)
        end
      end

      describe 'to true' do
        before do
          assert(@tokens_api_token.active)
        end

        it 'should authenticate' do
          assert_valid_token(@tokens_api_token_value)
        end
      end
    end

    describe 'switching token active' do
      before do
        @account.settings.api_token_access = true
        @account.save!
      end

      describe 'to false' do
        before do
          @tokens_api_token.active = false
          @tokens_api_token.save!
        end

        describe_with_arturo_disabled :restrict_unauthenticated_user do
          it 'will not return a valid user object' do
            json = whoami(@tokens_api_token_value)
            refute_equal @owner.id, json['user']['id']
          end
        end

        it 'should not authenticate' do
          refute_valid_token(@tokens_api_token)
        end
      end

      describe 'to true' do
        before do
          assert(@tokens_api_token.active)
        end

        it 'should authenticate' do
          assert_valid_token(@tokens_api_token_value)
        end
      end
    end

    describe "when api_token_access is off" do
      before do
        @account.settings.api_token_access = false
        @account.save!
      end

      describe_with_arturo_disabled :restrict_unauthenticated_user do
        it 'will not return a valid user object' do
          json = whoami(@tokens_api_token_value)
          refute_equal @owner.id, json['user']['id']
        end
      end

      it "does not accept api token from tokens table" do
        refute_valid_token(@tokens_api_token_value)
      end
    end

    describe "api_token_access is on" do
      before do
        @account.settings.api_token_access = true
        @account.save!
      end

      it "does not accept an empty token" do
        refute_valid_token(nil)
      end

      it "does not accept an invalid token" do
        refute_valid_token('bad-token')
      end

      it "does not accept the crypted token" do
        refute_valid_token(@tokens_api_token.crypted_value)
      end

      it "accepts api token from tokens table" do
        assert_valid_token(@tokens_api_token_value)
      end
    end
  end
end
