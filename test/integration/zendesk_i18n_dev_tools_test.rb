require_relative "../support/test_helper"
require 'zendesk_i18n_dev_tools'

files_with_default = %w[
  account_settings.yml
  account_setup.yml
  active_record_attributes.yml
  activities.yml
  admin.yml
  admin_automation.yml
  admin_macro.yml
  admin_trigger.yml
  admin_view.yml
  agent_classic.yml
  auth.yml
  channels_admin.yml
  chat.yml
  date_time.yml
  default_content.yml
  email.yml
  entries_page.yml
  entry_page.yml
  error_messages.yml
  facebook_integration.yml
  feedback_tab.yml
  forums_widget_page.yml
  hidden.yml
  inbound_mail.yml
  login_help_page.yml
  login_page.yml
  lotus_bootstrap.yml
  mail_subscription.yml
  merge_page.yml
  misc.yml
  moderation.yml
  onboarding.yml
  placeholders.yml
  recaptcha.yml
  registration_page.yml
  request_page.yml
  rules_admin.yml
  sample_tickets.yml
  satisfaction.yml
  search_page.yml
  security_policy.yml
  slas_admin.yml
  suggestions.yml
  ticket_object.yml
  timezones.yml
  user_page.yml
  user_settings.yml
  verification_page.yml
  via_types.yml
  widgets.yml
]

invalid_value_chars = %w[
  txt.admin.any_channels.views.registered_integration_service.heading.channel_integrations_service_name_2
  txt.admin.macro.page.edit.title
  txt.admin.trigger.page.edit.title
]

invalid_key_chars = %w[txt.admin.views.settings.extensions._target.are_you_sure?]

invalid_value_type_array = %w[
  date.abbr_day_names
  date.abbr_month_names
  date.day_names
  date.min_day_names
  date.month_names
  date.order
  number_format.numeric_symbols
]

invalid_value_type_boolean = %w[support.array.skip_last_comma]

# Only works if the same list is present in Rosetta (https://github.com/zendesk/rosetta/blob/master/config/initializers/zendesk_i18n_dev_tools.rb), so basically NEVER add to any of those lists.
# Teams should try to fix those files / keys.

paths = files_with_default.map { |file| "config/locales/translations/#{file}" }

ZendeskI18nDevTools::YamlValidator.default_package_allow_file_list           = paths
ZendeskI18nDevTools::YamlValidator.invalid_key_chars_allow_key_list          = invalid_key_chars
ZendeskI18nDevTools::YamlValidator.invalid_value_chars_allow_key_list        = invalid_value_chars
ZendeskI18nDevTools::YamlValidator.invalid_value_type_array_allow_key_list   = invalid_value_type_array
ZendeskI18nDevTools::YamlValidator.invalid_value_type_boolean_allow_key_list = invalid_value_type_boolean

describe 'English strings YML files validation' do
  it 'passes as expected' do
    ZendeskI18nDevTools::YamlValidator.validate!(['config/locales/translations'], fail_fast: false)
  end
end
