require_relative '../support/test_helper'

describe "rails compatibility Integration" do
  fixtures :all

  class RailsCompatibilityTestController < ActionController::Base
    def create_session
      session[:a] = 1
      session[:b] = [1, "two", :tree]
      flash['notice'] = 'foo'
      redirect_to '/'
    end
  end

  prepend ControllerDefaultParams
  integrate_test_routes RailsCompatibilityTestController

  describe 'session cookies' do
    it 'is serialized in a compatible way' do
      get('/test/route/rails_compatibility_test/create_session')
      assert_response :redirect

      session_cookie_value = cookies['_zendesk_test_Session']

      # still valid ?
      verifier = ActiveSupport::MessageVerifier.new(Zendesk::Configuration[:session_salt], serializer: Marshal)
      assert verifier.verify(session_cookie_value)

      # encoded using same tools ?
      decoded_cookie_value = Base64.decode64(session_cookie_value.split('--').first)
      unmarshalled_cookie_value = Marshal.load(decoded_cookie_value)
      unmarshalled_cookie_value.delete('session_id')

      expected_cookie_value = {
        "a" => 1,
        "b" => [1, "two", :tree],
        "flash" => {
          "discard" => [],
          "flashes" => {
            "notice" => "foo"
          }
        }
      }

      assert_equal expected_cookie_value, unmarshalled_cookie_value
    end
  end
end
