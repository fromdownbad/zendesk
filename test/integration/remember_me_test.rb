require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

describe "Remember Me Integration" do
  include ZendeskDSL
  fixtures :all

  before do
    @account = accounts(:minimum)
    @admin   = users(:minimum_admin)
  end

  describe "after login" do
    before do
      Timecop.freeze

      logs_in_as @admin, '123456', remember_me: 1
    end

    it "should set the duration to the max session duration" do
      assert_equal shared_session["auth_duration"], 24 * 60 * 60 * 14
    end

    it "should expire the shared_session record when the session expires" do
      expires_at = @admin.shared_sessions.where(session_id: shared_session.id).first.expire_at
      assert_equal expires_at, shared_session["auth_authenticated_at"] + shared_session["auth_duration"]
    end

    it "should not issue a _zendesk_remember_me cookie" do
      assert_nil cookies['_zendesk_remember_me']
    end
  end
end
