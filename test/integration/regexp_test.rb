require_relative "../support/test_helper"

describe 'Regexp' do
  it "tests urls" do
    valid = [
      'http://www.foo.com/',
      'http://goo.com',
      'http://www.foo.com:12345/',
      'http://example.foobar.com:1234/jay',
      'http://foo.bar/en/tre/to',
      'http://foo.bar.baz.longtld/'
    ]

    valid.each do |s|
      refute (s =~ URL_PATTERN).nil?
    end

    invalid = [
      'hej',
      'http:///'
    ]

    invalid.each do |s|
      assert_nil s =~ URL_PATTERN
    end
  end
end
