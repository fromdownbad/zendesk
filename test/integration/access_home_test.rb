require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class AccessHomeTest < ActionDispatch::IntegrationTest
  fixtures :all

  describe "go to home" do
    before do
      @account = accounts(:minimum)
      @account.stubs(:has_public_forums?).returns(true)
      @spanish = translation_locales(:spanish)
      @account.update_attribute('allowed_translation_locales', [ENGLISH_BY_ZENDESK, @spanish])
    end

    it "caches response of home" do
      HomeController.any_instance.expects(:index).once
      get "#{@account.url}/home"
      get "#{@account.url}/home"
    end

    it "renders with different locales correctly" do
      HomeController.any_instance.expects(:index).twice
      get "#{@account.url}/home"
      get "#{@account.url}/home?locale=2"
    end
  end
end
