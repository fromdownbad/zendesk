require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class LoggingOutFromVoiceTest < ActionDispatch::IntegrationTest
  fixtures :all

  describe "on an account with voice enabled" do
    before do
      Account.any_instance.stubs(:has_public_forums?).returns(false)
      Account.any_instance.stubs(:has_voice_enabled?).returns(true)

      @account = accounts(:minimum)
      @account.update_attribute(:host_mapping, "wibble.example.com")
    end

    describe "logging out when available for voice on client" do
      before do
        @agent = users(:minimum_agent)
        @agent.voice_number = "+14155551234"
        assert(login(@agent, remember_me: true))

        stub_request(:post, "https://minimum.zendesk-test.com/api/v2/channels/voice/availabilities/#{@agent.id}/logout.json").
          to_return(status: 200, body: "", headers: {})

        visit("https://minimum.zendesk-test.com/access/logout")
        follow_all_redirects!
      end

      it "is logged out on the unmapped domain" do
        visit("http://minimum.zendesk-test.com/users")
        headers["Location"].must_include('/access/unauthenticated')
        follow_all_redirects!
        assert_template 'auth/sso_v2_index'
      end
    end

    describe "logging out when available for voice on phone" do
      before do
        Account.any_instance.stubs(:has_voice_enabled?).returns(true)
        @agent = users(:minimum_agent)
        @agent.voice_number = "+14155551234"
        assert(login(@agent, remember_me: true))

        stub_request(:post, "https://minimum.zendesk-test.com/api/v2/channels/voice/availabilities/#{@agent.id}/logout.json").
          to_return(status: 200, body: "", headers: {})

        visit("https://minimum.zendesk-test.com/access/logout")
        follow_all_redirects!
      end

      it "is logged out on the unmapped domain" do
        visit("http://minimum.zendesk-test.com/users")
        headers["Location"].must_include('/access/unauthenticated')
        follow_all_redirects!
        assert_template 'auth/sso_v2_index'
      end
    end

    describe "logging out when user is anonymous user" do
      it "is okay" do
        Account.any_instance.stubs(:has_voice_enabled?).returns(true)
        visit("https://minimum.zendesk-test.com/access")
        visit("https://minimum.zendesk-test.com/access/logout")
        follow_all_redirects!
      end
    end
  end
end
