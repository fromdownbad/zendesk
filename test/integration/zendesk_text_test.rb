# Doing this here because zendesk_text and the app environments differ slightly.
# Possibly rails_xss is changing the behaviour of content_tag method and string
# concatination to affect how unsafe html strings are made safe.
# For more information check ticket https://support.zendesk.com/tickets/196792?col=347658
require_relative "../support/test_helper"
require 'zendesk_text/string_extensions'
require 'zendesk_text/linking'

class TestProxy
  include ZendeskText::Linking
  if ENV['USE_RAGEL']
    alias :auto_link_urls :auto_link_urls_ragel
    alias :auto_link_email_addresses :auto_link_email_addresses_ragel
  end
end

describe 'ZendeskAutoLink' do
  describe "Auto link a url" do
    describe "of the form &lt;url&gt;" do
      it "correctly autolink" do
        text = "&lt;http://www.zendesk.com&gt;"
        assert_equal '&lt;<a href="http://www.zendesk.com" rel="noreferrer">http://www.zendesk.com</a>&gt;', TestProxy.new.auto_link_urls(text)
      end
    end
  end
end
