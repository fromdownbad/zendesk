require_relative "../support/test_helper"

class ZendeskAuthAssetsTest < ActionDispatch::IntegrationTest
  let(:account) { accounts(:minimum) }
  let(:owner)   { account.owner }

  describe "auth/v2/host.js" do
    before { login(owner) }

    describe 'with auth_host_js_no_set_cookie disabled' do
      before { Arturo.disable_feature!(:auth_host_js_no_set_cookie) }

      it "returns a shared session set-cookie header" do
        get "https://minimum.zendesk-test.com/auth/v2/host.js"
        assert_match /_zendesk_authenticated/, response.headers['Set-Cookie']
      end
    end

    describe 'with auth_host_js_no_set_cookie enabled' do
      before { Arturo.enable_feature!(:auth_host_js_no_set_cookie) }

      it "does not return a shared session set-cookie header" do
        get "https://minimum.zendesk-test.com/auth/v2/host.js"
        assert_no_match /_zendesk_shared_session/, response.headers['Set-Cookie']
        assert_no_match /_zendesk_authenticated/, response.headers['Set-Cookie']
      end
    end
  end
end
