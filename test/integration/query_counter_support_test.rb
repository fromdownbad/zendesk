require_relative "../support/test_helper"

SingleCov.covered! file: 'test/support/query_counter_support.rb', uncovered: 20

describe 'query counter support' do
  describe 'n_plus_one_in_logs' do
    def publish(sql)
      ActiveSupport::Notifications.publish("sql.active_record", Time.now - 10.seconds, Time.now, 12, sql: sql, name: 'foo')
    end

    it 'detects n+1' do
      n_plus_one_in_logs do
        [
          "SELECT `forums`.* FROM `forums`  WHERE `forums`.`deleted_at` IS NULL AND `forums`.`account_id` = 90538  ORDER BY position ASC, id ASC /* Api::V2::SearchController#index */",
          "SELECT `forums`.* FROM `forums`  WHERE `forums`.`deleted_at` IS NULL AND `forums`.`account_id` = 90539  ORDER BY position ASC, id ASC /* Api::V2::SearchController#index */"
        ].each { |line| publish(line) }
      end.wont_equal []
    end

    it 'detects n+1 with ``' do
      n_plus_one_in_logs do
        [
          "SELECT `forums`.* FROM `forums`  WHERE `forums`.`deleted_at` IS NULL AND `forums`.`account_id` = 90538  ORDER BY position ASC, id ASC /* Api::V2::SearchController#index */",
          "SELECT forums.* FROM forums  WHERE forums.deleted_at IS NULL AND forums.account_id = 90538  ORDER BY position ASC, id ASC /* Api::V2::SearchController#index */"
        ].each { |line| publish(line) }
      end.wont_equal []
    end

    it 'detects n+1 with different case' do
      n_plus_one_in_logs do
        [
          "SELECT `forums`.* FROM `forums`  WHERE `forums`.`deleted_at` IS NULL AND `forums`.`account_id` = 90538  ORDER BY position ASC /* Api::V2::SearchController#index */",
          "SELECT `forums`.* FROM `forums`  WHERE `forums`.`deleted_at` IS NULL AND `forums`.`account_id` = 90538  ORDER BY position asc /* Api::V2::SearchController#index */"
        ].each { |line| publish(line) }
      end.wont_equal []
    end

    it 'detects multiple counts' do
      n_plus_one_in_logs do
        [
          "SELECT COUNT(*) FROM `ticket_forms` WHERE `ticket_forms`.`deleted_at` IS NULL AND `ticket_forms`.`account_id` = 513073",
          "SELECT COUNT(*) FROM `ticket_forms` WHERE `ticket_forms`.`deleted_at` IS NULL AND `ticket_forms`.`account_id` = 513073",
          "SELECT COUNT(*) FROM `ticket_forms` WHERE `ticket_forms`.`deleted_at` IS NULL AND `ticket_forms`.`account_id` = 513073"
        ].each { |line| publish(line) }
      end.wont_equal []
    end

    it 'is fine with IN' do
      n_plus_one_in_logs do
        [
          "select * from tickets where id IN (1, 2, 3)",
          "select * from tickets where id IN (4, 5, 6)"
        ].each { |line| publish(line) }
      end.must_equal []
    end

    it 'is fine if no n+1' do
      n_plus_one_in_logs { 'nothing to see' }.must_equal []
    end
  end
end
