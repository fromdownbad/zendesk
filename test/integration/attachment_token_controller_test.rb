require_relative "../support/test_helper"

class AttachmentTokenControllerTest < ActionDispatch::IntegrationTest
  fixtures :accounts, :attachments, :events, :tickets

  describe "with a valid jwe token" do
    let(:attachment_domain) { "https://p1.zdusercontent.com" }
    let(:account) { attachment.account }
    let(:user) { attachment.author }
    let(:token) { Zendesk::Attachments::Token.new(account: account, user: user, attachment: attachment) }
    let(:comment) { events(:create_comment_for_minimum_ticket_1) }
    let(:attachment) do
      attachments(:on_s3).tap { |a| a.update_attribute(:source, comment) }
    end

    before do
      StoresSynchronizationJob.stubs(:work) # prevent network calls from this job
      AttachmentTokenController.any_instance.stubs(via_nginx?: true)
      Attachment.any_instance.stubs(:audio?).returns(false)
      account.settings.private_attachments = true
      account.settings.save!
      assert account.settings.private_attachments?
    end

    def attachment_url(attachment)
      "#{attachment_domain}/attachment/#{attachment.account_id}/#{attachment.token}"
    end

    def assert_attachment_served
      assert_response :ok
      assert_equal "image/png", response.headers["Content-Type"]
      assert_equal "/s3/data/attachments/37536/s3.png", response.headers["X-Accel-Redirect"]
      assert_equal "inline; filename=\"s3.png\"", response.headers["Content-Disposition"]
    end

    it "requires the attachment domain" do
      get "#{account.url}/attachment/#{attachment.account_id}/#{attachment.token}", params: { token: token.encrypt }
      assert_response :forbidden
    end

    describe "account on a different pod" do
      it "redirects to the correct pod's URL" do
        account.update_attribute(:shard_id, 3) # move account to pod 2
        t = token.encrypt
        get(attachment_url(attachment), params: { token: t })
        assert_redirected_to "https://p2.zdusercontent.com/attachment/90538/89734692873?token=#{t}"
      end
    end

    describe "on the attachment domain" do
      before do
        RoutingErrorsController.any_instance.stubs(:current_brand).returns(account.brands.first)
      end

      it "redirects with an expired token" do
        t = token.encrypt
        Timecop.travel(30.minutes.from_now) do
          get(attachment_url(attachment), params: { token: t })
          assert_redirected_to "https://minimum.zendesk-test.com/attachments/token/89734692873/?name=s3.png"
        end
      end

      it "fails with an expired token if there isn't an attachment_token" do
        t = token.encrypt
        Timecop.travel(30.minutes.from_now) do
          get(attachment_domain + "/attachment/foo", params: { token: t })
          assert_response :not_found
          assert_nil response.headers["X-Accel-Redirect"]
        end
      end

      it "fails with an expired token and an invalid attachment token" do
        t = token.encrypt
        Timecop.travel(30.minutes.from_now) do
          get("#{attachment_domain}/attachment/#{attachment.account_id}/foo", params: { token: t })
          assert_response :not_found
          assert_nil response.headers["X-Accel-Redirect"]
        end
      end

      it "fails with old style links" do
        t = token.encrypt
        get("#{attachment_domain}/attachment/foo", params: { token: t })
        assert_response :not_found
      end

      it "redirects with an token issued in the future" do
        t = token.encrypt
        Timecop.travel(30.minutes.ago) do
          get(attachment_url(attachment), params: { token: t })
          assert_redirected_to "https://minimum.zendesk-test.com/attachments/token/89734692873/?name=s3.png"
        end
      end

      it "doesn't attempt to load the attachment if there isn't an account" do
        AttachmentTokenController.any_instance.stubs(:current_brand).returns(account.brands.first)
        Attachment.expects(:where).never
        get("#{attachment_domain}/attachment/777/foo", params: { token: 'invalid' })
        assert_response :not_found
      end

      it "redirects with an invalid token" do
        get(attachment_url(attachment), params: { token: 'bar' })
        assert_redirected_to "https://minimum.zendesk-test.com/attachments/token/89734692873/?name=s3.png"
      end

      it "redirects with a tampered token" do
        get(attachment_url(attachment), params: { token: token.encrypt[0...-1] }) # remove last character from token
        assert_redirected_to "https://minimum.zendesk-test.com/attachments/token/89734692873/?name=s3.png"
      end

      it "can render an error message with the mobile layout" do
        get("#{attachment_domain}/attachment/1/123", headers: { "HTTP_USER_AGENT" => 'iphone' })
        assert_response :not_found
      end

      describe "with public attachments" do
        before do
          account.settings.private_attachments = false
          account.settings.save!
        end

        it "serves the attachment with an expired token if attachments are public" do
          t = token.encrypt
          Timecop.travel(30.minutes.from_now) do
            get(attachment_url(attachment), params: { token: t })
            assert_attachment_served
          end
        end

        it "redirects if the user can't view the attachment and the token is expired" do
          t = token.encrypt
          Timecop.travel(30.minutes.from_now) do
            User.any_instance.expects(:can?).with(:view, attachment).returns(false).once
            get(attachment_url(attachment), params: { token: t })
            assert_redirected_to "https://minimum.zendesk-test.com/attachments/token/89734692873/?name=s3.png"
          end
        end

        it "redirects with an invalid token" do
          get(attachment_url(attachment), params: { token: 'bar' })
          assert_redirected_to "https://minimum.zendesk-test.com/attachments/token/89734692873/?name=s3.png"
        end

        it "redirects with a tampered token" do
          get(attachment_url(attachment), params: { token: token.encrypt[0...-1] }) # remove last character from token
          assert_redirected_to "https://minimum.zendesk-test.com/attachments/token/89734692873/?name=s3.png"
        end
      end

      describe "with a valid token" do
        it "sets the X-Accel-Redirect headers for nginx" do
          get(attachment_url(attachment), params: { token: token.encrypt })
          assert_attachment_served
        end

        it "refuses to use the same token again but redirects to get another valid token" do
          get(attachment_url(attachment), params: { token: token.encrypt })
          assert_attachment_served

          get(attachment_url(attachment), params: { token: token.encrypt })
          assert_redirected_to "https://minimum.zendesk-test.com/attachments/token/89734692873/?name=s3.png"
        end

        it "sets the X-Access-Control-Allow-Origin to *" do
          get(attachment_url(attachment), params: { token: token.encrypt })
          assert_equal "*", response.headers["X-Access-Control-Allow-Origin"]
        end

        it "is allowed with a mobile app user agent" do
          get(attachment_url(attachment), params: { token: token.encrypt }, headers: { 'HTTP_USER_AGENT' => Zendesk::Accounts::Source::IPHONE })

          assert_attachment_served
        end
      end

      describe "with ip restrictions" do
        before do
          Subscription.any_instance.stubs(:has_ip_restrictions?).returns(true)
          account.settings.ip_restriction_enabled = '1'
          account.settings.enable_agent_ip_restrictions = '1'
          account.texts.ip_restriction = "1.1.1.1"
          account.save!
        end

        describe "for agents" do
          let(:user) { users(:minimum_agent) }

          it "allows access from a whitelisted ip" do
            get(attachment_url(attachment), params: { token: token.encrypt }, headers: { 'REMOTE_ADDR' => '1.1.1.1' })
            assert_attachment_served
          end

          it "denies access from a non whitelisted ip" do
            get(attachment_url(attachment), params: { token: token.encrypt }, headers: { 'REMOTE_ADDR' => '2.2.2.2' })
            assert_response :forbidden
          end
        end

        describe "for end users" do
          let(:user) { users(:minimum_end_user) }

          it "allow access" do
            get(attachment_url(attachment), params: { token: token.encrypt }, headers: { 'REMOTE_ADDR' => '2.2.2.2' })
            assert_attachment_served
          end

          it "denies access if ip restrictions apply to end users" do
            account.settings.enable_agent_ip_restrictions = '0'
            account.save!

            get(attachment_url(attachment), params: { token: token.encrypt }, headers: { 'REMOTE_ADDR' => '2.2.2.2' })
            assert_response :forbidden
          end
        end

        describe "for anonymous users" do
          let(:user) { account.anonymous_user }

          it "allows access" do
            get(attachment_url(attachment), params: { token: token.encrypt }, headers: { 'REMOTE_ADDR' => '2.2.2.2' })
            assert_attachment_served
          end

          it "denies access if ip restrictions apply to end users" do
            account.settings.enable_agent_ip_restrictions = '0'
            account.save!

            get(attachment_url(attachment), params: { token: token.encrypt }, headers: { 'REMOTE_ADDR' => '2.2.2.2' })
            assert_response :forbidden
          end
        end
      end
    end
  end
end
