require_relative "../support/test_helper"

describe 'Mysql' do
  it "is running mysql with a UTC timezone" do
    assert ActiveRecord::Base.connection.select_value("select now() = utc_timestamp()").to_i == 1,
      "Your mysql is not running in a UTC timezone.  This will cause problems.
        Please open up my.cnf (generally in etc or your homebrew directory) and add the following
        lines:

        [mysqld_safe]
        timezone=UTC"
  end
end
