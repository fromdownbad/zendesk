require_relative '../../../support/test_helper'

describe Zendesk::InboundMail::TicketProcessingJob do
  include MailTestHelper

  fixtures :accounts

  before do
    accounts(:support)
  end

  let(:account) do
    account = accounts(:minimum)
    account.domain_blacklist = ''
    account.save!

    field_subject = FieldSubject.new
    account.stubs(:field_subject).returns(field_subject)

    account
  end
  let(:attributes) do
    options = {
      account_id: account.id
    }
    recipient = options[:recipient] || account.try(:reply_address)
    {
      mail: mail,
      account_id: options[:account_id],
      route_id: options[:route_id],
      recipient: recipient
    }
  end

  describe 'When it is a new author' do
    describe 'when the new author is valid' do
      before do
        @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes)
      end

      let(:mail) do
        mail = FixtureHelper::Mail.read('html_gmail_reply.json')
        mail.stubs(account_id: account.id, account: account)
        mail
      end

      it 'creates the author' do
        assert @state.author.id
      end
    end

    describe 'when a user identity already exists for the author address' do
      let(:user) { users(:minimum_end_user) }

      before do
        address = mail.from.address
        conflict_user = users(:minimum_end_user2)

        # The purpose of this setup is to emulate a race condition where the running process does not find
        # a previously created user for the author, but experiences a failure saving a new user to the DB
        # because an email identity already existing for that author. When the error is caught the process attempts
        # to recover by 'repairing' the author with the pre-existing user.

        UserEmailIdentity.create!(account: account, value: address, user: user)
        # We overwrite ActiveModel#delete so have to reference the class method
        conflict_user.class.delete(conflict_user.id)

        Zendesk::InboundMail::TicketProcessingJob.any_instance.stubs(:account).returns(account)
        account.stubs(:find_user_by_email).with(address, anything).returns(nil, nil, nil, nil, user)

        @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes)
      end

      let(:mail) do
        mail = FixtureHelper::Mail.read('html_gmail_reply.json')
        mail.stubs(account_id: account.id, account: account)
        mail
      end

      it 'repairs the author with an existing user' do
        assert @state.author.id
        assert_equal user, @state.ticket.requester
      end
    end
  end
end
