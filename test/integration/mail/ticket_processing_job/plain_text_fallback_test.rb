require 'zendesk_comment_markup'
require_relative '../../../support/test_helper'

describe Zendesk::InboundMail::TicketProcessingJob do
  include MailTestHelper

  fixtures :accounts, :tickets

  let(:account)           { accounts(:minimum) }
  let(:ticket)            { tickets(:minimum_1) }
  let(:requester_address) { address(ticket.requester.email) }
  let(:attributes) do
    options = {
      account_id: account.id
    }
    recipient = options[:recipient] || account.try(:reply_address)
    {
      mail: mail,
      account_id: options[:account_id],
      route_id: options[:route_id],
      recipient: recipient
    }
  end
  let(:mail) do
    mail = FixtureHelper::Mail.read('html_gmail_reply.json')
    mail.stubs(account_id: account.id, account: account, from: requester_address, reply_to: requester_address)
    mail
  end

  before { Zendesk::InboundMail::MailParsingQueue::ProcessingState.any_instance.stubs(from: mail.from) }

  describe 'With rich_content_in_emails enabled' do
    before do
      account.settings.enable(:rich_content_in_emails)
      account.save!
    end

    describe 'with an existing ticket' do
      before { Zendesk::InboundMail::Processors::TicketFinderProcessor.any_instance.stubs(ticket_for_update: ticket) }

      describe 'when ZendeskCommentMarkup.filter_input fails' do
        describe 'with a TypeError' do
          before do
            ZendeskCommentMarkup.stubs(:filter_input).raises(TypeError.new("no implicit conversion of nil into String"))
            @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes)
          end

          it 'falls back to plain text' do
            ticket.comments.last.body.must_equal @state.plain_content
            refute ticket.comments.last.rich?
          end
        end

        describe 'with a NoMethodError' do
          before do
            ZendeskCommentMarkup.stubs(:filter_input).raises(NoMethodError.new("undefined method `strip' for nil:NilClass", 'strip'))
            @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes)
          end

          it 'falls back to plain text' do
            ticket.comments.last.body.must_equal @state.plain_content
            refute ticket.comments.last.rich?
          end
        end
      end

      describe 'when ZendeskCommentMarkup.filter_input succeeds' do
        before { @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes) }

        it 'does not fall back to plain text' do
          # just checking that some of the correct HTML is there, rather than copying in the entire sanitized HTML
          ticket.comments.last.html_body.html_must_include "<div dir=\"ltr\">Celebrate with <b>more </b>HTML!<div>"
          assert ticket.comments.last.rich?
        end
      end
    end
  end
end
