require_relative "../../../support/test_helper"

describe Zendesk::InboundMail::TicketProcessingJob do
  include MailTestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }

  let(:mail) do
    mail = FixtureHelper::Mail.read("basic.json")
    mail.stubs(account_id: account.id, account: account)
    mail.reply_to.stubs(address: "noreply@zopim.com")
    mail
  end

  let(:attributes) do
    {
      mail: mail,
      account_id: account.id,
      route_id: nil,
      recipient: "support@minimum.zendesk-test.com"
    }
  end

  before { Zendesk::InboundMail::MailParsingQueue::ProcessingState.any_instance.stubs(from: mail.reply_to) }

  describe "Notification email from Zopim to Zendesk" do
    subject { Zendesk::InboundMail::TicketProcessingJob.work(attributes) }

    it "allows the normally blacklisted email noreply@zopim.com to be the ticket requester" do
      assert_equal "noreply@zopim.com", subject.ticket.requester.email
    end
  end
end
