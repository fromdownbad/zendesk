require_relative '../../../support/test_helper'

describe Zendesk::InboundMail::TicketProcessingJob do
  include MailTestHelper

  fixtures :accounts

  describe_with_arturo_disabled :email_suspend_mail_from_auto_response do
    before do
      accounts(:support)
      stub_request(:head, %r{\.amazonaws\.com/data/attachments/368793/564510a7-9631-46f8-b091-28ebdb002a14/attachment_0_heatwave\.gif})
      stub_request(:put, %r{\.amazonaws\.com/data/attachments})
    end

    let(:account) do
      account = accounts(:minimum)
      account.domain_blacklist = ''
      account.save!

      field_subject = FieldSubject.new
      Account.any_instance.stubs(:field_subject).returns(field_subject)

      account
    end

    let(:attributes) do
      options = {
        account_id: account.id
      }
      recipient = options[:recipient] || account.try(:reply_address)
      {
        mail: mail,
        account_id: options[:account_id],
        route_id: options[:route_id],
        recipient: recipient
      }
    end

    describe 'the email is sent by an agent on the account' do
      describe 'the email contains text api commands' do
        describe 'the email has content following the text api commands' do
          describe 'the email is not a forward' do
            before do
              @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes)
            end

            let(:mail) do
              mail = FixtureHelper::Mail.read('text_api_agent.json')
              mail.stubs(account_id: account.id, account: account)
              mail
            end

            it 'sets the ticket assignee' do
              agent = account.agents.find { |agt| agt.email == 'minimum_agent@aghassipour.com' }

              assert_equal agent, @state.ticket.assignee
            end

            it 'sets the ticket priority' do
              assert_equal 'High', @state.ticket.priority
            end

            it 'sets the ticket status' do
              assert_equal 'Pending', @state.ticket.status
            end

            it 'sets the comment as public' do
              assert(@state.ticket.comments.first.is_public?)
            end

            it 'sets the ticket requester' do
              user = account.users.find { |usr| usr.email == 'minimum_end_user@aghassipour.com' }
              assert_equal user, @state.ticket.requester
            end

            it 'sets the ticket tags' do
              assert_equal 'up what', @state.ticket.current_tags
            end

            it 'sets the comment body to the email body stripped of text api commands' do
              assert_equal("This is a pretty basic email fixture.\n\nThis is using the text API.",
                @state.ticket.comments.last.body)
            end
          end

          describe 'the email is a forward from the agent on behalf of an end user' do
            let(:mail) do
              mail = FixtureHelper::Mail.read('text_api_agent_forward.json')
              mail.stubs(account_id: account.id, account: account)
              mail
            end

            before do
              account.settings.agent_forwardable_emails = true
              account.settings.save!
              @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes)
            end

            it 'sets the ticket status' do
              assert_equal 'Pending', @state.ticket.status
            end

            it 'sets the comment as public' do
              assert(@state.ticket.comments.first.is_public?)
            end

            it 'creates two comments' do
              assert_equal 2, @state.ticket.comments.count
            end

            it 'creates a comment with the agent content' do
              assert_equal "meow\nmeow\nmeow\nmeow", @state.ticket.comments.last.body
            end

            it 'creates a comment with the user content' do
              assert_equal 'test 3', @state.ticket.comments.first.body
            end
          end
        end

        describe 'the email does not have content following the text api commands' do
          describe 'the email is not a forward' do
            describe 'the email has attachments' do
              let(:mail) do
                mail = FixtureHelper::Mail.read('text_api_agent_with_attachments.json')
                mail.stubs(account_id: account.id, account: account)
                mail
              end

              before do
                # attachments.yml has :fs as default in test
                Attachment.stubs(default_s3_stores: [:s3])
                @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes)
              end

              it 'does not reject the email' do
                assert @state.ticket
              end
            end

            describe 'the email has no attachments' do
              let(:mail) do
                mail = FixtureHelper::Mail.read('text_api_agent_no_plain_content.json')
                mail.stubs(account_id: account.id, account: account)
                mail
              end

              before do
                @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes)
              end

              it 'hard rejects the email' do
                refute @state.ticket
              end
            end
          end

          describe 'the email is a forward from the agent on behalf of an end user' do
            before do
              account.settings.agent_forwardable_emails = true
              account.settings.save!
            end

            describe 'the email has attachments' do
              let(:mail) do
                mail = FixtureHelper::Mail.read('text_api_agent_forward_with_attachments.json')
                mail.stubs(account_id: account.id, account: account)
                mail
              end

              before do
                # attachments.yml has :fs as default in test
                Attachment.stubs(default_s3_stores: [:s3])
                mail.attachments.stubs(blank?: false)
                @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes)
              end

              it 'does not reject the email' do
                assert @state.ticket
              end
            end

            describe 'the email has no attachments' do
              let(:mail) do
                mail = FixtureHelper::Mail.read('text_api_agent_forward_blank.json')
                mail.stubs(account_id: account.id, account: account)
                mail
              end

              before do
                @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes)
              end

              it 'hard rejects the email' do
                refute @state.ticket
              end
            end
          end
        end
      end
    end

    describe 'the email is sent by an end user' do
      describe 'the email does contain text api commands' do
        describe 'end users are not processed' do
          let(:mail) do
            mail = FixtureHelper::Mail.read('text_api.json')
            mail.stubs(account_id: account.id, account: account)
            mail
          end

          before do
            @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes)
          end

          it 'sets the comment body to the email body including text api commands' do
            assert_equal(mail.body.strip, @state.ticket.comments.last.body)
          end
        end
      end
    end
  end
end
