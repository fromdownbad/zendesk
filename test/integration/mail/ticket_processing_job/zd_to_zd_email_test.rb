require_relative '../../../support/test_helper'

describe Zendesk::InboundMail::TicketProcessingJob do
  include MailTestHelper

  fixtures :accounts

  describe_with_arturo_disabled :email_suspend_mail_from_auto_response do
    before { accounts(:support) }

    let(:account) do
      account = accounts(:minimum)
      account.domain_blacklist = ''
      account.save!

      field_subject = FieldSubject.new
      Account.any_instance.stubs(:field_subject).returns(field_subject)

      account
    end

    let(:attributes) do
      options = {
        account_id: account.id
      }
      recipient = options[:recipient] || account.try(:reply_address)
      {
        mail: mail,
        account_id: options[:account_id],
        route_id: options[:route_id],
        recipient: recipient
      }
    end

    let(:expected_collaborators) do
      %w[cgwippern@example.com admin@hostmap.com]
    end

    describe 'Notification email between two Zendesk instances' do
      let(:mail) do
        mail = FixtureHelper::Mail.read('zd_to_zd.eml.json')
        mail.stubs(account_id: account.id, account: account)
        mail
      end

      before { @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes) }

      it 'does not add the reply-to as a ticket collaborator if it is a +id address for the receiving account' do
        collaborators = @state.ticket.collaborators || []

        assert_equal expected_collaborators.sort, collaborators.map { |c| c.try(:email) }.sort
      end

      it 'it strips the reply-to address of any +id and sets as the ticket requester' do
        assert_equal 'support@support.zendesk-test.com', @state.ticket.requester.email
      end
    end

    describe 'Notification email between two Zendesk instances where originator has hostmapped domain' do
      let(:mail) do
        mail = FixtureHelper::Mail.read('zd_to_zd_2.eml.json')
        mail.stubs(account_id: account.id, account: account)
        mail
      end

      before { @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes) }

      it 'does not add the reply-to as a ticket collaborator if it is a +id address for the receiving account' do
        collaborators = @state.ticket.collaborators || []

        assert_equal expected_collaborators.sort, collaborators.map { |c| c.try(:email) }.sort
      end

      it 'it strips the reply-to address of any +id and sets as the ticket requester' do
        assert_equal 'support@support.zendesk-test.com', @state.ticket.requester.email
      end
    end

    describe 'Notification email between two Zendesk instances orignating from a linked gmail account' do
      let(:mail) do
        mail = FixtureHelper::Mail.read('zd_to_zd_gmail.eml.json')
        mail.stubs(account_id: account.id, account: account)
        mail
      end

      before { @state = Zendesk::InboundMail::TicketProcessingJob.work(attributes) }

      it 'does not add the reply-to as a ticket collaborator if it is a +id address for the receiving account' do
        collaborators = @state.ticket.collaborators || []

        assert_equal expected_collaborators.sort, collaborators.map { |c| c.try(:email) }.sort
      end

      it 'it strips the reply-to address of any +id and sets as the ticket requester' do
        assert_equal 'support-test@hostmap.com', @state.ticket.requester.email
      end
    end
  end
end
