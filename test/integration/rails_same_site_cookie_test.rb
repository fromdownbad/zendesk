require_relative "../support/test_helper"

class RailsSameSiteCookie::Middleware::App
  def call(_env)
    response = ActionDispatch::Response.new(422, {}, "Hey")
    [422, {'Set-Cookie' => 'test_cookie=foo'}, response]
  end
end

describe "Zendesk::RailsSameSiteCookie::Middleware Integration" do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:metadata) do
    {
      'request_id' => 'asdf1234',
      'rack.session.options' => { skip: nil },
      'zendesk.account' => account
    }
  end
  let(:env) { ActiveSupport::HashWithIndifferentAccess.new(metadata) }
  let(:app) { RailsSameSiteCookie::Middleware::App.new }

  it 'adds SameSite=None to the cookies' do
    response = RailsSameSiteCookie::Middleware.new(app).call(env)
    response[1]['Set-Cookie'].must_equal "test_cookie=foo; SameSite=None"
  end
end
