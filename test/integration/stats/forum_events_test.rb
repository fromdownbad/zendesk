require_relative "../integration_test_helper"

describe 'ForumEvents' do # FactTest
  fixtures :entries, :accounts, :users, :forums, :subscriptions

  it "tests forum reassignment" do
    # don't run unless you have mongo etc.
    return true unless @suite_enabled

    # currently in "solutions" forum
    entry = entries(:ponies)
    entry.forum_stats.create!(ts: Time.at(3600), duration: 3600, value: 5, type: "entry_view")
    entry.reload
    assert_equal 1, entry.forum_stats.to_a.size

    entry.forum = forums(:announcements)
    entry.save!
    entry.reload
    assert_equal 1, entry.forum_stats.to_a.size
    assert_equal 1, entry.forum_stats.by_entry.to_a.size
    assert_equal forums(:announcements).id, entry.forum_stats.by_entry.first.forum_id
  end
end
