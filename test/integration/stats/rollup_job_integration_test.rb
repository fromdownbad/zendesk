require_relative "../integration_test_helper"
require "zendesk_stats/test_helper/stats_integration"

describe Zendesk::StatRollup::RollupJob do
  before do
    # return true unless @suite_enabled

    ActiveRecord::Base.default_shard = 1 # just 1 shard
    StatRollup::Ticket.without_arsi.delete_all
    @fact_klass = Zendesk::ClassicStats::TicketEvent
    @rollup_job = Zendesk::StatRollup::RollupJob.new

    input = [
      {ticket_id: 1, type: 'create', ts: 0, account_id: 1, group_id: 1, organization_id: 55, via_id: 0},
      {ticket_id: 2, type: 'create', ts: 0, account_id: 1, group_id: 2, organization_id: 65, via_id: 0}
    ]

    clear_queue_and_dbs

    @jobex = Zendesk::Stats::TestHelper::JobExecution.new(@fact_klass)
    @jobex.load_input(input, true, true, true)

    stub_time(input, Zendesk::ClassicStats::TicketEvent)
  end

  it "produces some database rows" do
    @rollup_job.do_work("Zendesk::ClassicStats::TicketEvent", "backfill", 1)
    assert_equal 2, StatRollup::Ticket.count(:all)

    # validate
    ts = 0
    expected_output = [
      { account_id: 1, agent_id: 0, duration: 3600, type: "created_count", ts: ts, value: 2.0, group_id: 0, organization_id: 0 },
      { account_id: 1, agent_id: 0, duration: 3600, type: "created_count_via_0", ts: ts, value: 2.0, group_id: 0, organization_id: 0 }
    ]

    @jobex.output = # [ ActiveRecord ] => [ hash ]
      StatRollup::Ticket.all.map do |r|
        {
          account_id: r.account_id,
          agent_id: r.agent_id,
          duration: r.duration,
          type: r.type,
          ts: r.ts,
          value: r.value,
          group_id: r.group_id,
          organization_id: r.organization_id
        }
      end
    @jobex.validate_output_contains expected_output
  end

  it "drops an entry in memcache denoting the high-watermark for the job" do
    @rollup_job.do_work("Zendesk::ClassicStats::TicketEvent", "backfill", 1)
    assert_equal 2, StatRollup::Ticket.count(:all)

    # NOTE: Rails.cache not available; remove reference to last_rollup in waterline
    # assert_equal Time.now.to_i, Rails.cache.fetch("last_rollup/stat_rollup/ticket/backfill/1")
  end

  private

  def clear_queue_and_dbs(skip_redis = false)
    Zendesk::Stats::Connection.on_all_shards do |db, _shard_id|
      db.drop
    end

    Zendesk::Stats::Queue.clear unless skip_redis
  end

  def stub_time(input, fact_klass, at = nil)
    # here we pretend it's a couple hours after the last stat-row was generated -- so we don't map-reduce all the
    # way to the real Time.now.
    max_ts_in_set = input.map { |r| r[:ts].to_i }.max
    max_window_in_jobs = fact_klass.jobs.map(&:window).max
    Timecop.freeze at || Time.at(max_ts_in_set + max_window_in_jobs * 2)
  end
end
