require_relative "../integration_test_helper"

describe Zendesk::StatRollup::RollupJob do
  class TestFact < Zendesk::Stats::FactItem
    set_collection_name "test_fact"

    def self.job1
      @job1 ||= register_job(Zendesk::Stats::MapReduce::CountJob.new("flexmaster_test_job").for(:account_id).window(:hourly))
    end

    def self.remove_job1
      remove_job(@job1.name)
    end
  end

  describe "While running" do
    before do
      @rollup_job = Zendesk::StatRollup::RollupJob.new
    end

    it "correctly invokes map reduce" do
      ActiveRecord::Base.expects(:on_shard).once.with(1).yields
      TestFact.expects(:map_reduce_job).with(TestFact.job1, 1, is_a(Array))
      @rollup_job.do_work("TestFact", "flexmaster_test_job", 1)
      TestFact.remove_job1.name # cleanup
    end

    it "logs without crash" do
      assert @rollup_job.args_to_log("Fact1", "job1", 100)
    end
  end
end
