require_relative '../support/test_helper'

describe Zendesk::Auth::User do
  include CustomFieldsTestHelper

  fixtures :accounts, :organizations, :users

  let(:account) { user.account }
  let(:user) { users(:minimum_end_user) }
  let(:auth_user) { Zendesk::Auth::User.new(user.account, user) }

  describe "#build_email_identity" do
    it "builds identity owned by user" do
      identity = auth_user.build_email_identity(value: 'abc@abc.com')
      assert_equal user, identity.user
      assert_equal 'abc@abc.com', identity.value
    end
  end

  describe "#name=" do
    it "sets the name" do
      auth_user.name = "Hello!"
      assert_equal "Hello!", user.name
    end
  end

  describe "#role=" do
    it "sets the agent role" do
      auth_user.role = "agent"
      assert_equal "Agent", user.role
    end
  end

  describe "#custom_role_id=" do
    it "sets the custom_role_id" do
      auth_user.custom_role_id = "8"
      assert_equal 8, user.custom_role_id
    end
  end

  describe "in a multiple organizations account" do
    before do
      account.stubs(has_multiple_organizations_enabled?: true)

      @default_organizations = user.organizations.to_a
      @default_organization = user.organization
    end

    describe "#set_organization_by_external_id" do
      it "adds the organization if it is valid" do
        organization = organizations(:minimum_organization3)
        auth_user.organization_by_external_id = organization.external_id
        auth_user.save!
        user.reload

        assert_includes(user.organizations, organization)
      end

      it "does not change the default organization" do
        organization = organizations(:minimum_organization3)
        auth_user.organization_by_external_id = organization.external_id
        auth_user.save!
        user.reload

        assert_equal @default_organization, user.organization
      end

      it "ignores the organization if it is not valid" do
        assert_not_change_organizations { auth_user.organization_by_external_id = 'invalid-organization-id' }
      end

      it "ignores the organization if it is nil" do
        assert_not_change_organizations { auth_user.organization_by_external_id = nil }
      end
    end

    describe "#set_organizations" do
      it "adds the organizations" do
        organization = organizations(:minimum_organization3)
        organization2 = organizations(:minimum_organization2)
        auth_user.organizations = [organization.id.to_s, organization2.id.to_s]
        auth_user.save!
        user.reload

        assert_includes(user.organizations, organization)
        assert_includes(user.organizations, organization2)
        assert_equal 2, user.organizations.length
      end
    end

    describe "#set_organization" do
      let(:organization) { organizations(:minimum_organization3) }

      it "adds the organization if it is valid" do
        auth_user.organization = organization.name
        auth_user.save!
        user.reload

        assert_includes(user.organizations, organization)
      end

      it "does not change the default organization" do
        auth_user.organization = organization.name
        auth_user.save!
        user.reload

        assert_equal @default_organization, user.organization
      end

      it "ignores the organization if it is not valid" do
        assert_not_change_organizations { auth_user.organization = 'invalid-organization' }
      end

      it "ignores the organization if it is nil" do
        assert_not_change_organizations { auth_user.organization = nil }
      end
    end
  end

  describe "in a single organization account" do
    before do
      account.stubs(has_multiple_organizations_enabled?: false)

      assert @default_organizations = user.organizations.to_a
      assert @default_organization = user.organization
    end

    describe "#set_organization_by_external_id" do
      it "adds the organization if it is valid" do
        organization = organizations(:minimum_organization3)
        auth_user.organization_by_external_id = organization.external_id
        auth_user.save!
        user.reload

        assert_equal [organization], user.organizations
        assert_equal organization, user.organization
      end

      it "ignores the organization if it is not valid" do
        assert_not_change_organizations { auth_user.organization_by_external_id = 'invalid-organization-id' }
      end

      it "ignores the organization if it is nil" do
        assert_not_change_organizations { auth_user.organization_by_external_id = nil }
      end
    end

    describe "#set_organization" do
      it "adds the organization if it is valid" do
        organization = organizations(:minimum_organization3)
        auth_user.organization = organization.name
        auth_user.save!
        user.reload

        assert_equal [organization], user.organizations
        assert_equal organization, user.organization
      end

      it "ignores the organization if it is not valid" do
        assert_not_change_organizations { auth_user.organization = 'invalid-organization' }
      end

      it "ignores the organization if it is nil" do
        assert_not_change_organizations { auth_user.organization = nil }
      end
    end
  end

  describe "#set_tags=" do
    before do
      account.settings.has_user_tags = true
      account.settings.save!
    end

    it "sets tags" do
      auth_user.set_tags = %w[tag1 tag2]

      assert_equal "tag1 tag2", user.current_tags
    end
  end

  describe "#phone=" do
    it "sets the user's phone number" do
      auth_user.phone = "123456"

      assert_equal "123456", user.phone
    end
  end

  describe "#remote_photo_url=" do
    it "sets the remote_photo_url" do
      auth_user.remote_photo_url = "http://www.google.com/image.gif"

      assert_equal "http://www.google.com/image.gif", user.remote_photo_url
    end
  end

  describe "#custom_fields=" do
    before do
      # For the helper
      @account = account

      create_user_custom_field!("text_field", "Text Field", "Text")
      create_user_custom_field!("checkbox_field", "Checkbox Field", "Checkbox")
    end

    it "sets the custom fields if they have valid keys" do
      auth_user.custom_fields = { "text_field" => "abc", "checkbox_field" => true }
      user.custom_field_values.value_for_key("text_field").value.must_equal('abc')
      user.custom_field_values.value_for_key("checkbox_field").value.must_equal('t')
    end

    it "updates the custom fields if they have valid keys" do
      user.custom_field_values.update_from_hash("text_field" => "old", "checkbox_field" => false)
      auth_user.custom_fields = { "text_field" => "abc", "checkbox_field" => true }
      user.custom_field_values.value_for_key("text_field").value.must_equal('abc')
      user.custom_field_values.value_for_key("checkbox_field").value.must_equal('t')
    end

    it "silently handle custom fields if they have invalid keys" do
      user.custom_field_values.update_from_hash("text_field" => "old")
      auth_user.custom_fields = { "text_field" => "abc", "invalid_key" => true }
      user.custom_field_values.value_for_key("text_field").value.must_equal('abc')
    end

    it "silently update custom fields if field validation fails" do
      create_user_custom_field!("integer_field", "Integer Field", "Integer")
      user.custom_field_values.update_from_hash("text_field" => "old")
      auth_user.custom_fields = { "text_field" => "abc", "integer_field" => "invalid" }
      user.custom_field_values.value_for_key("text_field").value.must_equal('abc')
      user.custom_field_values.value_for_key("integer_field").must_be_nil
    end
  end

  def assert_not_change_organizations
    yield

    auth_user.save!
    user.reload

    assert_equal @default_organization, user.organization
    assert_equal @default_organizations, user.organizations
  end
end
