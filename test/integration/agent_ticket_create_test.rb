require_relative "../support/test_helper"

class AgentTicketCreateTest < ActionDispatch::IntegrationTest
  describe "An agent creating a new ticket" do
    fixtures :all

    before do
      @account = accounts(:minimum)
      @agent   = users(:minimum_admin)

      login(@agent)
    end

    it "redirects to new help center request" do
      get "#{@account.url}/tickets/new"
      assert_response :redirect
      assert_redirected_to "#{@account.url}/requests/new"
      follow_redirect!
      assert_redirected_to "#{@account.url}/hc/requests/new"
      assert_response :redirect
    end
  end
end
