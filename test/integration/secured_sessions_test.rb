require_relative "../support/test_helper"

class SecuredSessionsTest < ActionDispatch::IntegrationTest
  fixtures :accounts, :certificates

  before do
    @account = accounts(:minimum)
    @account.enable_help_center!
    @account.brands.any_instance.stubs(:help_center_in_use?).returns(true)
  end

  describe "exceptions" do
    before do
      @account.update_attribute(:host_mapping, (@host_mapping = "wibble.example.com"))
    end

    describe "mobile api" do
      before { post @account.url(ssl: false, mapped: false) + '/requests/mobile_api/create.json' }

      it "does not redirect to the ssl site" do
        refute redirect?
      end
    end
  end

  describe "un-hostmapped" do
    describe "hitting /" do
      describe "non-ssl" do
        before { get(@account.url(ssl: false)) }

        it "redirects to the ssl site" do
          assert redirect?
          headers["Location"].must_include @account.url
        end

        it "does not set any session cookie" do
          assert_nil headers["Set-Cookie"]
        end
      end

      describe "ssl" do
        before { get(@account.url(ssl: true)) }

        it "keeps you on the ssl site" do
          assert redirect?
          headers["Location"].must_include @account.url
        end

        it "sets shared session cookies" do
          headers["Set-Cookie"].must_include "zendesk_shared_session"
        end

        it "sets session cookie" do
          headers["Set-Cookie"].must_include "zendesk_test_Session"
        end
      end
    end
  end

  describe "hostmapped" do
    before do
      @account.update_attribute(:host_mapping, (@host_mapping = "wibble.example.com"))
    end

    describe "hosted ssl" do
      describe "with an unexpired, active cert" do
        before do
          certificate = @account.certificates.last
          certificate.update_attribute(:state, "active")
          certificate.update_attribute(:valid_until, 10.days.from_now.to_date)
        end

        describe "hitting the http subdomain" do
          before { get(@account.url(mapped: false, ssl: false)) }

          it "redirects to the ssl site" do
            assert redirect?
            headers["Location"].must_include @account.url
          end

          it "does not set any session cookie" do
            assert_nil headers["Set-Cookie"]
          end
        end

        describe "hitting the https subdomain" do
          before { get(@account.url(mapped: false, ssl: true)) }

          it "redirects to the ssl site" do
            assert redirect?
            headers["Location"].must_include @account.url
          end

          it "sets shared session cookies" do
            headers["Set-Cookie"].must_include "zendesk_shared_session"
          end

          it "sets session cookie" do
            headers["Set-Cookie"].must_include "zendesk_test_Session"
          end
        end

        describe "hitting the http hostmapped domain" do
          before { get(@account.url(mapped: true, ssl: false)) }

          it "redirects to the ssl site" do
            assert redirect?
            headers["Location"].must_include @account.url
          end

          it "does not set any session cookie" do
            assert_nil headers["Set-Cookie"]
          end
        end

        describe "hitting the https hostmapped domain" do
          before { get(@account.url(mapped: true, ssl: true)) }

          it "keeps you on the ssl site" do
            assert redirect?
            headers["Location"].must_include @account.url
          end

          it "sets shared session cookies" do
            headers["Set-Cookie"].must_include "zendesk_shared_session"
          end

          it "sets session cookie" do
            headers["Set-Cookie"].must_include "zendesk_test_Session"
          end
        end
      end
    end

    describe "ssl enabled" do
      before do
        @account.update_attribute(:is_ssl_enabled, true)
        @account.reload
      end

      describe "hitting the http subdomain" do
        before { get(@account.url(mapped: false, ssl: false)) }

        it "redirects to the ssl site" do
          assert redirect?
          headers["Location"].must_include @account.url
        end

        it "does not set any session cookie" do
          assert_nil headers["Set-Cookie"]
        end
      end

      describe "hitting the https subdomain" do
        before { get(@account.url(mapped: false, ssl: true)) }

        it "redirects to the ssl site" do
          assert redirect?
          headers["Location"].must_include @account.url
        end

        it "sets shared session cookies" do
          headers["Set-Cookie"].must_include "zendesk_shared_session"
        end

        it "sets session cookie" do
          headers["Set-Cookie"].must_include "zendesk_test_Session"
        end
      end

      describe "hitting the http hostmapped domain" do
        before { get(@account.url(mapped: true, ssl: false)) }

        it "redirects to the ssl site" do
          assert redirect?
          headers["Location"].must_include @account.url
        end

        it "does not set any session cookie" do
          assert_nil headers["Set-Cookie"]
        end
      end
    end

    describe "ssl disabled" do
      before do
        @account.is_ssl_enabled = false
        @account.save!
      end

      describe "hitting the http subdomain" do
        before { get(@account.url(mapped: false, ssl: false)) }

        it "redirects to the http hostmapped site" do
          assert redirect?
          headers["Location"].must_include @account.url
        end

        it "does not set any session cookie" do
          assert_nil headers["Set-Cookie"]
        end
      end

      describe "hitting the https subdomain" do
        describe "root or access" do
          before { get(@account.url(mapped: false, ssl: true)) }

          it "sends the user to HC" do
            assert redirect?
            headers["Location"].must_include @account.url
          end

          it "sets shared session cookies" do
            headers["Set-Cookie"].must_include "zendesk_shared_session"
          end

          it "sets session cookie" do
            headers["Set-Cookie"].must_include "zendesk_test_Session"
          end
        end

        describe "other" do
          before do
            get(@account.url(mapped: false, ssl: true) + '/generated/javascripts/user.json')
          end

          it "does not redirect to the ssl session domain" do
            assert !redirect?
          end

          it "sets shared session cookies" do
            headers["Set-Cookie"].must_include "zendesk_shared_session"
          end

          it "sets session cookie" do
            headers["Set-Cookie"].must_include "zendesk_test_Session"
          end
        end
      end

      describe "hitting the http hostmapped domain" do
        describe "if logged in" do
          describe "login" do
            before do
              post(@account.authentication_domain + "/access/login",
                params: { user: { email: "minimum_agent@aghassipour.com", password: "123456" } })
            end

            it "redirects to secure session domain w/challenge token" do
              assert redirect?
              headers["Location"].must_include @account.url(mapped: false, ssl: true)
              assert_match /\/agent/, headers["Location"]
            end

            it "sets shared session cookies" do
              headers["Set-Cookie"].must_include "zendesk_shared_session"
            end

            it "sets session cookie" do
              headers["Set-Cookie"].must_include "zendesk_test_Session"
            end

            describe "after" do
              before do
                get(@account.url(mapped: true, ssl: false))
              end

              it "keeps user on the http domain" do
                assert redirect?
                headers["Location"].must_include @account.url
              end

              it "sets shared session cookies" do
                headers["Set-Cookie"].must_include "zendesk_shared_session"
              end

              it "sets session cookie" do
                headers["Set-Cookie"].must_include "zendesk_test_Session"
              end
            end
          end
        end

        describe "if not logged in" do
          before do
            get(@account.url(mapped: true, ssl: false))
          end

          it "does not redirect to the ssl authentication domain" do
            assert redirect?
            headers["Location"].must_include @account.url
          end

          it "sets shared session cookies" do
            headers["Set-Cookie"].must_include "zendesk_shared_session"
          end

          it "sets session cookie" do
            headers["Set-Cookie"].must_include "zendesk_test_Session"
          end
        end
      end
    end
  end
end
