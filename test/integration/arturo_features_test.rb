require_relative "../support/test_helper"

SingleCov.not_covered!

# Arturo tests / code live in zendesk_core, make sure we have exactly the same versions of rails + arturo as it has
describe 'ArturoFeatures' do
  describe Arturo::Feature do
    it "caches the lookup when caching is enabled" do
      feature = FactoryBot.create(:arturo_feature)
      cache = Arturo::Feature.feature_cache
      revert('Arturo::Feature.feature_cache.enabled') do
        cache.enabled = true
        assert_nil cache.read("arturo.all")
        Arturo::Feature.find_feature(feature.symbol)
        assert_equal feature, cache.read("arturo.all")[feature.symbol.to_sym]
      end
    end
  end
end
