require_relative "../support/test_helper"
require './script/nps/customer_list_creator'

SingleCov.covered! file: 'script/nps/customer_list_creator.rb'

describe 'CustomerListCreator' do
  fixtures :all

  describe "given the right options" do
    before do
      @account = accounts(:minimum)
      CustomerListCreator.expects(:read_options).returns(
        subdomain: @account.subdomain,
        email: 'foo',
        enduser_number: 10,
        tag: 'pt_1'
      )
      CustomerListCreator.create_cl
    end

    should_eventually "create the given number of users with given tag on given account" do
      assert_equal 10, @account.users.reload.select { |s| s.tags.include?('pt_1') }.size
    end
  end
end
