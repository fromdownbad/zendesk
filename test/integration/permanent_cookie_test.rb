require_relative "../support/test_helper"

class PermanentCookieTest < ActionDispatch::IntegrationTest
  fixtures :users, :accounts

  def decrypted_cookie(message)
    value = CGI.unescape(message)
    json_string = @verifier.verify(value)
    JSON.parse(json_string)
  end

  describe 'Consistency' do
    before do
      @account  = accounts(:minimum)
      @agent    = users(:minimum_agent)
      @agent.stubs(:account).returns(@account)

      secret = Zendesk::PermanentCookie::Store::COOKIE_SECRETS[0]
      @verifier = ActiveSupport::MessageVerifier.new(secret)
    end

    describe 'with is_ssl_enabled?' do
      before do
        @account.is_ssl_enabled = true
        @account.save!

        assert @account.is_ssl_enabled?
        login(@agent, password: "123456")
        @cookies = @controller.send(:cookies)
      end

      it 'saves a secure permanent cookie' do
        original_cookie = decrypted_cookie(@cookies['_zendesk_cookie'])

        visit "/users"
        @cookies = @controller.send(:cookies)
        assert @controller.permanent_cookies.options[:secure]
        updated_cookie = decrypted_cookie(@cookies['_zendesk_cookie'])
        assert_equal original_cookie, updated_cookie
      end
    end

    describe 'without is_ssl_enabled?' do
      before do
        refute @account.is_ssl_enabled?
        login(@agent, password: "123456")
        assert @controller.permanent_cookies.options[:secure]
        @cookies = @controller.send(:cookies)
      end

      it 'saves a secure permanent cookie' do
        assert @cookies['_zendesk_cookie'].present?
        original_cookie = decrypted_cookie(@cookies['_zendesk_cookie'])

        visit "/users"
        @cookies = @controller.send(:cookies)
        assert @controller.permanent_cookies.options[:secure]
        updated_cookie = decrypted_cookie(@cookies['_zendesk_cookie'])
        assert_equal original_cookie, updated_cookie
      end
    end
  end
end
