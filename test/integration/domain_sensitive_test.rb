require_relative "../support/test_helper"

describe "domain sensitive Integration" do
  class DomainSensitiveTestController < ActionController::Base
    def session_started_at
      render plain: session[:session_start] ||= Time.now.to_f.to_s
    end

    def session_read
      render plain: "#{session[:foo]} -- #{session["bar"]}"
    end

    def session_write
      session[:foo] = 1
      session["bar"] = 1
      head :ok
    end
  end

  prepend ControllerDefaultParams
  integrate_test_routes DomainSensitiveTestController

  fixtures :all

  describe "plain integration test" do
    it "maintains session" do
      get("/test/route/domain_sensitive_test/session_started_at")
      session_started_at = response.body
      get("/test/route/domain_sensitive_test/session_started_at")
      assert_equal(session_started_at, response.body)
    end

    it "has different sessions for different domains" do
      get("http://domain1.com/test/route/domain_sensitive_test/session_started_at")
      session1_started_at = response.body
      Timecop.travel(Time.now + 1.seconds)
      get("http://domain2.com/test/route/domain_sensitive_test/session_started_at")
      session2_started_at = response.body
      assert_not_equal(session1_started_at, session2_started_at)
    end

    it "has different sessions for https" do
      get("https://domain1.com/test/route/domain_sensitive_test/session_started_at")
      session1_started_at = response.body
      Timecop.travel(Time.now + 1.seconds)
      get("http://domain2.com/test/route/domain_sensitive_test/session_started_at")
      session2_started_at = response.body
      assert_not_equal(session1_started_at, session2_started_at)
    end

    it "has different sessions via open session" do
      self.host = "domain1.com"
      get("/test/route/domain_sensitive_test/session_started_at")
      session1_started_at = response.body
      Timecop.travel(Time.now + 1.seconds)

      self.host = "domain2.com"
      get("/test/route/domain_sensitive_test/session_started_at")
      session2_started_at = response.body

      assert_not_equal(session1_started_at, session2_started_at)
    end
  end

  describe "webrat test" do
    it "maintains session" do
      get("/test/route/domain_sensitive_test/session_started_at")
      assert_response 200
      session_started_at = response.body
      get("/test/route/domain_sensitive_test/session_started_at")
      assert_equal(session_started_at, response.body)
    end

    it "has indifferent session" do
      get("/test/route/domain_sensitive_test/session_write")
      get("/test/route/domain_sensitive_test/session_read")
      assert_equal("1 -- 1", response.body)
    end

    it "has different sessions for different domains" do
      get("http://domain1.com/test/route/domain_sensitive_test/session_started_at")
      session1_started_at = response.body

      Timecop.freeze 1.second.from_now
      get("http://domain2.com/test/route/domain_sensitive_test/session_started_at")
      session2_started_at = response.body

      assert_not_equal(session1_started_at, session2_started_at)

      get("http://domain1.com/test/route/domain_sensitive_test/session_started_at")
      assert_equal(session1_started_at, response.body)

      get("http://domain2.com/test/route/domain_sensitive_test/session_started_at")
      assert_equal(session2_started_at, response.body)
    end
  end
end
