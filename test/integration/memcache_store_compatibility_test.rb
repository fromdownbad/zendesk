require_relative '../support/test_helper'

describe 'MemcacheStoreCompatibility' do
  describe ActiveSupport::Cache::LibmemcachedStore do
    before do
      @cache = ActiveSupport::Cache::LibmemcachedStore.new
      @store = @cache.instance_variable_get(:@cache)
    end

    it "supports cache keys that are too long" do
      key = 'very long key' * 1000
      fixed_key = 'very%20long%20keyvery%20long%20keyvery%20long%20keyvery%20long%20keyvery%20long%20keyvery%20long%20keyvery%20long%20keyvery%20long%20keyvery%20long%20keyvery%20long%20keyvery%20long%20keyvery%20long%20keyvery%20lo:md5:a3fa9114de338a41447020f2001642f8'

      @store.expects(:set).with(fixed_key, anything, anything, anything, anything)
      @store.expects(:get).with(fixed_key, anything, anything)

      @cache.write(key, 'value')
      @cache.read(key)
    end
  end

  describe "the cache store used in tests" do
    it 'behaves just like the store used in production' do
      return_from_set = Rails.cache.write('key', 'value')

      assert(return_from_set)

      return_from_get = Rails.cache.read('key')

      assert_equal 'value', return_from_get
      refute return_from_get.frozen?
    end
  end
end
