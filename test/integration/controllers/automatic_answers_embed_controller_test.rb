require_relative "../../support/test_helper"

describe AutomaticAnswersEmbedController do
  fixtures :tickets, :users, :accounts, :brands, :ticket_deflections

  let(:account) { accounts(:minimum) }
  let(:article_id) { '2233445566' }
  let(:ticket) { tickets(:minimum_1) }
  let(:brand) { brands(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:via_id) { ticket.via_id }
  let(:article_content) { { id: 1, title: 'title', html_url: 'http://whatever', body: 'This is so rad bro.'} }
  let(:ticket_deflection) { ticket_deflections(:minimum_ticket_deflection) }
  let(:article_info) { { 'article_id' => article_id, 'score' => 0.192819, 'locale' => 'en-us' } }
  let(:base_time) { Time.utc(2016, 3, 31) }
  let(:token) { stub('token', ticket: ticket, value: 'CRC32VALUE') }
  let(:reason_not_related_to_question) { TicketDeflectionArticle::REASON_RELATED_NOT_ANSWERED }
  let(:ticket_deflection_article) do
    TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info)
  end
  let(:decrypted_auth_token) do
    {
      account_id: account.id,
      user_id: ticket.requester_id,
      ticket_id: ticket.nice_id,
      articles: [article_id.to_i],
      token: token.value,
      exp: 30.days.from_now.to_i
    }
  end
  let(:encrypted_auth_token) { JWT.encode(decrypted_auth_token, AUTOMATIC_ANSWERS_JWT_SECRET) }

  before do
    Timecop.freeze(base_time)
    AnswerBot::TicketSolverJob.stubs(:enqueue)

    Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:default_locale).returns('en-us')
    Zendesk::HelpCenter::InternalApiClient.any_instance.
      stubs(:fetch_articles_for_ids_and_locales).
      with([{'article_id' => article_id, 'locale' => 'en-us'}]).
      returns([article_content])

    @request.account = @current_account = account
    @controller.stubs(:current_brand).returns(@current_account.brands.first)
  end

  describe 'GET /fetch_ticket' do
    let(:clicked_time) { Time.now }
    let(:article_id) { 12345 }
    let(:params) do
      {
        auth_token: encrypted_auth_token,
        article_id: article_id
      }
    end
    let!(:deflection_article) do
      TicketDeflectionArticle.create!(
        ticket_deflection: ticket_deflection,
        article_id: article_id,
        account: account,
        score: 0.89,
        brand: brand
      )
    end

    it 'updates ticket deflection article clicked_at timestamp' do
      get :fetch_ticket, params: params
      assert_equal clicked_time, deflection_article.reload.clicked_at
    end
  end

  describe 'POST /solve_ticket' do
    let(:updated_at) { Time.now }

    let(:solve_ticket!) do
      post :solve_ticket, params: { auth_token: encrypted_auth_token, article_id: article_id }
    end

    describe 'when ticket status is less than solved' do
      before do
        ticket.status_id = StatusType.OPEN
        ticket.will_be_saved_by(user)
        ticket.save!
      end

      it 'responds with a success code' do
        solve_ticket!

        assert_response :success
      end
    end

    describe 'when ticket status is greater than or equal to solved' do
      before do
        ticket.status_id = StatusType.SOLVED
        ticket.will_be_saved_by(user)
        ticket.save!
      end

      it 'responds with a success code' do
        solve_ticket!

        assert_response :success
      end

      it 'does not change the state of ticket_deflection' do
        assert_no_difference lambda { ticket_deflection.reload.state } do
          solve_ticket!
        end
      end
    end
  end

  describe 'POST /reject_article' do
    before do
      ticket_deflection.ticket_deflection_articles.destroy_all
      ticket_deflection_article.save!
    end

    describe 'when the submitted article was in the list of suggested articles' do
      before do
        post :reject_article, params: { auth_token: encrypted_auth_token, article_id: article_id, reason: reason_not_related_to_question }

        ticket_deflection_article.reload
      end

      it 'marks irrelevant_by_end_user as true' do
        assert ticket_deflection_article.irrelevant_by_end_user
      end

      it 'updates irrelevant_reason_by_end_user with a reason code' do
        assert_equal reason_not_related_to_question, ticket_deflection_article.irrelevant_reason_by_end_user
      end

      it 'updates the irrelevant_by_end_user_updated_at for the suggested article' do
        assert_equal base_time, ticket_deflection_article.irrelevant_by_end_user_updated_at
      end

      it 'responds with a success code' do
        assert_response :success
      end
    end

    describe 'when the article to be marked as irrelevant was not in the list of suggested articles' do
      let(:unsuggested_article_id) { 999999 }

      def irrelevant_article_count
        ticket_deflection.ticket_deflection_articles.select(&:irrelevant_by_end_user).count
      end

      it 'does not mark any article that was in the list of suggested articles as irrelevant' do
        assert_no_difference lambda { irrelevant_article_count } do
          post :reject_article, params: { auth_token: encrypted_auth_token, article_id: unsuggested_article_id, reason: reason_not_related_to_question }
        end
      end

      it 'responds with a success code' do
        post :reject_article, params: { auth_token: encrypted_auth_token, article_id: unsuggested_article_id, reason: reason_not_related_to_question }

        assert_response :success
      end
    end
  end
end
