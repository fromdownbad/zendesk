require_relative "../../support/test_helper"

describe TicketDeflectionController do
  fixtures :tickets, :users, :accounts, :brands, :ticket_deflections

  let(:account) { accounts(:minimum) }
  let(:article_id) { 2233445566 }
  let(:article_locale) { 'en-us' }
  let(:ticket) { tickets(:minimum_1) }
  let(:brand) { brands(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:article_content) { { id: 1, title: 'title', html_url: 'http://whatever', body: 'This is so rad bro.'} }
  let(:ticket_deflection) { ticket_deflections(:minimum_ticket_deflection) }
  let(:article_info) { { 'article_id' => article_id, 'score' => 0.192819, 'locale' => article_locale } }
  let(:ticket_deflection_article) { TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info) }
  let(:base_time) { Time.utc(2016, 3, 31) }

  let(:encrypted_auth_token) { AutomaticAnswersJwtToken.encrypted_token(account.id, ticket.requester_id, ticket.nice_id, ticket_deflection.id, [article_id]) }

  before do
    Timecop.freeze(base_time)
    AnswerBot::TicketSolverJob.stubs(:enqueue_in)

    Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_article).
      with(as_id(article_id), article_locale).returns(article_content)

    ticket_deflection.ticket_deflection_articles.destroy_all
    ticket_deflection_article.save!

    @request.account = @current_account = accounts(:minimum)
    @controller.stubs(:current_brand).returns(@current_account.brands.first)
    Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)
  end

  describe 'GET /solve_ticket_and_redirect' do
    let(:updated_at) { Time.now }

    let(:solve_ticket_and_redirect!) do
      get :solve_ticket_and_redirect, params: { auth_token: encrypted_auth_token, article_id: article_id }
    end

    describe 'when ticket status is less than solved' do
      before do
        ticket.status_id = StatusType.OPEN
        ticket.will_be_saved_by(user)
        ticket.save!
      end

      it 'responds with a redirect code' do
        solve_ticket_and_redirect!

        assert_response :redirect
      end
    end

    describe 'when ticket status is greater than or equal to solved' do
      before do
        ticket.status_id = StatusType.SOLVED
        ticket.will_be_saved_by(user)
        ticket.save!
      end

      it 'responds with a redirect code' do
        solve_ticket_and_redirect!

        assert_response :redirect
      end

      it 'does not change the state of ticket_deflection' do
        assert_no_difference lambda { ticket_deflection.reload.state } do
          solve_ticket_and_redirect!
        end
      end
    end
  end

  describe 'HEAD /solve_ticket_and_redirect' do
    let(:solve_ticket_and_redirect!) do
      head :solve_ticket_and_redirect, params: { auth_token: encrypted_auth_token, article_id: article_id }
    end

    describe 'when ticket status is less than solved' do
      before do
        ticket.status_id = StatusType.OPEN
        ticket.will_be_saved_by(user)
        ticket.save!
      end

      it 'does not change the state of ticket_deflection' do
        assert_no_difference lambda { ticket_deflection.reload.state } do
          solve_ticket_and_redirect!
        end
      end
    end
  end

  describe 'GET /article_feedback_redirect' do
    it 'updates ticket deflection state to NOT_SOLVED' do
      get :article_feedback_redirect, params: { auth_token: encrypted_auth_token, article_id: article_id }

      assert_equal TicketDeflection::STATE_NOT_SOLVED, ticket_deflection.reload.state
    end

    it 'updates ticket deflection resolution channel as ViaType.MAIL' do
      get :article_feedback_redirect, params: { auth_token: encrypted_auth_token, article_id: article_id }

      assert_equal ViaType.MAIL, ticket_deflection.reload.resolution_channel_id
    end
  end
end
