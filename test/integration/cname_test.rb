require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class CnameTest < ActionDispatch::IntegrationTest
  include ZendeskDSL

  describe "Cname" do
    fixtures :all

    before do
      https!(false)
      stub_request(:post, /https:\/\/api.rollbar.com/)
      Account.any_instance.stubs(:has_public_forums?).returns(false)
      @account = accounts(:minimum)
      @account.enable_help_center!
    end

    describe "on an account with hosted ssl" do
      before do
        assert(@account.update_attribute(:host_mapping, 'wibble.example.com'))
        @account.certificates.create! do |c|
          c.state       = 'active'
          c.valid_until = 10.days.from_now.to_date
        end
        assert(@account.certificates.active.any?)
        @account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
      end

      describe "and public forums" do
        before { Account.any_instance.stubs(:has_public_forums?).returns(true) }

        describe "visting the public forum on the zendesk.com domain" do
          before { @url_to_visit = "https://minimum.zendesk-test.com/home" }
          describe "and SSL is enabled" do
            before { @account.update_attribute(:is_ssl_enabled, true) }

            it "renders the page" do
              visit(@url_to_visit)
              # TODO: Why does this not redirect to the mapped domain??
              assert_response :ok
            end
          end
        end
      end

      describe "logging out from the unmapped domain" do
        before do
          visit("http://wibble.example.com/requests")
          visit(headers["Location"]) while redirect?

          fill_in('user[email]', with: users(:minimum_end_user).email)
          fill_in('user[password]', with: '123456')
          check('remember_me')
          submit_form('login-form')
          visit(headers["Location"]) while redirect?
          assert_equal(users(:minimum_end_user), warden.user)

          visit("http://minimum.zendesk-test.com/requests")
          click_link('logout')
          visit(headers["Location"]) while redirect?

          assert_template 'access/index'
          assert_nil warden.user
        end

        should_eventually "be logged out on the mapped domain" do
          visit("http://wibble.example.com/requests")
          visit(headers["Location"]) while redirect?
          assert_template 'access/index'
        end

        should_eventually "be logged out on the unmapped domain" do
          visit("http://minimum.zendesk-test.com/requests")
          visit(headers["Location"]) while redirect?
          assert_template 'access/index'
        end
      end

      describe "registering a new user," do
        before do
          self.host = @account.host_mapping
        end

        describe "the form" do
          before do
            https!
            get '/registration'
            assert_response :success
          end

          it "submits to https on the UNMAPPED host" do
            contain 'https://minimum.zendesk-test.com/auth/v2/host.js'
          end
        end
      end

      describe "as an anonymous user," do
        before do
          self.host = @account.host_mapping
        end

        describe "the login form" do
          before do
            https!
            get "/access"
            follow_redirect!
            assert_response :success
          end

          it "submits to https on the UNMAPPED host" do
            contain("https://minimum.zendesk-test.com/auth/v2/host.js")
          end
        end

        describe "accessing a url that requires HTTPS" do
          it "redirects to the secure MAPPED host with no challenge token" do
            get '/access/login'
            assert_redirected_to 'https://wibble.example.com/access/login'
          end

          it "redirects on post" do
            post '/access/login'
            assert_response :redirect
          end

          it "redirects to the secure MAPPED host and keep return_to" do
            get '/access/login', params: { return_to: @account.url + "?xxx" }
            assert_redirected_to 'https://wibble.example.com/access/login?return_to=https://wibble.example.com?xxx'
          end
        end
      end

      describe "as a logged in user," do
        before do
          @user = users(:minimum_admin)
          logs_in_as(@user, '123456')
        end

        describe "the password update form" do
          before do
            get '/password'
            assert_response :success
          end

          it "submits to https on the UNMAPPED host" do
            contain 'https://minimum.zendesk-test.com/auth/v2/host.js'
          end
        end
      end

      describe "verifying a user identity" do
        before do
          @user = users(:to_be_verified)
          @user.update_attribute(:crypted_password, nil)

          self.host = @account.host_mapping
          https!
        end

        describe "the verification form" do
          before do
            @token = @user.primary_email_identity.verification_token
            get("/verification/email/#{@token}")
            assert_response :success
          end

          it "submits to https on the UNMAPPED host" do
            contain 'https://minimum.zendesk-test.com/auth/v2/host.js'
          end
        end
      end

      describe "editing remote authentication" do
        before do
          @user = users(:minimum_admin)
          login_to_host_mapped(@user, 'https://wibble.example.com')

          self.host = 'https://wibble.example.com'
          @remote_authentication = remote_authentications(:minimum_remote_authentication)
          https!
        end

        describe 'on the unmapped domain' do
          before do
            get "#{@account.url(mapped: false, ssl: true)}/settings/security/"
            assert_response :success
          end

          it "submits to https on the unmapped host" do
            assert_select('form[action=?]', "/settings/security#support_product")
          end
        end
      end
    end

    describe "on an account with a host mapping," do
      before do
        assert(@account.update_attribute(:host_mapping, 'wibble.example.com'))
        assert_empty(@account.certificates.active)
      end

      describe "registering a new user," do
        before do
          self.host = @account.host_mapping
        end

        describe "the form" do
          before do
            get '/registration'
            assert_response :success
          end

          it "submits to https on the UNMAPPED host" do
            contain 'https://minimum.zendesk-test.com/auth/v2/host.js'
          end
        end

        describe "successfully" do
          before do
            self.host = @account.host_name(mapped: false)
            https!
            post '/registration/register', params: { user: {name: 'Joe Doe', email: 'joedoe@aghassipour.com', password: '123456'} }
            assert_response :redirect
            @user = @account.find_user_by_email('joedoe@aghassipour.com')
            assert_not_nil(@user)
            @remote_ip = request.remote_ip
          end

          it "redirects to success page" do
            assert_redirected_to 'http://wibble.example.com/registration/success?user%5Bname%5D=Joe+Doe'
          end
        end

        describe "with invalid data" do
          before do
            self.host = @account.host_name(mapped: false)
            https!
            post '/registration/register', params: { user: {name: '', email: '', password: ''}, return_to: 'http://wibble.example.com/home' }
            assert_response :redirect
          end

          it "redirects to /registration on the protocol and host of the return_to" do
            assert_response :redirect
            assert_match(/^http:\/\/wibble\.example\.com\/registration/, headers['Location'])
          end
        end
      end

      describe "as an anonymous user," do
        before do
          self.host = @account.host_mapping
        end

        describe "the login form" do
          before do
            get "/access"
            follow_redirect!
            assert_response :success
          end

          it "submits to https on the UNMAPPED host" do
            contain("https://minimum.zendesk-test.com/auth/v2/host.js")
          end
        end

        describe "accessing a url that requires HTTPS" do
          before do
            get '/access/login'
          end

          it "redirects to the secure UNMAPPED host with no challenge token" do
            assert_redirected_to 'https://minimum.zendesk-test.com/access/login'
          end
        end
      end

      describe "as a logged in user," do
        before do
          @host = @account.host_mapping
          @unmapped_host = @account.host_name(mapped: false)
          @user = users(:minimum_admin)
          logs_in_as @user, '123456', return_to: "http://#{@host}/ping/host"
          @remote_ip = request.remote_ip
        end

        it "is logged in on the mapped domain" do
          visit! "http://#{@host}/password"
        end

        it "is logged in on the unmapped domain" do
          visit! "http://#{@unmapped_host}/password"
        end

        describe "the password update form" do
          before do
            visit! "http://#{@host}/password"
          end

          it "submits to https on the UNMAPPED host" do
            contain 'https://minimum.zendesk-test.com/auth/v2/host.js'
          end
        end

        describe "updating a user password" do
          before do
            visit "https://#{@unmapped_host}/password/update", :put,
              current_password: '123456',
              new_password: 'abcdefg',
              return_to: "http://#{@host}/limbo" # faster then home
          end

          it "signs in the user" do
            visit current_url # trying to login again
            assert_equal @user, warden.user
          end
        end
      end

      describe "verifying a user identity" do
        before do
          @user = users(:to_be_verified)
          @user.update_attribute(:crypted_password, nil)
          @token = @user.primary_email_identity.verification_token

          self.host = @account.host_mapping
          get("/verification/email/#{@token}")
          assert_response :success
        end

        describe "the verification form" do
          it "submits to https on the UNMAPPED host" do
            contain 'https://minimum.zendesk-test.com/auth/v2/host.js'
          end
        end
      end

      describe "editing remote authentication" do
        before do
          @user = users(:minimum_admin)
          self.host = @account.host_mapping
          login_to_host_mapped(@user, 'https://wibble.example.com')
          @remote_ip = request.remote_ip

          @remote_authentication = remote_authentications(:minimum_remote_authentication)
        end

        describe "the form" do
          before do
            @unmapped_host = "minimum.zendesk-test.com"
            get "https://#{@unmapped_host}/settings/security"
          end

          it "submits to https on the UNMAPPED host" do
            assert_select('form[action=?]', "/settings/security#support_product")
          end
        end
      end

      describe "with secure_ssl_sessions and ssl disabled and ssl-required" do
        before do
          refute @account.is_ssl_enabled?
        end

        it "redirects to https" do
          logs_in_as users(:minimum_admin), '123456'
          get "http://#{@account.host_mapping}/settings/account"
          assert_response :redirect
          follow_all_redirects!
          assert_equal "https://minimum.zendesk-test.com/settings/account", current_url
        end
      end
    end

    describe "on an account with a capitalized host_mapping entry" do
      before do
        @account.update_attribute(:host_mapping, "wibble.example.com")
        Account.any_instance.stubs(:host_mapping).returns('Wibble.example.com');
        @account.certificates.create! do |c|
          c.state       = 'active'
          c.valid_until = 10.days.from_now.to_date
        end
        assert(@account.certificates.active.any?)
        @account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
      end

      describe "as a logged in user," do
        before do
          @user = users(:minimum_admin)
          self.host = @account.host_mapping
          login_to_host_mapped(@user, 'https://wibble.example.com')
        end
      end
    end
  end
end
