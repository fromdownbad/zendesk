require_relative "../support/test_helper"

class RulesTest < ActionDispatch::IntegrationTest
  fixtures :all

  describe "An authorized user" do
    before do
      @account = accounts(:minimum)
      login(users(:minimum_admin), password: "123456")

      stub_occam_find(Array(tickets(:minimum_1).id))
    end

    it "reroutes a GET to /rules to the tickets controller" do
      visit "/rules"
      assert_equal TicketsController, @controller.class
    end

    %w[automations triggers views].each do |rule_type|
      describe "navigating to #{rule_type}" do
        before do
          stub_request(:get, %r{/api/v2/apps/requirements.json})
        end

        describe "via the filter param" do
          it "succeeds for #index" do
            visit "/rules?filter=#{rule_type}"
            assert_equal "Rules::#{rule_type.capitalize}Controller", @controller.class.name
            assert_response :success
          end

          it "succeeds for #new" do
            visit "/rules/new?filter=#{rule_type}"
            assert_equal "Rules::#{rule_type.capitalize}Controller", @controller.class.name
            assert_response :success
          end
        end

        describe "via rule id" do
          before do
            @rule = @account.send(rule_type).first
            assert @rule
          end

          it "succeeds for #show" do
            visit "/rules/#{@rule.id}"
            assert_equal "Rules::#{rule_type.capitalize}Controller", @controller.class.name
            assert_response :success
          end

          it "succeeds for #edit" do
            visit "/rules/#{@rule.id}/edit"
            assert_equal "Rules::#{rule_type.capitalize}Controller", @controller.class.name
            assert_response :success
          end
        end
      end
    end
  end
end
