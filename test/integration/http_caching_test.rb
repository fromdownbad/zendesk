require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class HttpCachingTest < ActionDispatch::IntegrationTest
  fixtures :all

  describe "HTTP Caching" do
    before do
      @account = accounts(:minimum)
      login(users(:minimum_admin), password: "123456")
      ZendeskSearch::Client.any_instance.stubs(:search).returns(count: 0, results: [])
      Access::ExternalPermissions::Permissions.any_instance.stubs(:allowed_scopes).returns(["user_fields:manage"])
    end

    describe "An agent" do
      before do
        @urls = ["/users"]
      end

      it "is able to use auto-generated etags" do
        @urls.each do |url|
          visit("#{@account.url}#{url}")

          assert_response :success
          etag = @response.headers['ETag']
          assert etag, "Missing etag for #{url}"

          get(url, headers: { "HTTP_IF_NONE_MATCH" => etag })

          assert_response :not_modified, "#{url}: #{response.status}"
          assert_equal '', @response.body, "#{url} should be blank"
        end
      end
    end

    describe "Sensitive resources" do
      before do
        @account.is_ssl_enabled = true
        @account.save!
      end

      it "uses the no-cache directive to avoid public caching for users" do
        get("https://minimum.zendesk-test.com/users")
        assert_response :success
        assert_match /no-cache/, @response.headers['Cache-Control']
      end

      it "uses the no-cache directive to avoid public caching for people" do
        get("https://minimum.zendesk-test.com/people")
        assert_response :success
        assert_match /no-cache/, @response.headers['Cache-Control']
      end

      it "uses the no-cache directive to avoid public caching for settings/account" do
        get("https://minimum.zendesk-test.com/settings/account")
        assert_response :success
        assert_match /no-cache/, @response.headers['Cache-Control']
      end

      it "uses the no-cache directive to avoid public caching for tickets" do
        nice_id = tickets(:minimum_1).nice_id
        get("https://minimum.zendesk-test.com/tickets/#{nice_id}")
        assert_redirected_to "/agent/tickets/#{nice_id}"
        assert_match /no-cache/, @response.headers['Cache-Control']
      end

      it "uses the no-store directive to completely avoid caching for LotusBootstrapController#index" do
        mock_asset_manifest = Lotus::AssetManifest.new(users(:minimum_agent).account, develop: true)
        Lotus::ManifestManager.any_instance.stubs(:find).returns(mock_asset_manifest)
        get("https://minimum.zendesk-test.com/agent")
        assert_response :success
        assert_match /no-store/, @response.headers['Cache-Control']
      end
    end
  end
end
