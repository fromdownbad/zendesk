require_relative "../support/test_helper"

# Integration test for backfiller to migrate S3 data stored through attachment_fu after an account move.
class StoresBackfillerIntegrationTest < ActionDispatch::IntegrationTest
  fixtures :accounts, :brands

  let(:backfill_classes) { Zendesk::Stores::Backfill::StoresBackfillConfig::BACKFILL_CLASSES + [::Voice::Upload.name] }
  let(:account) { accounts(:minimum) }

  let(:recs) { Hash.new { |h, k| h[k] = {} } }

  before do
    # Stub out all (expected) S3 calls:
    stub_request(:get, %r{https://zendesk-test.*.amazonaws.com/.*})
    stub_request(:head, %r{https://zendesk-test.*.amazonaws.com/.*})
    stub_request(:put, %r{https://zendesk-test.*.amazonaws.com/.*})
    stub_request(:delete, %r{https://zendesk-test.*.amazonaws.com/.*})

    # Stub out some validations / model (some of these may end up being bypassed in production backfiller for old data...)
    Attachment.any_instance.stubs(:valid_size?).returns(true)
    Favicon.any_instance.stubs(:validate_dimensions).returns(true)
    BrandLogo.any_instance.stubs(:valid_image_file).returns(true)
    BrandLogo.any_instance.stubs(:validate_dimensions).returns(true)
    Photo.any_instance.stubs(:valid_image_file).returns(true)
    Voice::Upload.any_instance.stubs(:ensure_valid_audio_file).returns(true)
  end

  it 'configuration matches assumptions' do
    [BrandLogo, Favicon, Photo, Voice::Upload, Attachment].each do |k|
      assert_equal [:fs, :s3], Zendesk::Stores::StoresSynchronizerHelper.new(k).preferred_stores.sort
    end
  end

  describe_with_arturo_enabled :stores_backfill do
    describe 'with kill switch off' do
      let(:backfiller) do
        Zendesk::Stores::Backfill::StoresBackfiller.create_backfiller(loop: false, dry_run: false, backfill_classes: backfill_classes)
      end

      before do
        @exp_updates, @backfill_updates = setup_subject_only_backfill(account, recs, backfiller)
      end

      it 'backfills everyone to preferred stores, removes unsupported stores only if rec needs to be backfilled' do
        verify_stores([:fs, :s3], recs, :atts, [:in_s3eu, :in_s3, :happy_no_x, :in_s3_xfiles, :in_xfiles],
          "backfilled recs now in preferred stores")

        # happy_x will not be backfilled (or have 'xfiles' removed) since it's in the preferred_stores; even though
        # xfiles is not preferred
        # contrast this with :in_s3_xfiles and :in_xfiles, which had xfiles removed because they were missing
        # a preferred store (:fs)
        verify_stores([:fs, :s3, :xfiles], recs, :atts, [:happy_x], "atts in xfiles")

        verify_stores([:fs, :s3], recs, :favicons)
        verify_stores([:fs, :s3], recs, :brandlogos)
        verify_stores([:fs, :s3], recs, :photos)
        verify_stores([:fs, :s3], recs, :voice)

        verify_audits(account, backfill_classes, @exp_updates, @backfill_updates)

        total_exp_updates = @exp_updates.values.sum(Zendesk::Stores::Backfill::Update.new)
        assert_equal total_exp_updates, @backfill_updates

        # A second run should do nothing.
        verify_noupd_backfill(backfiller, account)
      end
    end

    describe 'with kill-switch on' do
      let(:backfiller) do
        Zendesk::Stores::Backfill::StoresBackfiller.create_backfiller(loop: false, dry_run: false, disable_nap: 0.00001, backfill_classes: backfill_classes)
      end

      before do
        # You have to stub the arturo check; the describe_with_arturo... methods only work on account-based arturo checks
        Arturo.stubs(:feature_enabled_for_pod?).with(:disable_sync_attachments, 1).returns(true)

        # Not using the other setup method because we want to make sure no account is updated.
        setup_account(account, recs)
        StoresBackfillAudit.delete_all(account_id: account.id)
        @backfill_updates = backfiller.run
        reload_recs(recs)
      end

      it 'does nothing' do
        assert_empty audits(account)
        assert_equal Zendesk::Stores::Backfill::Update.new, @backfill_updates
      end
    end
  end

  describe_with_arturo_disabled :stores_backfill do
    let(:backfiller) do
      Zendesk::Stores::Backfill::StoresBackfiller.create_backfiller(loop: false, dry_run: false, backfill_classes: backfill_classes)
    end

    before do
      # Not using the other setup method because we want to make sure no account is updated.
      setup_account(account, recs)
      StoresBackfillAudit.delete_all(account_id: account.id)
      @backfill_updates = backfiller.run
      reload_recs(recs)
    end

    it 'will not backfill accounts' do
      assert_empty audits(account)
      assert_equal Zendesk::Stores::Backfill::Update.new, @backfill_updates
    end
  end

  # Verifies stores match expected for all recs[model_ind] and (optional) sub-set of keys w/in that hash.
  def verify_stores(exp, recs, model_ind, keys = nil, test_set = nil)
    keys ||= recs[model_ind].keys
    test_set ||= model_ind
    keys.each do |s|
      assert_equal exp.sort, recs[model_ind][s].stores.sort, "bad stores for recs[#{model_ind}][#{s}] for set : #{test_set}"
    end
  end

  def verify_audits(account, backfill_classes, exp_updates, backfill_updates)
    audits = audits(account)

    # All models should have an audit (possibly done)
    assert_equal backfill_classes.size, audits.size

    # Populated models should not be done (all recs migrated, but needs another run to confirm no more ids):
    audit_updates = 0
    audit_evals = 0
    exp_updates.each do |model, exp|
      audit = audits.find { |a| a.model_class == model }

      audit_updates += audit.total_updated
      audit_evals += audit.total_evaluated

      assert_equal exp.updated, audit.total_updated
      assert_equal exp.evaluated, audit.total_evaluated
      refute audit.done?, "Model #{model} should not be done"
    end

    assert_equal audit_updates, backfill_updates.updated, "unexpected number of backfill updates"
    assert_equal audit_evals, backfill_updates.evaluated, "unexpected number of backfill evaluations"

    # Unpopulated models should already be done
    backfill_classes.reject { |model| exp_updates.keys.include? model }.each do |model|
      audit = audits.find { |a| a.model_class == model }

      assert audit.done?, "Model #{model} should be done"
      assert_equal 0, audit.total_evaluated
      assert_equal 0, audit.total_updated
    end
  end

  # Verifies that running backfill doesn't update any records for account and
  # that after the run all audits flagged as done (either before or as part of second run).
  def verify_noupd_backfill(backfiller, account)
    second_run_updates = backfiller.run
    audits(account).each do |audit|
      assert audit.done?, "Model #{audit.model_class} should be done"
    end
    assert_equal Zendesk::Stores::Backfill::Update.new, second_run_updates
  end

  def audits(account)
    StoresBackfillAudit.where(account_id: account.id).to_a
  end

  # Runs the backfill for all accounts (to create audits for uninteresting accounts), then sets
  # the audits to complete for those accounts (to avoid any error from those accounts being re-tried
  # w/the subject backfill).
  #
  # After that, sets up the subject account to be backfilled w/test data, and then runs backfiller
  # (which should only backfill the subject account).
  #
  # After that, reloads recs so the stores reflect values on disk.
  # Returns the expected updates (based on setting up account) and the actual updates from backfiller.
  def setup_subject_only_backfill(account, recs, backfiller)
    backfiller.run
    StoresBackfillAudit.where("account_id != #{account.id}").update_all(last_id: 0)
    StoresBackfillAudit.delete_all(account_id: account.id)
    exp_updates = setup_account(account, recs)
    backfill_updates = backfiller.run
    reload_recs(recs)
    [exp_updates, backfill_updates]
  end

  # Builds records for an account, along with expected updates based on those records;
  # returned value is a map of { model (class) name => expected Zendesk::Stores::Backfill::Update for that model }
  # the return should be compared against the actual audit totals after the backfill is run.
  def setup_account(account, recs)
    # Wipe out any residual data for the subject account:
    backfill_classes.each { |kn| kn.constantize.delete_all(account_id: account.id) }

    # Note that the ID-selection will not remove based on stores because test has multiple preferred
    exp_updates = Hash.new { Zendesk::Stores::Backfill::Update.new }

    # Set up some attachments that will be updated (missing :fs):
    recs[:atts][:in_s3eu] = create_attachment(account, [:s3eu])
    recs[:atts][:in_s3] = create_attachment(account, [:s3])
    recs[:atts][:in_s3_xfiles] = create_attachment(account, [:s3, :xfiles])
    recs[:atts][:in_xfiles] = create_attachment(account, [:s3, :xfiles])
    exp_updates[Attachment.name] += Zendesk::Stores::Backfill::Update.new(4, 4)

    # happy_x will not be updated since it's in preferred stores (unsupported 'xfiles' store is ignored in this case)
    recs[:atts][:happy_x] = create_attachment(account, [:fs, :xfiles, :s3])
    exp_updates[Attachment.name] += Zendesk::Stores::Backfill::Update.new(1, 0)

    # happy_no_x will not be updated since it's in preferred stores.
    recs[:atts][:happy_no_x] = create_attachment(account, [:s3, :fs])
    exp_updates[Attachment.name] += Zendesk::Stores::Backfill::Update.new(1, 0)

    # Set up a favicon (one per account)
    recs[:favicon][:in_fs] = create_favicon(account, [:fs])
    exp_updates[Favicon.name] += Zendesk::Stores::Backfill::Update.new(1, 1)

    # Set up BrandLogos (5 max allowed per account w/o bypassing validations)
    recs[:brandlogos][:in_fs] = create_brand_logo(account, [:fs])
    recs[:brandlogos][:in_s3eu] = create_brand_logo(account, [:s3eu])
    recs[:brandlogos][:happy] = create_brand_logo(account, [:s3, :fs])
    exp_updates[BrandLogo.name] += Zendesk::Stores::Backfill::Update.new(3, 2)

    # Set up photos:
    recs[:photos][:in_fs] = create_photo(account, [:fs])
    recs[:photos][:in_s3eu] = create_photo(account, [:s3eu])
    recs[:photos][:happy] = create_photo(account, [:fs, :s3])
    exp_updates[Photo.name] += Zendesk::Stores::Backfill::Update.new(3, 2)

    # Set up a voice uploads (only one per account allowed):
    recs[:voice][:in_s3eu] = create_voice_upload(account, [:s3eu])
    exp_updates[Voice::Upload.name] += Zendesk::Stores::Backfill::Update.new(1, 1)

    exp_updates
  end

  def reload_recs(recs)
    recs.values.flat_map(&:values).each(&:reload)
  end

  def create_attachment(account, stores)
    attachment = Attachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/normal_1.jpg"))
    attachment.author = account.users.last
    attachment.account = account
    attachment.stores = stores
    attachment.save!
    attachment
  end

  def create_favicon(account, stores)
    logo = Favicon.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new('test/files/normal_1.jpg'))
    logo.account = account
    logo.stores = stores
    logo.save!
    logo
  end

  def create_photo(account, stores)
    photo = Photo.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new('test/files/normal_1.jpg'))
    photo.account = account
    photo.user = account.users.last
    photo.stores = stores
    photo.save!
    photo
  end

  def create_brand_logo(account, stores)
    logo = BrandLogo.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new('test/files/normal_1.jpg'))
    logo.brand = account.brands.first
    logo.account = account
    logo.stores = stores
    logo.save!
    logo
  end

  def create_voice_upload(account, stores)
    upload = Voice::Upload.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new('test/files/dtmf-1.mp3'))
    upload.account = account
    upload.stores = stores
    upload.save!
    upload
  end
end
