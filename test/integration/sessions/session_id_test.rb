require_relative "../../support/test_helper"
require_relative "../../support/zendesk_dsl"
require_relative "../../support/sessions_test_helper"

class SessionIdTest < ActionDispatch::IntegrationTest
  include ZendeskDSL
  include SessionsTestHelper

  fixtures :accounts, :users, :role_settings

  describe "shared_sessions" do
    before do
      @account = accounts(:minimum)
      @admin = users(:minimum_admin)
      @unverified_admin = users(:minimum_admin_not_verified)
      stub_request(:get, "https://minimum.zendesk-test.com/agent/tessaManifest.json")

      logs_in_as(@admin, '123456')
      assert_response :success
      assert_equal @admin.id, shared_session["warden.user.default.key"]
      @admin_cookie = cookies["_zendesk_shared_session"]
      @admin_session_id = get_session_id(@admin_cookie)
    end

    describe "when verifying primary email as an owner who has never logged in" do
      before do
        identity = @admin.identities.email.first
        identity.update_attribute(:is_verified, false)
        @admin.update_attribute(:last_login, nil)

        visit("https://minimum.zendesk-test.com/verification/email/#{identity.verification_token}")
      end

      it "is logged in as admin" do
        visit("https://minimum.zendesk-test.com/api/v2/users/me.json")
        assert_response :success
        assert_equal @admin.id, JSON.parse(response.body)["user"]["id"]
      end

      it "does not reuse shared_session_ids" do
        new_session_id = get_session_id(cookies["_zendesk_shared_session"])
        assert_equal @admin.id, shared_session["warden.user.default.key"]
        assert_not_equal @admin_session_id, new_session_id
      end

      it "invalidates previous shared_session_id" do
        url = "https://minimum.zendesk-test.com/api/v2/users/me.json"
        get(url, headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{@admin_cookie}" })
        assert_response :unauthorized
      end

      describe_with_arturo_disabled :restrict_unauthenticated_user do
        it "returns empty user object" do
          url = "https://minimum.zendesk-test.com/api/v2/users/me.json"
          get(url, headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{@admin_cookie}" })
          assert_response :success
          assert_nil JSON.parse(response.body)["user"]["id"]
        end
      end
    end

    describe "when logging in" do
      before do
        visit("https://minimum.zendesk-test.com/access/logout")
        follow_all_redirects!

        @unverified_admin.identities.email.first.verify!

        logs_in_as(@unverified_admin, '123456')
      end

      it "is logged in as unverified_admin" do
        visit("https://minimum.zendesk-test.com/api/v2/users/me.json")
        assert_response :success
        assert_equal @unverified_admin.id, JSON.parse(response.body)["user"]["id"]
      end

      it "does not reuse shared_session_ids" do
        new_session_id = get_session_id(cookies["_zendesk_shared_session"])
        assert_equal @unverified_admin.id, shared_session["warden.user.default.key"]
        assert_not_equal @admin_session_id, new_session_id
      end

      it "invalidates previous shared_session_id" do
        url = "https://minimum.zendesk-test.com/api/v2/users/me.json"
        get(url, headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{@admin_cookie}" })
        assert_response :unauthorized
      end

      describe_with_arturo_disabled :restrict_unauthenticated_user do
        it "returns empty user object" do
          url = "https://minimum.zendesk-test.com/api/v2/users/me.json"
          get(url, headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{@admin_cookie}" })
          assert_response :success
          assert_nil JSON.parse(response.body)["user"]["id"]
        end
      end
    end
  end

  def get_session_id(cookie)
    decode_cookie(cookie).split(':').last
  end
end
