require_relative "../../support/test_helper"
require_relative "../../support/zendesk_dsl"
require_relative "../../support/api_test_helper"
require_relative "../../support/sessions_test_helper"

describe "session Integration" do
  include ApiTestHelper
  include ZendeskDSL
  include SessionsTestHelper

  fixtures :all

  class SessionsCounterTestController < ApplicationController
    ssl_required :count

    def count
      shared_session[:count] ||= 0
      shared_session[:count] += 1
      render plain: shared_session[:count]
    end
  end
  prepend ControllerDefaultParams
  integrate_test_routes SessionsCounterTestController

  describe "sessions" do
    before do
      Timecop.freeze(Time.at(1413567954))
      @account = accounts(:minimum)
      @admin   = users(:minimum_admin)
      @admin_cookie = "eyJpZCI6IjhhOWU0ZWJiZWFiYjY1MDM2ZjdjNDY5MWExMmQ3MWExIiwid2FyZGVuLnVzZXIuZGVmYXVsdC5rZXkiOjg2MTMzLCJhdXRoX3ZpYSI6InBhc3N3b3JkIiwiYXV0aF9hdXRoZW50aWNhdGVkX2F0IjoxNDEzNTY3OTU0LCJhdXRoX2R1cmF0aW9uIjoyODgwMCwiYXV0aF9wYXNzd29yZF92ZXJzaW9uIjoiZTQ2MTI5OGU2ZjU5OGQwNzY4NTc0MjQ3MmUxYWM4YzMxNjBkZDViNSIsImF1dGhfdXBkYXRlZF9hdCI6MTQxMzU2Nzk1NCwiYXV0aF9zdG9yZWQiOnRydWUsInVzZXJfcm9sZSI6MiwiY3NyZl90b2tlbiI6IlB6VXFCWkZzS2ZJME56c3VUVDMva29SdW92NU91YVUyaHVzTzFCdkN1N2s9IiwibG9jYWxlX2lkIjpudWxsfQ==--9b68c008d89b51c02085233e66b2ade67685fc4c"
    end

    def whoami(cookie)
      url = "https://minimum.zendesk-test.com/api/v2/users/me.json"
      get(url, headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{cookie}" })
      assert_response :success
      JSON.parse(response.body)
    end

    def logout(cookie)
      get('/access/logout', headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{cookie}" })

      while redirect?
        get(response.location, headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{response.cookies['_zendesk_shared_session']}" })
      end

      assert_equal "/access/unauthenticated", request.path
    end

    def assert_has_session_with_cookie(cookie)
      json = whoami(cookie)
      assert_equal @admin.id, json['user']['id']
      assert_equal @admin.id, shared_session.user_id
    end

    def assert_not_logged_in(cookie)
      url = "https://minimum.zendesk-test.com/api/v2/users/me.json"
      get(url, headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{cookie}" })
      assert_response :unauthorized
    end

    def assert_db_session_cookie(cookie)
      account_id, user_id, _session_id = decode_cookie(cookie).split(':')
      assert_equal @admin.id.to_s, user_id
      assert_equal @account.id.to_s, account_id
    end

    def count(cookie)
      url = "https://minimum.zendesk-test.com/test/route/sessions_counter_test/count"
      post(url, session: { 'HTTP_COOKIE' => "_zendesk_shared_session=#{cookie}" })
      assert_response :success
      response.body.to_i
    end

    describe "with db sessions" do
      it "uses cookie sessions for anonymous users" do
        get 'https://minimum.zendesk-test.com/access/login'
        cookie = response.cookies["_zendesk_shared_session"]
        assert_not_logged_in(cookie)
        json = decode_cookie(cookie)
        assert_includes json.keys, 'id'
      end

      describe_with_arturo_disabled :restrict_unauthenticated_user do
        it 'returns empty user object' do
          get 'https://minimum.zendesk-test.com/access/login'
          cookie = response.cookies["_zendesk_shared_session"]
          json = whoami(cookie)

          assert_nil json["user"]["id"]
        end
      end

      describe "and authenticated" do
        before do
          logs_in_as(@admin, '123456')
          assert_response :success
          @cookie = cookies["_zendesk_shared_session"]
          assert_db_session_cookie(@cookie)
        end

        it "persists across requests" do
          assert_has_session_with_cookie(@cookie)
        end

        it "is modifiable" do
          assert_equal 1, count(@cookie)
          assert_equal 2, count(@cookie)
        end

        it "logouts" do
          logout(@cookie)

          new_cookie = response.cookies["_zendesk_shared_session"]
          assert_not_equal @cookie, new_cookie
          assert_not_logged_in(new_cookie)
          assert_not_logged_in(@cookie)
        end

        it "sets proper no cache headers on api/v2/sessions responses" do
          get 'https://minimum.zendesk-test.com/api/v2/users/me/session/renew'

          assert_equal 'no-store', response.headers['Cache-Control']
          assert_equal '-1', response.headers['Expires']
          assert_nil response.headers['ETag']
        end

        describe_with_arturo_disabled :restrict_unauthenticated_user do
          before { logout(@cookie) }

          it 'returns empty user object' do
            json = whoami(@cookie)
            assert_nil json["user"]["id"]
          end
        end
      end
    end
  end
end
