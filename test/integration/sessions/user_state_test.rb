require_relative "../../support/test_helper"
require_relative "../../support/zendesk_dsl"

describe "SharedSession User State Integration" do
  include ZendeskDSL

  fixtures :accounts, :users, :role_settings

  let(:account) { accounts(:minimum).tap { |a| a.update_attribute('is_open', true) } }
  let(:agent) { users(:minimum_agent) }
  let(:end_user) { users(:minimum_end_user) }
  let(:url) { "https://#{account.host_name}" }

  it "sets the cookie for end users" do
    logs_in_as end_user, '123456'
    assert_equal '1', cookies['_zendesk_authenticated']
  end

  it "sets the cookie for agents" do
    logs_in_as agent, '123456'
    assert_equal '1', cookies['_zendesk_authenticated']
  end

  it "clears the cookie for unauthenticated users" do
    get "#{url}/home"
    assert_equal '', cookies['_zendesk_authenticated']
  end
end
