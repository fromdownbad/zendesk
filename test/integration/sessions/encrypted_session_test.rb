require_relative "../../support/test_helper"
require_relative "../../support/zendesk_dsl"
require_relative "../../support/api_test_helper"
require_relative "../../support/sessions_test_helper"

class EncryptedSessionCounterTestController < ApplicationController
  ssl_allowed :count

  def count
    shared_session[:count] ||= 0
    shared_session[:count] += 1
    render plain: shared_session[:count]
  end
end

describe "encrypted session Integration" do
  include ApiTestHelper
  include ZendeskDSL
  include SessionsTestHelper
  prepend ControllerDefaultParams
  integrate_test_routes EncryptedSessionCounterTestController

  fixtures :all

  describe "sessions" do
    before do
      Timecop.freeze(Time.at(1413567954))
      @account = accounts(:minimum)
      @mapped_domain = "hello.example.com"
      @admin = users(:minimum_admin)
      @admin_cookie = "eyJpZCI6IjhhOWU0ZWJiZWFiYjY1MDM2ZjdjNDY5MWExMmQ3MWExIiwid2FyZGVuLnVzZXIuZGVmYXVsdC5rZXkiOjg2MTMzLCJhdXRoX3ZpYSI6InBhc3N3b3JkIiwiYXV0aF9hdXRoZW50aWNhdGVkX2F0IjoxNDEzNTY3OTU0LCJhdXRoX2R1cmF0aW9uIjoyODgwMCwiYXV0aF9wYXNzd29yZF92ZXJzaW9uIjoiZTQ2MTI5OGU2ZjU5OGQwNzY4NTc0MjQ3MmUxYWM4YzMxNjBkZDViNSIsImF1dGhfdXBkYXRlZF9hdCI6MTQxMzU2Nzk1NCwiYXV0aF9zdG9yZWQiOnRydWUsInVzZXJfcm9sZSI6MiwiY3NyZl90b2tlbiI6IlB6VXFCWkZzS2ZJME56c3VUVDMva29SdW92NU91YVUyaHVzTzFCdkN1N2s9IiwibG9jYWxlX2lkIjpudWxsfQ==--9b68c008d89b51c02085233e66b2ade67685fc4c"

      @encrypted_cookie = "-OFF6MStQL3NqaHF5QmR2MldQMkpKWXBPOXgxZUJNZXFPV3I0b1ZPdjVaNGFEYS90NmlmdHRQb0ZsNklBUnRZdi0taDVjMFRtRzg5cGZtbUZwSHJPSmVSQT09--2723852a65eea72532d0fe2dacd00da310e78661"
    end

    def whoami(cookie)
      url = "https://minimum.zendesk-test.com/api/v2/users/me.json"
      get(url, headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{cookie}" })
      assert_response :success
      JSON.parse(response.body)
    end

    def logout(cookie)
      get('/access/logout', headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{cookie}" })

      while redirect?
        get(response.location, headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{response.cookies['_zendesk_shared_session']}" })
      end

      assert_response 200
    end

    def assert_has_session_with_cookie(cookie)
      json = whoami(cookie)
      assert_equal @admin.id, json['user']['id']
      assert_equal @admin.id, shared_session.user_id
    end

    def assert_encrypted_cookie(cookie)
      assert_equal '-', cookie[0]
    end

    def count(cookie, host = 'minimum.zendesk-test.com', scheme = 'https')
      url = "#{scheme}://#{host}/test/route/encrypted_session_counter_test/count"
      post(url, headers: { 'HTTP_COOKIE' => "_zendesk_shared_session=#{cookie}" })
      assert_response :success
      response.body.to_i
    end

    it "refuses unencrypted cookies" do
      url = "https://minimum.zendesk-test.com/api/v2/users/me.json"
      get(url, headers: { 'X-Zendesk-Lotus-Version' => 'v1.2.3', 'HTTP_COOKIE' => "_zendesk_shared_session=#{@admin_cookie}" })
      assert_response :unauthorized
    end

    describe_with_arturo_disabled :restrict_unauthenticated_user do
      it 'returns empty user' do
        user = whoami(@admin_cookie)
        assert_nil user['user']['id']
      end
    end

    it "decrypts existing cookies" do
      assert_equal "90538:86133:8a9e4ebbeabb65036f7c4691a12d71a1", decode_cookie(@encrypted_cookie)
    end

    it "does not decrypt cookies issues on a different domain" do
      logs_in_as(@admin, '123456')
      assert_response :success

      assert_equal 1, count(cookies["_zendesk_shared_session"])
      @account.update_attribute(:host_mapping, @mapped_domain)
      url = "http://#{@mapped_domain}/test/route/encrypted_session_counter_test/count"
      post(url, headers: { 'HTTP_COOKIE' => "_zendesk_shared_session=#{cookies["_zendesk_shared_session"]}" })

      assert_response :redirect
    end
  end
end
