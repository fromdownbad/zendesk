require_relative "../support/test_helper"

describe 'RubyCompatibility' do
  describe "YAML" do
    it "uses Syck in 1.9" do
      assert_equal Syck, YAML
    end

    it "deserializes !binary types properly in 1.9" do
      yaml = "!binary |\n    44GK5ZWP44GE5ZCI44KP44Gb44GC44KK44GM44Go44GG44GU44GW44GE44G+\n"
      string = YAML.load(yaml)
      assert_equal Encoding::UTF_8, string.encoding
      assert_equal "お問い合わせありがとうございま", string
    end
  end
end
