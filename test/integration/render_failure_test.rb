require_relative "../support/test_helper"

describe ApplicationController do
  fixtures :accounts, :users

  class RenderFailureTestController < ApplicationController
    def failure
      render_failure(status: 410)
    end
  end

  tests RenderFailureTestController
  use_test_routes

  let(:current_account) { accounts(:minimum) }

  before do
    @request.account = current_account
    login(current_account.owner)
  end

  describe 'when there is an account' do
    it "renders a failure message for html" do
      get :failure, format: 'html'
      assert_response 410
      assert_includes @response.body, I18n.t('txt.error_message.when_no_page_defined.unspecified_error')
      assert_includes @response.body, 'box-error-center'
    end

    it "renders a failure message for json" do
      get :failure, format: "json"
      assert_response 410
      assert_includes @response.body, '"message":'
    end

    it "responds with a 404 for other" do
      get :failure, format: "txt"
      assert_response 410
    end
  end

  describe 'when there is no account' do
    before do
      @request.account = nil
      logout
    end

    it "redirects to marketing for html" do
      get :failure, format: 'html'
      assert_response :redirect
      assert_equal 301, @response.status
    end

    it "responds with a 404 for other" do
      get :failure, format: "txt"
      assert_response 404
    end
  end
end
