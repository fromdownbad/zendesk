require_relative "../support/test_helper"

class DeviceCaptureTest < ActionDispatch::IntegrationTest
  fixtures :users, :devices, :tickets, :accounts, :role_settings, :account_settings

  describe 'Device Control' do
    let(:account) { accounts :minimum }
    let(:client) do
      c = Zendesk::OAuth::GlobalClient.create!(
        name: "Zendesk for iOS", secret: "test", identifier: 'zdg-foo',
        account_id: account.id, company: "Zendesk", description: "None",
        redirect_uri: 'zendesk://authenticate'
      )
      c.update_columns(identifier: 'zendesk_mobile_ios')
      c
    end

    before do
      @agent = users(:minimum_agent)
    end

    describe "Making an API call" do
      before do
        account = @agent.account

        account.is_ssl_enabled = true
        account.settings.api_token_access = true
        account.save!

        account.api_tokens.create!

        params = {}
        username = "#{@agent.email}/token"
        credentials = Base64.encode64("#{username}:#{account.api_tokens.first}")
        headers = { "HTTP_AUTHORIZATION" => "Basic #{credentials}" }
        get "#{account.url}/api/v2/users/me.json", params: params, headers: headers
      end

      should_not_change("the number of devices") do
        Device.count(:all)
      end
    end

    describe "logging in with a tracked strategy" do
      before do
        assert_difference 'Device.count(:all)' do
          login(@agent, password: "123456", remember_me: true)
        end
      end

      describe "cookies" do
        it "assigns the device token to the permanent cookie store" do
          store = Zendesk::PermanentCookie::Store.new(Zendesk::PermanentCookie::COOKIE_KEY, cookies)
          assert(device_tokens = store[Zendesk::Devices::Manager::STORE_KEY].presence)
          assert device_tokens[users(:minimum_agent).id.to_s]
        end
      end

      describe "accessing a special api endpoint that does not set the account" do
        it "does not blow up" do
          get("#{account.url(protocol: 'https')}/api/v2/accounts/available.json?subdomain=somthing")
          assert_response :success
        end
      end

      describe 'while assumed' do
        before do
          @end_user = users(:minimum_end_user)
        end

        it 'does not create another device' do
          refute_difference 'Device.count(:all)' do
            visit("/users/#{@end_user.id}/assume", :post)
          end
        end

        it 'revokes original device' do
          visit("/users/#{@end_user.id}/assume", :post)
          Device.without_arsi.delete_all
          visit "/users"
          assert_response :forbidden
        end
      end

      describe "revocation" do
        describe "after logging out and a manual user set" do
          before do
            logout

            token = @agent.verification_tokens.create!(expires_at: 1.hour.from_now)
            account = @agent.account

            post account.url + '/password/reset', params: { token: token.value, new_password: 'test12345' }

            Device.without_arsi.delete_all
          end

          it "invalidates session on a classic request" do
            visit "/users"
            assert_response :redirect
          end

          it "invalidates session on a v2 api call" do
            visit api_v2_user_devices_url('me', :json, protocol: 'https')
            assert_response :unauthorized
          end

          it "invalidates session on a v1 api call" do
            visit "/api/v1/stats/graph_data/account/0/ticket_stats_by_account/solve_count"
            assert_response :unauthorized
          end
        end

        describe "with a valid token" do
          it "preserves valid session on a classic request" do
            visit "/users"
            assert_response :success
          end

          it "preserves session on a v2 api call" do
            visit api_v2_user_devices_url('me', :json, protocol: 'https')
            assert_response :success
          end

          it "preserves session on a v1 api call" do
            visit "/api/v1/stats/graph_data/account/0/ticket_stats_by_account/solve_count"
            assert_response :success
          end
        end

        describe "with an invalid token" do
          before { Device.without_arsi.delete_all }

          it "invalidates session on a classic request" do
            visit "/users"
            assert_response :redirect
          end

          it "invalidates session on a v2 api call" do
            visit api_v2_user_devices_url('me', :json, protocol: 'https')
            assert_response :unauthorized
          end

          it "invalidates session on a v1 api call" do
            visit "/api/v1/stats/graph_data/account/0/ticket_stats_by_account/solve_count"
            assert_response :unauthorized
          end
        end
      end
    end

    describe "logging in with the token strategy" do
      let(:client) { account.clients.create!(identifier: "123", name: "test", secret: "test") }
      let(:token) { client.tokens.create!(account_id: account.id, user_id: users(:minimum_admin).id, scopes: "read") }

      it 'does not add a device' do
        refute_difference 'Device.count(:all)' do
          refute_difference 'MobileDevice.count(:all)' do
            get "#{account.url(ssl: true)}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => "http://www.example.com", 'HTTP_AUTHORIZATION' => "Bearer #{token.token(:full)}" }
          end
        end
      end
    end

    describe "logging in with a mobile strategy" do
      let(:agent) { users(:minimum_admin) }

      before do
        body = {
          "client_id" => client.identifier,
          "native_mobile" => true,
          "user" => {
            "email" => agent.email,
            "password" => "123456"
          }
        }
        headers = {
          "CONTENT_TYPE" => "application/json",
          "HTTP_USER_AGENT" => "Zendesk for iPhone",
          "HTTP_ACCEPT" => "application/json"
        }

        assert_difference('MobileDevice.count(:all)', 1) do
          post "#{account.url(ssl: true)}/access/oauth_mobile", params: body.to_json, headers: headers
        end
      end

      it 'has a linked device' do
        assert account.mobile_devices.find_by_user_id_and_token(agent.id, client.tokens.first.id)
      end

      it "doesn't create another device when it's used" do
        cookies.delete("_zendesk_shared_session")
        get(account.url(ssl: true) + "/api/v2/users/me.json", headers: { "HTTP_AUTHORIZATION" => "Bearer #{client.tokens.first.token(:full)}" })
        json = JSON.parse(response.body)
        assert_equal agent.id, json["user"]["id"]
        assert_equal 1, agent.devices(true).count(:all)
      end
    end

    describe "logging in with the token strategy for a mobile device" do
      let(:token) { client.tokens.create!(account_id: account.id, user_id: users(:minimum_admin).id, scopes: "read") }

      before do
        assert_difference('MobileDevice.count(:all)', 1) do
          get "#{account.url(ssl: true)}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => "http://www.example.com", 'HTTP_AUTHORIZATION' => "Bearer #{token.token(:full)}" }
        end
      end

      it "uses the token id as the device token" do
        assert account.mobile_devices.find_by_user_id_and_token(current_user.id, token.id)
      end

      it "is ok" do
        assert_equal "200", @response.code
      end
    end

    describe "with existing mobile device" do
      let(:token) { client.tokens.create!(account_id: account.id, user_id: users(:minimum_admin).id, scopes: "read") }

      before do
        assert_difference('MobileDevice.count(:all)', 1) do
          get "#{account.url(ssl: true)}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => "http://www.example.com", 'HTTP_AUTHORIZATION' => "Bearer #{token.token(:full)}" }
          logout
          get "#{account.url(ssl: true)}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => "http://www.example.com", 'HTTP_AUTHORIZATION' => "Bearer #{token.token(:full)}" }
        end
      end

      it "is ok" do
        assert_equal "200", @response.code
      end

      it "uses the token id as the device token" do
        assert account.mobile_devices.find_by_user_id_and_token(current_user.id, token.id)
      end
    end
  end
end
