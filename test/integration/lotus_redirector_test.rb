require_relative "../support/test_helper"

class LotusRedirectorTest < ActionDispatch::IntegrationTest
  describe "return_to on login" do
    before do
      @account = accounts(:minimum)
      @account.settings.prefer_lotus = true
      @account.settings.save!

      @agent = users(:minimum_agent)

      post "https://#{@account.host_name}/access/login", params: { user: { email: @agent.email, password: '123456' }, return_to: subject }
    end

    describe "forums" do
      subject { "https://#{@account.host_name}/entries/123-test" }

      it "redirects to entry and not lotus" do
        assert_redirected_to subject
      end
    end

    describe "nil" do
      subject { nil }

      it "redirects to lotus" do
        assert_redirected_to "/agent"
      end
    end

    describe "different domain" do
      subject { "https://www.evil.com/evil" }

      it "redirects to lotus" do
        assert_redirected_to "/agent"
      end
    end
  end

  describe "when the account has the correct settings" do
    before do
      @account = accounts(:minimum)
      @account.settings.prefer_lotus = true
      @account.settings.save!

      @agent = users(:minimum_agent)

      post "https://#{@account.host_name}/access/login", params: { user: { email: @agent.email, password: '123456'} }
    end

    it "ends up in lotus" do
      assert_redirected_to "https://#{@account.host_name}/agent"
    end

    describe "and then visiting a classic page" do
      before do
        get("#{@account.url}/tickets/1")
      end

      it "ends up at specific ticket page in lotus" do
        assert_redirected_to "#{@account.url(ssl: true)}/agent/tickets/1"
      end
    end
  end

  describe "with hostmapping, without ssl, plus prefer_lotus" do
    before do
      @agent = users(:minimum_agent)

      @account = accounts(:minimum)
      @account.settings.prefer_lotus = true
      @account.settings.is_google_login_enabled = true
      @account.settings.save!

      @account.update_attribute(:host_mapping, "wibble.example.com")

      post "#{@account.url(mapped: true)}/login", params: { profile: "google" }
    end

    it "ends up at /agent" do
      @response.location =~ %r{#{@account.url(mapped: false, ssl: true)}/access/return_to}
      @response.location =~ %r{challenge_token=[a-z0-9]+}
      @response.location =~ %r{return_to=#{@account.url(mapped: false, ssl: true)}/agent}
    end
  end
end
