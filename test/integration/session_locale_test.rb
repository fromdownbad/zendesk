require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class SessionLocaleTest < ActionDispatch::IntegrationTest
  fixtures :all

  describe "when the prefer_lotus flag is off" do
    before do
      @account = accounts(:minimum)
      @english = translation_locales(:english_by_zendesk)
      @brazilian_portuguese = translation_locales(:brazilian_portuguese)
      Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
      Account.any_instance.stubs(:available_languages).returns([@english, @brazilian_portuguese])
      @user = users(:minimum_end_user)
      @user.translation_locale = @english
    end

    it "changes locale when passing locale in the url and it is an anonymous user" do
      visit("http://minimum.zendesk-test.com/home?locale=2")
      assert_equal @brazilian_portuguese, I18n.previous_request_locale
    end

    it "changes locale when passing locale in the url and it is an end user" do
      login(@user)
      assert_equal @english.to_sym, I18n.locale
      visit("http://minimum.zendesk-test.com/home?locale=2")
      assert_equal @brazilian_portuguese, I18n.previous_request_locale
    end
  end

  describe "when the prefer_lotus flag is on" do
    before do
      @account = accounts(:minimum)
      @english = translation_locales(:english_by_zendesk)
      @brazilian_portuguese = translation_locales(:brazilian_portuguese)
      Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
      Account.any_instance.stubs(:available_languages).returns([@english, @brazilian_portuguese])
      Account.any_instance.stubs(:prefer_lotus).returns(true)
      @user = users(:minimum_end_user)
      @user.translation_locale = @english
    end

    it "changes locale when passing locale in the url and it is an anonymous user" do
      visit("http://minimum.zendesk-test.com/home?locale=2")
      assert_equal @brazilian_portuguese, I18n.previous_request_locale
    end

    it "changes locale when passing locale in the url and it is an end user" do
      login(@user)
      assert_equal @english.to_sym, I18n.locale
      visit("http://minimum.zendesk-test.com/home?locale=2")
      assert_equal @brazilian_portuguese, I18n.previous_request_locale
    end
  end
end
