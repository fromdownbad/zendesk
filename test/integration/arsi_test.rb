require_relative "../support/test_helper"

SingleCov.not_covered!

describe 'Arsi' do
  it "raises exceptions on unscoped deletes" do
    assert_raises Arsi::UnscopedSQL do
      assert User.where("1=0").delete_all
    end
  end

  it "raises expections on unscoped update_alls" do
    assert_raises Arsi::UnscopedSQL do
      assert User.where("1=0").update_all("is_active = false")
    end
  end

  it "allows scoped deletes" do
    assert User.where("account_id = -1").delete_all
  end

  it "allows scoped update_alls" do
    assert User.where("account_id = -1").update_all("is_active = false")
  end
end
