require_relative "../support/test_helper"

class V2CORSIntegrationTest < ActionDispatch::IntegrationTest
  describe "V2 CORS" do
    before do
      @account = accounts(:minimum)
    end

    describe "enabled API endpoint" do
      before do
        get "#{@account.url}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => "http://www.example.com" }
      end

      it "be unauthorized" do
        assert_response :unauthorized
      end

      it "not be allowed" do
        assert_nil response.headers["Access-Control-Allow-Origin"]
      end
    end

    describe "OPTIONS request" do
      before do
        options "#{@account.url}/api/v2/topics.json",
          headers: { 'HTTP_ORIGIN' => "http://www.example.com" }
      end

      it "be ok" do
        assert_response :ok
      end

      it "be allowed" do
        assert_equal "*", response.headers["Access-Control-Allow-Origin"]
      end
    end

    describe "with oauth2" do
      before do
        client = @account.clients.create!(identifier: "123", name: "test", secret: "test")
        token = client.tokens.create!(account_id: @account.id, user_id: users(:minimum_admin).id, scopes: "read")

        get "#{@account.url(ssl: true)}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => "http://www.example.com", 'HTTP_AUTHORIZATION' => "Bearer #{token.token(:full)}" }
      end

      it "be successful" do
        assert_response :success
      end

      it "be allowed" do
        assert_equal "*", response.headers["Access-Control-Allow-Origin"]
      end
    end

    describe "with a session" do
      before do
        post "#{@account.url(ssl: true)}/access/login", params: { user: { email: users(:minimum_admin).email, password: "123456" } }
      end

      describe "and host mapping" do
        before do
          @account.update_attribute(:host_mapping, "wibble.example.com")
        end

        describe "from the host mapped site" do
          describe 'with an active cert' do
            before do
              @account.settings.ssl_enabled = true
              @account.save!
              cert = @account.certificates.first
              cert.state = 'active'
              cert.valid_until = 10.days.from_now
              cert.save!

              get "#{@account.url(ssl: true, mapped: false)}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => "https://wibble.example.com" }
            end

            it "be successful" do
              assert_response :success
            end

            it "be allowed" do
              assert_equal "https://wibble.example.com",
                response.headers["Access-Control-Allow-Origin"]
            end
          end

          describe 'with no cert' do
            before do
              get "#{@account.url(ssl: true)}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => "http://wibble.example.com" }
            end

            it "be successful" do
              assert_response :success
            end

            it "be allowed" do
              assert_equal "http://wibble.example.com",
                response.headers["Access-Control-Allow-Origin"]
            end
          end
        end

        describe "from another site" do
          before do
            get "#{@account.url(ssl: true)}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => "http://www.example.com" }
          end

          it "be successful" do
            assert_response :success
          end

          it "not be allowed" do
            assert_nil response.headers["Access-Control-Allow-Origin"]
          end
        end

        describe "and ssl disabled" do
          before do
            @account.is_ssl_enabled = false
            @account.save!

            get "https://#{@account.default_host}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => "http://#{@account.default_host}" }
          end

          it "be successful" do
            assert_response :success
          end

          it "is not allowed" do
            assert_nil response.headers["Access-Control-Allow-Origin"]
          end
        end
      end

      describe "and ssl disabled" do
        before do
          @account.is_ssl_enabled = false
          @account.save!

          get "https://#{@account.default_host}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => @account.url }
        end

        it "be successful" do
          assert_response :success
        end

        it "be allowed" do
          assert_equal @account.url, response.headers["Access-Control-Allow-Origin"]
        end
      end

      describe "and no host mapping" do
        before do
          get "#{@account.url(ssl: true)}/api/v2/tickets.json", headers: { 'HTTP_ORIGIN' => "http://www.example.com" }
        end

        it "be successful" do
          assert_response :success
        end

        it "not be allowed" do
          assert_nil response.headers["Access-Control-Allow-Origin"]
        end
      end
    end

    describe "with multibrand" do
      before do
        Account.any_instance.stubs(help_center_enabled?: true)
        post "#{@account.url(ssl: true)}/access/login", params: { user: { email: users(:minimum_admin).email, password: "123456" } }
      end

      it "is false from a random origin" do
        assert_invalid_cors_response_for('http://example.com')
      end

      it "is true on the https account subdomain" do
        assert_valid_cors_response_for("https://minimum.zendesk-test.com")
      end

      it "is false on the http account subdomain" do
        assert_invalid_cors_response_for("http://minimum.zendesk-test.com")
      end

      describe "with https host mapping" do
        before do
          c = @account.certificates.create!
          c.update_attribute(:state, 'active')
          @account.settings.ssl_enabled = true
          @account.save!
          @route = FactoryBot.create(:route, account: @account, host_mapping: 'support.wibble.com')
        end

        it "is true on the https account subdomain" do
          assert_valid_cors_response_for("https://minimum.zendesk-test.com")
        end

        it "is true on the https host mapped domain" do
          assert_valid_cors_response_for("https://support.wibble.com")
        end

        it "is false on the http host mapped domain" do
          assert_invalid_cors_response_for("http://support.wibble.com")
        end

        it "is false with a devious origin" do
          assert_invalid_cors_response_for("https://support.wibble.com.attacker.com")
        end

        it "is false without a scheme" do
          assert_invalid_cors_response_for('support.wibble.com')
        end
      end

      describe "with http host mapping" do
        before do
          @route = FactoryBot.create(:route, account: @account, host_mapping: 'support.wibble.com')
        end

        it "is false on the https host mapped domain" do
          assert_invalid_cors_response_for("https://support.wibble.com")
        end

        it "is true on the http host mapped domain" do
          assert_valid_cors_response_for("http://support.wibble.com")
        end
      end
    end
  end

  def assert_invalid_cors_response_for(origin)
    url = "https://minimum.zendesk-test.com/api/v2/tickets.json"
    get url, headers: { 'HTTP_ORIGIN' => origin }
    assert_response :success
    assert_nil response.headers["Access-Control-Allow-Origin"]
  end

  def assert_valid_cors_response_for(origin)
    url = "https://minimum.zendesk-test.com/api/v2/tickets.json"
    get url, headers: { 'HTTP_ORIGIN' => origin }
    assert_response :success
    assert_equal origin, response.headers["Access-Control-Allow-Origin"]
  end
end
