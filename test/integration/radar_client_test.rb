require_relative "../support/test_helper"
require 'zendesk/radar_factory'

describe 'RadarClient' do
  describe 'within a given shard' do
    before do
      @accounts = []
      ActiveRecord::Base.on_shard(1) do
        Account.shard_local.find_each do |account|
          @accounts << account
        end
      end
      assert @accounts.size > 1
    end

    it 'reuses the redis connection for accounts of a given shard' do
      # TODO: add a test for a given cluster when we have radar.yml ready

      clients = []
      @accounts.each do |account|
        clients << ::RadarFactory.create_radar_client(account).redis
      end
      clients.uniq!
      assert_equal clients.size, 1
    end
  end
end
