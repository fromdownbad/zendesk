require_relative '../support/test_helper'

describe 'ZendeskJobResque' do
  describe 'ZendeskJob::Resque' do
    it 'has a statsd client' do
      assert_instance_of Zendesk::StatsD::Client, ZendeskJob::Resque.statsd_client
    end

    it 'sends metrics to statsd client' do
      client = ZendeskJob::Resque.statsd_client
      tags = ['job:UserCountJob', 'queue:maintenance', 'app:classic']

      client.expects(:increment).with('resque.starting', tags: tags)
      client.expects(:increment).with('resque.client.enqueue', tags: ['job:UserCountJob', 'queue:maintenance', 'app:classic'])
      client.expects(:timing).with('resque.dequeue_delay', instance_of(Integer), tags: tags)
      client.expects(:timing).with do |name, value, options|
        name == 'resque.duration' &&
          value >= 0 &&
          options == { tags: tags}
      end
      client.expects(:increment).with('resque.finished', tags: tags)

      Zendesk::Maintenance::Jobs::UserCountJob.enqueue
    end
  end
end
