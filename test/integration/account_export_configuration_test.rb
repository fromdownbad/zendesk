require_relative '../support/test_helper'

class AccountExportConfigurationTest < ActionDispatch::IntegrationTest
  fixtures :accounts, :users

  describe "Account Export Configuration settings" do
    let(:account) { accounts(:minimum) }
    let(:owner)   { account.owner }
    let(:admin)   { users(:minimum_admin_not_owner) }

    let(:account_settings_url) { account.url(ssl: true) + settings_account_path }
    let(:export_tab_url)       { account.url(ssl: true) + "/reports?tab=export" }

    before do
      Subscription.any_instance.stubs(has_report_feed?: true)
      stub_request(:get, /\/api\/v2\/exports/).
        to_return(status: 200, body: { 'export_jobs' => [] }.to_json, headers: { 'Content-Type' => 'application/json' })
    end

    describe "as the account owner" do
      before { login(owner) }

      describe "viewing the reporting export tab" do
        before do
          visit export_tab_url
          assert_contain("CSV export")
        end

        it "renders the whitelisted domain form" do
          assert_contain("Approved email domain")
        end

        it "renders the 'Disable exports' link" do
          assert_contain("Disable exports")
        end
      end
    end

    describe "as an admin, not the owner" do
      before { login(admin) }

      describe "viewing the reporting export tab" do
        before do
          account.settings.export_whitelisted_domain = admin.email_domain
          account.save!
          visit export_tab_url
          assert_contain("CSV export")
        end

        it "does not render the whitelisted domain form" do
          assert_not_contain("Approved email domain")
        end

        it "does not render the 'Disable exports' link" do
          assert_not_contain("Disable exports")
        end
      end
    end
  end
end
