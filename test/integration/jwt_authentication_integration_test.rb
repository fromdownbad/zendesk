require_relative "../support/test_helper"

require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'

class JwtAuthenticationIntegrationTestController < ApplicationController
  skip_before_action :ensure_proper_protocol, :authenticate_user

  def return_to
    session[:return_to] = params[:return_to]
    head :ok
  end
end

describe "Jwt authentication Integration" do
  include AuthenticationIntegrationTestHelper

  fixtures :accounts, :remote_authentications, :users, :organizations, :organization_memberships

  prepend ControllerDefaultParams
  integrate_test_routes JwtAuthenticationIntegrationTestController

  describe "JWT Authentication" do
    before do
      @account = accounts(:minimum)

      @auth = remote_authentications(:minimum_remote_authentication)
      @auth.auth_mode = RemoteAuthentication::JWT
      @auth.is_active = true
      @auth.remote_login_url = "https://sso.cpconsulting.dk/zendesk_remote_auth.asp"
      @auth.save!

      role_settings = @account.role_settings
      role_settings.agent_zendesk_login    = false
      role_settings.end_user_zendesk_login = false
      role_settings.agent_remote_login     = true
      role_settings.end_user_remote_login  = true
      role_settings.save!
    end

    describe "redirection" do
      should_redirect_to_remote_auth("https://minimum.zendesk-test.com/tickets")

      describe "when disabled" do
        before { @auth.update_attribute(:is_active, false) }

        should_not_redirect_to_remote_auth("http://minimum.zendesk-test.com/tickets")
      end

      describe "set to handle an out of range CIDR ip net block range" do
        before do
          @auth.ip_ranges = '123.123.123.123/32'
          @auth.save!
        end

        should_not_redirect_to_remote_auth("http://minimum.zendesk-test.com/tickets")
      end

      describe "set to handle an in range CIDR ip net block range" do
        before do
          @auth.ip_ranges = '127.0.0.1/32'
          @auth.save!
        end

        should_redirect_to_remote_auth("https://minimum.zendesk-test.com/tickets")
      end

      describe "set to only handle an IP range" do
        before do
          @auth.ip_ranges = '123.123.876.876'
          @auth.save!
        end

        should_not_redirect_to_remote_auth("http://minimum.zendesk-test.com/tickets")
      end

      describe "brand_id" do
        it "includes the brand_id that initiated the login" do
          visit "#{@account.url}/access/login"
          assert_match /brand_id=#{@account.default_brand.id}/, response.location
        end

        it "brand_id can be overridden from the params" do
          visit "#{@account.url}/access/sso?brand_id=1"
          assert_match /brand_id=1/, response.location
        end
      end
    end

    describe "authentication" do
      before do
        @user = users(:minimum_end_user)
      end

      describe "return_to" do
        let(:valid_return_to) { "https://#{@account.host_name}/users/4" }

        before do
          visit(valid_return_to)
          authenticate
        end

        it "return to return_to" do
          assert_equal valid_return_to, current_url
        end
      end

      describe "valid jwt" do
        before { authenticate }

        it "redirect home" do
          assert_match %r{^https://#{@account.host_name}}, current_url
        end

        it "not redirect back to /access" do
          assert_no_match %r{^https://#{@account.host_name}/access}, current_url
        end

        should_redirect_to_hc
      end

      describe "GoodData" do
        it "should not sync when user hasn't been updated during SSO" do
          Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).with(@user).never
          get "#{@account.authentication_domain}/access/jwt?#{valid_jwt.to_param}"
          assert_response :redirect
        end
      end

      describe "with a blacklisted email address" do
        before do
          @account.update_attribute(:domain_blacklist, 'blacklist.com')
        end

        it "should not create the user" do
          authenticate(valid_jwt(email: 'testing@blacklist.com', name: 'Test'))

          assert_response 200
          assert_nil response.headers['X-Zendesk-User-Id']
        end

        describe "but if the user already exists" do
          before do
            @account.users.create!(email: 'testing@blacklist.com', name: 'Test')
            authenticate(valid_jwt(email: 'testing@blacklist.com', name: 'Test'))
          end

          should_redirect_to_hc
        end

        describe "should not apply to agents" do
          it "redirects to agent" do
            stub_request(:get, %r{/agent/tessaManifest.json\z})
            jwt = valid_jwt(email: 'testing@blacklist.com', name: 'Test', role: 'agent')
            get "#{@account.authentication_domain}/access/jwt?#{jwt.to_param}"
            assert_redirected_to "/agent"
          end
        end
      end

      describe "with malformed JWT tokens" do
        before do
          jwt = JSON::JWT.new(jti: "unique.request",
                              iat: Time.now.to_i,
                              name: @user.name,
                              email: @user.email).sign(@auth.token)

          jwt.header[:alg] = :none
          jwt.signature = ''
          @unsigned_jwt = jwt.to_s
        end

        it "should not accept an unsigned JWT token" do
          params = {jwt: @unsigned_jwt}
          visit("#{@account.authentication_domain}/access/jwt?#{params.to_param}")

          assert_response 200
          assert_nil response.headers['X-Zendesk-User-Id']
        end

        it "should not accept an unsigned JWT token with a bogus signature" do
          params = {jwt: @unsigned_jwt += 'bogus-signature'}
          visit("#{@account.authentication_domain}/access/jwt?#{params.to_param}")

          assert_response 200
          assert_nil response.headers['X-Zendesk-User-Id']
        end
      end

      describe "when already logged in" do
        before do
          @other_user = @account.owner
          authenticate
        end

        it "should log another user in" do
          jwt = valid_jwt(name: @other_user.name, email: @other_user.email)
          assert_equal @user.id.to_s, response.headers['X-Zendesk-User-Id']
          authenticate(jwt)

          assert_equal @other_user.id.to_s, response.headers['X-Zendesk-User-Id']
        end
      end

      describe "with a multiorg account" do
        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
          @default_organization = @user.organization
        end

        describe "and an existing organization" do
          before do
            authenticate(valid_jwt(organization: @default_organization.name))
            @user.reload
          end

          it "redirect home" do
            assert_match %r{^https://#{@account.host_name}}, current_url
          end
        end

        describe "and a new organization" do
          before do
            @new_organization = organizations(:minimum_organization2)
            refute_includes @user.organizations, @new_organization

            authenticate(valid_jwt(organization: @new_organization.name))
            @user.reload
          end

          it "add the new organization" do
            assert_includes @user.organizations, @new_organization
          end

          it "not change the default organization" do
            assert_equal @default_organization, @user.organization
          end
        end

        describe "and organizations-parameter contains multiple organizations" do
          let(:organization) { @user.account.organizations.create!(name: "Buddhists") }
          let(:organization2) { @user.account.organizations.create!(name: "Carpenters") }
          let(:default_organization) { @user.organization_memberships.find(&:default?).organization }

          describe "with a default organization for the user" do
            before do
              assert_includes @user.organizations, default_organization
              refute_includes @user.organizations, organization
              refute_includes @user.organizations, organization2

              authenticate(valid_jwt(organizations: [organization.name, organization2.name].join(',')))
            end

            it "adds the new organizations and keeps the default" do
              organizations = @user.reload.organizations
              assert_equal 3, organizations.count
              assert_includes organizations, organization
              assert_includes organizations, organization2
              assert_includes organizations, default_organization
            end
          end

          describe "with a default organization for the user and the default organization in the organizations string" do
            before do
              assert_includes @user.organizations, default_organization
              refute_includes @user.organizations, organization
              refute_includes @user.organizations, organization2

              authenticate(valid_jwt(organizations: [organization.name, organization2.name, default_organization.name].join(',')))
            end

            it "adds the new organizations and only has one occurrence of the default org" do
              organizations = @user.reload.organizations
              assert_equal 3, organizations.count
              assert_includes organizations, organization
              assert_includes organizations, organization2
              assert_includes organizations, default_organization
            end
          end

          describe "with no default organization for the user" do
            before do
              Account.any_instance.stubs(:default_organization_for).returns(nil)
              assert_includes @user.organizations, default_organization
              refute_includes @user.organizations, organization
              refute_includes @user.organizations, organization2

              authenticate(valid_jwt(organizations: [organization.name, organization2.name].join(',')))
            end

            it "adds the new organizations and removes the existing" do
              organizations = @user.reload.organizations
              assert_equal 2, organizations.count
              assert_includes organizations, organization
              assert_includes organizations, organization2
            end
          end
        end

        describe "and organizations-parameter is an empty string" do
          let(:organization) { @user.account.organizations.create!(name: "Buddhists") }
          let(:organization2) { @user.account.organizations.create!(name: "Carpenters") }
          let(:default_organization) { @user.organizations.first }

          describe "with no default organization for the user" do
            before do
              Account.any_instance.stubs(:default_organization_for).returns(nil)
              assert_includes @user.organizations, default_organization
              assert_equal 1, @user.organizations.count
              authenticate(valid_jwt(organizations: ""))
            end

            it "removes the organizations" do
              organizations = @user.reload.organizations
              assert_equal 0, organizations.count
            end
          end

          describe "with a default organization for the user" do
            before do
              assert_includes @user.organizations, default_organization
              assert_equal 1, @user.organizations.count
              authenticate(valid_jwt(organizations: ""))
            end

            it "keeps the default organization" do
              organizations = @user.reload.organizations
              assert_includes organizations, default_organization
              assert_equal 1, organizations.count
            end
          end
        end
      end

      describe "with custom fields" do
        let(:new_user) { @account.find_user_by_email("someone@gmail.com") }
        let(:actual_custom_fields) { new_user.custom_field_values.as_json(account: @account) }
        let(:user_fields) { {"text1" => "sample", "integer1" => 123, "system::integer2" => 456} }
        let(:nil_fields) { {"text1" => "", "integer1" => nil} }

        before do
          @user = OpenStruct.new(name: "John Smith", email: "someone@gmail.com")
          authenticate(valid_jwt(user_fields: user_fields))
        end

        it "sets custom fields" do
          user_fields.each do |k, v|
            assert_equal(v, actual_custom_fields[k])
          end
        end

        it "overwrites an existing field" do
          custom_field_values_for("integer1", new_user)[0].update_attributes!(value: 789)

          authenticate(valid_jwt(user_fields: user_fields))
          new_user.reload

          assert_equal 123, actual_custom_fields["integer1"]
        end

        describe "with empty/nil custom fields" do
          before do
            authenticate(valid_jwt(user_fields: nil_fields))
            new_user.reload
          end

          it "deletes custom fields that are empty strings in token" do
            assert_nil actual_custom_fields["text1"]
          end

          it "deletes custom fields that are nil in token" do
            assert_nil actual_custom_fields["integer1"]
          end

          it "doesn't delete custom fields not present in token" do
            assert_equal 456, actual_custom_fields["system::integer2"]
          end
        end
      end

      describe "when throttled" do
        before do
          Prop.expects(:throttled?).returns(true)
          authenticate
        end

        should_give_an_error(nil, selector: '.notification-error')
      end

      describe "invalid jwt" do
        describe "with an invalidly formatted jwt" do
          before { authenticate(jwt: "this.has.way.too.many.dots") }

          should_give_an_error(nil, selector: '.notification-error')
        end

        describe "with no name and email" do
          before { authenticate(valid_jwt(name: nil, email: nil)) }

          should_give_an_error(nil, selector: '.notification-error')
        end
      end

      describe "with a return URL" do
        before do
          @auth.remote_logout_url = 'https://thankyoumachine.com/return/'
          @auth.save!
        end

        describe "with a suspended user" do
          before do
            User.any_instance.stubs(suspended?: true)
            authenticate valid_jwt
          end

          it "redirect to the return URL" do
            assert_match %r{^https://thankyoumachine.com/return/}, headers['Location']
          end

          it "set the error parameters" do
            assert_equal 'error', get_query_params_from(response.location)['kind']
            assert_not_nil location_params['message']
          end
        end

        describe "on errors" do
          before { authenticate(jwt: "terrible") }

          it "redirect errors to the return URL" do
            assert_match %r{^https://thankyoumachine.com/return/}, headers["Location"]
          end

          it "set the error parameters" do
            assert_equal 'error', location_params['kind']
            assert_not_nil location_params['message']
          end
        end

        describe "on logout" do
          before do
            authenticate
            visit("https://#{@account.host_name}/access/logout?#{{brand_id: @account.default_brand_id, return_to: @account.url + "hc/en-us"}.to_query}")
          end

          it "redirect to the return URL" do
            assert_match %r{^https://thankyoumachine.com/return/}, headers["Location"]
          end

          it "redirect with the user info" do
            assert_match /email=#{CGI.escape(@user.email)}/, headers["Location"]
          end

          it "includes a brand_id if present" do
            assert_match /brand_id=#{@account.default_brand_id}/, headers["Location"]
          end

          it "includes a return_to if present" do
            assert_match /return_to=#{CGI.escape(@account.url + "hc/en-us")}/, headers["Location"]
          end
        end
      end

      describe "with a return URL that overrides a field" do
        before do
          @auth.remote_logout_url = 'https://thankyoumachine.com/return/?email=email'
          @auth.save!
        end

        describe "on logout" do
          before do
            authenticate
            visit("https://#{@account.host_name}/access/logout")
          end

          it "overwrites" do
            assert_match /email=email&/, headers["Location"]
          end
        end
      end

      describe "with a return URL that clears a field" do
        before do
          @auth.remote_logout_url = 'https://thankyoumachine.com/return/?email='
          @auth.save!
        end

        describe "on logout" do
          before do
            authenticate
            visit("https://#{@account.host_name}/access/logout")
          end

          it "overwrites" do
            refute_includes headers["Location"], 'email'
          end
        end
      end

      describe "with the mobile client" do
        let(:client) { FactoryBot.create(:global_client, user: @account.owner, redirect_uri: 'zendesk://authenticate') }
        let(:headers) { {'HTTP_USER_AGENT' => 'Zendesk for iPhone', 'HTTP_CLIENT_IDENTIFIER' => client.identifier} }

        describe "with a valid jwt token it should redirect" do
          before do
            get(@account.authentication_domain + "/access/jwt?#{valid_jwt.to_param}&return_to=/home", headers: headers)
            uri = URI.parse(response.location)
            @redirect_params = Rack::Utils.parse_query(uri.query)
          end

          it "redirects back to the native app" do
            assert_response :redirect
            assert response.location.starts_with?('zendesk://authenticate')
          end

          it "includes the username" do
            assert_equal @user.email, @redirect_params['username']
          end

          it "includes a valid oauth token" do
            oauth_token = Zendesk::OAuth::Token.where(token: @redirect_params['access_token']).first
            assert_equal @user, oauth_token.user
          end
        end

        describe "with the oauth client in the cookie" do
          let(:headers) { {'HTTP_USER_AGENT' => 'Zendesk for iPhone', 'HTTP_COOKIE' => "_zendesk_client_identifier=#{client.identifier}"} }
          before do
            get(@account.authentication_domain + "/access/jwt?#{valid_jwt.to_param}&return_to=/home", headers: headers)
            uri = URI.parse(response.location)
            @redirect_params = Rack::Utils.parse_query(uri.query)
          end

          it "cookie redirects back to the native app" do
            assert_response :redirect
            assert response.location.starts_with?('zendesk://authenticate')
          end

          it "includes the username" do
            assert_equal @user.email, @redirect_params['username']
          end

          it "includes a valid oauth token" do
            oauth_token = Zendesk::OAuth::Token.where(token: @redirect_params['access_token']).first
            assert_equal @user, oauth_token.user
          end
        end

        describe "with an invalid jwt token" do
          before do
            get(@account.authentication_domain + "/access/jwt?jwt=not.a.jwt.token&return_to=/home", headers: headers)
          end

          it 'should redirect back to the native app error handler' do
            assert_redirected_to "zendesk://authenticate/errors"
          end
        end
      end

      describe "JWT on the HTTP subdomain" do
        before do
          @account.update_attribute(:host_mapping, 'support.example.com')
        end

        it "be blocked" do
          get "#{@account.url(mapped: false, ssl: false)}/access/jwt?#{valid_jwt.to_param}"
          assert_response :forbidden
        end

        it "be blocked even with an ssl_forced account" do
          Account.any_instance.stubs(ssl_should_be_used?: true)
          get "#{@account.url(mapped: false, ssl: false)}/access/jwt?#{valid_jwt.to_param}"
          assert_response :forbidden
        end

        it "redirect to https if grandfathered" do
          Arturo.enable_feature! :grandfathered_hostmapped_jwt
          get "#{@account.url(mapped: false, ssl: false)}/access/jwt?#{valid_jwt.to_param}"
          assert_response :redirect
          assert response.location.starts_with?(@account.url(ssl: true, mapped: false)), "should be redirected to authentication domain"
        end

        it "redirects with the return_to value from the session" do
          Arturo.enable_feature! :grandfathered_hostmapped_jwt
          open_session do |session|
            jwt = valid_jwt
            session.post "#{@account.url(mapped: false, ssl: false)}/test/route/jwt_authentication_integration_test/return_to", return_to: '/foo'
            session.get "#{@account.url(mapped: false, ssl: false)}/access/jwt?#{jwt.to_param}"
            session.assert_response :redirect

            params = Rack::Utils.parse_query(URI.parse(session.response.location).query)
            assert_equal jwt[:jwt], params['jwt']
            assert_equal '/foo', params['return_to']
          end
        end

        it "not redirect with the session return_to if there is an existing return_to" do
          Arturo.enable_feature! :grandfathered_hostmapped_jwt
          open_session do |session|
            jwt = valid_jwt
            session.post "#{@account.url(mapped: false, ssl: false)}/test/route/jwt_authentication_integration_test/return_to", return_to: '/foo'
            session.get "#{@account.url(mapped: false, ssl: false)}/access/jwt?#{jwt.to_param}&return_to=/bar"
            session.assert_response :redirect

            params = Rack::Utils.parse_query(URI.parse(session.response.location).query)
            assert_equal jwt[:jwt], params['jwt']
            assert_equal '/bar', params['return_to']
          end
        end
      end

      describe "on an account with a host mapping" do
        before do
          @account.update_attribute(:host_mapping, 'wibble.example.com')
          @account.update_attribute(:is_ssl_enabled, true)
          @account.certificates.create! do |c|
            c.state       = 'active'
            c.valid_until = 10.days.from_now.to_date
          end
          @account.reload
          assert_equal 'https://wibble.example.com', @account.url
        end

        describe "with grandfathered_hostmapped_jwt" do
          before do
            Arturo.enable_feature! :grandfathered_hostmapped_jwt
          end

          it "redirect to https even with a return to in the session" do
            get "#{@account.url}/access/sso"
            get "#{@account.url}/access/jwt?#{valid_jwt.to_param}"
            assert_response :redirect
            assert response.location.starts_with?(@account.url(ssl: true, mapped: false)), "should be redirected to authentication domain"
          end

          it "be logged in on mapped domain" do
            authenticate(valid_jwt.merge(return_to: 'https://wibble.example.com/ping/host'), @account.url)
            get "#{@account.url}/access"
            assert_response :redirect
            assert_equal 'https://wibble.example.com/hc', response.location
          end
        end

        describe "when remote authentication script returns to mapped domain" do
          before do
            get "#{@account.url}/access/jwt?#{valid_jwt.to_param}"
          end

          it "block the request" do
            assert_response :forbidden
          end

          it "not be authenticated" do
            get "#{@account.url}/access"
            assert_response :redirect
            assert response.location.start_with?(@auth.remote_login_url)
          end
        end

        describe "starting from the zendesk hosted domain" do
          before do
            @return_to = "#{@account.url}/users/current_user.json"
            visit("#{@account.authentication_domain}/access/jwt?#{valid_jwt.merge(return_to: @return_to).to_param}")
          end

          it "send to host-mapped /access/return_to" do
            assert_match %r{^#{@account.url}/access/return_to}, headers["Location"]
          end

          describe "after hitting sso" do
            before { visit(headers["Location"]) }

            it "go to the return_to" do
              assert_equal @return_to, current_url.sub(/\?flash_digest=[a-f\d]+/, '')
            end
          end
        end

        describe "and with a return URL" do
          before do
            @auth.remote_logout_url = 'https://thankyoumachine.com/return/'
            @auth.save!
          end

          describe "on logout from the auth domain" do
            before do
              authenticate
              visit("#{@account.authentication_domain}/access/logout")
              follow_all_redirects! except: /thankyoumachine.com\/return/
            end

            it "redirect to the return URL" do
              assert_match %r{^https://thankyoumachine.com/return/}, headers["Location"]
            end

            it "redirect with user info" do
              assert_match /email=#{CGI.escape(@user.email)}/, headers["Location"]
            end

            it "redirects with the brand_id" do
              assert_match /brand_id=#{@account.default_brand.id}/, headers["Location"]
            end
          end

          describe "on logout from the hostmapped domain" do
            before do
              authenticate
              visit("#{@account.url}/access/logout")
              follow_all_redirects! except: /thankyoumachine.com\/return/
            end

            it "redirects to the return URL" do
              assert_match %r{^https://thankyoumachine.com/return/}, headers["Location"]
            end

            it "redirects with user info" do
              assert_match /email=#{CGI.escape(@user.email)}/, headers["Location"]
            end

            it "redirects with the brand_id" do
              assert_match /brand_id=#{@account.default_brand.id}/, headers["Location"]
            end
          end
        end
      end
    end
  end

  private

  def valid_jwt(options = {})
    {
      jwt: JWT.encode({
        jti: "unique.request.#{SecureRandom.hex}",
        iat: Time.now.to_i,
        name: @user.name,
        email: @user.email,
      }.merge(options), @auth.token)
    }
  end

  def authenticate(params = valid_jwt, domain = @account.authentication_domain)
    visit("#{domain}/access/jwt?#{params.to_param}")
    follow_all_redirects! except: /^https?:\/\/thankyoumachine\.com/
  end
end
