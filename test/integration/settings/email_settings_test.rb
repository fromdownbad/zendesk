require_relative "../../support/test_helper"

class EmailSettingsTest < ActionDispatch::IntegrationTest
  extend ArturoTestHelper

  fixtures :accounts, :role_settings, :users, :account_settings

  let (:account) { accounts(:minimum) }
  let (:agent)   { users(:minimum_admin) }

  before { login(agent) }

  it "has the rich_content_in_email setting" do
    visit("#{account.url}/settings/email")
    assert_response :success
    assert_contain "Rich content in email"
  end

  it "has the option to import the last X emails" do
    visit("#{account.url}/settings/email")
    assert_response :success
    assert_contain "Also create tickets from your most recent (up to) #{ExternalEmailCredential::INITIAL_IMPORT_COUNT} emails."
  end

  it "has the option to revert the default delimiter" do
    visit("#{account.url}/settings/email")
    assert_response :success
    assert_contain "#delimiter_revert_to_default"
  end
end
