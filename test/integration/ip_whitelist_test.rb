require_relative "../support/test_helper"

class IpWhitelistTest < ActionDispatch::IntegrationTest
  fixtures :all

  # This also affects any users of IpTools outside of Classic
  # This specific use-case is because of Zendesk::Auth::Warden::ControllerMixin#authenticate_user
  describe "ip whitelisting " do
    let(:account) { accounts(:minimum) }
    let(:url) { account.url(ssl: true) }
    let(:agent) { users(:minimum_agent) }

    before do
      # Adds has_ip_restriction? => true
      account.subscription.update_attributes!(plan_type: SubscriptionPlanType.Large)
    end

    describe "as account with ip restrictions feature" do
      before do
        account.settings.ip_restriction_enabled = true
        account.texts.ip_restriction = '1.1.1.1'
        account.save!
      end

      it "does not allow you to login and make an API request" do
        # Calls IpWhitelistMiddleware
        post "#{url}/access/login", params: { user: { email: agent.email, password: '123456' } }, headers: { 'REMOTE_ADDR' => '2.1.1.1' }

        assert_response :forbidden

        # Calls authenticate_user -> Net::IpTools.should_block_user?
        get "#{url}/api/v2/users/me.json", headers: { 'REMOTE_ADDR' => '2.1.1.1' }

        assert_response :forbidden
      end
    end

    describe "after downgrade" do
      before do
        account.subscription.update_attributes!(plan_type: SubscriptionPlanType.Medium)
      end

      it "allows you to login and make an API request" do
        post "#{url}/access/login", params: { user: { email: agent.email, password: '123456' } }, headers: { 'REMOTE_ADDR' => '2.1.1.1' }

        assert_response :redirect
        assert_redirected_to "#{account.url(mapped: false, ssl: true)}/agent"

        get "#{url}/api/v2/users/me.json", headers: { 'REMOTE_ADDR' => '2.1.1.1' }

        assert_response :ok
        assert_equal agent.id, JSON.parse(@response.body)["user"]["id"]
      end
    end
  end
end
