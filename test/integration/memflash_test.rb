require_relative "../support/test_helper"

describe "Memflash Integration" do
  fixtures :all

  class MemflashTestController < ActionController::Base
    LARGE_STRING = ("this is a very large string " * 1000).freeze

    def set_flash
      flash[:notice] = LARGE_STRING.dup
      head :ok
    end

    def get_flash # rubocop:disable Naming/AccessorMethodName
      render plain: flash[:notice]
    end
  end

  prepend ControllerDefaultParams
  integrate_test_routes MemflashTestController

  describe "large flash messages" do
    it "makes it across via memcache" do
      get("/test/route/memflash_test/set_flash")

      get("/test/route/memflash_test/get_flash")

      MemflashTestController::LARGE_STRING.must_equal response.body
    end
  end
end
