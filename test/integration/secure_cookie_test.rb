require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class SecureCookieTest < ActionDispatch::IntegrationTest
  fixtures :all

  before do
    @user_credentials = { email: 'minimum_admin@aghassipour.com', password: '123456' }
  end

  describe "An account with SSL enabled" do
    before do
      @account = accounts(:minimum)
      Account.any_instance.stubs(is_ssl_enabled?: true)
    end

    it "informs clients to only send the shared session cookie over encrypted (HTTPS) requests" do
      get "https://minimum.zendesk-test.com/home"
      assert_match /_zendesk_shared_session.*? secure;/, response.headers['Set-Cookie']
    end

    it "informs clients to send the app session cookie over encrypted (HTTPS) requests" do
      get "https://minimum.zendesk-test.com/home"
      assert_match /_zendesk_test_Session.*? secure;/, response.headers['Set-Cookie']
    end

    it "does not set the shared session cookie for unencrypted requests" do
      get "http://minimum.zendesk-test.com/home"
      assert_no_match /shared_session/, response.headers['Set-Cookie']
    end

    it "does not set the app session cookie for unencrypted requests" do
      get "http://minimum.zendesk-test.com/home"
      assert_no_match /_zendesk_test_Session/, response.headers['Set-Cookie']
    end

    it "redirects a logged in user to HTTPS without clobbering the secure cookie" do
      post('https://minimum.zendesk-test.com/access/login', params: { user: @user_credentials })
      assert logged_in?

      get "http://minimum.zendesk-test.com/tickets/1"
      assert_no_match /shared_session/, response.headers['Set-Cookie']
      assert_redirected_to "https://minimum.zendesk-test.com/tickets/1"

      get "https://minimum.zendesk-test.com/tickets/1"
      assert_redirected_to "https://minimum.zendesk-test.com/agent/tickets/1"

      # looks like this is out of our hands now, part of lotus session establishment?
      # response.headers['Set-Cookie'].must_include "shared_session"
    end

    it "serves 'ssl_allowed' requests over HTTPS" do
      assert_redirected_to_ssl "http://minimum.zendesk-test.com/access/unauthenticated"
    end

    it "only serve requests over HTTPS" do
      assert_redirected_to_ssl "http://minimum.zendesk-test.com/home"
      assert_redirected_to_ssl "http://minimum.zendesk-test.com/users"
    end

    it "does not allow login over HTTP" do
      post('http://minimum.zendesk-test.com/access/login', params: { user: @user_credentials })
      assert_response :redirect
      assert_equal false, logged_in?
    end

    it "allows login over HTTPS" do
      post('https://minimum.zendesk-test.com/access/login', params: { user: @user_credentials })
      assert(logged_in?)
    end

    it "only serve 'ssl_allowed' requests over HTTPS" do
      assert_redirected_to_ssl "http://minimum.zendesk-test.com/access/unauthenticated"
    end

    describe "when hitting ticket_updates rate limit" do
      before do
        @user = users(:minimum_agent)
        @ticket = tickets(:minimum_6)
        @account = users(:minimum_end_user).account
        login @user
        @account.settings.ticket_update_rate_limit = 0
        @account.save!

        ApiRateLimitedMiddleware.any_instance.expects(:render_response).never
        put("https://minimum.zendesk-test.com/api/v2/tickets/#{@ticket.nice_id}.json", params: { ticket: { subject: "Updated Subject" } })
      end

      it "adds rate-limit + cache headers" do
        assert_response 429

        assert_nil response.headers["X-Rate-Limit-Remaining"]
        assert_nil response.headers["X-Rate-Limit"]
        assert_match /public/, response.headers["Cache-Control"]
        assert_no_match /shared_session/, response.headers['Set-Cookie']
        assert_no_match /zendesk_authenticated/, response.headers['Set-Cookie']
      end

      it "skips session persistence" do
        assert request.env['rack.session.options'][:skip]
        assert_equal false, request.env['zendesk.session.persistence']
      end
    end

    describe 'generated javascripts locale' do
      describe_with_arturo_disabled :assets_strip_response_cookies_generated_javascript_locale do
        it 'sets the shared session cookie' do
          get "https://minimum.zendesk-test.com/generated/javascripts/locale/0/#{ENGLISH_BY_ZENDESK.id}/123.js"

          assert_match /shared_session/, response.headers['Set-Cookie']
        end
      end

      describe_with_arturo_enabled :assets_strip_response_cookies_generated_javascript_locale do
        it 'does not set the shared session cookie' do
          get "https://minimum.zendesk-test.com/generated/javascripts/locale/0/#{ENGLISH_BY_ZENDESK.id}/123.js"

          assert_no_match /shared_session/, response.headers['Set-Cookie']
        end
      end
    end

    describe 'photos and logos' do
      describe_with_arturo_disabled :assets_strip_response_cookies_photos_and_logos do
        it 'sets the shared session cookie' do
          get "https://minimum.zendesk-test.com/cassets/system/logos/2009/0591/minimum_logo.jpg"

          assert_match /shared_session/, response.headers['Set-Cookie']
        end
      end

      describe_with_arturo_enabled :assets_strip_response_cookies_photos_and_logos do
        it 'does not set the shared session cookie' do
          get "https://minimum.zendesk-test.com/cassets/system/logos/2009/0591/minimum_logo.jpg"

          assert_no_match /shared_session/, response.headers['Set-Cookie']
        end
      end

      # Should not be affected by voice/uploads arturo

      describe_with_arturo_disabled :assets_strip_response_cookies_voice_uploads do
        it 'sets the shared session cookie' do
          get "https://minimum.zendesk-test.com/cassets/system/logos/2009/0591/minimum_logo.jpg"

          assert_match /shared_session/, response.headers['Set-Cookie']
        end
      end

      describe_with_arturo_enabled :assets_strip_response_cookies_voice_uploads do
        it 'sets the shared session cookie' do
          get "https://minimum.zendesk-test.com/cassets/system/logos/2009/0591/minimum_logo.jpg"

          assert_match /shared_session/, response.headers['Set-Cookie']
        end
      end
    end

    describe 'voice uploads' do
      describe_with_arturo_disabled :assets_strip_response_cookies_voice_uploads do
        it 'sets the shared session cookie' do
          get "https://minimum.zendesk-test.com/cassets/system/voice/uploads/2009/0589/railroad.mp3"

          assert_match /shared_session/, response.headers['Set-Cookie']
        end
      end

      describe_with_arturo_enabled :assets_strip_response_cookies_voice_uploads do
        it 'does not set the shared session cookie' do
          get "https://minimum.zendesk-test.com/cassets/system/voice/uploads/2009/0589/railroad.mp3"

          assert_no_match /shared_session/, response.headers['Set-Cookie']
        end
      end

      # Should not be affected by photos/logos arturo

      describe_with_arturo_disabled :assets_strip_response_cookies_photos_and_logos do
        it 'sets the shared session cookie' do
          get "https://minimum.zendesk-test.com/cassets/system/voice/uploads/2009/0589/railroad.mp3"

          assert_match /shared_session/, response.headers['Set-Cookie']
        end
      end

      describe_with_arturo_enabled :assets_strip_response_cookies_photos_and_logos do
        it 'sets the shared session cookie' do
          get "https://minimum.zendesk-test.com/cassets/system/voice/uploads/2009/0589/railroad.mp3"

          assert_match /shared_session/, response.headers['Set-Cookie']
        end
      end
    end

    describe 'radar_token' do
      describe_with_arturo_disabled :v2_users_radar_token_strip_session_set_cookie do
        it 'sets the shared session cookie' do
          post('https://minimum.zendesk-test.com/access/login', params: { user: @user_credentials })
          get "https://minimum.zendesk-test.com/api/v2/users/radar_token.json", headers: { 'X-Zendesk-Lotus-Version' => '1' }

          assert_match /shared_session/, response.headers['Set-Cookie']
        end
      end

      describe_with_arturo_enabled :v2_users_radar_token_strip_session_set_cookie do
        it 'does not set the shared session cookie' do
          post('https://minimum.zendesk-test.com/access/login', params: { user: @user_credentials })
          get "https://minimum.zendesk-test.com/api/v2/users/radar_token.json", headers: { 'X-Zendesk-Lotus-Version' => '1' }

          assert_no_match /shared_session/, response.headers['Set-Cookie']
        end
      end
    end
  end

  describe "non SSL enabled accounts" do
    before do
      @account = accounts(:minimum)
      assert_equal false, @account.is_ssl_enabled?
    end

    it "informs clients to only send the shared session cookie over encrypted (HTTPS) requests" do
      get "https://minimum.zendesk-test.com/home"
      assert_match /_zendesk_shared_session.*? secure;/, response.headers['Set-Cookie']
    end

    it "informs clients to send the app session cookie over encrypted (HTTPS) requests" do
      get "https://minimum.zendesk-test.com/home"
      assert_match /_zendesk_test_Session.*? secure;/, response.headers['Set-Cookie']
    end

    it "does not set the shared session cookie for unencrypted requests" do
      get "http://minimum.zendesk-test.com/home"
      assert_no_match /shared_session/, response.headers['Set-Cookie']
    end

    it "does not set the app session cookie for unencrypted requests" do
      get "http://minimum.zendesk-test.com/home"
      assert_no_match /_zendesk_test_Session/, response.headers['Set-Cookie']
    end

    it "redirects a logged in user to HTTPS without clobbering the secure cookie" do
      post('https://minimum.zendesk-test.com/access/login', params: { user: @user_credentials })
      assert logged_in?

      get "http://minimum.zendesk-test.com/tickets/1"
      assert_no_match /shared_session/, response.headers['Set-Cookie']
      assert_redirected_to "https://minimum.zendesk-test.com/tickets/1"

      get "https://minimum.zendesk-test.com/tickets/1"
      assert_redirected_to "https://minimum.zendesk-test.com/agent/tickets/1"

      # looks like this is out of our hands now, part of lotus session establishment?
      # response.headers['Set-Cookie'].must_include "shared_session"
    end

    it "serves 'ssl_allowed' requests over HTTPS" do
      assert_redirected_to_ssl "http://minimum.zendesk-test.com/access/unauthenticated"
    end

    it "only serve requests over HTTPS" do
      assert_redirected_to_ssl "http://minimum.zendesk-test.com/home"
      assert_redirected_to_ssl "http://minimum.zendesk-test.com/users"
    end

    it "does not allow login over HTTP" do
      post('http://minimum.zendesk-test.com/access/login', params: { user: @user_credentials })
      assert_response :redirect
      assert_equal false, logged_in?
    end

    it "allows login over HTTPS" do
      post('https://minimum.zendesk-test.com/access/login', params: { user: @user_credentials })
      assert(logged_in?)
    end

    it "only serve 'ssl_allowed' requests over HTTPS" do
      assert_redirected_to_ssl "http://minimum.zendesk-test.com/access/unauthenticated"
    end

    it "only serve API requests over HTTPS without redirecting" do
      get "http://minimum.zendesk-test.com/api/v2/users/me.json"
      assert_response :forbidden
      assert_equal "This is an HTTPS only API", @response.body
    end
  end

  protected

  def assert_redirected_to_ssl(url)
    assert(url =~ /http:\/\/(.*)/)
    ssl_url = 'https://' + $1
    get(url)
    assert_redirected_to ssl_url, request.url
  end
end
