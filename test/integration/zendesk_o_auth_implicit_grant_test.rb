require File.expand_path('../support/test_helper', __dir__)
require 'zendesk/testing/factories/global_uid'
require "zendesk/o_auth/testing/client_factory"

class Zendesk::OAuth::ImplicitGrantTest < ActionDispatch::IntegrationTest
  fixtures :accounts, :role_settings, :users

  before do
    @user = users(:minimum_agent)
    @client = FactoryBot.create(:client, account: (@account = accounts(:minimum)), user: @user)

    Zendesk::OAuth.configure do |config|
      config.allowed_strategies = config.valid_strategies
    end

    @host = "https://#{@account.host_name}"
    host! @host
    https!

    login(@user, password: "123456")
  end

  it "tests cancelled grant flow" do
    params = {
      client_id: @client.identifier,
      redirect_uri: (redirect_uri = "https://dev.localhost/home"), response_type: "token",
      state: (state = Zendesk::OAuth::TokenGenerator.hex(15))
    }

    get "#{@host}/oauth/authorizations/new", params: params

    assert_response :ok
    assert_template :new

    # user clicked "deny"
    params[:deny] = true

    post "#{@host}/oauth/authorizations", params: params

    assert_response :redirect
    assert @response.headers["Location"].start_with?(redirect_uri), @response.headers.inspect

    redirect_params = Rack::Utils.parse_query(@response.headers["Location"].split("#").last)

    assert_equal "access_denied", redirect_params["error"]
    assert_equal state, redirect_params["state"]
  end

  it "tests approved grant flow" do
    begin
      params = {
        client_id: @client.identifier,
        redirect_uri: (redirect_uri = "https://dev.localhost/home"), response_type: "token",
        scope: "read write",
        state: (state = Zendesk::OAuth::TokenGenerator.hex(15))
      }

      get "#{@host}/oauth/authorizations/new", params: params

      assert_response :ok
      assert_template :new

      # user clicked "approve"
      params[:approve] = true

      post "#{@host}/oauth/authorizations", params: params

      assert_response :redirect
      assert @response.headers["Location"].start_with?(redirect_uri), @response.headers.inspect

      hash_params = Rack::Utils.parse_query(@response.headers["Location"].split("#").last)

      assert_not_nil hash_params["access_token"], hash_params.inspect
      assert_nil hash_params["refresh_token"], hash_params.inspect
      assert_equal state, hash_params["state"], hash_params.inspect

      cookies.instance_variable_set(:@cookies, [])

      Api::V2::BaseController.allow_forgery_protection = true
      orig = ApplicationController.allow_forgery_protection

      authorization = { "HTTP_AUTHORIZATION" => "Bearer #{hash_params["access_token"]}", "CONTENT_TYPE" => "application/json"}

      post "#{@host}/api/v2/tickets.json", params: { ticket: { comment: { value: "test" }, subject: "test" } }.to_json, headers: authorization

      assert_response :created

      get @response.location, headers: authorization

      assert_response :ok
    ensure
      Api::V2::BaseController.allow_forgery_protection = orig
    end
  end
end
