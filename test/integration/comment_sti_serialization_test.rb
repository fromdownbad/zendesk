require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class CommentStiSerializationTest < ActionDispatch::IntegrationTest
  fixtures :events

  describe "STI serialize foo" do
    before do
      # trigger in correct load order
      [Comment, VoiceComment, TwitterAction].each { |name| name.new.type }
    end

    let(:normal) { {value: :previous} }
    let(:object) { events(:create_audit_for_minimum_ticket_1) }

    def find_object(klass)
      obj = klass.find(object.id)
      def obj.readonly?
        false
      end
      obj
    end

    it "properly reads serialized :value_previous" do
      [VoiceComment, TwitterAction].each do |klass| # do not put outside -> load-order changes
        Event.where(id: object.id).update_all(type: klass, value_previous: normal.to_yaml)
        assert_equal(normal, klass.find(object.id).value_previous)
      end
    end

    it "properly write :value_previous" do
      [VoiceComment, TwitterAction].each do |klass| # do not put outside -> load-order changes
        Event.where(id: object.id).update_all(type: klass)
        obj = find_object(klass)
        obj.update_attribute(:value_previous, normal)
        assert_equal(normal, obj.reload.value_previous)
      end
    end

    it "does not allow insecure write to value_previous" do
      [VoiceComment, TwitterAction].each do |klass| # do not put outside -> load-order changes
        Event.where(id: object.id).update_all(type: klass)
        obj = find_object(klass)

        obj.update_attribute(:value_previous, {value_previous: :previous}.to_yaml)

        assert_not_equal normal, obj.value_previous
      end
    end
  end
end
