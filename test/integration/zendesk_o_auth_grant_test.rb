require File.expand_path('../support/test_helper', __dir__)
require 'zendesk/testing/factories/global_uid'
require "zendesk/o_auth/testing/client_factory"

describe "OAuth grant Integration" do
  fixtures :accounts, :role_settings, :users

  before do
    @user = users(:minimum_agent)
    @client = FactoryBot.create(:client, account: (@account = accounts(:minimum)), user: @user)

    Zendesk::OAuth.configure do |config|
      config.allowed_strategies = config.valid_strategies
      config.default_expiry = 1.day
    end

    @host = "https://#{@account.host_name}"

    host! @host
    https!

    login(@user, password: "123456")
  end

  it "renders no such client page on mobile_v2" do
    params = {
          client_id: "fake",
          redirect_uri: "https://dev.localhost/home", response_type: "code",
          state: Zendesk::OAuth::TokenGenerator.hex(15),
          format: "mobile_v2"
    }
    get "#{@host}/oauth/authorizations/new", params: params
    assert_response 400
  end

  it "renders the authorization form on mobile_v2" do
    params = {
          client_id: @client.identifier,
          redirect_uri: "https://dev.localhost/home", response_type: "code",
          state: Zendesk::OAuth::TokenGenerator.hex(15),
          format: "mobile_v2"
    }
    get "#{@host}/oauth/authorizations/new", params: params
    assert_response :ok
  end

  it "does test cancelled grant flow" do
    params = {
      client_id: @client.identifier,
      redirect_uri: (redirect_uri = "https://dev.localhost/home"), response_type: "code",
      state: Zendesk::OAuth::TokenGenerator.hex(15)
    }

    get "#{@host}/oauth/authorizations/new", params: params

    assert_response :ok
    assert_template :new

    # user clicked "deny"
    params[:deny] = true

    post "#{@host}/oauth/authorizations", params: params

    assert_response :redirect
    assert @response.headers["Location"].start_with?(redirect_uri), @response.headers.inspect

    redirect_params = Rack::Utils.parse_query(@response.headers["Location"].split("?").last)

    assert_equal "access_denied", redirect_params["error"]
  end

  it "does test approved grant flow" do
    params = {
      client_id: @client.identifier,
      redirect_uri: (redirect_uri = "https://dev.localhost/home"), response_type: "code",
      state: (state = Zendesk::OAuth::TokenGenerator.hex(15))
    }

    get "#{@host}/oauth/authorizations/new", params: params

    assert_response :ok
    assert_template :new

    # user clicked "approve"
    params[:approve] = true

    post "/oauth/authorizations", params: params

    assert_response :redirect
    assert @response.headers["Location"].start_with?(redirect_uri), @response.headers.inspect

    redirect_params = Rack::Utils.parse_query(@response.headers["Location"].split("?").last)

    assert_not_nil redirect_params["code"], redirect_params.inspect
    assert_equal state, redirect_params["state"]

    params = {
      client_id: @client.identifier,
      client_secret: @client.secret(:full),
      redirect_uri: redirect_uri,
      code: redirect_params["code"],
      grant_type: "authorization_code"
    }

    post "#{@host}/oauth/tokens", params: params

    body = JSON.parse(@response.body)

    assert_response :ok
    assert_not_nil body["access_token"], body.inspect
    assert_not_nil body["refresh_token"], body.inspect
  end
end
