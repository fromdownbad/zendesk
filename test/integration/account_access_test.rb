require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class AccountAccessTest < ActionDispatch::IntegrationTest
  include ZendeskDSL

  describe "Settings level access" do
    fixtures :all

    before do
      @sections = %w[/settings/channels /settings/customers /settings/account]
      stub_request(:get, "https://minimum.zendesk-test.com/agent/tessaManifest.json")
    end

    it "does not be possible for anonymous users" do
      self.host = accounts(:minimum).host_name
      @sections.each do |section|
        goes_to_login section
      end
    end

    it "does not be possible for end users or admins" do
      ['minimum_end_user@aghassipour.com', 'minimum_agent@aghassipour.com'].each do |email|
        self.host = accounts(:minimum).host_name
        post '/access/login', params: { user: { email: email, password: '123456'} }
        redirected_to_hc?

        @sections.each do |section|
          get section
          assert_response 403 # Do not get routed to login as they have a session, access denied
        end
      end
    end

    it "redirects admins to lotus" do
      self.host = accounts(:minimum).host_name
      post '/access/login', params: { user: { email: 'minimum_admin@aghassipour.com', password: '123456'} }
      assert_redirected_to "https://minimum.zendesk-test.com/agent"

      @sections.each do |section|
        get section
        assert_response :success
      end
    end
  end
end
