require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"
require_relative "../support/api_test_helper"

class CsrfTest < ActionDispatch::IntegrationTest
  include ApiTestHelper

  fixtures :all

  CONTROLLERS = [AccessController, ActionController::Base].freeze

  before do
    CONTROLLERS.each { |c| c.allow_forgery_protection = true }
  end

  teardown do
    CONTROLLERS.each { |c| c.allow_forgery_protection = false }
  end

  describe 'login' do
    before do
      @user = users(:minimum_agent)

      @account = accounts(:minimum)
      @account.is_ssl_enabled = true
      @account.save!
    end

    describe 'password login' do
      before do
        visit "#{@user.account.url(mapped: false, protocol: 'https')}/auth/v2/login/signin"
        fill_in('user[email]', with: @user.email)
        fill_in('user[password]', with: '123456')
      end

      [['unauthenticated', 'login-form']].each do |login_path, form|
        describe login_path do
          it "is able to post with valid authenticity_token" do
            submit_form(form)
            assert_equal @user, warden.user
            assert_response :success
          end

          it "is not be able to post without authenticity_token" do
            set_hidden_field('authenticity_token', to: '')
            submit_form(form)
            assert_nil warden.user
            assert_response :forbidden
          end
        end
      end
    end
  end

  describe 'protect from forgery with invalid token' do
    before do
      PingController.stubs(:allow_forgery_protection).returns true # normally of in test env
      PingController.log_warning_on_csrf_failure = false
      Arturo.disable_feature!(:log_csrf_invalid_token)
      Arturo.disable_feature!(:log_csrf_invalid_token_debug)
    end

    it "fails and does not log" do
      Rails.logger.expects(:warn).with('InvalidAuthenticityToken caught for Account(No-Account) /ping/host')
      ZendeskExceptions::Logger.expects(:record).never
      post "/ping/host", params: { authenticity_token: "Foooooo" }
      assert_response :forbidden
    end
  end

  describe 'logs extra debugging info when arturo is enabled' do
    before do
      login(users(:minimum_admin))
      PingController.stubs(:allow_forgery_protection).returns true # normally of in test env
      PingController.log_warning_on_csrf_failure = false
      Arturo.disable_feature!(:log_csrf_invalid_token)
      Arturo.enable_feature!(:log_csrf_invalid_token_debug)
    end

    it "fails and does not log" do
      ZendeskExceptions::Logger.expects(:record).once
      post "/ping/host", params: { authenticity_token: "Foooooo" }
      assert_response :forbidden
    end
  end

  describe "logs_invalid_token information" do
    let(:statsd) { stub_for_statsd }
    before do
      login(users(:minimum_admin))
      PingController.stubs(:allow_forgery_protection).returns true # normally of in test env
      PingController.log_warning_on_csrf_failure = false
      Arturo.enable_feature!(:log_csrf_invalid_token)
      Arturo.enable_feature!(:log_csrf_invalid_token_debug)
      @account = accounts(:minimum)
      @session_token = @account.shared_sessions&.first.try(:data).dig('csrf_token')
      Zendesk::StatsD::Client.stubs(:new).returns(statsd)
    end

    it "logs mismatched tokens and add metrics" do
      Rails.logger.expects(:warn).with("InvalidAuthenticityToken caught for Account(#{@account.id}) /ping/host")
      Rails.logger.expects(:warn).with("InvalidAuthenticityToken: Authenticity token from session #{@session_token} with request token as Foooooo")
      ZendeskExceptions::Logger.expects(:record).once

      statsd.expects(:increment).with('invalid_token.mismatch').once

      post "/ping/host", params: { authenticity_token: "Foooooo" }
      assert_response :forbidden
    end
  end

  describe "logs_invalid token when missing request token" do
    let(:statsd) { stub_for_statsd }
    before do
      login(users(:minimum_admin))
      PingController.stubs(:allow_forgery_protection).returns true # normally of in test env
      PingController.log_warning_on_csrf_failure = false
      Arturo.enable_feature!(:log_csrf_invalid_token)
      Arturo.disable_feature!(:log_csrf_invalid_token_debug)
      @account = accounts(:minimum)
      @session_token = @account.shared_sessions&.first.try(:data).dig('csrf_token')
      Zendesk::StatsD::Client.stubs(:new).returns(statsd)
    end

    it "fails on invalid access with request token missing" do
      Rails.logger.expects(:warn).with("InvalidAuthenticityToken caught for Account(#{@account.id}) /ping/host")
      Rails.logger.expects(:warn).with("InvalidAuthenticityToken: Authenticity token from session #{@session_token} with request token as ")

      statsd.expects(:increment).with('invalid_token.request_token_missing').once

      post "/ping/host", params: { authenticity_token: '' }
      assert_response :forbidden
    end
  end

  describe "logs_invalid token when missing session token" do
    let(:statsd) { stub_for_statsd }
    before do
      login(users(:minimum_admin))
      PingController.stubs(:allow_forgery_protection).returns true # normally of in test env
      PingController.log_warning_on_csrf_failure = false
      Arturo.enable_feature!(:log_csrf_invalid_token)
      Arturo.disable_feature!(:log_csrf_invalid_token_debug)

      @account = accounts(:minimum)
      @account.shared_sessions.delete_all
      @account.save!
      Zendesk::StatsD::Client.stubs(:new).returns(statsd)
    end

    it "fails on invalid access with request token missing" do
      Rails.logger.expects(:warn).with("InvalidAuthenticityToken caught for Account(#{@account.id}) /ping/host")
      Rails.logger.expects(:warn).with("InvalidAuthenticityToken: Authenticity token missing from session with only request token as Fooo")

      statsd.expects(:increment).with('invalid_token.session_token_missing').once

      post "/ping/host", params: { authenticity_token: 'Fooo' }
      assert_response :forbidden
    end
  end

  describe ".protect_from_forgery" do
    before do
      PingController.stubs(:allow_forgery_protection).returns true # normally of in test env
    end

    it "fails on invalid access without token" do
      post "/ping/host"
      assert_response :forbidden
    end

    it "fails on invalid access with invalid token" do
      post "/ping/host", params: { authenticity_token: "Foooooo" }
      assert_response :forbidden
    end

    describe "accessing https" do
      before do
        PingController.any_instance.stubs(action_requires_ssl?: true)
        PingController.any_instance.stubs(current_account: accounts(:minimum))
        @url = "https://#{accounts(:minimum).host_name}/ping/host"
      end

      it "succeeds on access from http with invalid token" do
        Account.any_instance.stubs(is_ssl_enabled?: false, ssl_should_be_used?: false) # could be coming from http
        Account.any_instance.stubs(has_restrict_csrf_bypass_to_mobile?: false)
        post @url, params: { authenticity_token: "Foooooo" }
        assert_response :success
      end

      it "fails on access from https with invalid token" do
        Account.any_instance.stubs(is_ssl_enabled?: true) # must be coming from https
        post @url, params: { authenticity_token: "Foooooo" }
        assert_response :forbidden
      end
    end

    it "succeeds on valid access" do
      PingController.any_instance.expects(:form_authenticity_token).at_least_once.returns "Foooooo"
      post "/ping/host", params: { authenticity_token: "Foooooo" }
      assert_response :success
    end

    describe "GET /api/v2/users/me.json as an agent" do
      before do
        login(users(:minimum_admin))
      end

      it "propagates a valid CSRF authenticity_token to the shared session" do
        # Lotus asks for information abouth the logged in user.
        get "https://#{accounts(:minimum).host_name}/api/v2/users/me.json"

        # We get an authenticity_token in a successful response.
        assert_response :success
        authenticity_token = JSON.parse(@response.body)['user']['authenticity_token']
        assert_not_nil authenticity_token

        # We have the same token in the shared session.
        assert_equal authenticity_token, shared_session[:csrf_token]

        # Furtermore we test that token it's a valid token that can be used
        # for any POST request. E.g. creation of tickets
        Api::V2::TicketsController.stubs(:allow_forgery_protection).returns true
        post "https://#{accounts(:minimum).host_name}/api/v2/tickets.json", params: { 'ticket' => { 'subject' => 'Test', 'comment' => { 'body' => 'ciao' } } }.to_json, headers: { 'HTTP_X_CSRF_TOKEN' => authenticity_token, "CONTENT_TYPE" => "application/json" }
        assert_response :success

        # The same request with a wrong authenticity_token fails
        post "https://#{accounts(:minimum).host_name}/api/v2/tickets.json", params: { 'ticket' => { 'subject' => 'Test', 'comment' => { 'body' => 'ciao' } } }.to_json, headers: { 'HTTP_X_CSRF_TOKEN' => 'WRONG-TOKEN', "CONTENT_TYPE" => "application/json" }
        assert_response :forbidden
        assert_equal({"error" => {"title" => "Forbidden", "message" => "Invalid authenticity token"}}, JSON.parse(@response.body))
      end
    end
  end
end
