require_relative "../../support/test_helper"
require_relative '../../support/channels_test_helper'
require 'zendesk_comment_markup'

describe Channels::Converter::IncomingConversionWorker do
  include ChannelsTestHelper
  fixtures :accounts, :sequences, :users, :monitored_twitter_handles, :channels_resources, :facebook_pages,
    :channels_brands, :user_identities, :twitter_user_profiles

  before do
    @account = accounts(:minimum)
    @max_conversion_milliseconds = 10_000
  end

  def assert_count_diffs(user_profile_class_name)
    assert_difference('Ticket.count(:all)', 1) do
      assert_difference('Comment.count(:all)', 1) do
        assert_difference('EventDecoration.count(:all)', 1) do
          assert_difference('::Channels::Resource.count(:all)', 1) do
            assert_difference("::Channels::#{user_profile_class_name}.count(:all)", 1) do
              yield
            end
          end
        end
      end
    end
  end

  def imported_objects(via_id, profile_class)
    ticket = Ticket.where(via_id: via_id).order('id DESC').first
    comment = Comment.where(ticket_id: ticket.id).order('id DESC').first
    event_decoration = EventDecoration.where(event_id: comment.id).order('id DESC').first
    channels_profile = profile_class.order('created_at DESC, id DESC').first

    [ticket, comment, event_decoration, channels_profile]
  end

  def new_conversion_worker(source, payload, resource_type)
    conversion = Channels::IncomingConversion.create!(
      account_id: @account.id,
      external_id: "233",
      thread_id: '344',
      source_id: source.id,
      payload: payload,
      resource_type: resource_type
    )
    ::Channels::Converter::IncomingConversionWorker.new(conversion)
  end

  def twitter_assertions(payload, ticket, comment, event_decoration, channels_profile, source, resource_type, user_key)
    assert_attributes_from_hash(
      payload,
      [
      # [path in payload,                 actual value,                                   label]
        ['text',                          ticket.subject,                                 'ticket.subject'],
        ['text',                          comment.body,                                   'comment.body'],
        ['id_str',                        event_decoration.data.external_id,              'ed.external_id'],
        [[user_key, 'id_str'],            event_decoration.data.author.id,                'ed.id'],
        [[user_key, 'name'],              event_decoration.data.author.name,              'ed.name'],
        [[user_key, 'screen_name'],       event_decoration.data.author.screen_name,       'ed.screen_name'],
        ['id_str',                        event_decoration.data.external_id,              'ed.external_id'],
        [[user_key, 'id_str'],            channels_profile.external_id,                   'tup.external_id'],
        [[user_key, 'screen_name'],       channels_profile.name,                          'tup.name'],
        [[user_key, 'location'],          channels_profile.metadata[:location],           'tup.location'],
        [[user_key, 'url'],               channels_profile.metadata[:url],                'tup.url'],
        [[user_key, 'followers_count'],   channels_profile.metadata[:followers_count],    'tup.followers_count'],
        [[user_key, 'protected'],         channels_profile.metadata[:protected],          'tup.protected'],
        [[user_key, 'verified'],          channels_profile.metadata[:verified],           'tup.verified'],
        [[user_key, 'statuses_count'],    channels_profile.metadata[:statuses_count],     'tup.statuses_count'],
        [[user_key, 'friends_count'],     channels_profile.metadata[:friends_count],      'tup.friends_count'],
        [[user_key, 'lang'],              channels_profile.metadata[:lang],               'tup.lang'],
        [[user_key, 'description'],       channels_profile.metadata[:description],        'tup.description'],
        [[user_key, 'time_zone'],         channels_profile.metadata[:time_zone],          'tup.time_zone'],
        [[user_key, 'utc_offset'],        channels_profile.metadata[:utc_offset],         'tup.utc_offset'],
        [[user_key, 'name'],              channels_profile.metadata[:display_name],       'tup.user.display_name'],
        [[user_key, 'created_at'],        channels_profile.metadata[:twitter_created_at], 'tup.user.twitter_created_at'],
        [[user_key, 'favourites_count'],  channels_profile.metadata[:favorites_count],    'tup.user.favorites_count']
      ]
    )
    expected_image_url = payload[user_key]['profile_image_url_https'].gsub('_normal.', '.')
    assert_equal expected_image_url,          event_decoration.data.author.photo_url,   'ed.photo_url'
    assert_equal resource_type,               event_decoration.data.resource_type,      'ed.resource_type'
    assert_equal source.twitter_user_id,      event_decoration.data.source.id,          'ed.source.id'
    assert_equal source.id,                   event_decoration.data.source.zendesk_id,  'ed.source.zendesk_id'
    assert_equal source.twitter_screen_name,  event_decoration.data.source.screen_name, 'ed.source.screen_name'

    assert_equal expected_image_url,          channels_profile.metadata[:profile_image_url]
  end

  def twitter_dm_assertions(payload, user_payload, ticket, comment, event_decoration, channels_profile, source)
    assert_attributes_from_hash(
      payload,
      [
      # [path in payload,                                       actual value,                           label]
        [['event', 'message_create', 'message_data', 'text'],   ticket.subject,                         'ticket.subject'],
        [['event', 'message_create', 'message_data', 'text'],   comment.body,                           'comment.body'],
        [['event', 'id'],                                       event_decoration.data.external_id,      'ed.external_id'],
        [['event', 'message_create', 'sender_id'],              event_decoration.data.author.id,        'ed.id'],
        [['event', 'message_create', 'sender_id'],              channels_profile.external_id,           'tup.external_id'],
      ]
    )
    assert_attributes_from_hash(
      user_payload,
      [
      # [path in payload,     actual value,                                       label]
        ['name',              event_decoration.data.author.name,                  'ed.name'],
        ['screen_name',       event_decoration.data.author.screen_name,           'ed.screen_name'],
        ['screen_name',       channels_profile.name,                              'tup.name'],
        ['location',          channels_profile.metadata[:location],               'tup.location'],
        ['url',               channels_profile.metadata[:url],                    'tup.url'],
        ['followers_count',   channels_profile.metadata[:followers_count],        'tup.followers_count'],
        ['protected',         channels_profile.metadata[:protected],              'tup.protected'],
        ['verified',          channels_profile.metadata[:verified],               'tup.verified'],
        ['statuses_count',    channels_profile.metadata[:statuses_count],         'tup.statuses_count'],
        ['friends_count',     channels_profile.metadata[:friends_count],          'tup.friends_count'],
        ['lang',              channels_profile.metadata[:lang],                   'tup.lang'],
        ['description',       channels_profile.metadata[:description],            'tup.description'],
        ['time_zone',         channels_profile.metadata[:time_zone],              'tup.time_zone'],
        ['utc_offset',        channels_profile.metadata[:utc_offset],             'tup.utc_offset'],
        ['name',              channels_profile.metadata[:display_name],           'tup.user.display_name'],
        ['created_at',        channels_profile.metadata[:twitter_created_at],     'tup.user.twitter_created_at'],
        ['favourites_count',  channels_profile.metadata[:favorites_count],        'tup.user.favorites_count']
      ]
    )
    expected_image_url = user_payload['profile_image_url_https'].gsub('_normal.', '.')
    assert_equal expected_image_url,          event_decoration.data.author.photo_url,         'ed.photo_url'
    assert_equal 'twitter_dm',                event_decoration.data.resource_type,            'ed.resource_type'
    assert_equal source.twitter_user_id,      event_decoration.data.source.id,                'ed.source.id'
    assert_equal source.id,                   event_decoration.data.source.zendesk_id,        'ed.source.zendesk_id'
    assert_equal source.twitter_screen_name,  event_decoration.data.source.screen_name,       'ed.source.screen_name'

    assert_equal expected_image_url,          channels_profile.metadata[:profile_image_url]
  end

  describe 'twitter' do
    before do
      @source = monitored_twitter_handles(:minimum_monitored_twitter_handle_1)
    end

    describe 'twitter status' do
      before do
        @payload = read_json('twitter_status_1.1.json')
        @worker = new_conversion_worker(@source, @payload, ::Channels::Constants::TWITTER_STATUS)
      end

      it "goes through the creation flow for a new ticket" do
        ::Channels::Converter::ConversionJob.expects(:enqueue_in).never
        assert_count_diffs('TwitterUserProfile') do
          @worker.perform_conversion(@max_conversion_milliseconds)

          # This is a 'favorite' because the MTH user is not mentioned in the Tweet.  See Channels::TwitterApi::StatusExtractor.
          ticket, comment, event_decoration, channels_profile = imported_objects(ViaType.TWITTER_FAVORITE, ::Channels::TwitterUserProfile)
          twitter_assertions(@payload, ticket, comment, event_decoration, channels_profile, @source, 'twitter_status', 'user')
        end
      end
    end

    describe 'twitter direct message' do
      before do
        @payload = read_json('twitter_direct_message.json')
        @user_payload = read_json('twitter_users_show_110542029_1.1.json')
        @worker = new_conversion_worker(@source, @payload['event'], ::Channels::Constants::TWITTER_DM)
        # We now fetch user data to hydrate the DM event object.
        stub_request(:get, "https://api.twitter.com/1.1/users/show.json?user_id=#{@user_payload['id']}").
          to_return(
            status: 200,
            body: @user_payload.to_json,
            headers: { 'Content-Type' => 'application/json' }
          )
      end

      it "goes through the creation flow for a new ticket" do
        ::Channels::Converter::ConversionJob.expects(:enqueue_in).never
        assert_count_diffs('TwitterUserProfile') do
          @worker.perform_conversion(@max_conversion_milliseconds)

          ticket, comment, event_decoration, channels_profile = imported_objects(ViaType.TWITTER_DM, ::Channels::TwitterUserProfile)
          twitter_dm_assertions(@payload, @user_payload, ticket, comment, event_decoration, channels_profile, @source)
        end
      end
    end
  end

  def facebook_assertions(payload, ticket, event_decoration, channels_profile, source, resource_type)
    assert_attributes_from_hash(
      payload,
      [
        # [path in payload, actual value, label]
        ['message',         ticket.subject,                     'ticket.subject'],
        [['from', 'id'],    event_decoration.data.author.id,    'ed.id'],
        [['from', 'name'],  event_decoration.data.author.name,  'ed.name'],
        ['id',              event_decoration.data.external_id,  'ed.external_id'],
        [['from', 'id'],    channels_profile.external_id,       'up.external_id'],
        [['from', 'name'],  channels_profile.name,              'up.name'],
        [['from', 'id'],    channels_profile.metadata['id'],    'up.metadata.external_id'],
        [['from', 'name'],  channels_profile.metadata['name'],  'up.metadata.name']
      ]
    )
    assert_equal "https://graph.facebook.com/#{payload['from']['id']}/picture?type=large",
      event_decoration.data.author.photo_url, 'ed.photo_url'
    assert_equal resource_type,          event_decoration.data.resource_type,      'ed.resource_type'
    assert_equal source.graph_object_id, event_decoration.data.source.id,          'ed.source.id'
    assert_equal source.id,              event_decoration.data.source.zendesk_id,  'ed.source.zendesk_id'
    assert_equal source.name,            event_decoration.data.source.name,        'ed.source.name'
  end

  describe 'facebook' do
    before do
      @source = facebook_pages(:minimum_facebook_page_1)
    end

    describe 'facebook wall post' do
      before do
        @payload = read_json('facebook_wall_post.json')
        @worker = new_conversion_worker(@source, @payload, ::Channels::Constants::FACEBOOK_POST)
      end

      it "goes through the creation flow for a new ticket" do
        ::Channels::Converter::ConversionJob.expects(:enqueue_in).never
        assert_count_diffs('FacebookUserProfile') do
          @worker.perform_conversion(@max_conversion_milliseconds)

          ticket, comment, event_decoration, channels_profile = imported_objects(ViaType.FACEBOOK_POST, ::Channels::FacebookUserProfile)
          facebook_assertions(@payload, ticket, event_decoration, channels_profile, @source, 'facebook_post')
          assert_attributes_from_hash(
            @payload,
            [
              ['message', comment.body, 'comment.body']
            ]
          )
        end
      end
    end

    describe 'facebook message' do
      before do
        @payload = read_json('facebook_message_with_attachments_and_shares.json')
        @worker = new_conversion_worker(@source, @payload, ::Channels::Constants::FACEBOOK_MESSAGE)
      end

      it "goes through the creation flow for a new ticket" do
        ::Channels::Converter::ConversionJob.expects(:enqueue_in).never
        assert_count_diffs('FacebookUserProfile') do
          @worker.perform_conversion(@max_conversion_milliseconds)

          ticket, comment, event_decoration, channels_profile = imported_objects(ViaType.FACEBOOK_MESSAGE, ::Channels::FacebookUserProfile)
          facebook_assertions(@payload, ticket, event_decoration, channels_profile, @source, 'facebook_message')
          expected_html_body = Channels::FacebookApi::MessageExtractor.new(@payload,
            @worker.conversion.source,
            @worker.conversion.resource_type).html_body(@worker.conversion)
          expected_html_body = ZendeskCommentMarkup::StripperFilter.to_html(expected_html_body)
          assert_equal expected_html_body, comment.body, 'comment.body'
          assert_attributes_from_hash(
            @payload,
            [
              [['shares', 'data'],      event_decoration.data.links.shares, 'ed.shares'],
              [['attachments', 'data'], event_decoration.data.links.attachments, 'ed.attachments'],
              [['from', 'email'], channels_profile.metadata['email'], 'up.metadata.email']
            ]
          )
        end
      end
    end
  end
end
