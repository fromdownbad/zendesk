require_relative "../../support/test_helper"

describe Channels::Converter::SynchronousTicketCreator do
  fixtures :accounts, :monitored_twitter_handles, :channels_brands, :sequences

  before do
    @account = accounts(:minimum)
  end

  describe "#create_ticket" do
    before do
      @mth = monitored_twitter_handles(:minimum_monitored_twitter_handle_1)
      @creator = Channels::Converter::SynchronousTicketCreator.new(
        @account, Channels::Constants::TWITTER_STATUS
      )
      stub_request(:get, "https://api.twitter.com/1.1/statuses/show/22039567313928192.json").
        to_return(status: 200,
                  body: read_test_file('twitter_status_1.1.json'),
                  headers: { 'Content-Type' => 'application/json' })
      stub_request(:get, "https://api.twitter.com/1.1/users/show.json?user_id=153594297").
        to_return(status: 200,
                  body: read_test_file('twitter_users_show_153594297_1.1.json'),
                  headers: { 'Content-Type' => 'application/json' })
      @ticket_params = {
        monitored_twitter_handle_id: @mth.id,
        twitter_status_message_id: '22039567313928192'
      }
    end

    it "creates Ticket, Comment, EventDecoration, IncomingChannelsConversion, and ChannelsResource objects" do
      assert_difference('Ticket.count(:all)', 1) do
        assert_difference('Comment.count(:all)', 1) do
          assert_difference('EventDecoration.count(:all)', 1) do
            assert_difference('::Channels::IncomingConversion.count(:all)', 1) do
              assert_difference('::Channels::Resource.count(:all)', 1) do
                ticket_id = @creator.create_ticket(@mth.id, '22039567313928192', ticket_params: @ticket_params)
                assert_equal @account.tickets.last.id, ticket_id
                assert_equal Channels::Resource.last.source_id, @mth.id
                assert_equal Channels::Resource.last.resource_type, ::Channels::Constants::TWITTER_STATUS
              end
            end
          end
        end
      end
    end

    describe "with errors" do
      # We must run this test in Resque.inline false mode because the ConversionJob enqueues itself and causes an infinite loop
      # in the process
      before do
        @old_resque_inline_status = Resque.inline
        Resque.inline = false
      end

      after { Resque.inline = @old_resque_inline_status }

      it "raises a Conversion error if the conversion did not succeed" do
        Channels::Converter::IncomingConversionWorker.any_instance.stubs(:find_or_create_link!).raises(RuntimeError.new("error info"))
        assert_raises(Channels::Errors::ConversionError) do
          @creator.create_ticket(@mth.id, '22039567313928192', ticket_params: @ticket_params)
        end
      end
    end

    it "skips all work and raise if conversion is filtered" do
      ::Channels::Converter::SynchronousConversionFactory.any_instance.expects(:create_incoming_conversion).returns nil
      refute_difference('Ticket.count(:all)') do
        refute_difference('Comment.count(:all)') do
          refute_difference('EventDecoration.count(:all)') do
            refute_difference('::Channels::IncomingConversion.count(:all)') do
              refute_difference('::Channels::Resource.count(:all)') do
                assert_raises Channels::Errors::FilterError do
                  @creator.create_ticket(@mth.id, '22039567313928192', ticket_params: @ticket_params)
                end
              end
            end
          end
        end
      end
    end
  end
end
