require_relative "../../support/test_helper"
require_relative '../../support/channels_test_helper'

describe Channels::Converter::OutgoingConversionWorker do
  include ChannelsTestHelper
  fixtures :accounts, :sequences, :tickets, :events, :monitored_twitter_handles, :facebook_pages, :channels_resources

  before do
    @account = accounts(:minimum)
    @comment = events(:create_comment_for_minimum_ticket_2)
    @thread_id = "123"
    @max_conversion_milliseconds = 10_000
  end

  def new_conversion_worker(source, resource_type)
    conversion = Channels::OutgoingConversion.create!(
      thread_id: @thread_id,
      source_id: source.id,
      resource_type: resource_type,
      ticket_id: @comment.ticket.id,
      comment_id: @comment.id,
      text: 'test text',
      account_id: @account.id
    )
    Channels::Converter::OutgoingConversionWorker.new(conversion)
  end

  def twitter_assertions(source, event_decoration, channels_resource, payload, resource_type, user_key)
    assert_attributes_from_hash(
      payload,
      [
        # [path in payload,             actual value,                                   label]
        ['id_str',                      event_decoration.data.external_id,              'ed.external_id'],
        [[user_key, 'id_str'],          event_decoration.data.author.id,                'ed.id'],
        [[user_key, 'name'],            event_decoration.data.author.name,              'ed.name'],
        [[user_key, 'screen_name'],     event_decoration.data.author.screen_name,       'ed.screen_name'],
        ['id_str',                      channels_resource.external_id,                  'cr.external_id'],
      ]
    )
    expected_image_url = payload[user_key]['profile_image_url_https'].gsub('_normal.', '.')
    assert_equal expected_image_url,          event_decoration.data.author.photo_url,   'ed.photo_url'
    assert_equal resource_type,               event_decoration.data.resource_type,      'ed.resource_type'
    assert_equal source.twitter_user_id,      event_decoration.data.source.id,          'ed.source.id'
    assert_equal source.id,                   event_decoration.data.source.zendesk_id,  'ed.source.zendesk_id'
    assert_equal source.twitter_screen_name,  event_decoration.data.source.screen_name, 'ed.source.screen_name'
    assert_equal resource_type,               channels_resource.resource_type,          'cr.resource_type'
    empty_hash = {}
    assert_equal empty_hash, channels_resource.metadata, 'cr.metadata'
  end

  def twitter_dm_assertions(source, event_decoration, channels_resource, payload, user_payload)
    assert_attributes_from_hash(
      payload,
      [
        # [path in payload,                           actual value,                                   label]
        [['event', 'id'],                             event_decoration.data.external_id,              'ed.external_id'],
        [['event', 'message_create', 'sender_id'],    event_decoration.data.author.id,                'ed.id'],
        [['event', 'id'],                             channels_resource.external_id,                  'cr.external_id']
      ]
    )
    assert_attributes_from_hash(
      user_payload,
      [
        # [path in payload,                           actual value,                                   label]
        ['name',                                      event_decoration.data.author.name,              'ed.name'],
        ['screen_name',                               event_decoration.data.author.screen_name,       'ed.screen_name']
      ]
    )
    expected_image_url = user_payload['profile_image_url_https'].gsub('_normal.', '.')
    assert_equal expected_image_url,                  event_decoration.data.author.photo_url,         'ed.photo_url'
    assert_equal 'twitter_dm',                        event_decoration.data.resource_type,            'ed.resource_type'
    assert_equal source.twitter_user_id,              event_decoration.data.source.id,                'ed.source.id'
    assert_equal source.id,                           event_decoration.data.source.zendesk_id,        'ed.source.zendesk_id'
    assert_equal source.twitter_screen_name,          event_decoration.data.source.screen_name,       'ed.source.screen_name'
    assert_equal 'twitter_dm',                        channels_resource.resource_type,                'cr.resource_type'
    empty_hash = {}
    assert_equal empty_hash, channels_resource.metadata, 'cr.metadata'
  end

  describe 'twitter' do
    before do
      @source = monitored_twitter_handles(:minimum_monitored_twitter_handle_1)
    end

    describe 'twitter status' do
      before do
        payload_text = read_test_file('twitter_status_1.1.json')
        @payload = JSON.parse(payload_text)
        @worker = new_conversion_worker(@source, Channels::Constants::TWITTER_STATUS)
        stub_request(:post, "https://api.twitter.com/1.1/statuses/update.json").
          with(body: {"auto_populate_reply_metadata" => "false", "in_reply_to_status_id" => "sample_id", "status" => "test text"}).
          to_return(
            status: 200,
            body: payload_text,
            headers: { 'Content-Type' => 'application/json' }
          )
      end

      it "goes through the creation flow for a new comment" do
        Channels::Converter::ConversionJob.expects(:enqueue).never
        assert_difference('EventDecoration.count(:all)', 1) do
          assert_difference('::Channels::Resource.count(:all)', 1) do
            @worker.perform_conversion(@max_conversion_milliseconds)
            event_decoration = EventDecoration.where(event_id: @comment.id).first
            channels_resource = Channels::Resource.where(thread_id: @thread_id, external_id: @payload['id_str']).first
            twitter_assertions(@source, event_decoration, channels_resource, @payload, 'twitter_status', 'user')
          end
        end
      end
    end

    describe 'twitter direct message' do
      before do
        payload_text = read_test_file('twitter_direct_message.json')
        @payload = JSON.parse(payload_text)
        user_payload_text = read_test_file('twitter_users_show_110542029_1.1.json')
        @user_payload = JSON.parse(user_payload_text)
        @worker = new_conversion_worker(@source, Channels::Constants::TWITTER_DM)
        stub_request(:post, "https://api.twitter.com/1.1/direct_messages/events/new.json").
          with(
            body: {
              event: {
                type: 'message_create',
                message_create: {
                  target: { recipient_id: @thread_id.to_i },
                  message_data: { text: 'test text' }
                }
              }
            }.to_json
          ).
          to_return(
            status: 200,
            body: payload_text,
            headers: { 'Content-Type' => 'application/json' }
          )
        # We now fetch user data to hydrate the DM event object.
        stub_request(:get, "https://api.twitter.com/1.1/users/show.json?user_id=#{@user_payload['id']}").
          to_return(
            status: 200,
            body: user_payload_text,
            headers: { 'Content-Type' => 'application/json' }
          )
      end

      it "goes through the creation flow for a new comment" do
        Channels::Converter::ConversionJob.expects(:enqueue).never
        assert_difference('EventDecoration.count(:all)', 1) do
          assert_difference('::Channels::Resource.count(:all)', 1) do
            @worker.perform_conversion(@max_conversion_milliseconds)
            event_decoration = EventDecoration.where(event_id: @comment.id).first
            channels_resource = Channels::Resource.where(thread_id: @thread_id, external_id: @payload['event']['id']).first
            twitter_dm_assertions(@source, event_decoration, channels_resource, @payload, @user_payload)
          end
        end
      end
    end
  end

  def facebook_assertions(source, event_decoration, channels_resource, payload, resource_type)
    assert_attributes_from_hash(payload,
      [
        # [path in payload, actual value,                       label]
        [['from', 'id'],    event_decoration.data.author.id,    'ed.id'],
        [['from', 'name'],  event_decoration.data.author.name,  'ed.name'],
        ['id',              event_decoration.data.external_id,  'ed.external_id'],
        ['id',              channels_resource.external_id,      'cr.external_id'],
      ])

    assert_equal "https://graph.facebook.com/#{payload['from']['id']}/picture?type=large",
      event_decoration.data.author.photo_url, 'ed.photo_url'
    assert_equal resource_type,               event_decoration.data.resource_type,      'ed.resource_type'
    assert_equal source.graph_object_id,      event_decoration.data.source.id,          'ed.source.id'
    assert_equal source.id,                   event_decoration.data.source.zendesk_id,  'ed.source.zendesk_id'
    assert_equal source.name,                 event_decoration.data.source.name,        'ed.source.name'
    assert_equal resource_type,               channels_resource.resource_type,          'cr.resource_type'
    assert_equal @thread_id,                  channels_resource.thread_id,              'cr.thread_id'
    empty_hash = {}
    assert_equal empty_hash, channels_resource.metadata, 'cr.metadata'
  end

  describe 'facebook' do
    before do
      @source = facebook_pages(:minimum_facebook_page_1)
    end

    describe 'facebook wall post' do
      before do
        payload_text = read_test_file('facebook_wall_post.json')
        @payload = JSON.parse(payload_text)
        @worker = new_conversion_worker(@source, Channels::Constants::FACEBOOK_POST)
        stub_request(:post, "https://graph.facebook.com/v3.2/123/comments").
          with(body: {"access_token" => @source.access_token, "appsecret_proof" => facebook_appsecret_proof(@source.access_token), "message" => "test text"}).
          to_return(
            status: 200,
            body: payload_text,
            headers: { 'Content-Type' => 'application/json' }
          )

        stub_request(:get, "https://graph.facebook.com/v3.2/#{@payload['id']}?access_token=#{@source.access_token}&appsecret_proof=#{facebook_appsecret_proof(@source.access_token)}").
          with(headers: {'User-Agent' => 'Faraday v0.11.0'}).
          to_return(status: 200, body: payload_text, headers: { 'Content-Type' => 'application/json' })
      end

      it "goes through the creation flow for a new comment" do
        Channels::Converter::ConversionJob.expects(:enqueue).never
        assert_difference('EventDecoration.count(:all)', 1) do
          assert_difference('::Channels::Resource.count(:all)', 1) do
            @worker.perform_conversion(@max_conversion_milliseconds)
            event_decoration = EventDecoration.where(event_id: @comment.id).first
            channels_resource = Channels::Resource.where(thread_id: @thread_id, external_id: @payload['id']).first

            facebook_assertions(@source, event_decoration, channels_resource, @payload, 'facebook_post')
          end
        end
      end
    end

    describe 'facebook message' do
      let(:resource_type) { 'facebook_message' }

      before do
        payload_text = read_test_file('facebook_message_with_attachments_and_shares.json')
        conversation_entities = read_test_file('facebook_message_conversation_entities.json')
        send_api_response = read_test_file('facebook_message_send_api_response.json')

        @send_api_payload = JSON.parse(send_api_response)
        @payload = JSON.parse(payload_text)
        @worker = new_conversion_worker(@source, Channels::Constants::FACEBOOK_MESSAGE)

        # fetch_conversation_entities
        stub_request(:get, "https://graph.facebook.com/v3.2/messages?access_token=#{@source.access_token}&appsecret_proof=#{facebook_appsecret_proof(@source.access_token)}&fields=from").
          with(headers: { 'User-Agent' => 'Faraday v0.11.0'}).
          to_return(status: 200, body: conversation_entities, headers: {})

        # create_private_message - production
        stub_request(:post, "https://graph.facebook.com/v3.2/33452/messages").
          with(
            body: {
              recipient: { id: "100965388116474" },
              message: { text: "test text" },
              messaging_type: "MESSAGE_TAG",
              tag: "HUMAN_AGENT",
              access_token: "231d2A",
              appsecret_proof: "97e5c282369a3952c8241e43a13217832e32e54109bdd5f751dfd0dae541abf3"
            },
            headers: {
              'Content-Type' => 'application/json',
              'User-Agent' => 'Faraday v0.11.0'
            }
          ).
          to_return(status: 200, body: send_api_response, headers: {})

        # create_private_message - staging
        stub_request(:post, "https://graph.facebook.com/v3.2/33452/messages").
          with(
            body: {
              recipient: { id: "100965388116474" },
              message: { text: "test text" },
              messaging_type: "RESPONSE",
              access_token: "231d2A",
              appsecret_proof: "97e5c282369a3952c8241e43a13217832e32e54109bdd5f751dfd0dae541abf3"
            },
            headers: {
              'Content-Type' => 'application/json',
              'User-Agent' => 'Faraday v0.11.0'
            }
          ).
          to_return(status: 200, body: send_api_response, headers: {})

        stub_request(:get, "https://graph.facebook.com/v3.2/#{@send_api_payload['message_id']}?access_token=#{@source.access_token}&appsecret_proof=#{facebook_appsecret_proof(@source.access_token)}").
          with(headers: {'User-Agent' => 'Faraday v0.11.0'}).
          to_return(status: 200, body: payload_text, headers: { 'Content-Type' => 'application/json' })
      end

      it "goes through the creation flow for a new comment" do
        Channels::Converter::ConversionJob.expects(:enqueue).never
        assert_difference('EventDecoration.count(:all)', 1) do
          assert_difference('::Channels::Resource.count(:all)', 1) do
            @worker.perform_conversion(@max_conversion_milliseconds)
            event_decoration = EventDecoration.where(event_id: @comment.id).first
            channels_resource = Channels::Resource.where(thread_id: @thread_id, external_id: @send_api_payload['message_id']).first

            assert_attributes_from_hash(@send_api_payload,
              [
                # [path in payload, actual value,                       label]
                ['recipient_id',   event_decoration.data.source.id,    'ed.source.id'],
                ['message_id',     event_decoration.data.external_id,  'ed.external_id'],
                ['message_id',     channels_resource.external_id,      'cr.external_id']
              ])

            assert_equal resource_type,               event_decoration.data.resource_type,      'ed.resource_type'
            assert_equal @source.graph_object_id,     event_decoration.data.source.id,          'ed.source.id'
            assert_equal @source.id,                  event_decoration.data.source.zendesk_id,  'ed.source.zendesk_id'
            assert_equal @source.name,                event_decoration.data.source.name,        'ed.source.name'
            assert_equal resource_type,               channels_resource.resource_type,          'cr.resource_type'
            assert_equal @thread_id,                  channels_resource.thread_id,              'cr.thread_id'
            empty_hash = {}
            assert_equal empty_hash, channels_resource.metadata, 'cr.metadata'
          end
        end
      end
    end
  end
end
