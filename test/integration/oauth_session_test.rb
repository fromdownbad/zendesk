require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

class Zendesk::OAuthSessionTest < ActionDispatch::IntegrationTest
  fixtures :all
  include ZendeskDSL

  before do
    @account = accounts(:minimum)
    @user = users(:minimum_agent)
    @admin = users(:minimum_admin)
    @client = FactoryBot.create(:client, account: @account, user: @user)
    @host = "https://#{@account.host_name}"

    host! @host
    https!
  end

  it "uses the oauth token" do
    token = FactoryBot.create(:token, user: @user, account: @account, client: @client)
    authorization = { "HTTP_AUTHORIZATION" => "Bearer #{token.token(:full)}" }
    logs_in_as(@admin, '123456')
    get "#{@host}/api/v2/users/me.json", headers: authorization
    assert_response :ok
    assert_equal JSON.parse(response.body)["user"]["id"], token.user_id
  end
end
