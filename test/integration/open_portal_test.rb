require_relative "../support/test_helper"
require_relative "../support/zendesk_dsl"

class OpenPortalTest < ActionDispatch::IntegrationTest
  include ZendeskDSL

  describe "the portal" do
    fixtures :all

    before do
      @account = accounts(:minimum)
      ZendeskSearch::Client.any_instance.stubs(:search).returns(count: 0, results: [])
    end

    describe "when public forums are disabled" do
      before do
        Account.any_instance.stubs(:has_public_forums?).returns(false)
      end

      it "redirects anonymous portal users to login" do
        self.host = @account.host_name
        goes_to_login "/portal"
      end

      it "renders closed page for anonymous users on other endpoints" do
        closed = "https://www.zendesk.com/app/help-center-closed/?utm_source=helpcenter-closed&utm_medium=poweredbyzendesk&utm_campaign=text&utm_content=www.example.com"

        get "/entries"
        assert_redirected_to closed

        get "/forums/#{forums(:solutions).id}/entries"
        assert_redirected_to closed

        get "/forums/#{forums(:solutions).id}/entries/#{entries(:ponies).id}"
        assert_redirected_to closed

        get "/forums/#{forums(:solutions).id}/entries/new"
        assert_redirected_to closed

        get "/search"
        assert_redirected_to closed

        get "/posts.rss"
        assert_response :not_found
      end
    end

    it "honors locale settings" do
      english   = translation_locales(:english_by_zendesk)
      brazilian = translation_locales(:brazilian_portuguese)

      # Not available to the account
      another_locale = translation_locales(:japanese)
      another_locale.account = accounts(:support)
      another_locale.localized_agent = false
      another_locale.official_translation = false
      another_locale.public = false
      another_locale.save!

      @account.update_attribute(:locale_id, brazilian.id)
      Account.any_instance.stubs(:has_public_forums?).returns(true)
      Account.any_instance.stubs(:available_languages).returns([english, brazilian])
      Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)

      self.host = @account.host_name
      # show default account language
      get '/home'
      assert_equal brazilian, I18n.previous_request_locale
      # force allowed locale with a parameter
      get '/home', params: { locale: english.id }
      assert_equal english, I18n.previous_request_locale
      # should stick with the forced locale from now on
      get '/home'
      assert_equal english, I18n.previous_request_locale
      # force not allowed locale with a parameter
      get '/home', params: { locale: another_locale.id }
      # should still show the previous locale
      assert_equal english, I18n.previous_request_locale
      # force non existent locale with a parameter
      get '/home', params: { locale: 999 }
      # should still show the previous locale
      assert_equal english, I18n.previous_request_locale
      # pass a locale with different format
      get '/home', params: { locale: { hello: "world" } }
      # should still show the previous locale
      assert_equal english, I18n.previous_request_locale
      # user selects a different language
      put choose_locale_user_path(brazilian)
      assert_equal brazilian, I18n.previous_request_locale
      # should stick with the selected locale from now on
      get '/home'
      assert_equal brazilian, I18n.previous_request_locale
    end
  end
end
