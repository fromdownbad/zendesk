require_relative '../support/test_helper'

describe 'UserSecuritySettings' do
  fixtures :accounts, :users

  describe "User security settings" do
    before do
      @account       = accounts(:minimum)
      @role_settings = @account.role_settings
      @agent         = @user = users(:minimum_agent)
      @end_user      = users(:minimum_end_user)
    end

    describe "when fetching the user's security policy" do
      it "returns the correct policy" do
        assert_equal Zendesk::SecurityPolicy::Low.id, @agent.security_policy.id
        assert_equal Zendesk::SecurityPolicy::Low.id, @end_user.security_policy.id
      end

      describe "and the setting has since been changed" do
        before do
          @role_settings.update_attributes!(
            agent_security_policy_id: Zendesk::SecurityPolicy::High.id,
            end_user_security_policy_id: Zendesk::SecurityPolicy::Medium.id
          )
        end

        it "returns the correct policy" do
          assert_equal Zendesk::SecurityPolicy::High.id,   @agent.security_policy.id
          assert_equal Zendesk::SecurityPolicy::Medium.id, @end_user.security_policy.id
        end
      end
    end

    describe "when determining whether a user can use a strategy to log in" do
      describe "if the strategy has specific requirements" do
        before do
          @twitter_strategy = Zendesk::Auth::Warden::TwitterOauthStrategy.new({})
        end

        it "returns the correct answer" do
          assert_equal @agent.login_allowed?(:twitter), @twitter_strategy.allowed?(@agent)
        end
      end

      describe "if the strategy does not have specific requirements" do
        before do
          @unregistered_strategy = Zendesk::Auth::Warden::ChallengeTokenStrategy.new({})
        end

        it "returns true" do
          assert(@unregistered_strategy.allowed?(@agent))
        end
      end
    end

    describe "when determining whether a user can use a login service" do
      describe "and the settings have since been changed" do
        before do
          @role_settings.update_attributes!(
            agent_zendesk_login: false,
            agent_google_login: true,
            agent_twitter_login: true,
            agent_facebook_login: false,
            end_user_zendesk_login: true,
            end_user_google_login: false,
            end_user_twitter_login: false,
            end_user_facebook_login: true
          )
        end

        it "returns the correct answer" do
          refute @agent.login_allowed?(:zendesk)
          assert @agent.login_allowed?(:twitter)
          refute @agent.login_allowed?(:facebook)
          assert @agent.login_allowed?(:google)

          assert @end_user.login_allowed?(:zendesk)
          refute @end_user.login_allowed?(:twitter)
          assert @end_user.login_allowed?(:facebook)
          refute @end_user.login_allowed?(:google)
        end
      end
    end
  end
end
