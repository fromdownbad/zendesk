require_relative "../support/test_helper"

class ArchiveV2IntegrationTest < ActionDispatch::IntegrationTest
  fixtures :all

  def create_and_close_ticket(user)
    ticket = user.account.tickets.new
    ticket.description = "hello i need help yes please thanks thanks!"
    ticket.requester = user
    ticket.add_comment(body: 'public comment', is_public: false)
    ticket.will_be_saved_by(user)

    retval = yield ticket if block_given?
    return ticket if retval == false

    ticket.save!

    ticket.reload
    ticket.status_id = StatusType.CLOSED
    ticket.will_be_saved_by(users(:minimum_admin))
    ticket.add_comment(body: 'private comment', is_public: false)

    ticket.save!

    ticket
  end

  before do
    @account = accounts(:minimum)
    @account.settings.customer_satisfaction = true
    @account.is_ssl_enabled = true
    @account.save!
    @host = @account.url(ssl: true)
    login users(:minimum_admin)

    @ticket_with_satisfaction = tickets(:minimum_1)
    @ticket_with_satisfaction.satisfaction_score = SatisfactionType.GOOD
    @ticket_with_satisfaction.satisfaction_comment = "mostly good."
    @ticket_with_satisfaction.will_be_saved_by(@ticket_with_satisfaction.requester)
    @ticket_with_satisfaction.save!

    @ticket_with_satisfaction.status_id = StatusType.CLOSED
    @ticket_with_satisfaction.will_be_saved_by User.find(-1)
    @ticket_with_satisfaction.save!

    @ticket_with_satisfaction.reload

    @ticket = tickets(:minimum_5)

    @linked_incident = tickets(:minimum_4)
    @linked_incident.ticket_type_id = TicketType.PROBLEM
    @linked_incident.status_id = StatusType.CLOSED
    @linked_incident.will_be_saved_by User.find(-1)
    @linked_incident.linked_id = @ticket.id
    @linked_incident.save!

    @linked_incident.reload

    @end_user_ticket = create_and_close_ticket(users(:minimum_end_user))
    @collaborated_ticket = create_and_close_ticket(users(:minimum_admin)) do |ticket|
      ticket.set_collaborators = [users(:minimum_end_user).id]
    end

    Ticket::Bookmark.new.tap do |mark|
      mark.ticket = @ticket_with_satisfaction
      mark.user = users(:minimum_admin)
    end.save!

    @to_archive = [@ticket, @linked_incident, @ticket_with_satisfaction, @end_user_ticket, @collaborated_ticket]
  end

  def should_respond_identically(url, method = :get, options = {})
    srand(1)

    send(method, url, options)
    assert_response :ok

    unarchived = response_body

    @to_archive.each { |t| t.reload; archive_and_delete(t) }
    Rails.cache.clear

    send(method, url, options)
    assert_response :ok

    archived = response_body

    if response.content_type == "application/json"
      archived, unarchived = [archived, unarchived].map { |b| JSON.parse(b) }
    end
    assert_equal unarchived, archived
  end

  def url_replacements(ticket)
    {
      nice_id: ticket.nice_id,
      requester_id: ticket.requester_id,
      audit_id: ticket.audits.first.id,
      organization_id: ticket.organization_id
    }
  end

  describe "tickets/show" do
    before do
      Zendesk::UserPortalState.any_instance.stubs(:show_anonymous_modify_csat_link?).returns(true)
    end

    # we have things like recent ticket counts, organization counts, etc that
    # are not identical -- so eventually, the same
    should_eventually "be identical whether archived or not" do
      should_respond_identically("#{@host}/tickets/#{@ticket.nice_id}")
    end

    it "renders an archived ticket" do
      @ticket.archive!
      get "#{@host}/tickets/#{@ticket.nice_id}"
      assert_redirected_to "/agent/tickets/#{@ticket.nice_id}"
    end

    it "shows satisfaction on an archived ticket" do
      @ticket_with_satisfaction.archive!
      get "#{@host}/tickets/#{@ticket_with_satisfaction.nice_id}"
      assert_redirected_to "/agent/tickets/#{@ticket_with_satisfaction.nice_id}"
    end
  end

  urls = %w[
    /api/v2/tickets/%{nice_id}.json
    /api/v2/tickets/%{nice_id}/collaborators.json
    /api/v2/tickets/%{nice_id}/incidents.json
    /api/v2/tickets/%{nice_id}/audits.json
    /api/v2/tickets/%{nice_id}/comments.json
    /api/v2/tickets/%{nice_id}/audits/%{audit_id}.json
    /api/v2/users/%{requester_id}/tickets/requested.json
    /api/v2/users/%{requester_id}/tickets/requested.json?include=satisfaction_ratings
    /api/v2/users/%{requester_id}/tickets/ccd.json
    /api/v2/users/%{requester_id}/related.json
    /api/v2/bookmarks.json
  ]
  urls.each do |url|
    describe url do
      before do
        stub_occam_find([tickets(:minimum_1).id])
      end

      it "is identical whether archived or no" do
        u = @host + url
        should_respond_identically(u % url_replacements(@ticket))
      end
    end

    describe url + " with satisfaction" do
      it "is identical for cust-sat whether archived or no" do
        u = @host + url
        should_respond_identically(u % url_replacements(@ticket_with_satisfaction))
      end
    end
  end

  describe "comments.json" do
    it "works with merge ticket results" do
      merge_ticket_1 = create_and_close_ticket(users(:minimum_end_user)) do |ticket_1|
        ticket_1.save!

        create_and_close_ticket(users(:minimum_end_user)) do |ticket_2|
          ticket_2.save!
          ticket_2.reload

          Zendesk::Tickets::Merger.new(user: users(:minimum_admin)).merge(
            target: ticket_1,
            sources: [ticket_2],
            target_comment: "winner",
            source_comment: "losers",
            target_is_public: true,
            source_is_public: true
          )
          false
        end
      end

      archive_and_delete(merge_ticket_1)
      merge_ticket_1 = merge_ticket_1.account.tickets.find_by_nice_id(merge_ticket_1.nice_id)

      u = @host + '/api/v2/tickets/%{nice_id}/comments.json'
      should_respond_identically(u % url_replacements(merge_ticket_1))
    end
  end

  describe "with a ticket with metric sets" do
    it "is identical for include?metrics" do
      u = @host + "/api/v2/tickets/%{nice_id}.json?include=metrics"
      should_respond_identically(u % url_replacements(@ticket_with_satisfaction))
    end
  end

  describe "with a voice comment" do
    it "has identical comments.json" do
      ticket = create_and_close_ticket(users(:minimum_admin)) do |t|
        t.save!
        t.reload

        call_stub = stub(voice_comment_data: {}, account_id: accounts(:minimum).id)
        t.add_voice_comment(call_stub,
          author_id: users(:minimum_admin).id,
          body: "Call recording")
        t.will_be_saved_by(t.requester)
        t.save!
        t
      end

      @to_archive << ticket
      u = @host + "/api/v2/tickets/%{nice_id}/comments.json"
      should_respond_identically(u % url_replacements(ticket))
    end
  end

  describe "a follow up to an archived ticket" do
    before do
      @followup = create_and_close_ticket(users(:minimum_end_user)) do |ticket|
        ticket.via_followup_source_id = @end_user_ticket.nice_id
        ticket.set_followup_source(users(:minimum_end_user))
      end
      @followup
    end

    it "has an ok /api/v2beta/related page" do
      archive_and_delete(@end_user_ticket)
      get(@host + "/api/v2beta/tickets/#{@followup.nice_id}/related.json")

      assert_response :ok
      assert_equal [@end_user_ticket.nice_id], JSON.parse(response.body)['followupSources']
    end
  end

  end_user_urls = %w[
    /organizations/%{organization_id}/requests?filter=solved
    /api/v2/requests.json
    /api/v2/requests.json?status=closed
    /api/v2/requests/%{nice_id}/comments.json
    /api/v2/requests/solved.json
    /api/v2/requests/ccd.json
    /api/v2/users/%{requester_id}/requests.json
    /api/v2/organizations/%{organization_id}/requests.json
  ]

  end_user_urls.each do |url|
    describe "as the end user" do
      before do
        logout
        o = users(:minimum_end_user).organization
        o.is_shared = true
        o.save!
        login users(:minimum_end_user)

        stub_occam_find([])
      end

      describe url do
        it "is identical whether archived or no" do
          u = @host + url
          should_respond_identically(u % url_replacements(@ticket))
        end
      end
    end
  end

  describe "view previewing" do
    before do
      Zendesk::Rules::OccamClient.
        any_instance.
        expects(:find).
        returns('ids' => Array(tickets(:minimum_2).id)).twice

      @url = @host + "/api/v2/views/preview"
      @payload = { view:         {
        all: [{field: "requester", operator: "is", value: users(:minimum_end_user).id}],
        output:          {
           columns: ["status", "id", "subject", "requester", "created", "updated", "group", "assignee"],
           group_by: "status", group_order: "asc", sort_by: "updated", sort_order: "desc"
         }
        },
        _include_archive: true }
    end

    mutate_view_params = [
      {sort_by: "updated", sort_order: "desc"},
      {sort_by: "updated", sort_order: "asc"},
      {sort_by: "created", sort_order: "desc"},
      {sort_by: "created", sort_order: "asc"},
      {sort_by: "status", sort_order: "desc"},
      {sort_by: "status", sort_order: "asc"},
      {sort_by: "requester", sort_order: "asc"},
      {sort_by: "requester", sort_order: "desc"},
      {sort_by: "group", sort_order: "asc"},
      {sort_by: "group", sort_order: "desc"},
      {sort_by: "assignee", sort_order: "asc"},
      {sort_by: "assignee", sort_order: "desc"}
    ]

    mutate_view_params.each do |param|
      describe "with #{param.inspect}" do
        it "responds the same" do
          @payload[:view][:output].merge!(param)
          should_respond_identically(@url, :post, @payload)
        end
      end
    end

    describe "with a small per_page (triggering a view count)" do
      it "responds the same" do
        @payload[:view][:output].merge!(sort_by: "updated", sort_order: "desc")
        @payload[:per_page] = 3
        should_respond_identically(@url, :post, @payload)
      end
    end
  end

  describe "preloading bug (double loaded audits)" do
    it "does not crash when both an archived and non-archived ticket is preloaded" do
      @end_user_ticket.archive! do
        @end_user_ticket.update_columns(status_id: StatusType.ARCHIVED)
      end
      tickets(:minimum_3).destroy

      get("#{@host}/api/v2/users/#{@end_user_ticket.requester_id}/tickets/requested.json?include=last_audits")
    end
  end

  describe "search" do
    before do
      results = {
        count: @to_archive.size,
        results: @to_archive.map { |t| {'id' => t.id.to_s, 'type' => 'ticket'} }
      }
      ZendeskSearch::Client.any_instance.stubs(:search).returns(results).at_least_once
    end

    describe "api v2" do
      before do
        @to_archive.each { |t| archive_and_delete(t) }
      end

      it "is ok" do
        get "#{@host}/api/v2/search.json?query=archived"
        assert_response :ok
      end
    end

    describe "incremental (lotus end-point) search" do
      before do
        @to_archive.each { |t| archive_and_delete(t) }
      end

      around do |test|
        with_in_memory_archive_router(test)
      end

      it "is ok" do
        url = "#{@host}/api/v2/search/incremental.json?query=archived"

        get url
        assert_response :ok
      end

      it "uses the :stub_only rows" do
        ZendeskArchive.router.expects(:get).never
        ZendeskArchive.router.expects(:get_multi).never

        url = "#{@host}/api/v2/search/incremental.json?query=archived"

        get url
        assert_response :ok
      end
    end
  end
end
