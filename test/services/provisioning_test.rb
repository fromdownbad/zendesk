require_relative '../support/test_helper'

SingleCov.covered! uncovered: 1

describe Provisioning do
  let(:described_class) { Provisioning }

  describe '.call' do
    subject do
      described_class.call(
        master_account_id,
        payload: payload,
        dry_run: dry_run
      )
    end

    let(:account)           { stub }
    let(:master_account_id) { account.id }
    let(:dry_run)           { stub }
    let(:journal)           { stub }

    let(:expected_options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end
    let(:payload) do
      {
        account: {
          subscriptions: [
            {
              type:   'current',
              status: 'active',
              products: [
                { name: 'support' },
                { name: 'chat' },
                { name: 'talk' },
                { name: 'talk_partner' }
              ],
              addons: [],
              prepaid_credits: []
            }
          ]
        }
      }
    end

    before do
      ZendeskBillingCore::Journal.stubs(:new).
        returns(journal)
      journal.stubs(:add).returns(stub)
    end

    describe 'when support product is not yet inflated' do
      let(:account) { accounts(:shell_account_without_support) }

      it 'inflates the support product' do
        described_class::SupportProduct::Inflator.expects(:call).
          with(expected_options)
        subject
      end
    end

    describe 'when support product is already inflated' do
      let(:account) { accounts(:minimum) }

      it 'synchronizes account, support, and all other products' do
        described_class::BeforeActions.expects(:call).
          with(expected_options)
        described_class::SupportProduct.expects(:call).
          with(expected_options)
        described_class::ChatProduct.expects(:call).
          with(expected_options)
        described_class::TalkProduct.expects(:call).
          with(expected_options)
        described_class::TalkPartnerProduct.expects(:call).
          with(expected_options)
        described_class::MultiBrandState.expects(:call).
          with(expected_options)
        described_class::AddonsState.expects(:call).
          with(expected_options)
        described_class::FeaturesState.expects(:call).
          with(expected_options)
        described_class::AccountState.expects(:call).
          with(expected_options)
        described_class::AfterActions.expects(:call).
          with(expected_options)
        subject
      end
    end
  end
end
