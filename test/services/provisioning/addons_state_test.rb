require_relative '../../support/test_helper'

SingleCov.covered!

describe Provisioning::AddonsState do
  let(:described_class) { Provisioning::AddonsState }

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account) { accounts(:minimum) }
    let(:payload) { stub }
    let(:dry_run) { stub }
    let(:journal) { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    describe 'when there is a support product' do
      let(:payload) do
        {
          account: {
            subscriptions: [
              {
                type:     'current',
                status:   'active',
                products: [
                  { name: 'support' }
                ]
              }
            ]
          }
        }
      end

      let(:support_subscription) do
        account.subscription
      end

      describe 'and there is no support subscription record' do
        before do
          account.stubs(:subscription).returns(nil)
          refute support_subscription.present?
        end

        it 'does not synchronize the addon states' do
          described_class::Synchronizer.expects(:call).never
          subject
        end
      end

      describe 'and there is a support subscription record' do
        before do
          assert support_subscription.present?
        end

        it 'synchronizes the addon states' do
          described_class::Synchronizer.expects(:call).
            with(options)
          subject
        end
      end
    end

    describe 'when there is no support product' do
      let(:payload) do
        {
          account: {
            subscriptions: [
              {
                type:     'current',
                status:   'active',
                products: [
                  { name: 'chat' }
                ]
              }
            ]
          }
        }
      end

      it 'does not synchronize the addon states' do
        described_class::Synchronizer.expects(:call).never
        subject
      end
    end
  end
end
