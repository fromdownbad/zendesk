require_relative '../../support/test_helper'

SingleCov.covered!

describe Provisioning::JournalComparator do
  let(:described_class) { Provisioning::JournalComparator }

  describe '.call' do
    subject do
      described_class.call(payload)
    end

    let(:payload) do
      {
        account: {
          master_account_id: master_account_id,
          billing_id:        billing_id
        }
      }
    end

    let(:account)           { accounts(:minimum) }
    let(:master_account_id) { account.id }
    let(:billing_id)        { stub }

    describe_with_arturo_disabled :billing_journal_comparator do
      let(:expected) do
        OpenStruct.new(
          matched?: true,
          details:  [:diff_skipped]
        )
      end

      it { assert_equal expected, subject }
    end

    describe_with_arturo_enabled :billing_journal_comparator do
      let(:zbc_journal) do
        ZBC::Journal.new(zbc_journal_data)
      end

      let(:classic_journal) do
        ZBC::Journal.new(classic_journal_data)
      end

      let(:zbc_journal_data)     { stub }
      let(:classic_journal_data) { stub }

      let(:synchronizer) do
        stub(synchronize!: zbc_journal)
      end

      before do
        ZBC::Zuora::Synchronizer.stubs(:new).
          with(billing_id, dry_run: true).
          returns(synchronizer)
        Provisioning.stubs(:call).with(
          master_account_id,
          payload: payload,
          dry_run: true
        ).returns(classic_journal)
      end

      describe "when zbc and classic journals match" do
        let(:zbc_journal_data)     { { foo: 'bar' } }
        let(:classic_journal_data) { { foo: 'bar' } }

        let(:expected) do
          OpenStruct.new(
            matched?: true,
            details:  [
              :diff_checked,
              [{ change: '=', content: [[:foo, 'bar']] }]
            ],
            zbc_journal:     zbc_journal_data,
            classic_journal: classic_journal_data
          )
        end

        it { assert_equal expected, subject }
      end

      describe "when zbc and classic journals do not match" do
        let(:zbc_journal_data)     { { foo: 'bar' } }
        let(:classic_journal_data) { { foo: 'zoo' } }

        let(:expected) do
          OpenStruct.new(
            matched?: false,
            details:  [
              :diff_checked,
              [
                { change: '-', content: [[:foo, 'bar']] },
                { change: '+', content: [[:foo, 'zoo']] }
              ]
            ],
            zbc_journal:     zbc_journal_data,
            classic_journal: classic_journal_data
          )
        end

        it { assert_equal expected, subject }
      end
    end
  end
end
