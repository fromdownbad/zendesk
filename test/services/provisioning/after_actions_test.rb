require_relative '../../support/test_helper'

SingleCov.covered!

describe Provisioning::AfterActions do
  let(:described_class) { Provisioning::AfterActions }

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account) { accounts(:minimum) }
    let(:journal) { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    let(:payload) do
      {
        account: {
          subscriptions: [
            {
              type:     'current',
              status:   'active',
              products: products
            }
          ]
        }
      }
    end

    let(:dry_run) { false }

    describe 'when there is a support product' do
      let(:products) do
        [
          {
            name: 'support',
            is_suite_product: is_suite_product
          },
        ]
      end

      let(:is_suite_product) { stub }

      describe 'when is_suite_product is true' do
        let(:is_suite_product) { true }

        describe_with_arturo_enabled :ocp_chat_only_agent_deprecation do
          it 'does not destroy the chat-agent permission set' do
            PermissionSet.expects(:destroy_chat_agent!).never
            subject
          end
        end

        describe_with_arturo_disabled :ocp_chat_only_agent_deprecation do
          it 'destroys the chat-agent permission set' do
            PermissionSet.expects(:destroy_chat_agent!).
              with(account)
            subject
          end
        end
      end

      describe 'when is_suite_product is false' do
        let(:is_suite_product) { false }

        it 'does not destroy the chat-agent permission set' do
          PermissionSet.expects(:destroy_chat_agent!).never
          subject
        end
      end
    end

    describe 'when there is no support product' do
      let(:products) do
        [
          { name: 'chat' }
        ]
      end

      before do
        assert_blank account.settings.find_by(name: 'suite_subscription')
      end

      it 'does not set the account settings for :suite_subscription' do
        subject
        assert_blank account.settings.find_by(name: 'suite_subscription')
      end
    end
  end
end
