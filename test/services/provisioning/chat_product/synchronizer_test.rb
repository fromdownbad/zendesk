require_relative '../../../support/test_helper'

SingleCov.covered!

describe Provisioning::ChatProduct::Synchronizer do
  let(:described_class) { Provisioning::ChatProduct::Synchronizer }

  fixtures :accounts,
    :zopim_subscriptions

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account)           { accounts(:with_trial_zopim_subscription) }
    let(:chat_subscription) { account.zopim_subscription }
    let(:dry_run)           { false }
    let(:journal)           { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    let(:zopim_reseller_client) do
      stub(update_account!: stub)
    end

    before do
      # NOTE: Timecop looses a millisecond and will cause the assertion related
      # time values to fail-- this is why we are freezing a specific time.
      #
      # REF: https://gist.github.com/weidenfreak/3987288
      Timecop.freeze('30 Apr 2019 00:00:00')
      Zopim::Reseller.stubs(:client).
        returns(zopim_reseller_client)
      account.owner.stubs(:zopim_email).
        returns('foo@example.com')
      journal.stubs(:add).
        returns(stub)
    end

    after do
      Timecop.return
    end

    describe 'when there is a chat product' do
      let(:payload) do
        {
          account: {
            subscriptions: [
              {
                type: 'current',
                status: 'active',
                billing_cycle_id: 1,
                products: [
                  {
                    name: 'chat',
                    plan: {
                      id:   2,
                      name: 'basic'
                    },
                    quantity: 5,
                    seasonal_quantity: 3
                  }
                ]
              }
            ]
          }
        }
      end

      let(:actual_attributes) do
        chat_subscription.reload.
          attributes.
          symbolize_keys.
          slice(*expected_attributes.keys)
      end

      describe 'and is already purchased' do
        let(:account) { accounts(:with_paid_zopim_subscription) }

        let(:expected_attributes) do
          {
            zopim_account_id: 43,
            billing_cycle_type: 1,
            plan_type: 'basic',
            max_agents: 8,
            status: 'active'
          }
        end

        before do
          subject
        end

        it { assert_equal expected_attributes, actual_attributes }
        it { assert_not_equal Time.now, chat_subscription.purchased_at }
      end

      describe 'and is not yet purchased' do
        let(:account) { accounts(:with_trial_zopim_subscription) }

        let(:expected_attributes) do
          {
            zopim_account_id: 42,
            billing_cycle_type: 1,
            plan_type: 'basic',
            max_agents: 8,
            status: 'active',
            owner_email: 'foo@example.com',
            purchased_at: Time.now
          }
        end

        before do
          subject
        end

        it { assert_equal expected_attributes, actual_attributes }
        it { assert_equal Time.now, chat_subscription.purchased_at }
      end
    end

    describe 'when there is no chat product' do
      let(:payload) do
        {
          account: {
            subscriptions: [
              {
                type: 'current',
                status: 'active',
                products: [
                  { name: 'support' }
                ]
              }
            ]
          }
        }
      end

      it 'cancels the chat subscription record' do
        chat_subscription.expects(:cancel!).once
        subject
      end
    end
  end
end
