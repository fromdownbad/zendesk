require_relative '../../support/test_helper'

SingleCov.covered!

describe Provisioning::SupportProduct do
  let(:described_class) { Provisioning::SupportProduct }

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account)            { accounts(:minimum) }
    let(:payload)            { stub }
    let(:dry_run)            { stub }
    let(:zuora_subscription) { stub('zuora_subscription') }
    let(:journal)            { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    describe 'when the account has synched before' do
      before do
        account.stubs(:zuora_subscription).
          returns(zuora_subscription)
      end

      it 'synchronizes the support product' do
        described_class::Synchronizer.expects(:call).
          with(options)
        subject
      end
    end

    describe 'when the account has not synched before' do
      let(:support_subscription) do
        account.subscription
      end

      before do
        account.stubs(:zuora_subscription).
          returns(nil)
      end

      describe 'and there is a support product is in the payload' do
        let(:payload) do
          {
            account: {
              subscriptions: [
                {
                  type:   'current',
                  status: 'active',
                  products: [
                    { name: 'support' }
                  ]
                }
              ]
            }
          }
        end

        describe 'and the support product is still in trial' do
          before do
            support_subscription.stubs(:is_trial?).
              returns(true)
          end

          it 'synchronizes the support product' do
            described_class::Synchronizer.expects(:call).
              with(options)
            subject
          end
        end

        describe 'and the support product is an expired trial' do
          before do
            support_subscription.stubs(
              is_trial?:             false,
              past_expiration_date?: true,
              zuora_managed?:        false
            )
          end

          it 'synchronizes the support product' do
            described_class::Synchronizer.expects(:call).
              with(options)
            subject
          end
        end
      end

      describe 'and there is no support product is not in the payload' do
        let(:payload) do
          {
            account: {
              subscriptions: [
                {
                  type:   'current',
                  status: 'active',
                  products: [
                    { name: 'chat' }
                  ]
                }
              ]
            }
          }
        end

        describe 'and the support product is still in trial' do
          before do
            support_subscription.stubs(:is_trial?).
              returns(true)
          end

          it 'does not synchronize the support product' do
            described_class::Synchronizer.expects(:call).never
            subject
          end
        end

        describe 'and the support product is an expired trial' do
          before do
            support_subscription.stubs(
              is_trial?:             false,
              past_expiration_date?: true,
              zuora_managed?:        false
            )
          end

          it 'does not synchronize the support product' do
            described_class::Synchronizer.expects(:call).never
            subject
          end
        end
      end
    end
  end
end
