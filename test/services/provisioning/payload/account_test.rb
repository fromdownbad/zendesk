require_relative '../../../support/test_helper'

SingleCov.covered!

describe Provisioning::Payload::Account do
  let(:described_class) { Provisioning::Payload::Account }

  let(:instance) do
    described_class.new(content)
  end

  let(:content) do
    {
      master_account_id:      stub,
      dunning:                { state: 'OK' },
      talk_recharge_settings: { enabled: false },
      subscriptions:          subscriptions_section
    }
  end

  let(:subscriptions_section) do
    [
      { type: 'current' },
      { type: 'future' }
    ]
  end

  describe '#dunning' do
    subject { instance.dunning }

    let(:expected_type) { Provisioning::Payload::Dunning }

    it { assert subject.instance_of?(expected_type) }
  end

  describe '#talk_recharge_settings' do
    subject { instance.talk_recharge_settings }

    let(:expected_type) { Provisioning::Payload::TalkRechargeSettings }

    it { assert subject.instance_of?(expected_type) }
  end

  describe '#subscriptions' do
    subject { instance.subscriptions }

    let(:expected_type) { Provisioning::Payload::Subscription }

    it { assert_equal 2, subject.count }
    it { assert subject.all? { |item| item.instance_of?(expected_type) } }

    describe 'when subscriptions-section is nil' do
      let(:subscriptions_section) { nil }

      it { assert_empty subject }
      it { assert subject.instance_of?(Array) }
    end
  end

  describe 'current_subscription' do
    subject { instance.current_subscription }

    let(:expected_type) { Provisioning::Payload::Subscription }

    it { assert subject.instance_of?(expected_type) }
    it { assert_equal 'current', subject.type }
  end

  describe 'future_subscription' do
    subject { instance.future_subscription }

    let(:expected_type) { Provisioning::Payload::Subscription }

    it { assert subject.instance_of?(expected_type) }
    it { assert_equal 'future', subject.type }

    describe 'when there is no future-subscription' do
      let(:subscriptions_section) do
        [
          { type: 'current' }
        ]
      end

      it { assert_nil subject }
    end
  end
end
