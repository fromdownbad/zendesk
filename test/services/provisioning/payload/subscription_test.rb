require_relative '../../../support/test_helper'

SingleCov.covered!

describe Provisioning::Payload::Subscription do
  let(:described_class) { Provisioning::Payload::Subscription }

  let(:instance) do
    described_class.new(content)
  end

  let(:content) do
    {
      type:            stub,
      status:          status,
      products:        products_section,
      addons:          addons_section,
      prepaid_credits: prepaid_credits_section
    }
  end

  let(:status) { 'active' }

  let(:products_section) do
    [
      { name: 'support' }
    ]
  end

  let(:addons_section) do
    [
      { name: 'light_agents' }
    ]
  end

  let(:prepaid_credits_section) do
    [
      { name: 'talk_prepaid' }
    ]
  end

  describe '#products' do
    subject { instance.products }

    let(:expected_type) { Provisioning::Payload::Product }

    it { assert_equal 1, subject.count }
    it { assert subject.all? { |item| item.instance_of?(expected_type) } }

    describe 'when products-section is nil' do
      let(:products_section) { nil }

      it { assert_empty subject }
      it { assert subject.instance_of?(Array) }
    end
  end

  describe '#addons' do
    subject { instance.addons }

    let(:expected_type) { Provisioning::Payload::Addon }

    it { assert_equal 1, subject.count }
    it { assert subject.all? { |item| item.instance_of?(expected_type) } }

    describe 'when addons-section is nil' do
      let(:addons_section) { nil }

      it { assert_empty subject }
      it { assert subject.instance_of?(Array) }
    end
  end

  describe '#prepaid_credits' do
    subject { instance.prepaid_credits }

    let(:expected_type) { Provisioning::Payload::PrepaidCredit }

    it { assert_equal 1, subject.count }
    it { assert subject.all? { |item| item.instance_of?(expected_type) } }

    describe 'when prepaid-credits-section is nil' do
      let(:prepaid_credits_section) { nil }

      it { assert_empty subject }
      it { assert subject.instance_of?(Array) }
    end
  end

  describe '#active?' do
    subject { instance.active? }

    it { assert subject }

    describe 'when status is not "active"' do
      let(:status) { 'cancelled' }

      it { refute subject }
    end
  end

  describe 'generated methods for supported products' do
    let(:expected_products) do
      %i[
        chat
        support
        talk
        talk_partner
      ]
    end

    it { assert_equal expected_products, described_class::SUPPORTED_PRODUCTS }

    it 'has a convenience method for each' do
      described_class::SUPPORTED_PRODUCTS.all? do |name|
        assert instance.respond_to?(:"#{name}_product")
      end
    end
  end

  describe '#product' do
    subject { instance.product(product_name) }

    let(:product_name) { 'support' }

    let(:expected_type) { Provisioning::Payload::Product }

    it { assert_equal 'support', subject.name }
    it { assert subject.instance_of?(expected_type) }

    describe 'when product is not in the products-section' do
      let(:product_name) { 'talk' }

      it { assert_nil subject }
    end
  end
end
