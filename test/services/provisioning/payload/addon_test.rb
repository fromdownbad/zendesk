require_relative '../../../support/test_helper'

SingleCov.covered!

describe Provisioning::Payload::Addon do
  let(:described_class) { Provisioning::Payload::Addon }

  let(:instance) do
    described_class.new(content)
  end

  before do
    Timecop.freeze
  end

  after do
    Timecop.return
  end

  describe '#to_addon_rateplan_info' do
    subject { instance.to_addon_rateplan_info }

    describe 'when content is for any addon' do
      let(:content) do
        {
          name: 'light_agents',
          reference_id: '8a80830f693a824b016b4d9db8535812',
          quantity: 10,
          starts_at: Time.now,
          expires_at: 3.months.from_now
        }
      end

      let(:expected_value) do
        [
          'light_agents',
          { zuora_rate_plan_id: '8a80830f693a824b016b4d9db8535812' }
        ]
      end

      it { assert_equal expected_value, subject }
    end

    describe 'when content is for a temporary zendesk agent addon' do
      let(:content) do
        {
          name: 'temporary_zendesk_agents',
          reference_id: '8a8083ff693a8238016b4e727e7d1b3a',
          quantity: 50,
          starts_at: Time.now,
          expires_at: 3.months.from_now
        }
      end

      let(:expected_value) do
        [
          'temporary_zendesk_agents',
          {
            zuora_rate_plan_id: '8a8083ff693a8238016b4e727e7d1b3a',
            quantity: 50,
            starts_at: Time.now,
            expires_at: 3.months.from_now
          }
        ]
      end

      it { assert_equal expected_value, subject }
    end
  end
end
