require_relative '../../../support/test_helper'

SingleCov.covered!

describe Provisioning::TalkProduct::Synchronizer do
  let(:described_class) { Provisioning::TalkProduct::Synchronizer }

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account)           { accounts(:minimum) }
    let(:talk_subscription) { account.voice_subscription }
    let(:dry_run)           { false }
    let(:journal)           { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    before do
      Voice::CreateVoiceJob.stubs(:enqueue)
      Voice::UpdateVoiceJob.stubs(:enqueue)
      ZBC::Zuora::Jobs::AddVoiceJob.stubs(:enqueue)
      Zendesk::Voice::InternalApiClient.stubs(:new).
        with(account.subdomain).
        returns(stub)
    end

    describe 'when there is a talk product' do
      let(:payload) do
        {
          account: {
            subscriptions: [
              {
                type: 'current',
                products: [
                  {
                    name: 'talk',
                    plan: {
                      id:   7,
                      name: 'professional'
                    },
                    quantity: 5,
                    seasonal_quantity: 3
                  }
                ]
              }
            ]
          }
        }
      end

      let(:actual_attributes) do
        talk_subscription.reload.
          attributes.
          symbolize_keys.
          slice(*expected_attributes.keys)
      end

      let(:expected_attributes) do
        {
          plan_type: 7,
          max_agents: 8,
          suspended: false,
          is_prepaid: false
        }
      end

      it 'updates the subscription record' do
        subject
        assert_equal expected_attributes, actual_attributes
      end

      describe 'processing for talk sub-account' do
        before do
          assert account.is_active?
        end

        describe 'when account is serviceable' do
          before do
            assert account.is_serviceable?
          end

          describe 'when there is a talk sub-account' do
            let(:talk_sub_account) { stub }

            before do
              account.stubs(:voice_sub_account).
                returns(talk_sub_account)
            end

            describe 'and it is suspended' do
              let(:talk_sub_account) { stub(suspended?: true) }

              it 'activates the sub-account' do
                ZBC::Voice::Jobs::ActivateJob.
                  expects(:enqueue).
                  with(account.id)
                subject
              end
            end

            describe 'and it is not suspended' do
              let(:talk_sub_account) { stub(suspended?: false) }

              it 'does not activate the sub-account' do
                ZBC::Voice::Jobs::ActivateJob.
                  expects(:enqueue).
                  never
                subject
              end
            end
          end

          describe 'when there is no talk sub-account' do
            before do
              refute account.voice_sub_account.present?
            end

            it 'does not activate the sub-account' do
              ZBC::Voice::Jobs::ActivateJob.
                expects(:enqueue).
                never
              subject
            end
          end
        end

        describe 'when account is not serviceable' do
          before do
            account.stubs(:is_serviceable?).
              returns(false)
          end

          describe 'when there is a talk sub-account' do
            let(:talk_sub_account) { stub(suspended?: true) }

            before do
              account.stubs(:voice_sub_account).
                returns(talk_sub_account)
            end

            it 'does not activate the sub-account' do
              ZBC::Voice::Jobs::ActivateJob.
                expects(:enqueue).
                never
              subject
            end
          end
        end
      end

      describe 'processing for talk-usage' do
        describe 'when :billing_check_voice_usage_subscription_during_sync is enabled' do
          before do
            Arturo.enable_feature!(:billing_check_voice_usage_subscription_during_sync)
          end

          it 'enqueues the job to process talk-usage' do
            ZBC::Zuora::Jobs::AddVoiceJob.
              expects(:enqueue).
              with(account.id)
            subject
          end
        end

        describe 'when :billing_check_voice_usage_subscription_during_sync is disabled' do
          before do
            Arturo.disable_feature!(:billing_check_voice_usage_subscription_during_sync)
          end

          it 'does not enqueue the job to process talk-usage' do
            ZBC::Zuora::Jobs::AddVoiceJob.
              expects(:enqueue).
              never
            subject
          end
        end
      end
    end

    describe 'when there is no talk product' do
      let(:payload) do
        {
          account: {
            subscriptions: [
              {
                type: 'current',
                products: [
                  { name: 'support' }
                ]
              }
            ]
          }
        }
      end

      describe 'and there is a talk subscription' do
        before do
          account.create_voice_subscription(
            plan_type: 1,
            max_agents: 5,
            suspended: false,
            is_prepaid: false
          )
          refute account.voice_subscription.suspended?
          subject
        end

        it 'suspends the subscription' do
          assert account.voice_subscription.suspended?
        end
      end

      describe 'and there is no talk subscription' do
        before do
          assert account.voice_subscription.blank?
        end

        it 'the action to suspend is not performed' do
          Voice::Subscription.any_instance.
            expects(:suspend!).never
          subject
        end
      end
    end
  end
end
