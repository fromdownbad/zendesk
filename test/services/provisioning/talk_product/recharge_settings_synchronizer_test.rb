require_relative '../../../support/test_helper'

SingleCov.covered!

describe Provisioning::TalkProduct::RechargeSettingsSynchronizer do
  let(:described_class) do
    Provisioning::TalkProduct::RechargeSettingsSynchronizer
  end

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account)                { accounts(:minimum) }
    let(:talk_recharge_settings) { account.voice_recharge_settings }
    let(:dry_run)                { false }
    let(:journal)                { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    let(:payload) do
      {
        account: {
          talk_recharge_settings: {
            amount: talk_recharge_settings_amount,
            enabled: true
          }
        }
      }
    end

    describe_with_arturo_enabled :voice_recharge do
      let(:actual_attributes) do
        talk_recharge_settings.reload.
          attributes.
          symbolize_keys.
          slice(*expected_attributes.keys)
      end

      describe 'when recharge-settings amount is present' do
        let(:talk_recharge_settings_amount) { 10.0 }

        let(:expected_attributes) do
          {
            amount: 10.0,
            enabled: true
          }
        end

        describe 'when the recharge-settings record exists' do
          before do
            account.create_voice_recharge_settings!(
              amount:  500.0,
              enabled: false
            )
            subject
          end

          it { assert_equal expected_attributes, actual_attributes }
        end

        describe 'when there is no recharge-settings record' do
          before do
            refute account.voice_recharge_settings.present?
            subject
          end

          it { assert_equal expected_attributes, actual_attributes }
        end
      end

      describe 'when recharge-settings amount is not present' do
        let(:talk_recharge_settings_amount) { nil }

        it 'the talk-recharge settings record is not created/updated' do
          Voice::RechargeSettings.any_instance.
            expects(:save).
            never
          subject
        end
      end
    end

    describe_with_arturo_disabled :voice_recharge do
      let(:talk_recharge_settings_amount) { anything }

      it 'the talk-recharge settings record is not created/updated' do
        Voice::RechargeSettings.any_instance.
          expects(:save).
          never
        subject
      end
    end
  end
end
