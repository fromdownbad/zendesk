require_relative '../../../support/test_helper'

SingleCov.covered!

describe Provisioning::TalkProduct::PrepaidCreditsSynchronizer do
  let(:described_class) do
    Provisioning::TalkProduct::PrepaidCreditsSynchronizer
  end

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account)          { accounts(:minimum) }
    let(:talk_sub_account) { account.voice_sub_account }
    let(:dry_run)          { false }
    let(:journal)          { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    let(:payload) do
      {
        account: {
          subscriptions: [
            {
              type: 'current',
              prepaid_credits: [
                {
                  name:         'talk_prepaid',
                  reference_id: 'reference-id1',
                  quantity:     200
                },
                {
                  name:         'talk_prepaid',
                  reference_id: 'reference-id2',
                  quantity:     500
                }
              ]
            }
          ]
        }
      }
    end

    before do
      ZBC::Voice::Jobs::ProcessUsageJob.stubs(:enqueue)
      ZBC::Zuora::Jobs::VoiceRechargeJob.stubs(:enqueue)
      ZBC::Zuora::Jobs::VoiceUsageJob.stubs(:enqueue)
      ZBC::Zuora::Jobs::UpdateVoiceSubAccountJob.stubs(:enqueue)
    end

    describe 'when there is a talk sub-account' do
      let(:talk_sub_account) { stub }

      let(:actual_unprocessed_usage_records) do
        ZBC::Voice::UnprocessedUsage.
          where(zuora_reference_id: %w[reference-id1 reference-id2]).
          map do |record|
            record.attributes.
              slice(*synchronized_attributes).
              symbolize_keys
          end
      end

      let(:actual_processed_usage_records) do
        ZBC::Zuora::VoiceUsage.
          where(zuora_reference_id: %w[reference-id1 reference-id2]).
          map do |record|
            record.attributes.
              slice(*synchronized_attributes).
              symbolize_keys
          end
      end

      let(:synchronized_attributes) do
        %w[
          account_id
          units
          zuora_reference_id
          usage_type
        ]
      end

      before do
        account.stubs(:voice_sub_account).
          returns(talk_sub_account)
      end

      describe 'when :serialize_voice_usage is enabled' do
        before do
          Arturo.enable_feature!(:serialize_voice_usage)
          subject
        end

        let(:expected_records) do
          [
            {
              account_id:         account.id,
              units:              2000,
              zuora_reference_id: 'reference-id1',
              usage_type:         'credit'
            },
            {
              account_id:         account.id,
              units:              5000,
              zuora_reference_id: 'reference-id2',
              usage_type:         'credit'
            }
          ]
        end

        it { assert_empty actual_processed_usage_records }
        it { assert_equal expected_records, actual_unprocessed_usage_records }
      end

      describe 'when :serialize_voice_usage is disabled' do
        before do
          Arturo.disable_feature!(:serialize_voice_usage)
          subject
        end

        let(:expected_records) do
          [
            {
              account_id:         account.id,
              units:              2000,
              zuora_reference_id: 'reference-id1',
              usage_type:         'credit'
            },
            {
              account_id:         account.id,
              units:              5000,
              zuora_reference_id: 'reference-id2',
              usage_type:         'credit'
            }
          ]
        end

        it { assert_empty actual_unprocessed_usage_records }
        it { assert_equal expected_records, actual_processed_usage_records }
      end
    end
  end
end
