require_relative '../../support/test_helper'

SingleCov.covered!

describe Provisioning::AccountState do
  let(:described_class) { Provisioning::AccountState }

  describe '.call' do
    subject { described_class.call(options) }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    let(:account)           { accounts(:minimum) }
    let(:master_account_id) { stub('master_account_id') }
    let(:journal)           { ZBC::Journal.new }

    let(:payload) do
      {
        account: {
          dunning: {
            state:   dunning_state,
            days_in: stub,
            offset:  stub
          },
          subscriptions: [
            {
              type: 'current',
              status: current_subscription_status
            }
          ]
        }
      }
    end

    let(:dry_run)                     { false }
    let(:current_subscription_status) { stub }
    let(:dunning_state)               { stub }

    describe 'when account.multiproduct? is false' do
      before do
        account.stubs(:multiproduct?).
          returns(false)
      end

      it 'does not process cancellation or updates to the dunning states' do
        account.expects(:cancel!).never
        account.expects(:update_attributes!).never
        subject
      end
    end

    describe 'when account.multiproduct? is true' do
      before do
        account.stubs(:multiproduct?).
          returns(true)
      end

      describe 'when current subscription status is "cancelled"' do
        let(:current_subscription_status) { 'cancelled' }

        before do
          account.stubs(:cancel!)
        end

        it 'cancels the account' do
          account.expects(:cancel!)
          subject
        end
      end

      describe 'when current subscription status is "active"' do
        let(:current_subscription_status) { 'active' }

        before do
          account.stubs(:update_attributes!)
        end

        describe 'when dunning-state is "OK"' do
          let(:dunning_state) { 'OK' }

          it 'updates the dunning states' do
            account.expects(:update_attributes!).
              with(is_active: true, is_serviceable: true)
            subject
          end
        end

        describe 'when dunning-state is "LASTCHANCE"' do
          let(:dunning_state) { 'LASTCHANCE' }

          it 'updates the dunning states' do
            account.expects(:update_attributes!).
              with(is_active: true, is_serviceable: false)
            subject
          end
        end

        describe 'when dunning-state is "SUSPENDED"' do
          let(:dunning_state) { 'SUSPENDED' }

          it 'updates the dunning states' do
            account.expects(:update_attributes!).
              with(is_active: false, is_serviceable: false)
            subject
          end
        end
      end
    end
  end
end
