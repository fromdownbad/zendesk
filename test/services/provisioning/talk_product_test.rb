require_relative '../../support/test_helper'

SingleCov.covered!

describe Provisioning::TalkProduct do
  let(:described_class) { Provisioning::TalkProduct }

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account) { accounts(:minimum) }
    let(:payload) { stub }
    let(:dry_run) { stub }
    let(:journal) { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    it 'synchronizes the talk product records' do
      described_class::PrepaidCreditsSynchronizer.expects(:call).
        with(options)
      described_class::Synchronizer.expects(:call).
        with(options)
      described_class::RechargeSettingsSynchronizer.expects(:call).
        with(options)
      subject
    end
  end
end
