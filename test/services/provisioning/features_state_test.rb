require_relative '../../support/test_helper'

SingleCov.covered!

describe Provisioning::FeaturesState do
  let(:described_class) { Provisioning::FeaturesState }

  before do
    Timecop.freeze('30 Apr 2019 00:00:00')
  end

  after do
    Timecop.return
  end

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account) { accounts(:minimum) }

    let(:payload) do
      {
        account: {
          subscriptions: [
            {
              type:     'current',
              status:   'active',
              products: [
                {
                  name: 'support',
                  plan: { id: 1, name: 'professional' },
                  pricing_model_revision: 7
                }
              ],
              addons:   [
                {
                  name: 'high_volume_api',
                  reference_id: '8a8083ff693a8238016b4d9d93720fb6',
                  quantity: 10,
                  starts_at: Time.now,
                  expires_at: 3.months.from_now
                },
                {
                  name: 'temporary_zendesk_agents',
                  reference_id: '8a8083ff693a8238016b4e727e7d1b3a',
                  quantity: 50,
                  starts_at: Time.now,
                  expires_at: 3.months.from_now
                }
              ]
            }
          ]
        }
      }
    end

    let(:dry_run) { false }
    let(:journal) { ZBC::Journal.new }

    let(:subscription_feature_service) do
      stub(:execute!)
    end

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    let(:addons) do
      {
        'high_volume_api' => {
          zuora_rate_plan_id: '8a8083ff693a8238016b4d9d93720fb6',
          name:               'high_volume_api'
        },
        'temporary_zendesk_agents' => {
          zuora_rate_plan_id: '8a8083ff693a8238016b4e727e7d1b3a',
          quantity: 50,
          starts_at: Time.now,
          expires_at: 3.months.from_now,
          name: 'temporary_zendesk_agents'
        }
      }
    end

    before do
      account.stubs(:set_use_feature_framework_persistence)
      ::Zendesk::Features::SubscriptionFeatureService.stubs(:new).
        with(account, 7, 1, addons).
        returns(subscription_feature_service)
    end

    it "sets the feature framework persistence flag and service" do
      account.expects(:set_use_feature_framework_persistence).
        with(true)
      subscription_feature_service.expects(:execute!)
      subject
    end
  end
end
