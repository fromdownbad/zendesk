require_relative '../../../support/test_helper'

SingleCov.covered!

describe Provisioning::SupportProduct::Inflator do
  let(:described_class) { Provisioning::SupportProduct::Inflator }

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account)           { accounts(:minimum) }
    let(:master_account_id) { stub('master_account_id') }
    let(:dry_run)           { false }
    let(:journal)           { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    let(:payload) do
      {
        account: {
          subscriptions: [
            {
              type: 'current',
              status: 'active',
              products: [
                { name: 'support' }
              ]
            }
          ]
        }
      }
    end

    before do
      account.stubs(:start_support_trial)
    end

    describe 'when support product is not yet inflated' do
      before do
        account.stubs(:subscription).
          returns(nil)
      end

      it 'inflates the support product' do
        account.expects(:start_support_trial)
        subject
      end
    end

    describe 'when support product is already inflated' do
      let(:support_subscription) { stub('support_subscription') }

      before do
        account.stubs(:subscription).
          returns(support_subscription)
      end

      it 'does not inflate the support product' do
        account.expects(:start_support_trial).never
        subject
      end
    end
  end
end
