require_relative '../../../support/test_helper'

SingleCov.covered!

describe Provisioning::SupportProduct::Synchronizer do
  let(:described_class) { Provisioning::SupportProduct::Synchronizer }

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account)              { accounts(:minimum) }
    let(:support_subscription) { account.subscription }
    let(:dry_run)              { false }
    let(:journal)              { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    let(:payload) do
      {
        account: {
          billing_id: 'abcdefghij0123456789',
          currency_id: 1,
          sales_model: 'self-service',
          payment_method_type_id: 1,
          assisted_agent_add_enabled: false,
          dunning: {
            level_id: 1,
            state: 'OK',
            days_in: 0,
            offset: 0,
            enabled: true
          },
          subscriptions: [
            {
              type: 'current',
              status: 'active',
              billing_cycle_id: 1,
              term_start_date: 0.days.from_now,
              term_end_date: 1.month.from_now,
              products: [
                {
                  name: 'support',
                  is_elite: false,
                  plan: { id: 1, name: 'professional' },
                  pricing_model_revision: 7,
                  quantity: 5,
                  seasonal_quantity: 1
                }
              ]
            },
            {
              type: 'future',
              status: 'active',
              billing_cycle_id: 4,
              term_start_date: 1.month.from_now
            }
          ]
        }
      }
    end

    let(:zuora_subscription) do
      stub(
        'zuora_subscription',
        :bypass_approval=,
        :seasonal_quantity=,
        :account_id=,
        :subscription_id=
      )
    end

    before do
      # NOTE: Timecop looses a millisecond and will cause the assertion related
      # time values to fail-- this is why we are freezing a specific time.
      #
      # REF: https://gist.github.com/weidenfreak/3987288
      Timecop.freeze('30 Apr 2019 00:00:00')
      subject
    end

    after do
      Timecop.return
    end

    let(:expected_attributes) do
      {
        account_id: 90538,
        subscription_id: 90538,
        assisted_agent_add_enabled: false,
        billing_cycle_type: 1,
        currency_type: 1,
        dunning_level: 1,
        future_subscription_starts_at: 1.month.from_now,
        is_active: true,
        is_dunning_enabled: true,
        is_elite: false,
        max_agents: 6,
        plan_type: 1,
        pricing_model_revision: 7,
        sales_model: "self-service",
        term_end_date: 1.month.from_now,
        payment_method_type: 1,
        last_synchronized_at: Time.now.utc
      }
    end

    let(:actual_attributes) do
      account.reload.
        zuora_subscription.
        attributes.
        symbolize_keys.
        slice(*expected_attributes.keys)
    end

    it { assert_equal expected_attributes, actual_attributes }
  end
end
