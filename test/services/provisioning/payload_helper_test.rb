require_relative '../../support/test_helper'

SingleCov.covered!

describe Provisioning::PayloadHelper do
  let(:described_class) { Provisioning::PayloadHelper }

  let(:dummy_class) do
    Class.new do
      include Provisioning::PayloadHelper
    end
  end

  let(:dummy_instance) do
    dummy_class.new
  end

  # SEE: https://github.com/zendesk/billing/wiki/Classic-Provisioning-Payload
  let(:payload) { stub }

  before do
    dummy_instance.stubs(:payload).
      returns(payload)
  end

  describe 'methods acquired' do
    it { assert dummy_instance.respond_to? :_account }

    it { assert dummy_instance.respond_to? :_dunning }
    it { assert dummy_instance.respond_to? :_talk_recharge_settings }

    it { assert dummy_instance.respond_to? :_current_subscription }
    it { assert dummy_instance.respond_to? :_future_subscription }

    it { assert dummy_instance.respond_to? :_support_product }
    it { assert dummy_instance.respond_to? :_chat_product }
    it { assert dummy_instance.respond_to? :_talk_product }
    it { assert dummy_instance.respond_to? :_talk_partner_product }
  end

  describe '#_account' do
    subject { dummy_instance._account }

    let(:payload) do
      {
        account: {
          master_account_id: '1'
        }
      }
    end

    it { assert Provisioning::Payload::Account }
    it { assert subject.master_account_id == '1' }
  end

  describe '#_dunning' do
    subject { dummy_instance._dunning }

    let(:payload) do
      {
        account: {
          dunning: { state: 'OK' }
        }
      }
    end

    it { assert Provisioning::Payload::Dunning }
    it { assert subject.state == 'OK' }
  end

  describe 'subscription fetchers' do
    let(:payload) do
      {
        account: {
          subscriptions: subscriptions
        }
      }
    end

    describe '#_current_product' do
      subject { dummy_instance._current_subscription }

      describe 'when included in the payload' do
        let(:subscriptions) { [{ type: 'current' }] }

        it { assert Provisioning::Payload::Subscription }
        it { assert subject.type == 'current' }
      end

      describe 'when not included in the payload' do
        let(:products) { [] }

        it { assert subject.blank? }
      end
    end

    describe '#_future_product' do
      subject { dummy_instance._future_subscription }

      describe 'when included in the payload' do
        let(:subscriptions) { [{ type: 'future' }] }

        it { assert Provisioning::Payload::Subscription }
        it { assert subject.type == 'future' }
      end

      describe 'when not included in the payload' do
        let(:products) { [] }

        it { assert subject.blank? }
      end
    end
  end

  describe 'product fetchers' do
    let(:payload) do
      {
        account: {
          subscriptions: [
            {
              type: 'current',
              products: products
            }
          ]
        }
      }
    end

    describe '#_support_product' do
      subject { dummy_instance._support_product }

      describe 'when included in the payload' do
        let(:products) { [{ name: 'support' }] }

        it { assert Provisioning::Payload::Product }
        it { assert subject.name == 'support' }
      end

      describe 'when not included in the payload' do
        let(:products) { [] }

        it { assert subject.blank? }
      end
    end

    describe '#_chat_product' do
      subject { dummy_instance._chat_product }

      describe 'when included in the payload' do
        let(:products) { [{ name: 'chat' }] }

        it { assert Provisioning::Payload::Product }
        it { assert subject.name == 'chat' }
      end

      describe 'when not included in the payload' do
        let(:products) { [] }

        it { assert subject.blank? }
      end
    end
  end
end
