require_relative '../../support/test_helper'

SingleCov.covered!

describe Provisioning::ChatProduct do
  let(:described_class) { Provisioning::ChatProduct }

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account) { accounts(:minimum) }
    let(:payload) { stub }
    let(:dry_run) { stub }
    let(:journal) { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    describe 'when there is a chat product' do
      let(:payload) do
        {
          account: {
            subscriptions: [
              {
                type:   'current',
                status: 'active',
                products: [
                  { name: 'chat' }
                ]
              }
            ]
          }
        }
      end

      describe 'and there is no chat subscription record' do
        before do
          account.stubs(:zopim_subscription).
            returns(nil)
        end

        it 'does not synchronize the chat product' do
          described_class::Synchronizer.expects(:call).never
          subject
        end
      end

      describe 'and there is a chat subscription record' do
        before do
          account.stubs(:zopim_subscription).
            returns(stub)
        end

        describe 'and there is a chat integration' do
          let(:chat_integration) do
            stub(external_zopim_id: stub)
          end

          before do
            ZopimIntegration.stubs(:find_by_account_id).
              with(account.id).
              returns(chat_integration)
          end

          describe 'and there is no chat subscription record' do
            before do
              account.stubs(:zopim_subscription).
                returns(nil)
            end

            it 'does not synchronize the chat product' do
              described_class::Synchronizer.expects(:call).never
              subject
            end
          end

          describe 'and there is a chat subscription record' do
            before do
              account.stubs(:zopim_subscription).
                returns(stub)
            end

            it 'synchronizes the chat product' do
              described_class::Synchronizer.expects(:call).
                with(options)
              subject
            end
          end
        end
      end
    end

    describe 'when there is no chat product' do
      let(:payload) do
        {
          account: {
            subscriptions: [
              {
                type:   'current',
                status: 'active',
                products: [
                  { name: 'support' }
                ]
              }
            ]
          }
        }
      end

      describe 'and there is no chat subscription record' do
        before do
          account.stubs(:zopim_subscription).
            returns(nil)
        end

        it 'synchronizes the chat product' do
          described_class::Synchronizer.expects(:call).never
          subject
        end
      end

      describe 'and there is a chat subscription record' do
        let(:chat_subscription) do
          stub(is_trial?: is_trial, purchased_at: purchased_at)
        end

        let(:is_trial)     { false }
        let(:purchased_at) { Time.now }

        before do
          account.stubs(:zopim_subscription).
            returns(chat_subscription)
        end

        describe 'and it is a recorded as purchased' do
          it 'does not synchronize the chat product' do
            described_class::Synchronizer.expects(:call).
              with(options)
            subject
          end
        end

        describe 'and is still in trial' do
          let(:is_trial) { true }

          it 'does not synchronize the chat product' do
            described_class::Synchronizer.expects(:call).never
            subject
          end
        end

        describe 'and it has no purchased_at stamp' do
          let(:purchased_at) { nil }

          it 'does not synchronize the chat product' do
            described_class::Synchronizer.expects(:call).never
            subject
          end
        end
      end
    end
  end
end
