require_relative '../../../support/test_helper'

SingleCov.covered!

describe Provisioning::AddonsState::Synchronizer do
  let(:described_class) { Provisioning::AddonsState::Synchronizer }

  before do
    Timecop.freeze
  end

  after do
    Timecop.return
  end

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account) { accounts(:minimum) }
    let(:journal) { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    let(:support_subscription) do
      account.subscription
    end

    let(:payload) do
      {
        account: {
          subscriptions: [
            {
              type: 'current',
              addons: addons
            }
          ]
        }
      }
    end

    let(:dry_run)       { false }
    let(:addon_service) { stub(:execute!) }

    describe 'when there a temporary zendesk agent addon' do
      let(:addons) do
        [
          {
            name: 'light_agents',
            reference_id: '8a80830f693a824b016b4d9db8535812',
            quantity: 10,
            starts_at: Time.now,
            expires_at: 3.months.from_now
          },
          {
            name: 'temporary_zendesk_agents',
            reference_id: '8a8083ff693a8238016b4e727e7d1b3a',
            quantity: 50,
            starts_at: Time.now,
            expires_at: 3.months.from_now
          }
        ]
      end

      let(:expected_addons) do
        {
          'light_agents' => {
            zuora_rate_plan_id: '8a80830f693a824b016b4d9db8535812'
          },
          'temporary_zendesk_agents' => {
            zuora_rate_plan_id: '8a8083ff693a8238016b4e727e7d1b3a',
            quantity: 50,
            starts_at: Time.now,
            expires_at: 3.months.from_now
          }
        }
      end

      before do
        ::Zendesk::Features::AddonService.stubs(:new).
          with(account, expected_addons).
          returns(addon_service)
      end

      it 'updates addons service and support-subscription record' do
        addon_service.expects(:execute!)
        support_subscription.expects(:save!)
        subject
      end
    end

    describe 'when there is no temporary zendesk agents addon' do
      let(:addons) do
        [
          {
            name: 'light_agents',
            reference_id: '8a80830f693a824b016b4d9db8535812',
            quantity: 10,
            starts_at: Time.now,
            expires_at: 3.months.from_now
          }
        ]
      end

      let(:expected_addons) do
        {
          'light_agents' => {
            zuora_rate_plan_id: '8a80830f693a824b016b4d9db8535812'
          }
        }
      end

      before do
        ::Zendesk::Features::AddonService.stubs(:new).
          with(account, expected_addons).
          returns(addon_service)
      end

      it 'updates addons service and support-subscription record' do
        addon_service.expects(:execute!)
        support_subscription.expects(:save!).never
        subject
      end
    end
  end
end
