require_relative '../../../support/test_helper'

SingleCov.covered!

describe Provisioning::TalkPartnerProduct::Synchronizer do
  let(:described_class) { Provisioning::TalkPartnerProduct::Synchronizer }

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account)          { accounts(:minimum) }
    let(:tpe_subscription) { account.tpe_subscription }
    let(:dry_run)          { false }
    let(:journal)          { ZBC::Journal.new }

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    let(:voice_partner_edition_account) do
      stub(:updated_subscription!)
    end

    before do
      Voice::PartnerEditionAccount.
        stubs(:find_or_create_by_account).
        with(account).
        returns(voice_partner_edition_account)
    end

    describe 'when there is a talk-partner product' do
      let(:payload) do
        {
          account: {
            subscriptions: [
              {
                type: 'current',
                products: [
                  {
                    name: 'talk_partner',
                    plan: {
                      id:   1,
                      name: 'partner_edition'
                    },
                    quantity: 5,
                    seasonal_quantity: 3
                  }
                ]
              }
            ]
          }
        }
      end

      let(:actual_attributes) do
        tpe_subscription.reload.
          attributes.
          symbolize_keys.
          slice(*expected_attributes.keys)
      end

      let(:expected_attributes) do
        {
          active: true,
          plan_type: 1,
          max_agents: 8
        }
      end

      before do
        subject
      end

      it { assert_equal expected_attributes, actual_attributes }
    end

    describe 'when there is no talk-partner product' do
      let(:payload) do
        {
          account: {
            subscriptions: [
              {
                type: 'current',
                products: [
                  { name: 'support' }
                ]
              }
            ]
          }
        }
      end

      describe 'and there is a talk-partner subscription' do
        before do
          account.create_tpe_subscription(
            active:     true,
            plan_type:  1,
            max_agents: 5
          )
          assert account.tpe_subscription.active?
          subject
        end

        it 'deactivates the subscription' do
          refute account.tpe_subscription.active?
        end
      end

      describe 'and there is no talk-partner subscription' do
        before do
          assert account.tpe_subscription.blank?
        end

        it 'the action to deactivate is not performedf' do
          Tpe::Subscription.any_instance.
            expects(:deactivate).never
          subject
        end
      end
    end
  end
end
