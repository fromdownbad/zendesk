require_relative '../../support/test_helper'

SingleCov.covered!

describe Provisioning::MultiBrandState do
  let(:described_class) { Provisioning::MultiBrandState }

  describe '.call' do
    subject do
      described_class.call(options)
    end

    let(:account) { accounts(:minimum) }
    let(:journal) { ZBC::Journal.new }
    let(:dry_run) { false }

    let(:payload) do
      {
        account: {
          multibrand_includes_help_centers: true
        }
      }
    end

    let(:options) do
      {
        account: account,
        payload: payload,
        dry_run: dry_run,
        journal: journal
      }
    end

    let(:expected_multibrand_includes_help_centers?) do
      account.settings.reload.multibrand_includes_help_centers?
    end

    before do
      refute account.settings.multibrand_includes_help_centers?
      subject
    end

    it { assert expected_multibrand_includes_help_centers? }
  end
end
