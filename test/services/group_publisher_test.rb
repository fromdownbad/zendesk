require_relative "../support/test_helper"

SingleCov.covered!

describe GroupPublisher do
  fixtures :accounts
  fixtures :groups

  let(:topic) { 'support.group_entities' }
  let(:account) { accounts(:minimum) }
  let(:group) { groups(:minimum_group) }

  let(:publisher) { GroupPublisher.new(account_id: account.id) }

  describe '#publish_tombstone' do
    it 'should produce Kafka record with null value' do
      EscKafkaMessage.expects(:create!).with(
        account_id: account.id,
        topic: topic,
        key: "#{account.id}:#{group.id}",
        value: nil
      )

      publisher.publish_tombstone(group_id: group.id)
    end
  end

  describe '#publish' do
    it 'should produce Kafka record with protobuf value' do
      EscKafkaMessage.expects(:create!).with(
        account_id: account.id,
        topic: topic,
        key: "#{account.id}:#{group.id}",
        value: GroupProtobufEncoder.new(group).to_proto
      )

      publisher.publish(group: group)
    end
  end
end
