require_relative "../support/test_helper"

SingleCov.covered!

describe AccountSubdomainPropagator do
  let(:described_class) { AccountSubdomainPropagator }

  subject do
    described_class.call(account, subdomain: new_account_subdomain, subdomain_was: old_account_subdomain)
  end

  fixtures :accounts, :recipient_addresses

  describe "when updating an account subdomain" do
    let(:account) { accounts(:minimum) }
    let(:old_account_subdomain) { account.subdomain }
    let(:new_account_subdomain) { account.subdomain + "-new234" }
    let(:expected_emails) { ["support@#{new_account_subdomain}.zendesk-test.com"] }
    let(:actual_emails) { account.recipient_addresses.where("email like '%@#{new_account_subdomain}.%'").pluck(:email) }

    it "updates recipient_addresses" do
      subject
      assert_equal actual_emails, expected_emails
    end
  end
end
