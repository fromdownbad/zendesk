require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe BrandPublisher do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:subject) { BrandPublisher.new(account_id: account.id) }

  describe '#publish_for_account' do
    it "publishes all brands for a given account" do
      EscKafkaMessage.expects(:create!).at_most(account.brands.size)

      subject.publish_for_account
    end

    describe 'with a soft deleted account' do
      before do
        account.cancel!
        account.soft_delete!
      end

      it "updates are ignored" do
        # Preventing failures further down the stack
        EscKafkaMessage.expects(:create!).never

        subject.publish_for_account
      end
    end
  end

  describe '#publish_for_route' do
    it "publishes a brand that is related to a route" do
      EscKafkaMessage.expects(:create!).with(
        has_entries(
          topic: "guide.brand_entities",
          key: "#{account.id}:#{account.default_brand.id}"
        )
      )

      subject.publish_for_route(route_id: account.default_brand.route_id)
    end
  end

  describe '#publish_for_brand' do
    it "publishes a specific brand" do
      EscKafkaMessage.expects(:create!).with(
        has_entries(
          topic: "guide.brand_entities",
          key: "#{account.id}:#{account.default_brand.id}"
        )
      )

      subject.publish_for_brand(brand_id: account.default_brand.id)
    end
  end

  describe '#publish_tombstone' do
    it "publishes a publish_tombstone for a specific brand" do
      EscKafkaMessage.expects(:create!).with(
        has_entries(
          topic: "guide.brand_entities",
          key: "#{account.id}:#{account.default_brand.id}",
          value: nil
        )
      )

      subject.publish_tombstone(brand_id: account.default_brand.id)
    end
  end
end
