require_relative '../../support/test_helper'

SingleCov.covered!

describe Billing::SobaParticipation do
  let(:account) { accounts(:minimum) }

  let(:participation) do
    Billing::SobaParticipation.eligible?(account)
  end

  describe 'with :billing_multi_product_participant enabled' do
    before do
      Arturo.enable_feature! :billing_multi_product_participant
    end

    describe 'when reseller account' do
      before do
        account.stubs(reseller_account?: true)
      end

      it { refute participation }
    end

    describe 'when in trial' do
      before do
        account.stubs(is_trial?: true, trial_expired?: false)
      end

      it { assert participation }

      describe 'and not self-service' do
        before do
          account.subscription.stubs(self_service?: false)
        end

        describe 'with :billing_multi_product_for_all_sales_models enabled' do
          before do
            Arturo.enable_feature! :billing_multi_product_for_all_sales_models
          end

          it { assert participation }
        end

        describe 'with :billing_multi_product_for_all_sales_models disabled' do
          before do
            Arturo.disable_feature! :billing_multi_product_for_all_sales_models
          end

          it { refute participation }
        end
      end
    end

    describe 'when not in trial (expired)' do
      before do
        # NOTE: is_trial attr is set to false when it expires
        account.stubs(is_trial?: false, trial_expired?: true)
      end

      it { assert participation }

      describe 'and not self-service' do
        before do
          account.subscription.stubs(self_service?: false)
        end

        describe 'with :billing_multi_product_for_all_sales_models enabled' do
          before do
            Arturo.enable_feature! :billing_multi_product_for_all_sales_models
          end

          it { assert participation }
        end

        describe 'with :billing_multi_product_for_all_sales_models disabled' do
          before do
            Arturo.disable_feature! :billing_multi_product_for_all_sales_models
          end

          it { refute participation }
        end
      end
    end

    describe 'when not in trial (purchased)' do
      let(:subscription) { stub }

      before do
        account.stubs(
          is_trial?:          false,
          zuora_subscription: zuora_subscription
        )
      end

      describe 'and initiated-by billing' do
        let(:zuora_subscription) { stub(initiated_by: 'Billing') }

        it { assert participation }

        describe 'and not self-service' do
          before do
            account.subscription.stubs(self_service?: false)
          end

          describe 'with :billing_multi_product_for_all_sales_models enabled' do
            before do
              Arturo.enable_feature! :billing_multi_product_for_all_sales_models
            end

            it { assert participation }
          end

          describe 'with :billing_multi_product_for_all_sales_models disabled' do
            before do
              Arturo.disable_feature! :billing_multi_product_for_all_sales_models
            end

            it { refute participation }
          end
        end
      end

      describe 'and not initiated-by billing' do
        let(:zuora_subscription) { stub(initiated_by: 'Zendesk') }

        describe 'with :billing_multi_product_for_legacy_accounts enabled' do
          before do
            Arturo.enable_feature! :billing_multi_product_for_legacy_accounts
          end

          describe 'with :billing_multi_product_for_all_pricing_models enabled' do
            before do
              Arturo.enable_feature! :billing_multi_product_for_all_pricing_models
            end

            it { assert participation }
          end

          describe 'with :billing_multi_product_for_all_pricing_models disabled' do
            let(:pricing_model_revision) { nil }

            before do
              Arturo.disable_feature! :billing_multi_product_for_all_pricing_models
              account.subscription.stubs(pricing_model_revision: pricing_model_revision)
            end

            it { refute participation }

            describe 'and pricing-model is allowed' do
              let(:pricing_model_revision_feature) do
                :"billing_multi_product_for_pricing_model_#{pricing_model_revision}"
              end

              before do
                Arturo.enable_feature! pricing_model_revision_feature
                account.subscription.stubs(pricing_model_revision: pricing_model_revision)
              end

              describe 'for pricing-model 0' do
                let(:pricing_model_revision) do
                  ZBC::Zendesk::PricingModelRevision::INITIAL
                end

                it { assert participation }
              end

              describe 'for pricing-model 1' do
                let(:pricing_model_revision) do
                  ZBC::Zendesk::PricingModelRevision::APRIL_2010
                end

                it { assert participation }
              end

              describe 'for pricing-model 2' do
                let(:pricing_model_revision) do
                  ZBC::Zendesk::PricingModelRevision::NEW_FORUMS
                end

                it { assert participation }
              end

              describe 'for pricing-model 3' do
                let(:pricing_model_revision) do
                  ZBC::Zendesk::PricingModelRevision::ENTERPRISE
                end

                it { assert participation }
              end

              describe 'for pricing-model 4' do
                let(:pricing_model_revision) do
                  ZBC::Zendesk::PricingModelRevision::UCSF_CAUSEWARE
                end

                it { assert participation }
              end

              describe 'for pricing-model 5' do
                let(:pricing_model_revision) do
                  ZBC::Zendesk::PricingModelRevision::ENTERPRISE_ELITE
                end

                it { assert participation }
              end

              describe 'for pricing-model 6' do
                let(:pricing_model_revision) do
                  ZBC::Zendesk::PricingModelRevision::TEMPORARY_PATAGONIA
                end

                it { assert participation }
              end

              describe 'for pricing-model 7' do
                let(:pricing_model_revision) do
                  ZBC::Zendesk::PricingModelRevision::PATAGONIA
                end

                it { assert participation }
              end
            end
          end
        end

        describe 'with :billing_multi_product_for_legacy_accounts disabled' do
          before do
            Arturo.disable_feature! :billing_multi_product_for_legacy_accounts
          end

          it { refute participation }
        end
      end
    end
  end

  describe 'with :billing_multi_product_participant disabled' do
    before do
      Arturo.disable_feature! :billing_multi_product_participant
    end

    it { refute participation }
  end
end
