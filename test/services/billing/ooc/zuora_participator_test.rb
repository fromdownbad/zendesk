require_relative '../../../support/test_helper'

SingleCov.covered!

describe Billing::OOC::ZuoraParticipator do
  let(:account) { accounts(:minimum) }

  subject do
    Billing::OOC::ZuoraParticipator.call(account)
  end

  describe_with_arturo_enabled :billing_ooc_zuora do
    it { assert subject }
  end

  describe_with_arturo_disabled :billing_ooc_zuora do
    it { refute subject }
  end
end
