require_relative "../support/test_helper"

SingleCov.covered!

describe AccountNamePropagator do
  let(:described_class) { AccountNamePropagator }

  subject do
    described_class.call(account, name: new_account_name, name_was: old_account_name)
  end

  fixtures :accounts, :organizations

  describe "when updating an account name" do
    let(:account) { accounts(:minimum) }
    let(:old_account_name) { account.name }
    let(:new_name) { "New Name Woo" }
    let(:new_account_name) { new_name }

    before do
      account.address.name = old_account_name
    end

    describe "when updating the default organization name" do
      let(:default_org) { organizations(:minimum_organization1) }

      before do
        default_org.name = "Minimum account"
        default_org.save!
      end

      describe "when an organization with the new account name already exists" do
        let(:new_org) { organizations(:minimum_organization2) }

        before do
          new_org.name = new_name
          new_org.save!
        end

        it "attempts to update the default organization name" do
          account.organizations.expects(:where).with(name: old_account_name).returns([default_org])
          default_org.expects(:update).with(name: new_name).once
          subject
        end

        it "silently fails to update the default organization name" do
          assert_equal false, default_org.update(name: new_name)
          assert_equal "Minimum account", default_org.reload.name
          subject
        end
      end

      describe "when an organization with the new account name does not exist" do
        it "attempts to update the default organization name" do
          account.organizations.expects(:where).with(name: old_account_name).returns([default_org])
          default_org.expects(:update).with(name: new_name).once
          subject
        end

        it "successfully updates the default organization name" do
          assert default_org.update(name: new_name)
          assert_equal new_name, default_org.reload.name
          subject
        end
      end

      describe "with a pipe in the account name" do
        let(:new_account_name) { "This New Name | Has | Pipes" }
        let(:sample_org) { organizations(:minimum_organization2) }

        it "strips the pipes before updating the org name" do
          account.organizations.expects(:where).with(name: old_account_name).returns([sample_org])
          sample_org.expects(:update).with(name: "This New Name Has Pipes")
          subject
        end
      end
    end

    it "updates recipient_addresses" do
      account.recipient_addresses.expects(:where).with(name: old_account_name).returns([])
      subject
    end

    it "updates address name" do
      subject
      assert_equal account.address.name, new_account_name
    end
  end
end
