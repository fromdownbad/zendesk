require_relative '../support/test_helper'

SingleCov.covered!

describe AccountSynchronizer do
  let(:described_class) { AccountSynchronizer }

  describe '.call' do
    subject do
      described_class.call(payload)
    end

    let(:payload) do
      {
        account: {
          master_account_id: master_account_id,
          billing_id:        billing_id
        }
      }
    end

    let(:master_account_id)  { stub }
    let(:billing_id)         { stub }
    let(:journal_comparison) { stub }
    let(:span)               { stub(:span) }

    let(:service_options) do
      {
        service:  "classic-provisioning-service",
        resource: resource
      }
    end

    before do
      Provisioning::JournalComparator.stubs(:call).
        with(payload).
        returns(journal_comparison)
      span.expects(:set_tag).
        with('account.master_account_id', master_account_id)
      span.expects(:set_tag).
        with('account.billing_id', billing_id)
      span.expects(:set_tag).
        with('payload', payload)
      span.expects(:set_tag).
        with('comparison', journal_comparison.to_h)
    end

    describe 'when old and new implementation run is consistent' do
      let(:journal_comparison) do
        OpenStruct.new(matched?: true, details: [:diff_checked, stub])
      end

      let(:resource) { 'classic.sync.executed' }

      it 'invokes the new provisioning service using the payload' do
        ZendeskAPM.expects(:trace).
          with('classic-provisioning', service_options).
          yields(span)
        Provisioning.expects(:call).
          with(master_account_id, payload: payload, dry_run: false)

        subject
      end
    end

    describe 'when old and new implementation run is not consistent' do
      let(:synchronizer) { stub }

      let(:journal_comparison) do
        OpenStruct.new(matched?: false, details: [:diff_checked, stub])
      end

      let(:resource) { 'zbc.sync.executed' }

      before do
        ZendeskAPM.expects(:trace).
          with('classic-provisioning', service_options).
          yields(span)
        ZBC::Zuora::Synchronizer.stubs(:new).
          with(billing_id, bypass_approval: true).
          returns(synchronizer)
      end

      it 'invokes the zbc zuora synchronizer' do
        Provisioning.expects(:call).never
        synchronizer.expects(:synchronize!)

        subject
      end
    end
  end
end
