require_relative '../support/test_helper'

SingleCov.covered!

describe Base do
  describe '.call' do
    it "fails with NotImpemented error" do
      assert_raises(NotImplementedError) do
        Base.call({})
      end
    end
  end
end
