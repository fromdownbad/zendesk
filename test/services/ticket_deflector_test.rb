require_relative "../support/test_helper"

SingleCov.covered!

describe TicketDeflector do
  fixtures :tickets, :accounts
  let(:created_at) { 2.minutes.ago }
  let(:account) { accounts(:minimum) }
  let(:account_id) { account.id }
  let(:brand) { stub('brand', subdomain: 'another_brand') }
  let(:resolution_channel_id) { ViaType.WEB_WIDGET }
  let(:deflection_channel_id) { Zendesk::Types::ViaType.WEB_WIDGET }
  let(:ticket) do
    ticket = tickets(:minimum_1)
    ticket.via_id = ViaType.MAIL
    ticket.will_be_saved_by User.system
    ticket.save!
    ticket
  end
  let(:ab_statsd_client) { stub('ab_statsd_client', via_id: ticket.via_id, subdomain: brand.subdomain) }
  let(:additional_tags) { 'ab_resolved' }
  let(:ticket_deflection) do
    stub(
      'ticket_deflection',
      brand: brand,
      via_id: ViaType.MAIL,
      detected_locale: 'en',
      model_version: '40100',
      deflection_channel_id: deflection_channel_id,
      ticket: ticket
    )
  end
  let(:ticket_deflector) do
    TicketDeflector.new(account: account, deflection: ticket_deflection, resolution_channel_id: resolution_channel_id)
  end

  let(:ticket_deflector_without_ticket) do
    TicketDeflector.new(account: account, deflection: ticket_deflection, resolution_channel_id: resolution_channel_id)
  end

  before do
    AnswerBot::TicketSolverJob.stubs(:enqueue_in)
    Datadog::AnswerBot.stubs(:new).
      with(
        via_id: ticket_deflection.via_id,
        subdomain: ticket_deflection.brand.subdomain,
        deflection_channel_id: deflection_channel_id,
        resolution_channel_id: resolution_channel_id
      ).
      returns(ab_statsd_client)
  end

  describe 'solve' do
    let(:article_id) { 666777 }
    let(:mobile) { true }
    let(:user_id) { 666 }
    let(:user) { stub('user', id: user_id, is_agent?: false) }
    let(:ticket_deflection_state) { TicketDeflection::STATE_NO_ACTION }
    let(:created_at) { 2.minutes.ago }
    let(:ticket_deflection) do
      stub(
        'ticket_deflection',
        brand: brand,
        id: 333444,
        via_id: ticket.via_id,
        user: user,
        state: ticket_deflection_state,
        created_at: created_at,
        detected_locale: 'en',
        model_version: '40100',
        ticket: ticket
      )
    end
    let(:stats) do
      {
        subdomain: ticket_deflection.brand.subdomain,
        via_id: ticket_deflection.via_id,
        mobile: mobile,
        model_version: ticket_deflection.model_version,
        language: ticket_deflection.detected_locale
      }
    end
    let(:solve!) do
      ticket_deflector.solve(
        article_id: article_id,
        mobile: mobile
      )
    end

    before do
      account.stubs(:id).returns(account_id)
      ticket_deflection.stubs(:deflection_channel_id).returns(deflection_channel_id)
      ticket.stubs(:ticket_deflection).returns(ticket_deflection)

      AnswerBot::TicketSolverJob.stubs(:enqueue)
    end

    describe 'when ticket is finished' do
      before do
        ticket.stubs(:finished?).returns(true)
      end

      it 'returns false' do
        refute solve!
      end
    end

    describe 'when ticket is not finished' do
      before do
        ticket.stubs(:finished?).returns(false)
        ab_statsd_client.stubs(:solve_initiated)
        ticket.stubs(:additonal_tags).returns(additional_tags)
        AnswerBot::TicketSolverJob.stubs(:enqueue).returns(true)
      end

      describe 'when solve attempt is too quick via email' do
        let(:created_at) { 10.seconds.ago }
        let(:deflection_channel_id) { Zendesk::Types::ViaType.MAIL }

        before do
          ticket_deflection.stubs(:created_at).returns(created_at)
        end

        it 'returns false' do
          ab_statsd_client.stubs(:solved_too_quick)

          refute solve!
        end

        it 'sends datadog metric' do
          ab_statsd_client.expects(:solved_too_quick).
            with(mobile: mobile, language: 'en', model_version: '40100')

          solve!
        end
      end

      it 'enqueues TicketSolverJob' do
        AnswerBot::TicketSolverJob.expects(:enqueue).
          with(
            account_id,
            user_id,
            ticket_deflection.id,
            article_id,
            resolution_channel_id,
            stats
          )

        solve!
      end

      describe "when Answer Bot automatic tagging is enabled" do
        before do
          Arturo.enable_feature!(:answer_bot_tag_automation)
        end

        it "starts the deflection tagging job" do
          AnswerBot::TicketDeflectionTaggingJob.expects(:enqueue).once

          solve!
        end
      end
    end
  end

  describe '#process_deflection_rejection' do
    let(:article_id) { 11112222 }

    before do
      ticket.stubs(:finished?).returns(false)
      ticket.stubs(:ticket_deflection).returns(ticket_deflection)
      ticket.stubs(:additonal_tags).returns(additional_tags)

      ticket_deflection.stubs(:solved?).returns(false)
      ticket_deflection.stubs(:via_id).returns(ticket.via_id)

      ab_statsd_client.stubs(:irrelevant)
    end

    it 'updates ticket deflection state to NOT_SOLVED' do
      TicketDeflectionArticle.stubs(:reject_by_end_user)
      ticket_deflection.expects(:mark_as_not_solved)

      ticket_deflector.process_deflection_rejection(article_id: article_id)
    end

    it 'updates ticket deflection articles to be marked as irrelevant' do
      ticket_deflection.stubs(:mark_as_not_solved)
      TicketDeflectionArticle.expects(:reject_by_end_user).
        with(ticket_deflection, article_id, 1)

      ticket_deflector.process_deflection_rejection(
        article_id: article_id,
        reason: 1
      )
    end

    it 'sends irrelevant stats to datadog' do
      ticket_deflection.stubs(:mark_as_not_solved)
      TicketDeflectionArticle.stubs(:reject_by_end_user)
      ab_statsd_client.expects(:irrelevant).
        with(language: 'en', model_version: '40100')

      ticket_deflector.process_deflection_rejection(article_id: article_id)
    end

    describe "when Answer Bot automatic tagging is enabled" do
      before do
        Arturo.enable_feature!(:answer_bot_tag_automation)
      end

      it "adds ab_marked_unhelpful tag to ticket" do
        ticket_deflection.stubs(:mark_as_not_solved)
        TicketDeflectionArticle.stubs(:reject_by_end_user)
        ticket_deflector.process_deflection_rejection(article_id: article_id)

        ticket.reload
        assert ticket.current_tags.include?('ab_marked_unhelpful')
      end
    end

    describe 'ticket is nil' do
      it 'updates ticket deflection state to NOT_SOLVED' do
        TicketDeflectionArticle.stubs(:reject_by_end_user)
        ticket_deflection.expects(:mark_as_not_solved)

        ticket_deflector_without_ticket.process_deflection_rejection(article_id: article_id)
      end

      it 'updates ticket deflection articles to be marked as irrelevant' do
        ticket_deflection.stubs(:mark_as_not_solved)
        TicketDeflectionArticle.expects(:reject_by_end_user).
          with(ticket_deflection, article_id, 1)

        ticket_deflector_without_ticket.process_deflection_rejection(
          article_id: article_id,
          reason: 1
        )
      end

      it 'sends irrelevant stats to datadog' do
        ticket_deflection.stubs(:mark_as_not_solved)
        TicketDeflectionArticle.stubs(:reject_by_end_user)
        ab_statsd_client.expects(:irrelevant).
          with(language: 'en', model_version: '40100')

        ticket_deflector_without_ticket.process_deflection_rejection(article_id: article_id)
      end
    end

    describe 'ticket is finished' do
      before do
        ticket.stubs(:finished?).returns(true)
      end

      it 'does nothing' do
        ticket_deflection.expects(:mark_as_not_solved).never

        ticket_deflector.process_deflection_rejection(article_id: article_id)
      end
    end
  end
end
