require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe UserEntityPublisher do
  fixtures :users
  let(:described_class) { UserEntityPublisher }
  let(:user) { users(:minimum_end_user) }
  let(:publisher) { UserEntityPublisher.new(account_id: user.account_id) }

  describe '#publish_for_account' do
    it "publishes users for an account" do
      user_count = Account.unscoped.find(user.account_id).users.count
      EscKafkaMessage.expects(:create!).times(user_count)

      publisher.publish_for_account(backfill_status: BackfillStatus.new("backfill", FakeRedis::Redis.new), user_counter: 100)
    end
  end

  describe '#publish_for_user' do
    it "publishes a specific user" do
      EscKafkaMessage.expects(:create!).with(
        has_entries(
          topic: "support.user_entity",
          key: "#{user.account_id}/#{user.id}"
        )
      )

      publisher.publish_for_user(user_id: user.id)
    end
  end

  describe '#publish_tombstone' do
    it "publishes a publish_tombstone for a specific user" do
      EscKafkaMessage.expects(:create!).with(
        has_entries(
          topic: "support.user_entity",
          key: "#{user.account_id}/#{user.id}",
          value: nil
        )
      )

      publisher.publish_tombstone(user_id: user.id)
    end
  end
end
