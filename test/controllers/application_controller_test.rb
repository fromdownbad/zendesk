require_relative "../support/test_helper"

SingleCov.covered! uncovered: 79

ApplicationController.class_eval do
  public :current_device
end

describe ApplicationController do
  should_include_module Zendesk::CommentOnQueries

  class ApplicationTestController < ApplicationController
    before_action :authenticate_user, only: [:authenticated, :index, :update]
    before_action :set_mobile_format, only: :show
    skip_before_action :verify_current_account, only: :asset

    def show
      head 200
    end

    def index
      head 200
    end

    def update
      head 200
    end

    def create_201
      head 201
    end

    def asset
      head 200
    end

    def delete
      head 204
    end

    def not_found
      raise ActiveRecord::RecordNotFound
    end

    def create
      raise ActionController::InvalidAuthenticityToken
    end

    def authenticated
      head 200
    end

    def denied
      deny_access
    end

    def try_json
      render json: {}, callback: params[:callback], content_type: params[:content_type]
    end

    def with_set_source_page
      set_source_page
      head :ok
    end
  end

  class ApplicationCleanController < ApplicationController
  end

  tests ApplicationTestController
  use_test_routes

  let(:account) { accounts(:minimum) }

  fixtures :accounts, :subscriptions, :users, :addresses, :organizations,
    :translation_locales, :certificates, :remote_authentications, :role_settings

  before do
    @request.account = account
    login(:minimum_agent)
    @request.stubs(:language_region_compatible_from).returns("en-US")
    @english = translation_locales(:english_by_zendesk)
    @japanese = translation_locales(:japanese)
    Account.any_instance.stubs(:available_languages).returns([@english, @japanese])
    @controller.stubs(current_account: account)
  end

  describe "#use_remote_authentication?" do
    let(:remote_auth) { remote_authentications(:minimum_remote_authentication) }

    it "returns false -- by default" do
      refute @controller.send(:use_remote_authentication?)
    end

    describe "with no IP" do
      before do
        remote_auth.update_attributes!(is_active: true)
      end

      it "returns true" do
        assert @controller.send(:use_remote_authentication?)
      end
    end

    describe "with an IP" do
      before do
        remote_auth.update_attributes!(is_active: true, ip_ranges: "128.0.0.1")
      end

      it "returns false if ip not in range" do
        refute @controller.send(:use_remote_authentication?)
      end

      describe "remote_ip is in range" do
        before { @request.stubs(remote_ip: "128.0.0.1") }
        it "returns true" do
          assert @controller.send(:use_remote_authentication?)
        end
      end
    end
  end

  describe "#internal_request?" do
    describe "when the request is from an internal IP" do
      before { @request.env['REMOTE_ADDR'] = '127.0.0.1' }

      it "returns true" do
        assert @controller.send(:internal_request?)
      end
    end

    describe "when the request is from an external IP" do
      before { @request.env['REMOTE_ADDR'] = '192.0.0.0' }

      it "returns false" do
        refute @controller.send(:internal_request?)
      end
    end
  end

  describe "filter_parameter_logging" do
    it "has #filter_parameters" do
      assert_includes Rails.application.config.filter_parameters, :"card[verification_value]"
    end
  end

  describe "#set_mobile_format" do
    before do
      @request.stubs(:is_mobile?).returns(true)
      account.stubs(:has_mobile_switch_enabled?).returns(true)
    end

    it "pass if the format parameter is nil" do
      @request.stubs(:format).returns(nil)
      get :show
      assert_response :success
    end

    it "does not set format to mobile if params[:format] is set to html and request.is_mobile?" do
      get :show, format: 'html'
      assert_equal 'html', @request.parameters[:format]
    end

    describe "params[:format] isn't set" do
      it "sets template_format to mobile_v2" do
        get :show
        assert_equal 'mobile_v2', @request.parameters[:format]
      end
    end
  end

  describe "action caching" do
    before do
      logout
    end

    it "is unsafe for logged in users" do
      login(:minimum_end_user)
      get :show

      assert_equal false, @controller.send(:is_action_cache_safe?)
    end

    it "is safe for anonymous users" do
      get :show
      assert(@controller.send(:is_action_cache_safe?))
    end
  end

  describe '#deny_access' do
    it "is unauthenticated if user is not logged in" do
      logout
      get :denied
      assert_response :unauthorized
    end

    it "is forbidden if user is logged in" do
      get :denied
      assert_response :forbidden
    end
  end

  describe "CSRF token within the shared session" do
    it "calls #ensure_csrf_token_into_shared_session" do
      @controller.expects(:ensure_csrf_token_into_shared_session)

      get :show
    end
  end

  it 'logs the ActionController::InvalidAuthenticityToken error' do
    Rails.logger.expects(:warn).at_least_once
    post :create
    assert_response :forbidden, @response.body
  end

  it "transcodes request.url to valid UTF8" do
    utf8_path = "/entries/22116418-Accéder-à-mon"
    utf8_path.force_encoding(Encoding::ISO_8859_1)
    @request.env["PATH_INFO"] = utf8_path
    get :show

    assert_equal "/entries/22116418-Accéder-à-mon", @request.fullpath
    assert_equal "https://minimum.zendesk-test.com/entries/22116418-Accéder-à-mon", @request.url
    assert_equal Encoding::UTF_8, @request.url.encoding
    assert(@request.url.valid_encoding?)
  end

  describe "password expiration" do
    before do
      User.any_instance.stubs(:password_expired?).returns(password_expired)
    end

    describe "without expired password" do
      let(:password_expired) { false }

      it "works normally" do
        get :index

        assert_nil shared_session[:auth_password_expired]
        assert_response :success
      end
    end

    describe "with expired password" do
      let(:password_expired) { true }

      it "expires password" do
        get :index

        assert(@controller.send(:authentication).password_expired?)
        assert_redirected_to '/password'
      end

      it "expires password when it was checked a while ago" do
        shared_session[:auth_password_expired_checked] = 1.day.ago.to_i

        get :index

        assert(@controller.send(:authentication).password_expired?)
        assert_redirected_to '/password'
      end

      it "does not expire password if it is expired but we recently checked" do
        shared_session[:auth_password_expired_checked] = 1.minute.ago.to_i

        get :index

        assert_equal false, @controller.send(:authentication).password_expired?
        assert_response :success
      end

      it "allows access to users that have expired password but used master_token strategy" do
        assert(@controller.send(:authentication).password_expired?)
        session = assigns(:authentication).send(:session)
        session[:via] = "master_token"

        assert_equal false, @controller.send(:authentication).password_expired?
      end

      it "does not touch password expiration for actions that disrupt a user's work (e.g. update, autocompletion, etc.)" do
        shared_session[:auth_password_expired_checked] = 1.day.ago.to_i

        put :update
        assert_response :success
      end
    end
  end

  describe "session expiration" do
    it "does not allow access to users with expired sessions on authenticated actions" do
      get :authenticated
      assert_equal false, @controller.send(:authentication).stale?
      assert_response :success

      @controller.send(:authentication).stubs(:stale?).returns(true)
      get :authenticated
      assert_response :unauthorized
    end

    it "expires the session after 8 hours of inactivity" do
      get :authenticated
      assigns(:authentication).touch

      Timecop.travel(9.hours.from_now) do
        assert(assigns(:authentication).stale?)
      end

      Timecop.travel(7.hours.from_now) do
        assert_equal false, assigns(:authentication).stale?
      end
    end

    it "expires inactive sessions for custom session timeout values" do
      account.settings.session_timeout = 60 * 24 * 14 # 2 weeks
      account.settings.save

      logout
      login(account.owner)

      get :authenticated
      assert_response :success

      Timecop.travel(13.days.from_now) do
        assert_equal false, assigns(:authentication).stale?
      end

      Timecop.travel(15.days.from_now) do
        assert(assigns(:authentication).stale?)
      end
    end

    it "returns a session expiration header" do
      session_expiration_time = @controller.send(:authentication).timeout

      get :show

      assert_equal response.headers['X-Zendesk-User-Session-Expires-At'], session_expiration_time.to_i
    end
  end

  describe 'session extension' do
    it 'extends the session if HTTP_X_ZENDESK_RENEW_SESSION header is true' do
      before_timeout = @controller.send(:authentication).timeout
      @controller.send(:statsd_client).expects(:increment).with('renew_session', tag: 'renew_header').once

      Timecop.travel(10.minutes.from_now) do
        set_header('HTTP_X_ZENDESK_RENEW_SESSION', 'true')
        get :show

        assert @controller.send(:authentication).timeout > before_timeout
      end
    end

    it 'does not extend session if HTTP_X_ZENDESK_RENEW_SESSION header is false' do
      before_timeout = @controller.send(:authentication).timeout
      @controller.send(:statsd_client).expects(:increment).with('renew_session', tag: 'renew_header').never

      Timecop.travel(10.minutes.from_now) do
        set_header('HTTP_X_ZENDESK_RENEW_SESSION', 'false')
        get :show

        assert_equal @controller.send(:authentication).timeout, before_timeout
      end
    end

    it 'does not extend session if no header' do
      before_timeout = @controller.send(:authentication).timeout

      Timecop.travel(10.minutes.from_now) do
        get :show

        assert_equal @controller.send(:authentication).timeout, before_timeout
      end
    end
  end

  describe 'for anonymous users' do
    before do
      logout
      @locale = translation_locales(:japanese)
    end

    it 'sets the locale on the user object to the locale stored in the session' do
      shared_session[:locale_id] = @locale.id
      get :show
      user = @controller.send :current_user
      assert_equal @locale, user.translation_locale

      # assert it hasn't been saved:
      assert user.locale_id_changed?
    end

    it 'sets the locale based on accept headers, if no locale is stored in the session' do
      shared_session[:locale_id] = nil
      set_header('HTTP_ACCEPT_LANGUAGE', "ja,en-US;q=0.8,en;q=0.6,fr-FR;q=0.4,fr;q=0.2")
      set_header("http_accept_language.parser", HttpAcceptLanguage::Parser.new(@request.env["HTTP_ACCEPT_LANGUAGE"]))

      get :show
      user = @controller.send :current_user

      assert_equal @japanese.locale, user.translation_locale.locale

      # assert it hasn't been saved:
      assert user.locale_id_changed?
    end
  end

  describe 'for users with a nil locale_id, use accept-language headers' do
    before do
      Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
      @user = login(:minimum_end_user)
      @user.locale_id = nil
      set_header('HTTP_ACCEPT_LANGUAGE', "ja,en-US;q=0.8,en;q=0.6,fr-FR;q=0.4,fr;q=0.2")
      set_header("http_accept_language.parser", HttpAcceptLanguage::Parser.new(@request.env["HTTP_ACCEPT_LANGUAGE"]))
    end

    describe "detect latinoamerica spanish" do
      before do
        set_header('HTTP_ACCEPT_LANGUAGE', "es-419,en-US;q=0.8,en;q=0.6,fr-FR;q=0.4,fr;q=0.2")
        set_header("http_accept_language.parser", HttpAcceptLanguage::Parser.new(@request.env["HTTP_ACCEPT_LANGUAGE"]))
        @latin_american_spanish = translation_locales(:latin_american_spanish)
        @spanish                = translation_locales(:spanish)
        Account.any_instance.stubs(:available_languages).returns([@spanish, @latin_american_spanish, @japanese])
      end

      it 'does not fail to detect and set es-419' do
        get :show
        assert_equal @latin_american_spanish.locale, @user.translation_locale.locale
      end

      # Firefox doesn't suppor es-419, check ZD#614895 for details
      it 'detects in firefox' do
        set_header('HTTP_ACCEPT_LANGUAGE', "es-mx,en-US;q=0.8,en;q=0.6,fr-FR;q=0.4,fr;q=0.2")
        set_header("http_accept_language.parser", HttpAcceptLanguage::Parser.new(@request.env["HTTP_ACCEPT_LANGUAGE"]))
        get :show
        assert_equal @latin_american_spanish.locale, @user.translation_locale.locale
      end
    end

    it 'sets the users translation locale to their browser preferred language but NOT save it in a get request' do
      get :show
      assert_equal @japanese.locale, @user.translation_locale.locale
    end

    it 'sets the users translation locale to their browser preferred language AND save it in a post request' do
      put :update
      assert_equal @japanese.locale, @user.translation_locale.locale
      assert_equal @japanese.id, @user.locale_id
    end
  end

  describe '#set_ip_address_for_current_user when user is present' do
    before do
      @user = @controller.send(:current_user)
      @request.stubs(:remote_ip).returns("131.175.6.2")
      get :show
    end

    it 'returns request#remote_ip value' do
      assert_equal "131.175.6.2", @controller.send(:set_ip_address_for_current_user)
    end

    it 'assigns ip_address to current_user' do
      assert_equal "131.175.6.2", @user.ip_address
    end
  end

  describe '#set_ip_address_for_current_user when user is not present' do
    before do
      logout
      @user = @controller.send(:current_user)
    end

    it 'shoulds not raise if current_user is not present' do
      assert @user.is_anonymous_user?
      @controller.send(:set_ip_address_for_current_user)
    end
  end

  describe "error catching" do
    before do
      @controller.stubs(:current_brand).returns(@request.account.brands.first)
    end

    it "renders 404 on 404not found" do
      get :not_found
      assert_response :not_found
      assert_equal @response.body, File.read("#{Rails.public_path}/404.html")
    end

    it "renders 404 with ie and empty headers" do
      @request.user_agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'
      @request.env['HTTP_ACCEPT'] = "''"
      get :not_found
      assert_response :not_found
      assert_equal "text/html", @response.content_type.split(";").first
    end
  end

  should_not_have_extra_actions(ApplicationCleanController)

  describe "security" do
    describe "with no account" do
      before do
        @request.env['zendesk.account'] = nil
        logout
        get :asset
      end

      %w[
        X-FRAME-OPTIONS X-Content-Type-Options X-WebKit-CSP-Report-Only
        X-Content-Security-Policy-Report-Only Content-Security-Policy-Report-Only
      ].each do |header|
        it "does not set #{header}" do
          assert_nil @response.headers[header], "Found #{header} in #{@response.headers.inspect}"
        end
      end
    end

    describe "with an account" do
      before do
        account.settings.x_frame_header = true
        account.settings.xss_header = true
        account.settings.csp_header = false
        account.settings.content_type_header = false
        account.settings.save!
      end

      describe "in a normal request (default security header settings)" do
        before do
          get :show
        end

        it "sets xss header" do
          assert_not_nil @response.headers["X-XSS-Protection"], @response.headers.inspect
        end

        it "sets x-frame header" do
          assert_not_nil @response.headers["X-Frame-Options"], @response.headers.inspect
        end

        it "doesn't set csp_header" do
          assert_nil @response.headers["Content-Security-Policy"], @response.headers.inspect
        end

        it "doesn't set x-content-type header" do
          assert_nil @response.headers["X-Content-Type-Options"], @response.headers.inspect
        end
      end

      describe "with csp header option" do
        describe "enabled" do
          before do
            account.settings.csp_header = true
            account.settings.save!
            get :show
          end

          it "sets csp header" do
            assert_not_nil(@response.headers.key?("X-WebKit-CSP") || @response.headers.key?("Content-Security-Policy"))
          end
        end

        describe "disabled" do
          before do
            account.settings.csp_header = false
            account.settings.save!
            get :show
          end

          it "does not set csp header" do
            assert_nil @response.headers["Content-Security-Policy"], @response.headers.inspect
          end
        end
      end
    end
  end

  describe "#current_device" do
    before do
      @user = login(:minimum_agent)
    end

    describe "Given a device with the token, TOKEN" do
      before do
        @device = Device.create!(token: "TOKEN") do |device|
          device.account = account
          device.user = @user
        end
      end

      describe "And a session with that token" do
        before do
          @controller.permanent_cookies[:device_tokens] = { @user.id.to_s => @device.token }
        end

        it "returns the device" do
          assert_equal(@device, @controller.current_device)
        end
      end

      describe "And a session without that token" do
        before do
          @controller.permanent_cookies[:device_tokens] = nil
        end

        it "returns a null device" do
          assert @controller.current_device.is_a?(NilDevice)
        end
      end

      describe "And a session with multiple tokens" do
        before do
          @controller.permanent_cookies[:device_tokens] = {
            @user.id.to_s => @device.token,
            '12345678'    => 'ANOTHER_TOKEN'
          }
        end

        it "returns the device" do
          assert_equal(@device, @controller.current_device)
        end
      end

      describe "And a session with an invalid token" do
        before do
          @controller.permanent_cookies[:device_tokens] = { @user.id.to_s => 'DIFFERENT' }
        end

        it "logs the user out" do
          get :authenticated
          assert_response :unauthorized
        end
      end
    end

    describe "Without a device" do
      describe "And a session with that token" do
        it "returns a null device" do
          assert @controller.current_device.is_a?(NilDevice)
        end
      end

      describe "And a session without that token" do
        before do
          @controller.permanent_cookies[:device_tokens] = nil
        end

        it "returns a null device" do
          assert @controller.current_device.is_a?(NilDevice)
        end
      end
    end
  end

  describe "#action_cache_key" do
    it "has different keys for html and mobile" do
      html = @controller.send(:action_cache_key, {})
      @request.format = Mime[:mobile_v2]
      mobile = @controller.send(:action_cache_key, {})
      refute_equal html, mobile
    end
  end

  describe "#set_source_page" do
    it "stores source page" do
      get :with_set_source_page, params: { a: "a" }
      assert_equal "/test/route/application_test/with_set_source_page?a=a", session[:source_page]
    end

    it "does not store pages that cause cookie overflows" do
      get :with_set_source_page, params: { a: "a" * 100000 }
      refute session[:source_page]
    end
  end

  describe "#block_monitor_changes" do
    before do
      shared_session[:auth_assuming_monitor_user_id] = 1
    end

    describe "for an RO session" do
      before { request.shared_session[:assuming_monitor_read_only] = true }
      [[:put, :update], [:post, :create_201], [:delete, :delete]].each do |action, method|
        it "prevents #{action}" do
          send(action, method)
          assert_response :bad_request
          assert_includes @response.body, "Your change has been blocked"
        end
      end
    end

    describe "for an RW session" do
      before { request.shared_session[:assuming_monitor_read_only] = false }

      it "allows put/post/delete" do
        post :create_201
        assert_response :created

        put :update
        assert_response :ok

        delete :delete
        assert_response :no_content
      end
    end
  end

  describe "#verify_current_account" do
    describe "with no current account" do
      before do
        @request.account = nil
        @controller.stubs(current_account: nil)
      end

      it "redirects to marketing site" do
        get :show
        assert_response :redirect
        assert_equal 301, @response.status
      end
    end

    describe "with a current inactive account" do
      before do
        account.stubs(:is_active?).returns(false)
      end

      it "redirects to marketing site" do
        get :show
        assert_response :redirect
        assert_equal 301, @response.status
      end
    end
  end

  describe 'with a multiproduct account' do
    let(:account) { accounts(:multiproduct) }
    let(:support_trial_product) { FactoryBot.build(:support_trial_product) }
    let(:support_expired_trial_product) { FactoryBot.build(:support_expired_trial_product) }
    let(:support_not_started_product) { FactoryBot.build(:support_not_started_product) }
    let(:support_free_product) { FactoryBot.build(:support_free_product) }
    let(:support_subscribed_product) { FactoryBot.build(:support_subscribed_product) }
    let(:support_inactive_product) { FactoryBot.build(:support_inactive_product) }
    let(:chat_trial_product) { FactoryBot.build(:chat_trial_product) }

    describe 'there is only the support product' do
      before do
        response_body = { products: [support_not_started_product] }.to_json
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
          to_return(status: 200, body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      end

      it 'redirects to trial activation  if there are no products' do
        get :show
        assert_redirected_to 'https://multiproduct.zendesk-test.com/agent/start'
      end

      it ' does not activate a support trial' do
        account.expects(:start_support_trial).never
        get :show
      end
    end

    describe_with_arturo_enabled :ocp_support_shell_account_creation do
      describe 'there is chat and not started support product' do
        before do
          response_body = { products: [support_not_started_product, chat_trial_product] }.to_json
          stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
            to_return(status: 200, body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
        end

        it 'redirects to trial activation start' do
          account.stubs(:start_support_trial)
          get :show
          assert_redirected_to 'https://multiproduct.zendesk-test.com/agent/start'
        end

        it 'does not activate a support trial' do
          account.expects(:start_support_trial).never
          get :show
        end
      end

      describe 'there is chat product' do
        before do
          response_body = { products: [chat_trial_product] }.to_json
          stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
            to_return(status: 200, body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
        end

        it 'redirects to trial activation start' do
          account.stubs(:start_support_trial)
          get :show
          assert_redirected_to 'https://multiproduct.zendesk-test.com/agent/start'
        end

        it 'does not activate a support trial' do
          account.expects(:start_support_trial).never
          get :show
        end
      end
    end

    it 'redirects to trial activation if there is no support product' do
      stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products/support").
        to_return(status: 404, headers: { content_type: 'application/json; charset=utf-8' })
      get :show
      assert_redirected_to 'https://multiproduct.zendesk-test.com/agent/start'
    end

    it 'redirects to trial activation if support product state is not_started' do
      response_body = { product: support_not_started_product }.to_json
      stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products/support").
        to_return(status: 200, body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      get :show
      assert_redirected_to 'https://multiproduct.zendesk-test.com/agent/start'
    end

    it 'returns 200 if support product is in a trial' do
      response_body = { products: [support_trial_product] }.to_json
      stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
        to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      get :show
      assert_equal 200, @response.status
    end

    it 'returns 200 if support product is in a subscribed state' do
      response_body = { products: [support_subscribed_product] }.to_json
      stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
        to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      get :show
      assert_equal 200, @response.status
    end

    it 'returns 200 if support product is in a free state' do
      response_body = { products: [support_free_product] }.to_json
      stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
        to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      get :show
      assert_equal 200, @response.status
    end

    it 'redirects to billing if support product is an expired trial' do
      response_body = { products: [support_expired_trial_product] }.to_json
      stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
        to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      get :show
      assert_redirected_to 'https://multiproduct.zendesk-test.com/account/subscription'
    end

    it 'redirects to billing if support product is marked inactive' do
      response_body = { products: [support_inactive_product] }.to_json
      stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
        to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      get :show
      assert_redirected_to 'https://multiproduct.zendesk-test.com/account/subscription'
    end

    it 'returns 200 if already on the billing page' do
      response_body = { product: support_expired_trial_product }.to_json
      stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products/support").
        to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      @controller.request.stubs(:path).returns(@controller.send(:subscription_settings_path))
      get :show
      assert_equal 200, @response.status
    end
  end
end
