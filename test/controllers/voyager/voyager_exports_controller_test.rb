require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 5

describe Voyager::VoyagerExportsController do
  fixtures :accounts, :users, :role_settings, :brands, :routes

  let(:account) { accounts(:minimum) }
  let(:token) { 'valid_token' }
  let(:invalid_token) { 'invalid_token' }

  let(:authorized_url_response) do
    OpenStruct.new(
      url: 'http://some-fake-host.com/download/real_file.zip',
      content_type: 'application/zip',
      filename: 'real_file.zip',
      successful?: true
    )
  end

  let(:not_found_response) do
    OpenStruct.new(errors: ['Could not find export job'], successful?: false)
  end

  let(:statsd_client) { @controller.send(:statsd_client) }

  let (:response_201) do
    response = Faraday::Response.new
    response.expects(:status).returns(201)
    response
  end

  let (:response_202) do
    response = Faraday::Response.new
    response.expects(:status).returns(202)
    response
  end

  let (:response_error) do
    response = Faraday::Response.new
    response.expects(:status).returns(500)
    response
  end

  describe 'on a GET to #download' do
    should_route :get, '/voyager/download/asd1', controller: 'voyager/voyager_exports', action: :download, id: 'asd1'

    describe 'with a valid token' do
      before do
        Voyager::API::Client.any_instance.stubs(:authorized_url).with(token).returns(authorized_url_response)
      end

      as_an_end_user do
        before { get :download, params: { id: token } }
        it 'is forbidden' do
          must_respond_with :forbidden
        end
      end

      as_an_agent do
        before { get :download, params: { id: token } }
        it 'is forbidden' do
          must_respond_with :forbidden
        end
      end

      as_an_admin do
        describe 'with export configuration' do
          before do
            @request.account = account
          end

          it 'respond with an nginx S3 redirect if the admin is whitelisted' do
            get :download, params: { id: token }
            # statsd_client.expects(:increment).with('request.download', tags: ['response:200'])
            assert_correct_s3_headers
          end

          it 'be forbidden if the admin is not whitelisted' do
            Zendesk::Export::Configuration.any_instance.
              expects(:whitelisted?).
              with(users(:minimum_admin_not_owner)).returns(false)

            get :download, params: { id: token }
            must_respond_with :forbidden
          end
        end

        describe 'as an assuming user from monitor' do
          before do
            @controller.stubs(is_assuming_user_from_monitor?: true)
            get :download, params: { id: token }
          end

          it 'blocks the download' do
            must_respond_with :forbidden
          end
        end

        describe 'with an account with no ssl required' do
          before do
            @account.stubs(ssl_required?: false)
          end

          it 'does not redirect to the http version of the url' do
            get :download, params: { id: token }
            must_respond_with :ok
            assert_correct_s3_headers
          end
        end
      end

      as_an_owner do
        before { get :download, params: { id: token } }

        it 'respond with an nginx S3 redirect' do
          # statsd_client.expects(:increment).with('request.download', tags: ['response:200'])
          assert_correct_s3_headers
        end
      end
    end

    describe 'with an invalid token' do
      before do
        Voyager::API::Client.any_instance.stubs(:authorized_url).with(invalid_token).returns(not_found_response)
      end

      as_an_end_user do
        before { get :download, params: { id: invalid_token } }
        it 'is forbidden' do
          must_respond_with :forbidden
        end
      end

      as_an_agent do
        before { get :download, params: { id: invalid_token } }
        it 'is forbidden' do
          must_respond_with :forbidden
        end
      end

      as_an_admin do
        before { get :download, params: { id: invalid_token } }
        it 'returns not_found' do
          # statsd_client.expects(:increment).with('request.download', tags: ['response:404'])
          must_respond_with :not_found
        end
      end
    end
  end

  describe 'on a POST to #create' do
    should_route :post, '/voyager/voyager_exports', controller: 'voyager/voyager_exports', action: :create

    as_an_end_user do
      should_be_forbidden [:post, :create]
    end

    as_an_agent do
      should_be_forbidden [:post, :create]
    end

    as_an_admin do
      describe 'with export configuration without whitelisted admin' do
        before do
          @request.account = account
          Zendesk::Export::Configuration.any_instance.
            expects(:whitelisted?).
            with(users(:minimum_admin_not_owner)).returns(false)
        end

        should_be_forbidden [:post, :create]
      end

      describe 'when creating a csv export' do
        before { statsd_client.expects(:increment).with('validate_limits') }

        it 'calls Voyager::API::Client#create_export' do
          export_params = {
              export_type: :tickets,
              format_type: :csv,
              start_time: @account.created_at.to_i,
              user_id: users(:minimum_admin_not_owner).id,
              legacy_metrics: nil,
              raw_export: true
          }
          statsd_client.expects(:increment).with('request.create', tags: ['response:201'])
          Voyager::API::Client.any_instance.expects(:create_export).with(export_params).returns(response_201)

          post :create
        end
      end

      describe 'when creating a json export' do
        before { statsd_client.expects(:increment).with('validate_limits') }

        it 'calls Voyager::API::Client#create_export' do
          export_params = {
              export_type: 'users',
              format_type: :json,
              start_time: @account.created_at.to_i,
              user_id: users(:minimum_admin_not_owner).id,
              raw_export: false
          }
          statsd_client.expects(:increment).with('request.create', tags: ['response:201'])
          Voyager::API::Client.any_instance.expects(:create_export).with(export_params).returns(response_201)

          post :create, params: { export_type: :users, export_format: :json }
        end
      end

      describe 'when trying to create a new export when one is already running' do
        before { statsd_client.expects(:increment).with('validate_limits') }

        it 'throws an error' do
          export_params = {
              export_type: :tickets,
              format_type: :csv,
              start_time: @account.created_at.to_i,
              user_id: users(:minimum_admin_not_owner).id,
              legacy_metrics: nil,
              raw_export: true
          }
          statsd_client.expects(:increment).with('request.create', tags: ['response:202'])
          Voyager::API::Client.any_instance.expects(:create_export).with(export_params).returns(response_202)

          post :create

          message = I18n.t('txt.admin.controllers.csv_exports_controller.job_already_submitted')

          assert_not_empty message
          assert_not_nil flash[:error]
          assert_equal message, flash[:error]
        end
      end

      describe 'when Voyager sends an unexpected status code' do
        before { statsd_client.expects(:increment).with('validate_limits') }

        it 'throws an error' do
          export_params = {
              export_type: :tickets,
              format_type: :csv,
              start_time: @account.created_at.to_i,
              user_id: users(:minimum_admin_not_owner).id,
              legacy_metrics: nil,
              raw_export: true
          }
          statsd_client.expects(:increment).with('request.create', tags: ['response:error'])
          Voyager::API::Client.any_instance.expects(:create_export).with(export_params).returns(response_error)

          post :create

          error_message = I18n.t('txt.admin.controllers.csv_exports_controller.generic_error')

          assert_not_empty error_message
          assert_not_nil flash[:error]
          assert_equal error_message, flash[:error]
        end
      end

      describe 'validate_limits filter' do
        let(:export_size_limit) { Voyager::VoyagerExportsController::EXPORT_SIZE_LIMITS['json'] }
        let(:export_range_limit) { Voyager::VoyagerExportsController::EXPORT_RANGE_LIMIT }

        before { statsd_client.expects(:increment).with('validate_limits') }

        describe 'with voyager_export_limits enabled' do
          before do
            Voyager::VoyagerExportsController.any_instance.expects(:export_limits_enabled?).returns(true)
            statsd_client.expects(:increment).with('validate_limits.enabled')
          end

          it 'blocks the export for big accounts' do
            Rails.cache.stubs(:fetch).returns(export_size_limit + 1_000)
            Voyager::API::Client.any_instance.expects(:create_export).never
            statsd_client.expects(:increment).with('validate_limits.check')
            statsd_client.expects(:increment).with('validate_limits.enforced')

            post :create, params: { export_type: :tickets, export_format: :json, start: (export_range_limit + 1).days.ago.to_i }

            assert_not_nil flash[:error]
            assert_equal(
              flash[:error],
              "The export limit was reached. Your account can export tickets in JSON format for only #{export_range_limit} days at a time."
            )
            Rails.cache.unstub
          end

          it 'works for big accounts that export within the date range limit' do
            Rails.cache.stubs(:fetch).returns(export_size_limit + 1_000)
            Voyager::API::Client.any_instance.expects(:create_export).returns(response_201)
            statsd_client.expects(:increment).with('validate_limits.check')
            statsd_client.expects(:increment).with('request.create', tags: ['response:201'])

            post :create, params: { export_type: :tickets, export_format: :json, start: (export_range_limit - 1).days.ago.to_i }

            assert_nil flash[:error]
            Rails.cache.unstub
          end

          it 'works for small accounts' do
            Rails.cache.stubs(:fetch).returns(export_size_limit - 1_000)
            Voyager::API::Client.any_instance.expects(:create_export).returns(response_201)
            statsd_client.expects(:increment).with('validate_limits.check')
            statsd_client.expects(:increment).with('request.create', tags: ['response:201'])

            post :create, params: { export_type: :tickets, export_format: :json, start: (export_range_limit + 1).days.ago.to_i }

            assert_nil flash[:error]
            Rails.cache.unstub
          end

          after { Voyager::VoyagerExportsController.unstub }
        end

        describe 'with voyager_export_limits disabled' do
          before { Voyager::VoyagerExportsController.any_instance.expects(:export_limits_enabled?).returns(false) }

          it 'works the export for big accounts' do
            Rails.cache.stubs(:fetch).returns(export_size_limit + 1_000)
            statsd_client.expects(:increment).with('request.create', tags: ['response:201'])
            Voyager::API::Client.any_instance.expects(:create_export).returns(response_201)

            post :create, params: { export_type: :tickets, export_format: :json, start: (export_range_limit + 1).days.ago.to_i }

            assert_nil flash[:error]
            Rails.cache.unstub
          end

          it 'works for others' do
            Rails.cache.stubs(:fetch).returns(export_size_limit - 1_000)
            statsd_client.expects(:increment).with('request.create', tags: ['response:201'])
            Voyager::API::Client.any_instance.expects(:create_export).returns(response_201)

            post :create, params: { export_type: :tickets, export_format: :json, start: (export_range_limit + 1).days.ago.to_i }

            assert_nil flash[:error]
            Rails.cache.unstub
          end

          after { Voyager::VoyagerExportsController.unstub }
        end
      end

      describe 'when creating a json export' do
        it 'calls Voyager::API::Client#create_export' do
          export_params = {
              export_type: 'users',
              format_type: :json,
              start_time: @account.created_at.to_i,
              user_id: users(:minimum_admin_not_owner).id,
              raw_export: false
          }
          Voyager::API::Client.any_instance.expects(:create_export).with(export_params).returns(response_201)

          post :create, params: { export_type: :users, export_format: :json }
        end
      end
    end

    as_an_owner do
      describe 'when creating an export' do
        it 'calls Voyager::API::Client#create_export' do
          export_params = {
              export_type: :tickets,
              format_type: :csv,
              start_time: 123,
              user_id: users(:minimum_admin).id,
              legacy_metrics: nil,
              raw_export: true
          }
          Voyager::API::Client.any_instance.expects(:create_export).with(export_params).returns(response_201)

          post :create, params: { start: 123 }
        end

        it 'calls Voyager::API::Client#create_export with default start_time' do
          export_params = {
              export_type: :tickets,
              format_type: :csv,
              start_time: @account.created_at.to_i,
              user_id: users(:minimum_admin).id,
              legacy_metrics: nil,
              raw_export: true
          }
          Voyager::API::Client.any_instance.expects(:create_export).with(export_params).returns(response_201)

          post :create
        end
      end
    end
  end

  def assert_correct_s3_headers
    assert_equal authorized_url_response.url, @response.headers['X-S3-Storage-Url']
    assert_equal Mime[:zip].to_s, @response.headers['Content-Type'].sub('; charset=utf-8', '')
    assert_equal "attachment; filename=\"#{authorized_url_response.filename}\"", @response.headers['Content-Disposition']
    assert_equal "/s3/#{authorized_url_response.filename}", @response.headers['X-Accel-Redirect']
    assert_equal "yes", @response.headers['X-Accel-Buffering']
  end
end
