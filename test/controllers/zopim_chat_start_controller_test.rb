require_relative '../support/test_helper'
require_relative '../support/zopim_test_helper'
require_relative '../support/suite_test_helper'

SingleCov.covered!

describe ZopimChatStartController do
  include SuiteTestHelper
  include ZopimTestHelper

  fixtures :users, :accounts, :role_settings

  before do
    ZopimIntegration.any_instance.stubs(:ensure_not_phase_four)
    ZBC::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)
  end

  with_options(controller: 'zopim_chat_start') do |request|
    request.should_route :get, '/zendeskchat/start', action: 'index'
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_agent do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }

    describe '#index without Zopim integration' do
      describe 'with voltron_chat disabled' do
        before do
          Account.any_instance.stubs(has_voltron_chat?: false)
          get :index
        end

        it 'redirects to sign up page' do
          assert_redirected_to ZopimChatRedirectionMixin::NO_ZOPIM_AGENT
        end
      end

      describe 'with voltron_chat enabled' do
        before do
          Account.any_instance.stubs(has_voltron_chat?: true)
          get :index
        end

        it 'redirects to new Zopim start page' do
          assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
        end
      end
    end

    describe '#index with Zopim phase one' do
      before do
        ZopimIntegration.any_instance.stubs(app_installed?: true)
        current_account.create_zopim_integration!(
          external_zopim_id: 1,
          zopim_key: 'zopim-key'
        )
      end

      describe 'with Zopim app installed and enabled' do
        before do
          ZopimIntegration.any_instance.stubs(app_enabled?: true)
        end

        describe 'with voltron_chat disabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: false)
            get :index
          end

          it 'redirects to Zopim app' do
            assert_redirected_to ZopimChatRedirectionMixin::ZOPIM_STANDALONE_PAGE
          end
        end

        describe 'with voltron_chat enabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: true)
            get :index
          end

          it 'redirects to new Zopim start page' do
            assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
          end
        end
      end

      describe 'without Zopim app enabled' do
        before do
          ZopimIntegration.any_instance.stubs(app_enabled?: false)
        end

        describe 'with voltron_chat disabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: false)
            get :index
          end

          it 'redirects to Zopim dashboard' do
            assert_redirected_to ZopimChatRedirectionMixin::ZOPIM_PHASE_ONE_APP_NOT_INSTALLED_OR_ENABLED
          end
        end

        describe 'with voltron_chat enabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: true)
            get :index
          end

          it 'redirects to new Zopim start page' do
            assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
          end
        end
      end

      describe 'without Zopim app installed' do
        before do
          ZopimIntegration.any_instance.stubs(app_installed?: false)
        end

        describe 'with voltron_chat disabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: false)
            get :index
          end

          it 'redirects to Zopim dashboard' do
            assert_redirected_to ZopimChatRedirectionMixin::ZOPIM_PHASE_ONE_APP_NOT_INSTALLED_OR_ENABLED
          end
        end

        describe 'with voltron_chat enabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: true)
            get :index
          end

          it 'redirects to new Zopim start page' do
            assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
          end
        end
      end
    end

    describe '#index with Zopim phase two' do
      before do
        Zopim::AgentSyncJob.stubs(:work)
        Account.any_instance.stubs(has_chat_permission_set?: true)
        ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(is_serviceable?: true)
        ZopimIntegration.any_instance.stubs(
          install_app: true,
          uninstall_app: true
        )

        ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.stubs(:enqueue)
        ZopimIntegration.stubs(where: stub(:first_or_create!))
        Zopim::Reseller.client.stubs(
          create_account!: Hashie::Mash.new(id: 1),
          account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
          create_account_agent!: stub(id: 1),
          find_accounts!: [],
          delete_account_agent!: true
        )

        setup_zopim_subscription(current_account)
      end

      describe 'with Zopim app installed and enabled' do
        before do
          ZopimIntegration.any_instance.stubs(app_installed?: true)
          ZopimIntegration.any_instance.stubs(app_enabled?: true)
        end

        describe 'with voltron_chat disabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: false)
            get :index
          end

          it 'redirects to Zopim app' do
            assert_redirected_to ZopimChatRedirectionMixin::ZOPIM_STANDALONE_PAGE
          end
        end

        describe 'with voltron_chat enabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: true)
            get :index
          end

          it 'redirects to new Zopim start page' do
            assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
          end
        end
      end

      describe 'without Zopim app installed' do
        before do
          ZopimIntegration.any_instance.stubs(app_installed?: false)
        end

        describe 'with voltron_chat disabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: false)
            get :index
          end

          it 'redirects to Zopim app landing page' do
            assert_redirected_to ZopimChatRedirectionMixin::ZOPIM_PHASE_TWO_APP_NOT_INSTALLED
          end
        end

        describe 'with voltron_chat enabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: true)
            get :index
          end

          it 'redirects to new Zopim start page' do
            assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
          end
        end
      end

      describe 'without Zopim app enabled' do
        before do
          ZopimIntegration.any_instance.stubs(app_installed?: true)
          ZopimIntegration.any_instance.stubs(app_enabled?: false)
        end

        describe 'with voltron_chat disabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: false)
            get :index
          end

          it 'redirects to Zopim app landing page' do
            assert_redirected_to ZopimChatRedirectionMixin::ZOPIM_PHASE_TWO_APP_NOT_INSTALLED
          end
        end

        describe 'with voltron_chat enabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: true)
            get :index
          end

          it 'redirects to new Zopim start page' do
            assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
          end
        end
      end
    end
  end

  as_an_admin do
    describe '#index with no Zopim integration' do
      describe 'with the Zopim app enabled' do
        before do
          ZopimIntegration.any_instance.stubs(:app_enabled?).returns(true)
        end

        describe 'with voltron_chat disabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: false)
            get :index
          end

          it 'redirects to Zopim app' do
            assert_redirected_to ZopimChatRedirectionMixin::ZOPIM_STANDALONE_PAGE
          end
        end

        describe 'with voltron_chat enabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: true)
            get :index
          end

          it 'redirects to new Zopim start page' do
            assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
          end
        end
      end

      describe 'with the Zopim app disabled' do
        before do
          ZopimIntegration.any_instance.stubs(:app_installed?).returns(true)
          ZopimIntegration.any_instance.stubs(:app_enabled?).returns(false)
        end

        describe 'with voltron_chat disabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: false)
            get :index
          end

          it 'redirects to admin error page' do
            assert_redirected_to ZopimChatRedirectionMixin::ZOPIM_APP_DISABLED_FOR_ADMIN
          end
        end

        describe 'with voltron_chat enabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: true)
            get :index
          end

          it 'redirects to new Zopim start page' do
            assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
          end
        end
      end

      describe 'without the Zopim app installed' do
        before do
          ZopimIntegration.any_instance.stubs(:app_installed?).returns(false)
          ZopimIntegration.any_instance.stubs(:app_enabled?).returns(false)
        end

        describe 'with voltron_chat disabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: false)
            get :index
          end

          it 'redirects to admin error page' do
            assert_redirected_to ZopimChatRedirectionMixin::NO_ZOPIM_ADMIN
          end
        end

        describe 'with voltron_chat enabled' do
          before do
            Account.any_instance.stubs(has_voltron_chat?: true)
            get :index
          end

          it 'redirects to new Zopim start page' do
            assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
          end
        end
      end
    end
  end
end
