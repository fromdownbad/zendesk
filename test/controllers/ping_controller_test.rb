require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe PingController do
  fixtures :accounts, :groups

  should_route :get, "/ping/shards.json", controller: "ping", action: "shards", format: 'json'
  should_route :get, "/ping/shards/all", controller: "ping", action: "shards", shard_id: "all"
  should_route :get, "/ping/shards/1",   controller: "ping", action: "shards", shard_id: "1"
  should_route :get, "/ping/redirect_to_account", controller: "ping", action: "redirect_to_account"
  should_route :get, "/ping/host", controller: "ping", action: "host"
  should_route :get, "/ping/remote_ip", controller: "ping", action: "remote_ip"

  before do
    # This can be removed when we're no longer simulating allowed parameters
    Arturo.stubs(:feature_enabled_for_pod?).
      with(:enable_simulated_allowed_parameters, 1).returns(true)
  end

  describe "#host" do
    it "responds with status ok" do
      get :host
      assert_response :ok
      assert_equal 'ok', response.body
    end
  end

  describe "#shards" do
    before do
      groups(:system_group).update_column(:id, -1)
      @controller.stubs(internal_request?: true)
    end

    describe 'for an external request' do
      before { @controller.stubs(internal_request?: false) }

      it "redirects to 404" do
        @controller.expects(:render_404_page).once
        get :shards
      end
    end

    describe 'for an internal request' do
      before { @controller.stubs(internal_request?: true) }

      it "responds with status ok" do
        get :shards
        assert_response :ok
      end

      it "doesn't hammer the database" do
        ActiveRecord::Base.expects(:on_all_shards).once

        get :shards
        get :shards
      end

      describe "default behavior" do
        it "reads from all shards" do
          ActiveRecord::Base.expects(:on_all_shards).once

          get :shards
          assert_response :ok
        end

        it "reads from the account DB" do
          account = stub
          account.expects(:touch).never
          Account.expects(:find).with(1).returns(account)

          get :shards
          assert_response :ok
        end
      end

      describe "when given an explicit shard_id" do
        it "reads from that shard" do
          ActiveRecord::Base.expects(:on_shard).with(123).once
          get :shards, params: { shard_id: "123" }
        end

        it "reads from all shards when shard_id is \"all\"" do
          ActiveRecord::Base.expects(:on_all_shards).once
          get :shards, params: { shard_id: "all" }
        end
      end

      describe "when passed a write parameter" do
        it "tests write when write parameter is true" do
          account = stub
          account.expects(:touch).once
          Account.expects(:find).with(1).returns(account)

          Group.any_instance.expects(:touch).once

          get :shards, params: { write: "true" }
        end

        it "successfully test records belonging to other accounts" do
          # Ensure loaded group does not belong to current account
          account = @request.account = accounts(:minimum)
          Group.where(account_id: account.id).delete_all
          assert Group.last.account != account
          Group.any_instance.expects(:touch).once

          get :shards, params: { write: "true" }
        end

        it "does not test write when write parameter is false" do
          account = stub
          account.expects(:touch).never
          Account.expects(:find).with(1).returns(account)
          Group.any_instance.expects(:touch).never

          get :shards, params: { write: "false" }
        end
      end
    end
  end

  describe "#remote_ip" do
    it "returns the remote_ip address" do
      request.remote_addr = '1.2.3.4'
      get :remote_ip
      response.body.must_equal '1.2.3.4'
    end
  end

  describe "#redirect_to_account" do
    before do
      @account = accounts(:minimum)
      @support = accounts(:support)
      @request.stubs(:user_agent).returns("Zendesk for iPhone 3.0") # This should work from mobile apps
    end

    describe 'from Z1' do
      before do
        @controller.stubs(current_account: @support)
      end

      describe 'to any account' do
        before do
          @subdomain = @account.subdomain
        end

        [
          'agent',
          'external_email_credentials/oauth_callback',
          'access/oauth?profile=facebook',
          'access/oauth?profile=twitter',
          'access/oauth?profile=twitter_readonly',
        ].each do |path|
          describe "with valid path #{path}" do
            before do
              @separator = path.include?('?') ? '&' : '?'
            end

            it "redirects to the account with path #{path}" do
              get :redirect_to_account, params: { state: "#{@subdomain}:#{path}", foo: "bar" }
              assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}foo=bar"
            end

            it "supports path #{path} with parameters" do
              get :redirect_to_account, params: { state: "#{@subdomain}:#{path}", yyy: "zzz", foo: "bar" }
              assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}foo=bar&yyy=zzz"
            end

            it "supports path #{path} starting with /" do
              get :redirect_to_account, params: { state: "#{@subdomain}:/#{path}" }
              assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}"
            end

            it "extract the csrf token and passed along with path #{path}" do
              get :redirect_to_account, params: { state: "#{@subdomain}:/#{path}:csrf" }
              assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}csrf_token=csrf"
            end

            describe 'with host-mapped account' do
              before do
                @account.stubs(:valid_cname_record?).returns(true)
                Account.any_instance.stubs(:host_mapping_available?).returns(true)
                Account.any_instance.stubs(:is_ssl_enabled?).returns(true)
                @account.host_mapping = "Foo.bar.com"
                @account.save!
              end

              it "redirects to the account with path #{path}" do
                get :redirect_to_account, params: { state: "#{@subdomain}:#{path}", foo: "bar" }
                assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}foo=bar"
              end

              it "supports path #{path} with parameters" do
                get :redirect_to_account, params: { state: "#{@subdomain}:#{path}", yyy: "zzz", foo: "bar" }
                assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}foo=bar&yyy=zzz"
              end

              it "supports path #{path} starting with /" do
                get :redirect_to_account, params: { state: "#{@subdomain}:/#{path}" }
                assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}"
              end

              it "extract the csrf token and passed along with path #{path}" do
                get :redirect_to_account, params: { state: "#{@subdomain}:/#{path}:csrf" }
                assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}csrf_token=csrf"
              end
            end
          end
        end

        describe "with invalid path" do
          before do
            @path = 'xxx'
          end

          it "logs redirecting subdomain as well as state subdomain and path" do
            Zendesk::StatsD::Client.any_instance.expects(:increment).with(
              @support.subdomain.to_s,
              tags: [
                "subdomain:#{@subdomain}",
                "path:#{@path}",
              ]
            )
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.PingController.redirect_to_account', anything).at_least(1)
            Zendesk::StatsD::Client.any_instance.stubs(:increment).with('mobile_v2', anything)

            get :redirect_to_account, params: { state: "#{@subdomain}:#{@path}" }
          end

          it "responds with forbidden" do
            get :redirect_to_account, params: { state: "#{@subdomain}:#{@path}" }
            assert_response :forbidden
          end
        end
      end

      describe "with invalid requests" do
        it "responds with bad_request status" do
          assert_raises(ActionController::ParameterMissing) do
            get :redirect_to_account, params: { foo: "bar" }
          end
        end
      end
    end

    describe 'from not Z1' do
      before do
        @controller.stubs(current_account: @account)
      end

      describe 'redirects to itself' do
        before do
          @subdomain = @account.subdomain
        end

        [
          'agent',
          'external_email_credentials/oauth_callback',
          'access/oauth?profile=facebook',
          'access/oauth?profile=twitter',
          'access/oauth?profile=twitter_readonly',
        ].each do |path|
          describe "with valid path #{path}" do
            before do
              @separator = path.include?('?') ? '&' : '?'
            end

            it "redirects to the account with path #{path}" do
              get :redirect_to_account, params: { state: "#{@subdomain}:#{path}", foo: "bar" }
              assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}foo=bar"
            end

            it "supports path #{path} with parameters" do
              get :redirect_to_account, params: { state: "#{@subdomain}:#{path}", yyy: "zzz", foo: "bar" }
              assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}foo=bar&yyy=zzz"
            end

            it "supports path #{path} starting with /" do
              get :redirect_to_account, params: { state: "#{@subdomain}:/#{path}" }
              assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}"
            end

            it "extract the csrf token and passed along with path #{path}" do
              get :redirect_to_account, params: { state: "#{@subdomain}:/#{path}:csrf" }
              assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}csrf_token=csrf"
            end

            describe 'with host-mapped account' do
              before do
                @account.stubs(:valid_cname_record?).returns(true)
                Account.any_instance.stubs(:is_ssl_enabled?).returns(true)
                Account.any_instance.stubs(:host_mapping_available?).returns(true)
                Zendesk::Routes::UrlGenerator.any_instance.stubs(:host_mapping_available?).returns(true)
                @account.host_mapping = "Foo.bar.com"
                @account.save!
              end

              it "redirects to the account with path #{path}" do
                get :redirect_to_account, params: { state: "#{@subdomain}:#{path}", foo: "bar" }
                assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}foo=bar"
              end

              it "supports path #{path} with parameters" do
                get :redirect_to_account, params: { state: "#{@subdomain}:#{path}", yyy: "zzz", foo: "bar" }
                assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}foo=bar&yyy=zzz"
              end

              it "supports path #{path} starting with /" do
                get :redirect_to_account, params: { state: "#{@subdomain}:/#{path}" }
                assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}"
              end

              it "extract the csrf token and passed along with path #{path}" do
                get :redirect_to_account, params: { state: "#{@subdomain}:/#{path}:csrf" }
                assert_redirected_to "https://#{@subdomain}.zendesk-test.com/#{path}#{@separator}csrf_token=csrf"
              end
            end
          end
        end

        describe "with invalid path" do
          before do
            @path = 'xxx'
          end

          it "logs redirecting subdomain as well as state subdomain and path" do
            Zendesk::StatsD::Client.any_instance.expects(:increment).with(
              @subdomain.to_s,
              tags: [
                "subdomain:#{@subdomain}",
                "path:#{@path}",
              ]
            )
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.PingController.redirect_to_account', anything).at_least(1)
            Zendesk::StatsD::Client.any_instance.stubs(:increment).with('mobile_v2', anything)

            get :redirect_to_account, params: { state: "#{@subdomain}:#{@path}" }
          end

          it "responds with forbidden" do
            get :redirect_to_account, params: { state: "#{@subdomain}:#{@path}" }
            assert_response :forbidden
          end
        end
      end

      describe 'redirects to another account' do
        before do
          @subdomain = @support.subdomain
        end

        it 'logs redirecting subdomain as well as state subdomain and path' do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(
            @account.subdomain.to_s,
            tags: [
              "subdomain:#{@subdomain}",
              "path:xxx",
            ]
          )
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.PingController.redirect_to_account', anything).at_least(1)
          Zendesk::StatsD::Client.any_instance.stubs(:increment).with('mobile_v2', anything)

          get :redirect_to_account, params: { state: "#{@subdomain}:xxx" }
        end

        describe "with any path" do
          before do
            @path = 'xxx'
          end

          it "responds with forbidden" do
            get :redirect_to_account, params: { state: "#{@subdomain}:#{@path}" }
            assert_response :forbidden
          end
        end
      end

      describe "with invalid requests" do
        it "responds with bad_request status" do
          assert_raises(ActionController::ParameterMissing) do
            get :redirect_to_account, params: { foo: "bar" }
          end
        end
      end
    end
  end

  describe "#version" do
    before do
      @current_account = accounts(:support)
      @controller.stubs(current_account: @current_account)
    end

    describe 'for an internal request' do
      before { @controller.stubs(internal_request?: true) }

      it "renders" do
        get :version
        assert_response :success
      end
    end

    describe 'for an external request' do
      before { @controller.stubs(internal_request?: false) }

      it "redirects to 404" do
        @controller.expects(:render_404_page).once
        get :version
      end
    end
  end
end
