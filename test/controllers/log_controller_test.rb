require_relative "../support/test_helper"

SingleCov.covered!

describe LogController do
  fixtures :accounts

  before do
    @request.account = @minimum_account = accounts(:minimum)
  end

  with_options(controller: "log") do |request|
    request.should_route :get, "/log/error", action: "error"
  end

  as_an_anonymous_user do
    describe "a GET to #error" do
      describe "with no message" do
        before { get :error }

        before_should "not call ZendeskExceptions::Logger.record" do
          ZendeskExceptions::Logger.expects(:record).never
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.LogController.error', anything).at_least(1)
        end

        it('responds with ok') { assert_response :ok }
      end

      describe "with a message" do
        before { get :error, params: { message: "test" } }

        before_should "call ZendeskExceptions::Logger.record" do
          ZendeskExceptions::Logger.expects(:record).once.with do |exception, params|
            assert_equal(LogController::JavaScriptError, exception.class)
            assert_equal("test", params[:message])
            assert_equal(@controller, params[:location])
          end

          Zendesk::StatsD::Client.any_instance.expects(:increment).once.with(
            'javascript_error', tags: ["exception_class:log_controller/java_script_error"]
          )
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.LogController.error', anything).at_least(1)
        end

        it('responds with ok') { assert_response :ok }
      end

      describe "with a message, via web.archive.org" do
        before do
          set_header('HTTP_VIA', "HTTP/1.0 web.archive.org (Wayback Save Page)")
          get :error, params: { message: "test" }
        end

        before_should "not call ZendeskExceptions::Logger.record" do
          ZendeskExceptions::Logger.expects(:record).never.
            with(@controller, "test", instance_of(LogController::JavaScriptError))

          Zendesk::StatsD::Client.any_instance.expects(:increment).never.with(
            'javascript_error', tags: ["exception_class:log_controller/java_script_error"]
          )
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.LogController.error', anything).at_least(1)
        end

        it('responds with ok') { assert_response :ok }
      end
    end
  end
end
