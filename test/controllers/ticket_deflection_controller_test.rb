require_relative '../support/test_helper'

SingleCov.covered!

describe TicketDeflectionController do
  fixtures :tickets, :users, :accounts, :brands, :ticket_deflections

  let(:account) { accounts(:minimum) }
  let(:article_id) { 2233445566 }
  let(:article_locale) { 'en-us' }
  let(:ticket) { tickets(:minimum_1) }
  let(:brand) { brands(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:article_content) { { id: 1, title: 'title', html_url: 'http://whatever', body: 'This is so rad bro.'} }
  let(:ticket_deflection) { ticket_deflections(:minimum_ticket_deflection) }
  let(:article_info) { { 'article_id' => article_id, 'score' => 0.192819, 'locale' => article_locale } }
  let(:ticket_deflection_article) { TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info) }
  let(:base_time) { Time.utc(2016, 3, 31) }
  let(:decrypted_auth_token) do
    {
      account_id: account.id,
      user_id: ticket.requester_id,
      ticket_id: ticket.nice_id,
      deflection_id: ticket_deflection.id,
      articles: [article_id],
      token: nil,
      exp: 30.days.from_now.to_i
    }
  end
  let(:encrypted_auth_token) { AutomaticAnswersJwtToken.encrypted_token(account.id, ticket.requester_id, ticket.nice_id, ticket_deflection.id, [article_id]) }

  let(:articles) do
    [
      { id: 2233445566, title: 'title', url: 'http:whatever/82', html_url: 'http://whatever/82', body: 'I definitely used way more glue in the first five years of my life than I have in all the time since then.' },
      { id: 91, title: 'title', url: 'http:whatever/91', html_url: 'http://whatever/91', body: "Vinyl records don't have grooves. They have one groove that spirals inward. They have groove." },
      { id: 32, title: 'title', url: 'http:whatever/32', html_url: 'http://whatever/32', body: "I'd update my software much more often if prompted to when exiting the program rather than opening it." }
    ]
  end

  let(:automatic_answer_send) do
    send_event = AutomaticAnswerSend.new
    send_event.suggested_articles = articles
    send_event
  end

  before do
    Timecop.freeze(base_time)
    Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_article).
      with(as_id(article_id), article_locale).returns(article_content)

    ticket_deflection.ticket_deflection_articles.destroy_all
    ticket_deflection_article.save!
    @request.account = @current_account = accounts(:minimum)
    @controller.stubs(:current_brand).returns(@current_account.brands.first)
    @token = encrypted_auth_token
    Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)
  end

  describe 'routing' do
    describe '#article' do
      it 'routes to the article action' do
        options = { controller: 'ticket_deflection',
                    action: 'article',
                    article_id: '20',
                    format: 'html',
                    'auth_token' => 'this.needs.to.use.dots'}
        extras = { 'auth_token' => 'this.needs.to.use.dots' }
        assert_routing({method: 'get', path: '/requests/automatic-answers/ticket/article/20'},
          options,
          {},
          extras)
      end
    end

    describe '#solve_ticket_and_redirect' do
      it 'routes to solve_ticket_and_redirect' do
        options = { controller: 'ticket_deflection',
                    action: 'solve_ticket_and_redirect',
                    article_id: '20',
                    format: 'html',
                    'auth_token' => 'this.needs.to.use.dots'}
        extras = { 'auth_token' => 'this.needs.to.use.dots' }
        assert_routing({method: 'get', path: '/requests/automatic-answers/ticket/solve/article/20'},
          options,
          {},
          extras)
      end
    end

    describe '#article_feedback_redirect' do
      describe 'when auth_token is present' do
        it 'routes to article_feedback_redirect' do
          options = { controller: 'ticket_deflection',
                      action: 'article_feedback_redirect',
                      article_id: '20',
                      format: 'html',
                      'auth_token' => 'this.needs.to.use.dots'}
          extras = { 'auth_token' => 'this.needs.to.use.dots' }
          assert_routing({method: 'get', path: '/requests/automatic-answers/ticket/feedback/article/20'},
            options,
            {},
            extras)
        end
      end
    end
  end

  describe 'when ticket_deflection is not available' do
    before do
      Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(false)
      get :article, params: { article_id: article_id, auth_token: encrypted_auth_token }
    end

    it 'renders not_found' do
      assert_response :not_found
    end
  end

  describe 'GET /article' do
    before do
      ticket.will_be_saved_by(User.system)
      ticket.audit.events << automatic_answer_send
      ticket.save!
    end

    describe 'success' do
      before do
        @controller.expects(:valid_auth_token?).returns(true)
        Timecop.freeze(base_time)
      end

      it 'is a redirect' do
        get :article, params: { article_id: article_id, auth_token: encrypted_auth_token }
        assert_response :redirect
      end

      it 'redirects to the help center article' do
        get :article, params: { article_id: article_id, auth_token: encrypted_auth_token }
        assert_equal 'http://whatever', response.redirect_url.split('?').first
      end

      describe '#fragment' do
        let(:url) { URI.parse(response.redirect_url) }
        let(:query) { Rack::Utils.parse_nested_query(url.query) }

        it 'adds auth_token as param to the help center url' do
          get :article, params: { article_id: article_id, auth_token: encrypted_auth_token }
          assert_equal encrypted_auth_token, query['auth_token']
        end
      end
    end

    describe 'failure' do
      describe 'when token is valid format' do
        it 'when ticket_deflection_article is not found' do
          get :article, params: { article_id: 1, auth_token: encrypted_auth_token }

          assert_response :not_found
        end

        it 'when help center does not have article' do
          Zendesk::HelpCenter::InternalApiClient.any_instance.
            stubs(:fetch_article).with(as_id(article_id), article_locale).returns(nil)

          get :article, params: { article_id: 2233445566, auth_token: encrypted_auth_token }

          assert_response :not_found
        end

        it 'when ticket is not found' do
          ticket.delete
          get :article, params: { article_id: article_id, auth_token: encrypted_auth_token }

          assert_response :not_found
        end
      end

      describe 'when token is not valid' do
        let(:encrypted_auth_token) { 'some.shady.shifter' }
        it 'is forbidden from the page' do
          get :article, params: { article_id: article_id, auth_token: encrypted_auth_token }

          assert_response :forbidden
        end
      end
    end

    describe 'logging' do
      let(:statsd_client_stub) { stub_for_statsd }

      it 'the statsd client is incremented with article_viewed' do
        TicketDeflectionController.any_instance.stubs(:statsd_client).returns(statsd_client_stub)
        statsd_client_stub.expects(:increment).
          with('article_viewed', tags: ['subdomain:minimum', 'channel:email']).once

        get :article, params: { auth_token: encrypted_auth_token, article_id: article_id }
      end
    end
  end

  describe 'GET /solve_ticket_and_redirect' do
    let(:subdomain) { account.subdomain }
    let(:channel) { 'email' }
    let(:ticket_deflector) { mock('ticket_deflector') }

    before do
      TicketDeflector.
        stubs(:new).
        with(account: account, deflection: ticket.ticket_deflection, resolution_channel_id: ViaType.MAIL).
        returns(ticket_deflector)
    end

    it 'calls TicketDeflector to solve ticket with article_id' do
      ticket_deflector.
        expects(:solve).
        with(has_key('article_id')).
        with(has_value(as_id(article_id)))

      get :solve_ticket_and_redirect, params: { auth_token: encrypted_auth_token, article_id: article_id }
    end

    describe 'redirection behaviour' do
      before do
        ticket_deflector.
          stubs(:solve).
          with(has_key('article_id')).
          with(has_value(as_id(article_id))).
          returns(true)

        get :solve_ticket_and_redirect, params: { auth_token: encrypted_auth_token, article_id: article_id }
      end

      it 'redirects to the help center article' do
        assert_response :redirect
        assert_equal 'http://whatever', response.redirect_url.split('?').first
      end

      describe 'redirection url query parameters include' do
        let(:url) { URI.parse(response.redirect_url) }
        let(:query) { Rack::Utils.parse_nested_query(url.query) }

        it 'returns a solved value of 1' do
          assert_equal '1', query['solved']
        end

        it 'adds auth_token as param to the help center url' do
          assert_equal encrypted_auth_token, query['auth_token']
        end
      end
    end
  end

  describe 'GET /article_feedback_redirect' do
    let(:ticket_deflector) { mock('ticket_deflector') }

    before do
      TicketDeflector.
        stubs(:new).
        with(account: account, deflection: ticket.ticket_deflection, resolution_channel_id: ViaType.MAIL).
        returns(ticket_deflector)

      ticket_deflector.stubs(:process_deflection_rejection)
    end

    it 'JWT token is being decoded correctly' do
      @controller.expects(:valid_auth_token?).returns(true)
      get :article_feedback_redirect, params: { auth_token: encrypted_auth_token, article_id: article_id }
    end

    it 'calls TicketDeflector to process irrelevant article' do
      ticket_deflector.
        expects(:process_deflection_rejection).
        with(has_key('article_id')).
        with(has_value(as_id(article_id)))

      get :article_feedback_redirect, params: { auth_token: encrypted_auth_token, article_id: article_id }
    end

    describe 'redirection behaviour' do
      before do
        get :article_feedback_redirect, params: { auth_token: encrypted_auth_token, article_id: article_id }
      end

      it 'redirects to the help center article' do
        assert_response :redirect
        assert_equal 'http://whatever', response.redirect_url.split('?').first
      end

      describe 'redirection url query parameters include' do
        let(:url) { URI.parse(response.redirect_url) }
        let(:query) { Rack::Utils.parse_nested_query(url.query) }

        it 'returns a article_feedback value of 1' do
          assert_equal '1', query['article_feedback']
        end

        it 'adds auth_token as param to the help center url' do
          assert_equal encrypted_auth_token, query['auth_token']
        end
      end
    end

    describe 'an event' do
      let(:statsd_client_stub) { stub_for_statsd }

      it 'is logged' do
        TicketDeflectionController.any_instance.stubs(:statsd_client).returns(statsd_client_stub)
        statsd_client_stub.expects(:increment).
          with('article_feedback', tags: ['subdomain:minimum', 'channel:email']).once

        get :article_feedback_redirect, params: { auth_token: encrypted_auth_token, article_id: article_id }
      end
    end
  end

  describe '#url_with_query_params' do
    let(:url_existing_query) { 'https://www.whatever.com/hc/article/123?thing=true' }
    let(:url_empty_query) { 'https://www.whatever.com/hc/article/123' }
    let(:auth_token) { 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9' }
    let(:query_params) { { auth_token: auth_token, solved: 1 } }
    let(:controller) { TicketDeflectionController.new }

    describe 'given a url with existing query params' do
      let(:expected_url) { "https://www.whatever.com/hc/article/123?thing=true&auth_token=#{auth_token}&solved=1" }

      it 'appends the given query params to the url' do
        actual_url = controller.send(:url_with_query_params, url_existing_query, query_params)

        assert_equal expected_url, actual_url
      end
    end

    describe 'given a url without query params' do
      let(:expected_url) { "https://www.whatever.com/hc/article/123?auth_token=#{auth_token}&solved=1" }

      it 'appends the given query params to the url' do
        actual_url = controller.send(:url_with_query_params, url_empty_query, query_params)

        assert_equal expected_url, actual_url
      end
    end
  end
end
