require_relative "../../support/test_helper"

SingleCov.covered!

describe AutomaticAnswers::AgentFeedbackController do
  fixtures :tickets, :users, :accounts, :brands, :ticket_deflections
  let(:ticket) { tickets(:minimum_1) }
  let(:user) { users(:minimum_agent) }
  let(:brand) { brands(:minimum) }
  let(:article_id) { '2233445566' }
  let(:article_content) { { id: 1, title: 'title', html_url: 'http://whatever', body: 'This is so rad bro.'} }
  let(:ticket_deflection) { ticket_deflections(:minimum_ticket_deflection) }
  let(:article_info) { { 'article_id' => article_id, 'score' => 0.192819, 'locale' => 'en-us' } }
  let(:ticket_deflection_article) { TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info) }
  let(:updated_at) { Time.now }
  let(:base_time) { Time.utc(2016, 3, 31) }

  before do
    Timecop.freeze(base_time)
    Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_articles_for_ids_and_locales).with(article_id, 'en-us').returns([article_content])

    ticket_deflection.ticket_deflection_articles.destroy_all
    ticket_deflection_article.save!
    @request.account = @current_account = accounts(:minimum)
  end

  describe 'POST /agent/feedback' do
    describe 'success' do
      before do
        @controller.expects(:authenticate_user).returns(true)
        @controller.stubs(:current_user).returns(user)
      end

      describe 'a ticket without a ticket_deflection' do
        before do
          ticket.ticket_deflection.destroy
          post :agent_feedback, params: { ticket_id: ticket.nice_id, article_id: article_id, irrelevant: false }
        end

        it 'returns :no_content' do
          assert_response :not_found
        end
      end

      describe 'mark as irrelevant' do
        before do
          refute ticket_deflection_article.irrelevant_by_agent
          assert_equal TicketDeflectionArticle::REASON_UNKNOWN, ticket_deflection_article.irrelevant_reason_by_agent
          post :agent_feedback, params: { ticket_id: ticket.nice_id, article_id: article_id, irrelevant: true }

          ticket_deflection_article.reload
        end

        it 'returns 200' do
          assert_response :ok
        end

        it 'marks irrelevant_by_agent to true' do
          assert ticket_deflection_article.irrelevant_by_agent
        end

        it 'marks irrelevant_reason_by_agent to REASON_IRRELEVANT' do
          assert_equal TicketDeflectionArticle::REASON_IRRELEVANT, ticket_deflection_article.irrelevant_reason_by_agent
        end

        it 'updates irrelevant_by_agent_updated_at to freezed time' do
          assert_equal updated_at, ticket_deflection_article.irrelevant_by_agent_updated_at
        end
      end

      describe 'unmark as irrelevant' do
        before do
          ticket_deflection_article.update_attributes(
            irrelevant_by_agent: true,
            irrelevant_reason_by_agent: TicketDeflectionArticle::REASON_IRRELEVANT
          )
          ticket_deflection_article.save!
          assert ticket_deflection_article.irrelevant_by_agent
          assert_equal TicketDeflectionArticle::REASON_IRRELEVANT, ticket_deflection_article.irrelevant_reason_by_agent
          assert_nil ticket_deflection_article.irrelevant_by_agent_updated_at
          post :agent_feedback, params: { ticket_id: ticket.nice_id, article_id: article_id, irrelevant: 'false' }
          ticket_deflection_article.reload
        end

        it 'returns 200' do
          assert_response :ok
        end

        it 'marks irrelevant_by_agent to false' do
          refute ticket_deflection_article.irrelevant_by_agent
        end

        it 'marks irrelevant_reason_by_agent to REASON_UNKNOWN' do
          assert_equal TicketDeflectionArticle::REASON_UNKNOWN, ticket_deflection_article.irrelevant_reason_by_agent
        end

        it 'updates irrelevant_by_agent_updated_at' do
          assert_equal updated_at, ticket_deflection_article.irrelevant_by_agent_updated_at
        end
      end
    end

    describe 'failure' do
      before do
        @controller.expects(:authenticate_user).returns(true)
      end

      describe 'not an agent' do
        before do
          @controller.stubs(:current_user).returns(users(:minimum_end_user))
          post :agent_feedback, params: { ticket_id: ticket.nice_id, article_id: article_id, irrelevant: true }
        end

        it 'returns 401' do
          assert_response 401
        end
      end

      describe 'missing parameters' do
        before do
          @controller.stubs(:current_user).returns(user)
        end

        describe 'missing :ticket_id' do
          before do
            post :agent_feedback, params: { article_id: article_id, irrelevant: true }
          end

          it 'returns :unprocessable_entity' do
            assert_response :unprocessable_entity
          end
        end

        describe 'missing :article_id' do
          before do
            post :agent_feedback, params: { ticket_id: ticket.nice_id, irrelevant: true }
          end

          it 'returns :unprocessable_entity' do
            assert_response :unprocessable_entity
          end
        end

        describe 'missing :irrelevant' do
          before do
            post :agent_feedback, params: { ticket_id: ticket.nice_id, article_id: article_id }
          end

          it 'returns :unprocessable_entity' do
            assert_response :unprocessable_entity
          end
        end
      end

      describe 'wrong parameter type' do
        before do
          @controller.stubs(:current_user).returns(user)
        end

        describe ':ticket_id is not integer' do
          before do
            post :agent_feedback, params: { ticket_id: 'abc' }
          end

          it 'returns :unprocessable_entity' do
            assert_response :unprocessable_entity
          end
        end

        describe ':article_id is not integer' do
          before do
            post :agent_feedback, params: { article_id: 'abc' }
          end

          it 'returns :unprocessable_entity' do
            assert_response :unprocessable_entity
          end
        end

        describe ':irrelevant is not boolean' do
          before do
            post :agent_feedback, params: { irrelevant: 'blah' }
          end

          it 'returns :unprocessable_entity' do
            assert_response :unprocessable_entity
          end
        end
      end
    end

    describe 'not authenticated' do
      before do
        post :agent_feedback, params: { ticket_id: ticket.nice_id, article_id: article_id, irrelevant: true }
      end

      it 'returns :unauthorized' do
        assert_response :unauthorized
      end
    end
  end
end
