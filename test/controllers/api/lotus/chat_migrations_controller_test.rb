require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::ChatMigrationsController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :role_settings

  let(:account) { accounts(:minimum) }

  before do
    accept :json
    use_ssl
  end

  with_options(controller: 'api/lotus/chat_migrations') do |request|
    request.should_route :get, '/api/lotus/chat_migrations/departments/options', action: 'departments_options'
    request.should_route :post, '/api/lotus/chat_migrations/departments', action: 'departments_migrate'
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :departments_options]
    should_be_unauthorized [:post, :departments_migrate]
  end

  as_an_end_user do
    should_be_forbidden [:get, :departments_options]
    should_be_forbidden [:post, :departments_migrate]
  end

  as_an_agent do
    should_be_forbidden [:get, :departments_options]
    should_be_forbidden [:post, :departments_migrate]
  end

  as_an_admin do
    let(:json_response) { JSON.parse(response.body) }

    describe '#departments_options' do
      before do
        api_response = mock_json_response(options: ['rename'])
        Zopim::InternalApiClient.any_instance.expects(:get_migration_options).with(:departments).returns(api_response)
        get :departments_options
      end

      it 'sets the correct status' do
        assert_response :ok
      end

      it 'sets the client response' do
        expected_body = { 'options' => ['rename'] }
        assert_equal expected_body, json_response
      end
    end

    describe '#departments_migrate' do
      describe 'without required parameter' do
        before do
          Zopim::InternalApiClient.any_instance.expects(:start_migration).never
          post :departments_migrate
        end

        it 'sets the correct status' do
          assert_response :bad_request
        end
      end

      describe 'with required parameter' do
        before do
          api_response = mock_json_response({}, status: 202)
          Zopim::InternalApiClient.any_instance.expects(:start_migration).with(:departments, option: 'rename').returns(api_response)
          post :departments_migrate, params: { option: 'rename' }
        end

        it 'sets the correct status' do
          assert_response :accepted
        end
      end
    end
  end

  def mock_json_response(body, status: 200)
    stub('response', body: body.to_json, status: status)
  end
end
