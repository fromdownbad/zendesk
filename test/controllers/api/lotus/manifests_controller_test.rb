require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::Lotus::ManifestsController do
  extend Api::V2::TestHelper

  fixtures :accounts, :users

  before do
    accept :json
  end

  describe '#show' do
    as_an_agent do
      before do
        manifest_manager = mock('manifest_manager_mock')
        asset_manifest = mock('asset_manifest_mock')
        asset_manifest.stubs(:manifest).returns(hello: 'world')
        manifest_manager.stubs(:find).returns(asset_manifest)
        ::Lotus::ManifestManager.stubs(:new).returns(manifest_manager)

        get :show, params: { id: :current }
      end

      it 'makes call to get a manifest' do
        expected = { hello: 'world' }.to_json
        assert_equal expected, response.body
      end
    end
  end

  describe '#versions' do
    let(:asset_manifests) { [{}, {}] }

    as_an_agent do
      before do
        manifest_manager = mock('manifest_manager_mock')
        manifest_manager.stubs(:versions).returns(asset_manifests)
        ::Lotus::ManifestManager.stubs(:new).returns(manifest_manager)
      end

      describe 'when on an authorized integration network' do
        before { @request.env['REMOTE_ADDR'] = '127.0.0.1' }

        it 'correctly returns the correct asset manifests' do
          get :versions
          assert_equal asset_manifests.to_json, response.body
        end
      end

      describe 'when not on an authorized integration network' do
        before { get :versions }
        should_be_forbidden :versions
      end
    end
  end
end
