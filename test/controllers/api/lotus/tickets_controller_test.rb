require_relative "../../../support/test_helper"
require_relative "../../../support/archive_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::Lotus::TicketsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: "api/lotus/tickets") do |request|
    request.should_route :get, "/api/lotus/tickets/recent", action: "recent"
  end

  describe 'with an oauth token' do
    before do
      @ticket_id = tickets(:minimum_1).nice_id
      @many_ids = [tickets(:minimum_1).nice_id, tickets(:minimum_2).nice_id].join(",")
      @owner = accounts(:minimum).owner
      Arturo.enable_feature! :new_oauth_scopes_requirements
    end

    with_scopes('tickets:read') do
      should_be_authorized { get :recent }
    end

    with_scopes('tickets:write') do
      should_not_be_authorized { get :recent }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :recent
  end

  as_an_end_user do
    should_be_forbidden :recent
  end

  as_an_admin do
    describe "a GET to :recent" do
      before { get :recent }
      should_use_presenter Api::Lotus::RecentTicketPresenter, with: :recent_tickets
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end
  end

  as_an_agent do
    describe "a GET to :recent" do
      before { get :recent }
      should_use_presenter Api::Lotus::RecentTicketPresenter, with: :recent_tickets
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end
  end
end
