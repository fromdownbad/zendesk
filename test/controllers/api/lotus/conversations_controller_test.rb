require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::Lotus::ConversationsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :users, :tickets, :events, :role_settings

  let(:ticket) { tickets(:minimum_3) }
  let(:comment) { ticket.comments.first }
  let(:suggested_articles) do
    [
      { id: 1, url: 'http://helpcenter.article/1.json', html_url: 'http://helpcenter.article/1', title: 'article title 1' },
      { id: 2, url: 'http://helpcenter.article/2.json', html_url: 'http://helpcenter.article/2', title: 'article title 2' },
      { id: 3, url: 'http://helpcenter.article/3.json', html_url: 'http://helpcenter.article/3', title: 'article title 3' }
    ]
  end

  let(:automatic_answer_send) do
    AutomaticAnswerSend.new.tap do |event|
      event.suggested_articles = suggested_articles
      event.created_at = 1.day.ago
    end
  end

  let(:automatic_answer_solve) do
    AutomaticAnswerSolve.new.tap do |event|
      event.solved_article = suggested_articles.first
      event.created_at = 1.week.ago
    end
  end

  let(:collab_thread_created) do
    CollabThreadCreated.new.tap do |event|
      event.subject = "Help?"
      event.thread_id = "Thread-1"
      event.recipient_count = 2
      event.created_at = 1.month.ago
    end
  end

  around do |test|
    Timecop.freeze(Time.now) do
      test.call
    end
  end

  before do
    accept :json
    use_ssl

    ticket.will_be_saved_by(User.system)
    ticket.audit.events << automatic_answer_send
    ticket.audit.events << automatic_answer_solve
    ticket.audit.events << collab_thread_created
    ticket.save!
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index, ticket_id: 1]
    should_be_unauthorized [:get, :show, ticket_id: 1, id: 1]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index, ticket_id: 1]
    should_be_forbidden [:get, :show, ticket_id: 1, id: 1]
  end

  as_an_agent do
    describe "a GET to :index" do
      describe "basic" do
        before { get :index, params: { ticket_id: ticket.nice_id } }
        should_use_presenter Api::Lotus::ConversationItemPresenter

        it "sets cache headers based on ticket" do
          etag = @response.headers["ETag"]
          last = Time.parse(@response.headers["Last-Modified"])
          assert_equal Time.parse(ticket.updated_at.to_s), last
          assert etag.present?
        end

        it "retrieves auto answer send events" do
          conversation = JSON.parse(@response.body)["conversations"]
          auto_answer_send_event = conversation.find { |part| part["type"] == "AutomaticAnswerSend" }
          refute_nil auto_answer_send_event
        end

        it "retrieves auto answer solve events" do
          conversation = JSON.parse(@response.body)["conversations"]
          auto_answer_solve_event = conversation.find { |part| part["type"] == "AutomaticAnswerSolve" }
          refute_nil auto_answer_solve_event
        end

        it "retrieves collaboration thread created events" do
          conversation = JSON.parse(@response.body)["conversations"]
          collab_thread_created_event = conversation.find { |part| part["type"] == "CollabThreadCreated" }
          refute_nil collab_thread_created_event
        end
      end

      describe "changing the sort_order" do
        it "does not break cache if given nil and asc sort_order" do
          get :index, params: { ticket_id: ticket.nice_id }
          etag1 = @response.headers["ETag"]
          get :index, params: { ticket_id: ticket.nice_id, sort_order: "asc" }
          etag2 = @response.headers["ETag"]
          assert_equal etag1, etag2
        end
      end

      describe "with a sorting preference" do
        let(:extra_params) { {} }

        before do
          get :index, params: extra_params.merge(ticket_id: ticket.nice_id, sort_order: "asc")
          @asc_response = JSON.parse(@response.body).fetch('conversations')

          get :index, params: extra_params.merge(ticket_id: ticket.nice_id, sort_order: "desc")
          @desc_response = JSON.parse(@response.body).fetch('conversations')
        end

        describe 'with standard pagination' do
          it "honors the desired order" do
            assert(
              Time.parse(@asc_response.first['created_at']) < Time.parse(@asc_response.last['created_at']),
              'expected `created_at` to ascend for `sort_order=asc`'
            )

            assert(
              Time.parse(@desc_response.first['created_at']) > Time.parse(@desc_response.last['created_at']),
              'expected `created_at` to descend for `sort_order=desc`'
            )
          end

          it "maintains mirrored ordering" do
            assert_equal(
              @asc_response,
              @desc_response.reverse
            )
          end
        end

        describe 'with cursor pagination' do
          let(:extra_params) { { cursor: nil } }

          it "honors the desired order" do
            assert(
              Time.parse(@asc_response.first['created_at']) < Time.parse(@asc_response.last['created_at']),
              'expected `created_at` to ascend for `sort_order=asc`'
            )

            assert(
              Time.parse(@desc_response.first['created_at']) > Time.parse(@desc_response.last['created_at']),
              'expected `created_at` to descend for `sort_order=desc`'
            )
          end

          it "maintains mirrored ordering" do
            assert_equal(
              @asc_response,
              @desc_response.reverse
            )
          end
        end
      end

      describe "pagination" do
        let(:response) { JSON.parse(@response.body) }

        describe "with no cursor parameter" do
          before do
            get :index, params: { ticket_id: ticket.nice_id }
          end

          it "uses offset pagination" do
            assert_includes response, 'next_page'
            assert_includes response, 'previous_page'
            assert_includes response, 'count'

            refute_includes response, 'after_url'
            refute_includes response, 'before_url'
            refute_includes response, 'after_cursor'
            refute_includes response, 'before_cursor'
          end
        end

        describe "with a cursor parameter" do
          before do
            get :index, params: { ticket_id: ticket.nice_id, cursor: nil }
          end

          it "uses cursor pagination" do
            refute_includes response, 'next_page'
            refute_includes response, 'previous_page'
            refute_includes response, 'count'

            assert_includes response, 'after_url'
            assert_includes response, 'before_url'
            assert_includes response, 'after_cursor'
            assert_includes response, 'before_cursor'
          end
        end
      end

      describe "with exclude_chat param" do
        let(:entitlement) { stub }
        let(:chat_entitlement) { nil }

        before do
          Zendesk::SupportUsers::Entitlement.stubs(:for).returns(entitlement)
          entitlement.stubs(:role).returns(chat_entitlement)
          comment = ticket.add_comment(
            body: "from chat",
            is_public: false,
            author_id: ticket.requester_id,
            via_id: ViaType.CHAT,
            via_reference_id: 0
          )
          comment.save!
          get :index, params: { ticket_id: ticket.nice_id, exclude_chat: exclude_chat }
          conversation_items = JSON.parse(@response.body)["conversations"]
          @conversation_item_ids = conversation_items.map { |e| e["id"] }
        end

        # Despite these tests being written for the controller, they execute presenter code.
        # A follow-up JIRA has been created to re-assess these tests before Lychee GAs:
        # https://zendesk.atlassian.net/browse/IRIS-577
        describe_with_arturo_enabled(:polaris) do
          before { ticket.account.settings.polaris = true } # 'polaris' is a capability depending on arturo and setting

          describe "when exclude_chat is set to true" do
            let(:exclude_chat) { true }

            describe "when agent has chat entitlements" do
              let(:chat_entitlement) { 'agent' }

              it "does NOT return chat comments" do
                refute_includes @conversation_item_ids, comment.id
              end
            end

            describe "when agent does NOT have chat entitlements" do
              it "returns chat comments" do
                assert_includes @conversation_item_ids, comment.id
              end
            end
          end

          describe "when exclude_chat is set to false" do
            let(:exclude_chat) { false }

            it "returns chat comments" do
              assert_includes @conversation_item_ids, comment.id
            end
          end
        end

        # Despite these tests being written for the controller, they execute presenter code.
        # A follow-up JIRA has been created to re-assess these tests before Lychee GAs:
        # https://zendesk.atlassian.net/browse/IRIS-577
        describe_with_arturo_disabled(:polaris) do
          before { ticket.account.settings.polaris = false } # 'polaris' is a capability depending on arturo and setting

          describe "when exclude_chat is set to true" do
            let(:exclude_chat) { true }

            describe "when agent has chat entitlements" do
              let(:chat_entitlement) { 'agent' }

              it "does NOT return chat comments" do
                refute_includes @conversation_item_ids, comment.id
              end
            end

            describe "when agent does NOT have chat entitlements" do
              it "returns chat comments" do
                assert_includes @conversation_item_ids, comment.id
              end
            end
          end

          describe "when exclude_chat is set to false" do
            let(:exclude_chat) { false }

            it "returns chat comments" do
              assert_includes @conversation_item_ids, comment.id
            end
          end
        end
      end

      describe "bad events" do
        let(:ticket) { tickets(:minimum_3) }
        let(:author) { ticket.account.users.first }

        describe "with bad parent events" do
          let(:sql) do
            "INSERT INTO `events` (`id`, `account_id`, `parent_id`, `ticket_id`," \
            " `author_id`, `created_at`, `via_id`, `via_reference_id`, `is_public`," \
            " `type`, `value`, `value_previous`, `value_reference`, `updated_at`," \
            " `notification_sent_at`) VALUES" \
            "(12302, 0, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL)," \
            "(12303, 0, 12302, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL)," \
            "(12304, #{ticket.account.id}, 12302, #{ticket.id}, #{author.id}, '2018-06-27 21:14:18'," \
            " 0, NULL, 0, 'Comment', 'A comment', 'rich', NULL, '2018-06-27 21:14:18', NULL)"
          end

          before do
            Event.without_arsi.delete_all
            ActiveRecord::Base.connection.execute(sql)

            get :index, params: { ticket_id: ticket.nice_id }
          end

          it { assert_response 200 }

          it "renders the comment" do
            conversation = JSON.parse(@response.body)["conversations"].first
            assert_equal 12304, conversation["id"]
            assert_equal "A comment", conversation["body"]
          end
        end

        describe "with bad child events" do
          let(:sql) do
            "INSERT INTO `events` (`id`, `account_id`, `parent_id`, `ticket_id`," \
            " `author_id`, `created_at`, `via_id`, `via_reference_id`, `is_public`," \
            " `type`, `value`, `value_previous`, `value_reference`, `updated_at`," \
            " `notification_sent_at`) VALUES" \
            "(12302, #{ticket.account.id}, NULL, #{ticket.id}, #{author.id}," \
            " '2018-06-27 21:14:18', 0, NULL, 0, 'Audit', NULL, NULL, NULL, '2018-06-27 21:14:18', NULL)," \
            "(12303, 0, 12302, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL)," \
            "(12304, #{ticket.account.id}, 12302, #{ticket.id}, #{author.id}, '2018-06-27 21:14:18'," \
            " 0, NULL, 0, 'Comment', 'A comment', 'rich', NULL, '2018-06-27 21:14:18', NULL)"
          end

          before do
            Event.without_arsi.delete_all
            ActiveRecord::Base.connection.execute(sql)

            get :index, params: { ticket_id: ticket.nice_id }
          end

          it { assert_response 200 }

          it "renders the comment" do
            conversation = JSON.parse(@response.body)["conversations"].first
            assert_equal 12304, conversation["id"]
            assert_equal "A comment", conversation["body"]
          end
        end
      end

      describe "Ticket is from HelpCenter" do
        [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
          describe "via_id is #{via_id}" do
            before do
              ticket.update_column(:via_id, via_id)
              ticket.update_column(:via_reference_id, ViaType.HELPCENTER)
            end

            describe "agent_as_end_user Arturo is disabled" do
              before do
                Arturo.disable_feature!(:agent_as_end_user)
              end

              it "succeeds" do
                get :index, params: { ticket_id: ticket.nice_id }
                assert_response :ok
              end
            end

            describe "agent_as_end_user Arturo is enabled" do
              before do
                Arturo.enable_feature!(:agent_as_end_user)
              end

              it "fails" do
                get :index, params: { ticket_id: ticket.nice_id }
                assert_response :forbidden
              end
            end
          end
        end
      end
    end

    describe 'a GET to :show' do
      let(:event) { ticket.conversation_items.last }

      before do
        ticket.reload

        get :show, params: { ticket_id: ticket.nice_id, id: event.id }
      end

      should_use_presenter Api::Lotus::ConversationItemPresenter

      it 'sets cache headers based on the audt event' do
        etag = @response.headers['ETag']
        last = Time.parse(@response.headers['Last-Modified'])

        assert_equal Time.parse(event.updated_at.to_s), last
        assert etag.present?
      end

      it 'renders a single audit event' do
        item = JSON.parse(@response.body)['conversation_item']

        refute_nil item
        assert_equal event.id, item['id']
      end
    end
  end
end
