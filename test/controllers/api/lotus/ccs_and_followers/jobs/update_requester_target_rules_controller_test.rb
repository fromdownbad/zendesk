require_relative '../../../../../support/test_helper'
require "zendesk/testing/factories/global_uid"

SingleCov.covered!

describe Api::Lotus::CcsAndFollowers::Jobs::UpdateRequesterTargetRulesController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:admin) { users(:minimum_admin_not_owner) }

  before do
    @request.account = account
    accept :json
  end

  describe :routing do
    with_options(controller: 'api/lotus/ccs_and_followers/jobs/update_requester_target_rules') do |request|
      request.should_route(
        :post,
        '/api/lotus/ccs_and_followers/jobs/update_requester_target_rules',
        action: 'create'
      )
      request.should_route(:get, '/api/lotus/ccs_and_followers/jobs/update_requester_target_rules', action: 'index')
    end

    as_an_anonymous_user do
      should_be_unauthorized :create
    end

    as_an_end_user do
      should_be_forbidden :create
    end

    as_an_agent do
      should_be_forbidden :create
    end

    as_an_admin do
      describe '#create' do
        let(:expected_params) do
          {
            account_id: account.id, admin_id: admin.id, dry_run: true, onboarding_completed: false,
            via_id: 5, raise_on_too_many?: true, user_id: 23927,
            request: {user_agent: 'Rails Testing', remote_ip: '0.0.0.0'}
          }
        end

        before do
          job_id_stub = stub
          job_status_stub = stub
          ::CcsAndFollowers::UpdateRequesterTargetRulesJob.expects(:enqueue).
            once.with(expected_params).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status: job_status_stub)
          Api::V2::JobStatusPresenter.any_instance.expects(:present).returns(job_status: 'stub_response')
        end

        it 'responds with ok' do
          post :create, params: { dry_run: true, onboarding_completed: false }
          assert_response :ok
        end
      end

      describe '#index' do
        it 'sends the expected response' do
          get :index
          assert_response :success
          json_response = JSON.parse(@response.body)
          assert_equal({ "result" => { "url" => "" } }, json_response)
        end
      end
    end
  end
end
