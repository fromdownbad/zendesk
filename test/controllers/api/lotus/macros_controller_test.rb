require_relative '../../../support/test_helper'
require_relative '../../../support/rule'

SingleCov.covered!

describe Api::Lotus::MacrosController do
  include CustomFieldsTestHelper
  include TestSupport::Rule::Helper

  extend Api::V2::TestHelper

  fixtures :accounts, :users, :tickets, :rules, :role_settings

  let(:user)    { users(:minimum_agent) }
  let(:account) { user.account }
  let(:group)   { groups(:minimum_group) }

  before do
    @account = account

    accept :json
  end

  with_options(controller: 'api/lotus/macros') do |request|
    request.should_route :get, '/api/lotus/macros', action: 'index'
    request.should_route(:post,
      '/api/lotus/macros/42/apply',
      action:   'apply',
      macro_id: 42)
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_admin do
    describe 'a GET to :index' do
      before { get :index }

      it 'includes most-used macros' do
        assert json.key?('most_used')
      end

      should_use_presenter Api::Lotus::MacroPresenter

      it 'responds with `200 OK`' do
        assert_response :ok
      end

      it 'responds with JSON' do
        assert_equal 'application/json', @controller.response.content_type.to_s
      end
    end
  end

  as_an_agent_with_permissions(macro_access: 'full') do
    describe 'a GET to :index' do
      let(:macros) do
        json['macros'].map do |macro|
          {
            title:             macro['title'],
            description:       macro['description'],
            availability_type: macro['availability_type']
          }
        end
      end

      before do
        GroupMacro.destroy_all
        Macro.destroy_all

        Subscription.any_instance.stubs(has_group_rules?: true)
        Subscription.any_instance.stubs(has_personal_rules?: true)

        create_macro(title: 'AA', owner: account, active: true,  position: 7)
        create_macro(title: 'AB', owner: account, active: false, position: 8)
        create_macro(title: 'ZA', owner: account, active: true,  position: 5)
        create_macro(title: 'ZB', owner: account, active: false, position: 6)

        create_macro(title: 'AC', owner: group, active: true,  position: 11)
        create_macro(title: 'AD', owner: group, active: false, position: 12)
        create_macro(title: 'ZC', owner: group, active: true,  position: 9)
        create_macro(title: 'ZD', owner: group, active: false, position: 10)

        create_macro(title: 'AG', owner: user, active: true,  position: 3)
        create_macro(title: 'AH', owner: user, active: false, position: 4)
        create_macro(title: 'ZG', owner: user, active: true,  position: 1)
        create_macro(title: 'ZH', owner: user, active: false, position: 2)
      end

      describe 'with all features off' do
        before { get :index }

        should_use_presenter Api::Lotus::MacroPresenter

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'responds with JSON' do
          assert_equal(
            'application/json',
            @controller.response.content_type.to_s
          )
        end
      end

      describe 'when successful' do
        before { get :index }

        it 'returns the correct macros' do
          assert_equal(
            [
              {title: 'AA', availability_type: 'everyone', description: 'description'},
              {title: 'AC', availability_type: 'group',    description: 'description'},
              {title: 'AG', availability_type: 'personal', description: 'description'},
              {title: 'ZA', availability_type: 'everyone', description: 'description'},
              {title: 'ZC', availability_type: 'group',    description: 'description'},
              {title: 'ZG', availability_type: 'personal', description: 'description'}
            ],
            macros
          )
        end
      end
    end

    describe 'a GET to :apply' do
      let(:macro) { rules(:macro_incident_escalation) }

      before { Subscription.any_instance.stubs(has_personal_rules?: true) }

      describe 'with invalid id' do
        before { get :apply, params: { macro_id: 999 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'with an id of -1' do
        before do
          get :apply, params: { macro_id: macro.id, id: -1, ticket: {group_id: -1} }
        end

        should_use_presenter Api::Lotus::MacroApplicationPresenter
      end

      describe 'with a valid ticket id' do
        let(:ticket_id) { tickets(:minimum_1).nice_id }

        describe 'and the macro is active' do
          before { get :apply, params: { macro_id: macro.id, id: ticket_id } }

          should_use_presenter Api::Lotus::MacroApplicationPresenter
        end

        describe 'and the macro is inactive' do
          describe 'and the macro is not editable by the user' do
            let(:macro) do
              create_macro(title: 'A', owner: account, active: false, position: 13)
            end

            before do
              user.permission_set.permissions.macro_access = 'manage-group'

              get :apply, params: { macro_id: macro.id, id: ticket_id }
            end

            it 'responds with `404 Not Found`' do
              assert_response :not_found
            end
          end

          describe 'and the macro is editable by the user' do
            let(:macro) do
              create_macro(title: 'A', owner: user, active: false, position: 14)
            end

            before { get :apply, params: { macro_id: macro.id, id: ticket_id } }

            should_use_presenter Api::Lotus::MacroApplicationPresenter
          end
        end
      end

      describe 'with different permissions' do
        let(:ticket_id) { tickets(:minimum_1).nice_id }

        describe 'agent does not have permissions to view ticket' do
          before(:each) do
            User.any_instance.stubs(can?: false)
            get :apply, params: { ticket: { requester_id: user.id }, macro_id: macro.id, id: ticket_id }
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end

        describe 'agent has permissions to view ticket' do
          before(:each) do
            User.any_instance.stubs(can?: true)
            get :apply, params: { ticket: { requester_id: user.id }, macro_id: macro.id, id: ticket_id }
          end

          should_use_presenter Api::Lotus::MacroApplicationPresenter
        end

        describe 'the ticket is new' do
          before(:each) do
            # Lotus does not send :id parameter on new tickets
            get :apply, params: { macro_id: macro.id }
          end

          should_use_presenter Api::Lotus::MacroApplicationPresenter
        end
      end

      describe 'with an id of zero' do
        let(:definition) do
          macro.definition.actions.detect { |d| d.source == 'comment_value' }
        end

        before { post :apply, params: { id: '0', macro_id: macro.id } }

        it 'includes liquid template in the definition' do
          assert(
            definition.value.first.include?('{{'),
            'Liquid content in ticket data is present'
          )
        end

        it 'processes liquid in the comment' do
          refute(
            @response.body.include?('{{'),
            'Liquid content in ticket data is present'
          )
        end
      end
    end
  end
end
