require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::GroupsController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :groups, :role_settings

  before do
    accept :json
  end

  with_options(controller: "api/lotus/groups") do |request|
    request.should_route :get, "/api/lotus/groups/assignable", action: "assignable"
    request.should_route :get, "/api/lotus/groups/1", action: "show", id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized :assignable
  end

  as_an_end_user do
    should_be_forbidden :assignable
  end

  describe "#show" do
    as_an_admin do
      describe "with an invalid group id" do
        before { get :show, params: { id: -1 } }

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with a valid group id" do
        before { get :show, params: { id: groups(:minimum_group).id } }

        should_use_presenter Api::Lotus::GroupPresenter
        it('responds with success') { assert_response :success }
        it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
      end
    end

    as_an_agent do
      before { get :show, params: { id: groups(:minimum_group).id } }

      should_use_presenter Api::Lotus::GroupPresenter
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end
  end

  describe "#assignable" do
    as_an_admin do
      before { get :assignable }

      should_use_presenter Api::Lotus::GroupCollectionPresenter
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    as_an_agent do
      before { get :assignable }

      should_use_presenter Api::Lotus::GroupCollectionPresenter
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end
  end
end
