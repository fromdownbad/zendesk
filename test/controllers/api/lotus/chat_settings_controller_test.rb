require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::ChatSettingsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :role_settings

  before do
    accept :json
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index]
  end

  as_an_agent do
    describe 'with a valid pravda response' do
      before do
        body_json = {
          product: {
            id: 5,
            account_id: 2,
            name: "chat",
            active: true,
            state: "trial",
            state_updated_at: "2015-03-11T05:00Z",
            trial_expires_at: "2015-03-11T05:00Z",
            plan_settings: {
              plan_type: 1,
              max_agents: 100,
              light_agents: false,
              phase: 3,
              some_string_setting: "string",
              max_resolutions: 100,
              billing_cycle_day: 15,
              boosted_resolutions: 100,
              boost_expires_at: "date",
              boosted_plan_type: 1,
              insights: "string",
              monthly_messaged_users: 2000,
              target_environment: "string",
              suite: false
            },
            created_at: "2015-03-11T05:00Z",
            updated_at: "2015-03-11T05:00Z"
          }
        }.to_json

        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/90538/products/chat").to_return(
          status: 200,
          headers: { content_type: 'application/json; charset=utf-8' },
          body: body_json
        )

        get :index
      end

      should_use_presenter Api::Lotus::ChatSettingsPresenter

      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }

      it 'responds with chat phase' do
        response_json = JSON.parse(@response.body)
        account = response_json["account"]

        assert_includes account, "phase"
      end
    end

    describe 'with no response from pravda' do
      before do
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/90538/products/chat").to_raise(Kragle::ResponseError)
      end

      it 'presents nil' do
        Api::Lotus::ChatSettingsPresenter.any_instance.expects(:present).with(nil)

        get :index

        assert_response :success
      end
    end
  end
end
