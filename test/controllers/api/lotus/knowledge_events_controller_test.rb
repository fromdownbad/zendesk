require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::Lotus::KnowledgeEventsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :users, :tickets, :events, :role_settings

  let(:ticket) { tickets(:minimum_3) }
  let(:article) { { id: 1, url: 'http://helpcenter.article/1.json', html_url: 'http://helpcenter.article/1', title: 'article title 1', locale: 'en-US' } }
  let(:template) { { id: 1, url: 'http://helpcenter.article/1.json', html_url: 'http://helpcenter.article/1', title: 'article title 1' } }

  before do
    accept :json
    use_ssl
  end

  with_options(controller: "api/lotus/knowledge_events") do |request|
    request.should_route :post, "/api/lotus/tickets/1/knowledge_events", action: "create", ticket_id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized [:post, :create, ticket_id: 1]
  end

  as_an_end_user do
    should_be_forbidden [:post, :create, ticket_id: 1]
  end

  as_an_agent do
    def self.validate_event_handling(event_type)
      should_use_presenter Api::Lotus::KnowledgeEventPresenter

      it "is success" do
        assert_response :success
      end

      it "sends knowledge event with response" do
        @json_response = JSON.parse(@response.body)
        assert_not_nil @json_response["knowledge_event"]
      end

      it "sends the kcs event with response" do
        @json_response = JSON.parse(@response.body)
        assert_not_nil @json_response["knowledge_event"]["type"] == event_type
      end

      it "successfully saves the knowledge event" do
        @json_response = JSON.parse(@response.body)
        assert_not_nil @json_response["knowledge_event"]["id"]
      end
    end

    describe "a POST to :create" do
      it "does not fire triggers" do
        Audit.any_instance.expects(:execute_triggers).never
        post :create, params: { ticket_id: ticket.nice_id, article: article, knowledge_event: 'KnowledgeLinked' }
      end

      it "sends a radar notification" do
        RadarFactory.expects(:create_radar_notification).returns(stub(updated: true))
        post :create, params: { ticket_id: ticket.nice_id, article: article, knowledge_event: 'KnowledgeLinked' }
      end

      describe "KnowledgeLinked response" do
        before do
          post :create, params: { ticket_id: ticket.nice_id, article: article, knowledge_event: 'KnowledgeLinked' }
        end

        validate_event_handling('KnowledgeLinked')
      end

      describe "KnowledgeCaptured response" do
        before do
          post :create, params: { ticket_id: ticket.nice_id, article: article, template: template, knowledge_event: 'KnowledgeCaptured' }
        end

        validate_event_handling('KnowledgeCaptured')
      end

      describe "KnowledgeFlagged response" do
        before do
          post :create, params: { ticket_id: ticket.nice_id, article: article, knowledge_event: 'KnowledgeFlagged' }
        end

        validate_event_handling('KnowledgeFlagged')
      end

      describe "KnowledgeLinkAccepted response" do
        before do
          post :create, params: { ticket_id: ticket.nice_id, article: article, knowledge_event: 'KnowledgeLinkAccepted' }
        end

        validate_event_handling('KnowledgeLinkAccepted')
      end

      describe "KnowledgeLinkRejected response" do
        before do
          post :create, params: { ticket_id: ticket.nice_id, article: article, knowledge_event: 'KnowledgeLinkRejected' }
        end

        validate_event_handling('KnowledgeLinkRejected')
      end
    end
  end
end
