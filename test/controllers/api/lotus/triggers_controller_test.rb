require_relative '../../../support/test_helper'
require_relative '../../../support/rule'
require_relative '../../../support/api_scopes_helper'
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::Lotus::TriggersController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper

  include TestSupport::Rule::Helper

  fixtures :accounts,
    :rules,
    :tickets,
    :users

  let(:trigger) { rules(:trigger_notify_requester_of_received_request) }
  let(:account) { accounts(:minimum) }
  let(:filter_in_query) do
    {
      conditions: [
        {
          field: 'status',
          operator: 'is',
          value: 'open'
        },
        {
          field: 'group_id'
        }
      ],
      actions: [
        {
          field: 'notification_user',
          value: 557372
        }
      ],
      title: 'foo'
    }
  end

  let(:json_filter) do
    CGI.escape(JSON.generate(filter_in_query))
  end

  let(:triggers) do
    JSON.parse(@response.body)['triggers'].map do |trigger|
      {
        title:    trigger['title'],
        active:   trigger['active'],
        position: trigger['position']
      }
    end
  end

  let(:active) { nil }

  let(:structured_filter) do
    {
      json: JSON.generate({
        filter: {
          "$and" => [
            {
              "conditions" => { "$eq" => "status_id is 1" }
            }, {
              "conditions" => { "$eq" => "group_id" }
            }, {
              "actions" => { "$eq" => "notification_user 557372" }
            }, {
              "title" => { "$eq" => "foo" }
            }
          ]
        }
      })
    }
  end

  before do
    accept :json
    set_header('X-Zendesk-Lotus-Version', 'v1.2.3')
    use_ssl
  end

  with_options(controller: 'api/lotus/triggers') do |request|
    [
      [:get, 'api/lotus/triggers/search', {action: :search}]
    ].each do |verb, path, options|
      request.should_route(verb, path, options)
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :search
  end

  as_an_end_user do
    should_be_forbidden :search
  end

  as_an_agent_with_permissions(business_rule_management: true) do
    describe 'a GET to `search`' do
      let(:triggers) do
        json['triggers'].map { |trigger| {title: trigger['title']} }
      end

      before do
        Trigger.destroy_all

        create_trigger(title: 'ZB', active: false, position: 3)
        create_trigger(title: 'ZA', active: true,  position: 5)
        create_trigger(title: 'CB', active: false, position: 6)
        create_trigger(title: 'CA', active: true,  position: 2)
        create_trigger(title: 'CC', active: true,  position: 8)
        create_trigger(title: 'AB', active: false, position: 4)
        create_trigger(title: 'AA', active: true,  position: 1)
      end

      describe 'without a `filter` parameter' do
        before { get :search }

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe 'with a valid `query` parameter' do
        let(:results) do
          [
            {'id' => Trigger.find_by_title('AA').id, 'type' => 'Trigger'},
            {'id' => Trigger.find_by_title('AB').id, 'type' => 'Trigger'},
            {'id' => Trigger.find_by_title('ZA').id, 'type' => 'Trigger'},
            {'id' => Trigger.find_by_title('CA').id, 'type' => 'Trigger'}
          ]
        end

        before do
          ZendeskSearch::Client.
            any_instance.
            stubs(search: {'results' => results, 'count' => results.size})
        end

        describe 'and no other parameters' do
          before { get :search, params: { filter: json_filter } }

          should_use_presenter Api::V2::Rules::TriggerPresenter

          it 'returns the results' do
            assert_equal(
              [{title: 'AA'}, {title: 'AB'}, {title: 'ZA'}, {title: 'CA'}],
              triggers
            )
          end
        end

        describe 'and sorting parameters' do
          describe_with_arturo_setting_disabled :trigger_categories_api do
            before { get :search, params: { filter: json_filter, sort_by: 'position' } }

            before_should 'pass through the query and sort options' do
              ZendeskSearch::Client.
                any_instance.
                expects(:search).
                with(
                  'Notify order_by:position,title sort:asc', # We over-write the query param
                  account_id:  account.id,
                  endpoint:    'rule',
                  from:        0,
                  fq:          '',
                  hl:          true,
                  incremental: true,
                  size:        100,
                  type:        'trigger'
                ).
                returns('results' => results, 'count' => results.size)
            end
          end

          describe_with_arturo_setting_enabled :trigger_categories_api do
            before do
              create_category

              get :search, params: { filter: json_filter, sort_by: 'position' }
            end

            before_should 'pass through the query and sort options' do
              ZendeskSearch::Client.
                any_instance.
                expects(:search).
                with(
                  'Notify order_by:category_position,position,title sort:asc', # We over-write the query param
                  account_id:  account.id,
                  endpoint:    'rule',
                  from:        0,
                  fq:          '',
                  hl:          true,
                  incremental: true,
                  size:        100,
                  type:        'trigger'
                ).
                returns('results' => results, 'count' => results.size)
            end
          end
        end
      end
    end
  end

  describe_with_arturo_enabled :new_trigger_search do
    before { Arturo.enable_feature!(:new_trigger_search) }

    as_an_agent_with_permissions(business_rule_management: true) do
      describe 'a GET to `search`' do
        let(:triggers) do
          json['triggers'].map { |trigger| {title: trigger['title']} }
        end

        before do
          Trigger.destroy_all

          create_trigger(title: 'ZB', active: false, position: 3)
          create_trigger(title: 'ZA', active: true,  position: 5)
          create_trigger(title: 'CB', active: false, position: 6)
          create_trigger(title: 'CA', active: true,  position: 2)
          create_trigger(title: 'CC', active: true,  position: 8)
          create_trigger(title: 'AB', active: false, position: 4)
          create_trigger(title: 'AA', active: true,  position: 1)
        end

        describe 'without a `filter` parameter' do
          before { get :search }

          it 'responds with `400 Bad Request`' do
            assert_response :bad_request
          end
        end

        describe 'with a valid `filter` parameter' do
          let(:results) do
            [
              {'id' => Trigger.find_by_title('AA').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('AB').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('ZA').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('CA').id, 'type' => 'Trigger'}
            ]
          end

          before do
            ZendeskSearch::Client.
              any_instance.
              stubs(search: {'results' => results, 'count' => results.size})
          end

          describe 'and no other parameters' do
            before { get :search, params: { filter: json_filter } }

            should_use_presenter Api::V2::Rules::TriggerPresenter

            it 'returns the results' do
              assert_equal(
                [{title: 'AA'}, {title: 'AB'}, {title: 'ZA'}, {title: 'CA'}],
                triggers
              )
            end
          end

          describe 'and sorting parameters' do
            describe 'using the default sorting order' do
              describe_with_arturo_setting_disabled :trigger_categories_api do
                before { get :search, params: { filter: json_filter, sort_by: 'position' } }

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:  account.id,
                      endpoint:    'trigger',
                      filter:      structured_filter,
                      from:        0,
                      hl:          true,
                      incremental: true,
                      size:        100,
                      sort:        'position,title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end

              describe_with_arturo_setting_enabled :trigger_categories_api do
                before do
                  create_category

                  get :search, params: { filter: json_filter, sort_by: 'position' }
                end

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:  account.id,
                      endpoint:    'trigger',
                      filter:      structured_filter,
                      from:        0,
                      hl:          true,
                      incremental: true,
                      size:        100,
                      sort:        'category_position,position,title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end
            end

            describe 'using the ascending sorting order' do
              describe_with_arturo_setting_disabled :trigger_categories_api do
                before { get :search, params: { filter: json_filter, sort_by: 'position', sort_order: 'asc' } }

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:  account.id,
                      endpoint:    'trigger',
                      filter:      structured_filter,
                      from:        0,
                      hl:          true,
                      incremental: true,
                      size:        100,
                      sort:        'position,title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end

              describe_with_arturo_setting_enabled :trigger_categories_api do
                before do
                  create_category

                  get :search, params: { filter: json_filter, sort_by: 'position', sort_order: 'asc' }
                end

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:  account.id,
                      endpoint:    'trigger',
                      filter:      structured_filter,
                      from:        0,
                      hl:          true,
                      incremental: true,
                      size:        100,
                      sort:        'category_position,position,title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end
            end

            describe 'using the descending sorting order' do
              describe_with_arturo_setting_disabled :trigger_categories_api do
                before { get :search, params: { filter: json_filter, sort_by: 'position', sort_order: 'desc' } }

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:  account.id,
                      endpoint:    'trigger',
                      filter:      structured_filter,
                      from:        0,
                      hl:          true,
                      incremental: true,
                      size:        100,
                      sort:        '-position,-title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end

              describe_with_arturo_setting_enabled :trigger_categories_api do
                before do
                  create_category

                  get :search, params: { filter: json_filter, sort_by: 'position', sort_order: 'desc' }
                end

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:  account.id,
                      endpoint:    'trigger',
                      filter:      structured_filter,
                      from:        0,
                      hl:          true,
                      incremental: true,
                      size:        100,
                      sort:        '-category_position,-position,-title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end
            end
          end
        end
      end
    end
  end
end
