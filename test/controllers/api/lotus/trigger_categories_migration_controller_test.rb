require_relative "../../../support/test_helper"
require_relative '../../../support/rule'

SingleCov.covered!

describe Api::Lotus::TriggerCategoriesMigrationController do
  extend Api::V2::TestHelper
  include TestSupport::Rule::Helper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:trigger_categories) { RuleCategory.where(account: account) }
  let(:trigger_category) { trigger_categories.first }
  let(:triggers) { account.all_triggers }

  before do
    accept :json
    use_ssl
    RuleCategory.destroy_all
  end

  with_options(controller: "api/lotus/trigger_categories_migration") do |request|
    request.should_route :put, "/api/lotus/trigger_categories_migration/migrate", action: "migrate"
    request.should_route :put, '/api/lotus/trigger_categories_migration/revert', action: 'revert'
  end

  describe_with_arturo_disabled :trigger_categories do
    as_an_admin do
      should_be_forbidden %i[put migrate]
      should_be_forbidden %i[put revert]
    end
  end

  describe_with_arturo_enabled :trigger_categories do
    as_an_anonymous_user do
      should_be_unauthorized %i[put migrate]
      should_be_unauthorized %i[put revert]
    end

    as_an_end_user do
      should_be_forbidden %i[put migrate]
      should_be_forbidden %i[put revert]
    end

    as_an_agent do
      should_be_forbidden %i[put migrate]
      should_be_forbidden %i[put revert]
    end

    as_an_admin do
      let(:response) { JSON.parse(@response.body) }

      describe "a PUT to :migrate" do
        describe_with_arturo_setting_enabled :trigger_categories_api do
          it "returns an error response" do
            put :migrate

            assert_response :forbidden
            assert_equal "TriggerCategoriesAlreadyMigrated", response["errors"].first["code"]
          end
        end

        describe_with_arturo_disabled :trigger_categories_api do
          describe "on success" do
            before { put :migrate }

            it "returns an OK response" do
              assert_response :ok
            end

            it "creates the default trigger category" do
              assert_equal 1, trigger_categories.size
              assert_equal "Initial category", trigger_category.name
            end

            it "migrates all existing triggers to use the created category's ID" do
              assert_equal [trigger_category.id], triggers.map(&:rules_category_id).uniq
            end

            it "enables the trigger_categories_api account setting" do
              account.reload

              assert account.has_trigger_categories_api_enabled?
            end
          end

          describe "on failure" do
            let(:response) { JSON.parse(@response.body) }

            before do
              Account.any_instance.stubs(:update_attributes!).raises(StandardError)

              put :migrate
            end

            before_should 'record an exception' do
              message = "Trigger categories migration error for account: #{account.id}"
              fingerprint = '5522d801364aeb3ba9e671c48d30c17127e86661'

              ZendeskExceptions::Logger.expects(:record).with do |_e, params|
                assert_equal(fingerprint, params[:fingerprint])
                assert_equal(message, params[:message])
              end
            end

            it "returns an error response" do
              assert_response :unprocessable_entity
              assert_equal "TriggerCategoriesMigrationFailed", response["errors"].first["code"]
            end

            it "does not persist data changes" do
              assert_empty trigger_categories
              assert_empty triggers.map(&:rules_category_id).reject(&:blank?)
              refute account.has_trigger_categories_api_enabled?
            end
          end
        end
      end

      describe 'a PUT to :revert' do
        describe_with_arturo_setting_disabled :trigger_categories_api do
          before { put :revert }

          it { assert_response :forbidden }
        end

        describe_with_arturo_setting_enabled :trigger_categories_api do
          describe 'when successful' do
            before do
              RuleCategory.destroy_all
              Trigger.destroy_all

              category_1 = create_category(position: 1)
              category_2 = create_category(position: 2)

              create_trigger(position: 5, rules_category_id: category_1.id)
              create_trigger(position: 6, rules_category_id: category_1.id)
              create_trigger(position: 7, rules_category_id: category_1.id)

              create_trigger(position: 1, rules_category_id: category_2.id)
              create_trigger(position: 2, rules_category_id: category_2.id)
              create_trigger(position: 3, rules_category_id: category_2.id)
              create_trigger(position: 4, rules_category_id: category_2.id)

              @relative_ordering = account.triggers_with_category_position.pluck(:id)

              put :revert
            end

            it { assert_response :ok }

            it 'deletes all of the accounts categories' do
              assert_empty RuleCategory.trigger_categories(account)
            end

            it 'unsets rules_category_id for all of the account\'s triggers' do
              assert_equal [nil], triggers.pluck(:rules_category_id).uniq
            end

            it 'resets to absolute positions' do
              assert_equal (1..@relative_ordering.length).to_a, account.triggers.pluck(:position)
              assert_equal @relative_ordering, account.triggers.pluck(:id)
            end
          end

          describe 'when unsuccessful' do
            before do
              RuleCategory.expects(:trigger_categories).raises(StandardError)

              put :revert
            end

            before_should 'record an exception' do
              message = "Trigger categories revert error for account: #{account.id}"
              fingerprint = '0c75dbe44bddf81c0b9692afd68fbad0f4c3b2ab'

              ZendeskExceptions::Logger.expects(:record).with do |_e, params|
                assert_equal(fingerprint, params[:fingerprint])
                assert_equal(message, params[:message])
              end
            end

            it { assert_response :unprocessable_entity }

            it 'presents the error' do
              assert_equal 'TriggerCategoriesRevertFailed', response["errors"].first["code"]
            end
          end
        end
      end
    end
  end
end
