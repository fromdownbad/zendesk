require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::AgentsController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :groups, :role_settings

  before do
    accept :json
  end

  with_options(controller: "api/lotus/agents") do |request|
    request.should_route :get, "/api/lotus/groups/1234/agents/assignable", action: "assignable", group_id: 1234
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :assignable, group_id: 1234]
  end

  as_an_end_user do
    should_be_forbidden [:get, :assignable, group_id: 1234]
  end

  as_an_admin do
    describe "a GET to :assignable" do
      describe "with a valid group id" do
        before { get :assignable, params: { group_id: groups(:minimum_group).id } }
        should_use_presenter Api::Lotus::AgentCollectionPresenter
        it('responds with success') { assert_response :success }
        it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
      end

      describe "with an invalid group id" do
        before { get :assignable, params: { group_id: 0 } }
        it('responds with not_found') { assert_response :not_found }
        it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
      end
    end
  end

  as_an_agent do
    describe "a GET to :assignable" do
      before { get :assignable, params: { group_id: groups(:minimum_group).id } }
      should_use_presenter Api::Lotus::AgentCollectionPresenter
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end
  end
end
