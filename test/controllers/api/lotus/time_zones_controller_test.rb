require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::TimeZonesController do
  extend Api::Presentation::TestHelper

  before do
    accept :json
  end

  with_options(controller: 'api/lotus/time_zones') do |request|
    request.should_route :get, '/api/lotus/time_zones', action: 'index'
  end

  as_an_anonymous_user do
    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::Lotus::TimeZonePresenter, status: :ok

      it 'presents time zones' do
        amsterdam = ActiveSupport::TimeZone["Amsterdam"]
        time_zones = JSON.parse(@response.body)['time_zones']
        assert_not_nil time_zones.find { |tz| tz['name'] == amsterdam.name }
      end

      it 'sorts by offset, then name' do
        time_zones = JSON.parse(@response.body)['time_zones']

        sorted = time_zones.sort do |x, y|
          offset_diff = x['offset'] <=> y['offset']
          offset_diff == 0 ? x['name'] <=> y['name'] : offset_diff
        end

        assert_equal time_zones, sorted
      end

      it 'caches for a week' do
        assert_match /max-age=604800/, @response.headers['Cache-Control']
      end

      it 'caches publicly, so reverse proxies can reduce load' do
        @response.headers['Cache-Control'].must_include "public"
      end

      it 'sets an ETag' do
        assert_not_nil @response.headers['ETag']
      end
    end
  end
end
