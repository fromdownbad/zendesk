require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::CfaMigrationController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :ticket_fields

  let(:account) { accounts(:minimum) }
  let(:cfa_installation_id) { rand(10000) }

  before do
    accept :json
    use_ssl
    Account.any_instance.stubs(:has_ticket_forms?).returns(true)
  end

  with_options(controller: "api/lotus/cfa_migration") do |request|
    request.should_route :put, "/api/lotus/cfa_migration/migrate", action: "migrate"
    request.should_route :put, "/api/lotus/cfa_migration/exit", action: "exit"
  end

  as_an_anonymous_user do
    should_be_unauthorized %i[put migrate]
    should_be_unauthorized %i[put exit]
  end

  as_an_end_user do
    should_be_forbidden %i[put migrate]
    should_be_forbidden %i[put exit]
  end

  as_an_agent do
    should_be_forbidden %i[put migrate]
    should_be_forbidden %i[put exit]
  end

  as_an_admin do
    describe "a PUT to :migrate" do
      describe "without a CFA installation" do
        before { put :migrate }
        it { assert_response :unprocessable_entity }
      end

      describe "with multiple enabled CFA installations" do
        before do
          install_cfa(true)
          another_cfa_installation_id = rand(10000)
          install_cfa(true, another_cfa_installation_id)
          put :migrate
        end
        it { assert_response :unprocessable_entity }
      end

      describe "with a disabled CFA installation" do
        before do
          install_cfa(false)
          Account.any_instance.stubs(:migrate_cfa_to_ctf).returns(
            successful: [stub],
            failed: []
          )

          # If we don't stub apps request and the migration still works,
          # we can assume that we're correctly detecting that the app is already disabled.

          put :migrate
        end

        it "doesn't try to disable CFA" do
          assert_response :ok
        end
      end

      describe "with a CFA installation" do
        before { install_cfa(true) }

        describe "when the migration is successful" do
          before do
            refute account.settings.native_conditional_fields_migrated?
            assert account.cfa_migration_needed?
            Account.any_instance.stubs(:migrate_cfa_to_ctf).returns(
              successful: [stub],
              failed: []
            )

            body = {
              'id' => cfa_installation_id,
              'app_id' => 123,
              'enabled' => false
            }
            stub_request(:put, "https://minimum.zendesk-test.com/api/v2/apps/installations/#{cfa_installation_id}.json?enabled=false").to_return(
              status: 200,
              body: body.to_json,
              headers: {'Content-Type' => 'application/json'}
            )

            put :migrate
          end
          it { assert_response :ok }

          it "marks ctf as migrated" do
            assert account.reload.settings.native_conditional_fields_migrated?
            refute account.reload.cfa_migration_needed?
          end
        end

        describe "when the API call to disable CFA fails" do
          before do
            Account.any_instance.stubs(:migrate_cfa_to_ctf).returns(
              successful: [stub],
              failed: []
            )
            stub_request(:put, "https://minimum.zendesk-test.com/api/v2/apps/installations/#{cfa_installation_id}.json?enabled=false").to_return(status: 422)
            ZendeskExceptions::Logger.expects(:record)

            put :migrate
          end
          it { assert_response :unprocessable_entity }

          it "marks migration as failed and not migrated" do
            assert account.reload.settings.native_conditional_fields_migration_failed?
            refute account.reload.settings.native_conditional_fields_migrated?
          end
        end

        describe "when the API call to disable CFA doesn't work" do
          before do
            Account.any_instance.stubs(:migrate_cfa_to_ctf).returns(
              successful: [stub],
              failed: []
            )

            body = {
              'id' => cfa_installation_id,
              'app_id' => 123,
              'enabled' => true # the updated installation is still enabled
            }
            stub_request(:put, "https://minimum.zendesk-test.com/api/v2/apps/installations/#{cfa_installation_id}.json?enabled=false").to_return(
              status: 200,
              body: body.to_json,
              headers: {'Content-Type' => 'application/json'}
            )

            put :migrate
          end
          it { assert_response :unprocessable_entity }
        end

        describe "when the migration fails" do
          let(:failed_condition) do
            TicketFieldCondition.new(
              account: account,
              ticket_form: nil,
              parent_field: account.ticket_fields.first,
              child_field: account.ticket_fields.last
            )
          end

          before do
            refute failed_condition.valid?

            Account.any_instance.stubs(:migrate_cfa_to_ctf).returns(
              successful: [],
              failed: [failed_condition]
            )

            put :migrate
          end

          it { assert_response :unprocessable_entity }

          ## {
          ##   "description": "Could not migrate from the Conditional Fields app",
          ##   "failed": [
          ##     "message": "Invalid condition. Ticket Form: '11'. Parent Field: '21'. Child Field: '31'. Value: 'some_value'",
          ##     "errors": [
          ##       "Ticket form is invalid",
          ##       "Parent field is invalid",
          ##       "Child field is invalid"
          ##     ]
          ##   ]
          ## }
          it "presents the errors" do
            @json_response = JSON.parse(@response.body)
            assert_equal I18n.t('txt.error_message.migrate_cfa_to_ctf.migration_error'), @json_response["description"]

            error_message = I18n.t('txt.error_message.migrate_cfa_to_ctf.invalid_condition',
              ticket_form_id: failed_condition.ticket_form_id,
              parent_field_id: failed_condition.parent_field_id,
              child_field_id: failed_condition.child_field_id,
              value: failed_condition.value)
            assert_equal error_message, @json_response["failed"].first["message"]

            assert_equal failed_condition.errors.full_messages, @json_response["failed"].first["errors"]
          end

          it "marks migration as failed" do
            assert account.reload.settings.native_conditional_fields_migration_failed?
          end
        end
      end
    end

    describe "a PUT to :exit" do
      describe "with an account that is not migrated" do
        before { put :exit }
        it { assert_response :unprocessable_entity }
      end

      describe "without a CFA installation and an account already migrated" do
        before do
          account.update_attributes!(settings: {
            native_conditional_fields_migrated: true
          })
          put :exit
        end
        it { assert_response :unprocessable_entity }
      end

      describe "with a CFA installation and an account already migrated" do
        before do
          install_cfa(false)
          account.update_attributes!(settings: {
            native_conditional_fields_migrated: true
          })
          account.reload
          refute account.settings.native_conditional_fields_abandoned?
          assert account.has_native_conditional_fields_enabled?
        end

        describe "when the exit is successful" do
          before do
            body = {
              'id' => cfa_installation_id,
              'app_id' => 123,
              'enabled' => true
            }
            stub_request(:put, "https://minimum.zendesk-test.com/api/v2/apps/installations/#{cfa_installation_id}.json?enabled=true").to_return(
              status: 200,
              body: body.to_json,
              headers: {'Content-Type' => 'application/json'}
            )

            put :exit
          end
          it { assert_response :ok }

          it "marks ctf as abandoned" do
            assert account.reload.settings.native_conditional_fields_abandoned?
          end
        end

        describe "when the API call to enable CFA fails" do
          before do
            stub_request(:put, "https://minimum.zendesk-test.com/api/v2/apps/installations/#{cfa_installation_id}.json?enabled=true").to_return(status: 422)
            ZendeskExceptions::Logger.expects(:record)

            put :exit
          end
          it { assert_response :unprocessable_entity }

          it "presents the error" do
            @json_response = JSON.parse(@response.body)
            assert_equal I18n.t('txt.error_message.migrate_cfa_to_ctf.unknown_error'), @json_response["description"]
          end
        end

        describe "when the API call to enable CFA doesn't work" do
          before do
            body = {
              'id' => cfa_installation_id,
              'app_id' => 123,
              'enabled' => false # the updated installation is still enabled
            }
            stub_request(:put, "https://minimum.zendesk-test.com/api/v2/apps/installations/#{cfa_installation_id}.json?enabled=true").to_return(
              status: 200,
              body: body.to_json,
              headers: {'Content-Type' => 'application/json'}
            )

            put :exit
          end
          it { assert_response :unprocessable_entity }

          it "presents the error" do
            @json_response = JSON.parse(@response.body)
            assert_equal I18n.t('txt.error_message.migrate_cfa_to_ctf.unknown_error'), @json_response["description"]
          end
        end
      end
    end
  end

  def install_cfa(enabled, installation_id = cfa_installation_id)
    app_data_query = "INSERT INTO apps_installations (id, account_id, app_id, enabled, created_at, updated_at, product_code) VALUES (#{installation_id.to_i}, #{account.id.to_i}, #{Account::CfaMigration::CFA_APP_ID.to_i}, #{enabled}, '#{Date.today}', '#{Date.today}', 1)"
    ActiveRecord::Base.connection.insert(app_data_query)
  end
end
