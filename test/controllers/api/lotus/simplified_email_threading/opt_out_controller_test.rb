require_relative '../../../../support/test_helper'
require "zendesk/testing/factories/global_uid"

SingleCov.covered!

describe Api::Lotus::SimplifiedEmailThreading::OptOutController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:admin) { users(:minimum_admin_not_owner) }

  before do
    @request.account = account
    accept :json
  end

  describe :routing do
    with_options(controller: 'api/lotus/simplified_email_threading/opt_out') do |request|
      request.should_route(
        :post,
        '/api/lotus/simplified_email_threading/opt_out',
        action: 'create'
      )
      request.should_route(:get, '/api/lotus/simplified_email_threading/opt_out', action: 'index')
      request.should_route(:post, "/api/lotus/simplified_email_threading/opt_out/feedback", action: "feedback")
    end

    as_an_anonymous_user do
      should_be_unauthorized :create
    end

    as_an_end_user do
      should_be_forbidden :create
    end

    as_an_agent do
      should_be_forbidden :create
    end

    as_an_admin do
      describe '#create' do
        before do
          job_id_stub = stub
          job_status_stub = stub

          SimplifiedEmailThreading::OptOutJob.expects(:enqueue).once.returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status: job_status_stub)
          Api::V2::JobStatusPresenter.any_instance.expects(:present).returns(job_status: 'stub_response')
        end

        it 'responds with ok' do
          post :create, params: { dry_run: true }
          assert_response :ok
        end
      end

      describe '#index' do
        it 'sends the expected response' do
          get :index
          assert_response :success
          json_response = JSON.parse(@response.body)
          assert_equal({ "result" => { "url" => "" } }, json_response)
        end
      end

      describe "#feedback" do
        let(:feedback) { "Hello world" }

        describe "when feedback creation succeeds" do
          let(:response_body) do
            {
              "request" => {
                "id" => 35436,
                "subject" => "My printer is on fire!"
              }
            }
          end
          let(:kragle_response) { stub(success?: true, body: response_body) }

          before { stub_request(:post, %r{/api/v2/requests}).to_return(status: 200, body: response_body.to_json, headers: { content_type: 'application/json; charset=utf-8' }) }

          it "sends the expected response" do
            post :feedback, params: { feedback: feedback }
            assert_response :success
            assert JSON.parse(response.body)["result"]["z1_request_url"]
          end

          describe "uses the correct presenter" do
            before { post :feedback, params: { feedback: feedback } }

            should_use_presenter Api::Lotus::SimplifiedEmailThreading::FeedbackPresenter, return_value: { result: { z1_request_url: "https://support.#{ENV.fetch('ZENDESK_HOST')}" } }
          end
        end

        describe "when feedback creation fails" do
          describe "and error is Kragle::ServerError" do
            before { stub_request(:post, %r{/api/v2/requests}).to_raise(Kragle::ServerError) }

            it "sends the expected response" do
              post :feedback, params: { feedback: feedback }
              assert_response :unprocessable_entity
            end
          end

          describe "and response body does not indicate success" do
            before do
              stub_request(:post, %r{/api/v2/requests})
              Faraday::Response.any_instance.expects(:success?).returns(false)
            end

            it "sends the expected response" do
              post :feedback, params: { feedback: feedback }
              assert_response :unprocessable_entity
            end
          end
        end
      end
    end
  end
end
