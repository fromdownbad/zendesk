require_relative '../../../../support/test_helper'

SingleCov.covered!

class Api::Lotus::SimplifiedEmailThreading::FeedbackSupportTest < ActionController::TestCase
  class FeedbackSupportTestController < ApplicationController
    include Api::Lotus::SimplifiedEmailThreading::FeedbackSupport
  end

  tests FeedbackSupportTestController

  describe Api::Lotus::SimplifiedEmailThreading::FeedbackSupport do
    fixtures :accounts

    let(:account) { accounts(:minimum) }

    as_an_agent do
      describe '#report_simplified_email_threading_opt_out_feedback' do
        let(:kragle_client) { stub }
        let(:success_response) { stub(success?: true, body: response_body.to_json) }
        let(:kragle_response) { success_response }
        let(:request_params) { {feedback: "Hello World" } }

        before do
          @controller.stubs(:params).returns(request_params)
          @controller.stubs(:internal_api_client).returns(kragle_client)
        end

        it 'creates a support ticket with the corrrect data' do
          kragle_client.expects(:post).with('api/v2/requests', request: {subject: "Simplified Email Threading Feedback (#{account.subdomain})",
                                                                         comment: {body: request_params[:feedback]},
                                                                         requester: users(:minimum_agent),
                                                                         tags: "simplified_email_threading_feedback"}).once
          @controller.send(:report_simplified_email_threading_opt_out_feedback)
        end
      end

      describe "#internal_api_client" do
        it "returns a client" do
          client = @controller.send(:internal_api_client)
          assert client.is_a?(Faraday::Connection)
        end
      end
    end
  end
end
