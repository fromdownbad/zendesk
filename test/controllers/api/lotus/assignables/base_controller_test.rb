require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::Lotus::Assignables::BaseController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :groups, :role_settings

  before do
    accept :json
    Account.any_instance.stubs(:has_groups?).returns(true)
  end

  with_options(controller: "api/lotus/assignables/base") do |request|
    request.should_route :get, "/api/lotus/assignables/groups", action: "groups"
    request.should_route :get, "/api/lotus/assignables/groups/1/agents", action: "group_agents", id: 1
    request.should_route :get, "/api/lotus/assignables/autocomplete", action: "autocomplete"
  end

  as_an_anonymous_user do
    should_be_unauthorized :groups
    should_be_unauthorized [:get, :group_agents, id: 1]
    should_be_unauthorized [:get, :autocomplete, name: 'retail']
  end

  as_an_end_user do
    should_be_forbidden :groups
    should_be_forbidden [:get, :group_agents, id: 1]
    should_be_forbidden [:get, :autocomplete, name: 'retail']
  end

  as_an_agent_with_permissions(available_user_lists: 'none') do
    before do
      matching_assignable_agents_mock = mock('matching_assignable_agents mock')
      matching_assignable_agents_mock.stubs('exception').returns(false)
      matching_assignable_agents_mock.stubs('+')
      agent_query_mock = mock('agent_query mock')
      agent_query_mock.stubs(:execute).returns(matching_assignable_agents_mock)
      @controller.stubs(:agent_query).returns(agent_query_mock)
      @controller.stubs(:matching_assignable_groups)
      Api::Lotus::Assignables::AutocompletePresenter.any_instance.stubs(:present)

      get :autocomplete, params: { name: 'retail' }
    end

    it('allows access to autocomplete') do
      assert_response :success
    end
  end

  describe "#agent_query" do
    as_an_agent do
      describe "when given a name" do
        before do
          @controller.params[:name] = 'spatula'
          @controller.send :agent_query
        end

        it "should return a valid query" do
          assert_match /name:spatula/, @controller.send(:query).string
        end
      end

      describe "when given a name with '.'" do
        before do
          @controller.params[:name] = 'foo.bar'
          @controller.send :agent_query
        end

        it "should replace '.' with space in the name" do
          assert_match /name:foo bar/, @controller.send(:query).string
        end
      end
    end
  end

  describe "#groups" do
    as_an_agent do
      before { get :groups }

      should_use_presenter Api::Lotus::Assignables::GroupsPresenter
    end
  end

  describe "#group_agents" do
    as_an_agent do
      describe "with an invalid group id" do
        before { get :group_agents, params: { id: -1 } }

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with a valid group id" do
        before { get :group_agents, params: { id: groups(:minimum_group).id } }

        should_use_presenter Api::Lotus::AgentCollectionPresenter
      end
    end
  end

  describe "#autocomplete" do
    as_an_agent do
      describe "without exceptions" do
        before do
          agent_results = Zendesk::Search::Results.new(1, 2, 3)
          agent_results << users(:minimum_agent)
          agent_results << users(:minimum_admin)
          query_mock = mock('agent_query mock')
          query_mock.stubs(:execute).returns(agent_results)
          @controller.stubs(:agent_query).returns(query_mock)

          group_results = [groups(:minimum_group), groups(:billing_group)]
          @controller.stubs(:matching_assignable_groups).returns(group_results)

          get :autocomplete, params: { name: query_string }
        end

        describe "with an empty query string" do
          let(:query_string) { '' }

          it "returns empty results for an empty query string" do
            expected = {
              "agents" => [],
              "groups" => [],
              "count" => 0
            }

            assert_equal expected, JSON.parse(@response.body)
          end
        end

        describe "with a non-empty query string" do
          let(:query_string) { 'spatula' }

          it "returns appropriate results for a non-empty query" do
            expected = {
              "agents" => [
                {
                  "id" => users(:minimum_agent).id,
                  "name" => "Agent Minimum",
                  "group_id" => groups(:minimum_group).id,
                  "group" => "minimum_group",
                  "photo_url" => nil
                },
                {
                  "id" => users(:minimum_admin).id,
                  "name" => "Admin Man",
                  "group_id" => groups(:minimum_group).id,
                  "group" => "minimum_group",
                  "photo_url" => nil
                },
              ],
              "groups" => [
                {
                  "id" => groups(:minimum_group).id,
                  "name" => "minimum_group"
                },
                {
                  "id" => groups(:billing_group).id,
                  "name" => "Billing Support"
                }
              ],
              "count" => 4
            }

            assert_equal expected, JSON.parse(@response.body)
          end

          should_use_presenter Api::Lotus::Assignables::AutocompletePresenter
          it('responds with success') { assert_response :success }
          it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
        end
      end

      describe "with an exception" do
        before do
          agent_results = Zendesk::Search::Results.new(1, 2, 3)
          agent_results.stubs(:exception).returns(Hashie::Mash.new(message: "exceptional"))
          query_mock = mock('agent_query mock')
          query_mock.stubs(:execute).returns(agent_results)
          @controller.stubs(:agent_query).returns(query_mock)

          get :autocomplete, params: { name: 'nope' }
        end

        it "responds with an error message" do
          expected = { "error" => "Unavailable", "description" => "exceptional" }

          assert_equal expected, JSON.parse(@response.body)
        end

        it('responds with internal_server_error') { assert_response :internal_server_error }
      end
    end
  end
end
