require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Lotus::ActivitiesController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :user_settings, :tickets, :events, :activities

  before do
    accept :json
  end

  with_options(controller: "api/lotus/activities") do |request|
    request.should_route :get, "/api/lotus/activities", action: "index"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_agent do
    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::Lotus::TicketActivityPresenter, status: :ok

      it "renders all activities" do
        activities = JSON.parse(@response.body)
        assert_equal 3, activities["count"]
      end
    end
  end
end
