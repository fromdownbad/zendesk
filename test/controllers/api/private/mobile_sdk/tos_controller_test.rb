require_relative '../../../../support/test_helper'
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::Private::MobileSdk::TosController do
  extend Api::V2::TestHelper

  fixtures :all

  before do
    accept :json

    @account = accounts(:minimum)
    @controller.stubs(:current_account).returns(@account)
  end

  with_options(controller: 'api/private/mobile_sdk/tos') do |request|
    request.should_route :post, '/api/private/mobile_sdk/tos/accept.json', action: :accept, format: 'json'
  end

  as_a_subsystem_user(user: "classic", account: :minimum) do
    describe "when the account owner is not verified" do
      before do
        @owner = users(:minimum_admin_not_verified)
        @owner.is_verified = false
        @account.stubs(:owner).returns(@owner)
        assert_equal false, @owner.is_verified
      end

      describe 'POST to #accept' do
        before { post :accept }

        it('responds with success') { assert_response :success }
        it('accepts terms and conditions') do
          assert @account.reload.settings.allowed_mobile_sdk, "Tos must be accepted after API call"
        end
      end

      describe 'POST to #accept and saving the account fails' do
        before do
          Account.any_instance.expects(:save).with(validate: false).returns(false)
          @account.errors.add(:base, "there was an error trying to save the account")

          post :accept
        end

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

        it('does not change terms and conditions') do
          assert !@account.reload.settings.allowed_mobile_sdk, "Tos must not be accepted after unsuccesful API call"
        end
      end
    end

    describe "when the account owner is verified" do
      before do
        assert(@account.owner.is_verified)
      end

      describe 'POST to #accept' do
        before { post :accept }

        it('responds with success') { assert_response :success }
        it('accepts terms and conditions') do
          assert @account.reload.settings.allowed_mobile_sdk, "Tos must be accepted after API call"
        end
      end

      describe 'POST to #accept and saving the account fails' do
        before do
          Account.any_instance.expects(:save).with(validate: false).returns(false)
          @account.errors.add(:base, "there was an error trying to save the account")
          post :accept
        end

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

        it('does not change terms and conditions') do
          assert !@account.reload.settings.allowed_mobile_sdk, "Tos must not be accepted after unsuccesful API call"
        end
      end
    end
  end

  as_an_admin do
    describe "when the account owner is not verified" do
      before do
        @owner = users(:minimum_admin_not_verified)
        @owner.is_verified = false
        @account.stubs(:owner).returns(@owner)
        assert_equal false, @owner.is_verified
      end

      describe 'POST to #accept' do
        before { post :accept }

        it('responds with success') { assert_response :success }
        it('accepts terms and conditions') do
          assert @account.reload.settings.allowed_mobile_sdk, "Tos must be accepted after API call"
        end
      end

      describe 'POST to #accept and saving the account fails' do
        before do
          Account.any_instance.expects(:save).with(validate: false).returns(false)
          @account.errors.add(:base, "there was an error trying to save the account")
          post :accept
        end

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

        it('does not change terms and conditions') do
          assert !@account.reload.settings.allowed_mobile_sdk, "Tos must not be accepted after unsuccesful API call"
        end
      end
    end

    describe "when the account owner is verified" do
      before do
        assert(@account.owner.is_verified)
      end

      describe 'POST to #accept' do
        before { post :accept }

        it('responds with success') { assert_response :success }

        it('accepts terms and conditions') do
          assert @account.reload.settings.allowed_mobile_sdk, "Tos must be accepted after API call"
        end
      end

      describe 'POST to #accept and saving the account fails' do
        before do
          Account.any_instance.expects(:save).with(validate: false).returns(false)
          @account.errors.add(:base, "there was an error trying to save the account")
          post :accept
        end

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

        it('does not change terms and conditions') do
          assert !@account.reload.settings.allowed_mobile_sdk, "Tos must not be accepted after unsuccesful API call"
        end
      end
    end
  end

  as_an_agent do
    describe 'POST to #accept' do
      before { post :accept }

      it('responds with forbidden') { assert_response :forbidden }
    end
  end

  as_an_end_user do
    describe 'POST to #accept' do
      before { post :accept }

      it('responds with forbidden') { assert_response :forbidden }
    end
  end

  as_an_sdk_anonymous_end_user do
    describe 'POST to #accept' do
      before { post :accept }

      it('responds with forbidden') { assert_response :forbidden }
    end
  end

  as_an_sdk_jwt_end_user do
    describe 'POST to #accept' do
      before { post :accept }

      it('responds with forbidden') { assert_response :forbidden }
    end
  end
end
