require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Private::MobileSdk::SettingsV2Controller do
  extend Api::Presentation::TestHelper
  extend MobileSdk::RateLimitHelper

  fixtures :accounts, :brands, :mobile_sdk_apps, :translation_locales, :mobile_sdk_auths, :oauth_clients

  before do
    accept :json
  end

  with_options(controller: "api/private/mobile_sdk/settings_v2", id: "identifier") do |request|
    request.should_route :get, "/api/private/mobile_sdk/settings/identifier", action: "show"
  end

  as_an_anonymous_user do
    let(:account) { @account }
    let(:brand) { brands(:minimum_sdk) }
    let(:mobile_sdk_auth) { mobile_sdk_auths(:minimum_sdk) }

    def stub_hc_brand_locale(brand, status, body = '')
      stub_request(:get, "https://#{brand.subdomain}.zendesk-test.com/hc/api/v2/locales.json").
        with(headers: {'Accept' => 'application/json'}).
        to_return(status: status, body: body.to_json, headers: {'Content-Type' => 'application/json'})
    end

    before do
      account.default_brand = brand
      @mobile_sdk_app = FactoryBot.create(:mobile_sdk_app, account: account, mobile_sdk_auth: mobile_sdk_auth)

      @controller.stubs(:allow_only_mobile_sdk_clients_access).returns(nil)
      @controller.stubs(:allow_only_mobile_sdk_tokens_access).returns(nil)

      @request.user_agent = 'Zendesk SDK for iOS'
      @request.stubs(:ssl?).returns(true)
      @request.account = account
    end

    describe 'a GET to :show' do
      let(:json_response) { JSON.parse(@response.body) }

      before do
        get :show, params: { id: @mobile_sdk_app.identifier }
      end

      should_use_presenter Api::Private::MobileSdk::SettingsPresenter, status: :ok

      it 'strips session set-cookie' do
        assert request.env['rack.session.options'][:skip]
      end
    end

    describe 'Rate Limits' do
      params = [
        :mobile_sdk_limit_medium,
        'mobile_sdk_settings_v2_show',
        :enable_throttle_for_mobile_sdk_settings_v2_show,
        :mobile_sdk_settings_v2_show_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        Prop.expects(:throttle!).with(:api_unauthorized_requests, '0.0.0.0')
        get :show, params: { id: @mobile_sdk_app.identifier }
      end
    end

    describe 'Cache-Control header' do
      describe_with_arturo_enabled :mobile_sdk_settings_cache_control_headers do
        it 'must have the Cache-Control header' do
          get :show, params: { id: @mobile_sdk_app.identifier }

          assert_equal "max-age=600, public", @response.headers['Cache-Control']
        end
      end

      describe_with_arturo_disabled :mobile_sdk_settings_cache_control_headers do
        it 'must not have the Cache-Control header' do
          get :show, params: { id: @mobile_sdk_app.identifier }

          assert_blank @response.headers['Cache-Control']
        end
      end
    end

    describe "Locale parsing" do
      let(:spanish) { translation_locales(:spanish) }
      let(:french) { translation_locales(:french) }
      let(:english) { translation_locales(:english_by_zendesk) }
      let(:presenter_mock) { mock { expects(:present).returns({}) } }

      before do
        account.stubs(:available_languages).returns([english, spanish, french])
      end

      describe "Locale provided on the 'HTTP_ACCEPT_LANGUAGE' header" do
        it "correctly finds the locale and build as presenter passing it as an option" do
          @request.env['HTTP_ACCEPT_LANGUAGE'] = 'es-ES'

          Api::Private::MobileSdk::SettingsPresenter.expects(:new).
            with(anything, has_entries(locale: spanish)).
            returns(presenter_mock)

          get :show, params: { id: @mobile_sdk_app.identifier }
        end
      end

      describe "Locale provided on the 'Accept-Language' header" do
        it "correctly finds the locale and build as presenter passing it as an option" do
          @request.env['Accept-Language'] = 'es-ES'

          Api::Private::MobileSdk::SettingsPresenter.expects(:new).
            with(anything, has_entries(locale: spanish)).
            returns(presenter_mock)

          get :show, params: { id: @mobile_sdk_app.identifier }
        end
      end

      describe "No locale provided on headers and user has a default locale" do
        it "passes the current user's default locale to the presenter" do
          Api::Private::MobileSdk::SettingsPresenter.expects(:new).
            with(anything, has_entries(locale: @controller.send(:current_user).translation_locale)).
            returns(presenter_mock)

          get :show, params: { id: @mobile_sdk_app.identifier }
        end
      end

      describe "No locale provided on headers and user has no default locale" do
        it "passes the current account's default locale to the presenter" do
          current_user = @controller.send(:current_user)
          current_user.stubs(:translation_locale).returns(nil)

          Api::Private::MobileSdk::SettingsPresenter.expects(:new).
            with(anything, has_entries(locale: account.translation_locale)).
            returns(presenter_mock)

          get :show, params: { id: @mobile_sdk_app.identifier }
        end
      end
    end
  end
end
