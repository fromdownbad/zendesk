require_relative "../../../support/test_helper"
require_relative "../../../support/api_test_helper"

SingleCov.covered! uncovered: 7

describe Api::V1::BaseController do
  fixtures :accounts, :translation_locales, :users

  class ApiV1BaseTestController < Api::V1::BaseController
    caches_action :cached

    def slave
      render plain: !!User.on_slave_by_default?
    end

    def cached
      respond_to do |format|
        format.json { render json: [].to_json }
        format.xml  { render xml: [].to_xml }
      end
    end

    def index
      render json: 'cool'
    end

    def i18n
      render plain: I18n.t('txt.email.footer')
    end

    def xml_as_default
      respond_to do |format|
        format.json { render json: [].to_json }
        format.xml  { render xml: [].to_xml }
      end
    end

    def raise_race_condition_error
      raise ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, 'test'
    end

    def failure
      render_failure(status: 410)
    end
  end

  class ApiV1BaseCleanController < Api::V1::BaseController
  end

  tests ApiV1BaseTestController
  use_test_routes

  include ApiTestHelper

  before do
    @current_account = accounts(:minimum)
    @request.account = @current_account
    login(@current_account.owner)
  end

  should_not_have_extra_actions(ApiV1BaseCleanController)
end
