require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 16

describe Api::V1::StatsController do
  fixtures :entries, :accounts, :role_settings, '../files/stat_rollup/ticket_stats'

  before do
    @account = accounts(:minimum)
    @request.account = @account
    @admin = users(:minimum_admin)
    @agent = users(:minimum_agent)
    @user  = users(:minimum_end_user)
    Timecop.freeze # prevent random ci failures
  end

  describe 'routes' do
    # benchmarking
    should_route :get, '/api/v1/stats/benchmarking/industry/education',
      controller: 'api/v1/stats', action: 'benchmarking', object_type: 'industry', object_name: 'education'
    should_route :get, '/api/v1/stats/benchmarking/target_audience/overall',
      controller: 'api/v1/stats', action: 'benchmarking', object_type: 'target_audience', object_name: 'overall'
    # latest
    should_route :get, '/api/v1/stats/latest/account/ticket_stats/backlog',
      controller: 'api/v1/stats', action: 'latest', assoc_name: 'ticket_stats', object_type: 'account', stat_name: 'backlog'
  end
end
