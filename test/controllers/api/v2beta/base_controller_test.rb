require_relative "../../../support/test_helper"
require_relative "../../../support/api_test_helper"

SingleCov.covered! uncovered: 12

describe Api::V2beta::BaseController do
  fixtures :accounts, :users, :translation_locales

  class ApiV2betaTestController < Api::V2beta::BaseController
    def index
      render json: 'cool'
    end

    def i18n
      render plain: I18n.t('txt.email.footer')
    end

    def raise_record_invalid
      user = User.find(params['user_id'])
      user.errors.add(:photo, :blank)
      user.errors.add(:safe_update_timestamp, "Error message in English") if params['safe_update_timestamp']
      raise ActiveRecord::RecordInvalid, user
    end

    def raise_race_condition_error
      raise ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, 'test'
    end
  end

  class ApiV2betaCleanController < Api::V2beta::BaseController
  end

  tests ApiV2betaTestController
  use_test_routes

  include ApiTestHelper

  before do
    accept :json
    @account = @request.account = accounts(:minimum)
  end

  describe "#invalid_request_format?" do
    it "returns true for non-JSON formats" do
      request = stub(format: Mime[:xml], accepts: [])
      @controller.expects(:request).at_least_once.returns(request)
      assert @controller.send(:invalid_request_format?)
    end

    it "returns false for JSON formats" do
      request = stub(format: Mime[:json], accepts: [])
      @controller.expects(:request).at_least_once.returns(request)
      refute @controller.send(:invalid_request_format?)
    end
  end

  describe "#many_ids" do
    describe "when given no :ids parameter" do
      before { @controller.stubs(:params).returns({}) }
      it "returns nil" do
        assert @controller.send(:many_ids).nil?
      end
    end

    describe "when given an :ids parameter" do
      describe "and there are more than page_size values" do
        before { @controller.stubs(:params).returns(ids: Array.new(200, 2).join(",")) }
        it "only return the first page_size" do
          assert_equal Array.new(100, 2), @controller.send(:many_ids)
        end
      end

      describe "and there are less than page_size values" do
        before { @controller.stubs(:params).returns(ids: Array.new(78, 2).join(",")) }
        it "only return the first page_size" do
          assert_equal Array.new(78, 2), @controller.send(:many_ids)
        end
      end
    end
  end

  describe "#invalid_content_type?" do
    it "returns false for non-write requests" do
      request = stub(content_type: Mime[:xml], put?: false, post?: false)
      @controller.stubs(:request).returns(request)
      refute @controller.send(:invalid_content_type?)
    end

    it "returns false for writes of type JSON" do
      request = stub(content_type: Mime[:json], put?: false, post?: true)
      @controller.stubs(:request).returns(request)
      refute @controller.send(:invalid_content_type?)
    end

    it "returns true for writes that are not JSON" do
      request = stub(content_type: Mime[:xml], put?: true, post?: false)
      @controller.stubs(:request).returns(request)
      assert @controller.send(:invalid_content_type?)
    end
  end

  describe "ssl" do
    before do
      accept :json
      login("minimum_agent")
    end

    it "process secure requests" do
      get :index
      assert_response :success
    end

    describe "race condition" do
      before do
        get :raise_race_condition_error
      end

      it "returns conflict response" do
        assert_response :conflict
      end
    end

    should_eventually "forbid non-secure requests" do
      https!(false)
      get :index
      assert_response :forbidden
    end
  end

  describe "authorization" do
    before do
      accept :json
    end

    it "allows agents" do
      login(:minimum_agent)
      get :index

      assert_response :success
    end

    it "forbids end-users" do
      login(:minimum_end_user)
      get :index

      assert_response :forbidden
    end
  end

  it "requires authentication" do
    get :index
    assert_response :unauthorized
  end

  describe "API version" do
    before do
      login(:minimum_agent)
      get :index
      assert_response :success
    end

    should_set_version_header("v2")

    it "does not be deprecated" do
      assert_no_match /deprecated/, @response.headers["X-Zendesk-API-Warn"]
    end
  end

  describe "I18n" do
    let(:portuguese) { translation_locales(:brazilian_portuguese) }
    let(:user) { users(:minimum_agent) }

    before do
      user.update_attribute(:translation_locale, portuguese)
      login(:minimum_agent)
      get :i18n
    end

    it "uses the real translation locale" do
      assert_equal "THIS IS A BRAZILIAN PORTUGUESE EMAIL FOOTER", @response.body
    end

    it "localizes full exception message" do
      params = ActionController::Parameters.new(user_id: user.id, safe_update_timestamp: true)
      get :raise_record_invalid, params: params
      @response.body.must_include I18n.t('activerecord.attributes.user.photo', locale: portuguese)
    end

    it "localizes exception records errors" do
      params = ActionController::Parameters.new(user_id: user.id)
      get :raise_record_invalid, params: params
      @response.body.must_include I18n.t('activerecord.errors.messages.blank', locale: portuguese)
    end
  end

  should_not_have_extra_actions(ApiV2betaCleanController)
end
