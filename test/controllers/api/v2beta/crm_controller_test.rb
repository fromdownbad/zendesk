require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2beta::CrmController do
  fixtures :accounts, :users, :salesforce_integrations

  describe 'routes' do
    should_route :get, '/api/v2beta/crm/10.json',                controller: 'api/v2beta/crm', action: 'show',            id: 10, format: 'json'
    should_route :get, '/api/v2beta/crm/10/sync_user_info.json', controller: 'api/v2beta/crm', action: 'sync_user_info',  id: 10, format: 'json'
  end

  before do
    @request.account = accounts(:minimum)
    Account.any_instance.stubs(:crm_integration).returns(SalesforceIntegration.new)
    SalesforceIntegration.any_instance.stubs(:enabled?).returns(true)
    login('minimum_agent')
    @user = users(:minimum_end_user)
  end

  describe "#get" do
    describe "without intergration enabled" do
      before do
        @request.account.expects(:crm_integration_enabled?).returns(false)
        get :show, params: { id: @user.id, format: 'json' }
      end

      it "denys access" do
        assert_response :forbidden
      end
    end

    it "responds with data when called on a user with salesforce data" do
      @user.salesforce_data = SalesforceData.create(user: @user, sync_status: ExternalUserData::SyncStatus::SYNC_OK, data: {records: [:foo, :bar]})
      @user.salesforce_data.save

      get :show, params: { id: @user.id, format: 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"records" => ["foo", "bar"]}, result)
    end

    it "responds properly when called on a user with stale salesforce data and start sync" do
      @user.salesforce_data = SalesforceData.create(user: @user, sync_status: ExternalUserData::SyncStatus::SYNC_OK)
      @user.salesforce_data.save

      SalesforceData.any_instance.expects(:start_sync)

      get :show, params: { id: @user.id, format: 'json' }
      assert_response :accepted

      result = JSON.parse(@response.body)
      assert_equal("Starting request to sync user.", result["message"])
      assert result.key?("records")
    end
  end

  describe "#sync_user_info" do
    describe "when a sync is pending" do
      before do
        @user.salesforce_data = SalesforceData.create(user: @user, sync_status: ExternalUserData::SyncStatus::SYNC_PENDING)
        @user.salesforce_data.save

        SalesforceData.any_instance.stubs(:start_sync)
      end

      it "responds with a pending state" do
        get :sync_user_info, params: { id: @user.id, format: 'json' }
        assert_response :ok

        result = JSON.parse(@response.body)
        assert_equal("pending", result["state"])
        assert result.key?("records")
      end
    end

    describe "if an error happened on the last fetch" do
      before do
        @user.salesforce_data = SalesforceData.create(user: @user)
        @user.salesforce_data.save
        @user.salesforce_data.sync_errored!(data: [])
      end

      it "responds with failed state" do
        get :sync_user_info, params: { id: @user.id, format: 'json' }
        assert_response :ok

        result = JSON.parse(@response.body)
        assert_equal("failed", result["state"])
      end
    end

    describe "when called after sync finishes" do
      before do
        @user.salesforce_data = SalesforceData.create(user: @user, sync_status: ExternalUserData::SyncStatus::SYNC_OK, data: {records: [:foo, :bar]})
        @user.salesforce_data.save
      end

      it "responds properly with new data and location header" do
        get :sync_user_info, params: { id: @user.id, format: 'json' }
        assert_response :see_other

        result = JSON.parse(@response.body)
        assert_equal("done", result["state"])
        assert_equal(["foo", "bar"], result["records"])
        assert_equal("#{api_v2beta_crm_url(@user)}.json", @response.location)
      end
    end
  end
end
