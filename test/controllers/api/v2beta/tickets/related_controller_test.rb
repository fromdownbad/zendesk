require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2beta::Tickets::RelatedController do
  fixtures :all

  before do
    @current_account = accounts(:minimum)
    @request.account = @current_account
  end

  should_route :get, '/api/v2beta/tickets/12/related', controller: 'api/v2beta/tickets/related', action: 'show', ticket_id: 12

  describe 'Given an api client accepting json' do
    before do
      accept :json
    end

    describe 'Given a logged in user' do
      before do
        login(:minimum_admin)
      end

      describe 'Given a valid ticket id' do
        before do
          @ticket = tickets(:minimum_1)
          get :show, params: { ticket_id: @ticket.nice_id }
        end

        it('responds with success') { assert_response :success }

        it 'returns a hash' do
          result = JSON.parse(@response.body)
          assert_kind_of(Hash, result)
        end
      end

      describe 'Given an invalid ticket id' do
        before do
          get :show, params: { ticket_id: 999999999999 }
        end

        it('responds with not_found') { assert_response :not_found }
      end

      describe 'an archived ticket' do
        before do
          @archived_ticket = tickets(:minimum_5)
          archive_and_delete(@archived_ticket)
          get :show, params: { ticket_id: @archived_ticket.nice_id }
        end

        it "sets fromArchive => true" do
          result = JSON.parse(@response.body)
          assert result['fromArchive']
        end
      end
    end

    describe 'Given a logged in agent that can only view assigned tickets' do
      before do
        login(:minimum_agent)
        @user = users(:minimum_agent)
        @user.restriction_id = RoleRestrictionType.ASSIGNED
        @user.save!
      end

      describe 'Given a ticket not assigned to agent' do
        before do
          @ticket = tickets(:minimum_2)
          get :show, params: { ticket_id: @ticket.nice_id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe 'Given a ticket assigned to agent' do
        before do
          @ticket = tickets(:minimum_1)
          get :show, params: { ticket_id: @ticket.nice_id }
        end

        it('responds with success') { assert_response :success }
      end
    end

    describe 'Given an unauthenticated user' do
      describe '#show' do
        before do
          get :show, params: { ticket_id: 12 }
        end

        it('responds with unauthorized') { assert_response :unauthorized }
      end
    end
  end
end
