require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::Mobile::LookupController do
  fixtures :accounts, :users, :role_settings

  let(:account) { accounts(:minimum) }

  before do
    @request.user_agent = 'Zendesk for iPhone'

    accept :json
  end

  it 'ensures route to be available only on the right subdomain' do
    assert_routing(
      'https://mobile-lookup.zd-test.com/api/mobile/lookup',
      subdomain: "mobile-lookup",
      controller: 'api/mobile/lookup',
      action: 'index',
      format: 'json'
    )

    assert_routing(
      'https://support.zd-test.com/api/mobile/lookup',
      controller: "routing_errors",
      action: "show",
      path: "api/mobile/lookup"
    )
  end

  describe 'exists on local and remote' do
    before do
      lookup_response = '{
        "lookup" : {
          "subdomain": "minimum",
          "url": "https://minimum.zendesk-test.com",
          "name": "Minimum account",
          "agent_logins": [
            {
              "service": "zendesk",
              "url": "https://minimum.zendesk-test.com/access/oauth_mobile"
            },
            {
              "service": "google",
              "url": "https://minimum.zendesk-test.com/access/google",
              "play_url": "https://minimum.zendesk-test.com/access/google_play"
            }
          ]
        }
      }'

      stub_request(:get, "https://#{account.subdomain}.zendesk-test.com/api/mobile/account/lookup.json").
        to_return(status: 200, body: lookup_response, headers: { 'Content-Type' => 'application/json' })
    end

    it 'finds an account by its subdomain' do
      get :index, params: { query: account.subdomain }

      assert_response :ok
      response = JSON.parse(@response.body)

      assert_equal account.subdomain, response['lookup']['subdomain']
      assert_equal account.url(ssl: true), response['lookup']['url']
      assert_equal account.name, response['lookup']['name']
    end

    it 'finds an account by its url' do
      get :index, params: { query: account.url.gsub(/https?:\/\//, '') }

      assert_response :ok
      response = JSON.parse(@response.body)

      assert_equal account.subdomain, response['lookup']['subdomain']
      assert_equal account.url(ssl: true), response['lookup']['url']
      assert_equal account.name, response['lookup']['name']
    end

    it 'finds an account by its host mapping' do
      account.update_attribute(:host_mapping, 'support.other-domain.com')
      get :index, params: { query: account.host_mapping }

      assert_response :ok
      response = JSON.parse(@response.body)

      assert_equal account.subdomain, response['lookup']['subdomain']
      assert_equal account.url(ssl: true), response['lookup']['url']
      assert_equal account.name, response['lookup']['name']
    end

    it 'does not generate `n+1` queries' do
      assert_no_n_plus_one do
        get :index, params: { query: account.subdomain }
      end
    end

    it 'returns not_found when no query is done' do
      get :index

      assert_response :not_found
    end
  end

  describe 'not found on remote req' do
    before do
      stub_request(:get, "https://#{account.subdomain}.zendesk-test.com/api/mobile/account/lookup.json").
        to_return(status: 404, body: '', headers: { 'Content-Type' => 'application/json' })
    end

    it 'returns not_found' do
      get :index, params: { query: account.subdomain }

      assert_response :not_found
    end
  end

  describe 'server error on remote req' do
    before do
      stub_request(:get, "https://#{account.subdomain}.zendesk-test.com/api/mobile/account/lookup.json").
        to_return(status: 500, body: '', headers: { 'Content-Type' => 'application/json' })
    end

    it 'returns internal_server_error' do
      get :index, params: { query: account.subdomain }

      assert_response :internal_server_error
    end
  end

  it 'returns not_found when the account is not found' do
    get :index, params: { query: 'INEXISTING-ACCOUNT' }

    assert_response :not_found
  end
end
