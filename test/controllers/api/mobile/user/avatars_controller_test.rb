require_relative '../../../../support/test_helper'
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::Mobile::User::AvatarsController do
  extend Api::V2::TestHelper

  fixtures :users, :role_settings, :photos

  before do
    @request.user_agent = Zendesk::Accounts::Source::IPHONE
    accept :json
  end

  with_options(controller: 'api/mobile/user/avatars') do |request|
    request.should_route :get, '/api/mobile/user/avatar', action: :show, format: :json
  end

  as_an_agent do
    it "redirects to the user's avatar if it exists" do
      user = users(:photo_user)

      get :show, params: { user_id: user.id }

      assert_redirected_to @controller.url_for(user.photo.path)
    end

    it 'responds with :not_found when the user has no avatar' do
      user = users(:minimum_end_user)

      get :show, params: { user_id: user.id }

      assert_response :not_found
    end

    it 'responds with :not_found when the user does not exist' do
      get :show, params: { user_id: 'NON-EXISTENT-ID' }

      assert_response :not_found
    end
  end
end
