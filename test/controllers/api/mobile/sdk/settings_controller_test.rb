require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Mobile::Sdk::SettingsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :brands, :mobile_sdk_apps, :translation_locales, :mobile_sdk_auths, :oauth_clients

  before do
    accept :json
  end

  with_options(controller: "api/mobile/sdk/settings", id: "identifier") do |request|
    request.should_route :get, "/api/mobile/sdk/settings/identifier", action: "show"
  end

  as_an_anonymous_user do
    let(:account) { accounts(:minimum) }
    let(:brand) { brands(:minimum) }
    let(:mobile_sdk_auth) { mobile_sdk_auths(:minimum_sdk) }

    def stub_hc_brand_locale(brand, status, body = '')
      stub_request(:get, "https://#{brand.subdomain}.zendesk-test.com/hc/api/v2/locales.json").
        with(headers: {'Accept' => 'application/json'}).
        to_return(status: status, body: body.to_json, headers: {'Content-Type' => 'application/json'})
    end

    before do
      account.default_brand = brand
      @mobile_sdk_app = FactoryBot.create(:mobile_sdk_app, account: account, mobile_sdk_auth: mobile_sdk_auth)

      # Set account available languages
      @english = translation_locales(:english_by_zendesk)
      @british_english = translation_locales(:british_english)
      @spanish = translation_locales(:spanish)
      @french = translation_locales(:french)
      account.stubs(:available_languages).returns([@english, @spanish, @french])
      account.stubs(:translation_locale).returns(@english)

      # Disabled this before filter as it's tested independently
      @controller.stubs(:allow_only_mobile_sdk_clients_access).returns(nil)
      @controller.stubs(:allow_only_mobile_sdk_tokens_access).returns(nil)

      @request.user_agent = 'Zendesk SDK for iOS'
      set_header('HTTP_ACCEPT_LANGUAGE', 'fr-CA')
      set_header('http_accept_language.parser', HttpAcceptLanguage::Parser.new(@request.env['HTTP_ACCEPT_LANGUAGE']))
      @request.stubs(:ssl?).returns(true)
      @request.account = account
    end

    describe 'a GET to :show' do
      let(:json_response) { JSON.parse(@response.body) }

      before do
        get :show, params: { id: @mobile_sdk_app.identifier }
      end

      should_use_presenter Api::Mobile::Sdk::SettingsPresenter, status: :ok

      it 'has the sdk model key' do
        assert_not_nil json_response['sdk']
      end

      it 'returns the correct app' do
        assert_equal @mobile_sdk_app.identifier, json_response['sdk']['identifier']
      end

      it 'strips session set-cookie' do
        assert request.env['rack.session.options'][:skip]
      end
    end

    describe 'Cache-Control header' do
      describe_with_arturo_enabled :mobile_sdk_settings_cache_control_headers do
        it 'must have the Cache-Control header' do
          get :show, params: { id: @mobile_sdk_app.identifier }

          assert_equal "max-age=600, public", @response.headers['Cache-Control']
        end
      end

      describe_with_arturo_disabled :mobile_sdk_settings_cache_control_headers do
        it 'must not have the Cache-Control header' do
          get :show, params: { id: @mobile_sdk_app.identifier }

          assert_blank @response.headers['Cache-Control']
        end
      end

      describe 'with missing account' do
        before do
          account.stubs(:is_serviceable?).raises(NoMethodError)
        end

        it 'must have Cache-Control header' do
          get :show, params: { id: '' }
          assert_equal 'max-age=600, public', @response.headers['Cache-Control']
        end

        it 'returns status 404' do
          get :show, params: { id: '' }
          assert_equal 404, @response.status
        end
      end

      describe 'with cancelled account' do
        before do
          account.stubs(:is_serviceable?).returns(false)
        end

        it 'must have Cache-Control header' do
          get :show, params: { id: '' }
          assert_equal 'max-age=600, public', @response.headers['Cache-Control']
        end

        it 'returns status 404' do
          get :show, params: { id: '' }
          assert_equal 404, @response.status
        end
      end
    end

    describe 'with helpcenter_enabled' do
      let(:json_response) { JSON.parse(@response.body) }

      before do
        activate_hc!

        @mobile_sdk_app.settings.helpcenter_enabled = true
        @mobile_sdk_app.save!
        @mobile_sdk_app.reload

        @current_brand = FactoryBot.create(:brand, name: "foo", subdomain: "foo")
        @controller.stubs(:current_brand).returns(@current_brand)
      end

      describe 'and requesting an translated locale' do
        it 'settles the HC locale' do
          set_header('HTTP_ACCEPT_LANGUAGE', 'fr')
          set_header('http_accept_language.parser', HttpAcceptLanguage::Parser.new(@request.env['HTTP_ACCEPT_LANGUAGE']))
          stub_hc_brand_locale(@current_brand, 200, 'locales' => ['en-us', 'fr'], 'default_locale' => 'es')

          get :show, params: { id: @mobile_sdk_app.identifier }
          assert_equal 'fr', json_response['sdk']['help_center']['locale']
        end
      end

      describe 'and requesting a locale that fits the default locale' do
        it 'settles the HC locale in favor of the default_locale' do
          account.stubs(:available_languages).returns([@english, @british_english])
          account.stubs(:translation_locale).returns(@english)
          set_header('HTTP_ACCEPT_LANGUAGE', 'en')
          set_header('http_accept_language.parser', HttpAcceptLanguage::Parser.new(@request.env['HTTP_ACCEPT_LANGUAGE']))
          stub_hc_brand_locale(@current_brand, 200, 'locales' => ['en-gb'], 'default_locale' => 'en-us')

          get :show, params: { id: @mobile_sdk_app.identifier }
          assert_equal 'en-us', json_response['sdk']['help_center']['locale']
        end
      end

      describe 'and requesting a not translated locale' do
        it 'settles the HC locale' do
          set_header('HTTP_ACCEPT_LANGUAGE', 'zh')
          set_header('http_accept_language.parser', HttpAcceptLanguage::Parser.new(@request.env['HTTP_ACCEPT_LANGUAGE']))
          stub_hc_brand_locale(@current_brand, 200, 'locales' => ['en-us', 'fr'], 'default_locale' => 'es')

          get :show, params: { id: @mobile_sdk_app.identifier }
          assert_equal 'es', json_response['sdk']['help_center']['locale']
        end
      end

      describe 'and failed to get locales from HC' do
        it 'settles the HC locale' do
          set_header('HTTP_ACCEPT_LANGUAGE', 'fr')
          set_header('http_accept_language.parser', HttpAcceptLanguage::Parser.new(@request.env['HTTP_ACCEPT_LANGUAGE']))
          stub_hc_brand_locale(@current_brand, 404)

          get :show, params: { id: @mobile_sdk_app.identifier }
          assert_equal 'en-us', json_response['sdk']['help_center']['locale']
        end
      end

      describe 'and failed to get locales from HC and current brand does not have HC' do
        it "it settles to current account 'translation_locale'" do
          brand.stubs(:help_center).returns(nil)
          set_header('HTTP_ACCEPT_LANGUAGE', 'fr')
          set_header('http_accept_language.parser', HttpAcceptLanguage::Parser.new(@request.env['HTTP_ACCEPT_LANGUAGE']))

          stub_hc_brand_locale(@current_brand, 404)
          Brand.any_instance.stubs(:help_center).returns(nil)

          get :show, params: { id: @mobile_sdk_app.identifier }
          assert_equal @english.locale.downcase, json_response['sdk']['help_center']['locale']
        end
      end

      describe "and failed to get locales from HC and current brand does not have HC and current account does not have a 'translation_locale'" do
        it "it settles to fallback local 'en-us'" do
          account.stubs(:translation_locale).returns(nil)
          set_header('HTTP_ACCEPT_LANGUAGE', 'fr')
          set_header('http_accept_language.parser', HttpAcceptLanguage::Parser.new(@request.env['HTTP_ACCEPT_LANGUAGE']))

          stub_hc_brand_locale(@current_brand, 404)

          Brand.any_instance.stubs(:help_center).returns(nil)

          get :show, params: { id: @mobile_sdk_app.identifier }
          assert_equal 'en-us', json_response['sdk']['help_center']['locale']
        end
      end
    end

    describe "account settings" do
      let(:json_response) { JSON.parse(@response.body) }

      describe "attachments enabled" do
        it "should show that" do
          account.stubs(:is_attaching_enabled).returns(true)

          get :show, params: { id: @mobile_sdk_app.identifier }
          assert json_response['account']['attachments']['enabled']
        end
      end

      describe "attachments disabled" do
        it "should show that" do
          account.is_attaching_enabled = false
          account.save

          get :show, params: { id: @mobile_sdk_app.identifier }
          refute json_response['account']['attachments']['enabled']
        end
      end

      describe "attachments size" do
        it "should show attachment limit size in Bytes" do
          Account.any_instance.stubs(:max_attachment_size).returns(rand(2..70).megabytes)

          get :show, params: { id: @mobile_sdk_app.identifier }
          assert_equal account.max_attachment_megabytes.megabytes, json_response['account']['attachments']['max_attachment_size']
        end
      end
    end

    describe 'getting a missing app' do
      before { get :show, params: { id: 'unknownidentifier' } }
      it('responds with not_found') { assert_response :not_found }
    end
  end
end
