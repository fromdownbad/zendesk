require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Mobile::Rules::ViewsController do
  extend Api::V2::TestHelper
  fixtures :rules, :tickets

  before do
    accept :json
    use_ssl
    @controller.stubs(:allow_only_zendesk_mobile_access).returns true

    stub_occam_find(Array(tickets(:minimum_1).id))
  end

  with_options(controller: "api/mobile/rules/views") do |request|
    request.should_route :get, "/api/mobile/views/1/execute", action: "execute", id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :execute, {id: 1}]
  end

  as_an_end_user do
    should_be_forbidden [:get, :execute, {id: 1}]
  end

  as_an_agent do
    let(:view) { rules(:view_for_minimum_agent) }
    describe "a GET to :execute" do
      before { get :execute, params: { id: view.id } }
      should_use_presenter Api::V2::Rules::ViewRowsPresenter
    end

    describe "a GET to :execute with output_columns" do
      describe "with output columns" do
        before do
          get :execute, params: { id: view.id, output_columns: "assigned,assignee,due_date,via" }
        end

        it "includes output_columns" do
          @json = JSON.parse(@response.body)
          row = @json['rows'].map.first
          assert row.key?("via") && row.key?("due_date") && row.key?("assignee_id") && row.key?("assigned")
        end
      end

      describe "with too many output columns" do
        it "raises an error" do
          assert_raise ActiveRecord::ActiveRecordError do
            get :execute, params: { id: "incoming", output_columns: "assigned,assignee,due_date,group,nice_id,updated,updated_assignee,updated_requester,updated_by_type,organization,priority,created,requester,locale_id" }
          end
        end
      end
    end

    describe "a GET to :execute with custom parameters" do
      describe "when passed the custom 'incoming' id" do
        before { get :execute, params: { id: MobileView::INCOMING } }
        should_use_presenter Api::V2::Rules::ViewRowsPresenter
      end

      describe "when passed the custom 'satisfaction' id" do
        before { get :execute, params: { id: MobileView::SATISFACTION } }
        should_use_presenter Api::V2::Rules::ViewRowsPresenter
      end

      describe "when passed the custom 'groups' id" do
        before { get :execute, params: { id: MobileView::GROUPS } }
        should_use_presenter Api::V2::Rules::ViewRowsPresenter
      end

      describe "when passed the custom 'unsolved' id" do
        before { get :execute, params: { id: MobileView::UNSOLVED } }
        should_use_presenter Api::V2::Rules::ViewRowsPresenter
      end
    end
  end
end
