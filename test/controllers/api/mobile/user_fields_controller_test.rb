require_relative "../../../support/test_helper"
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::Mobile::UserFieldsController do
  extend Api::V2::TestHelper

  before do
    accept :json
  end

  with_options(controller: "api/mobile/user_fields") do |request|
    request.should_route :get, "/api/mobile/user_fields.json", action: :index, format: "json"
  end

  as_an_end_user do
    describe 'GET to #index' do
      it "responds with HTTP 450 (Blocked)" do
        get :index
        assert_equal 450, response.status
      end
    end
  end

  as_an_sdk_anonymous_end_user do
    describe 'GET to #index' do
      it 'responds with 200 OK' do
        get :index
        assert_equal 200, response.status
      end
    end
  end

  as_an_sdk_jwt_end_user do
    describe 'GET to #index' do
      it 'responds with 200 OK' do
        get :index
        assert_equal 200, response.status
      end
    end
  end
end
