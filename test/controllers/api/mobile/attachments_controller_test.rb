require_relative '../../../support/test_helper'
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::Mobile::AttachmentsController do
  extend Api::V2::TestHelper

  fixtures :attachments

  before do
    @request.user_agent = Zendesk::Accounts::Source::IPHONE
    accept :json
  end

  with_options(controller: 'api/mobile/attachments') do |request|
    request.should_route :get, '/api/mobile/attachment/thumbnail', action: :show, format: :json
  end

  as_an_agent do
    describe 'searching by filename' do
      it "redirects to the attachment's thumbnail if it exists" do
        attachment = attachments(:attachment_entry)
        thumbnail = attachment.thumbnails.first

        get :show, params: { token: attachment.token, filename: attachment.filename }

        assert_redirected_to attachment_by_token_path(id: thumbnail.token, name: thumbnail.filename)
      end
    end

    describe 'searching by display_filename' do
      it "redirects to the attachment's thumbnail if it exists" do
        attachment = attachments(:attachment_entry)
        thumbnail = attachment.thumbnails.first
        displayed_filename = CGI.escape(attachment.display_filename)

        get :show, params: { token: attachment.token, filename: displayed_filename }

        assert_redirected_to attachment_by_token_path(id: thumbnail.token, name: thumbnail.filename)
      end

      it "finds attachments with spaces in the filename" do
        attachment = attachments(:name_with_spaces)
        thumbnail = attachment.thumbnails.first
        displayed_filename = CGI.escape(attachment.display_filename)

        get :show, params: { token: attachment.token, filename: displayed_filename }

        assert_redirected_to attachment_by_token_path(id: thumbnail.token, name: thumbnail.filename)
      end
    end

    it 'responds with :not_found when the attachment has no thumbnail' do
      attachment = attachments(:foreign_file_attachment)

      get :show, params: { token: attachment.token, filename: attachment.filename }

      assert_response :not_found
    end

    it 'responds with :not_found when the attachment does not exist' do
      get :show, params: { token: 'NON-EXISTENT-TOKEN', filename: 'NON-EXISTENT-FILE' }

      assert_response :not_found
    end
  end
end
