require_relative "../../../support/test_helper"
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::Mobile::UserTagsController do
  extend Api::V2::TestHelper
  extend MobileSdk::RateLimitHelper

  fixtures :accounts, :custom_field_options, :organizations, :tickets, :ticket_fields, :users,
    :role_settings, :organization_memberships, :mobile_sdk_apps

  before do
    accept :json
  end

  with_options(controller: "api/mobile/user_tags") do |request|
    request.should_route :post, "/api/mobile/user_tags.json", action: :create, format: "json"
    request.should_route :delete, "/api/mobile/user_tags/destroy_many.json", action: :destroy_many, format: "json"
  end

  as_an_end_user do
    let(:params) do
      { tags: ["foo", "bar"] }
    end

    describe "POST to #create" do
      it "responds with HTTP 450 (Blocked)" do
        post :create, params: params
        assert_equal 450, response.status
      end
    end

    describe "DELETE to #destroy_many" do
      it "responds with HTTP 450 (Blocked)" do
        delete :destroy_many, params: params
        assert_equal 450, response.status
      end
    end
  end

  as_an_sdk_anonymous_end_user do
    describe "POST to #create" do
      [{tags: "foo,bar"}, {tags: ["foo", "bar"]}].each do |create_params|
        before do
          post :create, params: create_params
        end

        it "attaches the tags to the user" do
          @user.reload
          assert_equal ["foo", "bar"], @user.tags
        end

        params = [
          :mobile_sdk_limit_high,
          'mobile_sdk_user_tags_create',
          :enable_throttle_for_mobile_sdk_user_tags_create,
          :mobile_sdk_user_tags_create_emergency_limit
        ]

        it_throttles_mobile_sdk_requests *params do
          post :create, params: create_params
        end

        describe "HTTP Interaction" do
          before do
            post :create, params: create_params
          end

          it "responds with 200 OK" do
            assert_equal 200, response.status
          end

          describe "the response" do
            before { @json = JSON.parse(response.body)["user"] }

            it 'sends the correct set of tags' do
              assert_equal ["foo", "bar"], @json["tags"]
            end
          end
        end
      end

      describe "Already existent tags" do
        it "does not repeat the tags that the use already has" do
          @user.update_attributes!(additional_tags: ["foo, bar"])
          post :create, params: { tags: ["foo"] }
          @user.reload
          assert_equal ["foo", "bar"], @user.tags
        end
      end

      describe "Bad Parameters" do
        [{}, {tags: ""}, {tags: []}].each do |create_params|
          it "should respond with HTTP 400" do
            post :create, params: create_params
            assert_equal 400, response.status
          end
        end
      end

      describe "Account has user tags disabled" do
        [{tags: "foo,bar"}, {tags: ["foo", "bar"]}].each do |create_params|
          before do
            @account.settings.has_user_tags = false
            @account.settings.save!
          end

          it "should respond with HTTP 422" do
            post :create, params: create_params
            assert_equal 422, response.status
          end

          it "should respond with correct error payload" do
            post :create, params: create_params
            json = JSON.parse(response.body)
            assert_equal "UserTagsDisabled", json["error"]
            assert_equal "User tags are disabled. Enable the feature in order to add/remove tags.", json["description"]
          end
        end
      end
    end

    describe "DELETE to #destroy_many" do
      [{tags: "foo,bar"}, {tags: ["foo", "bar"]}].each do |destroy_params|
        before do
          @user.update_attributes!(additional_tags: ["foo, bar, baz, quux"])
          delete :destroy_many, params: destroy_params
        end

        it "removes the tags from the user" do
          assert_equal ["baz", "quux"], @user.tags
        end

        params = [
          :mobile_sdk_limit_low,
          'mobile_sdk_user_tags_destroy_many',
          :enable_throttle_for_mobile_sdk_user_tags_destroy_many,
          :mobile_sdk_user_tags_destroy_many_emergency_limit
        ]

        it_throttles_mobile_sdk_requests *params do
          delete :destroy_many, params: destroy_params
        end

        describe "HTTP Interaction" do
          it "responds with 200 OK" do
            assert_equal 200, response.status
          end

          describe "the response" do
            before { @json = JSON.parse(response.body)["user"] }

            it 'sends the correct set of tags' do
              assert_equal ["baz", "quux"], @json["tags"]
            end
          end
        end
      end

      describe "Bad Parameters" do
        [{}, {tags: ""}, {tags: []}].each do |destroy_params|
          it "should respond with HTTP 400" do
            delete :destroy_many, params: destroy_params
            assert_equal 400, response.status
          end
        end
      end

      describe "Account has user tags disabled" do
        [{tags: "foo,bar"}, {tags: ["foo", "bar"]}].each do |destroy_params|
          before do
            @account.settings.has_user_tags = false
            @account.settings.save!
          end

          it "should respond with HTTP 422" do
            delete :destroy_many, params: destroy_params
            assert_equal 422, response.status
          end

          it "should respond with correct error payload" do
            delete :destroy_many, params: destroy_params
            json = JSON.parse(response.body)
            assert_equal "UserTagsDisabled", json["error"]
            assert_equal "User tags are disabled. Enable the feature in order to add/remove tags.", json["description"]
          end
        end
      end
    end
  end

  as_an_sdk_jwt_end_user do
    describe "POST to #create" do
      [{tags: "foo,bar"}, {tags: ["foo", "bar"]}].each do |create_params|
        before do
          post :create, params: create_params
        end

        params = [
          :mobile_sdk_limit_high,
          'mobile_sdk_user_tags_create',
          :enable_throttle_for_mobile_sdk_user_tags_create,
          :mobile_sdk_user_tags_create_emergency_limit
        ]

        it_throttles_mobile_sdk_requests *params do
          post :create, params: create_params
        end

        it "attaches the tags to the user" do
          @user.reload
          assert_equal ["foo", "bar"], @user.tags
        end

        describe "HTTP Interaction" do
          before do
            post :create, params: create_params
          end

          it "responds with 200 OK" do
            assert_equal 200, response.status
          end

          describe "the response" do
            before { @json = JSON.parse(response.body)["user"] }

            it 'sends the correct set of tags' do
              assert_equal ["foo", "bar"], @json["tags"]
            end
          end
        end
      end

      describe "Already existent tags" do
        it "does not repeat the tags that the use already has" do
          @user.update_attributes!(additional_tags: ["foo, bar"])
          post :create, params: { tags: ["foo"] }
          @user.reload
          assert_equal ["foo", "bar"], @user.tags
        end
      end

      describe "Bad Parameters" do
        [{}, {tags: ""}, {tags: []}].each do |create_params|
          it "should respond with HTTP 400" do
            post :create, params: create_params
            assert_equal 400, response.status
          end
        end
      end

      describe "Account has user tags disabled" do
        [{tags: "foo,bar"}, {tags: ["foo", "bar"]}].each do |create_params|
          before do
            @account.settings.has_user_tags = false
            @account.settings.save!
          end

          it "should respond with HTTP 422" do
            post :create, params: create_params
            assert_equal 422, response.status
          end

          it "should respond with correct error payload" do
            post :create, params: create_params
            json = JSON.parse(response.body)
            assert_equal "UserTagsDisabled", json["error"]
            assert_equal "User tags are disabled. Enable the feature in order to add/remove tags.", json["description"]
          end
        end
      end
    end

    describe "DELETE to #destroy_many" do
      [{tags: "foo,bar"}, {tags: ["foo", "bar"]}].each do |destroy_params|
        before do
          @user.update_attributes!(additional_tags: ["foo, bar, baz, quux"])
          delete :destroy_many, params: destroy_params
        end

        it "removes the tags from the user" do
          assert_equal ["baz", "quux"], @user.tags
        end

        params = [
          :mobile_sdk_limit_low,
          'mobile_sdk_user_tags_destroy_many',
          :enable_throttle_for_mobile_sdk_user_tags_destroy_many,
          :mobile_sdk_user_tags_destroy_many_emergency_limit
        ]

        it_throttles_mobile_sdk_requests *params do
          delete :destroy_many, params: destroy_params
        end

        describe "HTTP Interaction" do
          it "responds with 200 OK" do
            assert_equal 200, response.status
          end

          describe "the response" do
            before { @json = JSON.parse(response.body)["user"] }

            it 'sends the correct set of tags' do
              assert_equal ["baz", "quux"], @json["tags"]
            end
          end
        end
      end

      describe "Bad Parameters" do
        [{}, {tags: ""}, {tags: []}].each do |destroy_params|
          it "should respond with HTTP 400" do
            delete :destroy_many, params: destroy_params
            assert_equal 400, response.status
          end
        end
      end

      describe "Account has user tags disabled" do
        [{tags: "foo,bar"}, {tags: ["foo", "bar"]}].each do |destroy_params|
          before do
            @account.settings.has_user_tags = false
            @account.settings.save!
          end

          it "should respond with HTTP 422" do
            delete :destroy_many, params: destroy_params
            assert_equal 422, response.status
          end

          it "should respond with correct error payload" do
            delete :destroy_many, params: destroy_params
            json = JSON.parse(response.body)
            assert_equal "UserTagsDisabled", json["error"]
            assert_equal "User tags are disabled. Enable the feature in order to add/remove tags.", json["description"]
          end
        end
      end
    end
  end
end
