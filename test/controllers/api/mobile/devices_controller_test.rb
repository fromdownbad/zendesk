require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 18

describe Api::Mobile::DevicesController do
  # tested via test/integration/api/mobile/devices_integration_test.rb
  with_options(controller: 'api/mobile/devices') do |request|
    request.should_route :post, '/api/mobile/devices/register', action: 'register'
  end
end
