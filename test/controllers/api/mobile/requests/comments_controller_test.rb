require_relative "../../../../support/test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"
require "zendesk/o_auth/testing/token_factory"

SingleCov.covered!

describe Api::Mobile::Requests::CommentsController do
  extend Api::V2::TestHelper
  extend MobileSdk::RateLimitHelper

  fixtures :accounts, :custom_field_options, :organizations, :tickets, :ticket_fields, :users,
    :role_settings, :organization_memberships, :mobile_sdk_apps, :events, :mobile_sdk_auths, :oauth_clients

  let(:comment) { events(:create_comment_for_mininum_sdk_minimum_sdk_ticket) }
  let(:sdk_request) { comment.ticket }
  let(:sdk_request_token) { RequestToken.create(ticket: sdk_request) }
  let(:expected_tags) { ["foo"] }

  before do
    @request.user_agent = "Zendesk SDK for iOS"

    accept :json

    comment.author.update_attribute(:additional_tags, "foo")
  end

  with_options(controller: "api/mobile/requests/comments") do |request|
    request.should_route :get, "/api/mobile/requests/1/comments", action: :index, request_id: 1
    request.should_route :get, "/api/mobile/requests/1/comments/1", action: :show, request_id: 1, id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index, request_id: 1]
  end

  as_an_sdk_anonymous_end_user do
    describe "GET to #index" do
      before do
        get :index, params: { request_id: sdk_request_token.value }
      end

      it('responds with ok') { assert_response :ok }

      params = [
        :mobile_sdk_limit_high,
        "mobile_sdk_comments_index",
        :enable_throttle_for_mobile_sdk_comments_index,
        :mobile_sdk_comments_index_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        get :index, params: { request_id: sdk_request_token.value }
      end

      should_use_presenter Api::Mobile::Requests::CommentPresenter

      describe "the response" do
        before { @json = JSON.parse(response.body)["comments"].first }

        it 'uses the token as the id' do
          assert_equal sdk_request_token.value, @json["request_id"]
        end

        it 'uses the token in the url' do
          assert_equal "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{sdk_request_token.value}/comments/#{@json["id"]}.json", @json["url"]
        end
      end

      describe "The 'users' field" do
        before do
          @json = JSON.parse(response.body)["users"]
        end

        it 'includes the users related to the request' do
          assert @json
        end

        it 'includes the tags for each user' do
          assert_equal expected_tags, @json.first["tags"]
        end
      end
    end

    # A security risk was found where end users with unverified email addresses
    # had access to tickets coming from these unverified email addresses
    # potentially allowing hackers to access other companies via password reset
    # emails.
    #
    # A fix was made https://github.com/zendesk/zendesk/pull/29682 under the
    # 'restrict_end_users_with_unverified_emails' Arturo feature by the secdev
    # team which fixes the vulnerability by preventing the access to the requests
    # endpoint from users with unverified email identities created after 17-09-2017.
    # This same fix caused a side effect of preventing a subset of
    # SDK anonymous users (ones with unverified email user identities) to access
    # the mobile SDK requests endpoint breaking the SDK.
    # A temporarly fix was made skipping the 'ensure_end_user_email_is_verified'
    # and these tests ensure that the SDK continues working on future code changes.
    #
    # We will be skipping the checks only if the
    # 'mobile_sdk_skip_unverified_emails_check' arturo feature is enabled.
    describe "With an unverified email identity" do
      before do
        Arturo.enable_feature! :mobile_sdk_skip_unverified_emails_check

        email_identity = @user.identities.email.last
        email_identity.update_attributes(
          created_at: Date.new(2017, 9, 18),
          is_verified: false
        )

        email_identity.save!
      end

      describe "GET to #index" do
        before do
          get :index, params: { request_id: sdk_request_token.value }
        end

        it('responds with ok') { assert_response :ok }

        should_use_presenter Api::Mobile::Requests::CommentPresenter

        describe "the response" do
          before { @json = JSON.parse(response.body)["comments"].first }

          it 'uses the token as the id' do
            assert_equal sdk_request_token.value, @json["request_id"]
          end

          it 'uses the token in the url' do
            assert_equal "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{sdk_request_token.value}/comments/#{@json["id"]}.json", @json["url"]
          end
        end

        describe "The 'users' field" do
          before do
            @json = JSON.parse(response.body)["users"]
          end

          it 'includes the users related to the request' do
            assert @json
          end

          it 'includes the tags for each user' do
            assert_equal expected_tags, @json.first["tags"]
          end
        end
      end
    end
  end

  as_an_sdk_jwt_end_user do
    describe "GET to #index" do
      before do
        get :index, params: { request_id: sdk_request_token.value }
      end

      it('responds with ok') { assert_response :ok }

      params = [
        :mobile_sdk_limit_high,
        "mobile_sdk_comments_index",
        :enable_throttle_for_mobile_sdk_comments_index,
        :mobile_sdk_comments_index_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        get :index, params: { request_id: sdk_request_token.value }
      end

      should_use_presenter Api::Mobile::Requests::CommentPresenter

      describe "the response" do
        before { @json = JSON.parse(response.body)["comments"].first }

        it 'uses the token as the id' do
          assert_equal sdk_request_token.value, @json["request_id"]
        end

        it 'uses the token in the url' do
          assert_equal "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{sdk_request_token.value}/comments/#{@json["id"]}.json", @json["url"]
        end
      end

      describe "The 'users' field" do
        before do
          @json = JSON.parse(response.body)["users"]
        end

        it 'includes the users related to the request' do
          assert @json
        end

        it 'includes the tags for each user' do
          assert_equal expected_tags, @json.first["tags"]
        end
      end
    end

    describe "GET to #index with a non-existent ticket" do
      before do
        get :index, params: { request_id: "abcdef" }
      end

      it('responds with 404') { assert_response :not_found }
    end

    describe "GET to #show" do
      before do
        get :show, params: { request_id: sdk_request_token.value, id: comment.id }
      end

      it('responds with ok') { assert_response :ok }

      should_use_presenter Api::Mobile::Requests::CommentPresenter

      describe "the response" do
        before { @json = JSON.parse(response.body)["comment"] }

        it 'uses the token as the id' do
          assert_equal sdk_request_token.value, @json["request_id"]
        end

        it 'uses the token in the url' do
          assert_equal "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{sdk_request_token.value}/comments/#{@json["id"]}.json", @json["url"]
        end
      end

      describe "The 'users' field" do
        before do
          @json = JSON.parse(response.body)["users"]
        end

        it 'includes the users related to the request' do
          assert @json
        end

        it 'includes the tags for each user' do
          assert_equal expected_tags, @json.first["tags"]
        end
      end
    end

    # A security risk was found where end users with unverified email addresses
    # had access to tickets coming from these unverified email addresses
    # potentially allowing hackers to access other companies via password reset
    # emails.
    #
    # A fix was made https://github.com/zendesk/zendesk/pull/29682 under the
    # 'restrict_end_users_with_unverified_emails' Arturo feature by the secdev
    # team which fixes the vulnerability by preventing the access to the requests
    # endpoint from users with unverified email identities created after 17-09-2017.
    # This same fix caused a side effect of preventing a subset of
    # SDK anonymous users (ones with unverified email user identities) to access
    # the mobile SDK requests endpoint breaking the SDK.
    # A temporarly fix was made skipping the 'ensure_end_user_email_is_verified'
    # and these tests ensure that the SDK continues working on future code changes.
    #
    # We will be skipping the checks only if the
    # 'mobile_sdk_skip_unverified_emails_check' arturo feature is enabled.
    describe "With an unverified email identity" do
      before do
        Arturo.enable_feature! :mobile_sdk_skip_unverified_emails_check

        email_identity = @user.identities.email.last
        email_identity.update_attributes(
          created_at: Date.new(2017, 9, 18),
          is_verified: false
        )

        email_identity.save!
      end

      describe "GET to #index" do
        before do
          get :index, params: { request_id: sdk_request_token.value }
        end

        it('responds with ok') { assert_response :ok }

        should_use_presenter Api::Mobile::Requests::CommentPresenter

        describe "the response" do
          before { @json = JSON.parse(response.body)["comments"].first }

          it 'uses the token as the id' do
            assert_equal sdk_request_token.value, @json["request_id"]
          end

          it 'uses the token in the url' do
            assert_equal "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{sdk_request_token.value}/comments/#{@json["id"]}.json", @json["url"]
          end
        end

        describe "The 'users' field" do
          before do
            @json = JSON.parse(response.body)["users"]
          end

          it 'includes the users related to the request' do
            assert @json
          end

          it 'includes the tags for each user' do
            assert_equal expected_tags, @json.first["tags"]
          end
        end
      end

      describe "GET to #show" do
        before do
          get :show, params: { request_id: sdk_request_token.value, id: comment.id }
        end

        it('responds with ok') { assert_response :ok }

        should_use_presenter Api::Mobile::Requests::CommentPresenter

        describe "the response" do
          before { @json = JSON.parse(response.body)["comment"] }

          it 'uses the token as the id' do
            assert_equal sdk_request_token.value, @json["request_id"]
          end

          it 'uses the token in the url' do
            assert_equal "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{sdk_request_token.value}/comments/#{@json["id"]}.json", @json["url"]
          end
        end

        describe "The 'users' field" do
          before do
            @json = JSON.parse(response.body)["users"]
          end

          it 'includes the users related to the request' do
            assert @json
          end

          it 'includes the tags for each user' do
            assert_equal expected_tags, @json.first["tags"], @response.body
          end
        end
      end
    end
  end
end
