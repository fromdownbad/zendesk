require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Api::Mobile::RequestsController do
  extend Api::V2::TestHelper
  extend MobileSdk::RateLimitHelper

  fixtures :accounts, :tickets, :mobile_sdk_apps, :mobile_sdk_auths, :user_identities, :ticket_fields, :role_settings, :sequences, :oauth_clients

  before do
    accept :json
  end

  with_options(controller: "api/mobile/requests") do |request|
    request.should_route :get, "/api/mobile/requests",           action: :index
    request.should_route :post, "/api/mobile/requests",          action: :create
    request.should_route :get, "/api/mobile/requests/1",         action: :show, id: 1
    request.should_route :put, "/api/mobile/requests/1",         action: :update, id: 1
    request.should_route :get, "/api/mobile/requests/show_many", action: :show_many
    request.should_route :get, "/api/mobile/requests/count",     action: :count
  end

  as_an_anonymous_user do
    # check other inherit actions from api/v2/requests
    should_be_unauthorized :index, :show, :show_many
  end

  # Parameters
  let(:status_params) { "new,open,pending,on-hold,solved,closed,archived" }
  let(:update_params) do
    { comment: { body: "I have an update" } }
  end
  let(:create_params) do
    {
      request: {
        subject: "Help!",
        comment: { body: "My printer is on fire! Test SDK" }
      }
    }
  end
  def tokens_params(tokens)
    tokens.map(&:value).join(",")
  end

  let(:account) { accounts(:minimum_sdk) }
  let(:ticket) { tickets(:minimum_mobile_sdk) }
  let(:token) { RequestToken.create!(ticket: ticket) }
  let(:many_tickets) { create_tickets(10, account: account, requester: users(:minimum_sdk_end_user3)) }
  let(:many_tokens) { many_tickets.map { |t| RequestToken.create(ticket: t) } }
  let(:user) { users(:minimum_sdk_end_user) }
  let(:other_user) { users(:minimum_sdk_end_user2) }
  let(:other_user_ticket) { create_ticket(account: account, requester: other_user) }
  let(:other_user_token) { RequestToken.create!(ticket: other_user_ticket) }

  as_an_sdk_anonymous_end_user do
    let(:normal_tickets) { create_tickets(5, account: @account, requester: @user, via_id: ViaType.MAIL) }
    let(:response_json) { JSON.parse(response.body, symbolize_names: true)[:request] }

    params = [
      :mobile_sdk_limit_low,
      'mobile_sdk_requests_create',
      :enable_throttle_for_mobile_sdk_requests_create,
      :mobile_sdk_requests_create_emergency_limit
    ]

    it_throttles_mobile_sdk_requests *params do
      Prop.expects(:throttle!).with(:portal_tickets, user.id, threshold: 40)
      post :create, params: create_params
    end

    describe "POST to #create" do
      before do
        @device_identifier = PushNotifications::SdkAnonymousDeviceIdentifier.create! do |sadi|
          sadi.token = "foo"
          sadi.device_type = "iphone"
          sadi.account = account
          sadi.user_sdk_identity = @user_sdk_identity
          sadi.mobile_sdk_app = @mobile_sdk_app
        end

        post :create, params: create_params
      end

      it("responds with created") { assert_response :created }

      it "returns the request's token as the request 'id'" do
        assert_match(/^[a-zA-Z0-9]*$/, response_json[:id])
      end

      it "links the ticket to the user's device_identifier" do
        ticket = account.request_tokens.find_by_value!(response_json[:id]).ticket
        assert_equal ticket.sdk_anonymous_device_identifiers.first, @device_identifier
      end
    end

    describe "GET to #show" do
      let(:response_json) { JSON.parse(response.body, symbolize_names: true)[:request] }

      params = [
        :mobile_sdk_limit_low,
        'mobile_sdk_requests_show',
        :enable_throttle_for_mobile_sdk_requests_show,
        :mobile_sdk_requests_show_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        get :show, params: { id: token.value }
      end

      describe "sdk ticket request" do
        before { get :show, params: { id: token.value } }

        it("responds with ok") { assert_response :ok }

        should_use_presenter Api::Mobile::RequestPresenter

        describe "the response" do
          it "uses the token as the id" do
            assert_equal token.value, response_json[:id]
          end

          it "uses the token in the url" do
            assert_equal "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{token.value}.json", response_json[:url]
          end
        end
      end

      describe "non-sdk ticket request" do
        before do
          @ticket = normal_tickets.sample
          get :show, params: { id: @ticket.nice_id }
        end

        it("responds with ok") { assert_response :ok }

        should_use_presenter Api::Mobile::RequestPresenter

        describe "the response" do
          it "uses the nice_id as the id" do
            assert_equal @ticket.nice_id.to_s, response_json[:id]
          end

          it "uses the token in the url" do
            assert_equal "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{@ticket.nice_id}.json", response_json[:url]
          end
        end
      end

      describe "with a ticket that don't belong to the user" do
        it "returns not found" do
          get :show, params: { id: other_user_token.value }

          assert_response :not_found
        end

        it "does not destroy tokens from other users tickets" do
          get :show, params: { id: other_user_token.value }

          refute_nil RequestToken.find_by_id(other_user_token.id)
        end

        it "destroys orphan tokens" do
          other_user_ticket.delete

          get :show, params: { id: other_user_token.value }

          assert_nil RequestToken.find_by_id(other_user_token.id)
        end
      end

      describe "with a deleted ticket" do
        before do
          ticket.delete
          get :show, params: { id: token.value }
        end

        it("returns a 404 not found") { assert_response :not_found }

        it("deletes the request token") { assert_nil RequestToken.find_by_value(token) }
      end

      describe "satisfaction ratings" do
        let(:response_json) { JSON.parse(response.body, symbolize_names: true)[:request] }
        let (:bad_csat_params) { { score: "bad" } }

        before do
          @ticket = normal_tickets.sample
          Zendesk::Tickets::Initializer.set_rating(@ticket, bad_csat_params)
          @ticket.will_be_saved_by(user, via_id: ViaType.WEB_SERVICE)
          @ticket.current_user = user # Keep ticket validations happy
          @ticket.save!
        end

        it "contains csat when enabled" do
          MobileSdkApp.any_instance.stubs(:support_show_csat?).returns(true)
          get :show, params: { id: @ticket.nice_id }
          assert response_json[:csat], "Response should have a csat field when enabled in settings"
        end

        it "contains and empty csat when disabled" do
          MobileSdkApp.any_instance.stubs(:support_show_csat?).returns(false)
          get :show, params: { id: @ticket.nice_id }
          assert_nil response_json[:csat], "Response should have an empty csat field when disabled in settings"
        end
      end
    end

    describe "GET to #show_many" do
      let(:many_tickets) { @account.tickets.where(requester_id: [@user, other_user]) }
      let(:many_tokens) { many_tickets.map { |ticket| RequestToken.create!(ticket: ticket) } }
      let(:user_tokens) { @user.tickets.map { |ticket| ticket.request_token.value } }
      let(:response_json) { JSON.parse(response.body, symbolize_names: true)[:requests] }
      let(:found_tokens) { response_json.map { |r| r[:id] } }

      describe "presenter" do
        before { get :show_many, params: { tokens: tokens_params(many_tokens), status: status_params } }

        should_use_presenter Api::Mobile::RequestPresenter
      end

      it "responds with ok" do
        get :show_many, params: { tokens: tokens_params(many_tokens), status: status_params }

        assert_response :ok
      end

      it "responds with 400 (bad_request) when 'status' parameter is not present" do
        get :show_many, params: { tokens: tokens_params(many_tokens) }

        assert_response :bad_request
      end

      it "responds with 400 (bad_request) when 'tokens' parameter is not present" do
        get :show_many, params: { status: status_params }

        assert_response :bad_request
      end

      params = [
        :mobile_sdk_limit_high,
        'mobile_sdk_requests_show_many',
        :enable_throttle_for_mobile_sdk_requests_show_many,
        :mobile_sdk_requests_show_many_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        get :show_many, params: { tokens: tokens_params(many_tokens), status: status_params }
      end

      describe "the response" do
        before { get :show_many, params: { tokens: tokens_params(many_tokens), status: status_params } }

        let(:found_tokens) { response_json.map { |r| r[:id] } }

        it "uses the token as the id" do
          assert_same_elements user_tokens, found_tokens
        end

        it "does not returns other users requests" do
          refute_includes found_tokens, other_user_token
        end

        it "uses the token in the url" do
          expected_urls = user_tokens.map { |token| "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{token}.json" }
          actual_urls = response_json.map { |r| r[:url] }

          assert_same_elements expected_urls, actual_urls
        end
      end

      describe_with_arturo_enabled :mobile_requests_ordering_fix do
        describe "respect the required sorting params" do
          let!(:tickets) { @user.tickets.first(5) }
          let(:ordered_tokens) { tickets.sort_by(&:created_at).map(&:token) }
          let(:tokens) { tickets.map(&:token).join(",") }

          before do
            tickets.each_with_index do |ticket, index|
              ticket.update_column(:created_at, index.month.ago)
              RequestToken.create!(ticket: ticket)
            end
          end

          it "orders by created_at asc" do
            get :show_many, params: { tokens: tokens, sort_order: "asc", sort_by: "created_at", status: status_params }

            assert_equal ordered_tokens, found_tokens
          end

          it "orders by created_at desc" do
            get :show_many, params: { tokens: tokens, sort_order: "desc", sort_by: "created_at", status: status_params }

            assert_equal ordered_tokens.reverse, found_tokens
          end
        end
      end

      describe "Show closed requests functionality" do
        let(:closed_ticket) { tickets(:minimum_mobile_sdk_closed) }
        let(:closed_ticket_token) { RequestToken.create(ticket: closed_ticket, account: account) }

        describe "when the mobile sdk app has not disabled showing closed requests" do
          before do
            MobileSdkApp.any_instance.stubs(:support_show_closed_requests?).returns(true)

            get :show_many, params: { tokens: closed_ticket_token.value, status: "closed" }
          end

          it "includes the closed tickets" do
            assert_includes found_tokens, closed_ticket_token.value
          end
        end

        describe "when the mobile sdk app has disabled showing closed requests" do
          before do
            MobileSdkApp.any_instance.stubs(:support_show_closed_requests?).returns(false)

            get :show_many, params: { tokens: closed_ticket_token.value, status: "closed" }
          end

          it "does not include the closed tickets" do
            refute_includes found_tokens, closed_ticket_token.value
          end
        end
      end

      describe "satisfaction ratings" do
        let(:response_json) { JSON.parse(response.body, symbolize_names: true)[:requests] }
        let(:bad_csat_params) { { score: "bad" } }

        before do
          ticket = normal_tickets.sample
          Zendesk::Tickets::Initializer.set_rating(ticket, bad_csat_params)
          ticket.will_be_saved_by(user, via_id: ViaType.WEB_SERVICE)
          ticket.current_user = user # Keep ticket validations happy
          ticket.save!
          @token = RequestToken.create(ticket: ticket)
        end

        it "contains csat when enabled" do
          MobileSdkApp.any_instance.stubs(:support_show_csat?).returns(true)
          get :show_many, params: { tokens: @token.value, status: status_params }
          assert response_json.first[:csat], "Response should have a csat field when enabled in settings"
        end

        it "contains an empty csat when disabled" do
          MobileSdkApp.any_instance.stubs(:support_show_csat?).returns(false)
          get :show_many, params: { tokens: @token.value, status: status_params }
          assert_nil response_json.first[:csat], "Response should not have a csat field when disabled in settings"
        end
      end
    end

    describe "PUT to #update" do
      let(:response_json) { JSON.parse(response.body, symbolize_names: true)[:request] }

      params = [
        :mobile_sdk_limit_low,
        'mobile_sdk_requests_update',
        :enable_throttle_for_mobile_sdk_requests_update,
        :mobile_sdk_requests_update_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        Prop.expects(:throttle!).with(:ticket_updates, [token.ticket.id, user.id], threshold: 30)
        put :update, params: { id: token.value, request: update_params }
      end

      describe "sdk-ticket request" do
        before { put :update, params: { id: token.value, request: update_params } }

        it("responds with ok") { assert_response :ok }

        should_use_presenter Api::Mobile::RequestPresenter

        describe "the response" do
          it("uses the token as the id") { assert_equal token.value, response_json[:id] }
        end
      end

      describe "no-sdk-ticket request" do
        before do
          @ticket = normal_tickets.sample
          put :update, params: { id: @ticket.nice_id, request: update_params }
        end

        it("responds with ok") { assert_response :ok }

        should_use_presenter Api::Mobile::RequestPresenter

        describe "the response" do
          it "uses the nice_id as the id" do
            assert_equal @ticket.nice_id.to_s, response_json[:id]
          end
        end
      end

      describe "with a ticket that don't belong to the user" do
        it "returns not found" do
          put :update, params: { id: other_user_token.value, request: update_params }

          assert_response :forbidden
        end
      end
    end

    # A security risk was found where end users with unverified email addresses
    # had access to tickets coming from these unverified email addresses
    # potentially allowing hackers to access other companies via password reset
    # emails.
    #
    # A fix was made https://github.com/zendesk/zendesk/pull/29682 under the
    # 'restrict_end_users_with_unverified_emails' Arturo feature by the secdev
    # team which fixes the vulnerability by preventing the access to the requests
    # endpoint from users with unverified email identities created after 17-09-2017.
    # This same fix caused a side effect of preventing a subset of
    # SDK anonymous users (ones with unverified email user identities) to access
    # the mobile SDK requests endpoint breaking the SDK.
    # A temporarly fix was made skipping the 'ensure_end_user_email_is_verified'
    # and these tests ensure that the SDK continues working on future code changes.
    #
    # We will be skipping the checks only if the
    # 'mobile_sdk_skip_unverified_emails_check' arturo feature is enabled.
    describe "With an unverified email identity" do
      before do
        Arturo.enable_feature! :mobile_sdk_skip_unverified_emails_check

        email_identity = @user.identities.email.last
        email_identity.update_attributes(
          created_at: Date.new(2017, 9, 18),
          is_verified: false
        )

        email_identity.save!
      end

      describe "on a GET to #show" do
        before { get :show, params: { id: token.value } }

        it("responds with ok") { assert_response :ok }
      end

      describe "on a GET to #show_many" do
        before { get :show_many, params: { tokens: tokens_params(many_tokens), status: status_params } }

        it("responds with ok") do
          assert_response :ok
        end
      end

      describe "on a POST to #create" do
        before { post :create, params: create_params }

        it("responds with created") { assert_response :created }
      end

      describe "on a PUT to #update" do
        before { put :update, params: { id: token.value, request: update_params } }

        it("responds with ok") { assert_response :ok }
      end
    end
  end

  as_an_sdk_jwt_end_user do
    let(:normal_tickets) { create_tickets(5, account: @account, requester: @user, via_id: ViaType.MAIL) }

    params = [
      :mobile_sdk_limit_low,
      'mobile_sdk_requests_create',
      :enable_throttle_for_mobile_sdk_requests_create,
      :mobile_sdk_requests_create_emergency_limit
    ]

    it_throttles_mobile_sdk_requests *params do
      Prop.expects(:throttle!).with(:portal_tickets, user.id, threshold: 40)
      post :create, params: create_params
    end

    describe "POST to #create" do
      before { post :create, params: create_params }

      it("responds with created") { assert_response :created }

      it "returns the request's token as the request 'id'" do
        assert_match(/^[a-zA-Z0-9]*$/, JSON.parse(response.body, symbolize_names: true)[:request][:id])
      end
    end

    describe "GET to #show" do
      let(:response_json) { JSON.parse(response.body, symbolize_names: true)[:request] }

      params = [
        :mobile_sdk_limit_low,
        'mobile_sdk_requests_show',
        :enable_throttle_for_mobile_sdk_requests_show,
        :mobile_sdk_requests_show_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        get :show, params: { id: token.value }
      end

      describe "sdk ticket request" do
        before { get :show, params: { id: token.value } }

        it("responds with ok") { assert_response :ok }

        should_use_presenter Api::Mobile::RequestPresenter

        describe "the response" do
          it "uses the token as the id" do
            assert_equal token.value, response_json[:id]
          end

          it "uses the token in the url" do
            assert_equal "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{token.value}.json", response_json[:url]
          end
        end
      end

      describe "non-sdk ticket request" do
        before do
          @ticket = normal_tickets.sample
          get :show, params: { id: @ticket.nice_id }
        end

        it("responds with ok") { assert_response :ok }

        should_use_presenter Api::Mobile::RequestPresenter

        describe "the response" do
          it "uses the nice_id as the id" do
            assert_equal @ticket.nice_id.to_s, response_json[:id]
          end

          it "uses the token in the url" do
            assert_equal "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{@ticket.nice_id}.json", response_json[:url]
          end
        end
      end

      describe "with a ticket that don't belong to the user" do
        it "returns not found" do
          get :show, params: { id: other_user_token.value }

          assert_response :not_found
        end

        it "does not destroy tokens from other user's tickets" do
          get :show, params: { id: other_user_token.value }

          refute_nil RequestToken.find_by_id(other_user_token.id)
        end

        it "destroys orphan tokens" do
          other_user_ticket.delete

          get :show, params: { id: other_user_token.value }

          assert_nil RequestToken.find_by_id(other_user_token.id)
        end
      end

      describe "with a deleted ticket" do
        before do
          ticket.delete
          get :show, params: { id: token.value }
        end

        it("returns a 404 not found") { assert_response :not_found }

        it("deletes the request token") { assert_nil RequestToken.find_by_value(token) }
      end

      describe "satisfaction ratings" do
        let(:response_json) { JSON.parse(response.body, symbolize_names: true)[:request] }
        let (:bad_csat_params) { { score: "bad" } }

        before do
          @ticket = normal_tickets.sample
          Zendesk::Tickets::Initializer.set_rating(@ticket, bad_csat_params)
          @ticket.will_be_saved_by(user, via_id: ViaType.WEB_SERVICE)
          @ticket.current_user = user # Keep ticket validations happy
          @ticket.save!
        end

        it "contains csat when enabled" do
          MobileSdkApp.any_instance.stubs(:support_show_csat?).returns(true)
          get :show, params: { id: @ticket.nice_id }
          assert response_json[:csat], "Response should have a csat field when enabled in settings"
        end

        it "contains empty csat when disabled" do
          MobileSdkApp.any_instance.stubs(:support_show_csat?).returns(false)
          get :show, params: { id: @ticket.nice_id }
          assert_nil response_json[:csat], "Response should have an empty csat field when disabled in settings"
        end
      end
    end

    describe "on a GET to #index" do
      let(:response_json) { JSON.parse(response.body, symbolize_names: true)[:requests] }

      describe_with_arturo_enabled(:mobile_requests_index_cache_control_header) do
        it("responds with ok and default cache TTL") do
          get :index, params: { status: status_params }

          assert_equal response.headers['Cache-Control'], 'max-age=30, private'
          assert_response :ok
        end

        it("responds with ok and account settings TTL") do
          account.settings.mobile_requests_index_cache_control_ttl = 10.minutes
          account.settings.save!

          get :index, params: { status: status_params }
          assert_equal response.headers['Cache-Control'], 'max-age=600, private'
          assert_response :ok
        end
      end

      it("responds with ok") do
        get :index, params: { status: status_params }

        assert_nil response.headers['Cache-Control']
        assert_response :ok
      end

      params = [
        :mobile_sdk_limit_high,
        'mobile_sdk_requests_index',
        :enable_throttle_for_mobile_sdk_requests_index,
        :mobile_sdk_requests_index_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        get :index, params: { status: status_params }
      end

      describe "presenter" do
        before { get :index, params: { status: status_params } }

        should_use_presenter Api::Mobile::RequestPresenter
      end

      describe "the response" do
        before { get :index, params: { status: status_params } }

        it "shows all the mobile sdk created tickets" do
          identifiers = @user.tickets.map do |t|
            t.token ? t.token : t.nice_id.to_s
          end

          assert_same_elements(identifiers, response_json.map { |r| r[:id] })
        end

        it "uses the token in the url" do
          urls = @user.tickets.map do |t|
            identifier = t.token ? t.token : t.nice_id.to_s
            "https://minimumsdk.zendesk-test.com/api/mobile/requests/#{identifier}.json"
          end

          assert_same_elements(urls, response_json.map { |r| r[:url] })
        end
      end

      describe "Show closed requests functionality" do
        let(:found_ids) { response_json.map { |r| r[:id] } }
        let(:closed_ticket) { tickets(:minimum_mobile_sdk_closed) }

        describe "when the mobile sdk app has not disabled showing closed requests" do
          before do
            MobileSdkApp.any_instance.stubs(:support_show_closed_requests?).returns(true)

            get :index, params: { status: "closed" }
          end

          it "includes the closed tickets" do
            assert_includes found_ids, closed_ticket.nice_id.to_s
          end
        end

        describe "when the mobile sdk app has disabled showing closed requests" do
          before do
            MobileSdkApp.any_instance.stubs(:support_show_closed_requests?).returns(false)

            get :index, params: { status: "closed" }
          end

          it "does not include the closed tickets" do
            refute_includes found_ids, closed_ticket.nice_id.to_s
          end
        end
      end

      describe "Hide archived tickets functionality" do
        let(:found_ids) { response_json.map { |r| r[:id] } }

        describe_with_arturo_enabled :mobile_sdk_dont_show_archived_requests do
          let(:other_ticket) { create_ticket(account: account, requester: @user) }

          before do
            other_ticket

            # our archived ticket
            archive_and_delete(ticket)
          end

          describe "when the user has archived tickets" do
            it "does not include archived tickets" do
              get :index, params: { status: status_params }

              refute_includes found_ids, ticket.nice_id.to_s
              assert_includes found_ids, other_ticket.nice_id.to_s
            end
          end

          describe "when the user does not have archived tickets" do
            before do
              # Creating the scneario where the user does not have any archived tickets
              account.ticket_archive_stubs.destroy_all
            end

            it "does not include archived tickets but includes regular ones" do
              get :index, params: { status: status_params }

              refute_includes found_ids, ticket.nice_id.to_s
              assert_includes found_ids, other_ticket.nice_id.to_s
            end
          end

          it 'do not do extra queries' do
            assert_sql_queries(1, /NOT IN \(SELECT `ticket_archive_stubs`.`id` FROM `/) do
              get :index, params: { status: status_params }
            end
          end
        end

        describe_with_arturo_disabled :mobile_sdk_dont_show_archived_requests do
          before do
            get :index, params: { status: status_params }
          end

          it "includes archived tickets" do
            assert_includes found_ids, ticket.nice_id.to_s
          end
        end
      end
    end

    describe "PUT to #update" do
      let(:response_json) { JSON.parse(response.body, symbolize_names: true)[:request] }

      params = [
        :mobile_sdk_limit_low,
        'mobile_sdk_requests_update',
        :enable_throttle_for_mobile_sdk_requests_update,
        :mobile_sdk_requests_update_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        Prop.expects(:throttle!).with(:ticket_updates, [token.ticket.id, user.id], threshold: 30)
        put :update, params: { id: token.value, request: update_params }
      end

      describe "sdk-ticket request" do
        before { put :update, params: { id: token.value, request: update_params } }

        it("responds with ok") { assert_response :ok }

        should_use_presenter Api::Mobile::RequestPresenter

        describe "the response" do
          it("uses the token as the id") { assert_equal token.value, response_json[:id] }
        end
      end

      describe "no-sdk-ticket request" do
        before do
          @ticket = normal_tickets.sample
          put :update, params: { id: @ticket.nice_id, request: update_params }
        end

        it("responds with ok") { assert_response :ok }

        should_use_presenter Api::Mobile::RequestPresenter

        describe "the response" do
          it "uses the nice_id as the id" do
            assert_equal @ticket.nice_id.to_s, response_json[:id]
          end
        end
      end

      describe "with a ticket that don't belong to the user" do
        it "returns not found" do
          put :update, params: { id: other_user_token.value, request: update_params }

          assert_response :forbidden
        end
      end
    end

    describe "on a GET to #count" do
      let(:response_json) { JSON.parse(response.body, symbolize_names: true)[:requests] }

      describe "all" do
        before { get :count }

        it("responds with ok") { assert_response :ok }

        it "shows the count" do
          assert_equal @user.tickets.count(:all), response_json[:count]
        end
      end

      describe "filtered statuses" do
        before do
          @statuses = "new,open,pending,hold,solved"
          get :count, params: { status: @statuses }
        end

        it("responds with ok") { assert_response :ok }

        it "shows the count" do
          status_id = @statuses.split(",").map { |status| StatusType.find(status.to_s) }
          assert_equal @user.tickets.where(status_id: status_id).count(:all), response_json[:count]
        end
      end
    end

    # A security risk was found where end users with unverified email addresses
    # had access to tickets coming from these unverified email addresses
    # potentially allowing hackers to access other companies via password reset
    # emails.
    #
    # A fix was made https://github.com/zendesk/zendesk/pull/29682 under the
    # 'restrict_end_users_with_unverified_emails' Arturo feature by the secdev
    # team which fixes the vulnerability by preventing the access to the requests
    # endpoint from users with unverified email identities created after 17-09-2017.
    # This same fix caused a side effect of preventing a subset of
    # SDK anonymous users (ones with unverified email user identities) to access
    # the mobile SDK requests endpoint breaking the SDK.
    # A temporarly fix was made skipping the 'ensure_end_user_email_is_verified'
    # and these tests ensure that the SDK continues working on future code changes.
    #
    # We will be skipping the checks only if the
    # 'mobile_sdk_skip_unverified_emails_check' arturo feature is enabled.
    describe "With an unverified email identity" do
      before do
        Arturo.enable_feature! :mobile_sdk_skip_unverified_emails_check

        email_identity = @user.identities.email.last
        email_identity.update_attributes(
          created_at: Date.new(2017, 9, 18),
          is_verified: false
        )

        email_identity.save!
      end

      describe "on a GET to #show" do
        before { get :show, params: { id: token.value } }

        it("responds with ok") { assert_response :ok }
      end

      describe "on a POST to #create" do
        before { post :create, params: create_params }

        it("responds with created") { assert_response :created }
      end

      describe "on a PUT to #update" do
        before { put :update, params: { id: token.value, request: update_params } }

        it("responds with ok") { assert_response :ok }
      end
    end
  end

  def create_tickets(quantity, account:, requester:, via_id: ViaType.MOBILE_SDK)
    tickets = []
    quantity.times do
      tickets << create_ticket(account: account, requester: requester, via_id: via_id)
    end
    tickets
  end

  def create_ticket(account:, requester:, via_id: ViaType.MOBILE_SDK)
    account.tickets.create! do |t|
      t.requester = requester
      t.description = "HALP"
      t.subject = "Need halp, WELP!"
      t.via_id = via_id
      t.will_be_saved_by(requester)
    end
  end
end
