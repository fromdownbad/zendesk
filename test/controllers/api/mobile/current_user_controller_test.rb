require_relative '../../../support/test_helper'
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::Mobile::CurrentUserController do
  extend MobileSdk::RateLimitHelper
  extend Api::V2::TestHelper
  include CustomFieldsTestHelper

  fixtures :all

  before do
    # don't use these fixtures
    CustomField::Field.without_arsi.delete_all
    CustomField::Value.without_arsi.delete_all

    accept :json

    @account = accounts(:minimum_sdk)
    create_user_custom_field!(:text_cust_field, 'Sample test user field', 'Text')
    create_user_custom_field!(:decimal_cust_field, 'Sample decimal user field', 'Decimal')
    create_user_custom_field!(:integer_cust_field, 'Sample integer user field', 'Integer')
  end

  let(:params) do
    {
      user: {
        user_fields: {
          text_cust_field: 'Hello, world!',
          decimal_cust_field: 1.0,
          integer_cust_field: 10
        }
      }
    }
  end

  let(:expected_user_fields_hash) do
    {
      'decimal_cust_field' => 1.0,
      'integer_cust_field' => 10,
      'text_cust_field' => 'Hello, world!'
    }
  end

  with_options(controller: 'api/mobile/current_user') do |request|
    request.should_route :get, '/api/mobile/users/me.json', action: :show, format: 'json'
    request.should_route :put, '/api/mobile/users/me.json', action: :update, format: 'json'
  end

  as_an_end_user do
    describe 'GET to #show' do
      it 'responds with HTTP 450 (Blocked)' do
        get :show
        assert_equal 450, response.status
      end
    end

    describe 'PUT to #update' do
      it 'responds with HTTP 450 (Blocked)' do
        put :update, params: params
        assert_equal 450, response.status
      end
    end
  end

  as_an_sdk_anonymous_end_user do
    describe 'GET to #show' do
      before { get :show }

      should_use_presenter Api::Mobile::Users::EndUserPresenter
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
      it('responds with success') { assert_response :success }
    end

    describe 'PUT to #update' do
      before { put :update, params: params }

      should_use_presenter  Api::Mobile::Users::EndUserPresenter

      it 'updates the user custom fields with the provided values' do
        assert_equal 'Hello, world!', @user.custom_field_values.value_for_key('text_cust_field').value
        assert_equal 1.0, @user.custom_field_values.value_for_key('decimal_cust_field').value.to_f
        assert_equal 10, @user.custom_field_values.value_for_key('integer_cust_field').value.to_i
      end

      it 'ignores the non-existent custom fields' do
        params[:wat] = 'Fuuuu'
        refute @user.custom_field_values.value_for_key('wat')
      end

      describe 'HTTP Interaction' do
        it('responds with success') { assert_response :success }

        describe 'the response' do
          before { @json = JSON.parse(response.body)['user'] }

          it 'sends the correct set of custom_fields' do
            assert_equal expected_user_fields_hash, @json['user_fields']
          end
        end
      end
    end

    describe 'rate limiting' do
      describe 'GET to #show' do
        rate_limit_params = [
          :mobile_sdk_limit_low,
          'mobile_sdk_current_user_show',
          :enable_throttle_for_mobile_sdk_current_user_show,
          :mobile_sdk_current_user_show_emergency_limit
        ]

        it_throttles_mobile_sdk_requests *rate_limit_params do
          get :show
        end
      end

      describe 'PUT to #update' do
        rate_limit_params = [
          :mobile_sdk_limit_low,
          'mobile_sdk_current_user_update',
          :enable_throttle_for_mobile_sdk_current_user_update,
          :mobile_sdk_current_user_update_emergency_limit
        ]

        it_throttles_mobile_sdk_requests *rate_limit_params do
          put :update, params: params
        end
      end
    end
  end

  as_an_sdk_jwt_end_user do
    describe 'GET to #show' do
      before { get :show }

      should_use_presenter Api::Mobile::Users::EndUserPresenter
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
      it('responds with success') { assert_response :success }
    end

    describe 'PUT to #update' do
      before { put :update, params: params }

      should_use_presenter  Api::Mobile::Users::EndUserPresenter

      it 'updates the user custom fields with the provided values' do
        assert_equal 'Hello, world!', @user.custom_field_values.value_for_key('text_cust_field').value
        assert_equal 1.0, @user.custom_field_values.value_for_key('decimal_cust_field').value.to_f
        assert_equal 10, @user.custom_field_values.value_for_key('integer_cust_field').value.to_i
      end

      it 'ignores the non-existent custom fields' do
        params[:wat] = 'Fuuuu'
        refute @user.custom_field_values.value_for_key('wat')
      end

      describe 'HTTP Interaction' do
        it('responds with success') { assert_response :success }

        describe 'the response' do
          before { @json = JSON.parse(response.body)['user'] }

          it 'sends the correct set of custom_fields' do
            assert_equal expected_user_fields_hash, @json['user_fields']
          end
        end
      end

      describe 'rate limiting' do
        describe 'GET to #show' do
          rate_limit_params = [
            :mobile_sdk_limit_low,
            'mobile_sdk_current_user_show',
            :enable_throttle_for_mobile_sdk_current_user_show,
            :mobile_sdk_current_user_show_emergency_limit
          ]

          it_throttles_mobile_sdk_requests *rate_limit_params do
            get :show
          end
        end

        describe 'PUT to #update' do
          rate_limit_params = [
            :mobile_sdk_limit_low,
            'mobile_sdk_current_user_update',
            :enable_throttle_for_mobile_sdk_current_user_update,
            :mobile_sdk_current_user_update_emergency_limit
          ]

          it_throttles_mobile_sdk_requests *rate_limit_params do
            put :update, params: params
          end
        end
      end
    end
  end
end
