require_relative "../../../support/test_helper"
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered! uncovered: 1

describe Api::Mobile::PushNotificationDevicesController do
  extend Api::V2::TestHelper
  fixtures :accounts, :custom_field_options, :organizations, :tickets, :ticket_fields, :users,
    :role_settings, :organization_memberships, :mobile_sdk_apps, :mobile_sdk_auths, :sequences, :oauth_clients,
    :devices, :device_identifiers

  before do
    accept :json
  end

  with_options(controller: "api/mobile/push_notification_devices") do |request|
    request.should_route :post, "/api/mobile/push_notification_devices.json", action: :create, format: "json"
    request.should_route :delete, "/api/mobile/push_notification_devices/1.json", action: :destroy, format: "json", id: "1"
  end

  let(:device_token) { "foo" }
  let(:device_type) { "iphone" }
  let(:locale) { "en-US" }

  as_an_sdk_anonymous_end_user do
    describe "POST to #create" do
      let(:token_type) { "urban_airship_channel_id" }
      let(:create_params) do
        {
          "push_notification_device" => {
            "identifier" => device_token,
            "device_type" => device_type,
            "locale" => locale,
            "sdk_guid" => @user_sdk_identity.value,
            "token_type" => token_type
          }
        }
      end

      describe "HTTP Interaction" do
        before do
          post :create, params: create_params
        end

        it "responds with 201 Created" do
          assert_equal 201, response.status
        end

        should_use_presenter Api::Mobile::PushNotificationDevicePresenter

        describe "the response" do
          before { @json = JSON.parse(response.body)["push_notification_device"] }

          it 'sends the correct device identifier' do
            assert_equal device_token, @json["identifier"]
          end

          it 'sends the correct device type' do
            assert_equal device_type, @json["device_type"]
          end

          it 'sends the correct device status' do
            assert(@json["active"])
          end

          it 'sends the creation date' do
            assert @json["created_at"]
          end

          it 'sends the last updated date' do
            assert @json["updated_at"]
          end

          it 'sends the id' do
            assert @json['id']
          end

          it 'sends the url' do
            assert @json['url']
          end

          it 'sends the specified token_type' do
            assert_equal token_type, @json['token_type']
          end
        end
      end

      describe 'Rate limiting' do
        describe_with_arturo_enabled(:mobile_sdk_throttle_push_notification_registrations_1h) do
          it 'does rate limit the creation requests to 1 per hour' do
            Prop.expects(:throttle!).with(:push_notification_device_registrations_1h, [@account.id, device_token])
            post :create, params: create_params
          end
        end

        describe_with_arturo_disabled(:mobile_sdk_throttle_push_notification_registrations_1h) do
          it 'rate limit to 1 requests per second' do
            Prop.expects(:throttle!).with(:push_notification_device_registrations, [@account.id, device_token])
            post :create, params: create_params
          end
        end

        describe_with_arturo_enabled(:mobile_sdk_throttle_push_notification_deregistrations) do
          it 'rate limits device deletion to 1 per 15 minutes' do
            Prop.expects(:throttle!).with(:push_notification_device_deregistrations, [@account.id, device_token])
            Prop.expects(:throttle!).with(:push_notification_device_deregistrations_per_account,
              @account.id,
              threshold: @account.settings.push_notification_device_deregistrations_threshold)
            delete :destroy, params: { id: device_token }
          end
        end

        it 'rate limits deletions to the account setting push_notification_device_deregistrations_threshold' do
          Prop.expects(:throttle!).with(:push_notification_device_deregistrations_per_account,
            @account.id,
            threshold: @account.settings.push_notification_device_deregistrations_threshold)
          delete :destroy, params: { id: device_token }
        end
      end

      it "creates a PushNotifications::SdkAnonymousDeviceIdentifier" do
        PushNotifications::SdkAnonymousDeviceIdentifier.without_arsi.delete_all
        assert_difference "PushNotifications::SdkAnonymousDeviceIdentifier.count(:all)", 1 do
          post :create, params: create_params
        end
      end

      describe "Translation Locale" do
        fixtures :translation_locales

        before do
          spanish = translation_locales(:spanish)
          @french = translation_locales(:french)
          @account.stubs(:available_languages).returns([spanish, @french])
          Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
        end

        it "changes the user locale_translation" do
          post :create, params: create_params.merge!("locale" => 'fr-CA')
          assert_equal @french, @user.translation_locale
        end
      end

      describe "Device already exists" do
        before do
          PushNotifications::SdkAnonymousDeviceIdentifier.any_instance.stubs(:register).returns(true)
          @push_notification_device = PushNotifications::SdkAnonymousDeviceIdentifier.create!(
            token: "foo",
            device_type: "iphone",
            account: @account,
            user_sdk_identity: @user_sdk_identity,
            mobile_sdk_app: mobile_sdk_apps(:minimum_sdk),
            token_type: "urban_airship_channel_id"
          )

          @create_params = {
            "push_notification_device" => {
              "identifier" => @push_notification_device.token,
              "device_type" => @push_notification_device.device_type,
              "locale" => locale,
              "sdk_guid" => @user_sdk_identity.value,
              "token_type" => "urban_airship_channel_id"
            }
          }
        end

        it "does not create a new push notification device" do
          refute_difference "PushNotifications::SdkAnonymousDeviceIdentifier.count(:all)", 1 do
            post :create, params: @create_params
          end
        end

        it "marks the existing device as active" do
          @push_notification_device.update_attribute(:active, false)
          post :create, params: @create_params
          @push_notification_device.reload
          assert @push_notification_device.active
        end
      end

      describe "Same device for a new GUID" do
        before do
          PushNotifications::SdkAnonymousDeviceIdentifier.any_instance.stubs(:register).returns(true)
          @new_user_sdk_identity = user_identities(:mobile_sdk_identity_new)
          @push_notification_device = PushNotifications::SdkAnonymousDeviceIdentifier.create!(
            token: "foo",
            device_type: "iphone",
            account: @account,
            user_sdk_identity: user_identities(:mobile_sdk_identity),
            mobile_sdk_app: mobile_sdk_apps(:minimum_sdk),
            token_type: "urban_airship_channel_id"
          )

          @create_params = {
            "push_notification_device" => {
              "identifier" => @push_notification_device.token,
              "device_type" => @push_notification_device.device_type,
              "locale" => locale,
              "sdk_guid" => @new_user_sdk_identity.value,
              "token_type" => "urban_airship_channel_id"
            }
          }
        end

        it "does create a new push notification device" do
          assert_difference "PushNotifications::SdkAnonymousDeviceIdentifier.count(:all)", 1 do
            post :create, params: @create_params
          end
        end
      end

      describe "with invalid parameters" do
        let(:create_params) { {} }

        before { post :create, params: create_params }

        it("responds with bad_request") do
          assert_includes response.body, "Parameter push_notification_device is required"
          assert_response :bad_request
        end
      end
    end

    describe "DELETE to #destroy" do
      before do
        PushNotifications::SdkAnonymousDeviceIdentifier.any_instance.stubs(:register).returns(true)
        @push_notification_device = PushNotifications::SdkAnonymousDeviceIdentifier.create!(
          token: "foo",
          device_type: "iphone",
          account: @account,
          user_sdk_identity: @user_sdk_identity,
          mobile_sdk_app: mobile_sdk_apps(:minimum_sdk)
        )
      end

      describe "Using zendesk id as destroying identifier" do
        it "removes the push notification device" do
          assert_difference "PushNotifications::SdkAnonymousDeviceIdentifier.count(:all)", -1 do
            delete :destroy, params: { id: @push_notification_device.id }
          end
        end

        describe "HTTP Interaction" do
          before do
            delete :destroy, params: { id: @push_notification_device.id }
          end

          it "responds with 200 OK" do
            assert_equal 200, response.status
          end
        end
      end

      describe "Using device token as destroying identifier" do
        it "removes the push notification device" do
          assert_difference "PushNotifications::SdkAnonymousDeviceIdentifier.count(:all)", -1 do
            delete :destroy, params: { id: @push_notification_device.token }
          end
        end

        describe "HTTP Interaction" do
          before do
            delete :destroy, params: { id: @push_notification_device.token }
          end

          it "responds with 200 OK" do
            assert_equal 200, response.status
          end
        end
      end
    end
  end

  as_an_sdk_jwt_end_user do
    describe "POST to #create" do
      let(:token_type) { "urban_airship_channel_id" }
      let(:create_params) do
        {
          "push_notification_device" => {
            "identifier" => device_token,
            "device_type" => device_type,
            "locale" => locale,
            "token_type" => token_type
          }
        }
      end

      describe "HTTP Interaction" do
        before do
          post :create, params: create_params
        end

        it "responds with 201 Created" do
          assert_equal 201, response.status
        end

        should_use_presenter Api::Mobile::PushNotificationDevicePresenter

        describe "the response" do
          before { @json = JSON.parse(response.body)["push_notification_device"] }

          it 'sends the correct device identifier' do
            assert_equal device_token, @json["identifier"]
          end

          it 'sends the correct device type' do
            assert_equal device_type, @json["device_type"]
          end

          it 'sends the correct device status' do
            assert(@json["active"])
          end

          it 'sends the creation date' do
            assert @json["created_at"]
          end

          it 'sends the last updated date' do
            assert @json["updated_at"]
          end

          it 'sends the id' do
            assert @json['id']
          end

          it 'sends the url' do
            assert @json['url']
          end

          it 'sends the specified token_type' do
            assert_equal token_type, @json['token_type']
          end
        end
      end

      describe 'with a big integer as identifier' do
        before do
          create_params["push_notification_device"]["identifier"] = 12345678901234567890

          post :create, params: create_params
        end

        it 'doesn\'t blow up' do
          assert_equal 201, response.status
        end
      end

      describe 'Rate limiting' do
        describe_with_arturo_enabled(:mobile_sdk_throttle_push_notification_registrations_1h) do
          it 'does rate limit the creation requests to 1 per hour' do
            Prop.expects(:throttle!).with(:push_notification_device_registrations_1h, [@account.id, device_token])
            post :create, params: create_params
          end
        end

        describe_with_arturo_disabled(:mobile_sdk_throttle_push_notification_registrations_1h) do
          it 'rate limit to 1 requests per second' do
            Prop.expects(:throttle!).with(:push_notification_device_registrations, [@account.id, device_token])
            post :create, params: create_params
          end
        end
      end

      it "creates a PushNotifications::SdkAuthenticatedDeviceIdentifier" do
        PushNotifications::SdkAuthenticatedDeviceIdentifier.without_arsi.delete_all
        assert_difference "PushNotifications::SdkAuthenticatedDeviceIdentifier.count(:all)", 1 do
          post :create, params: create_params
        end
      end

      describe "Translation Locale" do
        fixtures :translation_locales

        before do
          spanish = translation_locales(:spanish)
          @french = translation_locales(:french)
          @account.stubs(:available_languages).returns([spanish, @french])
          Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
        end

        it "changes the user locale_translation" do
          post :create, params: create_params.merge!("locale" => 'fr-CA')
          assert_equal @french, @user.translation_locale
        end
      end

      describe "Device already exists" do
        before do
          PushNotifications::SdkAuthenticatedDeviceIdentifier.any_instance.stubs(:register).returns(true)
          @push_notification_device = PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(
            token: "foo",
            device_type: "iphone",
            account: @account,
            user: @user,
            mobile_sdk_app: mobile_sdk_apps(:minimum_sdk),
            token_type: "urban_airship_channel_id"
          )

          @create_params = {
            "push_notification_device" => {
              "identifier" => @push_notification_device.token,
              "device_type" => @push_notification_device.device_type,
              "locale" => locale,
              "token_type" => "urban_airship_channel_id"
            }
          }
        end

        it "does not create a new push notification device" do
          refute_difference "PushNotifications::SdkAuthenticatedDeviceIdentifier.count(:all)", 1 do
            post :create, params: @create_params
          end
        end

        it "marks the existing device as active " do
          @push_notification_device.update_attribute(:active, false)
          post :create, params: @create_params
          @push_notification_device.reload
          assert @push_notification_device.active
        end
      end
    end

    describe "DELETE to #destroy" do
      before do
        PushNotifications::SdkAuthenticatedDeviceIdentifier.any_instance.stubs(:register).returns(true)
        @push_notification_device = PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(
          token: "foo",
          device_type: "iphone",
          account: @account,
          user: @user,
          mobile_sdk_app: mobile_sdk_apps(:minimum_sdk)
        )
      end

      describe "Using zendesk id as destroying identifier" do
        it "removes the push notification device" do
          assert_difference "PushNotifications::SdkAuthenticatedDeviceIdentifier.count(:all)", -1 do
            delete :destroy, params: { id: @push_notification_device.id }
          end
        end

        describe "HTTP Interaction" do
          before do
            delete :destroy, params: { id: @push_notification_device.id }
          end

          it "responds with 200 OK" do
            assert_equal 200, response.status
          end
        end
      end

      describe "Using device token as destroying identifier" do
        it "removes the push notification device" do
          assert_difference "PushNotifications::SdkAuthenticatedDeviceIdentifier.count(:all)", -1 do
            delete :destroy, params: { id: @push_notification_device.token }
          end
        end

        describe "HTTP Interaction" do
          before do
            delete :destroy, params: { id: @push_notification_device.token }
          end

          it "responds with 200 OK" do
            assert_equal 200, response.status
          end
        end
      end
    end
  end
end
