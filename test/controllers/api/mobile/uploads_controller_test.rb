require_relative "../../../support/test_helper"
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::Mobile::UploadsController do
  extend Api::V2::TestHelper
  extend MobileSdk::RateLimitHelper

  fixtures :all

  before do
    @request.user_agent = "Zendesk SDK for iOS"

    # Disabled this before filter as it's tested independently
    @controller.stubs(:allow_only_mobile_sdk_clients_access).returns(nil)
    @controller.stubs(:allow_only_mobile_sdk_tokens_access).returns(nil)

    accept :json
  end

  with_options(controller: "api/mobile/uploads") do |request|
    request.should_route :post, "/api/mobile/uploads", action: :create
    request.should_route :delete, "/api/mobile/uploads/1", action: :destroy, id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized :create
  end

  as_an_end_user do
    let(:account) { accounts(:minimum) }

    let(:sdk_ticket) { FactoryBot.create(:ticket, account: account, via_id: ViaType.MOBILE_SDK, requester: @user) }
    let(:sdk_token) { RequestToken.create(ticket: sdk_ticket) }
    let(:normal_ticket) { FactoryBot.create(:ticket, account: account, requester: @user) }

    before do
      sdk_token
      normal_ticket
    end

    describe "on a POST to #create" do
      describe "upload file" do
        before do
          client = FactoryBot.create(:client, identifier: 'mobile_sdk_client', user: @account.owner, account: @account)
          token = FactoryBot.create(:token, client: client, user: nil, scopes: 'sdk')
          @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token
          post :create, params: { uploaded_data: fixture_file_upload("small.png") }
        end

        it('responds with created') { assert_response :created }

        should_use_presenter Api::V2::UploadPresenter, status: :created, location: %r{/api/v2/attachments/\d+\.json}
      end
    end
  end

  as_an_sdk_anonymous_end_user do
    describe "Rate Limits" do
      before do
        client = FactoryBot.create(:client, identifier: 'anonymous_mobile_sdk_client', user: @account.owner, account: @account)
        @token = FactoryBot.create(:token, client: client, user: nil, scopes: 'sdk')
        @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = @token
      end

      params = [
        :mobile_sdk_limit_low,
        "mobile_sdk_uploads_create",
        :enable_throttle_for_mobile_sdk_uploads_create,
        :mobile_sdk_uploads_create_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        post :create, params: { uploaded_data: fixture_file_upload("small.png") }
      end
    end
  end

  as_an_sdk_jwt_end_user do
    describe "Rate Limits" do
      before do
        client = FactoryBot.create(:client, identifier: 'jwt_mobile_sdk_client', user: @account.owner, account: @account)
        @token = FactoryBot.create(:token, client: client, user: nil, scopes: 'sdk')
        @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = @token
      end

      params = [
        :mobile_sdk_limit_low,
        "mobile_sdk_uploads_create",
        :enable_throttle_for_mobile_sdk_uploads_create,
        :mobile_sdk_uploads_create_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        post :create, params: { uploaded_data: fixture_file_upload("small.png") }
      end
    end
  end
end
