require_relative "../../../support/test_helper"
require_relative "../../../support/satisfaction_test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::Mobile::SatisfactionRatingsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  include SatisfactionTestHelper

  fixtures :accounts, :tickets, :users, :mobile_sdk_auths, :user_identities, :ticket_fields, :role_settings, :sequences, :oauth_clients

  let(:ticket) { tickets(:minimum_mobile_sdk) }
  let(:token) { RequestToken.create!(ticket: ticket) }
  let(:end_user_without_access) { users(:minimum_end_user) }
  let(:requester) { ticket.requester }
  let(:sdk_user_not_requester) { users(:minimum_sdk_end_user2) }
  let(:rating) { {satisfaction_rating: {score: "bad", comment: "Terrible"}} }

  describe_with_arturo_setting_enabled :customer_satisfaction do
    before do
      accept :json

      ticket.will_be_saved_by(requester)
      ticket.update_attribute(:status_id, 3)
    end

    with_options(controller: "api/mobile/satisfaction_ratings") do |request|
      request.should_route :post, "/api/mobile/requests/1/satisfaction_rating", action: :create, ticket_id: 1
    end

    describe "POST to #create" do
      before do
        MobileSdkApp.any_instance.stubs(support_show_csat: false)
      end

      as_an_sdk_anonymous_end_user do
        should_not_be_authorized { post :create, params: rating.merge(ticket_id: token.value) }
      end

      as_an_sdk_jwt_end_user do
        should_not_be_authorized { post :create, params: rating.merge(ticket_id: token.value) }
      end
    end

    as_an_sdk_anonymous_end_user do
      before do
        MobileSdkApp.any_instance.stubs(support_show_csat?: true)
      end

      describe "POST to #create" do
        should_be_authorized { post :create, params: rating.merge(ticket_id: token.value) }
      end

      describe "POST to #create as not the requester of the ticket" do
        before do
          login(sdk_user_not_requester)
          post :create, params: rating.merge(ticket_id: token.value)
        end

        it "will not find the ticket" do
          assert_response :not_found
        end
      end

      describe "POST to #create as not the requester of the ticket AND not an SDK user" do
        before do
          login(end_user_without_access)
          post :create, params: rating.merge(ticket_id: ticket.nice_id)
        end

        it "will not find the ticket" do
          assert_response :not_found
        end
      end

      describe "POST to #create sets the correct ratiing" do
        before { post :create, params: rating.merge(ticket_id: token.value) }

        it "sets the correct rating value" do
          comment = JSON.parse(response.body, symbolize_names: true)[:satisfaction_rating][:comment]
          assert_equal rating[:satisfaction_rating][:comment], comment
        end
      end
    end

    as_an_sdk_jwt_end_user do
      before do
        MobileSdkApp.any_instance.stubs(support_show_csat?: true)
      end

      describe "POST to #create" do
        should_be_authorized { post :create, params: rating.merge(ticket_id: token.value) }
      end

      describe "POST to #create as not the requester of the ticket" do
        before do
          login(sdk_user_not_requester)
          post :create, params: rating.merge(ticket_id: token.value)
        end

        it "will not find the ticket" do
          assert_response :not_found
        end
      end

      describe "POST to #create sets the correct ratiing" do
        before { post :create, params: rating.merge(ticket_id: token.value) }

        it "sets the correct rating value" do
          comment = JSON.parse(response.body, symbolize_names: true)[:satisfaction_rating][:comment]
          assert_equal rating[:satisfaction_rating][:comment], comment
        end
      end
    end
  end
end
