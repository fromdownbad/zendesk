require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::Mobile::BaseController do
  class ApiMobileTestController < Api::Mobile::BaseController
    skip_before_action :authenticate_user
    skip_before_action :enforce_ssl!

    allow_parameters :mobile_only, :skip
    def mobile_only
      render json: 'cool'
    end
  end

  tests ApiMobileTestController
  use_test_routes

  before do
    @current_account = accounts(:minimum)
    @request.account = @current_account
    login(@current_account.owner)
  end

  describe "#allow_only_zendesk_mobile_app_access" do
    describe "when an action is hit by a Zendesk mobile app" do
      before { @request.stubs(:user_agent).returns("Zendesk for iPhone 3.0") }

      describe "the action is permitted" do
        it "does not block" do
          get :mobile_only, format: 'json'
          assert_response :ok
        end
      end
    end

    describe "when a mobile_only action is hit by a regular browser" do
      before { @request.stubs(:user_agent).returns("MegaBrowse 2.0") }

      it "blocks" do
        get :mobile_only, format: 'json'
        assert_response 450
      end
    end
  end
end
