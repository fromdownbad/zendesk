require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Mobile::Account::GroupsController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :groups, :role_settings

  before do
    @request.user_agent = "Zendesk for iPhone"
    accept :json
  end

  with_options(controller: "api/mobile/account/groups") do |request|
    request.should_route :get, "/api/mobile/account/groups/assignable", action: "assignable"
  end

  as_an_anonymous_user do
    should_be_unauthorized :assignable
  end

  as_an_end_user do
    should_be_forbidden :assignable
  end

  as_an_admin do
    describe "a GET to :assignable" do
      before { get :assignable }
      should_use_presenter Api::Mobile::Account::GroupPresenter, with: :groups
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end
  end

  as_an_agent do
    describe "a GET to :assignable" do
      before { get :assignable }
      should_use_presenter Api::Mobile::Account::GroupPresenter, with: :groups
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end
  end
end
