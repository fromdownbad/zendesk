require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Mobile::Account::LookupController do
  before do
    accept :json
  end

  let(:json_response) { JSON.parse(@response.body) }

  with_options(controller: "api/mobile/account/lookup") do |request|
    request.should_route :get, "/api/mobile/account/lookup", action: "index"
  end

  as_an_anonymous_user do
    let(:account) { accounts(:minimum) }

    before do
      setup_request(@request)
    end

    describe "when mobile access is disabled" do
      before do
        account.settings.mobile_app_access = false
        account.settings.save!
      end

      it "blocks mobile access" do
        get :index
        assert_response :forbidden
        assert_equal({"error" => "MobileAccessDisabled", "description" => "The administrator for this Zendesk has disabled mobile app access."}, JSON.parse(response.body))
      end
    end

    describe "a GET to :index without SSL" do
      before { get :index }
      it('responds with success') { assert_response :success }
    end

    describe "a GET to :index" do
      before do
        get :index
      end

      it('responds with success') { assert_response :success }

      it "returns subdomain" do
        assert_equal account.subdomain, json_response["lookup"]["subdomain"]
      end

      it "returns the authentication domain" do
        assert_equal account.authentication_domain, json_response["lookup"]["url"]
      end

      it "returns account name" do
        assert_equal account.name, json_response["lookup"]["name"]
      end

      it "returns the main Zendesk login url" do
        body = json_response["lookup"]
        assert body["agent_logins"]
        zendesk_login = body["agent_logins"].find { |al| al["service"] == "zendesk" }
        assert zendesk_login
        assert zendesk_login["url"]
      end

      describe "for a host-mapped account" do
        before do
          @request.account.host_mapping = "example.com"
        end

        it "still returns the authentication domain for the lookup url" do
          assert_equal account.authentication_domain, json_response["lookup"]["url"]
        end
      end
    end

    describe 'validate active products' do
      let(:guide_product) { create_product(:guide) }
      let(:chat_product) { create_product(:chat) }
      let(:sell_product) { create_product(:sell) }
      let(:active_support_product) { create_product(:support) }
      let(:inactive_support_product) { create_product(:support, active: false) }

      describe 'when multiproduct is not enabled' do
        it 'without any product' do
          get :index

          assert_response :success
        end
      end

      describe 'when multiproduct is enabled' do
        let(:account) { accounts(:multiproduct) }

        it 'forbids the request without any product' do
          @controller.stubs(:current_account_products).returns([])
          get :index

          assert_response :forbidden
          assert_equal 'AccountDoesNotHaveAnyRequiredProduct', json_response['error']
        end

        it 'forbids the request without any valid product' do
          @controller.stubs(:current_account_products).returns([guide_product])
          get :index

          assert_response :forbidden
          assert_equal 'AccountDoesNotHaveAnyRequiredProduct', json_response['error']
        end

        it 'forbids the request with only support inactive product' do
          @controller.stubs(:current_account_products).returns([inactive_support_product])
          get :index

          assert_response :forbidden
          assert_equal 'AccountDoesNotHaveAnyRequiredProduct', json_response['error']
        end

        it 'allows the request with only support product' do
          @controller.stubs(:current_account_products).returns([active_support_product])
          @controller.stubs(:support_product).returns(active_support_product)
          get :index

          assert_response :success
          assert_equal ['support'], json_response['lookup']['active_products']
        end

        it 'allows the request with only chat product' do
          @controller.stubs(:current_account_products).returns([chat_product])
          get :index

          assert_response :success
          assert_equal ['chat'], json_response['lookup']['active_products']
        end

        it 'allows the request with only sell product' do
          @controller.stubs(:current_account_products).returns([sell_product])
          get :index

          assert_response :success
          assert_equal ['sell'], json_response['lookup']['active_products']
        end

        it 'allows the request with support and chat product' do
          @controller.stubs(:current_account_products).returns([active_support_product, chat_product])
          @controller.stubs(:support_product).returns(active_support_product)
          get :index

          assert_response :success
          assert_equal ['chat', 'support'].sort, json_response['lookup']['active_products'].sort
        end
      end
    end
  end

  # Seperate block because we need to stub some fields on the account.
  # The stubs run between the filters and index in the the controller.
  # We need to stub the account before the filters as agent_logins is built in a filter
  as_an_anonymous_user do
    let(:account) { stub_allowed_roles_for(accounts(:minimum), remote: true) }

    before do
      setup_request(@request)
    end

    describe "for an account with multiple remote authentications" do
      before do
        Account.any_instance.stubs(has_saml?: true)

        @saml                  = account.remote_authentications.build
        @saml.fingerprint      = "44:D2:9D:98:49:66:27:30:3A:67:A2:5D:97:62:31:65:57:9F:57:D1"
        @saml.is_active        = true
        @saml.remote_login_url = "https://idp.example.org/saml"
        @saml.auth_mode        = RemoteAuthentication::SAML
        @saml.save!

        @jwt                  = account.remote_authentications.build
        @jwt.auth_mode        = RemoteAuthentication::JWT
        @jwt.is_active        = true
        @jwt.remote_login_url = "https://sso.cpconsulting.dk/zendesk_remote_auth.asp"
        @jwt.token            = SecureRandom.hex(32)
        @jwt.save!
      end

      it "returns the primary remote authentication url" do
        get :index
        remote_login = json_response["lookup"]["agent_logins"].detect { |al| al["service"] == "remote" }
        assert remote_login["url"].starts_with?(@saml.remote_login_url)
      end

      it "uses the secondary remote auth if the primary remote authentication isn't valid" do
        @saml.update_attributes!(ip_ranges: '1.2.3.4/32')
        get :index

        remote_login = json_response["lookup"]["agent_logins"].detect { |al| al["service"] == "remote" }
        assert remote_login["url"].starts_with?(@jwt.remote_login_url)
      end
    end
  end

  as_an_anonymous_user do
    let(:account) { stub_allowed_roles_for(accounts(:minimum), google: true) }

    before do
      setup_request(@request)
    end

    describe "a GET to :index for an account with only google sso" do
      before do
        get :index
      end

      it "shows the google sso links" do
        google_login = json_response["lookup"]["agent_logins"].detect { |al| al["service"] == "google" }

        assert_equal "https://minimum.zendesk-test.com/access/google?google_domain=https%3A%2F%2Fminimum.zendesk-test.com&profile=google", google_login["url"]
        assert google_login["play_url"]
      end
    end
  end

  as_an_anonymous_user do
    let(:account) do
      @account = accounts(:minimum)
      stub_allowed_roles_for(@account)
      role_settings = role_settings(:minimum)
      role_settings.agent_password_allowed = false
      @account.stubs(:role_settings).returns(role_settings)
      @account
    end

    before do
      setup_request(@request)
    end

    describe "a GET to :index for an account with no agent logins" do
      describe_with_arturo_enabled :mobile_present_error_on_no_agent_logins do
        it "returns an error" do
          get :index
          assert_response :forbidden
          assert_equal({"error" => "ContactAdministrator", "description" => "Login restricted. Please contact your administrator"}, JSON.parse(response.body))
        end
      end

      describe_with_arturo_disabled :mobile_present_error_on_no_agent_logins do
        it "returns an empty array" do
          get :index
          assert_response :ok
          agent_logins = json_response["lookup"]["agent_logins"]
          assert agent_logins
          assert_empty(agent_logins)
        end
      end
    end
  end

  def create_product(name, active: true)
    Zendesk::Accounts::Product.new(
      'id' => name,
      'account_id' => @account.id,
      'state' => Zendesk::Accounts::Product::SUBSCRIBED,
      'name' => name,
      'active' => active
    )
  end

  def stub_allowed_roles_for(account, remote: false, office_365: false, google: false)
    account.stubs(:login_allowed_for_role?).with(:remote, :agent).returns(remote)
    account.stubs(:login_allowed_for_role?).with(:office_365, :agent).returns(office_365)
    account.stubs(:login_allowed_for_role?).with(:google, :agent).returns(google)
    account
  end

  def setup_request(request)
    request.env.delete("HTTPS")
    request.user_agent = "Zendesk for iPhone"
    request.stubs(:ssl?).returns(true)
    request.account = account
  end
end
