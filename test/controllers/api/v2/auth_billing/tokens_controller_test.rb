require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::AuthBilling::TokensController do
  extend Api::V2::TestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }

  let(:too_many_requests) { 429 }

  let(:shared_key) { 'something_random_and_secure' }

  before do
    @request.account = account
    accept :json
  end

  with_options(controller: 'api/v2/auth_billing/tokens') do |request|
    request.should_route :post, 'api/v2/auth_billing/tokens', action: 'create'
  end

  as_an_anonymous_user { should_be_unauthorized :create }
  as_an_end_user       { should_be_forbidden :create }
  as_an_agent          { should_be_forbidden :create }

  as_an_admin do
    before do
      ZBC::Zendesk::HostedPage::Encrypt.expects(:call).with(
        shared_key: ZBC::Zuora::Configuration.page['api_security_key'],
        subdomain: account.subdomain
      ).returns(stub(
        encrypted: 'subdomain_after_encryption',
        random_iv: 'something_random_generated_by_zbc'
      ))
    end

    describe '#create' do
      let(:content_type) { response.content_type.to_s }

      let(:response_encrypted) do
        JSON.parse(response.body)['token']['payload']
      end

      before { post :create }

      it { assert_response :success }

      it 'responds with content_type json' do
        content_type.must_equal 'application/json'
      end

      it 'encrypts the response in Base64' do
        response_encrypted.
          must_equal Base64.urlsafe_encode64('subdomain_after_encryption')
      end
    end

    let(:throttle_limit) { 10 }

    describe 'Arturo "throttle_zuora_credit_card_tokens" disabled' do
      before { Arturo.disable_feature! :throttle_zuora_credit_card_tokens }

      it 'is not throttled' do
        (throttle_limit + 1).times { post :create }
        assert_response :success
      end
    end

    describe 'Arturo "throttle_zuora_credit_card_tokens" disabled' do
      before { Arturo.enable_feature! :throttle_zuora_credit_card_tokens }

      it 'is throttled' do
        (throttle_limit + 1).times { post :create }
        assert_response too_many_requests
      end
    end
  end
end
