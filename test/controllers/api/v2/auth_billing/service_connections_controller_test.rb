require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 1

describe Api::V2::AuthBilling::ServiceConnectionsController do
  extend Api::V2::TestHelper

  fixtures :accounts

  let(:account)      { accounts(:minimum) }
  let(:user)         { account.owner }
  let(:subscription) { account.subscription }
  let(:assisted)     { anything }

  before do
    subscription.stubs(:assisted?).returns(assisted)
    @request.account = account
    accept :json
  end

  with_options(controller: 'api/v2/auth_billing/service_connections') do |request|
    request.should_route :get, '/api/v2/auth_billing/service_connection', action: 'show'
  end

  as_an_anonymous_user { should_be_unauthorized :show }
  as_an_end_user       { should_be_forbidden :show }
  as_an_agent          { should_be_forbidden :show }

  as_an_admin do
    before do
      @controller.stubs(:current_account).returns(account)
      @controller.stubs(:current_user).returns(user)
    end

    describe '#show' do
      let(:service_connection) { stub(build: {}) }
      let(:locale_param)       { { 'locale' => user.locale } }

      describe 'when account is a multi-product participant' do
        before do
          account.stubs(:billing_multi_product_participant?).returns(true)

          Billing::ServiceConnection::V2.expects(:new).with(
            account,
            user,
            base_url: 'https://minimum.zendesk-test.com',
            params:   locale_param
          ).returns(service_connection)

          get :show
        end

        it { assert_response :success }
        it { response.content_type.must_equal 'application/json' }

        should_use_presenter(
          Api::V2::Billing::ServiceConnectionPresenter,
          status: :success
        )
      end

      describe 'when account is not a multi-product participant' do
        before do
          account.stubs(:billing_multi_product_participant?).returns(false)
        end

        describe 'when account is Self-Service' do
          let(:assisted) { false }

          before do
            Billing::ServiceConnection::V1.expects(:new).with(
              account,
              user,
              locale_param
            ).returns(service_connection)

            get :show
          end

          it { assert_response :success }
          it { response.content_type.must_equal 'application/json' }

          should_use_presenter(
            Api::V2::Billing::ServiceConnectionPresenter,
            status: :success
          )
        end

        describe 'when account is Sales-Assisted' do
          let(:assisted) { true }

          before do
            Billing::ServiceConnection::V2.expects(:new).with(
              account,
              user,
              base_url: 'https://minimum.zendesk-test.com',
              params:   locale_param
            ).returns(service_connection)

            get :show
          end

          it { assert_response :success }
          it { response.content_type.must_equal 'application/json' }

          should_use_presenter(
            Api::V2::Billing::ServiceConnectionPresenter,
            status: :success
          )
        end
      end
    end
  end
end
