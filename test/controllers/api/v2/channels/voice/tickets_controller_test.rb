require_relative "../../../../../support/test_helper"
require_relative "../../../../../support/api_scopes_helper"
require 'zendesk/radar_factory'

SingleCov.covered!

describe Api::V2::Channels::Voice::TicketsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper

  fixtures :all

  before do
    accept :json
    use_ssl

    @account  = accounts(:minimum)
    @ticket   = tickets(:minimum_1)
    @agent    = users(:minimum_agent)
    @end_user = users(:minimum_end_user)

    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
    ::Voice::CtiUsage.any_instance.stubs(:app_id=).returns('123')
    ::Voice::CtiUsage.any_instance.stubs(:ticket_id=).returns(1023)
    ::Voice::CtiUsage.any_instance.stubs(:event=).returns(nil)
  end

  with_options(controller: 'api/v2/channels/voice/tickets') do |request|
    request.should_route :post, '/api/v2/channels/voice/agents/6/tickets/1/display', action: 'display', id: 1, agent_id: 6
    request.should_route :post, "/api/v2/channels/voice/tickets", action: "create"
  end

  describe "when using oauth tokens" do
    with_scopes('write') do
      should_be_authorized { post :display, params: { id: @ticket.nice_id, agent_id: @agent.id } }
      should_be_authorized do
        post :create, params: { ticket: {
          subject: "test",
          description: "test",
          via_id: Zendesk::Types::ViaType.API_VOICEMAIL.to_s
        } }
      end
    end

    with_scopes('tickets:write') do
      should_be_authorized { post :display, params: { id: @ticket.nice_id, agent_id: @agent.id } }
      should_be_authorized do
        post :create, params: { ticket: {
          subject: "test",
          description: "test",
          via_id: Zendesk::Types::ViaType.API_VOICEMAIL.to_s
        } }
      end
    end

    with_scopes('read') do
      should_not_be_authorized { post :display, params: { id: @ticket.nice_id, agent_id: @agent.id } }
      should_not_be_authorized do
        post :create, params: { ticket: {
          subject: "test",
          description: "test",
          via_id: Zendesk::Types::ViaType.API_VOICEMAIL.to_s
        } }
      end
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized [:post, :display, {id: 1, agent_id: 6}]
    should_be_unauthorized %i[post create]
  end

  as_an_end_user do
    should_be_forbidden [:post, :display, {id: 1, agent_id: 6}]
    should_be_forbidden %i[post create]
  end

  as_an_agent do
    describe "a POST to :display" do
      describe "with valid id and agent_id" do
        it "enqueues a radar status update job" do
          radar_notification = stub
          radar_notification.expects(:send).with("id", @ticket.nice_id).once
          ::RadarFactory.expects(:create_radar_notification).with(@account, "voice_integration_tickets/#{@agent.id}").returns(radar_notification).once
          post :display, params: { id: @ticket.nice_id, agent_id: @agent.id }
          assert_response :ok
        end
      end
      describe "with in invalid ticket id" do
        it "returns not found" do
          post :display, params: { id: 100000000, agent_id: @agent.id }
          assert_response :not_found
        end
      end
      describe "with a user_id that doesn't reference an agent (or user with agent permissions)" do
        it "returns not found" do
          post :display, params: { id: @ticket.nice_id, agent_id: @end_user.id }
          assert_response :not_found
        end
      end
    end

    describe "a POST to :create" do
      describe "display_to_agent" do
        before do
          @account = accounts(:minimum)
          @ticket = tickets(:minimum_1)
          @agent = users(:minimum_agent)

          @controller.stubs("initialized_ticket").returns(@ticket)
          @ticket.stubs("save!").returns(true)
        end

        describe "with a 'ticket' and valid 'display_to_agent' value" do
          it "enqueues a job to render ticket to agent" do
            radar_notification = stub
            radar_notification.expects(:send).with("id", @ticket.nice_id).once
            ::RadarFactory.expects(:create_radar_notification).with(@account, "voice_integration_tickets/#{@agent.id}").returns(radar_notification).once

            post :create, params: { display_to_agent: @agent.id, ticket: {
                subject: "test",
                description: "test",
                via_id: Zendesk::Types::ViaType.API_VOICEMAIL.to_s
              } }
          end
        end

        describe "with a 'ticket' and invalid 'display_to_agent' value" do
          it "does not enqueue a job to render ticket to agent" do
            ::RadarFactory.expects(:create_radar_notification).with(@account, "voice_integration_tickets/#{@agent.id}").never

            post :create, params: { display_to_agent: "a", ticket: {
                subject: "test",
                description: "test",
                via_id: Zendesk::Types::ViaType.API_VOICEMAIL.to_s
              } }
          end
        end

        describe "with a 'ticket' and no 'display_to_agent' value" do
          it "does not enqueue a job to render ticket to agent" do
            ::RadarFactory.expects(:create_radar_notification).with(@account, "voice_integration_tickets/#{@agent.id}").never

            post :create, params: { ticket: {
                subject: "test",
                description: "test",
                via_id: Zendesk::Types::ViaType.API_VOICEMAIL.to_s
              } }
          end
        end
      end

      describe "with via API_VOICEMAIL" do
        before do
          post :create, params: { ticket: {
          subject: "test",
          description: "test",
          via_id: Zendesk::Types::ViaType.API_VOICEMAIL.to_s
} }
        end

        should_use_presenter Api::V2::Tickets::TicketPresenter, status: :created, location: /\/api\/v2\/tickets\/\d+\.json$/

        should_change("the ticket count", by: 1) { Ticket.count(:all) }

        describe "response" do
          before { @ticket = JSON.parse(@response.body)["ticket"] }

          it "has 'api' via type and source rel 'voicemail'" do
            assert_equal "api", @ticket["via"]["channel"]
            assert_equal "voicemail", @ticket["via"]["source"]["rel"]
          end
        end
      end

      describe "with via PHONE_CALL_INBOUND" do
        before do
          post :create, params: { ticket: {
            subject: "test",
            description: "test",
            via_id: Zendesk::Types::ViaType.PHONE_CALL_INBOUND.to_s
          } }
          @ticket_response = JSON.parse(@response.body)["ticket"]
        end

        it "has 'voice' via type and rel 'inbound'" do
          assert_equal "voice", @ticket_response["via"]["channel"]
          assert_equal "inbound", @ticket_response["via"]["source"]["rel"]
        end

        describe "an audit" do
          it "has via_id set to PHONE_CALL_INBOUND" do
            ticket = Ticket.find_by_account_id_and_nice_id(@account.id, @ticket_response["id"])
            audit = ticket.audits.first
            assert_equal Zendesk::Types::ViaType.PHONE_CALL_INBOUND, audit.via_id
          end
        end
      end

      describe "with a via object" do
        before do
          post :create, params: { ticket: {
            subject: "test",
            description: "test",
            via: { channel: Zendesk::Types::ViaType.PHONE_CALL_INBOUND.to_s }
          } }
          @ticket_response = JSON.parse(@response.body)["ticket"]
        end

        it "has 'voice' via type and rel 'inbound'" do
          assert_equal "voice", @ticket_response["via"]["channel"]
          assert_equal "inbound", @ticket_response["via"]["source"]["rel"]
        end

        describe "an audit" do
          it "has via_id set to PHONE_CALL_INBOUND" do
            ticket = Ticket.find_by_account_id_and_nice_id(@account.id, @ticket_response["id"])
            audit = ticket.audits.first
            assert_equal Zendesk::Types::ViaType.PHONE_CALL_INBOUND, audit.via_id
          end
        end
      end

      describe "with voice comment" do
        let(:recording_url) { "http://yourdomain.com/recordings/1.mp3" } # Non-Twilio URL

        before do
          post :create, params: { ticket: {
            subject: "test",
            description: "test",
            via_id: Zendesk::Types::ViaType.API_VOICEMAIL.to_s,
            voice_comment: {
              from: "+16617480240",
              to: "+16617480123",
              recording_url: recording_url,
              started_at: "2013-07-11 15:31:44 +0000",
              call_duration: 40,
              answered_by_id: @agent.id,
              transcription_text: "The transcription of the call",
              location: "Dublin, Ireland"
            }
          } }
        end

        should_change("the ticket count", by: 1) { Ticket.count(:all) }

        describe "response" do
          it "includes the voice comment URL" do
            assert_includes @response.body, recording_url
          end
        end
      end

      describe "with via API_PHONE_CALL_INBOUND" do
        before do
          post :create, params: { ticket: {
            subject: "test",
            description: "test",
            via_id: Zendesk::Types::ViaType.API_PHONE_CALL_INBOUND.to_s
          } }
        end

        should_use_presenter Api::V2::Tickets::TicketPresenter, status: :created, location: /\/api\/v2\/tickets\/\d+\.json$/

        should_change("the ticket count", by: 1) { Ticket.count(:all) }

        describe "response" do
          before { @ticket = JSON.parse(@response.body)["ticket"] }
          it "has 'api' via type and source rel 'inbound'" do
            assert_equal "api", @ticket["via"]["channel"]
            assert_equal "inbound", @ticket["via"]["source"]["rel"]
          end
        end
      end

      describe "with via API_PHONE_CALL_OUTBOUND" do
        before do
          post :create, params: { ticket: {
            subject: "test",
            description: "test",
            via_id: Zendesk::Types::ViaType.API_PHONE_CALL_OUTBOUND.to_s
          } }
        end

        should_use_presenter Api::V2::Tickets::TicketPresenter, status: :created, location: /\/api\/v2\/tickets\/\d+\.json$/

        should_change("the ticket count", by: 1) { Ticket.count(:all) }

        describe "response" do
          before { @ticket = JSON.parse(@response.body)["ticket"] }
          it "has 'api' via type and source rel 'outbound'" do
            assert_equal "api", @ticket["via"]["channel"]
            assert_equal "outbound", @ticket["via"]["source"]["rel"]
          end
        end
      end

      describe 'CTI usage' do
        let(:create_ticket_request_data) do
          {
            display_to_agent: @agent.id,
            ticket: {
            subject: "test",
            description: "test",
            via_id: Zendesk::Types::ViaType.API_VOICEMAIL.to_s,
            voice_comment: {
              from: "+16617480240",
              to: "+16617480123",
              recording_url: "http://www.example.org/recording_url",
              started_at: "2013-07-11 15:31:44 +0000",
              call_duration: 40,
              answered_by_id: @agent.id,
              transcription_text: "The transcription of the call",
              location: "Dublin, Ireland"
              }
            }
          }
        end

        before { Account.any_instance.stubs(:has_cti_integrations?).returns(true) }

        describe '#create' do
          it 'is captured when it is not an internal request' do
            @controller.stubs(:internal_request?).returns(false)
            Voice::CtiUsage.any_instance.expects(:save).returns(true)
            Voice::CtiUsage.any_instance.expects(:account_id=).with(@account.id)
            Voice::CtiUsage.any_instance.expects(:user_id=).with(@user.id)
            Voice::CtiUsage.any_instance.expects(:action=).with('tickets#create')
            post :create, params: create_ticket_request_data
          end

          it 'is not captured when it is an internal request from Voice' do
            @controller.stubs(:internal_request?).returns(true)
            Voice::CtiUsage.any_instance.expects(:save).never
            post :create, params: create_ticket_request_data
          end

          it 'is captured when via_id is missing' do
            @controller.stubs(:internal_request?).returns(false)
            Voice::CtiUsage.any_instance.expects(:save).returns(true)
            create_ticket_request_data[:ticket].delete(:via_id)
            post :create, params: create_ticket_request_data
          end
        end

        describe '#display' do
          it 'is captured' do
            Voice::CtiUsage.any_instance.expects(:save).returns(true)
            post :display, params: { id: @ticket.nice_id, agent_id: @agent.id }
          end

          it 'logs on error level when saving the usage could not happen' do
            Voice::CtiUsage.any_instance.expects(:save).returns(false)
            Rails.logger.expects(:error).with(regexp_matches(/CTI/))
            post :display, params: { id: @ticket.nice_id, agent_id: @agent.id }
          end
        end
      end
    end
  end
end
