require_relative "../../../../../support/test_helper"
require_relative "../../../../../support/api_scopes_helper"
require 'zendesk/radar_factory'

SingleCov.covered!

describe Api::V2::Channels::Voice::AppInteractionsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper

  fixtures :all

  before do
    accept :json
    use_ssl
    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
    ::Voice::CtiUsage.any_instance.stubs(:event=).returns('dialout')
    ::Voice::CtiUsage.any_instance.stubs(:app_id=).returns('123')
    ::Voice::CtiUsage.any_instance.stubs(:ticket_id=).returns(nil)
    # Need to stub the 'action=' setter & return a string <= 20 chars due to DB constraint.
    ::Voice::CtiUsage.any_instance.stubs(:action=).returns('app_interaction#crea')
  end

  as_an_agent do
    describe 'a POST to :create' do
      before { @controller.stubs(:internal_request?).returns(false) }

      describe 'When event param is sent' do
        it 'returns 204 no_content' do
          post :create, params: { event: 'dial out' }
          assert_response :no_content
        end
      end

      describe 'when event param is NOT sent' do
        it 'returns 400 bad request' do
          post :create
          assert_response :bad_request
        end
      end
    end
  end
end
