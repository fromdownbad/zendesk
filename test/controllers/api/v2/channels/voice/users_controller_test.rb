require_relative "../../../../../support/test_helper"
require_relative "../../../../../support/api_scopes_helper"
require 'zendesk/radar_factory'

SingleCov.covered!

describe Api::V2::Channels::Voice::UsersController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper

  fixtures :all

  before do
    accept :json
    use_ssl

    @account  = accounts(:minimum)
    @agent    = users(:minimum_agent)
    @end_user = users(:minimum_end_user)

    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
    ::Voice::CtiUsage.any_instance.stubs(:app_id=).returns('123')
    ::Voice::CtiUsage.any_instance.stubs(:event=).returns(nil)
    ::Voice::CtiUsage.any_instance.stubs(:ticket_id=).returns(nil)
  end

  with_options(controller: 'api/v2/channels/voice/users') do |request|
    request.should_route :post, '/api/v2/channels/voice/agents/6/users/1/display', action: 'display', id: 1, agent_id: 6
  end

  describe "when using oauth tokens" do
    with_scopes('write') do
      should_be_authorized { post :display, params: { id: @end_user.id, agent_id: @agent.id } }
    end

    with_scopes('read') do
      should_not_be_authorized { post :display, params: { id: @end_user.id, agent_id: @agent.id } }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized [:post, :display, {id: 1, agent_id: 6}]
  end

  as_an_end_user do
    should_be_forbidden [:post, :display, {id: 1, agent_id: 6}]
  end

  as_an_agent do
    describe "a POST to :display" do
      describe "with valid id and agent_id" do
        it "enqueues a radar status update job" do
          radar_notification = stub
          radar_notification.expects(:send).with("id", @end_user.id).once
          ::RadarFactory.expects(:create_radar_notification).with(@account, "voice_integration_users/#{@agent.id}").returns(radar_notification).once
          post :display, params: { id: @end_user.id, agent_id: @agent.id }
          assert_response :ok
        end
      end

      describe "with in invalid user id" do
        it "returns not found" do
          post :display, params: { id: 1000000, agent_id: @agent.id }
          assert_response :not_found
        end
      end

      describe "with an agent user_id that doesn't reference an agent (or user with agent permissions)" do
        it "returns not found" do
          post :display, params: { id: @end_user.id, agent_id: 10000000 }
          assert_response :not_found
        end
      end

      describe 'CTI usage' do
        it 'is captured' do
          Account.any_instance.stubs(:has_cti_integrations?).returns(true)
          @controller.stubs(:internal_request?).returns(false)

          ::Voice::CtiUsage.any_instance.expects(:save).returns(false)
          post :display, params: { id: @end_user.id, agent_id: @agent.id }
        end
      end
    end
  end
end
