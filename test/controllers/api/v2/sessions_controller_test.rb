require_relative "../../../support/test_helper"
require_relative '../../../support/api_scopes_helper'

SingleCov.covered! uncovered: 2

describe Api::V2::SessionsController do
  extend Api::V2::TestHelper
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper

  fixtures :accounts, :users

  before do
    accept :json
    @account = accounts(:minimum)
    @user    = users(:minimum_end_user)
    @owner   = @account.owner

    @agent_sessions = [FactoryBot.create(:session_record, account: @account, user: @user),
                       FactoryBot.create(:session_record, account: @account, user: @user)]
    @owner_session = FactoryBot.create(:session_record, account: @account, user: @owner)

    @invalid_sessions = [FactoryBot.create(:session_record, account: @account, user: @owner, expire_at: Time.now - 1.hour)]

    @account_sessions = @agent_sessions + [@owner_session]
  end

  with_options(controller: 'api/v2/sessions') do |request|
    request.should_route :get, '/api/v2/sessions', action: 'index'
    request.should_route :get, '/api/v2/users/123/sessions', action: 'index', user_id: 123
    request.should_route :get, '/api/v2/users/123/sessions/1', action: 'show', user_id: 123, id: 1
    request.should_route :delete, '/api/v2/users/123/sessions/1', action: 'destroy', user_id: 123, id: 1
    request.should_route :delete, '/api/v2/users/123/sessions', action: 'destroy_many', user_id: 123
    request.should_route :delete, '/api/v2/users/me/logout', action: 'logout'
    request.should_route :get, 'api/v2/users/me/session', action: 'show_current'
    request.should_route :get, 'api/v2/users/me/session/renew', action: 'renew'
  end

  describe 'with an oauth token' do
    with_scopes('auditlogs:read') do
      should_not_be_authorized { get :index }
      should_not_be_authorized { get :show, params: { user_id: @owner.id, id: @owner_session.id } }
      should_not_be_authorized { delete :destroy, params: { user_id: @owner.id, id: @owner_session.id } }
      should_not_be_authorized { delete :destroy_many, params: { user_id: @owner.id } }
      should_not_be_authorized { delete :logout }
    end

    with_scopes('read') do
      should_be_authorized { get :index }
      should_be_authorized { get :show, params: { user_id: @owner.id, id: @owner_session.id } }
      should_not_be_authorized { delete :destroy, params: { user_id: @owner.id, id: @owner_session.id } }
      should_not_be_authorized { delete :destroy_many, params: { user_id: @owner.id } }
      should_not_be_authorized { delete :logout }
    end

    with_scopes('write') do
      should_not_be_authorized { get :index }
      should_not_be_authorized { get :show, params: { user_id: @owner.id, id: @owner_session.id } }
      should_be_authorized { delete :destroy, params: { user_id: @owner.id, id: @owner_session.id } }
      should_be_authorized { delete :destroy_many, params: { user_id: @owner.id } }
      should_be_authorized { delete :logout }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index, {user_id: 1}]
  end

  when_logged_in_as(:minimum_admin) do
    describe "a GET to :index" do
      before { get :index }

      should_paginate :session_records
      should_use_presenter Api::V2::SessionRecordPresenter, status: 200

      it "returns sessions records order by user_id" do
        session_scope = @account.shared_sessions.unexpired
        assert_equal session_scope.order('id'), @controller.send(:sessions_scope)
      end

      it "presents all sessions for the account" do
        results = JSON.parse(@response.body)['sessions'].map { |h| h["id"] }.sort
        assert_equal @account_sessions.map(&:id), results
      end

      it "sets proper caching headers" do
        assert_equal "no-store", response.headers['Cache-Control']
        assert_equal "-1", response.headers["Expires"]
        assert_nil response.headers["Etag"]
      end
    end

    describe "a GET to :show can access all account sessions" do
      before { get :show, params: { user_id: @agent_sessions[0].user_id, id: @agent_sessions[0].id } }

      should_use_presenter Api::V2::SessionRecordPresenter, status: 200

      it "presents session" do
        result = JSON.parse(@response.body)['session']
        assert_equal @agent_sessions[0].id, result['id']
      end

      it "sets proper caching headers" do
        assert_equal "no-store", response.headers['Cache-Control']
        assert_equal "-1", response.headers["Expires"]
        assert_nil response.headers["Etag"]
      end
    end

    describe "a DELETE to destroy can delete a session from this account" do
      it "deletes the session" do
        delete :destroy, params: { user_id: @agent_sessions[0].user_id, id: @agent_sessions[0].id }
        assert_nil Zendesk::SharedSession::SessionRecord.find_by_id(@agent_sessions[0].id)
        assert_response :no_content
      end
    end
  end

  as_an_end_user do
    describe "a GET to :index" do
      before { get :index, params: { user_id: @user.id } }

      should_paginate :session_records
      should_use_presenter Api::V2::SessionRecordPresenter, status: 200

      it "returns sessions records order by user_id" do
        session_scope = @account.shared_sessions.unexpired.where(user_id: @user.id)
        assert_equal session_scope.order('id'), @controller.send(:sessions_scope)
      end

      it "returns all sessions for the user" do
        results = JSON.parse(@response.body)['sessions'].map { |h| h["id"] }.sort
        assert_equal @agent_sessions.map(&:id), results
      end

      it "sets proper caching headers" do
        assert_equal "no-store", response.headers['Cache-Control']
        assert_equal "-1", response.headers["Expires"]
        assert_nil response.headers["Etag"]
      end
    end

    describe "a GET to :show can access a session for this user" do
      before { get :show, params: { user_id: @agent_sessions[0].user_id, id: @agent_sessions[0].id } }

      should_use_presenter Api::V2::SessionRecordPresenter, status: 200

      it "presents session" do
        result = JSON.parse(@response.body)['session']
        assert_equal @agent_sessions[0].id, result['id']
      end

      it "sets proper caching headers" do
        assert_equal "no-store", response.headers['Cache-Control']
        assert_equal "-1", response.headers["Expires"]
        assert_nil response.headers["Etag"]
      end
    end

    describe "a GET to :show can't access a session for another user" do
      before { get :show, params: { user_id: @owner.id, id: @owner_session.id } }

      it "does not be found" do
        assert_response :not_found
      end
    end

    describe "a DELETE to destroy can delete a session for this user" do
      before { delete :destroy, params: { user_id: @agent_sessions[0].user_id, id: @agent_sessions[0].id } }

      it "deletes the session" do
        assert_response :no_content
        assert_nil Zendesk::SharedSession::SessionRecord.find_by_id(@agent_sessions[0].id)
      end

      it "only delete one session" do
        assert_equal 1, Zendesk::SharedSession::SessionRecord.where(user_id: @user.id).count(:all)
      end
    end

    describe "a DELETE to destroy can't delete a session for another user" do
      before { delete :destroy, params: { user_id: @owner.id, id: @owner_session.id } }

      it "does not be found" do
        assert_response :not_found
      end
    end

    describe "a DELETE to destroy_many can delete all the sessions for this user" do
      it "deletes all sessions" do
        delete :destroy_many, params: { user_id: @agent_sessions[0].user_id }
        assert_response :no_content
        assert_equal 0, Zendesk::SharedSession::SessionRecord.where(user_id: @user.id).count(:all)
      end
    end

    describe "a DELETE to destroy_many can't delete sessions for another user" do
      before { delete :destroy_many, params: { user_id: @owner.id } }

      it "does not be found" do
        assert_response :not_found
      end
    end

    describe "a DELETE to logout will delete the current session" do
      before do
        shared_session.account_id = @account.id
        shared_session[Zendesk::SharedSession::Session::USER_KEY] = @user.id
        assert shared_session.save
      end

      it "deletes the session" do
        delete :logout, params: { user_id: 'me' }
        assert_response :no_content
        assert_empty Zendesk::SharedSession::SessionRecord.where(session_id: shared_session.id)
      end
    end

    describe "a GET to renew" do
      it "returns the authenticity_token" do
        get :renew
        assert_response :ok
        assert @controller.send(:form_authenticity_token), JSON.parse(@response.body)['authenticity_token']
      end

      it "sets proper caching headers" do
        get :renew

        assert_equal "no-store", response.headers['Cache-Control']
        assert_equal "-1", response.headers["Expires"]
        assert_nil response.headers["Etag"]
      end

      it 'returns a valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
        get :renew
        assert_equal response.headers['Access-Control-Allow-Origin'], 'http://app.futuresimple.com'
      end

      it 'returns a not valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
        get :renew
        assert_nil response.headers['Access-Control-Allow-Origin']
      end
    end
  end
end
