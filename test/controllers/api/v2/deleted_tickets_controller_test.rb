require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::DeletedTicketsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
    @deleted_ticket = tickets(:minimum_3)
    delete_and_reload_ticket(@deleted_ticket)
  end

  with_options(controller: "api/v2/deleted_tickets") do |request|
    request.should_route :get, "/api/v2/deleted_tickets", action: "index"
    request.should_route :delete, "api/v2/deleted_tickets/destroy_many", action: "destroy_many"
    request.should_route :delete, "api/v2/deleted_tickets/1", action: "destroy", id: 1
    request.should_route :put, "api/v2/deleted_tickets/1/restore", action: "restore", id: 1
    request.should_route :put, "api/v2/deleted_tickets/restore_many", action: "restore_many"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :destroy, :destroy_many, [:put, :restore, id: 1], :restore_many
  end

  as_an_end_user do
    should_be_forbidden :index, :destroy, :destroy_many, [:put, :restore, id: 1], :restore_many
  end

  as_an_agent_with_permissions(ticket_deletion: false) do
    should_be_forbidden [:put, :restore, id: 1], :restore_many, :destroy, :destroy_many
  end

  as_an_agent_with_permissions(view_deleted_tickets: false) do
    should_be_forbidden :destroy, :destroy_many, :index
  end

  as_an_agent_with_permissions(view_deleted_tickets: true) do
    describe "a GET to :index" do
      before { get :index }
      should_paginate :tickets
      should_use_presenter Api::Lotus::DeletedTicketPresenter, with: :tickets
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end
  end

  as_an_admin do
    describe "a GET to :index" do
      before { get :index }
      should_paginate :tickets
      should_use_presenter Api::Lotus::DeletedTicketPresenter, with: :tickets
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "a GET to :index with sorting" do
      let(:sort_order) { 'desc' }

      before { get :index, params: { sort_by: sort_by, sort_order: sort_order } }

      describe "using 'subject'" do
        let(:sort_by) { 'subject' }

        it('sorts by subject') { assert_equal 'subject desc', @controller.send(:sorting) }
      end

      describe "using 'id'" do
        let(:sort_by) { 'id' }

        it('sorts by nice_id') { assert_equal 'nice_id desc', @controller.send(:sorting) }
      end

      describe "using 'deleted_at'" do
        let(:sort_by) { 'deleted_at' }

        it('sorts by updated_at') { assert_equal 'updated_at desc', @controller.send(:sorting) }
      end

      describe "using 'actor_name'" do
        let(:sort_by) { 'actor_name' }

        it('does not sort by actor_name') { assert_equal 'created_at desc, id desc', @controller.send(:sorting) }
      end
    end
  end

  as_an_agent_with_permissions(view_deleted_tickets: true, ticket_deletion: true) do
    describe "a DELETE to :destroy" do
      before do
        t = tickets(:minimum_1)
        delete_and_reload_ticket(t)
        delete :destroy, params: { id: t.nice_id }
      end
      should_use_presenter Api::V2::JobStatusPresenter
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "a DELETE to :destroy with invalid ticket" do
      before { delete :destroy, params: { id: 99999 } }
      it('responds with not_found') { assert_response :not_found }
    end
  end

  as_an_admin do
    describe "a DELETE to :destroy" do
      before { delete :destroy, params: { id: @deleted_ticket.nice_id } }
      should_use_presenter Api::V2::JobStatusPresenter
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "a DELETE to :destroy with invalid ticket" do
      before { delete :destroy, params: { id: 99999 } }
      it('responds with not_found') { assert_response :not_found }
    end

    describe "a DELETE to :destroy with a ticket that has not been soft deleted" do
      describe_with_arturo_disabled :remove_soft_deletion_requirement do
        before { delete :destroy, params: { id: tickets(:minimum_1).nice_id } }
        it ('responds with not_found') { assert_response :not_found }
      end

      describe_with_arturo_enabled :remove_soft_deletion_requirement do
        before do
          delete :destroy, params: { id: tickets(:minimum_1).nice_id, allow_undeleted_tickets: allow_param }
        end

        describe 'with allow_undeleted_tickets = unsafe' do
          let(:allow_param) { 'unsafe' }

          it ('responds with ok') { assert_response :ok }
        end

        describe 'with allow_undeleted_tickets = true' do
          let(:allow_param) { true }

          it ('responds with bad_request') { assert_response :bad_request }
        end
      end
    end

    describe "a DELETE to :destroy_many" do
      before do
        deleted_ticket2 = tickets(:minimum_2)
        delete_and_reload_ticket(deleted_ticket2)
        delete :destroy_many, params: { ids: [@deleted_ticket.nice_id, deleted_ticket2.nice_id].join(",") }
      end

      should_use_presenter Api::V2::JobStatusPresenter
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "a DELETE to :destroy_many without many_ids" do
      before { delete :destroy_many }

      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "a DELETE to :destroy_many with a ticket that has not been soft deleted" do
      describe_with_arturo_disabled :remove_soft_deletion_requirement do
        before do
          delete :destroy_many, params: { ids: [@deleted_ticket.nice_id, tickets(:minimum_2).nice_id].join(",") }
        end

        it ('responds with not_found') { assert_response :not_found }
      end

      describe_with_arturo_enabled :remove_soft_deletion_requirement do
        before do
          delete :destroy_many, params: { ids: ticket_ids, allow_undeleted_tickets: allow_param }
        end

        describe 'with allow_undeleted_tickets = unsafe' do
          let(:ticket_ids) { [@deleted_ticket.nice_id, tickets(:minimum_2).nice_id].join(",") }
          let(:allow_param) { 'unsafe' }

          it ('responds with ok') { assert_response :ok }

          describe 'with an unknown ticket' do
            let(:ticket_ids) { [@deleted_ticket.nice_id, (accounts(:minimum).tickets.last.nice_id + 11)].join(",") }

            it ('responds with not_found') { assert_response :not_found }
          end
        end

        describe 'with allow_undeleted_tickets = true' do
          let(:ticket_ids) { [@deleted_ticket.nice_id, tickets(:minimum_2).nice_id].join(",") }
          let(:allow_param) { true }

          it ('responds with bad_request') { assert_response :bad_request }
        end
      end
    end

    describe "a DELETE to :destroy_many with a ticket that has been archived" do
      before do
        delete_and_reload_ticket(@deleted_ticket)
        archive_and_delete(@deleted_ticket)
        deleted_ticket2 = tickets(:minimum_2)
        delete_and_reload_ticket(deleted_ticket2)
        delete :destroy_many, params: { ids: [@deleted_ticket.nice_id, deleted_ticket2.nice_id].join(",") }
      end
      it('responds with success') { assert_response :success }
    end

    describe "a DELETE to :destroy_many with multiple instances of the same id" do
      before do
        deleted_ticket2 = tickets(:minimum_2)
        delete_and_reload_ticket(deleted_ticket2)
        delete :destroy_many, params: { ids: [@deleted_ticket.nice_id, deleted_ticket2.nice_id, @deleted_ticket.nice_id].join(",") }
      end
      it('responds with success') { assert_response :success }
    end
  end

  as_an_agent_with_permissions(ticket_deletion: true) do
    describe "a DELETE to :destroy_many" do
      before do
        deleted_ticket2 = tickets(:minimum_2)
        delete_and_reload_ticket(deleted_ticket2)
        delete :destroy_many, params: { ids: [@deleted_ticket.nice_id, deleted_ticket2.nice_id].join(",") }
      end

      should_use_presenter Api::V2::JobStatusPresenter
      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "a DELETE to :destroy_many without many_ids" do
      before { delete :destroy_many }

      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "a PUT to :restore_many with many_ids set to nothing" do
      before { put :restore_many, params: { ids: nil } }

      it('responds with bad request') { assert_response :bad_request }
      it('responds with Invalid Attribute') { assert_includes @controller.response.body, 'Invalid attribute' }
    end

    describe "a DELETE to :destroy_many with a ticket that has not been soft deleted" do
      before do
        delete :destroy_many, params: { ids: [@deleted_ticket.nice_id, tickets(:minimum_2).nice_id].join(",") }
      end
      it ('responds with not_found') { assert_response :not_found }
    end
  end

  as_an_agent_with_permissions(ticket_deletion: true) do
    describe "a PUT to :restore" do
      before do
        ticket = delete_and_reload_ticket(tickets(:minimum_1))
        Ticket.any_instance.expects(:valid?).never
        put :restore, params: { id: ticket.nice_id }
      end

      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "a PUT to :restore when user can't view deleted tickets" do
      let(:ticket) { delete_and_reload_ticket(tickets(:minimum_1)) }

      before do
        @user.stubs(:can?).with(:delete, Ticket).returns(true)
        @user.stubs(:can?).with(:view_deleted, Ticket).returns(false)
      end

      describe "and can edit the ticket" do
        before do
          @user.stubs(:can?).with(:edit, ticket).returns(true)
          put :restore, params: { id: ticket.nice_id }
        end

        it('responds with success') { assert_response :success }
      end

      describe "and cannot edit the ticket" do
        before do
          @user.stubs(:can?).with(:edit, ticket).returns(false)
          put :restore, params: { id: ticket.nice_id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe_with_arturo_disabled :ticket_restore_access_improvement do
        describe "and cannot edit the ticket" do
          before do
            @user.stubs(:can?).with(:edit, ticket).returns(false)
            put :restore, params: { id: ticket.nice_id }
          end

          it('responds with success') { assert_response :success }
        end
      end
    end

    describe "a PUT to :restore with a ticket that has not been deleted" do
      before do
        ticket = tickets(:minimum_1)
        put :restore, params: { id: ticket.nice_id }
      end

      it('responds with record not found') { assert_response :not_found }
    end

    describe "a PUT to :restore with a ticket that fails being soft_undeleted" do
      before do
        @ticket = delete_and_reload_ticket(tickets(:minimum_1))
        Ticket.connection.expects(:update).returns(false)
        Ticket.any_instance.expects(:valid?).never
      end

      # it 'raises RecordNotSaved' do
      #   assert_raises(ActiveRecord::RecordNotSaved) do
      #     put :restore, id: @ticket.nice_id
      #   end
      # end
    end

    describe "a PUT to :restore_many" do
      before do
        @ticket1 = delete_and_reload_ticket(tickets(:minimum_1))
        @ticket2 = delete_and_reload_ticket(tickets(:minimum_2))
        Ticket.any_instance.expects(:valid?).never
        put :restore_many, params: { ids: [@ticket1.nice_id, @ticket2.nice_id].join(",") }
      end

      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "a PUT to :restore_many when user can't view deleted tickets" do
      let(:ticket1) { delete_and_reload_ticket(tickets(:minimum_1)) }
      let(:ticket2) { delete_and_reload_ticket(tickets(:minimum_2)) }

      before do
        @user.stubs(:can?).with(:delete, Ticket).returns(true)
        @user.stubs(:can?).with(:view_deleted, Ticket).returns(false)
      end

      describe "and can edit all tickets" do
        before do
          @user.stubs(:can?).with(:edit, ticket1).returns(true)
          @user.stubs(:can?).with(:edit, ticket2).returns(true)
          put :restore_many, params: { ids: [ticket1.nice_id, ticket2.nice_id].join(",") }
        end

        it('responds with success') { assert_response :success }
      end

      describe "and cannot edit some tickets" do
        before do
          @user.stubs(:can?).with(:edit, ticket1).returns(false)
          @user.stubs(:can?).with(:edit, ticket2).returns(true)
          put :restore_many, params: { ids: [ticket1.nice_id, ticket2.nice_id].join(",") }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe_with_arturo_disabled :ticket_restore_access_improvement do
        describe "and cannot edit some tickets" do
          before do
            @user.stubs(:can?).with(:edit, ticket1).returns(false)
            @user.stubs(:can?).with(:edit, ticket2).returns(true)
            put :restore_many, params: { ids: [ticket1.nice_id, ticket2.nice_id].join(",") }
          end

          it('responds with success') { assert_response :success }
        end
      end
    end

    describe "a PUT to :restore_many without many_ids" do
      before { put :restore_many }

      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "a PUT to :restore_many with many_ids set to nothing" do
      before { put :restore_many, params: { ids: nil } }

      it('responds with bad request') { assert_response :bad_request }
      it('responds with Invalid Attribute') { assert_includes @controller.response.body, 'Invalid attribute' }
    end

    describe "a PUT to :restore_many with a ticket that fails being soft_undeleted" do
      before do
        @ticket1 = delete_and_reload_ticket(tickets(:minimum_1))
        @ticket2 = delete_and_reload_ticket(tickets(:minimum_2))
        Ticket.connection.expects(:update).returns(false)
      end

      # it 'raises RecordNotSaved' do
      #   assert_raises(ActiveRecord::RecordNotSaved) do
      #     put :restore_many, ids: [@ticket1.nice_id, @ticket2.nice_id].join(",")
      #   end
      # end
    end
  end

  as_an_admin do
    describe "a PUT to :restore" do
      before do
        ticket = delete_and_reload_ticket(tickets(:minimum_1))
        Ticket.any_instance.expects(:valid?).never
        put :restore, params: { id: ticket.nice_id }
      end

      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end
  end

  as_an_admin do
    describe "a PUT to :restore_many" do
      before do
        @ticket1 = delete_and_reload_ticket(tickets(:minimum_1))
        @ticket2 = delete_and_reload_ticket(tickets(:minimum_2))
        Ticket.any_instance.expects(:valid?).never
        put :restore_many, params: { ids: [@ticket1.nice_id, @ticket2.nice_id].join(",") }
      end

      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "a PUT to :restore_many without many_ids" do
      before { delete :restore_many }

      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end
  end
end
