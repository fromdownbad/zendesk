require_relative "../../../support/test_helper"
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::V2::PushNotificationDevicesController do
  extend Api::V2::TestHelper
  fixtures :accounts, :custom_field_options, :organizations, :tickets, :ticket_fields, :users,
    :role_settings, :organization_memberships, :mobile_sdk_apps

  before do
    accept :json
  end

  with_options(controller: "api/v2/push_notification_devices") do |request|
    request.should_route :post, "/api/v2/push_notification_devices/destroy_many.json", action: :destroy_many, format: "json"
  end

  as_an_anonymous_user do
    should_be_unauthorized :destroy_many
  end

  as_an_end_user do
    should_be_forbidden :destroy_many
  end

  as_an_admin do
    describe "POST to #destroy_many" do
      before do
        PushNotifications::DeviceIdentifier.without_arsi.delete_all
        PushNotifications::SdkAnonymousDeviceIdentifier.create!(
          token: "bar",
          device_type: "iphone",
          account: @account,
          user_sdk_identity: user_identities(:mobile_sdk_identity),
          mobile_sdk_app: mobile_sdk_apps(:minimum_sdk)
        )
        PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(
          token: "quux",
          device_type: "iphone",
          account: @account,
          user: @user,
          mobile_sdk_app: mobile_sdk_apps(:minimum_sdk)
        )

        @destroy_many_params = {
          push_notification_devices: ["bar", "quux"]
        }
      end

      it "removes the push notification devices" do
        assert_difference "PushNotifications::DeviceIdentifier.count(:all)", -2 do
          post :destroy_many, params: @destroy_many_params
        end
        assert_equal @account.device_identifiers(true).where(token: ["bar", "quux"]).all, []
      end

      it "only removes the device identifiers belonging the current account" do
        other_account = accounts(:minimum_sdk)
        PushNotifications::SdkAuthenticatedDeviceIdentifier.create!(
          token: "baz",
          device_type: "iphone",
          account: other_account,
          user: @user,
          mobile_sdk_app: mobile_sdk_apps(:minimum_sdk)
        )
        post :destroy_many, params: { push_notification_devices: ["baz", "bar", "quux"] }
        assert other_account.device_identifiers(true).where(token: "baz").first
      end

      it "sends a log message when the object could not be deleted" do
        PushNotifications::SdkAuthenticatedDeviceIdentifier.any_instance.stubs(:destroy).returns(false)
        PushNotifications::SdkAnonymousDeviceIdentifier.any_instance.stubs(:destroy).returns(false)
        Rails.logger.expects(:warn).with("Could not delete push_notification_device: bar").once
        Rails.logger.expects(:warn).with("Could not delete push_notification_device: quux").once
        post :destroy_many, params: @destroy_many_params
      end

      describe "HTTP Interaction" do
        before do
          post :destroy_many, params: @destroy_many_params
        end

        it('responds with ok') { assert_response :ok }
      end
    end
  end
end
