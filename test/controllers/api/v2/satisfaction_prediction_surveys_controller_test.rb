require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::SatisfactionPredictionSurveysController do
  extend Api::V2::TestHelper

  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    accept :json
    @ticket = tickets(:minimum_1)
  end

  with_options(controller: 'api/v2/satisfaction_prediction_surveys') do |request|
    request.should_route :post, '/api/v2/tickets/1/satisfaction_prediction_surveys', action: 'create', ticket_id: '1'
  end

  as_an_anonymous_user do
    should_be_unauthorized [:post, :create, {ticket_id: 1000}]
  end

  as_an_agent do
    describe "a POST to #create" do
      describe "with a valid ticket id" do
        it('responds with 200 if params is present') do
          post :create, params: { ticket_id: @ticket.nice_id, reason: 'may day may day', prediction_value: 0.01 }
          assert_response :ok
        end

        it('responds with a 422 if params is not present') do
          post :create, params: { ticket_id: @ticket.nice_id, prediction_value: 0.01 }
          assert_response :unprocessable_entity
        end
      end

      describe "with an invalid ticket id" do
        before { post :create, params: { ticket_id: 1000 } }

        it('responds with not_found') { assert_response :not_found }
      end
    end
  end
end
