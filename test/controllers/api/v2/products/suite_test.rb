require_relative "../../../../support/test_helper"
require_relative '../../../../support/account_products_test_helper'

SingleCov.covered!

describe Api::V2::Products::Suite do
  extend Api::V2::TestHelper
  include AccountProductsTestHelper

  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:agent) { users(:minimum_agent) }
  let(:admin) { users(:minimum_admin) }
  let(:entitlements) { { entitlements: { } } }

  before do
    stub_entitlements_response(entitlements.to_json, account, agent.id)
  end

  describe '#products' do
    let(:suite) { Api::V2::Products::Suite.new(account, agent) }

    it 'generates a list of Products' do
      suite.products.each do |product|
        assert_equal Api::V2::Products::Suite::Product, product.class
      end
    end
  end

  describe 'Lotus' do
    let(:suite) { Api::V2::Products::Suite.new(account, agent) }

    it 'always returns Lotus' do
      assert suite.available.map(&:key).include?(:lotus)
    end
  end

  describe :Guide do
    let(:suite) { Api::V2::Products::Suite.new(account, agent) }

    it 'returns Guide' do
      assert suite.available.map(&:key).include?(:guide)
    end
  end

  describe :Gather do
    let(:suite) { Api::V2::Products::Suite.new(account, agent) }

    describe 'when the account is not spp' do
      it 'should return gather' do
        account.stub :spp?, false do
          suite = Api::V2::Products::Suite.new(account, agent)
          assert suite.available.map(&:key).include?(:gather)
        end
      end
    end

    describe 'when the account is spp' do
      let(:client_products) do
        Zendesk::Accounts::Client.any_instance.expects(:products).
          with(use_cache: true).once
      end

      describe 'active gather plan' do
        before do
          client_products.returns([
                                    Zendesk::Accounts::Product.new(
                                      make_product(product_name: 'gather')
                                    )
                                  ])
        end

        it 'should return gather' do
          account.stub :spp?, true do
            suite = Api::V2::Products::Suite.new(account, agent)
            assert suite.available.map(&:key).include?(:gather)
          end
        end
      end

      describe 'inactive gather plan' do
        before do
          client_products.returns([
                                    Zendesk::Accounts::Product.new(
                                      make_product(product_name: 'gather', state: 'inactive')
                                    )
                                  ])
        end

        it 'should not return gather' do
          account.stub :spp?, true do
            suite = Api::V2::Products::Suite.new(account, agent)
            refute suite.available.map(&:key).include?(:gather)
          end
        end
      end
    end
  end

  describe :Chat do
    let(:suite) { Api::V2::Products::Suite.new(account, agent) }

    describe 'with feature chat_product_tray enabled' do
      before { Arturo.enable_feature!(:chat_product_tray) }
      it 'returns Chat' do
        assert suite.available.map(&:key).include?(:chat)
      end
    end

    describe 'with feature chat_product_tray disabled' do
      before { Arturo.disable_feature!(:chat_product_tray) }
      it 'does not return Chat' do
        refute suite.available.map(&:key).include?(:chat)
      end
    end
  end

  describe :Talk do
    let(:suite) { Api::V2::Products::Suite.new(account, agent) }

    describe 'with feature talk_product_tray enabled' do
      before { Arturo.enable_feature!(:talk_product_tray) }
      it 'returns Talk' do
        assert suite.available.map(&:key).include?(:talk)
      end
    end

    describe 'with feature talk_product_tray disabled' do
      before { Arturo.disable_feature!(:talk_product_tray) }
      it 'does not return Talk' do
        refute suite.available.map(&:key).include?(:talk)
      end
    end
  end

  describe :Sell do
    let(:suite) { Api::V2::Products::Suite.new(account, agent) }
    describe 'with feature sell_product_tray enabled' do
      before { Arturo.enable_feature!(:sell_product_tray) }
      it 'returns Sell' do
        assert suite.available.map(&:key).include?(:sell)
      end
    end

    describe 'with feature sell_product_tray disabled' do
      before { Arturo.disable_feature!(:sell_product_tray) }

      it 'does not return Sell' do
        refute suite.available.map(&:key).include?(:sell)
      end
    end
  end

  describe :Explore do
    let(:suite) { Api::V2::Products::Suite.new(account, agent) }

    let(:client_products) do
      Zendesk::Accounts::Client.any_instance.expects(:products).
        with(use_cache: true).once
    end

    describe 'with explore_product_tray_insights enabled' do
      before { Arturo.enable_feature!(:explore_product_tray_insights) }

      describe 'when an exception is raised while fetching explore product' do
        before { client_products.raises(Exception) }
        it 'does not return Explore' do
          refute suite.available.map(&:key).include?(:explore)
        end
      end

      describe 'when explore product is not present' do
        before { client_products.returns(nil) }
        it 'does not return Explore' do
          refute suite.available.map(&:key).include?(:explore)
        end
      end

      describe 'when explore product insight plan setting is good_data' do
        before do
          client_products.returns([
                                    Zendesk::Accounts::Product.new(
                                      make_product(product_name: 'support', plan_settings: {"insights" => "good_data"})
                                    )
                                  ])
        end

        it 'does not return Explore' do
          refute suite.available.map(&:key).include?(:explore)
        end
      end

      describe 'when explore product insight plan setting is good_data_and_explore' do
        before do
          client_products.returns([
                                    Zendesk::Accounts::Product.new(
                                      make_product(product_name: 'support', plan_settings: {"insights" => "good_data_and_explore"})
                                    )
                                  ])
        end

        it 'returns Explore' do
          assert suite.available.map(&:key).include?(:explore)
        end
      end

      describe 'when explore product insight plan setting is explore' do
        before do
          client_products.returns([
                                    Zendesk::Accounts::Product.new(
                                      make_product(product_name: 'support', plan_settings: {"insights" => "good_data_and_explore"})
                                    )
                                  ])
        end
        it 'returns Explore' do
          assert suite.available.map(&:key).include?(:explore)
        end
      end
    end

    describe 'with explore_product_tray_insights disabled' do
      before { Arturo.disable_feature!(:explore_product_tray_insights) }

      describe 'with the Explore feature enabled' do
        before { Arturo.enable_feature!(:bime_integration) }

        describe 'for an entitled user' do
          before do
            account.user_seats.explore.for_user(agent).create
          end
          it 'returns Explore' do
            assert suite.available.map(&:key).include?(:explore)
          end
        end

        describe 'for an non-entitled user' do
          it 'does not return Explore' do
            refute suite.available.map(&:key).include?(:explore)
          end

          describe 'with the bime_integration user tag' do
            before do
              agent.stubs('tags').returns(['zdbetafeature_bime_integration'])
            end
            it 'returns Explore' do
              assert suite.available.map(&:key).include?(:explore)
            end
          end
        end
      end

      describe 'with the Explore feature disabled' do
        before { Arturo.disable_feature!(:bime_integration) }
        it 'does not return Explore' do
          refute suite.available.map(&:key).include?(:explore)
        end
      end
    end

    describe 'with Explore' do
      before do
        Arturo.enable_feature!(:bime_integration)
        account.user_seats.explore.for_user(agent).create
      end

      describe 'with explore_ga disabled' do
        before { Arturo.disable_feature!(:explore_ga) }
        it 'returns Explore with label equals beta' do
          assert suite.available.map(&:key).include?(:explore)
          assert suite.available.find { |product| product.key == :explore }.label == 'beta'
        end
      end

      describe 'with explore_ga enabled' do
        before { Arturo.enable_feature!(:explore_ga) }
        it 'returns Explore without label property' do
          assert suite.available.map(&:key).include?(:explore)
          assert suite.available.find { |product| product.key == :explore }.label.nil?
        end
      end
    end
  end

  describe :Outbound do
    let(:pravda_support_product) do
      FactoryBot.build(:support_subscribed_product, plan_settings: { plan_type: ZBC::Zendesk::PlanType::Enterprise.plan_type })
    end

    let(:pravda_outbound_product) { FactoryBot.build(:outbound_trial_product) }
    let(:products) { [pravda_support_product, pravda_outbound_product] }
    let(:role) { admin }
    let(:suite) { Api::V2::Products::Suite.new(account, role) }

    before do
      response_body = { products: products }.to_json
      stub_products_response(response_body, account)
    end

    describe 'with an Admin' do
      describe 'with Outbound not in Pravda' do
        let(:products) { [pravda_support_product] }

        describe 'with Outbound product tray enabled' do
          before { Arturo.enable_feature!(:outbound_product_tray) }
          it 'returns Outbound' do
            assert suite.available.map(&:key).include?(:outbound)
          end
        end

        describe 'with Outbound product tray disabled' do
          before { Arturo.disable_feature!(:outbound_product_tray) }
          it 'does not return Outbound' do
            refute suite.available.select { |product| product.key == :outbound }.any?
          end
        end
      end

      describe 'with Outbound in Pravda' do
        it 'returns Outbound' do
          assert suite.available.find { |product| product.key == :outbound }
        end
      end
    end

    describe 'with a Contributor role' do
      let(:account) { accounts(:multiproduct) }

      describe 'with Outbound in Pravda' do
        it 'returns Outbound' do
          assert suite.available.find { |product| product.key == :outbound }
        end
      end

      describe 'with Outbound not in Pravda' do
        let(:products) { [pravda_support_product] }

        it 'does not return Outbound' do
          refute suite.available.select { |product| product.key == :outbound }.any?
        end
      end
    end

    describe 'without an Admin' do
      let(:role) { agent }

      it 'does not return Outbound' do
        refute suite.available.map(&:key).include?(:outbound)
      end
    end

    describe 'without Admin or Contributor role' do
      let(:role) { agent }

      it 'does not return Outbound' do
        refute suite.available.select { |product| product.key == :outbound }.any?
      end
    end
  end

  describe :CentralAdmin do
    let(:account) { accounts(:multiproduct) }
    let(:admin) { users(:multiproduct_support_admin) }
    let(:suite) { Api::V2::Products::Suite.new(account, admin) }

    before do
      response_body = { products: [] }.to_json
      stub_products_response(response_body, account)
    end

    describe 'caching of entitlements status' do
      let(:cache_key) do
        "product_tray/multiproduct/entitlements/#{account.id}/#{admin.id}"
      end
      let(:entitlements) { { entitlements: { chat: 'admin' } } }

      before do
        stub_entitlements_response(entitlements.to_json, account, admin.id)
      end

      it 'increments a datadog metric' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('staff_service_entitlement_api_calls')
        suite.access_to_central_admin?
      end

      it 'retrieves the multiproduct admin value wrapped with Rails cache' do
        Rails.cache.expects(:fetch).with(cache_key, anything).once.returns(entitlements)
        suite.access_to_central_admin?
      end

      it 'does not call staff client more than once' do
        Zendesk::StaffClient.any_instance.expects(:get_entitlements!).once.returns(entitlements)
        2.times { suite.access_to_central_admin? }
      end
    end

    describe 'with non multiproduct account' do
      let(:account) { accounts(:minimum) }
      let(:admin) { users(:minimum_admin) }
      let(:suite) { Api::V2::Products::Suite.new(account, admin) }
      let(:entitlements) { { entitlements: { chat: 'admin' } } }

      it 'returns central_admin' do
        assert suite.available.map(&:key).include?(:central_admin)
      end
    end

    describe 'with multiproduct account' do
      describe 'with admin' do
        let(:entitlements) { { entitlements: { chat: 'admin' } } }

        before do
          stub_entitlements_response(entitlements.to_json, account, admin.id)
        end

        it 'returns central_admin' do
          assert suite.available.map(&:key).include?(:central_admin)
        end
      end

      describe 'with owner' do
        let(:owner) { users(:multiproduct_owner) }
        let(:suite) { Api::V2::Products::Suite.new(account, owner) }
        let(:entitlements) { { entitlements: { chat: nil } } }

        before do
          stub_entitlements_response(entitlements.to_json, account, owner.id)
        end

        it 'returns central_admin' do
          assert suite.available.map(&:key).include?(:central_admin)
        end
      end

      describe 'with agent' do
        let(:agent) { users(:multiproduct_contributor) }
        let(:suite) { Api::V2::Products::Suite.new(account, agent) }
        let(:entitlements) { { entitlements: { chat: 'agent' } } }

        it 'does not return central_admin' do
          refute suite.available.map(&:key).include?(:central_admin)
        end
      end

      describe 'with support:custom_role' do
        let(:agent) { users(:multiproduct_contributor) }
        let(:suite) { Api::V2::Products::Suite.new(account, agent) }

        describe 'with explore admin' do
          let(:entitlements) { { entitlements: { support: 'custom_role', explore: 'admin' } } }

          it 'does not return central_admin' do
            refute suite.available.map(&:key).include?(:central_admin)
          end
        end

        describe 'with guide:admin' do
          let(:entitlements) { { entitlements: { support: 'custom_role', guide: 'admin' } } }

          it 'does not return central_admin' do
            refute suite.available.map(&:key).include?(:central_admin)
          end
        end

        describe 'with talk:admin' do
          let(:entitlements) { { entitlements: { support: 'custom_role', talk: 'admin' } } }

          it 'does not return central_admin' do
            refute suite.available.map(&:key).include?(:central_admin)
          end
        end
      end

      describe 'with support:empty' do
        let(:agent) { users(:multiproduct_contributor) }
        let(:suite) { Api::V2::Products::Suite.new(account, agent) }

        describe 'with explore admin' do
          let(:entitlements) { { entitlements: { explore: 'admin' } } }

          it 'does return central_admin' do
            assert suite.available.map(&:key).include?(:central_admin)
          end
        end

        describe 'with guide:admin' do
          let(:entitlements) { { entitlements: { guide: 'admin' } } }

          it 'does return central_admin' do
            assert suite.available.map(&:key).include?(:central_admin)
          end
        end

        describe 'with talk:admin' do
          let(:entitlements) { { entitlements: { talk: 'admin' } } }

          it 'does return central_admin' do
            assert suite.available.map(&:key).include?(:central_admin)
          end
        end
      end

      describe 'with entitlements:empty' do
        let(:agent) { users(:multiproduct_contributor) }
        let(:suite) { Api::V2::Products::Suite.new(account, agent) }
        let(:entitlements) { { entitlements: {} } }

        it 'does not return central_admin' do
          refute suite.available.map(&:key).include?(:central_admin)
        end
      end
    end
  end

  def stub_products_response(response_body, account)
    stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
      to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
  end

  def stub_entitlements_response(response_body, _account, user_id)
    stub_request(:get, "#{ENV['STAFF_SERVICE_URL']}/api/services/staff/#{user_id}/entitlements").
      to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
  end
end
