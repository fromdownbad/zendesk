require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::EndUserIdentitiesController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :user_identities, :role_settings, :recipient_addresses

  before do
    accept :json
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []

    # Verification mails are only sent to accounts with HC
    accounts(:minimum).enable_help_center!
  end

  with_options(controller: 'api/v2/end_user_identities') do |request|
    request.should_route :post, '/api/v2/end_users/123/identities', action: 'create', end_user_id: '123'
    request.should_route :put,  '/api/v2/end_users/123/identities/456/make_primary', action: 'make_primary', end_user_id: '123', id: '456'
  end

  as_an_anonymous_user do
    should_be_unauthorized *[[:post, :create, {}], [:put, :make_primary, {id: 1}]].map { |a, b, c| [a, b, c.merge(end_user_id: 1)] }
  end

  as_an_end_user do
    describe "a POST to :create" do
      describe "to a different user" do
        before do
          user = users(:minimum_search_user)
          post :create, params: { end_user_id: user.id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "as the same user" do
        before do
          post :create, params: { end_user_id: @user.id, identity: { type: "email", value: "test@test.com" } }
        end

        it('responds with created') { assert_response :created }
        it('sends a verification email') { assert_equal 1, ActionMailer::Base.deliveries.size }
      end

      describe "as the same user with more params" do
        before do
          params = {
            end_user_id: @user.id,
            identity: { type: 'email', value: 'test@test.com', primary: true, verified: true, skip_verify_email: true }
          }
          post :create, params: params
        end

        it('responds with created') {
          assert_response :created
          assert_equal false, JSON.parse(@response.body)['identity']['primary']
          assert_equal false, JSON.parse(@response.body)['identity']['verified']
        }
        it('sends a verification email') { assert_equal 1, ActionMailer::Base.deliveries.size }
      end
    end

    describe "a PUT to :make_primary" do
      describe "to a different user" do
        before do
          user = users(:minimum_search_user)
          identity = user.identities.first

          put :make_primary, params: { end_user_id: user.id, id: identity.id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "as the same user" do
        before do
          identity = @user.identities.first

          put :make_primary, params: { end_user_id: @user.id, id: identity.id }
        end

        it('responds with ok') { assert_response :ok }
      end
    end
  end

  describe "a PUT to :make_primary as an agent" do
    let(:user) { users(:minimum_end_user) }

    as_an_agent_with_permissions(end_user_profile: 'readonly') do
      before do
        put :make_primary, params: { end_user_id: user.id, id: user.identities.second.id }
      end

      it('responds with forbidden') { assert_response :forbidden }
    end

    as_an_agent_with_permissions(end_user_profile: 'full') do
      before do
        put :make_primary, params: { end_user_id: user.id, id: user.identities.second.id }
      end

      it('responds with success') { assert_response :success }
    end
  end

  as_an_agent do
    describe "a POST to create" do
      before do
        user = users(:minimum_end_user)
        post :create, params: { end_user_id: user.id, user_id: user.id, identity: { type: "email", value: "test@test.com" } }
      end

      it ('sends a verification email') { assert_equal 1, ActionMailer::Base.deliveries.size }
    end

    describe "a POST to create with more params" do
      before do
        user = users(:minimum_end_user)
        post :create, params: { end_user_id: user.id, user_id: user.id, identity: { type: "email", value: "test1@test.com", verified: true } }
      end

      it('responds with created') {
        assert_response :created
        assert(JSON.parse(@response.body)['identity']['verified'])
      }

      it('does not send a verification email') { assert_equal 0, ActionMailer::Base.deliveries.size }
    end
  end
end
