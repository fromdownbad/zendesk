require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ProblemsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: "api/v2/problems") do |request|
    request.should_route :get, "/api/v2/problems", action: "index"
    request.should_route :post, "/api/v2/problems/autocomplete", action: "autocomplete"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :autocomplete
  end

  as_an_end_user do
    should_be_forbidden :index, :autocomplete
  end

  as_an_agent do
    describe "a GET to #index" do
      before { get :index }
      should_use_presenter Api::V2::Tickets::TicketPresenter
    end

    describe "a GET to #index -- sorted" do
      before do
        Account.any_instance.stubs(has_extended_ticket_types?: true)

        [tickets(:minimum_1), tickets(:minimum_2)].each do |t|
          # default ordering is by id, so need to reverse the updated_at order
          date = (2 - t.nice_id).hours.ago.to_s(:db)
          t.update_columns(updated_at: date, ticket_type_id: TicketType.PROBLEM)
        end

        get :index
      end

      it "is ordered by updated_at DESC" do
        tickets = JSON.parse(@response.body)["tickets"].map { |ticket| ticket["updated_at"] }
        assert_equal tickets.sort.reverse, tickets
      end
    end

    describe "a POST to :autocomplete" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.account.stubs(has_extended_ticket_types?: true)

        @ticket.ticket_type_id = TicketType.PROBLEM
        @ticket.will_be_saved_by(users(:minimum_admin))
        @ticket.save!

        post :autocomplete, params: { text: "minimum" }
      end

      it "returns ticket" do
        tickets = JSON.parse(@response.body)["tickets"]

        assert_not_nil tickets.detect { |ticket| ticket["id"] == @ticket.nice_id }
      end

      should_use_presenter Api::V2::Tickets::TicketPresenter
    end
  end

  as_an_agent_with_permissions(ticket_access: 'assigned-only') do
    before do
      Account.any_instance.stubs(has_extended_ticket_types?: true)
      accounts(:minimum).tickets.working.each do |t|
        t.ticket_type_id = TicketType.PROBLEM
        t.will_be_saved_by(users(:minimum_admin))
        t.save!
      end
    end

    describe "a GET to #index" do
      before { get :index }

      it "only returns tickets the agent is permitted to see" do
        tickets = JSON.parse(@response.body)["tickets"]
        tickets.each { |t| assert_equal users(:minimum_agent).id, t["assignee_id"] }
      end
    end

    describe "a GET to :autocomplete" do
      before { post :autocomplete, params: { text: "minimum" } }

      it "only returns tickets the agent is permitted to see" do
        tickets = JSON.parse(@response.body)["tickets"]
        tickets.each { |t| assert_equal users(:minimum_agent).id, t["assignee_id"] }
      end
    end
  end
end
