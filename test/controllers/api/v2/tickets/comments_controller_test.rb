require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::Tickets::CommentsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :users, :tickets, :events

  let(:ticket) { tickets(:minimum_1) }
  let(:comment) { ticket.comments.first }

  before do
    accept :json
    use_ssl
  end

  with_options(controller: "api/v2/tickets/comments") do |request|
    request.should_route :put,  "/api/v2/tickets/1/comments/2/redact", action: "redact", ticket_id: 1, id: 2
    request.should_route :put,  "/api/v2/tickets/1/comments/2/make_private", action: 'make_private', ticket_id: 1, id: 2
    request.should_route :post, "/api/v2/tickets/1/comments/2/report", action: 'report', ticket_id: 1, id: 2
  end

  as_an_anonymous_user do
    should_be_unauthorized(
      [:get, :index, ticket_id: 1],
      [:put, :redact, {ticket_id: 1, id: 2}],
      [:put, :make_private, {ticket_id: 1, id: 2}],
      [:get, :full_email_body, ticket_id: 1, id: 1],
      [:post, :report, ticket_id: 1, id: 2]
    )
  end

  as_an_end_user do
    should_be_forbidden(
      [:get, :index, ticket_id: 1],
      [:put, :redact, {ticket_id: 1, id: 2}],
      [:put, :make_private, {ticket_id: 1, id: 2}],
      [:get, :full_email_body, ticket_id: 1, id: 1],
      [:post, :report, ticket_id: 1, id: 2]
    )
  end

  as_a_subsystem_user(account: :minimum, user: "collaboration") do
    should_be_forbidden(
      [:put, :redact, {ticket_id: 1, id: 2}],
      [:put, :make_private, {ticket_id: 1, id: 2}],
      [:get, :full_email_body, ticket_id: 1, id: 1],
      [:post, :report, ticket_id: 1, id: 2]
    )

    describe "a GET to :index" do
      before { get :index, params: { ticket_id: 1 } }
      it("responds with success") { assert_response :success }
    end
  end

  as_an_agent do
    should_be_forbidden [:put, :redact, {ticket_id: 1, id: 2}]

    describe "a GET to :index" do
      describe "basic" do
        before { get :index, params: { ticket_id: ticket.nice_id } }
        should_use_presenter Api::V2::Tickets::CommentPresenter

        it "sets cache headers based on ticket" do
          etag = @response.headers["ETag"]
          last = Time.parse(@response.headers["Last-Modified"])
          assert_equal ticket.updated_at, last
          assert etag.present?
        end
      end

      describe "changing the sort_order" do
        it "breaks cache" do
          get :index, params: { ticket_id: ticket.nice_id }
          etag1 = @response.headers["ETag"]
          get :index, params: { ticket_id: ticket.nice_id, sort_order: "desc" }
          etag2 = @response.headers["ETag"]
          refute (etag1 == etag2)
        end

        it "does not break cache if given nil and asc sort_order" do
          get :index, params: { ticket_id: ticket.nice_id }
          etag1 = @response.headers["ETag"]
          get :index, params: { ticket_id: ticket.nice_id, sort_order: "asc" }
          etag2 = @response.headers["ETag"]
          assert_equal etag1, etag2
        end
      end

      describe "with a sorting preference" do
        before { get :index, params: { ticket_id: ticket.nice_id, sort_order: "desc" } }

        it "reverses the order of the comments" do
          comments = JSON.parse(@response.body)["comments"]
          comment_ids = comments.map { |c| c["id"] }
          assert_equal comment_ids.sort.reverse, comment_ids
        end
      end

      describe "use of relative_attachment_urls" do
        describe "with a lotus request" do
          before { set_header('X-Zendesk-Lotus-Version', 'v1.2.3') }

          it "sets :relative_attachment_urls option" do
            get :index, params: { ticket_id: ticket.nice_id }
            assert @controller.send(:presenter).options[:relative_attachment_urls]
          end
        end

        describe "with a non-lotus request" do
          it "does not set :relative_attachment_urls option" do
            get :index, params: { ticket_id: ticket.nice_id }
            refute @controller.send(:presenter).options[:relative_attachment_urls]
          end
        end
      end

      describe "with a sorting preference" do
        before { get :index, params: { ticket_id: ticket.nice_id, sort_order: "desc" } }

        it "reverses the order of the comments" do
          comments = JSON.parse(@response.body)["comments"]
          comment_ids = comments.map { |c| c["id"] }
          assert_equal comment_ids.sort.reverse, comment_ids
        end
      end

      describe "Ticket is from HelpCenter" do
        [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
          describe "via_id is #{via_id}" do
            before do
              ticket.update_column(:via_id, via_id)
              ticket.update_column(:via_reference_id, ViaType.HELPCENTER)
            end

            describe "agent_as_end_user Arturo is disabled" do
              before do
                Arturo.disable_feature!(:agent_as_end_user)
              end

              it "succeeds" do
                get :index, params: { ticket_id: ticket.nice_id }
                assert_response :ok
              end
            end

            describe "agent_as_end_user Arturo is enabled" do
              before do
                Arturo.enable_feature!(:agent_as_end_user)
              end

              it "fails" do
                get :index, params: { ticket_id: ticket.nice_id }
                assert_response :forbidden
              end
            end
          end
        end
      end

      describe 'with valid external domain' do
        it 'returns a valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
          get :index, params: { ticket_id: ticket.nice_id }
          assert_equal response.headers['Access-Control-Allow-Origin'], 'http://app.futuresimple.com'
        end

        it 'returns a not valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
          get :index, params: { ticket_id: ticket.nice_id }
          assert_nil response.headers['Access-Control-Allow-Origin']
        end
      end
    end

    describe "a PUT to #make_private" do
      describe "with a public comment" do
        before { put :make_private, params: { ticket_id: ticket.nice_id, id: comment.id } }

        it('responds with ok') { assert_response :ok }

        it "makes the comment private" do
          assert_equal false, comment.reload.is_public?
        end
      end

      describe "with a private comment" do
        before do
          comment.ticket.will_be_saved_by(@user)
          comment.update_attributes!(is_public: false)
          put :make_private, params: { ticket_id: ticket.nice_id, id: comment.id }
        end

        it('responds with bad_request') { assert_response :bad_request }
      end
    end

    describe "a GET to :full_email_body" do
      let(:account) { accounts(:minimum) }
      let(:remote_json) do
        {
          processing_state: {
            content: {
              plain_quoted: "plain body json data from external storage (s3)",
              html_quoted: "<html><body><div>html body json data from external storage (s3)</div></body></html>",
            }
          }
        }.to_json
      end

      describe_with_arturo_enabled :email_no_delimiter do
        Zendesk::Tickets::Comments::EmailCommentSupport::SUPPORTED_VIA_TYPES.each do |type|
          describe "Comment is from #{type}" do
            let(:div_class) { type == ViaType.MAIL ? ' zd-comment-pre-styled' : '' }

            before { comment.update_column(:via_id, type) }

            describe "ticket, comment, and raw email json are found" do
              let(:expected_full_comment) do
                "<div class=\"zd-comment#{div_class}\" dir=\"auto\">plain body json data from external storage (s3)</div>"
              end
              let(:rich_content_in_emails) { false }

              before do
                Zendesk::Mailer::RawEmailAccess.any_instance.stubs(full_message_body_json: remote_json)
                account.settings.rich_content_in_emails = rich_content_in_emails
                account.save!

                Prop.expects(:throttle!).at_least_once

                get :full_email_body, params: { ticket_id: ticket.nice_id, id: comment.id }
              end

              should_use_presenter Api::V2::Tickets::EmailCommentPresenter

              it "sets cache headers based on comment event" do
                etag = @response.headers["ETag"]
                last = Time.parse(@response.headers["Last-Modified"])
                assert_equal Time.parse(comment.updated_at.to_s), last
                assert etag.present?
              end

              it "retrieves full email body" do
                assert_equal expected_full_comment, JSON.parse(@response.body)["comment"]["full_email_body"]
              end

              it "caches full email body" do
                comment.audit.expects(:raw_email).never

                get :full_email_body, params: { ticket_id: ticket.nice_id, id: comment.id }

                assert_equal expected_full_comment, JSON.parse(@response.body)["comment"]["full_email_body"]
              end

              describe "with rich content in emails enabled" do
                let(:expected_full_comment) do
                  "<div class=\"zd-comment#{div_class}\" dir=\"auto\"><div>html body json data from external storage (s3)</div></div>"
                end
                let(:rich_content_in_emails) { true }

                it "retrieves full email body" do
                  assert_equal expected_full_comment, JSON.parse(@response.body)["comment"]["full_email_body"]
                end
              end
            end

            describe "returns 404 (Not Found) when" do
              it "comment is not found" do
                get :full_email_body, params: { ticket_id: ticket.nice_id, id: 99_999 }
                assert_response :not_found
              end

              it "ticket is not found" do
                get :full_email_body, params: { ticket_id: 99_999, id: comment.id }
                assert_response :not_found
              end

              it "raw email json is not found" do
                Zendesk::Mailer::RawEmailAccess.any_instance.stubs(json: nil)

                get :full_email_body, params: { ticket_id: ticket.nice_id, id: comment.id }

                assert_response :not_found
              end

              it "is not an email comment" do
                ticket.update_column(:via_id, ViaType.WEB_SERVICE)

                get :full_email_body, params: { ticket_id: ticket.nice_id, id: comment.id }

                assert_response :not_found
              end
            end
          end
        end

        describe "Comment is from a channel that does not support raw email retrieval" do
          before do
            comment.update_column(:via_id, ViaType.WEB_SERVICE)

            get :full_email_body, params: { ticket_id: ticket.nice_id, id: comment.id }
          end

          it { assert_response :not_found }
        end
      end

      describe_with_arturo_disabled :email_no_delimiter do
        describe "Comment is from Mail" do
          before do
            comment.update_column(:via_id, ViaType.MAIL)
            Zendesk::Mailer::RawEmailAccess.any_instance.stubs(json: remote_json)

            get :full_email_body, params: { ticket_id: ticket.nice_id, id: comment.id }
          end

          it { assert_response :not_found }
        end
      end
    end

    describe "POST to :report" do
      let(:account) { accounts(:minimum) }
      let(:params) do
        {
          id: comment.id,
          ticket_id: ticket.nice_id,
          message_id: "<message_id>",
          description: "description",
          json_email_identifier: "email_identifier",
          raw_email_identifier: "raw_email_identifier"
        }
      end
      let(:response_body) do
        {
          "request" => {
            "id" => 35436,
            "subject" => "My printer is on fire!"
          }
        }
      end
      let(:kragle_client) { stub }

      describe_with_arturo_enabled :email_no_delimiter do
        describe "when ticket id is invalid" do
          let(:params) { {id: comment.id, ticket_id: "-1" } }

          it "returns :bad_request" do
            post :report, params: params
            assert_response :bad_request
          end
        end

        describe "when comment id is invalid" do
          let(:params) { {id: "-1", ticket_id: ticket.nice_id } }

          it "returns :bad_request" do
            post :report, params: params
            assert_response :bad_request
          end
        end

        describe 'when a z1 ticket already exists' do
          before { @controller.stubs(:should_report_mail_rendering_issue?).returns(false) }

          it 'returns 409 conflict' do
            post :report, params: params
            assert_response :conflict
          end
        end

        describe "when reporting issue creates z1 ticket sucessfully" do
          let(:success_response) { stub(success?: true, body: response_body) }
          let(:kragle_response) { success_response }

          before do
            @controller.stubs(:internal_api_client).returns(kragle_client)
            kragle_client.expects(:post).with('api/v2/requests', anything).returns(kragle_response).once
            kragle_client.expects(:put).with("api/v2/tickets/#{response_body['request']['id']}", anything).returns(kragle_response).once
          end

          describe "uses the correct presenter" do
            before { post :report, params: params }

            should_use_presenter Api::V2::Tickets::EmailCommentIssuePresenter, return_value: { ticket: { ticket_id: 35436, z1_request_url: "https://support.#{ENV.fetch('ZENDESK_HOST')}" } }
          end
        end

        describe "when reporting issue fails to create ticket successfully" do
          let(:failure_response) { stub(success?: false) }
          let(:kragle_response) { failure_response }

          before do
            @controller.stubs(:internal_api_client).returns(kragle_client)
            kragle_client.expects(:post).with('api/v2/requests', anything).returns(kragle_response).once
            post :report, params: params
          end

          it "returns 422 unprocessable entity" do
            assert_response :unprocessable_entity
          end
        end
      end

      describe_with_arturo_disabled :email_no_delimiter do
        it "responds with unauthorized" do
          post :report, params: params
          assert_response :unauthorized
        end
      end
    end
  end

  as_an_admin do
    describe "a PUT to :redact" do
      let(:params) { {ticket_id: ticket.nice_id, id: comment.id, text: "ticket"} }
      let(:domain_event_publisher) { FakeEscKafkaMessage.new }

      describe "simple" do
        before { put :redact, params: params }
        should_use_presenter Api::V2::Tickets::CommentPresenter
      end

      describe_with_arturo_enabled :redact_archived_and_closed_comments do
        describe "for archived tickets" do
          let(:ticket_to_be_archived) { tickets(:minimum_1) }
          let(:comment_to_be_archived) { ticket_to_be_archived.comments.first }
          let(:stub_record) { TicketArchiveStub.find_by_id(ticket_to_be_archived.id) }
          let(:archived_ticket) { ZendeskArchive.router.get(stub_record).data }
          let(:targeted_comment) { archived_ticket['events'].find { |event| event["type"] == "Comment" && event['id'] == comment_to_be_archived.id } }
          let(:audit_count_before_archived) { ticket_to_be_archived.audits.count }

          before do
            comment_to_be_archived
            audit_count_before_archived
            archive_and_delete(ticket_to_be_archived)
          end

          describe "with string redaction" do
            let(:params) { { ticket_id: ticket_to_be_archived.nice_id, id: comment_to_be_archived.id, text: "ticket" } }
            before do
              put :redact, params: params
            end

            it "redacts specific text from archived ticket descriptions" do
              assert_equal "minimum 1 ▇▇▇▇▇▇", archived_ticket['description']
            end

            it 'redacts specific text from archived ticket comment values' do
              assert_equal "minimum <span>▇▇▇▇▇▇ 1</span>", targeted_comment['value']
            end

            it 'does not create an audit for archived comment redactions' do
              audit_count = archived_ticket['events'].select { |event| event["type"] == "Audit" }.count
              assert_equal audit_count, audit_count_before_archived
            end
          end

          describe "with full redaction" do
            let(:params) { { ticket_id: ticket_to_be_archived.nice_id, id: comment_to_be_archived.id, text: nil, redact_full_comment: true} }

            it "redacts archived ticket descriptions" do
              local_archived_ticket = ZendeskArchive.router.get(stub_record).data
              local_comment = local_archived_ticket['events'].find { |event| event["type"] == "Comment" && event['id'] == comment_to_be_archived.id }
              local_archived_ticket['description'] = local_comment['value']
              ZendeskArchive.router.set(stub_record, local_archived_ticket)

              put :redact, params: params
              archived_ticket = ZendeskArchive.router.get(stub_record).data
              assert_response :success
              assert_equal "▇▇▇▇▇", archived_ticket['description']
            end

            it "does not redact archived ticket descriptions if ticket description does not match comment value" do
              put :redact, params: params
              refute_equal "▇▇▇▇▇", archived_ticket['description']
            end

            it 'redacts specific text from archived ticket comment values' do
              put :redact, params: params
              assert_equal "▇▇▇▇▇", targeted_comment['value']
            end

            it 'does not create an audit for archived comment redactions' do
              put :redact, params: params
              audit_count = archived_ticket['events'].select { |event| event["type"] == "Audit" }.count
              assert_equal audit_count, audit_count_before_archived
            end

            it "does not full redact if only text is nil" do
              params = { ticket_id: ticket_to_be_archived.nice_id, id: comment_to_be_archived.id, text: nil, redact_full_comment: false }

              assert_raises(ArgumentError) { put :redact, params: params }

              local_archived_ticket = ZendeskArchive.router.get(stub_record).data
              local_comment = local_archived_ticket['events'].find { |event| event["type"] == "Comment" && event['id'] == comment_to_be_archived.id }
              refute_equal "▇▇▇▇▇", local_comment['value']
            end

            it "does not full redact if only redact_full_comment is true" do
              params = { ticket_id: ticket_to_be_archived.nice_id, id: comment_to_be_archived.id, text: "minimum", redact_full_comment: true }
              put :redact, params: params

              local_archived_ticket = ZendeskArchive.router.get(stub_record).data
              refute_equal "▇▇▇▇▇", local_archived_ticket['description']
            end
          end
        end

        describe "for closed tickets" do
          let(:ticket_to_be_archived) { tickets(:minimum_1) }
          let(:closed_ticket) { Ticket.all.find { |ticket|  ticket.status_id == StatusType.CLOSED && ticket.id != ticket_to_be_archived.id && ticket.comments && ticket.comments.count > 0 } }
          let(:closed_ticket_comment) { closed_ticket.comments.first }
          let(:closed_audit_count_before_archived) { closed_ticket.audits.count }
          let(:params) { { ticket_id: closed_ticket.nice_id, id: closed_ticket_comment.id, text: "ticket" } }

          describe "with string redaction" do
            it "redacts specific text from closed ticket descriptions" do
              @closed_ticket_updated_at = closed_ticket.updated_at
              put :redact, params: params

              closed_ticket.reload
              assert_includes closed_ticket.description, "▇▇▇▇▇▇"
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end

            it 'redacts specific text from closed ticket comment values' do
              @closed_ticket_updated_at = closed_ticket.updated_at
              put :redact, params: params

              closed_ticket.reload
              closed_ticket_comment.reload
              assert_includes closed_ticket_comment.value, "▇▇▇▇▇▇"
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end

            it 'does not create an audit for closed comment redactions' do
              @closed_ticket_updated_at = closed_ticket.updated_at
              put :redact, params: params

              closed_ticket.reload
              audit_count = closed_ticket.events.select { |event| event["type"] == "Audit" }.count
              assert_equal audit_count, closed_audit_count_before_archived
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end
          end

          describe "with full redaction on closed ticket" do
            let(:params) { { ticket_id: closed_ticket.nice_id, id: closed_ticket_comment.id, text: nil, redact_full_comment: true } }

            it "redacts text from closed ticket descriptions" do
              @closed_ticket_updated_at = closed_ticket.updated_at
              closed_ticket_comment.update_column(:value, closed_ticket.description)
              put :redact, params: params

              closed_ticket.reload
              assert_equal closed_ticket.description, "▇▇▇▇▇"
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end

            it "does not redact text from closed ticket descriptions if the description does not match comment value" do
              @closed_ticket_updated_at = closed_ticket.updated_at
              put :redact, params: params

              closed_ticket.reload
              refute_equal closed_ticket.description, "▇▇▇▇▇"
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end

            it 'redacts full text from closed ticket comment values' do
              @closed_ticket_updated_at = closed_ticket.updated_at
              put :redact, params: params

              closed_ticket.reload
              closed_ticket_comment.reload
              assert_equal closed_ticket_comment.value, "▇▇▇▇▇"
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end

            it 'does not create an audit for closed comment redactions' do
              @closed_ticket_updated_at = closed_ticket.updated_at
              put :redact, params: params

              closed_ticket.reload
              audit_count = closed_ticket.events.select { |event| event["type"] == "Audit" }.count
              assert_equal audit_count, closed_audit_count_before_archived
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end

            it "does not full redact if only text is nil" do
              params = { ticket_id: ticket.nice_id, id: comment.id, text: nil, redact_full_comment: false }

              assert_raises(ArgumentError) { put :redact, params: params }
              refute_equal "▇▇▇▇▇", comment.reload.body
            end

            it "does not full redact if only redact_full_comment is true" do
              params = { ticket_id: ticket.nice_id, id: comment.id, text: "minimum", redact_full_comment: true }
              put :redact, params: params

              refute_equal "▇▇▇▇▇", ticket.reload.description
            end
          end
        end
      end

      describe_with_arturo_disabled :redact_archived_and_closed_comments do
        let(:ticket_to_be_archived) { tickets(:minimum_1) }

        describe "for archived tickets" do
          let(:comment_to_be_archived) { ticket_to_be_archived.comments.first }
          let(:stub_record) { TicketArchiveStub.find_by_id(ticket_to_be_archived.id) }
          let(:archived_ticket) { ZendeskArchive.router.get(stub_record).data }
          let(:targeted_comment) { archived_ticket['events'].find { |event| event["type"] == "Comment" && event['id'] == comment_to_be_archived.id } }
          let(:audit_count_before_archived) { ticket_to_be_archived.audits.count }
          let(:archived_ticket_description) { Ticket.where(id: ticket_to_be_archived.id).first.description }
          let(:archived_ticket_comment_value) { Ticket.where(id: ticket_to_be_archived.id).first.comments.first.value }

          before do
            _ = comment_to_be_archived
            _ = audit_count_before_archived
            @archived_ticket_description = archived_ticket_description
            @archived_ticket_comment_value = archived_ticket_comment_value
            archive_and_delete(ticket_to_be_archived)
          end

          describe "with string redaction on archived ticket" do
            let(:params) { { ticket_id: ticket_to_be_archived.nice_id, id: comment_to_be_archived.id, text: "ticket" } }

            before do
              put :redact, params: params
            end

            it "does not redact specific text from archived ticket descriptions" do
              assert_response 400
              assert_equal @archived_ticket_description, archived_ticket['description']
            end

            it 'does not redact specific text from archived ticket comment values' do
              assert_response 400
              assert_equal @archived_ticket_comment_value, targeted_comment['value']
            end

            it 'does not create an audit for archived comment redactions' do
              audit_count = archived_ticket['events'].select { |event| event["type"] == "Audit" }.count

              assert_response 400
              assert_equal audit_count, audit_count_before_archived
            end
          end

          describe "with full redaction on archived ticket" do
            let(:params) { {ticket_id: ticket_to_be_archived.nice_id, id: comment_to_be_archived.id, text: nil, redact_full_comment: true} }
            before do
              @archived_ticket_description = archived_ticket_description
              @archived_ticket_comment_value = archived_ticket_comment_value
              put :redact, params: params
            end

            it "does not redact specific text from archived ticket descriptions" do
              assert_response 400
              assert_equal @archived_ticket_description, archived_ticket['description']
            end

            it 'does not redact specific text from archived ticket comment values' do
              assert_response 400
              assert_equal @archived_ticket_comment_value, targeted_comment['value']
            end

            it 'does not create an audit for archived comment redactions' do
              audit_count = archived_ticket['events'].select { |event| event["type"] == "Audit" }.count
              assert_response 400
              assert_equal audit_count, audit_count_before_archived
            end
          end
        end

        describe "for closed tickets" do
          let(:closed_ticket) { Ticket.all.find { |ticket| ticket.status_id == StatusType.CLOSED && ticket.id != ticket_to_be_archived.id && ticket.comments && ticket.comments.count > 0 } }
          let(:closed_ticket_comment) { closed_ticket.comments.first }
          let(:closed_audit_count_before_archived) { closed_ticket.audits.count }

          before do
            @closed_ticket_updated_at = closed_ticket.updated_at
          end

          describe "with string redaction on closed ticket" do
            let(:params) { { ticket_id: closed_ticket.nice_id, id: closed_ticket_comment.id, text: "ticket" } }

            before do
              @closed_ticket_description = closed_ticket.description
              @closed_ticket_comment_value = closed_ticket.comments.first.value
              put :redact, params: params
            end

            it "does not redact specific text from closed ticket descriptions" do
              closed_ticket.reload

              assert_response 422
              assert_equal @closed_ticket_description, closed_ticket.description
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end

            it 'does not redact specific text from closed ticket comment values' do
              closed_ticket.reload
              closed_ticket_comment.reload

              assert_response 422
              assert_equal @closed_ticket_comment_value, closed_ticket.comments.first.value
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end

            it 'does not create an audit for closed comment redactions' do
              closed_ticket.reload
              audit_count = closed_ticket.events.select { |event| event["type"] == "Audit" }.count

              assert_response 422
              assert_equal audit_count, closed_audit_count_before_archived
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end
          end

          describe "with full redaction on closed ticket" do
            let(:params) { { ticket_id: closed_ticket.nice_id, id: closed_ticket_comment.id, text: nil, redact_full_comment: true } }

            before do
              @closed_ticket_description = closed_ticket.description
              @closed_ticket_comment_value = closed_ticket.comments.first.value
              put :redact, params: params
            end

            it "does not redact text from closed ticket descriptions" do
              closed_ticket.reload

              assert_response 400
              assert_equal @closed_ticket_description, closed_ticket.description
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end

            it 'does not redact full redact closed ticket comment values' do
              closed_ticket.reload
              closed_ticket_comment.reload

              assert_response 400
              assert_equal @closed_ticket_comment_value, closed_ticket.comments.first.value
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end

            it 'does not create an audit for closed comment redactions' do
              closed_ticket.reload
              audit_count = closed_ticket.events.select { |event| event["type"] == "Audit" }.count
              assert_response 400
              assert_equal audit_count, closed_audit_count_before_archived
              assert_equal @closed_ticket_updated_at, closed_ticket.updated_at
            end
          end
        end
      end

      describe "for open tickets" do
        describe "with string redaction" do
          it "updates the comments text" do
            assert_equal "minimum ticket 1", comment.body
            put :redact, params: params
            assert_response :success
            assert_equal "minimum ▇▇▇▇▇▇ 1", comment.reload.body
          end

          it "redacts from ticket description" do
            put :redact, params: params.merge(text: "ticket")
            assert_response :success
            assert_equal "minimum 1 ▇▇▇▇▇▇", ticket.reload.description
          end
        end

        describe_with_arturo_enabled :redact_archived_and_closed_comments do
          describe "with full redaction" do
            let(:params) { { ticket_id: ticket.nice_id, id: comment.id, text: nil, redact_full_comment: true } }

            it "updates the comments text" do
              assert_equal "minimum ticket 1", comment.body
              put :redact, params: params

              assert_response :success
              assert_equal "▇▇▇▇▇", comment.reload.body
            end

            it "redacts from ticket description" do
              comment.update_column(:value, ticket.description)
              put :redact, params: params

              assert_response :success
              assert_equal "▇▇▇▇▇", ticket.reload.description
            end

            it "does not redact ticket description if description does not match comment value" do
              put :redact, params: params

              assert_response :success
              refute_equal "▇▇▇▇▇", ticket.reload.description
            end

            it "does not full redact if only text is nil" do
              params = { ticket_id: ticket.nice_id, id: comment.id, text: nil, redact_full_comment: false }

              assert_raises(ArgumentError) { put :redact, params: params }
              refute_equal "▇▇▇▇▇", comment.reload.body
            end

            it "does not full redact if only redact_full_comment is true" do
              params = { ticket_id: ticket.nice_id, id: comment.id, text: "minimum", redact_full_comment: true }
              put :redact, params: params

              refute_equal "▇▇▇▇▇", ticket.reload.description
            end
          end
        end

        describe_with_arturo_disabled :redact_archived_and_closed_comments do
          describe "with full redaction" do
            let(:params) { { ticket_id: ticket.nice_id, id: comment.id, text: nil, redact_full_comment: true } }

            it "does not scrub the comments text" do
              assert_equal "minimum ticket 1", comment.body
              put :redact, params: params

              assert_response 400
              refute_equal "▇▇▇▇▇", comment.reload.body
            end

            it "does not redact ticket description" do
              comment.update_column(:value, ticket.description)
              put :redact, params: params

              assert_response 400
              refute_equal "▇▇▇▇▇", ticket.reload.description
            end

            it "does not redact ticket description if description does not match comment value" do
              put :redact, params: params

              assert_response 400
              refute_equal "▇▇▇▇▇", ticket.reload.description
            end
          end
        end

        it "updates a public comment without making it private" do
          User.any_instance.stubs(is_light_agent?: true)
          assert comment.reload.is_public?
          assert comment.author.is_light_agent?

          put :redact, params: params
          assert comment.reload.is_public?
        end

        it "adds a redaction audit" do
          put :redact, params: params
          audit = ticket.audits.last
          event = audit.events.detect { |e| e.class == CommentRedactionEvent }
          assert_equal comment.id, event.comment_id.to_i
        end

        describe_with_arturo_enabled(:publish_ticket_events_to_bus) do
          it "publishes a domain event" do
            Ticket.any_instance.stubs(:domain_event_publisher).returns(domain_event_publisher)
            put :redact, params: params.merge(text: "ticket")

            refute_empty domain_event_publisher.events
            events = domain_event_publisher.events.map do |event|
              ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(event[:value]).event
            end
            assert_includes events, :comment_redacted
            assert_includes events, :description_changed
          end
        end

        it "fails with unfound text" do
          put :redact, params: params.merge(text: "xxxx")
          assert_response :bad_request
          assert_equal "minimum ticket 1", ticket.comments.first.reload.body
        end
      end
    end
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    should_be_authorized { :index }
    should_be_forbidden [:put, :redact, {ticket_id: 1, id: 2}], [:put, :make_private, {ticket_id: 1, id: 2}]

    describe "a GET to #index" do
      describe "with a valid ticket" do
        before { get :index, params: { ticket_id: ticket.nice_id } }
        it('responds with success') { assert_response :success }
      end
    end
  end
end
