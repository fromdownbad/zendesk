require_relative "../../../../support/test_helper"
require_relative "../../../../support/attachment_test_helper"

SingleCov.covered!

describe Api::V2::Tickets::AttachmentsController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :tickets, :attachments, :events

  before do
    accept :json
    use_ssl
  end

  with_options(controller: "api/v2/tickets/attachments") do |request|
    request.should_route :put, "/api/v2/tickets/1/comments/2/attachments/3/redact", action: "redact", ticket_id: 1, comment_id: 2, id: 3
  end

  as_an_anonymous_user do
    should_be_unauthorized [:put, :redact, {ticket_id: 1, comment_id: 2, id: 3}]
  end

  as_an_agent do
    should_be_forbidden [:put, :redact, {ticket_id: 1, comment_id: 2, id: 3}]
  end

  as_an_admin do
    describe "a PUT to :redact" do
      let(:ticket) { tickets(:minimum_1) }
      let(:comment) { ticket.comments.first }
      let(:attachment) { comment.attachments.last }
      let(:params) do
        {
        ticket_id: ticket.nice_id,
        comment_id: comment.id,
        id: attachment.id
      }
      end

      before do
        stub_request(:delete, %r{\.amazonaws\.com/data/attachments/.*\.jpg})
        attachment = create_attachment("#{Rails.root}/test/files/normal_1.jpg", ticket.requester)
        comment.attachments << attachment
      end

      describe "simple" do
        before do
          put :redact, params: params
          comment.reload
        end
        should_use_presenter Api::V2::AttachmentPresenter

        it "adds a redacted attachment" do
          assert_includes comment.attachments.map(&:filename), Attachment::REDACTED_FILENAME
        end

        it "adds an attachment redaction audit" do
          audit = ticket.audits.last
          event = audit.events.detect { |e| e.class == AttachmentRedactionEvent }

          assert event.attachment_id
          refute_equal attachment.id, event.attachment_id
          assert_equal attachment.source_id, event.comment_id
        end

        it "deletes the original attachment" do
          refute_includes comment.attachments.map(&:filename), "normal_1.jpg"
        end
      end

      describe_with_arturo_enabled(:publish_ticket_events_to_bus) do
        it 'publishes an AttachmentRedactedFromComment domain event' do
          domain_event_publisher = FakeEscKafkaMessage.new
          Ticket.any_instance.stubs(domain_event_publisher: domain_event_publisher)

          put :redact, params: params

          events = domain_event_publisher.events.map do |event|
            proto = event.fetch(:value)
            ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(proto)
          end

          attachment_events = events.select { |event| event.event == :attachment_redacted_from_comment }

          assert_equal 1, attachment_events.size
          assert_equal attachment.id, attachment_events.first.attachment_redacted_from_comment.attachment.id.value
        end
      end

      describe "a PUT to :redact on an archived ticket" do
        before do
          Attachment.any_instance.stubs(:source).returns(nil)
          put :redact, params: params
        end

        it "returns bad_request" do
          assert_response :bad_request
        end
      end

      describe "a PUT to :redact on an inoperable ticket" do
        before do
          Ticket.any_instance.stubs(:inoperable?).returns(true)
          put :redact, params: params
        end

        it "returns bad_request" do
          assert_response :bad_request
        end
      end
    end
  end
end
