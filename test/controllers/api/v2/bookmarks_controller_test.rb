require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::BookmarksController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :tickets

  before do
    @account  = accounts(:minimum)
    @ticket   = @account.tickets.working.last
    @agent    = users(:minimum_agent)
    @bookmark = Ticket::Bookmark.create!(ticket: @ticket, user: @agent)

    accept :json
  end

  with_options(controller: 'api/v2/bookmarks') do |request|
    request.should_route :get,    '/api/v2/bookmarks',     action: 'index'
    request.should_route :post,   '/api/v2/bookmarks',     action: 'create'
    request.should_route :delete, '/api/v2/bookmarks/123', action: 'destroy', id: '123'
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :create, :destroy
  end

  as_an_end_user do
    should_be_forbidden :index, :create, :destroy
  end

  as_an_agent do
    describe "a GET to :index" do
      describe "with no parameters" do
        before { get :index }
        should_use_presenter Api::V2::BookmarkPresenter, status: :ok
      end

      describe "with side-loading" do
        describe "users" do
          before { get :index, params: { include: :users } }
          it "has users in response" do
            assert_not_nil JSON.parse(@response.body)['users']
          end
        end
      end
    end

    describe "a POST to :create" do
      describe "when the ticket is already bookmarked" do
        before { post :create, params: { bookmark: { ticket_id: @ticket.nice_id } } }
        it('responds with ok') { assert_response :ok }
      end

      describe "when the ticket is not bookmarked" do
        before do
          @bookmark.destroy
          post :create, params: { bookmark: { ticket_id: @ticket.nice_id } }
        end

        should_use_presenter Api::V2::BookmarkPresenter, status: :created
      end

      describe "when the ticket does not exist" do
        before { post :create, params: { bookmark: { ticket_id: 10000000 } } }
        it('responds with not_found') { assert_response :not_found }
      end

      describe "when the agent don't have access to a ticket" do
        it "does not create bookmarks for the ticket" do
          @agent.stubs(:can?).with(:view, @ticket).returns(false)
          post :create, params: { bookmark: { ticket_id: @ticket.nice_id } }
          assert_response :forbidden
        end

        it "does not return bookmarks related to the ticket" do
          User.any_instance.stubs(:can?).returns(false)
          get :index
          assert_includes response.body, '"count":0'
          assert_response :ok
        end
      end
    end

    describe "a ticket that is archived" do
      before do
        @archived_ticket = @bookmark.ticket
        archive_and_delete(@archived_ticket)
        delete :destroy, params: { id: @bookmark.id }
      end

      it('responds with no_content') { assert_response :no_content }
      should_change("the number of bookmarks", by: -1) { Ticket::Bookmark.count(:all) }
    end

    describe "a DELETE to :destroy" do
      before { delete :destroy, params: { id: @bookmark.id } }
      it('responds with no_content') { assert_response :no_content }
      should_change("the number of bookmarks", by: -1) { Ticket::Bookmark.count(:all) }
    end
  end
end
