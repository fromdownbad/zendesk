require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 7

describe Api::V2::OnboardingTasksController do
  extend Api::Presentation::TestHelper
  extend Api::V2::TestHelper

  fixtures :accounts, :users

  before do
    User.any_instance.stubs(:invited_team?).returns(false)

    accept :json
  end

  with_options(controller: "api/v2/onboarding_tasks") do |request|
    request.should_route :get,  "/api/v2/onboarding_tasks", action: "index"
    request.should_route :post, "/api/v2/onboarding_tasks", action: "create"
    request.should_route :get, "/api/v2/onboarding_tasks/find_sample_ticket_id", action: "find_sample_ticket_id"
  end

  as_an_admin do
    before do
      Subscription.any_instance.stubs(:is_trial?).returns(true)
    end

    describe "a GET to :index" do
      describe "with no params" do
        before do
          get :index
        end

        it "returns bad request" do
          assert_response :bad_request
        end
      end

      describe "with type support" do
        let(:type_support) do
          {
            type: "support"
          }
        end

        before do
          stub_request(:get, "https://minimum.zendesk-test.com/embeddable/status.json")
          get :index, params: type_support
        end

        it "returns a list with 4 tasks" do
          assert_response :ok
          assert_equal 4, JSON.parse(@response.body)['tasks'].length
        end

        it "returns a list of tasks with changed, name, and completed attributes" do
          expected_keys = ['changed', 'completed', 'name']
          assert_equal expected_keys, JSON.parse(@response.body)['tasks'].first.keys.sort
        end
      end

      describe "with type suite" do
        let(:type_suite) do
          {
              type: "suite"
          }
        end

        before do
          stub_request(:get, "https://minimum.zendesk-test.com/embeddable/status.json")
          get :index, params: type_suite
        end

        it "returns a list with 9 tasks" do
          assert_response :ok
          assert_equal 9, JSON.parse(@response.body)['tasks'].length
        end
      end

      describe "with invalid type" do
        let(:type_invalid) do
          {
            type: "invalid"
          }
        end

        before do
          get :index, params: type_invalid
        end

        it "returns 404 indicating no onboarding tasks were found" do
          assert_response :not_found
        end
      end
    end

    describe "a POST to :create" do
      describe "returns persisted records" do
        let(:type_support) do
          {
            type: "support"
          }
        end

        let(:valid_params) do
          {
            name: "setup_support_email",
            completed: true,
            type: type_support[:type]
          }
        end

        before do
          stub_request(:get, "https://minimum.zendesk-test.com/embeddable/status.json")
          post :create, params: valid_params
        end

        it "returns saved object" do
          assert_response :created
          body = JSON.parse(@response.body)
          assert_equal body['zero_state'], valid_params[:name]
          assert_equal body['status'], OnboardingTask::CompletionStatus::COMPLETED
          assert_equal body['account_id'], @user.account_id
          assert_equal body['user_id'], @user.id
        end

        it "contains 1 completed task" do
          get :index, params: type_support
          assert_equal 1, JSON.parse(@response.body)['tasks'].select { |task| task['completed'] }.count
        end
      end

      describe "with bad params" do
        let(:invalid_params_without_completion_status) do
          {
            name: "invite_team"
          }
        end

        let(:invalid_params_with_wrong_name) do
          {
            name: "bogus",
            completed: true
          }
        end

        it "returns 422 indicating bad task name" do
          post :create, params: invalid_params_with_wrong_name
          assert_response :unprocessable_entity
          body = JSON.parse(@response.body)
          assert_match /#{invalid_params_with_wrong_name[:name]}.*is not currently supported/, body['error']
        end

        it "returns 422 indicating missing completion status" do
          post :create, params: invalid_params_without_completion_status
          assert_response :unprocessable_entity
          body = JSON.parse(@response.body)
          assert_match /completed/, body['error']
        end
      end
    end

    describe "a GET to :find_sample_ticket_id" do
      it "returns one sample ticket id" do
        get :find_sample_ticket_id
        assert_response :ok
        assert_equal ['sample_ticket_id'], JSON.parse(@response.body).keys
      end

      describe "with type" do
        before do
          get :find_sample_ticket_id, params: { ticket_type: "suite_trial" }
          @ticket_template = Ticket.ticket_template_with_context(@user, "suite_trial_sample_ticket")
        end

        it "returns the right sample ticket" do
          assert_response :ok
          sample_ticket_id = JSON.parse(@response.body)['sample_ticket_id']
          assert_equal Ticket.find_by_nice_id(sample_ticket_id).subject, @ticket_template[:subject]
        end

        should_change("the number of Tickets", by: 1) { @user.account.tickets.count }
      end

      describe "requested sample ticket already exists" do
        before do
          @ticket = Ticket.generate_sample_ticket_without_notifications(@user.account, @user, "suite_trial_sample_ticket")
        end

        it "does not create new tickets" do
          assert_difference -> { @user.account.tickets.count }, 0 do
            get :find_sample_ticket_id, params: { ticket_type: "suite_trial" }
          end
        end

        it "returns the existing sample ticket" do
          get :find_sample_ticket_id, params: { ticket_type: "suite_trial" }
          assert_equal @ticket.nice_id, JSON.parse(@response.body)["sample_ticket_id"]
        end
      end

      describe "sample ticket is in the SOLVED state" do
        before do
          @ticket = Ticket.generate_sample_ticket_without_notifications(@user.account, @user, "suite_trial_sample_ticket")
          # @ticket.assignee_id = @user.account.users.last.id
          @ticket.status_id = StatusType.SOLVED
          @ticket.will_be_saved_by(@user)
          @ticket.save!(validate: false)
        end

        it "does not create new tickets" do
          assert_difference -> { @user.account.tickets.count }, 0 do
            get :find_sample_ticket_id, params: { ticket_type: "suite_trial" }
          end
        end
      end

      describe "with invalid type" do
        before do
          get :find_sample_ticket_id, params: { ticket_type: "invalid_test" }
        end

        should_change("the number of Tickets", by: 0) { @user.account.tickets.count }

        it "returns bad_request" do
          assert_response :bad_request
        end
      end
    end
  end
end
