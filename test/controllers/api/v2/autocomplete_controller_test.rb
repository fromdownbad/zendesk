require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AutocompleteController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :tag_scores

  before do
    accept :json
  end

  with_options(controller: "api/v2/autocomplete") do |request|
    request.should_route :post, "/api/v2/autocomplete/tags", action: "tags"
    request.should_route :get, "/api/v2/autocomplete/tags", action: "tags"
  end

  as_an_anonymous_user do
    should_be_unauthorized :tags
  end

  as_an_end_user do
    should_be_forbidden :tags
  end

  as_an_agent do
    describe "a GET to :tags" do
      it('responds with ok') do
        get :tags, params: { name: "doc" }
        assert_response :ok
      end

      it "returns an array of matching tags" do
        get :tags, params: { name: "doc" }
        json = JSON.parse(@response.body)
        assert_equal({ "tags" => ["document"] }, json)
      end

      describe_with_arturo_disabled :search_cache_control_header do
        it "does not set cache-control headers" do
          get :tags, params: { name: "doc" }
          assert_nil @response.headers['Cache-Control']
        end
      end

      describe_with_arturo_enabled :search_cache_control_header do
        it "sets cache-control max-age 5 headers for Lotus requests" do
          @controller.stubs(:lotus_internal_request?).returns(true)
          get :tags, params: { name: "doc" }
          assert_equal @response.headers['Cache-Control'], 'max-age=5, private'
        end

        it "sets cache-control max-age 10 headers for non-Lotus requests" do
          @controller.stubs(:lotus_internal_request?).returns(false)
          get :tags, params: { name: "doc" }
          assert_equal @response.headers['Cache-Control'], 'max-age=10, private'
        end
      end
    end

    # For backwards compatibility, we will still allow POST requests to the
    # autocompletion API.
    describe "a POST to :tags" do
      before { post :tags, params: { name: "doc" } }

      it('responds with ok') { assert_response :ok }

      it "returns an array of matching tags" do
        json = JSON.parse(@response.body)
        assert_equal({ "tags" => ["document"] }, json)
      end
    end
  end
end
