require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::UploadsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: 'api/v2/uploads') do |request|
    request.should_route :post, '/api/v2/uploads', action: 'create'
  end

  as_an_anonymous_user do
    describe "a POST to #create" do
      describe_with_arturo_disabled :sse_prevent_anonymous_uploads do
        describe "with account_is_open set to true" do
          before { @controller.send(:current_account).update_attribute('is_open', true) }

          it "allows an upload through" do
            post :create, params: { uploaded_data: fixture_file_upload("small.png") }
            assert_response :created
          end

          it "should present the correct CORS header" do
            post :create, params: { uploaded_data: fixture_file_upload("small.png") }
            assert_equal "*", response.headers["Access-Control-Allow-Origin"]
          end
        end

        describe "with account_is_open set to false" do
          before { @controller.send(:current_account).update_attribute('is_open', false) }

          it "blocks an upload" do
            post :create, params: { uploaded_data: fixture_file_upload("small.png") }
            assert_response :unauthorized
          end
        end

        describe "with the private attachments setting enabled" do
          before do
            @controller.send(:current_account).settings.private_attachments = 1
            @controller.send(:current_account).save!
          end

          it "still allows uploads" do
            post :create, params: { uploaded_data: fixture_file_upload("small.png") }
            assert_response :success
          end
        end

        describe "when the customers can attach files setting is enabled" do
          before do
            @controller.send(:current_account).is_attaching_enabled = true
            @controller.send(:current_account).save!
            post :create, params: { uploaded_data: fixture_file_upload("small.png") }
          end

          it('responds with created') { assert_response :created }
        end

        describe "when the customers can attach files setting is disabled" do
          before do
            @controller.send(:current_account).is_attaching_enabled = false
            @controller.send(:current_account).save!
            post :create, params: { uploaded_data: fixture_file_upload("small.png") }
          end

          it('responds with unauthorized') { assert_response :unauthorized }
        end
      end

      describe_with_arturo_enabled :sse_prevent_anonymous_uploads do
        describe "with the private attachments setting enabled" do
          before do
            @controller.send(:current_account).settings.private_attachments = 1
            @controller.send(:current_account).save!
            post :create, params: { uploaded_data: fixture_file_upload("small.png") }
          end

          it { assert_response :unauthorized }
        end
      end
    end
  end

  as_an_end_user do
    describe "with missing file data" do
      before { post :create }
      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
    end

    describe "with filename longer than 249 characters" do
      before do
        data = fixture_file_upload("small.png")
        data.stubs(:original_filename).returns("#{'a' * 246}.png")
        post :create, params: { uploaded_data: data }
      end

      it('responds with bad_request') { assert_response :bad_request }

      it "has error code AttachmentFilenameTooLong" do
        assert_equal "AttachmentFilenameTooLong", JSON(response.body)["error"]
      end
    end

    describe "that cannot be saved" do
      before do
        data = fixture_file_upload("small.png")
        Upload::TokenHandler.any_instance.stubs(:save!).returns(false)
        post :create, params: { uploaded_data: data }
      end

      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }

      it "has error code AttachmentUnprocessable" do
        assert_equal "AttachmentUnprocessable", JSON(response.body)["error"]
      end
    end

    describe "when the end users can attach files setting is enabled" do
      before do
        Account.any_instance.stubs(is_attaching_enabled?: true)
        post :create, params: { uploaded_data: fixture_file_upload("small.png") }
      end

      it('responds with created') { assert_response :created }
    end

    describe "when the end users can attach files setting is not enabled" do
      before { Account.any_instance.stubs(is_attaching_enabled?: false) }

      should_be_forbidden :create
    end

    describe "a POST to #create" do
      it "responds with text/plain when the Accept header is not application/json" do
        # This is what IE9 actually sends as the Accept header for a file upload
        @request.accept = 'text/html, application/xhtml+xml, */*'

        post :create, params: { uploaded_data: fixture_file_upload("small.png") }

        assert_response :created
        @response.headers["Content-Type"].must_include(Mime[:text])
      end

      it "responds with application/json when the Accept header is application/json" do
        post :create, params: { uploaded_data: fixture_file_upload("small.png") }

        assert_response :created
        @response.headers["Content-Type"].must_include(Mime[:json])
      end

      it "should present the correct CORS header" do
        post :create, params: { uploaded_data: fixture_file_upload("small.png") }
        assert_equal "*", response.headers["Access-Control-Allow-Origin"]
      end

      describe "when the customers can attach files setting is enabled" do
        before do
          @controller.send(:current_account).is_attaching_enabled = true
          @controller.send(:current_account).save!
          post :create, params: { uploaded_data: fixture_file_upload("small.png") }
        end

        it('responds with created') { assert_response :created }
      end

      describe "when the customers can attach files setting is disabled" do
        before do
          @controller.send(:current_account).is_attaching_enabled = false
          @controller.send(:current_account).save!
          post :create, params: { uploaded_data: fixture_file_upload("small.png") }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end
    end

    describe "for a new token" do
      before { UploadToken.without_arsi.delete_all }

      describe "a POST to #create" do
        before { post :create, params: { uploaded_data: fixture_file_upload("small.png") } }
        should_use_presenter Api::V2::UploadPresenter, status: :created, location: %r{/api/v2/attachments/\d+\.json}
        should_change("the number of upload tokens", by: 1) { UploadToken.count(:all) }
        should_change("the number of attachments",   by: 1) { Attachment.originals.count(:all) }
        it "assigns target for upload token" do
          upload_token = UploadToken.last
          assert_equal users(:minimum_end_user), upload_token.target
        end
      end

      describe "a POST to #create with uploaded_data and a filename" do
        before { post :create, params: { uploaded_data: fixture_file_upload("small.png"), filename: "testing.png" } }

        should_use_presenter Api::V2::UploadPresenter, status: :created
        should_change("the number of attachments", by: 1) { Attachment.originals.count(:all) }

        it "has a proper filename and defaults to inline being false" do
          attachment = Attachment.originals.last
          assert_equal "testing.png", attachment.display_filename
          assert_equal false, attachment.inline
        end
      end

      describe "a POST to #create with uploaded_data and inline parameter specified" do
        before { post :create, params: { uploaded_data: fixture_file_upload("small.png"), inline: true } }

        should_use_presenter Api::V2::UploadPresenter, status: :created
        should_change("the number of attachments", by: 1) { Attachment.originals.count(:all) }

        it "sets the attachment as an inline attachment" do
          attachment = Attachment.originals.last
          assert(attachment.inline)
        end
      end

      describe_with_and_without_arturo_enabled :email_increase_attachment_size_limit do
        describe "a POST to #create with raw POST body and a filename" do
          before do
            raw_post :create, { filename: "testing.png" }, fixture_file_upload("small.png").read
          end

          should_use_presenter Api::V2::UploadPresenter, status: :created
          should_change("the number of attachments", by: 1) { Attachment.originals.count(:all) }

          it "has a proper filename" do
            attachment = Attachment.originals.last
            assert_equal "testing.png", attachment.display_filename
          end
        end
      end

      describe_with_arturo_disabled :email_increase_attachment_size_limit do
        let (:filename) { "too_large.bin" }
        let (:attachment) { fixture_file_upload(filename) }

        describe "a POST with uploaded_data that is too large" do
          before do
            post :create, params: { uploaded_data: attachment, filename: filename }
          end

          it ('responds with too large') { assert_response(:payload_too_large) }

          it "has error code AttachmentTooLarge" do
            assert_equal "AttachmentTooLarge", JSON(response.body)["error"]
          end
        end

        describe "a POST with raw POST body that is too large" do
          before do
            raw_post :create, { filename: filename }, attachment.read
          end

          it ('responds with too large') { assert_response(:payload_too_large) }

          it "has error code AttachmentTooLarge" do
            assert_equal "AttachmentTooLarge", JSON(response.body)["error"]
          end
        end

        describe "a POST with uploaded_data that is too large when the Accept header is not application/json" do
          before do
            # This is what IE9 actually sends as the Accept header for a file upload
            @request.accept = 'text/html, application/xhtml+xml, */*'
            post :create, params: { uploaded_data: attachment, filename: filename }
          end

          it ('responds with too large') { assert_response(:payload_too_large) }

          it "responds with text/plain" do
            @response.headers["Content-Type"].must_include(Mime[:text])
          end

          it "has error code AttachmentTooLarge" do
            assert_equal "AttachmentTooLarge", JSON(response.body)["error"]
          end
        end
      end

      describe_with_arturo_enabled :email_increase_attachment_size_limit do
        describe "a file that is larger than the previous plan based limits but smaller than the new 50MB account limit" do
          let(:attachment_30MB) do
            Tempfile.create(["attachment_30MB", ".txt"]) do |file|
              file.write 'a' * 30.megabytes
              Rack::Test::UploadedFile.new(file)
            end
          end

          describe "sent in a POST" do
            before do
              post :create, params: { uploaded_data: attachment_30MB, filename: attachment_30MB.original_filename }
            end

            it ('allows the upload') { assert_response :created }
          end

          describe "sent in a POST with raw POST body" do
            before do
              raw_post :create, { filename: attachment_30MB.original_filename }, attachment_30MB.read
            end

            it ('allows the upload') { assert_response :created }
          end

          describe "sent in a POST with uploaded_data when the Accept header is not application/json" do
            before do
              # This is what IE9 actually sends as the Accept header for a file upload
              @request.accept = 'text/html, application/xhtml+xml, */*'
              post :create, params: { uploaded_data: attachment_30MB, filename: attachment_30MB.original_filename }
            end

            it ('allows the upload') { assert_response :created }
          end
        end

        describe "a file that is larger than the account upload limit" do
          let(:attachment_51MB) do
            Tempfile.create(["attachment_51MB", ".txt"]) do |file|
              file.write 'a' * 51.megabytes
              Rack::Test::UploadedFile.new(file)
            end
          end

          describe "sent in a POST" do
            before do
              post :create, params: { uploaded_data: attachment_51MB, filename: attachment_51MB.original_filename }
            end

            it ('responds with too large') { assert_response(:payload_too_large) }

            it "has error code AttachmentTooLarge" do
              assert_equal "AttachmentTooLarge", JSON(response.body)["error"]
            end
          end

          describe "sent in a POST with raw POST body" do
            before do
              raw_post :create, { filename: attachment_51MB.original_filename }, attachment_51MB.read
            end

            it ('responds with too large') { assert_response(:payload_too_large) }

            it "has error code AttachmentTooLarge" do
              assert_equal "AttachmentTooLarge", JSON(response.body)["error"]
            end
          end

          describe "sent in a POST with uploaded_data when the Accept header is not application/json" do
            before do
              # This is what IE9 actually sends as the Accept header for a file upload
              @request.accept = 'text/html, application/xhtml+xml, */*'
              post :create, params: { uploaded_data: attachment_51MB, filename: attachment_51MB.original_filename }
            end

            it ('responds with too large') { assert_response(:payload_too_large) }

            it "responds with text/plain" do
              @response.headers["Content-Type"].must_include(Mime[:text])
            end

            it "has error code AttachmentTooLarge" do
              assert_equal "AttachmentTooLarge", JSON(response.body)["error"]
            end
          end
        end
      end

      describe "for an empty upload" do
        before do
          set_body('') # Workaround for TestRequest writing the parameters into the body
        end

        describe "a POST with uploaded_data that is blank" do
          before do
            post :create, params: { uploaded_data: "", filename: "empty.txt" }
          end
          it('responds with unprocessable') { assert_response(:unprocessable_entity) }

          it "has error code AttachmentUnprocessable" do
            assert_equal "AttachmentUnprocessable", JSON(response.body)["error"]
          end
        end

        describe "a POST with raw POST body that is blank" do
          before do
            raw_post :create, { filename: "empty.txt" }, ""
          end
          it('responds with unprocessable') { assert_response(:unprocessable_entity) }

          it "has error code AttachmentUnprocessable" do
            assert_equal "AttachmentUnprocessable", JSON(response.body)["error"]
          end
        end

        describe "a POST without params and a raw POST body that is blank" do
          before do
            raw_post :create, {}, ""
          end
          it('responds with unprocessable') { assert_response(:unprocessable_entity) }

          it "has error code AttachmentUnprocessable" do
            assert_equal "AttachmentUnprocessable", JSON(response.body)["error"]
          end
        end

        describe "a POST with uploaded_data that is blank when the Accept header is not application/json" do
          before do
            # This is what IE9 actually sends as the Accept header for a file upload
            @request.accept = 'text/html, application/xhtml+xml, */*'
            post :create, params: { uploaded_data: "", filename: "empty.txt" }
          end
          it('responds with unprocessable') { assert_response(:unprocessable_entity) }

          it "responds with text/plain" do
            @response.headers["Content-Type"].must_include(Mime[:text])
          end

          it "has error code AttachmentUnprocessable" do
            assert_equal "AttachmentUnprocessable", JSON(response.body)["error"]
          end
        end
      end
    end

    describe "for a existing token" do
      before { @token = @account.upload_tokens.first.value }

      describe "a POST to #create" do
        before { post :create, params: { token: @token, uploaded_data: fixture_file_upload("small.png") } }
        should_use_presenter Api::V2::UploadPresenter, status: :created
        should_not_change("the number of upload tokens") { UploadToken.count(:all) }
        should_change("the number of attachments", by: 1) { Attachment.originals.count(:all) }
      end

      describe "a DELETE to #destroy" do
        before { delete :destroy, params: { id: @token } }
        should_change("the number of tokens", by: -1) { UploadToken.count(:all) }
        it('responds with no_content') { assert_response :no_content }
      end
    end

    describe "with some random uploaded_data" do
      describe "a POST to #create" do
        describe "accepts json" do
          before do
            post :create, params: { uploaded_data: "@/tmp/contact_files/phpfl7w7A;type=image/jpeg" }
          end
          it('responds with bad_request') { assert_response :bad_request }
        end

        describe "accepts text" do
          before do
            set_header "HTTP_ACCEPT", "text/plain"
            post :create, params: { uploaded_data: "@/tmp/contact_files/phpfl7w7A;type=image/jpeg" }
          end
          it('responds with bad_request') { assert_response :bad_request }
        end
      end
    end
  end

  as_a_subsystem_user(user: "chat_widget_mediator", account: :minimum) do
    describe "a POST to #create" do
      before do
        post :create, params: { uploaded_data: fixture_file_upload("small.png") }
      end

      it('responds with created') { assert_response :created }
    end
  end

  as_an_agent do
    describe "a POST to #create" do
      describe "when the customers can attach files setting is enabled" do
        before do
          @controller.send(:current_account).is_attaching_enabled = true
          @controller.send(:current_account).save!
          post :create, params: { uploaded_data: fixture_file_upload("small.png") }
        end

        it('responds with created') { assert_response :created }
      end

      describe "when the customers can attach files setting is disabled" do
        before do
          @controller.send(:current_account).is_attaching_enabled = false
          @controller.send(:current_account).save!
          post :create, params: { uploaded_data: fixture_file_upload("small.png") }
        end

        it('responds with created') { assert_response :created }
      end

      describe_with_arturo_enabled :polaris do
        describe 'when a ticket_id is sent' do
          let(:ticket) { @controller.send(:current_account).tickets.first }

          before do
            @controller.send(:current_account).is_attaching_enabled = false
            @controller.send(:current_account).save!
            post :create, params: { uploaded_data: fixture_file_upload('small.png'), ticket_id: ticket.nice_id }
          end

          it('responds with created') { assert_response :created }

          it('creates an upload token with target Ticket') do
            token = JSON.parse(@response.body).dig('upload', 'token')
            upload_token = UploadToken.find_by_value!(token)

            assert_equal 'Ticket', upload_token.target_type
          end
        end

        describe 'when a ticket_id is not sent' do
          before do
            @controller.send(:current_account).is_attaching_enabled = false
            @controller.send(:current_account).save!
            post :create, params: { uploaded_data: fixture_file_upload('small.png') }
          end

          it('responds with created') { assert_response :created }

          it('creates an upload token with target User') do
            token = JSON.parse(@response.body).dig('upload', 'token')
            upload_token = UploadToken.find_by_value!(token)

            assert_equal 'User', upload_token.target_type
          end
        end
      end
    end
  end

  # this is a helper to test POST of raw data
  def raw_post(action, params, body)
    @request.env["RAW_POST_DATA"] = body
    response = post(action, params: params)
    @request.env.delete("RAW_POST_DATA")
    response
  end
end
