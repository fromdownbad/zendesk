require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::V2::FeatureUsageMetricsController do
  extend Api::Presentation::TestHelper

  before do
    accept :json
  end

  with_options(controller: 'api/v2/feature_usage_metrics') do |request|
    request.should_route :get, '/api/v2/feature_usage_metrics', action: :index
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_agent do
    describe 'a GET to #index' do
      before { get :index }

      should_use_presenter Api::V2::FeatureUsageMetricsPresenter
    end
  end
end
