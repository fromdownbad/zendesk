require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::RecipientAddressesController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users, :recipient_addresses

  let(:account) { accounts(:minimum) }
  let(:recipient_address) { recipient_addresses(:not_default) }

  def stub_mx(domain)
    mx = Resolv::DNS::Resource::IN::MX.new(0, domain)
    Resolv::DNS.stubs(:open).returns(stub(getresources: [mx]))
  end

  before do
    Arturo.enable_feature! "recipient_addresses"
    accept :json
  end

  should_route :get, '/api/v2/recipient_addresses', action: 'index', controller: 'api/v2/recipient_addresses'
  should_route :put, '/api/v2/recipient_addresses/1/verify', action: 'verify', controller: 'api/v2/recipient_addresses', id: 1

  it "stub_mx should stub properly" do
    domain = 'foo.com'
    stub_mx(domain)
    mx_handlers = Resolv::DNS.open.getresources(domain, Resolv::DNS::Resource::IN::MX)
    # normally has 3 results without stubs
    assert_equal 1, mx_handlers.count
    assert_equal 'foo.com', mx_handlers.first.exchange.to_s
  end

  as_an_anonymous_user do
    should_be_unauthorized :create, :show, :update, :destroy, :index, [:put, :verify, id: 1]
  end

  as_an_end_user do
    should_be_forbidden :create, :show, :update, :destroy, :index, [:put, :verify, id: 1]
  end

  as_an_agent_with_permissions(extensions_and_channel_management: false) do
    should_be_forbidden :create, :update, :destroy, [:put, :verify, id: 1]

    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::RecipientAddressPresenter, status: :ok
    end

    describe "a GET to :show" do
      before { get :show, params: { id: recipient_address.id } }
      should_use_presenter Api::V2::RecipientAddressPresenter
    end
  end

  as_an_agent_with_permissions(extensions_and_channel_management: true) do
    describe "a GET to :lookup" do
      it 'detects google mx records' do
        stub_mx('google.com')
        get :lookup, params: { domain: 'test.com' }
        assert_response :success
        response.body.must_include 'google'
      end

      it 'detects outlook mx records' do
        stub_mx('outlook.com')

        get :lookup, params: { domain: 'test.com' }
        assert_response :success
        response.body.must_include 'outlook'
      end

      it 'detects hotmail mx records' do
        stub_mx('hotmail.com')
        get :lookup, params: { domain: 'test.com' }
        assert_response :success
        response.body.must_include 'outlook'
      end

      it 'detects yahoo mx records' do
        stub_mx('yahoodns.net')
        get :lookup, params: { domain: 'test.com' }
        assert_response :success
        response.body.must_include 'yahoo'
      end

      it 'detects other mx records' do
        stub_mx('test.com')
        get :lookup, params: { domain: 'test.com' }
        assert_response :success
        response.body.must_include 'other'
      end
    end

    describe "a POST to :create with allowed parameters" do
      before do
        post :create, params: { recipient_address: {
          email: "john@doe.com",
          name: "John Doe",
          default: false,
          brand_id: account.brands.first.id
        } }
      end
      should_use_presenter Api::V2::RecipientAddressPresenter, status: 201
    end

    describe "a POST to :create with invalid payload" do
      before do
        post :create, params: { recipient_address: { email: "" } }
      end
      should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
    end

    describe "a PUT to :update" do
      describe "with allowed parameters" do
        let(:brand_id) { account.brands.last.id }

        before do
          put :update, params: { id: recipient_address.id, recipient_address: {
            name: "New Name",
            default: false,
            brand_id: brand_id
          } }
        end

        should_use_presenter Api::V2::RecipientAddressPresenter

        it "changes the record" do
          body = JSON.parse(@response.body)
          assert_equal "New Name", body["recipient_address"]["name"]
          refute body["recipient_address"]["default"]
          assert_equal brand_id, body["recipient_address"]["brand_id"]
        end
      end

      describe "with an invalid payload (model disallows changes to :email)" do
        before { put :update, params: { id: recipient_address.id, recipient_address: { email: "" } } }

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

        it "does not update" do
          recipient_address_name = recipient_address.reload.name
          assert_not_equal "default@example.net", recipient_address_name
          assert_not_empty recipient_address_name
        end
      end
    end

    describe "a PUT to :verify" do
      resque_inline false

      it "performs forwarding verification" do
        put :verify, params: { id: recipient_address.id, type: "forwarding" }
        assert_response :success
        assert_equal "{}", response.body # empty json so jquery does not blow up
        assert_equal :waiting, recipient_address.reload.forwarding_status
      end

      it "performs SPF verification" do
        recipient_address.update_column(:spf_status_id, RecipientAddress::SPF_STATUS.fetch(:failed))
        RecipientAddress.any_instance.expects(default_host?: true).twice

        put :verify, params: { id: recipient_address.id, type: "spf" }

        assert_response :success
        assert_equal "{}", response.body # empty json so jquery does not blow up
        assert_equal :verified, recipient_address.reload.spf_status
      end

      describe_with_arturo_enabled :email_deprecate_cname_checks do
        it "performs DNS verification for the domain verification record only" do
          recipient_address.update_column(:metadata, nil)
          RecipientAddress.any_instance.expects(default_host?: true).times(1)

          put :verify, params: { id: recipient_address.id, type: "dns" }

          assert_response :success
          assert_equal "{}", response.body # empty json so jquery does not blow up
          assert_equal "verified", recipient_address.reload.metadata["domain_verification"]["status"]
        end
      end

      describe_with_arturo_disabled :email_deprecate_cname_checks do
        it "performs DNS verification for TXT and CNAME records" do
          recipient_address.update_column(:metadata, nil)
          RecipientAddress.any_instance.expects(default_host?: true).times(3)

          put :verify, params: { id: recipient_address.id, type: "dns" }

          assert_response :success
          assert_equal "{}", response.body # empty json so jquery does not blow up
          assert_equal "verified", recipient_address.reload.metadata["domain_verification"]["status"]
        end
      end
    end

    describe "a DELETE to :destroy" do
      it "deletes and responds with 204" do
        assert_difference "RecipientAddress.count(:all)", -1 do
          delete :destroy, params: { id: recipient_address.id }
        end
        assert_response :no_content
      end

      it "does not delete backup" do
        recipient_address = account.recipient_addresses.detect(&:backup?)
        refute_difference "RecipientAddress.count(:all)" do
          delete :destroy, params: { id: recipient_address.id }
        end
        assert_response :unprocessable_entity
      end
    end
  end

  as_a_subsystem_user(user: 'collaboration', account: :minimum) do
    should_be_forbidden :create, :update, :destroy, :show

    describe 'index' do
      before { get :index }
      it('responds with success') { assert_response :success }
    end
  end

  # For a complete set of tests, go to `zendesk_cursor_pagination` gem.
  as_a_subsystem_user(account: :minimum) do
    describe 'using cursor based pagination' do
      let(:account) { accounts(:minimum) }
      let(:page_size) { 2 }
      let(:page_params) { {size: page_size} }
      let(:json) do
        get :index, params: { page: page_params }
        JSON.parse(@response.body).with_indifferent_access
      end

      describe 'index' do
        before { get :index }
        it('responds with success') { assert_response :success }
      end

      it 'generates the correct order clause' do
        assert_sql_queries(1, /ORDER BY `recipient_addresses`.`id`/) do
          json
        end
      end

      describe 'when there is only one page' do
        let(:page_size) { account.recipient_addresses.size + 1 }

        it 'presents the correct keys' do
          refute json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of recipient addresses' do
          assert_equal account.recipient_addresses.size, json[:recipient_addresses].size
        end
      end

      describe 'when there are multiple pages' do
        let(:page_size) { account.recipient_addresses.size - 1 }

        it 'presents the correct keys' do
          assert json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of recipient addresses' do
          assert_equal page_size, json[:recipient_addresses].size
        end
      end

      describe 'with a valid after cursor' do
        let(:page_size) { account.recipient_addresses.size - 1 }
        let(:after_json) { JSON.parse(@response.body).with_indifferent_access }

        before do
          after_cursor = json[:meta][:after_cursor]
          # reset memoized recipient_addresses
          @controller.remove_instance_variable(:@recipient_addresses)

          get :index, params: { page: {
            size: page_size,
            after: after_cursor
          } }
        end

        it 'presents the correct keys' do
          refute after_json[:meta][:has_more]
          assert after_json[:meta][:before_cursor].present?
          assert after_json[:meta][:after_cursor].present?
          assert after_json[:links][:prev].present?
          assert after_json[:links][:next].present?
        end

        it 'presents the correct number of recipient addresses' do
          assert_equal 1, after_json[:recipient_addresses].size
        end
      end

      describe 'with an invalid after cursor' do
        let(:page_params) do
          {
            size: page_size,
            after: 'invalid'
          }
        end

        it 'presents an error message' do
          assert_equal 'InvalidPaginationParameter', json[:error]
          assert_equal 'page[after] is not valid', json[:description]
          assert_response :bad_request
        end
      end

      describe_with_arturo_disabled :email_cursor_pagination_recipient_addresses_index do
        it 'presents the same error message we were showing before adding the types' do
          assert_equal 'Invalid attribute', json[:error][:title]
          assert_equal 'You passed an invalid value for the page attribute. Invalid parameter: page must be an integer', json[:error][:message]
          assert_response :bad_request
        end
      end
    end
  end
end
