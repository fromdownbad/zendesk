require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::DynamicContent::VariantsController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :translation_locales

  let(:account) { accounts(:minimum) }
  let(:fbattrs) { {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: 1 } }

  before do
    accept :json
  end

  with_options(controller: "api/v2/dynamic_content/variants") do |request|
    request.should_route :get,   "/api/v2/dynamic_content/items/6/variants",     action: "index", item_id: 6
    request.should_route :post,  "/api/v2/dynamic_content/items/6/variants",     action: "create", item_id: 6
    request.should_route :get,   "/api/v2/dynamic_content/items/6/variants/46",  action: "show", item_id: 6, id: 46
    request.should_route :put,   "/api/v2/dynamic_content/items/6/variants/46",  action: "update", item_id: 6, id: 46
    request.should_route :delete, "/api/v2/dynamic_content/items/6/variants/46", action: "destroy", item_id: 6, id: 46
    request.should_route :post,   "/api/v2/dynamic_content/items/6/variants/create_many", action: "create_many", item_id: 6
    request.should_route :put,    "/api/v2/dynamic_content/items/6/variants/update_many", action: "update_many", item_id: 6
  end

  describe "embeddable subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "embeddable") do
      should_be_forbidden [:get, :show, item_id: 1, id: 1], [:post, :create, item_id: 1], [:post, :create_many, item_id: 1], [:put, :update, item_id: 1, id: 1], [:put, :update_many, item_id: 1], [:delete, :destroy, item_id: 1, id: 1]

      describe "a GET to :index" do
        before do
          text = Cms::Text.create!(name: "new text", fallback_attributes: fbattrs, account: account)
          get :index, params: { item_id: text.id }
        end

        it("responds with success") { assert_response :success }
      end
    end
  end

  describe "when using oauth tokens" do
    let(:valid_params) { {content: "Content A", locale_id: 2, active: true, is_fallback: false} }
    let(:text) { Cms::Text.create!(name: "I haz a reference", fallback_attributes: fbattrs, account: account) }
    let(:variant) { text.variants.create!(value: "Content B", translation_locale_id: 4, active: true, is_fallback: false) }

    with_scopes('dynamic_content:read', 'read') do
      should_be_authorized { get :index, params: { item_id: text.id } }
      should_be_authorized { get :show, params: { item_id: text.id, id: variant.id } }

      should_not_be_authorized { post :create, params: { item_id: text.id, variant: valid_params } }
      should_not_be_authorized { put :update, params: { item_id: text.id, id: variant.id, variant: {content: "New content!"} } }
      should_not_be_authorized { delete :destroy, params: { item_id: text.id, id: variant.id } }
    end

    with_scopes('dynamic_content:write', 'write') do
      should_be_authorized { post :create, params: { item_id: text.id, variant: valid_params } }
      should_be_authorized { post :create_many, params: { item_id: text.id, variants: [valid_params] } }
      should_be_authorized { put :update, params: { item_id: text.id, id: variant.id, variant: {content: "New content!"} } }
      should_be_authorized { put :update_many, params: { item_id: text.id, id: variant.id, variants: [{content: "New content!"}] } }
      should_be_authorized { delete :destroy, params: { item_id: text.id, id: variant.id } }

      should_not_be_authorized { get :index, params: { item_id: text.id } }
      should_not_be_authorized { get :show, params: { item_id: text.id, id: variant.id } }
    end

    with_scopes('read', 'write') do
      should_be_authorized { post :create_many, params: { item_id: text.id, variants: [valid_params] } }
      should_be_authorized { put :update_many, params: { item_id: text.id, id: variant.id, variants: [{content: "New content!"}] } }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index, item_id: 1], [:post, :create, item_id: 1], [:post, :create_many, item_id: 1], [:get, :show, item_id: 1, id: 1], [:put, :update, item_id: 1, id: 1], [:put, :update_many, item_id: 1], [:delete, :destroy, item_id: 1, id: 1]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index, item_id: 1], [:post, :create, item_id: 1], [:post, :create_many, item_id: 1], [:get, :show, item_id: 1, id: 1], [:put, :update, item_id: 1, id: 1], [:put, :update_many, item_id: 1], [:delete, :destroy, item_id: 1, id: 1]
  end

  as_an_agent do
    describe "without permission" do
      before { User.any_instance.stubs(:can?).returns(false) }

      should_be_forbidden [:get, :index, item_id: 1], [:post, :create, item_id: 1], [:post, :create_many, item_id: 1], [:get, :show, item_id: 1, id: 1], [:put, :update, item_id: 1, id: 1], [:put, :update_many, item_id: 1], [:delete, :destroy, item_id: 1, id: 1]
    end

    describe "with permission" do
      before do
        User.any_instance.stubs(:can?).returns(true)
        @text = Cms::Text.create!(name: "I haz a reference", fallback_attributes: fbattrs, account: account)
        @variant = @text.variants.last
      end

      describe "a GET to :index" do
        before { get :index, params: { item_id: @text.id } }
        should_use_presenter Api::V2::DynamicContent::VariantPresenter, status: :ok
      end

      describe "a POST to :create" do
        describe "with valid params" do
          before do
            post :create, params: { item_id: @text.id, variant:             { content: "Content A", locale_id: 2, active: true, is_fallback: false} }
          end

          should_use_presenter Api::V2::DynamicContent::VariantPresenter, status: :created
          should_change("the account cms variants count", by: 1) { @account.cms_variants.count(:all) }
          should_change("the amount of DC variants count", by: 1) { DC::Snippet.count(:all) }
        end

        describe "with invalid params" do
          before do
            post :create, params: { item_id: @text.id, variant:             { content: "Content A", locale_id: 9999, active: true, is_fallback: false} }
          end

          it('responds with unprocessable_request') { assert_response 422 }
          should_not_change("the account cms variants count") { @account.cms_variants.count(:all) }
          should_not_change("the amount of DC variants count") { DC::Snippet.count(:all) }
        end
      end

      describe "a POST to :create_many" do
        describe "with valid params" do
          before do
            job_id_stub = stub
            job_status_stub = stub
            VariantsBulkCreateJob.expects(:enqueue).returns(job_id_stub)
            Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
            post :create_many, params: { item_id: @text.id, variants: [
              { content: "Content A", locale_id: 2, active: true, is_fallback: false},
              { content: "Content B", locale_id: 4, active: true, is_fallback: false}
            ] }
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end

        describe "with invalid params" do
          before { post :create_many, params: { item_id: @text.id } }

          it('responds with bad_request') { assert_response :bad_request }
          should_not_change("the the number of variants") { @text.variants.count(:all) }
        end
      end

      describe "a GET to :show" do
        before do
          get :show, params: { item_id: @text.id, id: @variant.id }
        end

        should_use_presenter Api::V2::DynamicContent::VariantPresenter, status: :ok
      end

      describe "a PUT to :update" do
        before do
          put :update, params: { item_id: @text.id, id: @variant.id, variant: {content: "New content!"} }
        end

        should_use_presenter Api::V2::DynamicContent::VariantPresenter, status: :ok

        it "updates content of item" do
          assert_equal @variant.reload.value, "New content!"
        end

        it "updates the content of a dynamic content variant" do
          snippet = DC::Snippet.find_by_identifier(@text.identifier)
          assert_equal snippet.default_value, "New content!"
        end
      end

      describe "a PUT to :update_many" do
        before do
          @second_variant = @text.variants.create!(value: "Content B", translation_locale_id: 4, active: true, is_fallback: false)
          assert_equal 2, @text.variants.reload.count(:all)
        end

        describe "with valid params" do
          before do
            job_id_stub = stub
            job_status_stub = stub
            VariantsBulkUpdateJob.expects(:enqueue).returns(job_id_stub)
            Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
            put :update_many, params: { item_id: @text.id, variants: [
              { content: "New content!",       id: @variant.id},
              { content: "New content for B!", id: @second_variant.id}
            ] }
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end

        describe "with invalid params" do
          before { put :update_many, params: { item_id: @text.id } }

          it('responds with bad_request') { assert_response :bad_request }
        end
      end

      describe "a DELETE to :destroy" do
        before do
          @second_variant = @text.variants.create!(value: "Content B", translation_locale_id: 4, active: true, is_fallback: false)
        end

        it "deletes the variant and responds with 204" do
          delete :destroy, params: { item_id: @text.id, id: @second_variant.id }
          assert_response :no_content
        end
      end

      describe "#paginate_scope" do
        let(:controller) { Api::V2::DynamicContent::VariantsController.new }
        before do
          controller.stubs(params: {item_id: @text.id})
          controller.stubs(current_account: account)
        end

        it "joins if sort is by outdated" do
          controller.stubs(sort_by: "cms_variants.updated_at < fallbacks_cms_texts.updated_at")
          scope = controller.send(:paginate_scope)
          assert_equal [], scope.includes_values
          assert_equal [{text: :fallback}], scope.joins_values
        end

        it "does not join if sort is not outdated" do
          controller.stubs(sort_by: "translation_locale_id")
          scope = controller.send(:paginate_scope)
          assert_equal [], scope.includes_values
          assert_equal [], scope.joins_values
        end
      end
    end
  end
end
