require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::DynamicContent::ItemsController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :translation_locales

  let(:account) { accounts(:minimum) }
  let(:fbattrs) { {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: 1 } }
  let(:text) { Cms::Text.create!(name: "I haz a reference", fallback_attributes: fbattrs, account: account) }

  before do
    accept :json
  end

  with_options(controller: "api/v2/dynamic_content/items") do |request|
    request.should_route :get,    "/api/v2/dynamic_content/items",           action: "index"
    request.should_route :post,   "/api/v2/dynamic_content/items",           action: "create"
    request.should_route :get,    "/api/v2/dynamic_content/items/46",        action: "show",   id: 46
    request.should_route :put,    "/api/v2/dynamic_content/items/46",        action: "update", id: 46
    request.should_route :delete, "/api/v2/dynamic_content/items/46",        action: "destroy", id: 46
    request.should_route :get,    "/api/v2/dynamic_content/items/show_many", action: "show_many"
  end

  describe "when using oauth tokens" do
    let(:valid_params) { {name: "Test", default_locale_id: 1, content: "First variant"} }

    with_scopes('dynamic_content:read', 'read') do
      should_be_authorized { get :index }
      should_be_authorized { get :show, params: { id: text.id } }
      should_be_authorized { get :show_many }

      should_not_be_authorized { post :create, params: { item: valid_params } }
      should_not_be_authorized { put :update, params: { id: text.id, item: {name: "New name!"} } }
      should_not_be_authorized { delete :destroy, params: { id: text.id } }
    end

    with_scopes('dynamic_content:write', 'write') do
      should_be_authorized { post :create, params: { item: valid_params } }
      should_be_authorized { put :update, params: { id: text.id, item: {name: "New name!"} } }
      should_be_authorized { delete :destroy, params: { id: text.id } }

      should_not_be_authorized { get :index }
      should_not_be_authorized { get :show, params: { id: text.id } }
      should_not_be_authorized { get :show_many }
    end
  end

  describe "embeddable subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "embeddable") do
      should_be_forbidden :show, :create, :update, :destroy

      describe "a GET to :index" do
        before { get :index }
        it("responds with success") { assert_response :success }
      end

      describe "a GET to :show_many" do
        before { get :show_many }
        it("responds with success") { assert_response :success }
      end
    end
  end

  describe "zopim subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "zopim") do
      should_be_forbidden :show, :create, :update, :destroy

      describe "a GET to :index" do
        before { get :index }
        it("responds with success") { assert_response :success }
      end
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :create, :show, :update, :destroy, :show_many
  end

  as_an_end_user do
    should_be_forbidden :index, :create, :show, :update, :destroy, :show_many
  end

  as_an_agent do
    describe "without permission" do
      before { User.any_instance.stubs(:can?).returns(false) }

      should_be_forbidden :index, :create, :show, :update, :destroy, :show_many
    end

    describe "with permission" do
      before { User.any_instance.stubs(:can?).returns(true) }

      describe "a GET to :index" do
        before { get :index }
        should_use_presenter Api::V2::DynamicContent::ItemPresenter, status: :ok

        describe_with_arturo_disabled :dynamic_content_cache_control_header do
          it 'a GET to :index sets cache-control to max-age=0' do
            get :index
            assert_nil @response.headers['Cache-Control']
          end
        end

        describe_with_arturo_enabled :dynamic_content_cache_control_header do
          it 'a GET to :index sets cache-control to max-age=60, private' do
            get :index
            assert_equal @response.headers['Cache-Control'], 'max-age=60, private'
          end
        end
      end

      describe "a POST to :create" do
        before do
          post :create, params: { item: { name: "Test", default_locale_id: 1, content: "First variant"} }
        end

        should_use_presenter Api::V2::DynamicContent::ItemPresenter, status: :created
        should_change("the account cms text count", by: 1) { @account.cms_texts.count(:all) }
        should_change("the account DC text count", by: 1) { DC::Content.count(:all) }
      end

      describe "a POST to :create with multiple variants" do
        before do
          @payload = {
            item: {
              name: "Another Test",
              default_locale_id: 1,
              variants: [
                {locale_id: translation_locales(:japanese).id, content: "First variant", default: true},
                {locale_id: translation_locales(:spanish).id, content: "Second variant", default: false}
              ]
            }
          }
        end

        describe "with one default variant" do
          before { post :create, params: @payload }

          should_use_presenter Api::V2::DynamicContent::ItemPresenter, status: :created

          it "should create an item with two new variants" do
            new_item = @account.reload.cms_texts[0]
            assert_equal 2, new_item.variants.count(:all)
          end
        end

        describe "with no default variants" do
          before do
            @payload[:item][:variants].map { |v| v[:default] = false }
            post :create, params: @payload
          end

          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
        end

        describe "with an invalid variant" do
          before do
            @payload[:item][:variants][0][:locale_id] = 9999
            post :create, params: @payload
          end

          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
        end
      end

      describe "a GET to :show" do
        before do
          get :show, params: { id: text.id }
        end

        should_use_presenter Api::V2::DynamicContent::ItemPresenter, status: :ok
      end

      describe "a GET to :show_many" do
        let(:another_text) { Cms::Text.create!(name: "hello world", fallback_attributes: fbattrs, account: account) }
        let(:identifiers) { [text.identifier, another_text.identifier] }
        let(:parameters) { { identifiers: identifiers.join(',') } }

        before do
          get :show_many, params: parameters
        end

        should_use_presenter Api::V2::DynamicContent::ItemPresenter, status: :ok

        it 'returns the items of the supplied identifiers' do
          res = JSON.parse(@response.body)['items']
          assert_equal 2, res.size
          assert_equal [text.id, another_text.id].sort, res.map { |item| item['id'] }.sort
        end

        describe 'no parameters passed' do
          let(:parameters) { {} }

          it 'returns items as an empty array' do
            res = JSON.parse(@response.body)['items']
            assert_equal 0, res.size
          end
        end
      end

      describe "a PUT to :update" do
        before do
          put :update, params: { id: text.id, item: {name: "New name!"} }
        end

        should_use_presenter Api::V2::DynamicContent::ItemPresenter, status: :ok

        it "updates the name of item" do
          assert_equal text.reload.name, "New name!"
        end
      end

      describe "a DELETE to :destroy" do
        before do
          DC::Synchronizer.synchronize_snippet(text)

          assert_equal 1, DC::Snippet.count(:all)
        end

        it "responds with 204" do
          delete :destroy, params: { id: text.id }
          assert_response :no_content
        end

        it "deletes the item" do
          delete :destroy, params: { id: text.id }
          assert_nil account.reload.cms_texts.find_by_id(text.id)
        end

        it "deletes all snippets" do
          delete :destroy, params: { id: text.id }
          DC::Snippet.count(:all)
        end
      end

      describe "#paginate_scope" do
        let(:controller) { Api::V2::DynamicContent::ItemsController.new }
        before do
          controller.stubs(current_account: account)
        end

        it "joins and include if we sort by outdated" do
          controller.stubs(sort_by: "updated_at < fallbacks_cms_texts.updated_at")
          scope = controller.send(:paginate_scope)
          assert_equal [:fallback], scope.includes_values
          assert_equal [:variants], scope.joins_values
        end

        it "only join if we sort by default_locale_id" do
          controller.stubs(sort_by: "cms_variants.translation_locale_id")
          scope = controller.send(:paginate_scope)
          assert_equal [], scope.includes_values
          assert_equal [:variants], scope.joins_values
        end

        it "justs get the association if sort is not outdated or default_locale_id" do
          controller.stubs(sort_by: "name")
          scope = controller.send(:paginate_scope)
          assert_equal [], scope.includes_values
          assert_equal [], scope.joins_values
        end
      end
    end
  end
end
