require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Users::PasswordController do
  extend Api::V2::TestHelper
  fixtures :users, :role_settings, :translation_locales

  before do
    accept :json

    @user = users(:minimum_end_user)
  end

  with_options(controller: "api/v2/users/password") do |request|
    request.should_route :post, "/api/v2/users/1/password",              action: "create",       user_id: "1"
    request.should_route :put,  "/api/v2/users/1/password",              action: "update",       user_id: "1"
    request.should_route :post, "/api/v2/users/1/password/reset",        action: "reset",        user_id: "1"
    request.should_route :get,  "/api/v2/users/1/password/requirements", action: "requirements", user_id: "1"
  end

  as_an_anonymous_user do
    should_be_unauthorized :create, :update, [:post, :reset, {id: 1}], add_to_all: {user_id: 1}
  end

  as_an_end_user do
    should_be_forbidden :create, add_to_all: {user_id: 1}

    describe "a PUT to :update" do
      describe 'without permissions' do
        before do
          User.any_instance.stubs(:can?).returns(false)
        end

        describe "a PUT to :update" do
          before { put :update, params: { user_id: @user.id } }
          it('responds with forbidden') { assert_response :forbidden }
        end
      end

      describe 'with permissions' do
        before do
          User.any_instance.stubs(:can?).returns(true)
        end

        describe "without a user" do
          before { put :update, params: { user_id: 999 } }

          it('responds with not_found') { assert_response :not_found }
        end

        describe "with valid attributes" do
          let(:params) { { user_id: @user.id, password: 'abcdef', previous_password: '123456', brand_id: '123' } }

          before do
            User.any_instance.expects(:active_brand_id=).with(123)
            put :update, params: params
          end

          it('responds with ok') { assert_response :ok }
        end

        describe "with invalid previous password" do
          let(:params) { { user_id: @user.id, password: 'abcdef', previous_password: 'notvalid' } }
          before { put :update, params: params }

          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
        end

        describe "with password that is too long" do
          let(:params) { { user_id: @user.id, password: '1' * 142 } }
          before { put :update, params: params }

          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
        end

        describe "with invalid forwarding number" do
          let(:params) { { user_id: @user.id, password: 'abcdef', previous_password: '123456' } }
          let(:uvfi) { UserVoiceForwardingIdentity.create(account_id: @user.account_id, user: @user, value: '+16195558175') } # valid number

          before do
            uvfi.update_column('value', '+6195558175') # force bad number
            @user.reload
            put :update, params: params
          end

          it 'succeeds' do
            assert_equal response.code.to_i, 200
          end
        end

        describe "same password" do
          let(:params) { { user_id: @user.id, password: '123456', previous_password: '123456' } }

          describe "without password history" do
            before { put :update, params: params }
            it('responds with ok') { assert_response :ok }
          end

          describe "with password history" do
            before do
              @user.security_policy.class.any_instance.
                stubs(:has_password_history?).returns(true)

              put :update, params: params
            end

            should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
          end
        end
      end
    end

    describe "a GET to :requirements" do
      before do
        get :requirements, params: { user_id: users(:minimum_search_user).id }
      end

      it "returns an array of password requirements" do
        get :requirements, params: { user_id: users(:minimum_search_user).id }
        assert_equal '{"requirements":["must be at least 5 characters","must be fewer than 128 characters","must be different from email address"]}',
          @response.body
      end
    end

    describe "a POST to :reset" do
      before do
        @deliveries = ActionMailer::Base.perform_deliveries
        ActionMailer::Base.perform_deliveries = true
      end

      after { ActionMailer::Base.perform_deliveries = @deliveries }

      describe "different user" do
        before do
          post :reset, params: { user_id: users(:minimum_search_user).id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "same user" do
        before do
          post :reset, params: { user_id: @user.id }
        end

        it 'should deliver email for password reset' do
          assert_equal 1, ActionMailer::Base.deliveries.size
          assert_equal "minimum password reset", ActionMailer::Base.deliveries.first.subject
        end

        it('responds with ok') { assert_response :ok }
      end
    end
  end

  as_an_agent do
    describe "without permission" do
      before do
        User.any_instance.stubs(:can?).returns(false)
      end

      describe "a POST to :create" do
        before { post :create, params: { user_id: @user.id } }
        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "a POST to :reset" do
        before { post :reset, params: { user_id: @user.id } }
        it('responds with forbidden') { assert_response :forbidden }
      end
    end
  end

  as_an_admin do
    describe "with permission" do
      before do
        User.any_instance.stubs(:can?).returns(true)
      end

      describe "a POST to :create" do
        describe "with a valid password" do
          let(:params) { { password: 'abcdef', user_id: @user.id } }
          before { post :create, params: params }
          it('responds with ok') { assert_response :ok }
        end

        describe "with an invalid password" do
          let(:params) { { user_id: @user.id, password: "1" } }
          before { post :create, params: params }
          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
        end

        describe "with too long password" do
          let(:params) { { user_id: @user.id, password: '1' * 142 } }
          before { put :create, params: params }

          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
        end

        describe "without a user" do
          before { post :create, params: { user_id: 999 } }
          it('responds with not_found') { assert_response :not_found }
        end
      end

      describe "a POST to :reset" do
        before do
          @deliveries = ActionMailer::Base.perform_deliveries
          ActionMailer::Base.perform_deliveries = true
        end
        after { ActionMailer::Base.perform_deliveries = @deliveries }

        describe "without a user" do
          before { post :reset, params: { user_id: 999 } }
          it('responds with not_found') { assert_response :not_found }
        end

        describe "with a user" do
          before do
            post :reset, params: { user_id: @user.id }
          end

          it 'should deliver email for password reset' do
            assert_equal 1, ActionMailer::Base.deliveries.size
            assert_equal "minimum password reset", ActionMailer::Base.deliveries.first.subject
          end

          it('responds with ok') { assert_response :ok }
        end

        describe "with a user with specific brand" do
          before do
            @brand = FactoryBot.create(:brand, account_id: @account.id, subdomain: "wombats")
            post :reset, params: { user_id: @user.id, brand_id: @brand.id }
          end

          it 'should deliver email for password reset with correct brand for the user' do
            assert_equal 1, ActionMailer::Base.deliveries.size
            ActionMailer::Base.deliveries.last.joined_bodies.must_include "https://wombats.zendesk-test.com/password/reset/"
          end

          it('responds with ok') { assert_response :ok }
        end

        describe "with a user without a password" do
          before do
            @user.update_attribute(:crypted_password, '')
            ActionMailer::Base.deliveries = [] # To remove email sent after a password is changed
            post :reset, params: { user_id: @user.id }
          end

          it 'should deliver email for password reset' do
            assert_equal 1, ActionMailer::Base.deliveries.size
            assert_equal "minimum password reset", ActionMailer::Base.deliveries.first.subject
          end

          it('responds with ok') { assert_response :ok }
        end
      end
    end
  end

  describe "I18n uses a translation locale" do
    let(:params) { { user_id: @user.id, password: "1" } }
    let(:portuguese) { translation_locales(:brazilian_portuguese) }

    before do
      login(:minimum_agent)
      User.any_instance.stubs(:can?).returns(true)
      I18n.stubs(:locale).returns(portuguese)
    end

    it "for an attribute error" do
      post :create, params: params
      response.body.must_include I18n.t('activerecord.attributes.error_presenter.password', locale: portuguese)
    end
  end
end
