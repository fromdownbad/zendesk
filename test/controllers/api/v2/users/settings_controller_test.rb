require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Users::SettingsController do
  extend Api::Presentation::TestHelper
  fixtures :users, :role_settings, :rules

  def self.should_update_setting(setting, value)
    describe "with values for whitelisted param: #{setting}" do
      before { put :update, params: { settings: { lotus: { setting.to_sym => value } } } }
      should_change("a setting") { @user.settings.send(setting.to_sym) }
      should_use_presenter Api::V2::Users::SettingsPresenter
    end
  end

  before do
    accept :json
    stub_request(:post, %r{.*/revere[^\/]*/api/internal/subscribers.json})
    stub_request(:delete, %r{.*/revere.*/api/internal/accounts/.*/subscribers/.*.json\?alert_type=EmailAlert})
  end

  with_options(controller: "api/v2/users/settings") do |request|
    request.should_route :get, "/api/v2/users/me/settings", action: "show"
    request.should_route :put, "/api/v2/users/me/settings", action: "update"
  end

  as_an_agent do
    describe "a GET to :show" do
      before { get :show }
      should_use_presenter Api::V2::Users::SettingsPresenter
    end

    should_update_setting :show_welcome_dialog,         false
    should_update_setting :show_feature_notifications,  false
    should_update_setting :show_hc_onboarding_tooltip,  false
    should_update_setting :show_options_move_tooltip,   false
    should_update_setting :show_filter_options_tooltip, false
    should_update_setting :show_user_nav_tooltip,       false
    should_update_setting :device_notification,         true
    should_update_setting :show_insights_onboarding,    true
    should_update_setting :show_color_branding_tooltip, false
    should_update_setting :show_new_search_tooltip,     false
    should_update_setting :show_onboarding_modal,       false
    should_update_setting :show_get_started_animation,  false
    should_update_setting :show_prediction_satisfaction_dashboard, false
    should_update_setting :show_hc_product_tray_tooltip, false
    should_update_setting :show_google_apps_onboarding, false
    should_update_setting :show_public_comment_warning, false
    should_update_setting :show_multiple_ticket_forms_tutorial, false
    should_update_setting :show_first_comment_private_tooltip, false
    should_update_setting :revere_subscription, true
    should_update_setting :show_enable_google_apps_modal, false
    should_update_setting :show_zero_state_tour_ticket, false
    should_update_setting :quick_assist_first_dismissal, true
    should_update_setting :show_help_panel_intro_tooltip, false
    should_update_setting :show_agent_workspace_onboarding, false
    should_update_setting :has_seen_channel_switching_onboarding, true
    should_update_setting :show_new_comment_filter_tooltip, false
    should_update_setting :show_answer_bot_dashboard_onboarding, false
    should_update_setting :show_answer_bot_trigger_onboarding, false
    should_update_setting :show_answer_bot_lotus_onboarding, false
    should_update_setting :show_manual_start_translation_tooltip, false
    should_update_setting :show_manual_stop_translation_tooltip, false
    should_update_setting :show_composer_will_translate_tooltip, false
    should_update_setting :show_composer_wont_translate_tooltip, false

    # Non-lotus settings
    describe "shared_views_order" do
      describe_with_arturo_disabled :user_level_shared_view_ordering do
        it "does not update the setting" do
          id = @user.shared_views.last.id
          put :update, params: { settings: { shared_views_order: [id] } }
          assert_response :ok
          @user.texts.shared_views_order.wont_equal [id]
        end
      end

      describe_with_arturo_enabled :user_level_shared_view_ordering do
        before do
          @user.shared_views << rules(:view_your_unsolved_tickets) unless @user.shared_views.any?
        end

        it "updates the setting" do
          id = @user.shared_views.last.id
          put :update, params: { settings: { shared_views_order: [id] } }
          assert_response :ok
          @user.texts.shared_views_order.must_equal [id]
        end

        it "accepts an array of ints for shared_views_order" do
          put :update, params: { settings: { shared_views_order: [@user.shared_views.last.id] } }
          assert_response :ok
        end
      end
    end

    describe 'revere_subscription' do
      describe 'starts false' do
        before do
          @user.settings.revere_subscription = false
          @user.settings.save!
        end

        it "only calls API when included in settings" do
          Zendesk::Revere::ApiClient.any_instance.expects(:update_subscription).never
          put :update, params: { settings: { lotus: { show_google_apps_onboarding: true } } }
        end

        it 'set to false, no api' do
          Zendesk::Revere::ApiClient.any_instance.expects(:update_subscription).never
          put :update, params: { settings: { lotus: { revere_subscription: false } } }
        end

        it 'set to true, call api' do
          Zendesk::Revere::ApiClient.any_instance.expects(:update_subscription).once
          put :update, params: { settings: { lotus: { revere_subscription: true } } }
        end
      end

      describe 'starts true' do
        before do
          @user.settings.revere_subscription = true
          @user.settings.save!
        end

        it 'set to false, call api' do
          Zendesk::Revere::ApiClient.any_instance.expects(:update_subscription).once
          put :update, params: { settings: { lotus: { revere_subscription: false } } }
        end

        it 'set to true, no api' do
          Zendesk::Revere::ApiClient.any_instance.expects(:update_subscription).never
          put :update, params: { settings: { lotus: { revere_subscription: true } } }
        end
      end
    end
  end

  as_an_admin do
    should_update_setting :show_color_branding_tooltip, false
  end
end
