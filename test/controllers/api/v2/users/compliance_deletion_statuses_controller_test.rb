require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Users::ComplianceDeletionStatusesController do
  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    accept :json
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index, { user_id: 1 }]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index, { user_id: 1 }]
  end

  describe 'users that can log in' do
    let(:deleted_user)     { users(:minimum_end_user2) }
    let(:inactive_user)    { users(:inactive_user) }
    let(:admin)            { users(:minimum_admin) }
    let(:non_deleted_user) { users(:minimum_end_user) }

    before do
      Zendesk::PushNotifications::Gdpr::GdprSnsPublisher.any_instance.stubs(:publish).returns(true)
      # Setup Account to have at least one soft-deleted user
      deleted_user.current_user = admin
      deleted_user.delete!
      deleted_user.mark_for_ultra_deletion!(executer: admin)
    end

    as_an_admin do
      describe '#index' do
        describe "user's compliance_deletion_status setting" do
          let(:status) do
            deleted_user.compliance_deletion_statuses.first
          end

          let(:account) { status.account }

          let(:classic_complete) do
            feedback = status.compliance_deletion_feedback.where(application: ComplianceDeletionStatus::CLASSIC).first
            feedback.complete!
            feedback
          end

          let(:talk_complete) do
            feedback = status.compliance_deletion_feedback.where(application: ComplianceDeletionStatus::TALK).first
            feedback.complete!
            feedback
          end

          let(:pigeon_pending) do
            status.compliance_deletion_feedback.where(application: ComplianceDeletionStatus::PIGEON).first
          end

          let(:text_redo) do
            feedback = status.compliance_deletion_feedback.where(application: ComplianceDeletionStatus::TEXT).first
            feedback.redo!
            feedback
          end

          let(:all_feedback) { [classic_complete, talk_complete, pigeon_pending, text_redo] }

          describe 'when no compliance_deletion_status record exists' do
            before do
              get :index, params: { user_id: inactive_user.id }
            end

            it 'returns empty list' do
              assert_empty JSON.parse(response.body)['compliance_deletion_statuses']
            end
          end

          describe 'without params' do
            describe 'when feedback exists' do
              before do
                all_feedback

                get :index, params: { user_id: deleted_user.id }
              end

              should respond_with :success

              it 'returns compliance_deletion_status initial request' do
                initial_request = JSON.parse(response.body)['compliance_deletion_statuses'].first

                assert_equal deleted_user.id, initial_request['user_id']
                assert_equal admin.id, initial_request['executer_id']
                assert_equal ComplianceDeletionStatus::REQUEST_DELETION, initial_request['action']
                assert_equal ComplianceDeletionStatus::ALL_APPLICATONS, initial_request['application']
              end

              it 'returns all feedback' do
                json_response = JSON.parse(response.body)['compliance_deletion_statuses']
                assert_equal 1 + ComplianceDeletionFeedback::VALID_APPLICATIONS.count, json_response.count # initial request + feedback

                all_feedback.each do |feedback|
                  assert json_response.find { |f| f['action'] == feedback.state && f['application'] == feedback.application }
                end
              end
            end
          end

          describe 'with params[:application]' do
            before do
              classic_complete
            end

            it 'returns compliance_deletion_status data for a specific application' do
              get :index, params: { user_id: deleted_user.id, application: 'support' }

              json_response = JSON.parse(response.body)['compliance_deletion_statuses']
              assert_equal 1, json_response.count
              feedback = json_response.first

              assert_equal deleted_user.id, feedback['user_id']
              assert_nil feedback['executer_id']
              assert_equal ComplianceDeletionFeedback::COMPLETE, feedback['action']
              assert_equal ComplianceDeletionStatus::CLASSIC, feedback['application']
            end

            it 'returns all feedback if application is invalid' do
              # existing behavior is to treat an invalid application like the parameter is missing
              get :index, params: { user_id: deleted_user.id, application: 'invalid-app' }

              json_response = JSON.parse(response.body)['compliance_deletion_statuses']
              assert_equal 1 + ComplianceDeletionFeedback::VALID_APPLICATIONS.count, json_response.count # initial request and all feedback
            end
          end

          describe 'with a user that has not been deleted' do
            it 'returns error' do
              get :index, params: { user_id: non_deleted_user.id }

              assert_equal response.status, 404
            end
          end
        end
      end
    end

    as_a_subsystem_user(user: 'embeddable', account: :minimum) do
      describe "with a recent start_time" do
        before do
          get :index, params: { user_id: deleted_user.id, format: :json }
        end

        should respond_with :success

        it 'returns compliance_deletion_status data' do
          assert_equal deleted_user.id, JSON.parse(response.body)['compliance_deletion_statuses'].first['user_id']
          assert_equal admin.id, JSON.parse(response.body)['compliance_deletion_statuses'].first['executer_id']
          assert_equal ComplianceDeletionStatus::REQUEST_DELETION, JSON.parse(response.body)['compliance_deletion_statuses'].first['action']
          assert_equal ComplianceDeletionStatus::ALL_APPLICATONS, JSON.parse(response.body)['compliance_deletion_statuses'].first['application']
        end
      end
    end
  end
end
