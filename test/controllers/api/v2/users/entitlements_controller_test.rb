require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Users::EntitlementsController do
  extend Api::V2::TestHelper

  fixtures :accounts, :users, :role_settings

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:current_user) { @controller.send(:current_user) }

  let(:entitlements_res) { { 'explore' => 'viewer', 'chat' => 'agent' } }
  let(:entitlements_update) { { 'explore' => 'viewer' } }

  let(:entitlements_full_update) { { 'explore' => { 'name' => 'viewer', 'is_active' => true } } }
  let(:staff_service_full_update_response) do
    {
        'chat' => { 'id' => 456, 'name' => 'agent', 'is_active' => 'true' },
        'explore' => { 'id' => 123, 'name' => 'viewer', 'is_active' => 'true' },
    }
  end

  let(:full_entitlements_res) do
    {
      'entitlements' => {
        'explore' => { 'name' => 'viewer', 'is_active' => 'true' },
        'chat' => { 'name' => 'agent', 'is_active' => 'true' },
        'talk' => { 'is_active' => false }
      }
    }
  end

  let(:get_entitlements) do
    Zendesk::StaffClient.any_instance.expects(:get_entitlements!).with(user.id)
  end

  let(:update_entitlements) do
    Zendesk::StaffClient.any_instance.expects(:update_entitlements!).with(user.id, entitlements_update, current_user.id)
  end

  let(:get_full_entitlements) do
    Zendesk::StaffClient.any_instance.expects(:get_full_entitlements!).with(user.id)
  end
  let(:update_full_entitlements) do
    Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).with(user.id, entitlements_full_update, skip_validation: false)
  end

  with_options(controller: 'api/v2/users/entitlements') do |request|
    request.should_route :get, '/api/v2/users/1/entitlements', action: 'show', user_id: 1
    request.should_route :put, '/api/v2/users/1/entitlements', action: 'update', user_id: 1
    request.should_route :get, "/api/v2/users/1/entitlements/full", action: "full", user_id: "1"
  end

  before do
    accept :json
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :update, user_id: 1]
    should_be_unauthorized [:put, :update, user_id: 1]
  end

  as_an_end_user do
    should_be_forbidden [:get, :update, user_id: 1]
    should_be_forbidden [:put, :update, user_id: 1]
  end

  as_an_agent do
    describe "#show" do
      describe "when the staff service client raises InternalServerError" do
        it "returns 500" do
          get_entitlements.raises(Kragle::InternalServerError)
          assert_raises Kragle::InternalServerError do
            get :show, params: { user_id: user.id }
            assert_response :internal_server_error
          end
        end
      end

      describe "when the staff service client raises ResourceNotFound" do
        it "returns 404" do
          get_entitlements.raises(Kragle::ResourceNotFound)
          get :show, params: { user_id: user.id }
          assert_response :not_found
        end
      end

      describe "when the staff service client returns the entitlements" do
        it "returns 200 with the entitlements" do
          get_entitlements.returns(entitlements_res)
          get :show, params: { user_id: user.id }
          assert_response :ok
          assert_equal entitlements_res, JSON.parse(@response.body)['entitlements']
        end
      end
    end

    describe "#full" do
      describe "when the staff service client raises InternalServerError" do
        it "returns 500" do
          get_full_entitlements.raises(Kragle::InternalServerError)
          assert_raises Kragle::InternalServerError do
            get :full, params: { user_id: user.id }
            assert_response :internal_server_error
          end
        end
      end

      describe "when the staff service client raises ResourceNotFound" do
        it "returns 404" do
          get_full_entitlements.raises(Kragle::ResourceNotFound)
          get :full, params: { user_id: user.id }
          assert_response :not_found
        end
      end

      it "returns 200 with entitlements" do
        get_full_entitlements.returns(staff_service_full_update_response)
        get :full, params: { user_id: user.id }
        assert_response :ok
        assert_equal full_entitlements_res, JSON.parse(@response.body)
      end
    end

    describe "#update" do
      it "returns 403" do
        update_entitlements.never
        put :update, params: { user_id: user.id, entitlements: entitlements_update }
        assert_response :forbidden
      end
    end

    describe "#update_full" do
      it "returns 403" do
        update_full_entitlements.never
        put :update_full, params: { user_id: user.id, entitlements: entitlements_full_update }
        assert_response :forbidden
      end
    end
  end

  as_an_admin do
    describe "#show" do
      describe "when the staff service client raises InternalServerError" do
        it "returns 500" do
          get_entitlements.raises(Kragle::InternalServerError)
          assert_raises Kragle::InternalServerError do
            get :show, params: { user_id: user.id }
            assert_response :internal_server_error
          end
        end
      end

      describe "when the staff service client raises ResourceNotFound" do
        it "returns 404" do
          get_entitlements.raises(Kragle::ResourceNotFound)
          get :show, params: { user_id: user.id }
          assert_response :not_found
        end
      end

      describe "when the staff service client returns the entitlements" do
        it "returns 200 with the entitlements" do
          get_entitlements.returns(entitlements_res)
          get :show, params: { user_id: user.id }
          assert_response :ok
          assert_equal entitlements_res, JSON.parse(@response.body)['entitlements']
        end
      end
    end

    describe "#update" do
      describe "when attempting to update a non-explore entitlement" do
        it "returns 400" do
          update_entitlements.never
          put :update, params: { user_id: user.id, entitlements: { 'chat': 'admin' } }
          assert_response :bad_request
        end
      end

      describe "when the staff service client raises InternalServerError" do
        it "returns 500" do
          update_entitlements.raises(Kragle::InternalServerError)
          assert_raises Kragle::InternalServerError do
            put :update, params: { user_id: user.id, entitlements: entitlements_update }
            assert_response :internal_server_error
          end
        end
      end

      describe "when the staff service client raises ResourceNotFound" do
        it "returns 404" do
          update_entitlements.raises(Kragle::ResourceNotFound)
          put :update, params: { user_id: user.id, entitlements: entitlements_update }
          assert_response :not_found
        end
      end

      describe "when the staff service client raises UnprocessableEntity" do
        it "returns 422" do
          res = Faraday::Response.new(status: 422, body: {
            "errors" => [
              {
                "title" => "Entitlements Product Missing Error",
                "detail" => 'Product "explore" is not enabled'
              }
            ]
          })
          update_entitlements.raises(Kragle::UnprocessableEntity.new('422 Unprocessable Entity', res))
          put :update, params: { user_id: user.id, entitlements: entitlements_update }
          assert_response :unprocessable_entity
          assert_equal res.body['errors'], JSON.parse(@response.body)['errors']
        end
      end

      describe "when the staff service client raises BadRequest" do
        it "returns 400" do
          res = Faraday::Response.new(status: 400, body: {
            "errors" => [
              {
                "title" => "Bad Request Error",
                "detail" => "The following product entitlement(s) are invalid: 'explore: foobar'."
              }
            ]
          })
          update_entitlements.raises(Kragle::BadRequest.new('400 Bad Request', res))
          put :update, params: { user_id: user.id, entitlements: entitlements_update }
          assert_response :bad_request
          assert_equal res.body['errors'], JSON.parse(@response.body)['errors']
        end
      end

      describe "when the staff service returns 200" do
        it "returns 200 with the entitlements" do
          update_entitlements.returns(entitlements_res)
          put :update, params: { user_id: user.id, entitlements: entitlements_update }
          assert_response :ok
          assert_equal entitlements_res, JSON.parse(@response.body)['entitlements']
        end
      end

      describe "when attempting to update the owner" do
        let(:user) { users(:minimum_admin) }

        it "returns 200 with the entitlements" do
          update_entitlements.returns(entitlements_res)
          put :update, params: { user_id: user.id, entitlements: entitlements_update }
          assert_response :ok
          assert_equal entitlements_res, JSON.parse(@response.body)['entitlements']
        end
      end
    end

    describe "#update_full" do
      describe "when attempting to update a non-explore entitlement" do
        it "returns 400" do
          update_full_entitlements.never
          put :update_full, params: { user_id: user.id, entitlements: { 'chat': { name => 'admin', 'is_active' => true } } }
          assert_response :bad_request
        end
      end

      describe "when the staff service client raises InternalServerError" do
        it "returns 500" do
          update_full_entitlements.raises(Kragle::InternalServerError)
          assert_raises Kragle::InternalServerError do
            put :update_full, params: { user_id: user.id, entitlements: entitlements_full_update }
            assert_response :internal_server_error
          end
        end
      end

      describe "when the staff service client raises ResourceNotFound" do
        it "returns 404" do
          update_full_entitlements.raises(Kragle::ResourceNotFound)
          put :update_full, params: { user_id: user.id, entitlements: entitlements_full_update }
          assert_response :not_found
        end
      end

      describe "when the staff service client raises UnprocessableEntity" do
        it "returns 422" do
          res = Faraday::Response.new(status: 422, body: {
            "errors" => [
              {
                "title" => "Entitlements Product Missing Error",
                "detail" => 'Product "explore" is not enabled'
              }
            ]
          })
          update_full_entitlements.raises(Kragle::UnprocessableEntity.new('422 Unprocessable Entity', res))
          put :update_full, params: { user_id: user.id, entitlements: entitlements_full_update }
          assert_response :unprocessable_entity
          assert_equal res.body['errors'], JSON.parse(@response.body)['errors']
        end
      end

      describe "when the staff service client raises BadRequest" do
        it "returns 400" do
          res = Faraday::Response.new(status: 400, body: {
            "errors" => [
              {
                "title" => "Bad Request Error",
                "detail" => "The following product entitlement(s) are invalid: 'explore: foobar'."
              }
            ]
          })
          update_full_entitlements.raises(Kragle::BadRequest.new('400 Bad Request', res))
          put :update_full, params: { user_id: user.id, entitlements: entitlements_full_update }
          assert_response :bad_request
          assert_equal res.body['errors'], JSON.parse(@response.body)['errors']
        end
      end

      describe "when the staff service returns 200" do
        it "returns 200 with the entitlements" do
          update_full_entitlements.returns(staff_service_full_update_response)
          put :update_full, params: { user_id: user.id, entitlements: entitlements_full_update }
          assert_response :ok
          assert_equal full_entitlements_res, JSON.parse(@response.body)
        end
      end

      describe "when attempting to update the owner" do
        let(:user) { users(:minimum_admin) }

        it "returns 200 with the entitlements" do
          update_full_entitlements.returns(staff_service_full_update_response)
          put :update_full, params: { user_id: user.id, entitlements: entitlements_full_update }
          assert_response :ok
          assert_equal full_entitlements_res, JSON.parse(@response.body)
        end
      end
    end
  end

  as_an_owner do
    describe "#update" do
      describe "when attempting to update the owner" do
        let(:user) { users(:minimum_admin) }

        it "returns 200 with the entitlements" do
          update_entitlements.returns(entitlements_res)
          put :update, params: { user_id: user.id, entitlements: entitlements_update }
          assert_response :ok
          assert_equal entitlements_res, JSON.parse(@response.body)['entitlements']
        end
      end
    end

    describe "#update_full" do
      describe "when attempting to update the owner" do
        let(:user) { users(:minimum_admin) }

        it "returns 200 with the entitlements" do
          update_full_entitlements.returns(staff_service_full_update_response)
          put :update_full, params: { user_id: user.id, entitlements: entitlements_full_update }
          assert_response :ok
          assert_equal full_entitlements_res, JSON.parse(@response.body)
        end
      end
    end
  end
end
