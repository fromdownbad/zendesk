require_relative '../../../../support/test_helper'
require_relative '../../../../support/zopim_test_helper'
require_relative '../../../../support/entitlement_test_helper'
require_relative '../../../../support/chat_phase_3_helper'
require_relative '../../../../support/suite_test_helper'

SingleCov.covered!

describe Api::V2::Users::ZopimIdentityController do
  extend Api::Presentation::TestHelper

  include ZopimTestHelper
  include EntitlementTestHelper
  include ChatPhase3Helper
  include SuiteTestHelper

  fixtures :all

  let(:patch_status) { 200 }
  let(:get_status) { 200 }

  before do
    Zopim::AgentSyncJob.stubs(:work)
    Account.any_instance.stubs(has_chat_permission_set?: true)
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(
      is_serviceable?: true,
      ensure_not_phase_four: nil
    )
    ZopimIntegration.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      ensure_not_phase_four: nil
    )

    ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.stubs(:enqueue)
    ZopimIntegration.stubs(where: stub(:first_or_create!))
    Zopim::Reseller.client.stubs(
      create_account!: Hashie::Mash.new(id: 1),
      account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
      create_account_agent!: stub(id: 1),
      find_accounts!: [],
      delete_account_agent!: true
    )

    stub_staff_service_patch_request('minimum', patch_status)
    stub_account_service_sync

    accept :json
  end

  with_options(controller: 'api/v2/users/zopim_identity') do |request|
    request.should_route :post,   '/api/v2/users/1/zopim_identity', action: 'create',  user_id: 1
    request.should_route :get,    '/api/v2/users/1/zopim_identity', action: 'show',    user_id: 1
    request.should_route :put,    '/api/v2/users/1/zopim_identity', action: 'update',  user_id: 1
    request.should_route :delete, '/api/v2/users/1/zopim_identity', action: 'destroy', user_id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized :show, :create, :destroy, :update, add_to_all: { user_id: 1 }
  end

  as_an_end_user do
    should_be_forbidden :create, add_to_all: { user_id: 1 }

    describe "#show" do
      before do
        get :show, params: { user_id: 1 }
      end

      should respond_with :forbidden
    end

    describe "#udpate" do
      before do
        put :update, params: { user_id: 1 }
      end

      should respond_with :forbidden
    end

    describe "#destroy" do
      before do
        delete :destroy, params: { user_id: 1 }
      end

      should respond_with :forbidden
    end
  end

  as_an_agent do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }
    let(:zendesk_agent) do
      agent = FactoryBot.create(:agent,
        account: current_account,
        name: "Double Agent")
      User.any_instance.stubs(:zopim_email).returns("test")
      setup_zopim_identity agent
      agent
    end
    let(:zopim_owner) { current_account.owner }
    let(:zendesk_only_agent) do
      current_account.users.find do |u|
        u.is_agent? && (u != current_user) && !u.zopim_identity
      end
    end
    let(:error) { JSON.parse(@response.body).symbolize_keys }

    before do
      setup_zopim_subscription(current_account)
      setup_zopim_identity(current_user)
    end

    describe "#show" do
      describe "current user zopim agent info" do
        before do
          get :show, params: { user_id: current_user.id }
        end

        should respond_with :success

        should_use_presenter Api::V2::Users::ZopimIdentityPresenter
      end

      describe "another agent's zopim identity" do
        before do
          get :show, params: { user_id: zendesk_agent.id }
        end

        should respond_with :success

        should_use_presenter Api::V2::Users::ZopimIdentityPresenter
      end

      describe "when the candidate user does not exist" do
        before do
          get :show, params: { user_id: 0 }
        end

        should respond_with :not_found
      end
    end

    describe "#create" do
      before do
        post :create, params: { user_id: zendesk_only_agent.id }
      end

      should respond_with :forbidden
    end

    describe "#update" do
      describe "another agents display name" do
        before do
          put :update, params: { user_id: zendesk_agent.id, zopim_identity: { display_name: "changing name" } }
        end

        should respond_with :unprocessable_entity
        it "produces the error 'PermissionRequired'" do
          expected = {
            error: "RecordInvalid",
            description: "Record validation errors",
            details: {
              "base" => [
                { "description" => "PermissionRequired" }
              ]
            }
          }
          assert_equal expected, error
        end
      end

      describe "the owner's display name" do
        before do
          put :update, params: { user_id: zopim_owner.id, zopim_identity: { display_name: "changing name" } }
        end

        should respond_with :unprocessable_entity
        it "produces the error 'PermissionRequired'" do
          expected = {
            error: "RecordInvalid",
            description: "Record validation errors",
            details: {
              "base" => [
                { "description" => "PermissionRequired" }
              ]
            }
          }
          assert_equal expected, error
        end
      end

      describe "my display name (the current_user)" do
        before do
          put(:update, params: { user_id: current_user.id, zopim_identity: { display_name: "changing name" } })
        end

        should respond_with :ok
      end
    end

    describe "#destroy" do
      before do
        delete :destroy, params: { user_id: zendesk_agent.id }
      end

      should respond_with :forbidden
    end
  end

  as_a_chat_agent do
    let(:current_account) { @controller.send(:current_account) }

    let(:current_user) { @controller.send(:current_user) }

    let(:zendesk_agent) do
      agent = FactoryBot.create(:agent,
        account: current_account,
        name: "Double Agent")
      User.any_instance.stubs(:zopim_email).returns("test")
      setup_zopim_identity agent
      agent
    end

    let(:zendesk_only_agent) do
      current_account.users.find do |u|
        u.is_agent? && (u != current_user) && !u.zopim_identity
      end
    end

    let(:error) { JSON.parse(@response.body).symbolize_keys }

    describe "#show" do
      describe "when everything is okay" do
        before do
          get :show, params: { user_id: current_user.id }
        end

        should respond_with :success
      end

      describe "when the candidate user does not exist" do
        before do
          get :show, params: { user_id: 0 }
        end

        should respond_with :not_found
      end
    end

    describe "#create" do
      before do
        post :create, params: { user_id: zendesk_only_agent.id }
      end

      should respond_with :forbidden
    end

    describe "#update" do
      describe "another agents display name" do
        before do
          put(:update, params: { user_id: zendesk_agent.id, zopim_identity: { display_name: "changing name" } })
        end

        should respond_with :unprocessable_entity
        it "produces the error 'PermissionRequired'" do
          expected = {
            error: "RecordInvalid",
            description: "Record validation errors",
            details: {
              "base" => [
                { "description" => "PermissionRequired" }
              ]
            }
          }
          assert_equal expected, error
        end
      end

      describe "my display name (the current_user chat only agent)" do
        before do
          put :update, params: { user_id: current_user.id, zopim_identity: { display_name: "changing name" } }
        end

        should respond_with :ok
      end
    end

    describe "#destroy" do
      before do
        delete :destroy, params: { user_id: zendesk_agent.id }
      end

      should respond_with :forbidden
    end
  end

  as_an_admin do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }
    let(:zendesk_agent) do
      FactoryBot.create(
        :agent,
        account: current_account,
        name: "Double Agent"
      )
    end
    let(:zopim_agent) do
      zopim_agent = FactoryBot.create(
        :agent,
        account: current_account,
        name: "Zopim Agent"
      )
      setup_zopim_identity(zopim_agent)
      zopim_agent
    end
    let(:zendesk_only_agent) do
      FactoryBot.create(
        :agent,
        account: current_account,
        name: "Zendesk Only Agent"
      )
    end
    let(:disabled_zopim_agent) do
      zopim_agent = FactoryBot.create(
        :agent,
        account: current_account,
        name: "Disabled Zopim Agent"
      )
      setup_zopim_identity(zopim_agent, is_enabled: false)
      zopim_agent
    end
    let(:end_user) { users(:minimum_end_user) }
    let(:error) { JSON.parse(@response.body).symbolize_keys }

    describe "and zopim account owner" do
      before do
        setup_zopim_subscription(current_account)
      end

      describe "#show" do
        describe "when everything is okay" do
          before do
            get :show, params: { user_id: current_account.owner.id }
          end

          should respond_with :success

          should_use_presenter Api::V2::Users::ZopimIdentityPresenter
        end

        describe "another agent's zopim identity" do
          before do
            get :show, params: { user_id: zopim_agent.id }
          end

          should respond_with :success

          should_use_presenter Api::V2::Users::ZopimIdentityPresenter
        end

        describe "another admin without a zopim identity" do
          before do
            get :show, params: { user_id: zendesk_only_agent.id }
          end

          should respond_with :not_found
        end

        describe "when the candidate user does not exist" do
          before do
            get :show, params: { user_id: 0 }
          end

          should respond_with :not_found
        end
      end

      describe "#create" do
        describe "without display_name parameter" do
          before do
            post :create, params: { user_id: zendesk_only_agent.id }
          end

          should respond_with :created

          it "assigns a zopim identity to the candidate user" do
            assert zendesk_only_agent.reload.zopim_identity.present?
          end

          it "sets the display name to user.name" do
            assert_equal "Zendesk Only Agent", zendesk_only_agent.reload.zopim_identity.display_name
          end
        end

        describe "with display_name parameter 'TONY' " do
          before do
            params = {
              user_id: zendesk_only_agent.id,
              zopim_identity: { display_name: "TONY" }
            }
            post :create, params: params
          end

          should respond_with :created

          it "assigns a zopim identity with display name set to TONY" do
            assert_equal "TONY", zendesk_only_agent.reload.zopim_identity.display_name
          end
        end

        describe "user who already has an enabled zopim_identity" do
          before do
            params = {
              user_id: zopim_agent.id
            }
            post :create, params: params
          end

          should respond_with :not_modified
        end

        describe "user who has a disabled zopim_identity" do
          before do
            post :create, params: { user_id: disabled_zopim_agent.id }
          end

          should respond_with :success

          it "enables the zopim_identity" do
            assert disabled_zopim_agent.reload.zopim_identity.is_enabled
          end
        end

        describe "user who is an end_user" do
          before do
            post :create, params: { user_id: end_user.id }
          end

          should respond_with :unprocessable_entity

          it "produces the error 'UserChatPrivilegeRequired'" do
            expected = {
              error: "RecordInvalid",
              description: "Record validation errors",
              details: {
                "base" => [{ "description" => "UserChatPrivilegeRequired" }]
              }
            }
            assert_equal expected, error
          end
        end

        describe "when max agent limit exceeded" do
          describe "with the addition of a new agent" do
            before do
              zopim_sub = current_account.zopim_subscription
              zopim_sub.update_column(:plan_type, "basic")
              zopim_sub.update_column(:max_agents, 1)
              post :create, params: { user_id: zendesk_only_agent.id }
            end

            should respond_with :unprocessable_entity
            it "produces the error 'TooManyAgents'" do
              expected = {
                error: "RecordInvalid",
                description: "Record validation errors",
                details: {
                  "base" => [{
                    "description" => "TooManyAgents",
                    "error"       => "MaxAgentExceeded"
                  }]
                }
              }
              assert_equal expected, error
            end
          end

          describe "when enabling an additional agent" do
            before do
              zagent    = disabled_zopim_agent
              zopim_sub = current_account.zopim_subscription
              zopim_sub.update_column(:plan_type, "basic")
              zopim_sub.update_column(:max_agents, 1)
              post :create, params: { user_id: zagent.id }
            end

            should respond_with :unprocessable_entity

            it "produces the error 'TooManyAgents'" do
              expected = {
                error: "RecordInvalid",
                description: "Record validation errors",
                details: {
                  "base" => [{
                    "description" => "TooManyAgents",
                    "error"       => "MaxAgentExceeded"
                  }]
                }
              }
              assert_equal expected, error
            end
          end
        end
      end

      describe "#update" do
        describe "another zopim agents display name" do
          before do
            put :update, params: { user_id: zopim_agent.id, zopim_identity: { display_name: "changing name" } }
          end

          should respond_with :ok
        end

        describe "my display name (the current_user who is an admin/owner)" do
          before do
            put :update, params: { user_id: current_account.owner.id, zopim_identity: { display_name: "changing name" } }
          end

          should respond_with :ok
        end
      end

      describe "#destroy" do
        describe "disable another zopim agent" do
          before do
            delete :destroy, params: { user_id: zopim_agent.id }
          end

          should respond_with :success

          it "should disable the zopim_agent" do
            refute zopim_agent.zopim_identity.reload.is_enabled?
          end
        end

        describe "disable the zopim owner" do
          before do
            delete :destroy, params: { user_id: current_account.owner.id }
          end

          should respond_with :unprocessable_entity

          it "produces the error 'CannotDisableZopimOwner'" do
            expected = {
              error: "RecordInvalid",
              description: "Record validation errors",
              details: {
                "base" => [{ "description" => "CannotDisableZopimOwner" }]
              }
            }
            assert_equal expected, error
          end
        end
      end
    end
  end

  as_a_subsystem_user(account: :minimum) do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }
    let(:account_owner) { current_account.owner }
    let(:zendesk_only_agent) do
      current_account.users.find do |u|
        u.name == "Not owner man"
      end
    end
    let(:end_user) { users(:minimum_end_user) }
    let(:zopim_agent) do
      zopim_agent = FactoryBot.create(:agent,
        account: current_account,
        name: "Zopim Agent")
      setup_zopim_identity(zopim_agent)
      zopim_agent
    end
    let(:error) { JSON.parse(@response.body).symbolize_keys }

    before do
      Zopim::Reseller.client.stubs(create_account_agent!: stub(id: 1))
      setup_zopim_subscription(current_account)
    end

    describe "#show" do
      describe "when everything is okay" do
        before do
          get :show, params: { user_id: account_owner.id }
        end

        should respond_with :success

        should_use_presenter Api::V2::Users::ZopimIdentityPresenter
      end

      describe "without a zopim identity" do
        before do
          get :show, params: { user_id: zendesk_only_agent.id }
        end

        should respond_with :not_found
      end

      describe "when the candidate user does not exist" do
        before do
          get :show, params: { user_id: 0 }
        end

        should respond_with :not_found
      end
    end

    describe "#create" do
      describe "without display_name parameter" do
        before do
          post :create, params: { user_id: zendesk_only_agent.id }
        end

        should respond_with :created

        it "assigns a zopim identity to the candidate user" do
          assert zendesk_only_agent.reload.zopim_identity.present?
        end

        it "sets the display name to user.name" do
          assert_equal "Not owner man", zendesk_only_agent.reload.zopim_identity.display_name
        end
      end

      describe "with display_name parameter 'TONY' " do
        before do
          params = {
            user_id: zendesk_only_agent.id,
            zopim_identity: { display_name: "TONY" }
          }
          post :create, params: params
        end

        should respond_with :created

        it "assigns a zopim identity with display name set to TONY" do
          assert_equal "TONY", zendesk_only_agent.reload.zopim_identity.display_name
        end
      end

      describe "user who already has zopim_identity" do
        before do
          params = {
            user_id: zopim_agent.id
          }
          post :create, params: params
        end

        should respond_with :not_modified
      end

      describe "user who is an end_user" do
        before do
          post :create, params: { user_id: end_user.id }
        end

        should respond_with :unprocessable_entity
        it "produces the error 'UserChatPrivilegeRequired'" do
          expected = {
            error: "RecordInvalid",
            description: "Record validation errors",
            details: {
              "base" => [{ "description" => "UserChatPrivilegeRequired" }]
            }
          }
          assert_equal expected, error
        end
      end
    end

    describe "#destroy" do
      describe "a zopim agent" do
        before do
          delete :destroy, params: { user_id: zopim_agent.id }
        end

        should respond_with :success

        it "should disable the zopim_agent" do
          refute zopim_agent.zopim_identity.reload.is_enabled?
        end
      end

      describe "the zopim owner" do
        before do
          delete :destroy, params: { user_id: account_owner.id }
        end

        should respond_with :unprocessable_entity

        it "produces the error 'CannotDisableZopimOwner'" do
          expected = {
            error: "RecordInvalid",
            description: "Record validation errors",
            details: {
              "base" => [{ "description" => "CannotDisableZopimOwner" }]
            }
          }
          assert_equal expected, error
        end
      end
    end
  end
end
