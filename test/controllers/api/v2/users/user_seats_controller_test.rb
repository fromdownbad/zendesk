require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::Users::UserSeatsController do
  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    ZendeskBillingCore::Zuora::Jobs::AddVoiceJob.stubs(:enqueue)
    stub_request(:post, %r{/voice_seat_lost.json})
    accept :json
  end

  with_options(controller: 'api/v2/users/user_seats') do |request|
    request.should_route :post, '/api/v2/users/1/user_seats', action: 'create', user_id: 1
    request.should_route :get, '/api/v2/users/1/user_seats', action: 'index', user_id: 1
    request.should_route :delete, '/api/v2/users/1/user_seats', action: 'destroy', user_id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized(
      [:get, :index, { user_id: 1 }],
      [:post, :create, { user_id: 1}],
      [:delete, :destroy, { user_id: 1 }],
      :all_user_seats,
      :voice_assignable_agents
    )
  end

  as_an_end_user do
    should_be_forbidden(
      [:get, :index, { user_id: 1 }],
      [:post, :create, { user_id: 1}],
      [:delete, :destroy, { user_id: 1 }],
      :all_user_seats,
      :voice_assignable_agents
    )
  end

  as_an_agent do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }
    let(:zendesk_agent) do
      FactoryBot.create(:agent,
        account: current_account,
        name: "Double Agent")
    end

    describe "#index" do
      describe "user's user_seat setting" do
        before do
          FactoryBot.create(:voice_subscription, account: current_account, plan_type: 0, max_agents: 1)
          current_user.user_seats.where(account_id: current_account.id, seat_type: 'voice').create
          get :index, params: { user_id: current_user.id }
        end

        should respond_with :success

        it 'returns user_seat data' do
          assert_equal response.body, {
            user_seats: [{ user_id: current_user.id, seat_type: 'voice', role: 0 }]
          }.to_json
        end
      end

      describe "an agent without a user_seat" do
        before do
          get :index, params: { user_id: zendesk_agent.id }
        end

        should respond_with :success

        it 'returns an empty array' do
          assert_equal response.body, { user_seats: [] }.to_json
        end
      end

      describe "when the user does not exist" do
        before do
          get :index, params: { user_id: 0 }
        end

        should respond_with :not_found
      end
    end

    should_be_forbidden(
      [:post, :create, { user_id: 1 }],
      [:delete, :destroy, { user_id: 1 }],
      :all_user_seats,
      :voice_assignable_agents
    )
  end

  as_an_admin do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }
    let(:error) { JSON.parse(@response.body).symbolize_keys }
    let!(:zendesk_agent) do
      FactoryBot.create(:agent,
        account: current_account,
        name: "Double Agent")
    end

    describe "#index" do
      describe "when a user_seat exists" do
        before do
          get :index, params: { user_id: current_account.owner.id }
        end

        should respond_with :success
      end

      describe "an agent with a voice_seat" do
        before do
          get :index, params: { user_id: zendesk_agent.id }
        end

        should respond_with :success
      end

      describe "an agent without a voice_seat" do
        before do
          get :index, params: { user_id: zendesk_agent.id }
        end

        should respond_with :success
      end

      describe "when the user does not exist" do
        before do
          get :index, params: { user_id: 0 }
        end

        should respond_with :not_found
      end
    end

    describe "#create" do
      describe "for voice when max agent limit exceeded" do
        it "produces an error with correct description when max_agents is not 1" do
          FactoryBot.create(:voice_subscription, account: current_account, plan_type: 0, max_agents: 0)
          post :create, params: { user_id: zendesk_agent.id, user_seat: { seat_type: 'voice' } }

          assert_response :unprocessable_entity
          expected = {
            error: "RecordInvalid",
            description: "Record validation errors",
            details: {
              "base" => [{ "description" => I18n.t('txt.job.user_bulk_update_job.talk_seats_limit_reached.other', count: 0) }]
            }
          }
          assert_equal expected, error
        end

        it "produces an error with correct description when max_agents is 1" do
          FactoryBot.create(:voice_subscription, account: current_account, plan_type: 0, max_agents: 1)
          FactoryBot.create(:voice_user_seat, account: current_account, user: zendesk_agent, seat_type: 'voice')
          zendesk_agent_1 = FactoryBot.create(:agent, account: current_account, name: "Second Agent")
          post :create, params: { user_id: zendesk_agent_1.id, user_seat: { seat_type: 'voice' } }

          assert_response :unprocessable_entity
          expected = {
            error: "RecordInvalid",
            description: "Record validation errors",
            details: {
              "base" => [{ "description" => I18n.t('txt.job.user_bulk_update_job.talk_seats_limit_reached.one', count: 0) }]
            }
          }
          assert_equal expected, error
        end
      end

      describe 'talk entitlement sync' do
        it 'performs entitlement sync' do
          Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.any_instance.expects(:perform!)
          post :create, params: { user_id: current_user.id, user_seat: { seat_type: 'voice' } }
        end
      end
    end

    describe '#all_user_seats' do
      describe 'successfully' do
        before do
          FactoryBot.create(:voice_subscription, account: current_account, plan_type: 0, max_agents: 10)
        end

        describe 'responds with :created' do
          before do
            post :all_user_seats, params: { user_seat: { seat_type: 'voice' } }
          end

          should respond_with :created
        end

        it "assigns voice seat to the user" do
          post :all_user_seats, params: { user_seat: { seat_type: 'voice' } }
          assert current_account.user_seats.voice.where(user_id: zendesk_agent.id).exists?
        end

        it "doesn't create an user seat record if it already exists" do
          post :all_user_seats, params: { user_seat: { seat_type: 'voice' } }
          assert_no_difference 'UserSeat.count' do
            post :all_user_seats, params: { user_seat: { seat_type: 'voice' } }
          end
        end

        it "creates user seats for agents in user_ids" do
          post :all_user_seats, params: { user_seat: { seat_type: 'voice' } }
          other_agent = FactoryBot.create(:agent, account: current_account, name: 'Agent')
          post :all_user_seats, params: { user_seat: { seat_type: 'voice' }, user_ids: [zendesk_agent.id] }

          assert current_account.user_seats.voice.where(user_id: zendesk_agent.id).exists?
          refute current_account.user_seats.voice.where(user_id: other_agent.id).exists?
        end

        it 'syncs talk entitlements' do
          Omnichannel::TalkEntitlementSyncJob.expects(:enqueue).times(current_account.agents.count)
          post :all_user_seats, params: { user_seat: { seat_type: 'voice' } }
        end
      end

      describe "when an agent can't be assigned to a ticket" do
        let(:other_agent) { FactoryBot.create(:agent, account: current_account) }

        before do
          FactoryBot.create(:voice_subscription, account: current_account, plan_type: 0, max_agents: 10)

          other_agent.permission_set = current_account.permission_sets.create!(name: "Lightish Agent")
          other_agent.save!

          other_agent.permission_set.permissions.disable(:ticket_editing)
          other_agent.permission_set.permissions.save!

          post :all_user_seats, params: { user_seat: { seat_type: 'voice' } }
        end

        should respond_with :created

        it "assigns voice seat to the assignable user" do
          assert current_account.user_seats.voice.where(user_id: zendesk_agent.id).exists?
        end

        it "does not assign voice seat to the unassignable user" do
          refute current_account.user_seats.voice.where(user_id: other_agent.id).exists?
        end
      end

      describe "when max agent limit exceeded" do
        before do
          FactoryBot.create(:voice_subscription, account: current_account, plan_type: 0, max_agents: 0)
        end

        it "produces the error 'TooManyAgents' with correct description" do
          post :all_user_seats, params: { user_seat: { seat_type: 'voice' } }

          assert_response :unprocessable_entity
          expected = {
            error: "RecordInvalid",
            description: "Record validation errors",
            details: {
              "base" => [{ "description" => I18n.t('txt.job.user_bulk_update_job.talk_seats_limit_reached', count: 0) }]
            }
          }
          assert_equal expected, error
        end
      end
    end

    describe "#destroy" do
      describe "remove a voice_seat" do
        before do
          FactoryBot.create(:voice_subscription, account: current_account, plan_type: 0, max_agents: 1)
          zendesk_agent.user_seats.voice.where(account_id: current_account.id).create
        end

        it "responds with 204" do
          delete :destroy, params: { user_id: zendesk_agent.id, user_seat: { seat_type: 'voice' } }
          assert_response :no_content
        end
      end

      describe 'talk entitlement sync' do
        before do
          current_user.user_seats.where(account_id: current_account.id, seat_type: 'voice').create
        end

        it 'performs entitlement sync' do
          Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.any_instance.expects(:perform!)
          delete :destroy, params: { user_id: current_user.id, user_seat: { seat_type: 'voice' } }
        end
      end
    end

    describe "#voice_assignable_agents" do
      let(:user_ids) { JSON.parse(response.body)['users'].map { |u| u['id'] } }
      let!(:admin) { users(:minimum_admin) }
      let!(:agent) { users(:minimum_agent) }
      let!(:end_user) { users(:minimum_end_user) }

      before do
        get :voice_assignable_agents
      end

      should respond_with :ok

      it "includes admins" do
        assert_includes user_ids, admin.id
      end

      it "includes agents" do
        assert_includes user_ids, agent.id
      end

      it "excludes end_users" do
        refute_includes user_ids, end_user.id
      end
    end
  end
end
