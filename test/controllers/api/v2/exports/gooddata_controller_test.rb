require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Exports::GooddataController do
  fixtures :all

  as_a_subsystem_user(account: :minimum, user: "gooddata") do
    describe 'a GET to :tickets_export' do
      before do
        @account = accounts(:minimum)
        @request.account = @account
      end

      describe 'without start_time' do
        before { get :tickets_export }

        it('responds with bad_request') { assert_response :bad_request }

        it "responds with text" do
          assert_equal Mime[:json].to_s, @response.content_type
        end
      end

      describe 'with no tickets' do
        before do
          @controller.stubs(:check_if_allowed).returns(nil)
          get :tickets_export, params: { start_time: (Time.now + 20.years).to_i }
        end

        it 'gives back just the headers' do
          result = JSON.parse(@response.body)
          assert result['field_headers']
          assert result['field_headers']['nice_id']
          assert_empty(result['results'])
        end
      end

      describe 'when there are > 1k tickets in the same window' do
        before do
          @start_time = Time.now.to_i
          @state = states('tries').starts_as('first')
          Zendesk::Export::IncrementalTicketExport.any_instance.stubs(:tickets).returns(Array.new(1000) { {'generated_timestamp' => @start_time } }).then(@state.is('second'))
          @controller.stubs(:check_if_allowed).returns(nil)
        end

        it "re-querys with exact_ts = true, limit = nil and bump end_ts" do
          Zendesk::Export::IncrementalTicketExport.any_instance.expects(:exact_ts=).with(true).when(@state.is('second'))

          Zendesk::Export::IncrementalTicketExport.any_instance.expects(:limit=).with(1000).when(@state.is('first'))
          Zendesk::Export::IncrementalTicketExport.any_instance.expects(:limit=).with(nil).when(@state.is('second'))
          get :tickets_export, params: { start_time: @start_time }
          result = JSON.parse(@response.body)
          assert_equal @start_time + 1, result['end_time']
        end
      end

      describe 'Starter and Regular accounts' do
        it 'returns a forbidden' do
          get :tickets_export, params: { start_time: Time.now.to_i }
          assert_response 403
        end
      end

      describe "limit of custom fields" do
        it "enforces a limit of 60 custom fields for non-enterprise accounts" do
          @controller.stubs(:check_if_allowed).returns(nil)
          get :tickets_export, params: { start_time: Time.now.to_i }
          assert_equal 60, assigns(:exporter).custom_field_limit
        end

        it "enforces a limit of 200 custom fields for enterprise accounts" do
          @controller.stubs(:custom_fields_limit).returns(200)
          @controller.stubs(:check_if_allowed).returns(nil)
          get :tickets_export, params: { start_time: Time.now.to_i }
          assert_equal 200, assigns(:exporter).custom_field_limit
        end
      end
    end

    describe 'a GET to :account_options' do
      before do
        Zendesk::Accounts::Client.any_instance.stubs(:products).returns([])
        Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
        @account = accounts(:minimum)
        @request.account = @account
      end

      describe 'with a "Professional" plan type' do
        before do
          @controller.stubs(:check_if_allowed).returns(nil)
          @account.subscription.update_attribute(:plan_type, SubscriptionPlanType.Large)
          @account.subscription.update_attribute(:pricing_model_revision, ZBC::Zendesk::PricingModelRevision::PATAGONIA)
        end

        it 'presents as a Plus plan' do
          get :account_options
          result = JSON.parse(@response.body)
          assert_equal '00', result['options']['hour_offset']
          assert_equal 'Plus', result['options']['plan_name']
        end
      end

      describe 'when allowed' do
        before do
          @controller.stubs(:check_if_allowed).returns(nil)
        end

        it 'gives account timezone and ticket load frequency and plan name' do
          get :account_options
          result = JSON.parse(@response.body)
          assert_equal '00', result['options']['hour_offset']
          assert_equal 'Regular', result['options']['plan_name']
        end

        it 'includes the state of features for the current account' do
          get :account_options
          result = JSON.parse(@response.body)

          assert_includes result['options'].keys, 'deployments'
          refute result['options']['deployments'].empty?
        end

        it 'includes the state of settings for the current account' do
          get :account_options
          result = JSON.parse(@response.body)

          assert_includes result['options'].keys, 'settings'
          refute result['options']['settings'].empty?
        end

        describe 'with an hourly load subscription' do
          before do
            @account.subscription.plan_type = SubscriptionPlanType.ExtraLarge
            @account.subscription.save!
            Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
          end
          it 'gives hourly load frequency responses' do
            get :account_options
            result = JSON.parse(@response.body)
            assert_equal 'hourly', result['options']['hour_offset']
          end
        end

        describe 'with feature boost' do
          before do
            @feature_boost = FeatureBoost.new(
              account: @account,
              boost_level: SubscriptionPlanType.ExtraLarge,
              valid_until: 20.days.from_now
            )
            @feature_boost.save
          end

          describe 'when the feature boost is active' do
            before do
              @feature_boost.update_attribute :valid_until, 30.days.from_now
              @feature_boost.update_attribute :is_active, true
            end

            it 'gives the subcription plan type of the feature boost' do
              get :account_options
              result = JSON.parse(@response.body)
              assert_equal @feature_boost.boost_plan_score, result['options']['plan_name']
            end
          end

          describe 'when the feature boost is inactive' do
            before do
              @feature_boost.update_attribute :valid_until, 30.days.ago
              @feature_boost.update_attribute :is_active, false
            end

            it 'gives the subcription plan type of the account' do
              get :account_options
              result = JSON.parse(@response.body)
              assert_equal @account.subscription.plan_name, result['options']['plan_name']
            end
          end
        end
      end
    end

    describe "a GET to :show_many_tickets" do
      let(:account) { accounts(:minimum) }
      let(:admin) { users(:minimum_admin) }
      let(:ids) { account.tickets.map(&:nice_id).sort }

      before do
        @controller.stubs(:check_if_allowed).returns(nil)
      end

      describe "with archived tickets" do
        let(:archived_ticket_id) { ids.first }

        before do
          ticket = account.tickets.find_by_nice_id(archived_ticket_id)
          archive_and_delete(ticket)
          get :show_many_tickets, params: { ids: ids.join(",") }
        end

        it "gives me back the tickets I asked for" do
          json = JSON.parse(@response.body)
          assert_equal ids, json['tickets'].map { |u| u["id"] }.sort
        end
      end

      describe "with deleted tickets" do
        let(:deleted_ticket_id) { ids.first }

        before do
          ticket = account.tickets.find_by_nice_id(deleted_ticket_id)
          CIA.audit(actor: admin) do
            ticket.will_be_saved_by(admin)
            ticket.soft_delete!
          end
          ticket.reload

          get :show_many_tickets, params: { ids: ids.join(",") }
        end

        it "gives me back the tickets I asked for" do
          json = JSON.parse(@response.body)
          assert_equal ids, json['tickets'].map { |u| u["id"] }.sort
        end
      end
    end
  end
end
