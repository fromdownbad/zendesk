require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Exports::TicketsController do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: "api/v2/exports/tickets") do |request|
    request.should_route :get, "/api/v2/exports/tickets", action: "index"
    request.should_route :get, "/api/v2/exports/tickets/sample", action: "sample"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_agent do
    should_be_forbidden :index
  end

  as_a_subsystem_user(account: :minimum) do
    describe "a GET to :index" do
      before { get :index, params: { start_time: 6.minutes.ago.to_i } }

      before_should "not call Prop.throttle!" do
        Prop.expects(:throttle!).never
      end

      it('responds with ok') { assert_response :ok }
    end
  end

  as_an_admin do
    describe "a GET to :index" do
      describe "without start_time" do
        before { get :index }

        it('responds with bad_request') { assert_response :bad_request }

        it "responds with json" do
          assert_equal Mime[:json].to_s, @response.content_type
        end
      end

      describe "with too recent start_time" do
        before { get :index, params: { start_time: Time.now.to_i } }

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }

        it "responds with json" do
          assert_equal Mime[:json].to_s, @response.content_type
        end
      end

      describe "with valid start_time" do
        before do
          Timecop.freeze
          Prop.reset(:incremental_ticket_exports, @account.id)
          get :index, params: { start_time: 6.minutes.ago.to_i }
        end

        should_use_presenter Api::V2::Exports::TicketPresenter

        describe "rate limiting" do
          before do
            get :index, params: { start_time: 6.minutes.ago.to_i }
          end

          it "rates limit if polled multiple times in the same window" do
            assert_response 429
            assert @response.headers.include?("Retry-After")
          end
        end
      end

      describe "when disabled via Arturo" do
        before do
          Arturo.enable_feature! :api_disable_v2_ticket_exports
          get :index, params: { start_time: 6.minutes.ago.to_i }
        end

        it "returns a 410 response" do
          assert_equal 410, @response.status
        end
      end

      it "initializes an exporter with 1000 records limit" do
        get :index, params: { start_time: 6.minutes.ago.to_i }
        assert_response :success
        assert_equal assigns(:exporter).limit, 1000
      end

      it "exports assignee_external_id, assignee_id and group_id" do
        get :index, params: { start_time: 0 }
        assert_response :success

        result = JSON.parse(@response.body)
        assert_equal "Assignee id", result["field_headers"]["assignee_id"]
        assert_equal "Assignee external id", result["field_headers"]["assignee_external_id"]
        assert_equal "Group id", result["field_headers"]["group_id"]

        result["results"].each do |row|
          ticket = Ticket.with_deleted { @account.tickets.find_by_nice_id(row["id"]) }
          if ticket.group
            assert_equal ticket.group.id, row["group_id"].to_i
          else
            assert_equal '', row["group_id"]
          end
          if ticket.assignee.present?
            assert_equal ticket.assignee.id, row["assignee_id"].to_i
            assert_equal ticket.assignee.external_id.to_s, row["assignee_external_id"]
          else
            assert_equal '', row["assignee_id"]
            assert_equal '', row["assignee_external_id"]
          end
        end
      end
    end

    describe "a GET to :sample" do
      it "initializes an exporter with 50 records limit" do
        get :sample, params: { start_time: 6.minutes.ago.to_i }
        assert_response :success
        assert_equal assigns(:sample_exporter).limit, 50
      end

      it "does not raise if polled twice" do
        get :sample, params: { start_time: 6.minutes.ago.to_i }
        assert_response :success
        get :sample, params: { start_time: 6.minutes.ago.to_i }
        assert_response :success
      end
    end

    describe "with instrumentation" do
      let(:statsd_client) { Zendesk::StatsD::Client.new(namespace: "test") }

      before do
        Zendesk::StatsD::Client.stubs(:new).with(anything).returns(statsd_client)
        get :index, params: { start_time: 0 }
      end

      before_should "log the time and status of the response" do
        statsd_client.expects(:timing).with('api.v2.exports.tickets', anything, tags: ['action:index', 'gooddata:false', 'status:200'])
      end
    end
  end
end
