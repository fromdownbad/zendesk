require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::AlertsController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users

  before do
    accept :json
  end

  with_options(controller: "api/v2/alerts") do |request|
    request.should_route :get,    "/api/v2/alerts",              action: "index"
    request.should_route :delete, "/api/v2/alerts/1",            action: "destroy", id: "1"
    request.should_route :delete, "/api/v2/alerts/destroy_many", action: "destroy_many"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :destroy, :destroy_many
  end

  as_an_end_user do
    should_be_forbidden :index, :destroy, :destroy_many
  end

  as_an_agent do
    describe "a GET to #index" do
      describe "presenter" do
        before do
          @alert = FactoryBot.create(:alert)
          get :index
        end
        should_use_presenter Api::V2::AlertPresenter
      end

      describe "with irrelevant alerts" do
        before do
          @alert = FactoryBot.create(:alert)

          # misdirection
          FactoryBot.create(:alert, pods: [123])
          FactoryBot.create(:alert, is_active: false)
          get :index
        end

        it "shows alerts for this user" do
          assert_equal 1, JSON.parse(@response.body)["alerts"].length
          assert_equal @alert.id, JSON.parse(@response.body)["alerts"][0]["id"]
        end
      end

      describe "with dismissed alerts" do
        before do
          @alert = FactoryBot.create(:alert)
          AlertDismissal.create alert: @alert, user: @user, account: @account
          get :index
        end

        it "does not show alerts that have been dismissed" do
          assert_equal [], JSON.parse(@response.body)["alerts"]
        end
      end

      describe "with interface specific alerts" do
        before do
          FactoryBot.create(:alert)
          FactoryBot.create(:alert, interfaces: ["classic"])
          FactoryBot.create(:alert, interfaces: ["lotus"])
        end

        describe "without interface specified in params" do
          before do
            get :index
          end

          should_use_presenter Api::V2::AlertPresenter

          it "returns alerts targeting all interfaces" do
            assert_equal 3, JSON.parse(@response.body)["alerts"].length
          end
        end

        describe "with interface specified in params" do
          before do
            get :index, params: { interface: "lotus" }
          end

          should_use_presenter Api::V2::AlertPresenter

          it "returns only alerts that show for interface" do
            assert_equal 2, JSON.parse(@response.body)["alerts"].length
          end
        end
      end
    end

    describe "DELETE" do
      before do
        @alert = Alert.create! do |a|
          a.is_active = true
          a.value = "ALERT!"
        end
      end

      it "responds with 204" do
        delete :destroy, params: { id: @alert.id }
        assert_response :no_content
      end

      it "creates a AlertDismissal" do
        delete :destroy, params: { id: @alert.id }
        @user.reload
        assert_equal [], Alert.for(@user)
        assert_equal @alert.id, AlertDismissal.first.alert_id
      end
    end

    describe "on a DELETE to #destroy_many" do
      before do
        @alert1 = Alert.create! do |a|
          a.is_active = true
          a.value = "ALERT!"
        end
        @alert2 = Alert.create! do |a|
          a.is_active = true
          a.value = "ALERT!"
        end
      end

      it "responds with 204" do
        delete :destroy_many, params: { ids: [@alert1.id, @alert2.id].join(',') }
        assert_response :no_content
      end

      it 'dismiss the alerts' do
        delete :destroy_many, params: { ids: [@alert1.id, @alert2.id].join(',') }
        @user.reload
        assert_equal [], Alert.for(@user)
        assert_equal [@alert1.id, @alert2.id], AlertDismissal.all.map(&:alert_id)
      end

      it "does not create dismissals for alerts not visible to user" do
        AlertDismissal.without_arsi.delete_all
        @alert = Alert.create! do |a|
          a.is_active = true
          a.value = "Alert"
          a.pods = [123]
        end

        delete :destroy_many, params: { ids: @alert.id }
        assert_equal [], @user.alert_dismissals
        @alert.update_attribute(:pods, 'all')

        delete :destroy_many, params: { ids: @alert.id }
        assert_equal 1, @user.reload.alert_dismissals.length
      end
    end
  end
end
