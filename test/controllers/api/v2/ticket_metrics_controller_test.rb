require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TicketMetricsController do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: "api/v2/ticket_metrics") do |request|
    request.should_route :get,    "/api/v2/ticket_metrics",     action: "index"
    request.should_route :get,    "/api/v2/ticket_metrics/1",   action: "show",  id: "1"
    request.should_route :get,    "/api/v2/tickets/1/metrics",  action: "show", ticket_id: "1"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :show
  end

  as_an_end_user do
    should_be_forbidden :index, :show
  end

  as_an_agent do
    describe "a GET to #index" do
      before { get :index }

      should_use_presenter Api::V2::TicketMetricSetPresenter

      it "sorts them by descending date" do
        sets = TicketMetricSet.includes(:ticket).order("tickets.created_at DESC").references(:tickets)
        known_dates = sets.map { |set| set.ticket.created_at.to_i }

        response = JSON.parse(@response.body)

        returned_dates = response['ticket_metrics'].map do |metric|
          ticket = sets.detect { |set| set.id == metric.fetch("id") }.try(:ticket)
          ticket ? ticket.created_at.to_i : nil
        end

        assert_equal known_dates, returned_dates
      end
    end

    describe "ticket_metrics_v2_limiter disabled" do
      let (:threshold) { 1 }
      before do
        Timecop.freeze
      end

      it "does not throttle traffic" do
        (threshold + 1).times do
          get :index
          assert_response :ok
        end
      end
    end

    describe "ticket_metrics_v2_limiter enabled" do
      let (:threshold) { 1 }
      before do
        Timecop.freeze
        Arturo.enable_feature!(:ticket_metrics_v2_limiter)
      end

      it "throttles after 1 within 10s" do
        threshold.times do
          get :index
          assert_response :ok
        end

        get :index
        assert_response :too_many_requests
      end

      describe "api traffic from lotus" do
        before { request.env['HTTP_X_ZENDESK_LOTUS_VERSION'] = 'v1.234' }

        it "does not throttle traffic" do
          (threshold + 1).times do
            get :index
            assert_response :ok
          end
        end
      end
    end

    describe "a GET to #show" do
      describe "using presenter" do
        before { get :show, params: { id: ticket_metric_sets(:full_set_1).id } }
        should_use_presenter Api::V2::TicketMetricSetPresenter
      end

      describe "using presenter also for archived ticket" do
        before do
          ticket_metric_sets = ticket_metric_sets(:full_set_1)
          ticket_metric_sets.ticket.archive!
          get :show, params: { id: ticket_metric_sets.id }
        end
        should_use_presenter Api::V2::TicketMetricSetPresenter
      end

      it "returns 404 if the ticket metric doesn't exist" do
        id = ticket_metric_sets(:full_set_1).id
        TicketMetricSet.without_arsi.delete_all
        get :show, params: { id: id }
        assert_response :not_found
      end
    end

    describe "a GET to #show with a ticket id" do
      describe "using presenter" do
        before { get :show, params: { ticket_id: tickets(:minimum_1).nice_id } }
        should_use_presenter Api::V2::TicketMetricSetPresenter
      end

      it "is 404 if the ticket has no ticket metric" do
        TicketMetricSet.without_arsi.delete_all
        refute tickets(:minimum_1).ticket_metric_set

        get :show, params: { ticket_id: tickets(:minimum_1).nice_id }
        assert_response :not_found
      end
    end
  end
end
