require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TicketPredictionsController do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    accept :json
    @ticket = tickets(:minimum_1)
    @ticket2 = tickets(:minimum_2)
    @ticket.create_ticket_prediction(satisfaction_probability: 0.1)
  end

  with_options(controller: 'api/v2/ticket_predictions') do |request|
    request.should_route :get, '/api/v2/tickets/1/ticket_prediction', action: 'show', ticket_id: '1'
  end

  as_an_anonymous_user do
    should_be_unauthorized :show
  end

  as_an_end_user do
    should_be_forbidden :show
  end

  as_an_agent do
    describe "a GET to #show" do
      describe "with a valid ticket id" do
        before { get :show, params: { ticket_id: @ticket.nice_id } }

        should_use_presenter Api::V2::Tickets::PredictionPresenter
      end

      describe "with an invalid ticket id" do
        before { get :show, params: { ticket_id: 1000 } }

        it('responds with not_found') { assert_response :not_found }
      end

      describe "with a valid ticket that has no ticket prediction" do
        before { get :show, params: { ticket_id: @ticket2.nice_id } }

        it('responds with not_found') { assert_response :not_found }
      end
    end
  end
end
