require_relative '../../../../support/test_helper'
require_relative '../../../../support/api_scopes_helper'

SingleCov.covered!

describe Api::V2::Integrations::FabricController do
  extend ApiScopesHelper

  fixtures :accounts, :account_settings

  def stub_json_post(endpoint:, body: nil, status: 201)
    stub_request(:post, endpoint).
      to_return(
        status: status,
        headers: { 'Content-Type' => 'application/json' },
        body: body.to_json
      )
  end

  # NOTE: The authentication setup for this endpoint works by checking that the OAuth Token
  # belongs to a specific Global Oauth client created for Twitter's Fabric servers. The
  # aforementioned Global Oauth Client has to have an identifier set to "twitter_fabric_zendesk"
  # in order to all of this to work. See app/controllers/api/v2/integrations/fabric_controller.rb
  # for more details.
  #
  # Also see https://github.com/twitterdev/3pk/wiki/Provisioning-APIs for understanding how Twitter
  # will request the token (it's pretty standard OAuth2 though)
  let(:account) { accounts(:minimum) }

  # This OAuth client is a regular one that belongs to the MobileSdkApp record has nothing to do
  # with the authentication for this endpoint. Is just the data that needs to be stubbed.
  let(:app_client) { FactoryBot.create(:client, account: account, user: account.owner) }
  let(:mobile_sdk_auth) { FactoryBot.create(:mobile_sdk_auth, account: account, client: app_client) }
  let(:app) { FactoryBot.create(:mobile_sdk_app, account: account, mobile_sdk_auth: mobile_sdk_auth) }

  let(:account_uri) { "https://#{account.subdomain}.#{Zendesk::Configuration.fetch(:host)}" }
  let(:accept_tos_endpoint) { "#{account_uri}/api/private/mobile_sdk/tos/accept" }
  let(:account_endpoint) { "https://zendeskaccounts.#{Zendesk::Configuration.fetch(:host)}/api/v2/accounts" }
  let(:mobile_sdk_app_endpoint) { "#{account_uri}/api/v2/mobile_sdk_apps" }

  let(:payload) do
    {
      platform: 'PLATAFORM',
      name: 'NAME',
      email: 'email@crazy-domain.com',
      app_name: 'APP NAME',
      bundle_identifier: 'BUNDLE',
      is_confirmed: true
    }
  end

  before do
    Arturo.enable_feature! :mobile_sdk_fabric_integration # Enable the endpoint

    client = FactoryBot.create(:global_client, user: account.owner)
    client.update_columns(identifier: 'twitter_fabric_zendesk')
    token = FactoryBot.create(:token, global_client: client, user: account.owner, scopes: [])
    @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token
    accept :json

    Zendesk::Configuration::PodConfig.stubs(pod_ids: [1])

    stub_json_post(endpoint: mobile_sdk_app_endpoint, status: 201, body: {
      mobile_sdk_app: {
        id: app.id,
        title: "Fabric my-app",
        identifier: app.identifier,
        client_identifier: app_client.identifier
      }
    })

    stub_json_post(endpoint: account_endpoint, status: 201, body: {
      account: {
        url: account.url,
        name: account.name,
        subdomain: account.subdomain
      }
    })

    stub_json_post(endpoint: accept_tos_endpoint, status: 200)
  end

  describe "non authenticated calls" do
    it 'is :forbidden when no token is provided' do
      @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = nil

      post :create, params: payload

      assert_response :forbidden
    end

    it "is :forbidden when a token with a different global oauth client (not 'twitter_fabric_zendesk') is provided" do
      client = FactoryBot.create(:global_client, identifier: "zdg-other_global_identifier", user: account.owner)
      token = FactoryBot.create(:token, global_client: client, user: account.owner, scopes: [])
      @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token

      post :create, params: payload

      assert_response :forbidden
    end

    it "is :forbidden when a token with a non-global oauth client is provided" do
      client = FactoryBot.create(:client, identifier: "regular_non_global", user: account.owner)
      token = FactoryBot.create(:token, client: client, user: account.owner, scopes: [])
      @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token

      post :create, params: payload

      assert_response :forbidden
    end
  end

  it 'ensures route to be available only on the right subdomain' do
    assert_routing(
      {method: :post, path: 'https://signup.zd-test.com/api/v2/integrations/fabric'},
      controller: 'api/v2/integrations/fabric',
      action: 'create',
      format: 'json'
    )
  end

  describe "Success" do
    it "creates an account and an mobile sdk app successfully" do
      post :create, params: payload

      assert_response :created
      json = JSON.parse(response.body)

      assert_equal app.id.to_s, json['id']
      assert_equal account.url(protocol: 'https'), json['keys']['primary']['url']
      assert_equal app.identifier, json['keys']['primary']['identifier']
      assert_equal app.mobile_sdk_auth.client.identifier, json['keys']['primary']['client_identifier']
    end

    it "builds the FabricAccountCreator correctly (passes 'accept_tos' set to true)" do
      Accounts::Fabric::AccountCreator.expects(:create).with(anything,
        subdomain: Accounts::Fabric::SubdomainGenerator.new(payload[:email]).subdomain,
        account_creation_endpoint: api_v2_accounts_path,
        accept_tos_endpoint: api_private_mobile_sdk_tos_accept_path,
        accept_tos: true).returns(account)

      post :create, params: payload
      assert_response :created
    end
  end

  it 'returns :unprocessable_entity when the internal API failed' do
    stub_json_post(endpoint: account_endpoint, status: 404, body: nil.to_json)

    post :create, params: payload

    assert_response :unprocessable_entity
  end

  it 'returns :conflict when the account already exists' do
    account = accounts(:minimum)
    @controller.stubs(:generated_subdomain).returns(account.subdomain)

    post :create, params: payload
    assert_response :conflict
  end

  describe 'rate limit' do
    before do
      @fabric_account_creations_prop = Prop.configurations[:fabric_account_creations]
      Prop.configure :fabric_account_creations,
        threshold: 1,
        interval: 1.minute
    end

    after do
      Prop.configure :fabric_account_creations, @fabric_account_creations_prop
    end

    it 'throttle the request that exceeds the limit' do
      post :create, params: payload

      assert_response 429
    end
  end

  describe 'accept tos' do
    it 'accepts the tos with success' do
      post :create, params: payload
      assert_response :success
    end

    it 'does not stop the process when it fails to accept the tos' do
      stub_json_post(endpoint: accept_tos_endpoint, status: 404)

      post :create, params: payload
      assert_response :success
    end
  end
end
