require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Integrations::JiraController do
  extend Api::V2::TestHelper
  fixtures :tickets

  before do
    accept :json

    @remote_host = "https://test.com"
    @agreement = FactoryBot.create(
      :agreement,
      remote_url: @remote_host.dup,
      account_id: accounts(:minimum).id,
      shared_with_id: Sharing::Agreement::SHARED_WITH_MAP[:jira],
      uuid: "uuid"
    )
  end

  with_options(controller: "api/v2/integrations/jira") do |request|
    request.should_route :get, "/api/v2/tickets/1/jira", action: "ticket_details", ticket_id: 1
    request.should_route :get, "/api/v2/sharing_agreements/1/jira", action: "projects", sharing_agreement_id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :projects, {sharing_agreement_id: 1}]
  end

  as_an_end_user do
    should_be_forbidden [:get, :projects, {sharing_agreement_id: 1}]
  end

  as_an_agent do
    describe "a GET to :projects" do
      before do
        @remote_url = "#{@remote_host}/plugins/servlet/zendesk/sharing/info"
      end

      describe "with an unsuccessful Jira request" do
        before do
          stub_request(:get, @remote_url).to_raise(Net::HTTPBadResponse)
          get :projects, params: { sharing_agreement_id: @agreement.id }
        end

        it "responds with json" do
          assert_equal({}.to_json, @response.body)
        end
      end

      describe "with a successful Jira request" do
        let(:body) { { projects: [1, 2, 3] } }

        before do
          stub_request(:get, @remote_url).to_return(body: body.to_json)
          get :projects, params: { sharing_agreement_id: @agreement.id }
        end

        it "responds with json" do
          assert_equal [1, 2, 3].to_json, @response.body
        end
      end
    end

    describe "a GET to :ticket" do
      before do
        @ticket = tickets(:minimum_1)
        @shared_ticket = SharedTicket.create!(agreement_id: @agreement.id, ticket: @ticket)
        @remote_url = "#{@remote_host}/plugins/servlet/zendesk/sharing/tickets/#{@shared_ticket.uuid}"
      end

      describe "with an unsuccessful Jira request" do
        before do
          stub_request(:get, @remote_url).to_raise(Net::HTTPBadResponse)
          get :ticket_details, params: { ticket_id: @ticket.nice_id }
        end

        it "responds with json" do
          assert_equal({}.to_json, @response.body)
        end
      end

      describe "with a successful Jira request" do
        let(:body) { { foo: "bar" } }

        before do
          stub_request(:get, @remote_url).to_return(body: body.to_json)
          get :ticket_details, params: { ticket_id: @ticket.nice_id }
        end

        it "responds with json" do
          assert_equal body.to_json, @response.body
        end
      end
    end
  end
end
