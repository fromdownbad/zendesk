require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Integrations::MsDynamicsPolicyController do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    @request.account = @account

    @integration = MsDynamicsIntegration.new

    accept :xml
  end

  should_route :get, "/clientaccesspolicy.xml", controller: "api/v2/integrations/ms_dynamics_policy", action: :index

  as_an_anonymous_user do
    describe "a GET to :index" do
      describe "when there is a configured integration" do
        before do
          @account.stubs(:ms_dynamics_integration).returns(@integration)
          @integration.stubs(:configured?).returns(true)
        end

        it "uses the correct presenter" do
          Api::V2::Integrations::MsDynamicsPolicyPresenter.any_instance.expects(:present_xml).with(@integration).returns("stub_response")

          get :index
        end
      end

      describe "when there is no configured integration" do
        before do
          @account.stubs(:ms_dynamics_integration).returns(nil)
        end

        should_be_unauthorized :index
      end
    end
  end
end
