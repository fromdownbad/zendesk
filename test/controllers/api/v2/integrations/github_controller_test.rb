require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Api::V2::Integrations::GithubController do
  fixtures :accounts, :tickets, :events, :users, :user_identities

  before do
    accept :json
  end

  should_route :post, "/api/v2/integrations/github", controller: "api/v2/integrations/github", action: "create"

  as_an_anonymous_user do
    should_be_unauthorized :create
  end

  as_an_end_user do
    should_be_forbidden :create
  end

  as_an_agent do
    describe "a POST to :create" do
      describe "on an open ticket" do
        before { @ticket = accounts(:minimum).tickets.working.last }

        it "is able to post payload" do
          post :create, params: { ticket_id: @ticket.nice_id, payload: commit_message(@ticket.nice_id)["payload"] }
          assert_response :created
          assert_equal ViaType.GITHUB, @ticket.reload.audits.last.via_id
        end

        it "does not update if the 'ref' is not master branch" do
          post :create, params: { ticket_id: @ticket.nice_id, payload: {ref: "refs/head/staging"} }
          assert_response :unprocessable_entity
        end

        it "adds a comment when an issue is labeled" do
          comment_count = @ticket.comments.size
          post :create, params: { ticket_id: @ticket.nice_id, payload: issue_tagged["payload"] }
          assert_equal 1, @ticket.comments.size - comment_count
        end

        it "adds a comment when an issue is milestoned" do
          comment_count = @ticket.comments.size
          post :create, params: { ticket_id: @ticket.nice_id, payload: issue_from_event("milestoned")["payload"] }
          assert_equal 1, @ticket.comments.size - comment_count
        end
      end

      describe "on a closed ticket" do
        before { @ticket = accounts(:minimum).tickets.where(status_id: StatusType.CLOSED).first }

        it "does not update a closed ticket" do
          post :create, params: { ticket_id: @ticket.nice_id, payload: commit_message(@ticket.nice_id)["payload"] }
          assert_response :unprocessable_entity

          assert_not_equal ViaType.GITHUB, @ticket.reload.audits.last.via_id
        end
      end
    end
  end

  describe Api::V2::Integrations::GithubController::GithubMessageRenderer do
    describe "a new commit" do
      before do
        payload = parse_payload(commit_message)[0]
        @renderer = Api::V2::Integrations::GithubController::GithubMessageRenderer.new(payload)
      end

      it "renders" do
        @renderer.to_s.must_include "referenced this ticket in commit"
      end
    end

    describe "multiple commits with only one referring to ticket" do
      before do
        payload = parse_payload(multiple_commit_messages(1))[0]
        @renderer = Api::V2::Integrations::GithubController::GithubMessageRenderer.new(payload)
      end

      it "renders" do
        @renderer.to_s.must_include "referenced this ticket in commit https://github.com/FakeGithubAccount/fakeRepo/commit/d2449f4d3dba3fd7871cc5985af7572850f4c6b3"
      end
    end

    describe "multiple commits with referring to different tickets" do
      before do
        @payload = parse_payload(multiple_commit_messages_1(1, 2))
      end

      it "renders" do
        assert_equal 2, @payload.size
        Api::V2::Integrations::GithubController::GithubMessageRenderer.new(@payload[0]).to_s.must_include "referenced this ticket in commit https://github.com/FakeGithubAccount/fakeRepo/commit/d2449f4d3dba3fd7871cc5985af7572850f4c6b3"
        Api::V2::Integrations::GithubController::GithubMessageRenderer.new(@payload[1]).to_s.must_include "referenced this ticket in commit https://github.com/FakeGithubAccount/fakeRepo/commit/5c73ba3a666bda4c6febc6ee6bdf43edcfe1ae2f"
      end
    end

    describe "comment on a commit" do
      before do
        payload = parse_payload(commit_comment)[0]
        @renderer = Api::V2::Integrations::GithubController::GithubMessageRenderer.new(payload)
      end

      it "renders" do
        @renderer.to_s.must_include "internal_user referenced this ticket a comment on commit"
      end
    end

    describe "a new issue" do
      before do
        payload = parse_payload(issue_created)[0]
        @renderer = Api::V2::Integrations::GithubController::GithubMessageRenderer.new(payload)
      end

      it "renders" do
        @renderer.to_s.must_include "internal_user opened issue"
      end
    end

    describe "comment on an issue" do
      before do
        payload = parse_payload(issue_updated)[0]
        @renderer = Api::V2::Integrations::GithubController::GithubMessageRenderer.new(payload)
      end

      it "renders" do
        @renderer.to_s.must_include "internal_user created a comment on issue"
      end
    end

    describe "closing an issue" do
      before do
        payload = parse_payload(issue_closed)[0]
        @renderer = Api::V2::Integrations::GithubController::GithubMessageRenderer.new(payload)
      end

      it "renders" do
        @renderer.to_s.must_include "internal_user closed issue"
      end
    end

    Api::V2::Integrations::GithubController::GITHUB_ISSUE_EVENTS.each do |action|
      describe "when issue is #{action}" do
        before do
          @payload = parse_payload(issue_from_event(action)).first
          @renderer = Api::V2::Integrations::GithubController::GithubMessageRenderer.new(@payload).to_s
        end

        it "renders" do
          if @payload["comment"].blank? || action == "reopened"
            @renderer.must_include "internal_user #{action} issue https://github.com/internal_user/testing/issues/5"
          else
            @renderer.must_include "internal_user #{action} a comment on issue https://github.com/internal_user/testing/issues/5"
          end
          @renderer.must_include "Label: label1" if action == "labeled"
          @renderer.must_include "Assignee: internal_user" if action == "assigned"
          @renderer.must_include "Milestone: milestone1" if action == "milestoned"
        end
      end
    end

    describe "a pull request" do
      before do
        payload = parse_payload(pull_request)[0]
        @renderer = Api::V2::Integrations::GithubController::GithubMessageRenderer.new(payload)
      end

      it "renders" do
        @renderer.to_s.must_include "functionality where you can click"
      end
    end
  end

  def issue_from_event(action)
    issue = issue_base
    issue["payload"]["action"] = action
    case action
    when "created"
      issue["payload"]["comment"] = { "body" => "my comment"}
    when "assigned"
      issue["payload"]["assignee"] = { "login" => "internal_user" }
    when "unassinegned"
      issue["payload"]["assignee"] = nil
    when "labeled"
      issue["payload"]["label"] = {"name" => "label1"}
    when "unlabeled"
      issue["payload"]["label"] = nil
    when "milestoned"
      issue["payload"]["issue"]["milestone"] = {"title" => "milestone1"}
    when "demilestoned"
      issue["payload"]["issue"]["milestone"] = nil
    when "reopened"
      issue["payload"]["comment"] = { "body" => "I'm reopening this" }
    end
    issue
  end

  def issue_base
    {"payload" => { "repository" => {"name" => "testing", "created_at" => "2012-03-02T07:03:16Z", "updated_at" => "2012-03-20T21:39:34Z", "url" => "https://api.github.com/repos/internal_user/testing", "pushed_at" => "2012-03-20T21:39:33Z", "id" => 3599985, "owner" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "sender" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "action" => "created", "issue" => {"number" => 5, "created_at" => "2012-03-21T00:47:24Z", "pull_request" => {"diff_url" => nil, "patch_url" => nil, "html_url" => nil}, "title" => "New issue", "body" => "Mentioning zd#380", "comments" => 2, "updated_at" => "2012-03-21T00:48:54Z", "url" => "https://api.github.com/repos/internal_user/testing/issues/5", "id" => 3738679, "assignee" => nil, "milestone" => nil, "closed_at" => nil, "html_url" => "https://github.com/internal_user/testing/issues/5", "user" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "labels" => [], "state" => "open"}}, "action" => "create", "ticket_id" => "380", "controller" => "api/v2/integrations/github"}
  end

  def commit_message(ticket_id = 1)
    {"payload" => {"pusher" => {"name" => "internal_user", "email" => "internal_user@zendesk.zen"}, "repository" => {"name" => "testing", "size" => 120, "has_wiki" => true, "created_at" => "2012/03/01 23:03:16 -0800", "private" => false, "watchers" => 1, "fork" => false, "url" => "https://github.com/internal_user/testing", "pushed_at" => "2012/03/20 17:51:25 -0700", "open_issues" => 6, "has_downloads" => true, "has_issues" => true, "homepage" => "", "description" => "", "forks" => 1, "owner" => {"name" => "internal_user", "email" => "internal_user@zendesk.zen"}}, "forced" => false, "after" => "c60e5200f53825843f2a41a905603afe6b51d5f2", "head_commit" => {"added" => [], "modified" => ["README"], "removed" => [], "author" => {"name" => "Internal User", "username" => "internal_user", "email" => "internal_user@zendesk.zen"}, "timestamp" => "2012-03-20T17:51:18-07:00", "url" => "https://github.com/internal_user/testing/commit/c60e5200f53825843f2a41a905603afe6b51d5f2", "id" => "c60e5200f53825843f2a41a905603afe6b51d5f2", "distinct" => true, "message" => "New commit for ticket zd##{ticket_id}", "committer" => {"name" => "Internal User", "username" => "internal_user", "email" => "internal_user@zendesk.zen"}}, "deleted" => false, "ref" => "refs/heads/master", "commits" => [{"added" => [], "modified" => ["README"], "removed" => [], "author" => {"name" => "Internal User", "username" => "internal_user", "email" => "internal_user@zendesk.zen"}, "timestamp" => "2012-03-20T17:51:18-07:00", "url" => "https://github.com/internal_user/testing/commit/c60e5200f53825843f2a41a905603afe6b51d5f2", "id" => "c60e5200f53825843f2a41a905603afe6b51d5f2", "distinct" => true, "message" => "New commit for ticket zd##{ticket_id}", "committer" => {"name" => "Internal User", "username" => "internal_user", "email" => "internal_user@zendesk.zen"}}], "compare" => "https://github.com/internal_user/testing/compare/767313a...c60e520", "before" => "767313a5a87530d41e42a32945fd567be86f066b", "created" => false}, "action" => "create", "ticket_id" => ticket_id, "controller" => "api/v2/integrations/github"}
  end

  def multiple_commit_messages(ticket_id)
    {"ticket_id" => ticket_id.to_s, "payload" => {"pusher" => {"name" => "external_user", "email" => "external_user@zendesk.zen"}, "repository" => {"name" => "compete", "has_wiki" => true, "size" => 2268, "created_at" => "2011-11-11T10:06:18-08:00", "private" => true, "watchers" => 1, "fork" => false, "url" => "https://github.com/FakeGithubAccount/fakeRepo", "language" => "Ruby", "pushed_at" => "2012-09-19T11:18:30-07:00", "open_issues" => 10, "has_downloads" => true, "homepage" => "", "has_issues" => true, "stargazers" => 1, "forks" => 0, "organization" => "FakeGithubAccount", "description" => "", "owner" => {"name" => "FakeGithubAccount", "email" => nil}}, "forced" => false, "after" => "5c73ba3a666bda4c6febc6ee6bdf43edcfe1ae2f", "head_commit" => {"added" => [], "modified" => ["config/domains.yml"], "author" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}, "removed" => [], "timestamp" => "2012-09-19T11:17:53-07:00", "url" => "https://github.com/FakeGithubAccount/fakeRepo/commit/5c73ba3a666bda4c6febc6ee6bdf43edcfe1ae2f", "id" => "5c73ba3a666bda4c6febc6ee6bdf43edcfe1ae2f", "distinct" => true, "message" => "Match development compare domain to seeds", "committer" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}}, "deleted" => false, "ref" => "refs/heads/master", "commits" => [{"added" => [], "modified" => ["Gemfile", "Gemfile.lock"], "author" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}, "removed" => [], "timestamp" => "2012-09-19T11:17:52-07:00", "url" => "https://github.com/FakeGithubAccount/fakeRepo/commit/a200c3cd16b56a1d0c5062b3860ba862d4cc1602", "id" => "a200c3cd16b56a1d0c5062b3860ba862d4cc1602", "distinct" => true, "message" => "Restore quiet_assets gem", "committer" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}}, {"added" => [], "modified" => ["app/controllers/application_controller.rb", "app/controllers/compare_controller.rb", "config/routes.rb", "spec/controllers/compare_controller_spec.rb"], "author" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}, "removed" => [], "timestamp" => "2012-09-19T11:17:52-07:00", "url" => "https://github.com/FakeGithubAccount/fakeRepo/commit/73a1973dcc09ef85d1d2460a54db71929017d358", "id" => "73a1973dcc09ef85d1d2460a54db71929017d358", "distinct" => true, "message" => "Move compare stuff to compare controller", "committer" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}}, {"added" => [], "modified" => ["app/controllers/compare_controller.rb", "spec/controllers/compare_controller_spec.rb"], "author" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}, "removed" => [], "timestamp" => "2012-09-19T11:17:53-07:00", "url" => "https://github.com/FakeGithubAccount/fakeRepo/commit/d2449f4d3dba3fd7871cc5985af7572850f4c6b3", "id" => "d2449f4d3dba3fd7871cc5985af7572850f4c6b3", "distinct" => true, "message" => "Fix mobile <-> standard redirecting\n\nFixes ZD##{ticket_id}", "committer" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}}, {"added" => [], "modified" => ["config/domains.yml"], "author" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}, "removed" => [], "timestamp" => "2012-09-19T11:17:53-07:00", "url" => "https://github.com/FakeGithubAccount/fakeRepo/commit/5c73ba3a666bda4c6febc6ee6bdf43edcfe1ae2f", "id" => "5c73ba3a666bda4c6febc6ee6bdf43edcfe1ae2f", "distinct" => true, "message" => "Match development compare domain to seeds", "committer" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}}], "before" => "ae9d2c15f00cbf4e7ed9b3acdf171c901d224390", "compare" => "https://github.com/FakeGithubAccount/fakeRepo/compare/ae9d2c15f00c...5c73ba3a666b", "created" => false}, "format" => "json", "controller" => "api/v2/integrations/github", "action" => "create"}
  end

  def multiple_commit_messages_1(t1_id, t2_id)
    {"ticket_id" => t1_id.to_s, "payload" => {"pusher" => {"name" => "external_user", "email" => "external_user@zendesk.zen"}, "repository" => {"name" => "compete", "has_wiki" => true, "size" => 2268, "created_at" => "2011-11-11T10:06:18-08:00", "private" => true, "watchers" => 1, "fork" => false, "url" => "https://github.com/FakeGithubAccount/fakeRepo", "language" => "Ruby", "pushed_at" => "2012-09-19T11:18:30-07:00", "open_issues" => 10, "has_downloads" => true, "homepage" => "", "has_issues" => true, "stargazers" => 1, "forks" => 0, "organization" => "FakeGithubAccount", "description" => "", "owner" => {"name" => "FakeGithubAccount", "email" => nil}}, "forced" => false, "after" => "5c73ba3a666bda4c6febc6ee6bdf43edcfe1ae2f", "head_commit" => {"added" => [], "modified" => ["config/domains.yml"], "author" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}, "removed" => [], "timestamp" => "2012-09-19T11:17:53-07:00", "url" => "https://github.com/FakeGithubAccount/fakeRepo/commit/5c73ba3a666bda4c6febc6ee6bdf43edcfe1ae2f", "id" => "5c73ba3a666bda4c6febc6ee6bdf43edcfe1ae2f", "distinct" => true, "message" => "Match development compare domain to seeds", "committer" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}}, "deleted" => false, "ref" => "refs/heads/master", "commits" => [{"added" => [], "modified" => ["Gemfile", "Gemfile.lock"], "author" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}, "removed" => [], "timestamp" => "2012-09-19T11:17:52-07:00", "url" => "https://github.com/FakeGithubAccount/fakeRepo/commit/a200c3cd16b56a1d0c5062b3860ba862d4cc1602", "id" => "a200c3cd16b56a1d0c5062b3860ba862d4cc1602", "distinct" => true, "message" => "Restore quiet_assets gem", "committer" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}}, {"added" => [], "modified" => ["app/controllers/application_controller.rb", "app/controllers/compare_controller.rb", "config/routes.rb", "spec/controllers/compare_controller_spec.rb"], "author" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}, "removed" => [], "timestamp" => "2012-09-19T11:17:52-07:00", "url" => "https://github.com/FakeGithubAccount/fakeRepo/commit/73a1973dcc09ef85d1d2460a54db71929017d358", "id" => "73a1973dcc09ef85d1d2460a54db71929017d358", "distinct" => true, "message" => "Move compare stuff to compare controller", "committer" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}}, {"added" => [], "modified" => ["app/controllers/compare_controller.rb", "spec/controllers/compare_controller_spec.rb"], "author" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}, "removed" => [], "timestamp" => "2012-09-19T11:17:53-07:00", "url" => "https://github.com/FakeGithubAccount/fakeRepo/commit/d2449f4d3dba3fd7871cc5985af7572850f4c6b3", "id" => "d2449f4d3dba3fd7871cc5985af7572850f4c6b3", "distinct" => true, "message" => "Fix mobile <-> standard redirecting\n\nFixes ZD##{t1_id}", "committer" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}}, {"added" => [], "modified" => ["config/domains.yml"], "author" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}, "removed" => [], "timestamp" => "2012-09-19T11:17:53-07:00", "url" => "https://github.com/FakeGithubAccount/fakeRepo/commit/5c73ba3a666bda4c6febc6ee6bdf43edcfe1ae2f", "id" => "5c73ba3a666bda4c6febc6ee6bdf43edcfe1ae2f", "distinct" => true, "message" => "Match development compare domain to seeds ZD##{t2_id}", "committer" => {"name" => "External User", "username" => "external_user", "email" => "externaluser@zendesk.zen"}}], "before" => "ae9d2c15f00cbf4e7ed9b3acdf171c901d224390", "compare" => "https://github.com/FakeGithubAccount/fakeRepo/compare/ae9d2c15f00c...5c73ba3a666b", "created" => false}, "format" => "json", "controller" => "api/v2/integrations/github", "action" => "create"}
  end

  def commit_comment
    {"payload" => {"comment" => {"position" => nil, "created_at" => "2012-03-21T00:52:07Z", "line" => nil, "body" => "Comment on ZD#380 commit message", "updated_at" => "2012-03-21T00:52:07Z", "commit_id" => "c60e5200f53825843f2a41a905603afe6b51d5f2", "url" => "https://api.github.com/repos/internal_user/testing/comments/1111372", "id" => 1111372, "path" => nil, "html_url" => "https://github.com/internal_user/testing/commit/c60e5200f5#commitcomment-1111372", "user" => {"avatar_url" => "https://zendesk.zen/", "gravatar_id" => "0", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "repository" => {"name" => "testing", "created_at" => "2012-03-02T07:03:16Z", "updated_at" => "2012-03-21T00:51:26Z", "url" => "https://api.github.com/repos/internal_user/testing", "pushed_at" => "2012-03-21T00:51:25Z", "id" => 3599985, "owner" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "sender" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "action" => "create", "ticket_id" => "380", "controller" => "api/v2/integrations/github"}
  end

  def commit_comment_other_user
    {"payload" => {"comment" => {"position" => nil, "created_at" => "2012-03-21T02:40:16Z", "body" => "This is so wrong zd#380", "line" => nil, "commit_id" => "c60e5200f53825843f2a41a905603afe6b51d5f2", "updated_at" => "2012-03-21T02:40:16Z", "url" => "https://api.github.com/repos/internal_user/testing/comments/1111569", "id" => 1111569, "path" => nil, "user" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/eac", "id" => 7752, "login" => "eac"}, "html_url" => "https://github.com/internal_user/testing/commit/c60e5200f5#commitcomment-1111569"}, "repository" => {"name" => "testing", "created_at" => "2012-03-02T07:03:16Z", "updated_at" => "2012-03-21T00:51:26Z", "url" => "https://api.github.com/repos/internal_user/testing", "pushed_at" => "2012-03-21T00:51:25Z", "id" => 3599985, "owner" => {"avatar_url" => "https://zendesk.zen/", "gravatar_id" => "0", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "sender" => {"avatar_url" => "https://zendesk.zen/", "gravatar_id" => "0", "url" => "https://api.github.com/users/eac", "id" => 7752, "login" => "eac"}}, "ticket_id" => "380"}
  end

  def issue_updated
    {"payload" => {"comment" => {"created_at" => "2012-03-21T00:48:54Z", "body" => "An issue comment for ticket ZD#380", "updated_at" => "2012-03-21T00:48:54Z", "url" => "https://api.github.com/repos/internal_user/testing/issues/comments/4608911", "id" => 4608911, "user" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "repository" => {"name" => "testing", "created_at" => "2012-03-02T07:03:16Z", "updated_at" => "2012-03-20T21:39:34Z", "url" => "https://api.github.com/repos/internal_user/testing", "pushed_at" => "2012-03-20T21:39:33Z", "id" => 3599985, "owner" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "sender" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "action" => "created", "issue" => {"number" => 5, "created_at" => "2012-03-21T00:47:24Z", "pull_request" => {"diff_url" => nil, "patch_url" => nil, "html_url" => nil}, "title" => "New issue", "body" => "Mentioning zd#380", "comments" => 2, "updated_at" => "2012-03-21T00:48:54Z", "url" => "https://api.github.com/repos/internal_user/testing/issues/5", "id" => 3738679, "assignee" => nil, "milestone" => nil, "closed_at" => nil, "html_url" => "https://github.com/internal_user/testing/issues/5", "user" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "labels" => [], "state" => "open"}}, "action" => "create", "ticket_id" => "380", "controller" => "api/v2/integrations/github"}
  end

  def issue_created
    {"payload" => {"repository" => {"name" => "testing", "created_at" => "2012-03-02T07:03:16Z", "updated_at" => "2012-03-20T21:39:34Z", "url" => "https://api.github.com/repos/internal_user/testing", "pushed_at" => "2012-03-20T21:39:33Z", "id" => 3599985, "owner" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "sender" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "action" => "opened", "issue" => {"number" => 7, "created_at" => "2012-03-21T00:50:14Z", "pull_request" => {"diff_url" => nil, "patch_url" => nil, "html_url" => nil}, "title" => "Final new issue", "body" => "For ticket ZD#380", "comments" => 0, "updated_at" => "2012-03-21T00:50:14Z", "url" => "https://api.github.com/repos/internal_user/testing/issues/7", "id" => 3738706, "assignee" => nil, "milestone" => nil, "closed_at" => nil, "html_url" => "https://github.com/internal_user/testing/issues/7", "user" => {"avatar_url" => "https://zendesk.zen/", "gravatar_id" => "0", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "labels" => [], "state" => "open"}}, "action" => "create", "ticket_id" => "380", "controller" => "api/v2/integrations/github"}
  end

  def issue_closed
    {"payload" => {"repository" => {"name" => "testing", "created_at" => "2012-03-02T07:03:16Z", "updated_at" => "2012-03-21T00:51:26Z", "url" => "https://api.github.com/repos/internal_user/testing", "pushed_at" => "2012-03-21T00:51:25Z", "id" => 3599985, "owner" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "sender" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "action" => "closed", "issue" => {"number" => 3, "created_at" => "2012-03-21T00:37:03Z", "pull_request" => {"diff_url" => nil, "patch_url" => nil, "html_url" => nil}, "title" => "NEw issue for zd#380", "body" => "Squibbles", "comments" => 0, "updated_at" => "2012-03-21T02:58:52Z", "url" => "https://api.github.com/repos/internal_user/testing/issues/3", "id" => 3738577, "assignee" => nil, "milestone" => nil, "closed_at" => "2012-03-21T02:58:52Z", "html_url" => "https://github.com/internal_user/testing/issues/3", "user" => {"avatar_url" => "https://zendesk.zen/", "gravatar_id" => "0", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "labels" => [], "state" => "closed"}}, "action" => "create", "ticket_id" => "380", "controller" => "api/v2/integrations/github"}
  end

  def issue_close_with_comment
    {"payload" => {"comment" => {"created_at" => "2012-03-21T03:10:28Z", "body" => "Closing with a comment", "updated_at" => "2012-03-21T03:10:28Z", "url" => "https://api.github.com/repos/internal_user/testing/issues/comments/4610203", "id" => 4610203, "user" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "repository" => {"name" => "testing", "created_at" => "2012-03-02T07:03:16Z", "updated_at" => "2012-03-21T00:51:26Z", "url" => "https://api.github.com/repos/internal_user/testing", "id" => 3599985, "pushed_at" => "2012-03-21T00:51:25Z", "owner" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "sender" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "action" => "created", "issue" => {"number" => 5, "created_at" => "2012-03-21T00:47:24Z", "pull_request" => {"diff_url" => nil, "patch_url" => nil, "html_url" => nil}, "title" => "New issue", "body" => "Mentioning zd#380", "comments" => 4, "updated_at" => "2012-03-21T03:10:28Z", "url" => "https://api.github.com/repos/internal_user/testing/issues/5", "id" => 3738679, "assignee" => nil, "milestone" => nil, "closed_at" => "2012-03-21T03:10:28Z", "user" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "labels" => [], "html_url" => "https://github.com/internal_user/testing/issues/5", "state" => "closed"}}, "action" => "create", "ticket_id" => "380", "controller" => "api/v2/integrations/github"}
  end

  def pull_request
    {"payload" => {"number" => 1593, "pull_request" => {"issue_url" => "https://github.com/FakeGithubAccount/fakeRepo/issues/1593", "number" => 1593, "head" => {"label" => "fakeRepo:example1/forum_count_link", "repo" => {"master_branch" => "master", "name" => "fakeRepo", "created_at" => "2008-09-07T16:49:58Z", "has_wiki" => true, "size" => 8292, "clone_url" => "https://github.com/FakeGithubAccount/fakeRepo.git", "updated_at" => "2012-05-29T18:03:41Z", "private" => true, "watchers" => 13, "ssh_url" => "git@github.com:FakeGithubAccount/fakeRepo.git", "git_url" => "git://github.com/FakeGithubAccount/fakeRepo.git", "language" => "Ruby", "url" => "https://api.github.com/repos/FakeGithubAccount/fakeRepo", "fork" => false, "pushed_at" => "2012-05-29T18:03:41Z", "svn_url" => "https://github.com/FakeGithubAccount/fakeRepo", "id" => 50179, "mirror_url" => nil, "open_issues" => 92, "has_downloads" => true, "homepage" => "http://zendesk.zen/", "has_issues" => true, "full_name" => "FakeGithubAccount/fakeRepo", "forks" => 2, "description" => "The fakeRepo application", "html_url" => "https://github.com/FakeGithubAccount/fakeRepo", "owner" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/fakeRepo", "id" => 11525, "login" => "fakeRepo"}}, "sha" => "2f209808f5ec9e9371996d7dbb853f66a108f3d9", "ref" => "example1/forum_count_link", "user" => {"avatar_url" => "https://zendesk.zen/", "gravatar_id" => "0", "url" => "https://api.github.com/users/fakeRepo", "id" => 11525, "login" => "fakeRepo"}}, "merged" => true, "merged_by" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/example2", "id" => 1045450, "login" => "example2"}, "changed_files" => 2, "created_at" => "2012-05-22T17:55:35Z", "title" => "Restore link in forum count", "comments" => 1, "body" => "ZD#216973\n\nI have restore the functionality where you can click to see who is a subscriptor of a forum.\n\nCC/ @example2 ", "additions" => 34, "updated_at" => "2012-05-29T18:03:41Z", "diff_url" => "https://github.com/FakeGithubAccount/fakeRepo/pull/1593.diff", "_links" => {"html" => {"href" => "https://github.com/FakeGithubAccount/fakeRepo/pull/1593"}, "self" => {"href" => "https://api.github.com/repos/FakeGithubAccount/fakeRepo/pulls/1593"}, "comments" => {"href" => "https://api.github.com/repos/FakeGithubAccount/fakeRepo/issues/1593/comments"}, "issue" => {"href" => "https://api.github.com/repos/FakeGithubAccount/fakeRepo/issues/1593"}, "review_comments" => {"href" => "https://api.github.com/repos/FakeGithubAccount/fakeRepo/pulls/1593/comments"}}, "url" => "https://api.github.com/repos/FakeGithubAccount/fakeRepo/pulls/1593", "id" => 1404066, "patch_url" => "https://github.com/FakeGithubAccount/fakeRepo/pull/1593.patch", "mergeable" => nil, "commits" => 1, "merged_at" => "2012-05-29T18:03:41Z", "closed_at" => "2012-05-29T18:03:41Z", "deletions" => 15, "html_url" => "https://github.com/FakeGithubAccount/fakeRepo/pull/1593", "user" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/anexample1", "id" => 1413356, "login" => "anexample1"}, "review_comments" => 0, "base" => {"label" => "fakeRepo:master", "repo" => {"master_branch" => "master", "name" => "fakeRepo", "created_at" => "2008-09-07T16:49:58Z", "has_wiki" => true, "size" => 8292, "clone_url" => "https://github.com/FakeGithubAccount/fakeRepo.git", "updated_at" => "2012-05-29T18:03:41Z", "private" => true, "watchers" => 13, "ssh_url" => "git@github.com:FakeGithubAccount/fakeRepo.git", "git_url" => "git://github.com/FakeGithubAccount/fakeRepo.git", "language" => "Ruby", "url" => "https://api.github.com/repos/FakeGithubAccount/fakeRepo", "fork" => false, "pushed_at" => "2012-05-29T18:03:41Z", "svn_url" => "https://github.com/FakeGithubAccount/fakeRepo", "id" => 50179, "mirror_url" => nil, "open_issues" => 92, "has_downloads" => true, "homepage" => "http://zendesk.zen/", "has_issues" => true, "full_name" => "FakeGithubAccount/fakeRepo", "forks" => 2, "description" => "The fakeRepo application", "html_url" => "https://github.com/FakeGithubAccount/fakeRepo", "owner" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/fakeRepo", "id" => 11525, "login" => "fakeRepo"}}, "sha" => "307386750d3c0987bb4b32224bd5a059571a3dcf", "ref" => "master", "user" => {"avatar_url" => "https://zendesk.zen/", "gravatar_id" => "0", "url" => "https://api.github.com/users/fakeRepo", "id" => 11525, "login" => "fakeRepo"}}, "state" => "closed"}, "repository" => {"name" => "fakeRepo", "master_branch" => "master", "size" => 8292, "has_wiki" => true, "created_at" => "2008-09-07T16:49:58Z", "clone_url" => "https://github.com/FakeGithubAccount/fakeRepo.git", "watchers" => 13, "private" => true, "updated_at" => "2012-05-29T18:03:42Z", "git_url" => "git://github.com/FakeGithubAccount/fakeRepo.git", "url" => "https://api.github.com/repos/FakeGithubAccount/fakeRepo", "ssh_url" => "git@github.com:FakeGithubAccount/fakeRepo.git", "fork" => false, "language" => "Ruby", "id" => 50179, "pushed_at" => "2012-05-29T18:03:42Z", "svn_url" => "https://github.com/FakeGithubAccount/fakeRepo", "mirror_url" => nil, "has_downloads" => true, "open_issues" => 92, "has_issues" => true, "homepage" => "http://zendesk.zen/", "full_name" => "FakeGithubAccount/fakeRepo", "description" => "The fakeRepo application", "forks" => 2, "html_url" => "https://github.com/FakeGithubAccount/fakeRepo", "owner" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/fakeRepo", "id" => 11525, "login" => "fakeRepo"}}, "sender" => {"avatar_url" => "https://zendesk.zen/", "gravatar_id" => "0", "url" => "https://api.github.com/users/example2", "id" => 1045450, "login" => "example2"}, "action" => "closed"}, "ticket_id" => "216973"}
  end

  def parse_payload(payload, ticket_id = 1)
    Api::V2::Integrations::GithubController::GithubPayload.new(payload: payload["payload"], ticket_id: ticket_id).payloads
  end

  def issue_tagged
    {"payload" => {"type" => "issue", "action" => "labeled", "comment" => {"created_at" => "2012-03-21T00:48:54Z", "body" => "An issue comment for ticket ZD#380", "updated_at" => "2012-03-21T00:48:54Z", "url" => "https://api.github.com/repos/internal_user/testing/issues/comments/4608911", "id" => 4608911, "user" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "repository" => {"name" => "testing", "created_at" => "2012-03-02T07:03:16Z", "updated_at" => "2012-03-20T21:39:34Z", "url" => "https://api.github.com/repos/internal_user/testing", "pushed_at" => "2012-03-20T21:39:33Z", "id" => 3599985, "owner" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}}, "label" => {"name" => "label1"}, "sender" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "issue" => {"number" => 5, "created_at" => "2012-03-21T00:47:24Z", "pull_request" => {"diff_url" => nil, "patch_url" => nil, "html_url" => nil}, "title" => "New issue", "body" => "Mentioning zd#380", "comments" => 2, "updated_at" => "2012-03-21T00:48:54Z", "url" => "https://api.github.com/repos/internal_user/testing/issues/5", "id" => 3738679, "assignee" => nil, "milestone" => nil, "closed_at" => nil, "html_url" => "https://github.com/internal_user/testing/issues/5", "user" => {"gravatar_id" => "0", "avatar_url" => "https://zendesk.zen/", "url" => "https://api.github.com/users/internal_user", "id" => 14324, "login" => "internal_user"}, "labels" => [], "state" => "open"}}}
  end
end
