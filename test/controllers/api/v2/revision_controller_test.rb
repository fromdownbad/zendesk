require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Api::V2::RevisionController do
  extend Api::V2::TestHelper

  before do
    @controller.stubs(:sha_from_file).returns("hello")
    accept :json
  end

  with_options(controller: "api/v2/revision") do |request|
    request.should_route :get, "/api/v2/revision", action: "index"
  end

  as_an_anonymous_user do
    describe "a GET to #index" do
      before { get :index }
      it('responds with ok') { assert_response :ok }

      it "returns a SHA" do
        response = JSON.parse(@response.body)
        assert_equal "hello", response["sha"]
      end
    end
  end
end
