require_relative '../../../support/test_helper'
require_relative '../../../support/pre_account_creation_helper'

SingleCov.covered! uncovered: 11

describe Api::V2::AccountsController do
  extend Api::V2::TestHelper
  include PreAccountCreationHelper

  fixtures :accounts, :users, :organizations, :translation_locales, :role_settings

  let(:new_account) { accounts(:minimum) }
  let(:account_subdomain) { "wombatsnowboards#{(rand * 10000).round}" }
  let(:account_name) { 'Wombat Snowboards' }
  let(:language) { 'ja' }
  let(:account_params) do
    {
      'account' => {
        'name'           => account_name,
        'subdomain'      => account_subdomain,
        'help_desk_size' => 'Small Team',
        'source'         => 'classic',
        'language'       => language
      },
      'owner' => owner_params,
      'address' => {
        'phone' => '+1-123-456-7890'
      },
      'trial_extras' => {}
    }
  end
  let(:owner_params) do
    {
      'name'  => 'Sir Wombat',
      'email' => 'wbsnowboards@tz.com'
    }
  end
  let(:params_subdomain) { account_params['account']['subdomain'] }
  let(:account) { Account.find_by_subdomain(params_subdomain) }
  let(:kragle_client) { stub }
  let(:creation_channel) { 'Admin Man2' }
  let(:success_response) { stub(success?: true, body: { status: 200, success: 'true'}) }
  let(:statsd_client) { @controller.send(:statsd_client) }

  before do
    Timecop.freeze
    Rails.logger.stubs(:info)
    accept :json
    AccountCreationShard.stubs(:pluck).returns([1])
    AccountsPrecreationJob.stubs(:enqueue)
  end

  with_options(controller: 'api/v2/accounts') do |request|
    request.should_route :post, '/api/v2/accounts',           action: 'create'
    request.should_route :get,  '/api/v2/accounts/available', action: 'available'
  end

  describe 'GET :available' do
    describe 'domain is available and valid' do
      before do
        Zendesk::RoutingValidations.expects(:subdomain_available?).returns(true)
        get :available, params: { subdomain: account_subdomain }
      end

      it 'succeeds' do
        assert_response :success
        assert JSON.parse(@response.body)['success']
      end
    end

    describe 'domain is invalid or unavailable' do
      before do
        Zendesk::RoutingValidations.expects(:subdomain_available?).returns(false)
        get :available, params: { subdomain: account_subdomain }
      end

      it 'returns false' do
        assert_response :success
        refute JSON.parse(@response.body)['success']
      end
    end
  end

  describe 'POST :create' do
    let(:account_subdomain) { 'z3nsomethingtest' }
    let(:token_generator) { accounts(:minimum) }
    let(:scopes) { ["accounts:write"] }
    let(:token)  { stub(id: 2, account_id: token_generator.id, user: token_generator.owner, scopes: scopes) }

    before do
      token_generator.update_column(:subdomain, 'zendeskaccounts')
      @controller.stubs(:oauth_access_token).returns(token)
      @request.account = token_generator
      @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token
      @controller.send(:current_user=, token.user)
    end

    describe 'with_arturo_enabled :ocp_support_shell_account_creation' do
      let(:expected_trial_extras) do
        {
          'trial_extras' => {
            'confirm_origin' => 'zendesk_internal_origin',
            'creation_channel' => creation_channel,
            'original_remote_ip' => '0.0.0.0',
            'source' => 'classic'
          }
        }
      end

      before do
        Arturo.enable_feature!(:ocp_support_shell_account_creation)
        @controller.stubs(:internal_account_service_client).returns(kragle_client)
      end

      it 'adds the expected trial_extras' do
        kragle_client.expects(:post).with('api/services/accounts', has_entry(expected_trial_extras)).returns(success_response)
        post :create, params: account_params
      end

      describe 'suite' do
        let(:expected_trial_extras) do
          {
            'trial_extras' => {
              'confirm_origin' => 'zendesk_internal_origin',
              'creation_channel' => creation_channel,
              'original_remote_ip' => '0.0.0.0',
              'source' => 'classic',
              'suite_trial' => true
            }
          }
        end

        before do
          account_params['account']['suite_trial'] = true
        end

        it 'adds the expected trial_extras' do
          kragle_client.expects(:post).with('api/services/accounts', has_entry(expected_trial_extras)).returns(success_response)
          post :create, params: account_params
        end
      end

      it 'maps the pod_id param correctly' do
        account_params['force_pod_id'] = '998'
        pod_id_param = { 'pod_id' => '998' }
        kragle_client.expects(:post).with('api/services/accounts', has_entry(pod_id_param)).returns(success_response)
        post :create, params: account_params
      end

      describe 'account params' do
        let(:expected_param) do
          {
            'account' => {
              'time_zone'      => 'Bangkok',
              'utc_offset'     => 7,
              'name'           => account_name,
              'subdomain'      => account_subdomain,
              'help_desk_size' => 'Small Team',
              'source'         => 'classic',
              'language'       => language,
              'locale'         => language
            }
          }
        end

        it 'maps the time_zone param correctly' do
          account_params['account']['utc_offset'] = '7'
          kragle_client.expects(:post).with('api/services/accounts', has_entry(expected_param)).returns(success_response)
          post :create, params: account_params
        end
      end
    end
  end

  describe 'POST :create' do
    describe 'with_arturo_enabled :ocp_support_shell_account_creation' do
      before do
        Arturo.enable_feature!(:ocp_support_shell_account_creation)
      end

      describe 'unauthenticated request' do
        before do
          Rails.logger.expects(:info).with("[ApiV2AccountsCreation] FORBIDDEN oauth_account_creation ON so failure")
          statsd_client.expects(:increment).with('oauth_create.invalid')
          statsd_client.expects(:increment).with('create_action', anything)
        end

        it 'is forbidden' do
          post :create, params: account_params
          assert_response :forbidden
        end
      end

      describe 'when oauth token is present for current account' do
        let(:token_generator) { accounts(:minimum) }
        let(:owner) { token_generator.owner }
        let(:token) do
          stub(id: 1, account_id: token_generator.id, user: owner, scopes: ["accounts:write"])
        end
        before do
          @controller.stubs(:oauth_access_token).returns(token)
          @request.account = token_generator
          @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token
          @controller.send(:current_user=, token.user)
        end

        describe 'and current account does not have "zendeskaccounts" subdomain' do
          before do
            Rails.logger.expects(:info).with("[ApiV2AccountsCreation] FORBIDDEN INVALID oauth_token id: #{token.id}")
            statsd_client.expects(:increment).with('oauth_create.invalid.token', tags: ["token:#{token.id}"])
            statsd_client.expects(:increment).with('create_action', anything)
          end

          it 'is forbidden' do
            post :create, params: account_params
            assert_response :forbidden
          end
        end

        describe 'and current account have "zendeskaccounts" subdomain' do
          let(:token_generator) { accounts(:minimum) }
          let(:token) do
            stub(id: 2, account_id: token_generator.id, user: token_generator.owner, scopes: scopes)
          end
          before do
            token_generator.update_column(:subdomain, 'zendeskaccounts')
            @controller.stubs(:oauth_access_token).returns(token)
            @request.account = token_generator
            @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token
            @controller.send(:current_user=, token.user)
          end

          describe 'with invalid scope write' do
            let(:scopes) { ['write'] }
            before do
              Rails.logger.expects(:info).with("[ApiV2AccountsCreation] FORBIDDEN INVALID oauth_token id: #{token.id}")
              statsd_client.expects(:increment).with('oauth_create.invalid.token', tags: ["token:#{token.id}"])
              statsd_client.expects(:increment).with('create_action', anything)
            end

            it 'is forbidden' do
              post :create, params: account_params
              assert_response 403
            end
          end

          describe 'with valid scope account_creation' do
            let(:scopes) { ["accounts:write"] }

            describe 'uses Redis to prevent concurrent requests for the same subdomain' do
              let(:account_service_kragle_client) { stub }
              let(:account_service_success_response) { stub(success?: true, body: { 'account' => { 'subdomain' => 'blah' }, status: 200, success: 'true'}) }
              let(:classic_kragle_client) { stub }
              let(:classic_kragle_response) { success_response }

              before do
                @controller.stubs(:internal_account_service_client).returns(account_service_kragle_client)
                @controller.stubs(:internal_api_account_create_client).returns(classic_kragle_client)
                account_service_kragle_client.stubs(:post).returns(account_service_success_response)
                classic_kragle_client.stubs(:post).returns(classic_kragle_response)
              end

              describe 'account subdomain is prefixed with z3n' do
                let(:account_subdomain) { 'z3nsomethingtest' }

                it 'creates account through account service' do
                  account_service_kragle_client.expects(:post).returns(account_service_success_response)
                  post :create, params: account_params
                  assert_response :success
                end

                describe 'account explicitly stated as not multiproduct' do
                  before do
                    account_params['account'].merge!('multiproduct' => '0')
                  end

                  it 'creates account through Classic' do
                    classic_kragle_client.expects(:post).returns(classic_kragle_response)
                    post :create, params: account_params
                    assert_response :success
                  end
                end
              end

              describe 'phased rollout' do
                describe 'phase 1' do
                  before do
                    Arturo.enable_feature!(:ocp_support_shell_account_creation_phase1)
                  end

                  describe 'inside rollout' do
                    let(:account_subdomain) { 'abc' }

                    it 'creates account through account service' do
                      account_service_kragle_client.expects(:post).returns(account_service_success_response)
                      post :create, params: account_params
                      assert_response :success
                    end
                  end

                  describe 'outside rollout' do
                    let(:account_subdomain) { 'ghi' }

                    it 'creates account through Classic' do
                      classic_kragle_client.expects(:post).returns(classic_kragle_response)
                      post :create, params: account_params
                      assert_response :success
                    end
                  end
                end

                describe 'phase 2' do
                  before do
                    Arturo.enable_feature!(:ocp_support_shell_account_creation_phase2)
                  end

                  describe 'inside rollout' do
                    let(:account_subdomain) { 'ghi' }

                    it 'creates account through account service' do
                      account_service_kragle_client.expects(:post).returns(account_service_success_response)
                      post :create, params: account_params
                      assert_response :success
                    end
                  end

                  describe 'outside rollout' do
                    let(:account_subdomain) { 'pqrs' }

                    it 'creates account through Classic' do
                      classic_kragle_client.expects(:post).returns(classic_kragle_response)
                      post :create, params: account_params
                      assert_response :success
                    end
                  end
                end

                describe 'complete' do
                  before do
                    Arturo.enable_feature!(:ocp_support_shell_account_creation_all)
                  end

                  describe 'phase 1' do
                    let(:account_subdomain) { 'abc' }

                    it 'creates account through account service' do
                      account_service_kragle_client.expects(:post).returns(account_service_success_response)
                      post :create, params: account_params
                      assert_response :success
                    end
                  end

                  describe 'phase 2' do
                    let(:account_subdomain) { 'ghi' }

                    it 'creates account through account service' do
                      account_service_kragle_client.expects(:post).returns(account_service_success_response)
                      post :create, params: account_params
                      assert_response :success
                    end
                  end

                  describe 'the rest' do
                    let(:account_subdomain) { 'pqrs' }

                    it 'creates account through account service' do
                      account_service_kragle_client.expects(:post).returns(account_service_success_response)
                      post :create, params: account_params
                      assert_response :success
                    end
                  end
                end
              end

              describe 'when requests account creation for same subdomain again' do
                it 'returns failure status' do
                  post :create, params: account_params
                  assert_response :success

                  post :create, params: account_params
                  assert_response :conflict
                  refute JSON.parse(@response.body)['success']
                end
              end

              describe 'when requesting account creation for the same G Suite domain again' do
                it 'returns failure status' do
                  account_params['account']['google_apps_domain'] = 'googledomain.com'
                  post :create, params: account_params
                  assert_response :success

                  post :create, params: account_params
                  assert_response :conflict
                  refute JSON.parse(@response.body)['success']
                end
              end

              describe 'when requesting account creation with "gmail.com" as G Suite domain' do
                it 'returns failure status' do
                  account_params['account']['google_apps_domain'] = 'gmail.com'
                  post :create, params: account_params
                  assert_response :conflict
                  refute JSON.parse(@response.body)['success']
                end
              end
            end

            describe "with invalid params" do
              let(:invalid_params) do
                {
                  account: {
                    name: account_name,
                    subdomain: "testsub#{(rand * 1000).round}",
                    help_desk_size: "Small team",
                    source: Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET,
                    language: "pt-BR"
                  },
                  owner: {
                    name: "Yolo Gucci",
                    email: "yolo@snacks.com",
                    password: "yolo@snacks.com"
                  },
                  address: {
                    phone: "+1-123-456-7890"
                  }
                }
              end

              describe 'missing subdomain' do
                let(:account_subdomain) { nil }
                before { post :create, params: account_params }
                it 'fails with conflict' do
                  assert_response :conflict
                  refute JSON.parse(@response.body)['success']
                end
              end

              describe 'when owner has invalid email' do
                let(:owner_params) do
                  {
                    'name'  => 'Sir Wombat',
                    'email' => 'wbsnowboards'
                  }
                end

                it 'returns 422 UnprocessableEntity' do
                  post :create, params: account_params

                  assert_response :unprocessable_entity
                  assert @response.body =~ /Owner email wbsnowboards is not properly formatted/
                end
              end

              describe 'owner name params is missing' do
                let(:owner_params) do
                  {
                    'email' => 'wbsnowboards@tz.com'
                  }
                end

                it 'returns 422 UnprocessableEntity' do
                  post :create, params: account_params

                  @controller.expects(:internal_api_account_create_client).never
                  assert_response :unprocessable_entity
                  assert @response.body =~ /Owner name can't be blank/
                end
              end

              describe 'owner name params is empty string' do
                let(:owner_params) do
                  {
                    'name'  => '',
                    'email' => 'wbsnowboards@tz.com'
                  }
                end

                it 'returns 422 UnprocessableEntity' do
                  post :create, params: account_params

                  @controller.expects(:internal_api_account_create_client).never
                  assert_response :unprocessable_entity
                  assert @response.body =~ /Owner name can't be blank/
                end
              end
            end

            describe "with request coming from one of the restricted countries" do
              before do
                Zendesk::GeoLocation.stubs(:locate).returns(country_code: 'CU', region: 'Americas')
              end

              it "does not create the account" do
                post :create, params: account_params
                assert @response.body =~ /You appear to be in a Prohibited Jurisdiction under our/
                assert_nil Account.find_by_name("A Company Name")
              end
            end
          end
        end
      end
    end

    describe 'without_arturo_enabled :ocp_support_shell_account_creation' do
      before do
        Arturo.disable_feature!(:ocp_support_shell_account_creation)
      end

      describe 'unauthenticated request' do
        before do
          Rails.logger.expects(:info).with("[ApiV2AccountsCreation] FORBIDDEN oauth_account_creation ON so failure")
          statsd_client.expects(:increment).with('oauth_create.invalid')
          statsd_client.expects(:increment).with('create_action', anything)
        end

        it 'is forbidden' do
          post :create, params: account_params
          assert_response :forbidden
        end
      end

      describe 'when oauth token is present for current account' do
        let(:token_generator) { accounts(:minimum) }
        let(:owner) { token_generator.owner }
        let(:token) do
          stub(id: 1, account_id: token_generator.id, user: owner, scopes: ["accounts:write"])
        end
        before do
          @controller.stubs(:oauth_access_token).returns(token)
          @request.account = token_generator
          @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token
          @controller.send(:current_user=, token.user)
        end

        describe 'and current account does not have "zendeskaccounts" subdomain' do
          before do
            Rails.logger.expects(:info).with("[ApiV2AccountsCreation] FORBIDDEN INVALID oauth_token id: #{token.id}")
            statsd_client.expects(:increment).with('oauth_create.invalid.token', tags: ["token:#{token.id}"])
            statsd_client.expects(:increment).with('create_action', anything)
          end

          it 'is forbidden' do
            post :create, params: account_params
            assert_response :forbidden
          end
        end

        describe 'and current account have "zendeskaccounts" subdomain' do
          let(:token_generator) { accounts(:minimum) }
          let(:token) do
            stub(id: 2, account_id: token_generator.id, user: token_generator.owner, scopes: scopes)
          end
          before do
            token_generator.update_column(:subdomain, 'zendeskaccounts')
            @controller.stubs(:oauth_access_token).returns(token)
            @request.account = token_generator
            @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token
            @controller.send(:current_user=, token.user)
          end

          describe 'with invalid scope write' do
            let(:scopes) { ['write'] }
            before do
              Rails.logger.expects(:info).with("[ApiV2AccountsCreation] FORBIDDEN INVALID oauth_token id: #{token.id}")
              statsd_client.expects(:increment).with('oauth_create.invalid.token', tags: ["token:#{token.id}"])
              statsd_client.expects(:increment).with('create_action', anything)
            end

            it 'is forbidden' do
              post :create, params: account_params
              assert_response 403
            end
          end

          describe 'with valid scope account_creation' do
            let(:scopes) { ["accounts:write"] }

            describe 'uses Redis to prevent concurrent requests for the same subdomain' do
              let(:kragle_response) { success_response }

              before do
                @controller.stubs(:internal_api_account_create_client).returns(kragle_client)
                kragle_client.stubs(:post).returns(kragle_response)
              end

              describe 'when requests account creation for same subdomain again' do
                it 'returns failure status' do
                  post :create, params: account_params
                  assert_response :success

                  post :create, params: account_params
                  assert_response :conflict
                  refute JSON.parse(@response.body)['success']
                end
              end

              describe 'when requesting account creation for the same G Suite domain again' do
                it 'returns failure status' do
                  account_params['account']['google_apps_domain'] = 'googledomain.com'
                  post :create, params: account_params
                  assert_response :success

                  post :create, params: account_params
                  assert_response :conflict
                  refute JSON.parse(@response.body)['success']
                end
              end

              describe 'when requesting account creation with "gmail.com" as G Suite domain' do
                it 'returns failure status' do
                  account_params['account']['google_apps_domain'] = 'gmail.com'
                  post :create, params: account_params
                  assert_response :conflict
                  refute JSON.parse(@response.body)['success']
                end
              end
            end

            describe "with invalid params" do
              let(:invalid_params) do
                {
                  account: {
                    name: account_name,
                    subdomain: "testsub#{(rand * 1000).round}",
                    help_desk_size: "Small team",
                    source: Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET,
                    language: "pt-BR"
                  },
                  owner: {
                    name: "Yolo Gucci",
                    email: "yolo@snacks.com",
                    password: "yolo@snacks.com"
                  },
                  address: {
                    phone: "+1-123-456-7890"
                  }
                }
              end

              describe 'missing subdomain' do
                let(:account_subdomain) { nil }
                before { post :create, params: account_params }
                it 'fails with conflict' do
                  assert_response :conflict
                  refute JSON.parse(@response.body)['success']
                end
              end

              describe 'when owner has invalid email' do
                let(:owner_params) do
                  {
                    'name'  => 'Sir Wombat',
                    'email' => 'wbsnowboards'
                  }
                end

                it 'returns 422 UnprocessableEntity' do
                  post :create, params: account_params

                  assert_response :unprocessable_entity
                  assert @response.body =~ /Owner email wbsnowboards is not properly formatted/
                end
              end

              describe 'owner name params is missing' do
                let(:owner_params) do
                  {
                    'email' => 'wbsnowboards@tz.com'
                  }
                end

                it 'returns 422 UnprocessableEntity' do
                  post :create, params: account_params

                  @controller.expects(:internal_api_account_create_client).never
                  assert_response :unprocessable_entity
                  assert @response.body =~ /Owner name can't be blank/
                end
              end

              describe 'owner name params is empty string' do
                let(:owner_params) do
                  {
                    'name'  => '',
                    'email' => 'wbsnowboards@tz.com'
                  }
                end

                it 'returns 422 UnprocessableEntity' do
                  post :create, params: account_params

                  @controller.expects(:internal_api_account_create_client).never
                  assert_response :unprocessable_entity
                  assert @response.body =~ /Owner name can't be blank/
                end
              end
            end

            describe "with request coming from one of the restricted countries" do
              before do
                Zendesk::GeoLocation.stubs(:locate).returns(country_code: 'CU', region: 'Americas')
              end

              it "does not create the account" do
                post :create, params: account_params
                assert @response.body =~ /You appear to be in a Prohibited Jurisdiction under our/
                assert_nil Account.find_by_name("A Company Name")
              end
            end
          end
        end
      end
    end
  end
end
