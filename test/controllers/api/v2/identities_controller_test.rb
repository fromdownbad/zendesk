require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::IdentitiesController do
  include DomainEventsHelper
  extend Api::V2::TestHelper
  extend ArturoTestHelper
  fixtures :accounts, :users, :user_identities, :role_settings, :recipient_addresses

  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  before do
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
    @end_user = users(:minimum_end_user)
    @account  = @end_user.account
    @identity = @end_user.identities.email.first
    accept :json
  end

  with_options(controller: 'api/v2/identities') do |request|
    request.should_route :post,   '/api/v2/users/123/identities',       action: 'create',  user_id: '123'
    request.should_route :get,    '/api/v2/users/123/identities',       action: 'index',   user_id: '123'
    request.should_route :get,    '/api/v2/users/123/identities/count', action: 'count',   user_id: '123'
    request.should_route :get,    '/api/v2/users/123/identities/456',   action: 'show',    user_id: '123', id: '456'
    request.should_route :put,    '/api/v2/users/123/identities/456',   action: 'update',  user_id: '123', id: '456'
    request.should_route :delete, '/api/v2/users/123/identities/456',   action: 'destroy', user_id: '123', id: '456'
    request.should_route :put,    '/api/v2/users/123/identities/456/verify',               action: 'verify',               user_id: '123', id: '456'
    request.should_route :put,    '/api/v2/users/123/identities/456/make_primary',         action: 'make_primary',         user_id: '123', id: '456'
    request.should_route :put,    '/api/v2/users/123/identities/456/request_verification', action: 'request_verification', user_id: '123', id: '456'
  end

  self::ALL_ACTIONS = [
    [:get, :index, {}],
    [:post, :create, {}],
    [:get, :show, {id: 1}],
    [:put, :make_primary, {id: 1}],
    [:put, :update, {id: 1}],
    [:delete, :destroy, {id: 1}],
    [:put, :verify, {id: 1}],
    [:put, :request_verification, {id: 1}]
  ].map { |a, b, c| [a, b, c.merge(user_id: 1)] }

  as_an_anonymous_user do
    should_be_unauthorized *self::ALL_ACTIONS
  end

  as_an_end_user do
    should_be_forbidden *self::ALL_ACTIONS
  end

  as_an_agent do
    describe "a GET to :index" do
      describe "with a user_id" do
        before do
          UserForeignIdentity.create!(user: @end_user, account: @end_user.account, value: 123456)
          get :index, params: { user_id: @end_user.id }
        end

        should_use_presenter Api::V2::IdentityPresenter, status: :ok

        it "does not list foreign identites" do
          identities = JSON.parse(@response.body)["identities"]
          assert identities.any?
          refute identities.any? { |i| i["type"] == "foreign" }
        end

        it "sets cache headers based on user" do
          etag = @response.headers["ETag"]
          last = Time.parse(@response.headers["Last-Modified"])
          assert_equal @end_user.updated_at.to_i, last.to_i
          assert etag.present?
        end

        it 'should include the default limit and ordering params for cursor based pagination' do
          get :index, params: { user_id: @end_user.id, page: { size: 100 } }
          assert_response :ok
          json_response = JSON.parse(response.body)
          assert json_response['identities'].size >= 1
          assert_not_nil json_response['links']['prev']
          assert_not_nil json_response['links']['next']
        end

        describe 'default sort parameter of id for v2' do
          it 'generates the correct order clause' do
            assert_sql_queries(1, /ORDER BY `user_identities`.`id` DESC/) do
              get :index, params: { user_id: @end_user.id, page: { size: 100 }, sort: '-id' }
            end

            assert_sql_queries(1, /ORDER BY `user_identities`.`priority` ASC/) do
              get :index, params: { user_id: @end_user.id, page: { size: 100 }, sort: 'priority' }
            end
          end

          it 'shows an error message for invalid sort options' do
            get :index, params: { user_id: @end_user.id, page: { size: 100 }, sort: '-invalid' }
            assert_response :bad_request
            assert @response.body =~ /sort is not valid/
          end
        end

        describe_with_arturo_enabled :cursor_pagination_identities_index do
          it 'should default to cursor pagination when no params are present' do
            get :index, params: { user_id: @end_user.id }
            assert_response :ok
            json_response = JSON.parse(response.body)
            assert json_response['identities'].size >= 1
            assert_not_nil json_response['links']['prev']
            assert_not_nil json_response['links']['next']
          end

          it 'should use offset pagination when offset pagination params are present' do
            get :index, params: { user_id: @end_user.id, per_page: 100 }
            assert_response :ok
            json_response = JSON.parse(response.body)
            assert json_response['identities'].size >= 1
            assert_equal 2, (json_response.keys & ['next_page', 'previous_page']).size
          end
        end

        describe_with_arturo_enabled :remove_offset_pagination_identities_index do
          it 'should use cursor pagination' do
            get :index, params: { user_id: @end_user.id, page: { size: 100 } }
            assert_response :ok
            json_response = JSON.parse(response.body)
            assert json_response['identities'].size >= 1
            assert_not_nil json_response['links']['prev']
            assert_not_nil json_response['links']['next']
          end

          it 'should use cursor pagination when offset pagination params are present' do
            get :index, params: { user_id: @end_user.id, per_page: 100 }
            assert_response :ok
            json_response = JSON.parse(response.body)
            assert json_response['identities'].size >= 1
            assert_not_nil json_response['links']['prev']
            assert_not_nil json_response['links']['next']
          end
        end
      end

      describe "with a invalid user_id" do
        before { get :index, params: { user_id: 999 } }
        it('responds with not_found') { assert_response :not_found }
      end
    end

    describe "a GET to :show" do
      before do
        get :show, params: { user_id: @end_user.id, id: @identity.id }
      end
      should_use_presenter Api::V2::IdentityPresenter, status: :ok
    end

    describe "a PUT to :update" do
      describe "for an unverified identity" do
        before do
          @identity.update_attribute(:is_verified, false)
          refute @identity.reload.is_verified?
        end

        describe "when the account owner tries to verify their own identity" do
          before do
            @account.owner = @warden.user
            @account.save!(validate: false)
            @owner_identity = @account.owner.identities.email.first
            @owner_identity.update_attributes(is_verified: false)
            refute @owner_identity.reload.is_verified?
          end

          it "does not verify the identity" do
            put :update, params: { user_id: @account.owner_id, id: @owner_identity.id, identity: { verified: true } }
            assert_equal false, @owner_identity.reload.is_verified?
          end
        end

        describe "to update verified" do
          before do
            put :update, params: { user_id: @end_user.id, id: @identity.id, identity: { verified: true } }
          end

          should_use_presenter Api::V2::IdentityPresenter, status: :ok

          it "sets verified but does not change value" do
            assert @identity.reload.is_verified?
            assert_equal @end_user.identities.email.first.value, @identity.value
          end
        end

        describe "to update value" do
          before do
            put :update, params: { user_id: @end_user.id, id: @identity.id, identity: { value: "skipper@example.org" } }
          end

          should_use_presenter Api::V2::IdentityPresenter, status: :ok

          it "changes value but does not verify" do
            refute @identity.reload.is_verified?
            assert_equal "skipper@example.org", @identity.value
          end

          it "emits user_identity_changed" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 1
          end
        end

        describe "to update value and verified" do
          before do
            put :update, params: { user_id: @end_user.id, id: @identity.id, identity: { value: "skipper@example.org", verified: true } }
          end

          should_use_presenter Api::V2::IdentityPresenter, status: :ok

          it "changes value and verifies" do
            assert @identity.reload.is_verified?
            assert_equal "skipper@example.org", @identity.value
          end

          it "emits user_identity_changed" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 1
          end
        end
      end

      describe "for a verified identity" do
        before do
          @identity.update_attribute(:is_verified, true)
          assert @identity.reload.is_verified?
        end

        describe "to updates value and verified" do
          before do
            put :update, params: { user_id: @end_user.id, id: @identity.id, identity: { value: "skipper@example.org" } }
          end

          should_use_presenter Api::V2::IdentityPresenter, status: :ok

          it "updates value and is still verified" do
            assert @identity.reload.is_verified?
            assert_equal "skipper@example.org", @identity.value
          end

          it "emits user_identity_changed" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 1
          end
        end

        describe "to update verified as false" do
          before do
            put :update, params: { user_id: @end_user.id, id: @identity.id, identity: { verified: false } }
          end

          should_use_presenter Api::V2::IdentityPresenter, status: :ok

          it "does not unverify" do
            assert @identity.reload.is_verified?
          end

          it "does not emits user identity events" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_created).size, 0
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 0
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 0
          end
        end
      end

      describe "for marking an identity deliverable" do
        def refute_deliverable_state_name_access(starting_state)
          @identity.update_attribute(:deliverable_state, starting_state)
          assert_equal starting_state, @identity.reload.deliverable_state
          Zendesk::Types::DeliverableStateType.any_instance.expects(:name).never
          put :update, params: { user_id: @end_user.id, id: @identity.id, identity: { deliverable_state: "deliverable" } }
        end

        describe "when the identity is undeliverable" do
          before do
            @identity.update_attribute(:deliverable_state, DeliverableStateType.UNDELIVERABLE)
            assert_equal DeliverableStateType.UNDELIVERABLE, @identity.reload.deliverable_state
          end

          it "calls mark_deliverable if state is set to deliverable in the parameters" do
            UserEmailIdentity.any_instance.expects(:mark_deliverable)
            put :update, params: { user_id: @end_user.id, id: @identity.id, identity: { deliverable_state: "deliverable" } }
          end

          it "does not call mark_deliverable if state is set to something other than deliverable in the parameters" do
            UserEmailIdentity.any_instance.expects(:mark_deliverable).never
            put :update, params: { user_id: @end_user.id, id: @identity.id, identity: { deliverable_state: "ticket_sharing_partner" } }
          end
        end

        it "does not attempt to update the deliverable_state if the identity's state is DELIVERABLE" do
          refute_deliverable_state_name_access DeliverableStateType.DELIVERABLE
        end

        it "does not attempt to update the deliverable_state if the identity's state is TICKET_SHARING_PARTNER" do
          refute_deliverable_state_name_access DeliverableStateType.TICKET_SHARING_PARTNER
        end
      end
    end

    describe "a POST to :create" do
      describe "with invalid data" do
        it 'fails' do
          post :create, params: { user_id: @end_user.id.to_s, identity: { foot: "mouth" } }
          assert_response :bad_request
        end
      end

      describe "to create a twitter identity" do
        before do
          stub_request(:post, "https://api.twitter.com/oauth2/token").
            to_return(
              status: 200,
              body: {"token_type" => "bearer", "access_token" => "AAAA%2FAAA%3DAAAAAAAA"}.to_json,
              headers: {'Content-Type' => 'application/json'}
            )
          stub_request(:get, "https://api.twitter.com/1.1/users/show.json?screen_name=somehandle").
            to_return(status: 200,
                      body: read_test_file('twitter_users_show_110542029_1.1.json'),
                      headers: { 'Content-Type' => 'application/json' })
          post :create, params: { user_id: @end_user.id.to_s, identity: { type: "twitter", value: "somehandle" } }
        end

        should_use_presenter Api::V2::IdentityPresenter, status: :created, location: %r{api/v2/users/\d+/identities/\d+.json}
        should_change("the identity count", by: 1) { UserIdentity.count(:all) }

        it "emits user_identity_created event" do
          identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
          assert_equal identity_events.size, 1
          identity_events.first.user_identity_created.identity.value.value.must_equal "110542029"
          identity_events.first.user_identity_created.identity.type.must_equal :TWITTER
        end
      end

      describe "to create a google identity" do
        before { post :create, params: { user_id: @end_user.id, identity: { type: "google", value: "wiggly@gmail.com" } } }

        should_change("the identity count", by: 1) { UserIdentity.count(:all) }
        should_use_presenter Api::V2::IdentityPresenter, status: :created, location: %r{api/v2/users/\d+/identities/\d+.json}

        it "emits user_identity_created event" do
          identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
          assert_equal identity_events.size, 1
          identity_events.first.user_identity_created.identity.value.value.must_equal "wiggly@gmail.com"
          identity_events.first.user_identity_created.identity.type.must_equal :EMAIL
        end
      end

      describe "to create an email identity" do
        describe "given a duplicate email address" do
          before { post :create, params: { user_id: @end_user.id, identity: { type: "email", value: @end_user.email } } }

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
          should_change("the identity count", by: 0) { UserIdentity.count(:all) }

          it "does not emits user identity events" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_created).size, 0
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 0
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 0
          end
        end

        describe "given an invalid email address" do
          before { post :create, params: { user_id: @end_user.id, identity: { type: "email", value: "The angry horse" } } }

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
          should_change("the identity count", by: 0) { UserIdentity.count(:all) }

          it "does not emits user identity events" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_created).size, 0
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 0
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 0
          end
        end

        describe "given the account reply address" do
          before { post :create, params: { user_id: @end_user.id.to_s, identity: { type: "email", value: @end_user.account.reply_address } } }
          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }

          it "does not emits user identity events" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_created).size, 0
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 0
            assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 0
          end
        end

        describe "given a valid email address" do
          describe "when should_skip_verification_email? is false" do
            before do
              @controller.expects(:should_skip_verification_email?).returns(false)
              ActionMailer::Base.perform_deliveries = true
              post :create, params: { user_id: @end_user.id, identity: { type: "email", value: "valid@example.com", verified: false } }
            end

            should_change("the identity count", by: 1) { UserIdentity.count(:all) }
            should_not_change("sent emails") { ActionMailer::Base.deliveries.size }
            should_use_presenter Api::V2::IdentityPresenter, status: :created, location: %r{api/v2/users/\d+/identities/\d+.json}

            it "emits user_identity_created event" do
              identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
              assert_equal identity_events.size, 1
              identity_events.first.user_identity_created.identity.value.value.must_equal "valid@example.com"
              identity_events.first.user_identity_created.identity.type.must_equal :EMAIL
            end
          end

          describe "when should_skip_verification_email? is true" do
            before do
              @controller.expects(:should_skip_verification_email?).returns(true)
              ActionMailer::Base.perform_deliveries = true
              post :create, params: { user_id: @end_user.id, identity: { type: "email", value: "valid@example.com", verified: false } }
            end

            should_change("the identity count", by: 1) { UserIdentity.count(:all) }
            should_not_change("sent emails") { ActionMailer::Base.deliveries.size }
            should_use_presenter Api::V2::IdentityPresenter, status: :created, location: %r{api/v2/users/\d+/identities/\d+.json}
            it "emits user_identity_created event" do
              identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
              assert_equal identity_events.size, 1
              identity_events.first.user_identity_created.identity.value.value.must_equal "valid@example.com"
              identity_events.first.user_identity_created.identity.type.must_equal :EMAIL
            end
          end

          describe "when skip_verify_email parameter is true" do
            before do
              @controller.expects(:should_skip_verification_email?).returns(false)
              ActionMailer::Base.perform_deliveries = true
              post :create, params: { user_id: @end_user.id, identity: { type: "email", value: "valid@example.com", verified: false, skip_verify_email: true } }
            end

            should_change("the identity count", by: 1) { UserIdentity.count(:all) }
            should_not_change("sent emails") { ActionMailer::Base.deliveries.size }
            should_use_presenter Api::V2::IdentityPresenter, status: :created, location: %r{api/v2/users/\d+/identities/\d+.json}
            it "emits user_identity_created event" do
              identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
              assert_equal identity_events.size, 1
              identity_events.first.user_identity_created.identity.value.value.must_equal "valid@example.com"
              identity_events.first.user_identity_created.identity.type.must_equal :EMAIL
            end
          end
        end

        describe "for himself given a valid email address" do
          before do
            @agent = users(:minimum_agent)
            ActionMailer::Base.perform_deliveries = true
            post :create, params: { user_id: @agent.id, identity: { type: "email", value: "valid@example.com" } }
          end

          should_change("the identity count", by: 1) { UserIdentity.count(:all) }
          should_change("sent emails") { ActionMailer::Base.deliveries.size }
          should_use_presenter Api::V2::IdentityPresenter, status: :created, location: %r{api/v2/users/\d+/identities/\d+.json}

          it "emits user_identity_created event" do
            identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
            assert_equal identity_events.size, 1
            identity_events.first.user_identity_created.identity.value.value.must_equal "valid@example.com"
            identity_events.first.user_identity_created.identity.type.must_equal :EMAIL
          end
        end

        describe "given a valid email address and verification" do
          describe "when account setting, is_welcome_email_when_agent_register_enabled? returns true" do
            before do
              Account.any_instance.stubs(:is_welcome_email_when_agent_register_enabled?).returns(true)
              ActionMailer::Base.perform_deliveries = true
              post :create, params: { user_id: @end_user.id, identity: { type: "email", value: "verified.valid@example.com", verified: true } }
            end

            should_change("the identity count", by: 1) { UserIdentity.count(:all) }
            should_not_change("sent emails") { ActionMailer::Base.deliveries.size }
            should_use_presenter Api::V2::IdentityPresenter, status: :created, location: %r{api/v2/users/\d+/identities/\d+.json}

            it "emits user_identity_created event" do
              identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
              assert_equal identity_events.size, 1
              identity_events.first.user_identity_created.identity.value.value.must_equal "verified.valid@example.com"
              identity_events.first.user_identity_created.identity.type.must_equal :EMAIL
            end
          end

          describe "when account setting, is_welcome_email_when_agent_register_enabled? returns false" do
            before do
              Account.any_instance.stubs(:is_welcome_email_when_agent_register_enabled?).returns(false)
              ActionMailer::Base.perform_deliveries = true
              post :create, params: { user_id: @end_user.id, identity: { type: "email", value: "verified.valid@example.com", verified: true } }
            end

            should_change("the identity count", by: 1) { UserIdentity.count(:all) }
            should_not_change("sent emails") { ActionMailer::Base.deliveries.size }
            should_use_presenter Api::V2::IdentityPresenter, status: :created, location: %r{api/v2/users/\d+/identities/\d+.json}

            it "emits user_identity_created event" do
              identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
              assert_equal identity_events.size, 1
              identity_events.first.user_identity_created.identity.value.value.must_equal "verified.valid@example.com"
              identity_events.first.user_identity_created.identity.type.must_equal :EMAIL
            end
          end
        end
      end

      describe "to create a facebook identity" do
        before { post :create, params: { user_id: @end_user.id.to_s, identity: { type: "facebook", value: "12345678" } } }

        it "emits user_identity_created event" do
          identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
          assert_equal identity_events.size, 1
          identity_events.first.user_identity_created.identity.value.value.must_equal "12345678"
          identity_events.first.user_identity_created.identity.type.must_equal :FACEBOOK
        end

        should_change("the identity count", by: 1) { UserIdentity.count(:all) }
        should_use_presenter Api::V2::IdentityPresenter, status: :created, location: %r{api/v2/users/\d+/identities/\d+.json}
      end

      describe "to create an agent forwarding identity" do
        before { post :create, params: { user_id: @end_user.id.to_s, identity: { type: "agent_forwarding", value: "+14155556666x5678" } } }

        should_change("the identity count", by: 1) { UserIdentity.count(:all) }
        should_use_presenter Api::V2::IdentityPresenter, status: :created, location: %r{api/v2/users/\d+/identities/\d+.json}

        it "emits user_identity_created event" do
          identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
          assert_equal identity_events.size, 1
          identity_events.first.user_identity_created.identity.value.value.must_equal "+14155556666x5678||UNIQ||"
          identity_events.first.user_identity_created.identity.type.must_equal :AGENT_FORWARDING
        end
      end

      describe "updating a user with more identities allowed" do
        let(:user) { FactoryBot.create(:user, account: @account, roles: Role::END_USER.id) }
        let(:existing_user_email) { user.identities.first.value }
        let(:new_identity_user_email) { "happy@users.com" }

        before do
          Arturo.enable_feature!(:user_identity_limit)
          User.any_instance.stubs(:account).returns(@account)
        end

        describe 'when create invalid indentity' do
          before do
            FactoryBot.create(:max_identities_account_setting, account: @account, value: 1)
            assert_equal 1, user.identities.size
            post :create, params: { user_id: user.id, identity: { type: "email", value: new_identity_user_email } }
          end

          it "throws error message" do
            assert_response :unprocessable_entity

            assert_equal "The number of identities for this user has exceeded the limit of 1. An identity can be an email address, a phone number, a Twitter account, or a Facebook account.", JSON.parse(response.body)['details']['base'].first['description']
            assert_equal 1, JSON.parse(response.body)['details']['base'].size
          end

          it "emits user_identity_created event" do
            identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
            assert_equal identity_events.size, 1
            identity_events.first.user_identity_created.identity.value.value.wont_equal new_identity_user_email
            identity_events.first.user_identity_created.identity.value.value.must_equal existing_user_email
            identity_events.first.user_identity_created.identity.type.must_equal :EMAIL
          end
        end

        describe "when create valid identity" do
          before do
            FactoryBot.create(:max_identities_account_setting, account: @account, value: 2)
            assert_equal 1, user.identities.size
            post :create, params: { user_id: user.id, identity: { type: "email", value: new_identity_user_email } }
          end

          it 'successfully create identity' do
            assert_response :success
            assert_equal 2, user.reload.identities.count
          end

          it "emits user_identity_created event" do
            identity_events = decode_user_events(domain_event_publisher.events, :user_identity_created)
            assert_equal identity_events.size, 2
            identity_events.first.user_identity_created.identity.value.value.must_equal existing_user_email
            identity_events.first.user_identity_created.identity.type.must_equal :EMAIL
            identity_events.last.user_identity_created.identity.value.value.must_equal new_identity_user_email
            identity_events.last.user_identity_created.identity.type.must_equal :EMAIL
          end
        end
      end
    end

    describe "a DELETE to :destroy" do
      describe "when the user has only 1 identity" do
        before do
          @end_user.identities.first.destroy while @end_user.identities.reload.size > 1
          delete :destroy, params: { user_id: @end_user.id, id: @end_user.identities.first.id }
        end

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

        it "emits user_identity_removed events" do
          identity_events = decode_user_events(domain_event_publisher.events, :user_identity_removed)
          assert_equal identity_events.size, 7
        end
      end

      describe "when the user has multiple identities" do
        before do
          assert @end_user.identities.size > 1
          delete :destroy, params: { user_id: @end_user.id, id: @end_user.identities.first.id }
        end

        it "responds with 204" do
          assert_response :no_content
        end

        it "emits user_identity_removed event" do
          identity_events = decode_user_events(domain_event_publisher.events, :user_identity_removed)
          assert_equal identity_events.size, 1
        end
      end
    end

    describe "a PUT to :make_primary" do
      before do
        @twitter_identity = user_identities(:minimum_end_user_twitter)
        refute @twitter_identity.primary?

        put :make_primary, params: { user_id: @end_user.id, id: @twitter_identity.id }
      end

      should_use_presenter Api::V2::IdentityPresenter, status: :ok
      it "changes the record to primary" do
        assert @twitter_identity.reload.primary?
      end

      it "does not emits user identity events" do
        assert_equal decode_user_events(domain_event_publisher.events, :user_identity_created).size, 0
        assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 0
        assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 0
      end
    end

    describe "a PUT to :verify" do
      before do
        @identity.update_attribute(:is_verified, false)
        @statsd_client = @controller.send(:statsd_client)
      end

      describe "verifying the identity" do
        before { put :verify, params: { user_id: @identity.user_id, id: @identity.id } }

        should_use_presenter Api::V2::IdentityPresenter, status: :ok
        it "verifies the identity" do
          assert @identity.reload.is_verified?
        end

        it "does not emits user identity events" do
          assert_equal decode_user_events(domain_event_publisher.events, :user_identity_created).size, 0
          assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 0
          assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 0
        end
      end

      describe "when owner is not verified" do
        it "increments the statsd_client with unverified_owner" do
          @account.owner.is_verified = false
          @account.owner.save!
          @statsd_client.expects(:increment).with('unverified_account', tags: ['action:verify']).once
          put :verify, params: { user_id: @identity.user_id, id: @identity.id }
        end
      end

      describe "when owner is verified" do
        it "does not increment the statsd_client" do
          @statsd_client.expects(:increment).with('unverified_account', tags: ['action:verify']).never
          put :verify, params: { user_id: @identity.user_id, id: @identity.id }
        end
      end
    end

    describe "a PUT to :request_verification for an invalid identity" do
      it "does not blow up on inappropriate identities" do
        @identity = @end_user.identities.facebook.first
        @controller.expects(:identity).at_least_once.returns(@identity)

        put :request_verification, params: { user_id: @identity.user_id, id: @identity.id }

        assert_response :bad_request
      end
    end

    describe "a PUT to :request_verification" do
      before do
        @controller.expects(:identity).at_least_once.returns(@identity)
        @default_brand = @identity.account.brands.first
        Account.any_instance.stubs(:default_brand).returns(@default_brand)
      end

      describe "with a restricted web_portal_state" do
        before do
          @request.account.settings.web_portal_state = "restricted"
          @request.account.save!
        end

        it "responds with OK" do
          @identity.expects(:send_verification_email)

          put :request_verification, params: { user_id: @identity.user_id, id: @identity.id }

          assert_response :ok
        end

        it "sets the primary attribute on the identity if primary is true" do
          @identity.expects(:set_primary_when_verifying!)

          put :request_verification, params: { user_id: @identity.user_id, id: @identity.id, primary: true }
        end

        it "does not set the primary attribute on the identity if primary is missing" do
          @identity.expects(:set_primary_when_verifying!).never

          put :request_verification, params: { user_id: @identity.user_id, id: @identity.id }
        end

        it "does not set the primary attribute on the identity if it is not of type email" do
          @identity.update_attribute(:type, "twitter")
          @identity.expects(:set_primary_when_verifying!).never

          put :request_verification, params: { user_id: @identity.user_id, id: @identity.id }
        end

        describe "with a brand_id" do
          before do
            @brand = FactoryBot.create(:brand, account_id: @identity.account.id, subdomain: 'wombat')
            ActionMailer::Base.perform_deliveries = true
            ActionMailer::Base.deliveries = []
            Account.any_instance.stubs(:default_brand).returns(@identity.account.brands.first)
          end

          it "responds with OK" do
            @identity.expects(:send_verification_email)

            put :request_verification, params: { user_id: @identity.user_id, id: @identity.id, brand_id: @brand.id }

            assert_response :ok
          end

          describe "with multiple brands enabled" do
            before do
              Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)
              put :request_verification, params: { user_id: @identity.user_id, id: @identity.id, brand_id: @brand.id }
            end

            it "sends a verification email with the requested brand" do
              assert_match %r{wombat.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
              assert_equal ["support@wombat.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
              assert_includes ActionMailer::Base.deliveries.last.subject, "verify"
            end
          end

          describe "without multiple brands enabled" do
            before do
              Account.any_instance.stubs(:has_multiple_active_brands?).returns(false)
              put :request_verification, params: { user_id: @identity.user_id, id: @identity.id, brand_id: @brand.id }
            end

            it "sends a verification email with the account default brand" do
              assert_includes ActionMailer::Base.deliveries.last.joined_bodies, "#{@default_brand.subdomain}.zendesk-test.com"
              assert_equal [@default_brand.reply_address.to_s], ActionMailer::Base.deliveries.last.from
            end
          end
        end
      end

      describe "with an enabled help center" do
        before do
          Account.any_instance.stubs(:help_center_enabled?).returns(true)
        end

        it "responds with OK" do
          @identity.expects(:send_verification_email)

          put :request_verification, params: { user_id: @identity.user_id, id: @identity.id }

          assert_response :ok
        end

        it "sets the primary attribute on the identity if primary is true" do
          @identity.expects(:set_primary_when_verifying!)

          put :request_verification, params: { user_id: @identity.user_id, id: @identity.id, primary: true }
        end

        it "does not set the primary attribute on the identity if primary is missing" do
          @identity.expects(:set_primary_when_verifying!).never

          put :request_verification, params: { user_id: @identity.user_id, id: @identity.id }
        end

        it "does not set the primary attribute on the identity if it is not of type email" do
          @identity.update_attribute(:type, "twitter")
          @identity.expects(:set_primary_when_verifying!).never

          put :request_verification, params: { user_id: @identity.user_id, id: @identity.id }
        end

        describe "with a brand_id" do
          before do
            @brand = FactoryBot.create(:brand, account_id: @identity.account.id, subdomain: 'wombat')
            ActionMailer::Base.perform_deliveries = true
            ActionMailer::Base.deliveries = []
            Account.any_instance.stubs(:default_brand).returns(@identity.account.brands.first)
          end

          it "responds with OK" do
            @identity.expects(:send_verification_email)

            put :request_verification, params: { user_id: @identity.user_id, id: @identity.id, brand_id: @brand.id }

            assert_response :ok
          end

          describe "with multiple brands enabled" do
            before do
              Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)
              put :request_verification, params: { user_id: @identity.user_id, id: @identity.id, brand_id: @brand.id }
            end

            it "sends a verification email with the requested brand" do
              assert_match %r{wombat.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
              assert_equal ["support@wombat.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
              assert_includes ActionMailer::Base.deliveries.last.subject, "verify"
            end
          end

          describe "without multiple brands enabled" do
            before do
              Account.any_instance.stubs(:has_multiple_active_brands?).returns(false)
              put :request_verification, params: { user_id: @identity.user_id, id: @identity.id, brand_id: @brand.id }
            end

            it "sends a verification email with the account default brand" do
              assert_includes ActionMailer::Base.deliveries.last.joined_bodies, "#{@default_brand.subdomain}.zendesk-test.com"
              assert_equal [@default_brand.reply_address.to_s], ActionMailer::Base.deliveries.last.from
            end
          end
        end
      end

      describe "with disabled web portal state" do
        before do
          @brand = FactoryBot.create(:brand, account_id: @identity.account.id, subdomain: 'wombat')
          ActionMailer::Base.perform_deliveries = true
          ActionMailer::Base.deliveries = []
          Account.any_instance.stubs(:default_brand).returns(@identity.account.brands.first)
          Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)
          put :request_verification, params: { user_id: @identity.user_id, id: @identity.id, brand_id: @brand.id }
        end

        it "does not send a verification email" do
          puts ActionMailer::Base.deliveries
          assert_empty ActionMailer::Base.deliveries
        end

        it "does not emits user identity events" do
          assert_equal decode_user_events(domain_event_publisher.events, :user_identity_created).size, 0
          assert_equal decode_user_events(domain_event_publisher.events, :user_identity_removed).size, 0
          assert_equal decode_user_events(domain_event_publisher.events, :user_identity_changed).size, 0
        end
      end
    end

    describe "#should_skip_verification_email?" do
      before do
        @controller.stubs(:current_user).returns(users(:minimum_agent))
        @controller.stubs(:user).returns(users(:minimum_end_user))
        @controller.send(:current_account).stubs(:is_welcome_email_when_agent_register_enabled?).returns(true)
        @controller.send(:user).stubs(:identities).returns([])
      end

      it "returns false on happy path" do
        assert_equal false, @controller.send(:should_skip_verification_email?)
      end

      it "returns false when current_user isn't an agent" do
        @controller.stubs(:current_user).returns(users(:minimum_search_user))
        assert_equal false, @controller.send(:should_skip_verification_email?)
      end

      it "returns false when current_user == user" do
        @controller.stubs(:user).returns(users(:minimum_agent))
        assert_equal false, @controller.send(:should_skip_verification_email?)
      end

      it "returns true when is_welcome_email_when_agent_register_enabled returns false" do
        @controller.send(:current_account).stubs(:is_welcome_email_when_agent_register_enabled?).returns(false)
        assert(@controller.send(:should_skip_verification_email?))
      end

      describe "when user has existing identities" do
        before do
          @user = @controller.send(:user)
          @user.stubs(:identities).returns([1])
        end

        it "returns true if user has verified email and params[:verified] is true" do
          @user.expects(:has_verified_email?).returns(true)
          @controller.params[:verified] = true
          assert(@controller.send(:should_skip_verification_email?))
        end
        it "returns false if user doesn't have a verified email" do
          @user.expects(:has_verified_email?).returns(false)
          @controller.params[:verified] = true
          assert_equal false, @controller.send(:should_skip_verification_email?)
        end
        it "returns false if params[:verified] is false" do
          @user.expects(:has_verified_email?).returns(true)
          @controller.params[:verified] = false
          assert_equal false, @controller.send(:should_skip_verification_email?)
        end
      end
    end
  end

  as_a_system_user do
    describe "a POST to :create" do
      describe "with a brand_id" do
        before do
          @default_brand = @end_user.account.brands.first
          @brand = FactoryBot.create(:brand, account_id: @end_user.account.id, subdomain: 'wombat')
          @controller.expects(:should_skip_verification_email?).returns(false)

          ActionMailer::Base.perform_deliveries = true
          ActionMailer::Base.deliveries = []

          Account.any_instance.stubs(:help_center_enabled?).returns(true)
          Account.any_instance.stubs(:default_brand).returns(@default_brand)
        end

        it "responds with 201 Created" do
          post :create, params: { user_id: @end_user.id, brand_id: @brand.id, identity: { primary: true, type: "email", value: "valid@example.com" } }

          assert_response :created
        end

        describe "with multiple brands enabled" do
          before do
            Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)

            post :create, params: { user_id: @end_user.id, brand_id: @brand.id, identity: { primary: true, type: "email", value: "valid@example.com" } }
          end

          it "sends a verification email with the requested brand" do
            assert_match %r{wombat.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
            assert_equal ["support@wombat.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
            assert_includes ActionMailer::Base.deliveries.last.subject, "verify"
          end
        end

        describe "without multiple brands enabled" do
          before do
            Account.any_instance.stubs(:has_multiple_active_brands?).returns(false)

            post :create, params: { user_id: @end_user.id, brand_id: @brand.id, identity: { primary: true, type: "email", value: "valid@example.com" } }
          end

          it "sends a verification email with the account default brand" do
            assert_includes ActionMailer::Base.deliveries.last.joined_bodies, "#{@default_brand.subdomain}.zendesk-test.com"
            assert_equal [@default_brand.reply_address.to_s], ActionMailer::Base.deliveries.last.from
          end
        end
      end
    end
  end

  as_a_subsystem_user(user: 'profile_service', account: :minimum) do
    describe 'a GET to :index' do
      before { get :index, params: { user_id: @end_user.id } }
      it("return a successful response") { assert_response :success }
    end

    describe 'a POST to :create' do
      describe "should successfully create an identity" do
        before do
          post :create, params: { user_id: @end_user.id, identity: { type: "email", value: "valid@example.com", verified: false } }
        end

        should_change("the identity count", by: 1) { UserIdentity.count(:all) }
      end
    end
  end
end
