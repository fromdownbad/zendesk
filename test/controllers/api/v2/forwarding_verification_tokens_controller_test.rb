require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ForwardingVerificationTokensController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :forwarding_verification_tokens

  before do
    https!
    accept :json
  end

  with_options(controller: 'api/v2/forwarding_verification_tokens') do |request|
    request.should_route :get, '/api/v2/forwarding_verification_tokens', action: 'index'
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_agent do
    should_be_forbidden :index
  end

  as_an_admin do
    describe "a GET to :index" do
      before do
        get :index
        @verification_token = forwarding_verification_tokens(:gmail_verification_1)
      end

      should_use_presenter Api::V2::ForwardingVerificationTokenPresenter, status: :ok

      it "shoulds return all the data" do
        response = JSON.parse(@response.body)
        perform_full_check(response, @verification_token)
      end
    end

    describe "a GET with from_email" do
      before do
        @verification_token = forwarding_verification_tokens(:gmail_verification_1)
      end

      it "returns no data" do
        get :index, params: { from_email: "random_stuff" }
        response = JSON.parse(@response.body)
        assert_equal 0, response["count"]
      end

      it "returns data" do
        get :index, params: { from_email: @verification_token.from_email }
        response = JSON.parse(@response.body)
        perform_full_check(response, @verification_token)
      end
    end
  end

  def perform_full_check(response, expected_token)
    assert_equal 1, response["count"]

    verification_token = response["forwarding_verification_tokens"].first
    assert_equal verification_token["from_email"], expected_token.from_email
    assert_equal verification_token["value"], expected_token.value
  end
end
