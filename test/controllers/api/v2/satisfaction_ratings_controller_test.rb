require_relative "../../../support/test_helper"
require_relative "../../../support/satisfaction_test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::SatisfactionRatingsController do
  extend Api::V2::TestHelper
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper
  include SatisfactionTestHelper

  fixtures :all

  let(:reason_code_enabled) { false }

  def http_get_export
    stub_request(:put, %r{\.amazonaws\.com/data/expirable_attachments/.*-csv\.zip})
    get :export
  end

  before do
    Account.any_instance.stubs(:csat_reason_code_enabled?).returns(reason_code_enabled)

    @account = accounts(:minimum)
    @agent   = users(:minimum_agent)
    @user    = users(:minimum_end_user)
    @group   = groups(:minimum_group)

    @rating1 = @account.satisfaction_ratings.create!(
      agent: @agent,
      group: @group,
      ticket: tickets(:minimum_2),
      enduser: @user,
      event: events(:create_comment_for_minimum_ticket_2),
      score: SatisfactionType.OFFERED,
      reason_code: Satisfaction::Reason::NONE,
      status_id: StatusType.CLOSED
    ) do |r|
      r.created_at = 1.day.ago
      r.ticket.update_column(:updated_at, r.created_at)
      r.ticket.update_column(:satisfaction_score, r.score)
    end

    @rating2 = @account.satisfaction_ratings.create!(
      agent: @agent,
      group: @group,
      ticket: tickets(:minimum_1),
      enduser: @user,
      event: events(:create_comment_for_minimum_ticket_1),
      score: SatisfactionType.BAD,
      reason_code: Satisfaction::Reason::OTHER,
      status_id: StatusType.CLOSED
    ) do |r|
      r.created_at = 1.hour.ago
      r.ticket.update_column(:updated_at, r.created_at)
      r.ticket.update_column(:satisfaction_score, r.score)
    end

    @reason = FactoryBot.create(:satisfaction_reason, account: @account)

    accept :json
  end

  with_options(controller: 'api/v2/satisfaction_ratings') do |request|
    request.should_route :get, '/api/v2/satisfaction_ratings',          action: 'index'
    request.should_route :get, '/api/v2/satisfaction_ratings/count',    action: 'count'
    request.should_route :get, '/api/v2/satisfaction_ratings/export',   action: 'export'
    request.should_route :get, '/api/v2/satisfaction_ratings/received', action: 'received'
    request.should_route :post, '/api/v2/tickets/1/satisfaction_rating', action: 'create', ticket_id: 1
  end

  describe "when using oauth tokens" do
    let(:ticket) { tickets(:minimum_4) }
    let(:rating) { {satisfaction_rating: {score: "bad", comment: "Terrible"}} }

    def post_create
      login(@user)

      post :create, params: rating.merge(ticket_id: ticket.nice_id)
    end

    describe_with_arturo_setting_enabled :customer_satisfaction do
      with_scopes('satisfaction_ratings:read', 'read') do
        should_be_authorized { get :index }
        should_be_authorized { get :show, params: { id: @rating1.id } }
        should_be_authorized { get :received }
        should_be_authorized { http_get_export }

        should_not_be_authorized { post_create }
      end

      with_scopes('satisfaction_ratings:write', 'write') do
        should_be_authorized { post_create }

        should_not_be_authorized { get :index }
        should_not_be_authorized { get :show, params: { id: @rating1.id } }
      end

      with_scopes('read', 'write') do
        should_be_authorized { http_get_export }
        should_be_authorized { get :received }
      end
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :count, :received, :show, :export

    # TODO: This fails as the responds is a 401, 403 is expected
    # should_be_forbidden [:post, :create, { ticket_id: 1 }]
  end

  as_an_agent do
    should_be_forbidden :index, :count, :received, :show, :export, [:post, :create, { ticket_id: 1 }]
  end

  as_an_admin do
    should_support_cursor_pagination :index, with_custom_index: true

    describe "a GET to :index" do
      let(:filter_params) { {} }
      let(:json) { JSON.parse(@response.body) }

      before { get :index, params: filter_params }
      should_use_presenter Api::V2::SatisfactionRatingPresenter
      it "lists all the satisfaction ratings" do
        assert_equal 2, json["satisfaction_ratings"].size
      end

      # Additional filtering tests can be found for Zendesk::SatisfactionRatings::Finder
      describe "with an invalid score filter" do
        let(:filter_params) { { score: "you_suck" } }

        it "responds with bad_request if the score is invalid" do
          assert_response :bad_request
        end
      end

      describe "with start time filter" do
        let(:filter_params) { { start_time: 2.hours.ago.to_i, include: 'statistics' } }

        it 'filters statistics' do
          assert_equal 1, json['statistics']['surveys_sent']
        end
      end

      describe "with statistics side load" do
        let(:filter_params) { { include: 'statistics' } }

        it "includes statistics" do
          assert_equal 2, json['statistics']['surveys_sent']
        end
      end

      describe "with desc ordering" do
        let(:filter_params) { { sort_order: 'desc' } }

        it "should reverse order" do
          assert @rating1.created_at < @rating2.created_at
          assert_equal @rating2.id, json['satisfaction_ratings'][0]['id'].to_i
          assert_equal @rating1.id, json['satisfaction_ratings'][1]['id'].to_i
        end
      end

      describe "with default ordering" do
        it "should order ASC" do
          assert @rating1.created_at < @rating2.created_at
          assert_equal @rating1.id, json['satisfaction_ratings'][0]['id'].to_i
          assert_equal @rating2.id, json['satisfaction_ratings'][1]['id'].to_i
        end
      end
    end

    describe 'when cursor_pagination_v2_satisfaction_ratings_endpoint arturo is disabled' do
      before do
        Account.any_instance.stubs(:has_cursor_pagination_v2_satisfaction_ratings_endpoint?).returns(false)
      end

      describe "a GET to :index" do
        let(:filter_params) { {} }
        let(:json) do
          get :index, params: filter_params
          JSON.parse(@response.body)
        end

        it "lists all the satisfaction ratings" do
          assert_equal 2, json["satisfaction_ratings"].size
        end

        describe "with score filter" do
          let(:filter_params) { { score: "bad_without_comment" } }

          it "filters" do
            assert_equal 1, json["satisfaction_ratings"].size
          end
        end

        describe "with reason_code filter" do
          let(:filter_params) { { reason_code: [Satisfaction::Reason::OTHER] } }

          describe "when csat_reason_code is enabled" do
            let(:reason_code_enabled) { true }

            it "filters" do
              assert_equal 1, json["satisfaction_ratings"].size
            end
          end

          describe "when csat_reason_code is disabled" do
            it "does not filter" do
              assert_equal 2, json["satisfaction_ratings"].size
            end
          end
        end

        describe "with start time filter" do
          let(:filter_params) { { start_time: 2.hours.ago.to_i, include: 'statistics' } }

          it "filters" do
            assert_equal 1, json['satisfaction_ratings'].size
          end
        end

        describe "with end time filter" do
          let(:filter_params) { { end_time: 2.hours.ago.to_i } }

          it "filters" do
            assert_equal 1, json['satisfaction_ratings'].size
          end
        end
      end
    end

    describe "when paginating" do
      let(:filter_params) { {} }
      let(:json) { JSON.parse(@response.body) }

      before do
        Satisfaction::Rating.without_arsi.update_all(score: SatisfactionType.GOOD)
        get :index, params: filter_params
      end

      it "uses offset pagination when no pagination params are present" do
        assert_response :ok
        satisfaction_ratings_keys = JSON.parse(response.body).keys
        assert_equal 2, (satisfaction_ratings_keys & ['next_page', 'previous_page']).size
      end

      describe "with offset pagination" do
        describe "with a score parameter" do
          let(:filter_params) { { per_page: 1, score: "good" } }

          it "uses the score parameter in the next_page url" do
            assert_equal 'https://minimum.zendesk-test.com/api/v2/satisfaction_ratings.json?page=2&per_page=1&score=good',
              json['next_page']
          end
        end

        describe "without a score parameter" do
          let(:filter_params) { { per_page: 1 } }

          it "does not use the score parameter in the next_page url" do
            assert_equal 'https://minimum.zendesk-test.com/api/v2/satisfaction_ratings.json?page=2&per_page=1',
              json['next_page']
          end
        end

        describe "with a reason_code parameter" do
          let(:filter_params) do
            {
              per_page: 1,
              reason_code: [
                Satisfaction::Reason::NONE, Satisfaction::Reason::OTHER
              ]
            }
          end

          it "uses the reason_code parameter in the next_page url" do
            assert_equal 'https://minimum.zendesk-test.com/api/v2/satisfaction_ratings.json?page=2&per_page=1&reason_code%5B%5D=0&reason_code%5B%5D=100',
              json['next_page']
          end
        end

        describe "with start time and end time" do
          let(:filter_params) { { per_page: 1, start_time: 2.days.ago.to_i, end_time: 10.minutes.ago.to_i } }

          it "uses the time parameters in the next_page url" do
            assert_equal "https://minimum.zendesk-test.com/api/v2/satisfaction_ratings.json?end_time=#{filter_params[:end_time]}&page=2&per_page=1&start_time=#{filter_params[:start_time]}",
              json['next_page']
          end
        end
      end
    end

    describe "using cursor based pagination v2" do
      let(:page_size) { 2 }
      let(:page_params) { { size: page_size } }
      let(:sort) { nil }
      let(:params) { { page: page_params, sort: sort } }
      let(:json) do
        get :index, params: params
        JSON.parse(@response.body).with_indifferent_access
      end

      before do
        # make a third rating to get a page_size > 1 that works with cbp v2 has_more
        @rating3 = @account.satisfaction_ratings.create!(@rating2.attributes) do |r|
          r.created_at = 30.minutes.ago
          r.ticket.update_column(:updated_at, r.created_at)
          r.ticket.update_column(:satisfaction_score, r.score)
        end
      end

      it "still presents offset pagination keys when no pagination params are present" do
        get :index
        satisfaction_ratings_keys = JSON.parse(@response.body).keys
        assert_response :ok
        assert_equal 2, (satisfaction_ratings_keys & ['next_page', 'previous_page']).size
      end

      describe 'when cursor_pagination_v2_satisfaction_ratings_endpoint arturo is disabled' do
        before do
          Account.any_instance.stubs(:has_cursor_pagination_v2_satisfaction_ratings_endpoint?).returns(false)
        end

        it "still uses cbp v1" do
          @controller.expects(:paginate_with_cursor).never
          get :index, params: { limit: 2 }
          satisfaction_ratings_keys = JSON.parse(@response.body).with_indifferent_access.keys
          assert_equal 4, (satisfaction_ratings_keys & ['after_cursor', 'before_cursor', 'after_url', 'before_url']).size
        end

        it "returns 400 bad request if the after param is passed in" do
          get :index, params: { page: { after: 'xxx' } }
          assert_response :bad_request, response.body
          assert_equal 'Invalid attribute', json[:error][:title]
          assert_includes json[:error][:message], 'You passed an invalid value for the page attribute. Invalid parameter: page must be an integer'
        end
      end

      describe 'when there is only one page' do
        let(:page_size) { @account.satisfaction_ratings.size + 1 }

        it 'presents the correct keys' do
          refute json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of tickets' do
          assert_equal @account.satisfaction_ratings.size, json[:satisfaction_ratings].size
        end
      end

      describe 'when there is more than one page' do
        let(:page_size) { @account.satisfaction_ratings.size - 1 }

        it 'presents the correct keys' do
          assert json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of satisfaction_ratings' do
          assert_equal page_size, json[:satisfaction_ratings].size
        end
      end

      describe 'with a valid after cursor' do
        let(:page_size) { @account.satisfaction_ratings.size - 1 }
        let(:after_json) do
          JSON.parse(@response.body).with_indifferent_access
        end

        before do
          after_cursor = json[:meta][:after_cursor]

          # The same controller instance is reused within each test and this controller memoizes
          # satisfaction_ratings. To get to the second page of results we need to reach in and
          # clear the controller's @satisfaction_ratings variable. Outside of controller
          # tests we get a new instance of the controller on each request - this mimics
          # that behavior for only this variable.
          @controller.instance_variable_set(:@satisfaction_ratings, nil)

          get :index, params: { page: {
            size: page_size,
            after: after_cursor
          } }
        end

        it 'presents the correct keys' do
          refute after_json[:meta][:has_more]
          assert after_json[:meta][:before_cursor].present?
          assert after_json[:meta][:after_cursor].present?
          assert after_json[:links][:prev].present?
          assert after_json[:links][:next].present?
        end

        it 'presents the correct number of satisfaction_ratings' do
          assert_equal 1, after_json[:satisfaction_ratings].size
        end
      end

      describe 'with an invalid after cursor' do
        let(:page_params) do
          {
            size: page_size,
            after: 'invalid'
          }
        end

        it 'presents an error message' do
          assert_equal 'InvalidPaginationParameter', json[:error]
          assert_equal 'page[after] is not valid', json[:description]
          assert_response :bad_request
        end
      end

      describe 'when sort parameter is created_at' do
        let(:sort) { 'created_at' }

        it 'generates the correct order clause' do
          assert_sql_queries(1, /ORDER BY `satisfaction_ratings`.`created_at` ASC, `satisfaction_ratings`.`id` ASC/) do
            json
          end
        end

        it 'uses the correct index' do
          assert_sql_queries(1, /#{Satisfaction::Rating::ACCOUNT_AND_CREATED_INDEX}/) do
            json
          end
        end
      end

      describe 'when sort parameter has multiple fields' do
        let(:sort) { '-created_at,id' }

        it 'shows an error message' do
          json
          assert_response :bad_request
          assert @response.body =~ /sort is not valid/
        end
      end

      describe 'when sort parameter has invalid fields' do
        let(:sort) { '-invalid' }

        it 'shows an error message' do
          json
          assert_response :bad_request
          assert @response.body =~ /sort is not valid/
        end
      end
    end

    describe "a GET to :received" do
      before { get :received }
      should_use_presenter Api::V2::SatisfactionRatingPresenter
      it "lists only the received satisfaction ratings" do
        assert_equal 1, JSON.parse(@response.body)["satisfaction_ratings"].size
      end
    end

    describe "a GET to :show" do
      describe "when the rating is present" do
        before { get :show, params: { id: @rating1.id } }
        should_use_presenter Api::V2::SatisfactionRatingPresenter
      end

      describe "when the rating is not present" do
        before { get :show, params: { id: Satisfaction::Rating.maximum(:id) + 1 } }
        it('responds with not_found') { assert_response :not_found }
      end
    end

    describe 'an archived ticket' do
      before do
        @archived_ticket = tickets(:minimum_5)
        archive_and_delete(@archived_ticket)
        @rating1.update_column(:ticket_id, @archived_ticket.id)
      end

      describe "a GET to :show" do
        it "uses the non_deleted_with_archived scope" do
          archived_scope = @account.satisfaction_ratings.non_deleted_with_archived
          @controller.expects(:scope).returns(archived_scope).once
          get :show, params: { id: @rating1.id }
        end

        describe "presentation" do
          before { get :show, params: { id: @rating1.id } }
          should_use_presenter Api::V2::SatisfactionRatingPresenter
        end
      end

      describe "a GET to :index" do
        it 'does not do n+1 queries if no comment if arturo is enabled' do
          archive_and_delete(@rating2.ticket)

          queries = sql_queries { get :index }

          ticket_archive_stub_requests = queries.grep /\ASELECT\s+`ticket_archive_stubs`/
          ticket_requests = queries.grep /\ASELECT\s+`tickets`/

          assert_equal 1, ticket_archive_stub_requests.size, ticket_archive_stub_requests
          assert_equal 1, ticket_requests.size, ticket_requests
        end

        describe "when the archived ticket is the first ticket created" do
          before do
            @rating1.update_column(:created_at, @rating2.created_at - 2.days)
            get :index, params: { per_page: 1 }
            assert_response :success
          end

          it "paginates correctly" do
            assert_equal @rating1.id, JSON.parse(@response.body)["satisfaction_ratings"].first["id"]
          end
        end

        describe "when the archived ticket is the last created" do
          before do
            @rating1.update_column(:created_at, @rating2.created_at + 2.days)
            get :index, params: { per_page: 1, page: 2 }
            assert_response :success
          end

          it "paginates correctly" do
            assert_equal @rating1.id, JSON.parse(@response.body)["satisfaction_ratings"].first["id"]
          end
        end
      end
    end

    describe "a POST to :create" do
      describe_with_arturo_setting_enabled :customer_satisfaction do
        describe "when user can update ticket" do
          let(:ticket) { tickets(:minimum_4) }
          let(:rating) { { satisfaction_rating: { score: "bad", comment: "Terrible" }} }

          before do
            @requester = ticket.requester

            login(@requester)

            post :create, params: rating.merge(ticket_id: ticket.nice_id)
          end

          should_use_presenter Api::V2::SatisfactionRatingPresenter
          should_change("ratings", by: 1) { Satisfaction::Rating.count(:all) }

          describe "Satisfaction rating event" do
            let(:rating_event) { ticket.events.last }

            it "should set via to 'web service'" do
              assert_equal 'Web service', rating_event.via
              assert_not_equal ticket.via, rating_event.via
            end
          end

          describe "without a comment" do
            let(:rating) { { satisfaction_rating: { score: "good" }} }

            it "does not keep the comment" do
              json = JSON.parse(@response.body)
              assert_nil json["satisfaction_rating"]["comment"]
            end
          end

          describe "with an invalid score" do
            let(:rating) { { satisfaction_rating: { score: "HAHA" }} }
            it('responds with bad_request') { assert_response :bad_request }
          end

          describe "with no params" do
            let(:rating) { {} }
            it('responds with bad_request') { assert_response :bad_request }
          end

          describe "with reason code enabled and present" do
            let(:reason_code_enabled) { true }

            describe "score is bad" do
              let(:rating) { { satisfaction_rating: { score: "bad", comment: "just awful", reason_code: @reason.reason_code }} }

              it "updates the reason code" do
                json = JSON.parse(@response.body)
                assert_equal @reason.id, json["satisfaction_rating"]["reason_id"]
              end
            end

            describe "score is good" do
              let(:rating) { { satisfaction_rating: { score: "good", comment: "it was fine", reason_code: @reason.reason_code }} }

              it "resets reason to NONE" do
                assert_equal Satisfaction::Reason::NONE, ticket.satisfaction_reason_code
              end
            end
          end
        end

        describe "when user submits a rating from Help Center" do
          let(:rating_event) { ticket.events.last }
          let(:ticket) { tickets(:minimum_4) }
          let(:rating) { { satisfaction_rating: { score: "bad", comment: "Terrible" }} }

          before do
            @requester = ticket.requester
            login(@requester)

            request.headers['User-Agent'] = 'HelpCenter'

            post :create, params: rating.merge(ticket_id: ticket.nice_id)
          end

          it "should set the via to 'help_center'" do
            assert_equal 'HelpCenter', rating_event.via
          end
        end

        describe "when creating a second satisfaction rating" do
          let(:second_rating) { { satisfaction_rating: { score: "bad", comment: "Second sat rating" }} }
          let(:ticket_with_rating) { build_ticket_with_satisfaction_rating }

          before do
            @requester = ticket_with_rating.requester

            login(@requester)

            assert_equal 1, ticket_with_rating.satisfaction_ratings.size
            assert_equal "I laughed. I cried. It was better than Cats.", ticket_with_rating.satisfaction_rating_comments.first.value

            post :create, params: second_rating.merge(ticket_id: ticket_with_rating.nice_id)
          end

          it "returns the more recent satisfaction rating" do
            assert_equal 2, ticket_with_rating.satisfaction_ratings.size

            json = JSON.parse(@response.body)
            assert_equal "Second sat rating", json["satisfaction_rating"]["comment"]
          end

          describe "and second rating has no comment" do
            let(:second_rating) { { satisfaction_rating: { score: "good", comment: "" }} }

            it "records comment as an empty string" do
              json = JSON.parse(@response.body)
              assert_equal "", json["satisfaction_rating"]["comment"]
            end
          end
        end
      end

      should_be_forbidden [:post, :create, { ticket_id: 1 }]
    end

    describe "a GET to :export" do
      before do
        Resque.stubs(:should_throttle?).returns(false)
      end

      describe "when enqueueing" do
        before { http_get_export }

        it('responds with ok') { assert_response :ok }

        it "responds with export status" do
          body = JSON.parse(@response.body)
          assert_equal 'enqueued', body['export']['status']
        end
      end

      describe "when throttled" do
        before do
          Resque.stubs(:should_throttle?).returns(true)
          get :export
        end

        it('responds with ok') { assert_response :ok }

        it "responds with export status" do
          body = JSON.parse(@response.body)
          assert_equal 'throttled', body['export']['status']
        end
      end

      describe "when filtering with an invalid score" do
        before do
          get :export, params: { score: "foobar" }
        end

        it "responds with bad_request" do
          assert_response :bad_request
        end
      end
    end

    describe "a GET to :count" do
      let(:iso_date) { '2020-12-15T03:21:55+00:00' }

      before do
        Zendesk::RecordCounter::SatisfactionRatings.any_instance.stubs(:iso_date).returns(iso_date)
        get :count
      end

      it "should respond with the correct format and values" do
        expected_response = {
          'count' => {
            'value' => @account.satisfaction_ratings.count,
            'refreshed_at' => iso_date
          },
          'links' => {
            'url' => 'https://minimum.zendesk-test.com/api/v2/satisfaction_ratings/count'
          }
        }

        assert_equal expected_response, JSON.parse(@response.body)
        assert_response :success
      end
    end
  end
end
