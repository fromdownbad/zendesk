require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::DashboardController do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :signatures

  let(:user) { users(:minimum_admin_not_owner) }
  let(:account) { user.account }

  before do
    accept :json
  end

  # should_route :get, "/api/v2/dashboard/agent", :action => "agent", :controller => "api/v2/dashboard"

  as_an_anonymous_user do
    should_be_unauthorized :agent
  end

  as_an_end_user do
    should_be_forbidden :agent
  end

  as_an_agent do
    describe "a GET to :agent" do
      before { get :agent }
      should_use_presenter Api::V2::DashboardPresenter
    end

    describe "with show_csat_on_dashboard disabled" do
      before do
        account.settings.show_csat_on_dashboard = false
      end

      it "does not include csat stats" do
        get :agent
        ['tickets_rated_good_this_week', 'tickets_rated_bad_this_week', 'sat_score_last_60_days', 'account_sat_score_last_60_days'].each do |key|
          assert_nil @response.body['dashboard'][key]
        end
      end
    end
  end

  as_an_agent do
    describe_with_arturo_enabled(:dashboard_agent_stats_caching) do
      before do
        @controller.stubs(:current_account).returns(account)
        get :agent
      end

      it("has HTTP header cache-control set with 300s TTL") do
        assert_equal @response.headers['Cache-Control'], 'max-age=300, private'
      end
    end

    describe_with_arturo_disabled(:dashboard_agent_stats_caching) do
      before do
        @controller.stubs(:current_account).returns(account)
        get :agent
      end

      it("has NO HTTP header cache-control set") do
        assert_blank @response.headers['Cache-Control']
      end
    end
  end
end
