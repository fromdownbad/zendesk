require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::GroupMembershipsController do
  include DomainEventsHelper
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :users, :groups, :memberships
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }
  let(:events) { decode_user_events(domain_event_publisher.events, :user_default_group_changed) }
  let(:new_group_encoded) { GroupProtobufEncoder.new(@new_group).to_object }
  let!(:membership_agent) { memberships(:minimum) }
  let!(:membership_admin) { memberships(:minimum_admin) }
  let(:encoded_membership_agent_group) { GroupProtobufEncoder.new(membership_agent.group).to_object }
  let(:encoded_membership_admin_group) { GroupProtobufEncoder.new(membership_admin.group).to_object }

  before do
    accept :json
    @membership = memberships(:minimum)
    @account    = accounts(:minimum)
    @agent      = users(:minimum_agent)
    @admin      = users(:minimum_admin)
    @old_group  = groups(:minimum_group)
    @new_group  = @account.groups.create!(name: "The dingalings")
    @end_user   = users(:minimum_end_user)
    Group.any_instance.stubs(:remove_from_phone_group_routing!)
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
  end

  with_options(controller: "api/v2/group_memberships") do |request|
    request.should_route :get,    "/api/v2/group_memberships",                       action: "index"
    request.should_route :get,    "/api/v2/group_memberships/assignable",            action: "assignable"
    request.should_route :get,    "/api/v2/group_memberships/1",                     action: "show", id: "1"
    request.should_route :get,    "/api/v2/group_memberships/count",                 action: "count"
    request.should_route :post,   "/api/v2/group_memberships",                       action: "create"
    request.should_route :post,   "/api/v2/group_memberships/create_many",           action: "create_many"
    request.should_route :delete, "/api/v2/group_memberships/1",                     action: "destroy", id: "1"
    request.should_route :delete, "/api/v2/group_memberships/destroy_many",          action: "destroy_many"
    request.should_route :get,    "/api/v2/users/2/group_memberships",               action: "index",       user_id: "2"
    request.should_route :get,    "/api/v2/users/2/group_memberships/count",         action: "count",       user_id: "2"
    request.should_route :get,    "/api/v2/users/2/group_memberships/1",             action: "show",        id: "1", user_id: "2"
    request.should_route :post,   "/api/v2/users/2/group_memberships",               action: "create",      user_id: "2"
    request.should_route :delete, "/api/v2/users/2/group_memberships/1",             action: "destroy",     id: "1", user_id: "2"
    request.should_route :put,    "/api/v2/users/2/group_memberships/1/make_default", action: "make_default", id: "1", user_id: "2"
    request.should_route :get,    "/api/v2/groups/1/memberships",                    action: "index",       group_id: "1"
    request.should_route :get,    "/api/v2/groups/1/memberships/assignable",         action: "assignable",  group_id: "1"
    request.should_route :get,    "/api/v2/groups/1/memberships/count",              action: "count",       group_id: "1"
  end

  as_an_end_user do
    should_be_forbidden :assignable, :index, :create, :destroy, :show, :destroy_many, :create_many, [:put, :make_default, {id: 1, user_id: 1}]
  end

  describe "per_page" do
    before do
      @params = { per_page: 123 }
      @controller.stubs(:params).returns(@params)
    end

    it "defaults to 100 records" do
      @params[:per_page] = nil
      assert_equal 100, @controller.send(:per_page)
    end

    it "allows less than 100 records" do
      @params[:per_page] = 50
      assert_equal 50, @controller.send(:per_page)
    end

    it "caps at a max of 1000 records" do
      @params[:per_page] = 2000
      assert_equal 1000, @controller.send(:per_page)
    end

    it "allows up to 1000 records" do
      @params[:per_page] = 700
      assert_equal 700, @controller.send(:per_page)
    end
  end

  as_an_agent do
    should_be_forbidden :create, :destroy_many, :create_many

    describe "a GET to :index" do
      describe "Emergency rate limiting for GET group_memberships" do
        describe "with an integer page param" do
          let(:limiter) { mock }
          let(:statsd_client) { mock }

          before { limiter.stubs(:throttle_index_with_deep_offset_pagination) }

          it "calls the pagination limiter" do
            Zendesk::OffsetPaginationLimiter.expects(:new).with(anything).returns(limiter)
            limiter.expects(:throttle_index_with_deep_offset_pagination).once

            get :index, params: { page: 1 }
          end

          describe "after 99 calls within 1 second" do
            let(:rate_limit) { 100 }

            before do
              Timecop.freeze
              Prop.throttle!(:index_group_memberships_with_deep_offset_pagination_ceiling_limit, @account.id, increment: rate_limit - 1)
            end

            describe "for page=1 and per_page=100" do
              let(:page) { 1 }

              it "responds with success on calls 100 and 101" do
                get :index, params: { page: page }
                assert_response :success
                get :index, params: { page: page }
                assert_response :success
              end

              describe "metrics and logging" do
                it "does not log any rate limit message" do
                  Rails.logger.expects(:info).with(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/)).never
                  # Once the above expectation is set up, you must specify that other calls might occur.
                  Rails.logger.expects(:info).with(Not(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not increment the 'group_memberships' ceiling rate limit metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "group_memberships").at_least(0).returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_group_memberships_index_ceiling_rate_limit_log_only", anything).never
                  statsd_client.expects(:increment).with("api_v2_group_memberships_index_ceiling_rate_limit", anything).never
                  2.times { get :index, params: { page: page } }
                end
              end
            end

            describe "for page=10,001 and per_page=100" do
              let(:page) { 10_001 }

              it "responds with success on calls 100 and 101" do
                get :index, params: { page: page }
                assert_response :success
                get :index, params: { page: page }
                assert_response :success
              end

              describe "metrics and logging" do
                it "logs the log_only message" do
                  Rails.logger.expects(:info).with(regexp_matches(/Log only: would have rate limited #index by account/)).once
                  Rails.logger.expects(:info).with(Not(regexp_matches(/Log only: would have rate limited #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not log the message for the active arturo" do
                  Rails.logger.expects(:info).with(regexp_matches(/Rate limiting #index by account/)).never
                  Rails.logger.expects(:info).with(Not(regexp_matches(/Rate limiting #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not increment the api_v2_group_memberships_index_ceiling_rate_limit metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "group_memberships").once.returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_group_memberships_index_ceiling_rate_limit", anything).never
                  statsd_client.expects(:increment).with(Not(equals("api_v2_group_memberships_index_ceiling_rate_limit")), anything).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "increments the log_only metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "group_memberships").once.returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_group_memberships_index_ceiling_rate_limit_log_only", anything).once
                  2.times { get :index, params: { page: page } }
                end
              end
            end
          end
        end
      end

      describe "without includes" do
        before { get :index }
        should_use_presenter Api::V2::GroupMembershipPresenter

        it "lists records for all subscribed users across all organizations" do
          assert_equal @account.memberships.active(@account.id).count, JSON.parse(@response.body)["group_memberships"].size
        end
      end

      describe "with a group_id" do
        before { get :index, params: { group_id: @new_group.id } }
        should_use_presenter Api::V2::GroupMembershipPresenter
      end

      describe "with includes" do
        before { get :index, params: { include: "users,groups" } }
        should_use_presenter Api::V2::GroupMembershipPresenter

        it "includes the users and groups used by the memberships" do
          json = JSON.parse(@response.body)
          assert_equal json["groups"].map { |r| r["id"] }.sort, json["group_memberships"].map { |r| r["group_id"] }.sort.uniq
          assert_equal json["users"].map { |r| r["id"] }.sort, json["group_memberships"].map { |r| r["user_id"] }.sort.uniq
        end
      end

      describe "with cursor pagination" do
        should_support_cursor_pagination_conversion :index, 'group_memberships', :cursor_pagination_group_memberships_index, :remove_offset_pagination_group_memberships_index, table_name: 'memberships'
      end
    end

    describe "a GET to :assignable" do
      describe "without includes" do
        before { get :assignable }
        should_use_presenter Api::V2::GroupMembershipPresenter
      end

      describe "with a group_id" do
        before { get :assignable, params: { group_id: @new_group.id } }
        should_use_presenter Api::V2::GroupMembershipPresenter
      end

      describe "with includes" do
        before { get :assignable, params: { include: "users,groups" } }
        should_use_presenter Api::V2::GroupMembershipPresenter

        it "includes the users and groups used by the memberships" do
          json = JSON.parse(@response.body)
          assert_equal json["groups"].map { |r| r["id"] }.sort, json["group_memberships"].map { |r| r["group_id"] }.sort.uniq
          assert_equal json["users"].map { |r| r["id"] }.sort, json["group_memberships"].map { |r| r["user_id"] }.sort.uniq
        end
      end

      describe "with cursor pagination" do
        should_support_cursor_pagination_conversion :assignable, 'group_memberships', :cursor_pagination_group_memberships_assignable, :remove_offset_pagination_group_memberships_assignable, table_name: 'memberships'
      end
    end

    describe "a GET to :show" do
      before { get :show, params: { id: @membership.id } }
      should_use_presenter Api::V2::GroupMembershipPresenter
    end

    describe "a DELETE to :destroy" do
      before { delete :destroy, params: { id: @membership.id } }
      it('responds with forbidden') { assert_response :forbidden }
    end

    describe "a PUT to :make_default" do
      before do
        put :make_default, params: { user_id: @agent.id, id: @membership.id }
      end
      it('responds with forbidden') { assert_response :forbidden }
    end
  end

  as_an_admin do
    describe "a GET to :index" do
      describe "with an inactive group" do
        before do
          group = groups(:minimum_group)
          group.current_user = @user
          group.deactivate!

          get :index
        end

        it "returns no results" do
          assert_equal [], JSON.parse(@response.body)["group_memberships"]
        end
      end

      describe "with proper groups" do
        before { get :index }
        should_use_presenter Api::V2::GroupMembershipPresenter

        it "uses ETag" do
          assert_etagged :index do
            @membership.update_attribute(:updated_at, 1.minute.from_now)
            @controller.instance_variable_set(:@memberships, nil)
          end
        end
      end
    end

    describe "a GET to :show" do
      describe "with an inactive group" do
        before do
          group = @membership.group
          group.current_user = @user
          group.deactivate!

          get :show, params: { id: @membership.id }
        end

        it('responds with not_found') { assert_response :not_found }
      end

      describe "with a proper group" do
        before { get :show, params: { id: @membership.id } }
        should_use_presenter Api::V2::GroupMembershipPresenter
      end
    end

    describe "a POST to :create" do
      describe "with valid params" do
        before { post :create, params: { group_membership: { user_id: @agent.id, group_id: @new_group.id } } }

        should_use_presenter Api::V2::GroupMembershipPresenter, status: :created, location: /\/api\/v2\/group_memberships\/\d+\.json$/
        should_change("the membership count", by: 1) { Membership.count(:all) }
      end

      describe "with invalid params" do
        before do
          post :create, params: { group_membership: { user_id: @end_user.id, group_id: @new_group.id } }
        end
        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
      end

      describe "publish user events to the bus at :create" do
        before do
          post :create, params: { group_membership: { user_id: @agent.id, group_id: @new_group.id, default: true } }
        end

        it "contains UserDefaultGroupChanged event on :create" do
          assert @user.id, CIA.current_actor
          assert_equal 1, events.size
          assert_nil events[0].user_default_group_changed.previous
          assert_equal new_group_encoded.id, events[0].user_default_group_changed.current.id
          assert_equal @user.id, events[0].actor.id.value
        end
      end
    end

    describe "a POST to :create_many" do
      describe "with valid params" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          GroupMembershipBulkCreateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          memberships = [
            { user_id: @agent.id, group_id: @new_group.id },
            { user_id: @admin.id, group_id: @new_group.id }
          ]
          post :create_many, params: { group_memberships: memberships }
        end
        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with invalid params" do
        before { post :create_many }
        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with duplicate default memberships" do
        before do
          memberships = [
            { user_id: @agent.id, group_id: @old_group.id, default: true },
            { user_id: @agent.id, group_id: @new_group.id, default: true },
            { user_id: @admin.id, group_id: @new_group.id, default: false }
          ]

          post :create_many, params: { group_memberships: memberships }
        end

        it('responds with bad_request') { assert_response :bad_request }

        it('has the CIA current_actor set for auditing purposes') do
          assert @user.id, CIA.current_actor
        end
      end
    end

    describe "publish user events to the bus at :create_many" do
      before do
        memberships = [
          { user_id: @agent.id, group_id: @new_group.id, default: true },
          { user_id: @admin.id, group_id: @new_group.id, default: true }
        ]
        post :create_many, params: { group_memberships: memberships }
      end

      it "contains UserDefaultGroupChanged event once per each membership created as default" do
        assert @user.id, CIA.current_actor
        assert_equal 2, events.size
        assert_equal 2, events.map(&:user_default_group_changed).map(&:previous).select(&:nil?).count
        current_values = events.map(&:user_default_group_changed).map(&:current).map(&:id)
        assert_includes current_values, new_group_encoded.id
      end
    end

    describe "a DELETE to :destroy" do
      describe "the only membership" do
        before do
          UnassignTicketsJob.expects(:enqueue)
        end

        it "responds with 204" do
          delete :destroy, params: { id: @membership.id }
          assert_response :no_content
        end
      end
    end

    describe "a DELETE to :destroy_many" do
      describe "with valid ids" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          GroupMembershipBulkDeleteJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          delete :destroy_many, params: { ids: "1,2,3" }
        end
        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with invalid params" do
        before { delete :destroy_many }
        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "publish user events to the bus at :destroy_many" do
        before do
          membership_agent.update_column :default, true
          membership_admin.update_column :default, true
          UnassignTicketsJob.expects(:enqueue).twice
          delete :destroy_many, params: { ids: [membership_agent.id, membership_admin.id].join(',') }
        end

        it 'contains UserDefaultGroupChanged on :destroy_many' do
          assert_equal 2, events.size
          assert_equal 2, events.map(&:user_default_group_changed).map(&:current).select(&:nil?).count
          previous_values = events.map(&:user_default_group_changed).map(&:previous).map(&:id)
          assert_includes previous_values, encoded_membership_agent_group.id
          assert_includes previous_values, encoded_membership_admin_group.id
        end
      end
    end

    describe "a PUT to :make_default" do
      let(:previous_group_encoded) { GroupProtobufEncoder.new(@membership.group).to_object }
      let(:current_group_encoded) { GroupProtobufEncoder.new(@new_group).to_object }
      before do
        @membership.update_column :default, true
        @new_membership = Membership.create!(group: @new_group, user: @agent)
        put :make_default, params: { user_id: @agent.id, id: @new_membership.id }
      end

      it "sets the group's default status" do
        assert @new_membership.reload.default?
      end

      it "unsets other group's default status" do
        assert_equal false, @membership.reload.default?
      end
      it('responds with ok') { assert_response :ok }

      describe "publish user events to the bus at :mark_default" do
        it 'contains UserDefaultGroupChanged on :mark_default' do
          assert_equal 1, events.size
          assert_equal current_group_encoded.id, events[0].user_default_group_changed.current.id
          assert_equal previous_group_encoded.id, events[0].user_default_group_changed.previous.id
        end
      end
    end
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    should_be_forbidden :assignable, :destroy_many, [:put, :make_default, { id: 1, user_id: 1 }]

    describe "a GET to :index" do
      before { get :index }
      it('responds with success') { assert_response :success }
    end

    describe "a GET to :show" do
      before { get :show, params: { id: @membership.id } }
      should_use_presenter Api::V2::GroupMembershipPresenter
    end

    describe "a POST to :create" do
      describe "with valid params" do
        before { post :create, params: { group_membership: { user_id: @agent.id, group_id: @new_group.id } } }

        should_use_presenter Api::V2::GroupMembershipPresenter, status: :created, location: /\/api\/v2\/group_memberships\/\d+\.json$/
        should_change("the membership count", by: 1) { Membership.count(:all) }
      end

      describe "with invalid params" do
        before do
          post :create, params: { group_membership: { user_id: @end_user.id, group_id: @new_group.id } }
        end
        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
      end
    end

    describe "a POST to :create_many" do
      describe "with valid params" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          GroupMembershipBulkCreateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          memberships = [
            { user_id: @agent.id, group_id: @new_group.id },
            { user_id: @admin.id, group_id: @new_group.id }
          ]

          post :create_many, params: { group_memberships: memberships }
        end
        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with invalid params" do
        before { post :create_many }
        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with duplicate default memberships" do
        before do
          memberships = [
            { user_id: @agent.id, group_id: @old_group.id, default: true },
            { user_id: @agent.id, group_id: @new_group.id, default: true },
            { user_id: @admin.id, group_id: @new_group.id, default: false }
          ]

          post :create_many, params: { group_memberships: memberships }
        end

        it('responds with bad_request') { assert_response :bad_request }
      end
    end

    describe "a DELETE to :destroy" do
      describe "the only membership" do
        before do
          UnassignTicketsJob.expects(:enqueue)
        end

        it "responds with 204" do
          delete :destroy, params: { id: @membership.id }
          assert_response :no_content
        end
      end

      describe "publish user events to the bus at :destroy" do
        let(:membership_group_encoded) { GroupProtobufEncoder.new(@membership.group).to_object }
        before do
          @membership.update_column :default, true
          UnassignTicketsJob.expects(:enqueue)
        end

        it "contains UserDefaultGroupChanged on :destroy" do
          delete :destroy, params: { id: @membership.id }
          assert_nil events[0].user_default_group_changed.current
          assert_equal membership_group_encoded.id, events[0].user_default_group_changed.previous.id
          assert_equal 1, events.size
        end
      end
    end
  end

  as_a_subsystem_user(user: 'bime', account: :minimum) do
    should_be_forbidden :assignable, :create, :destroy, :show, :destroy_many, :create_many, [:put, :make_default, {id: 1, user_id: 1}]

    describe "a GET to :index" do
      before { get :index }
      it('responds with success') { assert_response :success }
    end
  end
end
