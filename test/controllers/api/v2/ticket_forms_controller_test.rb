require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::V2::TicketFormsController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper
  extend MobileSdk::RateLimitHelper

  fixtures :accounts, :users, :ticket_fields, :custom_field_options, :account_settings, :role_settings, :mobile_sdk_auths, :brands

  before do
    accept :json
    @account = accounts(:minimum)
    setup_ticket_forms
  end

  with_options(controller: 'api/v2/ticket_forms') do |request|
    request.should_route :post,    '/api/v2/ticket_forms',   action: 'create'
    request.should_route :get,     '/api/v2/ticket_forms',   action: 'index'
    request.should_route :get,     '/api/v2/ticket_forms/1', action: 'show',    id: '1'
    request.should_route :put,     '/api/v2/ticket_forms/1', action: 'update',  id: '1'
    request.should_route :delete,  '/api/v2/ticket_forms/1', action: 'destroy', id: '1'
    request.should_route :post,    '/api/v2/ticket_forms/1/clone', action: 'clone', id: '1'
    request.should_route :put,     '/api/v2/ticket_forms/reorder', action: 'reorder'
    request.should_route :get,     '/api/v2/ticket_forms/show_many', action: 'show_many'
  end

  it "does not rate limit non-SDK requests to #show_many" do
    ids = [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id]

    Prop.expects(:throttle!).with(:mobile_sdk_ticket_forms_show_many, [@account.id, "mobile_sdk_ticket_forms_show_many"]).never
    get :show_many, params: { ids: ids.join(",") }
  end

  as_an_anonymous_user do
    should_be_unauthorized :create, :show, :update, :destroy, :index, [:post, :clone, {id: 1}], :reorder

    describe "for accounts with Ticket Forms feature" do
      before { @controller.send(:current_account).stubs(:has_ticket_forms?).returns(true) }

      describe "a GET to :index" do
        before { get :index }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200

        it "only present end user visible ticket forms" do
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal 2, res.count
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id], res.map { |ticket_form| ticket_form['id'] }
        end
      end

      describe "a GET to :show_many" do
        let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }

        before do
          get :show_many, params: { ids: ids.join(",") }
        end

        should_use_presenter Api::V2::TicketFormPresenter, status: 200

        it "only present end user visible ticket forms" do
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal 2, res.count
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id], res.map { |ticket_form| ticket_form['id'] }
        end

        it "should have CORS headers" do
          assert_equal "*", response.headers["Access-Control-Allow-Origin"]
        end
      end

      describe "a GET to :show_many without passing in ids" do
        before do
          get :show_many
        end

        it "only present end user visible ticket forms" do
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal 2, res.count
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id], res.map { |ticket_form| ticket_form['id'] }
        end
      end

      describe "with ticket_forms_cache_control_header off" do
        before { Arturo.disable_feature!(:ticket_forms_cache_control_header) }

        it 'a GET to :index sets cache-control to max-age=0' do
          get :index
          assert_nil @response.headers['Cache-Control']
        end

        it 'a GET to :show_many sets cache-control to max-age=0' do
          get :show_many
          assert_nil @response.headers['Cache-Control']
        end
      end

      describe "with ticket_forms_cache_control_header on" do
        before { Arturo.enable_feature!(:ticket_forms_cache_control_header) }

        it 'a GET to :index sets cache-control to max-age=30, public' do
          get :index
          assert_equal @response.headers['Cache-Control'], 'max-age=60, private'
        end

        it 'a GET to :show_many sets cache-control to max-age=30, public' do
          get :show_many
          assert_equal @response.headers['Cache-Control'], 'max-age=60, private'
        end
      end
    end
  end

  as_an_end_user do
    should_be_forbidden :create, :update, :destroy, [:post, :clone, {id: 1}], :reorder

    describe "for accounts with Ticket Forms feature" do
      before { @controller.send(:current_account).stubs(:has_ticket_forms?).returns(true) }

      describe "a GET to :show" do
        describe "when the ticket form is end user visible" do
          before { get :show, params: { id: @public_ticket_form.id } }
          should_use_presenter Api::V2::TicketFormPresenter, status: 200
        end

        describe "when the ticket form is not end user visible" do
          before { get :show, params: { id: @agent_ticket_form.id } }
          it('responds with not_found') { assert_response :not_found }
        end

        describe "with conditions" do
          it "presents only end user nested conditions" do
            @ticket_form = FactoryBot.create(:ticket_form, account: @account)
            FactoryBot.create(:ticket_field_condition, account: @account, ticket_form: @ticket_form, user_type: 'agent')
            # Load the ticket_form_fields created by the previous factory
            @ticket_form.reload
            FactoryBot.create(:ticket_field_condition, account: @account, ticket_form: @ticket_form, user_type: 'end_user')
            get :show, params: { id: @ticket_form.id }
            assert_nil JSON.parse(@response.body)["ticket_form"]["agent_conditions"]
            assert_equal JSON.parse(@ticket_form.end_user_conditions.to_json),
              JSON.parse(@response.body)["ticket_form"]["end_user_conditions"]
          end
        end
      end

      describe "a GET to :index" do
        before { get :index }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200

        it "only present end user visible ticket forms" do
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal 2, res.count
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id], res.map { |ticket_form| ticket_form['id'] }
        end
      end

      describe "a GET to :show_many" do
        let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }

        before do
          get :show_many, params: { ids: ids.join(",") }
        end

        should_use_presenter Api::V2::TicketFormPresenter, status: 200

        it "only present end user visible ticket forms" do
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal 2, res.count
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id], res.map { |ticket_form| ticket_form['id'] }
        end
      end
    end

    describe "for accounts without Ticket Forms feature" do
      before { @controller.send(:current_account).stubs(:has_ticket_forms?).returns(false) }

      should_be_forbidden :index

      describe "a GET to :show" do
        before { get :show, params: { id: @public_ticket_form.id } }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200
      end

      describe "a GET to :show_many" do
        let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }
        before { get :show_many, params: { ids: ids.join(",") } }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200
      end
    end
  end

  # End user authenticated with SDK anonymous auth
  as_an_sdk_anonymous_end_user do
    before do
      @account = accounts(:minimum_sdk)
      setup_ticket_forms
    end

    should_be_forbidden :create, :update, :destroy, [:post, :clone, {id: 1}], :reorder

    describe "for accounts with Ticket Forms feature" do
      let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }

      before { @controller.send(:current_account).stubs(:has_ticket_forms?).returns(true) }

      params = [
        :mobile_sdk_limit_high,
        "mobile_sdk_ticket_forms_show_many",
        :enable_throttle_for_mobile_sdk_ticket_forms_show_many,
        :mobile_sdk_ticket_forms_show_many_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        get :show_many, params: { ids: ids.join(",") }
      end

      describe "a GET to :show_many" do
        before do
          get :show_many, params: { ids: ids.join(",") }
        end

        should_use_presenter Api::V2::TicketFormPresenter, status: 200

        it "only present end user visible ticket forms" do
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal 2, res.count
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id], res.map { |ticket_form| ticket_form['id'] }
        end
      end
    end

    describe "for accounts without Ticket Forms feature" do
      before { @controller.send(:current_account).stubs(:has_ticket_forms?).returns(false) }

      should_be_forbidden :index

      describe "a GET to :show_many" do
        let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }
        before { get :show_many, params: { ids: ids.join(",") } }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200
      end
    end
  end

  # End user authenticated with SDK JWT auth
  as_an_sdk_jwt_end_user do
    before do
      @account = accounts(:minimum_sdk)
      setup_ticket_forms
    end

    should_be_forbidden :create, :update, :destroy, [:post, :clone, {id: 1}], :reorder

    describe "for accounts with Ticket Forms feature" do
      before { @controller.send(:current_account).stubs(:has_ticket_forms?).returns(true) }

      let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }

      params = [
        :mobile_sdk_limit_high,
        "mobile_sdk_ticket_forms_show_many",
        :enable_throttle_for_mobile_sdk_ticket_forms_show_many,
        :mobile_sdk_ticket_forms_show_many_emergency_limit
      ]

      it_throttles_mobile_sdk_requests *params do
        get :show_many, params: { ids: ids.join(",") }
      end

      describe "a GET to :show_many" do
        before do
          get :show_many, params: { ids: ids.join(",") }
        end

        should_use_presenter Api::V2::TicketFormPresenter, status: 200

        it "only present end user visible ticket forms" do
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal 2, res.count
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id], res.map { |ticket_form| ticket_form['id'] }
        end
      end
    end

    describe "for accounts without Ticket Forms feature" do
      before { @controller.send(:current_account).stubs(:has_ticket_forms?).returns(false) }

      should_be_forbidden :index

      describe "a GET to :show" do
        before { get :show, params: { id: @public_ticket_form.id } }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200
      end

      describe "a GET to :show_many" do
        let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }
        before { get :show_many, params: { ids: ids.join(",") } }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200
      end
    end
  end

  as_an_agent do
    should_be_forbidden :create, :update, :destroy, [:post, :clone, {id: 1}], :reorder

    describe "for accounts with Ticket Forms feature" do
      before { @controller.send(:current_account).stubs(:has_ticket_forms?).returns(true) }

      describe "a GET to :index" do
        before { get :index }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200

        it "presents all ticket forms for the account, both end user visible and non end user visible" do
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id, @agent_ticket_form.id].sort,
            res.map { |ticket_form| ticket_form['id'] }.sort
        end
      end

      describe "a GET to :index with parameters" do
        it "presents only active ticket forms if we send active=true" do
          @agent_ticket_form.update_attribute(:active, false)
          get :index, params: { active: true }
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id].sort, res.map { |ticket_form| ticket_form['id'] }.sort
        end

        it "presents only inactive ticket forms if we send active=false" do
          @agent_ticket_form.update_attribute(:active, false)
          get :index, params: { active: false }
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal [@agent_ticket_form.id].sort, res.map { |ticket_form| ticket_form['id'] }.sort
        end

        it "presents only end user visible ticket forms if we send end_user_visible=true" do
          get :index, params: { end_user_visible: true }
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id].sort, res.map { |ticket_form| ticket_form['id'] }.sort
        end

        it "presents only non end user visible ticket forms if we send end_user_visible=false" do
          get :index, params: { end_user_visible: false }
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal [@agent_ticket_form.id].sort, res.map { |ticket_form| ticket_form['id'] }.sort
        end

        it "presents the default form if we get no forms with the criteria defined by the filters and we send fallback_to_default=true" do
          @public_ticket_form.update_attribute(:default, true)
          get :index, params: { active: false, fallback_to_default: true }
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal [@public_ticket_form.id].sort, res.map { |ticket_form| ticket_form['id'] }.sort
        end
      end

      describe "a GET to :show" do
        before { get :show, params: { id: @public_ticket_form.id } }
        should_use_presenter Api::V2::TicketFormPresenter
      end

      describe "for accounts with multiple active brands and access to branded ticket forms" do
        before do
          @controller.send(:current_account).stubs(:has_multiple_active_brands?).returns(true)

          params = {ticket_form: { name: "For Brand", display_name: "For Brand", position: 2,
                                   default: false, active: true, end_user_visible: true } }
          @branded_ticket_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
          @branded_ticket_form.save!
        end

        it "presents the ticket forms of the current brand if we send associated_to_brand=true" do
          @account.default_brand.stubs(:ticket_forms).returns(TicketForm.where(id: [@branded_ticket_form.id]))
          @controller.stubs(:current_brand).returns(@account.default_brand)
          get :index, params: { associated_to_brand: true }
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal [@branded_ticket_form.id].sort, res.map { |ticket_form| ticket_form['id'] }.sort
        end
      end

      describe "a GET to :show_many" do
        let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }

        before do
          get :show_many, params: { ids: ids.join(",") }
        end

        should_use_presenter Api::V2::TicketFormPresenter, status: 200

        it "presents all ticket forms for the account, both end user visible and non end user visible" do
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal 3, res.count
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id, @agent_ticket_form.id],
            res.map { |ticket_form| ticket_form['id'] }.sort
        end
      end
    end

    describe ":index and :show_many" do
      let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }

      it "does not do n + 1 queries" do
        assert_no_n_plus_one do
          get :index
        end

        assert_no_n_plus_one do
          get :show_many, params: { ids: ids.join(",") }
        end
      end
    end

    describe "for accounts without Ticket Forms feature" do
      before { @controller.send(:current_account).stubs(:has_ticket_forms?).returns(false) }

      should_be_forbidden :index

      describe "a GET to :show" do
        before { get :show, params: { id: @public_ticket_form.id } }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200
      end

      describe "a GET to :show_many" do
        let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }
        before { get :show_many, params: { ids: ids.join(",") } }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200
      end
    end
  end

  as_an_admin do
    describe "for accounts with Ticket Forms feature" do
      before { @controller.send(:current_account).stubs(:has_ticket_forms?).returns(true) }

      describe "a POST to :create" do
        before do
          params = {ticket_form: { name: "Wombat", display_name: "Wombats are everywhere", position: 5, } }
          post :create, params: params
        end
        should_use_presenter Api::V2::TicketFormPresenter, status: 201
      end

      describe "a POST to :create with in_all_brands nil" do
        before do
          params = {
            ticket_form: {
              name: "Wombat",
              display_name: "Wombats are everywhere",
              position: 5,
              in_all_brands: nil
            }
          }

          post :create, params: params
        end

        it "responds with an error message" do
          body = JSON.parse(@response.body)
          ticket_form = TicketForm.find(body['ticket_form']['id'])
          assert ticket_form.in_all_brands
        end
      end

      describe "a POST to :create with raw params" do
        before do
          params = {ticket_form: { raw_name: "Raw Wombat", raw_display_name: "Raw Wombats are everywhere", position: 5, } }
          post :create, params: params
        end
        should_use_presenter Api::V2::TicketFormPresenter, status: 201

        it "maps to the attributes correctly" do
          ticket_form = @controller.send(:current_account).ticket_forms.last
          assert_equal "Raw Wombat", ticket_form.name
          assert_equal "Raw Wombats are everywhere", ticket_form.display_name
        end
      end

      describe "a POST to :create with no specified position and no current ticket_form" do
        it "automatically assigns a position to the number of ticket forms in the account + 1 (before posting)" do
          @controller.send(:current_account).ticket_forms.delete_all
          # not in before as we need to access the count + 1 before posting
          params = {ticket_form: { raw_name: "Positionless Wombat", raw_display_name: "Where am I??" } }
          post :create, params: params

          ticket_form = @controller.send(:current_account).ticket_forms.last
          assert_equal 1, ticket_form.position
        end
      end

      describe "a POST to :create with no specified position" do
        it "automatically assigns a position to the number of ticket forms in the account + 1 (before posting)" do
          res = @controller.send(:current_account).ticket_forms.maximum(:position) + 1

          # not in before as we need to access the count + 1 before posting
          params = {ticket_form: { raw_name: "Positionless Wombat", raw_display_name: "Where am I??" } }
          post :create, params: params

          ticket_form = @controller.send(:current_account).ticket_forms.last
          assert_equal res, ticket_form.position
        end
      end

      describe "a POST to :create with conditions" do
        it "creates conditions" do
          tagger1 = FactoryBot.create(:field_tagger, account: @account)
          tagger2 = FactoryBot.create(:field_tagger, account: @account)
          cfo1 = tagger1.custom_field_options.first

          agent_conditions = [
            {
              parent_field_id: tagger1.id,
              value: cfo1.value,
              child_fields: [
                {
                  id: tagger2.id,
                  is_required: false,
                  required_on_statuses: { type: TicketFieldCondition::REQUIRED_ON_NO_STATUSES }
                }
              ]
            }
          ]

          end_user_conditions = [
            {
              parent_field_id: tagger1.id,
              value: cfo1.value,
              child_fields: [
                { id: tagger2.id, is_required: false }
              ]
            }
          ]

          params = {
            ticket_form: {
              raw_name: "With Conditions",
              raw_display_name: "With Conditions",
              position: 5,
              ticket_field_ids: [tagger1.id, tagger2.id],
              agent_conditions: agent_conditions,
              end_user_conditions: end_user_conditions
            }
          }

          post :create, params: params

          assert_response :success

          stringified_agent_conditions = JSON.parse(agent_conditions.to_json)
          stringified_end_user_conditions = JSON.parse(end_user_conditions.to_json)

          assert_equal stringified_agent_conditions, JSON.parse(@response.body)["ticket_form"]["agent_conditions"]
          assert_equal stringified_end_user_conditions, JSON.parse(@response.body)["ticket_form"]["end_user_conditions"]
        end

        it "doesn't create conditions when CTF is off" do
          Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(false)

          tagger1 = FactoryBot.create(:field_tagger, account: @account)
          tagger2 = FactoryBot.create(:field_tagger, account: @account)
          cfo1 = tagger1.custom_field_options.first

          conditions = [
            {
              parent_field_id: tagger1.id,
              value: cfo1.value,
              child_fields: [
                { id: tagger2.id, is_required: false }
              ]
            }
          ]

          params = {
            ticket_form: {
              raw_name: "With Conditions",
              raw_display_name: "With Conditions",
              position: 5,
              ticket_field_ids: [tagger1.id, tagger2.id],
              agent_conditions: conditions,
              end_user_conditions: conditions
            }
          }

          assert_no_difference 'TicketFieldCondition.count' do
            post :create, params: params
          end

          assert_response :success

          json = JSON.parse(@response.body)

          assert_nil json["ticket_form"]["agent_conditions"]
          assert_nil json["ticket_form"]["end_user_conditions"]
        end

        it "accepts and correctly sets required_on_statuses" do
          tagger1 = FactoryBot.create(:field_tagger, account: @account)
          tagger2 = FactoryBot.create(:field_tagger, account: @account)
          cfo1 = tagger1.custom_field_options.first

          conditions = [
            {
              parent_field_id: tagger1.id,
              value: cfo1.value,
              child_fields: [
                {
                  id: tagger2.id,
                  is_required: false,
                  required_on_statuses: {
                    type: TicketFieldCondition::REQUIRED_ON_SOME_STATUSES,
                    statuses: ['new', 'open']
                  }
                }
              ]
            }
          ]

          params = {
            ticket_form: {
              raw_name: "With Conditions",
              raw_display_name: "With Conditions",
              position: 5,
              ticket_field_ids: [tagger1.id, tagger2.id],
              agent_conditions: conditions
            }
          }

          TicketFieldCondition.any_instance.expects(:required_on_statuses=).with('0 1')
          post :create, params: params
          assert_response :success
        end
      end

      describe "a POST to :create with invalid payload" do
        before do
          params = {ticket_form: { display_name: "Wombat" } }
          post :create, params: params
        end
        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
      end

      describe_with_arturo_enabled :ticket_forms_cache_control_header do
        # We don't want to use cache control for admins using Lotus because it causes refresh problems
        describe "for lotus internal requests" do
          before { set_header('X-Zendesk-Lotus-Version', 'v1.2.3') }

          it 'a GET to :index does not set cache-control' do
            get :index
            assert_nil @response.headers['Cache-Control']
          end

          it 'a GET to :show_many does not set cache-control' do
            get :show_many
            assert_nil @response.headers['Cache-Control']
          end
        end

        describe "for requests from apps inside lotus" do
          before do
            set_header('X-Zendesk-Lotus-Version', 'v1.2.3')
            set_header('X-Zendesk-App-Id', '123')
          end

          it 'a GET to :index sets cache-control' do
            get :index
            assert_equal @response.headers['Cache-Control'], 'max-age=60, private'
          end

          it 'a GET to :show_many sets cache-control' do
            get :show_many
            assert_equal @response.headers['Cache-Control'], 'max-age=60, private'
          end
        end

        describe "for non lotus requests" do
          before { set_header('X-Zendesk-Lotus-Version', nil) }

          it 'a GET to :index sets cache-control' do
            get :index
            assert_equal @response.headers['Cache-Control'], 'max-age=60, private'
          end

          it 'a GET to :show_many sets cache-control' do
            get :show_many
            assert_equal @response.headers['Cache-Control'], 'max-age=60, private'
          end
        end
      end

      describe "a GET to :index" do
        before { get :index }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200

        it "presents all ticket forms for the account, both end user visible and non end user visible" do
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id].sort,
            res.map { |ticket_form| ticket_form['id'] }.sort
        end
      end

      describe "a GET to :show" do
        describe "normal" do
          before { get :show, params: { id: @public_ticket_form.id } }
          should_use_presenter Api::V2::TicketFormPresenter
        end

        describe "with an id of a previously deleted ticket form" do
          before { get :show, params: { id: @deleted_ticket_form.id } }
          should_use_presenter Api::V2::TicketFormPresenter
        end

        describe "with an id of an inactive ticket form" do
          before do
            @public_ticket_form.active = false
            @public_ticket_form.save!
            get :show, params: { id: @public_ticket_form.id }
          end

          should_use_presenter Api::V2::TicketFormPresenter
        end

        describe "on ticket form with conditions" do
          it "presents nested conditions" do
            @ticket_form = FactoryBot.create(:ticket_field_condition, account: @account).ticket_form
            @ticket_form.reload
            get :show, params: { id: @ticket_form.id }
            assert_equal JSON.parse(@ticket_form.agent_conditions.to_json),
              JSON.parse(@response.body)["ticket_form"]["agent_conditions"]
          end
        end

        describe "on ticket form with conditions with required_on_statuses" do
          it "presents required_on_statuses" do
            shaped_required_on_statuses = {
              type: TicketFieldCondition::REQUIRED_ON_ALL_STATUSES
            }
            TicketFieldCondition.any_instance.expects(:shaped_required_on_statuses).returns(shaped_required_on_statuses)
            @ticket_form = FactoryBot.create(:ticket_field_condition, account: @account).ticket_form
            get :show, params: { id: @ticket_form.id }
            assert_equal JSON.parse(@response.body)["ticket_form"]["agent_conditions"][0]["child_fields"][0]["required_on_statuses"]["type"], TicketFieldCondition::REQUIRED_ON_ALL_STATUSES
          end
        end
      end

      describe "a PUT to :update" do
        describe "when the form has ticket fields with apps requirements" do
          before do
            ticket_field = ticket_fields(:field_text_custom)
            ticket_field_with_apps_requirement = ticket_fields(:field_integer_custom)
            FactoryBot.create(:resource_collection_resource,
              resource: ticket_field_with_apps_requirement,
              account_id: ticket_field_with_apps_requirement.account.id)

            old_fields = @public_ticket_form.ticket_fields
            new_fields = [ticket_field, ticket_field_with_apps_requirement]

            ticket_fields = old_fields + new_fields
            @ticket_fields_with_reversed_new_fields = old_fields + new_fields.reverse

            opts = {id: @public_ticket_form.id, ticket_form: {ticket_field_ids: ticket_fields.map(&:id)}}
            Zendesk::TicketForms::Initializer.new(@account, opts).updated_ticket_form.save

            put :update, params: { id: @public_ticket_form.id, ticket_form: {ticket_field_ids: @ticket_fields_with_reversed_new_fields.map(&:id)} }
          end

          it 'responds with ok' do
            assert_response :ok
          end

          it "responds with the correct/new fields order" do
            body = JSON.parse(@response.body)
            assert_equal body["ticket_form"]["ticket_field_ids"], @ticket_fields_with_reversed_new_fields.map(&:id)
          end

          it "allows me to update the position of the fields in the form" do
            assert_equal @public_ticket_form.reload.ticket_fields.map(&:id), @ticket_fields_with_reversed_new_fields.map(&:id)
          end
        end

        describe "in account with multiple active brands" do
          let(:brand) { brands(:minimum) }

          before do
            Account.any_instance.stubs(:has_multibrand?).returns(true)
            Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)
          end

          it "responds with the correct restricted_brand_ids when a brand is added" do
            put :update, params: { id: @agent_ticket_form.id, ticket_form: {
              in_all_brands: false,
              restricted_brand_ids: [brand.id]
            } }

            body = JSON.parse(@response.body)
            assert_equal body["ticket_form"]["restricted_brand_ids"], [brand.id]
          end

          it "responds with the correct restricted_brand_ids when a brand is removed" do
            @agent_ticket_form.restricted_brands << brand
            @agent_ticket_form.in_all_brands = false
            @agent_ticket_form.save!

            put :update, params: { id: @agent_ticket_form.id, ticket_form: {
              in_all_brands: true,
              restricted_brand_ids: []
            } }

            body = JSON.parse(@response.body)
            assert_equal body["ticket_form"]["restricted_brand_ids"], []
          end
        end

        describe "with valid parameters" do
          before do
            put :update, params: { id: @agent_ticket_form.id, ticket_form: { name: "Wombat issue" } }
          end

          should_use_presenter Api::V2::TicketFormPresenter

          it "changes the record" do
            body = JSON.parse(@response.body)
            assert_equal "Wombat issue", body["ticket_form"]["name"]
          end
        end

        describe "with no parameters" do
          before { put :update, params: { id: @agent_ticket_form.id } }
          should_use_presenter Api::V2::TicketFormPresenter
        end

        describe "when adding multiple ticket_fields" do
          it "touches the database once and not once per ticket_field" do
            checkbox_field = FactoryBot.create(:checkbox_ticket_field, account: @account)
            child_field = FactoryBot.create(:field_tagger, account: @account)

            assert_sql_queries(1, /UPDATE `ticket_forms`/) do
              put :update, params: { id: @agent_ticket_form.id, ticket_form: {
                  ticket_field_ids: [
                    checkbox_field.id, child_field.id
                  ]
                } }
            end
          end
        end

        describe "with existing conditions that need conversion" do
          before do
            @ticket_field_condition = FactoryBot.create(:ticket_field_condition, :checkbox_parent, account: @account)
            @ticket_form = @ticket_field_condition.ticket_form
          end

          it "doesn't recreate the conditions" do
            agent_conditions = [
              {
                parent_field_id: @ticket_field_condition.parent_field_id,
                value: false,
                child_fields: [
                  {
                    id: @ticket_field_condition.child_field_id,
                    is_required: @ticket_field_condition.is_required
                  }
                ]
              }
            ]

            put :update, params: { id: @ticket_form.id, ticket_form: {
              ticket_field_ids: [@ticket_field_condition.parent_field_id, @ticket_field_condition.child_field_id],
              agent_conditions: agent_conditions
            } }

            assert_response :success
            assert_equal @ticket_field_condition.id, @ticket_form.reload.ticket_field_conditions.first.id
          end
        end

        describe "with conditions that contain invalid values" do
          let(:ticket_form) { @agent_ticket_form }

          it "returns an error" do
            priority_field = @account.field_priority
            ticket_type_field = @account.field_ticket_type
            checkbox_field = FactoryBot.create(:checkbox_ticket_field, account: @account)
            child_field = FactoryBot.create(:field_tagger, account: @account)

            agent_conditions = [
              {
                parent_field_id: priority_field.id,
                value: "invalid_priority",
                child_fields: [
                  { id: child_field.id, is_required: false }
                ]
              },
              {
                parent_field_id: ticket_type_field.id,
                value: "incident",
                child_fields: [
                  { id: child_field.id, is_required: false }
                ]
              },
              {
                parent_field_id: checkbox_field.id,
                value: true,
                child_fields: [
                  { id: child_field.id, is_required: false }
                ]
              }
            ]

            put :update, params: { id: ticket_form.id, ticket_form: {
              ticket_field_ids: [
                priority_field.id, ticket_type_field.id, checkbox_field.id, child_field.id
              ],
              agent_conditions: agent_conditions
            } }, as: :json

            assert_response :unprocessable_entity
            assert_equal "Value: 'invalid_priority' is not a value of the parent field.", JSON.parse(response.body)['details']['ticket_field_conditions.value'].first['description']
            # Removes generic error message added by validates_associated
            assert_nil JSON.parse(response.body)['details']['ticket_field_conditions']
          end
        end

        describe "with nested conditions params" do
          before do
            @ticket_field_condition = FactoryBot.create(:ticket_field_condition, account: @account)
            @ticket_form = @ticket_field_condition.ticket_form

            @tagger_field_1 = FactoryBot.create(:field_tagger, account: @account)
            @tagger_field_2 = FactoryBot.create(:field_tagger, account: @account)
          end

          it "doesn't create conditions when CTF is off" do
            Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(false)

            agent_conditions = [
              {
                parent_field_id: @ticket_field_condition.parent_field_id,
                value: @ticket_field_condition.value,
                child_fields: [
                  {
                    id: @ticket_field_condition.child_field_id,
                    is_required: @ticket_field_condition.is_required
                  }
                ]
              },
              {
                parent_field_id: @tagger_field_1.id,
                value: @tagger_field_1.custom_field_options.first.value,
                child_fields: [
                  { id: @tagger_field_2.id, is_required: false }
                ]
              }
            ]

            assert_no_difference 'TicketFieldCondition.count' do
              put :update, params: { id: @ticket_form.id, ticket_form: {
                ticket_field_ids: [@ticket_field_condition.parent_field_id, @ticket_field_condition.child_field_id, @tagger_field_1.id, @tagger_field_2.id],
                agent_conditions: agent_conditions
              } }
            end

            assert_response :success

            @ticket_form.reload

            assert_nil JSON.parse(@response.body)["ticket_form"]["agent_conditions"]
          end

          it "can add new conditions" do
            agent_conditions = [
              {
                parent_field_id: @ticket_field_condition.parent_field_id,
                value: @ticket_field_condition.value,
                child_fields: [
                  {
                    id: @ticket_field_condition.child_field_id,
                    is_required: @ticket_field_condition.is_required,
                    required_on_statuses: { type: TicketFieldCondition::REQUIRED_ON_NO_STATUSES }
                  }
                ]
              },
              {
                parent_field_id: @tagger_field_1.id,
                value: @tagger_field_1.custom_field_options.first.value,
                child_fields: [
                  {
                    id: @tagger_field_2.id,
                    is_required: false,
                    required_on_statuses: { type: TicketFieldCondition::REQUIRED_ON_NO_STATUSES }
                  }
                ]
              }
            ]

            put :update, params: { id: @ticket_form.id, ticket_form: {
              ticket_field_ids: [@ticket_field_condition.parent_field_id, @ticket_field_condition.child_field_id, @tagger_field_1.id, @tagger_field_2.id],
              agent_conditions: agent_conditions
            } }, as: :json

            assert_response :success

            @ticket_form.reload

            assert_equal JSON.parse(agent_conditions.to_json),
              JSON.parse(@response.body)["ticket_form"]["agent_conditions"]

            assert_equal 2, @ticket_form.ticket_field_conditions.count
            assert_equal @ticket_field_condition, @ticket_form.ticket_field_conditions.first
          end

          it "can remove existing conditions" do
            put :update, params: { id: @ticket_form.id, ticket_form: {
              agent_conditions: []
            } }

            assert_response :success

            @ticket_form.reload

            assert_equal [], JSON.parse(@response.body)["ticket_form"]["agent_conditions"]
            assert_equal 0, @ticket_form.ticket_field_conditions.count
          end

          it "removes existing conditions if the parameter is nil" do
            put :update, params: { id: @ticket_form.id, ticket_form: {
              agent_conditions: nil
            } }

            assert_response :success

            @ticket_form.reload

            assert_equal [], JSON.parse(@response.body)["ticket_form"]["agent_conditions"]
            assert_equal 0, @ticket_form.ticket_field_conditions.count
          end

          it "cannot change is_required on conditions" do
            agent_conditions = [
              {
                parent_field_id: @ticket_field_condition.parent_field_id,
                value: @ticket_field_condition.value,
                child_fields: [
                  {
                    id: @ticket_field_condition.child_field_id,
                    is_required: true
                  }
                ]
              }
            ]

            put :update, params: { id: @ticket_form.id, ticket_form: {
              agent_conditions: agent_conditions
            } }

            assert_response :success
            refute @ticket_field_condition.reload.is_required
          end

          it "can change required_on_statuses on conditions" do
            agent_conditions = [
              {
                parent_field_id: @ticket_field_condition.parent_field_id,
                value: @ticket_field_condition.value,
                child_fields: [
                  {
                    id: @ticket_field_condition.child_field_id,
                    required_on_statuses: {
                      type: TicketFieldCondition::REQUIRED_ON_ALL_STATUSES
                    }
                  }
                ]
              }
            ]

            put :update, params: { id: @ticket_form.id, ticket_form: {
              agent_conditions: agent_conditions
            } }

            assert_response :success
            assert @ticket_field_condition.reload.is_required
            assert_equal TicketFieldCondition::REQUIRED_ON_ALL_STATUSES, @ticket_field_condition.reload.required_on_statuses
          end

          describe_with_arturo_enabled :special_chars_in_custom_field_options do
            it "can change value to special conditions" do
              tagger_field_3 = FactoryBot.create(:field_tagger, account: @account)
              tagger_field_3.custom_field_options.create!(name: "GDPR Option", value: "questionable_$!?", account: @ticket_form.account)
              @ticket_form.ticket_form_fields.create!(account: @ticket_form.account, ticket_field: tagger_field_3)

              agent_conditions = [
                {
                  parent_field_id: tagger_field_3.id,
                  value: "questionable_$!?",
                  child_fields: [
                    {
                      id: @ticket_field_condition.child_field_id,
                      is_required: false,
                      required_on_statuses: { type: TicketFieldCondition::REQUIRED_ON_NO_STATUSES }
                    }
                  ]
                }
              ]

              put :update, params: { id: @ticket_form.id, ticket_form: {
                agent_conditions: agent_conditions
              } }

              assert_response :success
              assert_equal JSON.parse(agent_conditions.to_json),
                JSON.parse(@response.body)["ticket_form"]["agent_conditions"]
            end
          end

          describe "maximum conditions for user type on form product limit" do
            before do
              @conditions = [
                {
                  parent_field_id: @ticket_field_condition.parent_field_id,
                  value: @ticket_field_condition.value,
                  child_fields: [
                    {
                      id: @ticket_field_condition.child_field_id,
                      is_required: @ticket_field_condition.is_required
                    }
                  ]
                }
              ]
            end

            it "does not allow conditions to be added over the maximum_user_conditions_per_form setting" do
              # Set a fake maximum number of allowed conditions to test the limit
              @account.update_attributes!(settings: { maximum_user_conditions_per_form: 1 })
              extra_condition = [{
                  parent_field_id: @tagger_field_1.id,
                  value: @tagger_field_1.custom_field_options.first.value,
                  child_fields: [
                    { id: @tagger_field_2.id, is_required: false }
                  ]
                }]

              put :update, params: { id: @ticket_form.id, ticket_form: {
                ticket_field_ids: [@ticket_field_condition.parent_field_id, @ticket_field_condition.child_field_id, @tagger_field_1.id, @tagger_field_2.id],
                agent_conditions: @conditions + extra_condition,
                end_user_conditions: @conditions
              } }, as: :json

              assert_response :unprocessable_entity
              assert_equal "Cannot exceed 1 conditions per user type on an individual form.", JSON.parse(response.body)['details']['base'].first['description']
            end
          end
        end

        describe "without nested conditions params" do
          it "does not remove existing conditions" do
            @ticket_form = FactoryBot.create(:ticket_field_condition, account: @account).ticket_form
            @ticket_form.reload
            agent_conditions = @ticket_form.agent_conditions

            put :update, params: { id: @ticket_form.id, ticket_form: {
              name: "Only change name",
            } }

            assert_response :success
            assert_equal JSON.parse(agent_conditions.to_json),
              JSON.parse(@response.body)["ticket_form"]["agent_conditions"]

            assert_equal @ticket_form.agent_conditions, agent_conditions
          end
        end
      end

      describe "a DELETE to :destroy" do
        describe "of an inactive ticket_form" do
          before do
            @agent_ticket_form.active = false
            @agent_ticket_form.save!
            delete :destroy, params: { id: @agent_ticket_form.id }
          end

          it 'responds with no_content' do
            assert_response :no_content
          end

          should_change("the number of ticket forms", by: -1) { TicketForm.count(:all) }
        end

        describe "of an active ticket_form" do
          before { delete :destroy, params: { id: @agent_ticket_form.id } }
          it('responds with no_content') { assert_response :no_content }
          should_change("the number of ticket forms", by: -1) { TicketForm.count(:all) }
        end

        describe "of the default ticket_form" do
          before do
            @public_ticket_form.update_attribute(:default, true)
            delete :destroy, params: { id: @public_ticket_form.id }
          end

          it('responds with error message') do
            assert_equal "Cannot delete the default ticket form", JSON.parse(response.body)['details']['base'].first['description']
            assert_response 422
          end
        end
      end

      describe "a PUT to :reorder" do
        describe "that includes all ticket form ids that exist on the account" do
          before do
            put :reorder, params: { ticket_form_ids: [@agent_ticket_form.id, @public_ticket_form.id, @second_public_ticket_form.id] }
          end

          it 'responds with ok' do
            assert_response :ok
          end

          it "presents the ticket forms in the correct order" do
            res = JSON.parse(@response.body)['ticket_forms']
            assert_equal [@agent_ticket_form.id, @public_ticket_form.id, @second_public_ticket_form.id].sort,
              res.map { |ticket_form| ticket_form['id'] }.sort
          end

          describe "when DB ids are not in order" do
            before do
              unordered_forms = @account.ticket_forms.sort_by(&:id).reverse
              unordered_forms.stubs(:reload).returns(unordered_forms)
              Account.any_instance.stubs(:ticket_forms).returns(unordered_forms)
              put :reorder, params: { ticket_form_ids: [@agent_ticket_form.id, @public_ticket_form.id, @second_public_ticket_form.id] }
            end

            it 'responds with ok' do
              assert_response :ok
            end
          end
        end

        describe_with_arturo_disabled :send_ticket_forms_in_reorder_request do
          it "doesn't present the ticket forms" do
            put :reorder, params: { ticket_form_ids: [@agent_ticket_form.id, @public_ticket_form.id, @second_public_ticket_form.id] }

            assert @response.body.blank?
          end
        end

        describe "that doesn't include all ticket form ids that exist on the account" do
          before do
            TicketForm.expects(:reorder_forms).never
            put :reorder, params: { ticket_form_ids: [@agent_ticket_form.id] }
          end

          it 'responds with bad_request' do
            assert_response :bad_request
          end
        end
      end

      describe "a POST to :clone without 'Copy of' prefix" do
        before do
          post :clone, params: { id: @agent_ticket_form.id }
        end

        it 'has equal names' do
          res = JSON.parse(@response.body)['ticket_form']
          assert_equal @agent_ticket_form.name, res['raw_name']
        end
        should_use_presenter Api::V2::TicketFormPresenter, status: 201
      end

      describe "a POST to :clone with 'Copy of' prefix" do
        before do
          post :clone, params: { id: @agent_ticket_form.id, prepend_clone_title: true }
        end

        it 'has a new name with "Copy Of" prepended' do
          res = JSON.parse(@response.body)['ticket_form']
          assert_equal "Copy of #{@agent_ticket_form.name}", res['raw_name']
        end
        should_use_presenter Api::V2::TicketFormPresenter, status: 201
      end

      describe "a GET to :show_many" do
        let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }

        before do
          get :show_many, params: { ids: ids.join(",") }
        end

        should_use_presenter Api::V2::TicketFormPresenter, status: 200

        it "presents all ticket forms for the account, both end user visible and non end user visible" do
          res = JSON.parse(@response.body)['ticket_forms']
          assert_equal 3, res.count
          assert_equal [@public_ticket_form.id, @second_public_ticket_form.id, @agent_ticket_form.id],
            res.map { |ticket_form| ticket_form['id'] }.sort
        end
      end
    end

    describe "for accounts without Ticket Forms feature" do
      before { @controller.send(:current_account).stubs(:has_ticket_forms?).returns(false) }
      should_be_forbidden :create, :index, :update, :destroy, :reorder

      describe "a GET to :show" do
        before { get :show, params: { id: @public_ticket_form.id } }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200
      end

      describe "a GET to :show_many" do
        let(:ids) { [@public_ticket_form.id, @agent_ticket_form.id, @second_public_ticket_form.id] }
        before { get :show_many, params: { ids: ids.join(",") } }
        should_use_presenter Api::V2::TicketFormPresenter, status: 200
      end
    end
  end

  as_a_system_user do
    describe "a PUT to :update" do
      before { put :update, params: { id: @agent_ticket_form.id } }
      should_use_presenter Api::V2::TicketFormPresenter
    end
  end

  describe "embeddable subsystem user" do
    as_a_subsystem_user(user: "embeddable", account: :minimum) do
      should_be_forbidden [:post, :clone, id: 1], :create, :destroy, :reorder, :show, :show_many, :update

      describe "a GET to :index" do
        before { get :index }
        it("responds with success") { assert_response :success }
      end
    end
  end

  ["gooddata", "bime"].each do |subsystem_user|
    as_a_subsystem_user(user: subsystem_user, account: :minimum) do
      describe "a GET to :index" do
        should_be_authorized do
          get :index, format: :json
        end
      end
    end
  end

  as_a_subsystem_user(user: "gooddata", account: :minimum) do
    should_be_forbidden :create, :show, :update, :destroy, [:post, :clone, {id: 1}], :reorder, :show_many

    describe "a GET to :index" do
      before { get :index }

      it 'responds with success' do
        assert_response :success
      end
    end
  end

  def setup_ticket_forms
    public_ticket_params = { ticket_form: { name: "Wombat", display_name: "Wombats are everywhere", position: 2,
                                            default: false, active: true, end_user_visible: true } }
    @public_ticket_form = Zendesk::TicketForms::Initializer.new(@account, public_ticket_params).ticket_form
    @public_ticket_form.save!

    second_public_ticket_params = { ticket_form: { name: "Kangaroo", display_name: "Wombats are in Australia", position: 3,
                                                   default: false, active: true, end_user_visible: true } }
    @second_public_ticket_form = Zendesk::TicketForms::Initializer.new(@account, second_public_ticket_params).ticket_form
    @second_public_ticket_form.save!

    agent_params = {ticket_form: { name: "Agents Only", display_name: "Agents only wombats", position: 4,
                                   default: false, active: true, end_user_visible: false } }
    @agent_ticket_form = Zendesk::TicketForms::Initializer.new(@account, agent_params).ticket_form
    @agent_ticket_form.save!

    deleted_params = {ticket_form: { name: "Agents Only", display_name: "Agents only wombats", position: 5,
                                     default: false, active: false, end_user_visible: false } }
    @deleted_ticket_form = Zendesk::TicketForms::Initializer.new(@account, deleted_params).ticket_form
    @deleted_ticket_form.save!
    @deleted_ticket_form.soft_delete!
  end
end
