require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::FormatController do
  fixtures :accounts, :users

  before do
    accept :json
  end

  with_options(controller: "api/v2/format") do |request|
    request.should_route :post, "/api/v2/format/markdown", action: "markdown"
    request.should_route :post, "/api/v2/format/dc", action: "dc"
  end

  as_an_anonymous_user do
    should_be_unauthorized :markdown
    should_be_unauthorized :dc
  end

  as_an_end_user do
    should_be_forbidden :markdown
    should_be_forbidden :dc
  end

  as_an_agent do
    describe "a POST to :markdown" do
      describe "without any markdown" do # when no text has been entered in a comment, lotus omits the 'text' attribute
        before { post :markdown }

        it('responds with ok') { assert_response :ok }

        it "returns an empty response and not an error" do
          json = JSON.parse(@response.body)
          assert_equal({ "markdown" => "", "html" => "" }, json)
        end
      end

      describe "with valid markdown" do
        describe "with standard markdown" do
          before { post :markdown, params: { text: "*hello*" } }

          it('responds with ok') { assert_response :ok }

          it "returns a hash with original text and markdown rendered html" do
            json = JSON.parse(@response.body)
            assert_equal({ "markdown" => "*hello*", "html" => "<p dir='auto'><em>hello</em></p>" }, json)
          end
        end

        describe "with some liquid in a code segment" do
          before do
            @account.settings.markdown_ticket_comments = true
            @account.save!

            post :markdown, params: { text: "`{{ticket.id}}`", ticket: {"ticket" => {"status" => "new", "requester_id" => @user.id, "comment" => {"public" => "true", "body" => "*hello* {{current_user.name}}"}, "subject" => "Ticket"}} }
          end

          it "returns the unexpanded liquid" do
            json = JSON.parse(@response.body)
            assert_equal({ "markdown" => "`{{ticket.id}}`", "html" => "<p dir='auto'><code>{{ticket.id}}</code></p>" }, json)
          end
        end
      end

      describe "when the emoji setting is enabled" do
        before do
          Account.any_instance.stubs(has_emoji_autocompletion_enabled?: true)
          post :markdown, params: { text: "*hello* :heart:" }
        end

        it "returns a hash with original text, and markdown and emoji rendered html" do
          json = JSON.parse(@response.body)
          assert_equal({ "markdown" => "*hello* :heart:", "html" => "<p dir='auto'><em>hello</em> <img src='https://static.zdassets.com/classic/images/emojis/heart.png' height='20px' width='20px' alt='heart' title='heart' /></p>" }, json)
        end
      end

      describe "when the emoji setting is not enabled" do
        before do
          Account.any_instance.stubs(has_emoji_autocompletion_enabled?: false)
          post :markdown, params: { text: "*hello* :heart:" }
        end

        it "returns a hash with original text and markdown rendered html" do
          json = JSON.parse(@response.body)
          assert_equal({ "markdown" => "*hello* :heart:", "html" => "<p dir='auto'><em>hello</em> :heart:</p>" }, json)
        end
      end

      describe "With a ticket ID" do
        before { post :markdown, params: { text: "*hello* {{current_user.name}}", ticket_id: 1 } }
        it "returns a hash with original text and markdown rendered html" do
          json = JSON.parse(@response.body)
          assert_equal({ "markdown" => "*hello* {{current_user.name}}", "html" => "<p dir='auto'><em>hello</em> Agent Minimum</p>" }, json)
        end
      end

      describe "With ticket data" do
        before do
          post :markdown, params: { text: "*hello* {{current_user.name}}", ticket: {"ticket" => {"status" => "new", "requester_id" => @user.id, "comment" => {"public" => "true", "body" => "*hello* {{current_user.name}}"}, "subject" => "Ticket"}} }
        end
        it "returns a hash with original text and markdown rendered html" do
          json = JSON.parse(@response.body)
          assert_equal({ "markdown" => "*hello* {{current_user.name}}", "html" => "<p dir='auto'><em>hello</em> Agent Minimum</p>" }, json)
        end
      end

      describe "Only replace placeholders" do
        before do
          post :markdown, params: { text: "*hello* {{current_user.name}}", ticket: {"ticket" => {"status" => "new", "requester_id" => @user.id, "comment" => {"public" => "true", "body" => "*hello* {{current_user.name}}"}, "subject" => "Ticket"}}, simple_format: "true" }
        end
        it "returns a hash with original text and markdown rendered html" do
          json = JSON.parse(@response.body)
          assert_equal({ "markdown" => "*hello* {{current_user.name}}", "html" => "<p>*hello* Agent Minimum</p>" }, json)
        end
      end

      describe "with placeholders" do
        before do
          post(:markdown, params: { text: "[test](http://test.com/{{current_user.name}})", ticket: {
              "ticket" => {
                "status"       => "new",
                "requester_id" => @user.id,
                "subject"      => "Ticket",
                "comment"      => {
                  "public" => "true",
                  "body"   => "[test](http://test.com/{{current_user.name}})"
                },
              }
            } })
        end

        it "replaces placeholders before Markdown conversion" do
          assert_equal({
            "markdown" => "[test](http://test.com/{{current_user.name}})",
            "html"     => "<p dir='auto'><a href=\"http://test.com/Agent Minimum\" rel=\"noreferrer\" target=\"_blank\">test</a></p>"
          }, JSON.parse(@response.body))
        end
      end
    end

    describe "a POST to :dc" do
      before do
        Zendesk::Liquid::DcContext.expects(:render).returns('Go here to reset your password')
        post :dc, params: { template: '{{password_help}}', locale_id: 1 }
      end
      it('responds with ok') { assert_response :ok }

      it "returns a hash with template and value" do
        json = JSON.parse(@response.body)
        assert_equal({"template" => '{{password_help}}', "value" => 'Go here to reset your password'}, json)
      end
    end
  end
end
