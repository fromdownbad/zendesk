require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::MobileDevicesController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    PushNotifications::UrbanAirshipClient.any_instance.stubs(:register) # do not send out device notifications
    accept :json
  end

  with_options(controller: 'api/v2/mobile_devices') do |request|
    request.should_route :post,   '/api/v2/mobile_devices',                  action: 'create'
    request.should_route :get,    '/api/v2/mobile_devices',                  action: 'index'
    request.should_route :get,    '/api/v2/mobile_devices/123',              action: 'show',    id: '123'
    request.should_route :delete, '/api/v2/mobile_devices/123',              action: 'destroy', id: '123'
    request.should_route :put,    '/api/v2/mobile_devices/123/clear_badge',  action: 'clear_badge', id: '123'
  end

  as_an_anonymous_user do
    should_be_unauthorized :create, :index, :show, [:put, :clear_badge, {id: 1}], :destroy
  end

  as_an_end_user do
    should_be_forbidden :create, :index, :show, [:put, :clear_badge, {id: 1}], :destroy
  end

  as_an_agent do
    before do
      @mobile_device = PushNotifications::FirstPartyDeviceIdentifier.create!(
        user: users(:minimum_agent),
        token: "HELLO",
        device_type: "iPhone",
        mobile_app: mobile_apps(:com_zendesk_agent)
      )
    end

    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::MobileDevicePresenter
    end

    describe "a GET to :show" do
      describe "by token" do
        before { get :show, params: { id: @mobile_device.token } }
        should_use_presenter Api::V2::MobileDevicePresenter
      end

      describe "by id" do
        before { get :show, params: { id: @mobile_device.id } }
        should_use_presenter Api::V2::MobileDevicePresenter
      end

      describe "by invalid id" do
        before { get :show, params: { id: "fish" } }
        it('responds with not_found') { assert_response :not_found }
      end
    end

    describe "a POST to :create" do
      let(:params) { { device_type: "Mandroid", token: "123456" } }
      describe "when given a new token" do
        before do
          PushNotifications::FirstPartyDeviceIdentifier.any_instance.expects(:register)
          post :create, params: { mobile_device: params }
        end

        should_change("the number of device identifiers", by: 1) { PushNotifications::FirstPartyDeviceIdentifier.count(:all) }

        describe "when no mobile app is specified" do
          it "defaults to using the com.zendesk.agent app" do
            assert_equal mobile_apps(:com_zendesk_agent), PushNotifications::FirstPartyDeviceIdentifier.last.mobile_app
          end
        end

        describe "when a mobile app is specified" do
          let(:params) { { device_type: "Mandroid", token: "123456", mobile_app: "com.zendesk.inbox" } }

          it "uses the specified mobile app" do
            assert_equal mobile_apps(:com_zendesk_inbox), PushNotifications::FirstPartyDeviceIdentifier.last.mobile_app
          end
        end

        describe "when no type is specified" do
          let(:params) { { device_type: "Android", token: "123456" } }
          it "creates a FirstPartyDeviceIdentifier" do
            device_identifier = PushNotifications::FirstPartyDeviceIdentifier.last
            assert_equal 'urban_airship_apid', device_identifier.token_type
          end
        end

        describe "when type is urban_airship_channel_id" do
          let(:params) { { device_type: "Android", token: "123456", token_type: "urban_airship_channel_id" } }
          it "creates a DeviceIdentifier for a UA channel" do
            device_identifier = PushNotifications::FirstPartyDeviceIdentifier.last
            assert_equal 'urban_airship_channel_id', device_identifier.token_type
          end
        end
      end

      describe "when given an existing token" do
        before do
          @registration_id = "Wibblybob!"
          device_type = 'Mandroid'
          PushNotificationRegistrationJob.expects(:enqueue).with(
            account_id: @user.account_id,
            operation: :register,
            mobile_app_identifier: 'com.zendesk.agent',
            identifier_id: @mobile_device.id,
            token: @mobile_device.token,
            device_type: device_type
          )

          post :create, params: { mobile_device: { device_type: device_type, token: @mobile_device.token, mobile_app: "com.zendesk.agent" } }
        end

        should_not_change("the number of device identifiers") { PushNotifications::FirstPartyDeviceIdentifier.count(:all) }
      end
    end

    describe "a PUT to clear_badge" do
      before do
        PushNotificationUpdateJob.expects(:enqueue).with(
          account_id: @user.account_id,
          token: @mobile_device.token,
          device_type: @mobile_device.device_type,
          mobile_app_identifier: "com.zendesk.agent",
          badge: 0
        )
        put :clear_badge, params: { id: @mobile_device.id }
      end

      it('responds with ok') { assert_response :ok }
    end

    describe "a DELETE to :destroy" do
      before { PushNotifications::FirstPartyDeviceIdentifier.any_instance.expects(:unregister) }

      describe "by id" do
        before { delete :destroy, params: { id: @mobile_device.id } }
        it('responds with no_content') { assert_response :no_content }
      end

      describe "by token" do
        before { delete :destroy, params: { id: @mobile_device.token } }
        it('responds with no_content') { assert_response :no_content }
      end
    end
  end
end
