require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::GooddataUsersController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :role_settings

  before do
    use_ssl
    accept :json
    GooddataIntegration.any_instance.stubs(:last_successful_process)
  end

  with_options(controller: 'api/v2/gooddata_users') do |request|
    request.should_route :get, '/api/v2/gooddata_user', action: :show
  end

  as_an_anonymous_user do
    should_be_unauthorized :create, :show
  end

  as_an_end_user do
    should_be_forbidden :create, :show
  end

  as_an_agent do
    before do
      @account.create_gooddata_integration(project_id: 'project_id', status: 'complete', version: 2)

      GooddataUser.destroy_all
    end

    describe "GET :show" do
      describe 'when everything is fine' do
        before do
          Api::V2::GooddataUserPresenter.any_instance.stubs(:present).
            returns(test_json: 'test_value')

          get :show
        end

        before_should 'records the failed call via statsd' do
          statsd_client = stub_for_statsd
          Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
          statsd_client.expects(:increment).with('api.v2.gooddata_users.show', tags: ["status:200"])
        end

        it "returns the presented user" do
          assert_equal(
            {'test_json' => 'test_value'},
            JSON.parse(@response.body)
          )
        end

        it('responds with ok') { assert_response :ok }
      end

      describe 'when the GoodData connection is broken' do
        before do
          Api::V2::GooddataUserPresenter.any_instance.stubs(:present).raises(StandardError.new)
        end

        it 'records the failed call via statsd' do
          statsd_client = stub_for_statsd
          Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
          assert_raises(StandardError) do
            statsd_client.expects(:increment).with('api.v2.gooddata_users.show', tags: ["status:500"])
            get :show
          end
        end

        it 'responds with not ok' do
          assert_raises(StandardError) do
            get :show
            assert_response 500
          end
        end
      end
    end

    describe "POST :create" do
      describe "GooddataUser already exists" do
        before do
          GooddataUser.create! do |u|
            u.account = @user.account
            u.user = @user
            u.gooddata_user_id = 'gooddata_user_id'
            u.gooddata_project_id = 'gooddata_project_id'
          end

          post :create
        end

        it('responds with conflict') { assert_response :conflict }
      end

      describe "when an integration does not exist" do
        before do
          @user.account.gooddata_integration.delete
          @user.account.reload
          assert_nil @user.account.gooddata_integration
        end

        it('responds with precondition_failed') do
          post :create
          assert_response :precondition_failed
        end
      end

      describe "when an integration exists" do
        before do
          Zendesk::Gooddata::UserProvisioning.any_instance.
            stubs(:create_gooddata_user)
        end

        describe "and it has a version of '1'" do
          before do
            @user.account.gooddata_integration.update_attributes(version: 1)

            post :create
          end

          it('responds with precondition_failed') { assert_response :precondition_failed }
        end

        describe "and it does not have a 'complete' status" do
          before do
            @user.account.gooddata_integration.update_attributes(
              version: 2, status: 'invited'
            )

            post :create
          end

          it('responds with precondition_failed') { assert_response :precondition_failed }
        end

        describe "and a GooddataUser does not exist for the user" do
          before do
            @user.account.gooddata_integration.update_attributes(
              version: 2, status: 'complete'
            )
            Api::V2::GooddataUserPresenter.any_instance.stubs(:present).
              returns(test_json: 'test_value')

            post :create
          end

          before_should "create a GoodData user" do
            Zendesk::Gooddata::UserProvisioning.any_instance.
              expects(:create_gooddata_user).with(@user)
          end

          it "returns the created user" do
            assert_equal(
              {'test_json' => 'test_value'},
              JSON.parse(@response.body)
            )
          end

          it('responds with ok') { assert_response :ok }
        end
      end
    end
  end
end
