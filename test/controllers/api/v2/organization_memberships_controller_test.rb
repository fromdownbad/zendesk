require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::OrganizationMembershipsController do
  include DomainEventsHelper
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :users, :organizations, :organization_memberships, :role_settings
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  before do
    accept :json
    @organization_membership = organization_memberships(:minimum)
    @new_organization        = organizations(:minimum_organization2)
    @end_user                = users(:minimum_end_user)
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
  end

  with_options(controller: "api/v2/organization_memberships") do |request|
    request.should_route :get,    "/api/v2/organization_memberships",                        action: "index"
    request.should_route :get,    "/api/v2/organization_memberships/count",                  action: "count"
    request.should_route :post,   "/api/v2/organization_memberships/create_many",            action: "create_many"
    request.should_route :delete, "/api/v2/organization_memberships/destroy_many",           action: "destroy_many"
    request.should_route :get,    "/api/v2/organization_memberships/1",                      action: "show",         id: "1"
    request.should_route :delete, "/api/v2/organization_memberships/1",                      action: "destroy",      id: "1"
    request.should_route :get,    "/api/v2/users/2/organization_memberships",                action: "index",        user_id: "2"
    request.should_route :get,    "/api/v2/users/2/organization_memberships/count",          action: "count",        user_id: "2"
    request.should_route :get,    "/api/v2/users/2/organization_memberships/1",              action: "show",         id: "1", user_id: "2"
    request.should_route :post,   "/api/v2/users/2/organization_memberships",                action: "create",       user_id: "2"
    request.should_route :delete, "/api/v2/users/2/organization_memberships/1",              action: "destroy",      id: "1", user_id: "2"
    request.should_route :put,    "/api/v2/users/2/organization_memberships/1/make_default", action: "make_default", id: "1", user_id: "2"
    request.should_route :get,    "/api/v2/organizations/1/organization_memberships",        action: "index",        organization_id: "1"
    request.should_route :get,    "/api/v2/organizations/1/organization_memberships/count",  action: "count",        organization_id: "1"
    request.should_route :post,   "/api/v2/users/2/organization_memberships",                action: "create",       user_id: "2"
  end

  describe "when using oauth tokens" do
    with_scopes('users:write', 'organizations:write', 'write') do
      should_be_authorized { post :create, params: { organization_membership: { user_id: @end_user.id, organization_id: @new_organization.id } } }
    end

    with_scopes('users:write') do
      should_be_authorized { post :create, params: { organization_membership: { user_id: @end_user.id, organization_id: @new_organization.id } } }
    end

    with_scopes('organizations:write') do
      should_be_authorized { post :create, params: { organization_membership: { user_id: @end_user.id, organization_id: @new_organization.id } } }
    end

    with_scopes('write') do
      should_be_authorized { post :create, params: { organization_membership: { user_id: @end_user.id, organization_id: @new_organization.id } } }
      should_be_authorized { put :make_default, params: { user_id: @end_user.id, id: @organization_membership.id } }
    end

    with_scopes('read') do
      should_not_be_authorized { post :create, params: { organization_membership: { user_id: @end_user.id, organization_id: @new_organization.id } } }
    end
  end

  as_an_end_user do
    should_be_forbidden :index, :show, :destroy, :destroy_many,
      [:post,   :create,       {user_id: 1}],
      [:delete, :destroy,      {id: 1, user_id: 1}],
      [:put,    :make_default, {id: 1, user_id: 1}]
  end

  as_an_agent do
    describe "a GET to :index" do
      describe "simple" do
        before { get :index }
        should_use_presenter Api::V2::OrganizationMembershipPresenter

        it "lists records for all subscribed users across all organizations" do
          assert_equal @account.organization_memberships.active.count, JSON.parse(@response.body)["organization_memberships"].size
        end
      end

      describe "Emergency rate limiting for GET organization_memberships" do
        describe "with an integer page param" do
          let(:limiter) { mock }
          let(:statsd_client) { mock }

          before { limiter.stubs(:throttle_index_with_deep_offset_pagination) }

          it "calls the pagination limiter" do
            Zendesk::OffsetPaginationLimiter.expects(:new).with(anything).returns(limiter)
            limiter.expects(:throttle_index_with_deep_offset_pagination).once

            get :index, params: { page: 1 }
          end

          describe "after 99 calls within 1 second" do
            let(:rate_limit) { 100 }

            before do
              Timecop.freeze
              Prop.throttle!(:index_organization_memberships_with_deep_offset_pagination_ceiling_limit, @account.id, increment: rate_limit - 1)
            end

            describe "for page=1 and per_page=100" do
              let(:page) { 1 }

              it "responds with success on calls 100 and 101" do
                get :index, params: { page: page }
                assert_response :success
                get :index, params: { page: page }
                assert_response :success
              end

              describe "metrics and logging" do
                it "does not log any rate limit message" do
                  Rails.logger.expects(:info).with(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/)).never
                  # Once the above expectation is set up, you must specify that other calls might occur.
                  Rails.logger.expects(:info).with(Not(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not increment the 'organization_memberships' ceiling rate limit metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "organization_memberships").at_least(0).returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_organization_memberships_index_ceiling_rate_limit_log_only", anything).never
                  statsd_client.expects(:increment).with("api_v2_organization_memberships_index_ceiling_rate_limit", anything).never
                  2.times { get :index, params: { page: page } }
                end
              end
            end

            describe "for page=10,001 and per_page=100" do
              let(:page) { 10_001 }

              it "responds with success on calls 100 and 101" do
                get :index, params: { page: page }
                assert_response :success
                get :index, params: { page: page }
                assert_response :success
              end

              describe "metrics and logging" do
                it "logs the log_only message" do
                  Rails.logger.expects(:info).with(regexp_matches(/Log only: would have rate limited #index by account/)).once
                  Rails.logger.expects(:info).with(Not(regexp_matches(/Log only: would have rate limited #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not log the message for the active arturo" do
                  Rails.logger.expects(:info).with(regexp_matches(/Rate limiting #index by account/)).never
                  Rails.logger.expects(:info).with(Not(regexp_matches(/Rate limiting #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not increment the api_v2_organization_memberships_index_ceiling_rate_limit metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "organization_memberships").once.returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_organization_memberships_index_ceiling_rate_limit", anything).never
                  statsd_client.expects(:increment).with(Not(equals("api_v2_organization_memberships_index_ceiling_rate_limit")), anything).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "increments the log_only metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "organization_memberships").once.returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_organization_memberships_index_ceiling_rate_limit_log_only", anything).once
                  2.times { get :index, params: { page: page } }
                end
              end
            end
          end
        end
      end

      describe "without includes with a user_id" do
        before { get :index, params: { user_id: @end_user.id } }
        should_use_presenter Api::V2::OrganizationMembershipPresenter
      end

      describe "without includes with a organization_id" do
        before { get :index, params: { organization_id: @new_organization.id } }
        should_use_presenter Api::V2::OrganizationMembershipPresenter
      end

      it "sorts user organizations by default and name" do
        ["ZZZ", "AAA"].each do |name|
          @end_user.organization_memberships.create!(organization: @account.organizations.create!(name: name))
        end
        get :index, params: { user_id: @end_user.id }
        assert_response :success
        oids = JSON.load(@response.body).fetch('organization_memberships').map { |om| om.fetch('organization_id') }
        names = oids.map { |oid| @account.organizations.detect { |o| o.id == oid }.name }
        names.must_equal ["minimum organization", "AAA", "ZZZ"]
      end

      describe "with includes" do
        before { get :index, params: { user_id: @end_user.id, include: "users,organizations" } }
        should_use_presenter Api::V2::OrganizationMembershipPresenter

        it "includes the users and organizations used by the organization_memberships" do
          json = JSON.parse(@response.body)

          assert_equal(
            json["organizations"].map { |r| r["id"] }.sort,
            json["organization_memberships"].map { |r| r["organization_id"] }.sort.uniq
          )
          assert_equal(
            json["users"].map { |r| r["id"] }.sort,
            json["organization_memberships"].map { |r| r["user_id"] }.sort.uniq
          )
        end
      end

      describe "with deleted users" do
        before do
          @deleted_user = users(:inactive_user)
          OrganizationMembership.create!(
            account: @new_organization.account,
            organization_id: @new_organization.id,
            user_id: @deleted_user.id
          )

          get :index, params: { organization_id: @new_organization.id }
        end

        should_use_presenter Api::V2::OrganizationMembershipPresenter

        it "only show active users's memberships" do
          json = JSON.parse(@response.body)

          assert_equal [], json["organization_memberships"]
        end
      end

      describe "with cursor pagination" do
        should_support_cursor_pagination_conversion :index, 'organization_memberships', :cursor_pagination_organization_memberships_index, :remove_offset_pagination_organization_memberships_index

        describe 'when narrowing results by user_id' do
          it 'should use cursor pagination v2' do
            get :index, params: { page: { size: 100 }, user_id: @end_user.id }
            assert_response :ok
            json_response = JSON.parse(response.body)
            assert json_response['organization_memberships'].size >= 1
            assert_not_nil json_response['links']['prev']
            assert_not_nil json_response['links']['next']
          end

          it 'generates the correct order clause' do
            assert_sql_queries(1, /ORDER BY `organization_memberships`.`id` DESC/) do
              get :index, params: { user_id: @end_user.id, page: { size: 100 }, sort: '-id' }
            end

            assert_sql_queries(1, /ORDER BY `organization_memberships`.`default` ASC/) do
              get :index, params: { user_id: @end_user.id, page: { size: 100 }, sort: 'default' }
            end
          end

          it 'shows an error message for invalid sort options' do
            get :index, params: { user_id: @end_user.id, page: { size: 100 }, sort: '-invalid' }
            assert_response :bad_request
            assert @response.body =~ /sort is not valid/
          end
        end
      end
    end

    describe "a GET to :show" do
      describe "scoped to a user" do
        before { get :show, params: { user_id: @end_user.id, id: @organization_membership.id } }
        should_use_presenter Api::V2::OrganizationMembershipPresenter
      end

      describe "for an organization_membership" do
        before { get :show, params: { id: @organization_membership.id } }
        should_use_presenter Api::V2::OrganizationMembershipPresenter
      end
    end

    describe "a POST to :create" do
      describe "with valid params" do
        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: false)
          @old_organization_id = @end_user.organization_memberships.first.organization_id
        end

        describe "presenter" do
          before do
            post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
          end

          should_use_presenter Api::V2::OrganizationMembershipPresenter, status: :created, location: /\/api\/v2\/organization_memberships\/\d+\.json$/
        end

        it "replaces rather than add a new organization membership" do
          post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
          assert_equal 1, @end_user.reload.organization_memberships.count(:all)
          assert_equal @new_organization.id, @end_user.organization_memberships.first.organization_id
          assert_equal @new_organization.id, @end_user.organization_id
        end

        it "emits user_organization_added and user_organization_removed domain events" do
          post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
          organization_added_event = decode_user_events(domain_event_publisher.events, :user_organization_added)
          organization_removed_event = decode_user_events(domain_event_publisher.events, :user_organization_removed)
          assert_equal organization_added_event.size, 1
          assert_equal organization_added_event.first.user_organization_added.organization.id.value, @end_user.organization_memberships.first.organization_id
          assert_equal organization_removed_event.size, 1
          assert_equal organization_removed_event.first.user_organization_removed.organization.id.value, @old_organization_id
        end

        it "moves user's tickets from previous organization to new organization, but not from other organizations" do
          User.any_instance.expects(:move_tickets_to_organization).with(@old_organization_id, @new_organization.id)
          User.any_instance.expects(:move_tickets_to_organization).with(:any, @new_organization.id).never
          post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
        end
      end

      describe "with valid params and no existing organization memberships" do
        before do
          @end_user.organization_memberships.destroy_all
        end

        describe "presenter" do
          before do
            post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
          end

          should_use_presenter Api::V2::OrganizationMembershipPresenter, status: :created, location: /\/api\/v2\/organization_memberships\/\d+\.json$/
        end

        it "makes the initial organization membership default and set organization_id" do
          post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
          assert_equal 1, @end_user.reload.organization_memberships.count(:all)
          assert_equal @new_organization.id, @end_user.organization_memberships.first.organization_id
          assert_equal @new_organization.id, @end_user.organization_id
        end

        it "emits an user_organization_added domain event" do
          post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
          organization_added_events = decode_user_events(domain_event_publisher.events, :user_organization_added)
          assert_equal organization_added_events.size, 1
          assert_equal organization_added_events.first.user_organization_added.organization.id.value, @new_organization.id
        end

        it "moves user's tickets from any organization to new organization" do
          User.any_instance.expects(:move_tickets_to_organization).with(@old_organization_id, @new_organization.id).never
          User.any_instance.expects(:move_tickets_to_organization).with(:any, @new_organization.id)
          post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
        end
      end

      describe "with valid params, multiple organizations enabled" do
        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
        end

        describe "presenter and change" do
          before do
            post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
          end

          should_use_presenter Api::V2::OrganizationMembershipPresenter, status: :created, location: /\/api\/v2\/organization_memberships\/\d+\.json$/
          should_change("the organization_membership count", by: 1) { OrganizationMembership.count(:all) }
        end

        it "emits an user_organization_added domain event" do
          post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
          organization_added_events = decode_user_events(domain_event_publisher.events, :user_organization_added)
          assert_equal organization_added_events.size, 1
          assert_equal organization_added_events.first.user_organization_added.organization.id.value, @new_organization.id
        end

        describe "with existing organization memberships" do
          it "does not move user's tickets from any organization to new organization" do
            User.any_instance.expects(:move_tickets_to_organization).with(@old_organization_id, @new_organization.id).never
            User.any_instance.expects(:move_tickets_to_organization).with(:any, @new_organization.id).never
            post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
          end
        end

        describe "with no existing organization memberships" do
          before do
            @end_user.organization_memberships.destroy_all
          end

          it "moves user's tickets from any organization to new organization" do
            User.any_instance.expects(:move_tickets_to_organization).with(@old_organization_id, @new_organization.id).never
            User.any_instance.expects(:move_tickets_to_organization).with(:any, @new_organization.id)
            post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: @new_organization.id} }
          end
        end
      end

      describe "with invalid params" do
        before do
          post :create, params: { user_id: @end_user.id, organization_membership: {organization_id: 999} }
        end

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

        it "does not emit user_organization_added domain events" do
          assert_equal decode_user_events(domain_event_publisher.events, :user_organization_added).size, 0
          assert_equal decode_user_events(domain_event_publisher.events, :user_organization_removed).size, 0
        end
      end

      describe "with no params" do
        before do
          post :create
        end

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

        it "does not emit user_organization_added domain events" do
          assert_equal decode_user_events(domain_event_publisher.events, :user_organization_added).size, 0
          assert_equal decode_user_events(domain_event_publisher.events, :user_organization_removed).size, 0
        end
      end
    end

    describe "a POST to :create_many" do
      describe "with ids" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          OrganizationMembershipBulkCreateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          post :create_many, params: { organization_memberships: [{user_id: 1, organization_id: 1}] }
        end

        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with invalid params" do
        before { post :create_many }

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "when users are not allowed to belong to multiple organizations" do
        before do
          account = accounts(:minimum)
          account.settings.multiple_organizations = false
          account.settings.save!
        end

        describe "with multiple organization ids per user id" do
          before do
            post :create_many, params: { organization_memberships: [{user_id: 1, organization_id: 1}, {user_id: 1, organization_id: 2}] }
          end
          it('responds with bad_request') { assert_response :bad_request }
          it "does not emit user_organization_added domain events" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_organization_added).size, 0
            assert_equal decode_user_events(domain_event_publisher.events, :user_organization_removed).size, 0
          end
        end

        describe "with at most one organization id per user id" do
          before do
            post :create_many, params: { organization_memberships: [{user_id: 1, organization_id: 1}, {user_id: 2, organization_id: 1}] }
          end
          should_use_presenter Api::V2::JobStatusPresenter
        end
      end

      describe "when users are allowed to belong to multiple organizations" do
        describe "with multiple organization ids per user id" do
          before do
            account = accounts(:minimum)
            account.settings.multiple_organizations = true
            account.settings.save!
            post :create_many, params: { organization_memberships: [{user_id: 1, organization_id: 1}, {user_id: 1, organization_id: 2}] }
          end
          should_use_presenter Api::V2::JobStatusPresenter

          it "does not emit user_organization_added or user_organization_removed domain events" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_organization_added).size, 0
            assert_equal decode_user_events(domain_event_publisher.events, :user_organization_removed).size, 0
          end
        end
      end
    end

    describe "a DELETE to :destroy" do
      describe "scoped to a user" do
        before { delete :destroy, params: { user_id: @end_user.id, id: @organization_membership.id } }
        it('responds with no_content') { assert_response :no_content }

        it "emits an user_organization_removed domain event" do
          organization_removed_events = decode_user_events(domain_event_publisher.events, :user_organization_removed)
          assert_equal organization_removed_events.size, 1
          assert_equal organization_removed_events.first.user_organization_removed.organization.id.value, @organization_membership.organization_id
        end
      end

      describe "unscoped" do
        before { delete :destroy, params: { id: @organization_membership.id } }
        it('responds with no_content') { assert_response :no_content }

        it "emits an user_organization_removed domain event" do
          organization_removed_events = decode_user_events(domain_event_publisher.events, :user_organization_removed)
          assert_equal organization_removed_events.size, 1
          assert_equal organization_removed_events.first.user_organization_removed.organization.id.value, @organization_membership.organization_id
        end
      end
    end

    describe "a DELETE to :destroy_many" do
      describe "with ids" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          OrganizationMembershipBulkDeleteJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          delete :destroy_many, params: { ids: "1,2,3" }
        end
        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with invalid params" do
        before { delete :destroy_many }
        it('responds with bad_request') { assert_response :bad_request }
      end
    end

    describe "a PUT to :make_default" do
      before do
        assert @organization_membership.default?

        @new_organization_membership = OrganizationMembership.create!(
          account: @new_organization.account,
          organization_id: @new_organization.id,
          user_id: @end_user.id
        )

        refute @new_organization_membership.default?
        put :make_default, params: { user_id: @end_user.id, id: @new_organization_membership.id }
      end

      it "sets the organization's default status" do
        assert @new_organization_membership.reload.default?
      end

      it "unsets other organization's default status" do
        refute @organization_membership.reload.default?
      end

      it "alters the users' organization_id" do
        assert_equal @new_organization_membership.reload.organization_id, @organization_membership.reload.user.organization_id
      end

      it('responds with ok') { assert_response :ok }
    end
  end
end
