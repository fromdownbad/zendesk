require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'
require_relative '../../../../support/api_scopes_helper'

SingleCov.covered!

describe Api::V2::Rules::AutomationsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  extend TestSupport::Rule::AdminRequest

  include TestSupport::Rule::Helper

  fixtures :accounts,
    :users,
    :rules,
    :tickets

  let(:automation) { rules(:automation_close_ticket) }
  let(:account)    { accounts(:minimum) }

  let(:automations) do
    JSON.parse(@response.body)['automations'].map do |automation|
      {
        title:    automation['title'],
        active:   automation['active'],
        position: automation['position']
      }
    end
  end

  let(:valid_params) do
    {
      title:   'Test Automation',
      all:     [{field: 'status', operator: 'is', value: 'open'}],
      actions: [{field: 'status', value: 'closed'}]
    }
  end

  before do
    accept :json
    use_ssl
  end

  with_options(controller: 'api/v2/rules/automations') do |request|
    request.should_route :get,    '/api/v2/automations/active',       action: 'active'
    request.should_route :get,    '/api/v2/automations/123',          action: 'show', id: 123
    request.should_route :get,    '/api/v2/automations',              action: 'index'
    request.should_route :post,   '/api/v2/automations',              action: 'create'
    request.should_route :put,    '/api/v2/automations/1',            action: 'update',  id: 1
    request.should_route :delete, '/api/v2/automations/1',            action: 'destroy', id: 1
    request.should_route :get,    '/api/v2/automations/search',       action: 'search'
    request.should_route :put,    '/api/v2/automations/update_many',  action: 'update_many'
    request.should_route :delete, '/api/v2/automations/destroy_many', action: 'destroy_many'
  end

  as_an_agent_with_permissions(business_rule_management: true) do
    let(:account) { accounts(:minimum) }

    records_admin_request(:index) { get :index }
    records_admin_request(:show)  { get :show, params: { id: create_automation.id } }

    records_admin_request(:create) do
      post :create, params: { automation: {
        title:   'Test Automation',
        all:     [{field: 'status', operator: 'is', value: 'open'}],
        actions: [{field: 'status', value: 'closed'}]
      } }
    end

    records_admin_request(:update) do
      put :update, params: { id: create_automation.id, automation: {active: false} }
    end

    records_admin_request(:destroy) { delete :destroy, params: { id: create_automation.id } }

    records_admin_request(:update_many) do
      put :update_many, params: { automations: [{id: create_automation.id, active: false}] }
    end

    records_admin_request(:destroy_many) do
      delete :destroy_many, params: { ids: create_automation.id }
    end

    records_admin_request(:active) { get :active }
    records_admin_request(:search) { get :search }
  end

  describe "when using oauth tokens" do
    with_scopes('automations:read', 'read') do
      should_be_authorized { get :index }
      should_be_authorized { get :active }
      should_be_authorized { get :show, params: { id: automation.id } }

      should_not_be_authorized { post :create, params: { automation: valid_params } }
    end

    with_scopes('automations:write', 'write') do
      should_be_authorized { post :create, params: { automation: valid_params } }
      should_be_authorized { put :update, params: { id: automation.id, automation: { title: 'Test Automation' } } }
      should_be_authorized { delete :destroy, params: { id: automation.id } }
      should_be_authorized { put :update_many, params: { automations: [{ id: automation.id, position: 4 }] } }
      should_be_authorized { delete :destroy_many, params: { ids: automation.id } }

      should_not_be_authorized { get :index }
    end

    with_scopes('read', 'write') do
      should_be_authorized { get :active }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index,
      :show,
      :active,
      :create,
      :update,
      :update_many,
      :destroy_many
  end

  as_an_end_user do
    should_be_forbidden :index,
      :show,
      :active,
      :create,
      :update,
      :update_many,
      :destroy_many
  end

  describe 'per_page' do
    let(:params)   { {per_page: per_page} }
    let(:per_page) { nil }

    before { @controller.stubs(params: params) }

    it 'defaults to 100 records' do
      assert_equal 100, @controller.send(:per_page)
    end

    describe 'when `per_page` is less than 1000' do
      describe 'and less than 100' do
        let(:per_page) { 50 }

        it 'is allowed' do
          assert_equal 50, @controller.send(:per_page)
        end
      end

      describe 'and greater than 100' do
        let(:per_page) { 700 }

        it 'is allowed' do
          assert_equal 700, @controller.send(:per_page)
        end
      end
    end

    describe 'when `per_page` is greater than 1000' do
      let(:per_page) { 2000 }

      it 'caps at a max of 1000 records' do
        assert_equal 1000, @controller.send(:per_page)
      end
    end
  end

  describe 'when the user does not have permission to edit' do
    as_an_agent_with_permissions(business_rule_management: false) do
      should_be_forbidden :create,
        :update,
        :destroy,
        :destroy_many

      describe 'a GET to `index`' do
        before do
          Automation.destroy_all

          create_automation(title: 'ZB', active: false, position: 2)
          create_automation(title: 'ZA', active: true,  position: 1)
          create_automation(title: 'AB', active: false, position: 4)
          create_automation(title: 'AA', active: true,  position: 3)

          get :index
        end

        should_use_presenter Api::V2::Rules::AutomationPresenter

        it 'returns automations based on permission' do
          assert_equal(
            [
              {title: 'ZA', active: true, position: 1},
              {title: 'AA', active: true, position: 3}
            ],
            automations
          )
        end
      end

      describe 'a GET to `show`' do
        before { get :show, params: { id: automation.id } }

        should_use_presenter Api::V2::Rules::AutomationPresenter
      end

      describe 'a GET to `active`' do
        before { get :active }

        should_use_presenter Api::V2::Rules::AutomationPresenter
      end
    end
  end

  as_an_agent_with_permissions(business_rule_management: true) do
    describe 'a GET to `index`' do
      before do
        Automation.destroy_all

        create_automation(title: 'ZB', active: false, position: 2)
        create_automation(title: 'ZA', active: true,  position: 1)
        create_automation(title: 'AB', active: false, position: 4)
        create_automation(title: 'AA', active: true,  position: 3)
      end

      describe 'without parameters' do
        before { get :index }

        should_use_presenter Api::V2::Rules::AutomationPresenter

        it 'returns all automations' do
          assert_equal(
            [
              {title: 'ZA', active: true,  position: 1},
              {title: 'ZB', active: false, position: 2},
              {title: 'AA', active: true,  position: 3},
              {title: 'AB', active: false, position: 4}
            ],
            automations
          )
        end
      end

      describe 'with `active` parameter' do
        before { get :index, params: { active: false } }

        should_use_presenter Api::V2::Rules::AutomationPresenter

        it 'returns automations based on `active`' do
          assert_equal(
            [
              {title: 'ZB', active: false, position: 2},
              {title: 'AB', active: false, position: 4}
            ],
            automations
          )
        end
      end

      describe 'with `sort_by` parameter' do
        before { get :index, params: { sort_by: 'alphabetical' } }

        should_use_presenter Api::V2::Rules::AutomationPresenter

        it 'returns automations sorted by that parameter' do
          assert_equal(
            [
              {title: 'AA', active: true,  position: 3},
              {title: 'AB', active: false, position: 4},
              {title: 'ZA', active: true,  position: 1},
              {title: 'ZB', active: false, position: 2}
            ],
            automations
          )
        end
      end

      describe 'with `sort_order` parameter' do
        before { get :index, params: { sort_order: 'desc' } }

        should_use_presenter Api::V2::Rules::AutomationPresenter

        it 'returns automations ordered by that parameter' do
          assert_equal(
            [
              {title: 'AB', active: false, position: 4},
              {title: 'AA', active: true,  position: 3},
              {title: 'ZB', active: false, position: 2},
              {title: 'ZA', active: true,  position: 1}
            ],
            automations
          )
        end
      end

      describe 'and side-loading usage counts' do
        let(:side_loads) { %w[usage_1h usage_24h usage_7d usage_30d] }

        describe 'with the `rule_usage_stats` feature' do
          before do
            Account.any_instance.stubs(has_rule_usage_stats?: true)

            get :index, params: { include: side_loads.join(',') }
          end

          it 'includes the usage side-loads' do
            assert(
              side_loads.all? do |side_load|
                json['automations'].first.key?(side_load)
              end
            )
          end
        end

        describe 'without the `rule_usage_stats` feature' do
          before do
            Account.any_instance.stubs(has_rule_usage_stats?: false)

            get :index, params: { include: side_loads.join(',') }
          end

          it 'does not include the usage side-loads' do
            assert(
              side_loads.none? do |side_load|
                json['automations'].first.key?(side_load)
              end
            )
          end
        end
      end

      describe 'and side-loading other resources' do
        let(:side_loads) { %w[app_installation permissions] }

        before do
          Rule.stubs(:apps_installations).returns(
            Automation.all.first.id => {
              'id'       => 42,
              'settings' => {'title' => 'AAA'}
            }
          )

          get :index, params: { include: side_loads.join(',') }
        end

        it 'includes the appropriate item level side-loads' do
          assert(
            %w[app_installation permissions].all? do |side_load|
              json['automations'].first.key?(side_load)
            end
          )
        end
      end
    end

    describe 'a GET to `show`' do
      before { get :show, params: { id: automation.id } }

      should_use_presenter Api::V2::Rules::AutomationPresenter
    end

    describe 'a GET to `active`' do
      before do
        Automation.destroy_all

        create_automation(title: 'ZB', active: false, position: 2)
        create_automation(title: 'ZA', active: true,  position: 1)
        create_automation(title: 'AB', active: false, position: 4)
        create_automation(title: 'AA', active: true,  position: 3)
      end

      describe 'without parameters' do
        before { get :active }

        should_use_presenter Api::V2::Rules::AutomationPresenter

        it 'returns all active automations' do
          assert_equal(
            [
              {title: 'ZA', active: true, position: 1},
              {title: 'AA', active: true, position: 3}
            ],
            automations
          )
        end
      end

      describe 'with `sort_by` parameter' do
        before { get :active, params: { sort_by: 'alphabetical' } }

        should_use_presenter Api::V2::Rules::AutomationPresenter

        it 'returns active automations sorted by that parameter' do
          assert_equal(
            [
              {title: 'AA', active: true, position: 3},
              {title: 'ZA', active: true, position: 1}
            ],
            automations
          )
        end
      end

      describe 'with `sort_order` parameter' do
        before { get :active, params: { sort_order: 'desc' } }

        should_use_presenter Api::V2::Rules::AutomationPresenter

        it 'returns active automations ordered by that parameter' do
          assert_equal(
            [
              {title: 'AA', active: true, position: 3},
              {title: 'ZA', active: true, position: 1}
            ],
            automations
          )
        end
      end
    end

    describe 'a GET to `search`' do
      let(:automations) do
        json['automations'].map { |automation| {title: automation['title']} }
      end

      before do
        Automation.destroy_all

        create_automation(title: 'ZB', active: false, position: 3)
        create_automation(title: 'ZA', active: true,  position: 5)
        create_automation(title: 'CB', active: false, position: 6)
        create_automation(title: 'CA', active: true,  position: 2)
        create_automation(title: 'CC', active: true,  position: 8)
        create_automation(title: 'AB', active: false, position: 4)
        create_automation(title: 'AA', active: true,  position: 1)
      end

      describe 'without a `query` parameter' do
        before { get :search }

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe 'with an invalid `query` parameter' do
        before { get :search, params: { query: '' } }

        it 'responds with `400 Bad Request' do
          assert_response :bad_request
        end
      end

      describe 'with a valid `query` parameter' do
        let(:results) do
          [
            {'id' => Automation.find_by_title('AA').id, 'type' => 'Automation'},
            {'id' => Automation.find_by_title('AB').id, 'type' => 'Automation'},
            {'id' => Automation.find_by_title('ZA').id, 'type' => 'Automation'},
            {'id' => Automation.find_by_title('CA').id, 'type' => 'Automation'}
          ]
        end

        before do
          ZendeskSearch::Client.
            any_instance.
            stubs(search: {'results' => results, 'count' => results.size})
        end

        describe 'and no other parameters' do
          before { get :search, params: { query: 'test' } }

          should_use_presenter Api::V2::Rules::AutomationPresenter

          it 'returns the results' do
            assert_equal(
              [{title: 'AA'}, {title: 'AB'}, {title: 'ZA'}, {title: 'CA'}],
              automations
            )
          end
        end

        describe 'and sorting parameters' do
          before { get :search, params: { query: 'A', sort_by: 'updated_at' } }

          before_should 'pass through the query and sort options' do
            ZendeskSearch::Client.
              any_instance.
              expects(:search).
              with(
                'A order_by:updated_at sort:desc',
                account_id:  account.id,
                endpoint:    'rule',
                from:        0,
                fq:          '',
                hl:          true,
                incremental: true,
                size:        100,
                type:        'automation'
              ).
              returns('results' => results, 'count' => results.size)
          end
        end
      end
    end

    describe 'a POST to `create`' do
      describe_with_arturo_enabled :apply_rules_table_limits do
        describe 'and the maximum limit for automations has not been reached' do
          before { post :create, params: { automation: valid_params } }

          it('responds with created') { assert_response :created }
        end

        describe 'and the maximum limit for automations has been reached' do
          let(:limit) { 0 }

          before do
            Rule.any_instance.stubs(usage_limit: limit)

            post :create, params: { automation: valid_params }
          end

          it('responds with forbidden') { assert_response :forbidden }

          it('includes an error message') do
            assert_equal(
              I18n.t(
                'txt.admin.models.rules.rule.limit_exceeded',
                limit: limit,
                rule_type: I18n.t(
                  'txt.admin.models.rules.rule.rule_type.automations'
                )
              ),
              JSON.parse(response.body)['errors'][0]['title']
            )
          end
        end
      end

      describe_with_arturo_disabled :apply_rules_table_limits do
        describe 'and the maximum limit for automations has been reached' do
          let(:limit) { 0 }

          before do
            Rule.any_instance.stubs(usage_limit: limit)

            post :create, params: { automation: valid_params }
          end

          it('responds with created') { assert_response :created }
        end
      end

      describe 'when parameters are valid' do
        before do
          post :create, params: { automation: valid_params }
        end

        should_use_presenter(Api::V2::Rules::AutomationPresenter,
          status:   :created,
          location: %r{api/v2/automations/\d+\.json})
      end

      describe 'when parameters are invalid' do
        before { post :create }

        should_use_presenter(Api::V2::ErrorsPresenter,
          status: :unprocessable_entity)
      end
    end

    describe 'a PUT to `update`' do
      describe 'when parameters are valid' do
        before do
          automation.update_attributes!(is_active: false, position: 9999)

          put :update, params: { id: automation.id, automation: params }
        end

        describe 'and includes `active`' do
          let(:params) { {active: true} }

          it 'sets active' do
            assert automation.reload.is_active?
          end
        end

        describe 'and includes `position`' do
          let(:params) { {position: 1} }

          it 'sets the position' do
            assert_equal 1, automation.reload.position
          end
        end

        describe 'and includes `actions` and `conditions`' do
          let(:params) do
            {
              all:     [{field: 'type', operator: 'is', value: 'problem'}],
              actions: [{field: 'type', value: 'task'}]
            }
          end
          let(:json) { JSON.parse(@response.body) }

          it 'updates the `all` conditions' do
            assert_equal(
              params[:all].map(&:stringify_keys),
              json['automation']['conditions']['all']
            )
          end

          it 'updates the `actions`' do
            assert_equal(
              params[:actions].map(&:stringify_keys),
              json['automation']['actions']
            )
          end
        end
      end

      describe 'when parameters are invalid' do
        before { put :update, params: { id: automation.id, automation: {all: []} } }

        should_use_presenter(
          Api::V2::ErrorsPresenter,
          status: :unprocessable_entity
        )
      end

      describe 'when the existing automation is invalid' do
        before { automation.update_attribute(:definition, Definition.new) }

        describe 'and the automation was active' do
          describe 'and remains active' do
            before do
              put :update, params: { id: automation.id, automation: {active: true} }
            end

            should_use_presenter(
              Api::V2::ErrorsPresenter,
              status: :unprocessable_entity
            )
          end

          describe 'and is deactivated' do
            describe 'and only `active` is specified' do
              before do
                put(
                  :update,
                  params: {
                    id:         automation.id,
                    automation: {active: false}
                  }
                )
              end

              it 'responds with `200 OK`' do
                assert_response :ok
              end

              it 'updates the automation' do
                refute automation.reload.is_active?
              end
            end

            describe 'and other parameters are specified' do
              before do
                put(
                  :update,
                  params: {
                    id:         automation.id,
                    automation: {active: false, title: 'New title'}
                  }
                )
              end

              should_use_presenter(
                Api::V2::ErrorsPresenter,
                status: :unprocessable_entity
              )
            end
          end
        end
      end
    end

    describe 'a PUT to `update_many`' do
      before do
        Automation.destroy_all

        create_automation(title: 'ZA', active: true,  position: 1)
        create_automation(title: 'AA', active: true,  position: 3)
        create_automation(title: 'ZB', active: false, position: 2)
        create_automation(title: 'AB', active: false, position: 4)

        put :update_many, params: {
          automations: [
            {
              id: Automation.where(title: 'ZB').first.id,
              active: true,
              position: 1
            },
            {
              id: Automation.where(title: 'AA').first.id,
              active: false,
              position: 4
            },
          ]
        }
      end

      should_use_presenter Api::V2::Rules::AutomationPresenter

      it 'updates the automations' do
        assert_equal(
          [
            {title: 'ZB', active: true,  position: 1},
            {title: 'AA', active: false, position: 4}
          ],
          json['automations'].map do |automation|
            {
              title:    automation['title'],
              active:   automation['active'],
              position: automation['position']
            }
          end
        )
      end
    end

    describe 'a DELETE to `destroy`' do
      let(:account) { accounts(:minimum) }

      before { delete :destroy, params: { id: automation.id } }

      it 'responds with `204 No Content`' do
        assert_response :no_content
      end

      should_change('the number of automations', by: -1) do
        account.automations.count
      end

      it 'softs delete the automation' do
        assert automation.reload.deleted?
      end
    end

    describe 'a DELETE to `destroy_many`' do
      before do
        Automation.destroy_all

        create_automation(title: 'ZA', active: true,  position: 1)
        create_automation(title: 'AA', active: true,  position: 3)
        create_automation(title: 'ZB', active: false, position: 2)
        create_automation(title: 'AB', active: false, position: 4)
      end

      before do
        delete(
          :destroy_many,
          params: {
            ids: %w[ZB AA AB].map do |title|
              Automation.where(title: title).first.id
            end.join(',')
          }
        )
      end

      it 'responds with `204 No Content`' do
        assert_response :no_content
      end

      it 'soft-deletes the automations' do
        assert_equal(
          3,
          Automation.with_deleted do
            Automation.where(title: %w[ZB AA AB]).
              where('deleted_at IS NOT NULL').
              count
          end
        )
      end
    end
  end
end
