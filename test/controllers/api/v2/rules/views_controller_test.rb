require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'
require_relative '../../../../support/api_scopes_helper'

SingleCov.covered! uncovered: 6

describe Api::V2::Rules::ViewsController do
  extend Api::V2::TestHelper
  extend TestSupport::Rule::AdminRequest
  extend ApiScopesHelper

  include TestSupport::Rule::Helper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:agent)   { users(:minimum_agent) }
  let(:another_agent) { users(:minimum_admin) }
  let(:group)         { groups(:minimum_group) }

  let(:view)        { rules(:view_for_minimum_agent) }
  let(:shared_view) { rules(:view_my_working_tickets) }

  before do
    Zendesk::Rules::View::ViewQueue.stubs(store: FakeRedis::Redis.new)
  end

  let(:views) do
    JSON.parse(@response.body)['views'].map do |view|
      {
        owner_type: (view['restriction'] || {})['type'] || 'Account',
        title:      view['title'],
        active:     view['active'],
        position:   view['position']
      }
    end
  end

  let(:valid_params) do
    {
      title:       'Test View',
      description: 'The Rain in Spain',
      conditions:  {all: [{field: 'status', operator: 'is', value: 'open'}]}
    }
  end

  let(:ticket_ids_from_occam) { [] }

  before do
    accept :json
    use_ssl

    stub_occam_find(ticket_ids_from_occam)
    stub_occam_count(0)
  end

  with_options(controller: 'api/v2/rules/views') do |request|
    [
      [:get,    '/api/v2/views',                   {action: :index}],
      [:get,    '/api/v2/views/active',            {action: :active}],
      [:get,    '/api/v2/views/compact',           {action: :compact}],
      [:get,    '/api/v2/views/search',            {action: :search}],
      [:get,    '/api/v2/views/1',                 {action: :show,    id: 1}],
      [:get,    '/api/v2/views/my',                {action: :show,    id: 'my'}],
      [:get,    '/api/v2/views/my_groups',         {action: :show,    id: 'my_groups'}],
      [:get,    '/api/v2/views/1/execute',         {action: :execute, id: 1}],
      [:get,    '/api/v2/views/my/execute',        {action: :execute, id: 'my'}],
      [:get,    '/api/v2/views/my_groups/execute', {action: :execute, id: 'my_groups'}],
      [:get,    '/api/v2/views/1/next',            {action: :next,    id: 1}],
      [:get,    '/api/v2/views/my/next',           {action: :next,    id: 'my'}],
      [:get,    '/api/v2/views/my_groups/next',    {action: :next,    id: 'my_groups'}],
      [:get,    '/api/v2/views/1/count',           {action: :count,   id: 1}],
      [:get,    '/api/v2/views/my/count',          {action: :count,   id: 'my'}],
      [:get,    '/api/v2/views/my_groups/count',   {action: :count,   id: 'my_groups'}],
      [:get,    '/api/v2/views/count_many',        {action: :count_many}],
      [:get,    '/api/v2/views/12/tickets',        {action: :tickets, id: 12}],
      [:get,    '/api/v2/views/my/tickets',        {action: :tickets, id: 'my'}],
      [:get,    '/api/v2/views/my_groups/tickets', {action: :tickets, id: 'my_groups'}],
      [:get,    '/api/v2/views/12/feed',           {action: :feed,    id: 12}],
      [:get,    '/api/v2/views/my/feed',           {action: :feed,    id: 'my'}],
      [:get,    '/api/v2/views/my_groups/feed',    {action: :feed,    id: 'my_groups'}],
      [:post,   '/api/v2/views',                   {action: :create}],
      [:put,    '/api/v2/views/1',                 {action: :update,  id: 1}],
      [:delete, '/api/v2/views/1',                 {action: :destroy, id: 1}],
      [:put,    '/api/v2/views/update_many',       {action: :update_many}],
      [:delete, '/api/v2/views/destroy_many',      {action: :destroy_many}],
      [:get,    '/api/v2/views/groups',            {action: :groups}],
      [:get,    '/api/v2/views/definitions',       {action: :definitions}],
      [:get,    '/api/v2/views/1/filter',          {action: :filter, id: 1}],
    ].each do |verb, path, options|
      request.should_route(verb, path, options)
    end
  end

  describe 'when using OAuth tokens' do
    with_scopes 'read' do
      should_be_authorized { get :definitions }
    end
  end

  as_an_agent_with_permissions(view_access: 'full') do
    records_admin_request(:index)  { get :index }
    records_admin_request(:show)   { get :show, params: { id: create_view.id } }
    records_admin_request(:create) { post :create, params: { view: valid_params } }

    records_admin_request(:update) do
      put :update, params: { id: create_view.id, view: {active: false} }
    end

    records_admin_request(:destroy) { delete :destroy, params: { id: create_view.id } }

    records_admin_request(:update_many) do
      put :update_many, params: { views: [{id: create_view.id, active: false}] }
    end

    records_admin_request(:destroy_many) do
      delete :destroy_many, params: { ids: create_view.id }
    end

    records_admin_request(:active)      { get :active }
    records_admin_request(:search)      { get :search }
    records_admin_request(:groups)      { get :groups }
    records_admin_request(:definitions) { get :definitions }
  end

  as_an_anonymous_user do
    should_be_unauthorized :index,
      :active,
      [:get, :tickets, {id: 1}],
      [:get, :tickets, {id: 'my'}],
      [:get, :tickets, {id: 'my_groups'}],
      :compact,
      :show,
      [:get, :count, {id: 1}],
      :count_many,
      [:get, :execute, {id: 1}],
      [:get, :execute, {id: 'my'}],
      [:get, :execute, {id: 'my_groups'}],
      [:get, :next,    {id: 1}],
      [:get, :next,    {id: 'my'}],
      [:get, :next,    {id: 'my_groups'}],
      [:get, :filter,  {id: 1}],
      :create,
      :update,
      :update_many,
      :destroy_many,
      [:get, :groups],
      [:get, :definitions]
  end

  as_an_end_user do
    should_be_forbidden :index,
      :active,
      [:get, :tickets, {id: 1}],
      [:get, :tickets, {id: 'my'}],
      [:get, :tickets, {id: 'my_groups'}],
      :compact,
      :show,
      [:get, :count, {id: 1}],
      :count_many,
      [:get, :execute, {id: 1}],
      [:get, :execute, {id: 'my'}],
      [:get, :execute, {id: 'my_groups'}],
      [:get, :next,    {id: 1}],
      [:get, :next,    {id: 'my'}],
      [:get, :next,    {id: 'my_groups'}],
      [:get, :filter,  {id: 1}],
      :create,
      :update,
      :update_many,
      :destroy_many,
      [:get, :groups],
      [:get, :definitions]
  end

  describe 'for agents with `view_access` permissions' do
    before do
      View.destroy_all

      Subscription.any_instance.stubs(has_group_rules?: true)

      create_view(title: 'ZB', owner: account, active: false, position: 2)
      create_view(title: 'ZA', owner: account, active: true,  position: 5)
      create_view(title: 'CB', owner: group,   active: false, position: 6)
      create_view(title: 'CA', owner: group,   active: true,  position: 1)
      create_view(title: 'AB', owner: agent,   active: false, position: 4)
      create_view(title: 'AA', owner: agent,   active: true,  position: 3)

      unnasociated_group = account.all_groups.last
      view = create_view(title: 'MV', owner: unnasociated_group, active: true, position: 7)

      GroupView.create! do |group_view|
        group_view.account_id = account.id
        group_view.group_id   = group.id
        group_view.view_id    = view.id
      end
      view.groups_views.reload
    end

    as_an_agent_with_permissions(view_access: 'full') do
      describe 'a GET to `index`' do
        before { get :index }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns views based on permission' do
          assert_equal(
            [
              {owner_type: 'Group',   title: 'CA', active: true,  position: 1},
              {owner_type: 'Account', title: 'ZB', active: false, position: 2},
              {owner_type: 'Account', title: 'ZA', active: true,  position: 5},
              {owner_type: 'Group',   title: 'CB', active: false, position: 6},
              {owner_type: 'Group',   title: 'MV', active: true,  position: 7},
              {owner_type: 'User',    title: 'AA', active: true,  position: 3},
              {owner_type: 'User',    title: 'AB', active: false, position: 4}
            ],
            views
          )
        end
      end

      describe 'a PUT to `update_many`' do
        let(:update_many_params) do
          {
            views: [
              {id: View.where(title: 'CA').first.id, position: 1, active: false},
              {id: View.where(title: 'ZA').first.id, position: 6, active: true},
              {id: View.where(title: 'AA').first.id, position: 10}
            ]
          }
        end

        before { put :update_many, params: update_many_params }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it "updates the views' positions" do
          assert_equal(
            [
              {owner_type: 'Group',   title: 'CA', active: false, position: 1},
              {owner_type: 'Account', title: 'ZA', active: true,  position: 5},
              {owner_type: 'User',    title: 'AA', active: true,  position: 2}
            ],
            views
          )
        end
      end

      describe 'a DELETE to `destroy_many`' do
        before do
          delete(
            :destroy_many,
            params: {
              ids: %w[CA ZA AA].map do |title|
                View.where(title: title).first.id
              end.join(',')
            }
          )
        end

        it 'responds with `204 No Content`' do
          assert_response :no_content
        end

        it 'soft-deletes the views' do
          assert_equal(
            3,
            View.with_deleted do
              View.where(title: %w[CA ZA AA]).
                where('deleted_at IS NOT NULL').
                count
            end
          )
        end
      end
    end

    as_an_agent_with_permissions(view_access: 'manage-group') do
      describe 'when creating a shared view' do
        describe 'and group id is nil' do
          before { post :create, params: { view: {restriction: {type: 'Group', id: nil}}.merge(valid_params) }, as: :json }

          it 'returns 422 status response' do
            assert_response :unprocessable_entity
          end
        end

        describe 'and is assigned to multiple groups' do
          let(:group_1) { account.groups.create!(name: '1', is_active: true) }
          let(:group_2) { account.groups.create!(name: '2', is_active: true) }

          before { post :create, params: { view: {restriction: {type: 'Group', ids: [group_1, group_2].map(&:id)}}.merge(valid_params) }, as: :json }

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end
      end

      describe 'a GET to `index`' do
        before { get :index }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns views based on permission' do
          assert_equal(
            [
              {owner_type: 'Group',   title: 'CA', active: true,  position: 1},
              {owner_type: 'Account', title: 'ZA', active: true,  position: 5},
              {owner_type: 'Group',   title: 'CB', active: false, position: 6},
              {owner_type: 'Group',   title: 'MV', active: true,  position: 7},
              {owner_type: 'User',    title: 'AA', active: true,  position: 3},
              {owner_type: 'User',    title: 'AB', active: false, position: 4}
            ],
            views
          )
        end
      end

      describe 'a PUT to `update_many`' do
        describe 'when the request includes views that cannot be managed' do
          let(:update_many_params) do
            {
              views: [
                {id: View.where(title: 'CA').first.id, position: 1},
                {id: View.where(title: 'ZA').first.id, position: 6},
                {id: View.where(title: 'AA').first.id, position: 10}
              ]
            }
          end

          before { put :update_many, params: update_many_params }

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end

        describe 'when the request only includes manageable views' do
          let(:update_many_params) do
            {
              views: [
                {id: View.where(title: 'AA').first.id, active: false},
                {id: View.where(title: 'AB').first.id, active: false}
              ]
            }
          end

          before { put :update_many, params: update_many_params }

          should_use_presenter Api::V2::Rules::ViewPresenter

          it "updates the views' positions" do
            assert_equal(
              [
                {owner_type: 'User', title: 'AA', active: false, position: 3},
                {owner_type: 'User', title: 'AB', active: false, position: 4}
              ],
              views
            )
          end
        end
      end

      describe 'a DELETE to `destroy_many`' do
        describe 'when the request includes views that cannot be managed' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[CA ZA AA].map do |title|
                  View.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end

        describe 'when the request only includes manageable views' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[AB AA].map do |title|
                  View.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'soft-deletes the views' do
            assert_equal(
              2,
              View.with_deleted do
                View.where(title: %w[AB AA]).
                  where('deleted_at IS NOT NULL').
                  count
              end
            )
          end
        end
      end
    end

    as_an_agent_with_permissions(view_access: 'manage-personal') do
      describe 'a GET to `index`' do
        before { get :index }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns views based on permission' do
          assert_equal(
            [
              {owner_type: 'Group',   title: 'CA', active: true,  position: 1},
              {owner_type: 'Account', title: 'ZA', active: true,  position: 5},
              {owner_type: 'Group',   title: 'MV', active: true,  position: 7},
              {owner_type: 'User',    title: 'AA', active: true,  position: 3},
              {owner_type: 'User',    title: 'AB', active: false, position: 4}
            ],
            views
          )
        end
      end

      describe 'a PUT to `update`' do
        describe 'when the view is personal' do
          before do
            put(
              :update,
              params: {
                id:   View.where(owner_type: 'User', is_active: true).first.id,
                view: {restriction: nil}
              }, as: :json
            )
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden, response.body
          end
        end

        describe 'when the view is shared' do
          before do
            put(
              :update,
              params: {
                id:   View.where(owner_type: 'Account', is_active: true).first.id,
                view: {restriction: {type: 'User', id: agent.id}}
              }
            )
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end
      end

      describe 'a PUT to `update_many`' do
        describe 'when the request includes views that cannot be managed' do
          let(:update_many_params) do
            {
              views: [
                {id: View.where(title: 'CA').first.id, position: 1},
                {id: View.where(title: 'ZA').first.id, position: 6},
                {id: View.where(title: 'AA').first.id, position: 10}
              ]
            }
          end

          before { put :update_many, params: update_many_params }

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end

        describe 'when the request only includes manageable views' do
          let(:update_many_params) do
            {
              views: [
                {id: View.where(title: 'AA').first.id, active: true},
                {id: View.where(title: 'AB').first.id, active: true}
              ]
            }
          end

          before { put :update_many, params: update_many_params }

          should_use_presenter Api::V2::Rules::ViewPresenter

          it "updates the views' positions" do
            assert_equal(
              [
                {owner_type: 'User', title: 'AA', active: true, position: 3},
                {owner_type: 'User', title: 'AB', active: true, position: 4}
              ],
              views
            )
          end
        end
      end

      describe 'a DELETE to `destroy`' do
        describe 'when the view is shared' do
          before do
            delete(
              :destroy,
              params: {
                id: View.where(owner_type: 'Account', is_active: true).first.id
              }
            )
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end
      end

      describe 'a DELETE to `destroy_many`' do
        describe 'when the request includes views that cannot be managed' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[ZA AA AB].map do |title|
                  View.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end

        describe 'when the request only includes manageable views' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[AA AB].map do |title|
                  View.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'soft-deletes the views' do
            assert_equal(
              2,
              View.with_deleted do
                View.where(title: %w[AA AB]).
                  where('deleted_at IS NOT NULL').
                  count
              end
            )
          end
        end
      end
    end

    as_an_agent_with_permissions(view_access: 'readonly') do
      describe 'a GET to `index`' do
        before { get :index }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns views based on permission' do
          assert_equal(
            [
              {owner_type: 'Group',   title: 'CA', active: true,  position: 1},
              {owner_type: 'Account', title: 'ZA', active: true,  position: 5},
              {owner_type: 'Group',   title: 'MV', active: true,  position: 7},
              {owner_type: 'User',    title: 'AA', active: true,  position: 3},
              {owner_type: 'User',    title: 'AB', active: false, position: 4}
            ],
            views
          )
        end
      end

      describe 'a PUT to `update_many`' do
        let(:update_many_params) do
          {
            views: [
              {id: View.where(title: 'CA').first.id, position: 1},
              {id: View.where(title: 'ZA').first.id, position: 6},
              {id: View.where(title: 'AA').first.id, position: 10}
            ]
          }
        end

        before { put :update_many, params: update_many_params }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe 'a DELETE to `destroy_many`' do
        before do
          delete(
            :destroy_many,
            params: {
              ids: %w[ZA AA AB].map do |title|
                View.where(title: title).first.id
              end.join(',')
            }
          )
        end

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end
    end
  end

  describe 'Views Rate Limits' do
    let(:account)       { accounts(:minimum) }
    let(:agent)         { users(:minimum_agent) }
    let(:another_agent) { users(:minimum_admin) }
    let(:view)          { rules(:view_unassigned_to_group) }
    let(:another_view)  { rules(:view_for_minimum_agent) }
    let(:statsd_client) { Zendesk::StatsD::Client.new(namespace: 'views') }
    let(:view_count) do
      stub('view_count', rule_id: view.id, value: 1, pretty_print: '1', is_updating?: true)
    end
    let(:view_counts) { [view_count] }
    let(:span) { Object.new }
    let(:request_type) { :get }

    behaves_like_rate_limited(:views_limit_view_count_request, threshold: 5) do
      let(:action) { :count }
      let(:params) { { id: view.id } }
      let(:other_params) { { id: another_view.id } }
      let(:throttle_key) { [account.id, agent.id, view.id] }
      let(:another_throttle_key) { [account.id, another_agent.id, view.id] }
      before { @controller.stubs(:view_count).returns(view_count) }
    end

    behaves_like_rate_limited(:views_limit_find_requests, threshold: 5) do
      let(:action) { :execute }
      let(:params) { { id: view.id } }
      let(:other_params) { { id: another_view.id } }
      let(:throttle_key) { [account.id, agent.id, view.id] }
      let(:another_throttle_key) { [account.id, another_agent.id, view.id] }
      before { @controller.stubs(:view_tickets).returns([]) }
    end

    behaves_like_rate_limited(:views_limit_find_requests, threshold: 5) do
      let(:action) { :filter }
      let(:params) { { id: view.id } }
      let(:other_params) { { id: another_view.id } }
      let(:throttle_key) { [account.id, agent.id, view.id] }
      let(:another_throttle_key) { [account.id, another_agent.id, view.id] }
      before { @controller.stubs(:view_tickets).returns([]) }
    end

    behaves_like_rate_limited(:views_limit_play_requests, threshold: 5) do
      let(:action) { :next }
      let(:params) { { id: view.id } }
      let(:other_params) { { id: another_view.id } }
      let(:throttle_key) { [account.id, agent.id, view.id] }
      let(:another_throttle_key) { [account.id, another_agent.id, view.id] }
      before { @controller.stubs(:response_for_next).returns([{}, :ok]) }
    end

    behaves_like_rate_limited(:views_limit_count_many, threshold: 6, variant: false) do
      let(:action) { :count_many }
      let(:params) { { ids: view.id } }
      let(:throttle_key) { [account.id, agent.id] }
      let(:another_throttle_key) { [account.id, another_agent.id] }
      before { @controller.stubs(:view_counts).returns([]) }
    end

    behaves_like_rate_limited(:views_limit_tickets_requests, threshold: 5) do
      let(:action) { :tickets }
      let(:params) { { id: view.id } }
      let(:other_params) { { id: another_view.id } }
      let(:throttle_key) { [account.id, agent.id, view.id] }
      let(:another_throttle_key) { [account.id, another_agent.id, view.id] }
      before { @controller.stubs(:view_tickets).returns([]) }
    end

    describe 'with a custom rate limit' do
      %i[handled_limit unhandled_limit broken_handler_limit].each do |limit|
        Prop.configure limit, threshold: 1, interval: 1, description: 'default_limited'
      end

      Api::V2::Rules::ViewsController.class_eval do
        allow_parameters :limited_action, :skip
        def limited_action
          Prop.throttle!(params[:limit].to_sym)
          render plain: 'ok'
        end

        def handle_rate_limited_handled_limit(_exception)
          render plain: 'handled by custom method'
        end

        def handle_rate_limited_broken_handler_limit
          render plain: 'handled by custom method'
        end
      end
      Rails.application.routes.disable_clear_and_finalize = true
      Rails.application.routes.draw do
        get '/api/v2/views/limited_action' => 'api/v2/rules/views#limited_action'
      end

      as_an_agent do
        describe 'when the handler is defined' do
          let(:handle) { :handled_limit }

          it 'renders the handler' do
            Timecop.freeze do
              Prop.throttle(handle)
              get :limited_action, params: { limit: handle }
            end
            assert_equal response.body, 'handled by custom method'
          end
        end

        describe 'when the handler is not defined' do
          let(:handle) { :unhandled_limit }

          it 'renders the default response' do
            Timecop.freeze do
              Prop.throttle(handle)
              get :limited_action, params: { limit: handle }
            end
            assert_equal response.body, 'default_limited'
          end
        end

        describe 'when a handler with the wrong signature' do
          let(:handle) { :broken_handler_limit }

          it 'renders the default response' do
            Timecop.freeze do
              Prop.throttle(handle)
              get :limited_action, params: { limit: handle }
            end
            assert_equal response.body, 'default_limited'
          end
        end
      end
    end
  end

  describe 'views kill switch' do
    let(:account) { accounts(:minimum) }
    let(:agent)   { users(:minimum_agent) }
    let(:view)    { rules(:view_unassigned_to_group) }
    let(:json)    { JSON.parse(response.body) }

    as_an_agent do
      describe_with_arturo_enabled :views_disable_all_features do
        %i[count count_many execute filter export].each do |action|
          describe "when action - #{action} is invoked" do
            before do
              get action, params: { id: view.id }
            end

            it 'responds with 503' do
              assert_response :service_unavailable
            end

            it 'responds with proper body' do
              assert_equal json, {
                'error' => 'ServiceUnavailable',
                'description' => 'View service unavailable. Please try again later.'
              }
            end
          end
        end
      end

      describe_with_arturo_disabled :views_disable_all_features do
        %w[count count_many execute filter export].each do |action|
          describe "when action - #{action} is invoked" do
            before do
              get action, params: { id: view.id }
            end

            it 'responds with 200' do
              assert_response :ok
            end
          end
        end
      end

      describe_with_and_without_arturo_enabled :views_disable_all_features do
        %i[index next show].each do |action|
          describe "when action - #{action} is invoked" do
            before do
              get action, params: { id: view.id }
            end

            it 'responds with 200' do
              assert_response :ok
            end
          end
        end
      end
    end
  end

  as_an_agent do
    let(:json) { JSON.parse(@response.body) }

    let(:definition) do
      Definition.new.tap do |definition|
        definition.conditions_all.push DefinitionItem.new('status_id', 'is', [1])
        definition.conditions_all.push DefinitionItem.new('priority_id', 'is', '1')
      end
    end

    let(:personal_view) do
      create_view(title: 'personal', owner: agent, is_active: true, position: 1)
    end

    describe 'a GET to `index`' do
      before do
        View.destroy_all

        Subscription.any_instance.stubs(has_group_rules?: true)

        create_view(title: 'ZB', owner: account, active: false, position: 2)
        create_view(title: 'ZA', owner: account, active: true,  position: 5)
        create_view(title: 'CB', owner: group,   active: false, position: 6)
        create_view(title: 'CA', owner: group,   active: true,  position: 1)
        create_view(title: 'AB', owner: agent,   active: false, position: 4)
        create_view(title: 'AA', owner: agent,   active: true,  position: 3)
      end

      describe 'without parameters' do
        before { get :index }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns views' do
          assert_equal(
            [
              {owner_type: 'Group',   title: 'CA', active: true,  position: 1},
              {owner_type: 'Account', title: 'ZA', active: true,  position: 5},
              {owner_type: 'User',    title: 'AA', active: true,  position: 3},
              {owner_type: 'User',    title: 'AB', active: false, position: 4}
            ],
            views
          )
        end
      end

      describe 'with `access` parameter' do
        before { get :index, params: { access: 'personal' } }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns views based on `access`' do
          assert_equal(
            [
              {owner_type: 'User', title: 'AA', active: true,  position: 3},
              {owner_type: 'User', title: 'AB', active: false, position: 4}
            ],
            views
          )
        end
      end

      describe 'with `active` parameter' do
        before { get :index, params: { active: 'true' } }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns views based on `active`' do
          assert_equal(
            [
              {owner_type: 'Group',   title: 'CA', active: true, position: 1},
              {owner_type: 'Account', title: 'ZA', active: true, position: 5},
              {owner_type: 'User',    title: 'AA', active: true, position: 3}
            ],
            views
          )
        end
      end

      describe 'with `group_id` parameter' do
        before { get :index, params: { group_id: group.id } }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns views based on `group_id`' do
          assert_equal(
            [{owner_type: 'Group', title: 'CA', active: true, position: 1}],
            views
          )
        end
      end

      describe 'with `sort_by` parameter' do
        before { get :index, params: { sort_by: 'alphabetical' } }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns views sorted by that parameter' do
          assert_equal(
            [
              {owner_type: 'User',    title: 'AA', active: true,  position: 3},
              {owner_type: 'User',    title: 'AB', active: false, position: 4},
              {owner_type: 'Group',   title: 'CA', active: true,  position: 1},
              {owner_type: 'Account', title: 'ZA', active: true,  position: 5}
            ],
            views
          )
        end
      end

      describe 'with `sort_order` parameter' do
        before { get :index, params: { sort_order: 'desc' } }

        it 'returns views ordered by that parameter' do
          assert_equal(
            [
              {owner_type: 'User',    title: 'AB', active: false, position: 4},
              {owner_type: 'User',    title: 'AA', active: true,  position: 3},
              {owner_type: 'Account', title: 'ZA', active: true,  position: 5},
              {owner_type: 'Group',   title: 'CA', active: true,  position: 1}
            ],
            views
          )
        end
      end

      describe 'and side-loading' do
        let(:side_loads) { %w[app_installation permissions] }

        before do
          Rule.stubs(:apps_installations).returns(
            View.all.first.id => {'id' => 42, 'settings' => {'title' => 'AAA'}}
          )

          get :index, params: { include: side_loads.join(',') }
        end

        it 'includes the appropriate item level side-loads' do
          assert(
            side_loads.all? { |side_load| json['views'].first.key?(side_load) }
          )
        end
      end
    end

    describe 'a GET to `show_many`' do
      before do
        View.destroy_all

        create_view(title: 'ZB', owner: account, active: false, position: 2)
        create_view(title: 'ZA', owner: account, active: true,  position: 5)
        create_view(title: 'CB', owner: group,   active: false, position: 6)
        create_view(title: 'CA', owner: group,   active: true,  position: 1)
        create_view(title: 'AB', owner: agent,   active: false, position: 4)
        create_view(title: 'AA', owner: agent,   active: true,  position: 3)
      end

      let(:view_ids) { View.pluck(:id).join(',') }

      it 'requires the `ids` param' do
        get :show_many

        assert_response :bad_request
      end

      it 'returns not found when the given ids do not exits' do
        get :show_many, params: { ids: '0' }

        assert_response :success
        assert_equal [], views
      end

      describe 'without group rules' do
        before do
          Subscription.any_instance.stubs(has_group_rules?: false)
        end

        it 'get active and inactive views' do
          get :show_many, params: { ids: view_ids }

          assert_response :success
          assert_equal [
            {owner_type: 'Account', title: 'ZA', active: true,  position: 5},
            {owner_type: 'User',    title: 'AA', active: true,  position: 3},
            {owner_type: 'User',    title: 'AB', active: false, position: 4}
          ], views
        end

        it 'get only active views' do
          get :show_many, params: { ids: view_ids, active: true }

          assert_response :success
          assert_equal [
            {owner_type: 'Account', title: 'ZA', active: true, position: 5},
            {owner_type: 'User',    title: 'AA', active: true, position: 3}
          ], views
        end

        it 'get only inactive views' do
          get :show_many, params: { ids: view_ids, active: false }

          assert_response :success
          assert_equal [
            {owner_type: 'User', title: 'AB', active: false, position: 4}
          ], views
        end
      end

      describe 'with group rules' do
        before do
          Subscription.any_instance.stubs(has_group_rules?: true)
        end

        it 'get active and inactive views' do
          get :show_many, params: { ids: view_ids }

          assert_response :success
          assert_equal [
            {owner_type: 'Group',   title: 'CA', active: true,  position: 1},
            {owner_type: 'Account', title: 'ZA', active: true,  position: 5},
            {owner_type: 'User',    title: 'AA', active: true,  position: 3},
            {owner_type: 'User',    title: 'AB', active: false, position: 4}
          ], views
        end

        it 'get only active views' do
          get :show_many, params: { ids: view_ids, active: true }

          assert_response :success
          assert_equal [
            {owner_type: 'Group',   title: 'CA', active: true, position: 1},
            {owner_type: 'Account', title: 'ZA', active: true, position: 5},
            {owner_type: 'User',    title: 'AA', active: true, position: 3}
          ], views
        end

        it 'get only inactive views' do
          get :show_many, params: { ids: view_ids, active: false }

          assert_response :success
          assert_equal [
            {owner_type: 'User', title: 'AB', active: false, position: 4}
          ], views
        end
      end
    end

    describe 'a GET to `tickets`' do
      let(:view)   { rules(:view_unassigned_to_group) }
      let(:params) { {} }

      before do
        get :tickets, params: {id: view.id}.merge(params)
      end

      it 'uses GenericCommentPresenter as option of TicketPresenter' do
        assert_equal(
          Api::V2::Tickets::GenericCommentPresenter,
          @controller.send(:ticket_presenter).options[:comment_presenter].class
        )
      end

      describe 'with no parameters' do
        let(:ticket_ids_from_occam) { [tickets(:minimum_2).id, tickets(:minimum_1).id] }

        should_use_presenter(Api::V2::Tickets::TicketPresenter,
          with: :view_tickets)

        describe 'when view has a default grouping option' do
          let(:view) { rules(:view_cloud) }
          let(:ticket_ids) do
            json['tickets'].map { |ticket| ticket['id'] }
          end

          it 'respects default grouping of the view' do
            refute_nil view.output.group
            refute_nil view.output.group_asc
            assert_not_equal ticket_ids.sort, ticket_ids
          end
        end
      end

      describe 'with `last_viewed` parameter' do
        let(:params) { {last_viewed: Date.yesterday} }
        let(:ticket_ids_from_occam) do
          [tickets(:minimum_1).id, tickets(:minimum_2).id]
        end

        it 'includes `audit_count_since` for each ticket' do
          assert_not_nil json['tickets'].first['audit_count_since']
          assert_not_nil json['tickets'].last['audit_count_since']
        end
      end

      describe 'with `sort_by` parameter' do
        let(:ticket_ids_from_occam) { [tickets(:minimum_1).id, tickets(:minimum_2).id] }

        describe 'when view has a default grouping option' do
          let(:view) { rules(:view_cloud) }
          let(:params) { {sort_by: 'id'} }
          let(:ticket_ids) do
            json['tickets'].map { |ticket| ticket['id'] }
          end

          it 'overrides default grouping of the view' do
            refute_nil view.output.group
            refute_nil view.output.group_asc
            assert_equal ticket_ids.sort, ticket_ids
          end
        end

        describe 'when view does NOT have a default grouping option' do
          let(:params) { {sort_by: 'id'} }
          let(:ticket_ids) do
            json['tickets'].map { |ticket| ticket['id'] }
          end

          it 'overrides default grouping of the view' do
            assert_nil view.output.group
            assert_equal ticket_ids.sort, ticket_ids
          end
        end

        # These tests are written with a view without a default grouping.
        # They are left as is for clarity.
        describe 'and it is status' do
          let(:params) { {sort_by: 'status'} }
          let(:ticket_ids) do
            json['tickets'].map { |ticket| StatusType.find(ticket['status']) }
          end

          it 'orders tickets by status' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end

        describe 'and it is ID' do
          let(:params) { {sort_by: 'id'} }
          let(:ticket_ids) { json['tickets'].map { |ticket| ticket['id'].to_i } }

          it 'orders tickets by `nice_id`' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end

        describe 'and it is subject' do
          let(:params) { {sort_by: 'subject'} }
          let(:ticket_ids) { json['tickets'].map { |ticket| ticket['subject'] } }

          it 'orders tickets by subject' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end
      end

      describe 'with `sort_order` parameter' do
        let(:ticket_ids) { json['tickets'].map { |ticket| ticket['id'].to_i } }

        describe 'and it is `DESC`' do
          let(:ticket_ids_from_occam) { [tickets(:minimum_2).id, tickets(:minimum_1).id] }
          let(:params) { {sort_by: 'id', sort_order: 'DESC'} }

          it 'returns tickets sorted by ID in descending order' do
            assert_equal ticket_ids.sort.reverse, ticket_ids
          end
        end

        describe 'and it is `ASC`' do
          let(:ticket_ids_from_occam) { [tickets(:minimum_1).id, tickets(:minimum_2).id] }
          let(:params) { {sort_by: 'id', sort_order: 'ASC'} }

          it 'returns tickets sorted by by ID in ascending order' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end

        describe 'and it is blank' do
          let(:ticket_ids_from_occam) { [tickets(:minimum_1).id, tickets(:minimum_2).id] }
          let(:params) { {sort_by: 'id', sort_order: ''} }

          it 'returns tickets sorted by by ID in ascending order' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end
      end

      describe 'with `group_by` parameter' do
        let(:ticket_ids) do
          json['tickets'].map { |ticket| StatusType.find(ticket['status']) }
        end
        let(:ticket_ids_from_occam) do
          [tickets(:minimum_2).id, tickets(:minimum_1).id]
        end

        describe 'when view has a default grouping option' do
          let(:view) { rules(:view_cloud) }
          let(:params) { {group_by: 'id'} }
          let(:ticket_ids) do
            json['tickets'].map { |ticket| ticket['id'] }
          end

          it 'does not overrides default grouping of the view' do
            refute_nil view.output.group
            refute_nil view.output.group_asc
            assert_not_equal ticket_ids, ticket_ids.sort
          end
        end

        describe 'when view does NOT have a default grouping option' do
          let(:params) { {group_by: 'id'} }
          let(:ticket_ids) do
            json['tickets'].map { |ticket| ticket['id'] }
          end

          it 'does not overrides default grouping of the view' do
            assert_nil view.output.group
            assert_not_equal ticket_ids, ticket_ids.sort
          end
        end

        # These tests are written with a view without a default grouping.
        # They are left as is for clarity.
        describe 'and it is `status`' do
          let(:params) { {group_by: 'status'} }

          it 'returns tickets grouped by status' do
            assert_equal ticket_ids.sort.reverse, ticket_ids
          end
        end
      end

      describe 'with `group_order` parameter' do
        let(:ticket_ids) do
          json['tickets'].map { |ticket| StatusType.find(ticket['status']) }
        end
        let(:ticket_ids_from_occam) do
          [tickets(:minimum_2).id, tickets(:minimum_1).id]
        end

        describe 'and it is `DESC`' do
          let(:params) { {group_by: 'status', group_order: 'DESC'} }

          it 'returns tickets grouped by status in descending order' do
            assert_equal ticket_ids.sort.reverse, ticket_ids
          end
        end

        describe 'and it is `DESC`' do
          let(:params) { {group_by: 'status', group_order: 'ASC'} }
          let(:ticket_ids_from_occam) do
            [tickets(:minimum_1).id, tickets(:minimum_2).id]
          end

          it 'returns tickets grouped by status in ascending order' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end

        describe 'and it is blank' do
          let(:params) { {group_by: 'status', group_order: ''} }

          it 'returns tickets grouped by status in descending order' do
            assert_equal ticket_ids.sort.reverse, ticket_ids
          end
        end
      end

      describe 'and occam returns a query timeout' do
        let(:occam_client) { Zendesk::Rules::OccamClient.any_instance }

        before do
          Account.any_instance.stubs(:has_occam_rule_execution?).returns(true)
          error = Occam::Client::QueryTimeoutError.new(StandardError.new)
          occam_client.stubs(:find).raises(error)
        end

        describe_with_arturo_disabled :views_api_rescue_occam_errors do
          it 'reraises the error' do
            assert_raises(Occam::Client::QueryTimeoutError) do
              get :tickets, params: { id: view.id }
            end
          end
        end

        describe_with_arturo_enabled :views_api_rescue_occam_errors do
          it 'does not raise an error and surfaces a JSON error to the client' do
            get :tickets, params: { id: view.id }

            assert_response :service_unavailable
            assert_equal 'View service unavailable. Please try again later.', json['description']
          end

          it 'sends metrics to datadog' do
            Rails.application.config.statsd.client.
              expects(:increment).with(
                'rules.view_api_occam_error',
                tags: ['error:occam/client/query_timeout_error', 'action:tickets']
              )

            get :tickets, params: { id: view.id }
          end
        end
      end
    end

    describe 'a GET to `export`' do
      let(:view) { rules(:view_assigned_working_tickets) }

      describe 'without an available attachment' do
        before do
          ViewCsvJob.stubs(enqueue: true)

          get :export, params: { id: view.id }
        end

        after { ViewCsvJob.unstub(:enqueue) }

        it 'returns an enqueued status' do
          assert_equal 'enqueued', json['export']['status']
        end
      end
    end

    describe 'a GET to `active`' do
      before do
        View.destroy_all

        Subscription.any_instance.stubs(has_group_rules?: true)

        create_view(title: 'ZB', owner: account, active: false, position: 2)
        create_view(title: 'ZA', owner: account, active: true,  position: 5)
        create_view(title: 'CB', owner: group,   active: false, position: 6)
        create_view(title: 'CA', owner: group,   active: true,  position: 1)
        create_view(title: 'AB', owner: agent,   active: false, position: 4)
        create_view(title: 'AA', owner: agent,   active: true,  position: 3)
      end

      describe 'without parameters' do
        before { get :active }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns active views' do
          assert_equal(
            [
              {owner_type: 'Group',   title: 'CA', active: true,  position: 1},
              {owner_type: 'Account', title: 'ZA', active: true,  position: 5},
              {owner_type: 'User',    title: 'AA', active: true,  position: 3}
            ],
            views
          )
        end
      end

      describe 'with `access` parameter' do
        before { get :active, params: { access: 'personal' } }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns active views based on `access`' do
          assert_equal(
            [{owner_type: 'User', title: 'AA', active: true, position: 3}],
            views
          )
        end
      end

      describe 'with `group_id` parameter' do
        before { get :active, params: { group_id: group.id } }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns all views based on `group_id`' do
          assert_equal(
            [{owner_type: 'Group', title: 'CA', active: true, position: 1}],
            views
          )
        end
      end

      describe 'with `sort_by` parameter' do
        before { get :active, params: { sort_by: 'alphabetical' } }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns active views sorted by that parameter' do
          assert_equal(
            [
              {owner_type: 'User',    title: 'AA', active: true,  position: 3},
              {owner_type: 'Group',   title: 'CA', active: true,  position: 1},
              {owner_type: 'Account', title: 'ZA', active: true,  position: 5}
            ],
            views
          )
        end
      end

      describe 'with `sort_order` parameter' do
        before { get :active, params: { sort_order: 'desc' } }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns active views ordered by that parameter' do
          assert_equal(
            [
              {owner_type: 'User',    title: 'AA', active: true,  position: 3},
              {owner_type: 'Account', title: 'ZA', active: true,  position: 5},
              {owner_type: 'Group',   title: 'CA', active: true,  position: 1}
            ],
            views
          )
        end
      end
    end

    describe 'a GET to `search`' do
      let(:views) { json['views'].map { |view| {title: view['title']} } }

      before do
        View.destroy_all

        Subscription.any_instance.stubs(has_group_rules?: true)

        create_view(title: 'ZB', owner: account, active: false, position: 2)
        create_view(title: 'ZA', owner: account, active: true,  position: 5)
        create_view(title: 'CB', owner: group,   active: false, position: 6)
        create_view(title: 'CA', owner: group,   active: true,  position: 3)
        create_view(title: 'CC', owner: group,   active: true,  position: 8)
        create_view(title: 'AB', owner: agent,   active: false, position: 4)
        create_view(title: 'AA', owner: agent,   active: true,  position: 1)
      end

      describe 'without parameters' do
        before { get :search }

        it 'responds with `400 Bad Request' do
          assert_response :bad_request
        end
      end

      describe 'with an invalid `query` parameter' do
        before { get :search, params: { query: '' } }

        it 'responds with `400 Bad Request' do
          assert_response :bad_request
        end
      end

      describe 'with a valid `query` parameter' do
        let(:results) do
          [
            {'id' => View.find_by_title('AA').id, 'type' => 'View'},
            {'id' => View.find_by_title('AB').id, 'type' => 'View'},
            {'id' => View.find_by_title('ZA').id, 'type' => 'View'},
            {'id' => View.find_by_title('CA').id, 'type' => 'View'}
          ]
        end

        before do
          ZendeskSearch::Client.
            any_instance.
            stubs(search: {'results' => results, 'count' => results.size})
        end

        describe 'and no other parameters' do
          before { get :search, params: { query: 'A' } }

          should_use_presenter Api::V2::Rules::ViewPresenter

          it 'returns the results' do
            assert_equal(
              [{title: 'AA'}, {title: 'AB'}, {title: 'ZA'}, {title: 'CA'}],
              views
            )
          end
        end

        describe 'and sorting parameters' do
          before { get :search, params: { query: 'A', sort_by: 'created_at' } }

          before_should 'pass through the query and sort options' do
            ZendeskSearch::Client.
              any_instance.
              expects(:search).
              with(
                'A order_by:created_at sort:desc',
                account_id:  account.id,
                endpoint:    'rule',
                from:        0,
                fq:          "((owner_type:Account AND is_active:true) OR " \
                             "(owner_type:Group AND group_ids:(66248) " \
                             "AND is_active:true) OR " \
                             "(owner_type:User AND owner_id:57888))",
                hl:          true,
                incremental: true,
                size:        100,
                type:        'view'
              ).
              returns('results' => results, 'count' => results.size)
          end
        end

        describe_with_arturo_disabled :multiple_group_views_reads do
          describe 'and sorting parameters' do
            before do
              get :search, params: { query: 'A', sort_by: 'created_at' }
            end

            before_should 'pass through the query and sort options' do
              ZendeskSearch::Client.
                any_instance.
                expects(:search).
                with(
                  'A order_by:created_at sort:desc',
                  account_id:  account.id,
                  endpoint:    'rule',
                  from:        0,
                  fq:          "((owner_type:Account AND is_active:true) OR " \
                               "(owner_type:Group AND owner_id:(66248) " \
                               "AND is_active:true) OR " \
                               "(owner_type:User AND owner_id:57888))",
                  hl:          true,
                  incremental: true,
                  size:        100,
                  type:        'view'
                ).
                returns('results' => results, 'count' => results.size)
            end
          end
        end
      end
    end

    describe 'a GET to `compact`' do
      let(:other_user)  { users(:minimum_admin) }
      let(:other_group) { groups(:with_groups_group1) }

      before do
        Subscription.any_instance.stubs(has_group_rules?: true)
        Account.any_instance.stubs(has_unlimited_views?: true)

        View.destroy_all

        create_view(title: 'AA', owner: account, active: true,  position: 3)
        create_view(title: 'AB', owner: account, active: false, position: 4)
        create_view(title: 'ZA', owner: account, active: true,  position: 1)
        create_view(title: 'ZB', owner: account, active: false, position: 2)

        create_view(title: 'AC', owner: group, active: true,  position: 15)
        create_view(title: 'AD', owner: group, active: false, position: 16)
        create_view(title: 'ZC', owner: group, active: true,  position: 13)
        create_view(title: 'ZD', owner: group, active: false, position: 14)

        create_view(title: 'AE', owner: other_group, active: true,  position: 11)
        create_view(title: 'AF', owner: other_group, active: false, position: 12)
        create_view(title: 'ZE', owner: other_group, active: true,  position: 9)
        create_view(title: 'ZF', owner: other_group, active: false, position: 10)

        create_view(title: 'AG', owner: agent, active: true,  position: 7)
        create_view(title: 'AH', owner: agent, active: false, position: 8)
        create_view(title: 'ZG', owner: agent, active: true,  position: 5)
        create_view(title: 'ZH', owner: agent, active: false, position: 6)

        create_view(title: 'AI', owner: other_user, active: true,  position: 19)
        create_view(title: 'AJ', owner: other_user, active: false, position: 20)
        create_view(title: 'ZI', owner: other_user, active: true,  position: 17)
        create_view(title: 'ZJ', owner: other_user, active: false, position: 18)
      end

      describe_with_arturo_disabled :user_level_shared_view_ordering do
        before { get :compact }

        should_use_presenter Api::V2::Rules::ViewPresenter

        it 'returns only viewable views' do
          assert_equal(
            [
              {title: 'ZA',  active: true, owner_type: 'Account', position: 1},
              {title: 'AA',  active: true, owner_type: 'Account', position: 3},
              {title: 'ZC',  active: true, owner_type: 'Group',   position: 13},
              {title: 'AC',  active: true, owner_type: 'Group',   position: 15},
              {title: 'ZG',  active: true, owner_type: 'User',    position: 5},
              {title: 'AG',  active: true, owner_type: 'User',    position: 7}
            ],
            views
          )
        end

        it 'does not return more than 20 views' do
          assert json['views'].size <= 20
        end
      end

      describe_with_arturo_enabled :user_level_shared_view_ordering do
        let!(:reordered_view) do
          create_view(title: 'ZZ', owner: account, active: true, position: 30)
        end

        before do
          agent.stubs(shared_views_order: [reordered_view.id])

          get :compact
        end

        it 'returns only viewable views using the users ordering' do
          assert_equal(
            [
              {title: 'ZZ',  active: true, owner_type: 'Account', position: 30},
              {title: 'ZA',  active: true, owner_type: 'Account', position: 1},
              {title: 'AA',  active: true, owner_type: 'Account', position: 3},
              {title: 'ZC',  active: true, owner_type: 'Group',   position: 13},
              {title: 'AC',  active: true, owner_type: 'Group',   position: 15},
              {title: 'ZG',  active: true, owner_type: 'User',    position: 5},
              {title: 'AG',  active: true, owner_type: 'User',    position: 7}
            ],
            views
          )
        end
      end

      describe_with_arturo_disabled :views_compact_cache_control_header do
        it "does not set cache-control" do
          get :compact
          assert_nil @response.headers['Cache-Control']
        end
      end

      describe_with_arturo_enabled :views_compact_cache_control_header do
        it "does set cache-control to max-age=5, private" do
          get :compact
          assert_equal @response.headers['Cache-Control'], 'max-age=5, private'
        end
      end
    end

    describe 'a GET to `show`' do
      let(:view) { rules(:view_for_minimum_agent) }

      describe 'when passed an existing view ID' do
        before { get :show, params: { id: view.id } }

        should_use_presenter Api::V2::Rules::ViewPresenter
      end

      describe 'when passed the special case `incoming` ID' do
        before { get :show, params: { id: View::INCOMING } }

        should_use_presenter Api::V2::Rules::ViewPresenter
      end

      describe 'when passed the special case `my` ID' do
        before { get :show, params: { id: View::MY } }

        should_use_presenter Api::V2::Rules::ViewPresenter
      end

      describe 'when passed the special case `my_groups` ID' do
        before { get :show, params: { id: View::MY_GROUPS } }

        should_use_presenter Api::V2::Rules::ViewPresenter
      end
    end

    describe 'a GET to `count`' do
      let(:view) { rules(:view_for_minimum_agent) }

      describe 'for an unblocked view' do
        describe_with_and_without_arturo_enabled :automatic_view_blocking do
          before do
            get :count, params: { id: view.id }
          end

          it 'responds with `200 OK`' do
            assert_response :ok
          end

          it 'does not include error & description in the response' do
            refute json['view_count'].key? :error
            refute json['view_count'].key? :description
          end
        end
      end

      describe 'for a blocked view' do
        before { CachedRuleTicketCount.any_instance.stubs(:value).returns(-1) }

        describe_with_and_without_arturo_enabled :automatic_view_blocking do
          before { get :count, params: { id: view.id } }

          it 'responds with `200 OK`' do
            assert_response :ok
          end
        end

        describe_with_arturo_enabled :automatic_view_blocking do
          before { get :count, params: { id: view.id } }

          it 'includes error & description in the response' do
            assert_equal 'LimitedViewExecution', json['view_count']['error']
            assert_equal I18n.t('txt.admin.views.rules.view_blocked_response'), json['view_count']['description']
          end
        end

        describe_with_arturo_disabled :automatic_view_blocking do
          before { get :count, params: { id: view.id } }

          it 'does not include error & description in the response' do
            refute json['view_count'].key? :error
            refute json['view_count'].key? :description
          end
        end
      end
    end

    describe 'a GET to `count_many`' do
      let(:occam_client) { Zendesk::Rules::OccamClient.any_instance }
      let(:occam_response) do
        [{ id: view.id, count: 5, fresh: true, updated_at: Time.now.to_s }]
      end

      describe_with_arturo_enabled :views_disable_count_many do
        let(:ids) { [view, shared_view].map(&:id) }
        let(:count_by_id) { ->(view_counts, id) { view_counts.find { |count| count['view_id'] == id } } }
        before { get :count_many, params: { ids: ids.join(',') } }
        should_use_presenter Api::V2::Rules::ViewCountPresenter

        describe 'without deleted or suspended' do
          it 'responds as if all views were blocked' do
            assert_response :ok
            assert_equal 2, json['view_counts'].count
            payload = json['view_counts']
            assert_nil count_by_id[payload, 'deleted']
            assert_nil count_by_id[payload, 'suspended']

            json['view_counts'].each do |rule_count|
              assert_nil rule_count['value']
              assert rule_count['fresh']
              assert_equal 'poll', rule_count['refresh']
              assert_equal Zendesk::Rules::BrokenOccamTicketCount::POLL_WAIT, rule_count['poll_wait']
            end
          end
        end

        describe 'with deleted and suspended' do
          let(:with_deleted_suspended_ids) { ids | %w[deleted suspended] }
          before { get :count_many, params: { ids: with_deleted_suspended_ids.join(',') } }

          it 'responds as if all views were blocked, except deleted and suspended' do
            assert_response :ok
            assert_equal 4, json['view_counts'].count
            payload = json['view_counts']
            assert count_by_id[payload, 'deleted'].fetch('value')
            assert count_by_id[payload, 'suspended'].fetch('value')
          end
        end
      end

      before do
        occam_client.stubs(:count_many).returns('counts' => occam_response)
      end

      describe 'validate count_many api limit' do
        before do
          @controller.expects(:request_origin_details).returns(request_origin_details).at_least_once
          @controller.stubs(:view_counts).returns([])
        end
        describe 'when occam_count_many_api_limit arturo is enabled' do
          before { Arturo.enable_feature!(:occam_count_many_api_limit) }

          describe 'with more than 20 ids' do
            let(:ids) { "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21" }

            describe 'when request_origin_details is lotus' do
              let(:request_origin_details) do
                stub_everything(lotus?: true)
              end

              it 'returns without any error' do
                @controller.stubs(:view_counts).returns([])
                get :count_many, params: { ids: ids }
                assert_response :ok
              end
            end

            describe 'when request_origin_details is a zendesk mobile client' do
              let(:request_origin_details) { stub_everything(zendesk_mobile_client?: true) }

              it 'returns without any error' do
                @controller.stubs(:view_counts).returns([])
                get :count_many, params: { ids: ids }
                assert_response :ok
              end
            end

            describe 'when request_origin_details is not lotus' do
              let(:request_origin_details) { stub_everything(lotus?: false) }

              it 'returns an error' do
                get :count_many, params: { ids: ids }
                assert_response :bad_request
              end
            end
          end

          describe 'with 20 ids' do
            let(:ids) { "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20" }
            describe('when request_origin_details is not lotus') do
              let(:request_origin_details) { stub_everything(lotus?: false) }

              it 'returns without any error' do
                get :count_many, params: { ids: ids }
                assert_response :ok
              end
            end
          end
        end

        describe 'when occam_count_many_api_limit arturo is disabled' do
          before { Arturo.disable_feature!(:occam_count_many_api_limit) }

          describe 'with more than 20 ids' do
            let(:ids) { "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21" }

            describe('when request_origin_details is not lotus') do
              let(:request_origin_details) { stub_everything(lotus?: false) }

              it 'returns without any error' do
                get :count_many, params: { ids: ids }
                assert_response :ok
              end
            end
          end
        end
      end

      describe 'and there are api errors' do
        let(:expected) do
          {
            'error' => 'ServiceUnavailable',
            'description' => 'View service unavailable. Please try again later.'
          }
        end

        before do
          error = Occam::Client::ClientTimeoutError.new(StandardError.new)
          occam_client.expects(:count_many).raises(error)
        end

        it 'returns service unavailable' do
          get :count_many, params: { ids: view.id }
          assert_response :service_unavailable
          assert_equal expected, json
        end

        it 'sends metrics to datadog' do
          Rails.application.config.statsd.client.
            expects(:increment).with(
              'rules.view_api_occam_error',
              tags: ['error:occam/client/client_timeout_error', 'action:count_many']
            ).once

          get :count_many, params: { ids: view.id }
        end
      end

      describe 'when not including suspended tickets' do
        before { get :count_many, params: { ids: view.id } }

        should_use_presenter Api::V2::Rules::ViewCountPresenter

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'includes a top-level `view_counts` key' do
          assert json['view_counts'].is_a?(Array)
        end

        it 'returns a single view count' do
          assert_equal 1, json['view_counts'].length
        end

        it 'does not include suspended ticket count' do
          refute json.key?('suspended_tickets_count')
        end
      end

      describe 'when including suspended tickets' do
        before do
          get :count_many, params: { ids: view.id, include: 'suspended_tickets_count' }
        end

        it 'includes a raw suspended ticket count' do
          assert json.key?('suspended_tickets_count')
        end

        it 'includes a top-level `view_counts` key' do
          assert json['view_counts'].is_a?(Array)
        end

        it 'returns a single view count' do
          assert_equal 1, json['view_counts'].length
        end

        describe 'and no ids are specified' do
          before { get :count_many, params: { include: 'suspended_tickets_count' } }

          it 'returns just a suspended count' do
            assert json.key?('suspended_tickets_count')
          end

          it 'includes a top-level `view_counts` key' do
            assert json['view_counts'].is_a?(Array)
          end

          it 'returns no view counts' do
            assert_equal 0, json['view_counts'].length
          end
        end
      end

      describe 'when passing `suspended` as an id' do
        before { get :count_many, params: { ids: [view.id, 'suspended'].join(',') } }

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'includes a top-level `view_counts` key' do
          assert json['view_counts'].is_a?(Array)
        end

        it 'returns 2 view counts' do
          assert_equal 2, json['view_counts'].length
        end

        it 'includes suspended ticket count' do
          assert json['view_counts'].find { |count|
            count['view_id'] == 'suspended'
          }
        end
      end

      describe 'when passing `deleted` as an id' do
        before do
          # Scrub one of the tickets to be sure we're not including in view count
          scrubbed_ticket = tickets(:minimum_2)
          scrubbed_ticket.will_be_saved_by(agent)
          scrubbed_ticket.soft_delete!
          Zendesk::Scrub.scrub_ticket_and_associations(scrubbed_ticket)

          get :count_many, params: { ids: [view.id, 'deleted'].join(',') }
        end

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'includes a top-level `view_counts` key' do
          assert json['view_counts'].is_a?(Array)
        end

        it 'returns 2 view counts' do
          assert_equal 2, json['view_counts'].length
        end

        it 'includes deleted ticket count' do
          assert json['view_counts'].find { |count|
            count['view_id'] == 'deleted'
          }
        end
      end

      describe 'when using the `active` param' do
        before { View.destroy_all }

        let(:definition) do
          Definition.new.tap do |definition|
            definition.conditions_all.push DefinitionItem.new('status_id', 'less_than', [3])
          end
        end
        let(:active_view) { create_view(title: 'Active', owner: agent, active: true, position: 1, definition: definition) }
        let(:inactive_view) { create_view(title: 'Inactive', owner: agent, active: false, position: 2, definition: definition) }
        let(:view_ids) { [active_view.id, inactive_view.id].join(',') }

        describe 'when `active` param is true' do
          let(:occam_response) do
            [{ id: active_view.id, count: 5, fresh: true, updated_at: Time.now.to_s }]
          end

          it 'returns only the active views count' do
            get :count_many, params: { ids: view_ids, active: true }

            view_count = json['view_counts'].first

            assert_equal 1, json['count']
            assert_equal active_view.id, view_count['view_id']
          end
        end

        describe 'when `active` param is false' do
          let(:occam_response) do
            [{ id: inactive_view.id, count: 5, fresh: true, updated_at: Time.now.to_s }]
          end

          it 'returns only the inactive views count' do
            get :count_many, params: { ids: view_ids, active: false }

            view_count = json['view_counts'].first

            assert_equal 1, json['count']
            assert_equal inactive_view.id, view_count['view_id']
          end
        end
      end

      describe 'with a blocked view being one of them' do
        let(:expected_error) { 'LimitedViewExecution' }
        let(:expected_description) do
          I18n.t('txt.admin.views.rules.view_blocked_response')
        end
        let(:occam_response) do
          [{ id: view.id, count: -1, fresh: true, updated_at: Time.now.to_s }]
        end

        before do
          Arturo.enable_feature!(:automatic_view_blocking)
          get :count_many, params: { ids: view.id, active: true }
        end

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'includes error & description in the response for the blocked views' do
          assert_equal expected_error,       json['view_counts'].first['error']
          assert_equal expected_description, json['view_counts'].first['description']
        end
      end
    end

    # keep in sync with api v1
    describe 'a GET to `next`' do
      let(:view)                       { rules(:view_for_minimum_agent) }
      let(:view_with_multiple_tickets) { rules(:view_unassigned_to_group) }
      let(:ticket_ids_from_occam) do
        [tickets(:minimum_1).id, tickets(:minimum_2).id]
      end

      before do
        Zendesk::Rules::View::ViewQueue.stubs(store: FakeRedis::Redis.new)

        Zendesk::AgentCollision.stubs(ticket_viewed_by_user_ids: [])
      end

      describe 'when executed' do
        before do
          Zendesk::Tickets::RecentTicketManager.any_instance.
            expects(:add_ticket).
            once

          get :next, params: { id: view.id }
        end

        should_use_presenter Api::V2::Tickets::TicketPresenter
      end

      describe 'when requesting a view with multiple tickets' do
        before { get :next, params: { id: view_with_multiple_tickets.id } }

        describe 'and requesting it again' do
          let!(:ticket_1) { JSON.load(@response.body)['ticket']['id'] }
          let(:ticket_2)  { JSON.load(@response.body)['ticket']['id'] }

          before do
            View.any_instance.stubs(:find_tickets_with_ids).returns([tickets(:minimum_2).id])
            get :next, params: { id: view_with_multiple_tickets.id }
          end

          it 'does not return the same ticket twice' do
            assert_not_equal ticket_2, ticket_1
          end
        end
      end

      describe 'when the view is empty' do
        let(:ticket_ids_from_occam) { [] }

        before do
          view.find_tickets(agent).each(&:delete)

          get :next, params: { id: view.id }
        end

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'returns nothing' do
          assert_equal({}, json)
        end
      end

      describe 'when view is refilling' do
        before do
          Zendesk::Rules::View::ViewQueue.any_instance.
            expects(:lock).
            raises(Zendesk::Rules::View::ViewQueue::Locked)

          get :next, params: { id: view.id }
        end

        it 'responds with `204 No Content`' do
          assert_response :no_content
        end
      end

      describe 'when the view contains invalid conditions' do
        let(:ticket_ids_from_occam) { [] }

        before do
          view.definition.conditions_all.push(DefinitionItem.new("status_id", "is", "3"))
          view.definition.conditions_all.push(DefinitionItem.new("status_id", "is", "4"))
          view.save!

          get :next, params: { id: view.id }
        end

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'returns nothing' do
          assert_equal({}, json)
        end
      end
    end

    describe 'account has hanlon_cache_info_api enabled' do
      before { Arturo.enable_feature!(:hanlon_cache_info_api) }

      describe 'a GET to `execute`' do
        let(:view) { rules(:view_unassigned_to_group) }

        before do
          get :execute, params: { id: view.id }
        end

        it 'adds hanlon cached info to api response' do
          assert_equal false, json['cached']
        end
      end
    end

    describe 'a GET to `execute` for broken views' do
      before do
        Arturo.enable_feature!(:occam_rule_execution)
        occam_client.
          stubs(:find).
          returns("ids" => occam_ticket_ids, "cached" => false, "reordered" => false)
      end
      let(:view)              { rules(:view_unassigned_to_group) }
      let(:occam_client)      { Zendesk::Rules::OccamClient.any_instance }
      let(:occam_ticket_ids)  { [tickets(:minimum_1), tickets(:minimum_2)].map(&:id) }

      describe 'total count for view is -1' do
        before do
          occam_client.
            stubs(:count).
            returns("count" => -1, "cached" => false)
        end
        describe 'when current page is not the last page' do
          let(:page_params) { { page: 1, per_page: 1 } }

          it 'counts total tickets' do
            get :execute, params: {id: view.id}.merge(page_params)
            assert_nil json['count']
          end

          it 'shows next page' do
            get :execute, params: {id: view.id}.merge(page_params)
            refute_nil json['next_page']
          end
        end

        describe 'when current page is the last page' do
          before do
            # pretend that -1 was cached when visiting the first page
            CachedRuleTicketCount.any_instance.stubs(:value).returns(-1)
          end
          describe 'and the number of tickets equals per_page' do
            it 'counts total tickets' do
              get :execute, params: {id: view.id}.merge(page: 2, per_page: 1)
              assert_nil json['count']
            end

            it 'shows next page because it is a limitation of this implementation' do
              get :execute, params: {id: view.id}.merge(page: 2, per_page: 1)
              refute_nil json['next_page']
            end
          end

          describe 'and the number of tickets is less than per_page' do
            let(:occam_ticket_ids) { [tickets(:minimum_1).id] }

            it 'shows nil for count of total tickets' do
              get :execute, params: {id: view.id}.merge(page: 2, per_page: 2)
              assert_nil json['count']
            end

            it 'does not show the next_page' do
              get :execute, params: {id: view.id}.merge(page: 2, per_page: 2)
              assert_nil json['next_page']
            end
          end
        end

        describe 'on OccamClientError' do
          before do
            occam_client.
              expects(:count).
              raises(Occam::Client::QueryTimeoutError)
          end

          describe_with_arturo_disabled :occam_slow_rule_handler do
            it 'raises occam_client error' do
              assert_raises (Occam::Client::QueryTimeoutError) do
                get :execute, params: {id: view.id}.merge(page: 1, per_page: 1)
              end
            end
          end

          describe_with_arturo_enabled :occam_slow_rule_handler do
            it 'shows nil for count of total tickets' do
              get :execute, params: {id: view.id}.merge(page: 1, per_page: 1)
              assert_nil json['count']
            end
          end
        end
      end
    end

    describe 'a GET to `execute` with pagination' do
      let(:view) { rules(:view_unassigned_to_group) }
      let(:paginator) do
        Zendesk::Rules::RuleExecuter::Collection.new(1, params[:per_page], 3)
      end

      describe 'with `paginate` false' do
        let(:params) { { paginate: false, per_page: 1 } }

        let(:ticket_ids_from_occam) { [tickets(:minimum_1).id] }

        before do
          Zendesk::Rules::RuleExecuter.any_instance.expects(:simulate_pagination).never

          get :execute, params: {id: view.id}.merge(params)
        end

        it 'does not count total tickets' do
          assert_equal json['count'], 1
        end

        it 'does not show next page' do
          assert_nil json['next_page']
        end
      end

      describe 'with `paginate` true' do
        let(:params) { { paginate: true, per_page: 1 } }
        let(:ticket_ids_from_occam) do
          [tickets(:minimum_1).id, tickets(:minimum_2).id, tickets(:minimum_3).id]
        end

        before do
          Zendesk::Rules::RuleExecuter.
            any_instance.
            expects(:simulate_pagination).
            once.
            returns(paginator)

          get :execute, params: {id: view.id}.merge(params)
        end

        it 'counts total tickets' do
          assert_equal json['count'], 3
        end

        it 'shows next page' do
          refute_nil json['next_page']
        end
      end

      describe 'without `paginate` param' do
        let(:params) { { per_page: 1 } }
        let(:ticket_ids_from_occam) do
          [tickets(:minimum_1).id, tickets(:minimum_2).id, tickets(:minimum_3).id]
        end

        before do
          Zendesk::Rules::RuleExecuter.
            any_instance.
            expects(:simulate_pagination).
            once.
            returns(paginator)

          get :execute, params: {id: view.id}.merge(params)
        end

        it 'counts total tickets' do
          assert_equal json['count'], 3
        end

        it 'shows next page' do
          refute_nil json['next_page']
        end
      end
    end

    describe 'a GET to `execute`' do
      let(:view) { rules(:view_unassigned_to_group) }
      let(:params) { {} }

      let(:ticket_ids_from_occam) { [] }

      before do
        get :execute, params: {id: view.id}.merge(params)
      end

      should_use_presenter Api::V2::Rules::ViewRowsPresenter

      describe 'with `sort_by` parameter' do
        let(:ticket_ids_from_occam) do
          [tickets(:minimum_1).id, tickets(:minimum_2).id]
        end

        describe 'and a view that is a list' do
          let(:params)    { {sort_by: 'status'} }
          let(:list_view) do
            view.tap do |view|
              view.output.type = 'list'
              view.save!
            end
          end
          let(:ticket_ids) do
            json['rows'].map { |row| StatusType.find(row['status']) }
          end

          it 'orders tickets by status' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end

        describe 'and it is `status`' do
          let(:params) { {sort_by: 'status'} }
          let(:ticket_ids) do
            json['rows'].map { |row| StatusType.find(row['status']) }
          end

          it 'orders tickets by status' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end

        describe 'and it is ID' do
          let(:params) { {sort_by: 'id'} }
          let(:ticket_ids) { json['rows'].map { |row| row['ticket']['id'].to_i } }

          it 'orders tickets by `nice_id`' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end

        describe 'and it is subject' do
          let(:params) { {sort_by: 'subject'} }
          let(:ticket_ids) { json['rows'].map { |row| row['subject'] } }

          it 'orders tickets by subject' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end
      end

      describe 'with `sort_order` parameter' do
        let(:ticket_ids) { json['rows'].map { |row| row['ticket']['id'].to_i } }
        let(:ticket_ids_from_occam) do
          [tickets(:minimum_1).id, tickets(:minimum_2).id]
        end

        describe 'and it is `DESC`' do
          let(:params) { {sort_by: 'id', sort_order: 'DESC'} }
          let(:ticket_ids_from_occam) do
            [tickets(:minimum_2).id, tickets(:minimum_1).id]
          end

          it 'orders tickets by ID in descending order' do
            assert_equal ticket_ids.sort.reverse, ticket_ids
          end
        end

        describe 'and it is `ASC`' do
          let(:params) { {sort_by: 'id', sort_order: 'ASC'} }

          it 'orders tickets by ID in ascending order' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end

        describe 'and it is blank' do
          let(:params) { {sort_by: 'id', sort_order: ''} }

          it 'orders tickets by ID in ascending order' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end
      end

      describe 'with `group_by` parameter' do
        describe 'and it is status' do
          let(:params) { {group_by: 'status'} }
          let(:ticket_ids) do
            json['rows'].map { |row| StatusType.find(row['status']) }
          end
          let(:ticket_ids_from_occam) do
            [tickets(:minimum_2).id, tickets(:minimum_1).id]
          end

          it 'returns tickets ordered by status' do
            assert_equal ticket_ids.sort.reverse, ticket_ids
          end
        end
      end

      describe 'with `group_order` parameter' do
        let(:ticket_ids) do
          json['rows'].map { |row| StatusType.find(row['status']) }
        end
        let(:ticket_ids_from_occam) do
          [tickets(:minimum_2).id, tickets(:minimum_1).id]
        end

        describe 'and it is `DESC`' do
          let(:params) { {group_by: 'status', group_order: 'DESC'} }

          it 'orders tickets by status in descending order' do
            assert_equal ticket_ids.sort.reverse, ticket_ids
          end
        end

        describe 'and it is `ASC`' do
          let(:params) { {group_by: 'status', group_order: 'ASC'} }
          let(:ticket_ids_from_occam) do
            [tickets(:minimum_1).id, tickets(:minimum_2).id]
          end

          it 'orders tickets by status in ascending order' do
            assert_equal ticket_ids.sort, ticket_ids
          end
        end

        describe 'and it is blank' do
          let(:params) { {group_by: 'status', group_order: ''} }

          it 'orders tickets by status in descending order' do
            assert_equal ticket_ids.sort.reverse, ticket_ids
          end
        end
      end
    end

    describe 'a GET to `execute` with Occam enabled' do
      let(:view) { rules(:view_unassigned_to_group) }
      let(:occam_client) { Zendesk::Rules::OccamClient.any_instance }

      before { Arturo.enable_feature!(:occam_rule_execution) }

      describe 'and api returns a query timeout' do
        before do
          error = Occam::Client::QueryTimeoutError.new(StandardError.new)
          occam_client.stubs(:find).raises(error)
        end

        describe_with_arturo_disabled :views_api_rescue_occam_errors do
          it 'reraises the error' do
            assert_raises(Occam::Client::QueryTimeoutError) do
              get :execute, params: { id: view.id }
            end
          end
        end

        describe_with_arturo_enabled :views_api_rescue_occam_errors do
          it 'does not raise an error and surfaces a JSON error to the client' do
            get :execute, params: { id: view.id }

            assert_response :service_unavailable
            assert_equal 'View service unavailable. Please try again later.', json['description']
          end

          describe 'a GET to `execute` with open circuit breaker' do
            before do
              Zendesk::Rules::RuleExecuter.any_instance.
                stubs(:find_tickets_with_occam_client).
                raises(JohnnyFive::CircuitTrippedError)
            end
            let(:view) { rules(:view_unassigned_to_group) }

            it 'responds with unavailable service specifically for the view' do
              get :execute, params: { id: view.id }

              assert_response :service_unavailable
              assert_equal(
                "View could not be processed, try again later.",
                json['description']
              )
            end
          end
        end
      end

      describe 'and api returns unprocessable entity error' do
        before do
          occam_client.expects(:find).returns('error' => 'ValidationError', 'message' => '間違い')
          get :execute, params: { id: view.id }
        end

        it 'returns 422 status response' do
          assert_response :unprocessable_entity
        end
      end

      describe 'and api returns unknown error' do
        before do
          occam_client.expects(:find).returns('error' => 'なに？', 'message' => '間違い')
          get :execute, params: { id: view.id }
        end

        it 'returns 500 status response' do
          assert_response :service_unavailable
        end
      end
    end

    describe 'a POST to `create`' do
      describe 'when creating a shared view' do
        before { post :create, params: { view: valid_params } }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe 'when creating a personal view' do
        describe 'and the params are valid' do
          before do
            post(
              :create,
              params: {
                view: {
                  restriction: {type: 'User', id: agent.id}
                }.merge(valid_params)
              }
            )

            assert_response :created, response.body
          end

          should_use_presenter(Api::V2::Rules::ViewPresenter,
            status:   :created,
            location: %r{api/v2/views/\d+\.json})

          it 'sets the `title`' do
            assert_equal 'Test View', json['view']['title']
          end

          it 'sets the `description`' do
            assert_equal(
              'The Rain in Spain',
              json['view']['description']
            )
          end

          it 'sets the `conditions`' do
            assert_equal(
              {
                'all' => [
                  {'field' => 'status', 'operator' => 'is', 'value' => 'open'}
                ],
                'any' => []
              },
              json['view']['conditions']
            )
          end
        end

        describe 'and the params are not valid' do
          before do
            post(
              :create,
              params: {
                view: { restriction: {type: 'User', id: agent.id} }
              }
            )
          end

          should_use_presenter Api::V2::ErrorsPresenter,
            status: :unprocessable_entity
        end
      end
    end

    describe 'a PUT to `update`' do
      describe 'when the view is personal' do
        describe 'and the params are valid' do
          before { put :update, params: { id: personal_view.id, view: params } }

          describe 'and it includes active' do
            let(:params) { {active: true} }

            it 'responds with `200 OK`' do
              assert_response :ok
            end

            it 'sets active' do
              assert personal_view.reload.is_active?
            end
          end

          describe 'and it includes conditions' do
            let(:params) do
              {
                all: [{field: 'type', operator: 'is', value: 'problem'}]
              }
            end

            it 'responds with `200 OK`' do
              assert_response :ok
            end

            it 'updates the conditions' do
              assert_equal(
                params[:all].map(&:stringify_keys),
                json['view']['conditions']['all']
              )
            end
          end

          describe 'and it includes columns' do
            describe 'and the columns are strings' do
              let(:params) { {output: {columns: %w[status updated]}} }

              it 'responds with `200 OK`' do
                assert_response :ok
              end

              it 'returns the added columns' do
                assert_equal(
                  [
                    {'id' => 'status',  'title' => 'Status'},
                    {'id' => 'updated', 'title' => 'Updated'}
                  ],
                  json['view']['execution']['columns']
                )
              end
            end

            describe 'and the columns are IDs' do
              let(:ticket_field) { ticket_fields(:field_decimal_custom) }
              let(:params)       { {output: {columns: [ticket_field.id]}} }

              it 'responds with `200 OK`' do
                assert_response :ok
              end

              it 'returns the added column' do
                assert_equal(
                  {
                    'id'    => ticket_field.id,
                    'title' => ticket_field.title,
                    'type'  => ticket_field.presentation_type.downcase,
                    'url'   => "https://minimum.zendesk-test.com/api/v2/ticket_fields/#{ticket_field.id}.json"
                  },
                  json['view']['execution']['columns'][0]
                )
              end
            end
          end
        end

        describe 'and the parameters are invalid' do
          before { put :update, params: { id: personal_view.id, view: {all: []} } }

          should_use_presenter(
            Api::V2::ErrorsPresenter,
            status: :unprocessable_entity
          )
        end

        describe 'and the existing view is invalid' do
          before do
            personal_view.update_attribute(:definition, Definition.new)
          end

          describe 'and the view was active' do
            describe 'and remains active' do
              before { put :update, params: { id: personal_view.id, view: {active: true} } }

              should_use_presenter(
                Api::V2::ErrorsPresenter,
                status: :unprocessable_entity
              )
            end

            describe 'and is deactivated' do
              describe 'and only `active` is specified' do
                before do
                  put(
                    :update,
                    params: {
                      id:   personal_view.id,
                      view: {active: false}
                    }
                  )
                end

                it 'responds with `200 OK`' do
                  assert_response :ok
                end

                it 'updates the view' do
                  refute personal_view.reload.is_active?
                end
              end

              describe 'and other parameters are specified' do
                before do
                  put(
                    :update,
                    params: {
                      id:   personal_view.id,
                      view: {active: false, output: {}}
                    }
                  )
                end

                should_use_presenter(
                  Api::V2::ErrorsPresenter,
                  status: :unprocessable_entity
                )
              end
            end
          end
        end
      end

      describe 'when the view is shared' do
        describe 'and the params are valid' do
          let(:params) do
            {all: [{field: 'type', operator: 'is', value: 'problem'}]}
          end

          before { put :update, params: { id: shared_view.id, view: params } }

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end
      end
    end

    describe 'a DELETE to `destroy`' do
      describe 'when the view is personal' do
        before { delete :destroy, params: { id: personal_view.id } }

        it 'responds with `204 No Content`' do
          assert_response :no_content
        end

        it 'softs delete the view' do
          assert personal_view.reload.deleted?
        end
      end

      describe 'when the view is shared' do
        before { delete :destroy, params: { id: shared_view.id } }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end

        should_not_change('the number of views') { account.views.count(:all) }
      end
    end

    describe 'a GET to `definitions`' do
      before { get :definitions }

      it 'responds with `200 OK`' do
        assert_response :ok
      end

      it 'does not perform N+1 queries' do
        assert_no_n_plus_one do
          get :definitions
        end
      end

      should_use_presenter Api::V2::Rules::ViewDefinitionsPresenter
    end

    describe '#filter' do
      let(:view)   { rules(:view_unassigned_to_group) }
      let(:params) { {} }

      describe 'presenter' do
        before do
          @controller.stubs(filtered_tickets: [])

          get :filter, params: {id: view.id}.merge(params)
        end

        should_use_presenter Api::V2::Rules::FilteredViewRowsPresenter
      end

      describe 'when deco responds with a `bad_request` error' do
        let(:body) { JSON.load(@response.body) }

        before do
          @controller.
            stubs(:filtered_tickets).
            raises(Kragle::BadRequest)

          get :filter, params: {id: view.id}.merge(params)
        end

        it 'responds with `bad_request`' do
          assert_response :bad_request
        end
      end

      describe 'when deco responds with a `Kragle::ResponseError`' do
        let(:body) { JSON.load(@response.body) }

        before do
          @controller.
            stubs(:filtered_tickets).
            raises(Kragle::ResponseError)

          get :filter, params: {id: view.id}.merge(params)
        end

        it 'responds with `internal_server_error`' do
          assert_response :internal_server_error
        end
      end

      describe 'when the controller fails to process the request' do
        let(:body) { JSON.load(@response.body) }

        before do
          @controller.
            stubs(:filtered_tickets).
            raises(Zendesk::Rules::RuleExecuter::UnprocessableEntity)

          get :filter, params: {id: view.id}.merge(params)
        end

        it 'responds with `unprocessable_entity`' do
          assert_response :unprocessable_entity
        end

        it 'returns an error' do
          assert_equal 'UnprocessableEntity', body['error']
        end
      end

      describe 'When `Occam` service is unavailable' do
        let(:body) { JSON.load(@response.body) }

        before do
          @controller.
            stubs(:filtered_tickets).
            raises(Zendesk::Rules::RuleExecuter::UnknownOccamError)

          get :filter, params: {id: view.id}.merge(params)
        end

        it 'responds with `service_unavailable`' do
          assert_response :service_unavailable
        end

        it 'returns an error' do
          assert_equal 'ServiceUnavailable', body['error']
        end
      end

      describe_with_arturo_enabled :skill_based_view_filters do
        describe 'when the view is a filtered view' do
          before do
            account.settings.skill_based_filtered_views = [view.id]

            account.settings.save!
          end

          it 'invokes `match_tickets`' do
            @controller.expects(:match_tickets).returns([])

            get :filter, params: {id: view.id}.merge(params)
          end
        end

        describe 'when the view is not a filtered view' do
          it 'invokes `view_tickets`' do
            @controller.expects(:view_tickets).returns([])

            get :filter, params: {id: view.id}.merge(params)
          end
        end
      end

      describe_with_arturo_disabled :skill_based_view_filters do
        it 'invokes `view_tickets`' do
          @controller.expects(:view_tickets).returns([])

          get :filter, params: {id: view.id}.merge(params)
        end
      end
    end
  end

  as_an_admin do
    let(:agent)   { @user }
    let(:account) { agent.account }

    let(:definition) do
      Definition.new.tap do |definition|
        definition.conditions_all.push DefinitionItem.new('status_id', 'is', [1])
        definition.conditions_all.push DefinitionItem.new('priority_id', 'is', '1')
      end
    end

    let(:personal_view) do
      create_view(
        definition: definition,
        is_active:  true,
        owner:      agent,
        title:      'personal'
      )
    end

    describe_with_arturo_enabled :apply_rules_table_limits do
      describe 'and the maximum limit for views has not been reached' do
        before { post :create, params: { view: valid_params } }

        it('responds with created') { assert_response :created }
      end

      describe 'and the maximum limit for views has been reached' do
        let(:limit) { 0 }

        before do
          Rule.any_instance.stubs(usage_limit: limit)

          post :create, params: { view: valid_params }
        end

        it('responds with forbidden') { assert_response :forbidden }

        it('includes an error message') do
          assert_equal(
            I18n.t(
              'txt.admin.models.rules.rule.limit_exceeded',
              limit: limit,
              rule_type: I18n.t(
                'txt.admin.models.rules.rule.rule_type.views'
              )
            ),
            JSON.parse(response.body)['errors'][0]['title']
          )
        end
      end
    end

    describe_with_arturo_disabled :apply_rules_table_limits do
      describe 'and the maximum limit for views has been reached' do
        let(:limit) { 0 }

        before do
          Rule.any_instance.stubs(usage_limit: limit)

          post :create, params: { view: valid_params }
        end

        it('responds with created') { assert_response :created }
      end
    end

    describe 'a POST to `create`' do
      before do
        GroupView.destroy_all

        post :create, params: { view: params }

        assert_response expected_status, response.body
      end

      let(:expected_status) { :created }

      describe 'when creating a shared view' do
        describe 'and the params are valid' do
          let(:params) { valid_params }

          should_use_presenter(Api::V2::Rules::ViewPresenter,
            status:   :created,
            location: %r{api/v2/views/\d+\.json})

          it 'does not associate a group with the view' do
            refute GroupView.any?
          end

          it 'sets the `title`' do
            assert_equal 'Test View', json['view']['title']
          end

          it 'sets the `description`' do
            assert_equal(
              'The Rain in Spain',
              json['view']['description']
            )
          end

          it 'sets the `conditions`' do
            assert_equal(
              {
                'all' => [
                  {'field' => 'status', 'operator' => 'is', 'value' => 'open'}
                ],
                'any' => []
              },
              json['view']['conditions']
            )
          end
        end

        describe 'and the params are not valid' do
          let(:params) { {} }
          let(:expected_status) { :unprocessable_entity }

          should_use_presenter Api::V2::ErrorsPresenter,
            status: :unprocessable_entity
        end
      end

      describe 'when creating a group view using `id`' do
        let(:params) do
          {restriction: {type: 'Group', id: group.id}}.merge(valid_params)
        end

        it 'sets the owner' do
          assert(
            View.where(
              account_id: account.id,
              owner_id:   group.id,
              owner_type: 'Group',
              title:      'Test View'
            ).one?
          )
        end

        it 'associates the group with the view' do
          assert(
            GroupView.where(
              account_id: account.id,
              group_id:   group.id,
              view_id:    JSON.parse(@response.body)['view']['id']
            ).one?
          )
        end

        it 'presents the associated groups' do
          assert_equal(
            [group.id],
            JSON.parse(@response.body)['view']['restriction']['ids']
          )
        end
      end

      describe 'when creating a group view using `ids`' do
        let(:group_1) { account.groups.create!(name: '1', is_active: true) }
        let(:group_2) { account.groups.create!(name: '2', is_active: true) }
        let(:params) do
          {
            restriction: {
              type: 'Group',
              ids:  [group_1, group_2].map(&:id)
            }
          }.merge(valid_params)
        end

        should_use_presenter(Api::V2::Rules::ViewPresenter,
          status:   :created,
          location: %r{api/v2/views/\d+\.json})

        it 'associates the groups with the view' do
          assert_equal(
            [group_1, group_2].map(&:id),
            GroupView.where(
              account_id: account.id,
              view_id:    JSON.parse(@response.body)['view']['id']
            ).pluck(:group_id)
          )
        end

        it 'presents the first associated group' do
          assert_equal(
            group_1.id,
            JSON.parse(@response.body)['view']['restriction']['id']
          )
        end

        it 'presents the associated groups' do
          assert_equal(
            [group_1, group_2].map(&:id),
            JSON.parse(@response.body)['view']['restriction']['ids']
          )
        end
      end

      describe 'when executing a shared view' do
        describe 'and the view has sorting' do
          let(:params) do
            {
              output: {sort_by: 'submitter', sort_order: 'desc'}
            }.merge(valid_params)
          end

          it 'sets the `sort_by` field' do
            assert_equal 'submitter', json['view']['execution']['sort_by']
          end

          it 'sets the `sort_order` field' do
            assert_equal 'desc', json['view']['execution']['sort_order']
          end
        end

        describe 'and the view has grouping' do
          let(:params) do
            {
              output: {group_by: 'assignee', group_order: 'desc'}
            }.merge(valid_params)
          end

          it 'sets the `group_by` field' do
            assert_equal 'assignee', json['view']['execution']['group_by']
          end

          it 'sets the `group_order` field' do
            assert_equal 'desc', json['view']['execution']['group_order']
          end
        end
      end
    end

    describe 'a PUT to `update`' do
      describe 'when the view is personal' do
        describe 'and the params are valid' do
          before { put :update, params: { id: personal_view.id, view: params } }

          describe 'and it includes active' do
            let(:params) { {active: true} }

            it 'sets active' do
              assert personal_view.reload.is_active?
            end
          end

          describe 'and it includes conditions' do
            let(:params) do
              {all: [{field: 'type', operator: 'is', value: 'problem'}]}
            end

            it 'responds with `200 OK`' do
              assert_response :ok
            end

            it 'updates the conditions' do
              assert_equal(
                params[:all].map(&:stringify_keys),
                json['view']['conditions']['all']
              )
            end
          end

          describe 'and it includes `description`' do
            let(:params) { {description: 'Lays mainly in the plain'} }

            it 'sets the `description`' do
              assert_equal(
                'Lays mainly in the plain',
                personal_view.reload.description
              )
            end
          end
        end

        describe 'and the parameters are invalid' do
          before { put :update, params: { id: personal_view.id, view: {all: []} } }

          should_use_presenter Api::V2::ErrorsPresenter,
            status: :unprocessable_entity
        end
      end

      describe 'when the view is shared' do
        describe 'and the params are valid' do
          before { put :update, params: { id: personal_view.id, view: params } }

          describe 'and it includes active' do
            let(:params) { {active: true} }

            it 'sets active' do
              assert personal_view.reload.is_active?
            end
          end

          describe 'and it includes conditions' do
            let(:params) do
              {all: [{field: 'type', operator: 'is', value: 'problem'}]}
            end

            it 'responds with `200 OK`' do
              assert_response :ok
            end

            it 'updates the conditions' do
              assert_equal(
                params[:all].map(&:stringify_keys),
                json['view']['conditions']['all']
              )
            end
          end

          describe 'and it includes `description`' do
            let(:params) { {description: 'Lays mainly in the plain'} }

            it 'sets the `description`' do
              assert_equal(
                'Lays mainly in the plain',
                personal_view.reload.description
              )
            end
          end

          describe 'and it includes sorting parameters' do
            let(:params) { {output: {sort_by: 'status', sort_order: 'desc'}} }

            it 'responds with `200 OK`' do
              assert_response :ok
            end

            it 'sets the `sort_by` field' do
              assert_equal 'status', json['view']['execution']['sort_by']
            end

            it 'sets the `sort_by` field' do
              assert_equal 'desc', json['view']['execution']['sort_order']
            end
          end

          describe 'and it includes grouping parameters' do
            let(:params) { {output: {group_by: 'status', group_order: 'desc'}} }

            it 'responds with `200 OK`' do
              assert_response :ok
            end

            it 'sets the `group_by` field' do
              assert_equal 'status', json['view']['execution']['group_by']
            end

            it 'sets the `group_by` field' do
              assert_equal 'desc', json['view']['execution']['group_order']
            end
          end
        end

        describe 'and the parameters are invalid' do
          before { put :update, params: { id: personal_view.id, view: {all: []} } }

          should_use_presenter(Api::V2::ErrorsPresenter,
            status: :unprocessable_entity)
        end
      end

      describe 'when updating ownership to a group using `id`' do
        before do
          put(
            :update,
            params: {
              id:   personal_view.id,
              view: {
                restriction: {type: 'Group', id: group.id}
              }
            }
          )
        end

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'sets the owner' do
          assert_equal group, personal_view.reload.owner
        end

        it 'associates the group with the view' do
          assert(
            GroupView.where(
              account_id: account.id,
              group_id:   group.id,
              view_id:    personal_view.id
            ).one?
          )
        end
      end

      describe 'when updating ownership to a group using `ids`' do
        let(:group_1) { account.groups.create!(name: '1', is_active: true) }
        let(:group_2) { account.groups.create!(name: '2', is_active: true) }

        before do
          put(
            :update,
            params: {
              id:   personal_view.id,
              view: {
                restriction: {
                  type: 'Group',
                  ids:  [group_1, group_2].map(&:id)
                }
              }
            }
          )
        end

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'associates the groups with the view' do
          assert_equal(
            [group_1, group_2].map(&:id),
            GroupView.
              where(account_id: account.id, view_id: personal_view.id).
              pluck(:group_id)
          )
        end
      end

      describe 'when updating a group view' do
        describe 'and ownership changes' do
          before do
            put :update, params: { id: shared_view.id, view: {restriction: restriction} }
          end

          describe 'to the account' do
            let(:restriction) { nil }

            it 'removes the existing group associations' do
              refute GroupView.where(view_id: shared_view.id).any?
            end
          end

          describe 'to a user' do
            let(:restriction) { {type: 'User', id: agent.id} }

            it 'removes the existing group associations' do
              refute GroupView.where(view_id: shared_view.id).any?
            end
          end

          describe 'to a different group' do
            let(:group_2)     { account.groups.create!(name: '2', is_active: true) }
            let(:group_3)     { account.groups.create!(name: '3', is_active: true) }
            let(:restriction) { {type: 'Group', ids: [group_2.id, group_3.id]} }

            it 'associates the groups with the view' do
              assert_equal(
                [group_2, group_3].map(&:id),
                GroupView.
                  where(account_id: account.id, view_id: shared_view.id).
                  pluck(:group_id)
              )
            end
          end
        end

        describe 'and ownership does not change' do
          before do
            put(
              :update,
              params: {
                id:   shared_view.id,
                view: {
                  restriction: {
                    type: 'Group',
                    ids:  [group.id]
                  }
                }
              }
            )
          end

          it 'does not change the existing group associations' do
            assert(
              GroupView.where(
                account_id: account.id,
                group_id:   group.id,
                view_id:    shared_view.id
              ).one?
            )
          end
        end

        describe 'without `groups_views`' do
          before do
            GroupView.destroy_all

            put :update, params: { id: shared_view.id, view: valid_params }
          end

          it 'does not build new group associations' do
            assert(
              GroupView.where(
                account_id: account.id,
                group_id:   group.id,
                view_id:    shared_view.id
              ).none?
            )
          end
        end
      end
    end

    describe 'a DELETE to `destroy`' do
      describe 'when the view is personal' do
        before { delete :destroy, params: { id: personal_view.id } }

        it 'responds with `204 No Content`' do
          assert_response :no_content
        end

        it 'softs delete the view' do
          assert personal_view.reload.deleted?
        end
      end

      describe 'when the view is shared' do
        before { delete :destroy, params: { id: shared_view.id } }

        it 'responds with `204 No Content`' do
          assert_response :no_content
        end

        should_change('the number of views', by: -1) { account.views.count }
      end
    end

    describe 'a GET to `groups`' do
      let(:user)    { users(:minimum_agent) }

      1.upto(4).each do |i|
        let(:"group_#{i}") { account.groups.create!(name: i, is_active: true) }
      end

      let(:inactive_group) do
        account.groups.build(name: '5').tap do |group|
          group.is_active = false

          group.save!
        end
      end

      before do
        View.destroy_all
        Group.destroy_all

        create_view(title: 'A', owner: account,        active: true,  position: 2)
        create_view(title: 'B', owner: group_1,        active: false, position: 3)
        create_view(title: 'C', owner: group_1,        active: true,  position: 4)
        create_view(title: 'D', owner: user,           active: true,  position: 5)
        create_view(title: 'E', owner: inactive_group, active: true,  position: 6)
        create_view(title: 'F', owner: group_2,        active: true,  position: 7)

        new_view = create_view(title: 'G', owner: group_1, active: true, position: 1)
        new_view.groups_views.create! do |group_view|
          group_view.group_id = group_4.id
          group_view.account_id = new_view.account_id
        end
      end

      describe_with_arturo_enabled :multiple_group_views_reads do
        before do
          get :groups
        end

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        should_use_presenter Api::V2::GroupPresenter

        it 'returns all `group_id` associated with a view' do
          assert_equal(
            [group_1, group_2, group_4].map(&:name),
            json['groups'].map { |group| group['name'] }
          )
        end
      end

      describe_with_arturo_disabled :multiple_group_views_reads do
        before do
          get :groups
        end

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        should_use_presenter Api::V2::GroupPresenter

        it 'returns the first `group_id` associated with a view' do
          assert_equal(
            [group_1, group_2].map(&:name),
            json['groups'].map { |group| group['name'] }
          )
        end
      end
    end
  end
end
