require_relative "../../../../support/test_helper"
require_relative "../../../../support/user_views_test_helper"
require_relative "../../../../support/custom_fields_test_helper"
require_relative "../../../../support/api_test_helper"

if ENV["USERS_VIEWS_ALL"]
  # use this to check coverage
  require_relative 'user_view_controller/admin_test'
  require_relative 'user_view_controller/agent_execute_test'
  require_relative 'user_view_controller/agent_test'
  require_relative 'user_view_controller/end_user_test'
  require_relative 'user_view_controller/subsystem_user_test'
  SingleCov.covered! uncovered: 1
else
  SingleCov.not_covered! # test was split into multiple parts because it took too long
end

describe Api::V2::Rules::UserViewsController do
  extend Api::V2::TestHelper
  include UserViewsTestHelper
  include CustomFieldsTestHelper
  include ApiTestHelper

  fixtures :all

  def create_custom_fields
    create_dropdown_field("plan_type", "Plan",
      [{ name: "Gold",   value: "gold" },
       { name: "Silver", value: "silver" },
       { name: "Bronze", value: "bronze" }])
    create_user_custom_field!("ltv", "LTV", "Decimal")
  end

  before do
    Account.any_instance.stubs(:is_serviceable?).returns(true)
    Account.any_instance.stubs(:has_user_views_negative_operators_enabled?).returns(true)
    accept :json
    use_ssl
  end

  with_options(controller: "api/v2/rules/user_views") do |request|
    request.should_route :get,    "/api/v2/user_views",           action: "index"
    request.should_route :get,    "/api/v2/user_views/1",         action: "show",    id: 1
    request.should_route :get,    "/api/v2/user_views/1/execute", action: "execute", id: 1
    request.should_route :post,   "/api/v2/user_views/preview",   action: "preview"
    request.should_route :post,   "/api/v2/user_views",           action: "create"
    request.should_route :put,    "/api/v2/user_views/1",         action: "update",  id: 1
    request.should_route :delete, "/api/v2/user_views/1",         action: "destroy", id: 1
    request.should_route :get,    "/api/v2/user_views/1/export",  action: "export",  id: 1
    request.should_route :put,    "/api/v2/user_views/reorder",   action: "reorder"
  end

  # other roles are tested in user_views_controller subfolder

  as_an_anonymous_user do
    should_be_unauthorized :show
  end
end
