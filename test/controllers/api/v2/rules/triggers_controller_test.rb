require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'
require_relative '../../../../support/api_scopes_helper'
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::V2::Rules::TriggersController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  extend TestSupport::Rule::AdminRequest

  include TestSupport::Rule::Helper

  fixtures :accounts,
    :rules,
    :tickets,
    :users

  let(:trigger)  { rules(:trigger_notify_requester_of_received_request) }
  let(:account)  { accounts(:minimum) }
  let(:category) { create_category }

  let(:valid_params) do
    {
      title:       'Test Trigger',
      description: 'Description of the trigger',
      actions:     [{field: 'status', value: 'closed'}],
      conditions:  {
        all: [{field: 'status', operator: 'is', value: 'open'}]
      },
      category_id: category.id
    }
  end

  let(:filter_in_query) do
    {
      conditions: [
        {
          field: 'status',
          operator: 'is',
          value: 'open'
        },
        {
          field: 'group_id'
        }
      ],
      actions: [
        {
          field: 'notification_user',
          value: 557372
        }
      ],
      title: 'foo'
    }
  end

  let(:filter_with_null_value) do
    {
      conditions: [
        {
          field: 'assignee',
          operator: 'is',
          value: nil
        },
        {
          field: 'tags',
          operator: 'is'
        }
      ]
    }
  end

  let(:json_filter) do
    CGI.escape(JSON.generate(filter_in_query))
  end

  let(:json_filter_with_null_value) do
    CGI.escape(JSON.generate(filter_with_null_value))
  end

  let(:triggers) do
    JSON.parse(@response.body)['triggers'].map do |trigger|
      {
        title:    trigger['title'],
        active:   trigger['active'],
        position: trigger['position']
      }
    end
  end

  let(:active) { nil }

  let(:structured_filter) do
    {
      json: JSON.generate(
        filter: {
          "$and" => [
            {
              "conditions" => { "$eq" => "status_id is 1" }
            }, {
              "conditions" => { "$eq" => "group_id" }
            }, {
              "actions" => { "$eq" => "notification_user 557372" }
            }, {
              "title" => { "$eq" => "foo" }
            }
          ]
        }
      )
    }
  end

  let(:structured_filter_with_null_value) do
    {
      json: JSON.generate(
        filter: {
          "$and" => [
            {
              "conditions" => { "$eq" => "assignee_id is __NULL__" }
            }, {
              "conditions" => { "$eq" => "tags is" }
            }
          ]
        }
      )
    }
  end

  before do
    accept :json
    set_header('X-Zendesk-Lotus-Version', 'v1.2.3')
    use_ssl
  end

  with_options(controller: 'api/v2/rules/triggers') do |request|
    [
      [:get,    'api/v2/triggers/active',       {action: :active}],
      [:get,    'api/v2/triggers/123',          {action: :show, id: 123}],
      [:get,    'api/v2/triggers',              {action: :index}],
      [:post,   'api/v2/triggers',              {action: :create}],
      [:put,    'api/v2/triggers/1',            {action: :update, id: 1}],
      [:delete, 'api/v2/triggers/1',            {action: :destroy, id: 1}],
      [:put,    'api/v2/triggers/reorder',      {action: :reorder}],
      [:get,    'api/v2/triggers/search',       {action: :search}],
      [:get,    'api/v2/triggers/new',          {action: :new}],
      [:put,    'api/v2/triggers/update_many',  {action: :update_many}],
      [:delete, 'api/v2/triggers/destroy_many', {action: :destroy_many}],
      [:get,    'api/v2/triggers/definitions',  {action: :definitions}]
    ].each do |verb, path, options|
      request.should_route(verb, path, options)
    end
  end

  as_an_agent_with_permissions(business_rule_management: true) do
    let(:account) { accounts(:minimum) }

    records_admin_request(:index) { get :index }
    records_admin_request(:show)  { get :show, params: { id: create_trigger.id } }

    records_admin_request(:create) do
      post :create, params: {
        trigger: {
          title:       'Test Trigger',
          description: 'Description of the trigger',
          actions:     [{field: 'status', value: 'closed'}],
          conditions:  {all: [{field: 'status', operator: 'is', value: 'open'}]}
        }
      }
    end

    records_admin_request(:update) do
      put :update, params: { id: create_trigger.id, trigger: {active: false} }
    end

    records_admin_request(:destroy) { delete :destroy, params: { id: create_trigger.id } }

    records_admin_request(:update_many) do
      put :update_many, params: { triggers: [{id: create_trigger.id, active: false}] }
    end

    records_admin_request(:destroy_many) do
      delete :destroy_many, params: { ids: create_trigger.id }
    end

    records_admin_request(:active)      { get :active }
    records_admin_request(:search)      { get :search }
    records_admin_request(:definitions) { get :definitions }
  end

  describe 'when using OAuth tokens' do
    before do
      ZendeskSearch::Client.
        any_instance.
        stubs(search: {'results' => [], 'count' => 0})
    end

    with_scopes('triggers:read', 'read') do
      should_be_authorized { get :index }
      should_be_authorized { get :active }
      should_be_authorized { get :show, params: { id: trigger.id } }
      should_be_authorized { get :search, params: { query: 'test' } }
      should_be_authorized { get :new, params: { trigger_id: trigger.id } }
      should_be_authorized { get :definitions }

      should_not_be_authorized { post :create, params: { trigger: valid_params } }
    end

    with_scopes('triggers:write', 'write') do
      should_be_authorized { post :create, params: { trigger: valid_params } }

      should_be_authorized do
        put :update, params: { id: trigger.id, trigger: {title: 'My Title'} }
      end

      should_be_authorized { delete :destroy, params: { id: trigger.id } }

      should_be_authorized do
        put :reorder, params: { trigger_ids: account.triggers.active.map(&:id) }
      end

      should_be_authorized do
        put :update_many, params: {
          triggers: [
            {id: trigger.id, position: 4, active: true}
          ]
        }
      end

      should_be_authorized { put :destroy_many, params: { ids: trigger.id } }

      should_not_be_authorized { get :index }
    end

    with_scopes('read', 'write') do
      should_be_authorized { get :active }
      should_be_authorized { get :new, params: { trigger_id: trigger.id } }

      should_be_authorized do
        put :reorder, params: { trigger_ids: account.triggers.active.map(&:id) }
      end
    end
  end

  describe 'per_page' do
    let(:params)   { {per_page: per_page} }
    let(:per_page) { nil }

    before { @controller.stubs(params: params) }

    it 'defaults to 100 records' do
      assert_equal 100, @controller.send(:per_page)
    end

    describe 'when `per_page` is less than 1000' do
      describe 'and less than 100' do
        let(:per_page) { 50 }

        it 'is allowed' do
          assert_equal 50, @controller.send(:per_page)
        end
      end

      describe 'and greater than 100' do
        let(:per_page) { 700 }

        it 'is allowed' do
          assert_equal 700, @controller.send(:per_page)
        end
      end
    end

    describe 'when `per_page` is greater than 1000' do
      let(:per_page) { 2000 }

      it 'caps at a max of 1000 records' do
        assert_equal 1000, @controller.send(:per_page)
      end
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index,
      :show,
      :active,
      :create,
      :update,
      :destroy,
      :reorder,
      :search,
      :update_many,
      :destroy_many,
      :definitions
  end

  as_an_end_user do
    should_be_forbidden :index,
      :show,
      :active,
      :create,
      :update,
      :destroy,
      :reorder,
      :search,
      :update_many,
      :destroy_many,
      :definitions
  end

  describe 'when the user does not have permission to edit' do
    as_an_agent_with_permissions(business_rule_management: false) do
      should_be_forbidden :create,
        :update,
        :destroy,
        :reorder,
        :destroy_many,
        :update_many

      describe 'a GET to `index`' do
        before do
          Trigger.destroy_all

          create_trigger(title: 'ZB', active: false, position: 2)
          create_trigger(title: 'ZA', active: true,  position: 1)
          create_trigger(title: 'AB', active: false, position: 4)
          create_trigger(title: 'AA', active: true,  position: 3)

          get :index
        end

        should_use_presenter Api::V2::Rules::TriggerPresenter

        it 'returns triggers based on permission' do
          assert_equal(
            [
              {title: 'ZA', active: true, position: 1},
              {title: 'AA', active: true, position: 3}
            ],
            triggers
          )
        end
      end

      describe 'a GET to `show`' do
        before { get :show, params: { id: trigger.id } }

        should_use_presenter Api::V2::Rules::TriggerPresenter
      end

      describe 'a GET to `active`' do
        before { get :active }

        should_use_presenter Api::V2::Rules::TriggerPresenter
      end

      describe 'a GET to `definitions`' do
        before { get :definitions }

        should_use_presenter Api::V2::Rules::TriggerDefinitionsPresenter
      end
    end
  end

  as_an_agent_with_permissions(business_rule_management: true) do
    describe 'a GET to `index`' do
      before do
        Trigger.destroy_all

        create_trigger(title: 'ZB', active: false, position: 2)
        create_trigger(title: 'ZA', active: true,  position: 1)
        create_trigger(title: 'AB', active: false, position: 4)
        create_trigger(title: 'AA', active: true,  position: 3)
      end

      describe 'without parameters' do
        before { get :index }

        should_use_presenter Api::V2::Rules::TriggerPresenter

        it 'returns all triggers' do
          assert_equal(
            [
              {title: 'ZA', active: true,  position: 1},
              {title: 'ZB', active: false, position: 2},
              {title: 'AA', active: true,  position: 3},
              {title: 'AB', active: false, position: 4}
            ],
            triggers
          )
        end
      end

      describe 'with `active` parameter' do
        before { get :index, params: { active: false } }

        should_use_presenter Api::V2::Rules::TriggerPresenter

        it 'returns triggers sorted by that parameter' do
          assert_equal(
            [
              {title: 'ZB', active: false, position: 2},
              {title: 'AB', active: false, position: 4}
            ],
            triggers
          )
        end
      end

      describe 'with `sort_by` parameter' do
        before { get :index, params: { sort_by: 'alphabetical' } }

        should_use_presenter Api::V2::Rules::TriggerPresenter

        it 'returns triggers sorted by that parameter' do
          assert_equal(
            [
              {title: 'AA', active: true,  position: 3},
              {title: 'AB', active: false, position: 4},
              {title: 'ZA', active: true,  position: 1},
              {title: 'ZB', active: false, position: 2}
            ],
            triggers
          )
        end
      end

      describe 'with `sort_order` parameter' do
        before { get :index, params: { sort_order: 'desc' } }

        it 'returns triggers ordered by that parameter' do
          assert_equal(
            [
              {title: 'AB', active: false, position: 4},
              {title: 'AA', active: true,  position: 3},
              {title: 'ZB', active: false, position: 2},
              {title: 'ZA', active: true,  position: 1}
            ],
            triggers
          )
        end
      end

      describe_with_arturo_setting_enabled :trigger_categories_api do
        describe 'with `category_id` parameter' do
          before do
            Trigger.destroy_all

            @category_1 = create_category
            @category_2 = create_category

            @trigger_1 = create_trigger(title: '1', position: 1, rules_category_id: @category_1.id)
            @trigger_2 = create_trigger(title: '2', position: 1, rules_category_id: @category_2.id)
            @trigger_3 = create_trigger(title: '3', position: 2, rules_category_id: @category_2.id)
            @trigger_4 = create_trigger(title: '4', position: 3, rules_category_id: @category_2.id)
          end

          describe 'for the given category ID' do
            it 'returns the correct triggers' do
              get :index, params: { category_id: @category_2.id }

              assert_equal(
                [
                  {title: '2', active: true, position: 1},
                  {title: '3', active: true, position: 2},
                  {title: '4', active: true, position: 3}
                ],
                triggers
              )
            end
          end
        end
      end

      describe 'and side-loading usage counts' do
        let(:side_loads) { %w[usage_1h usage_24h usage_7d usage_30d] }

        describe 'with the `rule_usage_stats` feature' do
          before do
            Account.any_instance.stubs(has_rule_usage_stats?: true)

            get :index, params: { include: side_loads.join(',') }
          end

          it 'includes the usage side-loads' do
            assert(
              side_loads.all? do |side_load|
                json['triggers'].first.key?(side_load)
              end
            )
          end
        end

        describe 'without the `rule_usage_stats` feature' do
          before do
            Account.any_instance.stubs(has_rule_usage_stats?: false)

            get :index, params: { include: side_loads.join(',') }
          end

          it 'does not include the usage side-loads' do
            assert(
              side_loads.none? do |side_load|
                json['triggers'].first.key?(side_load)
              end
            )
          end
        end
      end

      describe 'and side-loading other resources' do
        let(:side_loads) { %w[app_installation permissions] }

        before do
          Rule.stubs(:apps_installations).returns(
            Trigger.all.first.id => {
              'id'       => 42,
              'settings' => {'title' => 'AAA'}
            }
          )

          get :index, params: { include: side_loads.join(',') }
        end

        it 'includes the appropriate item level side-loads' do
          assert(
            %w[app_installation permissions].all? do |side_load|
              json['triggers'].first.key?(side_load)
            end
          )
        end
      end
    end

    describe 'a GET to `show`' do
      before { get :show, params: { id: trigger.id } }

      should_use_presenter Api::V2::Rules::TriggerPresenter
    end

    describe 'a GET to `active`' do
      before do
        Trigger.destroy_all

        create_trigger(title: 'ZB', active: false, position: 2)
        create_trigger(title: 'ZA', active: true,  position: 1)
        create_trigger(title: 'AB', active: false, position: 4)
        create_trigger(title: 'AA', active: true,  position: 3)
        create_trigger(title: 'FA', active: true,  position: 5)
      end

      describe 'without parameters' do
        before { get :active }

        should_use_presenter Api::V2::Rules::TriggerPresenter

        it 'returns active triggers' do
          assert_equal(
            [
              {title: 'ZA', active: true, position: 1},
              {title: 'AA', active: true, position: 3},
              {title: 'FA', active: true, position: 5}
            ],
            triggers
          )
        end
      end

      describe 'with `sort_by` parameter' do
        before { get :active, params: { sort_by: 'alphabetical' } }

        should_use_presenter Api::V2::Rules::TriggerPresenter

        it 'returns active triggers sorted by that parameter' do
          assert_equal(
            [
              {title: 'AA', active: true, position: 3},
              {title: 'FA', active: true, position: 5},
              {title: 'ZA', active: true, position: 1}
            ],
            triggers
          )
        end
      end

      describe 'with `sort_order` parameter' do
        before { get :active, params: { sort_order: 'desc' } }

        it 'returns active triggers ordered by that parameter' do
          assert_equal(
            [
              {title: 'FA', active: true, position: 5},
              {title: 'AA', active: true, position: 3},
              {title: 'ZA', active: true, position: 1}
            ],
            triggers
          )
        end
      end
    end

    describe 'a GET to `search`' do
      let(:triggers) do
        json['triggers'].map { |trigger| {title: trigger['title']} }
      end

      before do
        Trigger.destroy_all

        create_trigger(title: 'ZB', active: false, position: 3)
        create_trigger(title: 'ZA', active: true,  position: 5)
        create_trigger(title: 'CB', active: false, position: 6)
        create_trigger(title: 'CA', active: true,  position: 2)
        create_trigger(title: 'CC', active: true,  position: 8)
        create_trigger(title: 'AB', active: false, position: 4)
        create_trigger(title: 'AA', active: true,  position: 1)
      end

      describe_with_arturo_disabled :new_trigger_search do
        before { Arturo.disable_feature!(:new_trigger_search) }

        describe 'without a `query` or a `filter` parameter' do
          before { get :search }

          it 'responds with `400 Bad Request`' do
            assert_response :bad_request
          end
        end

        describe 'with an invalid `query` parameter' do
          before { get :search, params: { query: '' } }

          it 'responds with `400 Bad Request' do
            assert_response :bad_request
          end
        end

        describe 'with a valid `query` parameter' do
          let(:results) do
            [
              {'id' => Trigger.find_by_title('AA').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('AB').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('ZA').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('CA').id, 'type' => 'Trigger'}
            ]
          end

          before do
            ZendeskSearch::Client.
              any_instance.
              stubs(search: {'results' => results, 'count' => results.size})
          end

          describe 'and no other parameters' do
            before { get :search, params: { query: 'test' } }

            should_use_presenter Api::V2::Rules::TriggerPresenter

            it 'returns the results' do
              assert_equal(
                [{title: 'AA'}, {title: 'AB'}, {title: 'ZA'}, {title: 'CA'}],
                triggers
              )
            end
          end

          describe 'and sorting parameters' do
            describe_with_arturo_setting_disabled :trigger_categories_api do
              before { get :search, params: { query: 'A', sort_by: 'position' } }

              before_should 'pass through the query and sort options' do
                ZendeskSearch::Client.
                  any_instance.
                  expects(:search).
                  with(
                    'A order_by:position,title sort:asc',
                    account_id:  account.id,
                    endpoint:    'rule',
                    from:        0,
                    fq:          '',
                    hl:          true,
                    incremental: true,
                    size:        100,
                    type:        'trigger'
                  ).
                  returns('results' => results, 'count' => results.size)
              end
            end

            describe_with_arturo_setting_enabled :trigger_categories_api do
              before do
                create_category

                get :search, params: { query: 'A', sort_by: 'position' }
              end

              before_should 'pass through the query and sort options' do
                ZendeskSearch::Client.
                  any_instance.
                  expects(:search).
                  with(
                    'A order_by:category_position,position,title sort:asc',
                    account_id:  account.id,
                    endpoint:    'rule',
                    from:        0,
                    fq:          '',
                    hl:          true,
                    incremental: true,
                    size:        100,
                    type:        'trigger'
                  ).
                  returns('results' => results, 'count' => results.size)
              end
            end
          end
        end
      end

      describe_with_arturo_enabled :new_trigger_search do
        before { Arturo.enable_feature!(:new_trigger_search) }
        describe 'without a `query` or a `filter` parameter' do
          before { get :search }

          it 'responds with `400 Bad Request`' do
            assert_response :bad_request
          end
        end

        describe 'with an invalid `query` parameter' do
          before { get :search, params: { query: '' } }

          it 'responds with `400 Bad Request' do
            assert_response :bad_request
          end
        end

        describe 'with a valid `query` parameter' do
          let(:results) do
            [
              {'id' => Trigger.find_by_title('AA').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('AB').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('ZA').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('CA').id, 'type' => 'Trigger'}
            ]
          end

          before do
            ZendeskSearch::Client.
              any_instance.
              stubs(search: {'results' => results, 'count' => results.size})
          end

          describe 'and no other parameters' do
            before { get :search, params: { query: 'test' } }

            should_use_presenter Api::V2::Rules::TriggerPresenter

            it 'returns the results' do
              assert_equal(
                [{title: 'AA'}, {title: 'AB'}, {title: 'ZA'}, {title: 'CA'}],
                triggers
              )
            end
          end

          describe 'and sorting parameters' do
            describe_with_arturo_setting_disabled :trigger_categories_api do
              before { get :search, params: { query: 'A', sort_by: 'position' } }

              before_should 'pass through the query and sort options' do
                ZendeskSearch::Client.
                  any_instance.
                  expects(:search).
                  with(
                    'A order_by:position,title sort:asc',
                    account_id:  account.id,
                    endpoint:    'rule',
                    from:        0,
                    fq:          '',
                    hl:          true,
                    incremental: true,
                    size:        100,
                    type:        'trigger'
                  ).
                  returns('results' => results, 'count' => results.size)
              end
            end

            describe_with_arturo_setting_enabled :trigger_categories_api do
              before do
                create_category

                get :search, params: { query: 'A', sort_by: 'position' }
              end

              before_should 'pass through the query and sort options' do
                ZendeskSearch::Client.
                  any_instance.
                  expects(:search).
                  with(
                    'A order_by:category_position,position,title sort:asc',
                    account_id:  account.id,
                    endpoint:    'rule',
                    from:        0,
                    fq:          '',
                    hl:          true,
                    incremental: true,
                    size:        100,
                    type:        'trigger'
                  ).
                  returns('results' => results, 'count' => results.size)
              end
            end
          end
        end

        describe 'with a valid `filter` parameter' do
          let(:results) do
            [
              {'id' => Trigger.find_by_title('AA').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('AB').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('ZA').id, 'type' => 'Trigger'},
              {'id' => Trigger.find_by_title('CA').id, 'type' => 'Trigger'}
            ]
          end

          before do
            ZendeskSearch::Client.
              any_instance.
              stubs(search: {'results' => results, 'count' => results.size})
          end

          describe 'and no other parameters' do
            describe 'with a regular filter value' do
              before { get :search, params: { filter: json_filter } }

              should_use_presenter Api::V2::Rules::TriggerPresenter

              it 'returns the results' do
                assert_equal(
                  [{title: 'AA'}, {title: 'AB'}, {title: 'ZA'}, {title: 'CA'}],
                  triggers
                )
              end
            end

            describe 'with a filter that includes a null value' do
              before { get :search, params: { filter: json_filter_with_null_value } }

              before_should 'call the search client with the right params, including the sorting ones' do
                ZendeskSearch::Client.
                  any_instance.
                  expects(:search).
                  with(
                    '',
                    account_id:              account.id,
                    endpoint:                'trigger',
                    filter: structured_filter_with_null_value,
                    from:                    0,
                    hl:                      true,
                    incremental:             true,
                    size:                    100
                  ).
                  returns('results' => results, 'count' => results.size)
              end

              should_use_presenter Api::V2::Rules::TriggerPresenter

              it 'returns the results' do
                assert_equal(
                  [{title: 'AA'}, {title: 'AB'}, {title: 'ZA'}, {title: 'CA'}],
                  triggers
                )
              end
            end
          end

          describe 'and sorting parameters' do
            describe 'using the default sorting order' do
              describe_with_arturo_setting_disabled :trigger_categories_api do
                before { get :search, params: { filter: json_filter, sort_by: 'position' } }

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:              account.id,
                      endpoint:                'trigger',
                      filter: structured_filter,
                      from:                    0,
                      hl:                      true,
                      incremental:             true,
                      size:                    100,
                      sort:                    'position,title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end

              describe_with_arturo_setting_enabled :trigger_categories_api do
                before do
                  create_category

                  get :search, params: { filter: json_filter, sort_by: 'position' }
                end

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:              account.id,
                      endpoint:                'trigger',
                      filter: structured_filter,
                      from:                    0,
                      hl:                      true,
                      incremental:             true,
                      size:                    100,
                      sort:                    'category_position,position,title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end
            end

            describe 'using the ascending sorting order' do
              describe_with_arturo_setting_disabled :trigger_categories_api do
                before { get :search, params: { filter: json_filter, sort_by: 'position', sort_order: 'asc' } }

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:              account.id,
                      endpoint:                'trigger',
                      filter: structured_filter,
                      from:                    0,
                      hl:                      true,
                      incremental:             true,
                      size:                    100,
                      sort:                    'position,title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end

              describe_with_arturo_setting_enabled :trigger_categories_api do
                before do
                  create_category

                  get :search, params: { filter: json_filter, sort_by: 'position', sort_order: 'asc' }
                end

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:              account.id,
                      endpoint:                'trigger',
                      filter: structured_filter,
                      from:                    0,
                      hl:                      true,
                      incremental:             true,
                      size:                    100,
                      sort:                    'category_position,position,title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end
            end

            describe 'using the descending sorting order' do
              describe_with_arturo_setting_disabled :trigger_categories_api do
                before { get :search, params: { filter: json_filter, sort_by: 'position', sort_order: 'desc' } }

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:              account.id,
                      endpoint:                'trigger',
                      filter: structured_filter,
                      from:                    0,
                      hl:                      true,
                      incremental:             true,
                      size:                    100,
                      sort:                    '-position,-title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end

              describe_with_arturo_setting_enabled :trigger_categories_api do
                before do
                  create_category

                  get :search, params: { filter: json_filter, sort_by: 'position', sort_order: 'desc' }
                end

                before_should 'call the search client with the right params, including the sorting ones' do
                  ZendeskSearch::Client.
                    any_instance.
                    expects(:search).
                    with(
                      '',
                      account_id:              account.id,
                      endpoint:                'trigger',
                      filter: structured_filter,
                      from:                    0,
                      hl:                      true,
                      incremental:             true,
                      size:                    100,
                      sort:                    '-category_position,-position,-title'
                    ).
                    returns('results' => results, 'count' => results.size)
                end
              end
            end
          end
        end
      end
    end

    describe 'a GET to `new`' do
      describe 'and no parameters are passed' do
        before { get :new }

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe 'and the `trigger_id` parameter is passed' do
        before { get :new, params: { trigger_id: trigger_id } }

        describe 'and the specified trigger exists' do
          let(:trigger_id) { trigger.id }

          it 'responds with `200 OK`' do
            assert_response :ok
          end

          it 'returns a replica of the trigger' do
            assert_equal trigger.title, json['trigger']['title']
          end
        end

        describe 'and the specified trigger does not exist' do
          let(:trigger_id) { 987654321 }

          it 'responds with `400 Bad Request`' do
            assert_response :bad_request
          end
        end
      end
    end

    describe 'a POST to `create`' do
      describe_with_arturo_setting_enabled :trigger_categories_api do
        let(:error_attribute) { "base" }
        let(:error) do
          {
            "error" => "RecordInvalid",
            "description" => "Record validation errors",
            "details" => { error_attribute => [error_message] }
          }
        end

        describe 'with the `category_id` param' do
          describe 'and a `position` param' do
            let(:trigger_params) { valid_params }

            before { post :create, params: { trigger: trigger_params } }

            it 'sets the `category_id`' do
              assert_response :created
              assert_equal category.id.to_s, json['trigger']['category_id']
            end
          end

          describe 'and without a `position` param' do
            let(:trigger_params) do
              valid_params.tap do |vp|
                vp.delete(:position)
              end
            end

            before { post :create, params: { trigger: trigger_params } }

            it 'sets the `category_id` and defaults the position to the end' do
              assert_response :created
              assert_equal category.id.to_s, json['trigger']['category_id']
              assert_equal(
                Trigger.where(rules_category_id: category.id).order(position: :desc).first.position,
                json['trigger']['position']
              )
            end
          end
        end

        describe 'with a valid `category` param' do
          let(:trigger_params) do
            valid_params.tap do |vp|
              vp.delete(:category_id)
              vp[:category] = { name: 'Valid category' }
            end
          end

          before { post :create, params: { trigger: trigger_params } }

          it 'creates the category and sets the `category_id`' do
            created_category_id = json['trigger']['category_id']

            assert_response :created
            assert_not_equal category.id, created_category_id
            assert RuleCategory.find_by_id(created_category_id)
          end

          describe 'and `category` contains the position param' do
            let(:created_category_id) { RuleCategory.all.order(:created_at).last.id.to_s }

            let(:trigger_params) do
              valid_params.tap do |vp|
                vp.delete(:category_id)
                vp[:category] = { name: 'Valid category', position: 1 }
              end
            end

            it 'creates the category and sets the `category_id`' do
              created_category_id = json['trigger']['category_id']

              assert_response :created
              assert_not_equal category.id, created_category_id
              assert RuleCategory.find_by_id(created_category_id)
              assert_equal 1, RuleCategory.find_by_id(created_category_id).position
            end

            it 'repositions other categories' do
              assert_equal 2, RuleCategory.find_by_id(category.id).position
            end
          end
        end

        describe 'with invalid `category` params' do
          let(:error_attribute) { "category_name" }
          let(:error_message) { {"description" => "Category name cannot be blank", "error" => "BlankValue"} }

          let(:trigger_params) do
            valid_params.tap do |vp|
              vp.delete(:category_id)
              vp[:category] = { position: 5 }
            end
          end

          before { post :create, params: { trigger: trigger_params } }

          it 'returns an error' do
            assert_response :unprocessable_entity
            assert_equal error, json
          end
        end

        describe 'with both `category_id` and `category` params' do
          let(:trigger_params) do
            valid_params.tap { |vp| vp[:category] = { name: 'Valid category' } }
          end

          before { post :create, params: { trigger: trigger_params } }

          it 'returns an error' do
            assert_response :bad_request
            assert_equal 'InvalidCategoryParam', json['error']
            assert_equal 'Only one parameter needed. Add a category_id or a category parameter', json['description']
          end
        end

        describe 'with neither `category_id` nor `category` params' do
          before do
            @category_1 = create_category(position: 2)
            @category_2 = create_category(position: 3)

            create_trigger(rules_category_id: @category_1.id, position: 1)
            create_trigger(rules_category_id: @category_1.id, position: 2)
            create_trigger(rules_category_id: @category_1.id, position: 3)
            create_trigger(rules_category_id: @category_1.id, position: 4)
            create_trigger(rules_category_id: @category_1.id, position: 5)

            create_trigger(rules_category_id: @category_2.id, position: 1)
            create_trigger(rules_category_id: @category_2.id, position: 2)
            create_trigger(rules_category_id: @category_2.id, position: 3)
            create_trigger(rules_category_id: @category_2.id, position: 4)
          end

          describe_with_arturo_enabled :trigger_categories_break_triggers_endpoint do
            let(:trigger_params) do
              valid_params.tap do |vp|
                vp.delete(:category_id)
                vp.delete(:category)
              end
            end

            before { post :create, params: { trigger: trigger_params } }

            it 'returns an error' do
              assert_response :bad_request
              assert_equal 'InvalidCategoryParam', json['error']
              assert_equal 'Add a category_id or category parameter', json['description']
            end
          end

          describe_with_arturo_disabled :trigger_categories_break_triggers_endpoint do
            let(:last_category_id) { RuleCategory.trigger_categories(account).order(position: :desc).first.id.to_s }

            describe 'and the trigger contains a position param' do
              let(:trigger_params) do
                valid_params.tap do |vp|
                  vp.delete(:category_id)
                  vp.delete(:category)
                  vp[:position] = position
                end
              end

              describe 'and the position is within bounds' do
                let(:position) { 7 }

                before { post :create, params: { trigger: trigger_params } }

                it 'sets the trigger to the correct category and position' do
                  assert_response :created
                  assert_equal @category_2.id.to_s, json['trigger']['category_id']
                  assert_equal 2, json['trigger']['position']
                end
              end

              describe 'and the position is out of bounds' do
                let(:position) { 0 }

                before { post :create, params: { trigger: trigger_params } }

                it 'sets the trigger to the last category and position' do
                  assert_response :created
                  assert_equal last_category_id, json['trigger']['category_id']
                  assert_equal 5, json['trigger']['position']
                end
              end

              describe 'and there are no active triggers to compare to' do
                let(:position) { 7 }

                it 'sets the trigger to the last category' do
                  Trigger.destroy_all
                  post :create, params: { trigger: trigger_params }

                  assert_response :created
                  assert_equal last_category_id, json['trigger']['category_id']
                  assert_equal 1, json['trigger']['position']
                end
              end
            end

            describe 'and the trigger does not contain a position param' do
              let(:trigger_params) do
                valid_params.tap do |vp|
                  vp.delete(:category_id)
                  vp.delete(:category)
                  vp.delete(:position)
                end
              end

              before { post :create, params: { trigger: trigger_params } }

              it 'sets the trigger to the last category and position' do
                assert_response :created
                assert_equal last_category_id, json['trigger']['category_id']
                assert_equal 5, json['trigger']['position']
              end
            end
          end
        end
      end

      describe_with_arturo_setting_disabled :trigger_categories_api do
        before { post :create, params: { trigger: valid_params } }

        it 'does not set the `category_id`' do
          assert_response :created
          assert_nil json['trigger']['category_id']
        end
      end

      describe 'with a valid trigger' do
        before { post :create, params: { trigger: valid_params } }

        should_use_presenter Api::V2::Rules::TriggerPresenter,
          status:   :created,
          location: %r{api/v2/triggers/\d+\.json}

        it 'sets the `title`' do
          assert_equal 'Test Trigger', json['trigger']['title']
        end

        it 'sets the `description`' do
          assert_equal(
            'Description of the trigger',
            json['trigger']['description']
          )
        end

        it 'sets the `actions`' do
          assert_equal(
            [{'field' => 'status', 'value' => 'closed'}],
            json['trigger']['actions']
          )
        end

        it 'sets the `conditions`' do
          assert_equal(
            {
              'all' => [
                {'field' => 'status', 'operator' => 'is', 'value' => 'open'}
              ],
              'any' => []
            },
            json['trigger']['conditions']
          )
        end
      end

      describe 'with invalid params' do
        describe 'with no params' do
          before { post :create }

          should_use_presenter Api::V2::ErrorsPresenter,
            status: :unprocessable_entity
        end

        describe 'with non-existing update_type' do
          describe_with_arturo_enabled 'update_type_validation' do
            before do
              invalid_params = valid_params.tap do |vp|
                vp[:conditions][:all] << { field: 'update_type', operator: 'is', value: 'batman' }
              end
              post :create, params: { trigger: invalid_params }
            end

            it 'fails' do
              assert_response :unprocessable_entity
            end

            it 'returns an error' do
              error = {
                "error" => "RecordInvalid",
                "description" => "Record validation errors",
                "details" => {
                  "base" => [
                    {"description" => "Invalid value 'batman' in 'Is... / Is / batman'"}
                  ]
                }
              }

              assert_equal error, JSON.parse(response.body)
            end
          end

          describe_with_arturo_disabled 'update_type_validation' do
            before do
              invalid_params = valid_params.tap do |vp|
                vp[:conditions][:all] << { field: 'update_type', operator: 'is', value: 'batman' }
              end
              post :create, params: { trigger: invalid_params }
            end

            it 'succeeds' do
              assert_response :created
            end
          end
        end
      end

      describe_with_arturo_enabled :apply_rules_table_limits_for_triggers do
        describe 'and the maximum limit for triggers has not been reached' do
          before { post :create, params: { trigger: valid_params } }

          it('responds with created') { assert_response :created }
        end

        describe 'and the maximum limit for triggers has been reached' do
          let(:limit) { 0 }

          before do
            Rule.any_instance.stubs(usage_limit: limit)

            post :create, params: { trigger: valid_params }
          end

          it('responds with forbidden') { assert_response :forbidden }

          it('includes an error message') do
            assert_equal(
              I18n.t(
                'txt.admin.models.rules.rule.limit_exceeded',
                limit: limit,
                rule_type: I18n.t(
                  'txt.admin.models.rules.rule.rule_type.triggers'
                )
              ),
              JSON.parse(response.body)['errors'][0]['title']
            )
          end
        end
      end

      describe_with_arturo_disabled :apply_rules_table_limits_for_triggers do
        describe 'and the maximum limit for triggers has been reached' do
          let(:limit) { 0 }

          before do
            Rule.any_instance.stubs(usage_limit: limit)

            post :create, params: { trigger: valid_params }
          end

          it('responds with created') { assert_response :created }
        end
      end
    end

    describe 'a PUT to `update`' do
      describe 'with valid params' do
        before do
          trigger.update_attributes!(is_active: false, position: 9999)

          put :update, params: { id: trigger.id, trigger: params }
        end

        describe 'and it includes `active`' do
          let(:params) { {active: true} }

          it 'sets `active`' do
            assert trigger.reload.is_active?
          end
        end

        describe 'and it includes `position`' do
          let(:params) { {position: 1} }

          it 'sets the `position`' do
            assert_equal 1, trigger.reload.position
          end
        end

        describe 'and it includes `description`' do
          let(:params) { {description: 'Describe this'} }

          it 'sets the `description`' do
            assert_equal 'Describe this', trigger.reload.description
          end
        end

        describe 'and it includes `actions` and `conditions`' do
          let(:params) do
            {
              all:     [{field: 'type', operator: 'is', value: 'problem'}],
              actions: [{field: 'type', value: 'task'}]
            }
          end
          let(:response) { JSON.parse(@response.body) }

          it 'updates the `all` conditions' do
            assert_equal(
              params[:all].map(&:stringify_keys),
              response['trigger']['conditions']['all']
            )
          end

          it 'updates the actions' do
            assert_equal(
              params[:actions].map(&:stringify_keys),
              response['trigger']['actions']
            )
          end
        end
      end

      describe_with_arturo_setting_enabled :trigger_categories_api do
        before do
          trigger.update_column(:rules_category_id, create_category.id)
        end

        it 'can update with a new category_id' do
          c = create_category

          put :update, params: {id: trigger.id, trigger: {category_id: c.id}}

          trigger.reload

          assert_response :success

          assert_equal c.id, trigger.rules_category_id
        end

        it 'gives an error if the category_id does not exist' do
          put :update, params: { id: trigger.id, trigger: { category_id: 1234 } }

          assert_response :unprocessable_entity
          assert_equal(
            {
              "error" => "RecordInvalid",
              "description" => "Record validation errors",
              "details" => {"category_id" => [{"description" => "Category category_id does not exist"}]}
            },
            JSON.parse(response.body)
          )
        end

        it 'can update with category params' do
          put :update, params: {id: trigger.id, trigger: {category: {name: 'Hello'}}}

          assert_response :success
        end
      end

      describe_with_arturo_setting_disabled :trigger_categories_api do
        it 'removes rule_category_id param' do
          trigger.update_column(:rules_category_id, create_category.id)

          c = create_category
          initial_trigger_category_id = trigger.rules_category_id

          put :update, params: {id: trigger.id, trigger: {category_id: c.id}}

          assert_response :success

          trigger.reload

          assert_equal initial_trigger_category_id, trigger.rules_category_id
        end
      end

      describe 'when parameters are invalid' do
        before { put :update, params: { id: trigger.id, trigger: {all: []} } }

        should_use_presenter(
          Api::V2::ErrorsPresenter,
          status: :unprocessable_entity
        )
      end

      describe 'when the existing trigger is invalid' do
        before { trigger.update_attribute(:definition, Definition.new) }

        describe 'and the trigger was active' do
          describe 'and remains active' do
            before { put :update, params: { id: trigger.id, trigger: {active: true} } }

            should_use_presenter(
              Api::V2::ErrorsPresenter,
              status: :unprocessable_entity
            )
          end

          describe 'and is deactivated' do
            describe 'and only `active` is specified' do
              before do
                put(
                  :update,
                  params: {
                    id:      trigger.id,
                    trigger: {active: false}
                  }
                )
              end

              it 'responds with `200 OK`' do
                assert_response :ok
              end

              it 'updates the trigger' do
                refute trigger.reload.is_active?
              end
            end

            describe 'and other parameters are specified' do
              before do
                put(
                  :update,
                  params: {
                    id:      trigger.id,
                    trigger: {active: false, title: 'New title'}
                  }
                )
              end

              should_use_presenter(
                Api::V2::ErrorsPresenter,
                status: :unprocessable_entity
              )
            end
          end
        end
      end
    end

    describe 'a PUT to `update_many`' do
      before do
        @category1 = create_category
        @category2 = create_category

        Trigger.destroy_all

        @trigger1 = create_trigger(title: 'ZA', active: true, position: 1, rules_category_id: @category1.id)
        @trigger2 = create_trigger(title: 'AA', active: true, position: 3, rules_category_id: @category2.id)
        @trigger3 = create_trigger(title: 'ZB', active: false, position: 2, rules_category_id: @category1.id)
        @trigger4 = create_trigger(title: 'AB', active: false, position: 4, rules_category_id: @category2.id)
        @trigger5 = create_trigger(title: 'AC', active: true, position: 2, rules_category_id: @category1.id)
      end

      describe 'presenter' do
        before do
          put :update_many, params: {
            triggers: [
              {id: @trigger3.id, position: 1},
              {id: @trigger2.id, position: 3, active: false},
            ]
          }
        end

        should_use_presenter Api::V2::Rules::TriggerPresenter
      end

      it 'updates the triggers' do
        put :update_many, params: {
          triggers: [
            {id: @trigger3.id, position: 1},
            {id: @trigger2.id, position: 3, active: false},
          ]
        }

        assert_equal(
          [
            {title: 'ZB', active: false, position: 1},
            {title: 'AA', active: false, position: 3}
          ],
          json['triggers'].map do |trigger|
            {
              title: trigger['title'],
              active: trigger['active'],
              position: trigger['position']
            }
          end
        )
      end

      describe_with_arturo_setting_enabled :trigger_categories_api do
        it 'can update multiple category_ids' do
          put :update_many, params: {
            triggers: [
              {id: @trigger1.id, category_id: @category2.id},
              {id: @trigger2.id, category_id: @category1.id, position: 4}
            ]
          }

          assert_response :ok

          @trigger1.reload
          @trigger2.reload

          assert_equal @category2.id, @trigger1.rules_category_id
          assert_equal @category1.id, @trigger2.rules_category_id
          assert_equal 2, @trigger2.position
        end

        it 'returns an error if the category is not valid' do
          put :update_many, params: {
            triggers: [
              {id: @trigger1.id, category_id: 12345},
              {id: @trigger2.id, category_id: @category1.id, position: 4}
            ]
          }

          assert_response :unprocessable_entity
          assert_equal(
            {
              "error" => "RecordInvalid",
              "description" => "Record validation errors",
              "details" => {
                "category_id" => [{"description" => "Category category_id does not exist"}]
              }
            }, JSON.parse(response.body)
          )
        end

        describe 'trigger (de)activation and category change behavior' do
          before do
            put :update_many, params: { triggers: triggers }

            assert_response :ok
          end

          describe 'when a trigger is deactivated' do
            let(:triggers) { [{ id: @trigger1.id, active: false }] }

            it 'does not reposition active triggers' do
              assert_equal 2, @trigger5.reload.position
            end
          end

          describe 'when a trigger is reactivated' do
            describe 'and position is specified' do
              let(:triggers) { [{ id: @trigger3.id, active: true, position: 1 }] }

              it 'honors the specified position value' do
                assert_equal 1, @trigger3.reload.position
              end

              it 'repositions other active triggers' do
                assert_equal 2, @trigger1.reload.position
              end
            end

            describe 'and position is not specified' do
              let(:triggers) { [{ id: @trigger3.id, active: true }] }

              it 'uses the existing position value' do
                assert_equal 2, @trigger3.reload.position
              end

              it 'repositions active triggers' do
                assert_equal 3, @trigger5.reload.position
              end
            end
          end

          describe 'when a trigger changes categories' do
            describe 'and position is specified' do
              let(:triggers) { [{ id: @trigger1.id, category_id: @category2.id, position: 1 }] }

              it 'uses the given position value' do
                assert_equal 1, @trigger1.reload.position
              end

              it 'repositions active triggers' do
                assert_equal 2, @trigger2.reload.position
              end
            end

            describe 'and position is not specified' do
              let(:triggers) { [{ id: @trigger1.id, category_id: @category2.id }] }

              it 'defaults position to the end of the category of active triggers' do
                assert_equal 4, @trigger1.reload.position
              end

              describe 'and multiple triggers change to the same category' do
                let(:triggers) do
                  [
                    { id: @trigger5.id, category_id: @category2.id },
                    { id: @trigger1.id, category_id: @category2.id }
                  ]
                end

                it 'defaults their positions to the end of the category in list order' do
                  assert_equal 4, @trigger5.reload.position
                  assert_equal 5, @trigger1.reload.position
                end
              end
            end
          end
        end
      end

      describe_with_arturo_setting_disabled :trigger_categories_api do
        it 'disregards category_ids' do
          put :update_many, params: {
            triggers: [
              {id: @trigger1.id, category_id: @category2.id},
              {id: @trigger2.id, category_id: @category1.id, position: 4}
            ]
          }

          assert_response :ok

          @trigger1.reload
          @trigger2.reload

          assert_equal @category1.id, @trigger1.rules_category_id
          assert_equal @category2.id, @trigger2.rules_category_id
          assert_equal 4, @trigger2.position
        end
      end
    end

    describe 'a PUT to `reorder`' do
      let(:rule_1) { Trigger.where(account_id: account.id, title: 'ZA').first }
      let(:rule_2) { Trigger.where(account_id: account.id, title: 'AA').first }

      before do
        Trigger.destroy_all

        category = create_category
        create_trigger(title: 'ZA', active: true,  position: 1, rules_category_id: category.id)
        create_trigger(title: 'AA', active: true,  position: 3, rules_category_id: category.id)
        create_trigger(title: 'ZB', active: false, position: 2, rules_category_id: category.id)
        create_trigger(title: 'AB', active: false, position: 4, rules_category_id: category.id)
      end

      describe 'when it includes all active trigger IDs on the account' do
        before { put :reorder, params: { trigger_ids: [rule_1.id, rule_2.id] } }

        it 'responds with `200 OK`' do
          assert_response :ok
        end
      end

      describe 'when it does not include all active trigger IDs on the account' do
        before { put :reorder, params: { trigger_ids: [rule_1.id] } }

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe 'when an account has more than one category' do
        before { create_category }

        describe_with_arturo_setting_enabled :trigger_categories_api do
          before { put :reorder, params: {trigger_ids: [rule_1.id, rule_2.id]} }

          it 'responds with `400 Bad Request` and error description' do
            error = {
              "error" => "LimitOneCategory",
              "description" => "this action is not permitted with multiple categories"
            }

            assert_response :bad_request
            assert_equal error, JSON.parse(response.body)
          end
        end

        describe_with_arturo_setting_disabled :trigger_categories_api do
          before { put :reorder, params: {trigger_ids: [rule_1.id, rule_2.id]} }

          it 'responds with `200 OK`' do
            assert_response :ok
          end
        end
      end
    end

    describe 'a DELETE to `destroy`' do
      before { delete :destroy, params: { id: trigger.id } }

      it 'responds with `204 No Content`' do
        assert_response :no_content
      end

      should_change('the number of triggers', by: -1) do
        account.triggers.count
      end

      it 'softs delete the trigger' do
        assert trigger.reload.deleted?
      end
    end

    describe 'a DELETE to `destroy_many`' do
      let(:ids) do
        %w[ZB AA AB].map do |title|
          Trigger.where(title: title).first.id
        end.join(',')
      end

      before do
        Trigger.destroy_all

        create_trigger(title: 'ZA', active: true, position: 1)
        create_trigger(title: 'AA', active: false, position: 3)
        create_trigger(title: 'ZB', active: false, position: 2)
        create_trigger(title: 'AB', active: false, position: 4)
      end

      it 'responds with `204 No Content`' do
        delete :destroy_many, params: { ids: ids }

        assert_response :no_content
      end

      it 'soft-deletes the triggers' do
        delete :destroy_many, params: { ids: ids }

        assert_equal(
          3,
          Trigger.with_deleted do
            Trigger.where(title: %w[ZB AA AB]).
              where('deleted_at IS NOT NULL').
              count
          end
        )
      end

      describe 'and there are invalid params' do
        let(:ids) { "" }

        it 'returns an error' do
          delete :destroy_many, params: { ids: ids }

          assert_response :bad_request
        end
      end

      describe_with_arturo_setting_enabled :trigger_categories_api do
        describe 'and there are active triggers' do
          let(:ids) do
            %w[ZB AA AB ZA].map do |title|
              Trigger.where(title: title).first.id
            end.join(',')
          end

          it 'returns an error' do
            delete :destroy_many, params: { ids: ids }

            assert_response :bad_request
          end
        end
      end
    end

    describe 'a GET to `definitions`' do
      before { get :definitions }

      it 'does not perform N+1 queries' do
        assert_no_n_plus_one do
          get :definitions
        end
      end

      it 'responds with `200 OK`' do
        assert_response :ok
      end

      should_use_presenter Api::V2::Rules::TriggerDefinitionsPresenter
    end
  end
end
