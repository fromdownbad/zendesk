require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'
require_relative '../../../../support/api_scopes_helper'

SingleCov.covered!

describe Api::V2::Rules::TriggerRevisionsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper

  include TestSupport::Rule::Helper

  fixtures :all

  let(:account)  { accounts(:minimum) }
  let(:trigger)  { create_trigger }
  let(:revision) { trigger.revisions.first }

  before do
    accept :json
    use_ssl

    Subscription.any_instance.stubs(has_trigger_revision_history?: true)
  end

  with_options controller: 'api/v2/rules/trigger_revisions' do |request|
    [
      [
        :get,
        '/api/v2/triggers/1/revisions',
        {action: :index, trigger_id: 1}
      ],
      [
        :get,
        '/api/v2/triggers/1/revisions/2',
        {action: :show, trigger_id: 1, id: 2}
      ]
    ].each do |verb, path, options|
      request.should_route(verb, path, options)
    end
  end

  describe 'when using OAuth tokens' do
    with_scopes 'triggers:read', 'read' do
      should_be_authorized { get :index, params: { trigger_id: trigger.id } }

      should_be_authorized do
        get :show, params: { trigger_id: trigger.id, id: revision.nice_id }
      end
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized(
      [:get, :index, {trigger_id: 1}],
      [:get, :show,  {trigger_id: 1, id: 1}]
    )
  end

  as_an_end_user do
    should_be_forbidden(
      [:get, :index, {trigger_id: 1}],
      [:get, :show,  {trigger_id: 1, id: 1}]
    )
  end

  as_an_admin do
    should_be_authorized { get :index, params: { trigger_id: trigger.id } }

    should_be_authorized do
      get :show, params: { trigger_id: trigger.id, id: revision.nice_id }
    end

    describe 'when the account does not have trigger revision history' do
      before do
        Subscription.any_instance.stubs(has_trigger_revision_history?: false)
      end

      should_be_forbidden(
        [:get, :index, {trigger_id: 1}],
        [:get, :show,  {trigger_id: 1, id: 1}]
      )
    end
  end

  as_an_agent do
    let(:limit) { nil }

    let(:revision_ids) do
      json['trigger_revisions'].map { |revision| revision['id'] }
    end

    let(:sorted_revision_ids) do
      json['trigger_revisions'].
        sort_by { |revision| [revision['created_at'], revision['id']] }.
        map { |revision| revision['id'] }.
        reverse
    end

    describe 'a GET to `index`' do
      let(:trigger_id) { trigger.id }

      before do
        2.times { create_revision(trigger, after: 1.minutes) }
        create_revision(trigger, after: 2.minutes)

        get :index, params: { trigger_id: trigger_id, limit: limit }
      end

      should_use_presenter Api::V2::Rules::TriggerRevisionPresenter

      it 'responds with `200 OK`' do
        assert_response :ok
      end

      it 'orders the revisions by `created_at DESC, nice_id DESC`' do
        assert_equal sorted_revision_ids, revision_ids
      end

      describe 'with a limit parameter' do
        let(:limit) { 1 }

        it 'limits the number of fetched revisions' do
          assert_equal 1, json['trigger_revisions'].count
        end
      end

      describe 'when the cache is cleared' do
        before { Rails.cache.clear }

        it 'does not perform `n+1` queries' do
          assert_no_n_plus_one { get :index, params: { trigger_id: trigger.id } }
        end
      end

      describe 'when the trigger does not exist' do
        let(:trigger_id) { Trigger.maximum(:id) + 1 }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end
    end

    describe 'a GET to `show`' do
      let(:revision_nice_id) { revision.nice_id }

      before { get :show, params: { trigger_id: trigger.id, id: revision_nice_id } }

      should_use_presenter Api::V2::Rules::TriggerRevisionPresenter

      it 'returns the correct revision' do
        assert_equal revision_nice_id, json['trigger_revision']['id']
      end

      describe 'when the revision does not exist' do
        let(:revision_nice_id) { TriggerRevision.maximum(:nice_id) + 1 }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end
    end
  end
end
