require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Rules::MacroAttachmentsController do
  extend Api::V2::TestHelper

  include TestSupport::Rule::Helper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:actions) { [DefinitionItem.new('comment_value_html', nil, 'Nice tie!')] }

  let(:macro) do
    create_macro(
      definition: Definition.new.tap do |definition|
        definition.actions.push(*actions)
      end
    )
  end

  let(:attachment_json)  { JSON.parse(@response.body)['macro_attachment'] }
  let(:attachments_json) { JSON.parse(@response.body)['macro_attachments'] }

  before do
    accept :json
    use_ssl
  end

  def assert_attachment_url(id)
    assert_match %r{/api/v2/macros/attachments/#{id}}, attachment_json['url']
  end

  with_options controller: 'api/v2/rules/macro_attachments' do |request|
    [
      [:post, '/api/v2/macros/attachments', {action: :create}],
      [:post, '/api/v2/macros/1/attachments', {action: :create, macro_id: 1 }],
      [:get,  '/api/v2/macros/1/attachments', {action: :index, macro_id: 1 }],
      [:get,  '/api/v2/macros/attachments/1', {action: :show, id: 1 }],
      [:get,  '/api/v2/macros/attachments/1/content', {action: :content, id: 1}]
    ].each do |verb, path, options|
      request.should_route(verb, path, options)
    end
  end

  as_an_end_user do
    describe 'a POST to `create`' do
      before { post :create }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a GET to `index`' do
      before { get :index, params: { macro_id: 1 } }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a GET to `show`' do
      before { get :show, params: { id: 1 } }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a GET to `content`' do
      before { get :content, params: { id: 1 } }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end
  end

  as_an_agent do
    let(:filename)         { 'small.png' }
    let(:display_filename) { 'small pic.png' }
    let(:attachment)       { fixture_file_upload(filename, "image/png") }

    let(:rule_attachment) do
      RuleAttachment.where(
        account_id: account.id,
        id:         attachment_json['id']
      ).first
    end

    let(:rule_attachment_rule) do
      RuleAttachmentRule.where(
        account_id:         account.id,
        rule_attachment_id: rule_attachment.id
      ).first
    end

    describe 'a POST to `create`' do
      describe 'when successful' do
        before { post :create, params: { attachment: attachment, filename: filename } }

        it 'responds with `201 Created`' do
          assert_response :created
        end

        it 'creates a rule attachment in `rule_attachments`' do
          assert rule_attachment
        end

        should_use_presenter Api::V2::Rules::MacroAttachmentPresenter

        it 'presents the proper URL' do
          assert_attachment_url(rule_attachment.id)
        end
      end

      describe 'when the macro exists' do
        before do
          post :create,
            params: {
              macro_id:   macro.id,
              attachment: attachment,
              filename:   filename
            }
        end

        it 'responds with `201 Created`' do
          assert_response :created
        end

        it 'associates the attachment with the macro' do
          assert_equal macro.id, rule_attachment_rule.rule_id
        end
      end

      describe 'when the macro does not exist' do
        before do
          post :create,
            params: {
              macro_id:   123456789,
              attachment: attachment,
              filename:   filename
            }
        end

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'when the macro has reached the max number of attachments' do
        let!(:rule_attachments) do
          Array.new(5) do
            RuleAttachment.create! do |attachment|
              attachment.account_id       = account.id
              attachment.user_id          = users(:minimum_end_user)
              attachment.size             = 1
              attachment.content_type     = 'application/unknown'
              attachment.filename         = 'hello_world'
              attachment.display_filename = 'Hello World'
            end
          end
        end

        before do
          rule_attachments.each do |rule_attachment|
            rule_attachment.associate(macro)
          end

          post :create,
            params: {
              macro_id:   macro.id,
              attachment: attachment,
              filename:   filename
            }
        end

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe_with_and_without_arturo_enabled :email_increase_attachment_size_limit do
        describe 'when the attachment is smaller than the old plan based size limits and the new size limit' do
          let(:filename_small) { 'small.png' }
          let(:attachment_small) { fixture_file_upload(filename_small, "image/png") }

          before do
            post :create,
              params: {
                macro_id: macro.id,
                attachment: attachment_small,
                filename: filename_small
              }
          end

          it 'responds with `201 Created`' do
            assert_response :created
          end
        end
      end

      describe_with_arturo_enabled :email_increase_attachment_size_limit do
        describe 'when the attachment is larger than 25MB but smaller than 50MB' do
          let(:attachment_30MB) do
            Tempfile.create(["attachment_30MB", ".txt"]) do |file|
              file.write 'a' * 30.megabytes
              Rack::Test::UploadedFile.new(file)
            end
          end

          before do
            post :create,
              params: {
                macro_id: macro.id,
                attachment: attachment_30MB,
                filename: attachment_30MB.original_filename
              }
          end

          it 'responds with `201 Created`' do
            assert_response :created
          end
        end

        describe 'when the attachment (51MB) is larger than the 50MB account limit' do
          let(:attachment_51MB) do
            Tempfile.create(["attachment_51MB", ".txt"]) do |file|
              file.write 'a' * 51.megabytes
              Rack::Test::UploadedFile.new(file)
            end
          end

          before do
            post :create,
              params: {
                macro_id: macro.id,
                attachment: attachment_51MB,
                filename: attachment_51MB.original_filename
              }
          end

          it 'responds with `413 Payload Too Large`' do
            assert_response(:payload_too_large)
          end
        end
      end

      describe_with_arturo_disabled :email_increase_attachment_size_limit do
        describe 'when the attachment is larger than the account limit' do
          let(:attachment) { fixture_file_upload('too_large.bin') }

          before do
            post :create,
              params: {
                macro_id: macro.id,
                attachment: attachment,
                filename: filename
              }
          end

          it 'responds with `413 Payload Too Large`' do
            assert_response(:payload_too_large)
          end
        end
      end

      describe 'when the macro does not have a rich comment action' do
        let(:actions) { [DefinitionItem.new('set_tags', nil, ['test'])] }

        before do
          post :create,
            params: {
              macro_id:   macro.id,
              attachment: attachment,
              filename:   filename
            }
        end

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe 'when an attachment is not provided' do
        before { post :create, params: { filename: filename } }

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe 'when a filename is not specified' do
        before { post :create, params: { attachment: attachment } }

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end
    end

    describe 'a GET to `index`' do
      let!(:rule_attachments) do
        Array.new(3) do
          RuleAttachment.create! do |ra|
            ra.account_id       = account.id
            ra.user_id          = users(:minimum_agent).id
            ra.uploaded_data    = attachment
            ra.filename         = filename
            ra.display_filename = display_filename
          end
        end
      end

      describe 'when the macro is associated with rule attachments' do
        before do
          rule_attachments.each do |rule_attachment|
            rule_attachment.associate(macro)
          end
        end

        describe 'and the macro is not associated with macro attachments' do
          before do
            get :index, params: { macro_id: macro.id }
          end

          it 'returns the attachments' do
            assert_equal(
              rule_attachments.map(&:id).sort,
              attachments_json.map do |attachment_json|
                attachment_json['id']
              end.sort
            )
          end
        end
      end

      describe 'when the macro does not exist' do
        before { get :index, params: { macro_id: 123456789 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'when the macro is not associated with attachments' do
        before { get :index, params: { macro_id: macro.id } }

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'returns no attachments' do
          assert_equal [], attachments_json
        end
      end
    end

    describe 'a GET to `show`' do
      let!(:rule_attachment) do
        RuleAttachment.create! do |ra|
          ra.account_id       = account.id
          ra.user_id          = users(:minimum_agent).id
          ra.uploaded_data    = attachment
          ra.filename         = filename
          ra.display_filename = display_filename
        end
      end

      describe 'when the attachment is a rule attachment' do
        before { get :show, params: { id: rule_attachment.id } }

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'returns the composite attachment' do
          assert_equal rule_attachment.id, attachment_json['id']
        end

        should_use_presenter Api::V2::Rules::MacroAttachmentPresenter

        it 'presents the proper URL' do
          assert_attachment_url(rule_attachment.id)
        end
      end

      describe 'when the attachment does not exist' do
        before do
          get :show, params: { id: 12345 }
        end

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end
    end

    describe 'a GET to `content`' do
      let!(:rule_attachment) do
        RuleAttachment.create! do |ra|
          ra.account_id       = account.id
          ra.user_id          = users(:minimum_agent).id
          ra.uploaded_data    = attachment
          ra.filename         = filename
          ra.display_filename = display_filename
        end
      end

      describe 'when the environment is development' do
        before do
          Api::V2::Rules::MacroAttachmentsController.
            any_instance.
            stubs(:via_nginx?).
            returns(false)
        end

        describe 'and the attachment is an existing rule attachment' do
          before { get :content, params: { id: rule_attachment.id } }

          it 'sets the `Content-Type` header' do
            assert_equal 'image/png', @response.headers['Content-Type']
          end

          it 'sets the `Content-Disposition` header' do
            assert_equal(
              %(inline; filename="#{filename}"),
              @response.headers['Content-Disposition']
            )
          end

          it 'sets the `Content-Length` header' do
            assert_equal(
              rule_attachment['size'].to_s,
              @response.headers['Content-Length']
            )
          end

          it 'does not set the `X-Accel-Redirect` header' do
            assert_nil @response.headers['X-Accel-Redirect']
          end

          it 'does not set the `X-S3-Storage-Url` header' do
            assert_nil @response.headers['X-S3-Storage-Url']
          end

          it 'responds with `200 OK`' do
            assert_response :ok
          end
        end
      end

      describe 'when the environment is not development' do
        let(:full_s3_url) do
          "https://zen-aws.com/data/rule_attachments/#{filename}"
        end

        before do
          Api::V2::Rules::MacroAttachmentsController.
            any_instance.
            stubs(:via_nginx?).
            returns(true)

          RuleAttachment.any_instance.
            stubs(:authenticated_s3_url).
            returns(full_s3_url)
        end

        describe 'and the attachment is an existing rule attachment' do
          before { get :content, params: { id: rule_attachment.id } }

          it 'sets the `X-Accel-Redirect` header' do
            assert_equal(
              "/s3/#{rule_attachment.full_filename}",
              @response.headers['X-Accel-Redirect']
            )
          end

          it 'sets the `X-S3-Storage-Url` header' do
            assert_equal full_s3_url, @response.headers['X-S3-Storage-Url']
          end

          it 'responds with `200 OK`' do
            assert_response :ok
          end

          describe_with_arturo_enabled :set_macro_attachment_content_disposition do
            before { get :content, params: { id: rule_attachment.id } }

            it 'sets the `Content-Type` header' do
              assert_equal 'image/png', @response.headers['Content-Type']
            end

            it 'sets the `Content-Disposition` header' do
              assert_equal(
                %(inline; filename="#{display_filename}"),
                @response.headers['Content-Disposition']
              )
            end

            it 'sets the `Content-Length` header' do
              assert_equal(
                rule_attachment['size'].to_s,
                @response.headers['Content-Length']
              )
            end
          end

          describe_with_arturo_disabled :set_macro_attachment_content_disposition do
            before { get :content, params: { id: rule_attachment.id } }

            it 'does not set any disposition-related headers' do
              refute @response.headers['Content-Disposition']
              refute @response.headers['Content-Length']
            end
          end

          describe 'and the attachment is not an image' do
            let(:filename)   { 'hello.html' }
            let(:attachment) { fixture_file_upload('hello.html') }

            it 'responds with `200 OK`' do
              assert_response :ok
            end
          end
        end

        describe 'and the attachment does not exist' do
          before do
            get :content, params: { id: 12345 }
          end

          it 'responds with `404 Not Found`' do
            assert_response :not_found
          end
        end
      end
    end
  end
end
