require_relative '../../../../../support/test_helper'
require_relative '../../../../../support/rule'
require_relative '../../../../../support/api_scopes_helper'
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/token_factory'
require 'json'

SingleCov.covered!

describe Api::V2::Rules::Categories::TriggerCategoriesController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  extend TestSupport::Rule::AdminRequest

  include TestSupport::Rule::Helper

  before do
    accept :json
    use_ssl
  end

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:category) { create_category }

  describe_with_arturo_setting_disabled :trigger_categories_api do
    as_an_admin do
      should_be_forbidden :show, :index, :create, :update, :jobs, :destroy
    end
  end

  describe_with_arturo_setting_enabled :trigger_categories_api do
    describe 'scopes' do
      with_scopes('triggers:read', 'read') do
        should_be_authorized { get :index }
      end

      with_scopes('triggers:read', 'read') do
        should_be_authorized { get :show, params: { id: category.id } }
      end

      with_scopes('triggers:write', 'write') do
        should_be_authorized { post :create, params: { trigger_category: { name: 'Hello World', position: 2 } } }
      end

      with_scopes('triggers:write', 'write') do
        should_be_authorized { post :jobs, params: { job: { action: 'patch', items: { trigger_categories: [{ id: category.id, position: 2 }] } } } }
      end
    end

    as_an_admin do
      before(:each) { RuleCategory.destroy_all }

      describe_with_arturo_setting_enabled :trigger_categories_api do
        describe '#index' do
          let(:trigger_categories) { JSON.parse(response.body).with_indifferent_access['trigger_categories'] }

          it 'lists trigger categories' do
            trigger_category = create_category

            get :index

            assert_response :success

            assert_equal 1, trigger_categories.size
            assert_equal trigger_category.id.to_s, trigger_categories.first[:id]
          end

          it 'does not list categories of other rule types' do
            non_trigger_category = RuleCategory.new(rule_type: 'BingBong', name: 'Hello World', position: 2, account: account).save(validate: false)

            get :index

            assert_response :success

            assert_equal 0, trigger_categories.size
            refute_includes trigger_categories, non_trigger_category
          end

          describe 'with sort parameters' do
            [
              {
                sort: nil,
                output: [
                  { name: 'Z', position: 1 },
                  { name: 'D', position: 2 },
                  { name: 'A', position: 3 }
                ]
              },
              {
                sort: 'position',
                output: [
                  { name: 'Z', position: 1 },
                  { name: 'D', position: 2 },
                  { name: 'A', position: 3 }
                ]
              },
              {
                sort: '-position',
                output: [
                  { name: 'A', position: 3 },
                  { name: 'D', position: 2 },
                  { name: 'Z', position: 1 }
                ]
              },
              {
                sort: 'name',
                output: [
                  { name: 'A', position: 3 },
                  { name: 'D', position: 2 },
                  { name: 'Z', position: 1 }
                ]
              },
              {
                sort: '-name',
                output: [
                  { name: 'Z', position: 1 },
                  { name: 'D', position: 2 },
                  { name: 'A', position: 3 }
                ]
              },
              {
                sort: 'created_at',
                output: [
                  { name: 'D', position: 2 },
                  { name: 'A', position: 3 },
                  { name: 'Z', position: 1 }
                ]
              },
              {
                sort: '-created_at',
                output: [
                  { name: 'Z', position: 1 },
                  { name: 'A', position: 3 },
                  { name: 'D', position: 2 }
                ]
              },
              {
                sort: 'updated_at',
                output: [
                  { name: 'A', position: 3 },
                  { name: 'Z', position: 1 },
                  { name: 'D', position: 2 }
                ]
              },
              {
                sort: '-updated_at',
                output: [
                  { name: 'D', position: 2 },
                  { name: 'Z', position: 1 },
                  { name: 'A', position: 3 }
                ]
              },
            ].each do |test_case|
              describe "when sorted by #{test_case[:sort]}" do
                before do
                  RuleCategory.destroy_all
                  category_1 = nil
                  category_2 = nil
                  category_3 = nil

                  Timecop.freeze(10.days.ago) { category_1 = create_category(name: 'D', position: 2) }
                  Timecop.freeze(9.days.ago) { category_2 = create_category(name: 'A', position: 3) }
                  Timecop.freeze(8.days.ago) { category_3 = create_category(name: 'Z', position: 1) }

                  Timecop.freeze(5.days.ago) { category_2.update!(active: false) }
                  Timecop.freeze(4.days.ago) { category_3.update!(active: false) }
                  Timecop.freeze(3.days.ago) { category_1.update!(active: false) }

                  get :index, params: { sort: test_case[:sort] }
                end

                after { Timecop.return }

                it 'returns the expected output' do
                  assert_equal test_case[:output], trigger_categories.map { |category| category.symbolize_keys.slice(:name, :position) }
                end
              end
            end
          end

          describe 'with page parameters' do
            let(:meta) { JSON.parse(response.body).with_indifferent_access['meta'] }
            let(:links) { JSON.parse(response.body).with_indifferent_access['links'] }

            before do
              RuleCategory.destroy_all

              create_category(name: 'Category 1', position: 2)
              create_category(name: 'Category 2', position: 3)
              create_category(name: 'Category 3', position: 1)

              get :index, params: { page: page_params }
            end

            describe 'when there is only one page' do
              let(:page_params) { { size: 4 } }

              it 'presents the correct keys' do
                refute meta[:has_more]
                assert meta[:before_cursor].present?
                assert meta[:after_cursor].present?
                assert links[:prev].present?
                assert links[:next].present?
              end

              it 'presents the correct number of trigger categories' do
                assert_equal RuleCategory.trigger_categories(account).size, trigger_categories.size
              end
            end

            describe 'when there are multiple pages' do
              let(:page_params) { { size: 1 } }

              it 'presents the correct keys' do
                assert meta[:has_more]
                assert meta[:before_cursor].present?
                assert meta[:after_cursor].present?
                assert links[:prev].present?
                assert links[:next].present?
              end

              it 'presents the correct number of trigger categories' do
                assert_equal 1, trigger_categories.size
              end
            end
          end

          describe 'with the `rule_counts` sideload' do
            let(:get_params) { { include: :rule_counts } }

            it 'returns correct trigger counts' do
              trigger_category = create_category

              create_trigger(rules_category_id: trigger_category.id, active: true)
              create_trigger(rules_category_id: trigger_category.id, active: true)
              create_trigger(rules_category_id: trigger_category.id, active: true)

              create_trigger(rules_category_id: trigger_category.id, active: false)
              create_trigger(rules_category_id: trigger_category.id, active: false)

              get :index, params: get_params

              assert 3, trigger_categories.first[:active_count]
              assert 2, trigger_categories.first[:inactive_count]
            end
          end
        end

        describe '#show' do
          it 'can show a trigger category' do
            id = category.id

            get :show, params: { id: id }

            assert_response :ok

            body = JSON.parse(response.body).with_indifferent_access

            assert_equal id.to_s, body.dig(:trigger_category, :id)
          end

          it 'handles nonexistent trigger category' do
            get :show, params: { id: 1234 }

            assert_trigger_category_not_found
          end
        end

        describe '#create' do
          it 'can create a trigger category' do
            assert_difference 'RuleCategory.count', 1 do
              post :create, params: { trigger_category: { name: 'Foo', position: 9 } }
            end

            assert_response :created

            body = JSON.parse(response.body).with_indifferent_access

            assert_equal 'Foo', body.dig(:trigger_category, :name)
            assert_equal 1, body.dig(:trigger_category, :position)
          end

          it 'returns the correct response for a missing trigger_category param' do
            assert_difference 'RuleCategory.count', 0 do
              post :create, params: { trigger: { name: 'Foo', position: 1 } }
            end

            assert_response :bad_request

            assert_equal({ errors: [{code: 'InvalidTriggerCategory', title: 'Parameter trigger_category is required.' }]}.to_json, response.body)
          end

          it 'returns the correct response for an invalid trigger category' do
            assert_difference 'RuleCategory.count', 0 do
              post :create, params: { trigger_category: { } }
            end

            assert_response :bad_request

            assert_equal({ errors: [{code: 'InvalidTriggerCategory', title: 'Name cannot be blank' }]}.to_json, response.body)
          end

          it 'reorders other categories' do
            category_1 = create_category(position: 1)
            category_2 = create_category(position: 2)
            post :create, params: { trigger_category: { name: 'Foo', position: 1 } }

            assert_response :created

            assert_equal 2, category_1.reload.position
            assert_equal 3, category_2.reload.position
          end
        end

        describe '#update' do
          let(:body) { JSON.parse(response.body).with_indifferent_access }

          before(:each) do
            4.times { create_category }
          end

          it 'can update a trigger category' do
            put :update, params: { id: category.id, trigger_category: { name: 'Kategory', position: 9 } }

            assert_response :ok
            assert_equal 'Kategory', body.dig(:trigger_category, :name)
            assert_equal 5, body.dig(:trigger_category, :position)
          end

          it 'repositions other categories' do
            put :update, params: { id: category.id, trigger_category: { position: 2 } }

            assert_response :ok
            assert_equal 2, body.dig(:trigger_category, :position)
            assert_equal (1..5).to_a, RuleCategory.trigger_categories(account).pluck(:position).sort
          end

          it 'returns the correct response for a missing trigger_category param' do
            put :update, params: { id: 1234, trigger: { name: 'Foo', position: 2 } }

            assert_response :bad_request

            assert_equal({ errors: [{code: 'InvalidTriggerCategory', title: 'Parameter trigger_category is required.' }]}.to_json, response.body)
          end

          it 'returns the correct response for an invalid trigger category' do
            put :update, params: { id: 1234, trigger_category: { name: 'Foo', position: 2 } }

            assert_trigger_category_not_found
          end
        end

        describe '#jobs' do
          let!(:category_a) { create_category(position: 1, name: 'A') }
          let!(:category_b) { create_category(position: 2, name: 'B') }
          let!(:category_c) { create_category(position: 3, name: 'C') }
          let!(:category_d) { create_category(position: 4, name: 'D') }
          let!(:trigger_1) { create_trigger(position: 1, title: '1', rules_category_id: category_a.id) }
          let!(:trigger_2) { create_trigger(position: 2, title: '2', rules_category_id: category_a.id) }
          let!(:trigger_3) { create_trigger(position: 3, title: '3', rules_category_id: category_a.id) }
          let!(:trigger_4) { create_trigger(position: 4, title: '4', rules_category_id: category_a.id) }
          let!(:trigger_5) { create_trigger(position: 2, title: '5', rules_category_id: category_a.id, active: false) }
          let!(:trigger_6) { create_trigger(position: 1, title: '6', rules_category_id: category_c.id) }
          let!(:trigger_7) { create_trigger(position: 2, title: '7', rules_category_id: category_c.id) }
          let!(:non_trigger_category) do
            RuleCategory.new(
              name: 'Other', position: 5, rule_type: 'View', account: account
            ).save!(validate: false)
          end
          let(:body) { JSON.parse(response.body).with_indifferent_access }
          let(:categories) { body.dig(:results, :trigger_categories) }
          let(:triggers) { body.dig(:results, :triggers) }
          let(:active_triggers) { Trigger.where(rules_category_id: category_a.id).active.order(:position) }

          before(:each) do
            post :jobs, params: { job: { action: 'patch', items: items } }
          end

          describe 'when updating categories and triggers' do
            let(:items) do
              {
                trigger_categories: [
                  { id: category_a.id, position: 2 },
                  { id: category_d.id, position: 3 }
                ],
                triggers: [
                  { id: trigger_1.id, position: 2, category_id: category_d.id },
                  { id: trigger_4.id, position: 3, category_id: category_b.id }
                ]
              }
            end

            it 'updates the values properly' do
              assert_response :ok

              assert_equal 'complete', body.dig(:status)
              assert_equal 2, categories.length
              assert_equal %w[A D], categories.map { |category| category[:name] }
              assert_equal [2, 3], categories.map { |category| category[:position] }
              assert_equal [1, 1], triggers.map { |trigger| trigger[:position] }
              assert_equal %W[#{category_b.id} #{category_d.id}], triggers.map { |trigger| trigger[:category_id] }
            end
          end

          describe 'when a trigger is deactivated' do
            let(:items) do
              {
                triggers: [
                  { id: trigger_2.id, active: false }
                ]
              }
            end

            it 'does not reposition active triggers' do
              assert_equal [1, 3, 4], active_triggers.map(&:position)
            end
          end

          describe 'when a trigger is reactivated' do
            describe 'and position is specified' do
              let(:items) do
                {
                  triggers: [
                    { id: trigger_5.id, active: true, position: 4 }
                  ]
                }
              end

              it 'honors the specified position value' do
                assert_equal 4, triggers.first[:position]
              end

              it 'repositions active triggers' do
                assert_equal (1..5).to_a, active_triggers.map(&:position)
                assert_equal 5, Trigger.find_by_id(trigger_4.id).position
              end
            end

            describe 'and position is not specified' do
              let(:items) do
                {
                  triggers: [
                    { id: trigger_5.id, active: true }
                  ]
                }
              end

              it 'uses the existing position value' do
                assert_equal 2, triggers.first[:position]
              end

              it 'repositions active triggers' do
                assert_equal (1..5).to_a, active_triggers.map(&:position)
                assert_equal %w[1 5 2 3 4], active_triggers.map(&:title)
              end
            end
          end

          describe 'when a trigger changes categories' do
            describe 'and position is specified' do
              let(:items) do
                {
                  triggers: [
                    { id: trigger_6.id, category_id: category_a.id, position: 1 }
                  ]
                }
              end

              it 'uses the given position value' do
                assert_equal 1, triggers.first[:position]
              end

              it 'repositions active triggers' do
                assert_equal (1..5).to_a, active_triggers.map(&:position)
                assert_equal %w[6 1 2 3 4], active_triggers.map(&:title)
              end
            end

            describe 'and position is not specified' do
              let(:items) do
                {
                  triggers: [
                    { id: trigger_6.id, category_id: category_a.id }
                  ]
                }
              end

              it 'defaults position to the end of the category' do
                assert_equal 5, triggers.first[:position]
              end

              describe 'and multiple triggers change to the same category' do
                let(:items) do
                  {
                    triggers: [
                      { id: trigger_7.id, category_id: category_a.id },
                      { id: trigger_6.id, category_id: category_a.id }
                    ]
                  }
                end

                it 'defaults their positions to the end of the category in list order' do
                  assert_equal 5, Trigger.find_by_id(trigger_7.id).position
                  assert_equal 6, Trigger.find_by_id(trigger_6.id).position
                end
              end
            end
          end

          describe 'when there are multiple updates with same position' do
            let(:items) do
              {
                trigger_categories: [
                  {id: category_c.id, position: 1},
                  {id: category_d.id, position: 1}
                ],
                triggers: [
                  {id: trigger_2.id, position: 1},
                  {id: trigger_3.id, position: 1}
                ]
              }
            end

            it 'only respects the last update with that position' do
              assert_equal %w[D C], categories.map { |category| category[:name] }
              assert_equal [1, 4], categories.map { |category| category[:position] }
              assert_equal %w[3 2], triggers.map { |trigger| trigger[:title] }
              assert_equal [1, 3], triggers.map { |trigger| trigger[:position] }
            end
          end

          describe 'when there are multiple updates with same ID' do
            let(:items) do
              {
                trigger_categories: [
                  {id: category_d.id, position: 3},
                  {id: category_d.id, position: 1},
                  {id: category_d.id, position: 2}
                ],
                triggers: [
                  {id: trigger_3.id, position: 3},
                  {id: trigger_3.id, position: 1},
                  {id: trigger_3.id, position: 2}
                ]
              }
            end

            it 'only respects the last update with that ID' do
              assert_equal %w[D], categories.map { |category| category[:name] }
              assert_equal [2], categories.map { |category| category[:position] }
              assert_equal %w[3], triggers.map { |trigger| trigger[:title] }
              assert_equal [2], triggers.map { |trigger| trigger[:position] }
            end
          end

          describe 'when the update position is out of bounds' do
            let(:items) do
              {
                trigger_categories: [
                  {id: category_a.id, position: -123},
                  {id: category_b.id, position: 10_000_000}
                ],
                triggers: [
                  {id: trigger_1.id, position: -123},
                  {id: trigger_2.id, position: 10_000_000}
                ]
              }
            end

            it 'adds those categories to the end in order of position' do
              assert_equal %w[A B], categories.map { |category| category[:name] }
              assert_equal [3, 4], categories.map { |category| category[:position] }
              assert_equal %w[1 2], triggers.map { |trigger| trigger[:title] }
              assert_equal [3, 4], triggers.map { |trigger| trigger[:position] }
            end
          end

          describe 'when there are updates outside of scope' do
            let(:items) do
              {
                trigger_categories: [
                  {id: RuleCategory.where(rule_type: 'View').first.id, position: 2},
                  {id: category_b.id, position: 3}
                ]
              }
            end

            it 'does not include them in result' do
              assert_equal %w[B], categories.map { |category| category[:name] }
              assert_equal [3], categories.map { |category| category[:position] }
              assert_equal 5, RuleCategory.where(rule_type: 'View').first.position
            end
          end
        end

        describe '#destroy' do
          it 'can destroy an existing trigger category' do
            category

            assert_difference 'RuleCategory.count', -1 do
              delete :destroy, params: { id: category.id }
            end

            assert_response :no_content
          end

          it 'deletes inactive triggers in category' do
            create_trigger(rules_category_id: category.id, active: false)
            create_trigger(rules_category_id: category.id, active: false)
            create_trigger(rules_category_id: category.id, active: false)

            delete :destroy, params: { id: category.id }

            assert_empty Trigger.where(account: account, rules_category_id: category.id)
          end

          it 'correctly re-orders categories after a delete' do
            create_category(position: 1)
            category2 = create_category(position: 2)
            create_category(position: 3)
            create_category(position: 4)

            delete :destroy, params: { id: category2.id }

            RuleCategory.all.each_with_index do |category, index|
              assert_equal index + 1, category.position
            end
          end

          it 'returns correct response when category contains active triggers' do
            create_trigger(rules_category_id: category.id)

            delete :destroy, params: { id: category.id }

            assert_response :bad_request

            assert_equal({ errors: [{ code: 'InvalidTriggerCategoryDeletion', title: 'A category with active triggers cannot be deleted.' }] }.to_json, response.body)
          end

          it 'returns correct response when trigger category is not found' do
            delete :destroy, params: { id: '12345' }

            assert_response :not_found

            assert_equal({ errors: [{
              code: 'TriggerCategoryNotFound',
              title: I18n.t('txt.admin.models.rules.rule_categories.trigger_category.does_not_exist')
            }] }.to_json, response.body)
          end
        end
      end
    end

    as_an_anonymous_user do
      should_be_unauthorized :index, :show, :create, :update, :jobs
    end

    as_an_end_user do
      should_be_forbidden :index, :show, :create, :update, :jobs
    end

    as_an_agent_with_permissions(business_rule_management: false) do
      should_be_forbidden :create, :update, :jobs, :destroy
    end

    as_an_agent_with_permissions(business_rule_management: true) do
      should_be_authorized { post :create, params: { trigger_category: { name: 'Hello World', position: 2 } } }
      should_be_authorized { patch :update, params: { id: category.id, trigger_category: { name: 'Updated' } } }
      should_be_authorized { post :jobs, params: { job: { action: 'patch', items: { trigger_categories: [{ id: category.id, position: 2 }] } } } }
      should_be_authorized { delete :destroy, params: { id: category.id } }
    end
  end

  private

  def assert_trigger_category_not_found
    assert_response :not_found
    assert_equal({ errors: [{
      code: 'TriggerCategoryNotFound',
      title: I18n.t('txt.admin.models.rules.rule_categories.trigger_category.does_not_exist')
    }] }.to_json, response.body)
  end
end
