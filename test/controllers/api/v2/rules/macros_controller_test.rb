require_relative '../../../../support/test_helper'
require_relative '../../../../support/rule'
require_relative '../../../../support/api_scopes_helper'

SingleCov.covered!

describe Api::V2::Rules::MacrosController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  extend TestSupport::Rule::AdminRequest

  include TestSupport::Rule::Helper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }

  let(:json) { JSON.parse(@response.body) }

  let(:macros) do
    json['macros'].map do |macro|
      {title: macro['title'], active: macro['active']}
    end
  end

  let(:valid_params) do
    {
      title:   'Test Macro',
      actions: [{field: 'status', value: 'open'}]
    }
  end

  before do
    accept :json
    use_ssl
  end

  with_options(controller: 'api/v2/rules/macros') do |request|
    [
      [:get,    '/api/v2/macros',              {action: :index}],
      [:get,    '/api/v2/macros/123',          {action: :show, id: 123}],
      [:get,    '/api/v2/macros/active',       {action: :active}],
      [:post,   '/api/v2/macros',              {action: :create}],
      [:put,    '/api/v2/macros/123',          {action: :update, id: 123}],
      [:put,    '/api/v2/macros/update_many',  {action: :update_many}],
      [:delete, '/api/v2/macros/123',          {action: :destroy, id: 123}],
      [:delete, '/api/v2/macros/destroy_many', {action: :destroy_many}],
      [:get,    '/api/v2/macros/123/apply',    {action: :apply, id: 123}],
      [
        :get,
        '/api/v2/tickets/1/macros/123/apply',
        {action: :apply, id: 123, ticket_id: 1}
      ],
      [:get,    '/api/v2/macros/categories',  {action: :categories}],
      [:get,    '/api/v2/macros/search',      {action: :search}],
      [:get,    '/api/v2/macros/actions',     {action: :actions}],
      [:get,    '/api/v2/macros/new',         {action: :new}],
      [:get,    '/api/v2/macros/groups',      {action: :groups}],
      [:get,    '/api/v2/macros/definitions', {action: :definitions}]
    ].each do |verb, path, options|
      request.should_route(verb, path, options)
    end
  end

  as_an_agent_with_permissions(macro_access: 'full') do
    let(:account) { accounts(:minimum) }

    records_admin_request(:index)  { get  :index }
    records_admin_request(:show)   { get  :show,   params: { id:    create_macro.id } }
    records_admin_request(:create) { post :create, params: { macro: valid_params } }

    records_admin_request(:update) do
      put :update, params: { id: create_macro.id, macro: {active: false} }
    end

    records_admin_request(:destroy) { delete :destroy, params: { id: create_macro.id } }

    records_admin_request(:update_many) do
      put :update_many, params: { macros: [{id: create_macro.id, active: false}] }
    end

    records_admin_request(:destroy_many) do
      delete :destroy_many, params: { ids: create_macro.id }
    end

    records_admin_request(:categories)  { get :categories }
    records_admin_request(:definitions) { get :definitions }
    records_admin_request(:groups)      { get :groups }

    describe "with sunset_macro_actions_endpoint disabled" do
      before { Arturo.disable_feature!(:sunset_macro_actions_endpoint) }
      records_admin_request(:actions) { get :actions }
    end
  end

  describe 'when using oauth tokens' do
    let(:macro) { rules :macro_incident_escalation }

    def http_get_search
      ZendeskSearch::Client.
        any_instance.
        stubs(search: {'results' => [{'id' => macro.id, 'type' => 'Macro'}], 'count' => 1})

      get :search, params: { query: 'test' }
    end

    with_scopes('macros:read', 'read') do
      should_be_authorized { get :index }
      should_be_authorized { get :active }
      should_be_authorized { get :show, params: { id: macro.id } }
      should_be_authorized { get :apply, params: { id: macro.id, ticket: ticket } }
      should_be_authorized { get :categories }
      should_be_authorized { get :new, params: { macro_id: macro.id } }
      should_be_authorized { get :groups }
      should_be_authorized { http_get_search }
      should_be_authorized { get :definitions }

      should_not_be_authorized { post :create, params: { macro: valid_params } }
      should_not_be_authorized { put :update, params: { id: macro.id, macro: {active: false} } }
      should_not_be_authorized { delete :destroy, params: { id: macro.id } }

      describe "with sunset_macro_actions_endpoint disabled" do
        before { Arturo.disable_feature!(:sunset_macro_actions_endpoint) }
        should_be_authorized { get :actions }
      end
    end

    with_scopes('macros:write', 'write') do
      should_be_authorized { post :create, params: { macro: valid_params } }
      should_be_authorized { put :update, params: { id: macro.id, macro: {active: false} } }
      should_be_authorized { put :update_many, params: { macros: [{id: macro.id, active: false}] } }
      should_be_authorized { delete :destroy, params: { id: macro.id } }
      should_be_authorized { delete :destroy_many, params: { ids: macro.id } }

      should_not_be_authorized { get :index }
      should_not_be_authorized { get :show, params: { id: macro.id } }
    end

    with_scopes('read', 'write') do
      should_be_authorized { get :active }
      should_be_authorized { get :apply, params: { id: macro.id, ticket: ticket } }
      should_be_authorized { get :categories }
      should_be_authorized { get :new, params: { macro_id: macro.id } }
      should_be_authorized { get :groups }
      should_be_authorized { http_get_search }

      describe "with sunset_macro_actions_endpoint disabled" do
        before { Arturo.disable_feature!(:sunset_macro_actions_endpoint) }
        should_be_authorized { get :actions }
      end
    end
  end

  describe 'per_page' do
    let(:params)   { {per_page: per_page} }
    let(:per_page) { nil }

    before { @controller.stubs(params: params) }

    it 'defaults to 100 records' do
      assert_equal 100, @controller.send(:per_page)
    end

    describe 'when `per page` is less than 1000' do
      describe 'and less than 100' do
        let(:per_page) { 50 }

        it 'is allowed' do
          assert_equal 50, @controller.send(:per_page)
        end
      end

      describe 'and greater than 100' do
        let(:per_page) { 700 }

        it 'is allowed' do
          assert_equal 700, @controller.send(:per_page)
        end
      end
    end

    describe 'when `per page` is greater than 1000' do
      let(:per_page) { 2000 }

      it 'caps at a max of 1000 records' do
        assert_equal 1000, @controller.send(:per_page)
      end
    end
  end

  describe 'for agents with `macro_access` permissions' do
    let(:agent)   { users(:minimum_agent) }
    let(:account) { agent.account }
    let(:group)   { groups(:minimum_group) }

    before do
      GroupMacro.destroy_all
      Macro.destroy_all

      Subscription.any_instance.stubs(has_group_rules?: true)

      create_macro(title: 'ZB', owner: account, active: false, position: 2)
      create_macro(title: 'ZA', owner: account, active: true,  position: 1)
      create_macro(title: 'CB', owner: group,   active: false, position: 6)
      create_macro(title: 'CA', owner: group,   active: true,  position: 5)
      create_macro(title: 'AB', owner: agent,   active: false, position: 4)
      create_macro(title: 'AA', owner: agent,   active: true,  position: 3)
    end

    as_an_agent_with_permissions(macro_access: 'full') do
      describe 'a GET to `index`' do
        before { get :index }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns macros based on permission' do
          assert_equal(
            [
              {title: 'AA', active: true},
              {title: 'AB', active: false},
              {title: 'CA', active: true},
              {title: 'CB', active: false},
              {title: 'ZA', active: true},
              {title: 'ZB', active: false}
            ],
            macros
          )
        end
      end

      describe 'a POST to `create`' do
        let(:macro) { json['macro'] }

        let(:request_body) do
          {
            title:       'Test Macro',
            actions:     [{field: 'status', value: 'open'}],
            description: 'Test macro description'
          }
        end

        before { GroupMacro.destroy_all }

        describe 'when creating a shared macro' do
          before do
            post(
              :create,
              params: {
                macro: {
                  title:       'Test Macro',
                  actions:     [{field: 'status', value: 'open'}],
                  description: 'Test macro description'
                }
              }
            )
          end

          should_use_presenter(
            Api::V2::Rules::MacroPresenter,
            status:   :created,
            location: %r{api/v2/macros/\d+\.json}
          )

          it 'does not associate a group with the macro' do
            refute GroupMacro.any?
          end

          it 'sets the title' do
            assert_equal 'Test Macro', macro['title']
          end

          it 'sets the description' do
            assert_equal 'Test macro description', macro['description']
          end

          it 'sets the actions' do
            assert_equal(
              [{'field' => 'status', 'value' => 'open'}],
              macro['actions']
            )
          end
        end

        describe 'when creating a group macro using `id`' do
          before do
            post(
              :create,
              params: {
                macro: {
                  title:       'Test Macro',
                  actions:     [{field: 'status', value: 'open'}],
                  restriction: {type: 'Group', id: group.id}
                }
              }
            )
          end

          it 'sets the owner' do
            assert(
              Macro.where(
                account_id: account.id,
                owner_id:   group.id,
                owner_type: 'Group',
                title:      'Test Macro'
              ).one?
            )
          end

          it 'associates the group with the macro' do
            assert(
              GroupMacro.where(
                account_id: account.id,
                group_id:   group.id,
                macro_id:   JSON.parse(@response.body)['macro']['id']
              ).one?
            )
          end
        end

        describe_with_arturo_enabled :apply_rules_table_limits do
          describe 'and the maximum limit for macros has not been reached' do
            before { post :create, params: { macro: request_body } }

            it('responds with created') { assert_response :created }
          end

          describe 'and the maximum limit for macros has been reached' do
            let(:limit) { 0 }

            before do
              Rule.any_instance.stubs(usage_limit: limit)

              post :create, params: { macro: request_body }
            end

            it('responds with forbidden') { assert_response :forbidden }

            it('includes an error message') do
              assert_equal(
                I18n.t(
                  'txt.admin.models.rules.rule.limit_exceeded',
                  limit: limit,
                  rule_type: I18n.t(
                    'txt.admin.models.rules.rule.rule_type.macros'
                  )
                ),
                JSON.parse(response.body)['errors'][0]['title']
              )
            end
          end
        end

        describe_with_arturo_disabled :apply_rules_table_limits do
          describe 'and the maximum limit for macros has been reached' do
            let(:limit) { 0 }

            before do
              Rule.any_instance.stubs(usage_limit: limit)

              post :create, params: { macro: request_body }
            end

            it('responds with created') { assert_response :created }
          end
        end

        describe 'when creating a group macro using `ids`' do
          let(:group_1) { account.groups.create!(name: '1', is_active: true) }
          let(:group_2) { account.groups.create!(name: '2', is_active: true) }

          before do
            post(
              :create,
              params: {
                macro: {
                  title:       'Test Macro',
                  actions:     [{field: 'status', value: 'open'}],
                  restriction: {
                    type: 'Group',
                    ids:  [group_1, group_2].map(&:id)
                  }
                }
              }
            )
          end

          should_use_presenter(
            Api::V2::Rules::MacroPresenter,
            status:   :created,
            location: %r{api/v2/macros/\d+\.json}
          )

          it 'associates the groups with the macro' do
            assert_equal(
              [group_1, group_2].map(&:id),
              GroupMacro.where(
                account_id: account.id,
                macro_id:   JSON.parse(@response.body)['macro']['id']
              ).pluck(:group_id)
            )
          end

          it 'presents the associated groups' do
            assert_equal(
              [group_1, group_2].map(&:id),
              JSON.parse(@response.body)['macro']['restriction']['ids']
            )
          end
        end
      end

      describe 'a PUT to `update`' do
        let(:macro) { Macro.where(title: 'AA').first }

        describe 'when updating ownership to a group using `id`' do
          before do
            put(
              :update,
              params: {
                id:    macro.id,
                macro: {restriction: {type: 'Group', id: group.id}}
              }
            )
            assert_response :ok, response.body
          end

          it 'sets the owner' do
            assert_equal group, macro.reload.owner
          end

          it 'associates the group with the macro' do
            assert(
              GroupMacro.where(
                account_id: account.id,
                group_id:   group.id,
                macro_id:   macro.id
              ).one?
            )
          end
        end

        describe 'when updating ownership to a group using `ids`' do
          let(:group_1) { account.groups.create!(name: '1', is_active: true) }
          let(:group_2) { account.groups.create!(name: '2', is_active: true) }

          before do
            put(
              :update,
              params: {
                id:    macro.id,
                macro: {
                  restriction: {
                    type: 'Group',
                    ids:  [group_1, group_2].map(&:id)
                  }
                }
              }
            )
            assert_response :ok, response.body
          end

          should_use_presenter Api::V2::Rules::MacroPresenter

          it 'associates the groups with the macro' do
            assert_equal(
              [group_1, group_2].map(&:id),
              GroupMacro.where(
                account_id: account.id,
                macro_id:   macro.id
              ).pluck(:group_id)
            )
          end
        end

        describe 'when updating a group macro' do
          let(:group_macro) { Macro.where(title: 'CA').first }

          describe 'and ownership changes' do
            before do
              put(
                :update,
                params: {
                  id:    group_macro.id,
                  macro: {
                    actions:     [{field: 'type', value: 'problem'}],
                    restriction: restriction
                  }
                }
              )
            end

            describe 'to the account' do
              let(:restriction) { nil }

              it 'removes the existing group associations' do
                refute GroupMacro.where(macro_id: group_macro.id).any?
              end
            end

            describe 'to a user' do
              let(:restriction) { {type: 'User', id: agent.id} }

              it 'removes the existing group associations' do
                refute GroupMacro.where(macro_id: group_macro.id).any?
              end
            end

            describe 'to a different group' do
              let(:group_2) do
                account.groups.create!(
                  name:      '2',
                  is_active: true
                )
              end

              let(:group_3) do
                account.groups.create!(
                  name:      '3',
                  is_active: true
                )
              end

              let(:restriction) do
                {type: 'Group', ids: [group_2, group_3].map(&:id)}
              end

              it 'associates the groups with the macro' do
                assert_equal(
                  [group_2, group_3].map(&:id),
                  GroupMacro.where(
                    account_id: account.id,
                    macro_id:   group_macro.id
                  ).pluck(:group_id)
                )
              end
            end
          end

          describe 'and ownership does not change' do
            before do
              put(
                :update,
                params: {
                  id:    group_macro.id,
                  macro: {
                    actions:     [{field: 'type', value: 'problem'}],
                    restriction: {type: 'Group', ids: [group.id]}
                  }
                }
              )
            end

            it 'does not change the existing group associations' do
              assert(
                GroupMacro.where(
                  account_id: account.id,
                  group_id:   group.id,
                  macro_id:   group_macro.id
                ).one?
              )
            end
          end

          describe 'without `groups_macros`' do
            before { GroupMacro.destroy_all }

            before do
              put(
                :update,
                params: {
                  id:    group_macro.id,
                  macro: {actions: [{field: 'type', value: 'problem'}]}
                }
              )
            end

            it 'does not build new group associations' do
              assert(
                GroupMacro.where(
                  account_id: account.id,
                  group_id:   group.id,
                  macro_id:   group_macro.id
                ).none?
              )
            end
          end
        end

        describe 'when updating the description' do
          before do
            put(
              :update,
              params: {
                id:    macro.id,
                macro: {description: 'Updated macro description'}
              }
            )
            assert_response :ok, response.body
          end

          should_use_presenter Api::V2::Rules::MacroPresenter

          it 'sets the description' do
            assert_equal 'Updated macro description', macro.reload.description
          end
        end
      end

      describe 'a PUT to `update_many`' do
        before do
          put :update_many, params: { macros: [
            {id: Macro.where(title: 'CA').first.id, position: 1, active: false},
            {id: Macro.where(title: 'ZB').first.id, position: 4, active: true},
            {id: Macro.where(title: 'ZA').first.id, position: 2},
            {id: Macro.where(title: 'AA').first.id, active: false}
          ]}
          assert_response :ok, response.body
        end

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'updates shared and personal macros' do
          assert_equal(
            [
              {title: 'AA', active: false, position: 3},
              {title: 'CA', active: false, position: 1},
              {title: 'ZA', active: true,  position: 2},
              {title: 'ZB', active: true,  position: 4}
            ],
            json['macros'].map do |macro|
              {
                title:    macro['title'],
                active:   macro['active'],
                position: macro['position']
              }
            end
          )
        end
      end

      describe 'a DELETE to `destroy_many`' do
        before do
          delete(
            :destroy_many,
            params: {
              ids: %w[ZB CB AB].map do |title|
                Macro.where(title: title).first.id
              end.join(',')
            }
          )
        end

        it 'responds with `204 No Content`' do
          assert_response :no_content
        end

        it 'soft-deletes the macros' do
          assert_equal(
            3,
            Macro.with_deleted do
              Macro.where(title: %w[ZB CB AB]).
                where('deleted_at IS NOT NULL').
                count
            end
          )
        end
      end
    end

    as_an_agent_with_permissions(macro_access: 'manage-group') do
      describe 'a GET to `index`' do
        before { get :index }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns macros based on permission' do
          assert_equal(
            [
              {title: 'AA', active: true},
              {title: 'AB', active: false},
              {title: 'CA', active: true},
              {title: 'CB', active: false},
              {title: 'ZA', active: true}
            ],
            macros
          )
        end
      end

      describe 'a PUT to `update_many`' do
        describe 'when the request includes shared macros' do
          before do
            put :update_many, params: { macros: [
              {id: Macro.where(title: 'CA').first.id, position: 1},
              {id: Macro.where(title: 'ZA').first.id, position: 6},
              {id: Macro.where(title: 'AA').first.id, position: 10}
            ]}
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end

        describe 'when the request includes group macros' do
          before do
            put :update_many, params: { macros: [
              {id: Macro.where(title: 'CA').first.id, active: false},
              {id: Macro.where(title: 'AA').first.id, active: false}
            ]}
          end

          it 'updates group and personal macros' do
            assert_equal(
              [
                {title: 'AA', active: false},
                {title: 'CA', active: false}
              ],
              json['macros'].map do |macro|
                {title: macro['title'], active: macro['active']}
              end
            )
          end
        end

        describe 'when the request only includes personal macros' do
          before do
            put :update_many, params: { macros: [
              {id: Macro.where(title: 'AA').first.id, position: 10},
              {id: Macro.where(title: 'AB').first.id, position: 8}
            ]}
          end

          should_use_presenter Api::V2::Rules::MacroPresenter

          it 'updates personal macros' do
            assert_equal(
              [
                {title: 'AA', position: 2},
                {title: 'AB', position: 1}
              ],
              json['macros'].map do |macro|
                {title: macro['title'], position: macro['position']}
              end
            )
          end
        end
      end

      describe 'a DELETE to `destroy_many`' do
        describe 'when the request includes shared macros' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[ZB CB AB].map do |title|
                  Macro.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'responds with `404 Not Found`' do
            assert_response :not_found
          end
        end

        describe 'when the request only includes group and personal macros' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[CB AB].map do |title|
                  Macro.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'soft-deletes the macros' do
            assert_equal(
              2,
              Macro.with_deleted do
                Macro.where(title: %w[CB AB]).
                  where('deleted_at IS NOT NULL').
                  count
              end
            )
          end
        end
      end
    end

    as_an_agent_with_permissions(macro_access: 'manage-personal') do
      describe 'a GET to `index`' do
        before { get :index }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns macros based on permission' do
          assert_equal(
            [
              {title: 'AA', active: true},
              {title: 'AB', active: false},
              {title: 'CA', active: true},
              {title: 'ZA', active: true}
            ],
            macros
          )
        end
      end

      describe 'a PUT to `update`' do
        describe 'when the macro is personal' do
          before do
            put(
              :update,
              params: {
                id:    Macro.where(owner_type: 'User', is_active: true).first.id,
                macro: {restriction: nil}
              }
            )
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end

        describe 'when the macro is shared' do
          before do
            put(
              :update,
              params: {
                id: Macro.where(owner_type: 'Account', is_active: true).first.id,
                macro: {restriction: {type: 'User', id: @user.id}}
              }
            )
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end
      end

      describe 'a PUT to `update_many`' do
        describe 'when the request includes shared macros' do
          before do
            put :update_many, params: { macros: [
              {id: Macro.where(title: 'CA').first.id, position: 1},
              {id: Macro.where(title: 'ZA').first.id, position: 6},
              {id: Macro.where(title: 'AA').first.id, position: 10}
            ]}
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end

        describe 'when the request includes group macros' do
          before do
            put :update_many, params: { macros: [
              {id: Macro.where(title: 'CA').first.id, active: false},
              {id: Macro.where(title: 'AA').first.id, active: false}
            ]}
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end

        describe 'when the request only includes personal macros' do
          before do
            put :update_many, params: {
              macros: [
                {id: Macro.where(title: 'AA').first.id, position: 10},
                {id: Macro.where(title: 'AB').first.id, position: 8}
              ]
            }
          end

          should_use_presenter Api::V2::Rules::MacroPresenter

          it 'updates personal macros' do
            assert_equal(
              [
                {title: 'AA', position: 2},
                {title: 'AB', position: 1}
              ],
              json['macros'].map do |macro|
                {title: macro['title'], position: macro['position']}
              end
            )
          end
        end
      end

      describe 'a DELETE to `destroy`' do
        describe 'when the macro is shared' do
          before do
            delete(
              :destroy,
              params: { id: Macro.where(owner_type: 'Account', is_active: true).first.id }
            )
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end
      end

      describe 'a DELETE to `destroy_many`' do
        describe 'when the request includes shared macros' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[ZB CB AB].map do |title|
                  Macro.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'responds with `404 Not Found`' do
            assert_response :not_found
          end
        end

        describe 'when the request includes group macros' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[CB AB].map do |title|
                  Macro.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'responds with `404 Not Found`' do
            assert_response :not_found
          end
        end

        describe 'when the request only includes personal macros' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[AB AA].map do |title|
                  Macro.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'soft-deletes the macros' do
            assert_equal(
              2,
              Macro.with_deleted do
                Macro.where(title: %w[AA AB]).
                  where('deleted_at IS NOT NULL').
                  count
              end
            )
          end
        end
      end
    end

    as_an_agent_with_permissions(macro_access: 'readonly') do
      describe 'a GET to `index`' do
        before { get :index }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns macros based on permission' do
          assert_equal(
            [
              {title: 'AA', active: true},
              {title: 'AB', active: false},
              {title: 'CA', active: true},
              {title: 'ZA', active: true}
            ],
            macros
          )
        end
      end

      describe 'a PUT to `update_many`' do
        describe 'when the request includes shared macros' do
          before do
            put :update_many, params: { macros: [
              {id: Macro.where(title: 'CA').first.id, position: 1},
              {id: Macro.where(title: 'ZA').first.id, position: 6},
              {id: Macro.where(title: 'AA').first.id, position: 10}
            ]}
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end

        describe 'when the request only includes personal macros' do
          before do
            put :update_many, params: { macros: [
              {id: Macro.where(title: 'AA').first.id, position: 10},
              {id: Macro.where(title: 'AB').first.id, position: 8}
            ]}
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end
      end

      describe 'a DELETE to `destroy_many`' do
        describe 'when the request includes shared macros' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[ZB CB AB].map do |title|
                  Macro.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'responds with `404 Not Found`' do
            assert_response :not_found
          end
        end

        describe 'when the request includes group macros' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[CB AB].map do |title|
                  Macro.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'responds with `404 Not Found`' do
            assert_response :not_found
          end
        end

        describe 'when the request only includes personal macros' do
          before do
            delete(
              :destroy_many,
              params: {
                ids: %w[AB AA].map do |title|
                  Macro.where(title: title).first.id
                end.join(',')
              }
            )
          end

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end
      end
    end
  end

  as_an_agent do
    let(:user)    { @user }
    let(:account) { user.account }
    let(:group)   { groups(:minimum_group) }

    let(:personal_macro) { rules(:macro_for_minimum_agent) }
    let(:shared_macro)   { rules(:macro_incident_escalation) }

    let(:ticket) { tickets(:minimum_1) }

    describe 'a GET to `index`' do
      before do
        Macro.destroy_all

        Subscription.any_instance.stubs(has_group_rules?: true)

        create_macro(title: 'ZB',     owner: account, active: false, position: 2)
        create_macro(title: 'ZA',     owner: account, active: true,  position: 1)
        create_macro(title: 'CAT::A', owner: account, active: true,  position: 7)
        create_macro(title: 'CB',     owner: group,   active: false, position: 6)
        create_macro(title: 'CA',     owner: group,   active: true,  position: 5)
        create_macro(title: 'AB',     owner: user,    active: false, position: 4)
        create_macro(title: 'AA',     owner: user,    active: true,  position: 3)
        create_macro(title: 'CAT::B', owner: user,    active: false, position: 8)
      end

      describe 'without parameters' do
        before { get :index }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns macros' do
          assert_equal(
            [
              {title: 'AA',     active: true},
              {title: 'AB',     active: false},
              {title: 'CA',     active: true},
              {title: 'CAT::A', active: true},
              {title: 'CAT::B', active: false},
              {title: 'ZA',     active: true}
            ],
            macros
          )
        end
      end

      describe 'with `access` parameter' do
        before { get :index, params: { access: 'personal' } }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns macros based on `access`' do
          assert_equal(
            [
              {title: 'AA',     active: true},
              {title: 'AB',     active: false},
              {title: 'CAT::B', active: false}
            ],
            macros
          )
        end
      end

      describe 'with `active` parameter' do
        before { get :index, params: { active: 'true' } }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns macros based on `active`' do
          assert_equal(
            [
              {title: 'AA',     active: true},
              {title: 'CA',     active: true},
              {title: 'CAT::A', active: true},
              {title: 'ZA',     active: true}
            ],
            macros
          )
        end
      end

      describe 'with `category` parameter' do
        before { get :index, params: { category: 'CAT' } }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns macros based on `category`' do
          assert_equal(
            [
              {title: 'CAT::A', active: true},
              {title: 'CAT::B', active: false}
            ],
            macros
          )
        end
      end

      describe 'with `only_viewable` parameter' do
        before { get :index, params: { only_viewable: true } }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns macros based on `only_viewable`' do
          assert_equal(
            [
              {title: 'AA',     active: true},
              {title: 'CA',     active: true},
              {title: 'CAT::A', active: true},
              {title: 'ZA',     active: true}
            ],
            macros
          )
        end
      end

      describe 'with `group_id` parameter' do
        before { get :index, params: { group_id: group.id } }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns macros based on `group_id`' do
          assert_equal [{title: 'CA', active: true}], macros
        end
      end

      describe 'with `sort_by` parameter' do
        before { get :index, params: { sort_by: 'position' } }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns macros sorted by that parameter' do
          assert_equal(
            [
              {title: 'ZA',     active: true},
              {title: 'CA',     active: true},
              {title: 'CAT::A', active: true},
              {title: 'AA',     active: true},
              {title: 'AB',     active: false},
              {title: 'CAT::B', active: false}
            ],
            macros
          )
        end
      end

      describe 'with `sort_order` parameter' do
        before { get :index, params: { sort_order: 'desc' } }

        it 'returns macros ordered by that parameter' do
          assert_equal(
            [
              {title: 'ZA',     active: true},
              {title: 'CAT::B', active: false},
              {title: 'CAT::A', active: true},
              {title: 'CA',     active: true},
              {title: 'AB',     active: false},
              {title: 'AA',     active: true}
            ],
            macros
          )
        end
      end

      describe 'and side-loading usage counts' do
        let(:side_loads) { %w[usage_1h usage_24h usage_7d usage_30d] }

        describe 'with the `rule_usage_stats` feature' do
          before do
            Account.any_instance.stubs(has_rule_usage_stats?: true)

            get :index, params: { include: side_loads.join(',') }
          end

          it 'includes the usage side-loads' do
            assert(
              side_loads.all? do |side_load|
                json['macros'].first.key?(side_load)
              end
            )
          end
        end

        describe 'without the `rule_usage_stats` feature' do
          before do
            Account.any_instance.stubs(has_rule_usage_stats?: false)

            get :index, params: { include: side_loads.join(',') }
          end

          it 'does not include the usage side-loads' do
            assert(
              side_loads.none? do |side_load|
                json['macros'].first.key?(side_load)
              end
            )
          end
        end
      end

      describe 'and side-loading other resources' do
        let(:side_loads) { %w[app_installation categories permissions] }

        before do
          Rule.stubs(:apps_installations).returns(
            Macro.all.first.id => {'id' => 42, 'settings' => {'title' => 'AAA'}}
          )

          get :index, params: { include: side_loads.join(',') }
        end

        it 'includes the appropriate item level side-loads' do
          assert(
            %w[app_installation permissions].all? do |side_load|
              json['macros'].first.key?(side_load)
            end
          )
        end

        it 'includes the appropriate collection level side-loads' do
          assert json.key?('categories')
        end
      end
    end

    it 'uses ETag' do
      assert_etagged :index do
        shared_macro.update_attribute(:updated_at, 1.minute.from_now)

        @controller.instance_variable_set(:@macros, nil)
      end
    end

    it 'changes ETag when content changes due to user change' do
      assert_etagged :index do
        @user.touch

        @controller.instance_variable_set(:@macros, nil)
      end
    end

    describe 'a GET to `show`' do
      before { get :show, params: { id: shared_macro.id } }

      should_use_presenter Api::V2::Rules::MacroPresenter
    end

    describe 'a GET to `active`' do
      before do
        GroupMacro.destroy_all
        Macro.destroy_all

        Subscription.any_instance.stubs(has_group_rules?: true)

        create_macro(title: 'ZB',     owner: account, active: false, position: 2)
        create_macro(title: 'ZA',     owner: account, active: true,  position: 1)
        create_macro(title: 'CAT::A', owner: account, active: true,  position: 7)
        create_macro(title: 'CB',     owner: group,   active: false, position: 6)
        create_macro(title: 'CA',     owner: group,   active: true,  position: 5)
        create_macro(title: 'AB',     owner: user,    active: false, position: 4)
        create_macro(title: 'AA',     owner: user,    active: true,  position: 3)
        create_macro(title: 'CAT::B', owner: user,    active: false, position: 8)
      end

      describe 'without parameters' do
        before { get :active }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns active macros' do
          assert_equal(
            [
              {title: 'AA',     active: true},
              {title: 'CA',     active: true},
              {title: 'CAT::A', active: true},
              {title: 'ZA',     active: true}
            ],
            macros
          )
        end
      end

      describe 'with `access` parameter' do
        before { get :active, params: { access: 'personal' } }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns active macros based on `access`' do
          assert_equal [{title: 'AA', active: true}], macros
        end
      end

      describe 'with `category` parameter' do
        before { get :active, params: { category: 'Cat' } }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns active macros based on `category`' do
          assert_equal [{title: 'CAT::A', active: true}], macros
        end
      end

      describe 'with `group_id` parameter' do
        before { get :active, params: { group_id: group.id } }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns all macros based on `group_id`' do
          assert_equal [{title: 'CA', active: true}], macros
        end
      end

      describe 'with `sort_by` parameter' do
        before { get :active, params: { sort_by: 'alphabetical' } }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns active macros sorted by that parameter' do
          assert_equal(
            [
              {title: 'AA',     active: true},
              {title: 'CA',     active: true},
              {title: 'CAT::A', active: true},
              {title: 'ZA',     active: true}
            ],
            macros
          )
        end
      end

      describe 'with `sort_order` parameter' do
        before { get :active, params: { sort_order: 'desc' } }

        should_use_presenter Api::V2::Rules::MacroPresenter

        it 'returns active macros ordered by that parameter' do
          assert_equal(
            [
              {title: 'ZA',     active: true},
              {title: 'CAT::A', active: true},
              {title: 'CA',     active: true},
              {title: 'AA',     active: true}
            ],
            macros
          )
        end
      end
    end

    describe 'a POST to `create`' do
      before { GroupMacro.destroy_all }

      describe 'when there is no `macro` key in payload' do
        before do
          login :minimum_admin
          post(
            :create,
            params: {
              title:   'Test Macro',
              actions: [{field: 'status', value: 'open'}]
            }
          )
        end

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe 'when creating a shared macro' do
        before do
          post(
            :create,
            params: {
              macro: {
                title: 'Test Macro',
                actions: [{field: 'status', value: 'open'}]
              }
            }
          )
        end

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe 'when creating a personal macro' do
        before do
          post(
            :create,
            params: {
              macro: {
              title: 'Test Macro',
              actions: [{
                  field: 'status',
                  value: 'open'
                }],
              restriction: { type: 'User', id: user.id }
              }
            }
          )
        end

        should_use_presenter(
          Api::V2::Rules::MacroPresenter,
          status:   :created,
          location: %r{api/v2/macros/\d+\.json}
        )

        it 'does not associate a group with the macro' do
          refute GroupMacro.any?
        end
      end

      describe 'when creating a macro with attachments' do
        let(:rule_attachments) do
          Array.new(2) do
            RuleAttachment.create! do |attachment|
              attachment.account_id   = account.id
              attachment.user_id      = user.id
              attachment.size         = 1
              attachment.content_type = 'application/unknown'
              attachment.filename     = 'hello_world'
            end
          end
        end

        before do
          post(
            :create,
            params: {
              macro: {
              title:       'Test Macro',
              actions:     [{field: 'comment_value_html', value: 'Nice tie!'}],
              restriction: {type: 'User', id: user.id},
              attachments: rule_attachments.map(&:id)
              }
            }
          )
          assert_response :created, response.body
        end

        it 'returns the attachment IDs' do
          assert_equal(
            rule_attachments.map(&:id),
            json['macro']['attachments']
          )
        end

        it 'associates the attachments with the macro' do
          assert_equal(
            rule_attachments,
            Macro.find(json['macro']['id']).attachments
          )
        end
      end

      describe 'when the number of attachments exceeds the limit' do
        let(:rule_attachments) do
          Array.new(6) do
            RuleAttachment.create! do |attachment|
              attachment.account_id   = account.id
              attachment.user_id      = user.id
              attachment.size         = 1
              attachment.content_type = 'application/unknown'
              attachment.filename     = 'hello_world'
            end
          end
        end

        before do
          post(
            :create,
            params: {
              macro: {
              title:       'Test Macro',
              actions:     [{field: 'comment_value_html', value: 'Nice tie!'}],
              restriction: {type: 'User', id: user.id},
              attachments: rule_attachments.map(&:id)
              }
            }
          )
        end

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe 'when a provided attachment does not exist' do
        before do
          post(
            :create,
            params: {
              macro: {
              title:       'Test Macro',
              actions:     [{field: 'comment_value_html', value: 'Nice tie!'}],
              restriction: {type: 'User', id: user.id},
              attachments: [123456789]
              }
            }
          )
        end

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe 'when creating an attachment macro without a rich comment' do
        let(:rule_attachments) do
          Array.new(2) do
            RuleAttachment.create! do |attachment|
              attachment.account_id   = account.id
              attachment.user_id      = user.id
              attachment.size         = 1
              attachment.content_type = 'application/unknown'
              attachment.filename     = 'hello_world'
            end
          end
        end

        before do
          post(
            :create,
            params: {
              macro: {
              title:       'Test Macro',
              actions:     [{field: 'status', value: 'open'}],
              restriction: {type: 'User', id: user.id},
              attachments: rule_attachments.map(&:id)
              }
            }
          )
        end

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end
    end

    describe 'a PUT to `update`' do
      describe 'when updating a shared macro' do
        before do
          put(
            :update,
            params: {
              id: shared_macro.id,
              macro: {
                actions: [{field: 'type', value: 'problem'}]
              }
            }
          )
        end

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe 'when updating a personal macro' do
        describe 'with valid params' do
          before do
            personal_macro.update_attributes!(is_active: false)

            put :update, params: { id: personal_macro.id, macro: params }
          end

          describe 'setting active' do
            let(:params) { {active: true} }

            it 'makes it active' do
              assert personal_macro.reload.is_active?
            end
          end

          describe 'with actions' do
            let(:params) do
              {actions: [{field: 'type', value: 'problem'}]}
            end

            it 'replaces the parameters' do
              assert_equal(
                params[:actions].map(&:stringify_keys),
                json['macro']['actions']
              )
            end
          end
        end

        describe 'when parameters are invalid' do
          before { put :update, params: { id: personal_macro.id, macro: {actions: []} } }

          should_use_presenter(
            Api::V2::ErrorsPresenter,
            status: :unprocessable_entity
          )
        end

        describe 'when the existing macro is invalid' do
          before do
            personal_macro.update_attribute(:definition, Definition.new)
          end

          describe 'and the macro was active' do
            describe 'and remains active' do
              before do
                put :update, params: { id: personal_macro.id, macro: {active: true} }
              end

              should_use_presenter(
                Api::V2::ErrorsPresenter,
                status: :unprocessable_entity
              )
            end

            describe 'and is deactivated' do
              describe 'and only `active` is specified' do
                before do
                  put(
                    :update,
                    params: {
                      id:    personal_macro.id,
                      macro: {active: false}
                    }
                  )
                  assert_response :ok, response.body
                end

                it 'updates the macro' do
                  refute personal_macro.reload.is_active?
                end
              end

              describe 'and other parameters are specified' do
                before do
                  put(
                    :update,
                    params: {
                      id:    personal_macro.id,
                      macro: {active: false, description: 'New description'}
                    }
                  )
                end

                should_use_presenter(
                  Api::V2::ErrorsPresenter,
                  status: :unprocessable_entity
                )
              end
            end
          end
        end
      end

      describe 'when associating attachments with a macro' do
        let(:actions) do
          [DefinitionItem.new('comment_value_html', nil, 'Nice tie!')]
        end

        let(:macro) do
          create_macro(
            title: 'Some::Macro',
            owner: user,
            definition: Definition.new.tap do |definition|
              definition.actions.push(*actions)
            end
          )
        end

        let(:attachments) do
          Array.new(2) do
            RuleAttachment.create! do |ra|
              ra.account_id   = account.id
              ra.user_id      = user.id
              ra.size         = 1
              ra.content_type = 'application/unknown'
              ra.filename     = 'hello_world'
            end
          end
        end

        describe 'and the macro has no previous attachments' do
          before do
            put :update,
              params: {
                id:    macro.id,
                macro: {attachments: attachments.map(&:id)}
              }
            assert_response :ok, response.body
          end

          it 'returns the attachment IDs' do
            assert_equal(
              attachments.map(&:id),
              json['macro']['attachments']
            )
          end

          it 'associates the macro with the attachments' do
            assert_equal attachments, macro.attachments
          end
        end

        describe 'and the macro has previous attachments' do
          before do
            attachments.first.associate(macro)
          end

          describe 'and the macro is associated with new attachments' do
            describe 'and previous attachments are included' do
              before do
                put :update,
                  params: {
                    id:    macro.id,
                    macro: {attachments: attachments.map(&:id)}
                  }
                assert_response :ok, response.body
              end

              it 'returns the attachment IDs' do
                assert_equal(
                  attachments.map(&:id),
                  json['macro']['attachments']
                )
              end

              it 'associates the macro with all provided attachments' do
                assert_equal attachments, macro.attachments
              end
            end

            describe 'and previous attachments are not included' do
              let(:new_attachments) { attachments.drop(1) }

              before do
                put :update,
                  params: {
                    id:    macro.id,
                    macro: {attachments: new_attachments.map(&:id)}
                  }
                assert_response :ok, response.body
              end

              it 'returns only the provided attachment IDs' do
                assert_equal(
                  new_attachments.map(&:id),
                  json['macro']['attachments']
                )
              end

              it 'associates the macro with only the provided attachments' do
                assert_equal new_attachments, macro.attachments.to_a
              end

              it 'dissociates previous attachments from the macro' do
                refute_includes macro.attachments, attachments.first
              end
            end
          end
        end

        describe 'and the provided attachments are empty' do
          before do
            put :update,
              params: {
                id:    macro.id,
                macro: {attachments: attachments}
              }
            assert_response :ok, response.body
          end

          describe 'and `nil` is passed' do
            let(:attachments) { nil }

            it 'does not present any attachments' do
              refute json['macro'].key?('attachments')
            end

            it 'no longer associates the macro with attachments' do
              assert macro.attachments.none?
            end
          end

          describe 'and an empty array is passed' do
            let(:attachments) { [] }

            it 'does not present any attachments' do
              refute json['macro'].key?('attachments')
            end

            it 'no longer associates the macro with attachments' do
              assert macro.attachments.none?
            end
          end
        end

        describe 'and the number of attachments exceeds the limit' do
          let(:rule_attachments) do
            Array.new(6) do
              RuleAttachment.create! do |ra|
                ra.account_id   = account.id
                ra.user_id      = user.id
                ra.size         = 1
                ra.content_type = 'application/unknown'
                ra.filename     = 'hello_world'
              end
            end
          end

          before do
            put :update,
              params: {
                id:    macro.id,
                macro: {attachments: rule_attachments.map(&:id)}
              }
            assert_response :bad_request
          end

          it 'does not change the attachments associated with the macro' do
            assert_equal [], macro.attachments
          end
        end

        describe 'and a provided attachment does not exist' do
          before do
            attachments.each { |a| a.associate(macro) }

            put :update,
              params: {
                id:    macro.id,
                macro: {attachments: [123456789, *attachments.map(&:id)]}
              }
            assert_response :bad_request
          end

          it 'does not change the attachments associated with the macro' do
            assert_equal attachments, macro.attachments
          end
        end

        describe 'and the macro does not have a rich comment action' do
          let(:actions) { [DefinitionItem.new('set_tags', nil, ['test'])] }

          before do
            attachments.each { |a| a.associate(macro) }

            put :update,
              params: {
                id:    macro.id,
                macro: {attachments: attachments.map(&:id)}
              }
            assert_response :bad_request
          end

          it 'does not change the attachments associated with the macro' do
            assert_equal attachments, macro.attachments
          end
        end
      end
    end

    describe 'a GET to `apply`' do
      describe 'with invalid `id`' do
        before { get :apply, params: { id: 999 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'with a valid `ticket_id`' do
        before { get :apply, params: { id: shared_macro.id, ticket_id: ticket.nice_id } }

        should_use_presenter Api::V2::Rules::MacroApplicationPresenter
      end

      describe 'with an invalid `ticket_id`' do
        before { get :apply, params: { id: shared_macro.id, ticket_id: 999999 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'without a `ticket_id`' do
        before { get :apply, params: { id: shared_macro.id } }

        should_use_presenter Api::V2::Rules::MacroApplicationPresenter
      end

      describe 'with a valid `ticket_id` and some ticket params' do
        let(:requester)  { users(:minimum_end_user) }
        let(:ticket_url) { ticket.url(for_agent: true) }

        let(:comment) { json['result']['ticket']['comment']['body'] }

        let(:expected_comment) do
          <<~COMMENT
            [Minimum account] minimum_end_user,

            Your request has been escalated to incident.

            Cheers,
            Agent Minimum,

            --------------------
            View status of your ticket at #{ticket_url}
          COMMENT
        end

        before do
          get(
            :apply,
            params: {
              id: shared_macro.id,
              ticket_id: ticket.nice_id,
              ticket: {
                requester_id: requester.id,
                requester_name: requester.name
              }
            }
          )
          assert_response :ok, response.body
        end

        should_use_presenter Api::V2::Rules::MacroApplicationPresenter

        it 'uses the new requester provided to evaluate comment placeholder' do
          assert_equal expected_comment.strip, comment
        end
      end
    end

    describe 'a GET to `categories`' do
      before do
        Macro.destroy_all

        create_macro(title: 'Beta',                 owner: user)
        create_macro(title: 'Beta::One',            owner: user)
        create_macro(title: 'Beta::Two',            owner: user)
        create_macro(title: 'Beta::Epsilon::Three', owner: user)
        create_macro(title: 'Theta::Iota::Four',    owner: user)
        create_macro(title: 'Zeta::Five',           owner: user)
        create_macro(title: 'Alpha::Six Seven',     owner: user)
        create_macro(title: 'Gamma Delta::Eight',   owner: user)
        create_macro(title: 'Nine',                 owner: user)
        create_macro(title: 'Eta::',                owner: user)
        create_macro(title: '::Ten',                owner: user)

        get :categories
      end

      it 'responds with `200 OK`' do
        assert_response :ok
      end

      it 'returns a list of macro categories available to the user' do
        assert_equal(
          [
            '',
            'Alpha',
            'Beta',
            'Eta',
            'Gamma Delta',
            'Theta',
            'Zeta'
          ],
          json['categories']
        )
      end

      describe 'and there are macros with categories owned by other users' do
        let(:admin) { users(:minimum_admin) }

        before do
          create_macro(title: 'Personal::Test', owner: admin)

          get :categories
        end

        it 'does not include those categories' do
          refute json['categories'].include?('Personal')
        end
      end
    end

    describe 'a GET to `search`' do
      before do
        GroupMacro.destroy_all
        Macro.destroy_all

        Subscription.any_instance.stubs(has_group_rules?: true)

        create_macro(title: 'ZB',     owner: account, active: false, position: 2)
        create_macro(title: 'ZA',     owner: account, active: true,  position: 1)
        create_macro(title: 'CAT::A', owner: account, active: true,  position: 7)
        create_macro(title: 'CB',     owner: group,   active: false, position: 6)
        create_macro(title: 'CA',     owner: group,   active: true,  position: 5)
        create_macro(title: 'AB',     owner: user,    active: false, position: 4)
        create_macro(title: 'AA',     owner: user,    active: true,  position: 3)
        create_macro(title: 'CAT::B', owner: user,    active: false, position: 8)
      end

      let(:macros) do
        json['macros'].map { |macro| {title: macro['title']} }
      end

      describe 'without `query` parameter' do
        before { get :search }

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe 'with `query` parameter' do
        let(:results) do
          [
            {'id' => Macro.find_by_title('AA').id,     'type' => 'Macro'},
            {'id' => Macro.find_by_title('CAT::A').id, 'type' => 'Macro'},
            {'id' => Macro.find_by_title('AB').id,     'type' => 'Macro'},
            {'id' => Macro.find_by_title('CA').id,     'type' => 'Macro'},
            {'id' => Macro.find_by_title('ZA').id,     'type' => 'Macro'},
            {'id' => Macro.find_by_title('CAT::B').id, 'type' => 'Macro'}
          ]
        end

        before do
          ZendeskSearch::Client.
            any_instance.
            stubs(search: {'results' => results, 'count' => results.size})
        end

        describe 'and no other parameters' do
          before do
            @controller.stubs(per_page: 5)

            get :search, params: { query: 'test' }
          end

          it 'returns the results' do
            assert_equal(
              [
                {title: 'AA'},
                {title: 'CAT::A'},
                {title: 'AB'},
                {title: 'CA'},
                {title: 'ZA'},
                {title: 'CAT::B'}
              ],
              macros
            )
          end

          it 'returns the `next_page` URL' do
            assert_equal(
              'https://minimum.zendesk-test.com/api/v2/macros/search.json' \
                '?page=2&per_page=5&query=test',
              json['next_page']
            )
          end

          it 'has no `previous_page` URL' do
            assert_nil json['previous_page']
          end

          it 'returns the count' do
            assert_equal results.size, json['count']
          end
        end

        describe 'and sorting parameters' do
          before { get :search, params: { query: 'A', sort_by: 'alphabetical' } }

          before_should 'pass through the query and sort options' do
            ZendeskSearch::Client.
              any_instance.
              expects(:search).
              with(
                'A order_by:title sort:asc',
                account_id:  account.id,
                endpoint:    'rule',
                from:        0,
                fq:          "((owner_type:Account AND is_active:true) OR " \
                             "(owner_type:Group AND group_ids:(66248) " \
                             "AND is_active:true) OR " \
                             "(owner_type:User AND owner_id:57888))",
                hl:          true,
                incremental: true,
                size:        100,
                type:        'macro'
              ).
              returns('results' => results, 'count' => results.size)
          end
        end

        describe 'and pagination parameters' do
          before { get :search, params: { query: 'test', page: 2, per_page: 1, only_viewable: true } }

          it 'returns the `next_page` URL' do
            assert_equal(
              'https://minimum.zendesk-test.com/api/v2/macros/search.json' \
                '?only_viewable=true&page=3&per_page=1&query=test',
              json['next_page']
            )
          end

          it 'returns the `previous_page` URL' do
            assert_equal(
              'https://minimum.zendesk-test.com/api/v2/macros/search.json' \
                '?only_viewable=true&page=1&per_page=1&query=test',
              json['previous_page']
            )
          end

          it 'returns the count' do
            assert_equal results.size, json['count']
          end
        end

        describe 'and side-loading usage counts' do
          let(:side_loads) { %w[usage_1h usage_24h usage_7d usage_30d] }

          describe 'with the `rule_usage_stats` feature' do
            before do
              Account.any_instance.stubs(has_rule_usage_stats?: true)

              get :index, params: { include: side_loads.join(',') }
            end

            it 'includes the usage side-loads' do
              assert(
                side_loads.all? do |side_load|
                  json['macros'].first.key?(side_load)
                end
              )
            end
          end

          describe 'without the `rule_usage_stats` feature' do
            before do
              Account.any_instance.stubs(has_rule_usage_stats?: false)

              get :index, params: { include: side_loads.join(',') }
            end

            it 'does not include the usage side-loads' do
              assert(
                side_loads.none? do |side_load|
                  json['macros'].first.key?(side_load)
                end
              )
            end
          end
        end

        describe 'and side-loading other resources' do
          let(:side_loads) { %w[app_installation permissions] }

          before do
            Rule.stubs(:apps_installations).returns(
              Macro.all.first.id => {
                'id'       => 42,
                'settings' => {'title' => 'AAA'}
              }
            )

            get :index, params: { include: side_loads.join(',') }
          end

          it 'includes the appropriate item level side-loads' do
            assert(
              %w[app_installation permissions].all? do |side_load|
                json['macros'].first.key?(side_load)
              end
            )
          end
        end
      end
    end

    describe 'a GET to `actions`' do
      before { get :actions }

      it 'responds with `410 Gone`' do
        assert_response :gone
      end

      describe "with sunset_macro_actions_endpoint disabled" do
        before { Arturo.disable_feature!(:sunset_macro_actions_endpoint) }
        before { get :actions }

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'does not include the `PICKACTION` action' do
          refute(
            json['actions'].any? do |action|
              action['value'] == 'PICKACTION'
            end
          )
        end
      end
    end

    describe 'a GET to `definitions`' do
      before { get :definitions }

      it 'responds with `200 OK`' do
        assert_response :ok
      end

      # We are seeing cms_text related N+1's in test, when the test account
      # does not have any Cms::Text's and the custom fields and custom field
      # options include dynamic content references, e.g. `{{dc.welcome_wombat}}`
      # where the identifier does not map to anything.
      it 'does not perform too many N+1 queries' do
        assert_n_plus_ones 2 do
          get :definitions
        end
      end

      should_use_presenter Api::V2::Rules::MacroDefinitionsPresenter
    end

    describe 'a GET to `new`' do
      let(:agent) { users(:minimum_agent) }
      let(:macro) { personal_macro }

      describe 'and no parameters are passed' do
        before { get :new }

        it 'responds with `400 Bad Request`' do
          assert_response :bad_request
        end
      end

      describe 'and both `ticket_id` and `macro_id` parameters are passed' do
        before { get :new, params: { ticket_id: ticket.id, macro_id: macro.id } }

        it 'responds with `200 OK`' do
          assert_response :ok
        end

        it 'returns a replica of the specified macro' do
          assert_equal macro.title, json['macro']['title']
        end
      end

      describe 'and the `macro_id` parameter is passed' do
        before { get :new, params: { macro_id: macro_id } }

        describe 'and the specified macro exists' do
          let(:macro_id) { macro.id }

          it 'responds with `200 OK`' do
            assert_response :ok
          end

          it 'returns a replica of the specified macro' do
            assert_equal macro.title, json['macro']['title']
          end
        end

        describe 'and the specified macro does not exist' do
          let(:macro_id) { 987654321 }

          it 'responds with `400 Bad Request`' do
            assert_response :bad_request
          end
        end
      end

      describe 'and the `ticket_id` parameter is passed' do
        let(:current_tags) do
          json['macro']['actions'].find do |action|
            action['field'] == 'current_tags'
          end
        end

        before do
          ticket.tap do |ticket|
            ticket.current_tags = 'ticket_tags'

            ticket.will_be_saved_by(agent)
          end.save!

          get :new, params: { ticket_id: nice_id }
        end

        describe 'and the specified ticket exists' do
          let(:nice_id) { ticket.nice_id }

          it 'responds with `200 OK`' do
            assert_response :ok
          end

          it 'returns a macro replica created from the specified ticket' do
            assert_equal 'ticket_tags', current_tags['value']
          end
        end

        describe 'and the specified ticket does not exist' do
          let(:nice_id) { 987654321 }

          it 'responds with `400 Bad Request`' do
            assert_response :bad_request
          end
        end
      end
    end

    describe 'a GET to `groups`' do
      let(:user) { users(:minimum_agent) }

      ('1'..'4').each do |i|
        let(:"group_#{i}") { account.groups.create!(name: i, is_active: true) }
      end

      let(:inactive_group) do
        account.groups.build(name: '5').tap do |group|
          group.is_active = false

          group.save!
        end
      end

      before do
        Macro.destroy_all
        Group.destroy_all
        GroupMacro.destroy_all

        create_macro(title: 'A', owner: account,        active: true,  position: 1)
        create_macro(title: 'B', owner: group_1,        active: false, position: 2)
        create_macro(title: 'C', owner: group_1,        active: true,  position: 3)
        create_macro(title: 'D', owner: user,           active: true,  position: 4)
        create_macro(title: 'E', owner: inactive_group, active: true,  position: 5)

        create_macro(
          title:    'F',
          owner:    [group_2, group_3],
          active:   true,
          position: 6
        )

        create_macro(
          title:    'G',
          owner:    group_4,
          active:   true,
          position: 7
        ).soft_delete

        get :groups
      end

      it 'responds with `200 OK`' do
        assert_response :ok
      end

      should_use_presenter Api::V2::GroupPresenter

      it 'returns active groups associated with an accessible macro' do
        assert_equal(
          [group_1, group_2, group_3].map(&:name),
          json['groups'].map { |group| group['name'] }
        )
      end
    end
  end
end
