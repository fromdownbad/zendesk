require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Rules::PreviewsController do
  extend Api::V2::TestHelper
  fixtures :accounts, :expirable_attachments, :users, :rules, :tickets

  let(:agent)         { users(:minimum_agent) }
  let(:another_agent) { users(:minimum_admin) }
  let(:account)       { agent.account }

  before do
    accept :json
    use_ssl

    @view = rules(:view_for_minimum_agent)

    stub_occam_find(Array(tickets(:minimum_1).id))
    stub_occam_count(0)
  end

  with_options(controller: "api/v2/rules/previews") do |request|
    request.should_route :post, "/api/v2/views/preview", action: "create"
    request.should_route :post, "/api/v2/views/preview/count", action: "count"
  end

  as_an_anonymous_user do
    should_be_unauthorized :create, :count
  end

  as_an_end_user do
    should_be_forbidden :create, :count
  end

  describe 'Views Rate Limits' do
    let(:account)       { accounts(:minimum) }
    let(:agent)         { users(:minimum_agent) }
    let(:another_agent) { users(:minimum_admin) }
    let(:statsd_client) { Zendesk::StatsD::Client.new(namespace: 'views') }
    let(:view_count) do
      stub('view_count', rule_id: @view.id, value: 1, pretty_print: '1', is_updating?: true)
    end
    let(:view_counts) { [view_count] }
    let(:span) { Object.new }
    let(:preview_definition) do
      {
        all: [
          { operator: "is", field: "status", value: "solved" },
          { operator: "is", field: "assignee_id", value: ["current_user"] }
        ],
        any: []
      }
    end
    let(:other_preview_definition) do
      { all: [{ operator: "is", field: "status", value: "solved" }] }
    end
    let(:request_type) { :post }
    let(:other_params) { { view: other_preview_definition } }
    let(:params) { { view: preview_definition } }
    let(:throttle_key) { [account.id, agent.id, @view.preview_id] }
    let(:another_throttle_key) { [account.id, another_agent.id, @view.preview_id] }

    behaves_like_rate_limited(:views_limit_preview_count_requests, threshold: 5) do
      let(:action) { :count }
      before { @controller.stubs(:view_count).returns(view_count) }
    end

    behaves_like_rate_limited(:views_limit_preview_find_requests, threshold: 5) do
      let(:action) { :create }
      before { @controller.stubs(:view_tickets).returns([]) }
    end
  end

  as_an_agent do
    let(:presented_columns) { json["columns"].map { |col| col["id"] } }
    let(:json) { JSON.parse(@response.body) }
    let(:pre_setup) {}

    describe "a POST to :create" do
      describe "with valid parameters" do
        before do
          post :create, params: { view: { all: [{ operator: "is", field: "status", value: "open" }] } }
        end

        should_use_presenter Api::V2::Rules::ViewRowsPresenter
      end

      describe "with invalid parameters" do
        before { post :create, params: { view: {} }, as: :json }
        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
      end

      describe "with partially valid parameters" do
        before do
          post :create, params: { view: { all: [{ operator: "is", field: "status", value: "open" }], any: nil } }
        end

        should_use_presenter Api::V2::Rules::ViewRowsPresenter
      end

      describe 'with disabled pagination' do
        before do
          Zendesk::Rules::RuleExecuter.any_instance.expects(:simulate_pagination).never
          post :create, params: { per_page: 1, paginate: false, view: { all: [{ operator: "is", field: "status", value: "open" }] } }
        end
        let(:json) { JSON.parse(response.body) }

        it 'does not count total tickets' do
          assert_equal json['count'], 1
        end

        it 'does not show next page' do
          assert_nil json['next_page']
        end
      end

      describe "with page out of range" do
        before do
          stub_occam_find([])

          post :create, params: { page: 100, view: { all: [{ operator: "is", field: "status", value: "open" }] } }
        end

        it('responds with not_found') { assert_response :not_found }
      end

      describe "with score field" do
        before do
          post :create, params: { view: { all: [{ operator: "is", field: "score", value: 42 }] } }
        end

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
      end

      describe "with an existing id" do
        before do
          pre_setup
          post :create, params: { view: { id: @view.id } }
        end

        should_use_presenter Api::V2::Rules::ViewRowsPresenter

        it "uses view output" do
          columns = ZendeskRules::RuleField.lookup(@view.output.columns).map(&:identifier).map(&:to_s)
          assert_equal columns, presented_columns
        end

        describe "with invalid view" do
          let(:pre_setup) { View.any_instance.stubs(:valid?).returns false }
          it("be unprocessable") { assert_response :unprocessable_entity }
        end
      end

      describe "with an existing id and new output" do
        let(:columns) { ["subject", "priority", "requester", "created", "group", "assignee", "updated"] }

        before do
          pre_setup
          post :create, params: { view: { id: @view.id, output: { columns: columns } } }
        end

        should_use_presenter Api::V2::Rules::ViewRowsPresenter

        it "uses specified columns" do
          columns.each do |column|
            assert_includes presented_columns, column
          end
        end

        describe "with a ticket with due_date" do
          let(:pre_setup) do
            ticket = tickets(:minimum_1)
            ticket.due_date = Time.now
            @controller.class.any_instance.expects(:view_tickets).returns [ticket]
          end

          describe "with due_date requested" do
            let(:columns) { ["due_date"] }

            it "presents due_date" do
              assert_includes presented_columns, "due_date"
              assert json["rows"].first["due_date"]
              assert json["rows"].first["due_at"]
            end
          end

          describe "with due_at requested" do
            let(:columns) { ["due_at"] }

            it "presents due_at" do
              # assert_includes presented_columns, "due_at" # needs a new rule-field-definition to work
              assert json["rows"].first["due_date"]
              assert json["rows"].first["due_at"]
            end
          end
        end
      end

      describe 'and occam returns a query timeout' do
        let(:occam_client) { Zendesk::Rules::OccamClient.any_instance }

        before do
          Account.any_instance.stubs(:has_occam_rule_execution?).returns(true)
          error = Occam::Client::QueryTimeoutError.new(StandardError.new)
          occam_client.expects(:find).raises(error)
        end

        describe_with_arturo_disabled :views_api_rescue_occam_errors do
          it 'reraises the error' do
            assert_raises(Occam::Client::QueryTimeoutError) do
              post :create, params: { view: { id: @view.id } }
            end
          end
        end

        describe_with_arturo_enabled :views_api_rescue_occam_errors do
          it 'does not raise an error and surfaces a JSON error to the client' do
            post :create, params: { view: { id: @view.id } }

            assert_response :service_unavailable
          end
        end
      end

      describe 'with latest_chat_message include param' do
        describe_with_arturo_disabled :preview_with_last_chat_message do
          it 'invokes `view_tickets`' do
            @controller.expects(:view_tickets).with(includes: [:latest_comment]).returns([])

            post :create, params: { view: { all: [{ operator: "is", field: "status", value: "open" }] }, include: 'latest_chat_message' }
          end
        end

        describe_with_arturo_enabled :preview_with_last_chat_message do
          it 'invokes `view_tickets`' do
            @controller.expects(:view_tickets).with(includes: [:latest_comment, :latest_chat_message]).returns([])

            post :create, params: { view: { all: [{ operator: "is", field: "status", value: "open" }] }, include: 'latest_chat_message' }
          end
        end
      end
    end

    describe "a POST to :count" do
      describe "with valid parameters" do
        before do
          post :count, params: { view: { all: [{ operator: "is", field: "status", value: "open" }] } }
        end

        should_use_presenter Api::V2::Rules::ViewCountPresenter
      end

      describe "with invalid parameters" do
        before { post :count, params: { view: {} }, as: :json }
        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
      end

      describe "with partially valid parameters" do
        before do
          post :count, params: { view: { all: [{ operator: "is", field: "status", value: "open" }], any: nil } }
        end

        should_use_presenter Api::V2::Rules::ViewCountPresenter
      end

      describe "with score field" do
        before do
          post :count, params: { view: { all: [{ operator: "is", field: "score", value: 42 }] } }
        end

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
      end

      describe "with an existing id" do
        before do
          pre_setup
          post :count, params: { view: { id: @view.id } }
        end

        should_use_presenter Api::V2::Rules::ViewCountPresenter

        describe "with invalid view" do
          let(:pre_setup) { View.any_instance.stubs(:valid?).returns false }
          it("be unprocessable") { assert_response :unprocessable_entity }
        end
      end
    end
  end

  as_a_subsystem_user(user: "zopim", account: :minimum) do
    should_be_forbidden :count

    describe "a POST to :create" do
      before { post :create, params: { view: { all: [{ operator: "is", field: "status", value: "open" }] } } }
      should_use_presenter Api::V2::Rules::ViewRowsPresenter
    end
  end

  describe 'views kill switch' do
    let(:account) { accounts(:minimum) }
    let(:agent)   { users(:minimum_agent) }
    let(:json)    { JSON.parse(response.body) }

    let(:view_params) do
      { view: { all: [{ operator: "is", field: "status", value: "open" }] } }
    end

    as_an_agent do
      describe_with_arturo_enabled :views_disable_all_features do
        %i[count create].each do |action|
          describe "when action - #{action} is invoked" do
            before do
              post action, params: view_params
            end

            it 'responds with 503' do
              assert_response :service_unavailable
            end

            it 'responds with proper body' do
              assert_equal json, {
                'error' => 'ServiceUnavailable',
                'description' => 'View service unavailable. Please try again later.'
              }
            end
          end
        end
      end

      describe_with_arturo_disabled :views_disable_all_features do
        %w[count create].each do |action|
          describe "when action - #{action} is invoked" do
            before do
              post action, params: view_params
            end

            it 'responds with 200' do
              assert_response :ok
            end
          end
        end
      end
    end
  end
end
