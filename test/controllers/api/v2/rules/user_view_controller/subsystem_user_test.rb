require_relative "../../../../../support/test_helper"
require_relative "../../../../../support/user_views_test_helper"
require_relative "../../../../../support/custom_fields_test_helper"
require_relative "../../../../../support/api_test_helper"

SingleCov.not_covered! # test was split into multiple parts because it took too long

describe Api::V2::Rules::UserViewsController do
  extend Api::V2::TestHelper
  include UserViewsTestHelper
  include CustomFieldsTestHelper
  include ApiTestHelper

  fixtures :all

  def create_custom_fields
    create_dropdown_field(
      "plan_type", "Plan",
      [
        { name: "Gold",   value: "gold" },
        { name: "Silver", value: "silver" },
        { name: "Bronze", value: "bronze" }
      ]
    )
    create_user_custom_field!("ltv", "LTV", "Decimal")
  end

  before do
    Account.any_instance.stubs(:is_serviceable?).returns(true)
    Account.any_instance.stubs(:has_user_views_negative_operators_enabled?).returns(true)
    accept :json
    use_ssl
  end

  as_a_subsystem_user(account: :minimum, user: 'zendesk_nps') do
    before do
      create_custom_fields
      Arturo.enable_feature! :user_views
      Arturo.enable_feature! :user_and_organization_tags
    end

    describe "and requesting views of a particular user" do
      before do
        get :index, params: { user_id: users(:minimum_agent).id }
        @user_view_ids = JSON.parse(response.body)['user_views'].map { |uv| uv['id'] }
      end

      it('responds with ok') { assert_response :ok }

      it "contains account views" do
        assert @user_view_ids.include?(rules(:global_user_view).id)
      end

      it "contains group views" do
        assert @user_view_ids.include?(rules(:minimum_group_view_alt).id)
      end

      it "contains personal views" do
        assert @user_view_ids.include?(rules(:minimum_custom_fields_view_alt).id)
      end
    end

    describe "without passing in a user_id parameter" do
      before do
        get :index
      end

      it('responds with forbidden') { assert_response :forbidden }
    end

    describe "a POST to :preview" do
      before do
        users(:minimum_admin).update_attribute(:created_at, 3.days.ago)
        users(:minimum_agent).update_attribute(:created_at, 1.days.ago)
        users(:minimum_end_user).update_attribute(:created_at, 2.days.ago)
      end

      describe "presentation" do
        def post_preview
          post :preview, params: { user_view: user_view }
        end

        let(:view) { rules(:minimum_custom_fields_view) }
        let(:user_view) do
          conditions_presenter = Api::V2::Rules::UserViewConditionsPresenter.new(users(:minimum_agent), url_builder: mock_url_builder)
          preview              = conditions_presenter.present(view.definition)[:conditions]
          preview[:execution]  = {
            group: view.output.group,
            sort: view.output.sort,
            columns: view.output.columns
          }
          preview
        end
        let(:presented_columns) { json["columns"].map { |col| col['id'].to_sym } }
        let(:json) { JSON.parse(@response.body) }

        before do
          users(:minimum_author).custom_field_values.set_field_value(cf_fields(:date1), Time.now).save
        end

        describe "simple request" do
          before { post_preview }
          should_use_presenter Api::V2::Rules::UserViewRowsPresenter, status: :ok
        end

        it "annotate the query with correct subsystem" do
          sql_queries { post_preview }.join("\n").must_match %r{/\*.*subsystem:zendesk_nps.*\*/}
        end

        it "does not sample customer list" do
          Zendesk::UserViews::Executer.expects(:users_for_view).
            with(anything, anything, has_entry(sampling: false)).
            returns([])
          post_preview
        end
      end
    end

    describe "pagination for many users" do
      before do
        FactoryBot.create_list(:end_user, 102, account: @account)
      end

      it "maxs per page should be larger than 100" do
        view = create_view setup_params(
          "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
          "execution" => {
            "columns" => ["id"],
            "sort" => {"id" => "created_at", "order" => "asc"}
          }
        )

        get :execute, params: { id: view.id, per_page: 101 }
        @result = JSON.parse(@response.body)

        assert_equal 101, @result['rows'].length
      end

      it "supports paging" do
        view = create_view setup_params(
          "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
          "execution" => {
            "columns" => ["id"],
            "sort" => {"id" => "created_at", "order" => "asc"}
          }
        )

        get :execute, params: { id: view.id, per_page: 101, page: 2 }
        @result = JSON.parse(@response.body)
        assert(@result['rows'].any?)
      end
    end
  end
end
