require_relative "../../../../../support/test_helper"
require_relative "../../../../../support/user_views_test_helper"
require_relative "../../../../../support/custom_fields_test_helper"
require_relative "../../../../../support/api_test_helper"

SingleCov.not_covered! # test was split into multiple parts because it took too long

describe Api::V2::Rules::UserViewsController do
  extend Api::V2::TestHelper
  include UserViewsTestHelper
  include CustomFieldsTestHelper
  include ApiTestHelper

  fixtures :all

  def create_custom_fields
    create_dropdown_field("plan_type", "Plan",
      [{ name: "Gold",   value: "gold" },
       { name: "Silver", value: "silver" },
       { name: "Bronze", value: "bronze" }])
    create_user_custom_field!("ltv", "LTV", "Decimal")
  end

  before do
    Account.any_instance.stubs(:is_serviceable?).returns(true)
    Account.any_instance.stubs(:has_user_views_negative_operators_enabled?).returns(true)
    accept :json
    use_ssl
  end

  as_an_end_user do
    describe "view don't belong to user" do
      let(:view) { rules(:minimum_custom_fields_view_alt) }
      before do
        create_custom_fields
        get :show, params: { id: view.id }
      end
      should_be_forbidden [:get, :show, { id: ActiveRecord::FixtureSet.identify(:minimum_custom_fields_view_alt) }]
    end

    describe "a GET to :export" do
      let(:user_view) { rules(:minimum_custom_fields_view_alt) }

      it "does not export user view" do
        get :export, params: { id: user_view.id }
        expected_error = {
          "error" => {
            "message" => "You do not have access to this page. Please contact the account owner of this help desk for further help.",
            "title" => "Forbidden"
          }
        }
        assert_equal expected_error, JSON.parse(@response.body)
      end
    end
  end
end
