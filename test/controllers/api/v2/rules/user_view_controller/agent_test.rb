require_relative "../../../../../support/test_helper"
require_relative "../../../../../support/user_views_test_helper"
require_relative "../../../../../support/custom_fields_test_helper"
require_relative "../../../../../support/api_test_helper"

SingleCov.not_covered! # test was split into multiple parts because it took too long

describe Api::V2::Rules::UserViewsController do
  extend Api::V2::TestHelper
  include UserViewsTestHelper
  include CustomFieldsTestHelper
  include ApiTestHelper

  fixtures :all

  def create_custom_fields
    create_dropdown_field("plan_type", "Plan",
      [{ name: "Gold",   value: "gold" },
       { name: "Silver", value: "silver" },
       { name: "Bronze", value: "bronze" }])
    create_user_custom_field!("ltv", "LTV", "Decimal")
  end

  before do
    Account.any_instance.stubs(:is_serviceable?).returns(true)
    Account.any_instance.stubs(:has_user_views_negative_operators_enabled?).returns(true)
    accept :json
    use_ssl
  end

  as_an_agent do
    before do
      create_custom_fields
      Account.any_instance.stubs(:has_user_views?).returns(true)
      Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
    end

    describe "a GET to :definitions when disabling arturos are enabled" do
      describe "when user_views_negative_operators_enabled is disabled" do
        before do
          Account.any_instance.stubs(:has_user_views_negative_operators_enabled?).returns(false)
          get :definitions
          @definitions = JSON.parse(@response.body)["definitions"]
        end

        it "does not contain negative operators" do
          UserView.user_attribute_definitions(@account).each do |attribute, definition|
            assert item = @definitions.find { |d| d["key"] == attribute }
            assert_equal item["operators"].map { |o| o["key"] }, definition["operators"].select { |o|
              ["is_not", "is_not_present", "not_includes"].exclude?(o)
            }
          end
        end
      end
    end

    describe "a GET to :definitions when user tags are disabled" do
      before do
        Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
        get :definitions
        @definitions = JSON.parse(@response.body)["definitions"]
      end

      it "does not contain user tags definition" do
        assert_nil @definitions.find { |d| d["key"] == 'current_tags' }
      end
    end

    describe "a GET to :definitions" do
      before do
        get :definitions
        @definitions = JSON.parse(@response.body)["definitions"]
      end

      it "contains groupable" do
        @definitions.each do |definition|
          key = definition['key']
          if UserView.valid_execution_attributes_by_type(@account)['group'].include?(key)
            assert definition['groupable']
          elsif ["system", "textarea"].exclude? definition['type']
            assert definition['groupable']
          else
            refute definition['groupable']
          end
        end
      end

      it "contains sortable" do
        @definitions.each do |definition|
          if definition['type'] == 'system'
            if UserView.valid_execution_attributes_by_type(@account)['sort'].include?(definition['key'])
              assert definition['sortable']
            else
              refute definition['sortable']
            end
          else
            assert definition['sortable']
          end
        end
      end

      it "contains system fields" do
        UserView.user_attribute_definitions(@account).each do |attribute, definition|
          assert item = @definitions.find { |d| d["key"] == attribute }
          assert_equal item["operators"].map { |o| o["key"] }, definition["operators"]
        end
      end

      it "includes columns without operators" do
        operators = 0
        no_operators = 0

        UserView.valid_execution_attributes(@account).each do |attribute|
          assert item = @definitions.find { |d| d["key"] == attribute }, "expected '#{attribute}'"
          if UserView.user_attribute_definitions(@account)[attribute]
            operators += 1 unless item["operators"].empty?
          else
            no_operators += 1 if item["operators"].empty?
          end
        end

        assert_equal 7, operators
        assert_equal 8, no_operators
      end

      it "contains custom fields" do
        @account.custom_fields.for_user.each do |field|
          assert item = @definitions.find { |d| d["key"] == "custom_fields.#{field['key']}" }
          assert_equal item["operators"].map { |o| o["key"] }, UserView::CUSTOM_FIELD_OPERATORS[field.type]
        end
      end

      it "contains dropdown choices" do
        assert dropdown = @account.custom_fields.where(type: "Dropdown").last
        assert dropdown.custom_field_options.size > 1
        assert item = @definitions.find { |d| d["key"] == "custom_fields.#{dropdown['key']}" }
        assert_equal item["custom_field_options"], dropdown.custom_field_options.map { |option|
          { "id" => option.id, "name" => option.name, "value" => option.value }
        }
      end

      it 'does not blow up when dropdown choices have been deleted' do
        assert dropdown = @account.custom_fields.where(type: "Dropdown").last
        assert (size = dropdown.custom_field_options.size) > 1
        dropdown.custom_field_options.last.soft_delete!
        get :definitions
        @definitions = JSON.parse(@response.body)["definitions"]

        assert item = @definitions.find { |d| d["key"] == "custom_fields.#{dropdown['key']}" }
        assert_equal item["custom_field_options"].size, size - 1
        assert_equal item["custom_field_options"], dropdown.custom_field_options.map { |option|
          { "id" => option.id, "name" => option.name, "value" => option.value }
        }
      end

      it "contains the right operators for system dates" do
        assert item = @definitions.find { |d| d["key"] == "created_at" }
        UserView::SPECIAL_CASE_TITLES["date"].each do |key, title|
          assert op = item['operators'].find { |o| o['key'] == key }
          assert_equal op['title'], I18n.t("txt.admin.models.rules.rule_dictionary.#{title}_label")
        end

        UserView::SPECIAL_CASE_TYPES.each do |key, title|
          if key == "within_next_n_days"
            refute item['operators'].find { |o| o['key'] == key }
          else
            assert op = item['operators'].find { |o| o['key'] == key }
            assert_equal op['type'], title
          end
        end
      end

      it "contains the right operators for custom dates" do
        assert date = @account.custom_fields.find_by_type("Date")
        assert item = @definitions.find { |d| d["key"] == "custom_fields.#{date['key']}" }
        UserView::SPECIAL_CASE_TITLES["date"].each do |key, title|
          assert op = item['operators'].find { |o| o['key'] == key }
          assert_equal op['title'], I18n.t("txt.admin.models.rules.rule_dictionary.#{title}_label")
        end

        UserView::SPECIAL_CASE_TYPES.each do |key, title|
          assert op = item['operators'].find { |o| o['key'] == key }
          assert_equal op['type'], title
          assert_equal op['label'], "days"
        end
      end

      it "renders dynamic content properly" do
        assert dropdown = @account.custom_fields.where(type: "Dropdown").last
        dropdown.title = "{{dc.title}}"
        dropdown.save
        dropdown.custom_field_options.first.name = "{{dc.dropdown_choice}}"
        dropdown.custom_field_options.first.save
        Zendesk::Liquid::DcContext.stubs(:render).returns('rendered_value')
        Zendesk::Liquid::DcContext.expects(:render).with("{{dc.title}}", @account, @user, 'text/plain', nil, {}).returns('title')
        Zendesk::Liquid::DcContext.expects(:render).with("{{dc.dropdown_choice}}", @account, @user, 'text/plain', nil, {}).returns('dropdown')
        get :definitions
        @definitions = JSON.parse(@response.body)["definitions"]
        assert item = @definitions.find { |d| d["key"] == "custom_fields.#{dropdown['key']}" }
        assert_equal "title", item["title"]
        assert_equal "dropdown", item["custom_field_options"][0]["name"]
      end
    end

    describe "a GET to :index" do
      it "lists view" do
        view = create_view setup_params(all_conditions)
        get :index
        data = JSON.parse(@response.body)["user_views"]
        assert !data.empty?
        assert data.find { |u| u['id'] == view.id }
      end

      it "lists view restricted to user" do
        view = create_view setup_params(restricted_to_agent(@user.id))
        get :index
        data = JSON.parse(@response.body)["user_views"]
        assert data.find { |u| u['id'] == view.id }
      end

      it "lists shared (account, group) and personal views in order" do
        UserView.without_arsi.delete_all

        personal_1 = create_view setup_params(restricted_to_agent(@user.id).merge("title" => "Personal 1"))

        @controller.instance_variable_set(:@initializer, nil)
        shared_1 = create_view setup_params(all_conditions.merge("title" => "Shared 1"))

        # inactive, shared view
        @controller.instance_variable_set(:@initializer, nil)
        create_view setup_params(all_conditions.merge("active" => false, "title" => "Inactive Shared 2"))

        @controller.instance_variable_set(:@initializer, nil)
        shared_3 = create_view setup_params(all_conditions.merge("title" => "Shared 3"))

        @controller.instance_variable_set(:@initializer, nil)
        shared_4 = create_view setup_params(all_conditions.merge("title" => "Shared 4"))

        @controller.instance_variable_set(:@initializer, nil)
        group_shared_1 = create_view setup_params(restricted_to_group(groups(:minimum_group).id).merge("title" => "Group Shared 1"))

        UserView.update_position([shared_3.id, shared_1.id, group_shared_1.id, shared_4.id])

        @controller.instance_variable_set(:@initializer, nil)
        create_view setup_params(restricted_to_agent(users(:minimum_admin_not_owner).id))

        get :index

        data = JSON.parse(@response.body)["user_views"]
        shared_views = [shared_3, shared_1, group_shared_1, shared_4]
        personal_views = [personal_1]
        expected_views = (shared_views + personal_views).map(&:title)
        actual_views = data.map { |r| r['title'] }
        assert_equal expected_views, actual_views
      end

      it "orders by title if, by some freak accident, position is the same" do
        UserView.without_arsi.delete_all

        @controller.instance_variable_set(:@initializer, nil)
        shared_1 = create_view setup_params(all_conditions.merge("title" => "Shared B"))

        @controller.instance_variable_set(:@initializer, nil)
        shared_2 = create_view setup_params(all_conditions.merge("title" => "Shared A"))

        @controller.instance_variable_set(:@initializer, nil)
        shared_3 = create_view setup_params(all_conditions.merge("title" => "Shared C"))

        UserView.without_arsi.update_all(position: 10000)

        get :index

        data = JSON.parse(@response.body)["user_views"]
        expected_views = [shared_2, shared_1, shared_3].map(&:title)
        actual_views = data.map { |r| r['title'] }
        assert_equal expected_views, actual_views
      end

      it "does not list view restricted to another user" do
        assert_not_equal @user.id, users(:minimum_admin_not_owner).id
        view = create_view setup_params(restricted_to_agent(users(:minimum_admin_not_owner).id))
        get :index
        data = JSON.parse(@response.body)["user_views"]
        refute data.find { |u| u['id'] == view.id }
      end

      it "does not list inactive shared views" do
        UserView.without_arsi.delete_all

        create_view setup_params(all_conditions.merge("title" => "Shared 1"))

        @controller.instance_variable_set(:@initializer, nil)
        create_view setup_params(all_conditions.merge("active" => false, "title" => "Inactive Shared 2"))

        @controller.instance_variable_set(:@initializer, nil)
        create_view setup_params(restricted_to_group(groups(:minimum_group).id).merge("active" => false, "title" => "Inactive Group Shared 1"))

        get :index

        data = JSON.parse(@response.body)["user_views"]
        actual_views = data.map { |r| r['title'] }
        assert_equal ['Shared 1'], actual_views
      end

      it 'does not blow up with a view where the language has been deleted' do
        locale = TranslationLocale.find_by_locale("ja")
        view_with_removed_locale(locale)
        get :index
        assert_response 200
      end

      it 'does not blow up with a view where the organization has been deleted' do
        @organization = @account.organizations.create(name: "Org")
        create_view setup_params("all" => [{ "field" => "organization_id", "operator" => "is", "value" => @organization.id}])
        @organization.destroy
        get :index
        assert_response 200
      end

      describe "and has full enterprise permissions to manage user views" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          Subscription.any_instance.stubs(:has_group_rules?).returns(true)
          @permission_set = build_valid_permission_set
          @permission_set.permissions.user_view_access = "full"
          User.any_instance.stubs(:permission_set).returns(@permission_set)
          assert @user.can?(:manage_shared, UserView)
        end

        it "lists all account and group views, as well as and personal views" do
          UserView.without_arsi.delete_all

          post :create, params: { user_view: setup_params(restricted_to_agent(@user.id).merge("title" => "Personal 1")) }
          personal_1 = JSON.parse(@response.body)["user_view"]

          @controller.instance_variable_set(:@initializer, nil)
          post :create, params: { user_view: setup_params(all_conditions.merge("title" => "Shared 1")) }
          shared_1 = JSON.parse(@response.body)["user_view"]

          @controller.instance_variable_set(:@initializer, nil)
          post :create, params: { user_view: setup_params(all_conditions.merge("active" => false, "title" => "Shared 2")) }
          shared_2 = JSON.parse(@response.body)["user_view"]

          @controller.instance_variable_set(:@initializer, nil)
          post :create, params: { user_view: setup_params(all_conditions.merge("title" => "Shared 3")) }
          shared_3 = JSON.parse(@response.body)["user_view"]

          @controller.instance_variable_set(:@initializer, nil)
          post :create, params: { user_view: setup_params(restricted_to_group(groups(:minimum_group).id).merge("title" => "Group Shared 1")) }
          group_shared_1 = JSON.parse(@response.body)["user_view"]
          UserView.update_position([shared_3['id'], shared_1['id'], group_shared_1['id'], shared_2['id']])

          @controller.instance_variable_set(:@initializer, nil)
          post :create, params: { user_view: setup_params(restricted_to_agent(users(:minimum_admin_not_verified).id)) }

          get :index

          data = JSON.parse(@response.body)["user_views"]
          shared_views = [shared_3, shared_1, group_shared_1, shared_2]
          personal_views = [personal_1]
          expected_views = (shared_views + personal_views).map { |r| r['title'] }
          actual_views = data.map { |r| r['title'] }
          assert_equal expected_views, actual_views
        end
      end

      describe "and has enterprise permission to manage group views" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          Subscription.any_instance.stubs(:has_group_rules?).returns(true)
          @permission_set = build_valid_permission_set
          @permission_set.permissions.user_view_access = "manage-group"
          User.any_instance.stubs(:permission_set).returns(@permission_set)
          refute @user.can?(:manage_shared, UserView)
        end

        it "lists active global views; active and inactive group views; and all personal views" do
          UserView.without_arsi.delete_all

          personal_1 = create_view setup_params(restricted_to_agent(@user.id).merge("title" => "Personal 1"))

          @controller.instance_variable_set(:@initializer, nil)
          shared_1 = create_view setup_params(all_conditions.merge("title" => "Shared 1"))

          # inactive, shared view
          @controller.instance_variable_set(:@initializer, nil)
          create_view setup_params(all_conditions.merge("active" => false, "title" => "Inactive Shared 2"))

          @controller.instance_variable_set(:@initializer, nil)
          shared_3 = create_view setup_params(all_conditions.merge("title" => "Shared 3"))

          @controller.instance_variable_set(:@initializer, nil)
          group_shared_1 = create_view setup_params(restricted_to_group(groups(:minimum_group).id).merge("title" => "Group Shared 1"))

          # inactive, group view
          @controller.instance_variable_set(:@initializer, nil)
          group_shared_2 = create_view setup_params(restricted_to_group(groups(:minimum_group).id).merge("active" => false, "title" => "Inactive Group Shared 2"))

          # another agent's personal view
          @controller.instance_variable_set(:@initializer, nil)
          create_view setup_params(restricted_to_agent(users(:minimum_admin_not_owner).id))

          UserView.update_position([shared_3.id, shared_1.id, group_shared_1.id, group_shared_2.id])

          get :index

          data = JSON.parse(@response.body)["user_views"]
          shared_views = [shared_3, shared_1, group_shared_1, group_shared_2]
          personal_views = [personal_1]
          expected_views = (shared_views + personal_views).map(&:title)
          actual_views = data.map { |r| r['title'] }
          assert_equal expected_views, actual_views
        end
      end

      describe "when agent has readonly permissions" do
        before do
          create_view setup_params(all_conditions)

          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          @permission_set = build_valid_permission_set
          @permission_set.permissions.user_view_access = "readonly"
          User.any_instance.stubs(:permission_set).returns(@permission_set)

          get :index
        end

        it('responds with ok') { assert_response :ok }
      end

      describe "when agent has no permissions" do
        before do
          create_view setup_params(all_conditions)

          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          @permission_set = build_valid_permission_set
          @permission_set.permissions.available_user_lists = "none"
          User.any_instance.stubs(:permission_set).returns(@permission_set)

          get :index
        end

        it('responds with forbidden') { assert_response :forbidden }
      end
    end

    describe "a GET to :show" do
      describe "for a non-existent view" do
        before do
          get :show, params: { id: 9999999999 }
        end

        it('responds with not_found') { assert_response :not_found }
      end

      it "shows active view" do
        view = create_view setup_params(all_conditions)
        get :show, params: { id: view.id }
        assert_equal view.id, JSON.parse(@response.body)['user_view']['id']
      end

      it "shows inactive view" do
        view = create_view setup_params(all_conditions.merge("active" => false))
        get :show, params: { id: view.id }
        assert_equal view.id, JSON.parse(@response.body)['user_view']['id']
      end

      it "shows view restricted to user" do
        view = create_view setup_params(restricted_to_agent(@user.id))
        get :show, params: { id: view.id }
        assert_equal view.id, JSON.parse(@response.body)['user_view']['id']
      end

      it "does not show view restricted to other user" do
        assert_not_equal @user.id, users(:minimum_admin_not_owner).id
        view = create_view setup_params(restricted_to_agent(users(:minimum_admin_not_owner).id))
        get :show, params: { id: view.id }
        expected_error = {
          "error" => {
            "message" => "You do not have access to this page. Please contact the account owner of this help desk for further help.",
            "title" => "Forbidden"
          }
        }
        assert_equal expected_error, JSON.parse(@response.body)
      end

      describe "when view contains an organization condition" do
        it "includes label with condition" do
          org = Organization.first
          org_condition = { 'field' => 'organization_id', 'operator' => 'is', 'value' => org.id.to_s }
          options = all_conditions
          options['all'] << org_condition
          view = create_view setup_params(options)
          get :show, params: { id: view.id }
          definition = view.definition.conditions_all[1]
          assert_equal definition.source, 'organization_id'
          assert_equal definition.operator, 'is'
          assert_equal definition.value, org.id.to_s
        end
      end
    end

    describe "with a view" do
      let(:view) { rules(:minimum_custom_fields_view) }
      before { get :show, params: { id: view.id } }
      should_use_presenter Api::V2::Rules::UserViewPresenter, status: :ok
    end

    describe "a POST to :preview" do
      before do
        @user_a = users(:minimum_admin)
        @user_b = users(:minimum_agent)
        @user_c = users(:minimum_end_user)
        @user_a.update_attribute(:created_at, 3.days.ago)
        @user_b.update_attribute(:created_at, 1.days.ago)
        @user_c.update_attribute(:created_at, 2.days.ago)
      end

      describe "presentation" do
        let(:view) { rules(:minimum_custom_fields_view) }
        let(:user_view) do
          conditions_presenter = Api::V2::Rules::UserViewConditionsPresenter.new(users(:minimum_agent), url_builder: mock_url_builder)
          preview = conditions_presenter.present(view.definition)[:conditions]
          preview[:execution] = {
            group: view.output.group,
            sort: view.output.sort,
            columns: view.output.columns
          }
          preview
        end
        let(:presented_columns) { json["columns"].map { |col| col['id'].to_sym } }
        let(:json) { JSON.parse(@response.body) }

        before do
          users(:minimum_author).custom_field_values.set_field_value(cf_fields(:date1), Time.now).save
          post :preview, params: { user_view: user_view }
        end

        should_use_presenter Api::V2::Rules::UserViewRowsPresenter, status: :ok

        it "uses specified columns" do
          assert_equal view.output.columns, presented_columns
        end
      end

      describe "basic conditions" do
        before do
          @user_a.custom_field_values.update_from_hash("plan_type" => "gold", "ltv" => 10000)
          @user_a.save!
          @user_b.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 5700.25)
          @user_b.save!
          @user_c.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 2800)
          @user_c.save!
        end

        it "executes with all conditions" do
          preview_view(
            "all" => [
              {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"},
              {"field" => "custom_fields.ltv", "operator" => "less_than", "value" => 6000}
            ]
          )
          refute @user_ids.include?(@user_a.id)
          assert @user_ids.include?(@user_b.id)
          assert @user_ids.include?(@user_c.id)
        end

        it "executes with all conditions 2" do
          preview_view(
            "all" => [
              {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"},
              {"field" => "custom_fields.ltv", "operator" => "less_than", "value" => 5000}
            ]
          )
          refute @user_ids.include?(@user_a.id)
          refute @user_ids.include?(@user_b.id)
          assert @user_ids.include?(@user_c.id)
        end

        it "executes with any and all conditions" do
          preview_view(
            "all" => [{"field" => "custom_fields.ltv", "operator" => "less_than", "value" => 6000}],
            "any" => [
              {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"},
              {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold"}
            ]
          )
          refute @user_ids.include?(@user_a.id)
          assert @user_ids.include?(@user_b.id)
          assert @user_ids.include?(@user_c.id)
        end

        it "executes with any and all conditions 2" do
          preview_view(
            "all" => [{"field" => "custom_fields.ltv", "operator" => "less_than", "value" => 6000}],
            "any" => [{"field" => "custom_fields.plan_type", "operator" => "is", "value" => "bronze"}]
          )
          refute @user_ids.include?(@user_a.id)
          refute @user_ids.include?(@user_b.id)
          refute @user_ids.include?(@user_c.id)
        end

        it "does not execute without conditions" do
          post :preview, params: { user_view: setup_params('all' => nil).slice('all', 'any', 'execution') }

          if RAILS5
            code = 400
            exp = {
              "error" => {
                "title" => "Invalid attribute",
                "message" => "You passed an invalid value for the all attribute. Invalid parameter: all must be an array from api/v2/rules/user_views/preview"
              }
            }
          else
            code = 422
            exp = {
              "error" => "RecordInvalid",
              "description" => "Record validation errors",
              "details" => { "base" => [{ "error" => "BlankValue", "description" => "Missing all condition." }] }
            }
          end

          assert_response code, @response.body
          assert_equal exp, JSON.parse(@response.body)
        end

        it "executes without title" do
          post :preview, params: { user_view: setup_params('title' => nil,
                                                 "all" => [{ "field" => "name", "operator" => "is_not", "value" => nil }]).slice('all', 'any', 'execution') }

          assert_equal "200", @response.code
        end

        it "sorts default view sorting (created_at asc)" do
          user_view_param = setup_params(all_conditions.merge("all" => [{ "field" => "custom_fields.ltv", "operator" => "is_present" }])).slice('all', 'any', 'execution')
          post :preview, params: { user_view: user_view_param }

          @result = JSON.parse(@response.body)
          @user_ids = @result["rows"].map { |user| user["id"] }
          assert_equal [@user_a, @user_b, @user_c].sort_by(&:created_at).map(&:id), @user_ids
        end

        describe "by user attribute" do
          it "executes with all and any conditions" do
            preview_view(
              "all" => [{ "field" => "name", "operator" => "is_not", "value" => nil }],
              "any" => [
                {"field" => "name", "operator" => "is", "value" => @user_a.name},
                {"field" => "name", "operator" => "is", "value" => @user_b.name}
              ]
            )
            assert @user_ids.include?(@user_a.id)
            assert @user_ids.include?(@user_b.id)
            refute @user_ids.include?(@user_c.id)
          end
        end

        describe "by custom field" do
          it "executes with all and any conditions equal value" do
            preview_view(
              "all" => [{ "field" => "name", "operator" => "is_not", "value" => nil }],
              "any" => [
                {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold"},
                {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"}
              ]
            )
            assert @user_ids.include?(@user_a.id)
            assert @user_ids.include?(@user_b.id)
            assert @user_ids.include?(@user_c.id)
          end

          it "executes with all and any conditions not equal value" do
            preview_view(
              "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver" }],
              "any" => [
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => 5700.25}
              ]
            )
            refute @user_ids.include?(@user_a.id)
            refute @user_ids.include?(@user_b.id)
            assert @user_ids.include?(@user_c.id)
          end

          it "executes with all and any conditions not equal multiple values" do
            preview_view(
              "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold" }],
              "any" => [
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => 10000},
                {"field" => "custom_fields.ltv", "operator" => "greater_than", "value" => 10000}
              ]
            )
            refute @user_ids.include?(@user_a.id)
            refute @user_ids.include?(@user_b.id)
            refute @user_ids.include?(@user_c.id)
          end

          it "executes with all and any conditions not equal nil" do
            @user_c.custom_field_values.each(&:delete)
            @user_c.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => nil)
            @user_c.save!

            preview_view(
              "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver" }],
              "any" => [
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}
              ]
            )
            refute @user_ids.include?(@user_a.id)
            assert @user_ids.include?(@user_b.id)
            refute @user_ids.include?(@user_c.id)
          end

          it "executes with two conditions on the same field" do
            preview_view(
              "all" => [
                {"field" => "custom_fields.ltv", "operator" => "less_than_or_equal_to", "value" => 9000},
                {"field" => "custom_fields.ltv", "operator" => "greater_than_or_equal_to", "value" => 2800}
              ]
            )
            refute @user_ids.include?(@user_a.id)
            assert @user_ids.include?(@user_b.id)
            assert @user_ids.include?(@user_c.id)
          end

          it "executes with two conditions on the same field 2" do
            preview_view(
              "all" => [
                {"field" => "custom_fields.ltv", "operator" => "less_than_or_equal_to", "value" => 9000},
                {"field" => "custom_fields.ltv", "operator" => "greater_than_or_equal_to", "value" => 2900}
              ]
            )
            refute @user_ids.include?(@user_a.id)
            assert @user_ids.include?(@user_b.id)
            refute @user_ids.include?(@user_c.id)
          end

          it "executes with two conditions on the same field (one operator inverted)" do
            preview_view(
              "all" => [
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => 5700.25},
                {"field" => "custom_fields.ltv", "operator" => "greater_than_or_equal_to", "value" => 2900}
              ]
            )
            assert @user_ids.include?(@user_a.id)
            refute @user_ids.include?(@user_b.id)
            refute @user_ids.include?(@user_c.id)
          end

          it "executes with two conditions on the same field (both operators are inverted)" do
            preview_view(
              "all" => [
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => 5700.25},
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => 10000}
              ]
            )
            refute @user_ids.include?(@user_a.id)
            refute @user_ids.include?(@user_b.id)
            assert @user_ids.include?(@user_c.id)
          end
        end

        describe "by mixed fields" do
          it "executes with all and any conditions with dropdown field" do
            preview_view(
              "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
              "any" => [
                {"field" => "name", "operator" => "is", "value" => @user_a.name},
                {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"}
              ]
            )
            assert @user_ids.include?(@user_a.id)
            assert @user_ids.include?(@user_b.id)
            assert @user_ids.include?(@user_c.id)
          end

          it "executes with all and any conditions with custom field" do
            preview_view(
              "all" => [{ "field" => "name", "operator" => "is_not", "value" => nil }],
              "any" => [
                {"field" => "name", "operator" => "is", "value" => @user_a.name},
                {"field" => "custom_fields.ltv", "operator" => "is", "value" => 10000}
              ]
            )
            assert @user_ids.include?(@user_a.id)
            refute @user_ids.include?(@user_b.id)
            refute @user_ids.include?(@user_c.id)
          end
        end

        describe "for invalid sort_by" do
          it "sorts by view sorting if given (name asc)" do
            user_view_param = setup_params(all_conditions.merge(
              "all" => [{ "field" => "custom_fields.ltv", "operator" => "is_present" }],
              "execution" => {
                "sort" => {"id" => "name", "order" => "asc"}
              }
            )).slice('all', 'any', 'execution')
            post :preview, params: { user_view: user_view_param, sort_by: "sbdhfbjhdsbf", sort_order: "asc" }

            @result = JSON.parse(@response.body)
            @user_ids = @result["rows"].map { |user| user["id"] }
            assert_equal [@user_a, @user_b, @user_c].sort_by(&:name).map(&:id), @user_ids
          end

          it "sorts default view sorting (created_at asc)" do
            user_view_param = setup_params(all_conditions.merge("all" => [{ "field" => "custom_fields.ltv", "operator" => "is_present" }])).slice('all', 'any', 'execution')
            post :preview, params: { user_view: user_view_param, sort_by: "sbdhfbjhdsbf", sort_order: "asc" }

            @result = JSON.parse(@response.body)
            @user_ids = @result["rows"].map { |user| user["id"] }
            assert_equal [@user_a, @user_b, @user_c].sort_by(&:created_at).map(&:id), @user_ids
          end

          it "sorts default view sorting (created_at desc)" do
            user_view_param = setup_params(all_conditions.merge("all" => [{ "field" => "custom_fields.ltv", "operator" => "is_present" }])).slice('all', 'any', 'execution')
            post :preview, params: { user_view: user_view_param, sort_by: "sbdhfbjhdsbf", sort_order: "desc" }

            @result = JSON.parse(@response.body)
            @user_ids = @result["rows"].map { |user| user["id"] }
            assert_equal [@user_a, @user_b, @user_c].sort_by(&:created_at).reverse.map(&:id), @user_ids
          end
        end

        describe "for invalid sort_order" do
          before do
            @user_a.update_attribute(:created_at, 10.days.ago)
            @user_b.update_attribute(:created_at, 5.days.ago)
            @user_c.update_attribute(:created_at, 1.days.ago)
          end

          it "defaults sort order to asc" do
            user_view_param = setup_params(all_conditions.merge("all" => [{ "field" => "custom_fields.ltv", "operator" => "is_present" }])).slice('all', 'any', 'execution')
            post :preview, params: { user_view: user_view_param, sort_by: "created_at", sort_order: "" }

            @result = JSON.parse(@response.body)
            @user_ids = @result["rows"].map { |user| user["id"] }
            assert_equal [@user_a, @user_b, @user_c].sort_by(&:created_at).map(&:id), @user_ids
          end
        end
      end

      describe "when agent has readonly permissions" do
        before do
          User.any_instance.stubs(:can?).with(:view, UserView).returns(false)
          post :preview, params: { user_view: setup_params(all_conditions).slice('all', 'any', 'execution') }
        end

        it('responds with forbidden') { assert_response :forbidden }

        it "returns proper error message" do
          expected_error = {
            "error" => {
              "message" => "You do not have access to this page. Please contact the account owner of this help desk for further help.",
              "title" => "Forbidden"
            }
          }
          assert_equal expected_error, JSON.parse(@response.body)
        end
      end

      describe "default sampling" do
        before do
          preview_view(
            "all" => [
              {"field" => "name", "operator" => "is", "value" => "test"},
            ]
          )
        end

        before_should "not specify sampling option" do
          Zendesk::UserViews::Executer.expects(:users_for_view).
            with(anything, anything, has_entry(sampling: nil)).
            returns([])
        end
      end
    end

    describe "a POST to :create" do
      describe "when agent has enterprise permission to manage group views" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          Subscription.any_instance.stubs(:has_group_rules?).returns(true)
          @permission_set = build_valid_permission_set
          @permission_set.permissions.user_view_access = "manage-group"
          User.any_instance.stubs(:permission_set).returns(@permission_set)
          refute @user.can?(:manage_shared, UserView)
        end

        describe "restricted to group" do
          before do
            post :create, params: { user_view: setup_params(restricted_to_group(groups(:minimum_group).id)) }
          end

          it('responds with created') { assert_response :created }
        end
      end

      describe "when agent does not have enterprise permission to manage group views" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          Subscription.any_instance.stubs(:has_group_rules?).returns(true)
          @permission_set = build_valid_permission_set
          @permission_set.permissions.user_view_access = "manage-personal"
          User.any_instance.stubs(:permission_set).returns(@permission_set)
          refute @user.can?(:manage_shared, UserView)
        end

        describe "restricted to group" do
          before do
            post :create, params: { user_view: setup_params(restricted_to_group(groups(:minimum_group).id)) }
          end

          it('responds with forbidden') { assert_response :forbidden }

          it "returns proper error message" do
            expected_error = {
              "error" => {
                "message" => "You do not have access to this page. Please contact the account owner of this help desk for further help.",
                "title" => "Forbidden"
              }
            }
            assert_equal expected_error, JSON.parse(@response.body)
          end
        end
      end

      describe "when agent has readonly permissions" do
        before do
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          @permission_set = build_valid_permission_set
          @permission_set.permissions.user_view_access = "readonly"
          User.any_instance.stubs(:permission_set).returns(@permission_set)

          post :create, params: { user_view: setup_params(restricted_to_agent(@user.id)) }
        end

        it('responds with forbidden') { assert_response :forbidden }

        it "returns proper error message" do
          expected_error = {
            "error" => {
              "message" => "You do not have access to this page. Please contact the account owner of this help desk for further help.",
              "title" => "Forbidden"
            }
          }
          assert_equal expected_error, JSON.parse(@response.body)
        end
      end

      describe "when agent has no permissions" do
        before do
          create_view setup_params(all_conditions)

          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          @permission_set = build_valid_permission_set
          @permission_set.permissions.available_user_lists = "none"
          User.any_instance.stubs(:permission_set).returns(@permission_set)

          post :create, params: { user_view: setup_params(restricted_to_agent(@user.id)) }
        end

        it('responds with forbidden') { assert_response :forbidden }

        it "returns proper error message" do
          expected_error = {
            "error" => {
              "message" => "You do not have access to this page. Please contact the account owner of this help desk for further help.",
              "title" => "Forbidden"
            }
          }
          assert_equal expected_error, JSON.parse(@response.body)
        end
      end
    end

    describe "a DELETE to :destroy" do
      describe "valid" do
        before { delete :destroy, params: { id: rules(:minimum_custom_fields_view).id } }
        it('responds with no content') { assert_response :no_content }
        should_change("the number of user views", by: -1) { users(:minimum_end_user).rules.count(:all) }
        it "softs delete the view" do
          assert rules(:minimum_custom_fields_view).reload.deleted?
        end
      end

      it "does not be able to delete shared rules" do
        view = create_view setup_params(all_conditions)
        delete :destroy, params: { id: view.id }
        assert_equal @response.code, "403"
      end

      it "is able to delete personal rules" do
        view = create_view setup_params(restricted_to_agent(@user.id))
        delete :destroy, params: { id: view.id }
        assert_equal @response.code, "204"
      end
    end

    describe "a GET to :export" do
      let(:user_view) { rules(:minimum_custom_fields_view) }

      it "does not export user view" do
        get :export, params: { id: user_view.id }
        expected_error = {
          "error" => {
            "message" => "You do not have access to this page. Please contact the account owner of this help desk for further help.",
            "title" => "Forbidden"
          }
        }
        assert_equal expected_error, JSON.parse(@response.body)
      end

      describe "when agent has enterprise permission" do
        before do
          stub_request(:put, %r{\.amazonaws\.com/data/expirable_attachments/.*csv\.zip})
          Api::V2::Rules::UserViewsController.any_instance.stubs(form_authenticity_token: 'foo')
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          @permission_set = build_valid_permission_set
          @permission_set.permissions.user_view_access = "full"
          User.any_instance.stubs(:permission_set).returns(@permission_set)
          @request.env["HTTP_X_CSRF_TOKEN"] = "foo"
          get :export, params: { id: user_view.id }
        end

        it('responds with ok') { assert_response :ok }

        it "responds with export status" do
          body = JSON.parse(@response.body)
          assert_equal user_view.id, body['export']['user_view_id']
        end
      end
    end

    describe "a PUT to :update" do
      describe "when agent does not have enterprise permission to manage group views" do
        before do
          @view = create_view setup_params(restricted_to_account(@account.id))
          Account.any_instance.stubs(:has_permission_sets?).returns(true)
          Subscription.any_instance.stubs(:has_group_rules?).returns(true)
          @permission_set = build_valid_permission_set
          @permission_set.permissions.user_view_access = "manage-personal"
          User.any_instance.stubs(:permission_set).returns(@permission_set)
          refute @user.can?(:manage_shared, UserView)
        end

        describe "change restriction" do
          before do
            put :update, params: { id: @view.id, user_view: restricted_to_agent(@user.id) }
          end

          it('responds with forbidden') { assert_response :forbidden }

          it "returns proper error message" do
            expected_error = {
              "error" => {
                "message" => "You do not have access to this page. Please contact the account owner of this help desk for further help.",
                "title" => "Forbidden"
              }
            }
            assert_equal expected_error, JSON.parse(@response.body)
          end
        end
      end
    end

    describe "a PUT to :reorder" do
      describe "when sorting shared views" do
        before do
          create_view setup_params(restricted_to_account(@account.id))

          @user_views = UserView.account_owned(@account.id)

          @previous_ids = @user_views.order(:position).pluck(:id)
          assert_equal 3, @previous_ids.length

          put :reorder, params: { user_view_ids: @previous_ids.reverse }
        end

        it('responds with forbidden') { assert_response :forbidden }

        it "does not sort the views" do
          assert_equal @previous_ids, @user_views.order(:position).pluck(:id)
        end
      end

      describe "when sorting personal views" do
        before do
          @user_views = UserView.where(owner_type: 'User', owner_id: @user.id)

          @previous_ids = @user_views.order(:position).pluck(:id)
          assert_equal 2, @previous_ids.length

          put :reorder, params: { user_view_ids: @previous_ids.reverse }
        end

        it('responds with ok') { assert_response :ok }

        it "sorts the views as specified" do
          assert_equal @previous_ids.reverse, @user_views.order(:position).pluck(:id)
        end
      end

      describe "when sorting mixed views" do
        before do
          create_view setup_params(restricted_to_account(@account.id))

          account_scope  = UserView.account_owned(@account.id)
          personal_scope = UserView.where(owner_type: 'User', owner_id: @user.id)
          @user_views    = @account.user_views.and_disjunction_of(account_scope, personal_scope)

          @previous_ids = @user_views.order(:position).pluck(:id)
          assert_equal 5, @previous_ids.length

          put :reorder, params: { user_view_ids: @previous_ids.reverse }
        end

        it('responds with forbidden') { assert_response :forbidden }

        it "does not sort the views" do
          assert_equal @previous_ids, @user_views.order(:position).pluck(:id)
        end
      end

      describe "when user view ids is empty" do
        before do
          put :reorder
        end

        it "returns 403" do
          assert_response 403
        end
      end
    end
  end
end
