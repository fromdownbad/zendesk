require_relative "../../../../../support/test_helper"
require_relative "../../../../../support/user_views_test_helper"
require_relative "../../../../../support/custom_fields_test_helper"
require_relative "../../../../../support/api_test_helper"

SingleCov.not_covered! # test was split into multiple parts because it took too long

describe Api::V2::Rules::UserViewsController do
  extend Api::V2::TestHelper
  include UserViewsTestHelper
  include CustomFieldsTestHelper
  include ApiTestHelper

  fixtures :all

  def create_custom_fields
    create_dropdown_field("plan_type", "Plan",
      [{ name: "Gold",   value: "gold" },
       { name: "Silver", value: "silver" },
       { name: "Bronze", value: "bronze" }])
    create_user_custom_field!("ltv", "LTV", "Decimal")
  end

  before do
    Account.any_instance.stubs(:is_serviceable?).returns(true)
    Account.any_instance.stubs(:has_user_views_negative_operators_enabled?).returns(true)
    accept :json
    use_ssl
  end

  as_an_agent do
    before do
      create_custom_fields
      Account.any_instance.stubs(:has_user_views?).returns(true)
      Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
    end

    describe "a GET to :execute" do
      before do
        @admin = users(:minimum_admin)
        @agent = users(:minimum_agent)
        @end_user = users(:minimum_end_user)
        @admin.update_attribute(:created_at, 3.days.ago)
        @agent.update_attribute(:created_at, 1.days.ago)
        @end_user.update_attribute(:created_at, 2.days.ago)
      end

      describe "basic conditions" do
        before do
          @admin.custom_field_values.update_from_hash("plan_type" => "gold", "ltv" => 10000)
          @admin.save!
          @agent.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 5700.25)
          @agent.save!
          @end_user.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 2800)
          @end_user.save!
        end

        describe "by user attribute" do
          it "executes with all and any conditions" do
            create_and_execute_view(
              "all" => [{ "field" => "name", "operator" => "is_not", "value" => nil }],
              "any" => [
                {"field" => "name", "operator" => "is", "value" => @admin.name},
                {"field" => "name", "operator" => "is", "value" => @agent.name}
              ]
            )
            assert_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end
        end

        describe "by custom field" do
          it "executes with all and any conditions equal value" do
            create_and_execute_view(
              "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
              "any" => [
                {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold"},
                {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"}
              ]
            )
            assert_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it "executes with all and any conditions not equal value" do
            create_and_execute_view(
              "all" => [{"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"}],
              "any" => [
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => 5700.25}
              ]
            )
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it "executes with all and any conditions not equal multiple values" do
            create_and_execute_view(
              "all" => [{"field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold"}],
              "any" => [
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => 10000},
                {"field" => "custom_fields.ltv", "operator" => "greater_than", "value" => 10000}
              ]
            )
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "executes with all and any conditions not equal nil" do
            @end_user.custom_field_values.each(&:delete)
            @end_user.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => nil)
            @end_user.save!

            create_and_execute_view(
              "all" => [{"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"}],
              "any" => [
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}
              ]
            )
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "executes with two conditions on the same field" do
            create_and_execute_view(
              "all" => [
                {"field" => "custom_fields.ltv", "operator" => "less_than_or_equal_to", "value" => 9000},
                {"field" => "custom_fields.ltv", "operator" => "greater_than_or_equal_to", "value" => 2800}
              ]
            )
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it "executes with two conditions on the same field 2" do
            create_and_execute_view(
              "all" => [
                {"field" => "custom_fields.ltv", "operator" => "less_than_or_equal_to", "value" => 9000},
                {"field" => "custom_fields.ltv", "operator" => "greater_than_or_equal_to", "value" => 2900}
              ]
            )
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "executes with two conditions on the same field (one operator inverted)" do
            create_and_execute_view(
              "all" => [
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => 5700.25},
                {"field" => "custom_fields.ltv", "operator" => "greater_than_or_equal_to", "value" => 2900}
              ]
            )
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "executes with two conditions on the same field (both operators are inverted)" do
            create_and_execute_view(
              "all" => [
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => 5700.25},
                {"field" => "custom_fields.ltv", "operator" => "is_not", "value" => 10000}
              ]
            )
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end
        end

        describe "by mixed fields" do
          it "executes with all and any conditions with dropdown field" do
            create_and_execute_view(
              "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
              "any" => [
                {"field" => "name", "operator" => "is", "value" => @admin.name},
                {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"}
              ]
            )
            assert_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it "executes with all and any conditions with custom field" do
            create_and_execute_view(
              "all" => [{ "field" => "name", "operator" => "is_not", "value" => nil }],
              "any" => [
                {"field" => "name", "operator" => "is", "value" => @admin.name},
                {"field" => "custom_fields.ltv", "operator" => "is", "value" => 10000}
              ]
            )
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end
        end
      end

      describe "execution" do
        before do
          # The choices here are in reverse because for dropdowns, cf_values.value refers to cf_dropdown_choices.id
          # Since these are sequential, we don't want false positives by ascending or descending on cf_values.value
          # This forces the query builder to make the appropriate queries.
          create_user_custom_field!("age", "Age", "Integer")

          @admin.custom_field_values.update_from_hash("plan_type" => "gold", "ltv" => 10000)
          @admin.save!
          @agent.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 5700.25)
          @agent.save!
          @end_user.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 2800)
          @end_user.save!
        end

        it "returns 403 for inactive views" do
          view = create_view setup_params(all_conditions.merge("active" => false))
          get :execute, params: { id: view.id }
          assert_response 403
          assert_equal({
            "error" => {
              "title" => "Forbidden",
              "message" => I18n.t('txt.api.v2.user_views.errors.executed_inactive_filter', view: view.title)
            }
          }, JSON.parse(@response.body))
        end

        describe "for invalid sort_by" do
          it "sorts by view sorting if given (asc)" do
            view = create_view setup_params(all_conditions.merge(
              "all" => [{ "field" => "custom_fields.ltv", "operator" => "is_present" }],
              "execution" => {
                "sort" => {"id" => "name", "order" => "asc"}
              }
            ))
            get :execute, params: { id: view.id, sort_by: "sbdhfbjhdsbf", sort_order: "asc" }

            @result = JSON.parse(@response.body)
            @user_ids = @result["rows"].map { |user| user["id"] }
            assert_equal [@admin, @agent, @end_user].sort_by(&:name).map(&:id), @user_ids
          end

          it "sorts by default view sorting (created_at asc)" do
            view = create_view setup_params(all_conditions.merge("all" => [{ "field" => "custom_fields.ltv", "operator" => "is_present" }]))
            get :execute, params: { id: view.id, sort_by: "sbdhfbjhdsbf", sort_order: "asc" }

            @result = JSON.parse(@response.body)
            @user_ids = @result["rows"].map { |user| user["id"] }
            assert_equal [@admin, @agent, @end_user].sort_by(&:created_at).map(&:id), @user_ids
          end

          it "sorts by default view sorting (created_at desc)" do
            view = create_view setup_params(all_conditions.merge("all" => [{ "field" => "custom_fields.ltv", "operator" => "is_present" }]))
            get :execute, params: { id: view.id, sort_by: "sbdhfbjhdsbf", sort_order: "desc" }

            @result = JSON.parse(@response.body)
            @user_ids = @result["rows"].map { |user| user["id"] }
            assert_equal [@admin, @agent, @end_user].sort_by(&:created_at).reverse.map(&:id), @user_ids
          end
        end

        describe "for invalid sort_order" do
          before do
            @admin.update_attribute(:created_at, 10.days.ago)
            @agent.update_attribute(:created_at, 5.days.ago)
            @end_user.update_attribute(:created_at, 1.days.ago)
          end

          it "defaults sort order to asc" do
            view = create_view setup_params(all_conditions.merge("all" => [{ "field" => "custom_fields.ltv", "operator" => "is_present" }]))
            get :execute, params: { id: view.id, sort_by: "name", sort_order: "" }

            @result = JSON.parse(@response.body)
            @user_ids = @result["rows"].map { |user| user["id"] }
            assert_equal [@admin, @agent, @end_user].sort_by(&:name).map(&:id), @user_ids
          end
        end

        it "honors execution columns" do
          columns = ["created_at", "custom_fields.plan_type", "organization_id"]
          create_and_execute_view setup_params(all_conditions.merge("execution" => { "columns" => columns }))
          actual_columns = @result["columns"]

          assert_equal({"id" => "created_at", "title" => "Created"}, actual_columns[0])

          assert_equal('custom_fields.plan_type', actual_columns[1]['id'])
          assert_equal('Plan', actual_columns[1]['title'])
          assert_equal('dropdown', actual_columns[1]['type'])
          assert_match(%r{api/v2/user_fields}, actual_columns[1]['url'])

          assert_equal('organization_id', actual_columns[2]['id'])
          assert_equal('Organization', actual_columns[2]['title'])
        end

        it "defaults to name, email and created" do
          create_and_execute_view setup_params(all_conditions)
          actual_columns = @result["columns"]
          expected_columns = [
            {'id' => 'name', 'title' => 'Name'},
            {'id' => 'email', 'title' => 'Email'},
            {'id' => 'created_at', 'title' => 'Created'}
          ]
          assert_equal expected_columns, actual_columns
        end

        it "always include id in rows even if not in columns" do
          create_and_execute_view setup_params(all_conditions.merge("execution" => { "columns" => ["custom_fields.plan_type"] }))
          @result['rows'].each do |row|
            assert row.key?('id'), "key 'id' is missing"
          end
        end

        it "populates rows with dropdown choice titles" do
          create_and_execute_view setup_params("all" => [{"field" => "custom_fields.plan_type", "operator" => "is_not", "value" => nil}],
                                               "execution" => {
              "columns" => ["custom_fields.plan_type"],
              "sort" => {"id" => "custom_fields.plan_type", "order" => "asc"}
            })
          actual = @result['rows'].map { |row| row['custom_fields.plan_type'] }
          expected = %w[Gold Silver Silver]
          assert_equal expected, actual
        end

        it "populates rows with dropdown choice titles with dc" do
          assert dropdown = @account.custom_fields.where(type: "Dropdown").last
          dropdown.update_attributes!(title: "{{dc.title}}")
          dropdown.custom_field_options.first.name = "{{dc.gold}}"
          dropdown.custom_field_options.first.save
          @account.instance_variable_set(:@_custom_fields_cache, nil) # blow the cache since the dropdown was updated
          Zendesk::Liquid::DcContext.expects(:render).with("Silver", @account, @user, "text/plain", nil, {}).returns('Silver').twice
          Zendesk::Liquid::DcContext.expects(:render).with("{{dc.title}}", @account, @user, "text/plain", nil, {}).returns('title')
          Zendesk::Liquid::DcContext.expects(:render).with("{{dc.gold}}", @account, @user, "text/plain", nil, {}).returns('gold dc')
          create_and_execute_view setup_params(
            "all" => [{"field" => "custom_fields.plan_type", "operator" => "is_not", "value" => nil}],
            "execution" => {
              "columns" => ["custom_fields.plan_type"],
              "sort" => {"id" => "custom_fields.plan_type", "order" => "asc"}
            }
          )
          actual = @result['rows'].map { |row| row['custom_fields.plan_type'] }
          assert actual.include?("gold dc")
          assert_equal @result["columns"][0]["title"], "title"
        end

        ['group', 'sort'].each do |type|
          describe "ordering #{type}" do
            describe "by dropdown field" do
              describe "when dropdown tags don't match titles" do
                it "sorts by dropdown tags" do
                  dropdown_option = @account.custom_fields.find_by_key("plan_type").custom_field_options.first
                  dropdown_option.name = "ZZZZ"
                  dropdown_option.save

                  columns = ["id", "name", "custom_fields.plan_type"]
                  ordering = {"id" => "custom_fields.plan_type", "order" => "asc"}
                  create_and_execute_view(
                    "all" => [{"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  assert_equal 3, rows.length
                  assert_equal @admin.id, rows[0]['id']
                  same_plan_type = rows[1..2].map { |r| r['id'] }.sort
                  assert_equal [@end_user.id, @agent.id].sort, same_plan_type
                end
              end

              describe "with user attribute conditions" do
                it "ascends in order" do
                  columns = ["id", "name", "custom_fields.plan_type"]
                  ordering = {"id" => "custom_fields.plan_type", "order" => "asc"}
                  create_and_execute_view(
                    "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  same_plan_type = rows[11..13].map { |r| r['id'] }.sort
                  assert_equal [@end_user.id, @agent.id].sort, same_plan_type
                  assert_equal @admin.id, rows[10]['id']
                  rows[0..9].each do |row|
                    assert_nil row['custom_fields.plan_type']
                  end
                end

                it "descends in order" do
                  columns = ["id", "name", "custom_fields.plan_type"]
                  ordering = {"id" => "custom_fields.plan_type", "order" => "desc"}
                  create_and_execute_view(
                    "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]

                  same_plan_type = rows[0..1].map { |r| r['id'] }.sort
                  assert_equal [@end_user.id, @agent.id].sort, same_plan_type
                  assert_equal @admin.id, rows[2]['id']
                  rows[3..11].each do |row|
                    assert_nil row['plan_type']
                  end
                end
              end

              describe "with custom field conditions" do
                it "ascends in order" do
                  columns = ["id", "name", "custom_fields.plan_type"]
                  ordering = {"id" => "custom_fields.plan_type", "order" => "asc"}
                  create_and_execute_view(
                    "all" => [{"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  assert_equal 3, rows.length
                  assert_equal @admin.id, rows[0]['id']
                  same_plan_type = rows[1..2].map { |r| r['id'] }.sort
                  assert_equal [@end_user.id, @agent.id].sort, same_plan_type
                end

                it "descends in order" do
                  columns = ["id", "name", "custom_fields.plan_type"]
                  ordering = {"id" => "custom_fields.plan_type", "order" => "desc"}
                  create_and_execute_view(
                    "all" => [{"field" => "custom_fields.plan_type", "operator" => "is_not", "value" => nil}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  assert_equal 3, rows.length
                  same_plan_type = rows[0..1].map { |r| r['id'] }.sort
                  assert_equal [@end_user.id, @agent.id].sort, same_plan_type
                  assert_equal @admin.id, rows[2]['id']
                end
              end

              describe "with custom field conditions and ordering by same source as filtering" do
                before do
                  @admin.custom_field_values.update_from_hash("plan_type" => "gold")
                  @admin.save!
                  @agent.custom_field_values.update_from_hash("plan_type" => "silver")
                  @agent.save!
                  @end_user.custom_field_values.update_from_hash("plan_type" => "bronze")
                  @end_user.save!
                end

                it "ascends in order" do
                  columns = ["id", "name", "custom_fields.plan_type"]
                  ordering = {"id" => "custom_fields.plan_type", "order" => "asc"}
                  create_and_execute_view(
                    "all" => [{"field" => "custom_fields.plan_type", "operator" => "is_not", "value" => nil}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  assert_equal 3, rows.length
                  assert_equal [@end_user.id, @admin.id, @agent.id], @user_ids
                end

                it "descends in order" do
                  columns = ["id", "name", "custom_fields.plan_type"]
                  ordering = {"id" => "custom_fields.plan_type", "order" => "desc"}
                  create_and_execute_view(
                    "all" => [{"field" => "custom_fields.plan_type", "operator" => "is_not", "value" => nil}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  assert_equal 3, rows.length
                  assert_equal [@agent.id, @admin.id, @end_user.id], @user_ids
                end
              end
            end

            describe "by other custom field" do
              describe "with custom field conditions" do
                it "ascends in order" do
                  columns = ["id", "name", "custom_fields.ltv"]
                  ordering = {"id" => "custom_fields.ltv", "order" => "asc"}
                  create_and_execute_view(
                    "all" => [{"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  assert_equal 2, rows.length
                  assert_equal @end_user.id, rows[0]['id']
                  assert_equal @agent.id, rows[1]['id']
                end

                it "descends in order" do
                  columns = ["id", "name", "custom_fields.ltv"]
                  ordering = {"id" => "custom_fields.ltv", "order" => "desc"}
                  create_and_execute_view(
                    "all" => [{"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  assert_equal 2, rows.length
                  assert_equal @agent.id, rows[0]['id']
                  assert_equal @end_user.id, rows[1]['id']
                end
              end

              describe "with custom field conditions and ordering by same source as filtering" do
                it "ascends in order" do
                  columns = ["id", "name", "custom_fields.ltv"]
                  ordering = {"id" => "custom_fields.ltv", "order" => "asc"}
                  create_and_execute_view(
                    "all" => [{"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  assert_equal 3, rows.length
                  assert_equal [@end_user.id, @agent.id, @admin.id], @user_ids
                end

                it "descends in order" do
                  columns = ["id", "name", "custom_fields.ltv"]
                  ordering = {"id" => "custom_fields.ltv", "order" => "desc"}
                  create_and_execute_view(
                    "all" => [{"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  assert_equal 3, rows.length
                  assert_equal [@admin.id, @agent.id, @end_user.id], @user_ids
                end
              end
            end

            describe "by user attribute" do
              before do
                @admin.update_attribute(:created_at, 10.days.ago)
                @agent.update_attribute(:created_at, 5.days.ago)
                @end_user.update_attribute(:created_at, 1.days.ago)
                Timecop.freeze
              end

              describe "with custom field conditions" do
                it "ascends in order" do
                  columns = ["id", "name"]
                  ordering = {"id" => "created_at", "order" => "asc"}
                  create_and_execute_view(
                    "all" => [{"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  assert_equal 3, rows.length
                  expected_names = [@admin, @agent, @end_user].map(&:name)
                  actual_names = rows.map { |r| r['name'] }
                  assert_equal expected_names, actual_names
                end

                it "descends in order" do
                  columns = ["id", "name"]
                  ordering = {"id" => "created_at", "order" => "desc"}
                  create_and_execute_view(
                    "all" => [{"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}],
                    "execution" => {
                      type => ordering,
                      "columns" => columns
                    }
                  )
                  rows = @result["rows"]
                  assert_equal 3, rows.length
                  expected_names = [@end_user, @agent, @admin].map(&:name)
                  actual_names = rows.map { |r| r['name'] }
                  assert_equal expected_names.sort, actual_names.sort
                end
              end
            end
          end
        end

        describe "sort by created_at within a day" do
          before do
            @admin.update_attribute(:created_at, Time.parse("04/10/2013 3:00"))
            @admin.update_attribute(:name, "Y")
            @agent.update_attribute(:created_at, Time.parse("04/10/2013 2:00"))
            @agent.update_attribute(:name, "X")
            @end_user.update_attribute(:created_at, Time.parse("04/10/2013 5:00"))
            @end_user.update_attribute(:name, "Z")
          end

          it "sorts within a group by day and not a particular timestamp" do
            create_and_execute_view(
              "all" => [{"field" => "created_at", "operator" => "greater_than", "value" => Time.parse("03/10/2013")}],
              "execution" => {
                'sort' => { 'id' => 'created_at', 'order' => 'asc' }
              }
            )
            rows = @result['rows'].select { |u| [@admin.id, @agent.id, @end_user.id].include? u['id'] }
            assert_equal [@agent.id, @admin.id, @end_user.id], rows.map { |r| r['id'] }
          end
        end

        describe "group by date field and sort by other field for created_at" do
          before do
            @admin.update_attribute(:created_at, Time.parse("04/10/2013 3:00"))
            @admin.update_attribute(:name, "Y")
            @agent.update_attribute(:created_at, Time.parse("04/10/2013 4:00"))
            @agent.update_attribute(:name, "X")
            @end_user.update_attribute(:created_at, Time.parse("04/10/2013 5:00"))
            @end_user.update_attribute(:name, "Z")
          end

          it "sorts within a group by day and not a particular timestamp" do
            create_and_execute_view(
              "all" => [{"field" => "created_at", "operator" => "greater_than", "value" => Time.parse("03/10/2013")}],
              "execution" => {
                'sort' => { 'id' => 'name', 'order' => 'asc' },
                'group' => { 'id' => 'created_at', 'order' => 'desc' }
              }
            )
            rows = @result['rows'].select { |u| [@admin.id, @agent.id, @end_user.id].include? u['id'] }
            assert_equal [@agent.id, @admin.id, @end_user.id], rows.map { |r| r['id'] }
          end
        end

        describe "group by date field and sort by other field for last_login" do
          before do
            @admin.update_attribute(:last_login, Time.parse("04/10/2013 3:00"))
            @admin.update_attribute(:name, "Y")
            @agent.update_attribute(:last_login, Time.parse("04/10/2013 4:00"))
            @agent.update_attribute(:name, "X")
            @end_user.update_attribute(:last_login, Time.parse("04/10/2013 5:00"))
            @end_user.update_attribute(:name, "Z")
          end

          it "sorts within a group by day and not a particular timestamp" do
            create_and_execute_view(
              "all" => [{"field" => "last_login", "operator" => "greater_than", "value" => Time.parse("03/10/2013")}],
              "execution" => {
                'sort' => { 'id' => 'name', 'order' => 'asc' },
                'group' => { 'id' => 'last_login', 'order' => 'desc' }
              }
            )
            rows = @result['rows'].select { |u| [@admin.id, @agent.id, @end_user.id].include? u['id'] }
            assert_equal [@agent.id, @admin.id, @end_user.id], rows.map { |r| r['id'] }
          end
        end

        describe "group by dropdown field and sort by other custom field" do
          [
            {group: 'asc', sort: 'asc', expected: lambda { [@admin, @end_user, @agent] }},
            {group: 'asc', sort: 'desc', expected: lambda { [@admin, @agent, @end_user] }},
            {group: 'desc', sort: 'asc', expected: lambda { [@end_user, @agent, @admin] }},
            {group: 'desc', sort: 'desc', expected: lambda { [@agent, @end_user, @admin] }}
          ].each do |options|
            it "groups #{options[:group]} and sort #{options[:sort]}" do
              create_and_execute_view(
                "all" => [{"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}],
                "execution" => {
                  "group" => {"id" => "custom_fields.plan_type", "order" => options[:group]},
                  "sort" => {"id" => "custom_fields.ltv", "order" => options[:sort]},
                  "columns" => ["id", "name", "custom_fields.plan_type", "custom_fields.ltv"]
                }
              )
              rows = @result["rows"]
              assert_equal 3, rows.length
              expected_names = instance_exec(&options[:expected]).map(&:name)
              actual_names = rows.map { |r| r['name'] }
              assert_equal expected_names, actual_names
            end
          end
        end

        describe "group by custom field and sort by dropdown field" do
          before do
            @admin.custom_field_values.update_from_hash("plan_type" => "gold", "ltv" => 10000)
            @admin.save!
            @agent.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 10000)
            @agent.save!
            @end_user.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 2800)
            @end_user.save!
          end

          [
            {group: 'asc', sort: 'asc', expected: lambda { [@end_user, @admin, @agent] }},
            {group: 'asc', sort: 'desc', expected: lambda { [@end_user, @agent, @admin] }},
            {group: 'desc', sort: 'asc', expected: lambda { [@admin, @agent, @end_user] }},
            {group: 'desc', sort: 'desc', expected: lambda { [@agent, @admin, @end_user] }}
          ].each do |options|
            it "groups #{options[:group]} and sort #{options[:sort]}" do
              create_and_execute_view(
                "all" => [{"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}],
                "execution" => {
                  "group" => {"id" => "custom_fields.ltv", "order" => options[:group]},
                  "sort" => {"id" => "custom_fields.plan_type", "order" => options[:sort]},
                  "columns" => ["id", "name", "custom_fields.plan_type", "custom_fields.ltv"]
                }
              )
              rows = @result["rows"]
              assert_equal 3, rows.length
              expected_names = instance_exec(&options[:expected]).map(&:name)
              actual_names = rows.map { |r| r['name'] }
              assert_equal expected_names, actual_names
            end
          end
        end

        describe "group by custom field and sort other custom field" do
          before do
            @admin.custom_field_values.update_from_hash("age" => "30", "ltv" => 10000)
            @admin.save!
            @agent.custom_field_values.update_from_hash("age" => "25", "ltv" => 10000)
            @agent.save!
            @end_user.custom_field_values.update_from_hash("age" => "20", "ltv" => 2800)
            @end_user.save!
          end

          [
            { group: 'asc', sort: 'asc', expected: lambda { [@end_user, @agent, @admin] }},
            {group: 'asc', sort: 'desc', expected: lambda { [@end_user, @admin, @agent] }},
            {group: 'desc', sort: 'asc', expected: lambda { [@agent, @admin, @end_user] }},
            {group: 'desc', sort: 'desc', expected: lambda { [@admin, @agent, @end_user] }}
          ].each do |options|
            it "groups #{options[:group]} and sort #{options[:sort]}" do
              create_and_execute_view(
                "all" => [{"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}],
                "execution" => {
                  "group" => {"id" => "custom_fields.ltv", "order" => options[:group]},
                  "sort" => {"id" => "custom_fields.age", "order" => options[:sort]},
                  "columns" => ["id", "name", "custom_fields.age", "custom_fields.ltv"]
                }
              )
              rows = @result["rows"]
              assert_equal 3, rows.length
              expected_names = instance_exec(&options[:expected]).map(&:name)
              actual_names = rows.map { |r| r['name'] }
              assert_equal expected_names, actual_names
            end
          end
        end

        describe "group by custom field and sort by same custom field" do
          before do
            @admin.custom_field_values.update_from_hash("age" => "30")
            @admin.save!
            @agent.custom_field_values.update_from_hash("age" => "25")
            @agent.save!
            @end_user.custom_field_values.update_from_hash("age" => "20")
            @end_user.save!
          end

          [
            {group: 'asc', sort: 'asc', expected: lambda { [@end_user, @agent, @admin] }},
            {group: 'asc', sort: 'desc', expected: lambda { [@end_user, @agent, @admin] }},
            {group: 'desc', sort: 'asc', expected: lambda { [@admin, @agent, @end_user] }},
            {group: 'desc', sort: 'desc', expected: lambda { [@admin, @agent, @end_user] }}
          ].each do |options|
            it "groups #{options[:group]} and sort #{options[:sort]}" do
              create_and_execute_view(
                "all" => [{"field" => "custom_fields.age", "operator" => "is_not", "value" => nil}],
                "execution" => {
                  "group" => {"id" => "custom_fields.age", "order" => options[:group]},
                  "sort" => {"id" => "custom_fields.age", "order" => options[:sort]},
                  "columns" => ["id", "name", "custom_fields.age"]
                }
              )
              rows = @result["rows"]
              assert_equal 3, rows.length
              expected_names = instance_exec(&options[:expected]).map(&:name)
              actual_names = rows.map { |r| r['name'] }
              assert_equal expected_names, actual_names
            end
          end
        end

        describe "group by dropdown field and sort by same dropdown field" do
          before do
            @admin.custom_field_values.update_from_hash("plan_type" => "gold")
            @admin.save!
            @agent.custom_field_values.update_from_hash("plan_type" => "silver")
            @agent.save!
            @end_user.custom_field_values.update_from_hash("plan_type" => "bronze")
            @end_user.save!
          end

          [
            { group: 'asc', sort: 'asc', expected: lambda { [@end_user, @admin, @agent] }},
            {group: 'asc', sort: 'desc', expected: lambda { [@end_user, @admin, @agent] }},
            {group: 'desc', sort: 'asc', expected: lambda { [@agent, @admin, @end_user] }},
            {group: 'desc', sort: 'desc', expected: lambda { [@agent, @admin, @end_user] }}
          ].each do |options|
            it "groups #{options[:group]} and sort #{options[:sort]}" do
              create_and_execute_view(
                "all" => [{"field" => "custom_fields.plan_type", "operator" => "is_not", "value" => nil}],
                "execution" => {
                  "group" => {"id" => "custom_fields.plan_type", "order" => options[:group]},
                  "sort" => {"id" => "custom_fields.plan_type", "order" => options[:sort]},
                  "columns" => ["id", "name", "custom_fields.plan_type"]
                }
              )
              rows = @result["rows"]
              assert_equal 3, rows.length
              expected_names = instance_exec(&options[:expected]).map(&:name)
              actual_names = rows.map { |r| r['name'] }
              assert_equal expected_names, actual_names
            end
          end
        end

        describe "group by created_at and sort by created_at" do
          before do
            @account.users.update_all(created_at: nil)
            @admin.update_attribute(:created_at, Time.parse("2013-11-04 03:00:00 UTC"))
            @agent.update_attribute(:created_at, Time.parse("2013-10-04 02:00:00 UTC"))
            @end_user.update_attribute(:created_at, Time.parse("2013-10-04 05:00:00 UTC"))
            Timecop.freeze
          end

          [
            { group: 'asc', sort: 'asc', expected: lambda { [@agent, @end_user, @admin] } },
            {group: 'asc', sort: 'desc', expected: lambda { [@end_user, @agent, @admin] }},
            {group: 'desc', sort: 'asc', expected: lambda { [@admin, @agent, @end_user] }},
            {group: 'desc', sort: 'desc', expected: lambda { [@admin, @end_user, @agent] }}
          ].each do |options|
            it "groups #{options[:group]} and sort #{options[:sort]}" do
              create_and_execute_view(
                "all" => [{"field" => "created_at", "operator" => "greater_than", "value" => "2011-01-01"}],
                "execution" => {
                  "group" => {"id" => "created_at", "order" => options[:group]},
                  "sort" => {"id" => "created_at", "order" => options[:sort]},
                  "columns" => ["id", "name", "created_at"]
                }
              )
              rows = @result["rows"]
              assert_equal 3, rows.length
              expected_names = instance_exec(&options[:expected]).map(&:name)
              actual_names = rows.map { |r| r['name'] }
              assert_equal expected_names.sort, actual_names.sort
            end
          end
        end

        describe "group by created_at and sort by created_at based on time zone" do
          before do
            assert_equal 'Copenhagen', @account.time_zone, "This test was designed to run against the Copenhagen time zone."
            @account.users.update_all(created_at: nil)
            @admin.update_attribute(:created_at, Time.parse("2013-11-04 03:00:00 UTC")) # Mon, 04 Nov 2013 04:00:00 CET +01:00
            @agent.update_attribute(:created_at, Time.parse("2013-11-04 23:00:00 UTC")) # Tue, 05 Nov 2013 00:00:00 CET +01:00
            @end_user.update_attribute(:created_at, Time.parse("2013-11-04 04:00:00 UTC")) # Mon, 04 Nov 2013 05:00:00 CET +01:00
            Timecop.freeze
          end

          [
            {group: 'asc', sort: 'asc', expected: lambda { [@admin, @end_user, @agent] }},
            {group: 'asc', sort: 'desc', expected: lambda { [@end_user, @admin, @agent] }},
            {group: 'desc', sort: 'asc', expected: lambda { [@agent, @admin, @end_user] }},
            {group: 'desc', sort: 'desc', expected: lambda { [@agent, @end_user, @admin] }}
          ].each do |options|
            it "groups #{options[:group]} and sort #{options[:sort]}" do
              create_and_execute_view(
                "all" => [{"field" => "created_at", "operator" => "greater_than", "value" => "2011-01-01"}],
                "execution" => {
                  "group" => {"id" => "created_at", "order" => options[:group]},
                  "sort" => {"id" => "created_at", "order" => options[:sort]},
                  "columns" => ["id", "name", "created_at"]
                }
              )
              rows = @result["rows"]
              assert_equal 3, rows.length
              expected_names = instance_exec(&options[:expected]).map(&:name)
              actual_names = rows.map { |r| r['name'] }
              assert_equal expected_names.sort, actual_names.sort
            end
          end
        end

        describe "grouping" do
          it "includes group id in rows" do
            create_and_execute_view(
              "all" => [{"field" => "custom_fields.ltv", "operator" => "is_not", "value" => nil}],
              "execution" => {
                "group" => {"id" => "custom_fields.plan_type", "order" => "asc"},
                "columns" => ["id", "name"]
              }
            )
            @result["rows"].each do |row|
              assert row['custom_fields.plan_type'], 'plan_type key is missing'
            end
          end
        end

        describe "params sorting" do
          it "honors params sorting for custom fields" do
            view = create_view setup_params(
              "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
              "execution" => {
                "sort" => {"id" => "name", "order" => "desc"},
                "columns" => ["id", "name", "custom_fields.plan_type"]
              }
            )

            get :execute, params: { id: view.id, sort_by: 'custom_fields.plan_type', sort_order: 'asc' }
            @result = JSON.parse(@response.body)
            @user_ids = @result["rows"].map { |user| user["id"] }

            rows = @result["rows"]
            same_plan_type = rows[11..13].map { |r| r['id'] }.sort
            assert_equal [@end_user.id, @agent.id].sort, same_plan_type
            assert_equal @admin.id, rows[10]['id']
            rows[0..9].each do |row|
              assert_nil row['custom_fields.plan_type']
            end
          end

          it "honors params sorting for user fields" do
            view = create_view setup_params(
              "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
              "execution" => {
                "sort" => {"id" => "created_at", "order" => "desc"},
                "columns" => ["id", "name", "custom_fields.plan_type"]
              }
            )

            get :execute, params: { id: view.id, sort_by: 'name', sort_order: 'asc' }
            @result = JSON.parse(@response.body)
            @user_ids = @result["rows"].map { |user| user["id"] }
            assert_equal @account.users.order(:name).map(&:id), @user_ids
          end

          it "defaults sorting to view sorting for unsortable_fields" do
            view = create_view setup_params(
              "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
              "execution" => {
                "sort" => {"id" => "name", "order" => "desc"},
                "columns" => ["id", "name", "custom_fields.plan_type"]
              }
            )

            get :execute, params: { id: view.id, sort_by: 'current_tags', sort_order: 'asc' }
            @result = JSON.parse(@response.body)
            @user_ids = @result["rows"].map { |user| user["id"] }
            assert_equal @account.users.order(:name).pluck(:id), @user_ids
          end
        end
      end

      describe "dates" do
        before do
          @admin.update_attribute(:created_at, 20.days.ago)
          @agent.update_attribute(:created_at, 10.days.ago)
          @end_user.update_attribute(:created_at, 5.days.ago)
        end

        it "executes with created_at before a date" do
          create_and_execute_view setup_params("all" => [{"field" => "created_at", "operator" => "less_than", "value" => @agent.created_at}])
          assert_includes @user_ids, @admin.id
          refute_includes @user_ids, @agent.id
          refute_includes @user_ids, @end_user.id
        end

        it "executes with created_at on or before a date" do
          create_and_execute_view setup_params("all" => [{"field" => "created_at", "operator" => "less_than_or_equal_to", "value" => @agent.created_at}])
          assert_includes @user_ids, @admin.id
          assert_includes @user_ids, @agent.id
          refute_includes @user_ids, @end_user.id
        end

        it "executes with created_at after a date" do
          create_and_execute_view setup_params("all" => [{"field" => "created_at", "operator" => "greater_than", "value" => @agent.created_at}])
          refute_includes @user_ids, @admin.id
          refute_includes @user_ids, @agent.id
          assert_includes @user_ids, @end_user.id
        end

        it "executes with created_at on or after a date" do
          create_and_execute_view setup_params("all" => [{"field" => "created_at", "operator" => "greater_than_or_equal_to", "value" => @agent.created_at}])
          refute_includes @user_ids, @admin.id
          assert_includes @user_ids, @agent.id
          assert_includes @user_ids, @end_user.id
        end

        it "executes with created_at on a date" do
          create_and_execute_view setup_params("all" => [{"field" => "created_at", "operator" => "is", "value" => @agent.created_at}])
          refute_includes @user_ids, @admin.id
          assert_includes @user_ids, @agent.id
          refute_includes @user_ids, @end_user.id
        end

        it "executes with created_at on a date string" do
          create_and_execute_view setup_params("all" => [{"field" => "created_at", "operator" => "is", "value" => @agent.created_at.to_s}])
          refute_includes @user_ids, @admin.id
          assert_includes @user_ids, @agent.id
          refute_includes @user_ids, @end_user.id
        end

        it "executes with created_at not on a date" do
          create_and_execute_view setup_params("all" => [{"field" => "created_at", "operator" => "is_not", "value" => @agent.created_at}])
          assert_includes @user_ids, @admin.id
          refute_includes @user_ids, @agent.id
          assert_includes @user_ids, @end_user.id
        end

        it "executes with created_at within the previous 10 days" do
          Timecop.freeze do
            @admin.update_attribute(:created_at, 11.days.ago)
            @agent.update_attribute(:created_at, 9.days.ago)
            @end_user.update_attribute(:created_at, 5.days.ago)
            create_and_execute_view setup_params("all" => [{"field" => "created_at", "operator" => "within_previous_n_days", "value" => "10"}])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end
        end

        it "executes view with last login on a date" do
          @agent.update_attribute(:last_login, Time.now)
          @admin.update_attribute(:last_login, 3.days.ago)
          @end_user.update_attribute(:last_login, nil)
          create_and_execute_view setup_params("all" => [{"field" => "last_login", "operator" => "is", "value" => @agent.last_login}])
          refute_includes @user_ids, @admin.id
          assert_includes @user_ids, @agent.id
          refute_includes @user_ids, @end_user.id
        end

        it "executes view with last login not on a date" do
          @agent.update_attribute(:last_login, Time.now)
          @admin.update_attribute(:last_login, 3.days.ago)
          @end_user.update_attribute(:last_login, nil)
          create_and_execute_view setup_params("all" => [{"field" => "last_login", "operator" => "is_not", "value" => @agent.last_login}])
          assert_includes @user_ids, @admin.id
          refute_includes @user_ids, @agent.id
          assert_includes @user_ids, @end_user.id
        end

        describe "in columns" do
          it "includes created in rows" do
            create_and_execute_view setup_params(
              "all" => [{"field" => "created_at", "operator" => "less_than", "value" => @agent.created_at}],
              "execution" => {
                "columns" => ["id", "created_at"]
              }
            )
            actual_row = @result['rows'].last
            assert actual_row.key?('created_at'), "key 'created_at' is missing"
            assert_equal @admin.created_at.to_i, Time.parse(actual_row['created_at']).to_i
          end
        end

        ['group', 'sort'].each do |type|
          describe "ordering `#{type}`" do
            before { Timecop.freeze }

            it "ascends in order" do
              columns = ["id", "name", "created_at"]
              ordering = {"id" => "created_at", "order" => "asc"}
              create_and_execute_view(
                "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                "execution" => {
                  type => ordering,
                  "columns" => columns
                }
              )

              expected_order = @account.users.map { |u| u.created_at.to_i }.sort
              actual_order   = @result["rows"].map { |r| Time.parse(r['created_at']).to_i }

              flunk "Not in ascending order" unless actual_order == actual_order.sort

              assert_equal expected_order, actual_order
            end

            it "descends in order" do
              columns = ["id", "name", "created_at"]
              ordering = {"id" => "created_at", "order" => "desc"}
              create_and_execute_view(
                "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                "execution" => {
                  type => ordering,
                  "columns" => columns
                }
              )

              expected_order = @account.users.map { |u| u.created_at.to_i }.sort.reverse
              actual_order   = @result["rows"].map { |r| Time.parse(r['created_at']).to_i }

              flunk "Not in descending order" unless actual_order == actual_order.sort.reverse

              assert_equal expected_order, actual_order
            end
          end
        end
      end

      describe "language" do
        before do
          @english = translation_locales(:english_by_zendesk)
          @japanese = translation_locales(:japanese)
          Account.any_instance.stubs(:available_languages).returns([@english, @japanese])

          assert_equal @english.id, @account.locale_id
          @admin.update_attribute(:locale_id, @english.id)
          @agent.update_attribute(:locale_id, @japanese.id)
          @end_user.update_attribute(:locale_id, nil) # Uses default language for account
        end

        describe "with `user_views_use_default_language` Arturo disabled" do
          before do
            Arturo.disable_feature!(:user_views_use_default_language)
          end

          it 'executes view with language' do
            create_and_execute_view setup_params("all" => [{"field" => "locale_id", "operator" => "is", "value" => @english.id }])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it 'executes view without language' do
            create_and_execute_view setup_params("all" => [{"field" => "locale_id", "operator" => "is_not", "value" => @english.id }])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it 'executes view without nil language' do
            create_and_execute_view setup_params("all" => [{"field" => "locale_id", "operator" => "is_not", "value" => nil }])
            assert_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end
        end

        describe "with `user_views_use_default_language` Arturo enabled" do
          before do
            Arturo.enable_feature!(:user_views_use_default_language)
          end

          it 'executes view with language' do
            create_and_execute_view setup_params("all" => [{"field" => "locale_id", "operator" => "is", "value" => @english.id }])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            # Treat blank locale_id as default locale
            assert_includes @user_ids, @end_user.id
          end

          it 'executes view without default language' do
            create_and_execute_view setup_params("all" => [{"field" => "locale_id", "operator" => "is_not", "value" => @english.id }])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            # Treat blank locale_id as default locale
            refute_includes @user_ids, @end_user.id
          end

          it 'executes view without other language' do
            create_and_execute_view setup_params("all" => [{"field" => "locale_id", "operator" => "is_not", "value" => @japanese.id }])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            # Treat blank locale_id as default locale
            assert_includes @user_ids, @end_user.id
          end

          it 'executes view without nil language' do
            create_and_execute_view setup_params("all" => [{"field" => "locale_id", "operator" => "is_not", "value" => nil }])
            assert_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            # Treat blank locale_id as default locale
            assert_includes @user_ids, @end_user.id
          end
        end

        it 'does not execute a view where the language has been deleted' do
          view = view_with_removed_locale(@japanese)
          Account.any_instance.stubs(:available_languages).returns([@english])
          get :execute, params: { id: view.id }
          assert_record_invalidation("InvalidValue", "'Language is #{@japanese.id}' is an invalid condition. '#{@japanese.id}' is not a valid value.")
        end

        describe "in columns" do
          it "includes locale in rows" do
            create_and_execute_view setup_params(
              "all" => [{"field" => "locale_id", "operator" => "is", "value" => TranslationLocale.find_by_locale("en").id}],
              "execution" => {
                "columns" => ["id", "locale_id"]
              }
            )
            actual_row = @result['rows'][0]
            assert actual_row.key?('locale_id'), "key 'locale_id' is missing"
            assert_equal @admin.translation_locale.localized_name(display_in: @agent), actual_row['locale_id']
          end
        end

        ['group', 'sort'].each do |type|
          describe "ordering #{type}" do
            before do
              locales = TranslationLocale.where(account_id: @account.id).to_a
              @account.users.each_with_index do |user, index|
                selection_index = index % locales.length
                user.update_attribute(:locale_id, locales[selection_index].id)
              end
            end

            it "ascends in order" do
              create_and_execute_view setup_params(
                "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                "execution" => {
                  "group" => {"id" => "locale_id", "order" => "asc"},
                  "columns" => ["id", "locale_id"]
                }
              )
              actual_languages = @result["rows"].map { |r| r['locale_id'] }
              expected_locales = @account.users.order(:locale_id).pluck(:locale_id)
              expected_languages = expected_locales.map do |l|
                l.present? ? TranslationLocale.find(l).localized_name(display_in: @agent) : @account.translation_locale.localized_name(display_in: @agent)
              end
              assert_equal expected_languages, actual_languages
            end

            it "descends in order" do
              create_and_execute_view setup_params(
                "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                "execution" => {
                  "group" => {"id" => "locale_id", "order" => "desc"},
                  "columns" => ["id", "locale_id"]
                }
              )
              actual_languages = @result["rows"].map { |r| r['locale_id'] }
              expected_locales = @account.users.order("locale_id DESC").pluck(:locale_id)
              expected_languages = expected_locales.map do |l|
                l.present? ? TranslationLocale.find(l).localized_name(display_in: @agent) : @account.translation_locale.localized_name(display_in: @agent)
              end
              assert_equal expected_languages, actual_languages
            end
          end
        end
      end

      describe "organization_id" do
        before do
          @organization_a = @account.organizations.create(name: "Zendesk")
          @organization_b = @account.organizations.create(name: "Groupon")
          @admin.update_attribute(:organizations, [@organization_a])
          @agent.update_attribute(:organizations, [@organization_b])
          @end_user.organization_memberships.destroy_all
        end

        describe 'membership' do
          it 'executes view with organization' do
            create_and_execute_view setup_params("all" => [{"field" => "organization_id", "operator" => "is", "value" => @admin.organization_id }])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it 'executes view without organization' do
            create_and_execute_view setup_params("all" => [{"field" => "organization_id", "operator" => "is_not", "value" => @admin.organization_id }])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it 'does not execute a view where the organization has been deleted' do
            view = create_view setup_params("all" => [{"field" => "organization_id", "operator" => "is", "value" => @admin.organization_id }])
            @admin.organization.destroy
            get :execute, params: { id: view.id }
            assert_record_invalidation("InvalidValue", "'Organization is #{@admin.organization_id}' is an invalid condition. '#{@admin.organization_id}' is not a valid value.")
          end
        end

        describe 'multi-org membership' do
          before do
            Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
            @agent.update_attribute(:organizations, [@organization_a, @organization_b])
            @agent.default_organization = @organization_a
            @agent.save!
          end

          describe 'with positive operator' do
            it 'filters by default organization' do
              create_and_execute_view setup_params("all" => [{"field" => "organization_id", "operator" => "is", "value" => @organization_a.id }])
              assert_includes @user_ids, @admin.id
              assert_includes @user_ids, @agent.id
              refute_includes @user_ids, @end_user.id
            end

            it 'filters by non-default organization' do
              create_and_execute_view setup_params("all" => [{"field" => "organization_id", "operator" => "is", "value" => @organization_b.id }])
              refute_includes @user_ids, @admin.id
              assert_includes @user_ids, @agent.id
              refute_includes @user_ids, @end_user.id
            end

            it 'filters known organizations' do
              create_and_execute_view setup_params("all" => [{"field" => "organization_id", "operator" => "is_present" }])
              assert_includes @user_ids, @admin.id
              assert_includes @user_ids, @agent.id
              refute_includes @user_ids, @end_user.id
            end

            it 'filters by nil' do
              create_and_execute_view setup_params("all" => [{"field" => "organization_id", "operator" => "is", "value" => nil }])
              refute_includes @user_ids, @admin.id
              refute_includes @user_ids, @agent.id
              assert_includes @user_ids, @end_user.id
            end
          end

          describe 'with negative operator' do
            it 'filters by default organization' do
              create_and_execute_view setup_params("all" => [{"field" => "organization_id", "operator" => "is_not", "value" => @organization_a.id }])
              refute_includes @user_ids, @admin.id
              refute_includes @user_ids, @agent.id
              assert_includes @user_ids, @end_user.id
            end

            it 'filters by non-default organization' do
              create_and_execute_view setup_params("all" => [{"field" => "organization_id", "operator" => "is_not", "value" => @organization_b.id }])
              assert_includes @user_ids, @admin.id
              refute_includes @user_ids, @agent.id
              assert_includes @user_ids, @end_user.id
            end

            it 'filters not known organizations' do
              create_and_execute_view setup_params("all" => [{"field" => "organization_id", "operator" => "is_not_present" }])
              refute_includes @user_ids, @admin.id
              refute_includes @user_ids, @agent.id
              assert_includes @user_ids, @end_user.id
            end

            it 'filters by not nil' do
              create_and_execute_view setup_params("all" => [{"field" => "organization_id", "operator" => "is_not", "value" => nil }])
              assert_includes @user_ids, @admin.id
              assert_includes @user_ids, @agent.id
              refute_includes @user_ids, @end_user.id
            end
          end
        end

        describe 'in columns' do
          it 'side-loads organizations' do
            create_and_execute_view setup_params(
              "all" => [{"field" => "organization_id", "operator" => "is_not", "value" => @admin.organization_id }],
              "execution" => {
                "columns" => ["id", "name", "organization_id"]
              }
            )
            @result["rows"].each do |row|
              assert row.keys.include?('organization_id'), 'organization_id key is missing'
              next if row['organization_id'].nil?
              assert_equal @account.users.find(row['id']).organization_id, row['organization_id']
            end
            assert @result['organizations'].length == 2
            expected_org = @agent.organization
            actual_org = @result['organizations'][0]
            assert_equal expected_org.id, actual_org['id']
            assert_equal expected_org.name, actual_org['name']
            actual_org['url'].must_include "/api/v2/organizations/#{expected_org.id}.json"
          end
        end

        describe 'grouping' do
          it 'side-loads organizations' do
            create_and_execute_view setup_params(
              "all" => [{"field" => "organization_id", "operator" => "is_not", "value" => @admin.organization_id }],
              "execution" => {
                "group" => {"id" => "organization_id", "order" => "asc"},
                "columns" => ["id", "name"]
              }
            )
            @result["rows"].each do |row|
              assert row.keys.include?('organization_id'), 'organization_id key is missing'
              next if row['organization_id'].nil?
              assert_equal @account.users.find(row['id']).organization_id, row['organization_id']
            end
            assert @result['organizations'].length == 2
            expected_org = @agent.organization
            actual_org = @result['organizations'][0]
            assert_equal expected_org.id, actual_org['id']
            assert_equal expected_org.name, actual_org['name']
            actual_org['url'].must_include "/api/v2/organizations/#{expected_org.id}.json"
          end
        end

        ['group', 'sort'].each do |type|
          describe "ordering #{type}" do
            before do
              @organizations = @account.organizations
              @account.users.each_with_index do |user, index|
                selection_index = index % @organizations.length
                user.update_attribute(:organization_id, @organizations[selection_index].id)
              end
            end

            it 'ascends in order' do
              create_and_execute_view setup_params(
                "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                "execution" => {
                  type => {"id" => "organization_id", "order" => "asc"},
                  "columns" => ["id", "name", "organization_id"]
                }
              )
              actual_organizations = @result["rows"].map { |r| r['organization_id'] }
              expected_organizations = @account.users.map(&:organization).sort_by { |org| org.name.downcase }.map(&:id)
              assert_equal expected_organizations, actual_organizations
            end

            it 'descends in order' do
              create_and_execute_view setup_params(
                "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                "execution" => {
                  type => {"id" => "organization_id", "order" => "desc"},
                  "columns" => ["id", "name", "organization_id"]
                }
              )
              actual_organizations = @result["rows"].map { |r| r['organization_id'] }
              expected_organizations = @account.users.map(&:organization).sort_by { |org| org.name.downcase }.map(&:id).reverse
              assert_equal expected_organizations, actual_organizations
            end
          end
        end
      end

      describe "current_tags" do
        before do
          @admin.update_attribute(:current_tags, "round_1,round_3")
          @admin.save!
          @agent.update_attribute(:current_tags, "round_2,round_3")
          @agent.save!
          @end_user.update_attribute(:current_tags, nil)
        end

        describe "when account does not have user and organization tags" do
          before do
            Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
            @view = create_view(setup_params(
              "all" => [
                {
                  "field" => "current_tags",
                  "operator" => "includes",
                  "value" => ["round_1"]
                }
              ]
            ))
            Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
          end

          it "raises validation error on execute" do
            get :execute, params: { id: @view.id }

            assert_response :unprocessable_entity
            assert_equal({
              "error"       => "RecordInvalid",
              "description" => "Record validation errors",
              "details"     => {
                "base" => [
                  {
                    "error"       => "InvalidSource",
                    "description" => "'current_tags includes [\"round_1\"]' is an invalid condition. 'current_tags' is not a valid source."
                  }
                ]
              }
            }, JSON.parse(@response.body))
          end
        end

        describe "when account has user and organization tags" do
          before do
            Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
          end

          it 'executes view including one tag' do
            create_and_execute_view setup_params("all" => [{"field" => "current_tags", "operator" => "includes", "value" => ["round_1"]}])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
          end

          it 'executes view including multiple tags' do
            create_and_execute_view setup_params("all" => [{"field" => "current_tags", "operator" => "includes", "value" => ["round_1", "round_2"]}])
            assert_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it 'executes view not including one tag' do
            create_and_execute_view setup_params("all" => [{"field" => "current_tags", "operator" => "not_includes", "value" => ["round_1"]}])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it 'executes view not including another tag' do
            create_and_execute_view setup_params("all" => [{"field" => "current_tags", "operator" => "not_includes", "value" => ["round_3"]}])
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it 'executes view not including multiple tags' do
            create_and_execute_view setup_params("all" => [{"field" => "current_tags", "operator" => "not_includes", "value" => ["round_1", "round_2"]}])
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it 'executes view with all and any conditions not including tags' do
            create_and_execute_view setup_params("all" => [{"field" => "current_tags", "operator" => "includes", "value" => ["round_1"]}],
                                                 "any" => [{"field" => "current_tags", "operator" => "not_includes", "value" => ["round_2"]}])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it 'executes view with all and any conditions not including tags 2' do
            create_and_execute_view setup_params("all" => [{"field" => "current_tags", "operator" => "includes", "value" => ["round_1"]}],
                                                 "any" => [{"field" => "current_tags", "operator" => "not_includes", "value" => ["round_3"]}])
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it 'executes view with all and any conditions mixed with user attribute' do
            create_and_execute_view setup_params(
              "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
              "any" => [
                {"field" => "current_tags", "operator" => "not_includes", "value" => ["round_3"]},
                {"field" => "name", "operator" => "is", "value" => @admin.name}
              ]
            )
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          describe "when user fulfills multiple tags" do
            it "returns user once" do
              create_and_execute_view setup_params("all" => [{"field" => "current_tags", "operator" => "includes", "value" => ["round_1", "round_2", "round_3"]}])
              assert_equal [@admin.id, @agent.id].sort, @user_ids.sort
            end
          end

          describe 'with other fields' do
            before do
              @admin.custom_field_values.update_from_hash("plan_type" => "gold", "ltv" => 10000)
              @admin.save!
              @agent.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 5700.25)
              @agent.save!
              @end_user.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 2800)
              @end_user.save!
            end

            it 'executes view with all and any conditions mixed with dropdown field' do
              create_and_execute_view setup_params(
                "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                "any" => [
                  {"field" => "current_tags", "operator" => "not_includes", "value" => ["round_3"]},
                  {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold"}
                ]
              )
              assert_includes @user_ids, @admin.id
              refute_includes @user_ids, @agent.id
              assert_includes @user_ids, @end_user.id
            end

            it 'executes view with all and any conditions mixed with custom field' do
              create_and_execute_view setup_params(
                "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                "any" => [
                  {"field" => "current_tags", "operator" => "not_includes", "value" => ["round_3"]},
                  {"field" => "custom_fields.ltv", "operator" => "is", "value" => 10000}
                ]
              )
              assert_includes @user_ids, @admin.id
              refute_includes @user_ids, @agent.id
              assert_includes @user_ids, @end_user.id
            end
          end

          it "includes current tags in rows" do
            create_and_execute_view setup_params(
              "all" => [{"field" => "current_tags", "operator" => "includes", "value" => ["round_1", "round_2"]}],
              "execution" => {
                "columns" => ["id", "current_tags"],
                "sort" => {"id" => "name", "order" => "asc"}
              }
            )
            actual_tags = @result['rows'].map { |r| r['current_tags'] }
            expected_tags = [@admin, @agent].sort_by(&:name).map(&:tags)
            assert_equal expected_tags, actual_tags
          end
        end
      end

      describe "custom fields" do
        describe "integer" do
          before do
            create_user_custom_field!("age", "age", "Integer")
            @admin.custom_field_values.update_from_hash("age" => "10")
            @admin.save!
            @agent.custom_field_values.update_from_hash("age" => "20")
            @agent.save!
            @end_user.custom_field_values.update_from_hash("age" => "30")
            @end_user.save!
          end

          it "less than" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "less_than", "value" => "20"}])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "less than or equal" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "less_than_or_equal_to", "value" => "20"}])
            assert_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "greater_thans" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "greater_than", "value" => "20"}])
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it "greater_thans or equal" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "greater_than_or_equal_to", "value" => "20"}])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it "equals" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "is", "value" => "20"}])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "does not equal" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "is_not", "value" => "20"}])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it "is nil" do
            @end_user.custom_field_values.update_from_hash("age" => nil)
            @end_user.save!

            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "is", "value" => nil}])
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end
        end

        describe "presence" do
          before do
            create_user_custom_field!("age", "age", "Integer")
            @admin.custom_field_values.update_from_hash("age" => "10")
            @admin.save!
          end

          it 'executes view with field' do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "is_present"}])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it 'executes view without field' do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "is_not_present"}])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end
        end

        describe "decimal" do
          before do
            create_user_custom_field!("age", "age", "Decimal")
            @admin.custom_field_values.update_from_hash("age" => "10.5")
            @admin.save!
            @agent.custom_field_values.update_from_hash("age" => "20.5")
            @agent.save!
            @end_user.custom_field_values.update_from_hash("age" => "30.5")
            @end_user.save!
          end

          it "less than" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "less_than", "value" => 20.5}])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "less than float with same value as integer" do
            @agent.custom_field_values.update_from_hash("age" => 20)
            @agent.save!

            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "less_than", "value" => 20}])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "less than or equal" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "less_than_or_equal_to", "value" => 20.5}])
            assert_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "greater_thans" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "greater_than", "value" => 20.5}])
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it "greater_thans or equal" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "greater_than_or_equal_to", "value" => 20.5}])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it "equals" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "is", "value" => "20.5"}])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "does not equal" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "is_not", "value" => "20.5"}])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end

          it "is nil" do
            @end_user.custom_field_values.update_from_hash("age" => nil)
            @end_user.save!

            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.age", "operator" => "is", "value" => nil}])
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end
        end

        describe "checkbox" do
          before do
            create_user_custom_field!("paid", "paid", "Checkbox")
            @admin.custom_field_values.update_from_hash("paid" => "true")
            @admin.save!
            @agent.custom_field_values.update_from_hash("paid" => "false")
            @agent.save!
          end

          it "equals" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.paid", "operator" => "is", "value" => "true"}])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
          end

          it "does not equal" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.paid", "operator" => "is_not", "value" => "true"}])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
          end
        end

        ["Textarea", "Text"].each do |type|
          describe "#{type} field" do
            before do
              create_user_custom_field!("url", "url", type)
              @admin.custom_field_values.update_from_hash("url" => "http://linkedin.com")
              @admin.save!
              @agent.custom_field_values.update_from_hash("url" => "http://google.com")
              @agent.save!
            end

            it "equals" do
              create_and_execute_view setup_params("all" => [{"field" => "custom_fields.url", "operator" => "is", "value" => "http://linkedin.com"}])
              assert_includes @user_ids, @admin.id
              refute_includes @user_ids, @agent.id
              refute_includes @user_ids, @end_user.id
            end

            it "does not equal" do
              create_and_execute_view setup_params("all" => [{"field" => "custom_fields.url", "operator" => "is_not", "value" => "http://linkedin.com"}])
              refute_includes @user_ids, @admin.id
              assert_includes @user_ids, @agent.id
              assert_includes @user_ids, @end_user.id
            end

            it "does not equal nil" do
              create_and_execute_view setup_params("all" => [{"field" => "custom_fields.url", "operator" => "is_not", "value" => nil}])
              assert_includes @user_ids, @admin.id
              assert_includes @user_ids, @agent.id
              refute_includes @user_ids, @end_user.id
            end

            it "equals nil in any conditions" do
              @admin.custom_field_values.update_from_hash("url" => nil)
              @admin.save!

              create_and_execute_view setup_params(
                "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                "any" => [
                  {"field" => "custom_fields.url", "operator" => "is", "value" => nil}
                ]
              )
              assert_includes @user_ids, @admin.id
              refute_includes @user_ids, @agent.id
              assert_includes @user_ids, @end_user.id
            end

            it "does not equal in any conditions" do
              create_and_execute_view setup_params(
                "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
                "any" => [
                  {"field" => "custom_fields.url", "operator" => "is_not", "value" => "http://linkedin.com"},
                  {"field" => "custom_fields.url", "operator" => "is_not", "value" => "http://google.com"}
                ]
              )
              assert_includes @user_ids, @admin.id
              assert_includes @user_ids, @agent.id
              assert_includes @user_ids, @end_user.id
            end

            it "does not equal nil in any conditions" do
              create_and_execute_view setup_params("all" => [{"field" => "custom_fields.url", "operator" => "is_not", "value" => nil}])
              assert_includes @user_ids, @admin.id
              assert_includes @user_ids, @agent.id
              refute_includes @user_ids, @end_user.id
            end
          end
        end

        describe "regexp field" do
          before do
            create_user_custom_field!("matcher", "matcher", "Regexp", regexp_for_validation: "[0-9]+")
            @admin.custom_field_values.update_from_hash("matcher" => "123")
            @admin.save!
            @agent.custom_field_values.update_from_hash("matcher" => "234")
            @agent.save!
          end

          it "equals" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.matcher", "operator" => "is", "value" => "123"}])
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
          end

          it "does not equal" do
            create_and_execute_view setup_params("all" => [{"field" => "custom_fields.matcher", "operator" => "is_not", "value" => "123"}])
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
          end
        end
      end

      describe "channels" do
        describe "email" do
          it "includes email in columns and rows" do
            columns = ["created_at", "email"]
            create_and_execute_view setup_params(
              all_conditions.merge(
                "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is_not", "value" => "bronze"}],
                "execution" => { "columns" => columns }
              )
            )
            actual_columns = @result["columns"]
            assert_equal({"id" => "created_at", "title" => "Created"}, actual_columns[0])
            assert_equal({"id" => "email", "title" => "Email"}, actual_columns[1])

            rows = @result["rows"]
            rows.each do |row|
              user = @account.users.find(row['id'])
              assert_equal_with_nil user.email, row['email']
            end
          end
        end

        describe "twitter" do
          before do
            UserTwitterIdentity.destroy_all
            @admin.identities << UserTwitterIdentity.new(user: @admin, value: channels_user_profiles(:minimum_twitter_user_profile_1).external_id.to_s)
            @admin.save!

            @agent.identities << UserTwitterIdentity.new(user: @agent, value: channels_user_profiles(:minimum_twitter_user_profile_2).external_id.to_s)
            @agent.save!
          end

          it "includes twitter handle in columns and rows" do
            columns = ["created_at", "twitter"]
            create_and_execute_view setup_params(
              all_conditions.merge(
                "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is_not", "value" => "bronze"}],
                "execution" => { "columns" => columns }
              )
            )

            actual_columns = @result["columns"]
            assert_equal({"id" => "created_at", "title" => "Created"}, actual_columns[0])
            assert_equal({"id" => "twitter", "title" => "Twitter"}, actual_columns[1])

            had_twitter_user = nil
            rows = @result["rows"]
            rows.each do |row|
              user = @account.users.find(row['id'])
              next unless user.twitter_profile

              assert_equal user.twitter_profile.screen_name, row['twitter']
              had_twitter_user ||= true
            end
            assert had_twitter_user, 'expected to have at least one twitter user'
          end
        end

        describe "facebook" do
          before do
            @admin.identities << UserFacebookIdentity.new(user: @admin, value: '12345')
            @admin.save!

            @agent.identities << UserFacebookIdentity.new(user: @agent, value: '56789')
            @agent.save!
          end

          it "includes facebook in columns and rows" do
            columns = ["created_at", "facebook"]
            create_and_execute_view setup_params(
              all_conditions.merge(
                "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is_not", "value" => "bronze"}],
                "execution" => { "columns" => columns }
              )
            )

            actual_columns = @result["columns"]
            assert_equal({"id" => "created_at", "title" => "Created"}, actual_columns[0])
            assert_equal({"id" => "facebook", "title" => "Facebook"}, actual_columns[1])

            had_facebook_user = nil
            rows = @result["rows"]
            rows.each do |row|
              user = @account.users.find(row['id'])
              next unless user.identities.facebook.exists?

              assert_equal user.identities.facebook.first.value, row['facebook']
              had_facebook_user ||= true
            end
            assert had_facebook_user, 'expected to have at least one facebook user'
          end
        end

        describe "google" do
          before do
            @admin.identities << UserEmailIdentity.new(user: @admin, value: 'aa@example.com')
            @admin.save!

            @agent.identities << UserEmailIdentity.new(user: @agent, value: 'bb@example.com')
            @agent.save!

            @admin.identities.last.build_google_profile.save!
            @agent.identities.last.build_google_profile.save!
          end

          it "includes email in columns and rows" do
            columns = ["created_at", "google", "email"]
            create_and_execute_view setup_params(
              all_conditions.merge(
                "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is_not", "value" => "bronze"}],
                "execution" => { "columns" => columns }
              )
            )

            actual_columns = @result["columns"]
            assert_equal({"id" => "created_at", "title" => "Created"}, actual_columns[0])
            assert_equal({"id" => "google", "title" => "Google"}, actual_columns[1])

            had_google_user = nil
            rows = @result["rows"]
            rows.each do |row|
              user = @account.users.find(row['id'])
              if user.has_google_identity?
                assert_equal user.identities.all.find(&:google?).value, row['google']
                assert_equal user.email, row['email']

                had_google_user ||= true
              else
                assert_equal_with_nil user.email, row['email']
                assert_nil row['google']
              end
            end
            assert had_google_user, 'expected to have at least one google user'
          end
        end
      end

      describe "time_zone" do
        let(:time_zones) { ['Rome', 'Alaska', 'Pacific Time (US & Canada)', nil] }

        before do
          User.any_instance.stubs(:cannot_select_time_zone?).returns(false)

          @account.users.zip(time_zones.cycle).each do |user, time_zone|
            user.update_attribute(:time_zone, time_zone)
          end
        end

        describe "when added as a column" do
          before do
            create_and_execute_view setup_params(
              "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
              "execution" => {
                "columns" => ["id", "time_zone"]
              }
            )
          end

          it "includes time_zone column" do
            assert_equal "time_zone", @result['columns'][1]["id"]
          end

          it "includes time_zone in rows" do
            assert @result['rows'].all? { |row| row.key?('time_zone') }
          end
        end

        describe "when user doesn't have a Time Zone" do
          before do
            create_and_execute_view setup_params(
              "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
              "execution" => {
                "columns" => ["id", "time_zone"]
              }
            )
          end

          it "defaults to Account's Time Zone" do
            assert @result['rows'].none? { |row| row['time_zone'].nil? }
          end
        end

        ['group', 'sort'].each do |type|
          describe "when #{type}ing" do
            let(:expected_time_zones) do
              @account.admins.
                order("IFNULL(time_zone, '#{@account.time_zone}') #{order}").
                pluck(:time_zone).
                map { |time_zone| time_zone || @account.time_zone }
            end

            before do
              create_and_execute_view setup_params(
                "all" => [{"field" => "roles", "operator" => "is", "value" => "admin"}],
                "execution" => {
                  type => {"id" => "time_zone", "order" => order},
                  "columns" => ["id", "time_zone"]
                }
              )
            end

            describe "and order is ascending" do
              let(:order) { "asc" }

              it "ascends in order" do
                actual_time_zones = @result["rows"].map { |row| row['time_zone'] }

                assert_equal expected_time_zones, actual_time_zones
              end
            end

            describe "and order is descending" do
              let(:order) { "desc" }

              it "descends in order" do
                actual_time_zones = @result["rows"].map { |row| row['time_zone'] }

                assert_equal expected_time_zones, actual_time_zones
              end
            end
          end
        end
      end

      it "does not blow up when sorting is nil but sort is sent as a param" do
        view = create_view setup_params("all" => [{"field" => "name", "operator" => "is_not", "value" => nil}])
        view.output.sort = nil
        view.save!
        get :execute, params: { id: view.id, sort_by: "created_at", sort_order: "asc" }
        assert_response 200
      end

      it "does not blow up when sorting is nil but an invalid sort attribute is sent as a param" do
        view = create_view setup_params("all" => [{"field" => "name", "operator" => "is_not", "value" => nil}])
        view.output.sort = nil
        view.save!
        get :execute, params: { id: view.id, sort_by: "123123", sort_order: "asc" }
        assert_response 200
      end

      describe "pagination" do
        it "honors per_page limit" do
          view = create_view setup_params(
            "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
            "execution" => {
              "columns" => ["id"],
              "sort" => {"id" => "name", "order" => "asc"}
            }
          )
          get :execute, params: { id: view.id, per_page: 5 }
          @result = JSON.parse(@response.body)
          @user_ids = @result["rows"].map { |user| user["id"] }
          @all_user_ids = @account.users.order(:name).to_a.map(&:id)

          assert_equal 13, @result['count']
          assert_equal 5, @result['rows'].length
          assert_equal @all_user_ids[0...5], @user_ids
        end

        it "honors page" do
          view = create_view setup_params(
            "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
            "execution" => {
              "columns" => ["id"],
              "sort" => {"id" => "name", "order" => "asc"}
            }
          )
          get :execute, params: { id: view.id, per_page: 5, page: 2 }
          @result = JSON.parse(@response.body)
          @user_ids = @result["rows"].map { |user| user["id"] }
          @all_user_ids = @account.users.order(:name).to_a.map(&:id)

          assert_equal 13, @result['count']
          assert_equal 5, @result['rows'].length
          assert_equal @all_user_ids[5...10], @user_ids
        end

        it "honors page by getting second to last page" do
          view = create_view setup_params(
            "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
            "execution" => {
              "columns" => ["id"],
              "sort" => {"id" => "name", "order" => "asc"}
            }
          )
          get :execute, params: { id: view.id, per_page: 5, page: 2 }
          @result = JSON.parse(@response.body)
          @user_ids = @result["rows"].map { |user| user["id"] }
          @all_user_ids = @account.users.order(:name).to_a.map(&:id)

          assert_equal 13, @result['count']
          assert_equal 5, @result['rows'].length
          assert_equal @all_user_ids[5...10], @user_ids
        end

        it "honors page by getting last page" do
          view = create_view setup_params(
            "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
            "execution" => {
              "columns" => ["id"],
              "sort" => {"id" => "name", "order" => "asc"}
            }
          )
          get :execute, params: { id: view.id, per_page: 5, page: 3 }
          @result = JSON.parse(@response.body)
          @user_ids = @result["rows"].map { |user| user["id"] }
          @all_user_ids = @account.users.order(:name).to_a.map(&:id)

          assert_equal 13, @result['count']
          assert_equal 3, @result['rows'].length
          assert_equal @all_user_ids[10...13], @user_ids
        end
      end

      describe "pagination for many users" do
        before do
          FactoryBot.create_list(:end_user, 3, account: @account)
        end

        it "maxs per page should be 2" do
          view = create_view setup_params(
            "all" => [{"field" => "name", "operator" => "is_not", "value" => nil}],
            "execution" => {
              "columns" => ["id"],
              "sort" => {"id" => "created_at", "order" => "asc"}
            }
          )

          get :execute, params: { id: view.id, per_page: 2 }
          @result = JSON.parse(@response.body)

          assert_equal 2, @result['rows'].length
        end
      end

      describe "trusted requests" do
        before do
          configure_user
          view = create_view setup_params(all_conditions)
          2.times do
            get :execute, params: { id: view.id }
          end
        end

        describe 'when from Lotus' do
          let(:configure_user) { set_header('X-Zendesk-Lotus-Version', 'v1.2.3') }
          it "does not throttle" do
            assert_response :ok
          end
        end

        describe 'when from system user' do
          let(:configure_user) { @controller.send(:current_user).stubs(:is_system_user?).returns(true) }
          it "does not throttle" do
            assert_response :ok
          end
        end

        describe 'when from system user on behalf of another user' do
          let(:configure_user) { @controller.stubs(:signed_as_system_user?).returns(true) }
          it "does not throttle " do
            assert_response :ok
          end
        end
      end

      describe "trusted requests when view is already throttled" do
        describe_with_arturo_enabled :ocp_user_views_new_limit do
          before do
            @controller.stubs(:signed_as_system_user?).returns(true)
            view = create_view setup_params(all_conditions)
            Prop.stubs(:throttled?).with(:execute_user_view_2, [@account.id, view.id]).returns(true)
            Prop.stubs(:throttled?).with(:api_unauthorized_requests, '0.0.0.0').returns(false)

            2.times do
              get :execute, params: { id: view.id }
            end
          end

          it "is throttled" do
            assert_response 429
            assert_equal 'Rate limit for viewing this customer list exceeded. Please wait before viewing this customer list again.', @response.body
            assert_equal Mime[:text].to_s, @response.content_type
            assert @response.headers.include?("Retry-After")
          end
        end

        describe_with_arturo_disabled :ocp_user_views_new_limit do
          before do
            @controller.stubs(:signed_as_system_user?).returns(true)
            view = create_view setup_params(all_conditions)
            Prop.stubs(:throttled?).with(:execute_user_view, [@account.id, view.id]).returns(true)
            Prop.stubs(:throttled?).with(:api_unauthorized_requests, '0.0.0.0').returns(false)

            get :execute, params: { id: view.id }
            sleep 3 # rubocop:disable Lint/Sleep
            get :execute, params: { id: view.id }
          end

          it "is throttled" do
            assert_response 429
            assert_equal 'Rate limit for viewing this customer list exceeded. Please wait before viewing this customer list again.', @response.body
            assert_equal Mime[:text].to_s, @response.content_type
            assert @response.headers.include?("Retry-After")
          end
        end
      end

      describe "not from Lotus" do
        describe "with multiple executions on the same view" do
          before do
            view = create_view setup_params(all_conditions)
            2.times do
              get :execute, params: { id: view.id }
            end
          end

          it "is throttled to run once every 10 minutes" do
            assert_response 429
            assert_equal 'Rate limit for viewing this customer list exceeded. Please wait before viewing this customer list again.', @response.body
            assert @response.headers.include?("Retry-After")
          end
        end

        describe_with_arturo_enabled :ocp_user_views_new_limit do
          it "throttles specific to account and view" do
            view = create_view setup_params(all_conditions)
            Prop.expects(:throttle!).with(:execute_user_view_2, [@account.id, view.id])
            get :execute, params: { id: view.id }
          end
        end

        describe_with_arturo_disabled :ocp_user_views_new_limit do
          it "throttles specific to account and view" do
            view = create_view setup_params(all_conditions)
            Prop.expects(:throttle!).with(:execute_user_view, [@account.id, view.id])
            get :execute, params: { id: view.id }
          end
        end
      end

      describe "with sampling parameter" do
        it "does not sample customer list" do
          Zendesk::UserViews::Executer.expects(:users_for_view).
            with(anything, anything, has_entry(sampling: false)).
            returns([])

          view = create_view setup_params(all_conditions)
          get :execute, params: { id: view.id, sampling: false }

          assert_response :ok
        end
      end

      describe "with multiple users" do
        before do
          @agent_2 = FactoryBot.create(
            :agent,
            account: @account,
            name: "Agent 2",
            created_at: 1.year.ago
          )
          @agent_3 = FactoryBot.create(
            :agent,
            account: @account,
            name: "Agent 3",
            created_at: 2.years.ago
          )
          @agent_4 = FactoryBot.create(
            :agent,
            account: @account,
            name: "Agent 4",
            created_at: 3.years.ago
          )
          @admin_2 = FactoryBot.create(
            :verified_admin,
            account: @account,
            name: "Verified Admin 2",
            created_at: 4.years.ago
          )
        end

        describe "basic conditions" do
          before do
            @admin.custom_field_values.update_from_hash("plan_type" => "gold", "ltv" => 10000)
            @admin.save!
            @agent.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 5700.25)
            @agent.save!
            @end_user.custom_field_values.update_from_hash("plan_type" => "silver", "ltv" => 2800)
            @end_user.save!
          end

          it "executes with all conditions" do
            create_and_execute_view(
              "all" => [
                {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"},
                {"field" => "custom_fields.ltv", "operator" => "less_than", "value" => 6000}
              ]
            )
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
            refute_includes @user_ids, @agent_2.id
            refute_includes @user_ids, @agent_3.id
          end

          it "executes with all conditions 2" do
            create_and_execute_view(
              "all" => [
                {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"},
                {"field" => "custom_fields.ltv", "operator" => "less_than", "value" => 5000}
              ]
            )
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
            refute_includes @user_ids, @agent_2.id
            refute_includes @user_ids, @agent_3.id
          end

          it "executes with any and all conditions" do
            create_and_execute_view(
              "all" => [{ "field" => "custom_fields.ltv", "operator" => "less_than", "value" => 6000 }],
              "any" => [
                {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver"},
                {"field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold"}
              ]
            )
            refute_includes @user_ids, @admin.id
            assert_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
            refute_includes @user_ids, @agent_2.id
            refute_includes @user_ids, @agent_3.id
          end

          it "executes with any and all conditions 2" do
            create_and_execute_view(
              "all" => [{"field" => "custom_fields.ltv", "operator" => "less_than", "value" => 6000}],
              "any" => [{"field" => "custom_fields.plan_type", "operator" => "is", "value" => "bronze"}]
            )
            assert_empty @user_ids
          end
        end

        describe "roles" do
          before do
            @account.subscription.update_attributes!(base_agents: 30)
            @permission_set_1 = @account.permission_sets.create!(name: 'Permission Set 1')
            @permission_set_2 = @account.permission_sets.create!(name: 'Permission Set 2')
            @permission_set_3 = @account.permission_sets.create!(name: 'Permission Set 3')

            @agent_2.role_or_permission_set = @permission_set_1
            @agent_2.save!
            @agent_with_permission_set_1 = @agent_2
            @agent_3.role_or_permission_set = @permission_set_2
            @agent_3.save!
            @agent_with_permission_set_2 = @agent_3
            @agent_4.role_or_permission_set = @permission_set_3
            @agent_4.save!
            @agent_with_permission_set_3 = @agent_4

            @end_user_with_permission_set_1 = FactoryBot.create(:end_user,
              account: @account,
              name: "End User With Permission Set 1",
              created_at: 5.years.ago).tap do |user|
              user.permission_set = @permission_set_1
              user.save
            end
            @admin_with_permission_set_2 = FactoryBot.create(:verified_admin,
              account: @account,
              name: "Admin With Permission Set 2",
              created_at: 6.years.ago).tap do |user|
              user.permission_set = @permission_set_2
              user.save
            end
          end

          it "filters by admin role" do
            create_and_execute_view(
              "all" => [
                {"field" => "roles", "operator" => "is", "value" => "admin"}
              ]
            )
            assert_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            refute_includes @user_ids, @end_user.id
            refute_includes @user_ids, @agent_with_permission_set_1.id
            refute_includes @user_ids, @agent_with_permission_set_2.id
            refute_includes @user_ids, @agent_with_permission_set_3.id
            assert_includes @user_ids, @admin_2.id
            refute_includes @user_ids, @end_user_with_permission_set_1.id
            assert_includes @user_ids, @admin_with_permission_set_2.id
          end

          it "filters by end-user role" do
            create_and_execute_view(
              "all" => [
                {"field" => "roles", "operator" => "is", "value" => "end-user"}
              ]
            )
            refute_includes @user_ids, @admin.id
            refute_includes @user_ids, @agent.id
            assert_includes @user_ids, @end_user.id
            refute_includes @user_ids, @agent_with_permission_set_1.id
            refute_includes @user_ids, @agent_with_permission_set_2.id
            refute_includes @user_ids, @agent_with_permission_set_3.id
            refute_includes @user_ids, @admin_2.id
            assert_includes @user_ids, @end_user_with_permission_set_1.id
            refute_includes @user_ids, @admin_with_permission_set_2.id
          end

          describe "enterprise_roles_in_user_views enabled" do
            before do
              Account.any_instance.stubs(:enterprise_roles_in_user_views_enabled?).returns(true)
              Account.any_instance.stubs(:has_permission_sets?).returns(true)
              @controller.stubs(:throttle_view)
            end

            describe "with positive operator" do
              it "filters by admin role" do
                create_and_execute_view(
                  "all" => [
                    {"field" => "roles", "operator" => "is", "value" => "admin"}
                  ]
                )
                assert_includes @user_ids, @admin.id
                refute_includes @user_ids, @agent.id
                refute_includes @user_ids, @end_user.id
                refute_includes @user_ids, @agent_with_permission_set_1.id
                refute_includes @user_ids, @agent_with_permission_set_2.id
                refute_includes @user_ids, @agent_with_permission_set_3.id
                assert_includes @user_ids, @admin_2.id
                refute_includes @user_ids, @end_user_with_permission_set_1.id
                assert_includes @user_ids, @admin_with_permission_set_2.id
              end

              it "filters by end-user role" do
                create_and_execute_view(
                  "all" => [
                    {"field" => "roles", "operator" => "is", "value" => "end-user"}
                  ]
                )
                refute_includes @user_ids, @admin.id
                refute_includes @user_ids, @agent.id
                assert_includes @user_ids, @end_user.id
                refute_includes @user_ids, @agent_with_permission_set_1.id
                refute_includes @user_ids, @agent_with_permission_set_2.id
                refute_includes @user_ids, @agent_with_permission_set_3.id
                refute_includes @user_ids, @admin_2.id
                assert_includes @user_ids, @end_user_with_permission_set_1.id
                refute_includes @user_ids, @admin_with_permission_set_2.id
              end

              it "excludes end-users when filtered by permission set ID" do
                create_and_execute_view(
                  "all" => [
                    {"field" => "roles", "operator" => "is", "value" => "permission_set_id:#{@permission_set_1.id}"}
                  ]
                )
                refute_includes @user_ids, @admin.id
                refute_includes @user_ids, @agent.id
                refute_includes @user_ids, @end_user.id
                assert_includes @user_ids, @agent_with_permission_set_1.id
                refute_includes @user_ids, @agent_with_permission_set_2.id
                refute_includes @user_ids, @agent_with_permission_set_3.id
                refute_includes @user_ids, @admin_2.id
                refute_includes @user_ids, @end_user_with_permission_set_1.id # End-user overrides permission set
                refute_includes @user_ids, @admin_with_permission_set_2.id
              end

              it "includes agents when filtered by permission set ID" do
                create_and_execute_view(
                  "all" => [
                    {"field" => "roles", "operator" => "is", "value" => "permission_set_id:#{@permission_set_2.id}"}
                  ]
                )
                refute_includes @user_ids, @admin.id
                refute_includes @user_ids, @agent.id
                refute_includes @user_ids, @end_user.id
                refute_includes @user_ids, @agent_with_permission_set_1.id
                assert_includes @user_ids, @agent_with_permission_set_2.id
                refute_includes @user_ids, @agent_with_permission_set_3.id
                refute_includes @user_ids, @admin_2.id
                refute_includes @user_ids, @end_user_with_permission_set_1.id
                refute_includes @user_ids, @admin_with_permission_set_2.id # Admin overrides permission set
              end

              it "excludes admins when filtered by permission set ID" do
                create_and_execute_view(
                  "all" => [
                    {"field" => "roles", "operator" => "is", "value" => "permission_set_id:#{@permission_set_3.id}"}
                  ]
                )
                refute_includes @user_ids, @admin.id
                refute_includes @user_ids, @agent.id
                refute_includes @user_ids, @end_user.id
                refute_includes @user_ids, @agent_with_permission_set_1.id
                refute_includes @user_ids, @agent_with_permission_set_2.id
                assert_includes @user_ids, @agent_with_permission_set_3.id
                refute_includes @user_ids, @admin_2.id
                refute_includes @user_ids, @end_user_with_permission_set_1.id
                refute_includes @user_ids, @admin_with_permission_set_2.id
              end
            end

            describe "with negative operator" do
              it "filters by admin role" do
                create_and_execute_view(
                  "all" => [
                    {"field" => "roles", "operator" => "is_not", "value" => "admin"}
                  ]
                )
                refute_includes @user_ids, @admin.id
                assert_includes @user_ids, @agent.id
                assert_includes @user_ids, @end_user.id
                assert_includes @user_ids, @agent_with_permission_set_1.id
                assert_includes @user_ids, @agent_with_permission_set_2.id
                assert_includes @user_ids, @agent_with_permission_set_3.id
                refute_includes @user_ids, @admin_2.id
                assert_includes @user_ids, @end_user_with_permission_set_1.id
                refute_includes @user_ids, @admin_with_permission_set_2.id
              end

              it "filters by end-user role" do
                create_and_execute_view(
                  "all" => [
                    {"field" => "roles", "operator" => "is_not", "value" => "end-user"}
                  ]
                )
                assert_includes @user_ids, @admin.id
                assert_includes @user_ids, @agent.id
                refute_includes @user_ids, @end_user.id
                assert_includes @user_ids, @agent_with_permission_set_1.id
                assert_includes @user_ids, @agent_with_permission_set_2.id
                assert_includes @user_ids, @agent_with_permission_set_3.id
                assert_includes @user_ids, @admin_2.id
                refute_includes @user_ids, @end_user_with_permission_set_1.id
                assert_includes @user_ids, @admin_with_permission_set_2.id
              end

              it "includes end-users with permission sets when filtered by permission set ID" do
                create_and_execute_view(
                  "all" => [
                    {"field" => "roles", "operator" => "is_not", "value" => "permission_set_id:#{@permission_set_1.id}"}
                  ]
                )
                assert_includes @user_ids, @admin.id
                assert_includes @user_ids, @agent.id
                assert_includes @user_ids, @end_user.id
                refute_includes @user_ids, @agent_with_permission_set_1.id
                assert_includes @user_ids, @agent_with_permission_set_2.id
                assert_includes @user_ids, @agent_with_permission_set_3.id
                assert_includes @user_ids, @admin_2.id
                assert_includes @user_ids, @end_user_with_permission_set_1.id # End-user overrides permission set
                assert_includes @user_ids, @admin_with_permission_set_2.id
              end

              it "excludes agents when filtered by permission set ID" do
                create_and_execute_view(
                  "all" => [
                    {"field" => "roles", "operator" => "is_not", "value" => "permission_set_id:#{@permission_set_2.id}"}
                  ]
                )
                assert_includes @user_ids, @admin.id
                assert_includes @user_ids, @agent.id
                assert_includes @user_ids, @end_user.id
                assert_includes @user_ids, @agent_with_permission_set_1.id
                refute_includes @user_ids, @agent_with_permission_set_2.id
                assert_includes @user_ids, @agent_with_permission_set_3.id
                assert_includes @user_ids, @admin_2.id
                assert_includes @user_ids, @end_user_with_permission_set_1.id
                assert_includes @user_ids, @admin_with_permission_set_2.id # Admin overrides permission set
              end

              it "includes admins with permission set when filtered by permission set ID" do
                create_and_execute_view(
                  "all" => [
                    {"field" => "roles", "operator" => "is_not", "value" => "permission_set_id:#{@permission_set_3.id}"}
                  ]
                )
                assert_includes @user_ids, @admin.id
                assert_includes @user_ids, @agent.id
                assert_includes @user_ids, @end_user.id
                assert_includes @user_ids, @agent_with_permission_set_1.id
                assert_includes @user_ids, @agent_with_permission_set_2.id
                refute_includes @user_ids, @agent_with_permission_set_3.id
                assert_includes @user_ids, @admin_2.id
                assert_includes @user_ids, @end_user_with_permission_set_1.id
                assert_includes @user_ids, @admin_with_permission_set_2.id
              end
            end
          end
        end
      end
    end
  end
end
