require_relative "../../../../../support/test_helper"
require_relative "../../../../../support/user_views_test_helper"
require_relative "../../../../../support/custom_fields_test_helper"
require_relative "../../../../../support/api_test_helper"

SingleCov.not_covered! # test was split into multiple parts because it took too long

describe Api::V2::Rules::UserViewsController do
  extend Api::V2::TestHelper
  include UserViewsTestHelper
  include CustomFieldsTestHelper
  include ApiTestHelper

  fixtures :all

  def create_custom_fields
    create_dropdown_field("plan_type", "Plan",
      [{ name: "Gold",   value: "gold" },
       { name: "Silver", value: "silver" },
       { name: "Bronze", value: "bronze" }])
    create_user_custom_field!("ltv", "LTV", "Decimal")
  end

  before do
    Account.any_instance.stubs(:is_serviceable?).returns(true)
    Account.any_instance.stubs(:has_user_views_negative_operators_enabled?).returns(true)
    accept :json
    use_ssl
  end

  as_an_admin do
    before do
      Account.any_instance.stubs(:has_user_views?).returns(true)
      Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
      create_custom_fields
    end

    describe "a GET to :export" do
      before do
        Api::V2::Rules::UserViewsController.any_instance.expects(:form_authenticity_token).at_least_once.returns "foo"
        @user_view = rules(:minimum_custom_fields_view)
        Resque.stubs(:should_throttle?).returns(false)
      end

      describe "enqueueing with an invalid csrf_token " do
        before do
          @request.env["HTTP_X_CSRF_TOKEN"] = "invalid"
          get :export, params: { id: @user_view.id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "enqueueing" do
        before do
          stub_request(:put, %r{\.amazonaws\.com/data/expirable_attachments/.*-csv\.zip})
          @request.env["HTTP_X_CSRF_TOKEN"] = "foo"
          get :export, params: { id: @user_view.id }
        end

        it('responds with ok') { assert_response :ok }

        it "responds with export status" do
          body = JSON.parse(@response.body)
          assert_equal rules(:minimum_custom_fields_view).id, body['export']['user_view_id']
          assert_equal 'enqueued', body['export']['status']
        end
      end

      describe "enqueued" do
        before do
          UserViewCsvJob.expects(:enqueue).with(@account.id, @user.id, @user_view.id)
          @request.env["HTTP_X_CSRF_TOKEN"] = "foo"
          get :export, params: { id: @user_view.id }
        end

        it('responds with ok') { assert_response :ok }
        it "responds with export status" do
          body = JSON.parse(@response.body)
          assert_equal @user_view.id, body['export']['user_view_id']
          assert_equal 'enqueued', body['export']['status']
        end
      end

      describe "when using basic auth" do
        before do
          UserViewCsvJob.expects(:enqueue).with(@account.id, @user.id, @user_view.id)
          Api::V2::Rules::UserViewsController.any_instance.expects(:logged_via_session?).returns(false)
          get :export, params: { id: @user_view.id }, headers: { "HTTP_AUTHENTICATION" => api_basic_auth_headers(@user.email, '123456') }
        end
        it('responds with ok') { assert_response :ok }
        it "responds with export status" do
          body = JSON.parse(@response.body)
          assert_equal @user_view.id, body['export']['user_view_id']
        end
      end

      describe "when throttled" do
        before do
          Resque.stubs(:should_throttle?).returns(true)
          @request.env["HTTP_X_CSRF_TOKEN"] = "foo"
          get :export, params: { id: @user_view.id }
        end

        it('responds with ok') { assert_response :ok }
        it "responds with export status" do
          body = JSON.parse(@response.body)
          assert_equal @user_view.id, body['export']['user_view_id']
          assert_equal 'throttled', body['export']['status']
        end
      end

      describe "restricted to other user" do
        before do
          assert_not_equal @user.id, users(:minimum_admin).id
          post :create, params: { user_view: setup_params(restricted_to_agent(users(:minimum_admin).id)) }
          data = JSON.parse(@response.body)['user_view']

          get :export, params: { id: data['id'] }
        end

        it "does not export user view" do
          expected_error = {
            "error" => {
              "message" => "You do not have access to this page. Please contact the account owner of this help desk for further help.",
              "title" => "Forbidden"
            }
          }
          assert_equal expected_error, JSON.parse(@response.body)
        end
      end
    end

    describe "a GET to :index" do
      it "lists all account and group views, as well as and personal views" do
        UserView.without_arsi.delete_all

        post :create, params: { user_view: setup_params(restricted_to_agent(@user.id).merge("title" => "Personal 1")) }
        personal_1 = JSON.parse(@response.body)["user_view"]

        @controller.instance_variable_set(:@initializer, nil)
        post :create, params: { user_view: setup_params(all_conditions.merge("title" => "Shared 1")) }
        shared_1 = JSON.parse(@response.body)["user_view"]

        @controller.instance_variable_set(:@initializer, nil)
        post :create, params: { user_view: setup_params(all_conditions.merge("active" => false, "title" => "Shared 2")) }
        shared_2 = JSON.parse(@response.body)["user_view"]

        @controller.instance_variable_set(:@initializer, nil)
        post :create, params: { user_view: setup_params(all_conditions.merge("title" => "Shared 3")) }
        shared_3 = JSON.parse(@response.body)["user_view"]

        @controller.instance_variable_set(:@initializer, nil)
        post :create, params: { user_view: setup_params(restricted_to_group(groups(:minimum_group).id).merge("title" => "Group Shared 1")) }
        group_shared_1 = JSON.parse(@response.body)["user_view"]
        UserView.update_position([shared_3['id'], shared_1['id'], group_shared_1['id'], shared_2['id']])

        @controller.instance_variable_set(:@initializer, nil)
        post :create, params: { user_view: setup_params(restricted_to_agent(users(:minimum_admin_not_verified).id)) }

        get :index

        data = JSON.parse(@response.body)["user_views"]
        shared_views = [shared_3, shared_1, group_shared_1, shared_2]
        personal_views = [personal_1]
        expected_views = (shared_views + personal_views).map { |r| r['title'] }
        actual_views = data.map { |r| r['title'] }
        assert_equal expected_views, actual_views
      end
    end

    describe "a POST to :create" do
      describe "with all conditions" do
        before do
          post :create, params: { user_view: setup_params(all_conditions) }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :created, location: %r{api/v2/user_views/\d+\.json}

        it "has the right attributes" do
          data = (JSON.parse @response.body)["user_view"]
          assert_equal data["title"], all_conditions["title"]
          assert_equal data["conditions"]["all"], all_conditions["all"]
        end
      end

      describe "with any conditions" do
        before do
          post :create, params: { user_view: setup_params(any_conditions) }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :created, location: %r{api/v2/user_views/\d+\.json}

        it "has the right attributes" do
          data = (JSON.parse @response.body)["user_view"]
          assert_equal data["title"], any_conditions["title"]
          assert_equal data["conditions"]["any"], any_conditions["any"]
        end
      end

      describe "restricted to user" do
        before do
          post :create, params: { user_view: setup_params(restricted_to_agent(users(:minimum_admin).id)) }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :created, location: %r{api/v2/user_views/\d+\.json}

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          assert_equal @params["title"], data["title"]
          assert_equal @params["all"], data["conditions"]["all"]
          assert_equal 'User', data["restriction"]["type"]
          assert_equal users(:minimum_admin).id, data["restriction"]["id"]
        end
      end

      describe "restricted to other user" do
        before do
          post :create, params: { user_view: setup_params(restricted_to_agent(users(:minimum_admin_not_owner).id)) }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :created, location: %r{api/v2/user_views/\d+\.json}

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          assert_equal @params["title"], data["title"]
          assert_equal @params["all"], data["conditions"]["all"]
          assert_equal 'User', data["restriction"]["type"]
          assert_equal users(:minimum_admin_not_owner).id, data["restriction"]["id"]
        end
      end

      describe "restricted to group" do
        before do
          post :create, params: { user_view: setup_params(restricted_to_group(groups(:minimum_group).id)) }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :created, location: %r{api/v2/user_views/\d+\.json}

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          assert_equal @params["title"], data["title"]
          assert_equal @params["all"], data["conditions"]["all"]
          assert_equal 'Group', data["restriction"]["type"]
          assert_equal groups(:minimum_group).id, data["restriction"]["id"]
        end
      end

      describe "restricted to account" do
        before do
          post :create, params: { user_view: setup_params(restricted_to_account(@account.id)) }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :created, location: %r{api/v2/user_views/\d+\.json}

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          assert_equal @params["title"], data["title"]
          assert_equal @params["all"], data["conditions"]["all"]
          assert_nil data["restriction"]
        end
      end

      describe "restricted to other account" do
        before do
          post :create, params: { user_view: setup_params(restricted_to_account(accounts(:support).id)) }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :created, location: %r{api/v2/user_views/\d+\.json}

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          assert_equal @params["title"], data["title"]
          assert_equal @params["all"], data["conditions"]["all"]
          assert_nil data["restriction"]
        end
      end

      describe "without a title" do
        before do
          post :create, params: { user_view: setup_params(all_conditions.merge("title" => nil)) }
        end

        it "shows error" do
          assert_equal "422", @response.code
          assert_equal({
            "error" => "RecordInvalid",
            "description" => "Record validation errors",
            "details" => { "base" => [{ "description" => "You need to add a title" }] }
          }, JSON.parse(@response.body))
        end
      end

      describe "without all" do
        before do
          post :create, params: { user_view: setup_params("all" => nil) }
        end

        it "shows error" do
          assert_equal "422", @response.code
          assert_equal({
            "error" => "RecordInvalid",
            "description" => "Record validation errors",
            "details" => { "base" => [{ "error" => "BlankValue", "description" => "Missing all condition." }] }
          }, JSON.parse(@response.body))
        end
      end

      describe "inactive view" do
        before do
          post :create, params: { user_view: setup_params(all_conditions.merge("active" => false)) }
        end

        it "displays the right attributes" do
          refute (JSON.parse @response.body)["user_view"]["active"]
        end
      end

      describe "execution columns" do
        before do
          post :create, params: { user_view: setup_params(all_conditions) }
        end

        it "defaults to name, email, and created" do
          @result = JSON.parse(@response.body)
          actual_columns = @result['user_view']['execution']['columns']
          expected_columns = [
            {'id' => 'name', 'title' => 'Name'},
            {'id' => 'email', 'title' => 'Email'},
            {'id' => 'created_at', 'title' => 'Created'}
          ]
          assert_equal expected_columns, actual_columns
        end
      end

      describe "no execution" do
        before do
          post :create, params: { user_view: all_conditions }
        end

        it "defaults to name, email and created" do
          @result = JSON.parse(@response.body)
          actual_columns = @result['user_view']['execution']['columns']
          expected_columns = [
            {'id' => 'name', 'title' => 'Name'},
            {'id' => 'email', 'title' => 'Email'},
            {'id' => 'created_at', 'title' => 'Created'}
          ]
          assert_equal expected_columns, actual_columns
        end
      end

      describe "nil execution" do
        before do
          post :create, params: { user_view: setup_params(all_conditions.merge("execution" => nil)) }
        end

        it "defaults to name, email and created" do
          @result = JSON.parse(@response.body)
          actual_columns = @result['user_view']['execution']['columns']
          expected_columns = [
            {'id' => 'name', 'title' => 'Name'},
            {'id' => 'email', 'title' => 'Email'},
            {'id' => 'created_at', 'title' => 'Created'}
          ]
          assert_equal expected_columns, actual_columns
        end
      end

      describe "invalid parameters" do
        it "shows error for invalid operator" do
          post :create, params: { user_view: setup_params("all" => [{ "field" => "custom_fields.plan_type", "operator" => "isdsf", "value" => "gold" }]) }
          assert_record_invalidation("InvalidOperator", "'Plan isdsf gold' is an invalid condition. 'isdsf' is not a valid operator.")
        end

        it "shows error for invalid operator when user_views_negative_operators_enabled disabled" do
          Account.any_instance.stubs(:has_user_views_negative_operators_enabled?).returns(false)
          post :create, params: { user_view: setup_params("all" => [{ "field" => "custom_fields.plan_type", "operator" => "is_not", "value" => "gold" }]) }
          assert_record_invalidation("InvalidOperator", "'Plan is_not gold' is an invalid condition. 'is_not' is not a valid operator.")
        end

        it "shows error for invalid source for non existent custom fields" do
          post :create, params: { user_view: setup_params("all" => [{ "field" => "custom_fields.does_not_exist", "operator" => "is", "value" => "gold" }]) }
          assert_record_invalidation("InvalidSource", "'custom_fields.does_not_exist is gold' is an invalid condition. 'custom_fields.does_not_exist' is not a valid source.")
        end

        it "shows error for invalid source for deactivated custom fields" do
          field = CustomField::Field.find_by_key("plan_type")
          field.update_attributes!(is_active: false)
          post :create, params: { user_view: setup_params("all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold" }]) }
          assert_record_invalidation("InvalidSource", "'custom_fields.plan_type is gold' is an invalid condition. 'custom_fields.plan_type' is not a valid source.")
        end

        it "shows error for invalid source when user tags are disabled" do
          Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
          post :create, params: { user_view: setup_params("all" => [{ "field" => "current_tags", "operator" => "includes", "value" => "foo" }]) }
          assert_record_invalidation("InvalidSource", "'current_tags includes foo' is an invalid condition. 'current_tags' is not a valid source.")
        end

        it "shows error for invalid source" do
          post :create, params: { user_view: setup_params("all" => [{ "field" => "abcdef", "operator" => "is", "value" => "gold" }]) }
          assert_equal "422", @response.code
          assert_record_invalidation("InvalidSource", "'abcdef is gold' is an invalid condition. 'abcdef' is not a valid source.")
        end

        it "shows error for invalid column when user tags are disabled" do
          Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
          post :create, params: { user_view: setup_params("all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold" }],
                                                "execution" => { "columns" => ['current_tags'] }) }
          assert_record_invalidation("InvalidColumns", "'current_tags' is an invalid column.")
        end

        it "shows error for invalid group" do
          post :create, params: { user_view: setup_params("all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold" }],
                                                "execution" => { "group" => { "id" => "abcdef", "order" => "asc" } }) }
          assert_record_invalidation("InvalidValue", "'abcdef asc' is an invalid grouping.")
        end

        it "shows error for invalid sort" do
          post :create, params: { user_view: setup_params("all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold" }],
                                                "execution" => { "sort" => { "id" => "abcdef", "order" => "asc" } }) }
          assert_record_invalidation("InvalidValue", "'abcdef asc' is an invalid sorting.")
        end

        describe "invalid value" do
          it "shows error for invalid date with integer operator" do
            create_user_custom_field!("date", "Date", "Date")
            post :create, params: { user_view: setup_params("all" => [{ "field" => "custom_fields.date", "operator" => "within_previous_n_days", "value" => "1/2/2001" }]) }
            assert_record_invalidation("InvalidValue", "'Date is within the previous 1/2/2001' is an invalid condition. '1/2/2001' is not a valid value.")
          end

          ["Date", "Integer", "Decimal", "Checkbox"].each do |type|
            it "shows error for invalid #{type}" do
              key = type.downcase
              create_user_custom_field!(key, type, type)
              post :create, params: { user_view: setup_params("all" => [{ "field" => "custom_fields.#{key}", "operator" => "is", "value" => "abcd" }]) }
              assert_record_invalidation("InvalidValue", "'#{type} is abcd' is an invalid condition. 'abcd' is not a valid value.")
            end
          end

          ["Text", "Textarea", "Regexp"].each do |type|
            it "shows error for invalid #{type}" do
              key = type.downcase
              create_user_custom_field!(key, type, type)
              post :create, params: { user_view: setup_params("all" => [{ "field" => "custom_fields.#{key}", "operator" => "is", "value" => ["1", "a"] }]) }
              assert_record_invalidation("InvalidValue", "'#{type} is [\"1\", \"a\"]' is an invalid condition. '[\"1\", \"a\"]' is not a valid value.")
            end
          end

          it "shows error for invalid organizations" do
            invalid_id = Organization.last.id + 1
            post :create, params: { user_view: setup_params("all" => [{ "field" => "organization_id", "operator" => "is", "value" => invalid_id }]) }
            assert_record_invalidation("InvalidValue", "'Organization is #{invalid_id}' is an invalid condition. '#{invalid_id}' is not a valid value.")
          end

          it "shows error for invalid locales" do
            invalid_id = (TranslationLocale.all - @account.available_languages).first.id
            post :create, params: { user_view: setup_params("all" => [{ "field" => "locale_id", "operator" => "is", "value" => invalid_id }]) }
            assert_record_invalidation("InvalidValue", "'Language is #{invalid_id}' is an invalid condition. '#{invalid_id}' is not a valid value.")
          end

          it "shows error for invalid tags" do
            post :create, params: { user_view: setup_params("all" => [{ "field" => "current_tags", "operator" => "includes", "value" => "sdfsdf" }]) }
            assert_record_invalidation("InvalidValue", "'Tags contain at least one of the following sdfsdf' is an invalid condition. 'sdfsdf' is not a valid value.")
          end
        end
      end
    end

    describe "a PUT to :update" do
      describe "with all conditions" do
        before do
          @conditions = all_conditions
          @conditions['all'].first['value'] = 'silver'

          put :update, params: { id: rules(:user_view_date_custom_field).id, user_view: setup_params(@conditions) }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :ok

        it "has the right attributes" do
          data = (JSON.parse @response.body)["user_view"]
          assert_equal data["conditions"]['all'], @conditions['all']
          assert_equal data["conditions"]["all"].first['value'], 'silver'
        end
      end

      describe "with any conditions" do
        before do
          @conditions = any_conditions
          @conditions['any'].first['value'] = 'silver'

          put :update, params: { id: rules(:user_view_date_custom_field).id, user_view: setup_params(@conditions) }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :ok

        it "has the right attributes" do
          data = (JSON.parse @response.body)["user_view"]
          assert_equal data["conditions"]['any'], @conditions['any']
          assert_equal data["conditions"]["any"].first['value'], 'silver'
        end
      end

      describe "restricted to user" do
        before do
          @restriction = restricted_to_agent(users(:minimum_admin).id)['restriction']

          put :update, params: { id: rules(:global_user_view).id, user_view: { restriction: @restriction } }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :ok

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          assert_equal 'User', data["restriction"]["type"]
          assert_equal users(:minimum_admin).id, data["restriction"]["id"]
        end
      end

      describe "restricted to other user" do
        before do
          @restriction = restricted_to_agent(rules(:minimum_custom_fields_view).owner_id)['restriction']

          put :update, params: { id: rules(:minimum_custom_fields_view).id, user_view: { restriction: @restriction } }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :ok

        it "has the right attributes and not allow restriction update" do
          data = JSON.parse(@response.body)["user_view"]
          assert_equal 'User', data["restriction"]["type"]
          assert_equal rules(:minimum_custom_fields_view).owner_id, data["restriction"]["id"]
        end
      end

      describe "restricted to group" do
        before do
          @restriction = restricted_to_group(groups(:minimum_group).id)['restriction']

          put :update, params: { id: rules(:user_view_date_custom_field).id, user_view: { restriction: @restriction } }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :ok

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          assert_equal 'Group', data["restriction"]["type"]
          assert_equal groups(:minimum_group).id, data["restriction"]["id"]
        end
      end

      describe "restricted to account" do
        before do
          @restriction = restricted_to_account(@account.id)['restriction']

          put :update, params: { id: rules(:user_view_date_custom_field).id, user_view: { restriction: @restriction } }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :ok

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          assert_nil data["restriction"]
        end
      end

      describe "restricted to other account" do
        before do
          @restriction = restricted_to_account(accounts(:support).id)['restriction']

          put :update, params: { id: rules(:user_view_date_custom_field).id, user_view: { restriction: @restriction } }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :ok

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          assert_nil data["restriction"]
        end
      end

      describe "inactive view" do
        before do
          put :update, params: { id: rules(:user_view_date_custom_field).id, user_view: { active: false } }
        end

        it "displays the right attributes" do
          refute (JSON.parse @response.body)["user_view"]["active"]
        end
      end

      describe "with grouping" do
        before do
          @execution = grouping['execution']

          put :update, params: { id: rules(:user_view_date_custom_field).id, user_view: { execution: @execution } }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :ok

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          assert @execution["group"]["id"],    data["execution"]["group"]["id"]
          assert @execution["group"]["order"], data["execution"]["group"]["order"]
        end
      end

      describe "with sorting" do
        before do
          @execution = sorting['execution']

          put :update, params: { id: rules(:user_view_date_custom_field).id, user_view: { execution: @execution } }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :ok

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          assert @execution["sort"]["id"],    data["execution"]["sort"]["id"]
          assert @execution["sort"]["order"], data["execution"]["sort"]["order"]
        end
      end

      describe "with column selection" do
        before do
          @execution = column_selection['execution']

          put :update, params: { id: rules(:user_view_date_custom_field).id, user_view: { execution: @execution } }
        end
        should_use_presenter Api::V2::Rules::UserViewPresenter, status: :ok

        it "has the right attributes" do
          data = JSON.parse(@response.body)["user_view"]
          expected_columns = [
            {'id' => 'name', 'title' => 'Name'},
            {'id' => 'current_tags', 'title' => 'Tags'}
          ]

          assert_equal expected_columns, data['execution']['columns']
        end
      end

      describe "execution columns" do
        before do
          post :create, params: { user_view: setup_params(all_conditions) }
        end

        it "defaults to name, email and created" do
          @result = JSON.parse(@response.body)
          actual_columns = @result['user_view']['execution']['columns']
          expected_columns = [
            {'id' => 'name', 'title' => 'Name'},
            {'id' => 'email', 'title' => 'Email'},
            {'id' => 'created_at', 'title' => 'Created'}
          ]
          assert_equal expected_columns, actual_columns
        end
      end
    end

    describe "a DELETE to :destroy" do
      it "is able to delete shared rules and responds with 204" do
        delete :destroy, params: { id: rules(:minimum_custom_fields_view).id }
        assert_response :no_content
      end
    end

    describe "a PUT to :reorder" do
      describe "when sorting shared views" do
        before do
          create_view setup_params(restricted_to_account(@account.id))

          @user_views = UserView.account_owned(@account.id)

          @previous_ids = @user_views.order(:position).pluck(:id)
          assert_equal 3, @previous_ids.length

          put :reorder, params: { user_view_ids: @previous_ids.reverse }
        end

        it('responds with ok') { assert_response :ok }

        it "sorts the views as specified" do
          assert_equal @previous_ids.reverse, @user_views.order(:position).pluck(:id)
        end
      end

      describe "when sorting personal views" do
        before do
          create_view setup_params(restricted_to_agent(@user.id).merge("title" => "Personal 1"))
          @controller.instance_variable_set(:@initializer, nil)
          create_view setup_params(restricted_to_agent(@user.id).merge("title" => "Personal 2"))

          @user_views = UserView.where(owner_type: 'User', owner_id: @user.id)

          @previous_ids = @user_views.order(:position).pluck(:id)
          assert_equal 2, @previous_ids.length

          put :reorder, params: { user_view_ids: @previous_ids.reverse }
        end

        it('responds with ok') { assert_response :ok }

        it "sorts the views as specified" do
          assert_equal @previous_ids.reverse, @user_views.order(:position).pluck(:id)
        end
      end

      describe "when sorting mixed views" do
        before do
          create_view setup_params(restricted_to_account(@account.id))
          @controller.instance_variable_set(:@initializer, nil)
          create_view setup_params(restricted_to_agent(@user.id).merge("title" => "Personal 1"))
          @controller.instance_variable_set(:@initializer, nil)
          create_view setup_params(restricted_to_agent(@user.id).merge("title" => "Personal 2"))

          account_scope  = UserView.account_owned(@account.id)
          personal_scope = UserView.where(owner_type: 'User', owner_id: @user.id)
          @user_views    = @account.user_views.and_disjunction_of(account_scope, personal_scope)

          @previous_ids = @user_views.order(:position).pluck(:id)
          assert_equal 5, @previous_ids.length

          put :reorder, params: { user_view_ids: @previous_ids.reverse }
        end

        it('responds with forbidden') { assert_response :forbidden }

        it "does not sort the views" do
          assert_equal @previous_ids, @user_views.order(:position).pluck(:id)
        end
      end
    end
  end
end
