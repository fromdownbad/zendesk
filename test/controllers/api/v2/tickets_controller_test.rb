require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"
require "zendesk/testing/factories/global_uid"
require 'zendesk/radar_factory'

SingleCov.covered! uncovered: 1

describe Api::V2::TicketsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :all

  let(:spanish_translation_locale) { translation_locales(:spanish) }

  before do
    accept :json
  end

  with_options(controller: "api/v2/tickets") do |request|
    request.should_route :get,    "/api/v2/tickets",                    action: "index"
    request.should_route :get,    "/api/v2/tickets/1",                  action: "show", id: "1"
    request.should_route :post,   "/api/v2/tickets",                    action: "create"
    request.should_route :put,    "/api/v2/tickets/1",                  action: "update",       id: "1"
    request.should_route :delete, "/api/v2/tickets/1",                  action: "destroy",      id: "1"
    request.should_route :put,    "/api/v2/tickets/1/mark_as_spam",     action: "mark_as_spam", id: "1"
    request.should_route :post,   "/api/v2/tickets/1/merge",            action: "merge",        id: "1"
    request.should_route :get,    "/api/v2/tickets/1/related",          action: "related",      id: "1"
    request.should_route :get,    "/api/v2/users/12/tickets/requested", action: "requested",    user_id: 12
    request.should_route :get,    "/api/v2/users/12/tickets/ccd",       action: "ccd",          user_id: 12
    request.should_route :get,    "/api/v2/users/12/tickets/followed",  action: "followed",     user_id: 12
    request.should_route :get,    "/api/v2/users/12/tickets/assigned",  action: "assigned",     user_id: 12
    request.should_route :get,    "/api/v2/organizations/12/tickets",   action: "index",        organization_id: 12
    request.should_route :get,    "/api/v2/views/12/tickets/1",         action: "show",         view_id: 12, id: 1
    request.should_route :put,    "/api/v2/tickets/update_many",        action: "update_many"
    request.should_route :delete, "/api/v2/tickets/destroy_many",       action: "destroy_many"
    request.should_route :put,    "/api/v2/tickets/mark_many_as_spam",  action: "mark_many_as_spam"
    request.should_route :post,   "/api/v2/tickets/show_many",          action: "show_many"
    request.should_route :get,    "/api/v2/tickets/show_many",          action: "show_many"
    request.should_route :post,   "/api/v2/tickets/create_many",        action: "create_many"
    request.should_route :get,    "/api/v2/tickets/count",              action: "count"
    request.should_route :get,    "/api/v2/organizations/12/tickets/count", action: "count", organization_id: 12
  end

  describe "when using oauth tokens" do
    with_scopes('tickets:read', 'read') do
      should_be_authorized { get :index }
      should_be_authorized { get :index, params: { organization_id: account.organizations.first.id } }
      should_be_authorized { get :requested, params: { user_id: account.owner } }
      should_be_authorized { get :ccd, params: { user_id: account.owner } }
      should_be_authorized { get :followed, params: { user_id: account.owner } }
      should_be_authorized { get :assigned, params: { user_id: account.owner } }
      should_be_authorized { get :recent }
      should_be_authorized { get :related, params: { id: account.tickets.first.nice_id } }
      should_be_authorized { get :show, params: { id: account.tickets.first.nice_id } }
      should_be_authorized { get :show_many, params: { ids: account.tickets.first.nice_id } }

      should_be_forbidden([:post, :create, ticket: { subject: "test", description: "test" }])
    end

    with_scopes('tickets:write', 'write') do
      should_be_authorized { post :create, params: { ticket: { subject: "test", description: "test" } } }
      should_be_authorized { post :create_many, params: { tickets: [{ subject: "test", description: "test"}] } }
      should_be_authorized { put :update, params: { id: account.tickets.first.nice_id, ticket: { subject: "Updated"} } }
      should_be_authorized { put :update_many, params: { ids: account.tickets.first.nice_id, ticket: { subject: "Updated"} } }
      should_be_authorized { put :mark_as_spam, params: { id: account.tickets.first.nice_id } }
      should_be_authorized { put :mark_many_as_spam, params: { ids: account.tickets.first.nice_id } }
      should_be_authorized { put :merge, params: { id: account.tickets.first.nice_id, ids: [account.tickets.second.nice_id] } }
      should_be_authorized { delete :destroy, params: { id: account.tickets.first.nice_id } }
      should_be_authorized { delete :destroy_many, params: { ids: account.tickets.first.nice_id } }

      should_be_forbidden :index
    end

    with_scopes('read', 'write') do
      should_be_authorized { get :requested, params: { user_id: account.owner } }
      should_be_authorized { get :ccd, params: { user_id: account.owner } }
      should_be_authorized { get :followed, params: { user_id: account.owner } }
      should_be_authorized { get :assigned, params: { user_id: account.owner } }
      should_be_authorized { get :recent }
      should_be_authorized { get :related, params: { id: account.tickets.first.nice_id } }
      should_be_authorized { get :show_many, params: { ids: account.tickets.first.nice_id } }
      should_be_authorized { post :create_many, params: { tickets: [{ subject: "test", description: "test"}] } }
      should_be_authorized { put :update_many, params: { ids: account.tickets.first.nice_id, ticket: { subject: "Updated"} } }
      should_be_authorized { put :mark_as_spam, params: { id: account.tickets.first.nice_id } }
      should_be_authorized { put :mark_many_as_spam, params: { ids: account.tickets.first.nice_id } }
      should_be_authorized { put :merge, params: { id: account.tickets.first.nice_id, ids: [account.tickets.second.nice_id] } }
      should_be_authorized { delete :destroy_many, params: { ids: account.tickets.first.nice_id } }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized(
      :index,
      [:get, :requested, {user_id: 1}],
      [:get, :ccd, {user_id: 1}],
      [:get, :followed, {user_id: 1}],
      :recent,
      :create,
      :update,
      :destroy,
      :destroy_many,
      :update_many,
      :mark_many_as_spam,
      :create_many,
      [:put, :mark_as_spam, {id: 1}]
    )
  end

  as_an_end_user do
    should_be_forbidden :index, [:get, :requested, {user_id: 1}], [:get, :ccd, {user_id: 1}], :recent, :create, :update, :destroy, :destroy_many, :update_many, :mark_many_as_spam, :create_many, [:put, :mark_as_spam, {id: 1}]
  end

  as_a_system_user do
    it "uses GenericCommentPresenter as option of TicketPresenter" do
      assert_equal @controller.send(:presenter).options[:comment_presenter].class, Api::V2::Tickets::GenericCommentPresenter
    end

    describe "a GET to #index" do
      describe "as the root resource" do
        before { get :index }

        should_use_presenter Api::V2::Tickets::TicketPresenter, with: :tickets
      end
    end
  end

  as_a_subsystem_user(user: 'gooddata', account: :minimum) do
    describe "a GET to :show_many" do
      let(:ids) { accounts(:minimum).tickets.map(&:nice_id).sort }
      let(:archived_ticket_id) { ids.first }

      describe "with suitable permission to view the tickets" do
        before do
          ticket = accounts(:minimum).tickets.find_by_nice_id(archived_ticket_id)
          archive_and_delete(ticket)
          get :show_many, params: { ids: ids.join(",") }
        end

        it "gives me back the users I asked for" do
          json = JSON.parse(@response.body)
          assert_equal ids, json['tickets'].map { |u| u["id"] }.sort
        end
      end
    end
  end

  as_an_agent do
    it "uses GenericCommentPresenter as option of TicketPresenter" do
      assert_equal @controller.send(:presenter).options[:comment_presenter].class, Api::V2::Tickets::GenericCommentPresenter
    end

    describe "a GET to #index" do
      describe "as the root resource" do
        before { get :index }

        should_use_presenter Api::V2::Tickets::TicketPresenter, with: :tickets

        before_should 'calls tickets' do
          @controller.expects(:tickets).returns([])
        end

        it "presents all account tickets" do
          tickets = JSON.parse(@response.body)["tickets"]
          assert_equal tickets.size, @account.tickets.size
        end
      end

      describe "as the root resource with external_id" do
        before { get :index, params: { external_id: 1 } }

        should_use_presenter Api::V2::Tickets::TicketPresenter

        before_should "query using external_id" do
          Ticket.expects(:find_by_sql).with { |sql| assert_includes(sql.where_sql, "`external_id` ="); true }.returns([])
        end
      end

      describe "as the root resource with a blank external_id" do
        before { get :index, params: { external_id: '' } }

        should_use_presenter Api::V2::Tickets::TicketPresenter

        before_should "not query using external_id" do
          Ticket.expects(:find_by_sql).with { |sql| assert_not_includes(sql.where_sql, "`external_id` ="); true }.returns([])
        end
      end

      describe "as a sub-resource of an organization" do
        let(:organization) { organizations(:minimum_organization1) }

        before do
          get :index, params: { organization_id: organization.id.to_s }
        end

        it "presents the organization tickets" do
          tickets = JSON.parse(@response.body)["tickets"]
          assert_equal tickets.size, organization.tickets.size
          assert tickets.all? { |t| t["organization_id"] == organization.id }
        end
      end

      it 'returns a valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
        get :index, params: { external_id: 1 }
        assert_equal response.headers['Access-Control-Allow-Origin'], 'http://app.futuresimple.com'
      end

      it 'returns a not valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
        get :index, params: { external_id: 1 }
        assert_nil response.headers['Access-Control-Allow-Origin']
      end
    end

    describe "a GET to :show_many" do
      let(:ids) { accounts(:minimum).tickets.map(&:nice_id).sort }
      let(:archived_ticket_id) { ids.first }

      describe "with suitable permission to view the tickets" do
        before do
          ticket = accounts(:minimum).tickets.find_by_nice_id(archived_ticket_id)
          archive_and_delete(ticket)
          get :show_many, params: { ids: ids.join(",") }
        end

        it "gives me back the users I asked for" do
          json = JSON.parse(@response.body)
          assert_equal ids, json['tickets'].map { |u| u["id"] }.sort
        end

        it "retrieves archived tickets too" do
          json = JSON.parse(@response.body)
          assert json['tickets'].map { |u| u["id"] }.include?(archived_ticket_id)
        end

        it "makes a permission check on all tickets" do
          @controller.send(:current_user).expects(:can?).with(:view, anything).times(ids.size).returns(true)

          get :show_many, params: { ids: ids.join(",") }
        end
      end

      describe "with nil ids" do
        it "does not blow up" do
          # this stub is stupid and necessary otherwise the `require_that_user_can! :view, :bulk_tickets`
          # evaluates the local bulk_tickets for some reason and blows up prematurely >_<
          Api::V2::TicketsController.any_instance.stubs(:bulk_tickets)
          get :show_many, params: { ids: nil }
          assert_response :bad_request
        end
      end

      describe "without suitable permission to view the tickets" do
        before do
          tickets = Ticket.where(nice_id: ids).to_a
          Api::V2::TicketsController.any_instance.stubs(bulk_tickets: tickets)
          @controller.send(:current_user).stubs(:can?).with(:view, tickets.first).returns(false)
          get :show_many, params: { ids: ids.join(",") }
        end
        it('responds with forbidden') { assert_response :forbidden }
      end
    end

    describe "a GET to #show" do
      describe "without a :view_id" do
        before { get :show, params: { id: tickets(:minimum_1).nice_id } }

        should_use_presenter Api::V2::Tickets::TicketPresenter

        it "does not change" do
          assert_no_api_change "test/files/api_fixtures/v2-tickets-show.yml"
        end
      end

      describe "with a valid :view_id" do
        before { @view = rules(:view_assigned_working_tickets) }

        describe "with an invalid ticket id" do
          before do
            Zendesk::Rules::RuleExecuter.
              any_instance.
              stubs(:find_tickets_with_occam_client).
              returns([])

            get :show, params: { view_id: @view.id, id: 999999 }
          end

          it('responds with not_found') { assert_response :not_found }
        end

        describe "with a valid ticket" do
          before do
            Zendesk::Rules::RuleExecuter.
              any_instance.
              stubs(:find_tickets_with_occam_client).
              returns([tickets(:minimum_1)])

            get :show, params: { view_id: @view.id, id: tickets(:minimum_1).nice_id }
          end

          should_use_presenter Api::V2::Tickets::TicketPresenter
        end
      end

      describe "with an invalid :view_id" do
        before { get :show, params: { view_id: 9999, id: 9999 } }
        it('responds with not_found') { assert_response :not_found }
      end

      describe "coming from lotus" do
        before do
          set_header('X-Zendesk-Lotus-Version', 'v1.2.3')
          @ticket = tickets(:minimum_2)
          statsd_client = @controller.send(:statsd_client)
          statsd_client.expects(:increment)
          get :show, params: { id: @ticket.nice_id, format: :json }
        end

        it "tracks a stat" do
          assert_response :success
        end
      end

      describe "Ticket is from HelpCenter" do
        let(:ticket) { tickets(:minimum_1) }

        [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
          describe "via_id is #{via_id}" do
            before do
              ticket.update_column(:via_id, via_id)
              ticket.update_column(:via_reference_id, ViaType.HELPCENTER)
            end

            describe "agent_as_end_user Arturo is disabled" do
              before do
                Arturo.disable_feature!(:agent_as_end_user)
              end

              it "succeeds" do
                get :show, params: { id: ticket.nice_id }
                assert_response :ok
              end
            end

            describe "agent_as_end_user Arturo is enabled" do
              before do
                Arturo.enable_feature!(:agent_as_end_user)
              end

              it "fails" do
                get :show, params: { id: ticket.nice_id }
                assert_response :forbidden
              end
            end
          end
        end
      end
    end

    describe "a GET to :count" do
      let(:account) { accounts(:minimum) }
      let(:iso_date) { '2020-07-23T03:21:55+00:00' }

      before do
        Zendesk::RecordCounter::Tickets.any_instance.stubs(:iso_date).returns(iso_date)
        get :count
      end

      it "should respond with the correct format and values" do
        expected_response = {
          'count' => {
            'value' => account.tickets.count,
            'refreshed_at' => iso_date
          },
          'links' => {
            'url' => 'https://minimum.zendesk-test.com/api/v2/tickets/count'
          }
        }

        assert_equal expected_response, JSON.parse(@response.body)
        assert_response :success
      end
    end

    describe "a POST to #create" do
      describe 'idempotency' do
        before do
          request.headers['Idempotency-Key'] = '123'
          # This is needed, incase tests change the client underneath us somehow.
          Zendesk::Idempotency::ResponseStore.any_instance.stubs(:backend).returns(Zendesk::RedisStore.client)
        end

        after { Zendesk::RedisStore.client.flushall }

        describe 'for successful creation' do
          let(:cache_key) { "id_res:#{@account.id}:create:/api/v2/tickets:40bd001563085fc35165329ea1ff5c5ecbdbbeef" }
          let(:payload) { { ticket: { subject: 'test', description: 'foobar', comment: { 'body': 'comment body' } } } }
          before do
            post :create, params: payload
            @uncached_response = response.dup
            assert_equal 'miss', @uncached_response.headers['X-Idempotency-Lookup']
            # there's a lot of state retention in both the controller and the request object that needs to be cleared
            @controller = Api::V2::TicketsController.new
            request.cookie_jar.clear
            request.env.delete 'zendesk.cookie'
          end

          it 'sets the correct expiry' do
            ttl = Zendesk::RedisStore.client.ttl(cache_key)
            # doing an exact match here may cause a flaky spec
            assert ttl >= 119.minutes && ttl <= 2.hours
          end

          it 'extends ttl for cache hits' do
            Timecop.travel(50.minutes) do
              post :create, params: payload
              ttl = Zendesk::RedisStore.client.ttl(cache_key)
              # doing an exact match here may cause a flaky spec
              assert ttl >= 119.minutes && ttl <= 2.hours
            end
          end

          it 'returns an identical cached response' do
            original_ticket_count = Ticket.count
            post :create, params: payload

            # RAILS5UPGRADE: this has more to do with test obj setup than validity
            @uncached_response.headers.delete "Set-Cookie" if RAILS5

            assert_equal @uncached_response.status, response.status
            assert_equal @uncached_response.headers.merge('X-Idempotency-Lookup' => 'hit'), response.headers
            assert_equal @uncached_response.body, response.body
            assert_equal original_ticket_count, Ticket.count
          end

          it 'fails if params mismatch' do
            original_ticket_count = Ticket.count
            post :create, params: { ticket: { subject: 'test', description: 'foobar' } }
            assert_equal 400, response.status
            json_response = JSON.parse(response.body)
            assert_equal(
              { 'error' => 'IdempotentRequestError', 'description' => I18n.t('txt.error_message.controllers.idempotency.request_mismatch') },
              json_response
            )
            assert_equal original_ticket_count, Ticket.count
          end
        end

        describe 'for validation errors' do
          before do
            post :create, params: { ticket: { subject: 'test' } }
            @controller = Api::V2::TicketsController.new
            post :create, params: { ticket: { subject: 'test', description: 'test' } }
          end

          should_change('ticket count', by: 1) { Ticket.count }
        end
      end

      describe 'populating cloudflare rate limiting header' do
        describe_with_arturo_enabled :orca_classic_rate_limit_tickets_primary do
          describe 'when an account is whitelisted' do
            before { @account.update_attributes!(settings: { whitelisted_from_fraud_restrictions: true }) }
            describe 'when the account is a trial account' do
              before { Account.any_instance.stubs(:is_trial?).returns(true) }
              it 'does not populate the cloudflare rate limit response header' do
                post :create, params: { ticket: { subject: "test", description: "test" } }

                refute response.headers["ZENDESK-EP"]
              end
            end

            describe 'when the account is new (less than 20 days old)' do
              before { Account.any_instance.stubs(:recently_created_account?).returns(true) }
              it 'does not populate the cloudflare rate limit response header' do
                post :create, params: { ticket: { subject: "test", description: "test" } }

                refute response.headers["ZENDESK-EP"]
              end
            end
          end

          describe 'when an account is not whitelisted' do
            before { @account.update_attributes!(settings: { whitelisted_from_fraud_restrictions: false }) }
            describe 'when the account is a trial account' do
              before { Account.any_instance.stubs(:is_trial?).returns(true) }
              describe_with_arturo_enabled :orca_classic_rate_limit_trial_acc_tickets do
                it "populates the trial rate limit challenge response header" do
                  post :create, params: { ticket: { subject: "test", description: "test" } }

                  assert_equal response.headers["ZENDESK-EP"], 22
                end
              end

              describe_with_arturo_disabled :orca_classic_rate_limit_trial_acc_tickets do
                it "populates the trial rate limit simulate response header" do
                  post :create, params: { ticket: { subject: "test", description: "test" } }

                  assert_equal response.headers["ZENDESK-EP"], -22
                end
              end
            end

            describe 'when the account is new (less than 20 days old)' do
              before { Account.any_instance.stubs(:recently_created_account?).returns(true) }
              describe_with_arturo_enabled :orca_classic_rate_limit_new_acc_tickets do
                it "populates the new account challenge header" do
                  post :create, params: { ticket: { subject: "test", description: "test" } }

                  assert_equal response.headers["ZENDESK-EP"], 21
                end
              end

              describe_with_arturo_disabled :orca_classic_rate_limit_new_acc_tickets do
                it "populates the new account simulate header" do
                  post :create, params: { ticket: { subject: "test", description: "test" } }

                  assert_equal response.headers["ZENDESK-EP"], -21
                end
              end
            end

            describe 'when the account is not a new account or trial' do
              before do
                Account.any_instance.stubs(:is_trial?).returns(false)
                Account.any_instance.stubs(:recently_created_account?).returns(false)
              end

              it "does not populate the cloudflare rate limit response header" do
                post :create, params: { ticket: { subject: "test", description: "test" } }

                refute response.headers["ZENDESK-EP"]
              end
            end
          end
        end

        describe_with_arturo_disabled :orca_classic_rate_limit_tickets_primary do
          describe 'when an account is not whitelisted' do
            before { @account.update_attributes!(settings: { whitelisted_from_fraud_restrictions: false }) }
            describe 'when the account is a recently created trial account' do
              before do
                Account.any_instance.stubs(:is_trial?).returns(true)
                Account.any_instance.stubs(:recently_created_account?).returns(true)
              end

              it "does not populates the rate limit response header" do
                post :create, params: { ticket: { subject: "test", description: "test" } }

                refute response.headers["ZENDESK-EP"]
              end
            end
          end
        end
      end

      describe "with valid params" do
        before do
          post :create, params: { ticket: { subject: "test", description: "test" } }
        end

        should_use_presenter Api::V2::Tickets::TicketPresenter, status: :created, location: /\/api\/v2\/tickets\/\d+\.json$/

        should_change("the ticket count", by: 1) { Ticket.count(:all) }

        describe "response" do
          before { @json_response = JSON.parse(@response.body) }
          it "sends audit with response" do
            assert_not_nil @json_response["audit"]
          end

          it "includes comment with response" do
            assert_not_nil @json_response["audit"]["events"].detect { |event| event["type"] == "Comment" }
          end
        end
      end

      describe "without ticket object" do
        before do
          post :create
        end

        it "returns 400 bad request" do
          assert_response :bad_request
        end
      end

      describe "when sending a malformed body" do
        before do
          post :create, params: { ticket: { subject: "test", comment: {body: ["channel:all", "foob"] } } }
        end

        it "is not be allowed" do
          assert_response :bad_request
        end
      end

      describe "X-Force-Exception-Locale header" do
        let(:email) { "sketchy_mcsketcherson@domain.invalid.co3" }

        before do
          @account.translation_locale = spanish_translation_locale
          @account.save
          assert_equal spanish_translation_locale, @account.reload.translation_locale
        end

        let(:payload) do
          {
            ticket: {
              comment: {
                body: "Hi",
                public: false
              },
              via: {
                channel: "chat"
              },
              requester: {
                name: "Sketchy anonymous user",
                email: email
              },
              subject: "Test subject"
            }
          }
        end

        describe 'when header is included in the request' do
          before { request.headers['X-Force-Exception-Locale'] = '1' }

          it "returns ActiveRecord errors in English" do
            I18n.stubs(:t).with("activerecord.errors.models.user_identity.attributes.value.invalid",
              locale: "en-US-x-1".to_sym,
              value: email)
            I18n.stubs(:t)

            post :create, params: payload
            assert_response :unprocessable_entity
          end
        end

        it "returns ActiveRecord errors in Spanish" do
          I18n.stubs(:t).with("activerecord.errors.models.user_identity.attributes.value.invalid",
            locale: "es-x-6".to_sym,
            value: email)
          I18n.stubs(:t)

          post :create, params: payload
          assert_response :unprocessable_entity
        end
      end

      describe "with a comment with a malformed value" do
        before { post :create, params: { ticket: { subject: "test", comment: { "value": { "body": "foo" } } } } }

        it "is not allowed" do
          assert_response :bad_request
        end
      end

      describe "when email_ccs is not an array" do
        before { post :create, params: { ticket: { email_ccs: "test", description: "test" } } }

        it "is not allowed" do
          assert_response :bad_request
          assert @response.body =~ /email_ccs must be an array/
        end
      end

      describe "when email_ccs is not an array of hashes" do
        before { post :create, params: { ticket: { email_ccs: [{}, {}, "hi"], description: "test" } } }

        it "is not allowed" do
          assert_response :bad_request
          assert @response.body =~ /email_ccs must be a hash/
        end
      end

      describe "when followers is not an array" do
        before { post :create, params: { ticket: { followers: "test", description: "test" } } }

        it "is not allowed" do
          assert_response :bad_request
          assert @response.body =~ /followers must be an array/
        end
      end

      describe "when followers is not an array of hashes" do
        before { post :create, params: { ticket: { followers: [{}, {}, "hi"], description: "test" } } }

        it "is not allowed" do
          assert_response :bad_request
          assert @response.body =~ /followers must be a hash/
        end
      end

      describe "when using raw_subject" do
        before do
          post :create, params: { ticket: { description: 'foo', subject: 'not this', raw_subject: 'raw bar' } }
        end

        should_use_presenter Api::V2::Tickets::TicketPresenter, status: :created, location: /\/api\/v2\/tickets\/\d+\.json$/

        should_change("the ticket count", by: 1) { Ticket.count(:all) }

        it "creates tickets using the raw_subject not the subject" do
          assert_equal "raw bar", Ticket.last.subject
        end
      end

      it "should not fail with :url => nil ZD#315004" do
        post :create, params: { ticket: { subject: "test", description: "test", url: nil } }, as: :json
        assert_response :success
      end

      describe "with async: true" do
        describe "with valid parameters" do
          before do
            @nice_id = @account.nice_id_sequence.peek

            job_id_stub = stub
            job_status_stub = stub
            TicketBulkCreateJob.expects(:enqueue).returns(job_id_stub)
            Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
            Api::V2::JobStatusPresenter.any_instance.expects(:present).returns(job_status: 'stub_response')

            post :create, params: { ticket: { subject: "test", comment: { body: "test" } }, async: true }
          end

          it "returns the ticket nice_id and enqueues a ticket create job" do
            response = JSON.parse(@response.body)
            assert_equal @nice_id, response["ticket"]["id"]
          end
        end

        describe "without comment object" do
          before do
            post :create, params: { ticket: {}, async: true }, as: :json
          end

          it "returns 400 bad request" do
            assert_response :bad_request
          end
        end

        describe "without ticket object" do
          before do
            post :create, params: { async: true }
          end

          it "returns 400 bad request" do
            assert_response :bad_request
          end
        end
      end

      describe "with disable_triggers" do
        let(:rule_id) { rules(:trigger_notify_requester_of_received_request).id }

        before do
          Account.any_instance.stubs(:has_rule_usage_stats?).returns(true)
        end

        it 'runs triggers when unset' do
          post :create, params: { ticket: { subject: "test", description: "test" } }
          nice_id = JSON.parse(@response.body)["ticket"]["id"]
          assert TicketJoin.includes(:ticket).exists?(rule_id: rule_id, tickets: { nice_id: nice_id })
        end

        it 'does not run triggers when set' do
          post :create, params: { ticket: { subject: "test", description: "test" }, disable_triggers: true }
          nice_id = JSON.parse(@response.body)["ticket"]["id"]
          assert_empty(TicketJoin.includes(:ticket).where(rule_id: rule_id, tickets: { nice_id: nice_id }))
        end
      end

      # Archived and Deleted are valid ticket status's however we should
      # not allow our users to set these at will since they have distinct behavior
      # in the system
      ['archived', 'deleted'].each do |status|
        describe "with a ticket status of #{status}" do
          before do
            put :create, params: { ticket: { subject: "new wombats", description: 'heeey', status: status } }
          end

          it 'returns 400 bad request' do
            assert_response :bad_request
          end
        end
      end

      describe 'with valid external domain' do
        it 'returns a valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
          post :create, params: { ticket: { subject: "test", description: "test" } }
          assert_equal response.headers['Access-Control-Allow-Origin'], 'http://app.futuresimple.com'
        end

        it 'returns a not valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
          post :create, params: { ticket: { subject: "test", description: "test" } }
          assert_nil response.headers['Access-Control-Allow-Origin']
        end
      end
    end

    describe "a POST to :create_many" do
      let(:account) { accounts(:minimum) }
      let :valid_ticket_data do
        [{ subject: "test", description: "test" },
         { subject: "test2", description: "test2" }]
      end

      describe "with valid params 1" do
        before do
          post :create_many, params: { tickets: valid_ticket_data }
        end

        it('responds with success') { assert_response :success }
      end

      describe "with valid params 2" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          TicketBulkCreateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)

          post :create_many, params: { tickets: valid_ticket_data }
        end

        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with missing params" do
        before do
          post :create_many
        end

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "disallows >100 tickets" do
        before do
          post :create_many, params: { tickets: Array.new(101) { { subject: "test", description: "test" } } }
        end

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with some params blacklisted" do
        it 'saves the tickets but ignores the illegal param' do
          assert_difference('account.tickets.count(:all)', 2) do
            post :create_many, params: { tickets: [
              { subject: "test_created_at_whitelist", description: "test 1", created_at: "2001-01-01T01:23:45Z" },
              { subject: "test_created_at_whitelist", description: "test 2", created_at: "2001-01-01T01:23:45Z", comment: { body: "test 2" }}
            ] }
          end
          assert_response :ok
          assert_equal "test_created_at_whitelist", account.tickets.last.subject
          assert_equal Time.now.to_date, account.tickets.last.created_at.to_date
        end
      end
    end

    describe "a PUT to #update" do
      before { @ticket = tickets(:minimum_1) }

      describe "with valid params" do
        before do
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { subject: "Updated Subject" } }
        end

        should_use_presenter Api::V2::Tickets::TicketPresenter

        it "updates the ticket" do
          assert_equal "Updated Subject", @ticket.reload.subject
        end
      end

      describe "with invalid organization and requester params" do
        before do
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { subject: "Updated Subject", requester_id: 0, organization_id: 0 } }
        end

        it "returns 422 unprocessable entity" do
          assert_response :unprocessable_entity
        end
      end

      # Archived and Deleted are valid ticket status's however we should
      # not allow our users to set these at will since they have distinct behavior
      # in the system
      #
      ['archived', 'deleted'].each do |status|
        describe "with a ticket status of #{status}" do
          before do
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { subject: "updated wombats", status: status } }
          end

          it 'returns 400 bad request' do
            assert_response :bad_request
          end
        end
      end

      describe "X-Force-Exception-Locale header" do
        let(:email) { "sketchy_mcsketcherson@domain.invalid.co3" }

        before do
          I18n.stubs(:locale=)
          I18n.stubs(:t)
          I18n.expects(:t).with(
            "activerecord.errors.models.user_identity.attributes.value.invalid",
            value: email
          ).at_least_once
          @account.translation_locale = spanish_translation_locale
          @account.save
          assert_equal spanish_translation_locale, @account.reload.translation_locale
        end

        let(:payload) do
          {
            id: @ticket.nice_id.to_s,
            ticket: {
              requester: {
                name: "Sketchy anonymous user",
                email: email
              },
              subject: "Test subject"
            }
          }
        end

        describe 'when header is included in the request' do
          before { request.headers['X-Force-Exception-Locale'] = '1' }

          it "returns ActiveRecord errors in English" do
            I18n.expects(:locale=).with(ENGLISH_BY_ZENDESK).at_least_once

            put :update, params: payload
            assert_response :unprocessable_entity
          end
        end

        it "returns ActiveRecord errors in Spanish" do
          I18n.expects(:locale=).with(ENGLISH_BY_ZENDESK).never

          put :update, params: payload
          assert_response :unprocessable_entity
        end
      end
      describe "with an assignee_email param" do
        let(:agent) { users(:minimum_admin_not_owner) }

        before do
          @ticket = tickets(:minimum_6)
          assert @ticket.assignee.nil?
          assert @ticket.group.nil?

          agent.groups << groups(:minimum_group)
          agent.set_default_group(groups(:minimum_group).id)

          put :update, params: { id: @ticket.nice_id.to_s, ticket: { assignee_email: agent.email } }
        end

        it "sets the new assignee" do
          assert_equal agent.id, @ticket.reload.assignee_id
        end

        it "sets the group" do
          assert_equal agent.default_group_id, @ticket.reload.group_id
        end
      end

      describe "with an assignee_email param in a new ticket" do
        let(:agent) { users(:minimum_agent) }

        before do
          @ticket = tickets(:minimum_1)
          @ticket.update_column(:assignee_id, nil)
          @ticket.reload

          assert @ticket.status?(:new)
          assert @ticket.assignee.nil?

          agent.set_default_group(groups(:minimum_group).id)

          put :update, params: { id: @ticket.nice_id.to_s, ticket: { assignee_email: agent.email } }
        end

        it "sets the new assignee" do
          assert_equal agent.id, @ticket.reload.assignee_id
        end

        it "sets the group" do
          assert_equal agent.default_group_id, @ticket.reload.group_id
        end

        it "automatically opens the ticket" do
          assert @ticket.reload.status?(:open)
        end
      end

      describe "with both an assignee_email and an assignee_id param" do
        let(:agent) { users(:minimum_admin_not_owner) }
        let(:other_agent) { users(:minimum_admin) }

        before do
          @ticket = tickets(:minimum_2)
          agent.groups << groups(:minimum_group)

          put :update, params: { id: @ticket.nice_id.to_s, ticket: { assignee_email: other_agent.email, assignee_id: agent.id } }
        end

        it "sets the new assignee via the assignee_id param" do
          assert_equal agent.id, @ticket.reload.assignee_id
        end
      end

      describe "with safe_update set to true" do
        let(:ticket) do
          {
            subject: "Updated Subject",
            safe_update: true,
            updated_stamp: updated_stamp
          }
        end

        before do
          put :update, params: { id: @ticket.nice_id.to_s, ticket: ticket }
          @json = JSON.parse(@response.body)
        end

        describe "with an outdated updated_stamp" do
          let(:updated_stamp) { (@ticket.updated_at - 2.minutes).iso8601 }

          it('responds with conflict') { assert_response :conflict }

          it "returns an error message" do
            assert_equal 'UpdateConflict', @json['error']
          end

          it "returns an error description" do
            refute_nil @json['description']
          end
        end

        describe "with an up-to-date updated_stamp" do
          let(:updated_stamp) { @ticket.updated_at.iso8601 }

          it "updates the ticket" do
            assert_response :success
            assert_equal "Updated Subject", @ticket.reload.subject
          end
        end

        describe "without the required updated_stamp param" do
          let(:updated_stamp) { nil }

          it "returns 422 if updated_stamp param is not included" do
            assert_response :unprocessable_entity
          end

          it "returns an error message" do
            assert_equal 'UnprocessableEntity', @json['error']
          end

          it "returns an error description" do
            refute_nil @json['description']
          end
        end
      end

      describe "with collision notification" do
        it "updates viewing status on an update" do
          notification = mock('notification')
          ::RadarFactory.expects(:create_radar_notification).with(@ticket.account, "ticket/#{@ticket.nice_id}").returns(notification)
          notification.expects(:send)
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { subject: "Updated Subject" } }
        end
      end

      describe "that raises ActiveRecord::RecordNotSaved" do
        before do
          Ticket.any_instance.expects(:save!).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { subject: "Updated Subject" } }
        end

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
      end

      it "removes tags via tags => [nil] that is not tags => nil" do
        assert @ticket.taggings.present?
        put :update, params: { id: @ticket.nice_id.to_s, ticket: { tags: nil } }, as: :json
        refute @ticket.reload.taggings.present?
      end

      describe "with setting ticket_tagging" do
        describe "disabled" do
          before do
            @account.settings.ticket_tagging = false
            @account.save!
          end

          it "ignores tag params on update" do
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { tags: ["ignore_me"] } }
            assert_equal "hello", @ticket.reload.current_tags
          end
        end

        describe "enabled" do
          before do
            @account.settings.ticket_tagging = true
            @account.save!
          end

          it "allows updates of ticket tags" do
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { tags: ["include_me"] } }
            assert_equal "include_me", @ticket.reload.current_tags
          end
        end
      end

      describe "with role permission edit_ticket_tags" do
        before do
          @user.stubs(:can?).returns(true)
        end

        it "enabled, it allows updates of ticket tags" do
          @user.stubs(:can?).with(:edit_tags, Ticket).returns(true)
          @user.stubs(:can?).with(:edit_tags, @ticket).returns(true)
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { tags: ["include_me"] } }
          assert_equal "include_me", @ticket.reload.current_tags
        end

        it "disabled, it ignores tag params on update" do
          @user.stubs(:can?).with(:edit_tags, Ticket).returns(false)
          @user.stubs(:can?).with(:edit_tags, @ticket).returns(false)
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { tags: ["ignore_me"] } }
          assert_equal "hello", @ticket.reload.current_tags
        end
      end

      describe "setting the via attributes" do
        # Despite these tests being written for the controller, they execute presenter code.
        # A follow-up JIRA has been created to re-assess these tests before Lychee GAs:
        # https://zendesk.atlassian.net/browse/IRIS-577
        describe_with_arturo_enabled(:polaris) do
          before do
            @account.settings.polaris = true
            put :update, params: { id: @ticket.nice_id.to_s, ticket: params }
            @json_response = JSON.parse(@response.body)
          end

          describe "with a string" do
            let(:params) { { via: { channel: "Chat" } } }

            it "sets the via_id" do
              assert_equal "chat", @json_response["audit"]["via"]["channel"]
            end
          end

          describe "with an integer" do
            let(:params) { { via: { source: rules(:trigger_notify_blank_email).id, channel: Zendesk::Types::ViaType.RULE.to_s } } }

            it "sets the via_id" do
              assert_equal "rule", @json_response["audit"]["via"]["channel"]
            end
          end
        end

        # Despite these tests being written for the controller, they execute presenter code.
        # A follow-up JIRA has been created to re-assess these tests before Lychee GAs:
        # https://zendesk.atlassian.net/browse/IRIS-577
        describe_with_arturo_disabled(:polaris) do
          before do
            @account.settings.polaris = false
            put :update, params: { id: @ticket.nice_id.to_s, ticket: params }
            @json_response = JSON.parse(@response.body)
          end

          describe "with a string" do
            let(:params) { { via: { channel: "Chat" } } }

            it "sets the via_id" do
              assert_equal "chat", @json_response["audit"]["via"]["channel"]
            end
          end

          describe "with an integer" do
            let(:params) { { via: { source: rules(:trigger_notify_blank_email).id, channel: Zendesk::Types::ViaType.RULE.to_s } } }

            it "sets the via_id" do
              assert_equal "rule", @json_response["audit"]["via"]["channel"]
            end
          end
        end
      end

      describe "with a null priority" do
        describe "on a ticket with a prority" do
          before do
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { priority: nil } }, as: :json
          end

          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

          it "is intepreted as a priority reset" do
            @response.body.must_include "Priority: cannot be reset"
          end
        end

        describe "on a ticket without a priority" do
          before do
            assert(@ticket.update_columns(priority_id: 0))
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { priority: nil, subject: "Updated Subject" } }, as: :json
          end

          should_use_presenter Api::V2::Tickets::TicketPresenter, status: :ok

          it "represents the priority as a null" do
            @ticket.reload

            assert_equal 0, @ticket.priority_id
            assert_equal "Updated Subject", @ticket.subject

            response = JSON.parse(@response.body)
            assert     response["ticket"].key?("priority")
            assert_nil response["ticket"]["priority"]
          end
        end
      end

      describe "with a null type" do
        describe "on a ticket with a type" do
          before do
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { type: nil } }, as: :json
          end

          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

          it "is intepreted as a type reset" do
            @response.body.must_include "Type: cannot be reset"
          end
        end

        describe "on a ticket without a type" do
          before do
            assert(@ticket.update_columns(ticket_type_id: 0))
            assert_equal 0, @ticket.reload.ticket_type_id
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { type: nil, subject: "Updated Subject" } }, as: :json
          end

          should_use_presenter Api::V2::Tickets::TicketPresenter, status: :ok

          it "represents the status as a null" do
            assert_equal 0, @ticket.reload.ticket_type_id
            assert_equal "Updated Subject", @ticket.reload.subject

            response = JSON.parse(@response.body)
            assert     response["ticket"].key?("type")
            assert_nil response["ticket"]["type"]
          end
        end
      end

      describe "rich comments" do
        let(:comment) { { body: "New Comment", html_body: "New <span>Comment</span>", public: true } }

        it "stores the rich comment" do
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { comment: comment } }
          json_response = JSON.parse(@response.body)
          comment = json_response['audit']['events'].first { |e| e['type'] == 'Comment' }
          assert_equal 'New Comment', comment['body']
          assert_equal "<div class=\"zd-comment\" dir=\"auto\">New <span>Comment</span></div>", comment['html_body']
        end
      end

      describe "with valid comment" do
        before do
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { comment: comment } }
          @json_response = JSON.parse(@response.body)
        end

        describe "a public comment" do
          let(:comment) { { body: "New Comment", public: true } }

          it "sends audit with response" do
            assert_not_nil @json_response["audit"]
          end

          it "includes comment with response" do
            assert_not_nil @json_response["audit"]["events"].detect { |event| event["type"] == "Comment" }
          end

          it "creates a public comment" do
            assert @ticket.comments.last.is_public?
          end
        end

        describe "a private comment" do
          let(:comment) { { body: "New Comment", public: false } }

          it "creates a private comment" do
            refute @ticket.comments.last.is_public?
          end
        end
      end

      describe "with a valid channels comment" do
        before do
          ::Channels::Converter::ReplyInitiator.expects(:new).returns(stub('ReplyInitiator', reply: true))
          @ticket.update_column(:via_id, ViaType.TWITTER)
          @ticket.update_column(:requester_id, users(:minimum_end_user).id)
          mth = monitored_twitter_handles(:minimum_monitored_twitter_handle_1)
          channels_user_profiles(:minimum_twitter_user_profile_1).update_column(:external_id, mth.twitter_user_id)

          @ticket.requester.identities.twitter.first.update_column(:value, mth.twitter_user_id)
        end

        describe "and append ticket link is checked" do
          describe "and account has url shortener not adding by default" do
            before do
              @account.create_url_shortener!(name: 'custom', url: 'http://custom.org', add_url_by_default: false)
            end

            it "creates a ChannelBackEvent" do
              assert_difference('ChannelBackEvent.count(:all)', 1) do
                put :update, params: { id: @ticket.nice_id.to_s, ticket: {
                  comment: {
                    body: "New comment",
                    public: true,
                    channel_back: "1",
                    channel_source_id: MonitoredTwitterHandle.where(account_id: @account.id).first.id,
                    add_short_url: "1"
                  }
                } }
              end
            end
          end

          describe "and account doesn't have url shortener" do
            before do
              refute @account.url_shortener
            end

            it "creates a ChannelBackEvent" do
              assert_difference('ChannelBackEvent.count(:all)', 1) do
                put :update, params: { id: @ticket.nice_id.to_s, ticket: {
                  comment: {
                    body: "New comment",
                    public: true,
                    channel_back: "1",
                    channel_source_id: MonitoredTwitterHandle.where(account_id: @account.id).first.id,
                    add_short_url: "1"
                  }
                } }
              end
            end
          end
        end

        describe "and append ticket link is not checked" do
          describe "and account has url shortener adding by default" do
            before do
              @account.create_url_shortener!(name: 'custom', url: 'http://custom.org', add_url_by_default: true)
            end

            it "creates a ChannelBackEvent" do
              assert_difference('ChannelBackEvent.count(:all)', 1) do
                put :update, params: { id: @ticket.nice_id.to_s, ticket: {
                  comment: {
                    body: "New comment",
                    public: true,
                    channel_back: "1",
                    channel_source_id: MonitoredTwitterHandle.where(account_id: @account.id).first.id
                  }
                } }
              end
            end
          end
        end
      end

      describe "with a valid reply_option on the comment" do
        let(:comment) do
          {
            reply_option: {
              reply_via: [
                {
                  reply_via: 'Mail',
                  requester: 123
                }, {
                  comment: {
                    public: false
                  }
                }
              ]
            },
            body: "New Comment",
            public: true
          }
        end

        describe "and the reply_options Arturo flag is not enabled" do
          before do
            Arturo.disable_feature!(:reply_options)
          end

          it "ignores reply_option" do
            assert_difference('Comment.count(:all)', 1) do
              put :update, params: { id: @ticket.nice_id.to_s, ticket: { comment: comment } }
            end
          end
        end

        describe "and the reply_options Arturo flag is enabled" do
          before do
            Arturo.enable_feature!(:reply_options)
          end

          it "raises an exception" do
            assert_raises StandardError do
              put :update, params: { id: @ticket.nice_id.to_s, ticket: { comment: comment } }
            end
          end
        end
      end

      describe "with valid voice_comment" do
        before do
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { voice_comment: comment } }, as: :json
          @json_response = JSON.parse(@response.body)
        end

        let(:comment) do
          {
            from: "+14045555404",
            to: "+18005555800",
            recording_url: "http://domain.com/Accounts/AC97090f4127e39f6ba124883058a01f41/Recordings/CA0accec2ffe51a84bb7f4344e03b8b97a.mp3",
            recorded: true,
            recording_consent_action: nil,
            call_duration: 50,
            answered_by_id: 6,
            transcription_text: "a transcription",
            started_at: "2013-06-24 15:31:44 +0000",
            location: "location",
            author_id: @ticket.requester.id,
            public: true,
            brand_id: 2,
            via_id: Zendesk::Types::ViaType.PHONE_CALL_INBOUND,
            recording_type: 'call'
          }
        end

        it "sends audit with response" do
          assert_not_nil @json_response["audit"]
        end

        it "includes voice_comment with response" do
          assert_not_nil @json_response["audit"]["events"].detect { |event| event["type"] == "VoiceComment" }
        end
      end

      describe "with collaborator_ids" do
        let(:collaborator_id) { users(:minimum_end_user).id }

        before do
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { collaborator_ids: [collaborator_id] } }
          @json_response = JSON.parse(@response.body)
        end

        it "adds the collaborator" do
          assert_equal [collaborator_id], @json_response["ticket"]["collaborator_ids"]
        end
      end

      describe "with empty collaborator_ids" do
        let(:collaborator_id) { users(:minimum_end_user).id }

        before do
          @ticket.set_collaborators = [collaborator_id]
          @ticket.will_be_saved_by(@user)
          @ticket.save!
          assert_equal [collaborator_id], @ticket.reload.collaborators.map(&:id)
        end

        it "removes collaborators" do
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { collaborator_ids: [] } }, as: :json
          json = JSON.parse(@response.body)
          assert_empty json["ticket"]["collaborator_ids"]
        end
      end

      describe "with an inactive ticket form" do
        before do
          @account.stubs(:has_ticket_forms?).returns(true)
          @ticket_form = ticket_forms(:inactive_ticket_form)
          @controller.send(:current_account).ticket_forms << @ticket_form
        end

        describe "and ticket form is changed" do
          before do
            put :update, params: { id: @ticket.nice_id.to_s, ticket: {
              ticket_form_id: @ticket_form.id
            } }
          end

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
        end

        describe "and ticket form is unchanged" do
          before do
            @ticket.update_column(:ticket_form_id, @ticket_form.id)
            put :update, params: { id: @ticket.nice_id.to_s, ticket: {
              ticket_form_id: @ticket_form.id
            } }
          end

          it('responds with success') { assert_response :success }

          describe "and ticket form removed" do
            before do
              put :update, params: { id: @ticket.nice_id.to_s, ticket: {
                ticket_form_id: nil
              } }, as: :json
            end

            it('responds with success') { assert_response :success }
          end
        end
      end

      describe "with additional_collaborators" do
        before do
          @ticket.set_collaborators = [users(:minimum_agent).id]
          @ticket.will_be_saved_by(users(:minimum_admin))
          @ticket.save!

          assert_equal 1, @ticket.collaborators.size
          put :update, params: { id: @ticket.nice_id.to_s, ticket: {
            additional_collaborators: [users(:minimum_end_user).email]
          } }
        end

        it "adds collaborators" do
          assert_equal 2, @ticket.collaborators.reload.size
        end
      end

      describe "with TLS chat start and end events" do
        let(:comment) { { body: "New Comment", public: true } }
        let(:rule_id) { rules(:trigger_notify_requester_of_comment_update).id }

        before do
          Account.any_instance.stubs(:has_rule_usage_stats?).returns(true)
        end

        describe_with_arturo_disabled :enable_triggers_for_new_chat_ticket do
          it 'runs triggers when ticket is not created by TLS' do
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { comment: comment } }
            assert TicketJoin.exists?(rule_id: rule_id, ticket_id: @ticket.id)
          end

          it 'runs triggers when TLS chat has ended' do
            add_chat_end_event(@ticket, chat_started_event: add_chat_start_event(@ticket))
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { comment: comment } }
            assert TicketJoin.exists?(rule_id: rule_id, ticket_id: @ticket.id)
          end

          it 'runs triggers when TLS ChatEndedEvent is more than ChatStartedEvent' do
            # when number of ChatStartedEvent <= ChatEndedEvent
            chat_started_event = add_chat_start_event(@ticket)
            add_chat_end_event(@ticket, chat_started_event: chat_started_event)
            add_chat_end_event(@ticket, chat_started_event: chat_started_event)
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { comment: comment } }
            assert TicketJoin.exists?(rule_id: rule_id, ticket_id: @ticket.id)
          end

          it 'does not run triggers when TLS chat is still ongoing' do
            add_chat_start_event(@ticket)
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { comment: comment } }
            assert_empty(TicketJoin.where(rule_id: rule_id, ticket_id: @ticket.id))
          end
        end

        describe_with_arturo_enabled :enable_triggers_for_new_chat_ticket do
          it 'runs triggers even when TLS chat is still ongoing' do
            add_chat_start_event(@ticket)
            put :update, params: { id: @ticket.nice_id.to_s, ticket: { comment: comment } }
            assert TicketJoin.exists?(rule_id: rule_id, ticket_id: @ticket.id)
          end
        end
      end

      describe 'with valid external domain' do
        let(:comment) { { body: "New Comment", public: false } }

        it 'returns a valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { comment: comment } }
          assert_equal response.headers['Access-Control-Allow-Origin'], 'http://app.futuresimple.com'
        end

        it 'returns a not valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { comment: comment } }
          assert_nil response.headers['Access-Control-Allow-Origin']
        end
      end
    end

    describe "a PUT to #mark_as_spam" do
      before do
        @requester  = User.find(tickets(:minimum_1).requester.id)
        @ticket     = Ticket.find(tickets(:minimum_1).id)
      end

      describe 'when ticket cannot be mark as spam' do
        before do
          Ticket.any_instance.stubs(:mark_as_spam_by!).returns(false)
          put :mark_as_spam, params: { id: @ticket.nice_id }
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }

        it 'should contains an Error type and correct message' do
          json = JSON.parse(@response.body)
          assert_equal 'UnprocessableEntity', json['error']
          assert_equal I18n.t('txt.ticket.actions.mark_as_spam.error'), json['description']
        end

        it 'does not soft delete the ticket or suspend the user' do
          refute @ticket.reload.deleted?
          refute @requester.suspended?
        end
      end

      describe "with a regular user" do
        before do
          put :mark_as_spam, params: { id: @ticket.nice_id }
        end

        it('responds with success') { assert_response :success }

        it "soft delete the ticket and mark the reqester as suspended" do
          assert @ticket.reload.deleted?
          assert @requester.suspended?
        end
      end
    end

    describe "a POST to #merge" do
      let(:source) { tickets(:minimum_1) }
      let(:source2) { tickets(:minimum_3) }
      let(:target) { tickets(:minimum_2) }

      it "fails to merge a ticket to itself" do
        post :merge, params: {id: target.nice_id, ids: [target.nice_id], target_comment: "TARGET", source_comment: "SOURCE"}
        assert_response :unprocessable_entity, json['error']
        assert_equal "You selected the same ticket as source and target: ##{target.nice_id}. You cannot merge a ticket into itself.", json['description']
      end

      it "merges source tickets into target ticket" do
        post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE" }
        assert_response :success

        source.reload
        target.reload

        assert_includes source.comments.last.body, "SOURCE"
        refute target.status?(:closed)

        assert_includes target.comments.last.body, "TARGET"
        assert source.status?(:closed)

        assert_equal ViaType.MERGE, source.audits.last.via_id
        assert_equal ViaType.MERGE, target.audits.last.via_id
      end

      describe "comment privacy parameters" do
        describe "when sources and target are public" do
          before do
            assert source.is_public?
            assert target.is_public?
          end

          it "defaults to private comments" do
            post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE" }
            assert_response :success

            source.reload
            target.reload

            assert_includes source.comments.last.body, "SOURCE"
            assert source.comments.last.is_private?

            assert_includes target.comments.last.body, "TARGET"
            assert target.comments.last.is_private?
          end

          it "is possible to override default privacy" do
            post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE", target_comment_is_public: true, source_comment_is_public: true }
            assert_response :success

            source.reload
            target.reload

            assert_includes source.comments.last.body, "SOURCE"
            assert source.comments.last.is_public?

            assert_includes target.comments.last.body, "TARGET"
            assert target.comments.last.is_public?
          end

          describe_with_arturo_disabled :override_comment_publicity_in_ticket_merge_endpoint do
            it "is not possible to override default privacy" do
              post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE", target_comment_is_public: true, source_comment_is_public: true }
              assert_response :success

              source.reload
              target.reload

              assert_includes source.comments.last.body, "SOURCE"
              assert source.comments.last.is_private?

              assert_includes target.comments.last.body, "TARGET"
              assert target.comments.last.is_private?
            end
          end
        end

        describe "when any of the sources are private" do
          before do
            source.comments.map { |c| c.update_column(:is_public, false) }
            source.update_column(:is_public, false)
            assert source.reload.is_private?
          end

          it "always adds private comments" do
            post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE", target_comment_is_public: true, source_comment_is_public: true }
            assert_response :success

            source.reload
            target.reload

            assert_includes source.comments.last.body, "SOURCE"
            assert source.comments.last.is_private?

            assert_includes target.comments.last.body, "TARGET"
            assert target.comments.last.is_private?
          end
        end

        describe "when the target is private" do
          before do
            target.comments.map { |c| c.update_column(:is_public, false) }
            target.update_column(:is_public, false)
            assert target.reload.is_private?
          end

          it "always adds private comments" do
            post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE", target_comment_is_public: true, source_comment_is_public: true }
            assert_response :success

            source.reload
            target.reload

            assert_includes source.comments.last.body, "SOURCE"
            assert source.comments.last.is_private?

            assert_includes target.comments.last.body, "TARGET"
            assert target.comments.last.is_private?
          end
        end

        describe "when any of the sources are channel tickets" do
          before do
            source.update_column(:via_id, ViaType.Twitter)
            assert source.reload.via_channel?
          end

          it "always adds private comments" do
            post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE", target_comment_is_public: true, source_comment_is_public: true }
            assert_response :success

            source.reload
            target.reload

            assert_includes source.comments.last.body, "SOURCE"
            assert source.comments.last.is_private?

            assert_includes target.comments.last.body, "TARGET"
            assert target.comments.last.is_private?
          end
        end
      end

      describe "Reasons it does not enqueue TicketMergeJob: " do
        before { TicketMergeJob.expects(:enqueue).never }

        describe "it has an invalid source" do
          it "responds with unprocessable_entity" do
            source.status_id = StatusType.CLOSED
            source.will_be_saved_by(@user)
            source.save!
            post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE" }
            assert_response :unprocessable_entity
          end
        end

        describe "it has an invalid target" do
          it "responds with unprocessable_entity when target ticket is closed" do
            target.status_id = StatusType.CLOSED
            target.will_be_saved_by(@user)
            target.save!
            post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE" }
            assert_response :unprocessable_entity
          end

          describe "the source ticket and target ticket have different requesters" do
            describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
              it "returns unprocessable_entity" do
                Account.any_instance.stubs(:is_collaboration_enabled?).returns(false)
                post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE" }
                assert_response :unprocessable_entity
              end
            end

            describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
              describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
                describe_with_arturo_setting_enabled :ticket_followers_allowed do
                  it "returns unprocessable_entity when source requesters are not ALL agents" do
                    source2.requester = users(:minimum_agent)
                    source2.will_be_saved_by(@user)
                    source2.save!
                    refute [source, source2].all? { |s| s.requester.is_agent? }
                    post :merge, params: { id: target.nice_id, ids: [source.nice_id, source2.nice_id], target_comment: "TARGET", source_comment: "SOURCE" }
                    assert_response :unprocessable_entity
                  end
                end

                describe_with_arturo_setting_disabled :ticket_followers_allowed do
                  it "returns unprocessable_entity" do
                    source2.requester = users(:minimum_agent)
                    source2.will_be_saved_by(@user)
                    source2.save!
                    refute [source, source2].all? { |s| s.requester.is_agent? }
                    post :merge, params: { id: target.nice_id, ids: [source.nice_id, source2.nice_id], target_comment: "TARGET", source_comment: "SOURCE" }
                    assert_response :unprocessable_entity
                  end
                end
              end
            end
          end
        end

        it "has an invalid actor" do
          # TODO: set @user to light_agent or something else user specific that would fail can_merge_ticket?
          User.any_instance.expects(:can?).with(:merge, source).returns(false)
          post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE" }
          assert_response :unprocessable_entity
        end
      end

      it "tracks merge" do
        post :merge, params: { id: target.nice_id, ids: [source.nice_id], target_comment: "TARGET", source_comment: "SOURCE" }
      end
    end

    describe "a GET to #related" do
      describe "success" do
        before { get :related, params: { id: tickets(:minimum_1).nice_id } }
        should_use_presenter Api::V2::Tickets::RelatedPresenter
      end

      describe "Ticket is from HelpCenter" do
        let(:ticket) { tickets(:minimum_1) }

        [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
          describe "via_id is #{via_id}" do
            before do
              ticket.update_column(:via_id, via_id)
              ticket.update_column(:via_reference_id, ViaType.HELPCENTER)
            end

            describe "agent_as_end_user Arturo is disabled" do
              before do
                Arturo.disable_feature!(:agent_as_end_user)
              end

              it "succeeds" do
                get :related, params: { id: ticket.nice_id }
                assert_response :ok
              end
            end

            describe "agent_as_end_user Arturo is enabled" do
              before do
                Arturo.enable_feature!(:agent_as_end_user)
              end

              it "fails" do
                get :related, params: { id: ticket.nice_id }
                assert_response :forbidden
              end
            end
          end
        end
      end
    end

    describe "a PUT to #update_many" do
      before do
        @tickets = [tickets(:minimum_1), tickets(:minimum_2)]
        @ticket_ids = @tickets.map(&:id).join(",")
      end

      describe "with valid ids and nil array for an attribute" do
        before do
          put :update_many, params: { tickets: [{ id: @ticket_ids[0], subject: "wombat subject", comment: {body: "hi comment", uploads: nil} }] }, as: :json
        end

        it 'returns sucess' do
          assert_response :success, @controller.response
        end
      end

      describe "with valid params" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          TicketBulkUpdateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)

          @controller.expects(:store_bulk_macro_usage)

          put :update_many, params: { ids: @ticket_ids, ticket: { macro_id: 1, subject: "Updated Subject" } }
        end

        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "without :macro_id" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          TicketBulkUpdateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)

          @controller.expects(:store_bulk_macro_usage).never

          put :update_many, params: { ids: @ticket_ids, ticket: { subject: "Updated Subject" } }
        end

        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with additional_tags or remove_tags (with ids via query params)" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          presenter_stub = stub
          TicketBulkUpdateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          presenter_stub.expects(:present).with(job_status_stub).returns(nil)
          @controller.expects(:job_status_presenter).returns(presenter_stub)
        end

        it "whitelists additional_tags" do
          put :update_many, params: { ids: @ticket_ids, ticket: { additional_tags: "additional_tags" } }
          assert_equal 'additional_tags', @controller.params[:ticket][:additional_tags]
        end

        it "whitelists remove_tags" do
          put :update_many, params: { ids: @ticket_ids, ticket: { remove_tags: "remove_tags" } }
          assert_equal 'remove_tags', @controller.params[:ticket][:remove_tags]
        end
      end

      describe "with additional_tags or remove_tags (with inline ids)" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          presenter_stub = stub

          TicketBatchUpdateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          presenter_stub.expects(:present).with(job_status_stub).returns(nil)
          @controller.expects(:job_status_presenter).returns(presenter_stub)
        end

        it "whitelists additional_tags" do
          put :update_many, params: { tickets: [{ id: @tickets[0].id, additional_tags: "additional_tags" }] }
          assert_equal 'additional_tags', @controller.params[:tickets][0][:additional_tags]
        end

        it "whitelists remove_tags" do
          put :update_many, params: { tickets: [{ id: @tickets[0].id, remove_tags: "remove_tags" }] }
          assert_equal 'remove_tags', @controller.params[:tickets][0][:remove_tags]
        end
      end

      describe "with valid batch params" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          TicketBatchUpdateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)

          put :update_many, params: { tickets: [{ id: @ticket_ids[0], subject: "Updated Subject" }, { id: @ticket_ids[1], subject: "Updated Subject" }] }
        end

        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with disable_triggers" do
        it "passes the option through to the job" do
          Api::V2::JobStatusPresenter.any_instance.expects(:present).returns(job_status: 'stub_response')
          @controller.expects(:enqueue_job_with_status).with(TicketBulkUpdateJob, has_entry('disable_triggers' => true)).once

          put :update_many, params: { ids: @ticket_ids, ticket: { remove_tags: "remove_tags" }, disable_triggers: true }
        end
      end

      describe "with invalid params" do
        before { put :update_many }
        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "disallows > 100 individual attributes" do
        before { put_update_many(101) }
        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with duplicate ids in batch params" do
        before do
          put :update_many, params: { tickets: [{ id: 1 }, { id: 1 }] }
        end
        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with duplicate ids" do
        before do
          put :update_many, params: { ids: [1, 1, 2, 3].join(",") }
        end
        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "when payload is application/json and `followers` is an array" do
        let(:user) { users(:minimum_admin) }
        let(:params) do
          {
            ids: @ticket_ids,
            ticket: {
              macro_id: 1,
              subject: "Updated Subject",
              followers: [{
                user_id: user.id,
                user_email: user.email,
                user_name: user.name,
                action: "put"
              }]
            }
          }
        end

        it "returns success" do
          Api::V2::JobStatusPresenter.any_instance.expects(:present).returns(job_status: 'stub_response')
          @controller.expects(:enqueue_job_with_status).with(TicketBulkUpdateJob, has_entry('ticket' => has_entry('followers' => instance_of(Array)))).once
          put :update_many, params: params
          assert_response :success, @controller.response
        end
      end

      describe "when payload is application/x-www-form-urlencoded and `followers` is a map" do
        let(:user) { users(:minimum_admin) }
        let(:params) do
          {
            ids: @ticket_ids,
            ticket: {
              macro_id: 1,
              subject: "Updated Subject",
              followers: {
                "0" => {
                  user_id: user.id,
                  user_email: user.email,
                  user_name: user.name,
                  action: "put"
                }
              }
            }
          }
        end

        it "returns success" do
          Api::V2::JobStatusPresenter.any_instance.expects(:present).returns(job_status: 'stub_response')
          @controller.expects(:enqueue_job_with_status).with(TicketBulkUpdateJob, has_entry('ticket' => has_entry('followers' => kind_of(Hash)))).once
          put :update_many, params: params
          assert_response :success, @controller.response
        end
      end
    end

    describe "a DELETE to #destroy" do
      before { delete :destroy, params: { id: tickets(:minimum_1).nice_id.to_s } }
      it('responds with forbidden') { assert_response :forbidden }
    end

    describe "a DELETE to #destroy_many" do
      before do
        @tickets = [tickets(:minimum_1), tickets(:minimum_2)]
        @ticket_ids = @tickets.map(&:id).join(",")
        delete :destroy_many, params: { ids: @ticket_ids }
      end
      it('responds with forbidden') { assert_response :forbidden }
    end

    describe "a GET to #ccd" do
      before { get :ccd, params: { user_id: users(:minimum_end_user).id } }

      should_use_presenter Api::V2::Tickets::TicketPresenter, with: :ccd_tickets

      describe "with the exclude_count request param" do
        it "includes count if excluded" do
          assert_includes JSON.parse(@response.body), 'count'
        end

        it "includes count if false" do
          get :ccd, params: { user_id: users(:minimum_end_user).id, exclude_count: false }
          assert_includes JSON.parse(@response.body), 'count'
        end

        it "excludes count if true" do
          get :ccd, params: { user_id: users(:minimum_end_user).id, exclude_count: true }
          refute_includes JSON.parse(@response.body), 'count'
        end
      end
    end

    describe "a GET to #followed" do
      before { get :followed, params: { user_id: users(:minimum_end_user).id } }

      should_use_presenter Api::V2::Tickets::TicketPresenter, with: :followed_tickets

      describe "with the exclude_count request param" do
        it "includes count if excluded" do
          assert_includes JSON.parse(@response.body), 'count'
        end

        it "includes count if false" do
          get :followed, params: { user_id: users(:minimum_end_user).id, exclude_count: false }
          assert_includes JSON.parse(@response.body), 'count'
        end

        it "excludes count if true" do
          get :followed, params: { user_id: users(:minimum_end_user).id, exclude_count: true }
          refute_includes JSON.parse(@response.body), 'count'
        end
      end
    end

    describe "a GET to #assigned" do
      describe "agents" do
        before { get :assigned, params: { user_id: users(:minimum_admin).id } }

        should_use_presenter Api::V2::Tickets::TicketPresenter, with: :assigned_tickets

        describe "with the exclude_count request param" do
          it "includes count if excluded" do
            assert_includes JSON.parse(@response.body), 'count'
          end

          it "includes count if false" do
            get :assigned, params: { user_id: users(:minimum_end_user).id, exclude_count: false }
            assert_includes JSON.parse(@response.body), 'count'
          end

          it "excludes count if true" do
            get :assigned, params: { user_id: users(:minimum_end_user).id, exclude_count: true }
            refute_includes JSON.parse(@response.body), 'count'
          end
        end
      end

      describe "end_users" do
        before do
          User.any_instance.expects(:assigned).never
          get :assigned, params: { user_id: users(:minimum_end_user).id }
        end

        should_use_presenter Api::V2::Tickets::TicketPresenter
      end
    end

    describe "a GET to #requested" do
      describe "minimum_end_user" do
        before { get :requested, params: { user_id: users(:minimum_end_user).id } }

        should_use_presenter Api::V2::Tickets::TicketPresenter, with: :requested_tickets
      end

      describe "with ticket_requested_cache_control_header off" do
        before { Arturo.disable_feature!(:ticket_requested_cache_control_header) }

        it "sets cache-control to max-age=0" do
          get :requested, params: { user_id: users(:minimum_end_user).id }
          assert_nil @response.headers['Cache-Control']
        end
      end

      describe "with ticket_requested_cache_control_header on" do
        before { Arturo.enable_feature!(:ticket_requested_cache_control_header) }

        it "sets cache-control to max-age=60" do
          get :requested, params: { user_id: users(:minimum_end_user).id }
          assert_equal @response.headers['Cache-Control'], 'max-age=60, private'
        end
      end

      describe "with the exclude_count request param" do
        describe_with_arturo_enabled :exclude_archived_default_for_user_tickets do
          it "excludes counting archived if excluded" do
            @controller.expects(:count_method).returns("count")
            get :requested, params: { user_id: users(:minimum_end_user).id }
          end
        end

        describe_with_and_without_arturo_enabled :exclude_archived_default_for_user_tickets do
          it "includes count if excluded" do
            get :requested, params: { user_id: users(:minimum_end_user).id }
            assert_includes JSON.parse(@response.body), 'count'
          end

          it "includes count if false" do
            get :requested, params: { user_id: users(:minimum_end_user).id, exclude_count: false }
            assert_includes JSON.parse(@response.body), 'count'
          end

          it "excludes count if true" do
            get :requested, params: { user_id: users(:minimum_end_user).id, exclude_count: true }
            refute_includes JSON.parse(@response.body), 'count'
          end
        end
      end

      describe "with the exclude_archive request param" do
        before do
          @user = users(:minimum_end_user)
          @ticket = @user.tickets.last
          archive_and_delete(@ticket)
        end
        it "includes archived if exclude_archived param not given" do
          get :requested, params: { user_id: @user.id }
          assert_includes JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, @ticket.nice_id
        end

        describe_with_arturo_enabled :exclude_archived_default_for_user_tickets do
          it "excludes archived if exclude_archived param not given" do
            get :requested, params: { user_id: @user.id }
            refute_includes JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, @ticket.nice_id
          end
        end

        describe_with_and_without_arturo_enabled :exclude_archived_default_for_user_tickets do
          it "includes archived if false" do
            get :requested, params: { user_id: @user.id, exclude_archived: false }
            assert_includes JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, @ticket.nice_id
          end

          it "excludes archived if true" do
            get :requested, params: { user_id: @user.id, exclude_archived: true }
            refute_includes JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, @ticket.nice_id
          end

          it "includes next_page if applicable" do
            get :requested, params: { user_id: @user.id, per_page: 1, exclude_archived: true }
            JSON.parse(@response.body)['next_page'].wont_be_empty
          end
        end
      end
    end

    describe "a GET to #recent" do
      before { get :recent, params: { user_id: users(:minimum_end_user).id } }

      should_use_presenter Api::V2::Tickets::TicketPresenter, with: :recent_tickets
    end

    describe "a GET to #related" do
      before { @ticket = tickets(:minimum_2) }

      describe 'when the ticket is not assigned to the agent' do
        it 'responds with success when the agent can view the ticket' do
          get :related, params: { id: @ticket.nice_id }
          assert_response :success
        end

        it 'responds with forbidden when the agent cannot view the ticket' do
          @user.update_attribute(:restriction_id, RoleRestrictionType.ASSIGNED)
          get :related, params: { id: @ticket.nice_id }

          assert_response :forbidden
        end
      end
    end

    describe "lookup method" do
      let(:minimum_agent)    { users(:minimum_agent) }
      let(:minimum_end_user) { users(:minimum_end_user) }

      let!(:legacy_collaborated_ticket) do
        minimum_end_user.tickets[0].tap do |ticket|
          ticket.collaborations.create(user_id: minimum_end_user.id, collaborator_type: CollaboratorType.LEGACY_CC)
        end
      end
      let!(:followed_ticket) do
        minimum_end_user.tickets[1].tap do |ticket|
          ticket.collaborations.create(user_id: minimum_agent.id, collaborator_type: CollaboratorType.FOLLOWER)
        end
      end
      let!(:ccd_ticket) do
        minimum_end_user.tickets[2].tap do |ticket|
          ticket.collaborations.create(user_id: minimum_end_user.id, collaborator_type: CollaboratorType.EMAIL_CC)
        end
      end
      let!(:agent_legacy_collaborated_ticket) do
        minimum_end_user.tickets[3].tap do |ticket|
          ticket.collaborations.create(user_id: minimum_agent.id, collaborator_type: CollaboratorType.LEGACY_CC)
        end
      end
      let!(:archived_ticket) do
        minimum_end_user.tickets[4].tap do |ticket|
          ticket.collaborations.create(user_id: minimum_end_user.id, collaborator_type: CollaboratorType.EMAIL_CC)
          ticket.collaborations.create(user_id: minimum_agent.id, collaborator_type: CollaboratorType.FOLLOWER)
        end
      end

      before do
        archive_and_delete(archived_ticket)

        @user = minimum_end_user
        Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: true)
      end

      describe "#ccd_tickets" do
        before { @controller.params[:user_id] = @user.id }

        describe "with comment_email_ccs_allowed enabled" do
          before { Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true) }

          it "paginates the ordered ccd_tickets of the user" do
            tickets = @user.ccd_tickets.for_user(minimum_agent)
            options = {order: "#{Ticket::STATUS_SQL_ORDER}, created_at DESC", method: 'archived_paginate', total_entries: nil}
            @controller.expects(:paginate).with(tickets, options).returns(tickets)
            assert_equal Set.new([ccd_ticket, legacy_collaborated_ticket]), Set.new(@controller.send(:ccd_tickets))
          end

          describe "archived tickets" do
            describe "when exclude_archived request param not present" do
              it "includes archived tickets" do
                get :ccd, params: { user_id: @user.id }
                assert_equal JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, [ccd_ticket.nice_id, legacy_collaborated_ticket.nice_id, archived_ticket.nice_id]
              end
            end

            describe "when exclude_archived request param is false" do
              it "includes archived tickets" do
                get :ccd, params: { user_id: @user.id, exclude_archived: false }
                assert_equal JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, [ccd_ticket.nice_id, legacy_collaborated_ticket.nice_id, archived_ticket.nice_id]
              end
            end

            describe "when exclude_archived request param is true" do
              it "does not include archived tickets" do
                get :ccd, params: { user_id: @user.id, exclude_archived: true }
                refute_includes JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, archived_ticket.nice_id
              end
            end
          end

          describe "when agent is added as a LEGACY_CC on the ticket" do
            it "includes the legacy collaborated ticket in the ccd_tickets" do
              get :ccd, params: { user_id: minimum_agent.id }
              assert_includes JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, agent_legacy_collaborated_ticket.nice_id
            end
          end
        end

        describe "with comment_email_ccs_allowed disabled" do
          before { Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: false) }

          it "returns an empty array" do
            @controller.expects(:paginate).never
            assert_equal [], @controller.send(:ccd_tickets)
          end
        end

        describe "with comment_email_ccs_allowed AND follower_and_email_cc_collaborations disabled" do
          before do
            Account.any_instance.stubs(
              has_comment_email_ccs_allowed_enabled?: false,
              has_follower_and_email_cc_collaborations_enabled?: false
            )
          end

          it "paginates the ordered collaborated_tickets of the user (legacy behavior)" do
            tickets = @user.collaborated_tickets.for_user(minimum_agent)
            options = {order: "#{Ticket::STATUS_SQL_ORDER}, created_at DESC", method: 'archived_paginate', total_entries: nil}
            @controller.expects(:paginate).with(tickets, options).returns(tickets)
            assert_equal Set.new([ccd_ticket, legacy_collaborated_ticket]), Set.new(@controller.send(:ccd_tickets))
          end

          describe "archived tickets" do
            describe "when exclude_archived request param not present" do
              it "includes archived tickets" do
                get :ccd, params: { user_id: @user.id }
                assert_equal JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, [ccd_ticket.nice_id, legacy_collaborated_ticket.nice_id, archived_ticket.nice_id]
              end
            end

            describe "when exclude_archived request param is false" do
              it "includes archived tickets" do
                get :ccd, params: { user_id: @user.id, exclude_archived: false }
                assert_equal JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, [ccd_ticket.nice_id, legacy_collaborated_ticket.nice_id, archived_ticket.nice_id]
              end
            end

            describe "when exclude_archived request param is true" do
              it "does not include archived tickets" do
                get :ccd, params: { user_id: @user.id, exclude_archived: true }
                refute_includes JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, archived_ticket.nice_id
              end
            end
          end
        end
      end

      describe "#followed_tickets" do
        before do
          @user = minimum_agent
          @controller.params[:user_id] = @user.id
        end

        describe "with ticket_followers_allowed enabled" do
          before { Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true) }

          it "paginates the ordered followed_tickets of the user" do
            tickets = @user.followed_tickets.for_user(minimum_agent)
            options = {order: "#{Ticket::STATUS_SQL_ORDER}, created_at DESC", method: 'archived_paginate', total_entries: nil}
            @controller.expects(:paginate).with(tickets, options).returns(tickets)
            assert_equal [followed_ticket], @controller.send(:followed_tickets)
          end

          describe "archived tickets" do
            describe "when exclude_archived request param not present" do
              it "includes archived tickets" do
                get :followed, params: { user_id: @user.id }
                assert_equal JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, [followed_ticket.nice_id, archived_ticket.nice_id]
              end
            end

            describe "when exclude_archived request param is false" do
              it "includes archived tickets" do
                get :followed, params: { user_id: @user.id, exclude_archived: false }
                assert_equal JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, [followed_ticket.nice_id, archived_ticket.nice_id]
              end
            end

            describe "when exclude_archived param is true" do
              it "does not include archived tickets" do
                get :followed, params: { user_id: @user.id, exclude_archived: true }
                refute_includes JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, archived_ticket.nice_id
              end
            end
          end

          describe "when agent is added as a LEGACY_CC on the ticket" do
            it "does not include the legacy collaborated ticket in the followed_tickets" do
              get :followed, params: { user_id: @user.id }
              refute_includes JSON.parse(@response.body)['tickets'].map { |t| t['id'] }, agent_legacy_collaborated_ticket.nice_id
            end
          end
        end

        describe "with ticket_followers_allowed disabled" do
          before { Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: false) }

          it "returns an empty array" do
            @controller.expects(:paginate).never
            assert_equal [], @controller.send(:followed_tickets)
          end
        end
      end

      describe "#assigned_tickets" do
        before do
          @user = minimum_agent
          @controller.params[:user_id] = @user.id
        end

        it "paginates the ordered assigned_tickets of the user" do
          tickets = @user.assigned
          options = {order: "#{Ticket::STATUS_SQL_ORDER}, created_at DESC", method: 'archived_paginate', total_entries: nil}
          @controller.expects(:paginate).with(tickets, options).returns("paginated tickets")
          assert_equal "paginated tickets", @controller.send(:assigned_tickets)
        end

        describe "with the exclude_archived request param" do
          let(:tickets) { @user.assigned }
          let(:options) { {order: "#{Ticket::STATUS_SQL_ORDER}, created_at DESC", method: page_method, total_entries: nil} }
          describe "archives are included when" do
            let(:page_method) { 'archived_paginate' }

            it "it is excluded" do
              @controller.expects(:paginate).with(tickets, options).returns(@tickets)
              @controller.send(:assigned_tickets)
            end

            it "it is false" do
              @controller.params[:exclude_archived] = false

              @controller.expects(:paginate).with(tickets, options).returns(@tickets)
              @controller.send(:assigned_tickets)
            end
          end

          describe "archives are excluded when" do
            let(:page_method) { 'paginate' }

            it "is true" do
              @controller.params[:exclude_archived] = true

              @controller.expects(:paginate).with(tickets, options).returns(@tickets)
              @controller.send(:assigned_tickets)
            end
          end
        end
      end

      describe "#requested_tickets" do
        before do
          @controller.params[:user_id] = @user.id

          @user.tickets.each do |ticket|
            User.any_instance.stubs(:can?).with(:view, ticket).returns(false)
          end
        end

        describe "with Arturo feature enabled" do
          before do
            Arturo.enable_feature!(:api_filter_requested_tickets)
          end

          describe "with exclude_archived false" do
            it "only returns tickets which current_user can view" do
              assert_equal [], @controller.send(:requested_tickets)
            end
          end

          describe "with exclude_archived true" do
            before do
              @controller.params[:exclude_archived] = true
            end

            it "only returns tickets which current_user can view" do
              assert_equal [], @controller.send(:requested_tickets)
            end
          end
        end

        describe "with Arturo feature disabled" do
          before do
            Arturo.disable_feature!(:api_filter_requested_tickets)
          end

          it "returns all the tickets" do
            assert_equal @user.tickets, @controller.send(:requested_tickets)
          end
        end
      end

      describe "organization tickets" do
        before do
          @organization = organizations(:minimum_organization1)
          @controller.params[:organization_id] = @organization.id
        end

        it "paginates the tickets of the organization" do
          sql = @controller.send(:tickets).to_sql
          assert sql.end_with?("LIMIT 100 OFFSET 0")
        end

        it "supports cursor-based pagination and sorts by nice_id" do
          scope = @organization.
            tickets.
            use_index(Ticket::ACCOUNT_AND_NICE_ID_INDEX).
            reorder(nice_id: :asc)

          @controller.stubs(:with_cursor_pagination_v2?).returns(true)
          @controller.expects(:paginate_with_cursor).with(scope)
          @controller.send(:tickets)
        end

        describe "with a restricted agent" do
          before do
            User.any_instance.stubs(:agent_restriction?).with(:groups).returns(false)
            User.any_instance.stubs(:agent_restriction?).with(:organization).returns(false)
            User.any_instance.stubs(:agent_restriction?).with(:assigned).returns(true)
          end

          it "restricts tickets to the agent" do
            sql = @controller.send(:tickets).to_sql
            user_id = @controller.send(:current_user).id
            assert sql =~ /tickets\.assignee_id = #{user_id} OR tickets\.requester_id = #{user_id}/
          end
        end
      end

      describe "#tickets" do
        it "paginates the tickets of the account with offset-based pagination" do
          sql = @controller.send(:tickets).to_sql
          assert sql.end_with?("ORDER BY nice_id ASC LIMIT 100 OFFSET 0")
        end

        it "supports cursor-based pagination and sorts by nice_id" do
          scope = @account.
            tickets.
            use_index(Ticket::ACCOUNT_AND_NICE_ID_INDEX).
            reorder(nice_id: :asc)

          @controller.stubs(:with_cursor_pagination_v2?).returns(true)
          @controller.expects(:paginate_with_cursor).with(scope)
          @controller.send(:tickets)
        end

        describe "with a restricted agent" do
          before do
            User.any_instance.stubs(:agent_restriction?).with(:groups).returns(false)
            User.any_instance.stubs(:agent_restriction?).with(:organization).returns(false)
            User.any_instance.stubs(:agent_restriction?).with(:assigned).returns(true)
          end

          it "restricts tickets to the agent" do
            sql = @controller.send(:tickets).to_sql
            user_id = @controller.send(:current_user).id
            assert sql =~ /tickets\.assignee_id = #{user_id} OR tickets\.requester_id = #{user_id}/
          end
        end
      end
    end
  end

  as_an_admin do
    describe "a DELETE to #destroy" do
      it "responds with 204" do
        delete :destroy, params: { id: tickets(:minimum_1).nice_id.to_s }
        assert_response :no_content
      end

      it "soft-deletes the ticket" do
        delete :destroy, params: { id: tickets(:minimum_1).nice_id.to_s }
        assert tickets(:minimum_1).reload.deleted?
      end

      it "sets the via type to WEB_FORM for Lotus calls" do
        request.headers['X-Zendesk-Lotus-Version'] = '123'

        delete :destroy, params: { id: tickets(:minimum_1).nice_id.to_s }
        assert_equal ViaType.WEB_FORM, tickets(:minimum_1).reload.events.last.via_id
      end

      it "sets the via type to WEB_SERVICE for regular API calls" do
        delete :destroy, params: { id: tickets(:minimum_1).nice_id.to_s }
        assert_equal ViaType.WEB_SERVICE, tickets(:minimum_1).reload.events.last.via_id
      end
    end

    describe "a DELETE to #destroy_many" do
      describe "with ticket ids" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          TicketBulkUpdateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)

          @tickets = [tickets(:minimum_1), tickets(:minimum_2)]
          @ticket_ids = @tickets.map(&:id).join(",")

          delete :destroy_many, params: { ids: @ticket_ids }
        end

        should_use_presenter Api::V2::JobStatusPresenter
      end
    end

    describe "rate limits on #destroy" do
      describe "when account has a configured rate limit" do
        let(:rate_limit) { 5 }

        before do
          @account.update_attributes!(settings: { ticket_deletions_threshold: rate_limit })
          rate_limit.times { Prop.throttle(:ticket_deletions, @account.id) }
          delete :destroy, params: { id: tickets(:minimum_1).nice_id.to_s }
        end

        it "responds with too many requests" do
          assert_response 429
        end
      end

      describe "when account has no configured rate limit" do
        let(:rate_limit) { Prop.configurations[:ticket_deletions][:threshold] }

        before do
          @account.update_attributes!(settings: { ticket_deletions_threshold: rate_limit })
          rate_limit.times { Prop.throttle(:ticket_deletions, @account.id) }
          delete :destroy, params: { id: tickets(:minimum_1).nice_id.to_s }
        end

        it "responds with too many requests" do
          assert_response 429
        end
      end
    end

    describe "a GET to #index" do
      describe "as the root resource" do
        before { get :index }

        should_use_presenter Api::V2::Tickets::TicketPresenter, with: :tickets
      end

      describe "sorting by locale" do
        before { get :index, params: { sort_by: 'locale' } }

        it "uses the custom sort_by" do
          assert_equal "FIELD(locale_id, #{@controller.send(:current_account).available_languages.map(&:id).join(',')})", @controller.send(:sort_by)
        end
      end

      describe "sorting by status" do
        before do
          hold_ticket = FactoryBot.create(:ticket)
          hold_ticket.update_attributes(status_id: StatusType.HOLD)

          get :index, params: { sort_by: 'status', sort_order: 'asc' }
        end

        it "uses the custom sort_by with onhold ticket" do
          # Double checks sorting from controller, and also results
          assert_equal Ticket::STATUS_SQL_SORT_BY, @controller.send(:sort_by)

          result_order = @controller.send(:tickets).map(&:status_id).uniq
          # We want all original constants sorted by score, from the result we have
          expected_order = StatusType.order & result_order

          assert_equal expected_order, result_order
        end
      end

      describe "sorting by updated_at" do
        describe_with_arturo_enabled :force_ticket_index_index do
          it "forces the index for updated_at" do
            tickets = @account.tickets
            Account.any_instance.stubs(:tickets).returns(tickets)
            tickets.stubs(:with_best_index).with("updated_at").returns(tickets).once

            get :index, params: { sort_by: 'updated_at', sort_order: 'desc' }
          end
        end

        describe_with_arturo_disabled :force_ticket_index_index do
          it "forces the index for updated_at" do
            tickets = @account.tickets
            @controller.stubs(:account_tickets).returns(tickets)
            tickets.stubs(:with_best_index).with("updated_at").returns(tickets).never

            get :index, params: { sort_by: 'updated_at', sort_order: 'desc' }
          end
        end
      end

      describe 'without specific pagination parameters' do
        let(:account) { accounts(:minimum) }
        let(:json) do
          get :index
          JSON.parse(@response.body).with_indifferent_access
        end

        it 'uses offset based pagination by default' do
          assert json.key?(:next_page)
          assert json.key?(:previous_page)
        end
      end

      describe 'using offset based pagination' do
        let(:account) { accounts(:minimum) }
        let(:json) do
          get :index, params: { page: 2, per_page: 1 }
          JSON.parse(@response.body).with_indifferent_access
        end

        it 'presents the correct keys' do
          assert json[:previous_page].present?
          assert json[:next_page].present?
        end

        it 'presents the legacy fields key' do
          ticket_json = json[:tickets].first
          assert_not_nil ticket_json[:fields]
        end
      end

      # For a complete set of tests, go to `zendesk_cursor_pagination` gem.
      describe 'using cursor based pagination' do
        let(:account) { accounts(:minimum) }
        let(:page_size) { 2 }
        let(:page_params) { { size: page_size } }
        let(:sort) { '' }
        let(:params) { { page: page_params, sort: sort } }
        let(:json) do
          get :index, params: params
          JSON.parse(@response.body).with_indifferent_access
        end

        it 'generates the correct order clause' do
          assert_sql_queries(1, /ORDER BY `tickets`.`nice_id` ASC/) do
            json
          end
        end

        it 'does not present the legacy fields key' do
          ticket_json = json[:tickets].first
          assert_nil ticket_json[:fields]
        end

        describe 'when there is only one page' do
          let(:page_size) { account.tickets.size + 1 }

          it 'presents the correct keys' do
            refute json[:meta][:has_more]
            assert json[:meta][:before_cursor].present?
            assert json[:meta][:after_cursor].present?
            assert json[:links][:prev].present?
            assert json[:links][:next].present?
          end

          it 'presents the correct number of tickets' do
            assert_equal account.tickets.size, json[:tickets].size
          end
        end

        describe 'when there are multiple pages' do
          let(:page_size) { account.tickets.size - 1 }

          it 'presents the correct keys' do
            assert json[:meta][:has_more]
            assert json[:meta][:before_cursor].present?
            assert json[:meta][:after_cursor].present?
            assert json[:links][:prev].present?
            assert json[:links][:next].present?
          end

          it 'presents the correct number of tickets' do
            assert_equal page_size, json[:tickets].size
          end
        end

        describe 'with a valid after cursor' do
          let(:page_size) { account.tickets.size - 1 }
          let(:after_json) { JSON.parse(@response.body).with_indifferent_access }

          before do
            after_cursor = json[:meta][:after_cursor]

            get :index, params: { page: {
              size: page_size,
              after: after_cursor
            } }
          end

          it 'presents the correct keys' do
            refute after_json[:meta][:has_more]
            assert after_json[:meta][:before_cursor].present?
            assert after_json[:meta][:after_cursor].present?
            assert after_json[:links][:prev].present?
            assert after_json[:links][:next].present?
          end

          it 'presents the correct number of tickets' do
            assert_equal 1, after_json[:tickets].size
          end
        end

        describe 'with an invalid after cursor' do
          let(:page_params) do
            {
              size: page_size,
              after: 'invalid'
            }
          end

          it 'presents an error message' do
            assert_equal 'InvalidPaginationParameter', json[:error]
            assert_equal 'page[after] is not valid', json[:description]
            assert_response :bad_request
          end
        end

        describe 'when we filter by organization' do
          let(:organization) { organizations(:minimum_organization1) }
          let(:page_size) { organization.tickets.size + 1 }
          let(:params) { { page: page_params, organization_id: organization.id } }

          it 'presents the correct keys' do
            refute json[:meta][:has_more]
            assert json[:meta][:before_cursor].present?
            assert json[:meta][:after_cursor].present?
            assert json[:links][:prev].present?
            assert json[:links][:next].present?
          end

          it 'presents the correct number of tickets' do
            assert_equal organization.tickets.size, json[:tickets].size
          end
        end

        describe 'when sort parameter is id' do
          let(:sort) { '-id' }

          it 'does not include id' do
            assert_sql_queries(0, /`tickets`.`id` ASC/) do
              json
            end
          end
        end

        describe 'when sort parameter has multiple fields' do
          let(:sort) { '-created_at,id' }

          it 'shows an error message' do
            json
            assert_response :bad_request
            assert @response.body =~ /sort is not valid/
          end
        end

        describe 'when sort parameter has invalid fields' do
          let(:sort) { '-invalid,id' }

          it 'shows an error message' do
            json
            assert_response :bad_request
            assert @response.body =~ /sort is not valid/
          end
        end
      end
    end
  end

  as_an_admin do
    describe "origin driven rate limit for API" do
      let(:account) { accounts(:minimum) }
      let(:ticket) { tickets(:minimum_1) }

      before do
        Timecop.freeze(Time.zone.parse('2019-03-17 08:28:43'))
      end

      after { Timecop.return }

      describe "for ticket#update" do
        describe 'sends Zendesk-EP header when only origin_limit_ticket_api_log: true' do
          before do
            Arturo.enable_feature! :origin_limit_ticket_api_log
            Arturo.disable_feature!(:origin_limit_ticket_api)
          end

          it 'returns zendesk-ep header with a value of -10' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-07-02'))
            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
            assert_response :success
            assert_equal -10, @controller.response.headers["ZENDESK-EP"]
          end
        end

        describe 'sends Zendesk-EP header when origin_limit_ticket_api: true AND origin_limit_ticket_api_log: true' do
          before do
            Arturo.enable_feature! :origin_limit_ticket_api
            Arturo.enable_feature! :origin_limit_ticket_api_log
          end

          it 'returns zendesk-ep header with a value of 10' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-07-02'))
            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
            assert_response :success
            assert_equal 10, @controller.response.headers["ZENDESK-EP"]
          end
        end

        describe 'does not send Zendesk-EP header when origin_limit_ticket_api: false AND origin_limit_ticket_api_log: false' do
          before do
            Arturo.disable_feature!(:origin_limit_ticket_api)
            Arturo.disable_feature!(:origin_limit_ticket_api_simulate)
          end
          it 'returns zendesk-ep header with a value of nil' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-07-02'))
            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
            assert_response :success
            assert_nil @controller.response.headers["ZENDESK-EP"]
          end
        end

        describe 'does not send Zendesk-EP header when origin_limit_ticket_api_log: false' do
          before do
            Arturo.enable_feature!(:origin_limit_ticket_api)
            Arturo.disable_feature!(:origin_limit_ticket_api_simulate)
          end
          it 'returns zendesk-ep header with a value of nil' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-07-02'))
            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
            assert_response :success
            assert_nil @controller.response.headers["ZENDESK-EP"]
          end
        end
      end
    end
  end

  as_an_admin do
    describe "throttling the API" do
      let(:account) { accounts(:minimum) }
      let(:ticket) { tickets(:minimum_1) }

      before do
        Timecop.freeze(Time.zone.parse('2019-03-17 08:28:43'))
      end

      after { Timecop.return }

      describe "with throttling ticket index with ticket quantities" do
        before do
          Arturo.enable_feature!(:use_scaling_strategies_for_rate_limiting)
          Arturo.enable_feature!(:throttle_endpoint_limit_ticket_index_api)
          Arturo.enable_feature!(:throttle_endpoint_limit_ticket_index_api_master)
        end

        it "should send ScalingStrategy TicketQuantities to Datadog" do
          Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)
          Account.any_instance.stubs(:created_at).returns(Time.parse('2012-07-02'))
          @controller.stubs(:warn_throttle_monitor?).returns(true)
          statsd_client = @controller.send(:statsd_client)

          statsd_client.expects(:increment).with("warning_throttled.v2_tickets_index", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:true', 'page:-1', 'request_count:1', 'limit:20000', 'plan_type:Medium', 'strategy:ProductLimits::ScalingStrategies::TicketQuantities', 'high_volume_account:false']).once

          get :index
          assert_response :success
        end

        it "should send ScalingStrategies Stationary to Datadog" do
          Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)
          Account.any_instance.stubs(:created_at).returns(Time.parse('2021-07-02'))
          @controller.stubs(:warn_throttle_monitor?).returns(true)
          statsd_client = @controller.send(:statsd_client)

          statsd_client.expects(:increment).with("warning_throttled.v2_tickets_index", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:1', 'limit:100000', 'plan_type:Medium', 'strategy:ProductLimits::ScalingStrategies::Stationary', 'high_volume_account:false']).once

          get :index
          assert_response :success
        end
      end

      describe "with throttling ticket index" do
        before do
          account.settings.ticket_index_threshold = 1
          account.save!
          Arturo.enable_feature!(:throttle_endpoint_limit_ticket_index_api)
          Arturo.enable_feature!(:throttle_endpoint_limit_ticket_index_api_master)
        end

        it "should not respond with success" do
          Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)
          Account.any_instance.stubs(:created_at).returns(Time.parse('2020-07-02'))
          get :index
          assert_response :success

          statsd_client = @controller.send(:statsd_client)
          statsd_client.expects(:increment).with("throttled.v2_tickets_index", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:1', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).once

          get :index
          assert_response 429
        end

        it "should log true for high volume addon" do
          Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)
          Account.any_instance.stubs(:created_at).returns(Time.parse('2020-07-02'))
          Subscription.any_instance.stubs(:has_high_volume_api?).returns(true)

          get :index
          assert_response :success

          statsd_client = @controller.send(:statsd_client)
          statsd_client.expects(:increment).with("throttled.v2_tickets_index", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:1', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:true']).once

          get :index
          assert_response 429
        end

        it "should log the page number round up by 25s" do
          Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)
          Account.any_instance.stubs(:created_at).returns(Time.parse('2020-07-02'))
          get :index, params: { page: 11 }
          assert_response :success

          statsd_client = @controller.send(:statsd_client)
          statsd_client.expects(:increment).with("throttled.v2_tickets_index", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:25', 'request_count:1', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).once
          get :index, params: { page: 10 }
          assert_response 429
        end

        it "should handle CBPv2 requests where the page parameter is a Hash" do
          Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)
          Account.any_instance.stubs(:created_at).returns(Time.parse('2020-07-02'))
          get :index, params: { page: { size: 11 } }
          assert_response :success

          statsd_client = @controller.send(:statsd_client)
          statsd_client.expects(:increment).with("throttled.v2_tickets_index", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:1', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).once
          get :index, params: { page: { size: 10 } }
          assert_response 429
        end

        describe "limit types" do
          before do
            account.settings.ticket_index_threshold = nil
            account.settings.save!
          end

          it 'should use the pagination limits' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-06-30'))
            Prop.expects(:throttle!).with(:v2_tickets_index_pagination, account.id,
              threshold: 700,
              interval: 1.minute,
              description: ::I18n.t("txt.admin.controllers.tickets_controller.limited_ticket_index_pagination")).returns(1)

            get :index, params: { page: 105 }
          end

          it "should use the high volume addon limits" do
            Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)
            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-06-30'))
            Subscription.any_instance.stubs(:has_high_volume_api?).returns(true)
            Prop.expects(:throttle!).with(:v2_tickets_index_deep_pagination, account.id,
              threshold: 50,
              interval: 1.minute,
              description: ::I18n.t("txt.admin.controllers.tickets_controller.limited_ticket_index_pagination")).returns(1)

            get :index, params: { page: 505 }
            assert_response :success
          end

          it 'should use the deep pagination limits' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-06-30'))
            Prop.expects(:throttle!).with(:v2_tickets_index_deep_pagination, account.id,
              threshold: 50,
              interval: 1.minute,
              description: ::I18n.t("txt.admin.controllers.tickets_controller.limited_ticket_index_pagination")).returns(1)

            get :index, params: { page: 600 }
          end

          describe 'custom limits' do
            before do
              account.settings.ticket_index_deep_threshold = 11
              account.settings.ticket_index_shallow_threshold = 102
              account.settings.save!
            end

            it "should use the custom shallow pagination limits" do
              Account.any_instance.stubs(:created_at).returns(Time.parse('2019-06-30'))
              Prop.expects(:throttle!).with(:v2_tickets_index_pagination, account.id,
                threshold: 102,
                interval: 1.minute,
                description: ::I18n.t("txt.admin.controllers.tickets_controller.limited_ticket_index_pagination")).returns(1)

              get :index, params: { page: 60 }
            end

            it "should use the custom deep pagination limits" do
              Account.any_instance.stubs(:created_at).returns(Time.parse('2019-06-30'))
              Prop.expects(:throttle!).with(:v2_tickets_index_deep_pagination, account.id,
                threshold: 11,
                interval: 1.minutes,
                description: ::I18n.t("txt.admin.controllers.tickets_controller.limited_ticket_index_pagination")).returns(1)

              get :index, params: { page: 600 }
            end
          end
        end
      end

      describe 'with master switch ON' do
        before do
          account.settings.ticket_index_threshold = 1
          account.save!
          Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)
          Arturo.enable_feature!(:throttle_endpoint_limit_ticket_index_api_master)
        end

        describe "with other arturo OFF" do
          before do
            Arturo.disable_feature!(:throttle_endpoint_limit_ticket_index_api)
          end

          it 'should not throttle a grandfathered account' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2020-06-02'))
            get :index
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.stubs(:increment)

            get :index
            assert_response :success
          end

          it 'should throttle a new account' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2020-07-02'))
            get :index
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.stubs(:increment)

            get :index
            assert_response 429
          end

          it 'should not throttle a new account if the arturo is misspelled' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2020-07-02'))
            @controller.stubs(:throttle_arturo_master).returns("misspelled_arturo_hence_off")
            get :index
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.stubs(:increment)

            get :index
            assert_response :success
          end

          it 'should not throttle a old account if the arturo is misspelled' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2020-06-30'))
            @controller.stubs(:throttle_arturo).returns("misspelled_arturo_hence_off")
            get :index
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.stubs(:increment)

            get :index
            assert_response :success
          end
        end

        describe "with other arturo ON" do
          before do
            Arturo.enable_feature!(:throttle_endpoint_limit_ticket_index_api)
          end

          it 'should throttle a grandfathered account' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2020-06-02'))
            get :index
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.stubs(:increment)

            get :index
            assert_response 429
          end

          it 'should throttle a new account' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2020-07-02'))
            get :index
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.stubs(:increment)

            get :index
            assert_response 429
          end
        end
      end

      describe 'with master switch OFF' do
        before do
          account.settings.ticket_index_threshold = 1
          account.save!
          Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)
          Arturo.disable_feature!(:throttle_endpoint_limit_ticket_index_api_master)
        end

        describe "with other arturo OFF" do
          before do
            Arturo.disable_feature!(:throttle_endpoint_limit_ticket_index_api)
          end

          it 'should not throttle a grandfathered account' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2020-07-02'))
            get :index
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.stubs(:increment)

            get :index
            assert_response :success
          end

          it 'should not throttle a non-grandfathered account' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2020-07-02'))
            get :index
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.stubs(:increment)

            get :index
            assert_response :success
          end
        end

        describe "with other arturo ON" do
          before do
            Arturo.enable_feature!(:throttle_endpoint_limit_ticket_index_api)
          end
          it 'should not throttle a grandfathered account' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2020-07-02'))
            get :index
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.stubs(:increment)

            get :index
            assert_response :success
          end
          it 'should not throttle a non-grandfathered account' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2020-07-02'))
            get :index
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.stubs(:increment)

            get :index
            assert_response :success
          end
        end
      end

      describe "without throttling ticket update" do
        before do
          account.settings.ticket_updates_threshold = 1
          account.save!
          Arturo.disable_feature!(:throttle_endpoint_limit_ticket_api)
          Arturo.enable_feature!(:throttle_endpoint_limit_ticket_api_master)
          Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)

          Account.any_instance.stubs(:created_at).returns(Time.parse('2019-06-18'))
        end

        describe "and grandfathered date" do
          it "should rate limit any account created AFTER the set grandfathered date" do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-07-02'))
            assert account.created_at > ProductLimits::Controllers::DEFAULT_LIMITS[:v2_tickets_controller][:update][:grandfathered_date]

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.expects(:increment).with("throttled.v2_tickets_update", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:1', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).once

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
            assert_response 429
          end

          it "should not rate limit any account created before the grandfathered date" do
            assert account.created_at < ProductLimits::Controllers::DEFAULT_LIMITS[:v2_tickets_controller][:update][:grandfathered_date]

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.expects(:increment).with("throttled.v2_tickets_update", tags: ["log_only:true", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:true', 'page:-1', 'request_count:2', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).once

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
            assert_response :success
          end

          it "should not rate limit an account if no grandfathered date is set and arturo is off" do
            Arturo.disable_feature!(:throttle_endpoint_limit_ticket_api)
            default_limit_stub = ProductLimits::Controllers::DEFAULT_LIMITS[:v2_tickets_controller][:update].dup
            default_limit_stub.delete(:grandfathered_date)
            @controller.stubs(:controller_throttling_limits).returns(default_limit_stub)

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.expects(:increment).with("throttled.v2_tickets_update", tags: ["log_only:true", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:2', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).once

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
            assert_response :success
          end

          it "should rate limit an account if no grandfathered date is set and arturo is on" do
            Arturo.enable_feature!(:throttle_endpoint_limit_ticket_api)
            Arturo.enable_feature!(:throttle_endpoint_limit_ticket_api_master)
            default_limit_stub = ProductLimits::Controllers::DEFAULT_LIMITS[:v2_tickets_controller][:update].dup
            default_limit_stub.delete(:grandfathered_date)
            @controller.stubs(:controller_throttling_limits).returns(default_limit_stub)

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.expects(:increment).with("throttled.v2_tickets_update", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:1', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).once

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
            assert_response 429
          end
        end
      end

      describe "with throttling ticket update" do
        before do
          account.settings.ticket_updates_threshold = 1
          account.save!
          Arturo.enable_feature!(:throttle_endpoint_limit_ticket_api)
          Arturo.enable_feature!(:throttle_endpoint_limit_ticket_api_master)
        end

        it "should not respond with success" do
          Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)
          Account.any_instance.stubs(:created_at).returns(Time.parse('2019-07-02'))
          put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
          assert_response :success

          statsd_client = @controller.send(:statsd_client)
          statsd_client.expects(:increment).with("throttled.v2_tickets_update", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:1', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).once

          put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
          assert_response 429
        end

        it "should not send to datadog with supression arturo" do
          Arturo.enable_feature!(:suppress_sending_datadog_threshold_data)
          put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
          assert_response :success

          statsd_client = @controller.send(:statsd_client)
          statsd_client.expects(:increment).with("throttled.v2_tickets_update", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:1', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).never

          put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
          assert_response 429
        end

        describe "limit types" do
          before do
            account.settings.ticket_updates_threshold = nil
            account.settings.save!
            Ticket.any_instance.stubs(:throttle_updates).returns(true)
          end

          it 'should use the grandfathered default limits' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-06-30'))
            Prop.expects(:throttle!).with(:v2_tickets_update, account.id,
              threshold: 100,
              interval: 1.minute,
              description: ::I18n.t("txt.admin.controllers.tickets_controller.limited_ticket_updates")).returns(1)

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
          end

          it 'should use the default limits' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-07-02'))
            Prop.expects(:throttle!).with(:v2_tickets_update, account.id,
              threshold: 100,
              interval: 1.minute,
              description: ::I18n.t("txt.admin.controllers.tickets_controller.limited_ticket_updates")).returns(1)

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
          end
        end

        describe "and a grandfathered date" do
          before do
            Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)

            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-06-18'))
          end

          it "should rate limit any account created AFTER the set grandfathered date" do
            Arturo.disable_feature!(:throttle_endpoint_limit_ticket_api)
            Arturo.enable_feature!(:throttle_endpoint_limit_ticket_api_master)
            Account.any_instance.stubs(:created_at).returns(Time.parse('2019-07-02'))
            assert account.created_at > ProductLimits::Controllers::DEFAULT_LIMITS[:v2_tickets_controller][:update][:grandfathered_date]

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.expects(:increment).with("throttled.v2_tickets_update", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:1', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).once

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
            assert_response 429
          end

          it "should not rate limit any account created before the grandfathered date" do
            Arturo.disable_feature!(:throttle_endpoint_limit_ticket_api)
            assert account.created_at < ProductLimits::Controllers::DEFAULT_LIMITS[:v2_tickets_controller][:update][:grandfathered_date]

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
            assert_response :success

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
            assert_response :success
          end

          it "should rate limit when prop returns Float::INFINITY" do
            Arturo.disable_feature!(:throttle_endpoint_limit_ticket_api)
            Prop.stubs(:throttle).returns(Float::INFINITY)

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
            assert_response :success
          end

          it "should rate limit an account if no grandfathered date is set" do
            Arturo.disable_feature!(:throttle_endpoint_limit_ticket_api)
            default_limit_stub = ProductLimits::Controllers::DEFAULT_LIMITS[:v2_tickets_controller][:update].dup
            default_limit_stub.delete(:grandfathered_date)
            @controller.stubs(:controller_throttling_limits).returns(default_limit_stub)

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject1" } }
            assert_response :success

            statsd_client = @controller.send(:statsd_client)
            statsd_client.expects(:increment).with("throttled.v2_tickets_update", tags: ["log_only:true", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:2', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).once

            put :update, params: { id: ticket.nice_id.to_s, ticket: { subject: "Updated Subject2" } }
            assert_response :success
          end
        end
      end
    end
  end

  describe "embeddable subsystem user" do
    as_a_subsystem_user(user: "embeddable", account: :minimum) do
      should_be_forbidden(
        [:get, :requested, {user_id: 1}],
        [:get, :ccd, {user_id: 1}],
        [:get, :followed, {user_id: 1}],
        [:get, :assigned, {user_id: 1}],
        :recent,
        :show,
        :show_many,
        :create,
        :create_many,
        :update,
        :update_many,
        :mark_many_as_spam,
        [:get, :merge, {id: 1}],
        [:get, :related, {id: 1}],
        :destroy,
        :destroy_many
      )

      describe "a GET to :index" do
        before { get :index }
        it("responds with success") { assert_response :success }
      end
    end
  end

  describe "collaboration subsystem user" do
    as_a_subsystem_user(user: "collaboration", account: :minimum) do
      should_be_forbidden(
        [:get, :requested, {user_id: 1}],
        [:get, :ccd, {user_id: 1}],
        [:get, :followed, {user_id: 1}],
        [:get, :assigned, {user_id: 1}],
        :recent,
        :index,
        :show_many,
        :create_many,
        :update,
        :update_many,
        :mark_many_as_spam,
        [:get, :merge, {id: 1}],
        [:get, :related, {id: 1}],
        :destroy,
        :destroy_many
      )

      should_be_authorized { post :create, params: { ticket: { subject: "test", description: "test", requester: {name: "requester", email: "need_a_requester@zopim.com"} } } }
      should_be_authorized { get :show, params: {id: 1} }
    end
  end

  describe "a zopim user" do
    as_a_subsystem_user(user: 'zopim', account: :minimum) do
      should_be_forbidden(
        [:get, :ccd, {user_id: 1}],
        [:get, :followed, {user_id: 1}],
        :recent,
        :destroy,
        :destroy_many,
        :update_many,
        :mark_many_as_spam,
        :create_many,
        [:put, :mark_as_spam, {id: 1}]
      )

      should_be_authorized { get :index }
      should_be_authorized { get :show, params: { id: tickets(:minimum_1).nice_id } }
      should_be_authorized { get :requested, params: { user_id: users(:minimum_end_user).id } }
      should_be_authorized { post :create, params: { ticket: { subject: "test", description: "test", requester: {name: "requester", email: "need_a_requester@zopim.com"} } } }
      should_be_authorized { put :update, params: { id: tickets(:minimum_1).nice_id, ticket: { subject: "Updated"} } }

      describe "a GET to #show" do
        before { get :show, params: { id: tickets(:minimum_1).nice_id } }
        it('responds with success') { assert_response :success }
      end

      describe "a GET to #index" do
        before { get :index }
        it('responds with success') { assert_response :success }
      end

      describe "a GET to #requested" do
        before { get :requested, params: { user_id: users(:minimum_end_user).id } }

        it('responds with success') { assert_response :success }
      end

      describe "a POST to #create" do
        before { post :create, params: { ticket: { subject: "test", description: "test", requester: {name: "requester", email: "need_a_requester@zopim.com"} } } }
        it('responds with created') { assert_response :created }
      end

      describe "a PUT to #update" do
        before do
          @ticket = tickets(:minimum_1)
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { subject: "Updated Subject" } }
        end
        it('responds with success') { assert_response :success }
      end
    end
  end

  describe "a symphony user" do
    as_a_subsystem_user(user: 'symphony', account: :minimum) do
      should_be_forbidden(
        [:get, :ccd, {user_id: 1}],
        [:get, :followed, {user_id: 1}],
        :recent,
        :destroy,
        :destroy_many,
        :update_many,
        :mark_many_as_spam,
        :create_many,
        [:put, :mark_as_spam, {id: 1}]
      )

      should_be_authorized { get :index }
      should_be_authorized { get :show, params: { id: tickets(:minimum_1).nice_id } }
      should_be_authorized { get :requested, params: { user_id: users(:minimum_end_user).id } }
      should_be_authorized { post :create, params: { ticket: { subject: "test", description: "test", requester: {name: "requester", email: "need_a_requester@zendesk.com"} } } }
      should_be_authorized { put :update, params: { id: tickets(:minimum_1).nice_id, ticket: { subject: "Updated"} } }

      describe "a GET to #show" do
        before { get :show, params: { id: tickets(:minimum_1).nice_id } }
        it('responds with success') { assert_response :success }
      end

      describe "a GET to #index" do
        before { get :index }
        it('responds with success') { assert_response :success }
      end

      describe "a GET to #requested" do
        before { get :requested, params: { user_id: users(:minimum_end_user).id } }

        it('responds with success') { assert_response :success }
      end

      describe "a POST to #create" do
        before { post :create, params: { ticket: { subject: "test", description: "test", requester: {name: "requester", email: "need_a_requester@zendesk.com"} } } }
        it('responds with created') { assert_response :created }
      end

      describe "a POST to #create" do
        it('with contextual_workspace parameter responds with created') do
          post :create, params: {
            ticket: {
              subject: "test",
              description: "test",
              requester: {
                name: "requester",
                email: "need_a_requester@zendesk.com"
              },
              tde_workspace: {
                workspace: {
                  id: 1,
                  title: 'refunds workspace'
                },
                type: 'ADD'
              }
            }
          }
          assert_response :created
        end
      end

      describe "a PUT to #update" do
        it('with contextual_workspace parameter responds with success') do
          @ticket = tickets(:minimum_1)
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { subject: "Updated Subject", tde_workspace: { workspace: { id: 2, title: 'refunds_workspace' }, type: 'CHANGE', previous_workspace: { id: 1, title: 'billing_workspace' } } } }
          assert_response :success
        end
      end

      describe "a PUT to #update" do
        before do
          @ticket = tickets(:minimum_1)
          put :update, params: { id: @ticket.nice_id.to_s, ticket: { subject: "Updated Subject" } }
        end
        it('responds with success') { assert_response :success }
      end
    end
  end

  describe "aws_integration subsystem user" do
    as_a_subsystem_user(user: "aws_integration", account: :minimum) do
      should_be_forbidden(
        [:get, :requested, {user_id: 1}],
        [:get, :ccd, {user_id: 1}],
        [:get, :followed, {user_id: 1}],
        [:get, :assigned, {user_id: 1}],
        :recent,
        :index,
        :show,
        :show_many,
        :create_many,
        :update,
        :update_many,
        :mark_many_as_spam,
        [:get, :merge, {id: 1}],
        [:get, :related, {id: 1}],
        :destroy,
        :destroy_many
      )

      describe "a POST to #create" do
        before { post :create, params: { ticket: { subject: "test", description: "test", comment: { 'body': 'comment body' }, requester: {name: "requester", email: "need_a_requester@zendesk.com"} } } }
        it('responds with created') { assert_response :created }
      end
    end

    as_a_subsystem_user(user: "aws_integration_test", account: :minimum) do
      should_be_forbidden(:create)
    end
  end

  describe "as any subsystem user" do
    as_a_subsystem_user(user: "zendesk", account: :minimum) do
      describe "a PUT to #update_many" do
        it 'responds with success' do
          put_update_many(1)

          assert_response :ok
        end

        it 'responds with too_many_requests if the database load is too high' do
          ZendeskDatabaseSupport::Sensors::AuroraCpuSensor.any_instance.stubs(:cpu_utilization).returns(100)

          put_update_many(1)

          assert_response :too_many_requests
        end
      end
    end

    as_a_subsystem_user(user: "aws_integration_test", account: :minimum) do
      should_be_forbidden(:create)
    end
  end

  private

  def put_update_many(count)
    put :update_many, params: { tickets: Array.new(count) { |i| { "id" => i, "subject" => "Updated subject" } } }
  end

  def add_chat_start_event(ticket)
    ticket.will_be_saved_by(users(:minimum_admin))
    ChatStartedEvent.new(ticket: ticket, value: { chat_id: 'foo', visitor_id: 'bar' }).tap(&:apply!)
  end

  def add_chat_end_event(ticket, chat_started_event:)
    ticket.will_be_saved_by(users(:minimum_admin))
    ChatEndedEvent.new(ticket: ticket, chat_started_event: chat_started_event, value: { chat_id: 'foo', visitor_id: 'bar' }).tap(&:apply!)
  end
end
