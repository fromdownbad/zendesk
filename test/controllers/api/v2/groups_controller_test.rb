require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::V2::GroupsController do
  extend Api::V2::TestHelper

  fixtures :accounts,
    :users,
    :groups,
    :role_settings

  let(:account) { accounts(:minimum) }
  let(:user)    { users(:minimum_agent) }
  let(:minimum_end_user) { users(:minimum_end_user) }
  let(:response_groups) { JSON.parse(@response.body)['groups'] }

  before do
    https!
    accept :json

    @group = groups(:minimum_group)
    Group.any_instance.stubs(:remove_from_phone_group_routing!)
  end

  with_options(controller: 'api/v2/groups') do |request|
    request.should_route :get,    '/api/v2/groups',              action: 'index'
    request.should_route :get,    '/api/v2/groups/count',        action: 'count'
    request.should_route :get,    '/api/v2/groups/assignable',   action: 'assignable'
    request.should_route :get,    '/api/v2/groups/autocomplete', action: 'autocomplete'
    request.should_route :get,    '/api/v2/groups/1',            action: 'show', id: '1'
    request.should_route :post,   '/api/v2/groups',              action: 'create'
    request.should_route :put,    '/api/v2/groups/1',            action: 'update',  id: '1'
    request.should_route :delete, '/api/v2/groups/1',            action: 'destroy', id: '1'
    request.should_route :get,    '/api/v2/users/1/groups/count', action: 'count', user_id: "1"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :count,
      :create,
      :update,
      :destroy,
      :assignable,
      :autocomplete
  end

  as_an_end_user do
    should_be_forbidden :index, :count,
      :create,
      :update,
      :destroy,
      :assignable,
      :autocomplete
  end

  as_an_admin do
    describe "a GET TO :count " do
      it 'count should come back' do
        get :count
        assert_response :ok
        groups_json_keys = JSON.parse(response.body).keys
        assert_equal 1, (groups_json_keys & ['count']).size
      end
    end

    describe 'a GET to :index' do
      before { account.groups.create!(name: 'Bleh') }

      describe 'without any parameters' do
        before { get :index }

        should_use_presenter Api::V2::GroupPresenter

        describe "app requests" do
          before do
            request.headers.merge! "X-Zendesk-App-Id" => "3vil4pp3p1cHax"
          end

          describe_with_arturo_enabled :groups_controller_cache_control_header do
            before { get :index }

            it 'has cache headers' do
              assert_equal 'max-age=60, private', @response.headers['Cache-Control']
            end
          end

          describe_with_arturo_disabled :groups_controller_cache_control_header do
            before { get :index }

            it 'has no cache headers' do
              assert_nil @response.headers['Cache-Control']
            end
          end
        end

        describe "not an app request" do
          describe_with_arturo_enabled :groups_controller_cache_control_header do
            before { get :index }

            it 'should not have cache headers' do
              assert_nil @response.headers['Cache-Control']
            end
          end
        end

        it 'returns all groups' do
          assert_equal(
            ['Bleh', 'minimum_group'],
            response_groups.map { |group| group['name'] }
          )
        end
      end

      describe 'with `user_id` parameter' do
        before { get :index, params: { user_id: users(:minimum_agent).id } }

        should_use_presenter Api::V2::GroupPresenter

        it "only returns user's groups" do
          assert_equal(
            ['minimum_group'],
            response_groups.map { |group| group['name'] }
          )
        end
      end

      describe 'when account has more than 100 groups and less than 1,000 groups' do
        before do
          account.groups.destroy_all

          123.times { |index| account.groups.create!(name: "Bleh #{index}") }
        end

        describe 'with `per_page` parameter' do
          before { get :index, params: { per_page: 1_000 } }

          it 'returns all groups' do
            assert_equal 123, response_groups.length
          end
        end

        describe 'without `per_page` parameter' do
          before { get :index }

          it 'only returns 100 groups' do
            assert_equal 100, response_groups.length
          end
        end
      end

      describe 'when account has more than 1,000 groups' do
        before do
          account.groups.destroy_all

          1_234.times { |index| account.groups.create!(name: "Bleh #{index}") }
        end

        describe 'with `per_page` parameter' do
          before { get :index, params: { per_page: 1_000 } }

          it 'returns all groups' do
            assert_equal 1_000, response_groups.length
          end
        end

        describe 'without `per_page` parameter' do
          before { get :index }

          it 'only returns 100 groups' do
            assert_equal 100, response_groups.length
          end
        end
      end

      describe "with cursor pagination" do
        should_support_cursor_pagination_conversion :index, 'groups', :cursor_pagination_groups_index, :remove_offset_pagination_groups_index, sortable_fields: Api::V2::GroupsController::CBP_SORTABLE_FIELDS
      end
    end

    describe 'a GET to :show' do
      describe 'when a group is not deleted' do
        before { get :show, params: { id: @group.id } }

        should_use_presenter Api::V2::GroupPresenter
      end

      describe 'when Support is not active on a multiproduct account' do
        before do
          Account.any_instance.stubs(:multiproduct?).returns(true)
          @controller.stubs(:multiproduct_support_active?).returns(false)
        end

        it 'successfully returns group' do
          get :show, params: { id: @group.id }
          assert_response :ok
        end
      end

      describe 'when a group is deleted' do
        before do
          @group.update_attribute(:is_active, false)
          assert_predicate @group, :deleted?
          get :show, params: { id: @group.id }
        end

        should_use_presenter Api::V2::GroupPresenter
      end

      describe 'with deleted groups' do
        before do
          @group.update_attribute(:is_active, false)
          get :index

          @deleted_group = response_groups.detect { |group| group['id'] == @group.id }
        end

        it 'does not include deleted groups in the response' do
          assert_nil @deleted_group
        end
      end

      describe 'with valid external domain' do
        it 'returns a valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
          get :show, params: { id: @group.id }
          assert_equal 'http://app.futuresimple.com', @response.headers['Access-Control-Allow-Origin']
        end

        it 'returns a not valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
          get :show, params: { id: @group.id }
          assert_nil @response.headers['Access-Control-Allow-Origin']
        end
      end
    end

    describe 'a POST to :create' do
      describe 'with valid params' do
        before do
          Account.any_instance.stubs(:has_groups?).returns(true)
          post :create, params: { group: { name: 'Groupies', description: 'groupies description' } }
        end

        should_use_presenter Api::V2::GroupPresenter, status: :created, location: /\/api\/v2\/groups\/\d+\.json$/

        it 'creates the group' do
          refute_predicate @controller.send(:new_group), :new_record?
        end
      end

      describe 'with additional group_settings param' do
        before do
          Account.any_instance.stubs(:has_groups?).returns(true)
          post :create, params: { group: { name: 'Groupies', settings: { chat_enabled: true } } }
        end

        it 'creates the group' do
          group_id = JSON.parse(response.body)['group']['id']
          chat_enabled_settings = Group.find(group_id).settings.chat_enabled

          assert_predicate @controller.send(:new_group), :persisted?
          assert_equal true, chat_enabled_settings
        end
      end

      describe 'with invalid params' do
        before do
          Account.any_instance.stubs(:has_groups?).returns(true)
          post :create, params: { group: { name: '' } }
        end
        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
      end

      describe 'on account without groups feature' do
        before do
          Account.any_instance.stubs(:has_groups?).returns(false)
          post :create, params: { group: { name: 'Groupies' } }
        end

        it 'cannot create the group' do
          assert_response :forbidden
        end
      end
    end

    describe 'a PUT to :update' do
      describe 'update group name' do
        before { put :update, params: { id: @group.id, group: { name: 'group name' } } }

        should_use_presenter Api::V2::GroupPresenter

        it 'updates the group name' do
          assert_equal 'group name', @group.reload.name
        end
      end

      describe 'update group description' do
        before { put :update, params: { id: @group.id, group: { description: 'group description' } } }

        should_use_presenter Api::V2::GroupPresenter

        it 'updates the group' do
          assert_equal 'group description', @group.reload.description
        end
      end

      describe 'with group settings' do
        before do
          put :update, params: { id: @group.id, group: {
              settings: { chat_enabled: true }
            } }
        end

        it 'updates the group' do
          assert_predicate @group.reload.settings, :chat_enabled
        end
      end
    end

    describe 'a DELETE to :destroy' do
      describe 'the only group' do
        before { delete :destroy, params: { id: @group.id } }
        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
      end

      describe 'when there are multiple groups' do
        before do
          account.groups.create!(name: 'Oooo +1')
        end

        it 'responds with 204' do
          delete :destroy, params: { id: @group.id }
          assert_response :no_content
        end
      end
    end

    describe 'a GET to :assignable' do
      before { get :assignable }
      should_use_presenter Api::V2::GroupPresenter, with: :assignable_groups

      describe "with cursor pagination" do
        should_support_cursor_pagination_conversion :assignable, 'groups', :cursor_pagination_groups_assignable, :remove_offset_pagination_groups_assignable, sortable_fields: Api::V2::GroupsController::CBP_SORTABLE_FIELDS
      end
    end

    describe 'a GET to :assignable from apps' do
      describe "adds cache-control headers for apps" do
        before do
          request.headers.merge! "X-Zendesk-App-Id" => "b4dApp"
        end

        describe_with_arturo_enabled :groups_assignable_cache_control_header do
          it "responds with success and with cache headers" do
            get :assignable
            assert_response :success
            assert_equal 'max-age=60, private', @response.headers['Cache-Control']
          end
        end

        describe_with_arturo_disabled :groups_assignable_cache_control_header do
          it "responds with success and no cache headers" do
            get :assignable
            assert_response :success
            assert_nil @response.headers['Cache-Control']
          end
        end
      end
    end

    describe 'a GET to :autocomplete' do
      before { get :autocomplete }

      it('responds with `200 OK`') { assert_response :ok }
    end
  end

  as_an_agent do
    should_be_forbidden :create

    describe "a GET TO  :count " do
      it 'count should come back' do
        get :count
        assert_response :ok
        groups_json_keys = JSON.parse(response.body).keys
        assert_equal 1, (groups_json_keys & ['count']).size
      end

      it 'count should come back for userid' do
        get :count, params: { user_id: minimum_end_user.id }
        assert_response :ok
        groups_json_keys = JSON.parse(response.body).keys
        assert_equal 1, (groups_json_keys & ['count']).size
      end
    end

    describe 'a PUT to :update' do
      before { put :update, params: { id: @group.id } }
      it('responds with forbidden') { assert_response :forbidden }
    end

    describe 'a DELETE to :destroy' do
      before { delete :destroy, params: { id: @group.id } }
      it('responds with forbidden') { assert_response :forbidden }
    end

    describe 'a GET to :assignable' do
      before { get :assignable }
      it('responds with success') { assert_response :success }

      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }

      describe "with cursor pagination" do
        should_support_cursor_pagination_conversion :assignable, 'groups', :cursor_pagination_groups_assignable, :remove_offset_pagination_groups_assignable, sortable_fields: Api::V2::GroupsController::CBP_SORTABLE_FIELDS
      end
    end

    describe 'a GET to :autocomplete' do
      let(:matching_groups) do
        JSON.parse(@response.body)['groups'].map { |group| group['name'] }
      end

      before do
        Group.destroy_all

        %w[aDdA ab abc jkl bac].each do |name|
          account.groups.create(name: name)
        end
      end

      describe 'when the `name` parameter is not present' do
        before { get :autocomplete }

        it('responds with `200 OK`') { assert_response :ok }

        it 'returns no groups' do
          assert_empty matching_groups
        end
      end

      describe 'when the `name` parameter is empty' do
        before { get :autocomplete, params: { name: '' } }

        it('responds with `200 OK`') { assert_response :ok }

        it 'returns no groups' do
          assert_empty matching_groups
        end
      end

      describe 'when the `name` parameter does not match any groups' do
        before { get :autocomplete, params: { name: 'xyz' } }

        it('responds with `200 OK`') { assert_response :ok }

        it 'returns no groups' do
          assert_empty matching_groups
        end
      end

      describe 'when the `name` parameter matches groups' do
        before { get :autocomplete, params: { name: 'ab' } }

        it('responds with `200 OK`') { assert_response :ok }

        it 'returns matching groups' do
          assert_equal %w[ab abc], matching_groups
        end
      end

      describe 'when the `name` parameter matches the middle of a group name' do
        before { get :autocomplete, params: { name: 'b' } }

        it('responds with `200 OK`') { assert_response :ok }

        it 'returns matching groups in the right order' do
          assert_equal %w[bac ab abc], matching_groups
        end
      end

      describe 'when there are mismatched cases' do
        before { get :autocomplete, params: { name: 'ad' } }

        it('responds with `200 OK`') { assert_response :ok }

        it 'disregards case' do
          assert_equal %w[aDdA], matching_groups
        end
      end
    end
  end
  ["gooddata", "bime"].each do |subsystem_user|
    as_a_subsystem_user(user: subsystem_user, account: :minimum) do
      should_be_forbidden :create, :update, :destroy, :assignable

      describe 'for all groups' do
        before { get :index }
        it('responds with success') { assert_response :success }
      end

      describe "for an agent's groups" do
        before { get :index, params: { user_id: users(:minimum_agent).id } }
        it('responds with success') { assert_response :success }
      end

      describe 'with deleted groups' do
        before do
          @group.update_attribute(:is_active, false)
          get :index

          @deleted_group = response_groups.detect { |group| group['id'] == @group.id }
        end

        it 'includes deleted groups in the response' do
          assert @deleted_group
          assert @deleted_group['deleted']
        end
      end
    end
  end

  as_a_subsystem_user(user: "zopim", account: :minimum) do
    should_be_forbidden :destroy, :assignable, :autocomplete

    describe 'for all groups' do
      before { get :index }
      it('responds with success') { assert_response :success }
    end

    describe "for an agent's groups" do
      before { get :index, params: { user_id: users(:minimum_agent).id } }
      it('responds with success') { assert_response :success }
    end

    describe "for a group" do
      before { get :show, params: { id: @group.id } }
      it('responds with success') { assert_response :success }
    end

    describe 'create a group' do
      before do
        Account.any_instance.stubs(:has_groups?).returns(true)
        post :create, params: { group: { name: 'Groupies', description: 'groupies description', settings: { chat_enabled: true } } }
      end

      it('responds with success') { assert_response :success }
    end

    describe 'update a group' do
      before do
        put :update, params: { id: @group.id, group: { name: 'new name', settings: { chat_enabled: true } } }
      end

      it('responds with success') { assert_response :success }
    end
  end

  as_a_subsystem_user(user: 'collaboration', account: :minimum) do
    should_be_forbidden :create, :update, :destroy, :assignable, :autocomplete

    describe 'for all groups' do
      before { get :index }
      it('responds with success') { assert_response :success }
    end

    describe 'for a group' do
      before { get :show, params: { id: @group.id } }
      it('responds with success') { assert_response :success }
    end

    describe "for an agent's groups" do
      before { get :index, params: { user_id: users(:minimum_agent).id } }
      it('responds with success') { assert_response :success }
    end
  end

  as_a_subsystem_user(user: 'knowledge_api', account: :minimum) do
    should_be_forbidden :create, :update, :destroy, :assignable, :autocomplete

    describe 'for all groups' do
      before { get :index }
      it('responds with success') { assert_response :success }
    end
    describe 'for a group' do
      before { get :show, params: { id: @group.id } }
      it('responds with success') { assert_response :success }
    end

    describe "for an agent's groups" do
      before { get :index, params: { user_id: users(:minimum_agent).id } }
      it('responds with success') { assert_response :success }
    end
  end

  as_a_subsystem_user(user: 'audit_log_service', account: :minimum) do
    should_be_forbidden :create, :index, :update, :destroy, :assignable, :autocomplete

    describe 'for a group' do
      before { get :show, params: { id: @group.id } }
      it('responds with success') { assert_response :success }
    end
  end

  describe '#groups' do
    before { @controller.stubs(:current_account).returns(accounts(:minimum)) }

    describe 'with no user_id' do
      before { @controller.params.clear }

      it 'paginates all groups on the current account' do
        groups = stub(:groups)
        @controller.send(:current_account).groups.expects(:paginate).returns(groups)
        assert_equal groups, @controller.send(:groups)
      end
    end

    describe 'with a user_id' do
      before do
        @controller.params[:user_id] = user.id
      end

      it 'paginates all groups on the current account' do
        @controller.send(:current_account).users.expects(:find).with(user.id).returns(user)
        groups = stub(:groups)
        user.groups.expects(:paginate).returns(groups)
        assert_equal groups, @controller.send(:groups)
      end
    end
  end
end
