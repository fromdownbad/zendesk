require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"
require_relative '../../../support/multiproduct_test_helper'

SingleCov.covered! uncovered: 0

describe Api::V2::OrganizationsController do
  extend Api::V2::TestHelper
  include CustomFieldsTestHelper
  include DomainEventsHelper
  extend ApiScopesHelper
  fixtures :accounts, :users, :organizations

  let(:account) { accounts(:minimum) }
  let(:minimum_agent) { users(:minimum_agent) }
  let(:minimum_end_user) { users(:minimum_end_user) }
  let(:organization) { organizations(:minimum_organization1) }
  let(:organization2) { organizations(:minimum_organization2) }
  let(:organization_name) { "Wonderful Organization" }
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  before do
    Account.any_instance.stubs(:has_user_and_organization_fields?).returns(true)
    Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
    Organization.any_instance.stubs(domain_event_publisher: domain_event_publisher)

    # don't use these fixtures
    CustomField::Field.without_arsi.delete_all
    CustomField::Value.without_arsi.delete_all
    CustomField::Dropdown.without_arsi.delete_all
    use_ssl
    accept :json
  end

  with_options(controller: "api/v2/organizations") do |request|
    request.should_route :get,    "/api/v2/organizations", action: "index"
    request.should_route :get,    "/api/v2/organizations/count", action: "count"
    request.should_route :get,    "/api/v2/organizations/1", action: "show", id: "1"
    request.should_route :post,   "/api/v2/organizations", action: "create"
    request.should_route :put,    "/api/v2/organizations/1", action: "update", id: "1"
    request.should_route :delete, "/api/v2/organizations/1", action: "destroy", id: "1"
    request.should_route :post,   "/api/v2/organizations/autocomplete", action: "autocomplete" # deprecated
    request.should_route :get,    "/api/v2/organizations/autocomplete", action: "autocomplete"
    request.should_route :get,    "/api/v2/organizations/search", action: "search"
    request.should_route :get,    "/api/v2/organizations/1/related", action: "related", id: "1"
    request.should_route :get,    "/api/v2/organizations/show_many", action: "show_many"
    request.should_route :put,    "/api/v2/organizations/update_many", action: "update_many"
    request.should_route :get,    "/api/v2/users/1/organizations/count", action: "count", user_id: "1"
  end

  describe "when using oauth tokens" do
    with_scopes('organizations:read', 'read') do
      should_be_authorized { get :index, params: { user_id: account.owner.id } }
      should_be_authorized { get :index }
      should_be_authorized { get :count }
      should_be_authorized { get :autocomplete }
      should_be_authorized { get :related, params: { id: account.organizations.first.id } }
      should_be_authorized { get :show, params: { id: account.organizations.first.id } }
      should_be_authorized { get :show_many, params: { ids: account.organizations.first.id } }

      should_not_be_authorized { post :create, params: { organization: { name: organization_name } } }
    end

    with_scopes('organizations:write', 'write') do
      should_be_authorized { post :create, params: { organization: { name: organization_name } } }
      should_be_authorized { post :create_many, params: { organizations: [{"name" => "My Org 1"}] } }
      should_be_authorized { put :update, params: { id: organization.id.to_s, organization: { notes: "Updated"} } }
      should_be_authorized { put :update_many, params: { ids: organization.id.to_s, organization: { notes: "Updated"} } }
      should_be_authorized { delete :destroy, params: { id: organization.id.to_s } }
      should_be_authorized { delete :destroy_many, params: { ids: organization.id.to_s } }
      should_be_authorized { get :search, params: { external_id: "123" } }

      should_not_be_authorized { get :index }
    end

    with_scopes('read', 'write') do
      should_be_authorized { get :search, params: { external_id: "123" } }
      should_be_authorized { get :autocomplete }
      should_be_authorized { get :related, params: { id: account.organizations.first.id } }
      should_be_authorized { get :show_many, params: { ids: account.organizations.first.id } }
      should_be_authorized { post :create_many, params: { organizations: [{"name" => "My Org 1"}] } }
      should_be_authorized { put :update_many, params: { ids: organization.id.to_s, organization: { notes: "Updated"} } }
      should_be_authorized { delete :destroy_many, params: { ids: organization.id.to_s } }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :show, :create, :update, :destroy, :autocomplete, :search, :show_many, :update_many, :count
  end

  as_an_end_user do
    should_be_forbidden :index, :show, :create, :update, :destroy, :autocomplete, :search, :show_many, :update_many, :count
  end

  describe "as a restricted agent" do
    when_logged_in_as :minimum_agent do
      describe "with organization restriction" do
        before do
          @user.update_attributes(restriction_id: RoleRestrictionType.ORGANIZATION)
          put :update, params: { id: organization.id.to_s, organization: { notes: "Updated note" } }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end
    end
  end

  as_an_agent do
    describe "a GET TO  :count " do
      it 'count should come back' do
        get :count
        assert_response :ok
        organizations_json_keys = JSON.parse(response.body).keys
        assert_equal 1, (organizations_json_keys & ['count']).size
      end
    end

    describe_with_arturo_disabled :etag_caching_organizations_index do
      it "has_etag_caching_organizations_index disabled" do
        get :index
        assert_response :ok
        organizations_json_keys = JSON.parse(response.body).keys
        assert_equal 1, (organizations_json_keys & ['count']).size
      end
    end

    describe "a GET to :autocomplete" do
      describe "with a name parameter of more than 1 character" do
        before { get :autocomplete, params: { name: "org" } }

        it('responds with ok') { assert_response :ok }

        it "returns an array of matching organizations" do
          json = JSON.parse(@response.body)["organizations"]
          assert !json.empty?
          json.each do |organization|
            assert_match /^org/, organization["name"]
          end
        end
      end

      describe "with a name parameter of 1 character" do
        before { get :autocomplete, params: { name: "o" } }

        it('responds with ok') { assert_response :ok }

        it "returns an array of matching organizations" do
          json = JSON.parse(@response.body)["organizations"]
          assert !json.empty?, 'No organizations returned'
          json.each do |organization|
            assert_match /^o/, organization["name"]
          end
        end
      end
    end

    # For backwards compatibility, we will still allow POST requests to the
    # autocompletion API.
    describe "a POST to :autocomplete" do
      describe "with a name parameter of more than 1 character" do
        before { post :autocomplete, params: { name: "org" } }

        it('responds with ok') { assert_response :ok }

        it "returns an array of matching organizations" do
          json = JSON.parse(@response.body)["organizations"]
          assert !json.empty?
          json.each do |organization|
            assert_match /^org/, organization["name"]
          end
        end
      end

      describe "with a name parameter of 1 character" do
        before { post :autocomplete, params: { name: "o" } }

        it('responds with ok') { assert_response :ok }

        it "returns an array of matching organizations" do
          json = JSON.parse(@response.body)["organizations"]
          assert !json.empty?, 'No organizations returned'
          json.each do |organization|
            assert_match /^o/, organization["name"]
          end
        end
      end
    end

    describe "a GET to :index" do
      before { get :index }

      should_paginate :organizations
      should_use_presenter Api::V2::Organizations::AgentPresenter

      describe "with per_page=1000" do
        before { get :index, params: { per_page: 1000 } }
        it "allows 1000 results per page" do
          assert_equal 1000, @controller.send(:per_page)
        end
      end

      describe "Emergency rate limiting for GET organizations" do
        describe "with an integer page param" do
          let(:limiter) { mock }
          let(:statsd_client) { mock }

          before { limiter.stubs(:throttle_index_with_deep_offset_pagination) }

          it "calls the pagination limiter" do
            Zendesk::OffsetPaginationLimiter.expects(:new).with(anything).returns(limiter)
            limiter.expects(:throttle_index_with_deep_offset_pagination).once

            get :index, params: { page: 1 }
          end

          describe "after 99 calls within 1 second" do
            let(:rate_limit) { 100 }

            before do
              Timecop.freeze
              Prop.throttle!(:index_organizations_with_deep_offset_pagination_ceiling_limit, @account.id, increment: rate_limit - 1)
            end

            describe "for page=1 and per_page=100" do
              let(:page) { 1 }

              it "responds with success on calls 100 and 101" do
                get :index, params: { page: page }
                assert_response :success
                get :index, params: { page: page }
                assert_response :success
              end

              describe "metrics and logging" do
                it "does not log any rate limit message" do
                  Rails.logger.expects(:info).with(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/)).never
                  # Once the above expectation is set up, you must specify that other calls might occur.
                  Rails.logger.expects(:info).with(Not(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not increment the 'organizations' ceiling rate limit metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "organizations").at_least(0).returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_organizations_index_ceiling_rate_limit_log_only", anything).never
                  statsd_client.expects(:increment).with("api_v2_organizations_index_ceiling_rate_limit", anything).never
                  2.times { get :index, params: { page: page } }
                end
              end
            end

            describe "for page=10,001 and per_page=100" do
              let(:page) { 10_001 }

              it "responds with success on calls 100 and 101" do
                get :index, params: { page: page }
                assert_response :success
                get :index, params: { page: page }
                assert_response :success
              end

              describe "metrics and logging" do
                it "logs the log_only message" do
                  Rails.logger.expects(:info).with(regexp_matches(/Log only: would have rate limited #index by account/)).once
                  Rails.logger.expects(:info).with(Not(regexp_matches(/Log only: would have rate limited #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not log the message for the active arturo" do
                  Rails.logger.expects(:info).with(regexp_matches(/Rate limiting #index by account/)).never
                  Rails.logger.expects(:info).with(Not(regexp_matches(/Rate limiting #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not increment the api_v2_organizations_index_ceiling_rate_limit metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "organizations").once.returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_organizations_index_ceiling_rate_limit", anything).never
                  statsd_client.expects(:increment).with(Not(equals("api_v2_organizations_index_ceiling_rate_limit")), anything).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "increments the log_only metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "organizations").once.returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_organizations_index_ceiling_rate_limit_log_only", anything).once
                  2.times { get :index, params: { page: page } }
                end
              end
            end
          end
        end
      end

      describe "cache control headers" do
        describe "with default account setting organizations_index_cache_control_header_ttl_seconds" do
          before { get :index }

          it "are set and have the correct ttl" do
            assert_equal 'max-age=30, private', @response.headers['Cache-Control']
          end
        end

        describe "with non default account setting organizations_index_cache_control_header_ttl_seconds" do
          before do
            @controller.stubs(:current_account).returns(accounts(:minimum))
            account.settings.stubs(:organizations_index_cache_control_header_ttl_seconds).returns(42)
            get :index
          end

          it "are set and have the correct ttl" do
            assert_equal 'max-age=42, private', @response.headers['Cache-Control']
          end
        end
      end

      describe_with_arturo_enabled :etag_caching_organizations_index do
        it "uses etags" do
          assert_etagged :index do
            organization.update_attribute(:updated_at, 1.minute.from_now)
            @controller.instance_variable_set(:@organizations, nil)
          end
        end
      end

      describe "with cursor pagination" do
        should_support_cursor_pagination_conversion :index, 'organizations', :cursor_pagination_organizations_index, :remove_offset_pagination_organizations_index
      end
    end

    describe "#organizations" do
      before { @controller.stubs(:current_account).returns(accounts(:minimum)) }

      describe "with no user_id" do
        it "paginates all organizations on the current account" do
          @controller.params.clear
          organizations = stub(:organizations)
          @controller.send(:current_account).organizations.expects(:paginate).returns(organizations)

          assert_equal organizations, @controller.send(:organizations)
        end
      end

      describe "with a user_id" do
        it "paginates organizations for the user" do
          @controller.params[:user_id] = minimum_agent.id
          organizations = stub(:organizations)

          @controller.send(:current_account).users.expects(:find).with(minimum_agent.id).returns(minimum_agent)
          minimum_agent.organizations.expects(:paginate).returns(organizations)

          assert_equal organizations, @controller.send(:organizations)
        end
      end

      describe "a PUT to :update" do
        before do
          minimum_agent.stubs(:can?).with(:edit_notes, Organization).returns(true)
          minimum_agent.stubs(:can?).with(:edit, Organization).returns(true)
          put :update, params: { id: organization.id.to_s, organization: { :notes => "Updated note", "details" => "Updated details" } }
        end

        should_use_presenter Api::V2::Organizations::AgentPresenter

        it "applies changes" do
          response_organization = JSON.parse(@response.body)["organization"]
          assert_equal response_organization["notes"], "Updated note"
          assert_equal response_organization["details"], "Updated details"
        end
      end

      describe "only notes access" do
        before do
          minimum_agent.stubs(:can?).with(:edit_notes, Organization).returns(true)
          minimum_agent.stubs(:can?).with(:edit, Organization).returns(false)
        end

        describe "organization param is present" do
          before do
            put :update, params: { id: organization.id.to_s, organization: { :notes => "Updated note", "details" => "Updated details" } }
          end

          it "only applies notes" do
            response_organization = JSON.parse(@response.body)["organization"]
            assert_equal response_organization["notes"], "Updated note"
            assert_equal response_organization["details"], organization.details
          end
        end

        describe "when organization param is present and notes param is not" do
          before do
            put :update, params: { id: organization.id.to_s, organization: { "details" => "Updated details" } }
          end

          it "leaves notes alone" do
            response_organization = JSON.parse(@response.body)["organization"]
            assert_equal response_organization["notes"], "Example of notes for organization"
            assert_equal response_organization["details"], organization.details
          end
        end

        describe "when organization param is not present" do
          before do
            put :update, params: { id: organization.id.to_s, organization: nil }
          end

          should_use_presenter Api::V2::Organizations::AgentPresenter
        end
      end

      describe "a PUT to :update with invalid params" do
        describe "with invalid organization_fields param" do
          before do
            put :update, params: { id: organization.id, organization: {
              organization_fields: "not a hash"
            } }
          end

          it "responds with bad request" do
            assert_response :bad_request
            assert_includes response.body, "organization_fields must be a hash"
          end
        end
      end
    end

    describe "a GET to :related" do
      before do
        get :related, params: { id: organization.id.to_s }
      end

      it('responds with ok') { assert_response :ok }
      should_use_presenter Api::V2::OrganizationRelatedPresenter
    end

    describe "a GET to :show_many" do
      describe "with ids" do
        before do
          @ids = account.organizations.map(&:id).sort
          get :show_many, params: { ids: @ids.join(",") }
        end

        it "gives me back the organizations I asked for" do
          json = JSON.parse(@response.body)
          assert_equal @ids, json["organizations"].map { |o| o["id"] }.sort
        end
      end

      describe "with external_ids" do
        before do
          @external_ids = ['external_1', 'external_2', 'external_3']
          @external_ids.each do |external_id|
            FactoryBot.create(:organization, account: account, external_id: external_id, name: external_id)
          end
          get :show_many, params: { external_ids: @external_ids.join(",") }
        end

        it "gives me back the organizations I asked for" do
          json = JSON.parse(@response.body)
          assert_equal @external_ids, json["organizations"].map { |o| o["external_id"] }.sort
        end
      end

      describe "neither ids nor external_ids are passed in" do
        before do
          get :show_many
        end

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe 'with valid external domain' do
        before do
          @ids = account.organizations.map(&:id).sort
        end

        it 'returns a valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
          get :show_many, params: { ids: @ids.join(",") }
          assert_equal response.headers['Access-Control-Allow-Origin'], 'http://app.futuresimple.com'
        end

        it 'returns a not valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
          get :show_many, params: { ids: @ids.join(",") }
          assert_nil response.headers['Access-Control-Allow-Origin']
        end
      end
    end
  end

  as_an_admin do
    describe "a GET TO  :count " do
      it 'count should come back' do
        get :count
        assert_response :ok
        organizations_json_keys = JSON.parse(response.body).keys
        assert_equal 1, (organizations_json_keys & ['count']).size
      end

      it 'count should come back for userid' do
        get :count, params: { user_id: minimum_end_user.id }
        assert_response :ok
        organizations_json_keys = JSON.parse(response.body).keys
        assert_equal 1, (organizations_json_keys & ['count']).size
      end
    end

    describe "a DELETE to :delete_many" do
      def payload
        { "external_ids" => "#{organization.id}, #{organization2.id}" }
      end

      describe "with valid params" do
        it "and with user_and_organization_fields feature" do
          delete :destroy_many, params: payload
          assert_response :ok
        end
      end
    end

    describe "a DELETE to :delete_many with an error" do
      describe "with invalid params" do
        it "and with user_and_organization_fields feature" do
          delete :destroy_many
          assert_response :bad_request
        end
      end
    end

    describe "a GET to :search with external_id" do
      before do
        organization.update_attributes!(external_id: "123")

        get :search, params: { external_id: "123" }
      end

      it "returns matching organization" do
        assert_equal organization.id, JSON.parse(@response.body)["organizations"].first["id"]
      end
    end

    describe "a GET to :search without external_id" do
      before do
        get :search
      end

      it "returns a bad request status back" do
        assert_response :bad_request
        assert_equal JSON.parse(@response.body)["description"], "Parameter external_id is required"
      end
    end

    describe "a GET to :related" do
      before do
        get :related, params: { id: organization.id.to_s }
      end
      should_use_presenter Api::V2::OrganizationRelatedPresenter
    end

    describe "a GET to :show" do
      before do
        get :show, params: { id: organization.id.to_s }
      end

      should_use_presenter Api::V2::Organizations::AgentPresenter

      it "does not change" do
        assert_no_api_change "test/files/api_fixtures/v2-organizations-show.yml"
      end
    end

    describe "a POST to :create" do
      describe "with valid params and organizations feature is enabled" do
        before do
          Account.any_instance.stubs(:has_organizations?).returns(true)
          create_organization_custom_field!('field_text', 'title', 'Text')
        end

        describe "and with user_and_organization_fields feature" do
          before do
            post :create, params: { organization: {
              name: organization_name,
              tags: ['organized'],
              organization_fields: {
                field_text: 'orgtext'
              }
            } }
          end

          should_use_presenter Api::V2::Organizations::AgentPresenter, status: :created, location: /\/api\/v2\/organizations\/\d+\.json$/

          it "creates a organization with appropriate params on the account" do
            @organization = @account.organizations.find_by_name(organization_name)
            assert_not_nil @organization
            assert_equal ['organized'], @organization.tags
            assert_equal(
              { 'field_text' => 'orgtext' },
              @organization.custom_field_values.as_json(account: @account)
            )
          end

          describe "organization domain events" do
            it "emits an organization tags changed event" do
              events = decoded_organization_events(domain_event_publisher.events, :tags_changed)
              assert_equal 1, events.size
              assert_equal %w[organized], events.first.tags_changed.tags_added.tags
            end
          end
        end

        describe "and without user_and_organization_fields feature" do
          before do
            Account.any_instance.stubs(:has_user_and_organization_fields?).returns(false)
            Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
            post :create, params: { organization: {
              name: organization_name,
              tags: ['organized'],
              organization_fields: {
                field_text: 'orgtext'
              }
            } }
          end

          should_use_presenter Api::V2::Organizations::AgentPresenter, status: :created, location: /\/api\/v2\/organizations\/\d+\.json$/

          it "creates a organization with appropriate params on the account" do
            @organization = @account.organizations.find_by_name(organization_name)
            assert_equal [], @organization.tags
            assert_equal(
              { 'field_text' => nil },
              @organization.custom_field_values.as_json(account: @account)
            )
          end
        end

        describe "with a valid domain_names param" do
          before do
            post :create, params: { organization: {
              name: organization_name,
              domain_names: "southern-isles.gov elsa@arendelle.gov"
            } }
          end

          it "adds organization_domains and organization_emails associations to the new org" do
            @organization = @account.organizations.find_by_name(organization_name)

            assert_equal 1, @organization.send(:organization_domains).size
            assert_equal 1, @organization.send(:organization_emails).size
          end
        end
      end

      describe "with invalid params" do
        describe "without params" do
          before { post :create }
          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
          should_not_change("the number of Organizations") { Organization.count(:all) }
        end

        describe "with invalid organization_fields param" do
          before do
            post :create, params: { organization: {
              name: organization_name,
              tags: ['organized'],
              organization_fields: "not a hash"
            } }
          end

          it "responds with bad request" do
            assert_response :bad_request
            assert_includes response.body, "organization_fields must be a hash"
          end
        end
      end

      describe "when organizations feature is not enabled" do
        before do
          Account.any_instance.stubs(:has_organizations?).returns(false)
          post :create, params: { organization: { name: organization_name } }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end
    end

    describe "a POST to :create_many" do
      let(:job_status) { stub_everything('job_status', uuid: 1) }

      describe "with valid params" do
        Account.any_instance.stubs(:has_organizations?).returns(true)
        let(:payload) do
          {
            'organizations' => [
              {
                'name'                => 'Org A',
                'tags'                => ['bulked'],
                'organization_fields' => {
                  'field_text' => 'bulked'
                }
              },
              {
                'name'                => 'Org B',
                'tags'                => ['bulked'],
                'organization_fields' => {
                  'field_text' => 'bulked'
                }
              }
            ]
          }
        end

        describe "and with user_and_organization_fields feature" do
          before do
            @controller.stubs(:enqueue_job_with_status).with(
              OrganizationBulkCreateJob,
              has_entries(payload)
            ).returns(job_status)

            post :create_many, params: payload
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end

        describe "and without user_and_organization_fields feature" do
          before do
            Account.any_instance.stubs(:has_user_and_organization_fields?).returns(false)
            Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
            @controller.stubs(:enqueue_job_with_status).with(
              OrganizationBulkCreateJob,
              has_entries(payload)
            ).returns(job_status)

            post :create_many, params: payload
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end

        describe "when organizations feature is not enabled" do
          before do
            Account.any_instance.stubs(:has_organizations?).returns(false)
            post :create_many, params: payload
          end

          it('responds with forbidden') { assert_response :forbidden }
        end
      end

      describe "with invalid params" do
        describe "with empty params" do
          before { post :create_many }

          it('responds with bad_request') { assert_response :bad_request }
        end

        describe "with invalid organization_fields param" do
          let(:payload) do
            {
              'organizations' => [
                {
                  'name'                => 'Org A',
                  'organization_fields' => {
                    'field_text' => 'batched'
                  }
                },
                {
                  'name'                => 'Org B',
                  'organization_fields' => "not a hash"
                }
              ]
            }
          end

          before do
            post :create_many, params: payload
          end

          it "responds with bad request" do
            assert_response :bad_request
            assert_includes response.body, "organization_fields must be a hash"
          end
        end
      end
    end

    describe "a POST to :create_or_update" do
      describe "with invalid parameter" do
        before do
          post :create_or_update, params: { organizations: {
            name: "Organizations with an s",
            notes: "mispelled parameter"
          } }
        end

        it "respons with a bad_request status" do
          assert_response :bad_request
        end
      end

      describe "with invalid organization_fields param" do
        before do
          post :create_or_update, params: { organization: {
            name: "New Org",
            organization_fields: "not a hash"
          } }
        end

        it "responds with bad request" do
          assert_response :bad_request
          assert_includes response.body, "organization_fields must be a hash"
        end
      end

      describe "with details that don't yet exist" do
        before do
          post :create_or_update, params: { organization: {
            name: "New Org",
            notes: "This is a new org.",
            tags: %w[test]
          } }
        end

        it "creates the org" do
          assert_response :created

          id = JSON.parse(@response.body)["organization"]["id"]

          new_organization = Organization.find(id)

          assert_equal "New Org", new_organization.name
          assert_equal "This is a new org.", new_organization.notes
        end

        describe "organization domain events" do
          it "emits an organization tags changed event" do
            events = decoded_organization_events(domain_event_publisher.events, :tags_changed)
            assert_equal 1, events.size
            assert_equal %w[test], events.first.tags_changed.tags_added.tags
          end
        end

        describe "with a valid domain_names param" do
          before do
            post :create_or_update, params: { organization: {
              name: organization_name,
              domain_names: "southern-isles.gov elsa@arendelle.gov"
            } }
          end

          it "adds organization_domains and organization_emails associations to the new org" do
            @organization = @account.organizations.find_by_name(organization_name)

            assert_equal 1, @organization.send(:organization_domains).size
            assert_equal 1, @organization.send(:organization_emails).size
          end
        end
      end

      describe "with a name that already exists" do
        before do
          post :create_or_update, params: { organization: {
            name: organization.name,
            notes: "There can only be one."
          } }
        end

        it "fails" do
          assert_response :unprocessable_entity
        end
      end

      describe "with an ID that matches an existing organization" do
        before do
          post :create_or_update, params: { organization: {
            id: organization.id,
            notes: "This is going to be massive.",
            tags: %w[test]
          } }
        end

        it "updates correctly" do
          organization.reload

          assert_response :ok
          assert_equal "This is going to be massive.", organization.notes
        end

        describe "organization domain events" do
          it "emits an organization tags changed event" do
            events = decoded_organization_events(domain_event_publisher.events, :tags_changed)
            assert_equal 1, events.size
            assert_equal %w[test], events.first.tags_changed.tags_added.tags
          end
        end
      end

      describe "with an ID that *doesn't* match an existing organization" do
        before do
          post :create_or_update, params: { organization: {
            id: organization.id + 1,
            name: "Pie Co",
            notes: "We love pies."
          } }
        end

        it "creates a new organization and ignores the ID" do
          assert_response :created

          id = JSON.parse(@response.body)["organization"]["id"]
          refute_equal organization.id + 1, id
        end
      end
    end

    describe "a POST to :autocomplete" do
      describe "with a name parameter of more than 1 character" do
        before { post :autocomplete, params: { name: "org" } }

        it('responds with ok') { assert_response :ok }

        it "returns an array of matching organizations" do
          json = JSON.parse(@response.body)["organizations"]
          assert !json.empty?
          json.each do |organization|
            assert_match /^org/, organization["name"]
          end
        end
      end

      describe "with a name parameter of 1 character" do
        before { post :autocomplete, params: { name: "o" } }

        it('responds with ok') { assert_response :ok }

        it "returns an array of matching organizations" do
          json = JSON.parse(@response.body)["organizations"]
          assert !json.empty?, 'No organizations returned'
          json.each do |organization|
            assert_match /^o/, organization["name"]
          end
        end
      end
    end

    describe "a PUT to :update" do
      describe "with valid params" do
        before { put :update, params: { id: organization.id.to_s, organization: { name: "Updated Name", tags: %w[test] } } }

        should_use_presenter Api::V2::Organizations::AgentPresenter

        it "updates the organization" do
          assert_equal "Updated Name", organization.reload.name
        end

        describe "organization domain events" do
          it "emits an organization tags changed event" do
            events = decoded_organization_events(domain_event_publisher.events, :tags_changed)
            assert_equal 1, events.size
            assert_equal %w[test], events.first.tags_changed.tags_added.tags
          end
        end
      end

      describe "with nil group_id param" do
        before { put :update, params: { id: organization.id.to_s, organization: { name: "Updated Name", group_id: nil } } }

        should_use_presenter Api::V2::Organizations::AgentPresenter

        it "updates the organization" do
          assert_equal "Updated Name", organization.reload.name
        end
      end

      describe "with invalid params" do
        before { put :update, params: { id: organization.id.to_s, organization: { name: "" } } }

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

        it "does not update the organization" do
          assert_not_equal "", organization.reload.name
        end
      end

      describe "with empty params" do
        # same as doing `organization: {}` via an integration test
        before { put :update, params: { id: organization.id.to_s, organization: nil } }

        should_use_presenter Api::V2::Organizations::AgentPresenter
      end
    end

    describe "a PUT to :update_many" do
      let(:job_status) { stub_everything('job_status', uuid: 1) }

      before do
        create_organization_custom_field!('field_text', 'title', 'Text')
        @organizations = [
          FactoryBot.create(:organization, account: account, name: 'Org 1'),
          FactoryBot.create(:organization, account: account, name: 'Org 2')
        ]
        @organization_ids = @account.organizations.map(&:id).join(",")
      end

      describe "with valid params" do
        let(:payload) do
          {
            'ids' => @organization_ids,
            'organization' => {
              'tags' => ['bulked'],
              'organization_fields' => {
                'field_text' => 'bulked'
              }
            }
          }
        end

        describe "and with user_and_organization_fields feature" do
          before do
            @controller.stubs(:enqueue_job_with_status).with(
              OrganizationBulkUpdateJob,
              has_entries(
                'ids' => @organization_ids.split(',').map(&:to_i),
                'organization' => {
                  'tags' => ['bulked']
                },
                'custom_fields' => {
                  'field_text' => 'bulked'
                }
              )
            ).returns(job_status)

            put :update_many, params: payload
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end

        describe "and without user_and_organization_fields feature" do
          before do
            Account.any_instance.stubs(:has_user_and_organization_fields?).returns(false)
            Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
            @controller.stubs(:enqueue_job_with_status).with(
              OrganizationBulkUpdateJob,
              has_entries(
                'ids' => @organization_ids.split(',').map(&:to_i),
                'organization' => {
                  'tags' => ['bulked']
                },
                'custom_fields' => {
                  'field_text' => 'bulked'
                }
              )
            ).returns(job_status)
            put :update_many, params: payload
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end
      end

      describe "with valid batch params" do
        let(:payload) do
          {
            'organizations' => [
              {
                'id'                  => @organizations[0].id,
                'tags'                => ['batched'],
                'organization_fields' => {
                  'field_text' => 'batched'
                }
              },
              {
                'id'                  => @organizations[1].id,
                'tags'                => ['batched'],
                'organization_fields' => {
                  'field_text' => 'batched'
                }
              }
            ]
          }
        end

        describe "and with user_and_organization_fields feature" do
          before do
            @controller.stubs(:enqueue_job_with_status).with(
              OrganizationBatchUpdateJob,
              has_entries(payload)
            ).returns(job_status)

            put :update_many, params: payload
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end

        describe "and without user_and_organization_fields feature" do
          before do
            Account.any_instance.stubs(:has_user_and_organization_fields?).returns(false)
            Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
            @controller.stubs(:enqueue_job_with_status).with(
              OrganizationBatchUpdateJob,
              has_entries(payload)
            ).returns(job_status)

            put :update_many, params: payload
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end
      end

      describe "with many ids params" do
        it "updates many" do
          external_ids = account.organizations.map(&:external_id).select { |e| e }
          r = put :update_many, params: { organization: { "notes" => "interesting" }, external_ids: external_ids.join(",") }
          assert_response :ok
          success = JSON.parse(r.body)
          assert_equal(3, success["job_status"]["total"])
        end
      end

      describe "with invalid params" do
        describe "with empty params" do
          before do
            put :update_many
          end

          it('responds with bad_request') { assert_response :bad_request }
        end

        describe "with bad single org params" do
          before do
            put :update_many, params: { organization: { "notes" => "interesting" } }
          end

          it('responds with bad_request') { assert_response :bad_request }
        end

        describe "with invalid organization_fields param" do
          describe "with a single param" do
            before do
              post :update_many, params: { organization: {
                name: "New Org",
                organization_fields: "not a hash"
              } }
            end

            it "responds with bad request" do
              assert_response :bad_request
              assert_includes response.body, "organization_fields must be a hash"
            end
          end

          describe "with batch params" do
            let(:payload) do
              {
                'organizations' => [
                  {
                    'id'                  => @organizations[0].id,
                    'organization_fields' => {
                      'field_text' => 'batched'
                    }
                  },
                  {
                    'id'                  => @organizations[1].id,
                    'organization_fields' => "not a hash"
                  }
                ]
              }
            end

            before do
              post :update_many, params: payload
            end

            it "responds with bad request" do
              assert_response :bad_request
              assert_includes response.body, "organization_fields must be a hash"
            end
          end
        end
      end

      describe "allows 100 ids" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          OrganizationBulkUpdateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          put :update_many, params: { ids: [*1..100].join(","), organization: { "details" => "something interesting" } }
        end
        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "allows 100 individual attributes" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          OrganizationBatchUpdateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          put_update_many(100)
        end
        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "disallows > 100 ids " do
        before do
          put :update_many, params: { ids: [*1..101].join(","), organization: { "details" => "something interesting" } }
        end
        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "disallows > 100 individual attributes" do
        before do
          put_update_many(101)
        end
        it('responds with bad_request') { assert_response :bad_request }
      end
    end

    describe "a DELETE to :destroy" do
      before do
        delete :destroy, params: { id: organization.id.to_s }
      end

      it('responds with no_content') { assert_response :no_content }
      should_change("the organization count", by: -1) { Organization.count(:all) }
    end

    describe "with custom fields" do
      let(:domain_event_publisher) { FakeEscKafkaMessage.new }

      before do
        Organization.any_instance.stubs(domain_event_publisher: domain_event_publisher)
        cf_cbox = create_organization_custom_field!("field_checkbox", "title", "Checkbox")
        cf_cbox.tag = "toast"
        cf_cbox.save!
        create_organization_custom_field!("field_checkbox2", "title", "Checkbox") # not tag
        create_organization_custom_field!("field_text", "title", "Text")
      end

      describe "a POST to :create" do
        before do
          post :create, params: { organization: {
            name: organization_name, tags: "moose",
            organization_fields: {field_checkbox: true, field_checkbox2: true, field_text: "bar"}
          } }
        end

        it('responds with created') { assert_response :created }
        it "sets the custom fields on the organization" do
          org = Organization.last
          assert_equal({"field_checkbox" => true, "field_checkbox2" => true, "field_text" => "bar"}, org.custom_field_values.as_json(account: @account))
          assert_equal(["moose", "toast"], org.tags)
        end

        it 'emits correct domain events' do
          custom_field_events = decoded_organization_events(domain_event_publisher.events, :custom_field_changed)
          assert_equal 3, custom_field_events.size
          assert custom_field_events.first.custom_field_changed.current.checkbox_value
          assert custom_field_events.second.custom_field_changed.current.checkbox_value
          assert_equal "bar", custom_field_events.third.custom_field_changed.current.text_value
        end
      end

      describe "a PUT to :update" do
        describe "with user_and_organization_fields feature" do
          before do
            put :update, params: { id: organization.id.to_s, organization: {
                name: "Updated Name", tags: "moose",
                organization_fields: {field_text: "foobar"}
              } }
          end

          it('responds with success') { assert_response :success }
          it "updates the custom fields" do
            organization.reload
            assert_equal({"field_checkbox" => false, "field_checkbox2" => false, "field_text" => "foobar"}, organization.custom_field_values.as_json(account: @account))
            assert_equal(["moose"], organization.tags)
          end

          it 'emits correct domain events' do
            custom_field_events = decoded_organization_events(domain_event_publisher.events, :custom_field_changed)
            assert_equal 1, custom_field_events.size
            assert_equal "foobar", custom_field_events.first.custom_field_changed.current.text_value
          end
        end

        describe "without user_and_organization_fields feature" do
          before do
            Account.any_instance.stubs(:has_user_and_organization_fields?).returns(false)
            Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
            put :update, params: { id: organization.id.to_s, organization: {
              name: "Updated Name", tags: "moose",
              organization_fields: {field_text: "foobar"}
            } }
          end

          it('responds with success') { assert_response :success }
          it "does not update the custom fields" do
            organization.reload
            assert_equal({"field_checkbox" => false, "field_checkbox2" => false, "field_text" => nil}, organization.custom_field_values.as_json(account: @account))
            assert_equal([], organization.tags)
          end

          it 'does not emit custom field changed domain events' do
            custom_field_events = decoded_organization_events(domain_event_publisher.events, :custom_field_changed)
            assert_equal 0, custom_field_events.size
          end
        end
      end

      describe "adding custom dropdowns" do
        before do
          # 3 taggers
          @custom_field = create_organization_custom_field!("field_dropdown1", "title", "Dropdown")
          @custom_field.dropdown_choices.create(name: "test1", value: "first", position: 1)
          @custom_field.dropdown_choices.create(name: "test2", value: "second", position: 2)
          @custom_field = create_organization_custom_field!("field_dropdown2", "title", "Dropdown")
          @custom_field.dropdown_choices.create(name: "prueba1", value: "primero", position: 1)
          @custom_field.dropdown_choices.create(name: "prueba2", value: "segundo", position: 2)
          @custom_field = create_organization_custom_field!("field_dropdown3", "title", "Dropdown")
          @custom_field.dropdown_choices.create(name: "tesuto1", value: "hitotsu", position: 1)
          @custom_field.dropdown_choices.create(name: "tesuto2", value: "futatsu", position: 2)
        end

        describe "a POST to :create" do
          describe "with user_and_organization_fields feature" do
            before do
              post :create, params: { organization: {
                name: organization_name, tags: "moose",
                organization_fields: {
                  field_checkbox: true, field_checkbox2: true, field_text: "bar",
                  field_dropdown1: "first", field_dropdown2: "segundo"
                }
              } }
            end

            it('responds with created') { assert_response :created }
            it "sets custom field values on the organization" do
              org = Organization.last
              assert_equal({"field_checkbox" => true, "field_checkbox2" => true, "field_text" => "bar",
                            "field_dropdown1" => "first", "field_dropdown2" => "segundo", "field_dropdown3" => nil}, org.custom_field_values.as_json(account: @account))
              assert_equal(["moose", "toast", "first", "segundo"].sort, org.tags.sort)
            end

            it 'emits correct domain events' do
              custom_field_events = decoded_organization_events(domain_event_publisher.events, :custom_field_changed)
              assert_equal 5, custom_field_events.size
              assert custom_field_events.first.custom_field_changed.current.checkbox_value
              assert custom_field_events.second.custom_field_changed.current.checkbox_value
              assert_equal "bar", custom_field_events.third.custom_field_changed.current.text_value
              assert_equal "first", custom_field_events.fourth.custom_field_changed.current.tagger_value
              assert_equal "segundo", custom_field_events.fifth.custom_field_changed.current.tagger_value
            end
          end

          describe "without user_and_organization_fields feature" do
            before do
              Account.any_instance.stubs(:has_user_and_organization_fields?).returns(false)
              Account.any_instance.stubs(:has_user_and_organization_tags?).returns(false)
              post :create, params: { organization: {
                name: organization_name, tags: "moose",
                organization_fields: {
                  field_checkbox: true, field_checkbox2: true, field_text: "bar",
                  field_dropdown1: "first", field_dropdown2: "segundo"
                }
              } }
            end

            it('responds with created') { assert_response :created }
            it "does not set custom field values on the organization" do
              org = Organization.last
              assert_equal({
                "field_checkbox" => false, "field_checkbox2" => false, "field_text" => nil,
                "field_dropdown1" => nil, "field_dropdown2" => nil, "field_dropdown3" => nil
              }, org.custom_field_values.as_json(account: @account))
              assert_equal([], org.tags)
            end
          end
        end

        describe "a PUT to :update" do
          before do
            organization.current_tags = "moose"
            organization.custom_field_values.update_from_hash(field_text: "foobar", field_checkbox2: true, field_dropdown1: "first", field_dropdown2: "primero", field_dropdown3: "hitotsu")
            organization.save!

            domain_event_publisher.clear

            put :update, params: { id: organization.id.to_s, organization: {organization_fields: {field_checkbox: false, field_checkbox2: false, field_dropdown2: nil, field_dropdown3: "futatsu"} } } # update again
          end

          it('responds with success') { assert_response :success }
          it "updates the organization custom fields correctly" do
            organization.reload
            assert_equal({
              "field_checkbox" => false, "field_checkbox2" => false, "field_text" => "foobar",
              "field_dropdown1" => "first", "field_dropdown2" => nil, "field_dropdown3" => "futatsu"
            }, organization.custom_field_values.as_json(account: @account))
            assert_equal(["moose", "first", "futatsu"].sort, organization.tags.sort)
          end

          it 'emits correct domain events' do
            custom_field_events = decoded_organization_events(domain_event_publisher.events, :custom_field_changed)
            assert_equal 3, custom_field_events.size
            assert custom_field_events.first.custom_field_changed.previous.checkbox_value
            assert_nil custom_field_events.first.custom_field_changed.current
            assert_equal 'primero', custom_field_events.second.custom_field_changed.previous.tagger_value
            assert_nil custom_field_events.second.custom_field_changed.current
            assert_equal 'hitotsu', custom_field_events.third.custom_field_changed.previous.tagger_value
            assert_equal 'futatsu', custom_field_events.third.custom_field_changed.current.tagger_value
          end
        end

        describe "a PUT to :update with a bad value" do
          before do
            organization.current_tags = "moose"
            organization.custom_field_values.update_from_hash(field_text: "foobar", field_dropdown1: "first")
            organization.save!

            put :update, params: { id: organization.id, organization: {organization_fields: {field_dropdown1: "bad_value"} } } # update again
          end

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity } # 422 Unprocessable Entity (WebDAV; RFC 4918)

          it "updates the organization custom fields correctly" do
            organization.reload
            assert_equal({
              "field_checkbox" => false, "field_checkbox2" => false, "field_text" => "foobar",
              "field_dropdown1" => "first", "field_dropdown2" => nil, "field_dropdown3" => nil
            }, organization.custom_field_values.as_json(account: @account))
            assert_equal(["moose", "first"].sort, organization.tags.sort)
          end
        end

        describe "a GET to :show_many" do
          it "with external ids" do
            external_ids = account.organizations.map(&:external_id).select { |e| e }
            r = get :show_many, params: { external_ids: external_ids.join(",") }
            v = JSON.parse(r.body)
            assert_equal(v["organizations"].count, 3)
          end

          describe "with ids" do
            before do
              @ids = account.organizations.map(&:id).sort
              get :show_many, params: { ids: @ids.join(",") }
            end

            it "gives me back the organizations I asked for" do
              json = JSON.parse(@response.body)
              assert_equal @ids, json["organizations"].map { |o| o["id"] }.sort
            end
          end

          describe "with external_ids" do
            before do
              @external_ids = ['external_1', 'external_2', 'external_3']
              @external_ids.each do |external_id|
                FactoryBot.create(:organization, account: account, external_id: external_id, name: external_id)
              end
              get :show_many, params: { external_ids: @external_ids.join(",") }
            end

            it "gives me back the organizations I asked for" do
              json = JSON.parse(@response.body)
              assert_equal @external_ids, json["organizations"].map { |o| o["external_id"] }.sort
            end
          end

          describe "neither ids nor external_ids are passed in" do
            before do
              get :show_many
            end

            it('responds with bad_request') { assert_response :bad_request }
          end
        end
      end
    end
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    should_be_forbidden(
      [:get, :index, { user_id: 1 }],
      :index,
      :autocomplete,
      [:get, :related, { id: 1 }],
      [:get, :show_many, { ids: 1 }],
      [:post, :create, { organization: { name: 'x' } }]
    )

    with_scopes('organizations:read', 'read') do
      should_be_authorized { get :show, params: { id: account.organizations.first.id } }
    end

    describe "a GET to :show" do
      before { get :show, params: { id: organization.id.to_s } }
      it('responds with success') { assert_response :success }
    end
  end

  as_a_subsystem_user(user: 'zdp_metadata', account: :minimum) do
    should_be_forbidden(
      :autocomplete,
      [:get, :related, { id: 1 }],
      [:get, :show_many, { ids: 1 }],
      [:post, :create, { organization: { name: 'x' } }]
    )

    should_be_authorized { get :index, params: { user_id: minimum_end_user.id } }
  end

  private

  def put_update_many(count)
    put :update_many, params: { organizations: Array.new(count) { |i| { "id" => i, "details" => "Something interesting" } } }
  end
end
