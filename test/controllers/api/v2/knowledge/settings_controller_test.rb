require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Api::V2::Knowledge::SettingsController do
  extend Api::V2::TestHelper

  fixtures :all

  describe "A GET to :show" do
    it "responds with the settings for the Knowledge app" do
      get :show
    end
  end
end
