require_relative "../../../support/test_helper"
require_relative '../../../support/multiproduct_test_helper'

SingleCov.covered!

describe Api::V2::ProductsController do
  extend MultiproductTestHelper
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :groups, :role_settings

  let(:span) { stub(:span) }
  let(:parent_span) { stub(:parent_span) }

  before do
    accept :json
    Account.any_instance.stubs(:has_groups?).returns(true)
    Datadog.tracer.stubs(:active_span).returns(parent_span)
    parent_span.stubs(:parent).returns(span)
  end

  with_options(controller: "api/v2/products") do |request|
    request.should_route :get, "/api/v2/products", action: "index"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  describe "#index" do
    describe "with generic APM span" do
      before do
        span.stubs(:set_tag)
      end

      as_an_agent do
        before do
          Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).returns(support: 'agent')
          get :index
        end

        it "returns the expected products response" do
          expected = [
            { "key" => "lotus" },
            { "key" => "guide" },
            { "key" => "gather" }
          ]

          assert_equal expected, JSON.parse(@response.body)['products']
        end

        it "does not return the central_admin key" do
          assert_not_includes(JSON.parse(@response.body)["products"], "key" => "central_admin")
        end
      end

      as_an_admin do
        before do
          Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).returns(support: 'admin')
        end

        it 'returns the central_admin key' do
          get :index
          assert_includes(JSON.parse(@response.body)["products"], "key" => "central_admin")
        end

        describe "with the product tray list view enabled" do
          before do
            Arturo.enable_feature!(:product_tray_enable_list_view)
            get :index
          end

          it 'enables the product_tray_enable_list_view feature' do
            assert_equal(JSON.parse(@response.body)["features"]["product_tray_enable_list_view"], true)
          end
        end

        describe "with the product tray list view disabled" do
          before do
            Arturo.disable_feature!(:product_tray_enable_list_view)
            get :index
          end

          it 'disables the product_tray_enable_list_view feature' do
            assert_equal(JSON.parse(@response.body)["features"]["product_tray_enable_list_view"], false)
          end
        end
      end

      as_an_owner do
        before do
          Zendesk::StaffClient.any_instance.stubs(:owner?).returns(true)
          get :index
        end

        it 'returns the central_admin key' do
          assert_includes(JSON.parse(@response.body)["products"], "key" => "central_admin")
        end
      end

      when_logged_in_as(:multiproduct_support_agent) do
        before do
          Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).returns(support: 'agent')
        end

        it_allows_access_for_all_multiproduct_product_types
      end
    end

    describe "without a mocked APM set_tag function" do
      let(:example_tray_version) { '20.1.5' }
      let(:example_host_value) { 'lotus' }

      as_an_agent do
        before do
          Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).returns(support: 'agent')
        end

        it "adds URL valid params for APM tags" do
          span.expects(:set_tag).with('zendesk.products.client.trayversion', example_tray_version)
          span.expects(:set_tag).with('zendesk.products.client.host', example_host_value)

          get :index, params: { trayversion: example_tray_version, host: example_host_value }
        end

        it "adds blank tags with blank URL params" do
          span.expects(:set_tag).with('zendesk.products.client.trayversion', '')
          span.expects(:set_tag).with('zendesk.products.client.host', '')

          get :index, params: { trayversion: '', host: '' }
        end

        it "does not fail when URL params are missing" do
          span.expects(:set_tag).with('zendesk.products.client.trayversion', nil)
          span.expects(:set_tag).with('zendesk.products.client.host', nil)

          get :index
        end
      end
    end
  end
end
