require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 1

describe Api::V2::JetpackTasksController do
  extend Api::Presentation::TestHelper

  before do
    accept :json
  end

  with_options(controller: 'api/v2/jetpack_tasks') do |request|
    request.should_route :get, '/api/v2/jetpack_tasks', action: 'index'
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_agent do
    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::JetpackTaskPresenter, with: :tasks
    end

    describe "a PUT to :update" do
      before { put :update, params: { id: 'add_agents', status: 'started' } }

      before_should "#task_list should be updated" do
        return_values = {
          key: 'add_agents', order: 2, section: 'admin',
          status: 'started', enabled: true
        }
        JetpackTaskList.any_instance.expects(:update).with('add_agents', 'started').
          returns(JetpackTask.new(return_values))
      end

      should_use_presenter Api::V2::JetpackTaskPresenter
    end
  end
end
