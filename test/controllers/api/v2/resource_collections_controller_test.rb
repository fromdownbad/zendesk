require_relative "../../../support/test_helper"
require "zendesk/testing/factories/global_uid"
require 'zendesk/o_auth/testing/token_factory'
require 'zendesk/resource_collection/payload_data'
require 'zendesk/resource_collection/collection_create'

SingleCov.covered! uncovered: 6

describe Api::V2::ResourceCollectionsController do
  extend Api::V2::TestHelper

  fixtures :accounts, :users, :role_settings

  before do
    accept :json
  end

  def read_json(name)
    JSON.parse(File.read("test/files/resource_collection/#{name}.json"))
  end

  let(:one_of_each)          { read_json('one_of_each')          }
  let(:one_of_each_update_1) { read_json('one_of_each_update_1') }
  let(:one_of_each_update_2) { read_json('one_of_each_update_2') }
  let(:one_of_each_update_3) { read_json('one_of_each_update_3') }
  let(:trigger_with_fields)  { read_json('trigger_with_fields')  }
  let(:trigger_with_target)  { read_json('trigger_with_target')  }
  let(:payload)              { read_json('payload')              }
  let(:payload_errors)       { read_json('payload_errors')       }
  let(:payload_errors_2)     { read_json('payload_errors_2')     }

  let(:collection) { ResourceCollection.create!(account: @account) }

  as_an_anonymous_user do
    should_be_unauthorized :index, :show, :create, :update, :destroy
  end

  as_an_end_user do
    should_be_forbidden :index, :show, :create, :update, :destroy
  end

  as_an_agent do
    should_be_forbidden :index, :show, :create, :update, :destroy
  end

  as_an_admin do
    before do
      Resque.inline = true
    end

    after do
      Resque.inline = false
    end

    describe 'a GET to #index' do
      before do
        store_resources

        create_resources(one_of_each)

        get :index

        @json = JSON.parse(response.body)
        refute @json['error'], @json['description']
      end

      it 'returns the resources' do
        assert_equal 1, @json["resource_collections"].count
        assert_equal one_of_each.count, @json["resource_collections"][0]['resources'].count
        # expect every resource from the payload to have been created
        resource_keys = one_of_each.map { |key, _v| key }.sort
        assert_equal resource_keys, @json["resource_collections"][0]['resources'].map { |r| r['type'] }.sort
      end
    end

    describe 'a GET to #index does not error when a resource got somehow deleted' do
      before do
        create_resources(payload)
        store_resources

        # find the IDs for later use
        resources = ResourceCollection.find(@collection_id).collection_resources
        @automation_id = resources.find { |r| r['identifier'] == 'my_automation' }['resource_id']
        @macro_id = resources.find { |r| r['identifier'] == 'my_macro' }['resource_id']

        # we delete the automation
        automation = Automation.find(@automation_id)
        automation.deleted_at = '2008-01-01';
        automation.save(validate: false)

        get :index

        @json = JSON.parse(response.body)
        refute @json['error'], @json['description']
      end

      it 'returns the resources' do
        assert_equal 1, @json["resource_collections"].count
        assert_equal payload.count, @json["resource_collections"][0]['resources'].count
        # expect every resource from the payload to have been created. Also the type gets lost so it will show up as "Rule"
        resource_keys = payload.map { |key, _v| ["automations", "triggers"].include?(key) ? "Rule" : key }.sort
        assert_equal resource_keys, @json["resource_collections"][0]['resources'].map { |r| r['type'] }.sort
      end
    end

    describe 'a POST to #create' do
      before do
        store_resources

        post :create, params: { payload: one_of_each }, as: :json

        @json = JSON.parse(response.body)
        refute @json['error'], @json['description']
      end

      it 'creates resources' do
        check_resources automations: 1, macros: 1, organization_fields: 1, targets: 1, ticket_fields: 1, triggers: 1, user_fields: 1, views: 1
      end

      it 'created a automation' do
        automation = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_automation' }
        assert_instance_of Hash, automation
        assert_kind_of Integer, automation['resource_id']
        assert_equal 'automations', automation['type']

        automation = Automation.find(automation['resource_id'])
        assert_equal 'My Automation', automation.title
      end

      it 'created a macro' do
        macro = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_macro' }
        assert_instance_of Hash, macro
        assert_kind_of Integer, macro['resource_id']
        assert_equal 'macros', macro['type']

        macro = Macro.find(macro['resource_id'])
        assert_equal 'My Macro', macro.title
      end

      it 'created a organization field' do
        organization_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_organization_field' }
        assert_instance_of Hash, organization_field
        assert_kind_of Integer, organization_field['resource_id']
        assert_equal 'organization_fields', organization_field['type']

        organization_field = CustomField::Field.find(organization_field['resource_id'])
        assert_equal 'My Organization Field', organization_field.title
      end

      it 'created a target' do
        target = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_target' }
        assert_instance_of Hash, target
        assert_kind_of Integer, target['resource_id']
        assert_equal 'targets', target['type']

        target = Target.find(target['resource_id'])
        assert_equal 'My Target', target.title
      end

      it 'created a ticket' do
        ticket_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_ticket_field' }
        assert_instance_of Hash, ticket_field
        assert_kind_of Integer, ticket_field['resource_id']
        assert_equal 'ticket_fields', ticket_field['type']

        ticket_field = TicketField.find(ticket_field['resource_id'])
        assert_equal 'My Ticket Field', ticket_field.title
      end

      it 'created a trigger' do
        trigger = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_trigger' }
        assert_instance_of Hash, trigger
        assert_kind_of Integer, trigger['resource_id']
        assert_equal 'triggers', trigger['type']

        trigger = Trigger.find(trigger['resource_id'])
        assert_equal 'My trigger', trigger.title
      end

      it 'created a user field' do
        user_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_user_field' }
        assert_instance_of Hash, user_field
        assert_kind_of Integer, user_field['resource_id']
        assert_equal 'user_fields', user_field['type']

        user_field = CustomField::Field.find(user_field['resource_id'])
        assert_equal 'My User Field', user_field.title
      end

      it 'created a view' do
        view = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_view' }
        assert_instance_of Hash, view
        assert_kind_of Integer, view['resource_id']
        assert_equal 'views', view['type']

        view = View.find(view['resource_id'])
        assert_equal 'My View', view.title
      end
    end

    describe 'a PUT to #update with only changed resources' do
      before do
        create_resources(one_of_each)
        store_resources

        put :update, params: { payload: one_of_each_update_1, id: @collection_id }, as: :json
        assert_response :ok

        @json = JSON.parse(response.body)
        refute @json['error'], @json['description']
      end

      it 'keeps resources' do
        check_resources
      end

      it 'updated a automation' do
        automation = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_automation' }
        assert_instance_of Hash, automation

        automation = Automation.find(automation['resource_id'])
        assert_equal 'My Automation Updated', automation.title
      end

      it 'updated a macro' do
        macro = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_macro' }
        assert_instance_of Hash, macro

        macro = Macro.find(macro['resource_id'])
        assert_equal 'My Macro Updated', macro.title
      end

      it 'updated a organization field' do
        organization_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_organization_field' }
        assert_instance_of Hash, organization_field

        organization_field = CustomField::Field.find(organization_field['resource_id'])
        assert_equal 'My Organization Field Updated', organization_field.title
      end

      it 'updated a target' do
        target = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_target' }
        assert_instance_of Hash, target

        target = Target.find(target['resource_id'])
        assert_equal 'My Target Updated', target.title
      end

      it 'updated a ticket field' do
        ticket_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_ticket_field' }
        assert_instance_of Hash, ticket_field

        ticket_field = TicketField.find(ticket_field['resource_id'])
        assert_equal 'My Ticket Field Updated', ticket_field.title
      end

      it 'updated a trigger' do
        trigger = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_trigger' }
        assert_instance_of Hash, trigger

        trigger = Trigger.find(trigger['resource_id'])
        assert_equal 'My trigger Updated', trigger.title
      end

      it 'updated a user field' do
        user_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_user_field' }
        assert_instance_of Hash, user_field

        user_field = CustomField::Field.find(user_field['resource_id'])
        assert_equal 'My User Field Updated', user_field.title
      end

      it 'updated a view' do
        view = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_view' }
        assert_instance_of Hash, view

        view = View.find(view['resource_id'])
        assert_equal 'My View Updated', view.title
      end
    end

    describe 'a PUT to #update with only newly added resources' do
      before do
        create_resources(one_of_each)
        store_resources

        put :update, params: { payload: one_of_each_update_2, id: @collection_id }, as: :json

        @json = JSON.parse(response.body)
        refute @json['error'], @json['description']
      end

      it 'creates new resources' do
        check_resources automations: 1, macros: 1, organization_fields: 1, targets: 1, ticket_fields: 1, triggers: 1, user_fields: 1, views: 1
      end

      it 'created a new automation' do
        automation = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_added_automation' }
        assert_instance_of Hash, automation

        automation = Automation.find(automation['resource_id'])
        assert_equal 'My Added Automation', automation.title
      end

      it 'created a new macro' do
        macro = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_added_macro' }
        assert_instance_of Hash, macro

        macro = Macro.find(macro['resource_id'])
        assert_equal 'My Added Macro', macro.title
      end

      it 'created a new organization field' do
        organization_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_added_organization_field' }
        assert_instance_of Hash, organization_field

        organization_field = CustomField::Field.find(organization_field['resource_id'])
        assert_equal 'My Added Organization Field', organization_field.title
      end

      it 'created a new target' do
        target = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_added_target' }
        assert_instance_of Hash, target

        target = Target.find(target['resource_id'])
        assert_equal 'My Added Target', target.title
      end

      it 'created a new ticket field' do
        ticket_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_added_ticket_field' }
        assert_instance_of Hash, ticket_field

        ticket_field = TicketField.find(ticket_field['resource_id'])
        assert_equal 'My Added Ticket Field', ticket_field.title
      end

      it 'created a new trigger' do
        trigger = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_added_trigger' }
        assert_instance_of Hash, trigger

        trigger = Trigger.find(trigger['resource_id'])
        assert_equal 'My Added trigger', trigger.title
      end

      it 'created a new user field' do
        user_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_added_user_field' }
        assert_instance_of Hash, user_field

        user_field = CustomField::Field.find(user_field['resource_id'])
        assert_equal 'My Added User Field', user_field.title
      end

      it 'created a new view' do
        view = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_added_view' }
        assert_instance_of Hash, view

        view = View.find(view['resource_id'])
        assert_equal 'My Added View', view.title
      end
    end

    describe 'a PUT to #update with only deleted resources' do
      before do
        create_resources(one_of_each)
        store_resources

        put :update, params: { payload: one_of_each_update_3, id: @collection_id }, as: :json

        @json = JSON.parse(response.body)
        refute @json['error'], @json['description']
      end

      it 'deleted resources' do
        check_resources automations: -1, macros: -1, organization_fields: -1, targets: -1, ticket_fields: -1, triggers: -1, user_fields: -1, views: -1
      end

      it 'deleted a automation' do
        automation = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_automation' }
        assert_nil automation
      end

      it 'deleted a macro' do
        macro = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_macro' }
        assert_nil macro
      end

      it 'deleted a organization field' do
        organization_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_organization_field' }
        assert_nil organization_field
      end

      it 'deleted a target' do
        target = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_target' }
        assert_nil target
      end

      it 'deleted a ticket field' do
        ticket_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_ticket_field' }
        assert_nil ticket_field
      end

      it 'deleted a trigger' do
        trigger = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_trigger' }
        assert_nil trigger
      end

      it 'deleted a user field' do
        user_field = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_user_field' }
        assert_nil user_field
      end

      it 'deleted a view' do
        view = @json['job_status']['results']['resources'].find { |r| r['identifier'] == 'my_view' }
        assert_nil view
      end
    end

    describe 'a DELETE to #destroy' do
      before do
        create_resources(one_of_each)
        store_resources

        delete :destroy, params: { id: @collection_id }
      end

      it 'deleted resources' do
        check_resources automations: -1, macros: -1, organization_fields: -1, targets: -1, ticket_fields: -1, triggers: -1, user_fields: -1, views: -1
      end

      describe 'when the collection does not exist' do
        before do
          delete :destroy, params: { id: @collection_id }
        end

        it 'has a nice error message' do
          expected = {
            'error' => {
              'title' => 'Collection not found',
              'message' => "Collection ##{@collection_id} does not exist"
            }
          }
          assert_equal expected, JSON.parse(response.body)
        end
      end
    end

    describe 'a DELETE to #destroy does not error when a resource got somehow deleted' do
      before do
        create_resources(one_of_each)

        store_resources

        # find the IDs for later us
        resources = ResourceCollection.find(@collection_id).collection_resources
        @automation_id = resources.find { |r| r['identifier'] == 'my_automation' }['resource_id']
        @macro_id = resources.find { |r| r['identifier'] == 'my_macro' }['resource_id']

        # we delete the automation
        automation = Automation.find(@automation_id)
        automation.deleted_at = '2018-01-01'
        automation.save(validate: false)

        delete :destroy, params: { id: @collection_id }

        @json = JSON.parse(response.body)
        refute @json['error'], @json['description']
      end

      it 'deleted resources' do
        check_resources automations: -1, macros: -1, organization_fields: -1, targets: -1, ticket_fields: -1, triggers: -1, user_fields: -1, views: -1
      end
    end

    describe 'a POST to #create - with a trigger that utilises a ticket/user/organization field' do
      before do
        store_resources
        Timecop.travel(Time.now + 1.seconds)

        post :create, params: { payload: trigger_with_fields }, as: :json

        @json = JSON.parse(response.body)
        refute @json['error'], @json['description']
      end

      it 'creates new resources' do
        check_resources organization_fields: 1, ticket_fields: 2, triggers: 1, user_fields: 1
      end

      it 'correctly interpolates ticket field values in a trigger' do
        resources = @json['job_status']['results']['resources']
        trigger_id = resources.find { |r| r['identifier'] == 'set_ticket_is_worth_solving' }['resource_id']
        trigger = Rule.find(trigger_id)

        worth_solving_id = resources.find { |r| r['identifier'] == 'worth_solving' }['resource_id']

        assert_equal "ticket_fields_#{worth_solving_id}", trigger.definition.actions.first.source
      end

      it 'correctly interpolates user field values in a trigger' do
        resources = @json['job_status']['results']['resources']
        trigger_id = resources.find { |r| r['identifier'] == 'set_ticket_is_worth_solving' }['resource_id']
        trigger = Rule.find(trigger_id)

        assert_equal "requester.custom_fields.my_user_field_key", trigger.definition.conditions_any.first.source
      end

      it 'correctly interpolates organization field values in a trigger' do
        resources = @json['job_status']['results']['resources']
        trigger_id = resources.find { |r| r['identifier'] == 'set_ticket_is_worth_solving' }['resource_id']
        trigger = Rule.find(trigger_id)

        assert_equal "organization.custom_fields.my_org_field_key", trigger.definition.conditions_any.second.source
      end
    end

    describe 'a POST to #create - with a trigger that utilises a target' do
      before do
        store_resources

        post :create, params: { payload: trigger_with_target }, as: :json

        @json = JSON.parse(response.body)
        refute @json['error'], @json['description']
      end

      it 'creates new resources' do
        check_resources targets: 1, triggers: 1
      end

      it 'correctly interpolates a target into a trigger' do
        resources = @json['job_status']['results']['resources']
        trigger_id = resources.find { |r| r['identifier'] == 'set_ticket_is_worth_solving' }['resource_id']
        trigger = Rule.find(trigger_id)

        assert_equal "Ticket {{ticket.id}} is worth solving!", trigger.definition.actions.first.value.second
      end
    end

    describe 'a POST to #create with errors in the resources' do
      before do
        store_resources

        post :create, params: { payload: payload_errors }, as: :json
      end

      it 'does not create resources' do
        check_resources
      end

      it 'fails and returns an error' do
        json = JSON.parse(response.body)
        assert_equal 'RecordInvalid', json['error']
        errors = json['details']['base'].map { |e| e['description'] }

        assert_equal [
          "Invalid value 'high' in 'Status / Is / high'",
          "Status cannot be set to New",
          "An automation that runs multiple times per ticket is not allowed. Use a time-based condition that is true only once (<b>Hours since created is 24</b>), or add an action that nullifies the condition. For example, a <b>Priority is High</b> condition paired with an action that sets the <b>Priority to Urgent</b>. <a href=\"https://support.zendesk.com/hc/en-us/articles/203662236#topic_mbl_q4f_tm\" target=\"_blank\" rel=\"noopener noreferrer\">Learn more</a>"
        ], errors
      end
    end

    describe 'a POST to #create - with a simple payload' do
      before do
        store_resources

        post :create, params: { payload: payload }, as: :json

        @json = JSON.parse(response.body)
        refute @json['error'], @json['description']
      end

      it 'creates the resources' do
        check_resources automations: 1, macros: 1
      end
    end

    describe 'a PUT to #update - pre-existing requirement with an update that has errors' do
      before do
        create_resources(payload)
        store_resources

        # find the IDs for later us
        resources = ResourceCollection.find(@collection_id).collection_resources
        @automation_id = resources.find { |r| r['identifier'] == 'my_automation' }['resource_id']
        @macro_id = resources.find { |r| r['identifier'] == 'my_macro' }['resource_id']

        put :update, params: { payload: payload_errors, id: @collection_id }
      end

      it 'does not change the resources' do
        check_resources
      end

      it 'fails and receives errors' do
        json = JSON.parse(response.body)
        assert_equal 'RecordInvalid', json['error']
        errors = json['details']['base'].map { |e| e['description'] }

        assert_equal [
          "Invalid value 'high' in 'Status / Is / high'",
          "Status cannot be set to New",
          "An automation that runs multiple times per ticket is not allowed. Use a time-based condition that is true only once (<b>Hours since created is 24</b>), or add an action that nullifies the condition. For example, a <b>Priority is High</b> condition paired with an action that sets the <b>Priority to Urgent</b>. <a href=\"https://support.zendesk.com/hc/en-us/articles/203662236#topic_mbl_q4f_tm\" target=\"_blank\" rel=\"noopener noreferrer\">Learn more</a>"
        ], errors
      end

      it 'didn\'t change my automation' do
        automation = Automation.find(@automation_id)
        assert_equal 'My Automation', automation.title
      end

      it 'didn\'t delete my macro' do
        refute !Macro.exists?(@macro_id), 'Macro got deleted'
        macro = Macro.find(@macro_id)
        assert_equal 'My Macro', macro.title
      end
    end

    describe 'a PUT to #update - pre-existing requirement with an update that has new requirements with errors' do
      before do
        create_resources(payload)
        store_resources

        # find the IDs for later use
        resources = ResourceCollection.find(@collection_id).collection_resources
        @automation_id = resources.find { |r| r['identifier'] == 'my_automation' }['resource_id']
        @macro_id = resources.find { |r| r['identifier'] == 'my_macro' }['resource_id']

        put :update, params: { payload: payload_errors_2, id: @collection_id }
      end

      it 'does not change the resources' do
        check_resources
      end

      it 'fails and receives errors' do
        json = JSON.parse(response.body)
        assert_equal 'RecordInvalid', json['error']
        errors = json['details']['base'].map { |e| e['description'] }

        assert_equal [
          "Invalid value 'high' in 'Status / Is / high'",
          "Status cannot be set to New",
          "An automation that runs multiple times per ticket is not allowed. Use a time-based condition that is true only once (<b>Hours since created is 24</b>), or add an action that nullifies the condition. For example, a <b>Priority is High</b> condition paired with an action that sets the <b>Priority to Urgent</b>. <a href=\"https://support.zendesk.com/hc/en-us/articles/203662236#topic_mbl_q4f_tm\" target=\"_blank\" rel=\"noopener noreferrer\">Learn more</a>"
        ], errors
      end

      it 'didn\'t change my automation' do
        automation = Automation.find(@automation_id)
        assert_equal 'My Automation', automation.title
      end

      it 'didn\'t delete my macro' do
        macro = Macro.find(@macro_id)
        assert_equal 'My Macro', macro.title
      end
    end
  end

  private

  def put_update_many(count)
    put :update_many, params: { tickets: Array.new(count) { |i| { "id" => i, "subject" => "Updated subject" } } }
  end

  def create_resources(resource_payload)
    resource_payload_data = ResourceCollection::PayloadData.new(resource_payload)
    resource_collection = ResourceCollection::CollectionCreate.new(
      collection,
      payload_data: resource_payload_data,
      account: @account,
      user: @user
    )
    resource_collection.create_resources {}
    @collection_id = resource_collection.collection.id
  end

  def store_resources
    current_account            = @account
    @automations_count         = current_account.automations.count(:all)
    @macros_count              = current_account.macros.count(:all)
    @organization_fields_count = current_account.custom_fields.where(owner: 'Organization').count(:all)
    @targets_count             = current_account.targets.count(:all)
    @ticket_fields_count       = current_account.ticket_fields.count(:all)
    @triggers_count            = current_account.triggers.count(:all)
    @user_fields_count         = current_account.custom_fields.where(owner: 'User').count(:all)
    @views_count               = current_account.views.count(:all)
  end

  def check_resources(resources = {})
    should_be = Hash.new(0).merge(resources)

    current_account = @account
    assert_equal should_be[:automations],         current_account.automations.count(:all) - @automations_count
    assert_equal should_be[:macros],              current_account.macros.count(:all) - @macros_count
    assert_equal should_be[:organization_fields], current_account.custom_fields.where(owner: "Organization").count(:all) - @organization_fields_count
    assert_equal should_be[:targets],             current_account.targets.count(:all) - @targets_count
    assert_equal should_be[:ticket_fields],       current_account.ticket_fields.count(:all) - @ticket_fields_count
    assert_equal should_be[:triggers],            current_account.triggers.count(:all) - @triggers_count
    assert_equal should_be[:user_fields],         current_account.custom_fields.where(owner: "User").count(:all) - @user_fields_count
    assert_equal should_be[:views],               current_account.views.count(:all) - @views_count
  end
end
