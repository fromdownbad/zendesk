require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::SandboxController do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    use_ssl
    accept :json
  end

  with_options(controller: "api/v2/account/sandbox") do |request|
    request.should_route :get,    "/api/v2/account/sandbox",   action: "show"
    request.should_route :post,   "/api/v2/account/sandbox",   action: "create"
    request.should_route :delete, "/api/v2/account/sandbox", action: "destroy"
  end

  as_an_anonymous_user do
    should_be_unauthorized :show, :create, :destroy
  end

  as_an_end_user do
    should_be_forbidden :show, :create, :destroy
  end

  as_an_agent do
    should_be_forbidden :show, :create, :destroy
  end

  as_an_admin do
    before do
      @account = accounts(:minimum)
      Account.any_instance.stubs(:has_sandbox?).returns(true)
    end

    describe "a GET to :show" do
      describe "when there is no sandbox" do
        before { get :show }
        it('responds with not_found') { assert_response :not_found }
      end

      describe "when there is a sandbox" do
        before do
          @controller.stubs(:sandbox).returns(@account)
          get :show
        end

        should_use_presenter Api::V2::AccountPresenter, status: :ok
      end
    end

    describe "a POST to :create when sandbox feature is enabled" do
      before do
        @account.expects(:reset_sandbox)
        @controller.stubs(:current_account).returns(@account)
        Account.any_instance.stubs(:has_sandbox?).returns(true)
        post :create
      end

      describe "when account has sandbox" do
        before do
          @controller.stubs(:sandbox).returns(@account)
        end

        should_use_presenter Api::V2::AccountPresenter, status: :created
      end

      describe "when account does not have sandbox" do
        before do
          @controller.stubs(:sandbox).returns(nil)
        end

        should_use_presenter Api::V2::AccountPresenter, status: :created
      end
    end

    describe "rate limiting" do
      before do
        Account.any_instance.stubs(:reset_sandbox).returns(accounts(:minimum))
        @controller.stubs(:current_account).returns(@account)
        Account.any_instance.stubs(:has_sandbox?).returns(true)
        post :create
      end

      it "it rate limits" do
        Timecop.freeze(1.second.ago) do
          10.times do
            post :create
          end
        end
        assert_response 429
      end
    end

    describe "a POST to :create when sandbox feature is not enabled" do
      before do
        @controller.stubs(:current_account).returns(accounts(:minimum));
        Account.any_instance.stubs(:has_sandbox?).returns(false)
        post :create
      end

      it "cannot create sandbox" do
        assert_response :forbidden
      end
    end

    describe "a DELETE to :destroy" do
      before do
        @account.expects(:destroy_sandbox)
        @controller.stubs(:current_account).returns(@account)
      end

      it "responds with 204" do
        delete :destroy
        assert_response :no_content
      end
    end
  end
end
