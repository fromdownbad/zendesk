require_relative '../../../../support/test_helper'
require 'zendesk_billing_core/zuora/plan_validator'

SingleCov.covered! uncovered: 14

describe Api::V2::Account::SubscriptionController do
  include ZBC::ZuoraTestHelper
  extend Api::Presentation::TestHelper

  fixtures :all

  let(:zuora_managed) { true }
  let(:voice_subscription) do
    stub(
      is_active?: false,
      max_agents: 2,
      plan_type:  3
    )
  end

  let(:guide_subscription) do
    Guide::Subscription.new(
      active:             true,
      legacy:             false,
      max_agents:         2,
      plan_type:          2,
      trial:              false,
      trial_expires_at:   nil
    )
  end

  let(:answer_bot_subscription) { nil }

  let(:outbound_subscription) { nil }

  let(:explore_subscription) { nil }

  let(:statsd_client) { stub_for_statsd }

  before do
    Account.any_instance.stubs(:has_zuora_managed_billing_enabled?).returns(zuora_managed)
    Account.any_instance.stubs(:voice_subscription).returns(voice_subscription)
    Account.any_instance.stubs(:guide_subscription).returns(guide_subscription)
    Account.any_instance.stubs(:answer_bot_subscription).returns(answer_bot_subscription)
    Account.any_instance.stubs(:outbound_subscription).returns(outbound_subscription)
    Account.any_instance.stubs(:explore_subscription).returns(explore_subscription)
    Account.any_instance.stubs(:suite_allowed?).returns(anything)

    @zuora_client = stub(zuora_account_id: 'zuora_account_id')
    @controller.stubs(:zuora_client).returns(@zuora_client)

    ZBC::Zuora::ZopimSubscriptionPricing.any_instance.stubs(:zopim_plans_from_zuora)
    ZBC::Zuora::ZendeskSubscriptionPricing.any_instance.stubs(:available_plans)
    ZBC::Zuora::ZendeskSubscriptionPricing.any_instance.stubs(:zendesk_agent_cost_summary)
    ::Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)

    accept :json
  end

  with_options(controller: 'api/v2/account/subscription') do |request|
    request.should_route :get,    '/api/v2/account/subscription/preview', action: 'preview'
    request.should_route :get,    '/api/v2/account/subscription',         action: 'show'
    request.should_route :put,    '/api/v2/account/subscription',         action: 'update'
    request.should_route :delete, '/api/v2/account/subscription',         action: 'destroy'
  end

  as_an_agent do
    describe '#show' do
      before do
        get :show
      end

      it('responds with success') { assert_response :success }
      should_use_presenter Api::V2::Account::SubscriptionPresenter

      describe 'when subscription is managed by zendesk' do
        before do
          @controller.send(:current_account).stubs(:has_zuora_managed_billing_enabled?).returns(false)
          @controller.send(:current_account).save!
          get :show
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end

    should_be_forbidden :preview
    should_be_forbidden :update
    should_be_forbidden :destroy
  end

  as_an_owner do
    describe '#show' do
      describe 'when the subscription is managed by Zuora' do
        let(:zuora_managed) { true }

        before { get(:show) }

        it 'responds with success' do
          assert_response :success
        end

        should_use_presenter Api::V2::Account::SubscriptionPresenter
      end

      describe 'when subscription is not managed by Zuora' do
        let(:zuora_managed) { false }

        before { get(:show) }

        it 'responds with unprocessable_entity' do
          assert_response :unprocessable_entity
        end
      end

      describe 'when account is multiproduct' do
        let(:account) { @controller.send(:current_account) }

        before do
          account.stubs(multiproduct?: true)
          @controller.stubs(:support_product).returns(support_product)
        end

        describe 'and Support is not activated' do
          let(:support_product) { nil }

          before do
            account.subscription.destroy
            get(:show)
          end

          it 'responds with success forbidden' do
            assert_response :forbidden
          end
        end

        describe 'and Support is expired' do
          let(:support_product) { stub(expired?: true) }

          before do
            get(:show)
          end

          it 'responds with success status' do
            assert_response :success
          end
        end
      end

      describe 'when a Kragle::ServerError is raised' do
        before do
          @controller.stubs(:subscription).raises(Kragle::ServerError)
          get :show
        end

        it 'responds with the correct error code' do
          assert_response :bad_gateway
        end

        it 'responds with proper error message' do
          expected_description = 'Unable to connect to the account service.'
          actual_description   = JSON.parse(@response.body)['description']

          assert_equal expected_description, actual_description
        end
      end
    end

    describe '#preview' do
      let(:preview_params) do
        {
          subscription: {
            plan_type:          preview_plan_type,
            billing_cycle_type: preview_billing_cycle_type,
            max_agents:         preview_max_agents
          }
        }
      end

      let(:current_plan_type)          { SubscriptionPlanType.ExtraLarge }
      let(:current_billing_cycle_type) { BillingCycleType.Annually }
      let(:current_max_agents)         { 5 }

      let(:preview_plan_type)          { SubscriptionPlanType.Large }
      let(:preview_billing_cycle_type) { BillingCycleType.Annually }
      let(:preview_max_agents)         { 5 }

      before do
        @account = accounts(:minimum)
        @account.subscription.update_attributes!(
          plan_type:          current_plan_type,
          billing_cycle_type: current_billing_cycle_type,
          max_agents:         current_max_agents
        )

        @request.account = @account

        ZBC::Zuora::ZendeskSubscriptionPricing.any_instance.stubs(:zopim_agent_cost_summary).returns(
          monthly_unit_cost:                     20,
          monthly_unit_cost_with_annual_billing: 15,
          agents_per_unit:                       1,
          annual_unit_cost:                      15
        )

        @controller.stubs(:current_account).returns(@account)
      end

      subject { get(:preview, params: preview_params) }

      describe 'when the subscription is managed by Zuora' do
        let(:zuora_managed) { true }

        describe 'with a valid configuration' do
          it 'responds with :success' do
            subject

            assert_response :success
          end

          it "does not modify the subscription's plan_type" do
            subject

            actual_plan_type = @account.subscription.reload.plan_type

            assert_equal current_plan_type, actual_plan_type
          end

          it 'increments the preview_success counter' do
            statsd_client.expects(:increment).with('sli.preview_success', anything)

            subject
          end

          describe 'presenter' do
            before { subject }

            should_use_presenter Api::V2::Account::SubscriptionPresenter
          end
        end

        describe 'with an invalid configuration' do
          describe 'using an unsupported :plan_type' do
            let(:preview_plan_type) { -1 }

            it 'responds with :unprocessable_entity' do
              subject

              assert_response :unprocessable_entity
            end

            it 'renders the appropriate error information' do
              subject

              details = JSON.parse(@response.body)['details']

              assert details.key?('plan_type')
            end

            it 'does not increment the preview_success counter' do
              statsd_client.expects(:increment).with('sli.preview_success', anything).never

              subject
            end
          end

          describe 'using an unsupported :billing_cycle_type' do
            let(:preview_billing_cycle_type) { -1 }

            it 'responds with :unprocessable_entity' do
              subject

              assert_response :unprocessable_entity
            end

            it 'renders the appropriate error information' do
              subject

              details = JSON.parse(@response.body)['details']

              assert details.key?('billing_cycle_type')
            end

            it 'does not increment the preview_success counter' do
              statsd_client.expects(:increment).with('sli.preview_success', anything).never

              subject
            end
          end

          describe 'using an unsupported :max_agents' do
            let(:preview_max_agents) { -1 }

            it 'responds with :unprocessable_entity' do
              subject

              assert_response :unprocessable_entity
            end

            it 'renders the appropriate error information' do
              subject

              details = JSON.parse(@response.body)['details']

              assert details.key?('max_agents')
            end

            it 'does not increment the preview_success counter' do
              statsd_client.expects(:increment).with('sli.preview_success', anything).never

              subject
            end
          end
        end

        describe 'with coupons' do
          let(:preview_params) do
            {
              include: 'pricing',
              subscription: {
                plan_type:          preview_plan_type,
                billing_cycle_type: preview_billing_cycle_type,
                max_agents:         preview_max_agents,
                promo_code:         preview_promo_code
              }
            }
          end

          let(:preview_promo_code) { 'ANYCODE' }

          let(:presented_pricing) { JSON.parse(@response.body)['pricing'] }

          before do
            preview = stub

            ZBC::Zuora::Commands::Subscribe::Preview.expects(:new).with do |options|
              options.account_id == @account.id &&
                options.max_agents == preview_max_agents &&
                options.billing_cycle_type == 4 &&
                options.plan_type == preview_plan_type &&
                options.coupon_code == preview_promo_code
            end.returns(preview)

            preview.expects(:pricing_calculations).returns(pricing_calculations)

            Subscription.any_instance.stubs(:guide_preview).returns(nil)

            preview.stubs(:pending_changes).returns(
              guide:   {},
              tpe:     {},
              voice:   {},
              zendesk: {},
              zopim:   {}
            )

            ZBC::Zuora::Plan.stubs(:lookup).with(
              SubscriptionPlanType.Large,
              BillingCycleType.Monthly,
              @account.subscription.pricing_model_revision
            ).returns(
              stub(
                has_base_pricing?:        false,
                net_unit_price_per_month: 100.0,
                net_unit_price:           1200.0
              )
            )

            ZBC::Zuora::Plan.stubs(:lookup).with(
              SubscriptionPlanType.Large,
              BillingCycleType.Annually,
              @account.subscription.pricing_model_revision
            ).returns(
              stub(
                has_base_pricing?:        false,
                net_unit_price_per_month: 83.0,
                net_unit_price:           996.0
              )
            )
          end

          describe 'with a valid coupon' do
            let(:pricing_calculations) do
              {
                cycle_undiscounted_price: 3540,
                cycle_discount_no_coupon: 600,
                cycle_coupon_discount:    50,
                cycle_discounted_price:   2890,
                coupon_code:              'ANYCODE',
                coupon_expires_on:        '2012-12-21',
                coupon_status:            :ok
              }
            end

            it 'responds with :success' do
              subject

              assert_response :success
            end

            it "does not modify the subscription's plan_type" do
              subject

              actual_plan_type = @account.subscription.reload.plan_type

              assert_equal current_plan_type, actual_plan_type
            end

            it 'presents pricing information with a complete promo data' do
              subject

              presented_promo    = presented_pricing['promo']
              presented_discount = presented_promo['discount']
              presented_code     = presented_promo['code']
              presented_ends_at  = presented_promo['ends_at']

              assert_equal 50,                 presented_discount
              assert_equal preview_promo_code, presented_code
              assert_equal '2012-12-21',       presented_ends_at
            end

            it 'presents pricing information with an invalid promo status' do
              subject

              presented_promo  = presented_pricing['promo']
              presented_status = presented_promo['status']

              assert_equal 'ok', presented_status
            end

            it 'presents pricing information without the promo discount applied' do
              subject

              presented_charge = presented_pricing['charge']
              presented_gross  = presented_charge['gross']
              presented_net    = presented_charge['net']

              assert_equal 3_540, presented_gross
              assert_equal 2_890, presented_net
            end

            it 'presents pricing information with the requested plan type' do
              subject

              presented_plan    = presented_pricing['plan']
              presented_plan_id = presented_plan['id']

              assert_equal preview_plan_type, presented_plan_id
            end

            it 'increments the preview_success counter' do
              statsd_client.expects(:increment).with('sli.preview_success', anything)

              subject
            end
          end

          describe 'with an invalid coupon' do
            let(:pricing_calculations) do
              {
                cycle_undiscounted_price: 3540,
                cycle_discount_no_coupon: 600,
                cycle_coupon_discount:    0,
                cycle_discounted_price:   2940,
                coupon_code:              'ANYCODE',
                coupon_expires_on:        nil,
                coupon_status:            :not_valid
              }
            end

            it 'responds with :success' do
              subject

              assert_response :success
            end

            it 'presents pricing information with an incomplete promo data' do
              subject

              presented_promo    = presented_pricing['promo']
              presented_code     = presented_promo['code']
              presented_discount = presented_promo['discount']
              presented_ends_at  = presented_promo['ends_at']

              assert_equal 'ANYCODE', presented_code
              assert_equal 0, presented_discount
              assert_nil presented_ends_at
            end

            it 'presents pricing information with an invalid promo status' do
              subject

              presented_promo  = presented_pricing['promo']
              presented_status = presented_promo['status']

              assert_equal 'not_valid', presented_status
            end

            it 'presents pricing information without the promo discount applied' do
              subject

              presented_charge = presented_pricing['charge']
              presented_gross  = presented_charge['gross']
              presented_net    = presented_charge['net']

              assert_equal 3_540, presented_gross
              assert_equal 2_940, presented_net
            end

            it 'presents pricing information with the requested plan type' do
              subject

              presented_plan = presented_pricing['plan']
              presented_id   = presented_plan['id']

              assert_equal SubscriptionPlanType.Large, presented_id
            end

            it 'increments the preview_success counter' do
              statsd_client.expects(:increment).with('sli.preview_success', anything)

              subject
            end
          end
        end
      end

      describe 'when the subscription is not managed by Zuora' do
        let(:zuora_managed) { false }

        it 'responds with :unprocessable_entity' do
          subject

          assert_response :unprocessable_entity
        end

        it 'does not increment the preview_success counter' do
          statsd_client.expects(:increment).with('sli.preview_success', anything).never

          subject
        end
      end
    end

    describe '#update' do
      let(:current_billing_cycle_type) { BillingCycleType.Annually }
      let(:current_support_plan_type)  { SubscriptionPlanType.ExtraLarge }
      let(:current_support_max_agents) { 5 }

      let(:update_billing_cycle_type)  { BillingCycleType.Annually }
      let(:update_support_plan_type)   { SubscriptionPlanType.ExtraLarge }
      let(:update_support_max_agents)  { 999 }

      let(:update_params) do
        {
          subscription: {
            billing_cycle_type: update_billing_cycle_type,
            max_agents:         update_support_max_agents,
            plan_type:          update_support_plan_type
          }
        }
      end

      before do
        @account = accounts(:minimum)
        @account.subscription.update_attributes!(
          billing_cycle_type: current_billing_cycle_type,
          plan_type:          current_support_plan_type,
          max_agents:         current_support_max_agents
        )

        @controller.stubs(:log)

        ZBC::Zuora::Synchronizer.stubs(:new).returns(stub)
      end

      subject { put(:update, params: update_params) }

      describe 'when the subscription is managed by Zuora' do
        let(:zuora_managed) { true }

        describe 'with a valid plan_configuration' do
          let(:update_supportplan_type)  { SubscriptionPlanType.Large }
          let(:update_supportmax_agents) { 101 }

          describe 'when the Zuora update request is successful' do
            before do
              ZBC::Zuora::Commands::Amend.any_instance.
                stubs(:execute!).returns(true)
            end

            it 'responds with a :success status code' do
              subject

              assert_response :success
            end

            it 'calls #execute! on the ZBC::Zuora::Amender instance' do
              ZBC::Zuora::Commands::Amend.any_instance.expects(:execute!)

              subject
            end
          end

          describe 'when the Zuora update request is not successful' do
            before do
              ZBC::Zuora::Commands::Amend.any_instance.stubs(:execute!).
                raises(ZuoraClient::UpdateFailure)
            end

            it 'increments the :purchase_error counter' do
              statsd_client.expects(:increment).
                with('sli.purchase_error', anything)

              subject
            end
          end
        end

        describe 'with an invalid plan_configuration' do
          describe 'with an unsupported :plan_type' do
            let(:update_support_plan_type) { -1 }

            it 'responds with an :unprocessable_entity status code' do
              subject

              assert_response :unprocessable_entity
            end
          end

          describe "when the :zuora_subscription's sales model is ASSISTED" do
            let(:current_sales_model) { ZBC::States::SalesModel::ASSISTED }

            before do
              subscription = create_zuora_subscription(account: @request.account)
              subscription.update_attributes(sales_model: current_sales_model)
            end

            it 'responds with an :unprocessable_entity status code' do
              subject

              assert_response :unprocessable_entity
            end

            it 'responds the correct status message' do
              subject

              actual_status = JSON.parse(@response.body)['status']

              assert_match /Self-service/, actual_status
            end
          end
        end

        describe 'when the plan_configuration includes voice params' do
          let(:update_params) do
            {
              subscription: {
                billing_cycle_type: update_billing_cycle_type,
                plan_type:          update_support_plan_type,
                max_agents:         update_support_max_agents,
                voice_plan_type:    update_voice_plan_type,
                voice_max_agents:   update_voice_max_agents
              }
            }
          end

          let(:update_voice_plan_type) { 2 }

          describe 'with valid voice params' do
            let(:update_support_max_agents) { 10 }
            let(:update_voice_max_agents)   { 10 }

            before do
              ZBC::Zuora::Commands::Amend.any_instance.stubs(:execute!).returns(true)
            end

            it 'responds with a :success status code' do
              subject

              assert_response :success
            end

            it 'calls ZBC::Zuora::Amender#execute!' do
              ZBC::Zuora::Commands::Amend.any_instance.expects(:execute!)

              subject
            end
          end

          describe 'with an invalid plan_configuration' do
            describe 'when the voice max_agents is greater than the support max_agents' do
              let(:update_support_max_agents) { 5 }
              let(:update_voice_max_agents)   { 10 }

              it 'responds with an :unprocessable_entity status code' do
                subject

                assert_response :unprocessable_entity
              end

              it 'responds with the correct error details' do
                subject

                actual   = JSON.parse(@response.body)['description']
                expected = 'Unable to apply changes to plan, please ' \
                           'contact support@zendesk.com'

                assert_equal expected, actual
              end

              it 'does not call ZBC::Zuora::Amender#execute!' do
                ZBC::Zuora::Commands::Amend.any_instance.
                  expects(:execute!).never

                subject
              end
            end
          end
        end
      end

      describe 'when the subscription is not managed by Zuora' do
        let(:zuora_managed) { false }

        it 'responds with an :unprocessable_entity status code' do
          subject

          assert_response :unprocessable_entity
        end
      end
    end

    describe '#destroy' do
      before do
        @account         = accounts(:minimum)
        @request.account = @account
        Subscription.any_instance.stubs(zuora_managed?: true, zuora_subscription: stub(:cancel!))
      end

      describe 'when subscription is managed by zendesk' do
        before do
          @account.stubs(:has_zuora_managed_billing_enabled?).returns(false)
          delete :destroy, params: { confirm: @account.subdomain }
        end

        it('responds with success') { assert_response :success }
      end

      describe 'and confirmation is supplied' do
        before do
          Account.any_instance.expects(:cancel!)
          delete :destroy, params: { confirm: @account.subdomain }
        end

        it('responds with success') { assert_response :success }
      end

      describe 'and confirmation is not supplied' do
        before do
          Account.any_instance.expects(:cancel!).never
          refute_difference 'Account.where(is_active: true).count(:all)' do
            delete :destroy
          end
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end

    describe '#create' do
      let(:subscriber)       { stub(execute!: subscribe_result) }
      let(:subscribe_result) { stub(account_id: 'abcdefg') }
      let(:synchronizer)     { stub(synchronize!: true) }

      let(:create_params) do
        {
          account: {
            number: @account.id
          },
          subscription: {
            billing_cycle_type:          billing_cycle_type,
            plan_type:                   support_plan_type,
            max_agents:                  support_max_agents,
            payment_method_reference_id: payment_ref_id,
          }
        }
      end

      let(:billing_cycle_type) { BillingCycleType.Annually }
      let(:support_max_agents) { 5 }
      let(:support_plan_type)  { SubscriptionPlanType.Large }
      let(:payment_ref_id)     { '2c92c0f939072aca01390881697018e7' }

      before do
        @account = accounts(:minimum)
        @request.account = @account

        ZBC::Zuora::Commands::Subscribe.stubs(:new).returns(subscriber)
        subscriber.stubs('subscription_options=' => true)

        ZBC::Zuora::Synchronizer.stubs(:new).returns(synchronizer)

        ZUORA_LOG.stubs(warn: true)
      end

      subject { post(:create, params: create_params) }

      describe 'when the Zuora subscribe request is successful' do
        it 'responds with a :success status code' do
          subject

          assert_response :success
        end
      end

      describe 'when AnswerBot requirements are not met' do
        before do
          AnswerBot::SubscriptionOptions.expects(:build_for).
            raises(AnswerBot::SubscriptionOptions::Error.new)
        end

        it 'responds with an :unprocessable_entity status code' do
          subject

          assert_response :unprocessable_entity
        end
      end

      describe 'when Outbound requirements are not met' do
        before do
          Outbound::SubscriptionOptions.expects(:build_for).
            raises(Outbound::SubscriptionOptions::InvalidMonthlyMessagedUsers.new)
        end

        it 'responds with an :unprocessable_entity status code' do
          subject

          assert_response :unprocessable_entity
        end
      end

      describe 'when the Zuora subscribe request is not successful' do
        describe 'when an InvalidPlanConfiguration error is raised' do
          before do
            ZBC::Zuora::PlanValidator.any_instance.expects(:validate).raises(
              ZBC::Zuora::Errors::InvalidPlanConfigurationError.new('starter')
            )
          end

          it 'responds with an :unprocessable_entity status code' do
            subject

            assert_response :unprocessable_entity
          end
        end

        describe 'when a ZuoraClient::SubscribeCommandFailure error is raised due to taxation' do
          before do
            subscriber.expects(:execute!).raises(
              ZuoraClient::SubscribeCommandFailure.new('Taxation')
            )
          end

          it 'responds with an :unprocessable_entity status code' do
            subject

            assert_response :unprocessable_entity
          end

          it 'responds with a proper error message' do
            subject

            expected_msg = 'State is required if country is United States or ' \
                           'Canada. Please try again.'
            actual_msg   = JSON.parse(@response.body)['description']

            assert_equal expected_msg, actual_msg
          end

          it 'increments the :purchase_error counter' do
            statsd_client.expects(:increment).
              with('sli.purchase_error', anything)

            subject
          end

          it 'logs the error details' do
            ZUORA_LOG.expects(:warn)

            subject
          end
        end

        describe 'when a ZuoraClient::SubscribeCommandFailure error is raised not due to taxation' do
          before do
            subscriber.expects(:execute!).raises(
              ZuoraClient::SubscribeCommandFailure.new('Zuora is down')
            )
          end

          it 'responds with an :unprocessable_entity status code' do
            subject

            assert_response :unprocessable_entity
          end

          it 'responds with a proper error message' do
            subject

            expected_msg = 'An internal error occurred. Please try again'
            actual_msg   = JSON.parse(@response.body)['description']

            assert_equal expected_msg, actual_msg
          end

          it 'increments the :purchase_error counter' do
            statsd_client.expects(:increment).
              with('sli.purchase_error', anything)

            subject
          end

          it 'logs the error details' do
            ZUORA_LOG.expects(:warn)

            subject
          end
        end
      end

      describe 'when the create params include voice settings' do
        let(:create_params) do
          {
            account: {
              number: @account.id
            },
            subscription: {
              billing_cycle_type:          billing_cycle_type,
              plan_type:                   support_plan_type,
              max_agents:                  support_max_agents,
              voice_plan_type:             voice_plan_type,
              voice_max_agents:            voice_max_agents,
              payment_method_reference_id: payment_ref_id,
            }
          }
        end

        describe 'with valid voice settings' do
          let(:support_max_agents) { 5 }
          let(:voice_max_agents)   { 5 }
          let(:voice_plan_type)    { SubscriptionPlanType.Medium }

          it 'responds with a :success status code' do
            subject

            assert_response :success
          end
        end

        describe 'with invalid voice settings' do
          describe 'when the voice max_agents exceeds the support max_agents' do
            let(:support_max_agents) { 5 }
            let(:voice_max_agents)   { 10 }
            let(:voice_plan_type)    { SubscriptionPlanType.Medium }

            it 'responds with an :unprocessable_entity status code' do
              subject

              assert_response :unprocessable_entity
            end
          end
        end
      end
    end

    describe '#update_payment_method' do
      let(:zuora_subscription) do
        {
          zuora_account_id: '456'
        }
      end

      before do
        @account = accounts(:minimum)
        @request.account = @account
        ZBC::Zuora::Subscription.stubs(:find_by_account_id).
          returns(zuora_subscription)
        zuora_subscription.stubs(:zuora_account_id).returns('456')
      end

      describe 'when called' do
        before do
          Billing::Zuora::IronBank::CCPaymentMethodUpdate.expects(:call).with(
            zuora_account_id:            '456',
            payment_method_reference_id: '2c92c0f939072aca01390881697018e7',
            currency:                    1
          )
        end

        it 'renders success json' do
          post :update_payment_method, params: { subscription: {
            payment_method_reference_id: '2c92c0f939072aca01390881697018e7'
          } }
          assert_equal 'Payment method updated', JSON.parse(@response.body)['message']
        end
      end

      describe 'When update goes through without any exception' do
        before do
          Billing::Zuora::IronBank::CCPaymentMethodUpdate.expects(:call).with(
            zuora_account_id:            '456',
            payment_method_reference_id: '2c92c0f939072aca01390881697018e7',
            currency:                    1
          )

          post :update_payment_method, params: { subscription: {
            payment_method_reference_id: '2c92c0f939072aca01390881697018e7'
          } }
        end

        it('responds with success') { assert_response :success }
      end

      describe "when an UpdateFailure is raised with a message containing 'Taxation'" do
        before do
          Billing::Zuora::IronBank::CCPaymentMethodUpdate.stubs(:call).with(
            zuora_account_id:            '456',
            payment_method_reference_id: '2c92c0f939072aca01390881697018e7',
            currency:                    1
          ).raises(ZuoraClient::UpdateFailure.new('Taxation'))

          post :update_payment_method, params: { subscription: {
            payment_method_reference_id: '2c92c0f939072aca01390881697018e7'
          } }
        end

        it 'responds with :unprocessable_entity' do
          assert_response :unprocessable_entity
        end

        it 'responds with proper error message' do
          expected_msg = 'State is required if country is United States or ' \
                         'Canada. Please try again.'
          actual_msg   = JSON.parse(@response.body)['description']

          assert_equal expected_msg, actual_msg
        end

        before_should 'not report this error to the exception tracker' do
          ZendeskExceptions::Logger.expects(:record).never
        end
      end

      describe 'when other UpdateFailures are raised' do
        before do
          Billing::Zuora::IronBank::CCPaymentMethodUpdate.stubs(:call).with(
            zuora_account_id:            '456',
            payment_method_reference_id: '2c92c0f939072aca01390881697018e7',
            currency:                    1
          ).raises(ZuoraClient::UpdateFailure.new('Zuora Down'))

          post :update_payment_method, params: { subscription: {
            payment_method_reference_id: '2c92c0f939072aca01390881697018e7'
          } }
        end

        it 'responds with :unprocessable_entity' do
          assert_response :unprocessable_entity
        end

        it 'responds with proper error message' do
          assert_equal 'An internal error occurred. Please try again', JSON.parse(@response.body)['description']
        end

        before_should 'report this error to the exception tracker' do
          ZendeskExceptions::Logger.expects(:record)
        end
      end
    end

    describe '#update_currency' do
      describe 'for account not in trial' do
        before do
          post :update_currency, params: { subscription: {
            currency: 'GBP'
          } }
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end

    describe '#add_agents' do
      let(:answer_bot_subscription) { nil }
      let(:explore_subscription)    { nil }
      let(:tpe_subscription)        { nil }
      let(:voice_subscription)      { nil }
      let(:zopim_subscription)      { nil }

      before do
        @account = accounts(:minimum)
        @user    = @account.owner
        @account.subscription.update_attributes!(
          plan_type:          SubscriptionPlanType.ExtraLarge,
          billing_cycle_type: BillingCycleType.Annually,
          max_agents:         5
        )
        @subscription = @account.subscription
        @request.account = @account
      end

      describe 'with a valid configuration' do
        before do
          Subscription.any_instance.stubs(:add_agents_available?).
            returns(true)
          Billing::CustomerOrder::Client.expects(:post).
            with(@account, @user, 20).
            returns(success)
        end

        describe 'when CustomerOrder.post is successful' do
          let(:success) { true }

          before do
            ZBC::Zuora::Synchronizer.expects(:synchronize!).once
            post :add_agents, params: { agents: { zendesk_agent_add: 10 } }
          end

          it('responds with success') { assert_response :success }
        end

        describe 'when CustomerOrder.post is not successful' do
          let(:success) { false }

          before do
            ZBC::Zuora::Synchronizer.expects(:synchronize!).never
            post :add_agents, params: { agents: { zendesk_agent_add: 10 } }
          end

          it('responds with unprocessable_entity') do
            assert_response :unprocessable_entity
          end
        end
      end

      describe 'with a invalid configuration' do
        before { post :add_agents, params: { agents: { zendesk_agent_add: -1 } } }

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end
  end

  when_logged_in_as(:trial_admin) do
    describe '#update_currency for account in trial' do
      before do
        Subscription.any_instance.expects(:save)

        post :update_currency, params: { subscription: {
          currency: 'GBP'
        } }
      end

      it('responds with success') { assert_response :success }

      it "updates the currency to 'GBP'" do
        assert_equal CurrencyType.GBP, @account.subscription.currency_type
      end
    end
  end

  as_a_subsystem_user(user: 'bime', account: :minimum) do
    it 'has access to show subscription' do
      get :show
      assert_response :success
    end

    describe 'when subscription is managed by zendesk' do
      before do
        @controller.send(:current_account).stubs(:has_zuora_managed_billing_enabled?).returns(false)
        @controller.send(:current_account).save!
      end

      it('has access to show subscription') do
        get :show
        assert_response :success
      end
    end

    should_be_forbidden :preview
  end

  as_a_subsystem_user(user: 'zendesk_nps', account: :minimum) do
    should_be_forbidden :show
    should_be_forbidden :preview
  end
end
