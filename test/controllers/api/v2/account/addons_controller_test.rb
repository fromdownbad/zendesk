require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::AddonsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    accept :json

    account.subscription.update_attribute(:plan_type,
      SubscriptionPlanType.ExtraLarge)

    account.subscription.update_attribute(:pricing_model_revision,
      ZBC::Zendesk::PricingModelRevision::PATAGONIA)
  end

  with_options(controller: 'api/v2/account/addons') do |request|
    request.should_route :get, '/api/v2/account/addons', action: 'show'
  end

  as_an_anonymous_user do
    should_be_unauthorized :show
  end

  as_an_agent do
    let(:expected) do
      {
        addons:
          [
            {"name" => "enterprise_productivity_pack", "boosted" => false, "purchased" => true},
            {"name" => "eu_data_center", "boosted" => false, "purchased" => false},
            {"name" => "germany_data_center", "boosted" => false, "purchased" => false},
            {"name" => "high_volume_api", "boosted" => false, "purchased" => false},
            {"name" => "light_agents", "boosted" => true, "purchased" => false},
            {"name" => "limited_multibrand", "boosted" => false, "purchased" => false},
            {"name" => "nps", "boosted" => true, "purchased" => false},
            {"name" => "premier_support", "boosted" => false, "purchased" => false},
            {"name" => "priority_support", "boosted" => false, "purchased" => false},
            {"name" => "social_messaging", "boosted" => false, "purchased" => false},
            {"name" => "talk_cti_partner", "boosted" => false, "purchased" => false},
            {"name" => "temporary_zendesk_agents", "boosted" => false, "purchased" => false},
            {"name" => "support_collaboration", "boosted" => false, "purchased" => false},
            {"name" => "us_data_center", "boosted" => false, "purchased" => false},
            {"name" => "whatsapp_phone_number", "boosted" => false, "purchased" => false}
          ].sort_by { |h| h["name"] }
      }
    end

    before do
      account.subscription_feature_addons.create!(
        name:               'advanced_security',
        zuora_rate_plan_id: '123456'
      )
      account.subscription_feature_addons.create!(
        name:               'nps',
        boost_expires_at:   1.week.from_now
      )
      account.subscription_feature_addons.create!(
        name:               'light_agents',
        boost_expires_at:   1.day.from_now
      )
      account.subscription_feature_addons.create!(
        name:               'enterprise_productivity_pack',
        zuora_rate_plan_id: '123456'
      )
    end

    describe "#show" do
      before do
        Zendesk::Features::PlanTypeService.any_instance.stubs(:proposed_plan_type).returns(4)
        Subscription.any_instance.stubs(:plan_type).returns(3)
        get :show
      end

      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
      should_use_presenter Api::V2::CollectionPresenter

      it "renders all addon details (including purchased), regardless of account boost" do
        output = JSON.parse(response.body)["addons"].each { |h| h.except!("readable_name", "url") }
        assert_equal expected[:addons], output.sort_by { |h| h["name"] }
      end
    end
  end
end
