require_relative '../../../../support/test_helper'
require_relative '../../../../support/zopim_test_helper'
require_relative '../../../../support/chat_phase_3_helper'
require_relative '../../../../support/suite_test_helper'

SingleCov.covered! uncovered: 1

describe Api::V2::Account::ZopimSubscriptionController do
  extend Api::Presentation::TestHelper
  include ZopimTestHelper
  include ChatPhase3Helper
  include SuiteTestHelper

  fixtures :all

  before do
    Timecop.freeze('2014-12-23')

    Zopim.config.stubs(reseller_api_credentials: {})
    Account.any_instance.stubs(has_chat_permission_set?: true)

    Zopim::AgentSyncJob.stubs(:work)

    ZopimIntegration.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      ensure_not_phase_four: nil
    )
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)

    ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.stubs(:enqueue)
    ZopimIntegration.stubs(where: stub(:first_or_create!))
    Zopim::Reseller.client.stubs(
      account!: stub(id: 1, account_key: 1234, trial_end_date: Time.now),
      create_account!: Hashie::Mash.new(id: 1),
      account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
      find_accounts!: []
    )

    stub_account_service_sync

    accept :json
  end

  with_options(controller: 'api/v2/account/zopim_subscription') do |request|
    request.should_route :post,   '/api/v2/account/zopim_subscription',            action: 'create'
    request.should_route :delete, '/api/v2/account/zopim_subscription',            action: 'destroy'
    request.should_route :get,    '/api/v2/account/zopim_subscription',            action: 'show'
    request.should_route :post,   '/api/v2/account/zopim_subscription/authorize',  action: 'authorize'

    # DEPRECATED
    request.should_route :put,    '/api/v2/account/zopim_subscription/activate',   action: 'activate'
    request.should_route :delete, '/api/v2/account/zopim_subscription/deactivate', action: 'deactivate'
  end

  as_an_anonymous_user do
    should_be_unauthorized :create, :destroy, :authorize, :show, :update
  end

  as_an_end_user do
    should_be_forbidden :create, :destroy, :authorize, :show, :update
  end

  describe "without Zopim Subcription (possible phaseI customer)" do
    as_an_admin do
      let(:current_account) { @controller.send(:current_account) }
      let(:current_user) { @controller.send(:current_user) }

      before do
        current_account.create_zopim_integration!(
          external_zopim_id: 1,
          zopim_key: 'some-key'
        )
      end

      describe "#show" do
        before { get :show }
        it('responds with not_found') { assert_response :not_found }
      end

      describe "#authorize" do
        before { post :authorize }
        it('responds with not_found') { assert_response :not_found }
      end

      describe "#destroy" do
        before { delete :destroy }
        it('responds with not_found') { assert_response :not_found }
      end
    end
  end

  as_an_agent do
    # TODO: This fails as the response is a 400, 403 is expected
    # should_be_forbidden :destroy, :create

    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }

    before do
      setup_zopim_subscription(current_account)
      current_account.zopim_integration.stubs(:phase_two?).returns(true)
    end

    describe "#show" do
      let(:zopim_subscription) { current_account.zopim_subscription }

      before { get :show }

      it('responds with success') { assert_response :success }
      should_use_presenter Api::V2::Account::ZopimSubscriptionPresenter

      it "renders details of the associated chat account" do
        actual   = JSON.parse(response.body).symbolize_keys
        expected = {
          'zopim_account_id'         => zopim_subscription.zopim_account_id,
          'plan_type'                => 'trial',
          'plan_name'                => 'Trial',
          'max_agents'               => 10,
          'status'                   => 'active',
          'is_serviceable'           => true,
          'trial_expires_on'         => '2015-01-22',
          'expired_trial'            => false,
          'created_at'               => '2014-12-23',
          'trial_days'               => 30,
          'trial_ended_at'           => nil,
          'trial_extension_eligible' => true,
          'final_trial_expire_date'  => '2017-09-18',
          'days_left_in_trial'       => 30,
          'last_synchronized_at'     => '2014-12-23T00:00:00Z',
          'syncstate'                => 'ready',
          'purchased_at'             => nil,
          'zopim_reseller_id'        => 1055,
          'account_chat_limit'       => 3,
          'chat_limits_enabled'      => false
        }
        assert_equal expected, actual[:zopim_subscription]
      end

      describe "when the current user is not a verified user" do
        before do
          current_user.stubs(is_verified?: false)
        end

        describe "#show" do
          before { get :show }
          it('responds with success') { assert_response :success }
        end

        describe "#authorize" do
          before { post :authorize }
          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
        end
      end

      describe "when there is no serviceable chat account" do
        before do
          current_account.stubs(zopim_subscription: nil)
          get :show
        end

        it('responds with not_found') { assert_response :not_found }
      end
    end

    describe "#authorize" do
      let(:current_account) { @controller.send(:current_account) }
      let(:current_user) { @controller.send(:current_user=, current_account.owner) }

      let(:reseller_response) { JSON.parse(response.body).symbolize_keys }

      describe "when the SSO request succeeds" do
        before do
          Zopim::Reseller.client.expects(:request_authorization).
            with(data: { email: current_user.zopim_identity.email }).
            returns(anything)
          post :authorize
        end

        it('responds with success') { assert_response :success }

        it "renders the authorization details" do
          expected = { authorization: {} }
          assert_equal expected, reseller_response
        end
      end

      describe "when the SSO request fails" do
        before do
          Zopim::Reseller.client.expects(:request_authorization).
            with(data: { email: current_user.zopim_identity.email }).
            returns(nil)
          post :authorize
        end

        it('responds with unauthorized') { assert_response :unauthorized }
      end

      describe "when there is no serviceable chat account" do
        before do
          current_account.stubs(zopim_subscription: nil)
          post :authorize
        end

        it('responds with not_found') { assert_response :not_found }
      end

      describe "when the current user is not a verified user" do
        before do
          current_user.stubs(is_verified?: false)
          post :authorize
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end

      describe "when the current user has no serviceable chat identity" do
        before do
          current_user.stubs(zopim_identity: nil)
          post :authorize
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end
  end

  as_an_owner do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }

    should_be_forbidden :create

    describe "#destroy" do
      let(:subscription_deactivator) { stub }
      let(:agent_deactivator) { stub }

      before do
        setup_zopim_subscription(current_account)
        ZendeskBillingCore::Zopim::SubscriptionDeactivator.stubs(:new).
          with(current_account.zopim_subscription).
          returns(subscription_deactivator)
        Zopim::AgentDeactivator.stubs(:new).
          with(current_user.zopim_identity).
          returns(agent_deactivator)
      end

      describe "when deactivation succeeds" do
        let(:zopim_subscription) { current_account.zopim_subscription }
        let(:zopim_agent) { current_user.zopim_identity }

        before do
          subscription_deactivator.expects(:deactivate!)
          agent_deactivator.expects(:deactivate!)
        end

        it "responds with 204" do
          delete :destroy
          assert_response :no_content
        end

        it "destroys the chat-account record" do
          delete :destroy
          assert current_account.zopim_subscription.destroyed?
        end

        it "destroys the zopim_agent records" do
          delete :destroy
          assert_empty(current_account.zopim_agents)
        end
      end

      describe "when the current user is not a verified user" do
        before do
          current_user.stubs(is_verified?: false)
          delete :destroy
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end

      describe "when there is no active chat account" do
        before do
          ZendeskBillingCore::Zopim::Subscription.any_instance.
            stubs(status: ZendeskBillingCore::Zopim::SubscriptionStatus::Cancelled)
          delete :destroy
        end

        it('responds with not_modified') { assert_response :not_modified }
      end

      describe "when the current user has no serviceable chat identity" do
        before do
          current_user.stubs(zopim_identity: nil)
          delete :destroy
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end

      describe "when the current user is not a chat admin" do
        before do
          current_user.zopim_identity.stubs(is_administrator?: false)
          delete :destroy
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end
  end

  as_a_subsystem_user(account: :minimum, user: "zendesk") do
    let(:current_account) { @controller.send(:current_account) }
    let(:owner) { current_account.owner }

    # TODO: These both fail the response is 400, not 403
    # should_be_forbidden :destroy
    # should_be_forbidden :show
    should_be_forbidden [:post, :authorize, {}]

    describe "#create" do
      before do
        ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(
          zopim_account_id: current_account.id,
          chat_limits_enabled: false,
          account_chat_limit: 1
        )
        Zopim::Agent.any_instance.stubs(
          zopim_agent_id: owner.id,
          syncstate: ZendeskBillingCore::Zopim::SyncState::Ready
        )
      end

      describe "when activation succeeds" do
        let(:zopim_subscription) { current_account.zopim_subscription }

        before { post :create }

        it('responds with success') { assert_response :success }
        should_use_presenter Api::V2::Account::ZopimSubscriptionPresenter

        it "activates the chat account" do
          assert current_account.zopim_subscription.is_active?
        end

        it "provisions a chat identity for the owner" do
          assert owner.zopim_identity.is_serviceable?
        end
      end

      describe "when the owner is not a verified user" do
        before do
          User.any_instance.stubs(is_verified?: false)
          post :create
        end

        it('responds with success') { assert_response :success }
        should_use_presenter Api::V2::Account::ZopimSubscriptionPresenter

        it "activates the chat account" do
          assert current_account.zopim_subscription.is_active?
        end

        it "provisions a chat identity for the owner" do
          assert owner.zopim_identity.is_serviceable?
        end
      end

      describe "when there's already a serviceable chat account" do
        before do
          setup_zopim_subscription(current_account)
          post :create
        end

        it('responds with not_modified') { assert_response :not_modified }
      end

      describe "when an upstream exception occurs" do
        before do
          current_account.expects(:create_zopim_subscription!).raises(StandardError)
        end

        it "logs the exception and reraises" do
          ZendeskExceptions::Logger.expects(:record)
          assert_raises StandardError do
            post :create
          end
        end
      end

      describe "when zopim subscription creation fails" do
        before do
          current_account.expects(:create_zopim_subscription!).returns(nil)
        end

        it "does not enable a chat identity for the owner" do
          owner.expects(:enable_chat_identity!).never
          post :create
        end

        it "responds with unprocessable_entity" do
          post :create
          assert_response :unprocessable_entity
        end
      end
    end
  end

  as_a_subsystem_user(account: :minimum, user: "zopim") do
    # TODO: authorize was a POST above- this will default to a GET. Confirm which it is.
    should_be_forbidden :destroy, :create, :authorize

    describe "#show" do
      let(:current_account) { @controller.send(:current_account) }
      let(:current_user) { @controller.send(:current_user) }

      before do
        setup_zopim_subscription(current_account)
        get :show
      end

      it("responds with success") { assert_response :success }
    end

    describe "#update" do
      let(:current_account) { @controller.send(:current_account) }
      let(:current_user) { @controller.send(:current_user) }

      before do
        setup_zopim_subscription(current_account)
        put :update, params: { zopim_subscription: {zopim_account_id: -123} }
      end

      it "responds with success" do
        assert_response :success
      end
      it "has the correct zopim_account_id" do
        res = JSON.parse(response.body).symbolize_keys
        assert_equal -123, res[:zopim_subscription]['zopim_account_id']
      end
    end
  end
end
