require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::Account::SandboxesController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :users

  before do
    use_ssl
    accept :json
  end

  with_options(controller: "api/v2/account/sandboxes") do |request|
    request.should_route :get, '/api/v2/account/sandboxes', action: 'index'
    request.should_route :get, "/api/v2/account/sandboxes/2", action: "show", subdomain: 2
    request.should_route :post,   "/api/v2/account/sandboxes", action: "create"
    request.should_route :delete, "/api/v2/account/sandboxes/2", action: "destroy", subdomain: 2
  end

  as_an_anonymous_user do
    should_be_unauthorized :show, :create, :destroy
  end

  as_an_end_user do
    should_be_forbidden :show, :create, :destroy
  end

  as_an_agent do
    should_be_forbidden :create
    should_be_forbidden [:get, :show, subdomain: 1]
    should_be_forbidden [:delete, :destroy, subdomain: 1]
  end

  as_an_admin do
    let(:account) { accounts(:minimum) }
    let(:sb)      { accounts(:with_groups) }
    let(:sb2)     { accounts(:trial) }
    before do
      Account.any_instance.stubs(:has_sandbox?).returns(true)
    end

    describe "a GET to :index" do
      describe "when there are sandboxes" do
        before do
          @controller.stubs(:sandboxes).returns([sb, sb2])
          get :index
        end

        should_use_presenter Api::V2::Account::SandboxesPresenter, status: :ok
      end

      describe "rate limiting" do
        let(:rate_limit) { Prop.configurations[:sandboxes_indexes][:threshold] }

        before do
          Account.any_instance.stubs(:reset_sandbox).returns(accounts(:minimum))
          @controller.stubs(:current_account).returns(@account)
          Account.any_instance.stubs(:has_sandbox?).returns(true)
          Timecop.freeze do
            rate_limit.times { Prop.throttle(:sandboxes_indexes, @account.id) }
            get :index
          end
        end

        it "it rate limits" do
          assert_response 429
        end
      end
    end

    describe "a GET to :show" do
      describe "when no sandbox found" do
        before do
          account.stubs(:sandbox).returns(nil)
          get :show, params: { subdomain: 1 }
        end
        it('responds with not_found') { assert_response :not_found }
      end

      describe "when there is a sandbox" do
        before do
          @controller.stubs(:sandbox).returns(sb)
          get :show, params: { subdomain: 1 }
        end

        should_use_presenter Api::V2::Account::SandboxesPresenter, status: :ok
      end
    end

    describe "#POST" do
      before do
        Zendesk::StatsD::Client.any_instance.stubs(:increment)
      end

      describe "when account is created" do
        before do
          Account.any_instance.stubs(:has_sandbox?).returns(true)
          Account.any_instance.stubs(:spawn_sandbox).returns(sb)

          Zendesk::StatsD::Client.any_instance.expects(:increment).never
          post :create, params: { sandbox_type: 'metadata' }
        end
        should_use_presenter Api::V2::Account::SandboxesPresenter, status: :created
      end

      describe 'for each valid sandbox_type' do
        ::Accounts::Sandbox::SANDBOX_TYPES.each do |type|
          before do
            Account.any_instance.stubs(:has_sandbox?).returns(true)
            post :create, params: { sandbox_type: type }
          end

          it 'is created' do
            assert_response :created
          end
        end
      end

      describe 'when the sandbox type is not specified' do
        before do
          Account.any_instance.stubs(:has_sandbox?).returns(true)
          Account.any_instance.expects(:spawn_sandbox).with(sandbox_type: 'standard').returns(sb)
          post :create
        end

        it 'creates the sandbox with sandbox_type set to standard' do
          assert_response :created
        end
      end

      describe "when sandbox_type is not a valid option" do
        before do
          Account.any_instance.stubs(:has_sandbox?).returns(true)

          ZendeskExceptions::Logger.expects(:record).never
          Zendesk::StatsD::Client.any_instance.expects(:increment).once
          post :create, params: { sandbox_type: 'superduper' }
        end

        it "is not created" do
          assert_response :unprocessable_entity
        end
      end

      describe "when sandboxes are over the limit" do
        before do
          Account.any_instance.stubs(:has_sandbox?).returns(true)
          Account.any_instance.stubs(:sandboxes_maxed?).returns(true)

          ZendeskExceptions::Logger.expects(:record).never
          Zendesk::StatsD::Client.any_instance.expects(:increment).once
          post :create, params: { sandbox_type: 'metadata' }
        end
        it "is not created" do
          assert_response :unprocessable_entity
        end
      end

      describe "when sandbox feature is not enabled" do
        before do
          Account.any_instance.stubs(:has_sandbox?).returns(false)

          ZendeskExceptions::Logger.expects(:record).never
          Zendesk::StatsD::Client.any_instance.expects(:increment).never
          post :create, params: { sandbox_type: 'metadata' }
        end

        it "cannot create sandbox" do
          assert_response :forbidden
        end
      end

      describe "when an unexpected error occurs while creating a sandbox" do
        before do
          Account.any_instance.stubs(:has_sandbox?).returns(true)
          Account.any_instance.stubs(:add_sandbox).raises(StandardError.new('Oh no'))
        end

        it "is not created, and records the error" do
          ZendeskExceptions::Logger.expects(:record).once
          Zendesk::StatsD::Client.any_instance.expects(:increment).once

          post :create, params: { sandbox_type: 'metadata' }

          assert_response :unprocessable_entity
        end
      end
    end

    describe "a DELETE to :destroy" do
      before do
        account.expects(:destroy_sandbox)
        @controller.stubs(:current_account).returns(account)
      end

      it "responds with 204" do
        delete :destroy, params: { subdomain: 1 }
        assert_response :no_content
      end
    end
  end

  as_a_subsystem_user(account: :minimum, user: "sandbox_orchestrator") do
    before do
      Account.any_instance.stubs(:has_sandbox?).returns(true)

      sb = accounts(:with_groups)
      @controller.stubs(:sandbox).returns(sb)
    end

    should_be_authorized { get :index }
    should_be_authorized { get :show, params: { subdomain: 1 } }
    should_be_authorized { post :create, params: { sandbox_type: 'metadata' } }
    should_be_authorized { delete :destroy, params: { subdomain: 1 } }
  end
end
