require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::SurveyResponsesController do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    accept :json
  end

  with_options(controller: "api/v2/account/survey_responses") do |request|
    request.should_route :get, "/api/v2/account/survey_response", action: "show"
    request.should_route :post, "/api/v2/account/survey_response", action: "create"
    request.should_route :put, "/api/v2/account/survey_response", action: "update"
  end

  as_an_admin do
    describe "a POST to :create" do
      describe "with valid params" do
        before { post :create, params: { survey_response: { agent_count: 15 } } }

        should_use_presenter Api::V2::Account::SurveyResponsePresenter, status: :created

        should_change("the survey count", by: 1) { Account::SurveyResponse.count(:all) }

        it "creates a new survey" do
          assert_equal(15, Account::SurveyResponse.last.agent_count)
        end
      end

      describe_with_arturo_enabled :account_settings_benchmark_opt_out do
        describe "when the account is opted out of the benchmark survey" do
          before do
            @account.settings.benchmark_opt_out = true
            @account.settings.save
          end

          it "sets benchmark_opt_out setting to false and opts into the benchmark survey" do
            post :create, params: { survey_response: { agent_count: 15 } }
            refute @account.settings.benchmark_opt_out
          end

          describe "when the create_survey_response fails" do
            before do
              @account.stubs(:create_survey_response).with(agent_count: 15).raises(Exception)
            end

            it "sets the benchmark_opt_out setting to false and opts into the benchmark survey" do
              assert @account.settings.benchmark_opt_out
              assert_raises Exception do
                post :create, params: { survey_response: { agent_count: 15 } }
              end
              refute @account.settings.benchmark_opt_out
            end
          end
        end

        describe "when the account was opted into the benchmark survey" do
          before do
            @account.settings.benchmark_opt_out = false
            @account.settings.save
          end

          it "does not change the benchmark_opt_out settings and is opted into the benchmark survey" do
            post :create, params: { survey_response: { agent_count: 15 } }
            refute @account.settings.benchmark_opt_out
          end

          describe "when the create_survey_response fails" do
            before do
              @account.stubs(:create_survey_response).with(agent_count: 15).raises(Exception)
            end

            it "does not change the benchmark_opt_out settings and is opted into the benchmark survey" do
              refute @account.settings.benchmark_opt_out
              assert_raises Exception do
                post :create, params: { survey_response: { agent_count: 15 } }
              end
              refute @account.settings.benchmark_opt_out
            end
          end
        end
      end

      describe_with_arturo_disabled :account_settings_benchmark_opt_out do
        describe "when the account was opted out of the benchmark survey" do
          before do
            @account.settings.benchmark_opt_out = true
            @account.settings.save
          end

          it "does not change the benchmark_opt_out settings and is opted out of the benchmark survey" do
            post :create, params: { survey_response: { agent_count: 15 } }
            assert @account.settings.benchmark_opt_out
          end
        end

        describe "when the account was opted into the benchmark survey" do
          before do
            @account.settings.benchmark_opt_out = false
            @account.settings.save
          end

          it "does not change the benchmark_opt_out settings and is opted into the benchmark survey" do
            post :create, params: { survey_response: { agent_count: 15 } }
            refute @account.settings.benchmark_opt_out
          end
        end
      end
    end

    describe "a PUT to :update" do
      describe "with valid params and an existing survey response" do
        before do
          @account = accounts(:minimum)
          @account.create_survey_response(agent_count: 10)
          refute_difference "Account::SurveyResponse.count(:all)" do
            put :update, params: { survey_response: {agent_count: 15} }
          end
        end

        should_use_presenter Api::V2::Account::SurveyResponsePresenter, status: :ok

        it "updates the survey" do
          assert_equal(15, @account.reload.survey_response.agent_count)
        end
      end
    end

    describe "a DELETE to :destroy" do
      let(:account) { accounts(:minimum) }

      before do
        account.survey_response.try(:destroy)
        account.create_survey_response(agent_count: 10)
      end

      it "deletes the survey" do
        account.reload
        account.survey_response.wont_be_nil

        delete :destroy

        account.reload
        account.survey_response.must_be_nil
      end

      describe_with_arturo_enabled :account_settings_benchmark_opt_out do
        describe "when the account was opted into the benchmark survey" do
          before do
            account.settings.benchmark_opt_out = false
            account.settings.save
          end

          it "sets benchmark_opt_out setting to true and is opted out of the benchmark survey" do
            delete :destroy
            account.reload
            assert account.settings.benchmark_opt_out
          end

          describe "when survey response doesn't exist" do
            before do
              account.survey_response.try(:destroy)
            end

            it "sets benchmark_opt_out setting to true and is opted out of the benchmark survey" do
              delete :destroy
              account.reload
              assert account.settings.benchmark_opt_out
            end
          end

          describe "when the survey_response delete fails" do
            before do
              account.survey_response.stubs(:destroy).returns(false)
            end

            it "sets benchmark_opt_out setting to true and is opted out of the benchmark survey" do
              delete :destroy
              account.reload
              assert account.settings.benchmark_opt_out
            end
          end
        end

        describe "when the account was opted out of the benchmark survey" do
          before do
            account.settings.benchmark_opt_out = true
            account.settings.save
          end

          it "does not change the benchmark_opt_out settings and is opted out of the benchmark survey" do
            delete :destroy
            account.reload
            assert account.settings.benchmark_opt_out
          end

          describe "when survey response doesn't exist" do
            before do
              account.survey_response.try(:destroy)
            end

            it "does not change the benchmark_opt_out settings and is opted out of the benchmark survey" do
              delete :destroy
              account.reload
              assert account.settings.benchmark_opt_out
            end
          end

          describe "when the survey_response delete fails" do
            before do
              account.survey_response.stubs(:destroy).returns(false)
            end

            it "does not change the benchmark_opt_out settings and is opted out of the benchmark survey" do
              delete :destroy
              account.reload
              assert account.settings.benchmark_opt_out
            end
          end
        end
      end

      describe_with_arturo_disabled :account_settings_benchmark_opt_out do
        describe "when the account was opted into the benchmark survey" do
          before do
            account.settings.benchmark_opt_out = false
            account.settings.save
          end

          it "does not change the benchmark_opt_out settings and is opted into the benchmark survey" do
            delete :destroy
            account.reload
            refute(account.settings.benchmark_opt_out)
          end
        end

        describe "when the account was opted out of the benchmark survey" do
          before do
            account.settings.benchmark_opt_out = true
            account.settings.save
          end

          it "does not change the benchmark_opt_out settings and is opted out of the benchmark survey" do
            delete :destroy
            account.reload
            assert account.settings.benchmark_opt_out
          end
        end
      end
    end
  end

  as_an_agent do
    describe "a GET to :show" do
      describe "without existing survey" do
        before { get :show }
        it('responds with no_content') { assert_response :no_content }
      end

      describe "with existing (stubbed) survey" do
        before do
          @account = accounts(:minimum)
          @account.create_survey_response(id: 1)
          get :show
        end

        should_use_presenter Api::V2::Account::SurveyResponsePresenter, status: :ok
      end
    end

    should_be_forbidden :create, :update, :destroy
  end

  as_an_anonymous_user do
    should_be_unauthorized :show, :create, :update, :destroy
  end

  as_an_end_user do
    should_be_forbidden :show, :create, :update, :destroy
  end
end
