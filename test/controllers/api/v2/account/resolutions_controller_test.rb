require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::ResolutionsController do
  fixtures :accounts

  before do
    accept :json
  end

  with_options(controller: "api/v2/account/resolutions") do |request|
    request.should_route :get, "/api/v2/account/resolve", action: "show"
  end

  as_an_anonymous_user do
    before do
      @request.env.delete("HTTPS")
      @request.stubs(:ssl?).returns(true)
      @request.account = accounts(:minimum)
    end

    describe "a GET to :show without SSL" do
      before { get :show }
      it('responds with success') { assert_response :success }
    end

    describe "a GET to :show" do
      before do
        get :show
      end

      it('responds with success') { assert_response :success }

      it "returns subdomain" do
        assert_equal accounts(:minimum).subdomain, JSON.parse(@response.body)["subdomain"]
      end

      it "returns url" do
        assert_equal accounts(:minimum).url, JSON.parse(@response.body)["url"]
      end

      describe "for a host-mapped account" do
        before do
          @request.account.host_mapping = "example.com"
        end

        it "does not return host-mapped url" do
          assert_equal accounts(:minimum).url(mapped: false), JSON.parse(@response.body)["url"]
        end
      end
    end
  end
end
