require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::ExploreSubscriptionController do
  extend Api::V2::TestHelper

  before do
    stub_account_service_get
    accept :json
  end

  with_options(controller: 'api/v2/account/explore_subscription') do |request|
    request.should_route :delete, '/api/v2/account/explore_subscription', action: 'destroy'
  end

  as_an_anonymous_user do
    should_be_unauthorized :destroy
  end

  as_an_end_user do
    should_be_forbidden :destroy
  end

  as_an_owner do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user=, current_account.owner) }

    describe "#destroy" do
      # let(:subscription_deactivator) { stub }
      let(:explore_subscription) { stub(subscribed?: true) }

      before do
        # ZBC::Explore::SubscriptionDeactivator.stubs(:new).
        #   with(explore_subscription).
        #   returns(subscription_deactivator)
        current_account.stubs(explore_subscription: explore_subscription)
      end

      it "deactivates the explore subscription" do
        ZBC::Explore::SubscriptionDeactivator.expects(:deactivate!).
          with(explore_subscription)
        delete :destroy
      end

      describe "when the current user is not a verified user" do
        before do
          current_user.stubs(is_verified?: false)
          delete :destroy
        end

        it('responds with unauthorized') { assert_response :unauthorized }
      end

      describe "when the current user is not the account owner" do
        before do
          current_user.stubs(is_account_owner?: false)
          delete :destroy
        end

        it('responds with unauthorized') { assert_response :unauthorized }
      end

      describe "when there is no explore subscription" do
        before do
          current_account.stubs(:explore_subscription)
          delete :destroy
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end

      describe "when the explore subscription is not subscribed/active" do
        before do
          explore_subscription.stubs(subscribed?: false)
          delete :destroy
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end
  end
end
