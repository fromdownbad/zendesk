require_relative '../../../../support/test_helper'
require_relative '../../../../support/api_scopes_helper'

SingleCov.covered! uncovered: 1

describe Api::V2::Account::SettingsController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper

  fixtures :accounts,
    :users,
    :signatures,
    :translation_locales

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:products).returns([])
    Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
    Apps::ChatAppInstallation.any_instance.stubs(:handle_disable_installation)
    accept :json
  end

  with_options(controller: 'api/v2/account/settings') do |request|
    request.should_route :get, '/api/v2/account/settings', action: 'show'
  end

  describe "embeddable subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "embeddable") do
      should_be_forbidden :update

      describe "a GET to :show" do
        before { get :show }
        it("responds with success") { assert_response :success }
      end
    end
  end

  describe "pigeon subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "pigeon") do
      should_be_forbidden :update

      describe "a GET to :show" do
        before { get :show }
        it("responds with success") { assert_response :success }
      end
    end
  end

  describe 'mobile_sdk_api subsystem user' do
    as_a_subsystem_user(account: :minimum, user: 'mobile_sdk_api') do
      should_be_forbidden :update

      describe 'a GET to show' do
        before { get :show }

        it('responds with success') { assert_response :success }
      end
    end
  end

  describe "knowledge_api subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "knowledge_api") do
      should_be_forbidden :update

      describe "a GET to :show" do
        before { get :show }
        it("responds with success") { assert_response :success }
      end
    end
  end

  as_an_agent do
    describe 'a GET to :show' do
      before { get :show }

      should_use_presenter Api::V2::Account::SettingsPresenter

      describe "app requests" do
        before do
          request.headers.merge! "X-Zendesk-App-Id" => "3vil4pp3p1cHax"
        end

        describe_with_arturo_enabled :account_settings_cache_control_header do
          before { get :show }

          it 'has cache headers' do
            assert_equal 'max-age=60, private', @response.headers['Cache-Control']
          end
        end

        describe_with_arturo_disabled :account_settings_cache_control_header do
          before { get :show }

          it 'has no cache headers' do
            assert_nil @response.headers['Cache-Control']
          end
        end
      end

      describe "not an app request" do
        describe_with_arturo_enabled :account_settings_cache_control_header do
          before { get :show }

          it 'has cache headers' do
            assert_nil @response.headers['Cache-Control']
          end
        end
      end
    end

    describe 'a PUT to update' do
      should_be_forbidden [:put, :update, {}]
    end

    describe 'json response' do
      let(:expected) do
        [
          'https://edgecast.example.com',
          'https://cloudfront.example.com'
        ]
      end

      before do
        get :show

        json = JSON.parse(response.body)
        @urls = json['settings']['cdn']['hosts'].map { |h| h['url'] }
      end

      it 'contains expected https urls' do
        assert_equal @urls, expected
      end
    end
  end

  as_an_admin do
    describe 'successful update' do
      describe 'of a setting' do
        before { put :update, params: { settings: {tickets: {agent_ticket_deletion: true }} } }

        should_change('a setting') { @request.account.settings.ticket_delete_for_agents }

        should_use_presenter Api::V2::Account::SettingsPresenter
      end

      describe 'of boolean account settings' do
        [
          [:active_features, :user_tagging, 'has_user_tags'],
          [:active_features, :ticket_tagging, 'ticket_tagging'],
          [:active_features, :allow_ccs, 'collaboration_enabled'],
          [:brands, :require_brand_on_new_tickets, 'require_brand_on_new_tickets'],
          [:groups, :check_group_name_uniqueness, 'check_group_name_uniqueness'],
          [:tickets, :list_newest_comments_first, 'events_reverse_order'],
          [:tickets, :private_attachments, 'private_attachments'],
          [:tickets, :email_attachments, 'email_attachments'],
          [:tickets, :list_empty_views, 'ticket_show_empty_views'],
          [:tickets, :tagging, 'ticket_tagging'],
          [:tickets, :markdown_ticket_comments, 'markdown_ticket_comments'],
          [:tickets, :emoji_autocompletion, 'emoji_autocompletion'],
          [:user, :tagging, 'has_user_tags'],
          [:user, :have_gravatars_enabled, 'have_gravatars_enabled'],
          [:rule, :macro_most_used, 'macro_most_used']
        ].each do |feature_group, feature_id, setting_name|
          describe "of #{feature_group}.#{feature_id} setting" do
            [true, false].each do |setting_value|
              describe "set to #{setting_value}" do
                before do
                  put :update, params: { settings: {
                    feature_group => {feature_id => setting_value}
                  } }
                end

                it "sets #{setting_name} to #{setting_value}" do
                  assert_equal setting_value, ActiveRecord::Type::Boolean.new.cast(@request.account.settings.send(setting_name))
                end
              end
            end
          end
        end
      end

      describe 'of boolean account attributes' do
        [
          [:tickets, :comments_public_by_default, 'is_comment_public_by_default'],
          [:tickets, :is_first_comment_private_enabled, 'is_first_comment_private_enabled'],
          [:user, :agent_created_welcome_emails, 'is_welcome_email_when_agent_register_enabled']
        ].each do |feature_group, feature_id, attribute_name|
          describe "of #{feature_group}.#{feature_id} setting" do
            [true, false].each do |attribute_value|
              describe "set to #{attribute_value}" do
                before do
                  put :update, params: { settings: {
                    feature_group => {feature_id => attribute_value}
                  } }
                end

                it "sets #{attribute_name} to #{attribute_value}" do
                  value = @request.account.send(attribute_name)
                  actual = ActiveRecord::Type::Boolean.new.cast(value)
                  assert_equal attribute_value, actual
                end
              end
            end
          end
        end
      end

      [:header_color, :page_background_color, :tab_background_color, :text_color].each do |feature_id|
        describe " of branding #{feature_id}" do
          before do
            @test_color = 'deadbe'
            put :update, params: { settings: {
              branding: {feature_id => @test_color}
            } }
          end

          it "sets #{feature_id}" do
            assert_equal @test_color, @request.account.branding.send(feature_id)
          end
        end
      end

      describe 'of default_brand_id' do
        before do
          @brand = FactoryBot.create(:brand)

          put :update, params: { settings: {brands: {default_brand_id: @brand.id}} }
        end

        it 'sets the new default_brand_id' do
          assert_equal @brand.id, @request.account.settings.default_brand_id
        end

        should_use_presenter Api::V2::Account::SettingsPresenter
      end

      describe 'of customer satisfaction setting' do
        before do
          put :update, params: { settings: {
            active_features: {customer_satisfaction: true}
          } }
        end

        it 'turns on the customer satisfaction feature' do
          assert_predicate @request.account.settings, :customer_satisfaction
        end

        it 'sets changed_customer_satisfaction to true' do
          assert_predicate @request.account, :changed_customer_satisfaction
        end

        should_use_presenter Api::V2::Account::SettingsPresenter
      end

      describe 'of an account property set setting' do
        before do
          put :update, params: { settings: {
            ticket_form: {
              ticket_forms_instructions: 'Please choose below wombat'
            }
          } }
        end

        should_change('an account property set setting') do
          @request.account.ticket_forms_instructions
        end

        should_use_presenter Api::V2::Account::SettingsPresenter
      end

      describe 'of an account property set setting and a setting' do
        before do
          put :update, params: { settings: {
            ticket_form: {
              ticket_forms_instructions: 'Please choose below wombat'
            }
          } }
        end

        should_change('an account property set setting') do
          @request.account.ticket_forms_instructions
        end

        should_use_presenter Api::V2::Account::SettingsPresenter
      end

      describe 'of `macro_order` account setting' do
        before { put :update, params: { settings: {rule: {macro_order: new_value}} } }

        %w[alphabetical position].each do |valid_value|
          describe 'when the new value is valid' do
            let(:new_value) { valid_value }

            it 'updates the setting' do
              assert_equal new_value, @request.account.settings.macro_order
            end
          end
        end

        describe 'when the new value is invalid' do
          let(:new_value) { 'foobar' }

          it 'does not update the setting' do
            assert_equal 'alphabetical', @request.account.settings.macro_order
          end
        end
      end

      describe 'of `onboarding_segments` account setting' do
        describe 'when new value is valid' do
          let(:new_value) { 'b2b' }
          before { put :update, params: { settings: {onboarding: {onboarding_segments: new_value}} } }

          it 'updates the setting' do
            assert_equal new_value, @request.account.settings.onboarding_segments
          end
        end

        describe 'when new value is invalid' do
          let(:new_value) { 'ccc' }
          before { put :update, params: { settings: {onboarding: {onboarding_segments: new_value}} } }

          it 'does not update the setting' do
            refute_includes @request.account.settings.onboarding_segments, new_value
          end
        end
      end

      describe 'email ccs settings' do
        def perform_request
          put :update, params: { settings: {
            tickets: {
              accepted_new_collaboration_tos: true,
              agent_can_change_requester: true,
              agent_email_ccs_become_followers: false,
              auto_updated_ccs_followers_rules: true,
              comment_email_ccs_allowed: true,
              light_agent_email_ccs_allowed: true,
              follower_and_email_cc_collaborations: true,
              ticket_followers_allowed: true
            }
          } }
        end

        before do
          @account.settings.accepted_new_collaboration_tos = false
          @account.settings.agent_can_change_requester = false
          @account.settings.agent_email_ccs_become_followers = true
          @account.settings.auto_updated_ccs_followers_rules = false
          @account.settings.comment_email_ccs_allowed = false
          @account.settings.light_agent_email_ccs_allowed = false
          @account.settings.follower_and_email_cc_collaborations = false
          @account.settings.ticket_followers_allowed = false
          @account.settings.save!
        end

        describe_with_arturo_disabled :email_ccs do
          it 'does not update email ccs related settings' do
            perform_request

            refute_predicate @request.account.settings, :accepted_new_collaboration_tos
            refute_predicate @request.account.settings, :agent_can_change_requester
            assert_predicate @request.account.settings, :agent_email_ccs_become_followers
            refute_predicate @request.account.settings, :auto_updated_ccs_followers_rules
            refute_predicate @request.account.settings, :comment_email_ccs_allowed
            refute_predicate @request.account.settings, :light_agent_email_ccs_allowed
            refute_predicate @request.account.settings, :follower_and_email_cc_collaborations
            refute_predicate @request.account.settings, :ticket_followers_allowed
          end
        end

        describe_with_arturo_enabled :email_ccs do
          it 'updates email ccs related settings' do
            perform_request

            assert_predicate @request.account.settings, :accepted_new_collaboration_tos
            assert_predicate @request.account.settings, :agent_can_change_requester
            refute_predicate @request.account.settings, :agent_email_ccs_become_followers
            assert_predicate @request.account.settings, :auto_updated_ccs_followers_rules
            assert_predicate @request.account.settings, :comment_email_ccs_allowed
            assert_predicate @request.account.settings, :light_agent_email_ccs_allowed
            assert_predicate @request.account.settings, :follower_and_email_cc_collaborations
            assert_predicate @request.account.settings, :ticket_followers_allowed
          end

          describe_with_arturo_disabled :email_ccs_light_agents_v2 do
            it 'does not update the `light_agent_email_ccs_allowed` setting' do
              perform_request

              refute_predicate @request.account.settings, :light_agent_email_ccs_allowed
            end
          end
        end
      end

      describe '`skill_based_filtered_views` account setting' do
        describe 'when new value is an integer' do
          let(:new_value) { 123 }
          before { put :update, params: { settings: {rule: {skill_based_filtered_views: new_value}} } }

          it 'updates the setting' do
            assert_equal [new_value], @request.account.settings.skill_based_filtered_views
          end
        end

        describe 'when new value is an array of one integer' do
          let(:new_value) { [123] }
          before { put :update, params: { settings: {rule: {skill_based_filtered_views: new_value}} } }

          it 'updates the setting' do
            assert_equal new_value, @request.account.settings.skill_based_filtered_views
          end
        end

        describe 'when new value is invalid' do
          let(:new_value) { [123, 456] }
          before { put :update, params: { settings: {rule: {skill_based_filtered_views: new_value}} } }

          it 'does not update the setting' do
            assert_empty @request.account.settings.skill_based_filtered_views
          end
        end
      end

      describe '`edit_ticket_skills_permission` account setting' do
        Zendesk::Accounts::SettingsControllerSupport::MANAGE_TICKET_SKILLS_PERMISSION.each do |_, new_value|
          describe 'when new value is a valid integer' do
            before { put :update, params: { settings: {tickets: {edit_ticket_skills_permission: new_value}} } }

            it 'updates the setting' do
              assert_equal new_value, @request.account.settings.edit_ticket_skills_permission
            end
          end
        end

        describe 'when new value is an invalid integer' do
          let(:new_value) { 4 }
          before { put :update, params: { settings: {tickets: {edit_ticket_skills_permission: new_value}} } }

          it 'updates the setting to default' do
            assert_equal(
              Zendesk::Accounts::SettingsControllerSupport::MANAGE_TICKET_SKILLS_PERMISSION[:EDITABLE_BY_ADMINS],
              @request.account.settings.edit_ticket_skills_permission
            )
          end
        end
      end

      describe 'of `automatic_answers_threshold` account setting' do
        describe 'when new value is valid' do
          let(:new_value) { 'aggressive' }
          before { put :update, params: { settings: {automatic_answers: {threshold: new_value}} } }

          it 'updates the setting' do
            assert_equal Zendesk::Types::PredictionThresholdType.Aggressive, @request.account.settings.automatic_answers_threshold
          end
        end

        describe 'when new value is invalid' do
          let(:new_value) { 'dodgyvalue' }
          before { put :update, params: { settings: {automatic_answers: {threshold: new_value}} } }

          it 'does not update the setting' do
            assert_equal Zendesk::Types::PredictionThresholdType.Balanced, @request.account.settings.automatic_answers_threshold
          end
        end
      end

      describe 'of `locale_ids` setting' do
        describe 'when new value is valid' do
          let(:new_value) { [translation_locales(:brazilian_portuguese).id] }
          before { put :update, params: { settings: {localization: {locale_ids: new_value}} } }

          it 'updates the setting' do
            assert_equal new_value, @request.account.allowed_translation_locale_ids
          end
        end

        describe 'when new value is invalid' do
          let(:new_value) { 'notanarray' }
          before do
            @previous_value = @request.account.allowed_translation_locale_ids
            put :update, params: { settings: {localization: {locale_ids: new_value}} }
          end

          it 'does not update the setting' do
            assert_equal @previous_value, @request.account.allowed_translation_locale_ids
          end
        end
      end

      describe 'of `agent_workspace` account setting' do
        before do
          @account.settings.check_group_name_uniqueness = check_group_name_uniqueness
          @account.settings.save!
          put :update, params: { settings: {agents: {agent_workspace: true}} }
        end

        describe 'groups & departments has been migrated' do
          let(:check_group_name_uniqueness) { true }

          it 'updates the setting' do
            assert_predicate @request.account.settings, :polaris
          end
        end

        describe 'groups & departments has NOT been migrated' do
          let(:check_group_name_uniqueness) { false }

          it 'does not update the setting' do
            refute_predicate @request.account.settings, :polaris
          end
        end
      end

      describe 'of allowed parameters' do
        before do
          put :update, params: { settings: {
            tickets: { agent_ticket_deletion: true },
            brands: { default_brand_id: FactoryBot.create(:brand).id },
            ticket_form: {
              ticket_forms_instructions: 'Select all the things'
            },
            active_features: {
              business_hours:        true,
              customer_satisfaction: true,
              automatic_answers:     true,
            },
            rule: {skill_based_filtered_views: [123]},
            user: {
              end_user_phone_number_validation: true
            },
            branding: { header_color: '000000' },
            api: {
              accepted_api_agreement: true,
              api_token_access:       true,
              api_password_access:    true
            },
            automatic_answers: {
              threshold: 'conservative'
            },
          } }
        end

        should_change('automatic_answers_threshold', to: Zendesk::Types::PredictionThresholdType.Conservative) do
          @request.account.settings.automatic_answers_threshold
        end

        should_change('skill_based_filtered_views') do
          @request.account.settings.skill_based_filtered_views
        end

        should_change('ticket_delete_for_agents') do
          @request.account.settings.ticket_delete_for_agents?
        end

        should_change('default_brand_id') do
          @request.account.settings.default_brand_id
        end

        should_change('business_hours') do
          @request.account.settings.business_hours
        end

        should_change('automatic_answers') do
          @request.account.settings.business_hours
        end

        should_change('customer_satisfaction') do
          @request.account.settings.customer_satisfaction
        end

        should_change('ticket_form') do
          @request.account.ticket_forms_instructions
        end

        should_change('header_color') do
          @request.account.branding.header_color
        end

        should_change('accepted_api_agreement') do
          @request.account.settings.accepted_api_agreement
        end

        should_change('api_token_access') do
          @request.account.settings.api_token_access
        end

        should_change('api_password_access') do
          @request.account.settings.api_password_access
        end

        should_change('end_user_phone_number_validation') do
          @request.account.settings.end_user_phone_number_validation
        end

        should_use_presenter Api::V2::Account::SettingsPresenter
      end
    end

    describe 'of unsupported params' do
      it 'does not update with unsupported params' do
        put :update, params: { settings: {chat: {welcome_message: 'XXX'}} }

        assert_response :bad_request
      end

      it 'does not update header_color if not associated with branding' do
        put :update, params: { settings: {other: {header_color: 'FFFFFF'}} }

        assert_response :bad_request
      end

      it 'does not update accepted_api_agreement to false' do
        put :update, params: { settings: {api: {accepted_api_agreement: false}} }

        assert_response :bad_request
      end
    end
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    should_be_authorized do
      put :update, params: { settings: {branding: {header_color: '000000'}} }
    end

    should_be_authorized { get :show }

    describe 'a GET to :show' do
      before { get :show }

      it('responds with success') { assert_response :success }
    end

    describe 'a PUT to :update' do
      before { put :update, params: { settings: {branding: {header_color: '000000'}} } }

      it('responds with success') { assert_response :success }
    end
  end
end
