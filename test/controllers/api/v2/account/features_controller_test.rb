require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::FeaturesController do
  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    accept :json
  end

  with_options(controller: 'api/v2/account/features') do |request|
    request.should_route :get, '/api/v2/account/features', action: 'show'
    request.should_route :put, '/api/v2/account/features', action: 'update'
    request.should_route :get, '/api/v2/account/features/f123/status', action: 'status', feature_id: 'f123'
  end

  as_an_anonymous_user do
    should_be_unauthorized :show
    should_be_unauthorized :update
  end

  as_a_subsystem_user(user: 'mobile_sdk_api', account: :minimum) do
    should_be_forbidden :update

    it '#show responds with success' do
      get :show
      assert_response :success
    end
  end

  as_an_agent do
    let(:current_account) { @controller.send(:current_account) }

    describe "#show" do
      let(:features) do
        {
          the_most_amazing_feature: {
            enabled: true
          },
          the_other_feature: {
            enabled: false
          },
        }
      end

      describe 'not on a multiproduct account' do
        before do
          refute current_account.multiproduct?
        end

        describe 'on an active Support account' do
          before do
            Api::V2::Account::FeaturesPresenter.any_instance.stubs(model_json: features)
            get :show
          end

          it('responds with success') { assert_response :success }
          should_use_presenter Api::V2::Account::FeaturesPresenter

          it "renders the account's features details" do
            actual = JSON.parse(response.body).deep_symbolize_keys
            assert_equal features, actual[:features]
          end
        end

        describe 'on an inactive Support account' do
          before do
            current_account.update_attributes!(is_active: false)
            refute current_account.is_active?

            Api::V2::Account::FeaturesPresenter.any_instance.stubs(model_json: features)
          end

          it('responds with a 404') do
            get :show
            assert_response :not_found
          end
        end
      end

      describe 'on a multiproduct account' do
        let(:chat_product) { Zendesk::Accounts::Product.new('name' => 'chat', 'state' => Zendesk::Accounts::Product::TRIAL, 'plan_settings' => { 'phase' => 4 }) }

        before do
          Account.any_instance.stubs(:multiproduct?).returns(true)

          stub_request(:get, "https://#{current_account.subdomain}.zendesk-test.com/api/services/accounts/#{current_account.id}/products").
            to_return(body: { products: [support_product, chat_product].compact }.to_json, headers: { content_type: 'application/json; charset=utf-8' })
          stub_request(:get, "https://#{current_account.subdomain}.zendesk-test.com/api/services/accounts/#{current_account.id}/products/support").
            to_return(status: 200, body: { product: support_product }.to_json, headers: { content_type: 'application/json; charset=utf-8' })
        end

        [true, false].each do |support_active|
          describe "with Support #{support_active ? 'active' : 'inactive'}" do
            let(:support_product) { support_active ? FactoryBot.build(:support_subscribed_product) : FactoryBot.build(:support_inactive_product) }

            before do
              current_account.update_attributes!(is_active: support_active)
              assert_equal support_active, current_account.is_active?
              Api::V2::Account::FeaturesPresenter.any_instance.stubs(model_json: features)
              get :show
            end

            it('responds with success') { assert_response :success }

            should_use_presenter Api::V2::Account::FeaturesPresenter

            it "renders the account's features details" do
              actual = JSON.parse(response.body).deep_symbolize_keys
              assert_equal features, actual[:features]
            end
          end
        end

        describe "without a subscription" do
          let(:support_product) { FactoryBot.build(:support_inactive_product) }

          before do
            current_account.subscription.destroy

            get :show
          end

          it('responds with success') { assert_response :success }
        end
      end
    end

    describe '#status' do
      it 'sets enabled to true for enabled feature' do
        feature_id = 'f1'
        Arturo.enable_feature! feature_id
        get 'status', params: { feature_id: feature_id }
        assert(JSON.parse(response.body)['enabled'])
      end

      it 'sets enabled to false for not enabled feature' do
        feature_id = 'f1'
        get 'status', params: { feature_id: feature_id }
        assert(JSON.parse(response.body).key?('enabled'))
        refute(JSON.parse(response.body)['enabled'])
      end
    end
  end

  as_an_admin do
    let(:current_account) { @controller.send(:current_account) }
    let(:anti_subdomain) { "-#{current_account.subdomain}" }

    describe "#update" do
      before do
        @feature = Arturo::Feature.create!(symbol: :squelch_kittens)
        @feature.update_column(:phase, "external_beta")
        @feature_ga = Arturo::Feature.create!(symbol: :brush_llamas)
        @feature_ga.update_column(:phase, "on")
      end

      describe "success for non GA" do
        before do
          refute @feature.enabled_for?(current_account)
          put :update, params: { feature: "squelch_kittens", enabled: true }
        end

        it('responds with success') { assert_response :success }

        it "should contain confirmation" do
          assert JSON.parse(response.body)["enabled"]
        end

        it "does not contain an error" do
          refute JSON.parse(response.body)["error"]
        end

        it "enables the feature for the account" do
          assert @feature.reload.enabled_for?(current_account)
        end

        it "adds the subdomain to the feature" do
          assert @feature.reload.external_beta_subdomains.include?(current_account.subdomain)
        end

        it "disables a feature" do
          put :update, params: { feature: "squelch_kittens", enabled: false }
          assert_response :success
          refute @feature.reload.enabled_for?(current_account)
          refute @feature.reload.external_beta_subdomains.include?(current_account.subdomain)
        end

        it "excludes a feature" do
          put :update, params: { feature: "squelch_kittens", exclude: true }
          assert_response :success
          assert @feature.reload.external_beta_subdomains.include?(anti_subdomain)
          refute @feature.reload.external_beta_subdomains.include?(current_account.subdomain)
        end
      end

      describe "should include if enable after exclude subdomain from non ga feature" do
        before do
          put :update, params: { feature: "squelch_kittens", exclude: true }
          assert @feature.reload.external_beta_subdomains.include?(anti_subdomain)
          put :update, params: { feature: "squelch_kittens", enabled: true }
        end

        it('responds with success') { assert_response :success }

        it "should contain confirmation" do
          assert JSON.parse(response.body)["enabled"]
        end

        it "does not contain an error" do
          refute JSON.parse(response.body)["error"]
        end

        it "the account should be enabled once again" do
          assert @feature.reload.enabled_for?(current_account)
        end

        it "does not include ~subdomain to the feature" do
          refute @feature.reload.external_beta_subdomains.include?(anti_subdomain)
        end

        it "adds subdomain to the feature" do
          assert @feature.reload.external_beta_subdomains.include?(current_account.subdomain)
        end
      end

      describe "#exclude from ga feature" do
        before do
          put :update, params: { feature: "brush_llamas", exclude: true }
        end

        it('responds with success') { assert_response :success }

        it "should contain confirmation" do
          refute JSON.parse(response.body)["enabled"]
        end

        it "does not contain an error" do
          refute JSON.parse(response.body)["error"]
        end

        it "should exclude the account from a feature" do
          refute @feature_ga.reload.enabled_for?(current_account)
        end

        it "adds ~subdomain to the feature" do
          assert @feature_ga.reload.external_beta_subdomains.include?(anti_subdomain)
        end

        it "removes exclusion from a feature" do
          put :update, params: { feature: "brush_llamas", exclude: false }
          assert JSON.parse(response.body)["enabled"]
          assert @feature_ga.reload.enabled_for?(current_account)
          refute @feature_ga.reload.external_beta_subdomains.include?(anti_subdomain)
        end

        it "enables a feature but does not add to beta list" do
          put :update, params: { feature: "brush_llamas", enabled: true }
          assert JSON.parse(response.body)["enabled"]
          assert @feature_ga.reload.enabled_for?(current_account)
          refute @feature_ga.reload.external_beta_subdomains.include?(current_account.subdomain)
        end
      end

      describe "should error if enable and exclude at the same time" do
        before do
          put :update, params: { feature: "brush_llamas", exclude: true, enabled: true }
        end

        it('responds with bad_request') { assert_response :bad_request }

        it "should contain error" do
          assert JSON.parse(response.body)["error"].present?
        end
      end

      describe "disable feature" do
        before do
          put :update, params: { feature: "squelch_kittens", enabled: true }
          assert @feature.reload.enabled_for?(current_account)

          put :update, params: { feature: "squelch_kittens", enabled: false }
        end

        it('responds with success') { assert_response :success }

        it "should contain confirmation" do
          refute JSON.parse(response.body)["enabled"]
        end

        it "does not contain an error" do
          refute JSON.parse(response.body)["error"]
        end

        it "disables the feature for the account" do
          refute @feature.reload.enabled_for?(current_account)
        end

        it "removes the subdomain to the feature" do
          refute @feature.reload.external_beta_subdomains.include?(current_account.subdomain)
        end
      end

      describe "failure" do
        describe "unknown feature" do
          before do
            put :update, params: { feature: "unknown_feature", enabled: true }
          end

          it('responds with not_found') { assert_response :not_found }

          it "should contain error" do
            assert JSON.parse(response.body)["error"].present?
          end
        end

        describe "feature in wrong phase" do
          before do
            @feature.update_column(:phase, "on")
            put :update, params: { feature: "squelch_kittens", enabled: true }
          end

          it('responds with bad_request') { assert_response :bad_request }

          it "should contain error" do
            assert JSON.parse(response.body)["error"].present?
          end
        end
      end

      describe "production" do
        before do
          Api::V2::Account::FeaturesController.any_instance.expects(:is_production?).at_least(1).returns(true)
          put :update, params: { feature: "squelch_kittens", enabled: true }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end
    end
  end
end
