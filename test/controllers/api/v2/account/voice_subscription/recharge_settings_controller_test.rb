require_relative "../../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::VoiceSubscription::RechargeSettingsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users

  before do
    accept :json
    Account.any_instance.stubs(:zuora_subscription).returns(stub)
  end

  with_options(controller: 'api/v2/account/voice_subscription/recharge_settings') do |request|
    request.should_route :get,  '/api/v2/account/voice_subscription/recharge_settings', action: 'show'
    request.should_route :put,  '/api/v2/account/voice_subscription/recharge_settings', action: 'update'
    request.should_route :post, '/api/v2/account/voice_subscription/recharge_settings', action: 'create'
  end

  as_an_anonymous_user do
    should_be_unauthorized :show, :update, :create
  end

  as_an_agent do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }

    describe "a PUT to update" do
      should_be_forbidden [:put, :update, {}]
    end

    describe "a POST to create" do
      should_be_forbidden [:post, :create, {}]
    end

    describe "#show" do
      describe "when recharge_settings are present" do
        let(:voice_recharge_settings) { current_account.voice_recharge_settings }

        before do
          current_account.create_voice_recharge_settings(
            enabled: true,
            minimum_balance: 5.00,
            amount: 50.00
          )
          get :show
        end

        it('responds with success') { assert_response :success }
        should_use_presenter Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter

        it "renders voice recharge settings details" do
          actual   = JSON.parse(response.body).deep_symbolize_keys
          expected = {
            enabled: true,
            minimum_balance: 5.00,
            amount: 50.00,
            balance: 0.00,
            payment_status: nil
          }
          assert_equal expected, actual[:recharge_settings]
        end
      end

      describe "when no recharge settings" do
        before { get :show }

        it('responds with success') { assert_response :success }
        should_use_presenter Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter

        it "renders blank recharge settings details" do
          actual = JSON.parse(response.body).deep_symbolize_keys
          assert_nil actual[:recharge_settings]
        end
      end

      describe 'when trial subscription' do
        before do
          Account.any_instance.stubs(:zuora_subscription)
          get :show
        end

        it('returns not_found error') { assert_response :not_found }

        it "renders error message" do
          actual   = JSON.parse(response.body).symbolize_keys
          expected = {
            domain: 'Voice',
            error:  'Recharge not supported on trial'
          }
          assert_equal expected, actual
        end
      end
    end
  end

  as_an_admin do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }

    describe "a PUT to update" do
      should_be_forbidden [:put, :update, {}]
    end

    describe "a POST to create" do
      should_be_forbidden [:post, :create, {}]
    end

    describe "#show" do
      describe "when recharge_settings are present" do
        let(:voice_recharge_settings) { current_account.voice_recharge_settings }

        before do
          current_account.create_voice_recharge_settings(
            enabled: true,
            minimum_balance: 5.00,
            amount: 50.00
          )
          get :show
        end

        it('responds with success') { assert_response :success }
        should_use_presenter Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter

        it "renders voice recharge settings details" do
          actual   = JSON.parse(response.body).deep_symbolize_keys
          expected = {
            enabled: true,
            minimum_balance: 5.00,
            amount: 50.00,
            balance: 0.00,
            payment_status: nil
          }
          assert_equal expected, actual[:recharge_settings]
        end
      end

      describe "when no recharge settings" do
        before { get :show }

        it('responds with success') { assert_response :success }
        should_use_presenter Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter

        it "renders blank recharge settings details" do
          actual = JSON.parse(response.body).deep_symbolize_keys
          assert_nil actual[:recharge_settings]
        end
      end

      describe 'when trial subscription' do
        before do
          Account.any_instance.stubs(:zuora_subscription)
          get :show
        end

        it('returns not_found error') { assert_response :not_found }

        it "renders error message" do
          actual   = JSON.parse(response.body).symbolize_keys
          expected = {
            domain: 'Voice',
            error:  'Recharge not supported on trial'
          }
          assert_equal expected, actual
        end
      end
    end
  end

  as_an_owner do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user=, current_account.owner) }

    let(:client) { stub(update_recharge_settings: true) }

    let(:synchronizer) { ZendeskBillingCore::Zuora::Synchronizer.new(nil) }

    before do
      Account.any_instance.stubs(:zuora_subscription).returns(stub(zuora_account_id: nil, client: client))
      synchronizer.stubs(zuora_account: stub)
      synchronizer.stubs(:zendesk_account).returns(current_account)
      ZendeskBillingCore::Zuora::Synchronizer.stubs(:new).returns(synchronizer)
    end

    describe "#update" do
      describe "when account does not have voice recharge settings" do
        before { put :update, params: { recharge_settings: { enabled: false } } }

        it('returns not_found error') { assert_response :not_found }
      end

      describe "when account has voice recharge settings" do
        before do
          current_account.create_voice_recharge_settings(
            enabled: true,
            minimum_balance: 5.00,
            amount: 50.00
          )
        end

        describe "and updated settings are valid" do
          before do
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_enabled__c).returns('No')
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_amount__c).returns(100.0)
            put :update, params: { recharge_settings: { enabled: false, amount: 100 } }
          end

          it "sets the enabled property" do
            refute current_account.voice_recharge_settings.enabled
          end

          it "sets the amount property" do
            assert_equal 100.0, current_account.voice_recharge_settings.amount
          end

          should_use_presenter Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter
        end

        describe "and updating enabled to invalid value" do
          before do
            put :update, params: { settings: { recharge_settings: { enabled: 'foo' } } }
          end

          it('returns 400 error') { assert_response :bad_request }

          it "does not update" do
            assert current_account.voice_recharge_settings.enabled
          end
        end

        describe "and updating amount to invalid value" do
          before do
            put :update, params: { settings: { recharge_settings: { amount: 199 } } }
          end

          it('returns 400 error') { assert_response :bad_request }

          it "does not update" do
            assert_equal 50.0, current_account.voice_recharge_settings.amount
          end
        end

        describe "with unsupported params" do
          before do
            put :update, params: { settings: { recharge_settings: { minimum_balance: 100 } } }
          end

          it('returns 400 error') { assert_response :bad_request }

          it "does not update" do
            assert_equal 5.0, current_account.voice_recharge_settings.minimum_balance
          end
        end

        describe 'when trial subscription' do
          before do
            Account.any_instance.stubs(:zuora_subscription)
            put :update, params: { recharge_settings: { enabled: false, amount: 100 } }
          end

          it('returns not_found error') { assert_response :not_found }

          it 'does not update' do
            assert current_account.voice_recharge_settings.enabled
            assert_equal 50.0, current_account.voice_recharge_settings.amount
          end
        end

        describe 'when update_recharge_settings fails' do
          before do
            @controller.stubs(:update_recharge_settings).returns(false)
            put :update, params: { recharge_settings: { enabled: true, amount: 100 } }
          end

          it('returns unprocessable_entity') { assert_response :unprocessable_entity }
        end
      end
    end

    describe '#create' do
      describe 'when account already has recharge settings' do
        before do
          current_account.create_voice_recharge_settings(
            enabled: true,
            minimum_balance: 5.00,
            amount: 50.00
          )
          post :create, params: { recharge_settings: { enabled: true, amount: 100 } }
        end

        it('returns not_found error') { assert_response :not_found }
      end

      describe 'when account does not have recharge settings' do
        describe "and new settings are valid" do
          before do
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_enabled__c).returns('Yes')
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_amount__c).returns(100.0)
            post :create, params: { recharge_settings: { enabled: true, amount: 100 } }
          end

          it "sets the enabled property" do
            assert current_account.voice_recharge_settings.enabled
          end

          it "sets the amount property" do
            assert_equal 100.00, current_account.voice_recharge_settings.amount
          end

          should_use_presenter Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter
        end

        describe "and setting enabled to invalid value" do
          before do
            post :create, params: { settings: { recharge_settings: { enabled: 'foo', amount: 100 } } }
          end

          it('returns bad_request error') { assert_response :bad_request }

          it "does not create" do
            assert_nil current_account.voice_recharge_settings
          end
        end

        describe "and setting amount to invalid value" do
          before do
            post :create, params: { settings: { recharge_settings: { enabled: true, amount: 199 } } }
          end

          it('returns bad_request error') { assert_response :bad_request }

          it "does not create" do
            assert_nil current_account.voice_recharge_settings
          end
        end

        describe "with unsupported params" do
          before do
            post :create, params: { settings: { recharge_settings: { minimum_balance: 100 } } }
          end

          it('returns bad_request error') { assert_response :bad_request }

          it "does not create" do
            assert_nil current_account.voice_recharge_settings
          end
        end

        describe 'when trial subscription' do
          before do
            Account.any_instance.stubs(:zuora_subscription)
            post :create, params: { recharge_settings: { enabled: false, amount: 100 } }
          end

          it('returns not_found error') { assert_response :not_found }

          it "does not create" do
            assert_nil current_account.voice_recharge_settings
          end
        end
      end

      it 'defaults enabled param to true' do
        @controller.expects(:update_recharge_settings).with('enabled' => true, 'amount' => 50).once
        post :create, params: { recharge_settings: { amount: 50 } }
      end
    end
  end
end
