require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Account::BoostsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :role_settings

  before do
    @account = @request.account = accounts(:minimum)
    accept :json
  end

  with_options(controller: 'api/v2/account/boosts') do |request|
    request.should_route :get, '/api/v2/account/boosts', action: 'show'
  end

  as_an_anonymous_user do
    should_be_unauthorized :show
  end

  as_an_end_user do
    should_be_forbidden :show
  end

  as_an_admin do
    let(:expected) do
      {
        "boosts" => [
          {
            "type" => "addon",
            "name" => "nps",
            "days_remaining" => 7
          },
          {
            "type" => "addon",
            "name" => "light_agents",
            "days_remaining" => 7
          }
        ]
      }
    end

    before do
      @account.subscription_feature_addons.create!(
        name:               'nps',
        boost_expires_at:   1.week.from_now.to_s
      )
      @account.subscription_feature_addons.create!(
        name:               'light_agents',
        boost_expires_at:   1.week.from_now.to_s
      )
    end

    describe "#show" do
      before do
        get :show
      end

      it('responds with success') { assert_response :success }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
      should_use_presenter Api::V2::Account::BoostsPresenter

      it "returns a collection of boosts" do
        output = JSON.parse(response.body)

        assert_equal expected['boosts'].map { |b| b['name'] }.sort, output['boosts'].map { |b| b['name'] }.sort
      end
    end
  end
end
