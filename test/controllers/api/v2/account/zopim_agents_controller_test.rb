require_relative '../../../../support/test_helper'
require_relative '../../../../support/zopim_test_helper'
require_relative '../../../../support/entitlement_test_helper'
require_relative '../../../../support/suite_test_helper'

SingleCov.covered! uncovered: 2

describe Api::V2::Account::ZopimAgentsController do
  extend Api::Presentation::TestHelper
  include ZopimTestHelper
  include EntitlementTestHelper
  include SuiteTestHelper

  fixtures :all

  let(:patch_status) { 200 }

  before do
    Zopim::AgentSyncJob.stubs(:work)

    Account.any_instance.stubs(has_chat_permission_set?: true)
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(
      is_serviceable?: true,
      ensure_not_phase_four: nil
    )

    ZopimIntegration.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      ensure_not_phase_four: nil
    )

    ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.stubs(:enqueue)
    ZopimIntegration.stubs(where: stub(:first_or_create!))
    Zopim::Reseller.client.stubs(
      account!: stub(trial_end_date: Time.now),
      create_account!: Hashie::Mash.new(id: 1),
      update_account!: true,
      account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
      find_accounts!: [],
      delete_account_agent!: true
    )

    stub_staff_service_patch_request('minimum', patch_status)

    accept :json
  end

  with_options(controller: 'api/v2/account/zopim_agents') do |request|
    request.should_route :get, '/api/v2/account/zopim_agents/1', action: 'show', id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized :show
  end

  as_an_end_user do
    should_be_forbidden :show
  end

  def self.test_all
    let(:current_account) { @controller.send(:current_account) }

    before do
      setup_zopim_subscription(current_account)
    end

    describe "#show" do
      describe "when everything is okay" do
        before do
          get :show, params: { id: current_account.owner.id }
        end

        it('responds with success') { assert_response :success }
        should_use_presenter Api::V2::Account::ZopimAgentPresenter
      end

      describe "when the candidate user does not exist" do
        before do
          get :show, params: { id: 0 }
        end

        it('responds with not_found') { assert_response :not_found }
      end
    end
  end

  as_an_agent do
    test_all
  end

  as_an_admin do
    test_all
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    test_all
  end
end
