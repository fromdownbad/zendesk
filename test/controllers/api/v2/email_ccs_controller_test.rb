require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::EmailCcsController do
  extend Api::Presentation::TestHelper
  fixtures :tickets, :accounts, :users

  let(:ticket) { tickets(:minimum_3) }
  let(:agent) { users(:minimum_agent) }
  let(:account) { accounts(:minimum) }
  let(:collaboration) { Collaboration.create!(user: agent, ticket: ticket, account: account, collaborator_type: CollaboratorType.EMAIL_CC) }

  before do
    accept :json
  end

  with_options(controller: 'api/v2/email_ccs') do |request|
    request.should_route :get, '/api/v2/tickets/1/email_ccs', action: 'index', ticket_id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index, {ticket_id: 1}]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index, {ticket_id: 1}]
  end

  as_a_subsystem_user(account: :minimum, user: "pigeon") do
    describe "a GET to :index" do
      before { get :index, params: { ticket_id: ticket.nice_id } }

      it('responds with success') { assert_response :success }
    end
  end

  as_an_agent do
    describe "a GET to :index" do
      before { get :index, params: { ticket_id: ticket.nice_id } }
      should_use_presenter Api::V2::Users::AgentPresenter, status: :ok
    end

    describe 'a GET to :index, and the agent can only view assigned tickets' do
      before do
        # @agent = agent
        agent.restriction_id = RoleRestrictionType.ASSIGNED
        agent.save!
      end

      describe 'when the ticket is not assigned to the agent' do
        let(:ticket) { tickets(:minimum_2) }

        before do
          get :index, params: { ticket_id: ticket.nice_id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe 'when the ticket is assigned to the agent' do
        let(:ticket) { tickets(:minimum_1) }

        before do
          ticket = tickets(:minimum_1)
          get :index, params: { ticket_id: ticket.nice_id }
        end

        it('responds with success') { assert_response :success }
      end
    end
  end
end
