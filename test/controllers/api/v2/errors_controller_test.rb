require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ErrorsController do
  extend Api::V2::TestHelper
  use_test_routes

  before do
  end

  with_options(controller: "api/v2/errors") do |request|
    request.should_route :get,    "/api/v2/not_valid",        action: "routing", path: "not_valid"
    request.should_route :post,   "/api/v2/not_valid",        action: "routing", path: "not_valid"
    request.should_route :delete, "/api/v2/not_valid",        action: "routing", path: "not_valid"
    request.should_route :put,    "/api/v2/not_valid",        action: "routing", path: "not_valid"
    request.should_route :get,    "/api/v2/nested/not_valid", action: "routing", path: "nested/not_valid"
  end

  describe "without accept :xml" do
    before { accept :xml }

    as_an_anonymous_user do
      should_be_unauthorized :routing
    end

    as_an_end_user do
      should_be_forbidden :routing
    end

    as_an_agent do
      before { get :routing }
      it('responds with not_found') { assert_response :not_found }
    end

    as_a_system_user do
      before { get :routing }
      it('responds with not_found') { assert_response :not_found }
    end
  end

  describe "with accept :json" do
    before { accept :json }

    as_an_anonymous_user do
      should_be_unauthorized :routing
    end

    as_an_end_user do
      should_be_forbidden :routing
    end

    as_an_agent do
      before do
        get :routing
        @json = JSON.parse(@response.body)
      end

      it('responds with not_found') { assert_response :not_found }

      it "returns an error" do
        assert_equal 'InvalidEndpoint', @json['error']
      end
    end

    as_a_system_user do
      before do
        get :routing
        @json = JSON.parse(@response.body)
      end

      it('responds with not_found') { assert_response :not_found }

      it "returns an error" do
        assert_equal 'InvalidEndpoint', @json['error']
      end
    end
  end
end
