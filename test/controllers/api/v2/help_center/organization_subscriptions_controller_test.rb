require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::HelpCenter::OrganizationSubscriptionsController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :forums, :entries, :organizations

  with_options(controller: "api/v2/help_center/organization_subscriptions") do |request|
    request.should_route :get,    "/api/v2/users/123/organization_subscriptions",       action: "user_index", user_id: "123"
    request.should_route :get,    "/api/v2/users/123/organization_subscriptions/count", action: "count", user_id: "123"
    request.should_route :get,    "/api/v2/organizations/123/subscriptions",      action: "organization_index", organization_id: "123"
    request.should_route :get,    "/api/v2/organization_subscriptions",           action: "index"
    request.should_route :post,   "/api/v2/organization_subscriptions",           action: "create"
    request.should_route :get,    "/api/v2/organization_subscriptions/count",     action: "count"
    request.should_route :delete, "/api/v2/organization_subscriptions/123",       action: "destroy", id: "123"
    request.should_route :get,    "/api/v2/organization_subscriptions/123",       action: "show", id: "123"
  end

  let(:user) { users(:minimum_end_user) }
  let(:user2) { users(:minimum_end_user2) }
  let(:agent) { users(:minimum_agent) }
  let(:organization) { organizations(:minimum_organization1) } # shared
  let(:organization2) { organizations(:minimum_organization2) } # not shared
  let(:account) { accounts(:minimum) }

  before do
    accept :json
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :show, :create, :destroy
  end

  describe "as an authenticated user" do
    before do
      @request.account = account

      @agent_watching = Watching.create! do |w|
        w.user    = agent
        w.account = account
        w.source  = organization2
      end

      @user_watching = Watching.create! do |w|
        w.user    = user
        w.account = account
        w.source  = organization
      end
    end

    as_an_end_user do
      describe "a GET to :index" do
        describe "when not passing parameters" do
          before { get :index }
          should_use_presenter Api::V2::OrganizationSubscriptionPresenter, status: :ok

          it "lists only subscriptions of the end user" do
            assert_equal user.watchings.only_organization.count(:all), JSON.parse(@response.body)["organization_subscriptions"].size
          end
        end

        describe "when passing an organization_id" do
          before { get :organization_index, params: { organization_id: organization.id } }
          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "when passing an invalid user_id " do
          before { get :user_index, params: { user_id: agent.id } }
          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "when passing a valid user_id" do
          before { get :user_index, params: { user_id: user.id } }
          it "lists only subscriptions of the end user" do
            assert_equal user.watchings.only_organization.count(:all), JSON.parse(@response.body)["organization_subscriptions"].size
          end
        end

        describe "with cursor pagination" do
          should_support_cursor_pagination_conversion :index, 'organization_subscriptions', :cursor_pagination_organization_subscriptions_index, :remove_offset_pagination_organization_subscriptions_index, table_name: 'watchings', sortable_fields: Api::V2::HelpCenter::OrganizationSubscriptionsController::CBP_SORTABLE_FIELDS
        end
      end

      describe "a GET to :show" do
        describe "when passing invalid parameters" do
          before { get :show, params: { id: @agent_watching.id } }
          it('responds with not_found') { assert_response :not_found }
        end

        describe "when passing valid parameters" do
          before { get :show, params: { id: @user_watching.id } }
          should_use_presenter Api::V2::OrganizationSubscriptionPresenter, status: :ok
        end
      end

      describe "a POST to :create" do
        describe "when the organizations is shared" do
          before { post :create, params: { organization_subscription: { user_id: user2.id, organization_id: organization.id } } }

          should_use_presenter Api::V2::OrganizationSubscriptionPresenter, status: :created
          should_change("the number of watchings", by: 1) { account.watchings.count(:all) }
        end

        describe "when the organizations is not shared" do
          before { post :create, params: { organization_subscription: { user_id: user2.id, organization_id: organization2.id } } }
          it('responds with not_found') { assert_response :not_found }
        end
      end

      describe "a DELETE to :destroy" do
        before { delete :destroy, params: { id: @agent_watching.id } }
        it('responds with not_found') { assert_response :not_found }
      end
    end

    as_an_agent do
      describe "a GET to :index" do
        describe "when not passing parameters" do
          before { get :index }
          should_use_presenter Api::V2::OrganizationSubscriptionPresenter, status: :ok

          it "lists records for all subscribed users across all organizations" do
            assert_equal account.watchings.only_organization.count(:all), JSON.parse(@response.body)["organization_subscriptions"].size
          end
        end

        describe "when passing a user_id" do
          before { get :user_index, params: { user_id: user.id } }
          should_use_presenter Api::V2::OrganizationSubscriptionPresenter, status: :ok

          it "only list the subscriptions of the given user" do
            assert_equal user.watchings.only_organization.count(:all), JSON.parse(@response.body)["organization_subscriptions"].size
          end
        end

        describe "when passing a organization_id" do
          before { get :organization_index, params: { organization_id: organization2.id } }
          should_use_presenter Api::V2::OrganizationSubscriptionPresenter, status: :ok

          it "lists the subscriptions of the given organization" do
            assert_equal organization.watchings.count(:all), JSON.parse(@response.body)["organization_subscriptions"].size
          end
        end

        describe "with side-loading" do
          describe "users" do
            before { get :index, params: { include: :users } }
            it "has users in response" do
              assert_not_nil JSON.parse(@response.body)["users"]
            end
          end

          describe "organizations" do
            before { get :index, params: { include: :organizations } }
            it "has organizations in response" do
              assert_not_nil JSON.parse(@response.body)["organizations"]
            end
          end
        end

        describe "with cursor pagination" do
          should_support_cursor_pagination_conversion :index, 'organization_subscriptions', :cursor_pagination_organization_subscriptions_index, :remove_offset_pagination_organization_subscriptions_index, table_name: 'watchings', sortable_fields: Api::V2::HelpCenter::OrganizationSubscriptionsController::CBP_SORTABLE_FIELDS
        end
      end

      describe "a GET to :show" do
        describe "when passing invalid parameters" do
          before { get :show, params: { id: 1 } }
          it('responds with not_found') { assert_response :not_found }
        end

        describe "when passing valid parameters" do
          before { get :show, params: { id: @agent_watching.id } }
          should_use_presenter Api::V2::OrganizationSubscriptionPresenter, status: :ok
        end
      end

      describe "a POST to :create" do
        describe "when not passing a user_id" do
          before { post :create, params: { organization_subscription: { organization_id: organization.id } } }
          it('responds with not_found') { assert_response :not_found }
        end

        describe "when not passing a organization_id" do
          before { post :create, params: { organization_subscription: { user_id: agent.id } } }
          it('responds with not_found') { assert_response :not_found }
        end

        describe "when the user is not already subscribed" do
          before do
            refute agent.watchings.exists?(source_id: organization.id, source_type: organization.class.name)
            post :create, params: { organization_subscription: { user_id: agent.id, organization_id: organization.id } }
          end

          should_use_presenter Api::V2::OrganizationSubscriptionPresenter, status: :created
          should_change("the number of watchings", by: 1) { account.watchings.count(:all) }
        end

        describe "when the user is already subscribed" do
          before { post :create, params: { organization_subscription: { user_id: @agent_watching.user_id, organization_id: @agent_watching.source_id } } }

          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
          should_not_change("the number of watchings") { account.watchings.count(:all) }
        end

        describe "when adding an end-user to an unshared organization" do
          before { post :create, params: { organization_subscription: { user_id: user.id, organization_id: organization2.id } } }
          it('responds with forbidden') { assert_response :forbidden }
        end
      end

      describe "a DELETE to :destroy" do
        before { delete :destroy, params: { id: @agent_watching.id } }

        it('responds with no_content') { assert_response :no_content }
        should_change("the number of watchings", by: -1) { account.watchings.count(:all) }
      end
    end
  end
end
