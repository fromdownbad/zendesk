require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::MobileSdkAppsController do
  extend Api::V2::TestHelper

  fixtures :mobile_sdk_apps, :mobile_sdk_app_settings, :mobile_sdk_auths,
    :accounts, :users, :brands, :role_settings, :oauth_clients

  before do
    accept :json
  end

  with_options(controller: 'api/v2/mobile_sdk_apps') do |request|
    request.should_route :post,  '/api/v2/mobile_sdk_apps',        action: 'create'
    request.should_route :get,   '/api/v2/mobile_sdk_apps',        action: 'index'
    request.should_route :get,   '/api/v2/mobile_sdk_apps/123',    action: 'show', id: '123'
    request.should_route :put,   '/api/v2/mobile_sdk_apps/123',    action: 'update', id: '123'
    request.should_route :delete, '/api/v2/mobile_sdk_apps/123', action: 'destroy', id: '123'
  end

  as_an_anonymous_user do
    should_be_unauthorized :create, :index, :show, :update, :destroy
  end

  as_an_end_user do
    should_be_forbidden :create, :index, :show, :update, :destroy
  end

  as_an_admin do
    before do
      @model = mobile_sdk_apps(:minimum_sdk)
    end

    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::MobileSdkAppPresenter
    end

    describe "a GET to :show" do
      before do
        @mobile_sdk_app = MobileSdkApp.create!(
          title: @model.title,
          identifier: SecureRandom.hex(16),
          mobile_sdk_auth: @model.mobile_sdk_auth,
          account: @request.account,
          brand: @request.account.brands.first,
          signup_source: @model.signup_source
        )
      end

      describe "by id" do
        before { get :show, params: { id: @mobile_sdk_app.identifier } }
        should_use_presenter Api::V2::MobileSdkAppPresenter
      end

      describe "by id other account" do
        before { get :show, params: { id: @model.id } }
        should respond_with :not_found
      end

      describe "by invalid id" do
        before { get :show, params: { id: 1231231230 } }
        should respond_with :not_found
      end
    end

    describe "a POST to :create" do
      let(:params) do
        {
          title: @model.title,
          brand_id: @request.account.brands.first.id,
          signup_source: @model.signup_source,
          settings: {
            authentication: "jwt",
            contact_us_tags: "foo, bar",
            conversations_enabled: true,
            helpcenter_enabled: true,
            push_notifications_callback: "http://foo.com/foo/push",
            push_notifications_enabled: true,
            push_notifications_type: "webhook",
            rma_android_store_url: "http://android.com/store/foo",
            rma_delay: "1",
            rma_duration: "1",
            rma_enabled: true,
            rma_ios_store_url: "http://apple.com/store/foo",
            rma_tags: "foo, bar",
            rma_visits: "1",
            urban_airship_key: "ua_key",
            urban_airship_master_secret: "ua_secret",
            connect_enabled: true,
            blips: true,
            required_blips: true,
            behavioural_blips: true,
            pathfinder_blips: true,
            collect_only_required_data: true,
            support_never_request_email: true,
            support_show_closed_requests: true,
            support_show_referrer_logo: false,
            answer_bot: true,
            support_show_csat: false
          },
          mobile_sdk_auth: { endpoint: @model.mobile_sdk_auth.endpoint }
        }
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_blip_collection_control do
        describe "when creating a new app" do
          before do
            post :create, params: { mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :created
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should include creation params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_equal params[:signup_source], output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(params, output, mobile_sdk_blip_collection_control: @arturo_enabled)
            end
          end
        end

        describe "when creating a new app without 'signup_source'" do
          before do
            params[:signup_source] = nil
            post :create, params: { mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :created
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "response should include creation params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_nil output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(params, output, mobile_sdk_blip_collection_control: @arturo_enabled)
            end
          end
        end
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_support_never_request_email_setting do
        describe "when creating a new app" do
          before do
            post :create, params: { mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :created
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should include creation params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_equal params[:signup_source], output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(params, output, mobile_sdk_support_never_request_email_setting: @arturo_enabled)
            end
          end
        end

        describe "when creating a new app without 'signup_source'" do
          before do
            params[:signup_source] = nil
            post :create, params: { mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :created
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "response should include creation params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_nil output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(params, output, mobile_sdk_support_never_request_email_setting: @arturo_enabled)
            end
          end
        end
      end

      describe_with_arturo_enabled :mobile_sdk_help_center_article_voting_enabled do
        before do
          params[:settings][:help_center_article_voting_enabled] = true
          post :create, params: { mobile_sdk_app: params }
        end

        describe "success" do
          should respond_with :created
          should_use_presenter Api::V2::MobileSdkAppPresenter

          it "returns the correct response depending on whether or not the feature is enabled" do
            output = JSON.parse(@response.body)["mobile_sdk_app"]
            assert output["settings"]["help_center_article_voting_enabled"], "When the arturo is enabled, value should be true"
          end
        end
      end

      describe_with_arturo_disabled :mobile_sdk_help_center_article_voting_enabled do
        before do
          post :create, params: { mobile_sdk_app: params }
        end

        describe "success" do
          should respond_with :created
          should_use_presenter Api::V2::MobileSdkAppPresenter

          it "defaults to `false` for `help_center_article_voting_enabled` setting" do
            refute @request.request_parameters["mobile_sdk_app"]["settings"]["enable_help_center_article voting"], "When the arturo is disabled, value should be false"
          end
        end

        describe "when `help_center_article_voting_enabled` is `true`" do
          before do
            params[:settings][:help_center_article_voting_enabled] = true
            post :create, params: { mobile_sdk_app: params }
          end

          it "setting value is respected" do
            output = JSON.parse(@response.body)["mobile_sdk_app"]
            assert output["settings"]["help_center_article_voting_enabled"], "When the arturo is disabled, value should be false"
          end
        end
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_support_show_closed_requests_setting do
        describe "when creating a new app" do
          before do
            post :create, params: { mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :created
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should include creation params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_equal params[:signup_source], output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(params, output, mobile_sdk_support_show_closed_requests_setting: @arturo_enabled)
            end
          end
        end
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_support_show_referrer_logo_setting do
        describe "when creating a new app" do
          before do
            # Not ideal as depends on the actual implementation of the way we check for the account being Enterprise.
            # I settled for this after trying other intrusive (to this test) approaches, the main problem is the
            # usage of `as_an_admin` which sets a lot of state in the controller in this test (e.g: current account)
            # to the minimum account. This way we cheat the check currently living in
            # `Api::V2::MobileSdkAppsController::current_account_cannot_hide_referrer_logo?` making it think the
            # account is an Enterprise by the time it runs.
            Subscription.any_instance.stubs(:plan_type).returns(ZBC::Zendesk::PlanType::Enterprise.plan_type)

            post :create, params: { mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :created
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should include creation params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_equal params[:signup_source], output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(params, output, mobile_sdk_support_show_referrer_logo_setting: @arturo_enabled)
            end
          end
        end
      end

      describe "when creating a new app with `mobile_sdk_rma_tab_removal` feature is enabled" do
        before do
          Arturo.enable_feature!(:mobile_sdk_rma_tab_removal)
          params[:settings][:rma_enabled] = true
          post :create, params: { mobile_sdk_app: params }
        end

        describe "success" do
          should respond_with :created
          should_use_presenter Api::V2::MobileSdkAppPresenter

          it "reponse should always include rma_enabled with false value" do
            output = JSON.parse(@response.body)["mobile_sdk_app"]
            refute output["settings"]["rma_enabled"]
          end
        end
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_answer_bot do
        describe "when creating a new app" do
          before do
            stub_request(:get, "https://minimum.zendesk-test.com/api/v2/answer_bot/availability?deflection_channel_id=#{Zendesk::Types::ViaType.ANSWER_BOT_FOR_SDK}").
              to_return(status: 200, body: { available: true }.to_json, headers: { 'Content-Type' => 'application/json' })

            post :create, params: { mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :created
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should include creation params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_equal params[:signup_source], output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(params, output, mobile_sdk_answer_bot: @arturo_enabled)
            end
          end
        end
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_csat do
        describe "when creating a new app" do
          before do
            post :create, params: { mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :created
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should include creation params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_equal params[:signup_source], output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(params, output, mobile_sdk_csat: @arturo_enabled)
            end
          end
        end
      end
    end

    describe "a PUT to :update" do
      before do
        brand1 = request.account.brands.first
        brand2 = brands("minimum_sdk")
        @request.account.stubs(:brands).returns([brand1, brand2])
      end

      let(:settings_for_create) do
        {
          authentication: "jwt",
          contact_us_tags: "foo, bar",
          conversations_enabled: true,
          helpcenter_enabled: true,
          push_notifications_callback: "http://boo.com/foo/push",
          push_notifications_enabled: true,
          push_notifications_type: "webhook",
          rma_android_store_url: "http://bandroid.com/store/foo",
          rma_delay: "1",
          rma_duration: "1",
          rma_enabled: true,
          rma_ios_store_url: "http://bapple.com/store/foo",
          rma_tags: "foo, bar",
          rma_visits: "1",
          urban_airship_key: "ua_key",
          urban_airship_master_secret: "ua_secret",
          connect_enabled: true,
          blips: true,
          required_blips: true,
          behavioural_blips: true,
          pathfinder_blips: true,
          collect_only_required_data: true,
          support_never_request_email: true,
          support_show_closed_requests: true,
          support_show_referrer_logo: false,
          answer_bot: true,
          support_show_csat: false
        }
      end

      let(:params) do
        {
          title: "#{@model.title} updated!",
          brand_id: @request.account.brands.last.id,
          signup_source: "apple",
          settings: {
            authentication: "jwt",
            contact_us_tags: "new, way",
            conversations_enabled: false,
            helpcenter_enabled: false,
            push_notifications_callback: "http://foo.com/foo/push2",
            push_notifications_enabled: true,
            push_notifications_type: "webhook",
            rma_android_store_url: "http://android.com/store/foo2",
            rma_delay: "2",
            rma_duration: "2",
            rma_enabled: true,
            rma_ios_store_url: "http://apple.com/store/foo2",
            rma_tags: "foo1, bar1",
            rma_visits: "2",
            urban_airship_key: "ua_key1",
            urban_airship_master_secret: "ua_secret1",
            connect_enabled: false,
            blips: true,
            required_blips: true,
            behavioural_blips: true,
            pathfinder_blips: true,
            collect_only_required_data: true,
            support_never_request_email: true,
            support_show_closed_requests: true,
            support_show_referrer_logo: false,
            answer_bot: true,
            support_show_csat: false
          },
          mobile_sdk_auth: { endpoint: @model.mobile_sdk_auth.endpoint.to_s }
        }
      end

      before do
        @mobile_sdk_app = MobileSdkApp.create!(
          title: "#{@model.title} created!",
          identifier: SecureRandom.hex(16),
          mobile_sdk_auth: @model.mobile_sdk_auth,
          account: @request.account,
          brand: @request.account.brands.first,
          signup_source: @model.signup_source
        )

        @mobile_sdk_app.update_attributes(
          settings: settings_for_create
        )

        @expected_output = params
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_blip_collection_control do
        describe "when updated an existing app" do
          before do
            put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :ok
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should display updated params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_not_equal @request.account.brands.last.id, @request.account.brands.first.id
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_equal params[:signup_source], output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(@expected_output, output, mobile_sdk_blip_collection_control: @arturo_enabled)
            end
          end
        end

        describe "when updated an existing app without 'signup_source'" do
          before do
            params[:signup_source] = nil
            put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :ok
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should display updated params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_not_equal @request.account.brands.last.id, @request.account.brands.first.id
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_nil output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(@expected_output, output, mobile_sdk_blip_collection_control: @arturo_enabled)
            end
          end
        end
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_support_never_request_email_setting do
        describe "when updated an existing app" do
          before do
            put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :ok
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should display updated params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_not_equal @request.account.brands.last.id, @request.account.brands.first.id
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_equal params[:signup_source], output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(@expected_output, output, mobile_sdk_support_never_request_email_setting: @arturo_enabled)
            end
          end
        end

        describe "when updated an existing app without 'signup_source'" do
          before do
            params[:signup_source] = nil
            put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :ok
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should display updated params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_not_equal @request.account.brands.last.id, @request.account.brands.first.id
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_nil output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(@expected_output, output, mobile_sdk_support_never_request_email_setting: @arturo_enabled)
            end
          end
        end
      end

      describe_with_arturo_enabled :mobile_sdk_support_show_closed_requests_setting do
        [
          ZBC::Zendesk::PlanType::Team,
          ZBC::Zendesk::PlanType::Professional,
          ZBC::Zendesk::PlanType::Enterprise
        ].each do |plan|
          describe "and the account's plan is #{plan.name} while updated an existing app" do
            before do
              Subscription.any_instance.stubs(:plan_type).returns(plan.plan_type)

              put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
            end

            describe "success" do
              should respond_with :ok
              should_use_presenter Api::V2::MobileSdkAppPresenter

              it "reponse should display updated params" do
                output = JSON.parse(@response.body)["mobile_sdk_app"]
                assert_not_equal @request.account.brands.last.id, @request.account.brands.first.id
                assert_equal params[:title], output["title"]
                assert_equal params[:brand_id], output["brand_id"]

                assert_settings(@expected_output, output, mobile_sdk_support_show_closed_requests_setting: true)
              end
            end
          end
        end

        describe "and the account's plan is essential" do
          before do
            Subscription.any_instance.stubs(:plan_type).returns(ZBC::Zendesk::PlanType::Essential.plan_type)

            put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :ok
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should display updated params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_not_equal @request.account.brands.last.id, @request.account.brands.first.id
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]

              # We are hitting with false (see `params`), the app has true see (`settings_for_create`)
              # and the account is essential hence it should change.
              @expected_output[:settings][:support_show_closed_requests] = true
              assert_settings(@expected_output, output, mobile_sdk_support_show_closed_requests_setting: true)
            end
          end
        end
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_support_show_closed_requests_setting do
        describe "when updated an existing app" do
          before do
            put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :ok
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should display updated params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_not_equal @request.account.brands.last.id, @request.account.brands.first.id
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_equal params[:signup_source], output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(@expected_output, output, mobile_sdk_support_show_closed_requests_setting: @arturo_enabled)
            end
          end
        end
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_support_show_referrer_logo_setting do
        describe "when updated an existing app" do
          before do
            # Not ideal as depends on the actual implementation of the way we check for the account being Enterprise.
            # I settled for this after trying other intrusive (to this test) approaches, the main problem is the
            # usage of `as_an_admin` which sets a lot of state in the controller in this test (e.g: current account)
            # to the minimum account. This way we cheat the check currently living in
            # `Api::V2::MobileSdkAppsController::current_account_cannot_hide_referrer_logo?` making it think the
            # account is an Enterprise by the time it runs.
            Subscription.any_instance.stubs(:plan_type).returns(ZBC::Zendesk::PlanType::Enterprise.plan_type)

            put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :ok
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should display updated params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_not_equal @request.account.brands.last.id, @request.account.brands.first.id
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_equal params[:signup_source], output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(@expected_output, output, mobile_sdk_support_show_referrer_logo_setting: @arturo_enabled)
            end
          end
        end
      end

      describe "when updated an existing app with `mobile_sdk_rma_tab_removal` feature is enabled" do
        describe "`rma_enabled` was true before the update" do
          before do
            Arturo.enable_feature!(:mobile_sdk_rma_tab_removal)
            params[:settings][:rma_enabled] = true
            put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
          end

          should respond_with :ok
          should_use_presenter Api::V2::MobileSdkAppPresenter

          it "reponse should always include rma_enabled with false value" do
            output = JSON.parse(@response.body)["mobile_sdk_app"]
            assert output["settings"]["rma_enabled"]
          end
        end

        describe "`rma_enabled` was false before the update" do
          before do
            @mobile_sdk_app.update_attributes(
              settings: {rma_enabled: false}
            )
            Arturo.enable_feature!(:mobile_sdk_rma_tab_removal)
            params[:settings][:rma_enabled] = true
            put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
          end

          should respond_with :ok
          should_use_presenter Api::V2::MobileSdkAppPresenter

          it "reponse should always include rma_enabled with false value" do
            output = JSON.parse(@response.body)["mobile_sdk_app"]
            refute output["settings"]["rma_enabled"]
          end
        end
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_answer_bot do
        describe "when updated an existing app" do
          before do
            stub_request(:get, "https://minimum.zendesk-test.com/api/v2/answer_bot/availability?deflection_channel_id=#{Zendesk::Types::ViaType.ANSWER_BOT_FOR_SDK}").
              to_return(status: 200, body: { available: true }.to_json, headers: { 'Content-Type' => 'application/json' })

            put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
          end

          describe "success" do
            should respond_with :ok
            should_use_presenter Api::V2::MobileSdkAppPresenter

            it "reponse should display updated params" do
              output = JSON.parse(@response.body)["mobile_sdk_app"]
              assert_not_equal @request.account.brands.last.id, @request.account.brands.first.id
              assert_equal params[:title], output["title"]
              assert_equal params[:brand_id], output["brand_id"]
              assert_equal params[:signup_source], output["signup_source"]
              assert_nil output["settings"]["title"]

              assert_settings(@expected_output, output, mobile_sdk_answer_bot: @arturo_enabled)
            end
          end
        end
      end

      describe_with_and_without_arturo_enabled :mobile_sdk_csat do
        describe "when successfully updated an existing app" do
          should respond_with :ok
          should_use_presenter Api::V2::MobileSdkAppPresenter
          before do
            put :update, params: { id: @mobile_sdk_app.identifier, mobile_sdk_app: params }
          end

          it "reponse should display updated params" do
            output = JSON.parse(@response.body)["mobile_sdk_app"]
            refute_equal @request.account.brands.last.id, @request.account.brands.first.id
            assert_equal params[:title], output["title"]
            assert_equal params[:brand_id], output["brand_id"]
            assert_equal params[:signup_source], output["signup_source"]
            assert_nil output["settings"]["title"]

            assert_settings(@expected_output, output, mobile_sdk_csat: @arturo_enabled)
          end
        end
      end
    end

    describe "a DELETE to :destroy" do
      before do
        @mobile_sdk_app = MobileSdkApp.create!(
          title: @model.title,
          identifier: SecureRandom.hex(16),
          mobile_sdk_auth: MobileSdkAuth.new(name: "loo", endpoint: "", shared_secret: "EziF7yy38YSz8PbUqBLrOHHYw3Zqvik7GegR0KuwQ8wK8WrX"),
          account: @request.account,
          brand: @request.account.brands.first,
          signup_source: @model.signup_source
        )
      end

      describe "by id" do
        before do
          delete :destroy, params: { id: @mobile_sdk_app.identifier }
        end
        should respond_with :no_content
      end
    end
  end

  def assert_settings(expected, actual, arturo_features = {})
    assert_equal expected[:settings][:contact_us_tags], actual["settings"]["contact_us_tags"].join(", ")
    assert_equal expected[:settings][:conversations_enabled], actual["settings"]["conversations_enabled"]
    assert_equal expected[:settings][:helpcenter_enabled], actual["settings"]["helpcenter_enabled"]
    assert_equal expected[:settings][:push_notifications_callback], actual["settings"]["push_notifications_callback"]
    assert_equal expected[:settings][:push_notifications_enabled], actual["settings"]["push_notifications_enabled"]
    assert_equal expected[:settings][:push_notifications_type], actual["settings"]["push_notifications_type"]
    assert_equal expected[:settings][:rma_android_store_url], actual["settings"]["rma_android_store_url"]
    assert_equal expected[:settings][:rma_delay].to_i, actual["settings"]["rma_delay"]
    assert_equal expected[:settings][:rma_duration].to_i, actual["settings"]["rma_duration"]
    assert_equal expected[:settings][:rma_enabled], actual["settings"]["rma_enabled"]
    assert_equal expected[:settings][:rma_ios_store_url], actual["settings"]["rma_ios_store_url"]
    assert_equal expected[:settings][:rma_tags], actual["settings"]["rma_tags"].join(", ")
    assert_equal expected[:settings][:rma_visits].to_i, actual["settings"]["rma_visits"]
    assert_equal expected[:settings][:urban_airship_key], actual["settings"]["urban_airship_key"]
    assert_equal expected[:settings][:urban_airship_master_secret], actual["settings"]["urban_airship_master_secret"]
    assert_equal expected[:settings][:connect_enabled], actual["settings"]["connect_enabled"]
    assert_equal expected[:settings][:blips], actual["settings"]["blips"]
    assert_equal expected[:settings][:required_blips], actual["settings"]["required_blips"]
    assert_equal expected[:settings][:behavioural_blips], actual["settings"]["behavioural_blips"]
    assert_equal expected[:settings][:pathfinder_blips], actual["settings"]["pathfinder_blips"]

    if arturo_features[:mobile_sdk_blip_collection_control]
      assert_equal expected[:settings][:collect_only_required_data], actual["settings"]["collect_only_required_data"]
    else
      refute actual["settings"].key?(:collect_only_required_data), "setting 'collect_only_required_data' should not be here"
    end

    if arturo_features[:mobile_sdk_support_never_request_email_setting]
      assert_equal expected[:settings][:support_never_request_email], actual["settings"]["support_never_request_email"]
    else
      refute actual["settings"]["support_never_request_email"], "setting 'support_never_request_email' should be false"
    end

    if arturo_features[:mobile_sdk_support_show_closed_requests_setting]
      assert_equal expected[:settings][:support_show_closed_requests], actual["settings"]["support_show_closed_requests"]
    else
      assert actual["settings"]["support_show_closed_requests"], "setting 'support_show_closed_requests' should be true"
    end

    if arturo_features[:mobile_sdk_support_show_referrer_logo_setting]
      assert_equal expected[:settings][:support_show_referrer_logo], actual["settings"]["support_show_referrer_logo"]
    else
      assert actual["settings"]["support_show_referrer_logo"], "setting 'support_show_referrer_logo' should be true"
    end

    if arturo_features[:mobile_sdk_answer_bot]
      assert_equal expected[:settings][:answer_bot], actual["settings"]["answer_bot"]
    else
      refute actual["settings"]["answer_bot"], "setting 'answer_bot' should be false"
    end

    if arturo_features[:mobile_sdk_csat]
      assert_equal expected[:settings][:support_show_csat], actual["settings"]["support_show_csat"]
    else
      refute actual["settings"]["support_show_csat"], "setting 'csat' should be false"
    end
  end
end
