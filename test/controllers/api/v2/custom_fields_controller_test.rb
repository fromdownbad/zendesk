require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered! uncovered: 2

class Api::V2::CustomFieldsControllerTest < ActionController::TestCase
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  include CustomFieldsTestHelper
  fixtures :all

  def self.model_name
    "user"
  end

  def model_name
    self.class.model_name
  end

  def scope
    "for_#{model_name}"
  end

  def self.path
    "#{model_name}_fields"
  end

  def path
    self.class.path
  end

  def custom_field
    custom_field = @account.custom_fields.send(scope).build(title: "Foo Bar {{dc.dc_item}}", key: "foo")
    custom_field.type = "Text"
    custom_field.save!
    custom_field
  end

  def self.shared_setup
    before do
      # don't use these fixtures
      CustomField::Field.without_arsi.delete_all
      CustomField::Value.without_arsi.delete_all
      accept :json
      @account = accounts(:minimum)
      @custom_field = custom_field
    end
  end

  def self.define_tests
    # ugly way of sharing all the test code between the two controllers
    describe model_name + "controller" do
      shared_setup

      with_options(controller: "api/v2/#{path}") do |request|
        request.should_route :post,   "/api/v2/#{path}",        action: 'create'
        request.should_route :get,    "/api/v2/#{path}",        action: 'index'
        request.should_route :get,    "/api/v2/#{path}/count",  action: 'count'
        request.should_route :put,    "/api/v2/#{path}/123",    action: 'update',  id: '123'
        request.should_route :get,    "/api/v2/#{path}/123",    action: 'show',    id: '123'
        request.should_route :delete, "/api/v2/#{path}/123",    action: 'destroy', id: '123'
      end

      as_an_anonymous_user do
        should_be_unauthorized :index, :create, :update, :destroy, :show, :reorder
      end

      as_an_end_user do
        should_be_forbidden :index, :create, :update, :destroy, :show, :reorder
      end

      ["gooddata", "bime", "outbound"].each do |subsystem_user|
        as_a_subsystem_user(account: :minimum, user: subsystem_user) do
          should_be_forbidden :create, :update, :destroy

          should_be_authorized do
            get :show, params: { id: @custom_field.id }
            get :index
          end
        end
      end

      as_an_agent do
        should_be_forbidden :create, :update, :destroy, :reorder

        describe 'when account has user_and_organization_fields' do
          before do
            @account.stubs("has_user_and_organization_fields?").returns(true)
          end

          describe "a GET to :index" do
            it "exercises the damn code path" do
              get :index
              @res = JSON.parse(@response.body)
              assert_equal @custom_field.id, @res[model_name + "_fields"].first["id"]
            end

            it "shows both active and inactive fields, sorted by active,position" do
              accounts(:minimum).custom_fields.send(scope).checkboxes.create!(key: "cb5", is_active: true, title: "field 5", position: 5)
              accounts(:minimum).custom_fields.send(scope).checkboxes.create!(key: "cb1", is_active: true, title: "field 1", position: 1)
              accounts(:minimum).custom_fields.send(scope).checkboxes.create!(key: "cb8", is_active: false, title: "field 8", position: 0)
              get :index
              @res = JSON.parse(@response.body)
              assert_equal ["cb1", "cb5", "foo", "cb8"], @res[model_name + "_fields"].map { |r| r['key'] }
            end

            it "uses ETag and Last-Modified" do
              assert_etagged :index do
                CustomField::Field.last.update_attribute(:updated_at, 1.minute.from_now)
                @controller.instance_variable_set(:@custom_fields, nil)
              end
            end

            it "converts dynamic content" do
              Zendesk::Liquid::DcContext.stubs(:render).returns('rendered_value')
              get :index
              @res = JSON.parse(@response.body)
              assert_equal 'rendered_value', @res[model_name + "_fields"][0]['title']
            end

            describe "with cursor pagination" do
              should_support_cursor_pagination_conversion :index, model_name + "_fields", :cursor_pagination_custom_fields_index, :remove_offset_pagination_custom_fields_index, table_name: 'cf_fields', sortable_fields: Api::V2::CustomFieldsController::CBP_SORTABLE_FIELDS
            end

            describe 'with valid external domain' do
              it 'returns a valid allow-origin header with an allowed external domain as origin' do
                @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
                get :index
                assert_equal @response.headers['Access-Control-Allow-Origin'], 'http://app.futuresimple.com'
              end

              it 'returns a not valid allow-origin header with an allowed external domain as origin' do
                @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
                get :index
                assert_nil @response.headers['Access-Control-Allow-Origin']
              end
            end
          end

          describe "a GET to :show" do
            before { get :show, params: { id: @custom_field.id } }

            it "gives me back the right custom field" do
              assert_equal @custom_field.id, JSON.parse(@response.body)[model_name + "_field"]["id"]
            end

            should_use_presenter Api::V2::CustomFieldPresenter
          end
        end

        describe 'when account does not have user_and_organization_fields' do
          before do
            @account.stubs("has_user_and_organization_fields?").returns(false)
          end

          should_be_forbidden :index, :create, :update, :destroy, :show, :reorder
        end
      end

      as_an_agent do
        describe 'when agent has permissions' do
          describe "a POST to create" do
            describe "valid requests" do
              before do
                @account.stubs("has_user_and_organization_fields?").returns(true)
                @account.stubs("has_permissions_organization_fields_manage?").returns(true)
                @account.stubs("has_permissions_user_fields_manage?").returns(true)
                Access::ExternalPermissions::Permissions.any_instance.stubs(:allow?).returns(true)
                post :create, params: { model_name + "_field" => {title: "Test", key: "test_1", type: "text"} }
              end
              it('responds with created') { assert_response :created }
              should_change("the custom field count", by: 1) { CustomField::Field.count(:all) }
            end
          end
        end
      end

      as_an_admin do
        describe 'when account has user_and_organization_fields' do
          before do
            @account.stubs("has_user_and_organization_fields?").returns(true)
          end

          describe "a POST to create" do
            describe "valid requests" do
              before { post :create, params: { model_name + "_field" => {title: "Test", key: "test_1", type: "text"} } }

              it('responds with created') { assert_response :created }
              should_change("the custom field count", by: 1) { CustomField::Field.count(:all) }
            end

            describe "invalid requests" do
              describe "with empty body" do
                before { post :create }

                it "responds with bad_request" do
                  assert_response :bad_request
                end
              end

              describe "with invalid param" do
                before { post :create, params: { "wrong_field" => {title: "Test", key: "test_1", type: "text"} } }

                it "responds with bad_request" do
                  assert_response :bad_request
                end
              end
            end

            describe "a new field" do
              before do
                send("create_#{model_name}_custom_field!", "field_decimal", "decimal field 1", "Decimal")
                send("create_#{model_name}_custom_field!", "field_integer", "integer field 1", "Integer")
                @field_positions = CustomField::Field.order(:position).pluck(:id)
              end

              describe "without a position" do
                it "sets the last position" do
                  post :create, params: { model_name + "_field" => {title: "Test", key: "test_1", type: "text"} }
                  assert_equal CustomField::Field.all.size - 1, CustomField::Field.last.position
                  CustomField::Field.order(:position).each_with_index do |field, i|
                    assert_equal field.position, i
                  end
                end
              end

              describe "with a position" do
                it "sets the correct position" do
                  positions = {}
                  (0..10).each do |position|
                    assert_difference('CustomField::Field.count(:all)', 1) do
                      post :create, params: { model_name + "_field" => {title: "Test#{11 - position}", key: "position_#{11 - position}", type: "text", position: position.to_s} }
                    end
                    field = CustomField::Field.last
                    assert_equal position, field.position
                    positions[field.id] = position
                  end
                  CustomField::Field.order(:position).each_with_index do |field, i|
                    assert_equal field.position, i
                    if field['key'].start_with?("position_")
                      assert_equal field['key'], "position_#{11 - i}"
                    end
                  end
                end
              end
            end

            describe "a new system field" do
              describe "as a system user" do
                before do
                  User.any_instance.stubs(:is_system_user?).returns(true)
                  post :create, params: { model_name + "_field" => { title: "txt.system.test", key: "system::test_1", type: "text", is_system: true } }
                end

                it('responds with created') { assert_response :created }
                it "creates the system field" do
                  assert_equal CustomField::Field.last.is_system, true
                end
              end

              describe "as a non system user" do
                before do
                  User.any_instance.stubs(:is_system_user?).returns(false)
                  post :create, params: { model_name + "_field" => { title: "txt.system.test", key: "test_1", type: "text", is_system: true } }
                end

                it('responds with created') { assert_response :created }
                it "creates the non system field" do
                  assert_equal CustomField::Field.last.is_system, false
                end
              end
            end

            describe "invalid requests" do
              before { post :create, params: { model_name + "_field" => {} } }

              it('responds with bad_request') { assert_response :bad_request }
            end

            describe "field types" do
              let(:awkward_types) { ["regexp", "tagger", "dropdown"] }
              let(:standard_types) { Zendesk::CustomField::CustomFieldManager.type_map.keys - awkward_types }
              let(:invalid_types) { %w[asdf drop_down drop-down invalid_something_or_other] }

              it "which are standard succeed" do
                standard_types.each do |type|
                  params = { "#{model_name}_field": { title: "title", type: type, key: "key_#{model_name}_#{type}" } }
                  post :create, params: params
                  assert_response :success
                end
              end

              it "which are invalid fail" do
                invalid_types.each do |type|
                  params = { "#{model_name}_field": { title: "title", type: type, key: "key_#{model_name}_#{type}" } }
                  post :create, params: params
                  assert_response :bad_request
                end
              end

              describe "with specific params" do
                it "regexp succeeds" do
                  params = { "#{model_name}_field": { title: "title", type: "regexp", key: "key", regexp_for_validation: "[0-9]+" } }
                  post :create, params: params
                  assert_response :success
                end

                ["tagger", "dropdown"].each do |type|
                  it "#{type} succeeds" do
                    params = { "#{model_name}_field": { title: "title", type: type, key: "key_#{type}", custom_field_options: [{name: "1", value: "1"}]} }
                    post :create, params: params
                    assert_response :success
                  end
                end
              end
            end

            describe "text field with tag" do
              before do
                post :create, params: { model_name + "_field" => {title: "Test", key: "test_1", type: "text", tag: "something"} }
              end

              it "fails to set tag (it doesn't support tagging)" do
                assert_equal 422, @response.status.to_i
                cf = CustomField::Field.last
                assert_nil cf.tag
              end
            end

            describe "checkbox with tag" do
              before do
                post :create, params: { model_name + "_field" => {title: "Test", key: "test_1", type: "checkbox", tag: "something"} }
              end

              it "sets tag as expected" do
                assert_equal 201, @response.status.to_i
                cf = CustomField::Field.last
                assert_equal "something", cf.tag

                # make sure tag is retreived via :show
                get :show, params: { id: cf.id }
                assert_equal 200, @response.status.to_i, @response.body
                assert_equal cf.id, JSON.parse(@response.body)[model_name + "_field"]["id"]
                assert_equal cf.tag, JSON.parse(@response.body)[model_name + "_field"]["tag"]

                # clear tag
                put :update, params: { :id => cf.id, model_name + "_field" => { tag: nil } }, as: :json
                assert_equal 200, @response.status.to_i, @response.body
                cf = CustomField::Field.last
                assert_nil cf.tag

                # make sure tag is retreived as nil via :show
                get :show, params: { id: cf.id }
                assert_equal 200, @response.status.to_i, @response.body
                assert_equal cf.id, JSON.parse(@response.body)[model_name + "_field"]["id"]
                assert_nil JSON.parse(@response.body)[model_name + "_field"]["tag"]
              end
            end

            describe "with dropdown choices" do
              before do
                post :create, params: { model_name + "_field" => {
                    title: "Test", key: "test_1", type: "dropdown",
                    custom_field_options: [{name: "1", value: "1"}, {name: "2", value: "2"}]
                  } }
              end

              it "creates cf_dropdown_choices" do
                assert_equal 201, @response.status.to_i
                cf = CustomField::Field.last
                assert_equal 2, cf.dropdown_choices.size
                assert_equal [{name: "1", value: "1"}, {name: "2", value: "2"}],
                  cf.dropdown_choices.map { |d| d.attributes.symbolize_keys.slice(:name, :value) }

                # make sure tag is not retreived for dropdown via :show
                get :show, params: { id: cf.id }
                assert_equal 200, @response.status.to_i
                assert_equal cf.id, JSON.parse(@response.body)[model_name + "_field"]["id"]
                refute JSON.parse(@response.body)[model_name + "_field"].key("tag")
              end
            end

            describe "writing to the raw fields" do
              before do
                post :create, params: { model_name + "_field" => {
                    raw_title: "Test with raw", key: "test_1", type: "dropdown",
                    custom_field_options: [{raw_name: "Raw: 1", value: "1"}, {raw_name: "raw: 2", value: "2"}]
                  } }
              end

              it "maps and create cf_dropdown_choices" do
                assert_equal 201, @response.status.to_i
                cf = CustomField::Field.last
                assert_equal "Test with raw", cf.title
                assert_equal 2, cf.dropdown_choices.size
                assert_equal [{name: "Raw: 1", value: "1"}, {name: "raw: 2", value: "2"}],
                  cf.dropdown_choices.map { |d| d.attributes.symbolize_keys.slice(:name, :value) }
              end
            end

            describe "with 2nd set of dropdown choices" do
              before do
                post :create, params: { model_name + "_field" => {
                    title: "Test 2", key: "foo", type: "dropdown",
                    custom_field_options: [{name: "3", value: "3"}, {name: "4", value: "4"}]
                  } }
              end

              it "fails to create cf_dropdown_choices with a duplicate key" do
                assert_equal 422, @response.status.to_i
                cf = CustomField::Field.last
                assert_equal "Foo Bar {{dc.dc_item}}", cf.title
              end
            end
          end

          describe "a PUT to :update" do
            describe "with valid parameters" do
              describe "as a non system user updating a non system field" do
                before do
                  put :update, params: { :id => @custom_field.id, model_name + "_field" => { title: "Tell me about yourself", active: false, position: 10 } }
                end

                should_use_presenter Api::V2::CustomFieldPresenter

                it "changes the record" do
                  body = JSON.parse(@response.body)
                  assert_equal "Tell me about yourself", body[model_name + "_field"]["title"]
                  assert_equal false, body[model_name + "_field"]["active"]
                  assert_equal 10, body[model_name + "_field"]["position"]
                end
              end

              describe "for a system field" do
                before do
                  @system_field = accounts(:minimum).custom_fields.send(scope).build(title: "txt.something", key: "system::something", is_system: true)
                  @system_field.type = "Text"
                  @system_field.save!
                  I18n.stubs(:t).with('txt.something_v2').returns('txt.something_v2_translated')
                  I18n.stubs(:t).with('txt.something').returns('txt.something_translated')
                end

                describe "as a non system user" do
                  before do
                    put :update, params: { :id => @system_field.id, model_name + "_field" => { title: "txt.something_v2", active: false, position: 11 } }
                    @field_body = JSON.parse(@response.body)[model_name + "_field"]
                  end

                  it "does not change the record" do
                    assert_not_equal "txt.something_v2_translated", @field_body["title"]
                  end

                  it "is able to change active status" do
                    assert_equal false, @field_body["active"]
                  end

                  it "is able to change position" do
                    assert_equal 11, @field_body["position"]
                  end
                end

                describe "as a system user" do
                  before do
                    User.any_instance.stubs(:is_system_user?).returns(true)
                    put :update, params: { :id => @system_field.id, model_name + "_field" => { title: "txt.something_v2", active: false } }
                  end

                  it "changes the record" do
                    body = JSON.parse(@response.body)
                    assert_equal "txt.something_v2_translated", body[model_name + "_field"]["title"]
                    assert_equal false, body[model_name + "_field"]["active"]
                  end
                end
              end
            end

            describe "with a removed dropdown" do
              before do
                send("create_#{model_name}_custom_field!", "field_dropdown", "dropdown field", "Dropdown")
                dropdown = @account.custom_fields.last
                dropdown.dropdown_choices.create(name: "testing", value: "test", position: 1)

                put :update, params: { :id => dropdown.id, "#{model_name}_field" => { custom_field_options: []} }, as: :json
              end

              it "removes dropdown field options" do
                body = JSON.parse(@response.body)
                assert_empty(body["#{model_name}_field"]["custom_field_options"])
              end
            end

            describe "with a new field containing a deleted tag" do
              before do
                send("create_#{model_name}_custom_field!", "field_dropdown_1", "dropdown field 1", "Dropdown")
                dropdown = @account.custom_fields.last
                @dropdown_choice = dropdown.dropdown_choices.create(
                  name: "testing",
                  value: "test",
                  position: 0
                )
                @dropdown_choice.soft_delete!
                @dropdown_choice.save!

                field_options = [
                  { id: "", value: "test", name: "buggery" }
                ]
                put :update, params: { :id => dropdown.id, "#{model_name}_field" => { custom_field_options: field_options } }
              end

              it "undeletes the stored field with the tag" do
                body = JSON.parse(@response.body)
                assert_equal body["#{model_name}_field"]["custom_field_options"], [{
                  "id" => @dropdown_choice.id,
                  "name" => "buggery",
                  "raw_name" => "buggery",
                  "value" => "test"
                }]
              end
            end
          end

          describe "a PUT to :reorder" do
            describe "with valid parameters" do
              before do
                send("create_#{model_name}_custom_field!", "field_dropdown", "dropdown field", "Dropdown")
                @account.custom_fields.last. # dropdown is invalid without any choices
                  dropdown_choices.create(name: "testing", value: "test", position: 1)
                send("create_#{model_name}_custom_field!", "field_checkbox", "checkbox field", "Checkbox")
                send("create_#{model_name}_custom_field!", "field_textarea", "textarea field", "Textarea")
                send("create_#{model_name}_custom_field!", "field_integer", "integer field", "Integer")
                @account.custom_fields.send("for_#{model_name}").each do |custom_field|
                  assert custom_field.valid?
                end
                put :reorder, params: { "#{model_name}_field_ids" => @account.custom_fields.send("for_#{model_name}").map(&:id).reverse }
              end

              it('responds with ok') { assert_response :ok }

              it "sets new position values based on parameters" do
                @account.reload.custom_fields.send("for_#{model_name}").reverse.each_with_index do |c, i|
                  assert_equal i, c.position
                end
              end
            end
          end

          describe "a DELETE to :destroy" do
            describe "a non system field" do
              before { delete :destroy, params: { id: @custom_field.id } }
              it('responds with no_content') { assert_response :no_content }
              should_change("the number of custom fields", by: -1) { CustomField::Field.count(:all) }
            end

            describe "a system field" do
              before do
                CustomField::Text.any_instance.stubs(:is_system?).returns(true)
              end

              describe "by a system user" do
                before do
                  User.any_instance.stubs(:is_system_user?).returns(true)
                  delete :destroy, params: { id: @custom_field.id }
                end
                it('responds with no_content') { assert_response :no_content }
                should_change("the number of custom fields", by: -1) { CustomField::Field.count(:all) }
              end

              describe "by a non system user" do
                before do
                  delete :destroy, params: { id: @custom_field.id }
                end
                it('responds with forbidden') { assert_response :forbidden }
                should_not_change("the number of custom fields") { CustomField::Field.count(:all) }
              end
            end
          end
        end

        describe 'when account does not have user_and_organization_fields' do
          before do
            @account.stubs("has_user_and_organization_fields?").returns(false)
          end

          should_be_forbidden :index, :create, :update, :destroy, :show, :reorder
        end
      end
    end
  end
end

class Api::V2::UserFieldsControllerTest < Api::V2::CustomFieldsControllerTest
  tests Api::V2::UserFieldsController

  def self.model_name
    "user"
  end

  define_tests

  describe "usercontroller" do
    shared_setup

    as_an_agent do
      describe "adds cache-control headers for apps" do
        before do
          @request.headers["X-Zendesk-App-Id"] = "b4dApp"
        end

        describe_with_arturo_enabled :user_fields_cache_control_header do
          it "responds with success and with cache headers" do
            get :index
            assert_response :success
            assert_equal @response.headers['Cache-Control'], 'max-age=60, private'
          end
        end

        describe_with_arturo_disabled :user_fields_cache_control_header do
          it "responds with success and no cache headers" do
            get :index
            assert_response :success
            assert_equal @response.headers['Cache-Control'], 'max-age=0, private, must-revalidate'
          end
        end
      end
    end

    describe "embeddable system user" do
      let(:create_payload) do
        {
          user_field: {
            key: "system::a_key",
            is_system: true,
            type: "date",
            title: "txt.embeddable.nomine",
            description: "txt.embeddable.et_descriptionem"
          }
        }
      end

      as_a_subsystem_user(account: :minimum, user: "embeddable") do
        should_be_forbidden :show, :update, :destroy

        should_be_authorized do
          post :create, params: create_payload
          get :index
        end

        describe "a POST to :create" do
          before { post :create, params: create_payload }
          it("responds with created") { assert_response :created }
        end

        describe "a GET to :index" do
          before { get :index }
          it("responds with ok") { assert_response :ok }
        end
      end

      as_a_subsystem_user(account: :minimum, user: "bime") do
        should_be_authorized do
          @account.stubs("has_user_and_organization_fields?").returns(false)
          get :index
        end
      end
    end

    describe "when the user field has an apps requirement" do
      before do
        FactoryBot.create(:resource_collection_resource,
          account_id: @account.id,
          resource: @custom_field)
      end

      as_an_admin do
        describe "a PUT to :update" do
          before do
            put :update, params: { :id => @custom_field.id, model_name + "_field" => { title: "Tell me about yourself" } }
          end

          should_not_change("the title of the user field") { @custom_field.reload.title }
        end

        describe "a DELETE to :destroy" do
          before do
            delete :destroy, params: { id: @custom_field.id }
          end

          should_not_change("the number of user fields") { CustomField::Field.count(:all) }
        end
      end
    end
  end
end

class Api::V2::OrganizationFieldsControllerTest < Api::V2::CustomFieldsControllerTest
  tests Api::V2::OrganizationFieldsController

  def self.model_name
    "organization"
  end

  define_tests

  describe "OrganizationFieldController" do
    shared_setup

    as_an_agent do
      describe "adds cache-control headers for apps" do
        before do
          @request.headers["X-Zendesk-App-Id"] = "b4dApp"
        end

        describe_with_arturo_enabled :organization_fields_cache_control_header do
          it "responds with success and with cache headers" do
            get :index
            assert_response :success
            assert_equal @response.headers['Cache-Control'], 'max-age=60, private'
          end
        end

        describe_with_arturo_disabled :organization_fields_cache_control_header do
          it "responds with success and no cache headers" do
            get :index
            assert_response :success
            assert_equal @response.headers['Cache-Control'], 'max-age=0, private, must-revalidate'
          end
        end
      end
    end
  end
end
