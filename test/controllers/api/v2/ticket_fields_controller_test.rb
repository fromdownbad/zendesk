require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::TicketFieldsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :all

  before do
    accept :json
    @ticket_field = ticket_fields(:field_text_custom)
    @account = @ticket_field.account
  end

  with_options(controller: 'api/v2/ticket_fields') do |request|
    request.should_route :post,   '/api/v2/ticket_fields',           action: 'create'
    request.should_route :get,    '/api/v2/ticket_fields',           action: 'index'
    request.should_route :put,    '/api/v2/ticket_fields/123',       action: 'update',  id: '123'
    request.should_route :get,    '/api/v2/ticket_fields/123',       action: 'show',    id: '123'
    request.should_route :delete, '/api/v2/ticket_fields/123',       action: 'destroy', id: '123'
    request.should_route :get,    '/api/v2/ticket_fields/show_many', action: 'show_many'
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :create, :update, :destroy, :show
  end

  as_an_end_user do
    should_be_forbidden :index, :create, :update, :destroy, :show
  end

  as_a_subsystem_user(account: :minimum, user: "embeddable") do
    should_be_forbidden :show, :create, :update, :destroy

    describe "a GET to :index" do
      before { get :index }
      it("responds with ok") { assert_response :ok }
    end
  end

  as_a_subsystem_user(account: :minimum, user: "gooddata") do
    should_be_forbidden :create, :update, :destroy

    describe "a GET to :index" do
      before { get :index }
      it('responds with ok') { assert_response :ok }
    end

    describe "a GET to :show" do
      before { get :show, params: { id: @ticket_field.id } }
      it('responds with ok') { assert_response :ok }
    end
  end

  as_a_subsystem_user(account: :minimum, user: "answer_bot_flow_composer") do
    should_be_forbidden :create, :update, :destroy

    describe "a GET to :index" do
      before { get :index }
      it('responds with ok') { assert_response :ok }
    end

    describe "a GET to :show" do
      before { get :show, params: { id: @ticket_field.id } }
      it('responds with ok') { assert_response :ok }
    end
  end

  as_a_subsystem_user(account: :minimum, user: "answer_bot_flow_director") do
    should_be_forbidden :create, :update, :destroy

    describe "a GET to :index" do
      before { get :index }
      it('responds with ok') { assert_response :ok }
    end

    describe "a GET to :show" do
      before { get :show, params: { id: @ticket_field.id } }
      it('responds with ok') { assert_response :ok }
    end
  end

  as_a_subsystem_user(account: :minimum, user: "bime") do
    should_be_authorized do
      get :show, params: { id: @ticket_field.id }
      get :index
    end
    should_be_forbidden :create, :update, :destroy
  end

  as_an_agent do
    should_be_forbidden :create, :update, :destroy

    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::TicketFieldPresenter

      it "shoulds return active fields" do
        active_field = JSON.parse(@response.body)["ticket_fields"].detect { |ticket_field| ticket_field["id"] == ticket_fields(:minimum_field_status).id }
        assert active_field["active"]
      end

      it "returns inactive fields" do
        inactive_field = JSON.parse(@response.body)["ticket_fields"].detect { |ticket_field| ticket_field["id"] == ticket_fields(:minimum_field_subject).id }
        refute inactive_field["active"]
      end

      it "uses ETag" do
        assert_etagged :index do
          ticket_field = ticket_fields(:minimum_field_subject)
          ticket_field.update_attribute(:updated_at, 1.minute.from_now)
          Account.any_instance.stubs(:ticket_fields).returns [ticket_field]
          @controller.instance_variable_set(:@ticket_fields, nil)
        end
      end

      it "changes etag when changing field option" do
        assert_etagged :index do
          ticket_field = ticket_fields(:field_tagger_custom)
          ticket_field.custom_field_options[0].value = "changed"
          ticket_field.save!
          Account.any_instance.stubs(:ticket_fields).returns [ticket_field]
          @controller.instance_variable_set(:@ticket_fields, nil)
        end
      end

      it "does not use ETag when using includes" do
        etag = @response.headers['ETag']
        assert etag

        # Gets memoized in ZendeskApi::Controller::Sideloading, needs to be cleared as were sending two requests in sequence
        @controller.instance_variable_set('@includes', nil)

        # different response
        get :index, params: { include: "foo" }
        assert_response :ok
        refute_equal etag, @response.headers['ETag']
      end

      it "with the creator parameter, does not show the creator to agents" do
        get :index, params: { creator: true }
        assert_response :ok
        creators_shown = JSON.parse(@response.body)["ticket_fields"].detect { |ticket_field| ticket_field.key?("creator_user_id") }
        app_names_shown = JSON.parse(@response.body)["ticket_fields"].detect { |ticket_field| ticket_field.key?("creator_app_name") }
        assert_nil creators_shown
        assert_nil app_names_shown
      end
    end

    describe 'a GET to :index with valid external domain' do
      it 'returns a valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
        get :index
        assert_equal @response.headers['Access-Control-Allow-Origin'], 'http://app.futuresimple.com'
      end

      it 'returns a not valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
        get :index
        assert_nil @response.headers['Access-Control-Allow-Origin']
      end
    end

    describe "a GET to :index with the system parameter" do
      let(:presenter) { Api::V2::TicketFieldPresenter.new(users(:minimum_agent), url_builder: @controller, includes: []) }
      let(:system_fields) { @account.ticket_fields.system_fields }

      before { get :index, params: { only_system_fields: true } }

      it "only shows system fields" do
        assert_response :ok
        response_types = JSON.parse(@response.body)["ticket_fields"].map { |tf| tf['type'] }
        system_types = presenter.present(system_fields)[:ticket_fields].map { |tf| tf[:type] }
        assert_equal response_types.sort, system_types.sort
      end
    end

    describe "a GET to :show_many" do
      let(:ticket_field_2) { ticket_fields(:field_tagger_custom) }
      let(:ticket_field_3) { ticket_fields(:field_checkbox_custom) }
      let(:ids) do
        [
          @ticket_field,
          ticket_field_2,
          ticket_field_3
        ].map(&:id)
      end

      before { get :show_many, params: { ids: ids.join(",") } }

      should_use_presenter Api::V2::TicketFieldPresenter, status: 200

      it "only present requested ticket fields" do
        res = JSON.parse(@response.body)['ticket_fields']
        assert_equal 3, res.count
        assert_equal ids.sort, res.map { |ticket_field| ticket_field['id'] }.sort
      end

      it "should have CORS headers" do
        assert_equal "*", response.headers["Access-Control-Allow-Origin"]
      end
    end

    describe "a GET to :show" do
      before { get :show, params: { id: @ticket_field.id } }
      should_use_presenter Api::V2::TicketFieldPresenter
    end

    it "a GET to :show with the creator paramater, does not show the creator to agents" do
      get :show, params: { id: @ticket_field.id, creator: true }
      assert_response :ok
      creators_shown = JSON.parse(@response.body)["ticket_field"].key?("creator_user_id")
      app_name_shown = JSON.parse(@response.body)["ticket_field"].key?("creator_app_name")
      assert_equal false, creators_shown
      assert_equal false, app_name_shown
    end
  end

  as_an_admin do
    describe "a POST to :create" do
      describe "with no custom field options" do
        before { post :create, params: { ticket_field: { type: "textarea", title: "Tell us about you" } } }
        should_use_presenter Api::V2::TicketFieldPresenter, status: :created, location: /\/api\/v2\/ticket_fields\/\d+\.json$/
        should_change("the ticket field count", by: 1) { TicketField.count(:all) }
      end

      describe "with custom field options" do
        before do
          post :create, params: { ticket_field: { type: "tagger",
                                                  title: "How would you like it served?", custom_field_options: options } }
        end

        describe "valid" do
          let(:options) { [{name: "Cold", value: "fish"}, {name: "Hot", value: "hot"}] }

          it('responds with created') { assert_response :created }

          it "assigns custom field options" do
            refute assigns(:new_ticket_field).custom_field_options.empty?
          end

          it "has the same values" do
            options = assigns(:new_ticket_field).custom_field_options.map do |cfo|
              { name: cfo.name, value: cfo.value }
            end

            assert_equal options, options
          end
        end

        describe "with raw values" do
          let(:options) { [{raw_name: "Cold raw name", value: "fish"}, {raw_name: "Hot raw name", value: "hot"}] }

          it "is valid" do
            options = assigns(:new_ticket_field).custom_field_options.map do |cfo|
              { name: cfo.name, value: cfo.value }
            end

            expected = [{name: "Cold raw name", value: "fish"}, {name: "Hot raw name", value: "hot"}]
            assert_equal expected, options
          end
        end

        describe "invalid" do
          let(:options) { [{name: "Invalid"}] }

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
        end
      end

      describe "#ignore_agent_description" do
        let(:params) { {ticket_field: { title: "Wombat", agent_description: "wombats for agents" }} }

        it "is not called if no agent_description is sent in the parameters" do
          TicketFieldsController.any_instance.expects(:ignore_agent_description).never
          post :create, params: { ticket_field: { type: "textarea", title: "Wombat" } }
        end

        it "allows the agent_description parameter" do
          new_ticket_field = Zendesk::Tickets::TicketFieldManager.new(@account).build(title: 'Wombat', agent_description: 'wombats for agents')
          Zendesk::Tickets::TicketFieldManager.any_instance.expects(:build).with('title' => 'Wombat', 'agent_description' => 'wombats for agents').returns(new_ticket_field)
          post :create, params: params
        end

        it "sets the agent_description on the ticket field during creation" do
          post :create, params: params
          body = JSON.parse(@response.body)
          assert_equal "wombats for agents", body["ticket_field"]["agent_description"]
        end
      end

      describe "validating parameters" do
        let(:valid_types) { Zendesk::Tickets::TicketFieldManager.type_map.keys }
        let(:incorrect_types) { %w[asdf dropdown drop_down drop-down invalid_something_or_other] }
        let(:system_types) { %w[subject description status tickettype priority group assignee] }
        let(:invalid_types) { incorrect_types + system_types }

        it "succeeds" do
          valid_types.each do |type|
            params = { ticket_field: { title: "title", type: type } }
            post :create, params: params
            assert_response :success
          end
        end

        it "fails" do
          invalid_types.each do |type|
            params = { ticket_field: { title: "title", type: type } }
            post :create, params: params
            assert_response :bad_request
          end
        end
      end
    end

    describe "a PUT to :update" do
      let(:field_tagger) { ticket_fields(:field_tagger_custom) }
      let(:text_field) { FieldText.create!(account: @account, title: 'text field', is_active: true) }

      describe "Text Field" do
        let(:new_options) { [{"name" => "Cold", "raw_name" => "Cold", "value" => "cold", "default" => false}] }

        it 'should return an error when you pass custom_field_options' do
          put :update, params: { id: text_field.id, ticket_field: { custom_field_options: new_options } }
        end

        it 'should not return an error when you dont pass custom_field_options' do
          put :update, params: { id: text_field.id, ticket_field: { title: 'text field2' } }
        end
      end

      describe "Field Tagger" do
        before { @original_options = field_tagger.custom_field_options }

        describe "with valid parameters" do
          before { put :update, params: { id: field_tagger.id, ticket_field: { title: "Tell us about your tea preference", required: true } } }

          should_use_presenter Api::V2::TicketFieldPresenter

          it "changes the record" do
            body = JSON.parse(@response.body)
            assert_equal "Tell us about your tea preference", body["ticket_field"]["title"]
            assert(body["ticket_field"]["required"])
          end
        end

        describe "with no parameters" do
          before { put :update, params: { id: @ticket_field.id } }
          should_use_presenter Api::V2::TicketFieldPresenter
        end

        describe "when the ticket field has an apps requirement" do
          before do
            FactoryBot.create(:resource_collection_resource,
              account_id: field_tagger.account.id,
              resource: field_tagger)
            put :update, params: { id: field_tagger.id, ticket_field: { title: "Tell us about your tea preference", required: true } }
          end

          should_not_change("the title of the ticket field") { @ticket_field.reload.title }
        end

        describe "#ignore_agent_description" do
          let(:params) { {ticket_field: { title: "Wombat", agent_description: "wombats for agents" }} }

          it "is not called if no agent_description is sent in the parameters" do
            TicketFieldsController.any_instance.expects(:ignore_agent_description).never
            post :update, params: { id: field_tagger.id, ticket_field: { title: "Wombat" } }
          end

          it "allows the agent_description parameter" do
            updated_ticket_field = Zendesk::Tickets::TicketFieldManager.new(@account).update(field_tagger, title: 'Wombat', agent_description: 'wombats for agents')
            Zendesk::Tickets::TicketFieldManager.any_instance.expects(:update).with(field_tagger, 'title' => 'Wombat', 'agent_description' => 'wombats for agents').returns(updated_ticket_field)
            post :update, params: { id: field_tagger.id, ticket_field: { title: "Wombat", agent_description: "wombats for agents" } }
          end

          it "sets the agent_description on the ticket field during update" do
            post :update, params: { id: field_tagger.id, ticket_field: { title: "Wombat", agent_description: "wombats for agents" } }
            body = JSON.parse(@response.body)
            assert_equal "wombats for agents", body["ticket_field"]["agent_description"]
          end
        end

        describe "with custom field options" do
          let(:option) { field_tagger.custom_field_options.first }

          describe "with new options" do
            let(:new_options) { [{"name" => "Cold", "raw_name" => "Cold", "value" => "cold", "default" => false}, {"name" => "Hot", "raw_name" => "Hot", "value" => "hot", "default" => false}] }

            before { put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: new_options } } }

            it('responds with success') { assert_response :success }

            it "replaces custom field_options" do
              assert_equal new_options, JSON.parse(@response.body)["ticket_field"]["custom_field_options"].map { |option| option.delete("id"); option }
            end

            it "softs delete the original options" do
              new_ids = JSON.parse(@response.body)["ticket_field"]["custom_field_options"].map { |option| option["id"] }
              @original_options = field_tagger.custom_field_options.where("id not in (?)", new_ids)
              assert @original_options.all?(&:deleted?)
            end
          end

          describe 'cache expiration' do
            let(:new_options) { [{"name" => "Cold", "raw_name" => "Cold", "value" => "cold", "default" => false}, {"name" => "Hot", "raw_name" => "Hot", "value" => "hot", "default" => false}] }

            it 'should expire cache only once' do
              Account.any_instance.expects(:expire_scoped_cache_key).with(:settings).once
              Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_fields).once
              Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_forms).once
              Account.any_instance.expects(:expire_scoped_cache_key).with(:field_taggers).once
              Account.any_instance.expects(:expire_scoped_cache_key).with([:field_tagger, field_tagger.id]).once

              put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: new_options } }
            end
          end

          describe "soft deletion" do
            before { CustomFieldOption.soft_delete_all!(field_tagger.custom_field_options) }

            describe "when updating an option with the same value as a deleted option" do
              before do
                assert option.reload.deleted?
                put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: [{value: option.value, raw_name: "new_name", name: "new_name"}] } }
              end

              it "undeletes the deleted option and update it" do
                refute option.reload.deleted?
                assert_equal [{ "id" => option.id, "name" => "new_name", "raw_name" => "new_name", "value" => option.value, "default" => false}],
                  JSON.parse(@response.body)["ticket_field"]["custom_field_options"]
              end
            end

            describe "when updating an option with the same value as a deleted option" do
              before do
                assert option.reload.deleted?
                put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: [{ id: option.id, value: "booring", raw_name: "Booring", name: "Booring"}] } }
              end

              it "undeletes the deleted option" do
                refute option.reload.deleted?
                assert_equal [{ "id" => option.id, "name" => "Booring", "raw_name" => "Booring", "value" => "booring", "default" => false}],
                  JSON.parse(@response.body)["ticket_field"]["custom_field_options"]
              end
            end
          end

          describe "with no raw_name and matching value" do
            let(:option_without_raw_name) { [{ "id" => option.id, "name" => "test", "value" => "test_value" }] }

            before { put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: option_without_raw_name } } }

            it "updates option and use name for the raw_name value" do
              response_field_option = JSON.parse(@response.body)["ticket_field"]["custom_field_options"][0]
              assert_equal 'test', response_field_option['raw_name']
              assert_equal 'test_value', response_field_option['value']
            end
          end

          describe "with raw_name and matching value" do
            let(:option_with_raw_name) { [{ "id" => option.id, "name" => "test", "raw_name" => "test_raw_name", "value" => "test_value" }] }

            before { put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: option_with_raw_name } } }

            it "updates option and override name with raw_name" do
              response_field_option = JSON.parse(@response.body)["ticket_field"]["custom_field_options"][0]
              assert_equal 'test_raw_name', response_field_option['name']
              assert_equal 'test_value', response_field_option['value']
            end
          end

          describe "with matching options and a new option" do
            before do
              @old_options_size = field_tagger.custom_field_options.size
              new_field_options = field_tagger.custom_field_options.map { |option| {id: option.id, name: option.name, value: option.value } } + [{ name: "new name", value: "new_value" }]
              put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: new_field_options } }
            end

            it "adds the new option" do
              assert_equal @old_options_size + 1, JSON.parse(@response.body)["ticket_field"]["custom_field_options"].size
            end
          end

          describe "with matching options a new option and some new ordering" do
            before do
              @current_options_map = field_tagger.custom_field_options.map { |option| {id: option.id, name: option.name, value: option.value } }
              @new_option = [{ name: "new name", value: "new_value" }]
              @second_new_option = [{ name: "scond new name", value: "second_new_value" }]
            end

            it "option get position 0 if it's the first option in the params" do
              new_field_options = @new_option + @current_options_map
              put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: new_field_options } }
              assert_equal 0, field_tagger.custom_field_options.reload.find_by_value('new_value').position
            end

            it "option get position 4 if it's the last option in the params" do
              new_field_options = @current_options_map + @new_option
              put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: new_field_options } }
              assert_equal 4, field_tagger.custom_field_options.reload.find_by_value('new_value').position
            end

            it "positioning is correct also when adding more than one option" do
              new_field_options = @second_new_option + @current_options_map + @new_option
              put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: new_field_options } }
              assert_equal 0, field_tagger.custom_field_options.reload.find_by_value('second_new_value').position
              assert_equal 5, field_tagger.custom_field_options.reload.find_by_value('new_value').position
            end
          end

          describe "with new ordering on existing options" do
            before do
              old_time = Time.local(2015, 12, 7, 00, 00, 00)
              Timecop.travel(old_time) do
                @custom_tagger = FieldTagger.new(account: accounts(:minimum), title: "Wombat Dropdown")
                @custom_tagger.custom_field_options.build(name: "first wombat", value: "1_wombat")
                @custom_tagger.custom_field_options.build(name: "second wombat", value: "2_wombat")
                @custom_tagger.custom_field_options.build(name: "third wombat", value: "3_wombat")
                @custom_tagger.save!
              end
            end

            it "reorders the existing options and returns updated payload" do
              assert_equal ["1_wombat", "2_wombat", "3_wombat"], @custom_tagger.custom_field_options.map(&:value)
              new_order = @custom_tagger.custom_field_options.map { |option| {id: option.id, name: option.name, value: option.value } }.reverse
              old_timestamp = @custom_tagger.updated_at

              put :update, params: { id: @custom_tagger.id, ticket_field: { custom_field_options: new_order} }
              assert_equal ["3_wombat", "2_wombat", "1_wombat"], JSON.parse(@response.body)["ticket_field"]["custom_field_options"].collect { |o| o["value"] }
              assert_not_equal old_timestamp.to_datetime.to_s, JSON.parse(@response.body)["ticket_field"]["updated_at"].to_datetime.to_s
            end
          end

          describe "invalid" do
            describe "with an option with no value" do
              before { put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: [{name: "Invalid"}] } } }

              should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

              it "does not modify any options" do
                assert_equal @original_options, field_tagger.reload.custom_field_options
              end
            end

            describe "with an id parameter not matching any current option" do
              before do
                new_field_options = field_tagger.custom_field_options.map { |option| {id: option.id, name: option.name, value: option.value } }
                new_field_options.first[:id] = CustomFieldOption.maximum(:id) + 1
                put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: new_field_options } }
              end

              it('ignores id and responds with success') { assert_response :success }

              it "modifies options by value" do
                assert_equal @original_options.first.name, field_tagger.reload.custom_field_options.first.name
              end
            end

            describe "with duplicate option ids" do
              before do
                id = field_tagger.custom_field_options.first.id
                new_field_options = field_tagger.custom_field_options.map { |option| {id: id, name: option.name, value: option.value } }
                put :update, params: { id: field_tagger.id, ticket_field: { custom_field_options: new_field_options } }
              end

              it('ignores id and responds with success') { assert_response :success }

              it "modifies options by value" do
                assert_equal @original_options.first.name, field_tagger.reload.custom_field_options.first.name
              end
            end
          end

          describe "when there are conditions using options" do
            let(:account) { accounts(:minimum) }
            let(:ticket_form) { ticket_forms(:minimum_ticket_form) }
            let(:parent_field) { ticket_fields(:field_tagger_custom) }
            let(:child_field) { ticket_fields(:field_textarea_custom) }
            let(:cfo1) { custom_field_options(:field_tagger_custom_option_1) }
            let(:cfo2) { custom_field_options(:field_tagger_custom_option_2) }
            let(:user) { users(:minimum_admin) }
            let(:condition) do
              condition = TicketFieldCondition.new.tap do |c|
                c.account = account
                c.ticket_form = ticket_form
                c.parent_field = parent_field
                c.child_field = child_field
                c.value = cfo2.value
                c.user_type = :agent
              end
              condition.save

              condition
            end

            before do
              ticket_form.account_id = account.id
              Account.any_instance.stubs(:has_ticket_forms?).returns(true)
              Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(true)

              TicketFormField.create(
                ticket_form: ticket_form,
                account: account,
                ticket_field: parent_field,
                position: 1
              )
              TicketFormField.create(
                ticket_form: ticket_form,
                account: account,
                ticket_field: child_field,
                position: 2
              )

              condition
            end

            it "doesn't allow value updates on options used in conditions" do
              put :update, params: { id: parent_field.id, ticket_field: {
                custom_field_options: [
                  { value: cfo1.value, name: cfo1.name },
                  { value: "changed", name: cfo2.name }
                ]
              } }

              assert_response :unprocessable_entity
              response = JSON.parse(@response.body)
              assert_equal "Field options: #{I18n.t('txt.admin.models.ticket_field.ticket_field.option_cannot_be_changed_by_condition', option_name: cfo2.name)}", response["details"]["field_options"].first["description"]
            end

            it "doesn't allow deleting options used in conditions" do
              put :update, params: { id: parent_field.id, ticket_field: {
                custom_field_options: [
                  { value: cfo1.value, name: cfo1.name },
                ]
              } }

              assert_response :unprocessable_entity
              response = JSON.parse(@response.body)
              assert_equal "Field options: #{I18n.t('txt.admin.models.ticket_field.ticket_field.option_cannot_be_changed_by_condition', option_name: cfo2.name)}", response["details"]["field_options"].first["description"]
            end

            it "allows name updates on options used in conditions" do
              put :update, params: { id: parent_field.id, ticket_field: {
                custom_field_options: [
                  { value: cfo1.value, name: cfo1.name },
                  { value: cfo2.value, name: "changed" }
                ]
              } }

              assert_response :ok
            end

            it "allows value updates on other options not used in conditions" do
              put :update, params: { id: parent_field.id, ticket_field: {
                custom_field_options: [
                  { value: "changed", name: cfo1.name },
                  { value: cfo2.value, name: cfo2.name }
                ]
              } }

              assert_response :ok
            end

            it "allows deleting other options not used in conditions" do
              put :update, params: { id: parent_field.id, ticket_field: {
                custom_field_options: [
                  { value: cfo2.value, name: cfo2.name },
                ]
              } }

              assert_response :ok
            end
          end
        end
      end
    end

    describe "a DELETE to :destroy" do
      describe "when the ticket field does not have an apps requirement" do
        before { delete :destroy, params: { id: @ticket_field.id } }
        it('responds with no_content') { assert_response :no_content }
        should_change("the number of ticket fields", by: -1) { TicketField.count(:all) }
      end

      describe "when the ticket field has an apps requirement" do
        before do
          FactoryBot.create(:resource_collection_resource,
            account_id: @ticket_field.account.id,
            resource: @ticket_field)
          delete :destroy, params: { id: @ticket_field.id }
        end

        should_not_change("the number of ticket fields") { TicketField.count(:all) }
      end
    end

    describe "a GET to :show" do
      describe_with_arturo_enabled :ticket_field_created_by do
        it "with the creator paramater, shows the creator of the field" do
          get :show, params: { id: @ticket_field.id, creator: true }
          assert_response :ok
          creators_shown = JSON.parse(@response.body)["ticket_field"].key?("creator_user_id")
          app_name_shown = JSON.parse(@response.body)["ticket_field"].key?("creator_app_name")
          assert(creators_shown)
          assert(app_name_shown)
        end

        it "sideloads user with the creator parameter and sideload requested" do
          get :show, params: { id: @ticket_field.id, creator: true, include: "users" }
          assert_response :ok
          assert(JSON.parse(@response.body).key?("users"))
        end

        it "with the creator parameter set to something invalid returns a bad request" do
          TicketFieldsController.any_instance.expects(:set_creator_options).never
          get :show, params: { id: @ticket_field.id, creator: '' }
          assert_response :bad_request
        end

        it "with the creator parameter set to false does not show creator to agents" do
          TicketFieldsController.any_instance.expects(:set_creator_options).never
          get :show, params: { id: @ticket_field.id, creator: false }
          assert_response :ok
        end
      end

      describe_with_arturo_disabled :ticket_field_created_by do
        it "a GET to :show with the creator paramater, does not show the creator to agents" do
          get :show, params: { id: @ticket_field.id, creator: true }
          assert_response :ok
          creators_shown = JSON.parse(@response.body)["ticket_field"].key?("creator_user_id")
          app_name_shown = JSON.parse(@response.body)["ticket_field"].key?("creator_app_name")
          assert_equal false, creators_shown
          assert_equal false, app_name_shown
        end

        it "does not sideload user with the creator parameter and sideload requested" do
          get :show, params: { id: @ticket_field.id, creator: true, include: "users" }
          assert_response :ok
          assert_equal false, JSON.parse(@response.body).key?("users")
        end
      end
    end

    describe "a GET to :index" do
      describe_with_arturo_enabled :ticket_field_created_by do
        it "with the creator paramater, shows the creator of the field" do
          get :index, params: { creator: true }
          assert_response :ok
          creators_shown = JSON.parse(@response.body)["ticket_fields"].detect { |ticket_field| ticket_field.key?("creator_user_id") }
          app_names_shown = JSON.parse(@response.body)["ticket_fields"].detect { |ticket_field| ticket_field.key?("creator_app_name") }
          assert creators_shown
          assert app_names_shown
        end

        it "sideloads user with the creator parameter and sideload requested" do
          get :index, params: { creator: true, include: "users" }
          assert_response :ok
          assert(JSON.parse(@response.body).key?("users"))
        end

        it "with the creator parameter set to something invalid returns a bad request" do
          TicketFieldsController.any_instance.expects(:set_creator_options).never
          get :index, params: { creator: '' }
          assert_response :bad_request
        end

        it "with the creator parameter set to false does not show creator to agents" do
          TicketFieldsController.any_instance.expects(:set_creator_options).never
          get :index, params: { creator: false }
          assert_response :ok
        end
      end

      describe_with_arturo_disabled :ticket_field_created_by do
        it "with the creator paramater, does not show the creator to agents" do
          get :index, params: { creator: true }
          assert_response :ok
          creators_shown = JSON.parse(@response.body)["ticket_fields"].detect { |ticket_field| ticket_field.key?("creator_user_id") }
          app_names_shown = JSON.parse(@response.body)["ticket_fields"].detect { |ticket_field| ticket_field.key?("creator_app_name") }
          assert_nil creators_shown
          assert_nil app_names_shown
        end

        it "does not sideload user with the creator parameter and sideload requested" do
          get :index, params: { creator: true, include: "users" }
          assert_response :ok
          assert_equal false, JSON.parse(@response.body).key?("users")
        end
      end
    end

    describe "#set_creator_options" do
      let(:user) { @account.owner }
      before { login(user) }

      it "is not called if no creator param is sent in the parameters" do
        TicketFieldsController.any_instance.expects(:set_creator_options).never
        get :index
      end

      describe_with_arturo_enabled(:ticket_field_created_by) do
        it "adds the creator option to the presenter options" do
          Api::V2::TicketFieldsController.any_instance.expects(:presenter_options).returns(url_builder: @controller, includes: [])
          Api::V2::TicketFieldsController.any_instance.expects(:presenter_options).returns(url_builder: @controller, includes: [], creator: true)
          get :index, params: { creator: true }
        end
      end

      describe_with_arturo_disabled(:ticket_field_created_by) do
        it "ignores the creator parameter" do
          Api::V2::TicketFieldsController.any_instance.expects(:presenter_options).returns(url_builder: @controller, includes: [])
          get :index, params: { creator: true }
        end
      end
    end
  end
end
