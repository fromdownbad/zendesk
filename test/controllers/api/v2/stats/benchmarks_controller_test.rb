require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Stats::BenchmarksController do
  extend Api::V2::TestHelper

  before do
    accept :json
  end

  with_options(controller: "api/v2/stats/benchmarks") do |request|
    request.should_route :get, "/api/v2/stats/benchmarks/industry", action: "show", category: "industry"
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :show, category: "industry"]
  end

  as_an_end_user do
    should_be_forbidden [:get, :show, category: "industry"]
  end

  as_an_agent do
    describe "a GET to #show" do
      describe "using a valid category" do
        describe "and a valid type parameter" do
          before { get :show, params: { category: "industry", type: "software" } }

          should_use_presenter Api::V2::Stats::BenchmarkPresenter
          it('responds with ok') { assert_response :ok }
        end

        describe "and an invalid type parameter" do
          before { get :show, params: { category: "industry", type: "invalid" } }

          it('responds with bad_request') { assert_response :bad_request }
        end
      end

      describe "using an invalid category parameter" do
        before { get :show, params: { category: "invalid" } }

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "using 'overall' as the category parameter and no type parameter" do
        before { get :show, params: { category: "overall" } }

        should_use_presenter Api::V2::Stats::BenchmarkPresenter
        it('responds with ok') { assert_response :ok }
      end
    end
  end
end
