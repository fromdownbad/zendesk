require_relative "../../../support/test_helper"
require_relative "../../../support/sharing_test_helper"

SingleCov.covered!

describe Api::V2::SharedTicketsController do
  extend Api::Presentation::TestHelper
  include SharingTestHelper
  fixtures :accounts, :users, :tickets

  with_options(controller: "api/v2/shared_tickets") do |request|
    request.should_route :get, "/api/v2/sharing_agreements/123/shared_tickets", sharing_agreement_id: "123", action: "index"
  end

  describe Api::V2::SharingAgreementsController do
    let(:sharing_agreement) { create_valid_agreement }

    before do
      accept :json
    end

    as_an_anonymous_user do
      should_be_unauthorized [:get, :index, sharing_agreement_id: 123]
    end

    as_an_end_user do
      should_be_forbidden [:get, :index, sharing_agreement_id: 123]
    end

    as_an_agent do
      describe "a GET to :index" do
        before { get :index, params: { sharing_agreement_id: sharing_agreement.id } }
        should_use_presenter Api::V2::SharedTicketPresenter, status: :ok
      end

      describe "with an agreement with a shared ticket associated with an archived ticket" do
        before do
          @archived_ticket = tickets(:minimum_5)
          archive_and_delete(@archived_ticket)
          stub_request(:post, %r{/sharing/tickets/[0-9A-Z]+})
          SharedTicket.create!(ticket: @archived_ticket, agreement: sharing_agreement, uuid: '123ABC', original_id: 123)
          SharedTicket.create!(ticket: tickets(:minimum_1), agreement: sharing_agreement, uuid: '124ABD', original_id: 124)
        end

        describe "without pagination" do
          before { get :index, params: { sharing_agreement_id: sharing_agreement.id } }

          it "includes the nice_id of the archived ticket" do
            assert_equal @archived_ticket.nice_id,
              JSON.parse(@response.body)["shared_tickets"].first["local_ticket_id"]
          end
        end

        describe "with pagination" do
          before { get :index, params: { sharing_agreement_id: sharing_agreement.id, per_page: 1 } }

          it "includes a next_page url" do
            assert_not_nil JSON.parse(@response.body)["next_page"]
          end
        end
      end
    end
  end
end
