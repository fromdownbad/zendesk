require_relative '../../../../support/test_helper'
require_relative '../../../../support/routing'

SingleCov.covered!

describe Api::V2::Routing::RequirementsController do
  extend Api::V2::TestHelper

  include TestSupport::Routing::Helper

  fixtures :all

  before do
    accept :json
    use_ssl
  end

  with_options(controller: 'api/v2/routing/requirements') do |request|
    [
      [
        :get,
        '/api/v2/routing/requirements/fulfilled',
        {action: :fulfilled}
      ]
    ].each do |verb, path, options|
      request.should_route(verb, path, options)
    end
  end

  as_an_anonymous_user do
    describe 'a GET to `fulfilled`' do
      before { get :fulfilled }

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end
  end

  as_an_end_user do
    describe 'a GET to `fulfilled`' do
      before { get :fulfilled }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end
  end

  as_an_agent do
    let(:account) { accounts(:minimum) }

    let!(:client) { Zendesk::Deco::Client.new(account) }

    let(:agent) { users(:minimum_agent) }

    let(:agent_type_id) { Zendesk::Deco::Client::TYPE_ID_AGENT }

    let(:ticket_1) { tickets(:minimum_1) }
    let(:ticket_2) { tickets(:minimum_2) }

    let(:get_request) do
      get(:fulfilled, params: { ticket_ids: [
            ticket_1.nice_id,
            ticket_2.nice_id
          ] })
    end

    before do
      Zendesk::Deco::Client.stubs(:new).with(account).returns(client)
    end

    describe_with_arturo_disabled :skill_based_ticket_routing do
      describe 'a GET to `fulfilled`' do
        before { get :fulfilled }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end
    end

    describe_with_arturo_enabled :skill_based_ticket_routing do
      describe 'a GET to `fulfilled`' do
        before do
          client.
            stubs(:get).
            with(
              "requirements/10/fulfilled_by/20/#{agent.id}",
              {requiring_ids: [ticket_1.id, ticket_2.id]}.to_json
            ).returns(
              stub(
                'response',
                body: {'fulfilled_ids' => [ticket_1.id.to_s]}
              )
            )
        end

        describe 'when it succeeds' do
          before { get_request }

          it 'returns the fulfilled_ticket_ids' do
            assert_equal(
              [ticket_1.nice_id],
              json['fulfilled_ticket_ids']
            )
          end
        end
      end

      describe 'when Deco client raises a BadRequest error' do
        before do
          client.
            stubs(:get).
            with(
              "requirements/10/fulfilled_by/20/#{agent.id}",
              {requiring_ids: [ticket_1.id, ticket_2.id]}.to_json
            ).raises(Kragle::BadRequest.new)
        end

        it 'responds with `400 Bad Request`' do
          get_request
          assert_response :bad_request
        end
      end
    end
  end
end
