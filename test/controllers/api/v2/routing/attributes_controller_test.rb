require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Routing::AttributesController do
  extend Api::V2::TestHelper

  let(:account) { accounts(:minimum) }

  let!(:client) { Zendesk::Deco::Client.new(account) }

  fixtures :all

  before do
    accept :json
    use_ssl
    Zendesk::Deco::Client.stubs(:new).with(account).returns(client)
  end

  with_options(controller: 'api/v2/routing/attributes') do |request|
    [
      [:get,    '/api/v2/routing/attributes',   {action: :index}],
      [:post,   '/api/v2/routing/attributes',   {action: :create}],
      [:get,    '/api/v2/routing/attributes/1', {action: :show,    id: 1}],
      [:put,    '/api/v2/routing/attributes/1', {action: :update,  id: 1}],
      [:delete, '/api/v2/routing/attributes/1', {action: :destroy, id: 1}],
      [
        :get,
        '/api/v2/routing/attributes/definitions',
        {action: :definitions}
      ]
    ].each do |verb, path, options|
      request.should_route(verb, path, options)
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index,
      :create,
      :show,
      :update,
      :destroy,
      :definitions
  end

  as_an_end_user do
    should_be_forbidden :index,
      :create,
      :show,
      :update,
      :destroy,
      :definitions
  end

  as_an_agent do
    describe_with_arturo_enabled :skill_based_ticket_routing do
      should_be_forbidden :create,
        :update,
        :destroy,
        :definitions

      let(:attribute_fixtures) do
        YAML.load_file("#{Rails.root}/test/files/deco/attributes.yml")
      end

      describe 'a GET to `index`' do
        describe 'when it succeeds' do
          before do
            client.
              stubs(:get).
              with('attributes?include=attribute_values,agent_count').
              returns(stub('response', body: attribute_fixtures))

            get :index, params: { include: 'attribute_values,agent_count' }
          end

          it 'responds with `200 OK`' do
            assert_response :ok
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:get).
              with('attributes').
              raises(Kragle::BadRequest.new)

            get :index
          end

          it 'responds with `400 Bad Request`' do
            assert_response :bad_request
          end
        end
      end

      describe 'a GET to `show`' do
        let(:attribute_fixture) do
          attribute_fixtures['attributes'].find { |json| json['id'] == '6' }
        end

        describe 'when it succeeds' do
          before do
            client.
              stubs(:get).
              with('attributes/6').
              returns(
                stub('response', body: {'attribute' => attribute_fixture})
              )

            get :show, params: { id: 6 }
          end

          should_use_presenter Api::V2::Routing::AttributePresenter

          it 'returns the attribute' do
            assert_equal(
              {name: attribute_fixture['name']},
              name: json['attribute']['name']
            )
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:get).
              with('attributes/6').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            get :show, params: { id: 6 }
            assert_response :bad_request
          end
        end
      end
    end
  end

  as_an_admin do
    let(:attribute_fixtures) do
      YAML.load_file("#{Rails.root}/test/files/deco/attributes.yml")
    end

    describe_with_arturo_enabled :skill_based_ticket_routing do
      describe 'a GET to `index`' do
        describe 'with includes' do
          describe 'when it succeeds' do
            before do
              client.
                stubs(:get).
                with('attributes?include=attribute_values,agent_count').
                returns(stub('response', body: attribute_fixtures))

              get :index, params: { include: 'attribute_values,agent_count' }
            end

            it 'returns the attributes associated with the account' do
              assert_equal(
                attribute_fixtures['attributes'].map { |a| {name: a['name']} },
                json['attributes'].map { |attribute| {name: attribute['name']} }
              )
            end

            it 'returns the attributes values associated with the account' do
              assert_equal(
                attribute_fixtures['attribute_values'].map do |av|
                  {name: av['name']}
                end,
                json['attribute_values'].map { |av| {name: av['name']} }
              )
            end

            it 'returns agent counts associated each attribute value' do
              assert_equal(
                attribute_fixtures['agent_count'].values,
                json['agent_count'].values
              )
            end
          end
        end

        describe 'without includes' do
          describe 'when it succeeds' do
            before do
              client.
                stubs(:get).
                with('attributes').
                returns(stub('response',
                  body: {
                    'attributes' => attribute_fixtures['attributes']
                  }))

              get :index
            end

            it 'returns the attributes associated with the account' do
              assert_equal(
                attribute_fixtures['attributes'].map { |a| {name: a['name']} },
                json['attributes'].map { |a| {name: a['name']} }
              )
            end
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:get).
              with('attributes').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            get :index
            assert_response :bad_request
          end
        end
      end

      describe 'a POST to `create`' do
        let(:attribute_fixture) do
          attribute_fixtures['attributes'].find do |json|
            json['name'] == 'Language'
          end
        end

        describe 'when it succeeds' do
          before do
            client.
              stubs(:post).
              with('attributes', 'name' => 'Language').
              returns(
                stub('response', body: {'attribute' => attribute_fixture})
              )

            post :create, params: { attribute: {name: 'Language'} }
          end

          should_use_presenter Api::V2::Routing::AttributePresenter

          it 'creates the attribute' do
            assert_equal(
              {name: attribute_fixture['name']},
              name: json['attribute']['name']
            )
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:post).
              with('attributes', 'name' => 'Language').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            post :create, params: { attribute: {name: 'Language'} }
            assert_response :bad_request
          end
        end
      end

      describe 'a GET to `show`' do
        let(:attribute_fixture) do
          attribute_fixtures['attributes'].find { |json| json['id'] == '6' }
        end

        describe 'when it succeeds' do
          before do
            client.
              stubs(:get).
              with('attributes/6').
              returns(
                stub('response', body: {'attribute' => attribute_fixture})
              )

            get :show, params: { id: 6 }
          end

          should_use_presenter Api::V2::Routing::AttributePresenter

          it 'returns the attribute' do
            assert_equal(
              {name: attribute_fixture['name']},
              name: json['attribute']['name']
            )
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:get).
              with('attributes/6').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            get :show, params: { id: 6 }
            assert_response :bad_request
          end
        end
      end

      describe 'a PUT to `update`' do
        let(:attribute_fixture) do
          attribute_fixtures['attributes'].find { |json| json['id'] == '6' }
        end

        describe 'when it succeeds' do
          before do
            client.
              stubs(:put).
              with('attributes/6', 'name' => 'Produce').
              returns(
                stub('response', body: {'attribute' => attribute_fixture})
              )

            put :update, params: { id: 6, attribute: {name: 'Produce'} }
          end

          should_use_presenter Api::V2::Routing::AttributePresenter

          it 'updates the attribute' do
            assert_equal(
              {name: attribute_fixture['name']},
              name: json['attribute']['name']
            )
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:put).
              with('attributes/6', 'name' => 'Produce').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            put :update, params: { id: 6, attribute: {name: 'Produce'} }
            assert_response :bad_request
          end
        end
      end

      describe 'a DELETE to `destroy`' do
        let(:att_id) { '6' }

        describe 'when it succeeds' do
          before do
            AttributeTicketMap.destroy_all
            AttributeTicketMap.new(definition: Definition.new).tap do |atm|
              atm.account = account
              atm.attribute_id = att_id
              atm.attribute_value_id = '7'
            end.save!
            client.
              stubs(:delete).
              with("attributes/#{att_id}").
              returns(stub('response', status: 204))

            delete :destroy, params: { id: att_id }
          end

          it 'responds with `204 No Content`' do
            assert_response :no_content
          end

          it 'soft deletes associated AttributeTicketMaps' do
            cond = {attribute_id: att_id}
            refute AttributeTicketMap.where(cond).exists?
            assert AttributeTicketMap.unscoped.where(cond).exists?
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:delete).
              with("attributes/#{att_id}").
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            delete :destroy, params: { id: att_id }
            assert_response :bad_request
          end
        end
      end

      describe 'a GET to `definitions`' do
        before { get :definitions }

        should_use_presenter Api::V2::Routing::AttributeDefinitionsPresenter
      end
    end

    describe_with_arturo_disabled :skill_based_ticket_routing do
      describe 'a GET to `index`' do
        before { get :index }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a POST to `create`' do
        before { post :create, params: { attribute: {name: 'Language'} } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a GET to `show`' do
        let(:attribute_fixture) do
          attribute_fixtures['attributes'].find { |json| json['id'] == '6' }
        end

        describe 'when it succeeds' do
          before do
            client.
              stubs(:get).
              with('attributes/6').
              returns(
                stub('response', body: {'attribute' => attribute_fixture})
              )

            get :show, params: { id: 6 }
          end

          should_use_presenter Api::V2::Routing::AttributePresenter

          it 'returns the attribute' do
            assert_equal(
              {name: attribute_fixture['name']},
              name: json['attribute']['name']
            )
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:get).
              with('attributes/6').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            get :show, params: { id: 6 }
            assert_response :bad_request
          end
        end
      end

      describe 'a PUT to `update`' do
        before { put :update, params: { id: 6, attribute: {name: 'Produce'} } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a DELETE to `destroy`' do
        before { delete :destroy, params: { id: 6 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a GET to `definitions`' do
        before { get :definitions }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end
    end
  end
end
