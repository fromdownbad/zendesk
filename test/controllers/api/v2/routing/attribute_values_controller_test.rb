require_relative '../../../../support/test_helper'
require_relative '../../../../support/routing'

SingleCov.covered!

describe Api::V2::Routing::AttributeValuesController do
  extend Api::V2::TestHelper

  include TestSupport::Routing::Helper

  fixtures :all

  before do
    accept :json
    use_ssl
  end

  with_options(controller: 'api/v2/routing/attribute_values') do |request|
    [
      [
        :get,
        '/api/v2/routing/attributes/1/values',
        {action: :index, attribute_id: 1}
      ],
      [
        :post,
        '/api/v2/routing/attributes/1/values',
        {action: :create, attribute_id: 1}
      ],
      [
        :get,
        '/api/v2/routing/attributes/1/values/6',
        {action: :show, attribute_id: 1, id: 6}
      ],
      [
        :put,
        '/api/v2/routing/attributes/1/values/6',
        {action: :update, attribute_id: 1, id: 6}
      ],
      [
        :delete,
        '/api/v2/routing/attributes/1/values/6',
        {action: :destroy, attribute_id: 1, id: 6}
      ],
      [
        :get,
        '/api/v2/routing/attributes/1/values/6/agents',
        {action: :agents, attribute_id: 1, id: 6}
      ],
      [
        :get,
        '/api/v2/routing/attributes/1/values/6/agent_search',
        {action: :agent_search, attribute_id: 1, id: 6}
      ]
    ].each do |verb, path, options|
      request.should_route(verb, path, options)
    end
  end

  as_an_anonymous_user do
    describe 'a GET to `index`' do
      before { get :index, params: { attribute_id: 1 } }

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end

    describe 'a POST to `create`' do
      before { post :create, params: { attribute_id: 1 } }

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end

    describe 'a GET to `show`' do
      before { get :show, params: { attribute_id: 1, id: 6 } }

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end

    describe 'a PUT to `update`' do
      before do
        put(:update, params: { attribute_id: 1, id: 6, attribute_value: {name: 'Portuguese'} })
      end

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end

    describe 'a DELETE to `destroy`' do
      before { delete :destroy, params: { attribute_id: 1, id: 6 } }

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end

    describe 'a GET to `agents`' do
      before { get :agents, params: { attribute_id: 1, id: 6 } }

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end

    describe 'a GET to `agent_search`' do
      before { get :agent_search, params: { attribute_id: 1, id: 6 } }

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end
  end

  as_an_end_user do
    describe 'a GET to `index`' do
      before { get :index, params: { attribute_id: 1 } }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a POST to `create`' do
      before { post :create, params: { attribute_id: 1 } }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a GET to `show`' do
      before { get :show, params: { attribute_id: 1, id: 6 } }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a PUT to `update`' do
      before do
        put(:update, params: { attribute_id: 1, id: 6, attribute_value: {name: 'Portuguese'} })
      end

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a DELETE to `destroy`' do
      before { delete :destroy, params: { attribute_id: 1, id: 6 } }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a GET to `agents`' do
      before { get :agents, params: { attribute_id: 1, id: 6 } }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a GET to `agent_search`' do
      before { get :agent_search, params: { attribute_id: 1, id: 6 } }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end
  end

  as_an_agent do
    let(:attribute_value_fixtures) do
      YAML.load_file(
        "#{Rails.root}/test/files/deco/attribute_values.yml"
      )['attribute_values']
    end

    let(:account) { accounts(:minimum) }

    let!(:client) { Zendesk::Deco::Client.new(account) }

    let(:agent_type_id) { Zendesk::Deco::Client::TYPE_ID_AGENT }

    before do
      Zendesk::Deco::Client.stubs(:new).with(account).returns(client)

      AttributeTicketMap.destroy_all
    end

    describe_with_arturo_enabled :skill_based_ticket_routing do
      describe 'a GET to `index`' do
        before { get :index, params: { attribute_id: 1 } }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe 'a POST to `create`' do
        before { post :create, params: { attribute_id: 1 } }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe 'a GET to `show`' do
        let(:attribute_value_fixture) do
          attribute_value_fixtures.find { |json| json['id'] == '6' }
        end

        describe 'when it succeeds' do
          before do
            client.
              stubs(:get).
              with('attributes/1/values/6').
              returns(
                stub(
                  'response',
                  body: {'attribute_value' => attribute_value_fixture}
                )
              )
          end

          describe 'when the attribute value has conditions' do
            before do
              new_attribute_ticket_map(attribute_id: 1, attribute_value_id: 6)

              get :show, params: { attribute_id: 1, id: 6 }
            end

            should_use_presenter Api::V2::Routing::AttributeValuePresenter

            it 'returns the attribute value' do
              assert_equal(
                {name: attribute_value_fixture['name']},
                name: json['attribute_value']['name']
              )
            end

            it 'returns conditions' do
              assert_equal(
                {
                  all: [{
                    'subject'  => 'comment_includes_word',
                    'operator' => 'includes',
                    'value'    => 'test'
                  }],
                  any: []
                },
                all: json['attribute_value']['conditions']['all'],
                any: json['attribute_value']['conditions']['any']
              )
            end
          end

          describe 'when the attribute value does not have conditions' do
            before { get :show, params: { attribute_id: 1, id: 6 } }

            should_use_presenter Api::V2::Routing::AttributeValuePresenter

            it 'returns the attribute value' do
              assert_equal(
                {name: attribute_value_fixture['name']},
                name: json['attribute_value']['name']
              )
            end

            it 'does not return conditions' do
              assert_nil json['attribute_value']['conditions']
            end
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:get).
              with('attributes/1/values/6').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            get :show, params: { attribute_id: 1, id: 6 }
            assert_response :bad_request
          end
        end
      end

      describe 'a PUT to `update`' do
        before do
          put(:update, params: { attribute_id: 1, id: 6, attribute_value: {name: 'Portuguese'} })
        end

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe 'a DELETE to `destroy`' do
        before { delete :destroy, params: { attribute_id: 1, id: 6 } }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe 'a GET to `agents`' do
        before { get :agents, params: { attribute_id: 1, id: 6 } }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end
    end

    describe_with_arturo_disabled :skill_based_ticket_routing do
      describe 'a GET to `index`' do
        before { get :index, params: { attribute_id: 1 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a POST to `create`' do
        before { post :create, params: { attribute_id: 1 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a GET to `show`' do
        let(:attribute_value_fixture) do
          attribute_value_fixtures.find { |json| json['id'] == '6' }
        end

        describe 'when it succeeds' do
          before do
            client.
              stubs(:get).
              with('attributes/1/values/6').
              returns(
                stub(
                  'response',
                  body: {'attribute_value' => attribute_value_fixture}
                )
              )
          end

          describe 'when the attribute value has conditions' do
            before do
              new_attribute_ticket_map(attribute_id: 1, attribute_value_id: 6)

              get :show, params: { attribute_id: 1, id: 6 }
            end

            should_use_presenter Api::V2::Routing::AttributeValuePresenter

            it 'returns the attribute value' do
              assert_equal(
                {name: attribute_value_fixture['name']},
                name: json['attribute_value']['name']
              )
            end

            it 'returns conditions' do
              assert_equal(
                {
                  all: [{
                    'subject'  => 'comment_includes_word',
                    'operator' => 'includes',
                    'value'    => 'test'
                  }],
                  any: []
                },
                all: json['attribute_value']['conditions']['all'],
                any: json['attribute_value']['conditions']['any']
              )
            end
          end

          describe 'when the attribute value does not have conditions' do
            before { get :show, params: { attribute_id: 1, id: 6 } }

            should_use_presenter Api::V2::Routing::AttributeValuePresenter

            it 'returns the attribute value' do
              assert_equal(
                {name: attribute_value_fixture['name']},
                name: json['attribute_value']['name']
              )
            end

            it 'does not return conditions' do
              assert_nil json['attribute_value']['conditions']
            end
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:get).
              with('attributes/1/values/6').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            get :show, params: { attribute_id: 1, id: 6 }
            assert_response :bad_request
          end
        end
      end

      describe 'a PUT to `update`' do
        before do
          put(:update, params: { attribute_id: 1, id: 6, attribute_value: {name: 'Portuguese'} })
        end

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a DELETE to `destroy`' do
        before { delete :destroy, params: { attribute_id: 1, id: 6 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a GET to `agents`' do
        before { get :agents, params: { attribute_id: 1, id: 6 } }

        it 'responds with `404 Not FOund`' do
          assert_response :not_found
        end
      end
    end
  end

  as_an_admin do
    let(:attribute_value_fixtures) do
      YAML.load_file(
        "#{Rails.root}/test/files/deco/attribute_values.yml"
      )['attribute_values']
    end

    let(:account) { accounts(:minimum) }

    let!(:client) { Zendesk::Deco::Client.new(account) }

    let(:agent_type_id) { Zendesk::Deco::Client::TYPE_ID_AGENT }

    before do
      Zendesk::Deco::Client.stubs(:new).with(account).returns(client)

      AttributeTicketMap.destroy_all
    end

    describe_with_arturo_enabled :skill_based_ticket_routing do
      describe 'a GET to `index`' do
        describe 'when conditions are associated with attribute values' do
          let(:attribute_value_ids_with_conditions) do
            %i[first last].map do |position|
              attribute_value_fixtures.send(position)['id']
            end
          end

          let(:conditions) do
            {
              'all' => [{
                'subject'  => 'comment_includes_word',
                'operator' => 'includes',
                'value'    => 'test'
              }],
              'any' => []
            }
          end

          let(:attribute_values_with_conditions) do
            json['attribute_values'].select do |attribute_value|
              attribute_value_ids_with_conditions.include?(
                attribute_value['id']
              )
            end
          end

          let(:attribute_values_without_conditions) do
            json['attribute_values'].select do |attribute_value|
              attribute_value_ids_with_conditions.exclude?(
                attribute_value['id']
              )
            end
          end

          before do
            client.
              stubs(:get).
              with('attributes/1/values').
              returns(
                stub(
                  'response',
                  body: {'attribute_values' => attribute_value_fixtures}
                )
              )

            attribute_value_ids_with_conditions.each do |attribute_value_id|
              new_attribute_ticket_map(
                attribute_id:       1,
                attribute_value_id: attribute_value_id
              )
            end

            get :index, params: { attribute_id: 1 }
          end

          should_use_presenter Api::V2::Routing::AttributeValuePresenter

          it 'has attribute values with conditions' do
            refute attribute_values_with_conditions.empty?
          end

          it 'has attribute values without conditions' do
            refute attribute_values_without_conditions.empty?
          end

          it 'returns the values associated with the attribute' do
            assert_equal(
              attribute_value_fixtures.map do |attribute_value|
                {name: attribute_value['name']}
              end,
              json['attribute_values'].map do |attribute_value|
                {name: attribute_value['name']}
              end
            )
          end

          it 'returns the conditions associated with the attribute values' do
            assert(
              attribute_values_with_conditions.all? do |attribute_value|
                attribute_value['conditions'] == conditions
              end
            )
          end

          it 'does not return any conditions for att values without them' do
            assert(
              attribute_values_without_conditions.none? do |attribute_value|
                attribute_value.key?('conditions')
              end
            )
          end

          describe 'when the cache is cleared' do
            before { Rails.cache.clear }

            it 'does not perform `n+1` queries' do
              assert_no_n_plus_one { get :index, params: { attribute_id: 1 } }
            end
          end
        end

        describe 'with pagination parameters' do
          let(:attribute_values) { attribute_value_fixtures }
          let(:cursor)           { '1' }
          let(:limit)            { 3 }

          let(:base_url) do
            'https://minimum.zendesk-test.com/api/v2/routing/' \
            'attributes/1/values.json'
          end

          before do
            client.
              stubs(:get).
              with("attributes/1/values?cursor=#{cursor}&limit=#{limit}").
              returns(
                stub('response', body: {'attribute_values' => attribute_values})
              )

            get :index, params: { attribute_id: 1, cursor: cursor, limit: limit }
          end

          describe 'and there are attribute values' do
            let(:attribute_values) { attribute_value_fixtures }

            it 'returns the values associated with the attribute' do
              assert_equal(
                attribute_value_fixtures.map do |attribute_value|
                  {name: attribute_value['name']}
                end,
                json['attribute_values'].map do |attribute_value|
                  {name: attribute_value['name']}
                end
              )
            end

            it 'returns a count of the attribute values' do
              assert_equal attribute_values.length, json['count']
            end

            it 'returns the last att value id as the `next_page` cursor' do
              assert_equal(
                "#{base_url}?cursor=#{attribute_values.last['id']}&limit=3",
                json['next_page']
              )
            end
          end

          describe 'and there are no attribute values' do
            let(:attribute_values) { [] }

            it 'returns no attribute values' do
              assert_equal attribute_values, json['attribute_values']
            end

            it 'returns a 0 count' do
              assert_equal 0, json['count']
            end

            it 'returns nil as the `next_page`' do
              assert_nil json['next_page']
            end
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:get).
              with('attributes/1/values').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            get :index, params: { attribute_id: 1 }
            assert_response :bad_request
          end
        end
      end

      describe 'a POST to `create`' do
        let(:attribute_value_fixture) do
          attribute_value_fixtures.find { |json| json['name'] == 'Japanese' }
        end

        let(:conditions)   { {} }
        let(:added_agents) { [] }

        let(:post_request) do
          post(:create, params: { attribute_id: 1, attribute_value: {
              name:         'Japanese',
              conditions:   conditions,
              added_agents: added_agents
            } })
        end

        describe 'when it succeeds' do
          before do
            client.
              stubs(:post).
              with('attributes/1/values', 'name' => 'Japanese').
              returns(
                stub(
                  'response',
                  body: {'attribute_value' => attribute_value_fixture}
                )
              )
          end

          describe 'simple post' do
            before { post_request }

            should_use_presenter Api::V2::Routing::AttributeValuePresenter
          end

          describe 'when conditions are provided' do
            let(:conditions) do
              {
                all: [
                  {
                    subject:  'comment_includes_word',
                    operator: 'includes',
                    value:    'test'
                  }
                ],
                any: [
                  {
                    subject:  'comment_includes_word',
                    operator: 'includes',
                    value:    'more tests'
                  }
                ]
              }
            end

            before { post_request }

            it 'creates the attribute value' do
              assert_equal(
                {name: attribute_value_fixture['name']},
                name: json['attribute_value']['name']
              )
            end

            it 'creates an attribute ticket map' do
              assert(
                account.attribute_ticket_maps.where(
                  attribute_id:       1,
                  attribute_value_id: json['attribute_value']['id']
                ).one?
              )
            end

            it 'returns the conditions' do
              assert_equal(
                {
                  all: [{
                    'subject'  => 'comment_includes_word',
                    'operator' => 'includes',
                    'value'    => 'test'
                  }],
                  any: [{
                    'subject'  => 'comment_includes_word',
                    'operator' => 'includes',
                    'value'    => 'more tests'
                  }]
                },
                all: json['attribute_value']['conditions']['all'],
                any: json['attribute_value']['conditions']['any']
              )
            end
          end

          describe 'when no conditions are provided' do
            before { post_request }

            it 'creates the attribute value' do
              assert_equal(
                {name: attribute_value_fixture['name']},
                name: json['attribute_value']['name']
              )
            end

            it 'does not create an attribute ticket map' do
              refute AttributeTicketMap.any?
            end

            it 'does not return conditions' do
              assert_nil json['attribute_value']['conditions']
            end
          end

          describe 'when agents are added' do
            let(:agent_1)      { users(:minimum_agent) }
            let(:agent_2)      { users(:minimum_admin) }
            let(:added_agents) { [agent_1, agent_2].sort_by(&:name).map(&:id) }

            let(:bulk_instance_values) do
              "attributes/1/values/#{attribute_value_fixture['id']}/" \
                'bulk_instance_values'
            end

            before do
              client.
                expects(:post).
                with(
                  bulk_instance_values,
                  create_instance_ids: added_agents,
                  delete_instance_ids: [],
                  type_id:             agent_type_id
                ).
                once

              post_request
            end

            it 'creates the attribute value' do
              assert_equal(
                {name: attribute_value_fixture['name']},
                name: json['attribute_value']['name']
              )
            end
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:post).
              with('attributes/1/values', 'name' => 'Japanese').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            post_request
            assert_response :bad_request
          end
        end
      end

      describe 'a GET to `show`' do
        let(:attribute_value_fixture) do
          attribute_value_fixtures.find { |json| json['id'] == '6' }
        end

        describe 'when it succeeds' do
          before do
            client.
              stubs(:get).
              with('attributes/1/values/6').
              returns(
                stub(
                  'response',
                  body: {'attribute_value' => attribute_value_fixture}
                )
              )
          end

          describe 'when the attribute value has conditions' do
            before do
              new_attribute_ticket_map(attribute_id: 1, attribute_value_id: 6)

              get :show, params: { attribute_id: 1, id: 6 }
            end

            should_use_presenter Api::V2::Routing::AttributeValuePresenter

            it 'returns the attribute value' do
              assert_equal(
                {name: attribute_value_fixture['name']},
                name: json['attribute_value']['name']
              )
            end

            it 'returns conditions' do
              assert_equal(
                {
                  all: [{
                    'subject'  => 'comment_includes_word',
                    'operator' => 'includes',
                    'value'    => 'test'
                  }],
                  any: []
                },
                all: json['attribute_value']['conditions']['all'],
                any: json['attribute_value']['conditions']['any']
              )
            end
          end

          describe 'when the attribute value does not have conditions' do
            before { get :show, params: { attribute_id: 1, id: 6 } }

            should_use_presenter Api::V2::Routing::AttributeValuePresenter

            it 'returns the attribute value' do
              assert_equal(
                {name: attribute_value_fixture['name']},
                name: json['attribute_value']['name']
              )
            end

            it 'does not return conditions' do
              assert_nil json['attribute_value']['conditions']
            end
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:get).
              with('attributes/1/values/6').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            get :show, params: { attribute_id: 1, id: 6 }
            assert_response :bad_request
          end
        end
      end

      describe 'a PUT to `update`' do
        let(:attribute_value_fixture) do
          attribute_value_fixtures.find { |json| json['id'] == '6' }
        end

        describe 'when it succeeds' do
          before do
            client.
              stubs(:put).
              with('attributes/1/values/6', 'name' => 'Portuguese').
              returns(
                stub(
                  'response',
                  body: {'attribute_value' => attribute_value_fixture}
                )
              )
          end

          describe 'when a mapping exists for the attribute value' do
            let!(:attribute_ticket_map) do
              new_attribute_ticket_map(attribute_id: 1, attribute_value_id: 6)
            end

            let(:conditions) { ActionController::Parameters.new }

            before do
              put(:update, params: { attribute_id: 1, id: 6, attribute_value: {name: 'Portuguese', conditions: conditions} })
            end

            should_use_presenter Api::V2::Routing::AttributeValuePresenter

            describe 'and conditions are provided' do
              let(:conditions) do
                {
                  all: [
                    {
                      subject:  'comment_includes_word',
                      operator: 'includes',
                      value:    'test++'
                    }
                  ],
                  any: []
                }
              end

              it 'updates the attribute value' do
                assert_equal(
                  {name: attribute_value_fixture['name']},
                  name: json['attribute_value']['name']
                )
              end

              it 'updates the attribute ticket map' do
                assert_equal(
                  attribute_ticket_map.id,
                  account.attribute_ticket_maps.where(
                    attribute_id:       1,
                    attribute_value_id: 6
                  ).first.id
                )
              end

              it 'does not create a new attribute ticket map' do
                assert(
                  account.attribute_ticket_maps.where(
                    attribute_id:       1,
                    attribute_value_id: 6
                  ).one?
                )
              end

              it 'returns the conditions' do
                assert_equal(
                  {
                    all: [{
                      'subject'  => 'comment_includes_word',
                      'operator' => 'includes',
                      'value'    => 'test++'
                    }],
                    any: []
                  },
                  all: json['attribute_value']['conditions']['all'],
                  any: json['attribute_value']['conditions']['any']
                )
              end
            end

            describe 'and no conditions are provided' do
              it 'updates the attribute value' do
                assert_equal(
                  {name: attribute_value_fixture['name']},
                  name: json['attribute_value']['name']
                )
              end

              it 'removes the attribute ticket map' do
                refute AttributeTicketMap.any?
              end

              it 'does not return conditions' do
                assert_nil json['attribute_value']['conditions']
              end
            end

            describe 'when agents are added or removed' do
              let(:agent_1)        { users(:minimum_agent) }
              let(:agent_2)        { users(:minimum_admin) }
              let(:added_agents)   { [agent_1.id] }
              let(:removed_agents) { [agent_2.id] }

              let(:bulk_instance_values) do
                "attributes/1/values/#{attribute_value_fixture['id']}/" \
                  'bulk_instance_values'
              end

              before do
                client.
                  expects(:post).
                  with(
                    bulk_instance_values,
                    create_instance_ids: added_agents,
                    delete_instance_ids: removed_agents,
                    type_id:             agent_type_id
                  ).
                  once

                put(:update, params: { attribute_id: 1, id:              6, attribute_value: {
                    name: 'Portuguese',
                    conditions: conditions,
                    added_agents: added_agents,
                    removed_agents: removed_agents
                  } })
              end

              it 'updates the attribute value' do
                assert_equal(
                  {name: attribute_value_fixture['name']},
                  name: json['attribute_value']['name']
                )
              end
            end
          end

          describe 'when no mappings exist for the attribute value' do
            let(:conditions) do
              {
                all: [
                  {
                    subject:  'comment_includes_word',
                    operator: 'includes',
                    value:    'test'
                  }
                ],
                any: []
              }
            end

            before do
              put(:update, params: { attribute_id: 1, id: 6, attribute_value: {name: 'Portuguese', conditions: conditions} })
            end

            should_use_presenter Api::V2::Routing::AttributeValuePresenter

            describe 'and conditions are provided' do
              it 'updates the attribute value' do
                assert_equal(
                  {name: attribute_value_fixture['name']},
                  name: json['attribute_value']['name']
                )
              end

              it 'creates a new attribute ticket map' do
                assert(
                  account.attribute_ticket_maps.where(
                    attribute_id:       1,
                    attribute_value_id: 6
                  ).one?
                )
              end

              it 'returns the conditions' do
                assert_equal(
                  {
                    all: [{
                      'subject'  => 'comment_includes_word',
                      'operator' => 'includes',
                      'value'    => 'test'
                    }],
                    any: []
                  },
                  all: json['attribute_value']['conditions']['all'],
                  any: json['attribute_value']['conditions']['any']
                )
              end
            end

            describe 'and no conditions are provided' do
              let(:conditions) { {} }

              it 'updates the attribute value' do
                assert_equal(
                  {name: attribute_value_fixture['name']},
                  name: json['attribute_value']['name']
                )
              end

              it 'does not create an attribute ticket map' do
                refute AttributeTicketMap.any?
              end

              it 'does not return conditions' do
                assert_nil json['attribute_value']['conditions']
              end
            end
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:put).
              with('attributes/1/values/6', 'name' => 'Portuguese').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            put(:update, params: { attribute_id: 1, id: 6, attribute_value: {name: 'Portuguese'} })

            assert_response :bad_request
          end
        end
      end

      describe 'a DELETE to `destroy`' do
        let(:params) { { attribute_id: 1, id: 6 } }

        describe_with_arturo_disabled :delayed_routing_attribute_value_deletion do
          describe 'Deco client call is successful' do
            before do
              client.
                stubs(:delete).
                returns(stub('response', status: 204))
            end

            describe 'when a mapping exists for the attribute value' do
              before(:each) do
                new_attribute_ticket_map(attribute_id: params[:attribute_id], attribute_value_id: params[:id])

                delete :destroy, params: params
              end

              it 'responds with `204 No Content`' do
                assert_response :no_content
              end

              it 'removes the attribute ticket map' do
                refute AttributeTicketMap.any?
              end
            end

            describe 'when no mappings exist for the attribute value' do
              before { delete :destroy, params: params }

              it 'responds with `204 No Content`' do
                assert_response :no_content
              end
            end

            before_should 'not enqueue a `RoutingAttributeValueDeleteJob`' do
              RoutingAttributeValueDeleteJob.expects(:enqueue).never
            end
          end

          describe 'when Deco client raises a BadRequest error' do
            before do
              new_attribute_ticket_map(attribute_id: params[:attribute_id], attribute_value_id: params[:id])

              client.
                stubs(:delete).
                raises(Kragle::BadRequest.new)
            end

            it 'responds with `400 Bad Request`' do
              delete :destroy, params: params
              assert_response :bad_request
            end
          end
        end

        describe_with_arturo_enabled :delayed_routing_attribute_value_deletion do
          before do
            client.
              stubs(:delete).
              returns(stub('response', status: 204))
          end

          describe 'when a mapping exists for the attribute value' do
            before(:each) do
              new_attribute_ticket_map(attribute_id: params[:attribute_id], attribute_value_id: params[:id])
            end

            after(:each) { AttributeTicketMap.destroy_all }

            describe 'attribute ticket map successfully soft deletes' do
              before(:each) do
                delete :destroy, params: params
              end

              it 'responds with `204 No Content`' do
                assert_response :no_content
              end

              it 'removes the attribute ticket map' do
                refute AttributeTicketMap.any?
              end

              before_should 'enqueue a `RoutingAttributeValueDeleteJob`' do
                RoutingAttributeValueDeleteJob.expects(:enqueue).once
              end
            end

            describe 'attribute ticket map fails to soft delete' do
              before(:each) do
                AttributeTicketMap.any_instance.stubs(:soft_delete).returns(false)

                delete :destroy, params: params
              end

              it 'does not remove the attribute ticket map' do
                assert_equal 1, AttributeTicketMap.all.count
              end

              it 'responds with `422 Unprocessable Entity`' do
                assert_response :unprocessable_entity
              end

              before_should 'not enqueue a `RoutingAttributeValueDeleteJob`' do
                RoutingAttributeValueDeleteJob.expects(:enqueue).never
              end
            end
          end
        end
      end

      describe 'a GET to `agents`' do
        let(:agents) do
          [users(:minimum_agent), users(:minimum_admin)].sort_by(&:name)
        end

        let(:instance_value_fixtures) do
          agents.each_with_index.map do |agent, index|
            {'id' => index, 'instance_id' => agent.id}
          end
        end

        before do
          client.
            stubs(:get).
            with(
              "attributes/1/values/6/instance_values?type_id=#{agent_type_id}"
            ).
            returns(
              stub(
                'response',
                body: {'instance_values' => instance_value_fixtures}
              )
            )

          get :agents, params: { attribute_id: 1, id: 6 }
        end

        should_use_presenter Api::V2::Users::AgentPresenter

        it 'returns the agents associated with the attribute value' do
          assert_equal(
            agents.map do |agent|
              {id: agent.id, name: agent.name}
            end,
            json['users'].map do |agent|
              {id: agent['id'], name: agent['name']}
            end
          )
        end
      end

      describe 'a GET to `agent_search`' do
        let(:agents_with_value) do
          [
            users(:minimum_agent),
            users(:minimum_admin)
          ]
        end
        let(:agents) { account.agents.sort_by(&:id) }

        let(:end_user) { users(:minimum_end_user) }

        let(:instance_value_fixtures) do
          agents_with_value.each_with_index.map do |agent, index|
            {'id' => index, 'instance_id' => agent.id}
          end
        end

        let(:query)    { nil }
        let(:group_id) { nil }

        def assert_agents
          assert_equal(
            agents.map do |agent|
              {
                id:                  agent.id,
                name:                agent.name,
                has_attribute_value: agents_with_value.include?(agent)
              }
            end,
            json['users'].map do |agent|
              {
                id:                  agent['id'],
                name:                agent['name'],
                has_attribute_value: agent['has_attribute_value']
              }
            end
          )
        end

        before do
          end_user.name = users(:minimum_agent).name + 'End User'

          end_user.save!

          client.
            stubs(:get).
            with(
              'attributes/1/values/6/instances_with_value',
              instance_ids: agents.map(&:id),
              type_id:      agent_type_id
            ).
            returns(
              stub(
                'response',
                body: {'instance_values' => instance_value_fixtures}
              )
            )

          params = { attribute_id: 1, id: 6, query: query }
          params[:group_id] = group_id if group_id

          get(:agent_search, params: params)
          assert_response :ok, response.body
        end

        should_use_presenter Api::V2::Routing::AgentPresenter

        describe 'when no query is provided' do
          it 'returns all agents along with their association status' do
            assert_agents
          end
        end

        describe 'when a query is provided' do
          let(:agent)  { users(:minimum_agent) }
          let(:agents) { [agent] }

          let(:query) { agent.name }

          it 'returns matching agents along with their association status' do
            assert_agents
          end
        end

        describe 'when a group ID is provided' do
          let(:group)    { groups(:minimum_group) }
          let(:group_id) { group.id }

          let(:agents) { group.users.agents.sort_by(&:id) }

          before_should 'the group is not empty' do
            refute agents.empty?
          end

          before_should 'the group does not contain all of the agents' do
            refute_equal account.agents, agents
          end

          it 'returns agents in the group along with their assoc status' do
            assert_agents
          end
        end
      end
    end

    describe_with_arturo_disabled :skill_based_ticket_routing do
      describe 'a GET to `index`' do
        before { get :index, params: { attribute_id: 1 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a POST to `create`' do
        before do
          post :create, params: { attribute_id: 1, attribute_value: {name: 'Japanese'} }
        end

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a GET to `show`' do
        let(:attribute_value_fixture) do
          attribute_value_fixtures.find { |json| json['id'] == '6' }
        end

        describe 'when it succeeds' do
          before do
            client.
              stubs(:get).
              with('attributes/1/values/6').
              returns(
                stub(
                  'response',
                  body: {'attribute_value' => attribute_value_fixture}
                )
              )
          end

          describe 'when the attribute value has conditions' do
            before do
              new_attribute_ticket_map(attribute_id: 1, attribute_value_id: 6)

              get :show, params: { attribute_id: 1, id: 6 }
            end

            should_use_presenter Api::V2::Routing::AttributeValuePresenter

            it 'returns the attribute value' do
              assert_equal(
                {name: attribute_value_fixture['name']},
                name: json['attribute_value']['name']
              )
            end

            it 'returns conditions' do
              assert_equal(
                {
                  all: [{
                    'subject'  => 'comment_includes_word',
                    'operator' => 'includes',
                    'value'    => 'test'
                  }],
                  any: []
                },
                all: json['attribute_value']['conditions']['all'],
                any: json['attribute_value']['conditions']['any']
              )
            end
          end

          describe 'when the attribute value does not have conditions' do
            before { get :show, params: { attribute_id: 1, id: 6 } }

            should_use_presenter Api::V2::Routing::AttributeValuePresenter

            it 'returns the attribute value' do
              assert_equal(
                {name: attribute_value_fixture['name']},
                name: json['attribute_value']['name']
              )
            end

            it 'does not return conditions' do
              assert_nil json['attribute_value']['conditions']
            end
          end
        end

        describe 'when Deco client raises a BadRequest error' do
          before do
            client.
              stubs(:get).
              with('attributes/1/values/6').
              raises(Kragle::BadRequest.new)
          end

          it 'responds with `400 Bad Request`' do
            get :show, params: { attribute_id: 1, id: 6 }
            assert_response :bad_request
          end
        end
      end

      describe 'a PUT to `update`' do
        before do
          put(:update, params: { attribute_id: 1, id: 6, attribute_value: {name: 'Portuguese'} })
        end

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a DELETE to `destroy`' do
        before { delete :destroy, params: { attribute_id: 1, id: 6 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a GET to `agents`' do
        before { get :agents, params: { attribute_id: 1, id: 6 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a GET to `agent_search`' do
        before { get :agent_search, params: { attribute_id: 1, id: 6 } }

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end
    end
  end
end
