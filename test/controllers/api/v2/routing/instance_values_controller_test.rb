require_relative '../../../../support/test_helper'
require_relative '../../../../support/routing'

SingleCov.covered!

describe Api::V2::Routing::InstanceValuesController do
  extend Api::V2::TestHelper

  include TestSupport::Routing::Helper

  fixtures :all

  before do
    accept :json
    use_ssl
  end

  let(:attribute_fixture) do
    YAML.load_file("#{Rails.root}/test/files/deco/attributes.yml")['attributes'].
      find { |json| json['id'] == '6' }
  end

  let(:attribute_value_fixtures) do
    YAML.load_file(
      "#{Rails.root}/test/files/deco/attribute_values.yml"
    )['attribute_values']
  end

  let(:attribute_values_for_instance_fixtures) do
    [
      {
        'attribute' => attribute_fixture,
        'values' => attribute_value_fixtures
      }
    ]
  end

  let(:account) { accounts(:minimum) }

  let!(:client) { Zendesk::Deco::Client.new(account) }

  let(:agent_type_id) { Zendesk::Deco::Client::TYPE_ID_AGENT }

  let(:ticket) { tickets(:minimum_1) }

  let(:agent) { users(:minimum_agent) }

  let(:end_user) { users(:minimum_end_user) }

  with_options(controller: 'api/v2/routing/instance_values') do |request|
    [
      [
        :get,
        '/api/v2/routing/tickets/1/instance_values',
        {action: :show_ticket_instance_values, id: 1}
      ],
      [
        :post,
        '/api/v2/routing/tickets/1/instance_values',
        {action: :update_ticket_instance_values, id: 1}
      ],
      [
        :get,
        '/api/v2/routing/agents/1/instance_values',
        {action: :show_agent_instance_values, id: 1}
      ],
      [
        :post,
        '/api/v2/routing/agents/1/instance_values',
        {action: :update_agent_instance_values, id: 1}
      ],
      [
        :put,
        '/api/v2/routing/tickets/1/rules/execute',
        {action: :rerun_attribute_ticket_mapping_rules, id: 1}
      ]
    ].each do |verb, path, options|
      request.should_route(verb, path, options)
    end
  end

  as_an_anonymous_user do
    describe 'a GET to ticket instance_values' do
      before { get :show_ticket_instance_values, params: { id: 1 } }

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end

    describe 'a POST to ticket instance_values' do
      before do
        post :update_ticket_instance_values, params: { id: 1, attribute_value_ids: [] }
      end

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end

    describe 'a GET to agent instance_values' do
      before { get :show_agent_instance_values, params: { id: 1 } }

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end

    describe 'a POST to agent instance_values' do
      before do
        post :update_agent_instance_values, params: { id: 1, attribute_value_ids: [] }
      end

      it 'responds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end

    describe 'a PUT to /rules/execute' do
      before do
        put :rerun_attribute_ticket_mapping_rules, params: { id: 1 }
      end

      it 'responsds with `401 Unauthorized`' do
        assert_response :unauthorized
      end
    end
  end

  as_an_end_user do
    describe 'a GET to ticket instance_values' do
      before { get :show_ticket_instance_values, params: { id: 1 } }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a POST to ticket instance_values' do
      before do
        post :update_ticket_instance_values, params: { id: 1, attribute_value_ids: [] }
      end

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a GET to agent instance_values' do
      before { get :show_agent_instance_values, params: { id: 1 } }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a POST to agent instance_values' do
      before do
        post :update_agent_instance_values, params: { id: 1, attribute_value_ids: [] }
      end

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'a PUT to /rules/execute' do
      before do
        put :rerun_attribute_ticket_mapping_rules, params: { id: 1 }
      end

      it 'responsds with `403 Unauthorized`' do
        assert_response :forbidden
      end
    end
  end

  describe_with_arturo_enabled :skill_based_ticket_routing do
    as_an_agent do
      before do
        Zendesk::Deco::Client.stubs(:new).with(account).returns(client)
        AttributeTicketMap.destroy_all
      end

      describe 'a GET to ticket instance_values' do
        before do
          client.
            stubs(:get).
            with("type/10/instance/#{ticket.id}/attribute_values").
            returns(
              stub(
                'response',
                body: {'items' => attribute_values_for_instance_fixtures}
              )
            )

          get :show_ticket_instance_values, params: { id: ticket.nice_id }
        end

        it 'returns the values associated with the instance' do
          assert_equal(
            attribute_value_fixtures.map do |attribute_value|
              {name: attribute_value['name']}
            end,
            json['attribute_values'].map do |attribute_value|
              {name: attribute_value['name']}
            end
          )
        end
      end

      describe 'a POST to ticket instance_values' do
        def perform_request
          post :update_ticket_instance_values, params: { id: ticket.nice_id, attribute_value_ids: %w[abc def] }
        end

        before do
          Access::Policies::TicketPolicy.
            any_instance.
            stubs(edit_attribute_values?: edit_attribute_values)
        end

        before do
          client.
            stubs(:post).
            with("types/10/instances/#{ticket.id}/bulk_instance_values",
              attribute_value_ids: %w[abc def]).
            returns(
              stub(
                'response',
                body: {'items' => attribute_values_for_instance_fixtures}
              )
            )
        end

        def assert_response_body
          assert_equal(
            attribute_value_fixtures.map do |attribute_value|
              {
                name: attribute_value['name'],
                attribute_id: attribute_value['attribute_id']
              }
            end,
            json['attribute_values'].map do |attribute_value|
              {
                name: attribute_value['name'],
                attribute_id: attribute_value['attribute_id']
              }
            end
          )
        end

        describe 'when the user cannot edit the ticket skills' do
          let(:edit_attribute_values) { false }

          it 'responds with `403 Forbidden`' do
            perform_request
            assert_response :forbidden
          end
        end

        describe 'when the user can edit the ticket skills' do
          let(:edit_attribute_values) { true }

          before do
            perform_request
          end

          it 'responds with `200 ok`' do
            assert_response :ok
          end

          it('returns the correct response body') { assert_response_body }
        end
      end

      describe 'a GET to agent instance_values' do
        before do
          client.
            stubs(:get).
            with("type/20/instance/#{agent.id}/attribute_values").
            returns(
              stub(
                'response',
                body: {'items' => attribute_values_for_instance_fixtures}
              )
            )

          get :show_agent_instance_values, params: { id: agent.id }
        end

        it 'returns the values associated with the instance' do
          assert_equal(
            attribute_value_fixtures.map do |attribute_value|
              {
                name: attribute_value['name'],
                attribute_id: attribute_value['attribute_id']
              }
            end,
            json['attribute_values'].map do |attribute_value|
              {
                name: attribute_value['name'],
                attribute_id: attribute_value['attribute_id']
              }
            end
          )
        end
      end

      describe 'a POST to agent instance_values' do
        before do
          post :update_agent_instance_values, params: { id: 1, attribute_value_ids: [] }
        end

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe_with_arturo_enabled :rerun_skill_based_routing_rules do
        describe 'a PUT to /rules/execute' do
          before do
            Access::Policies::TicketPolicy.
              any_instance.
              stubs(edit_attribute_values?: edit_attribute_values)
          end

          before do
            attri_map_1 = new_attribute_ticket_map(attribute_value_id: '1')
            attri_map_2 = new_attribute_ticket_map(attribute_value_id: '2')
            attri_map_3 = new_attribute_ticket_map(attribute_value_id: '3')

            Zendesk::Rules::Match.stubs(:match?).
              with(attri_map_1, ticket).
              returns(true)

            Zendesk::Rules::Match.stubs(:match?).
              with(attri_map_2, ticket).
              returns(false)

            Zendesk::Rules::Match.stubs(:match?).
              with(attri_map_3, ticket).
              returns(true)
          end

          before do
            client.
              stubs(:post).
              with("types/10/instances/#{ticket.id}/bulk_instance_values",
                attribute_value_ids: %w[1 3]).
              returns(
                stub(
                  'response',
                  body: {'items' => attribute_values_for_instance_fixtures}
                )
              )
          end

          def assert_response_body
            assert_equal(
              attribute_value_fixtures.map do |attribute_value|
                {name: attribute_value['name']}
              end,
              json['attribute_values'].map do |attribute_value|
                {name: attribute_value['name']}
              end
            )
          end

          before do
            Ticket.any_instance.stubs(status_id: status_id)
          end

          describe "when a user can run routing rules manually" do
            let(:edit_attribute_values) { true }

            describe "when the ticket is not closed" do
              let(:status_id) { StatusType.OPEN }

              before do
                put :rerun_attribute_ticket_mapping_rules, params: { id: ticket.nice_id }
              end

              it 'responds with `200 ok`' do
                assert_response :ok
              end

              it('returns the correct response body') { assert_response_body }
            end

            describe "when the ticket is closed" do
              let(:status_id) { StatusType.CLOSED }

              before do
                put :rerun_attribute_ticket_mapping_rules, params: { id: ticket.nice_id }
              end

              it 'responds with unprocessable_entity' do
                assert_response :unprocessable_entity
              end
            end
          end

          describe 'when a user cannot run routing rules manually' do
            let(:edit_attribute_values) { false }

            describe "when the ticket is not closed" do
              let(:status_id) { StatusType.OPEN }

              before do
                put :rerun_attribute_ticket_mapping_rules, params: { id: ticket.nice_id }
              end

              it 'responds with `403 Forbidden`' do
                assert_response :forbidden
              end
            end

            describe "when the ticket is closed" do
              let(:status_id) { StatusType.CLOSED }

              before do
                put :rerun_attribute_ticket_mapping_rules, params: { id: ticket.nice_id }
              end

              it 'responds with `403 Forbidden`' do
                assert_response :forbidden
              end
            end
          end
        end
      end

      describe_with_arturo_disabled :rerun_skill_based_routing_rules do
        describe 'a PUT to /rules/execute' do
          before do
            put :rerun_attribute_ticket_mapping_rules, params: { id: ticket.nice_id }
          end

          it 'responds with `404 Not Found`' do
            assert_response :not_found
          end
        end
      end
    end

    as_an_admin do
      before do
        Zendesk::Deco::Client.stubs(:new).with(account).returns(client)
        AttributeTicketMap.destroy_all
      end

      describe 'a POST to ticket instance_values' do
        def perform_request
          post :update_ticket_instance_values, params: { id: ticket.nice_id, attribute_value_ids: %w[abc def] }
        end

        before do
          client.
            stubs(:post).
            with("types/10/instances/#{ticket.id}/bulk_instance_values",
              attribute_value_ids: %w[abc def]).
            returns(
              stub(
                'response',
                body: {'items' => attribute_values_for_instance_fixtures}
              )
            )
        end

        def assert_response_code
          perform_request

          assert_response :ok
        end

        def assert_response_body
          perform_request

          assert_equal(
            attribute_value_fixtures.map do |attribute_value|
              {
                name: attribute_value['name'],
                attribute_id: attribute_value['attribute_id']
              }
            end,
            json['attribute_values'].map do |attribute_value|
              {
                name: attribute_value['name'],
                attribute_id: attribute_value['attribute_id']
              }
            end
          )
        end

        describe 'when `AssociateAttValsEvent` creation fails' do
          let(:error) { StandardError.new('my error message') }

          before do
            AssociateAttValsEvent.expects(:new).raises(error)
            Rails.logger.expects(:warn).with(
              "Failed to create ticket audit event #{error.message}"
            ).once
          end

          it('returns the correct response code') { assert_response_code }

          it('returns the correct response body') { assert_response_body }
        end

        describe 'when `AssociateAttValsEvent` creation succeeds' do
          before do
            event_attribute_values = attribute_value_fixtures.map do |av|
              {
                id: av['id'],
                name: av['name'],
                attribute_id: av['attribute_id']
              }
            end
            associate_att_vals_event = AssociateAttValsEvent.new(
              attribute_values: event_attribute_values
            )
            AssociateAttValsEvent.expects(:new).with(
              attribute_values: event_attribute_values
            ).returns(associate_att_vals_event)
          end

          it('returns the correct response code') { assert_response_code }

          it('returns the correct response body') { assert_response_body }
        end
      end

      describe 'a POST to agent instance_values with an end-user id' do
        it 'responds with an error' do
          assert_raises(StandardError) do
            post :update_agent_instance_values, params: { id: end_user.id, attribute_value_ids: %w[abc def] }
          end
        end
      end

      describe 'a POST to agent instance_values with instance value ids' do
        def perform_request
          post :update_agent_instance_values, params: { id: agent.id, attribute_value_ids: %w[abc def] }
        end

        def assert_response_code
          perform_request

          assert_response :ok
        end

        def assert_response_body
          perform_request

          assert_equal(
            attribute_value_fixtures.map do |attribute_value|
              {
                name: attribute_value['name'],
                attribute_id: attribute_value['attribute_id']
              }
            end,
            json['attribute_values'].map do |attribute_value|
              {
                name: attribute_value['name'],
                attribute_id: attribute_value['attribute_id']
              }
            end
          )
        end

        before do
          client.
            stubs(:post).
            with("types/20/instances/#{agent.id}/bulk_instance_values",
              attribute_value_ids: %w[abc def]).
            returns(
              stub(
                'response',
                body: {'items' => attribute_values_for_instance_fixtures}
              )
            )
        end

        describe 'when CIA audit creation fails' do
          let(:error) { StandardError.new('my error message') }

          before do
            CIA::AttributeChange.expects(:new).raises(error)
            Rails.logger.expects(:warn).with(
              "Failed to create CIA::AttributeChange event #{error.message}"
            ).once
          end

          it('returns the correct response code') { assert_response_code }

          it('returns the correct response body') { assert_response_body }
        end

        describe 'when CIA audit creation succeeds' do
          before do
            CIA::AttributeChange.expects(:new).returns(stub(save!: true))
          end

          it('returns the correct response code') { assert_response_code }

          it('returns the correct response body') { assert_response_body }
        end
      end

      describe 'a POST to agent instance_values with no instance value ids' do
        before do
          client.
            stubs(:post).
            with("types/20/instances/#{agent.id}/bulk_instance_values",
              attribute_value_ids: []).
            returns(
              stub(
                'response',
                body: {'items' => []}
              )
            )

          cia_attribute_change_stub = stub(
            'cia_attribute_change_stub',
            save!: true
          )
          CIA::AttributeChange.stubs(:new).returns(cia_attribute_change_stub)

          post :update_agent_instance_values, params: { id: agent.id, attribute_value_ids: nil }
        end

        it 'succeeds' do
          assert_response :ok
        end
      end

      describe_with_arturo_enabled :rerun_skill_based_routing_rules do
        describe 'a PUT to /rules/execute' do
          before do
            attri_map_1 = new_attribute_ticket_map(attribute_value_id: '1')
            attri_map_2 = new_attribute_ticket_map(attribute_value_id: '2')
            attri_map_3 = new_attribute_ticket_map(attribute_value_id: '3')

            Zendesk::Rules::Match.stubs(:match?).
              with(attri_map_1, ticket).
              returns(true)

            Zendesk::Rules::Match.stubs(:match?).
              with(attri_map_2, ticket).
              returns(false)

            Zendesk::Rules::Match.stubs(:match?).
              with(attri_map_3, ticket).
              returns(true)
          end

          before do
            client.
              stubs(:post).
              with("types/10/instances/#{ticket.id}/bulk_instance_values",
                attribute_value_ids: %w[1 3]).
              returns(
                stub(
                  'response',
                  body: {'items' => attribute_values_for_instance_fixtures}
                )
              )
          end

          before do
            Ticket.any_instance.stubs(status_id: status_id)
          end

          def assert_response_body
            assert_equal(
              attribute_value_fixtures.map do |attribute_value|
                {name: attribute_value['name']}
              end,
              json['attribute_values'].map do |attribute_value|
                {name: attribute_value['name']}
              end
            )
          end

          describe "when a user can run routing rules manually" do
            describe "when the ticket is not closed" do
              let(:status_id) { StatusType.OPEN }

              before do
                put :rerun_attribute_ticket_mapping_rules, params: { id: ticket.nice_id }
              end

              it 'responds with `200 ok`' do
                assert_response :ok
              end

              it('returns the correct response body') { assert_response_body }
            end

            describe "when the ticket is closed" do
              let(:status_id) { StatusType.CLOSED }

              before do
                put :rerun_attribute_ticket_mapping_rules, params: { id: ticket.nice_id }
              end

              it 'responds with unprocessable_entity' do
                assert_response :unprocessable_entity
              end
            end
          end
        end
      end

      describe_with_arturo_disabled :rerun_skill_based_routing_rules do
        describe 'a PUT to /rules/execute' do
          before do
            put :rerun_attribute_ticket_mapping_rules, params: { id: ticket.nice_id }
          end

          it 'responds with `404 Not Found`' do
            assert_response :not_found
          end
        end
      end
    end
  end

  describe_with_arturo_disabled :skill_based_ticket_routing do
    as_an_admin do
      describe 'a GET to `instance_values`' do
        before do
          get :show_ticket_instance_values, params: { id: ticket.nice_id }
        end

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe 'a POST to `instance_values`' do
        before do
          post :update_ticket_instance_values, params: { id: 1, attribute_value_ids: %w[abc def] }
        end

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end

      describe_with_arturo_enabled :rerun_skill_based_routing_rules do
        describe 'a PUT to /rules/execute' do
          before do
            put :rerun_attribute_ticket_mapping_rules, params: { id: ticket.nice_id }
          end

          it 'responds with `404 Not Found`' do
            assert_response :not_found
          end
        end
      end

      describe_with_arturo_disabled :rerun_skill_based_routing_rules do
        describe 'a PUT to /rules/execute' do
          before do
            put :rerun_attribute_ticket_mapping_rules, params: { id: ticket.nice_id }
          end

          it 'responds with `404 Not Found`' do
            assert_response :not_found
          end
        end
      end
    end
  end
end
