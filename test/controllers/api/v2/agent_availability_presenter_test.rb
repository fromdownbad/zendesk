require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::V2::AgentAvailabilityPresenter do
  extend Api::Presentation::TestHelper
  fixtures :users, :user_settings, :accounts

  before do
    @model = users(:minimum_agent)
    @model.photo = FactoryBot.create(:photo, filename: 'large_1.png')
    @model.save!

    assert @model.settings.any?
    @presenter = Api::V2::AgentAvailabilityPresenter.new(@model, url_builder: mock_url_builder)
  end

  it "has 'agent' as the root key" do
    assert_equal :agent, @presenter.model_key
  end

  it 'inherits from Api::V2::Users::AgentPresenter' do
    assert @presenter.is_a?(Api::V2::Users::AgentPresenter)
  end
end
