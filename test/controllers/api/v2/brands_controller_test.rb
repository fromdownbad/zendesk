require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::BrandsController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :brands, :users, :role_settings, :routes, :account_settings, :subscription_features

  before do
    accept :json
    @account = accounts(:minimum)
    @brand   = @account.default_brand
    @wombat  = FactoryBot.create(:brand, account_id: @account.id,
                                         name: 'wombat')
    @wombat.create_logo(
      account: @account,
      uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/small.png")
    )
    @kitkat = FactoryBot.create(:brand, account_id: @account.id, name: 'kitkat', subdomain: 'kitkat')
    @account.route = @kitkat.route
    @account.save!
  end

  with_options(controller: 'api/v2/brands') do |request|
    request.should_route :post,   '/api/v2/brands',    action: 'create'
    request.should_route :get,    '/api/v2/brands',    action: 'index'
    request.should_route :put,    '/api/v2/brands/1',  action: 'update',  id: '1'
    request.should_route :delete, '/api/v2/brands/1',  action: 'destroy', id: '1'
    request.should_route :get,    '/api/v2/brands/check_host_mapping', action: 'check_host_mapping'
    request.should_route :get,    '/api/v2/brands/1/check_host_mapping', action: 'check_host_mapping_for_brand', id: '1'
  end

  as_an_anonymous_user do
    before { @controller.send(:current_account).stubs(:has_multibrand?).returns(true) }
    should_be_unauthorized :create, :index, :update, :destroy, :check_host_mapping
  end

  as_an_end_user do
    before { @controller.send(:current_account).stubs(:has_multibrand?).returns(true) }
    should_be_forbidden :create, :index, :update, :destroy, :check_host_mapping
  end

  as_an_agent do
    describe "for accounts with the multibrand feature" do
      before { @controller.send(:current_account).stubs(:has_multibrand?).returns(true) }
      should_be_forbidden :create, :update, :destroy, :check_host_mapping

      describe "a GET to :index" do
        before do
          @wombat.update_attribute(:active, false)
          get :index
        end
        should_use_presenter Api::V2::AgentBrandPresenter, status: 200

        it "presents all brands for the account sorted by name" do
          res = JSON.parse(@response.body)['brands']
          assert_equal [@kitkat.id, @brand.id, @wombat.id], res.map { |brand| brand['id'] }
        end
      end

      describe "a GET to :show" do
        before { get :show, params: { id: @wombat.id } }
        should_use_presenter Api::V2::AgentBrandPresenter, status: 200

        it "presents the brand requested" do
          res = JSON.parse(@response.body)['brand']
          assert_equal @wombat.name, res['name']
          assert_equal @wombat.id, res['id']
        end

        it "presents a brand that has been deleted" do
          @wombat.update_attribute(:deleted_at, Time.now)
          get :show, params: { id: @wombat.id }

          res = JSON.parse(@response.body)['brand']
          assert_equal @wombat.name, res['name']
          assert_equal @wombat.id, res['id']
        end
      end
    end

    describe "for accounts without the multibrand feature" do
      before { @controller.send(:current_account).stubs(:has_multibrand?).returns(false) }

      describe "a GET to :index" do
        before do
          @wombat.update_attribute(:active, false)
          @sql_queries = sql_queries do
            get :index
          end
        end
        should_use_presenter Api::V2::AgentBrandPresenter, status: 200

        it "presents all brands for the account sorted by name" do
          res = JSON.parse(@response.body)['brands']
          assert_equal [@kitkat.id, @brand.id, @wombat.id], res.map { |brand| brand['id'] }
        end

        it "does not query for the ticket forms many times" do
          assert_sql_queries(1, /FROM `ticket_forms`/)
        end
      end

      describe "a GET to :show" do
        describe "with an active brand" do
          before { get :show, params: { id: @wombat.id } }

          should_use_presenter Api::V2::AgentBrandPresenter, status: 200

          it "presents the brand requested" do
            res = JSON.parse(@response.body)['brand']
            assert_equal @wombat.name, res['name']
            assert_equal @wombat.id, res['id']
            refute res['is_deleted']
          end
        end

        it "presents a brand that has been deleted" do
          @wombat.update_column(:deleted_at, Time.now)
          get :show, params: { id: @wombat.id }

          res = JSON.parse(@response.body)['brand']
          assert_equal @wombat.name, res['name']
          assert_equal @wombat.id, res['id']
          assert(res['is_deleted'])
        end
      end
    end
  end

  as_an_admin do
    describe "for accounts with the multibrand feature" do
      before { @controller.send(:current_account).stubs(:has_multibrand?).returns(true) }
      let(:valid_params) do
        {
                      brand: {
                name:      'Giraffe',
                subdomain: 'giraffe',
                active:    true,
                logo: {
                  uploaded_data: fixture_file_upload('small.png')
                },
                signature_template: "Giraffe Support"
              }
            }
      end

      describe "a POST to :create" do
        describe "with a valid payload" do
          before do
            post :create, params: valid_params
          end

          should_use_presenter Api::V2::AdminBrandPresenter, status: 201

          should_change("the account brands count", by: 1) { @account.brands.count(:all) }
          should_change("the account routes count", by: 1) { @account.routes.count(:all) }

          it "creates route" do
            assert_equal "giraffe", @controller.send(:current_account).brands.last.route.subdomain
          end
        end

        describe "with invalid payload" do
          before do
            params = {brand: {active: true}}
            post :create, params: params
          end
          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
        end
      end

      describe "a GET to :index" do
        before do
          @brand.update_attribute(:active, false)
          get :index
        end

        should_use_presenter Api::V2::AdminBrandPresenter, status: 200

        it "presents all brands for the account sorted by name" do
          res = JSON.parse(@response.body)['brands']
          assert_equal [@kitkat.id, @brand.id, @wombat.id], res.map { |brand| brand['id'] }
        end

        describe "adds cache-control headers for apps" do
          before do
            request.headers.merge! "X-Zendesk-App-Id" => "b4dApp"
          end

          describe_with_arturo_enabled :brands_index_cache_control_header do
            it "responds with success and with cache headers" do
              get :index
              assert_response :success
              assert_equal 'max-age=60, private', @response.headers['Cache-Control']
            end
          end

          describe_with_arturo_disabled :brands_index_cache_control_header do
            it "responds with success and no cache headers" do
              get :index
              assert_response :success
              assert_nil @response.headers['Cache-Control']
            end
          end
        end
      end

      describe "a GET to :index with deleted brands" do
        before do
          @wombat.update_attribute(:active, false)
          @wombat.update_column(:deleted_at, Time.now)

          get :index
        end

        it "does not include a deleted brand in the response" do
          res = JSON.parse(@response.body)['brands']
          assert_nil res.detect { |brand| brand['name'] == @wombat.name && brand['id'] == @wombat.id }
        end
      end

      describe "a GET to :show" do
        before { get :show, params: { id: @wombat.id } }
        should_use_presenter Api::V2::AdminBrandPresenter, status: 200

        it "presents the brand requested" do
          res = JSON.parse(@response.body)['brand']
          assert_equal @wombat.name, res['name']
          assert_equal @wombat.id, res['id']
        end

        it "presents a brand that has been deleted" do
          @wombat.update_attribute(:deleted_at, Time.now)
          get :show, params: { id: @wombat.id }

          res = JSON.parse(@response.body)['brand']
          assert_equal @wombat.name, res['name']
          assert_equal @wombat.id, res['id']
        end
      end

      describe "a PUT to :update" do
        before do
          mock_cname "monkey.zendesk-test.com"
          put :update, params: { id: @brand.id, brand: {
              name: 'Monkey',
              subdomain: 'monkey',
              host_mapping: 'monkey.zendesk-test.com',
              signature_template: "Monkey Signature"
            } }
        end

        should_use_presenter Api::V2::AdminBrandPresenter, status: 200

        it "updates the brand with given params" do
          res = JSON.parse(@response.body)['brand']
          assert_equal 'Monkey', res['name']
          assert_equal 'monkey', res['subdomain']
          assert_equal 'monkey.zendesk-test.com', res['host_mapping']
          assert_equal @brand.id, res['id']
          assert_equal "Monkey Signature", res['signature_template']
        end
      end

      describe "a PUT to :update with invalid params" do
        before do
          put :update, params: { id: 1234, brand: {
              name: 'stuff'
            } }
        end

        it "returns a 404" do
          assert_response 404
        end
      end

      describe "a PUT to :update with an invalid subdomain change" do
        before { Brand.any_instance.stubs(invalid_subdomain_change?: true) }

        before do
          put :update, params: { id: @brand.id, brand: {
              subdomain: 'newsubdomain'
            } }
        end

        it "returns :unprocessable_entity" do
          assert_response :unprocessable_entity
        end
      end

      describe "a DELETE to :destroy" do
        describe "with a default brand id" do
          before { delete :destroy, params: { id: @brand.id } }

          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
          should_not_change("the account brands count") { @account.brands.count(:all) }
        end

        describe "with an agent brand id" do
          before do
            delete :destroy, params: { id: @kitkat.id }
          end

          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
          should_not_change("the account brands count") { @account.brands.count(:all) }
        end

        describe "with an active brand id" do
          before do
            delete :destroy, params: { id: @wombat.id }
          end

          it('responds with no_content') { assert_response :no_content }
          should_change("the account brands count", by: -1) { @account.brands.count(:all) }
        end

        describe "with an inactive brand id" do
          before do
            @wombat.update_attribute(:active, false)
            delete :destroy, params: { id: @wombat.id }
          end

          it('responds with no_content') { assert_response :no_content }
          should_change("the account brands count", by: -1) { @account.brands.count(:all) }
        end

        describe "with a soft deleted brand id" do
          before do
            @wombat.update_attribute(:active, false)
            @wombat.update_column(:deleted_at, Time.now)
            delete :destroy, params: { id: @wombat.id }
          end

          it('responds with no_content') { assert_response :no_content }
        end

        describe "with an invalid brand id" do
          before { delete :destroy, params: { id: '5000' } }
          it('responds with not_found') { assert_response :not_found }
          should_not_change("the account brands count") { @account.brands.count(:all) }
        end
      end

      describe "a GET to :check_host_mapping" do
        describe "when host_mapping is taken" do
          before do
            FactoryBot.create(:route, host_mapping: 'bar.com', account: @account)
          end

          it "fails and give reason if host_mapping is taken" do
            get :check_host_mapping, params: { subdomain: 'foo', host_mapping: 'bar.com' }
            res = JSON.parse(@response.body)
            refute res["is_valid"]
            assert_equal "already_taken", res["reason"]
          end
        end

        describe "when account should not use SSL" do
          before do
            @account.stubs(:ssl_should_be_used?).returns(false)
          end

          it "checks valid cname" do
            mock_cname "foo.zendesk-test.com"
            get :check_host_mapping, params: { subdomain: 'foo', host_mapping: 'bar.com' }
            res = JSON.parse(@response.body)
            assert(res["is_valid"])
            assert_equal "foo.zendesk-test.com", res["cname"]
          end

          it "checks missing cname" do
            mock_cname nil
            get :check_host_mapping, params: { subdomain: 'foo', host_mapping: 'bar.com' }
            res = JSON.parse(@response.body)
            refute res["is_valid"]
            assert_equal "not_a_cname", res["reason"]
            assert_equal ["foo.zendesk-test.com"], res["expected_cnames"]
          end

          it "checks wrong cname" do
            mock_cname "google.com"
            get :check_host_mapping, params: { subdomain: 'foo', host_mapping: 'bar.com' }
            res = JSON.parse(@response.body)
            refute res["is_valid"]
            assert_equal "wrong_cname", res["reason"]
            assert_equal ["foo.zendesk-test.com"], res["expected_cnames"]
            assert_equal "google.com", res["cname"]
          end
        end

        describe "when account should use SSL" do
          before do
            @account.stubs(:ssl_should_be_used?).returns(true)
          end

          it "checks missing cname" do
            mock_cname nil
            get :check_host_mapping, params: { subdomain: 'foo', host_mapping: 'bar.com' }
            res = JSON.parse(@response.body)
            refute res["is_valid"]
            assert_equal "not_a_cname", res["reason"]
            assert_equal ["foo.zendesk-test.com"], res["expected_cnames"]
          end

          it "checks wrong cname" do
            mock_cname "google.com"
            get :check_host_mapping, params: { subdomain: 'foo', host_mapping: 'bar.com' }
            res = JSON.parse(@response.body)
            refute res["is_valid"]
            assert_equal "wrong_cname", res["reason"]
            assert_equal ["foo.zendesk-test.com"], res["expected_cnames"]
            assert_equal "google.com", res["cname"]
          end

          it "allows correct cname" do
            mock_cname "foo.zendesk-test.com"
            get :check_host_mapping, params: { subdomain: 'foo', host_mapping: 'bar.com' }
            res = JSON.parse(@response.body)
            assert(res["is_valid"])
            assert_equal "foo.zendesk-test.com", res["cname"]
          end
        end
      end

      describe "for accounts without the multibrand feature" do
        before { @controller.send(:current_account).stubs(:has_multibrand?).returns(false) }
        should_be_forbidden :create, :update, :destroy, :check_host_mapping

        describe "a GET to :check_host_mapping_for_brand" do
          describe "when the brand is not host mapped" do
            before do
              assert_nil @kitkat.host_mapping
            end

            it "fails and gives the correct reason" do
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              refute res["is_valid"]
              assert_equal "not_host_mapped", res["reason"]
            end
          end

          describe "when account does not have a certificate" do
            before do
              @kitkat.update_attribute(:host_mapping, 'kitkat.zendesk-test.com')
              assert_empty(@account.certificates.active)
            end

            it "checks valid cname" do
              mock_cname "kitkat.zendesk-test.com"
              @account.update_attribute(:host_mapping, "kitkat.zendesk-test.com")
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              assert(res["is_valid"])
              assert_equal "kitkat.zendesk-test.com", res["cname"]
            end

            it "is not case sensitive" do
              mock_cname "kitkat.zendesk-test.com".upcase
              @account.update_attribute(:host_mapping, "kitkat.zendesk-test.com".upcase)
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              assert(res["is_valid"])
              assert_equal "kitkat.zendesk-test.com", res["cname"]
            end

            it "checks missing cname" do
              Zendesk::Net::CNAMEResolver.expects(:resolve?).returns(nil)
              @account.update_attribute(:host_mapping, 'bad')
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              refute res["is_valid"]
              assert_equal "not_a_cname", res["reason"]
              assert_equal ["kitkat.zendesk-test.com"], res["expected_cnames"]
            end

            it "checks wrong cname" do
              mock_cname "google.com"
              @account.update_attribute(:host_mapping, 'google.com')
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              refute res["is_valid"]
              assert_equal "wrong_cname", res["reason"]
              assert_equal ["kitkat.zendesk-test.com"], res["expected_cnames"]
              assert_equal "google.com", res["cname"]
            end

            it "disallows deprecated cname" do
              mock_cname "kitkat.ssl.zendesk-test.com"
              @account.update_attribute(:host_mapping, 'kitkat.ssl.zendesk-test.com')
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              refute res["is_valid"]
            end
          end

          describe "when account has an SNI certificate" do
            before do
              @kitkat.update_attribute(:host_mapping, 'kitkat.zendesk-test.com')
              c = @account.certificates.build
              c.sni_enabled = true
              c.save!
              c.update_attribute(:state, 'active')
            end

            it "checks valid cname" do
              mock_cname "kitkat.zendesk-test.com"
              @account.update_attribute(:host_mapping, 'kitkat.zendesk-test.com')
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              assert(res["is_valid"], res)
              assert_equal "kitkat.zendesk-test.com", res["cname"]
            end

            it "disallows ssl cname" do
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              refute res["is_valid"]
            end

            it "checks missing cname" do
              Zendesk::Net::CNAMEResolver.expects(:resolve?).returns(nil)
              @account.update_attribute(:host_mapping, 'bad')
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              refute res["is_valid"]
              assert_equal "not_a_cname", res["reason"]
              assert_equal ["kitkat.zendesk-test.com"], res["expected_cnames"]
            end

            it "checks wrong cname" do
              mock_cname "google.com"
              @account.update_attribute(:host_mapping, 'google.com')
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              refute res["is_valid"]
              assert_equal "wrong_cname", res["reason"]
              assert_equal ["kitkat.zendesk-test.com"], res["expected_cnames"]
              assert_equal "google.com", res["cname"]
            end
          end

          describe "when account has an IP certificate" do
            before do
              @kitkat.update_attribute(:host_mapping, 'kitkat.zendesk-test.com')
              c = @account.certificates.create!
              c.update_attribute(:state, 'active')
            end

            it "checks valid cname" do
              mock_cname "kitkat.zendesk-test.com"
              @account.update_attribute(:host_mapping, 'kitkat.zendesk-test.com')
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              assert(res["is_valid"])
              assert_equal "kitkat.zendesk-test.com", res["cname"]
            end

            it "checks missing cname" do
              Zendesk::Net::CNAMEResolver.expects(:resolve?).returns(nil)
              @account.update_attribute(:host_mapping, 'bad')
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              refute res["is_valid"]
              assert_equal "not_a_cname", res["reason"]
              assert_equal ["kitkat.zendesk-test.com"], res["expected_cnames"]
            end

            it "checks wrong cname" do
              mock_cname "google.com"
              @account.update_attribute(:host_mapping, 'google.com')
              get :check_host_mapping_for_brand, params: { id: @kitkat.id }

              res = JSON.parse(@response.body)
              refute res["is_valid"]
              assert_equal "wrong_cname", res["reason"]
              assert_equal ["kitkat.zendesk-test.com"], res["expected_cnames"]
              assert_equal "google.com", res["cname"]
            end
          end
        end
      end

      describe '#reload_kasket_route' do
        it "logs a warning if the brand has no route" do
          Route.any_instance.expects(:reload).once.raises(ActiveRecord::RecordNotFound)
          Rails.logger.expects(:warn).with("Failed to reload route back into kasket for brand '#{@brand.subdomain}': ActiveRecord::RecordNotFound").once
          @controller.send(:reload_kasket_route, @brand)
        end
        it "reloads if the brand has a route_id" do
          @brand.route.expects(:reload).once
          @controller.send(:reload_kasket_route, @brand)
        end
      end
    end
  end

  ['embeddable', 'knowledge_api', 'zopim', 'collaboration'].each do |subsystem_user|
    as_a_subsystem_user(account: :minimum, user: subsystem_user) do
      should_be_forbidden :destroy, :check_host_mapping, :create, :update

      describe "a GET to :index" do
        before { get :index }
        it("responds with success") { assert_response :success }
      end

      describe "a GET to :index with deleted brands" do
        before do
          @wombat.update_attribute(:active, false)
          @wombat.update_column(:deleted_at, Time.now)
          @controller.send(:current_account).stubs(:has_multibrand?).returns(true)

          get :index
        end

        it "excludes deleted brands in the response" do
          res = JSON.parse(@response.body)['brands']
          assert_nil res.detect { |brand| brand['name'] == @wombat.name && brand['id'] == @wombat.id }
        end
      end
    end
  end

  ["gooddata", "bime"].each do |subsystem_user|
    as_a_subsystem_user(user: subsystem_user, account: :minimum) do
      should_be_forbidden :create, :update, :destroy

      describe "a GET to :index" do
        before do
          @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
          get :index
        end

        it('responds with success') { assert_response :success }
      end

      describe "a GET to :index with deleted brands" do
        before do
          @wombat.update_attribute(:active, false)
          @wombat.update_column(:deleted_at, Time.now)
          @controller.send(:current_account).stubs(:has_multibrand?).returns(true)

          get :index
        end

        it "includes a deleted brand in the response" do
          res = JSON.parse(@response.body)['brands']
          assert_not_nil res.detect { |brand| brand['name'] == @wombat.name && brand['id'] == @wombat.id }
        end
      end
    end
  end

  as_a_subsystem_user(account: :minimum, user: "answer_bot_flow_composer") do
    should_be_forbidden :destroy, :check_host_mapping, :create, :update, :show

    describe "a GET to :index" do
      before { get :index }
      it('responds with ok') { assert_response :ok }
    end
  end

  private

  def mock_cname(cname)
    mocked = cname && mock(name: cname)
    Zendesk::Net::CNAMEResolver.expects(:resolve?).returns(mocked)
  end
end
