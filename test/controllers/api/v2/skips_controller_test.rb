require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::V2::SkipsController do
  extend Api::V2::TestHelper

  fixtures :accounts,
    :tickets,
    :users

  let(:account) { accounts(:minimum) }
  let(:ticket)  { tickets(:minimum_1) }
  let(:agent)   { users(:minimum_agent) }

  let(:response_body) { JSON.parse(@response.body) }

  before do
    use_ssl

    accept :json
  end

  with_options controller: 'api/v2/skips' do |request|
    [
      [:post, '/api/v2/skips',           {action: :create}],
      [:get,  '/api/v2/skips',           {action: :index}],
      [:get,  '/api/v2/tickets/1/skips', {action: :index, ticket_id: 1}],
      [:get,  '/api/v2/users/2/skips',   {action: :index, user_id: 2}]
    ].each do |verb, path, options|
      request.should_route(verb, path, options)
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized %i[post create]
    should_be_unauthorized %i[get index]
  end

  as_an_end_user do
    should_be_forbidden %i[post create]
    should_be_forbidden %i[get index]
  end

  as_an_agent do
    describe '#create' do
      let(:current_user) { @user }
      let(:reason)       { 'Because I wanted to.' }

      before { @request.account = account }

      describe 'when the ticket exists' do
        before do
          post :create, params: { skip: {ticket_id: ticket.nice_id, reason: reason} }
        end

        should_change 'skips', by: 1 do
          account.skips.count(:all)
        end

        it 'creates a skip with the correct attributes' do
          assert(
            account.skips.where(
              ticket_id: ticket.id,
              user_id:   current_user.id,
              reason:    reason
            ).any?
          )
        end

        should_use_presenter Api::V2::SkipPresenter

        it 'responds with `201 Created`' do
          assert_response :created
        end
      end

      describe 'when the ticket does not exist' do
        before { post :create, params: { skip: {ticket_id: 123456789, reason: reason} } }

        should_not_change 'skips' do
          account.skips.count(:all)
        end

        it 'responds with `404 Not Found`' do
          assert_response :not_found
        end
      end
    end
  end

  as_an_agent_with_permissions(report_access: 'none') do
    describe '#index' do
      describe 'without any parameters' do
        before { get :index }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe 'with a `ticket_id` parameter' do
        before { get :index, params: { ticket_id: ticket.nice_id } }

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe 'with a `user_id` parameter' do
        describe 'for the current user' do
          let(:current_user) { @user }

          before { get :index, params: { user_id: current_user.id } }

          it 'responds with `200 OK`' do
            assert_response :ok
          end
        end

        describe 'for another agent' do
          let(:other_agent) { users(:minimum_admin) }

          before { get :index, params: { user_id: other_agent.id } }

          it 'responds with `403 Forbidden`' do
            assert_response :forbidden
          end
        end
      end

      describe 'with a `ticket_id` and `user_id` parameter' do
        describe 'for the current user' do
          let(:current_user) { @user }

          before do
            get :index, params: { ticket_id: ticket.nice_id, user_id: current_user.id }
          end

          it('responds with ok') { assert_response :ok }
        end

        describe 'for another agent' do
          let(:other_agent) { users(:minimum_admin) }

          before do
            get :index, params: { ticket_id: ticket.nice_id, user_id: other_agent.id }
          end

          it('responds with forbidden') { assert_response :forbidden }
        end
      end
    end
  end

  as_an_agent_with_permissions(report_access: 'readonly') do
    describe '#index' do
      let(:current_user) { @user }
      let(:other_agent)  { users(:minimum_admin) }

      let(:other_ticket)    { tickets(:minimum_2) }
      let(:deleted_ticket)  { Ticket.unscoped { tickets(:minimum_deleted) } }
      let(:archived_ticket) { Ticket.unscoped { tickets(:minimum_archived) } }

      let(:base_time) { Time.utc(2017) }

      let(:accessible_skips) do
        account.
          skips.
          joins(:ticket).
          where(tickets: {status_id: Ticket::ACCESSIBLE_STATUSES})
      end

      before do
        Timecop.freeze(base_time)

        3.times do |i|
          Timecop.travel(base_time + i) do
            account.skips.create(ticket_id: ticket.id, user_id: current_user.id)
          end
        end

        2.times do |i|
          Timecop.travel(base_time + 3 + i) do
            account.skips.create(ticket_id: ticket.id, user_id: other_agent.id)
          end
        end

        2.times do |i|
          Timecop.travel(base_time + 5 + i) do
            account.skips.create(
              ticket_id: other_ticket.id,
              user_id:   other_agent.id
            )
          end
        end

        [deleted_ticket, archived_ticket].each do |inaccessible_ticket|
          account.skips.create(
            ticket_id: inaccessible_ticket.id,
            user_id:   current_user.id
          )
        end
      end

      describe 'without any parameters' do
        before { get :index }

        it 'returns skips for all users' do
          assert_equal 7, response_body['skips'].count
        end

        should_use_presenter Api::V2::SkipPresenter

        it 'responds with `200 OK`' do
          assert_response :ok
        end
      end

      describe 'with a `ticket_id` parameter' do
        before { get :index, params: { ticket_id: ticket.nice_id } }

        it 'returns skips for the ticket' do
          assert_equal 5, response_body['skips'].count
        end
      end

      describe 'with a `user_id` parameter' do
        describe 'for the current user' do
          before { get :index, params: { user_id: current_user.id } }

          it 'returns skips for the current user' do
            assert_equal 3, response_body['skips'].count
          end
        end

        describe 'for another agent' do
          before { get :index, params: { user_id: other_agent.id } }

          it 'returns skips for the specified agent' do
            assert_equal 4, response_body['skips'].count
          end
        end
      end

      describe 'with a `ticket_id` and `user_id` parameter' do
        describe 'for the current user' do
          before { get :index, params: { ticket_id: ticket.id, user_id: current_user.id } }

          it 'returns skips for the current user' do
            assert_equal 3, response_body['skips'].count
          end
        end

        describe 'for another agent' do
          before { get :index, params: { ticket_id: ticket.id, user_id: other_agent.id } }

          it 'returns skips for the specified agent' do
            assert_equal 4, response_body['skips'].count
          end
        end
      end

      describe 'without a `sort_order` parameter' do
        before { get :index }

        it 'orders ascending by `created_at`' do
          assert_equal(
            accessible_skips.order('skips.created_at ASC').map(&:id),
            response_body['skips'].map { |skip| skip['id'] }
          )
        end
      end

      describe 'with a `sort_order` parameter' do
        before { get :index, params: { sort_order: 'desc' } }

        it 'orders in the specified direction by `created_at`' do
          assert_equal(
            accessible_skips.order('skips.created_at DESC').map(&:id),
            response_body['skips'].map { |skip| skip['id'] }
          )
        end
      end

      describe 'with an invalid `sort_order` parameter' do
        before { get :index, params: { sort_order: 'bacon' } }

        it 'orders ascending by `created_at`' do
          assert_equal(
            accessible_skips.order('skips.created_at ASC').map(&:id),
            response_body['skips'].map { |skip| skip['id'] }
          )
        end
      end
    end
  end
end
