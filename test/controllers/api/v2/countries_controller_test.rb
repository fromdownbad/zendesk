require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::CountriesController do
  extend Api::V2::TestHelper

  before do
    accept :json
  end

  with_options(controller: "api/v2/countries") do |request|
    request.should_route :get, "/api/v2/countries", action: :index
  end

  as_an_anonymous_user do
    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::CountryPresenter
    end

    describe "a GET to :show" do
      before { get :show, params: { id: 2 } }
      should_use_presenter Api::V2::CountryPresenter
    end
  end
end
