require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TimeZonesController do
  extend Api::Presentation::TestHelper

  before do
    accept :json
  end

  with_options(controller: 'api/v2/time_zones') do |request|
    request.should_route :get, '/api/v2/time_zones', action: 'index'
  end

  it 'routes /api/v2/time_zones/:id to #show' do
    {
      'America%2FLos_Angeles.json' => { id: 'America/Los_Angeles', format: 'json' },
      'Atlantic%20Time%20(Canada)' => { id: 'Atlantic Time (Canada)' },
      'Cape%20Verde%20Is.'         => { id: 'Cape Verde Is.' },
      'Cape%20Verde%20Is..json'    => { id: 'Cape Verde Is.', format: 'json' }
    }.each do |segment, params|
      assert_recognizes(params.merge(controller: 'api/v2/time_zones', action: 'show'), path: "/api/v2/time_zones/#{segment}", method: :get)
    end
  end

  as_an_anonymous_user do
    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::TimeZonePresenter, status: :ok

      it 'presents time zones' do
        amsterdam = ActiveSupport::TimeZone["Amsterdam"]
        time_zones = JSON.parse(@response.body)['time_zones']
        assert_not_nil time_zones.find { |tz| tz['name'] == amsterdam.name }
      end

      it 'sorts by offset, then name' do
        time_zones = JSON.parse(@response.body)['time_zones']

        sorted = time_zones.sort do |x, y|
          offset_diff = x['offset'] <=> y['offset']
          offset_diff == 0 ? x['name'] <=> y['name'] : offset_diff
        end

        assert_equal time_zones, sorted
      end

      it 'caches for a week' do
        assert_match /max-age=604800/, @response.headers['Cache-Control']
      end

      it 'caches publicly, so reverse proxies can reduce load' do
        @response.headers['Cache-Control'].must_include "public"
      end

      it 'sets an ETag' do
        assert_not_nil @response.headers['ETag']
      end
    end

    describe "a GET to :show" do
      describe "with an Olson-style name" do
        before { get :show, params: { id: 'Europe/Oslo', format: :json } }

        should_use_presenter Api::V2::TimeZonePresenter, status: :ok

        it 'caches for a week' do
          assert_match /max-age=604800/, @response.headers['Cache-Control']
        end

        it 'caches publicly' do
          @response.headers['Cache-Control'].must_include "public"
        end

        it 'sets an ETag' do
          assert @response.headers['ETag'].present?, 'Expected an ETag header, but none was found.'
        end

        it 'strips session set-cookie' do
          assert request.env['rack.session.options'][:skip]
        end
      end

      describe "with a Rails-style name" do
        before { get :show, params: { id: 'Berlin', format: :json } }
        it('responds with ok') { assert_response :ok }
      end

      describe "with a strange, but valid name" do
        before { get :show, params: { id: 'Cape Verde Is.', format: :json } }
        it('responds with ok') { assert_response :ok }
        it 'picks the right time zone' do
          assert_match /Cape Verde Is\./, @response.body
        end
      end

      describe "with an unknown timezone name" do
        before { get :show, params: { id: 'Ramen', format: :json } }
        it('responds with not_found') { assert_response :not_found }
      end
    end
  end
end
