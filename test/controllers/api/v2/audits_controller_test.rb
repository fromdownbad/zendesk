require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AuditsController do
  extend Api::Presentation::TestHelper
  fixtures :all

  before do
    accept :json
    @agent   = users(:minimum_agent)
    @account = accounts(:minimum)
    @request.account = @account

    @ticket = tickets(:minimum_1)

    @event = events(:create_comment_for_minimum_ticket_1)
    @audit = @event.audit

    # Constants to assert against in various tests
    @invalid_ticket_id = 100000000
    @audit_fixture_size = 2
    @audit_events_comment_fixture_size = 2
  end

  with_options(controller: 'api/v2/audits') do |request|
    request.should_route :get,  '/api/v2/tickets/8783/audits',                 action: 'index',        ticket_id: '8783'
    request.should_route :get,  '/api/v2/tickets/8783/audits/123',             action: 'show',         ticket_id: '8783', id: '123'
    request.should_route :put, '/api/v2/tickets/8783/audits/123/make_private', action: 'make_private', ticket_id: '8783', id: '123'
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :show, [:put, :make_private, {id: 1}], add_to_all: {ticket_id: 1}
  end

  as_an_end_user do
    should_be_forbidden :index, :show, [:put, :make_private, {id: 1}], add_to_all: {ticket_id: 1}
  end

  as_an_agent do
    describe "a GET to #index" do
      describe "for a non-existent ticket" do
        before { get :index, params: { ticket_id: @invalid_ticket_id } }
        it('responds with not_found') { assert_response :not_found }
      end

      describe "for an existing ticket" do
        before do
          @sql_queries = sql_queries do
            get :index, params: { ticket_id: @ticket.nice_id }
          end
        end

        should_use_presenter Api::V2::Tickets::AuditPresenter, with: :audits

        it "does not run N+1 queries when listing audits" do
          # If you have a test failing here, make sure you're not just introducing an N+1
          assert_sql_queries 16..18
        end
      end

      describe "forces the index for the query planner to prevent long running queries" do
        it "uses force index on the audits query" do
          assert_sql_queries(1, /FROM events FORCE INDEX\(index_events_on_ticket_id_and_type\)/) do
            get :index, params: { ticket_id: @ticket.nice_id }
          end
        end
      end

      it "can find archived audits" do
        archive_and_delete(@ticket)
        get :index, params: { ticket_id: @ticket.nice_id }
        JSON.parse(@response.body)["audits"].size.must_equal @audit_fixture_size
      end

      describe "filtering events" do
        before do
          events(:create_comment_for_minimum_ticket_1)
          events(:create_change_for_minimum_ticket_1)
          get :index, params: { ticket_id: @ticket.nice_id, filter_events: ["Comment"] }
        end

        should_use_presenter Api::V2::Tickets::AuditPresenter, with: :audits

        it "only returns audit with events with the specified type" do
          events_json = JSON.parse(@response.body)["audits"].first["events"]
          assert_equal @audit_events_comment_fixture_size, events_json.size
          assert_equal "Comment", events_json.map { |e| e["type"] }.uniq.first
        end
      end

      describe "with an invalidly created follow-up ticket" do
        before do
          @source = tickets(:minimum_5)
          @source.update_attribute(:status_id, StatusType.CLOSED)

          @submitter = users(:minimum_admin)

          @ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), @submitter, ticket: { subject: "Hello", description: "Goodbye", via_followup_source_id: @source.nice_id })

          @ticket = @ticket_initializer.ticket
          @ticket.save!

          audit = @ticket.audits.first

          assert_equal ViaType.CLOSED_TICKET, audit.via_id
          assert_equal @source.id, audit.via_reference_id

          audit.update_attribute(:via_reference_id, @source.nice_id)

          assert_equal @source.nice_id, audit.reload.via_reference_id

          get :index, params: { ticket_id: @ticket.nice_id }
        end

        it "resets the via_reference_id" do
          audit = @ticket.audits.first.reload
          assert_equal @source.id, audit.via_reference_id
        end

        describe "and then..." do
          before { get :index, params: { ticket_id: @ticket.nice_id } }

          it('responds with success') { assert_response :success }

          it "keeps the via_reference_id" do
            audit = @ticket.audits.first.reload
            assert_equal @source.id, audit.via_reference_id
          end
        end

        describe "readonly comment" do
          before do
            audit = @ticket.audits.first
            Comment.any_instance.stubs(readonly?: false)
            audit.comment.update_attribute(:via_reference_id, @source.nice_id)

            get :show, params: { ticket_id: @ticket.nice_id, id: audit.id }
          end

          it('responds with success') { assert_response :success }
        end
      end

      describe "Ticket is from HelpCenter" do
        [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
          describe "via_id is #{via_id}" do
            before do
              @ticket.update_column(:via_id, via_id)
              @ticket.update_column(:via_reference_id, ViaType.HELPCENTER)
            end

            describe "agent_as_end_user Arturo is disabled" do
              before do
                Arturo.disable_feature!(:agent_as_end_user)
              end

              it "succeeds" do
                get :index, params: { ticket_id: @ticket.nice_id }
                assert_response :ok
              end
            end

            describe "agent_as_end_user Arturo is enabled" do
              before do
                Arturo.enable_feature!(:agent_as_end_user)
              end

              it "fails" do
                get :index, params: { ticket_id: @ticket.nice_id }
                assert_response :forbidden
              end
            end
          end
        end
      end

      describe "pagination" do
        let(:params) { {} }
        let(:response) { JSON.parse(@response.body) }

        before do
          get :index, params: params.merge(ticket_id: @ticket.nice_id)
        end

        it "defaults to offset based pagination" do
          assert_includes response, "next_page"
          assert_includes response, "previous_page"

          refute_includes response, "after_cursor"
          refute_includes response, "before_cursor"
        end

        describe "with a cursor parameter" do
          let(:params) { {cursor: ""} }

          it "includes cursor parameters, not offset params" do
            refute_includes response, "next_page"
            refute_includes response, "previous_page"

            assert_includes response, "after_cursor"
            assert_includes response, "before_cursor"
          end
        end
      end

      describe "archived tickets" do
        let(:basic_params) { { ticket_id: @ticket.nice_id } }
        let(:params) { basic_params }
        let(:response) { JSON.parse(@response.body) }

        before do
          archive_and_delete(@ticket)
          get :index, params: params
        end

        describe "with offset pagination" do
          it "renders all events with no pagination" do
            assert_includes response, "next_page"
            assert_includes response, "previous_page"
            assert_nil response["next_page"]
            assert_nil response["previous_page"]
          end
        end

        describe "with cursor pagination" do
          let(:params) { basic_params.merge(limit: 2) }

          it "renders all events with no pagination" do
            assert_includes response, "after_cursor"
            assert_includes response, "before_cursor"
            assert_nil response["after_cursor"]
            assert_nil response["before_cursor"]
          end
        end

        describe "in descending order" do
          describe "with offset pagination" do
            let(:params) { basic_params.merge(sort_order: 'desc') }

            it "renders all events with no pagination" do
              assert_includes response, "next_page"
              assert_includes response, "previous_page"
              assert_nil response["next_page"]
              assert_nil response["previous_page"]
            end
          end

          describe "with cursor pagination" do
            let(:params) { basic_params.merge(sort_order: 'desc', limit: 2) }

            it "renders all events with no pagination" do
              assert_includes response, "after_cursor"
              assert_includes response, "before_cursor"
              assert_nil response["after_cursor"]
              assert_nil response["before_cursor"]
            end
          end
        end
      end
    end

    describe "a GET to #show" do
      describe "with a valid audit" do
        before { get :show, params: { ticket_id: @ticket.nice_id, id: @audit.id } }

        should_use_presenter Api::V2::Tickets::AuditPresenter
      end

      describe "with an invalid audit" do
        before { get :show, params: { ticket_id: @ticket.nice_id, id: @invalid_ticket_id } }

        it('responds with not_found') { assert_response :not_found }
      end

      describe "Ticket is from HelpCenter" do
        [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
          describe "via_id is #{via_id}" do
            before do
              @ticket.update_column(:via_id, via_id)
              @ticket.update_column(:via_reference_id, ViaType.HELPCENTER)
            end

            describe "agent_as_end_user Arturo is disabled" do
              before do
                Arturo.disable_feature!(:agent_as_end_user)
              end

              it "succeeds" do
                get :show, params: { ticket_id: @ticket.nice_id, id: @audit.id }
                assert_response :ok
              end
            end

            describe "agent_as_end_user Arturo is enabled" do
              before do
                Arturo.enable_feature!(:agent_as_end_user)
              end

              it "fails" do
                get :show, params: { ticket_id: @ticket.nice_id, id: @audit.id }
                assert_response :forbidden
              end
            end
          end
        end
      end
    end

    describe "a PUT to #make_private" do
      describe "with a public comment" do
        describe "authored by agent" do
          before do
            put :make_private, params: { ticket_id: @ticket.nice_id, id: @audit.id }
          end

          it('responds with ok') { assert_response :ok }

          it "makes the comment private" do
            assert_equal false, @audit.comment.reload.is_public?
          end
        end

        describe "Ticket is from HelpCenter" do
          [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
            describe "via_id is #{via_id}" do
              before do
                @ticket.update_column(:via_id, via_id)
                @ticket.update_column(:via_reference_id, ViaType.HELPCENTER)
              end

              describe "agent_as_end_user Arturo is disabled" do
                before do
                  Arturo.disable_feature!(:agent_as_end_user)
                end

                it "succeeds" do
                  put :make_private, params: { ticket_id: @ticket.nice_id, id: @audit.id }
                  assert_response :ok
                end
              end

              describe "agent_as_end_user Arturo is enabled" do
                before do
                  Arturo.enable_feature!(:agent_as_end_user)
                end

                it "fails" do
                  put :make_private, params: { ticket_id: @ticket.nice_id, id: @audit.id }
                  assert_response :forbidden
                end
              end
            end
          end
        end
      end

      describe "with a private comment" do
        before do
          @audit.comment.ticket.will_be_saved_by(@agent)
          @audit.comment.update_attributes!(is_public: false)
          put :make_private, params: { ticket_id: @ticket.nice_id, id: @audit.id }
        end

        it('responds with bad_request') { assert_response :bad_request }
      end
    end

    describe "#audits" do
      before do
        @ticket = tickets(:minimum_2)
        @ticket.will_be_saved_by(@agent)
        @ticket.status_id = StatusType.PENDING
        @ticket.save!
        @controller.params[:ticket_id] = @ticket.nice_id
      end

      it "defaults to order by created_at asc" do
        audits = @controller.send(:audits)
        assert_equal audits.sort_by { |a| a.created_at.to_i }, audits
      end

      it "respects the sort_order parameter" do
        @controller.params[:sort_order] = 'desc'
        audits = @controller.send(:audits)
        assert_equal audits.sort_by { |a| -a.created_at.to_i }, audits
      end

      it "respects the sort_order when the ticket is archived" do
        archive_and_delete(@ticket)

        @controller.params[:sort_order] = 'desc'
        audits = @controller.send(:audits)
        assert_equal audits.sort_by { |a| -a.created_at.to_i }, audits
      end

      it "returns answer bot notifications" do
        Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)

        d = Definition.new
        d.actions.push(DefinitionItem.new("deflection", "is", recipient: 'requester_id', subject: 'email subject', body: 'email body'))
        d.conditions_all.push(DefinitionItem.new("status_id", "is", [StatusType.NEW]))
        trigger = Trigger.create!(account: @account, title: 'deflection trigger', definition: d, owner: @account)

        @ticket.will_be_saved_by(User.system)

        ab_notification = AnswerBotNotification.new(recipients: Array(@ticket.requester.id), subject: 'test subject', body: 'test body', audit: @ticket.audit)
        ab_notification.via_id = ViaType.RULE
        ab_notification.via_reference_id = trigger.id

        @ticket.audit.events << ab_notification
        @ticket.save!

        audits = @controller.send(:audits)
        assert(audits.last.events.pluck(:type).include?("AnswerBotNotification"))
      end
    end
  end

  as_a_subsystem_user(user: "zopim", account: :minimum) do
    should_be_forbidden [:put, :make_private, {id: 1}], add_to_all: {ticket_id: 1}

    describe "a GET to #index" do
      describe "for a non-existent ticket" do
        before { get :index, params: { ticket_id: @invalid_ticket_id } }
        it('responds with not_found') { assert_response :not_found }
      end

      describe "for an existing ticket" do
        before do
          @sql_queries = sql_queries do
            get :index, params: { ticket_id: @ticket.nice_id }
          end
        end

        should_use_presenter Api::V2::Tickets::AuditPresenter, with: :audits

        it "does not run N+1 queries when listing audits" do
          # If you have a test failing here, make sure you're not just introducing an N+1
          assert_sql_queries 12..15
        end

        it "can find archived audits" do
          archive_and_delete(@ticket)
          get :index, params: { ticket_id: @ticket.nice_id }
          JSON.parse(@response.body)["audits"].size.must_equal @audit_fixture_size
        end
      end

      describe "filtering events" do
        before do
          get :index, params: { ticket_id: @ticket.nice_id, filter_events: ["Comment"] }
        end

        should_use_presenter Api::V2::Tickets::AuditPresenter, with: :audits

        it "only returns audit with events with the specified type" do
          events_json = JSON.parse(@response.body)["audits"].first["events"]
          assert_equal @audit_events_comment_fixture_size, events_json.size
          assert_equal "Comment", events_json.map { |e| e["type"] }.uniq.first
        end
      end

      describe "with an invalidly created follow-up ticket" do
        before do
          @source = tickets(:minimum_5)
          @source.update_attribute(:status_id, StatusType.CLOSED)

          @submitter = users(:minimum_admin)

          @ticket_initializer = Zendesk::Tickets::Initializer.new(accounts(:minimum), @submitter, ticket: { subject: "Hello", description: "Goodbye", via_followup_source_id: @source.nice_id })

          @ticket = @ticket_initializer.ticket
          @ticket.save!

          audit = @ticket.audits.first

          assert_equal ViaType.CLOSED_TICKET, audit.via_id
          assert_equal @source.id, audit.via_reference_id

          audit.update_attribute(:via_reference_id, @source.nice_id)

          assert_equal @source.nice_id, audit.reload.via_reference_id

          get :index, params: { ticket_id: @ticket.nice_id }
        end

        it "resets the via_reference_id" do
          audit = @ticket.audits.first.reload
          assert_equal @source.id, audit.via_reference_id
        end

        describe "and then..." do
          before { get :index, params: { ticket_id: @ticket.nice_id } }

          it('responds with success') { assert_response :success }

          it "keeps the via_reference_id" do
            audit = @ticket.audits.first.reload
            assert_equal @source.id, audit.via_reference_id
          end
        end

        describe "readonly comment" do
          before do
            audit = @ticket.audits.first
            Comment.any_instance.stubs(readonly?: false)
            audit.comment.update_attribute(:via_reference_id, @source.nice_id)

            get :show, params: { ticket_id: @ticket.nice_id, id: audit.id }
          end

          it('responds with success') { assert_response :success }
        end
      end
    end

    describe "a GET to #show" do
      describe "with a valid audit" do
        before { get :show, params: { ticket_id: @ticket.nice_id, id: @audit.id } }

        should_use_presenter Api::V2::Tickets::AuditPresenter
      end

      describe "with an invalid audit" do
        before { get :show, params: { ticket_id: @ticket.nice_id, id: @invalid_ticket_id } }

        it('responds with not_found') { assert_response :not_found }
      end
    end

    describe "#audits" do
      before do
        @ticket = tickets(:minimum_2)
        @ticket.will_be_saved_by(@agent)
        @ticket.status_id = StatusType.PENDING
        @ticket.save!
        @controller.params[:ticket_id] = @ticket.nice_id
      end

      it "defaults to order by created_at asc" do
        audits = @controller.send(:audits)
        assert_equal audits.sort_by { |a| a.created_at.to_i }, audits
      end

      it "respects the sort_order parameter" do
        @controller.params[:sort_order] = 'desc'
        audits = @controller.send(:audits)
        assert_equal audits.sort_by { |a| -a.created_at.to_i }, audits
      end

      it "respects the sort_order when the ticket is archived" do
        archive_and_delete(@ticket)

        @controller.params[:sort_order] = 'desc'
        audits = @controller.send(:audits)
        assert_equal audits.sort_by { |a| -a.created_at.to_i }, audits
      end

      it "returns answer bot notifications" do
        Account.any_instance.stubs(:has_automatic_answers_enabled?).returns(true)

        d = Definition.new
        d.actions.push(DefinitionItem.new("deflection", "is", recipient: 'requester_id', subject: 'email subject', body: 'email body'))
        d.conditions_all.push(DefinitionItem.new("status_id", "is", [StatusType.NEW]))
        trigger = Trigger.create!(account: @account, title: 'deflection trigger', definition: d, owner: @account)

        @ticket.will_be_saved_by(User.system)

        ab_notification = AnswerBotNotification.new(recipients: Array(@ticket.requester.id), subject: 'test subject', body: 'test body', audit: @ticket.audit)
        ab_notification.via_id = ViaType.RULE
        ab_notification.via_reference_id = trigger.id

        @ticket.audit.events << ab_notification
        @ticket.save!

        audits = @controller.send(:audits)
        assert(audits.last.events.pluck(:type).include?("AnswerBotNotification"))
      end
    end
  end
end
