require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::EndUsersController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :user_settings, :signatures, :organizations

  before do
    accept :json
  end

  with_options(controller: "api/v2/end_users") do |request|
    request.should_route :get,    "/api/v2/end_users/1", action: "show",    id: "1"
    request.should_route :put,    "/api/v2/end_users/1", action: "update",  id: "1"
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :show, id: 1], [:put, :update, id: 1]
  end

  as_an_end_user do
    describe "on a GET to :show" do
      describe "different user" do
        before do
          get :show, params: { id: users(:minimum_search_user).id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "same user" do
        before do
          get :show, params: { id: @user.id }
        end

        should_use_presenter Api::V2::Users::EndUserPresenter
      end
    end

    describe "on a PUT to :update" do
      describe "different user" do
        before do
          put :update, params: { id: users(:minimum_search_user).id, user: { name: 'test' } }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "same user" do
        before do
          put :update, params: { id: @user.id, user: { name: "test" } }
        end

        should_use_presenter Api::V2::Users::EndUserPresenter
      end
    end
  end
end
