require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ZopimIntegrationsController do
  extend Api::V2::TestHelper
  fixtures :users, :accounts, :role_settings

  before do
    @request.account = accounts(:minimum)
    ZopimIntegration.any_instance.stubs(
      app_path: 'some-path',
      app_installed?: true,
      ensure_not_phase_four: nil
    )
    stub_request(:post, %r{embeddable/api/internal/config_sets.json})
  end

  def create_zopim_integration(options = {})
    options = {
      account: @request.account,
      external_zopim_id: 1234,
      zopim_key: 'abcd'
    }.merge(options)
    ZopimIntegration.create!(options)
  end

  with_options(controller: "api/v2/zopim_integrations") do |request|
    request.should_route :get,    "/api/v2/zopim_integration.json", action: "show",    format: :json
    request.should_route :post,   "/api/v2/zopim_integration.json", action: "create",  format: :json
    request.should_route :put,    "/api/v2/zopim_integration.json", action: "update",  format: :json
    request.should_route :delete, "/api/v2/zopim_integration.json", action: "destroy", format: :json
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :show, {format: :json}],
      [:put, :update, {format: :json}],
      [:post, :create, {format: :json}],
      [:delete, :destroy, {format: :json}]
  end

  as_an_end_user do
    should_be_forbidden [:get, :show, {format: :json}],
      [:put, :update, {format: :json}],
      [:post, :create, {format: :json}],
      [:delete, :destroy, {format: :json}]
  end

  describe "embeddable subsystem user" do
    before do
      accept :json
    end

    as_a_subsystem_user(account: :minimum, user: "embeddable") do
      should_be_forbidden(
        [:post, :create, {}],
        [:put, :update, {}],
        [:delete, :destroy, {}]
      )

      describe "a GET to :show" do
        before do
          create_zopim_integration
          get :show, format: :json
        end

        it("responds with success") { assert_response :success }
      end
    end
  end

  describe "billing subsystem user" do
    before do
      accept :json
    end

    as_a_subsystem_user(account: :minimum, user: "billing") do
      should_be_forbidden(
        [:post, :create, {}],
        [:put, :update, {}],
        [:delete, :destroy, {}]
      )

      describe "a GET to :show" do
        before do
          create_zopim_integration
          get :show, format: :json
        end

        it("responds with success") { assert_response :success }
      end
    end
  end

  describe "sell subsystem user" do
    before do
      accept :json
    end

    as_a_subsystem_user(account: :minimum, user: "sell") do
      should_be_forbidden(
        [:post, :create, {}],
        [:put, :update, {}],
        [:delete, :destroy, {}]
      )

      describe "a GET to :show" do
        before do
          create_zopim_integration
          get :show, format: :json
        end

        it("responds with success") { assert_response :success }
      end
    end
  end

  def self.test_all
    describe '#show' do
      it 'gets zopim integration json' do
        zopim_integration = create_zopim_integration
        get :show, format: :json
        expected_response = {
          'zopim_integration' => {
            'external_zopim_id' => zopim_integration.external_zopim_id,
            'zopim_key'         => zopim_integration.zopim_key,
            'phase'             => 1,
            'agent_sync'        => false,
            'billing'           => false,
            'created_at'        => zopim_integration.created_at.strftime('%Y-%m-%dT%H:%M:%SZ'),
            'updated_at'        => zopim_integration.updated_at.strftime('%Y-%m-%dT%H:%M:%SZ')
          }
        }
        assert_equal expected_response, JSON.parse(@response.body)
      end
    end

    describe '#create' do
      it 'creates new zopim integration with valid input' do
        post :create, params: { zopim_integration: {external_zopim_id: 1234, zopim_key: 'abcd'}, format: :json }
        assert_equal 201, @response.status
        assert_equal 1234, JSON.parse(@response.body)['zopim_integration']['external_zopim_id']
        assert_equal "https://minimum.zendesk-test.com/api/v2/zopim_integration.json", @response.headers['Location']
      end

      it 'fails to create new zopim integration with invalid input' do
        post :create, params: { zopim_integration: 1234, format: :json }
        assert_equal 400, @response.status
      end

      it 'fails to create new zopim integration when zopim integration exists' do
        create_zopim_integration
        post :create, params: { zopim_integration: {external_zopim_id: 1234, zopim_key: 'abcd'}, format: :json }
        assert_equal 400, @response.status
        assert_equal 'Zopim integration already exists', @response.body
      end
    end

    describe '#update' do
      it 'updates zopim id with valid input' do
        zopim_integration = create_zopim_integration
        put :update, params: { zopim_integration: {external_zopim_id: 5678, zopim_key: 'xyz'}, format: :json }
        assert_equal 200, @response.status
        assert_equal 5678, JSON.parse(@response.body)['zopim_integration']['external_zopim_id']
        zopim_integration.reload
        assert_equal 5678, zopim_integration.external_zopim_id
        assert_equal 'xyz', zopim_integration.zopim_key
      end

      it 'fails to update zopim id with invalid input' do
        zopim_integration = create_zopim_integration(external_zopim_id: 1234)
        put :update, params: { zopim_integration: 5678, format: :json }
        assert_equal 400, @response.status
        zopim_integration.reload
        assert_equal 1234, zopim_integration.external_zopim_id
      end

      it 'updates zopim phase with valid input' do
        zopim_integration = create_zopim_integration
        put :update, params: { zopim_integration: {phase: 3}, format: :json }
        assert_equal 200, @response.status
        assert_equal 3, JSON.parse(@response.body)['zopim_integration']['phase']
        zopim_integration.reload
        assert_equal 1234, zopim_integration.external_zopim_id
        assert_equal 'abcd', zopim_integration.zopim_key
        assert_equal 3, zopim_integration.phase
      end

      it 'fails to update zopim phase with invalid input' do
        zopim_integration = create_zopim_integration
        expected_phase = zopim_integration.phase
        put :update, params: { zopim_integration: {phase: 'troll'}, format: :json }
        assert_equal 400, @response.status
        zopim_integration.reload
        assert_equal expected_phase, zopim_integration.phase
      end
    end

    describe '#destroy' do
      before do
        create_zopim_integration
      end

      describe "when it is a phase one integration" do
        before do
          ZopimIntegration.any_instance.expects(:uninstall_app)
        end

        it "responds with 204" do
          delete :destroy, format: :json
          assert_response :no_content
        end

        it "deletes the zopim_integration record" do
          delete :destroy, format: :json
          assert_nil ZopimIntegration.find_by_account_id(@account.id)
        end
      end

      describe "when it is a not a phase one integration" do
        before do
          ZopimIntegration.any_instance.stubs(phase_one?: false)
          delete :destroy, format: :json
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end
  end

  as_an_agent do
    test_all
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    test_all
  end
end
