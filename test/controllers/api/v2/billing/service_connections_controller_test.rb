require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Billing::ServiceConnectionsController do
  extend Api::V2::TestHelper

  fixtures :accounts

  let(:account)      { accounts(:minimum) }
  let(:user)         { account.owner }
  let(:subscription) { account.subscription }
  let(:assisted)     { anything }

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns(false)
    @controller.stubs(:current_account).returns(account)
    @controller.stubs(:current_user).returns(user)
    @controller.stubs(:current_registered_user).returns(user)
    subscription.stubs(:assisted?).returns(assisted)
    @request.account = account
    accept :json
  end

  with_options(controller: 'api/v2/billing/service_connections') do |request|
    request.should_route :post, '/api/v2/billing/service_connection', action: 'create'
  end

  describe 'when logged in as an admin to a fraudulent account' do
    before do
      account.settings.is_abusive = true
      account.settings.save
    end

    subject { post(:create) }

    it 'should be forbidden' do
      subject
      assert_response :forbidden
    end
  end

  describe 'when logged in as an admin to a fraudulent whitelisted account' do
    before do
      account.settings.is_abusive = true
      account.stubs(:whitelisted?).returns(true)
      account.settings.save
    end

    subject { post(:create) }

    it 'should be successful' do
      subject
      assert_response :success
    end
  end

  describe 'when as a user that is not logged in' do
    let(:user) { nil }

    subject { post(:create) }

    it 'should be unauthorized' do
      subject
      assert_response :unauthorized
    end
  end

  describe 'when logged in as an end user' do
    let(:user) { users(:minimum_end_user) }

    subject { post(:create) }

    it 'should be forbidden' do
      subject
      assert_response :forbidden
    end
  end

  describe 'when logged in as an agent' do
    let(:user) { users(:minimum_agent) }

    subject { post(:create) }

    it 'should be forbidden' do
      subject
      assert_response :forbidden
    end
  end

  as_an_admin do
    before do
      @controller.stubs(:current_account).returns(account)
      @controller.stubs(:current_user).returns(user)
    end

    describe '#create' do
      let(:service_connection) { stub(build: {}) }
      let(:locale_param)       { { 'locale' => user.locale } }

      describe 'when account is a multi-product participant' do
        before do
          account.stubs(:billing_multi_product_participant?).returns(true)

          Billing::ServiceConnection::V2.expects(:new).with(
            account,
            user,
            base_url: 'https://minimum.zendesk-test.com',
            params:   { 'greetings' => 'hello world' }.merge(locale_param)
          ).returns(service_connection)

          post :create, params: { greetings: 'hello world' }
        end

        it { assert_response :success }
        it { response.content_type.must_equal 'application/json' }

        should_use_presenter(
          Api::V2::Billing::ServiceConnectionPresenter,
          status: :success
        )
      end

      describe 'when account is not a multi-product participant' do
        before do
          account.stubs(:billing_multi_product_participant?).returns(false)
        end

        describe 'when subscription is Sales-Assisted' do
          let(:assisted) { true }

          before do
            Billing::ServiceConnection::V2.expects(:new).with(
              account,
              user,
              base_url: 'https://minimum.zendesk-test.com',
              params:   { 'greetings' => 'hello world' }.merge(locale_param)
            ).returns(service_connection)

            post :create, params: { greetings: 'hello world' }
          end

          it { assert_response :success }
          it { response.content_type.must_equal 'application/json' }

          should_use_presenter(
            Api::V2::Billing::ServiceConnectionPresenter,
            status: :success
          )
        end

        describe 'when subscription is Self-Service' do
          let(:assisted) { false }

          before do
            Billing::ServiceConnection::V1.expects(:new).with(
              account,
              user,
              { 'greetings' => 'hello world' }.merge(locale_param)
            ).returns(service_connection)

            post :create, params: { greetings: 'hello world' }
          end

          it { assert_response :success }
          it { response.content_type.must_equal 'application/json' }

          should_use_presenter(
            Api::V2::Billing::ServiceConnectionPresenter,
            status: :success
          )
        end
      end
    end
  end
end
