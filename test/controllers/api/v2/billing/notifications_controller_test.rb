require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Billing::NotificationsController do
  fixtures :all

  with_options(controller: 'api/v2/billing/notifications') do |request|
    request.should_route :post, '/api/v2/billing/notification', action: 'create'
  end

  let(:claims) do
    { iss: issuer, content: payload }
  end

  let(:payload) do
    { billing_id: 'feedbeef', master_account_id: account.id }
  end

  let(:issuer) { 'billing' }

  let(:account) { accounts(:multiproduct) }

  before do
    accept :json
    @request.account = account
    Billing::JWT.stubs(:decrypt).returns(claims)
  end

  let(:account) { accounts(:multiproduct) }

  as_a_subsystem_user(user: 'billing', account: :multiproduct) do
    describe '#create' do
      let(:token) { 'jwt-token' }

      describe 'when token and issuer is valid' do
        before do
          Billing::JWT.expects(:decrypt).
            with('jwt-token').returns(claims)
          ZBC::Zuora::Jobs::AccountSyncJob.expects(:enqueue).
            with('feedbeef').once
          post :create, params: { token: token }
        end

        it { assert_response :created }
      end

      describe 'when unable to decrypt token' do
        before do
          Billing::JWT.expects(:decrypt).
            with('jwt-token').
            raises(Billing::JWT::Decryptor::Error)
          post :create, params: { token: token }
        end

        it { assert_response :unauthorized }
      end

      describe 'when token issuer is not "billing"' do
        let(:issuer) { 'not-billing' }

        before { post :create, params: { token: token } }

        it { assert_response :forbidden }
      end
    end
  end
end
