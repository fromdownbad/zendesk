require_relative '../../../support/test_helper'
require_relative '../../../support/api_scopes_helper'

SingleCov.covered!

describe Api::V2::TargetsController do
  fixtures :targets, :users, :accounts, :subscription_features

  extend Api::Presentation::TestHelper
  extend Api::V2::TestHelper
  extend ApiScopesHelper

  let(:trial) { accounts(:trial) }
  let(:minimum) { accounts(:minimum) }

  let(:valid_params) do
    {
      target: {
        title: "My Title",
        type: "email_target",
        subject: "Xys",
        email: "aaa@bbb.com"
      }
    }
  end
  let (:valid_twitter) do
    {
      target: {
        title: "My Title",
        type: "email_target",
      }
    }
  end
  let(:target) { targets(:twitter_valid) }

  before do
    @account  = accounts(:minimum)
    @agent    = users(:minimum_agent)

    accept :json
  end

  with_options(controller: 'api/v2/targets') do |request|
    request.should_route :get,    '/api/v2/targets',     action: 'index'
    request.should_route :post,   '/api/v2/targets',     action: 'create'
    request.should_route :get,    '/api/v2/targets/1',   action: 'show',    id: 1
    request.should_route :put,    '/api/v2/targets/1',   action: 'update',  id: 1
    request.should_route :delete, '/api/v2/targets/1',   action: 'destroy', id: 1
  end

  describe "when using oauth tokens" do
    with_scopes('targets:read', 'read') do
      should_be_authorized { get :index }
      should_be_authorized { get :show, params: { id: target.id } }

      should_not_be_authorized { post :create, params: valid_params }
    end

    with_scopes('targets:write', 'write') do
      should_be_authorized { post :create, params: valid_params }
      should_be_authorized { put :update, params: { id: target.id, target: { title: 'My Title' } } }
      should_be_authorized { delete :destroy, params: { id: target.id } }

      should_not_be_authorized { get :index }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :create, :show, :update, :destroy
  end

  as_an_end_user do
    should_be_forbidden :index, :create, :show, :update, :destroy
  end

  describe "when not having permission to edit" do
    as_an_agent do
      should_be_forbidden :create, :update, :destroy

      describe "a GET to :index" do
        before { get :index }

        should_use_presenter Api::V2::TargetPresenter, status: :ok
      end

      describe "a GET to :show" do
        before { get :show, params: { id: target.id } }

        should_use_presenter Api::V2::TargetPresenter, status: :ok

        it "does not change" do
          assert_no_api_change "test/files/api_fixtures/v2-targets-show.yml"
          assert "token", target.token
        end
      end
    end
  end

  as_an_admin do
    describe "a POST to :create" do
      describe "simple" do
        before { post :create, params: valid_params }

        should_use_presenter Api::V2::TargetPresenter, status: :created

        it "sets a valid type" do
          assert_equal "EmailTarget", Target.last.type
        end
      end

      describe "validating params" do
        describe "empty body" do
          before { post :create }

          it "responds with bad request" do
            assert_response :bad_request
          end
        end

        describe "invalid param" do
          before { post :create, params: { invalid_param: nil } }

          it "responds with bad request" do
            assert_response :bad_request
          end
        end
      end

      describe "when creating an http_target with auth" do
        let(:params) do
          {
          target: {
              title: "Title",
              type: "http_target",
              target_url: "http://www.google.com",
              method: "GET"
            }
        }
        end

        describe "with no attribute param" do
          before { post :create, params: params }

          it "creates a UrlTargetV2" do
            assert_equal "UrlTargetV2", Target.last.type
          end
        end

        describe "with an attribute param" do
          let(:params_with_attribute) do
            params[:target][:attribute] = "attr"
            params
          end

          before { post :create, params: params_with_attribute }

          it "returns a bad request" do
            assert_response :bad_request
          end
        end
      end

      describe "when creating an http_target with auth" do
        let(:params) do
          {
          target: {
              title: "Title",
              type: "http_target",
              target_url: "http://www.google.com",
              method: "GET",
              username: "user@example.com",
              password: "password"
            }
        }
        end

        describe "with no attribute param" do
          before { post :create, params: params }

          it "creates a UrlTargetV2" do
            subject = Target.last
            assert_equal "UrlTargetV2", subject.type
            assert_equal "password", subject.password
            assert_equal "user@example.com", subject.username
          end
        end
      end

      it "is not able to assign an invalid type" do
        valid_params[:target][:type] = "User"

        refute_difference "Target.count(:all)" do
          post :create, params: valid_params
          assert_response :bad_request
        end
      end

      it "is not be able to create type that needs integration" do
        valid_params[:target][:type] = "salesforce_target"
        post :create, params: valid_params
      end
    end

    describe "a PUT to :update" do
      describe "simple" do
        before { put :update, params: valid_twitter.merge(id: target.id) }

        should_use_presenter Api::V2::TargetPresenter, status: :success

        it "changes the record" do
          assert_equal valid_params[:target][:title], target.reload.title
        end
      end

      describe "validating params" do
        describe "empty body" do
          before { put :update, params: { id: target.id } }

          it "responds with bad request" do
            assert_response :bad_request
          end
        end

        describe "invalid param" do
          before { put :update, params: { id: target.id, invalid_param: "invalid_param" } }

          it "responds with bad request" do
            assert_response :bad_request
          end
        end
      end

      describe "when the target has an apps requirement" do
        before do
          FactoryBot.create(:resource_collection_resource,
            account_id: @account.id,
            resource: target)
          put :update, params: valid_twitter.merge(id: target.id)
        end

        it "does not change the record" do
          assert_not_equal valid_params[:target][:title], target.reload.title
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end

      it "is not be able to update the type" do
        valid_params[:target][:type] = "email_target"
        put :update, params: valid_params.merge(id: target.id)
        assert_equal "TwitterTarget", target.reload.type
      end

      it "uses the initializer" do
        target = targets(:url_valid)
        put :update, params: { id: target.id, target: {target_url: "http://www.example.com"} }

        assert_response :success
        assert_equal "http://www.example.com", target.reload.url
      end
    end

    describe "a DELETE to :destroy" do
      describe "simple" do
        before { delete :destroy, params: { id: target.id } }

        it('responds with no_content') { assert_response :no_content }
        should_change("the number of targets", by: -1) { Target.count(:all) }
      end

      describe "when the target has an apps requirement" do
        before do
          FactoryBot.create(:resource_collection_resource,
            account_id: @account.id,
            resource: target)
          delete :destroy, params: { id: target.id }
        end

        should_not_change("the number of targets") { Target.count(:all) }
      end
    end
  end

  when_logged_in_as(:trial_admin) do
    describe_with_arturo_enabled :trial_account_target_limit do
      describe "a POST to :create by trial account is successful since limit has not been exceeded" do
        before { post :create, params: valid_params }

        should_use_presenter Api::V2::TargetPresenter, status: :created

        it "sets a valid type" do
          assert_equal "EmailTarget", Target.last.type
        end
      end

      describe "a POST to :create by trial account is failed since limit has been exceeded" do
        let (:error_response) { "{\"error\":\"ExceededTrialAccountTargetLimitError\",\"description\":\"Reached target limit: Trial accounts are limited to a maximum of two targets\"}" }

        before do
          [1, 2].each do |index|
            target = UrlTargetV2.new(
              title: "Url Target #{index}",
              account: trial,
              url: "http://www.example.com/#{index}",
              method: "get"
            )
            target.save!
          end
          post :create, params: valid_params
        end

        it "has expected error response" do
          assert_response :bad_request
          assert_equal error_response, @response.body
        end

        it "sets a valid type" do
          assert_equal "UrlTargetV2", Target.last.type
        end
      end
    end

    describe_with_arturo_disabled :trial_account_target_limit do
      describe "a POST to :create by trial account is successful since limit has not been exceeded" do
        before { post :create, params: valid_params }

        should_use_presenter Api::V2::TargetPresenter, status: :created

        it "sets a valid type" do
          assert_equal "EmailTarget", Target.last.type
        end
      end

      describe "a POST to :create by trial account is successful since arturo trial_account_target_limit is off" do
        before do
          [1, 2].each do |index|
            target = UrlTargetV2.new(
              title: "Url Target #{index}",
              account: trial,
              url: "http://www.example.com/#{index}",
              method: "get"
            )
            target.save!
          end
          post :create, params: valid_params
        end

        should_use_presenter Api::V2::TargetPresenter, status: :created

        it "sets a valid type" do
          assert_equal "EmailTarget", Target.last.type
        end
      end
    end
  end
end
