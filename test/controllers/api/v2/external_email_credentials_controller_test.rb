require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ExternalEmailCredentialsController do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :recipient_addresses, :brands, :role_settings, :account_settings

  before do
    @agent    = users(:minimum_agent)
    @account  = accounts(:minimum)

    accept :json
  end

  should_route :delete, '/api/v2/external_email_credentials/123', action: 'destroy', controller: 'api/v2/external_email_credentials', id: '123'

  as_an_anonymous_user do
    should_be_unauthorized :destroy
  end

  as_an_end_user do
    should_be_forbidden :destroy
  end

  as_an_agent do
    should_be_forbidden :destroy
  end

  as_an_admin do
    describe "a DELETE to :destroy" do
      let(:credential) do
        @account.external_email_credentials.new(
          username: "xxx@gmail.com",
          current_user: users(:minimum_agent)
        ).tap do |external_email_credential|
          external_email_credential.encrypted_value = "unencrypted_token"
          external_email_credential.save!
        end
      end
      before { credential }

      it "deletes and responds with 204" do
        assert_difference "ExternalEmailCredential.count(:all)", -1 do
          delete :destroy, params: { id: credential.id }
          assert_response :no_content
        end
      end

      it "does not delete default" do
        credential.recipient_address.update_attributes!(default: true)
        refute_difference "ExternalEmailCredential.count(:all)" do
          delete :destroy, params: { id: credential.id }
          assert_response :unprocessable_entity
          assert_includes @response.body, "The support address cannot be deleted because it is the default address"
        end
      end
    end
  end
end
