require_relative '../../../../support/test_helper'
require_relative '../../../../support/api_scopes_helper'
require_relative '../../../../support/ticket_forms_helper'

SingleCov.covered! uncovered: 6

describe Api::V2::Incremental::TicketsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  include TicketFormsHelper

  fixtures :all

  let(:account) { accounts(:minimum) }

  before do
    @request.account = account
  end

  as_a_subsystem_user(user: 'gooddata', account: :minimum) do
    describe 'with a recent start_time' do
      before do
        get :index, params: { format: :json, start_time: Time.now.to_i }
      end

      it('responds with success') { assert_response :success }
    end

    describe 'with all tickets' do
      before do
        get :index, params: { format: :json, start_time: 0 }
      end

      it 'does not contain personally identifiable information' do
        assert_not_empty json['tickets']
        assert_empty json['tickets'].select { |t| t.keys.include? 'recipient' }
      end
    end

    describe 'with a deleted ticket' do
      before do
        ticket = account.tickets.joins(:ticket_field_entries).first!
        @nice_id = ticket.nice_id
        ticket.will_be_saved_by(account.owner)
        ticket.soft_delete!
      end

      it 'returns deleted tickets' do
        get :index, params: { format: :json, start_time: 0 }

        assert_response :ok
        assert_not_empty json['tickets']

        deleted_ticket_json = json['tickets'].find { |j| j['id'] == @nice_id }
        refute_nil deleted_ticket_json['custom_fields'][0]['id']
      end

      describe 'with exclude_deleted true' do
        it 'does not return deleted tickets' do
          get :index, params: { format: :json, start_time: 0, exclude_deleted: true }

          assert_response :ok
          assert_not_empty json['tickets']
          refute_includes json['tickets'].map { |t| t['id'] }, @nice_id
        end
      end
    end

    describe 'with a recent start_time and an account that has contextual workspaces' do
      before do
        account.stubs(:has_contextual_workspaces?).returns(true)
        get :index, params: { format: :json, start_time: Time.now.to_i, include: 'ticket_forms' }
      end

      it('responds with success') { assert_response :success }
    end

    describe 'with archived tickets' do
      before do
        account.tickets.each { |t| archive_and_delete(t) }
      end

      it 'works' do
        get :index, params: { format: :json, start_time: 0 }

        assert_not_empty json['tickets']
      end
    end

    describe 'with only archived and deleted tickets' do
      before do
        archive_and_delete(tickets(:minimum_5))
        # Removes solved tickets, because it will transform into a closed
        # ticket after archival
        ticket_ids = account.tickets.unscoped.where(status_id: StatusType.SOLVED).pluck(:id)
        Ticket.unscoped.where(id: ticket_ids).delete_all

        # Removes existing closed tickets from archives
        archive_ids = TicketArchiveStub.where(account_id: account.id).where(status_id: StatusType.CLOSED).pluck(:id)
        TicketArchiveStub.unscoped.where(id: archive_ids).delete_all

        # Archives the rest
        account.tickets.unscoped.map do |t|
          t.will_be_saved_by(account.owner)
          t.soft_delete!
          archive_and_delete(t)
        end
      end

      it 'has correct custom fields' do
        get :index, params: { format: :json, start_time: 0 }
        assert_response :ok

        assert_not_empty json['tickets']
        json['tickets'].map do |ticket_json|
          assert_equal %w[id value], ticket_json['custom_fields'].flat_map(&:keys).uniq.sort
        end
      end

      it 'calls raw_ticket_field_entries' do
        Api::V2::Tickets::TicketPresenter.
          any_instance.
          expects(:raw_ticket_field_entries).
          at_least(1)

        get :index, params: { format: :json, start_time: 0 }
        assert_response :ok
      end
    end

    describe 'rate limiting' do
      before do
        11.times do |_i|
          get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
        end
      end

      before_should 'not limit system users' do
        Prop.expects(:throttle!).never
      end

      ['gooddata', 'bime', 'outbound'].each do |subsystem_user|
        as_a_subsystem_user(user: subsystem_user, account: :minimum) do
          describe 'with a recent start_time' do
            should_be_authorized do
              get :index, params: { format: :json, start_time: Time.now.to_i }
            end
          end
        end
      end
    end
  end

  as_a_system_user do
    it 'does not rate limit' do
      Prop.expects(:throttle!).never
      get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
    end
  end

  as_an_admin do
    describe 'without start_time' do
      before { get :index, format: :json }
      it('responds with bad_request') { assert_response :bad_request }
    end

    describe 'with a recent start_time' do
      before do
        get :index, params: { format: :json, start_time: Time.now.to_i }
      end

      it 'responds with success' do
        assert_response :success
      end
    end

    describe 'with no tickets' do
      before do
        @controller.stubs(:check_start_time_not_too_recent!)
        get :index, params: { format: :json, start_time: (Time.now + 20.years).to_i }
      end

      it 'gives back just the headers' do
        assert_empty json['tickets']
        assert_nil json['end_time']
      end
    end

    describe 'with all tickets' do
      before do
        get :index, params: { format: :json, start_time: 0 }
      end

      it 'does not remove personally identifiable information fields' do
        assert_not_empty json['tickets']
        assert_empty json['tickets'].reject { |t| t.keys.include? 'recipient' }
      end

      it 'does not have expanded_via_types by default' do
        @controller.send(:expanded_via_types?).must_equal false
      end

      describe 'a request from voyager' do
        before do
          @request.env['HTTP_USER_AGENT'] = "Voyager API Client"
          get :index, params: { format: :json, start_time: 0 }
        end

        it 'has expanded_via_types' do
          @controller.send(:expanded_via_types?).must_equal true
        end
      end
    end

    describe_with_arturo_enabled :inc_api_enforce_last_minute_limit do
      describe 'with recent ticket updates' do
        let(:recent_ticket) { account.tickets.first }

        before do
          account.tickets.where.not(id: recent_ticket.id).update_all(generated_timestamp: 2.minutes.ago)
          recent_ticket.touch
        end

        [:index, :cursor].each do |api|
          describe "in the #{api} api" do
            it 'excludes updates less than one minute old' do
              get api, params: { format: :json, start_time: 3.minutes.ago.to_i }
              refute_includes json['tickets'].map { |t| t['id'] }, recent_ticket.nice_id
            end

            it 'returns a 400 if the start time is less than 1 minute ago' do
              get api, params: { format: :json, start_time: 30.seconds.ago.to_i }
              assert_response 400
            end
          end
        end
      end
    end

    describe 'full timestamp instrumentation' do
      before do
        Timecop.freeze
        @export_limit_const = Api::V2::Exports::BaseController::EXPORT_LIMIT
      end

      it 'should warn if we find a timestamp that has more than EXPORT_LIMIT' do
        # find the generated_timestamp with the most tickets. Stub the EXPORT_LIMIT with that - 1 so we generate a warning
        max_timestamp = account.tickets.group_by(&:generated_timestamp).map { |t, d| [t, d.size] }.sort_by(&:last).last
        Api::V2::Exports::BaseController.const_set('EXPORT_LIMIT', max_timestamp[1] - 1)

        Rails.logger.expects(:warn).once

        get :index, params: { format: :json, start_time: 0 }
      end

      after do
        Api::V2::Exports::BaseController.const_set('EXPORT_LIMIT', @export_limit_const)
      end
    end

    describe 'with a per_page limit' do
      before do
        Ticket.with_deleted do
          accounts(:minimum).tickets.each_with_index { |t, i| t.update_column(:generated_timestamp, i.days.ago) }
        end
      end

      it 'only returns tickets up to the per_page limit' do
        get :index, params: { format: :json, start_time: 0, per_page: 'abcde' }
        assert_response 400
        get :index, params: { format: :json, start_time: 0, per_page: -3 }
        assert_response 400
        get :index, params: { format: :json, start_time: 0, per_page: 0 }
        assert_response 400
      end

      it 'only returns tickets up to the per_page limit' do
        get :index, params: { format: :json, start_time: 0, per_page: 4 }

        assert_response 200
        assert_equal 4, json['tickets'].count
      end

      describe 'with tickets bunched onto one timestamp' do
        before do
          Ticket.with_deleted do
            accounts(:minimum).tickets.update_all(generated_timestamp: 1.day.ago)
          end
        end

        it 'returns all of the data for the timestamp, ignoring per_page limit' do
          get :index, params: { format: :json, start_time: 2.days.ago.to_i, per_page: 4 }

          assert_response 200
          assert_equal 7, json['tickets'].count
          assert_equal 1, json['tickets'].map { |ticket| ticket['generated_timestamp'] }.uniq.size
        end
      end
    end

    describe 'rate limiting from BIME' do
      describe_with_arturo_disabled :load_limit_explore_exports do
        it 'does not rate limit requests from BIME' do
          @request.env['zendesk.bime.trusted_request'] = true
          Timecop.freeze
          11.times do
            get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
          end
          assert_response 200
        end
      end

      describe_with_arturo_enabled :load_limit_explore_exports do
        before do
          Zendesk::UnicornLoadRateLimiting.stubs(:unicorn_load_exceeds?).returns(exceeds)
        end

        describe 'if our unicorn load is exceeded' do
          let(:exceeds) { true }

          it 'rate limits requests from BIME' do
            @request.env['zendesk.bime.trusted_request'] = true
            get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
            assert_response 429
          end
        end

        describe 'if our unicorn load is NOT exceeded' do
          let(:exceeds) { false }

          it 'does not rate limit requests from BIME' do
            @request.env['zendesk.bime.trusted_request'] = true
            get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
            assert_response 200
          end
        end
      end
    end

    it 'does not rate limit requests from SEGMENT' do
      @request.env['zendesk.segment.trusted_request'] = true
      Timecop.freeze
      11.times do
        get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
      end
      assert_response 200
    end

    it 'rate limits if polled multiple times in the same window' do
      Timecop.freeze
      11.times do
        get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
      end
      assert_response 429
      assert @response.headers.include?('Retry-After')
    end

    it 'has a limit of 1000' do
      assert_equal 1000, @controller.send(:export_limit)
    end

    describe_with_arturo_enabled :reduce_incremental_ticket_results do
      it 'has a limit of 500' do
        assert_equal 500, @controller.send(:export_limit)
      end
    end

    describe 'archived tickets' do
      it 'uses the archive finder' do
        finder = mock
        Zendesk::Export::IncrementalExportArchiveFinder.stubs(:new).returns(finder)
        Zendesk::Export::IncrementalExport.
          expects(:new).
          with(has_entry(custom_finder: finder)).
          returns(mock(as_json: { tickets: [] }))

        get :index, params: { format: :json, start_time: 0 }
      end
    end

    describe 'tickets with deleted_ticket_forms' do
      let(:non_active_form) do
        params = {
          ticket_form: { name: 'non default form', display_name: 'non default form', position: 2, default: false, end_user_visible: false }
        } # active & deleted_at can't be set here
        Zendesk::TicketForms::Initializer.new(account, params).ticket_form
      end

      let(:ticket1) { FactoryBot.create(:ticket, account: account, ticket_form_id: non_active_form.id) }
      let(:ticket2) { FactoryBot.create(:ticket, account: account, ticket_form_id: non_active_form.id) }
      let(:ticket3) { FactoryBot.create(:ticket, account: account, ticket_form_id: non_active_form.id) }

      before do
        non_active_form.save!
        ticket1
        ticket2
        ticket3
        Account.any_instance.stubs(:has_ticket_forms?).returns(true)
        non_active_form.soft_delete!
      end

      it 'does not query for the deleted ticket forms many times' do
        assert_sql_queries(4, /FROM `ticket_forms`/) do
          get :index, params: { format: :json, start_time: 1.minutes.ago.to_i, include: 'brands,groups,organizations,sharing_agreements,ticket_forms,users' }
        end
      end
    end

    describe_with_arturo_setting_enabled :customer_satisfaction do
      describe 'with archived tickets and ticket_forms' do
        before do
          setup_ticket_forms

          account.stubs(:has_ticket_forms?).returns(true)

          # Makes creating a comment easier
          Comment.any_instance.stubs(:should_add_signature?).returns(false)

          account.tickets.order('created_at asc').map.with_index do |t, i|
            # Sets current_user= on the ticket
            t.will_be_saved_by(account.owner)
            add_satisfaction_rating(t)

            if i == 0
              t.update_column(:ticket_form_id, @non_active_form.id)
              @ticket0 = t
            elsif i == 1
              t.update_column(:ticket_form_id, @default_form.id)
              @ticket1 = t
            end

            # Need that otherwise the events won't be archived with the ticket
            t.reload
            archive_and_delete(t)
          end
        end

        it 'does not do n+1 queries for Riak or ticket_archive_stubs after fetching the satisfaction_ratings' do
          assert_no_n_plus_one(only_tables: %w[ticket_archive_stubs ticket_forms]) do
            get :index, params: { format: :json, start_time: 0, include: 'metric_sets,satisfaction_ratings' }
          end
          assert_response :ok
        end

        it 'does not do n+1 queries on ticket_forms with a deleted ticket form' do
          assert_no_n_plus_one(only_tables: %w[ticket_forms]) do
            start_time = @ticket0.created_at.to_i
            get :index, params: { format: :json, start_time: start_time, include: 'metric_sets,satisfaction_ratings' }
          end
          assert_response :ok
        end
      end
    end

    describe 'sample end point' do
      before do
        get :sample, params: { format: :json, start_time: 1.minutes.ago.to_i }
      end

      it('responds with success') { assert_response :success }

      describe 'rate limiting' do
        before do
          get :sample, params: { format: :json, start_time: 1.minutes.ago.to_i }
        end

        it 'is successful when polled twice' do
          assert_response :success
        end
      end
    end

    describe 'cursor end point' do
      describe 'when neither `start_time` nor a valid cursor is provided' do
        before { get :cursor, format: :json }

        it 'fails' do
          assert_response :bad_request
        end
      end

      describe 'when an invalid cursor is provided' do
        before { get :cursor, params: { format: :json, cursor: 1 } }

        it 'fails' do
          assert_response :bad_request
        end
      end

      describe 'when a valid cursor is provided' do
        before { get :cursor, params: { format: :json, cursor: Zendesk::CursorPagination::Cursor.new(min_timestamp: 0).base64 } }

        it 'succeeds' do
          assert_response :ok
        end
      end

      describe 'when a start_time is provided' do
        before { get :cursor, params: { format: :json, start_time: 0 } }

        it 'succeeds' do
          assert_response :ok
        end
      end

      describe 'navigation' do
        before do
          get :cursor, params: { format: :json, start_time: 0 }
        end

        it 'prevents going backward' do
          assert_nil json['before_url']
          assert_nil json['before_cursor']
        end

        it 'excludes start_time in after_url' do
          refute_includes json['after_url'], 'start_time='
        end
      end

      describe 'entire export' do
        before do
          archive_and_delete(tickets(:minimum_5))

          get :cursor, params: { format: :json, start_time: 0 }
        end

        it 'includes archived and deleted tickets' do
          all_tickets = Ticket.unscoped.where(account_id: account.id).where('generated_timestamp > ?', Time.at(0))
          tickets = json['tickets']
          assert_equal all_tickets.count, tickets.count
        end
      end

      describe 'subset of export' do
        let(:start_time) { 1.hour.ago.to_i }
        let(:export_limit) { 2 }
        let!(:old_ticket) { FactoryBot.create(:ticket, account: account, generated_timestamp: Time.at(start_time - 1)) }
        let!(:old_archived_ticket) do
          ticket = FactoryBot.create(:ticket, account: account, generated_timestamp: Time.at(start_time - 1))
          ticket.archive!
          ticket.reload
        end

        before do
          @controller.stubs(export_limit: export_limit)

          get :cursor, params: { format: :json, start_time: start_time }
        end

        it 'responds only with tickets created after the specified start_time' do
          assert(
            json['tickets'].none? { |ticket| ticket['generated_timestamp'] < start_time },
            'response contains tickets older than the specified start_time'
          )
        end

        let(:first_set) { json['tickets'] }

        it 'stops at the export limit' do
          assert_equal export_limit, first_set.count
        end

        describe 'when a second request is made' do
          let(:second_set) { json['tickets'] }

          before do
            first_set

            get :cursor, params: { format: :json, start_time: start_time, cursor: json['after_cursor'] }

            second_set
          end

          it 'returns monotonically increasing results' do
            assert second_set.first['generated_timestamp'] >= first_set.last['generated_timestamp']
          end

          it 'returns strictly increasing ids' do
            assert second_set.first['id'] > first_set.last['id']
          end
        end
      end

      describe 'with a per_page limit' do
        let(:start_time) { 1.hour.ago.to_i }
        let(:limit) { 2 }

        let!(:old_ticket) { FactoryBot.create(:ticket, account: account, generated_timestamp: Time.at(start_time - 1)) }
        let!(:old_archived_ticket) do
          ticket = FactoryBot.create(:ticket, account: account, generated_timestamp: Time.at(start_time - 1))
          ticket.archive!
          ticket.reload
        end

        let(:first_set) { json['tickets'] }

        it 'only returns tickets up to the per_page limit' do
          get :cursor, params: { format: :json, start_time: start_time, per_page: 'abcde' }
          assert_response 400
          get :cursor, params: { format: :json, start_time: start_time, per_page: -3 }
          assert_response 400
          get :cursor, params: { format: :json, start_time: start_time, per_page: 0 }
          assert_response 400
        end

        it 'stops at the given per_page limit' do
          get :cursor, params: { format: :json, start_time: start_time, per_page: limit }

          assert_equal limit, json['tickets'].count
        end

        describe 'when a second request is made' do
          it 'returns monotonically increasing results' do
            get :cursor, params: { format: :json, start_time: start_time, per_page: limit }
            first_set = json['tickets']

            get :cursor, params: { format: :json, start_time: start_time, cursor: json['after_cursor'] }
            second_set = json['tickets']

            assert second_set.first['generated_timestamp'] >= first_set.last['generated_timestamp']
          end

          it 'returns strictly increasing ids' do
            get :cursor, params: { format: :json, start_time: start_time, per_page: limit }
            first_set = json['tickets']

            get :cursor, params: { format: :json, start_time: start_time, cursor: json['after_cursor'] }
            second_set = json['tickets']

            assert second_set.first['id'] > first_set.last['id']
          end
        end
      end
    end
  end

  def add_satisfaction_rating(ticket)
    audit = events(:create_audit_for_minimum_ticket_2).dup
    audit.id = nil
    audit.ticket = ticket
    audit.save!

    event = events(:create_comment_for_minimum_ticket_2).dup
    event.audit = audit
    event.ticket = ticket
    # event.ticket_id = Integer(ticket.id)
    event.id = nil
    event.save!

    account.satisfaction_ratings.create!(
      agent: @agent,
      group: @group,
      ticket: ticket,
      enduser: @user,
      event: event,
      score: SatisfactionType.OFFERED,
      reason_code: Satisfaction::Reason::NONE,
      status_id: StatusType.CLOSED
    ) do |r|
      r.created_at = 1.day.ago
      r.ticket.update_column(:updated_at, r.created_at)
      r.ticket.update_column(:satisfaction_score, r.score)
    end
  end

  as_a_subsystem_user(user: 'bime', account: :minimum) do
    describe_with_arturo_enabled :reduce_incremental_ticket_api_payload do
      describe 'with all tickets' do
        before do
          get :index, params: { format: :json, start_time: 0 }
        end

        it 'is marked as reduced size' do
          assert @controller.send(:reduced_payload_size?)
        end

        it 'returns a successful response' do
          assert_response :success
        end

        it 'does not return the description' do
          assert_nil json['tickets'][0]['description']
        end

        it 'does not return fields' do
          assert_nil json['tickets'][0]['fields']
        end
      end
    end
  end
end
