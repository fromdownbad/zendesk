require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered! uncovered: 0

describe Api::V2::Incremental::RoutingController do
  extend ApiScopesHelper
  fixtures :all

  let(:attribute_fixtures) do
    YAML.load_file("#{Rails.root}/test/files/deco/attributes.yml")
  end

  let(:attribute_value_fixtures) do
    YAML.load_file("#{Rails.root}/test/files/deco/attribute_values.yml")
  end

  let(:instance_value_fixtures) do
    YAML.load_file("#{Rails.root}/test/files/deco/instance_values.yml")
  end

  let(:first_instance_value) do
    instance_value_fixtures['instance_values'].first
  end

  let(:last_instance_value) do
    instance_value_fixtures['instance_values'].last
  end

  let(:ticket_id) do
    Ticket.where(account_id: account.id).first.id.to_s
  end

  let(:account) { accounts(:minimum) }

  let(:ticket) do
    Ticket.where(account_id: account.id).first
  end

  let(:ticket_nice_id) do
    ticket.nice_id.to_s
  end

  let!(:client) { Zendesk::Deco::Client.new(account) }

  before do
    @timestamp = Timecop.freeze(Date.today).to_i
    @future_timestamp = (Time.now + 20.years).to_i
  end

  describe_with_arturo_disabled :incremental_skill_based_routing_api do
    describe 'a GET to attributes' do
      before { get :attributes, format: :json }

      it 'responds with `404 Not Found`' do
        assert_response :not_found
      end
    end

    describe 'a GET to attribute_values' do
      before { get :attribute_values, format: :json }

      it 'responds with `404 Not Found`' do
        assert_response :not_found
      end
    end

    describe 'a GET to instance_values' do
      before { get :instance_values, format: :json }

      it 'responds with `404 Not Found`' do
        assert_response :not_found
      end
    end
  end

  describe_with_arturo_enabled :incremental_skill_based_routing_api do
    before do
      @request.account = account
      Zendesk::Deco::Client.stubs(:new).with(account).returns(client)

      client.
        stubs(:get).
        with('attributes/incremental?start_time=0').
        returns(stub('response', body: attribute_fixtures))

      client.
        stubs(:get).
        with("attributes/incremental?start_time=#{@timestamp}").
        returns(stub('response', body: attribute_fixtures))

      client.
        stubs(:get).
        with("attributes/incremental?start_time=#{@future_timestamp}").
        returns(stub('response', body: { 'attributes' => [] }))

      client.
        stubs(:get).
        with('attribute_values/incremental?start_time=0').
        returns(stub('response', body: attribute_value_fixtures))

      client.
        stubs(:get).
        with("attribute_values/incremental?start_time=#{@timestamp}").
        returns(stub('response', body: attribute_value_fixtures))

      client.
        stubs(:get).
        with("attribute_values/incremental?start_time=#{@future_timestamp}").
        returns(stub('response', body: { 'attribute_values' => [] }))

      instance_value_fixtures["instance_values"].first["instance_id"] = ticket_id

      client.
        stubs(:get).
        with("instance_values/incremental?cursor=#{first_instance_value['id']}", nil, 5).
        returns(stub('response', body: {'instance_values' => [instance_value_fixtures['instance_values'].slice(1)] }))

      client.
        stubs(:get).
        with("instance_values/incremental?cursor=#{last_instance_value['id']}", nil, 5).
        returns(stub('response', body: {'instance_values' => [] }))

      client.
        stubs(:get).
        with('instance_values/incremental?start_time=1', nil, 5).
        returns(stub('response', body: instance_value_fixtures))

      client.
        stubs(:get).
        with("instance_values/incremental?start_time=#{@timestamp}", nil, 5).
        returns(stub('response', body: {'instance_values' => []}))

      client.
        stubs(:get).
        with("instance_values/incremental?start_time=#{@future_timestamp}", nil, 5).
        returns(stub('response', body: {'instance_values' => []}))
    end

    as_an_admin do
      describe 'with start_time and cursor' do
        let(:cursor) { 'my-cursor' }

        before do
          client.
            stubs(:get).
            with("attributes/incremental?cursor=#{cursor}").
            returns(stub('response', body: attribute_fixtures))

          get :attributes, params: { start_time: @timestamp, cursor: cursor, format: :json }
        end

        it ('makes a request to deco with the cursor') do
          assert_response :ok
        end
      end

      describe 'without start_time' do
        before { get :attributes, format: :json }

        it ('responds with `ok`') do
          assert_response :ok
        end
      end

      describe 'rate limiting' do
        before do
          Timecop.freeze
          Prop.reset(:incremental_exports, @account.id)
          10.times do
            get :attributes, params: { start_time: @timestamp, format: :json }
            assert_response :ok
          end
        end

        it 'does not rate limit requests from Segment' do
          @request.env["zendesk.segment.trusted_request"] = true
          Timecop.freeze
          11.times do
            get :attributes, params: { start_time: @timestamp, format: :json }
          end
          assert_response :ok
        end

        it 'rate limits if polled multiple times in the same window' do
          get :attributes, params: { start_time: @timestamp, format: :json }
          assert_response 429
          assert @response.headers.include?("Retry-After")
        end
      end

      ['attributes', 'attribute_values'].each do |type|
        describe "with no `#{type}`" do
          before do
            get type.to_sym, params: { format: :json, start_time: @future_timestamp }
          end

          it 'returns an empty set' do
            result = JSON.parse(@response.body)
            assert_empty result[type]
          end
        end
      end

      describe 'instance_values' do
        describe 'with no instance_values' do
          before do
            get :instance_values, params: { format: :json, start_time: @future_timestamp }
          end

          it 'returns an empty set' do
            result = JSON.parse(@response.body)

            assert_nil result['next_page']
            assert_empty result['instance_values']
          end
        end
      end
    end

    describe '#attributes' do
      def assert_attributes_response
        get :attributes, params: { start_time: 0, format: :json }

        result = JSON.parse(@response.body)
        next_cursor = result['attributes'].last['id']
        end_time = attribute_fixtures['attributes'].last['created_at'].to_datetime.to_i
        assert_equal(
          "https://minimum.zendesk-test.com/api/v2/incremental/routing/attributes.json?cursor=#{next_cursor}",
          result['next_page']
        )
        assert_equal(end_time, result['end_time'])
        assert_equal(
          [{"id" => "1",
            "name" => "Language",
            "type" => "create",
            "time" => "2017-08-25T10:43:48Z"},
           {"id" => "1",
            "name" => "Language",
            "type" => "update",
            "time" => "2017-08-28T14:51:29Z"},
           {"id" => "6",
            "name" => "Product",
            "type" => "create",
            "time" => "2017-08-26T12:17:13Z"},
           {"id" => "11",
            "name" => "Tier",
            "type" => "create",
            "time" => "2017-08-28T16:35:21Z"},
           {"id" => "11",
            "name" => "Tier",
            "type" => "update",
            "time" => "2017-08-28T18:54:05Z"},
           {"id" => "12",
            "name" => "Region",
            "type" => "create",
            "time" => "2017-08-28T16:35:21Z"},
           {"id" => "12",
            "name" => "Region",
            "type" => "update",
            "time" => "2017-09-28T18:54:05Z"},
           {"id" => "12",
            "name" => "Region",
            "type" => "delete",
            "time" => "2017-10-28T18:54:05Z"}],
          result['attributes']
        )
      end

      as_a_subsystem_user(user: 'gooddata', account: :minimum) do
        it ('returns the expected export') { assert_attributes_response }
      end

      as_an_admin do
        it ('returns the expected export') { assert_attributes_response }
      end
    end

    describe '#attribute_values' do
      def assert_attribute_values_response
        get :attribute_values, params: { start_time: 0, format: :json }

        result = JSON.parse(@response.body)
        next_cursor = result['attribute_values'].last['id']
        end_time = attribute_value_fixtures['attribute_values'].last['created_at'].to_datetime.to_i
        assert_equal(
          "https://minimum.zendesk-test.com/api/v2/incremental/routing/attribute_values.json?cursor=#{next_cursor}",
          result['next_page']
        )
        assert_equal(end_time, result['end_time'])
        assert_equal(
          [{
            "id" => "1",
            "attribute_id" => "1",
            "name" => "Japanese",
            "type" => "create",
            "time" => "2017-08-25T10:43:48Z"
          }, {
            "id" => "1",
            "attribute_id" => "1",
            "name" => "Japanese",
            "type" => "update",
            "time" => "2017-09-28T14:51:29Z"
          }, {
            "id" => "1",
            "attribute_id" => "1",
            "name" => "Japanese",
            "type" => "delete",
            "time" => "2017-10-28T14:51:29Z"
          }, {
            "id" => "6",
            "attribute_id" => "1",
            "name" => "Spanish",
            "type" => "create",
            "time" => "2017-08-26T12:17:13Z"
          }, {
            "id" => "11",
            "attribute_id" => "1",
            "name" => "English",
            "type" => "create",
            "time" => "2017-08-28T16:35:21Z"
          }, {
            "id" => "11",
            "attribute_id" => "1",
            "name" => "English",
            "type" => "update",
            "time" => "2017-09-28T18:54:05Z"
          }, {
            "id" => "11",
            "attribute_id" => "1",
            "name" => "English",
            "type" => "delete",
            "time" => "2017-10-28T14:51:29Z"
          }],
          result['attribute_values']
        )
      end

      as_a_subsystem_user(user: 'gooddata', account: :minimum) do
        it ('returns the expected export') { assert_attribute_values_response }
      end

      as_an_admin do
        it ('returns the expected export') { assert_attribute_values_response }
      end
    end

    describe '#instance_values' do
      describe 'when querying with `start_time`' do
        def assert_instance_values_response
          get :instance_values, params: { start_time: 1, format: :json }

          result = JSON.parse(@response.body)
          next_cursor = result['instance_values'].last['id']
          end_time = instance_value_fixtures['instance_values'].last['created_at'].to_datetime.to_i
          assert_equal(
            "https://minimum.zendesk-test.com/api/v2/incremental/routing/instance_values.json?cursor=#{next_cursor}",
            result['next_page']
          )
          assert_equal(end_time, result['end_time'])
          assert_equal(
            [{
              "id" => "4",
              "attribute_value_id" => "6",
              "instance_id" => "61",
              "type" => "associate_ticket",
              "time" => "2017-09-29T18:58:00Z"
            }, {
              "id" => "5",
              "attribute_value_id" => "6",
              "instance_id" => "123456",
              "type" => "associate_agent",
              "time" => "2017-08-29T18:58:00Z"
            }, {
              "id" => "5",
              "attribute_value_id" => "6",
              "instance_id" => "123456",
              "type" => "unassociate_agent",
              "time" => "2017-10-30T18:58:00Z"
            }],
            result['instance_values']
          )
        end
        as_a_subsystem_user(user: 'gooddata', account: :minimum) do
          it ('returns the expected export') { assert_instance_values_response }
        end

        as_an_admin do
          it ('returns the expected export') { assert_instance_values_response }
        end

        describe 'when `instance_id` refers to a deleted ticket ' do
          before do
            delete_and_reload_ticket(ticket)
          end

          as_a_subsystem_user(user: 'gooddata', account: :minimum) do
            it ('returns the expected export') { assert_instance_values_response }
          end

          as_an_admin do
            it ('returns the expected export') { assert_instance_values_response }
          end
        end
      end

      describe 'when querying with `cursor`' do
        as_a_subsystem_user(user: 'gooddata', account: :minimum) do
          it ('returns the expected export') do
            get :instance_values, params: { cursor: first_instance_value['id'], format: :json }
            result = JSON.parse(@response.body)
            next_cursor = instance_value_fixtures['instance_values'].last['id']
            end_time = instance_value_fixtures['instance_values'].last['created_at'].to_datetime.to_i

            assert_equal(
              "https://minimum.zendesk-test.com/api/v2/incremental/routing/instance_values.json?cursor=#{next_cursor}",
              result['next_page']
            )
            assert_equal(end_time, result['end_time'])
            assert_equal(
              [
                {"id" => "5",
                 "attribute_value_id" => "6",
                 "instance_id" => "123456",
                 "type" => "associate_agent",
                 "time" => "2017-08-29T18:58:00Z"},
                {"id" => "5",
                 "attribute_value_id" => "6",
                 "instance_id" => "123456",
                 "type" => "unassociate_agent",
                 "time" => "2017-10-30T18:58:00Z"}
              ],
              result['instance_values']
            )
          end
        end

        as_an_admin do
          it ('returns the expected export') do
            get :instance_values, params: { cursor: last_instance_value['id'], format: :json }
            result = JSON.parse(@response.body)

            assert_nil result['next_page']
            refute result['end_time']
            assert_equal(
              [],
              result['instance_values']
            )
          end
        end
      end
    end
  end
end
