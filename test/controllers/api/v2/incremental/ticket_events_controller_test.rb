require_relative '../../../../support/test_helper'
require_relative '../../../../support/api_scopes_helper'

SingleCov.covered! uncovered: 2

describe Api::V2::Incremental::TicketEventsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper

  fixtures :all

  let(:account) { accounts(:minimum) }

  before do
    @request.account = account
  end

  as_an_admin do
    describe 'when changing a ticket subject' do
      before do
        # This ticket needs to be recent/not closed
        @ticket = tickets(:minimum_2)
        @first_event = @ticket.events.first
        @ticket.will_be_saved_by @user
        @ticket.subject = 'foo'
        @ticket.save!
      end

      it 'returns a subject change event' do
        get :index, params: { start_time: @first_event.created_at.to_i - 1, format: :json }
        assert_response :ok

        child_events = json['ticket_events'].
          select { |e| e['ticket_id'] == @ticket.nice_id }.
          flat_map { |e| e['child_events'] }

        assert_equal 1, child_events.select { |e| e.try(:[], 'subject') == 'foo' }.size
      end
    end

    describe 'when including comments' do
      it 'does not fail' do
        assert_no_n_plus_one do
          get :index, params: { start_time: 1.minutes.ago.to_i, format: :json, include: 'comment_events' }
        end

        assert_response :ok
      end

      it 'returns something' do
        ticket = Comment.where(account_id: account.id).last.ticket
        first_event = ticket.events.first

        assert_no_n_plus_one do
          get :index, params: { start_time: first_event.created_at.to_i - 1, format: :json, include: 'comment_events' }
        end
        refute_empty JSON.parse(response.body.presence || {}).fetch('ticket_events')
        assert_response :ok

        json_ticket_child_event_keys = JSON.parse(response.body.presence)['ticket_events'].
          select { |e| e['ticket_id'] == ticket.nice_id }.
          flat_map { |e| e['child_events'].flat_map(&:keys) }

        assert json_ticket_child_event_keys.include?('body')
        assert json_ticket_child_event_keys.include?('html_body')
      end

      # Addresses https://support.zendesk.com/agent/tickets/2275707
      it 'returns something for archived tickets' do
        ticket = Comment.where(account_id: account.id).last.ticket
        first_event = ticket.events.first
        start_time = first_event.created_at.to_i - 1

        archive_and_delete(ticket)

        assert_no_n_plus_one(ignore_duplicates: true) do # TODO: can remove 'ignore_duplicates: true' once TP-179 is deployed
          get :index, params: { start_time: start_time, format: :json, include: 'comment_events' }
        end
        assert_response :ok

        json_ticket_child_event_keys = JSON.parse(response.body.presence)['ticket_events'].
          select { |e| e['ticket_id'] == ticket.nice_id }.
          flat_map { |e| e['child_events'].flat_map(&:keys) }

        assert json_ticket_child_event_keys.include?('body')
        assert json_ticket_child_event_keys.include?('html_body')
      end

      describe 'with a deleted ticket' do
        let(:ticket) { Comment.where(account_id: account.id).last.ticket }
        let(:first_event) do
          first_event = ticket.events.first
          first_event.update_column :via_id, ViaType.FACEBOOK_POST
          first_event
        end
        let(:start_time) { first_event.created_at.to_i - 1 }

        # This block was added to address this ticket:
        # https://support.zendesk.com/agent/tickets/2190423
        # It happens when the event is related to a deleted ticket, and updated
        # via something specific that display the ticket's JSON in the response.
        describe 'via something that requires a deleted ticket for presenting' do
          it 'returns something' do
            assert_no_n_plus_one do
              get :index, params: { start_time: start_time, format: :json, include: 'comment_events' }
            end
            refute_empty JSON.parse(response.body.presence || {}).fetch('ticket_events')
            assert_response :ok
          end
        end

        describe 'via something that requires an archived and deleted ticket for presenting' do
          before do
            ticket.will_be_saved_by(@user, via_id: ViaType.FACEBOOK_POST)
            ticket.soft_delete!
            archive_and_delete(ticket)
          end

          it 'returns something' do
            assert_no_n_plus_one(ignore_duplicates: true) do # TODO: can remove 'ignore_duplicates: true' once TP-179 is deployed
              get :index, params: { start_time: start_time, format: :json, include: 'comment_events' }
            end
            refute_empty JSON.parse(response.body.presence || {}).fetch('ticket_events')
            assert_response :ok
          end
        end
      end

      describe 'n+1 queries caused by presenting `via` properties' do
        let(:subject) { 'Api::V2::Incremental::TicketEventsController integration test for n+1 queries' }

        def validate_ticket_event_via_name(ticket_event, via_id)
          via_name = ViaType[via_id].name

          ticket_event['via'] == via_name &&
            ticket_event['child_events'].any? do |ch|
              ch['subject'] == subject
            end
        end

        # When patches to this problem are applied to other via types, add more data here.
        it 'do not happen when all comments have via type of MERGE or MAIL only' do
          via_ids = [ViaType.MERGE, ViaType.MAIL]

          via_ids.each do |via_id|
            FactoryBot.create_list(
              :ticket,
              10,
              account: account,
              via_id: via_id,
              subject: subject
            )
          end

          assert_n_plus_ones 1..3 do
            get :index, params: { start_time: 1, format: :json, include: 'comment_events' }
          end

          ticket_events = JSON.parse(response.body.presence).fetch('ticket_events')

          via_ids.each do |via_id|
            assert(ticket_events.any? do |ticket_event|
              validate_ticket_event_via_name(ticket_event, via_id)
            end)
          end
        end
      end
    end

    describe 'with exclude_deleted true' do
      it 'does not include events for that ticket' do
        archive_and_delete(tickets(:minimum_2))

        get :index, params: { start_time: 0, exclude_deleted: false, format: :json }
        json['ticket_events'].map { |e| e['ticket_id'] }.must_include(tickets(:minimum_2).nice_id)

        get :index, params: { start_time: 0, exclude_deleted: true, format: :json }
        json['ticket_events'].map { |e| e['ticket_id'] }.wont_include(tickets(:minimum_2).nice_id)
      end
    end

    describe 'with a per_page limit passed in the call' do
      before do
        events(:create_audit_for_minimum_ticket_1).update_column(:created_at, 3.minutes.ago)
      end

      it 'only permits positive integers' do
        get :index, params: { start_time: 5.minute.ago, format: :json, per_page: 0 }
        assert_response 400
        get :index, params: { start_time: 5.minute.ago, format: :json, per_page: -10 }
        assert_response 400
        get :index, params: { start_time: 5.minute.ago, format: :json, per_page: '5' }
        assert_response 400
      end

      it 'limits results based on the per_page limit' do
        get :index, params: { start_time: 5.minute.ago.to_i, format: :json, per_page: 1 }

        assert_equal 1, json['ticket_events'].count
      end

      describe 'when the data to fetched is clumped into one timestamp' do
        before do
          Event.connection.execute('update events e set created_at = NOW() - INTERVAL 5 DAY')
        end

        it 'ignores the per_page limit and fetches all data for the timestamp' do
          get :index, params: { start_time: 6.days.ago.to_i, format: :json, per_page: 2 }

          assert_equal 5, json['ticket_events'].count
          assert_equal 1, json['ticket_events'].map { |event| event['created_at'] }.uniq.size
        end
      end
    end
  end

  describe 'rate limiting' do
    before { Timecop.freeze }

    as_an_admin do
      describe '#index' do
        it 'does not rate limit requests from BIME' do
          @request.env['zendesk.bime.trusted_request'] = true
          Prop.expects(:throttle!).never
          get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
          assert_response 200
        end

        it 'does not rate limit requests from SEGMENT' do
          @request.env['zendesk.segment.trusted_request'] = true
          Prop.expects(:throttle!).never
          get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
          assert_response 200
        end

        it 'rates limits' do
          Prop.reset(:incremental_exports, account.id)
          11.times do |_i|
            get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
          end
          assert_response 429
          assert @response.headers.include?('Retry-After')
        end
      end

      describe '#sample' do
        it 'is successful when polled twice' do
          get :sample, params: { start_time: 1.minutes.ago.to_i, format: :json }
          assert_response :success

          get :sample, params: { start_time: 1.minutes.ago.to_i, format: :json }
          assert_response :success
        end
      end
    end

    ['gooddata', 'bime', 'outbound'].each do |subsystem_user|
      as_a_subsystem_user(user: subsystem_user, account: :minimum) do
        describe 'with a recent start_time' do
          should_be_authorized do
            get :index, params: { format: :json, start_time: Time.now.to_i }
          end
        end
      end
    end

    as_a_subsystem_user(user: 'gooddata', account: :minimum) do
      it 'does not rate limit' do
        Prop.expects(:throttle!).never
        get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
      end
    end

    as_a_system_user do
      it 'does not rate limit' do
        Prop.expects(:throttle!).never
        get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
      end
    end
  end

  describe '#index' do
    as_an_admin do
      before do
        account.events.where(type: 'Audit').each_with_index do |audit, i|
          audit.created_at = audit.created_at + i.seconds
          audit.save
        end
      end

      it 'enforces a maximum response size limit' do
        get :index, params: { start_time: 0, format: :json }
        assert_response :success

        num_events = JSON.parse(response.body)['ticket_events'].size
        assert_equal(5, num_events)

        stub_const(Api::V2::Exports::BaseController, :EXPORT_LIMIT, 4) do
          get :index, params: { start_time: 0, format: :json }
          assert_response :success

          num_events = JSON.parse(response.body)['ticket_events'].size
          assert_equal(4, num_events)
        end
      end

      describe_with_arturo_enabled :reduce_incremental_event_results do
        it 'enforces a reduced maximum response size limit' do
          get :index, params: { start_time: 0, format: :json }
          assert_response :success

          num_events = JSON.parse(response.body)['ticket_events'].size
          assert_equal(5, num_events)

          stub_const(Api::V2::Exports::BaseController, :REDUCED_EXPORT_LIMIT, 2) do
            get :index, params: { start_time: 0, format: :json }
            assert_response :success

            num_events = JSON.parse(response.body)['ticket_events'].size
            assert_equal(2, num_events)
          end
        end
      end
    end
  end

  describe '#sample' do
    as_an_admin do
      before do
        account.events.where(type: 'Audit').each_with_index do |audit, i|
          audit.created_at = audit.created_at + i.seconds
          audit.save
        end
      end

      it 'enforces a maximum response size limit' do
        get :sample, params: { start_time: 0, format: :json }
        assert_response :success

        num_events = JSON.parse(response.body)['ticket_events'].size
        assert_equal(5, num_events)

        stub_const(Api::V2::Exports::BaseController, :SAMPLE_EXPORT_LIMIT, 3) do
          get :sample, params: { start_time: 0, format: :json }
          assert_response :success

          num_events = JSON.parse(response.body)['ticket_events'].size
          assert_equal(3, num_events)
        end
      end
    end
  end
end
