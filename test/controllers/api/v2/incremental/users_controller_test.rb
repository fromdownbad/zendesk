require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::Incremental::UsersController do
  extend ApiScopesHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @request.account = @account
  end

  ["gooddata", "bime", "outbound"].each do |subsystem_user|
    as_a_subsystem_user(user: subsystem_user, account: :minimum) do
      describe "with a recent start_time" do
        should_be_authorized do
          get :index, params: { format: :json, start_time: Time.now.to_i }
        end
      end
    end
  end

  as_a_subsystem_user(user: 'gooddata', account: :minimum) do
    describe 'with all users' do
      before do
        get :index, params: { start_time: 0 }
      end

      it 'does not contain personally identifiable information fields' do
        result = JSON.parse(@response.body)

        assert_not_empty result['users']
        assert_empty result['users'].select { |u| u.key?('email') }
        assert_empty result['users'].select { |u| u.key?('phone') }
      end
    end

    describe 'when the account has api name removal' do
      before do
        @account.settings.create!(name: "insights_user_name_removal", value: 1)
        get :index, params: { start_time: 0 }
      end

      it 'sets the end user name to null' do
        result = JSON.parse(@response.body)

        end_user_ids = @account.end_users.map(&:id)
        agent_ids = @account.agents.map(&:id)
        agent_names = @account.agents.map(&:name)

        end_user_results = result['users'].select { |user| end_user_ids.include?(user['id']) }
        agent_results = result['users'].select { |user| agent_ids.include?(user['id']) }

        assert_not_empty end_user_results
        assert_empty end_user_results.map { |u| u['name'] }.compact
        assert_equal agent_names.sort, agent_results.map { |u| u['name'] }.sort
      end
    end

    describe "when the account doesn't have api name removal" do
      before do
        @account.settings.create!(name: "insights_user_name_removal", value: 0)
        get :index, params: { start_time: 0 }
      end

      it 'leaves the user names intact' do
        result = JSON.parse(@response.body)
        account_user_names = @account.all_users.map { |u| u['name'] }.sort
        result_users = result['users'].map { |u| u['name'] }.sort

        assert_not_empty result['users']
        assert_equal result_users, account_user_names
      end
    end

    describe "rate limiting" do
      before do
        11.times do |_i|
          get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
        end
      end

      before_should "not limit system users" do
        Prop.expects(:throttle!).never
      end
    end
  end

  as_a_system_user do
    it "does not rate limit" do
      Prop.expects(:throttle!).never
      get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
    end
  end

  as_an_admin do
    describe 'without start_time' do
      before { get :index }
      it('responds with bad_request') { assert_response :bad_request }
    end

    describe "rate limiting" do
      before do
        Timecop.freeze
        Prop.reset(:incremental_exports, @account.id)
        11.times do |_i|
          get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
        end
      end

      it "doesn't rate limit requests from BIME" do
        @request.env["zendesk.bime.trusted_request"] = true
        Timecop.freeze
        11.times do
          get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
        end
        assert_response 200
      end

      it "doesn't rate limit requests from SEGMENT" do
        @request.env["zendesk.segment.trusted_request"] = true
        Timecop.freeze
        11.times do
          get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
        end
        assert_response 200
      end

      it "rates limit if polled multiple times in the same window" do
        assert_response 429
        assert @response.headers.include?("Retry-After")
      end
    end

    describe 'with no users' do
      before do
        get :index, params: { start_time: (Time.now + 20.years).to_i }
      end

      it 'gives back just the headers' do
        result = JSON.parse(@response.body)
        assert_empty result['users']
        assert_nil result['end_time']
      end
    end

    describe 'with all users' do
      before do
        get :index, params: { start_time: 0 }
      end

      it 'does not remove personally identifiable information fields' do
        result = JSON.parse(@response.body)

        assert_not_empty result['users']
        assert_not_empty result['users'].select { |u| u.key?('email') }
        assert_not_empty result['users'].select { |u| u.key?('phone') }
      end
    end

    describe 'with deleted users' do
      let(:current_user) { users(:minimum_admin) }
      let(:active_user) { users(:minimum_end_user) }
      let(:user_to_delete) { users(:minimum_user_to_delete) }

      before do
        user_to_delete.current_user = current_user
        user_to_delete.delete!
      end

      it 'shows permanently_deleted key only on deleted users' do
        get :index, params: { start_time: 0 }
        users = JSON.parse(@response.body)['users']

        assert_equal false, users.find { |u| u["id"] == user_to_delete.id }["permanently_deleted"]
        assert_nil users.find { |u| u["id"] == active_user.id }["permanently_deleted"]
      end

      it "does not do n + 1 queries" do
        assert_no_n_plus_one(only_tables: %w[compliance_deletion_statuses]) do
          get :index, params: { start_time: 0 }
        end
      end
    end

    describe "when the account has api name removal" do
      before do
        @account.settings.create!(name: "insights_user_name_removal", value: 1)
        get :index, params: { start_time: 0 }
      end

      it 'leaves the user names intact' do
        result = JSON.parse(@response.body)
        account_user_names = @account.all_users.map { |u| u['name'] }.sort
        result_users = result['users'].map { |u| u['name'] }.sort

        assert_not_empty result['users']
        assert_equal result_users, account_user_names
      end
    end

    describe 'sample end point' do
      before do
        get :sample, params: { format: :json, start_time: 1.minutes.ago.to_i }
      end

      it('responds with success') { assert_response :success }

      describe "rate limiting" do
        before do
          get :sample, params: { format: :json, start_time: 1.minutes.ago.to_i }
        end

        it "is successful when polled twice" do
          assert_response :success
        end
      end
    end
  end

  describe 'exporting custom fields' do
    before do
      @non_export_field = @account.custom_fields.for_user.build(key: "dont_export_me", title: "Field 1")
      @non_export_field.type = "Checkbox"
      @non_export_field.is_exportable = false
      @non_export_field.save!

      @export_field = @account.custom_fields.for_user.build(key: "export_me", title: "Field 2")
      @export_field.type = "Integer"
      @export_field.save!
    end

    as_a_subsystem_user(user: 'gooddata', account: :minimum) do
      it 'does not include fields excluded from export' do
        get :index, params: { format: :json, start_time: 0 }
        result = JSON.parse(@response.body)

        exported_field_keys = result['users'].first['user_fields'].map { |key, _value| key }
        refute_includes exported_field_keys, 'dont_export_me'
        assert_includes exported_field_keys, 'export_me'
      end
    end

    as_an_admin do
      it 'always include fields excluded from export' do
        get :index, params: { format: :json, start_time: 0 }
        result = JSON.parse(@response.body)

        exported_field_keys = result['users'].first['user_fields'].map { |key, _value| key }
        assert_includes exported_field_keys, 'dont_export_me'
        assert_includes exported_field_keys, 'export_me'
      end
    end
  end
end
