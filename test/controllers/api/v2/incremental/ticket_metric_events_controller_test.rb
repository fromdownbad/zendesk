require_relative '../../../../support/test_helper'
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::Incremental::TicketMetricEventsController do
  extend ApiScopesHelper
  fixtures :all

  let(:ticket) { tickets(:minimum_1) }
  let(:result) { JSON.parse(@response.body) }

  describe '#index' do
    before { Timecop.freeze }

    as_an_admin do
      describe 'with a recent start_time' do
        before { get :index, params: { format: :json, start_time: Time.now.to_i } }

        it('responds with success') { assert_response :success }
      end

      describe 'with all ticket metric events' do
        before do
          2.times do
            ticket.metric_events.reply_time.activate.create(
              account: @account,
              time:    Time.now
            )
          end

          get :index, params: { format: :json, start_time: 0 }
        end

        it 'returns the ticket metric events' do
          assert_equal 2, result['ticket_metric_events'].count
        end

        it('responds with success') { assert_response :success }
      end

      describe 'with deleted metric events' do
        before do
          ticket.metric_events.reply_time.breach.create(
            account: @account,
            time:    Time.now
          ).tap(&:soft_delete)

          get :index, params: { format: :json, start_time: 0 }
        end

        it 'does not return the deleted ticket metric events' do
          assert_equal 0, result['ticket_metric_events'].count
        end

        it('responds with success') { assert_response :success }
      end
    end

    ["gooddata", "bime"].each do |subsystem_user|
      as_a_subsystem_user(user: subsystem_user, account: :minimum) do
        describe "with a recent start_time" do
          should_be_authorized do
            get :index, params: { format: :json, start_time: Time.now.to_i }
          end
        end
      end
    end

    as_a_subsystem_user(user: 'gooddata', account: :minimum) do
      describe 'with all ticket metric events' do
        before do
          2.times do
            ticket.metric_events.reply_time.activate.create(
              account: @account,
              time:    Time.now
            )
          end

          get :index, params: { format: :json, start_time: 0 }
        end

        it 'returns the ticket metric events' do
          assert_equal 2, result['ticket_metric_events'].count
        end

        it('responds with success') { assert_response :success }
      end

      describe 'with deleted metric events' do
        before do
          ticket.metric_events.reply_time.breach.create(
            account: @account,
            time:    Time.now
          ).tap(&:soft_delete)

          get :index, params: { format: :json, start_time: 0 }
        end

        it 'does not return the deleted ticket metric events' do
          assert_equal 0, result['ticket_metric_events'].count
        end

        it('responds with success') { assert_response :success }
      end
    end
  end
end
