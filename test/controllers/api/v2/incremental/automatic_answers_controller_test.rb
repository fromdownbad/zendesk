require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::Incremental::AutomaticAnswersController do
  extend ApiScopesHelper
  fixtures :tickets, :ticket_deflections, :ticket_deflection_articles, :role_settings
  let(:account) { accounts(:minimum) }

  before do
    @request.account = account
  end

  ["gooddata", "bime"].each do |subsystem_user|
    as_a_subsystem_user(user: subsystem_user, account: :minimum) do
      describe "with a recent start_time" do
        should_be_authorized do
          get :index, params: { format: :json, start_time: Time.now.to_i }
        end
      end

      it "does not rate limit" do
        Prop.expects(:throttle!).never
        get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
      end
    end
  end

  as_an_admin do
    describe 'without start_time' do
      before { get :index, format: :json }
      it('responds with bad_request') { assert_response :bad_request }
    end

    describe 'with no ticket deflections' do
      before do
        @controller.stubs(:check_start_time_not_too_recent!)
        get :index, params: { format: :json, start_time: (Time.now + 20.years).to_i }
      end

      it 'gives back just the headers' do
        result = JSON.parse(@response.body)
        assert_empty result['automatic_answers']
        assert_nil result['end_time']
      end
    end

    describe 'with some ticket deflections' do
      let(:start_time) { 1.hour.ago }

      before do
        get :index, params: { format: :json, start_time: 1.hour.ago.to_i }
      end

      it 'gives back some deflections' do
        result = JSON.parse(@response.body)
        deflection_article = ticket_deflection_articles(:minimum_ticket_deflection_article)
        expected_deflection_count = account.ticket_deflections.where("updated_at >= ?", start_time).count

        assert_equal expected_deflection_count, result['automatic_answers'].count
        assert_equal deflection_article.article_id, result['automatic_answers'].first['articles'].first['article_id']
        assert result['end_time']
      end
    end

    it "doesn't rate limit requests from SEGMENT" do
      @request.env["zendesk.segment.trusted_request"] = true
      Timecop.freeze
      11.times do
        get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
      end
      assert_response 200
    end

    it "rates limit if polled multiple times in the same window" do
      Timecop.freeze
      11.times do
        get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
      end
      assert_response 429
      assert @response.headers.include?("Retry-After")
    end

    describe 'sample end point' do
      before do
        get :sample, params: { format: :json, start_time: 1.minutes.ago.to_i }
      end

      it('responds with success') { assert_response :success }

      describe "rate limiting" do
        before do
          get :sample, params: { format: :json, start_time: 1.minutes.ago.to_i }
        end

        it "is successful when polled twice" do
          assert_response :success
        end
      end
    end
  end
end
