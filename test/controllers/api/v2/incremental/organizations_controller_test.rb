require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::Incremental::OrganizationsController do
  extend ApiScopesHelper
  fixtures :all

  before do
    @account = accounts(:minimum)
    @request.account = @account
  end

  as_an_admin do
    describe 'without start_time' do
      before { get :index, format: :json }
      it('responds with bad_request') { assert_response :bad_request }
    end

    describe "rate limiting" do
      before do
        Timecop.freeze
        Prop.reset(:incremental_exports, @account.id)
        10.times do
          get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
          assert_response 200
        end
      end

      it "doesn't rate limit requests from SEGMENT" do
        @request.env["zendesk.segment.trusted_request"] = true
        Timecop.freeze
        11.times do
          get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
        end
        assert_response 200
      end

      it "rate limits if polled multiple times in the same window" do
        get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
        assert_response 429
        assert @response.headers.include?("Retry-After")
      end
    end

    describe 'with no organizations' do
      before do
        get :index, params: { format: :json, start_time: (Time.now + 20.years).to_i }
      end

      it 'gives back just the headers' do
        result = JSON.parse(@response.body)
        assert_empty result['organizations']
        assert_nil result['end_time']
      end
    end

    describe '#sample' do
      before do
        get :sample, params: { format: :json, start_time: 1.minutes.ago.to_i }
      end

      it('responds with success') { assert_response :success }

      describe "rate limiting" do
        before do
          get :sample, params: { format: :json, start_time: 1.minutes.ago.to_i }
        end

        it "is successful when polled twice" do
          assert_response :success
        end
      end
    end

    describe "ordering" do
      it "does not order by name ASC" do
        assert_sql_queries(0, /`organizations`.`name` ASC/) do
          get :index, params: { format: :json, start_time: Time.at(0) }
        end
      end

      it "includes deleted orgs" do
        assert_sql_queries(0, /WHERE `organizations`\.`deleted_at` IS NULL/) do
          get :index, params: { format: :json, start_time: Time.at(0) }
        end
      end
    end
  end

  ["gooddata", "bime"].each do |subsystem_user|
    as_a_subsystem_user(user: subsystem_user, account: :minimum) do
      describe "with a recent start_time" do
        should_be_authorized do
          get :index, params: { format: :json, start_time: Time.now.to_i }
        end
      end

      it "does not rate limit" do
        Prop.expects(:throttle!).never
        get :index, params: { start_time: 1.minutes.ago.to_i, format: :json }
      end
    end
  end

  describe 'exporting custom fields' do
    before do
      @non_export_field = @account.custom_fields.for_organization.build(key: "dont_export_me", title: "Field 1")
      @non_export_field.type = "Checkbox"
      @non_export_field.is_exportable = false
      @non_export_field.save!

      @export_field = @account.custom_fields.for_organization.build(key: "export_me", title: "Field 2")
      @export_field.type = "Integer"
      @export_field.save!
    end

    as_a_subsystem_user(user: 'gooddata', account: :minimum) do
      it 'does not include fields excluded from export' do
        get :index, params: { format: :json, start_time: 0 }
        result = JSON.parse(@response.body)

        exported_field_keys = result['organizations'].first['organization_fields'].map { |key, _value| key }
        refute_includes exported_field_keys, 'dont_export_me'
        assert_includes exported_field_keys, 'export_me'
      end
    end

    as_an_admin do
      it 'always include fields excluded from export' do
        get :index, params: { format: :json, start_time: 0 }
        result = JSON.parse(@response.body)

        exported_field_keys = result['organizations'].first['organization_fields'].map { |key, _value| key }
        assert_includes exported_field_keys, 'dont_export_me'
        assert_includes exported_field_keys, 'export_me'
      end
    end
  end
end
