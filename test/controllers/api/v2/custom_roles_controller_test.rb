require_relative "../../../support/test_helper"
require_relative "../../../support/multiproduct_test_helper"

SingleCov.covered!

describe Api::V2::CustomRolesController do
  extend Api::Presentation::TestHelper
  include MultiproductTestHelper
  fixtures :all
  let(:account) { @controller.send(:current_account) }

  before do
    accept :json
  end

  with_options(controller: 'api/v2/custom_roles') do |request|
    request.should_route :get, '/api/v2/custom_roles', action: 'index'
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
    should_be_unauthorized :create
    should_be_unauthorized :show
    should_be_unauthorized :update
    should_be_unauthorized :destroy
  end

  as_an_end_user do
    should_be_forbidden :index
    should_be_forbidden :create
    should_be_forbidden :show
    should_be_forbidden :update
    should_be_forbidden :destroy
  end

  describe "embeddable subsystem user" do
    describe "in account without permission sets" do
      as_a_subsystem_user(account: :minimum, user: "embeddable") do
        should_respond_with_status_on_actions(:payment_required, :create)
        should_respond_with_status_on_actions(:payment_required, :show)
        should_respond_with_status_on_actions(:payment_required, :update)
        should_respond_with_status_on_actions(:payment_required, :destroy)
      end
    end

    as_a_subsystem_user(account: :extra_large, user: "embeddable") do
      let(:config_params) do
        {
          assign_tickets_to_any_group: true,
          end_user_list_access: "full",
          end_user_profile_access: "edit-within-org",
          explore_access: "edit",
          forum_access: "edit-topics",
          forum_access_restricted_content: true,
          macro_access: "manage-personal",
          manage_business_rules: false,
          manage_dynamic_content: true,
          manage_extensions_and_channels: true,
          manage_facebook: true,
          organization_editing: false,
          report_access: "readonly",
          ticket_access: "within-groups",
          ticket_comment_access: "none",
          ticket_deletion: false,
          ticket_editing: false,
          ticket_merge: false,
          ticket_tag_editing: false,
          twitter_search_access: false,
          user_view_access: "manage-personal",
          view_access: "playonly",
          view_deleted_tickets: false,
          view_ticket_satisfaction_prediction: false,
          voice_access: false,
          voice_dashboard_access: true,
          side_conversation_create: true
        }
      end

      let(:expected_permissions) do
        {
          "assign_tickets_to_any_group" => "1",
          "available_user_lists" => "all",
          "end_user_profile" => "edit-within-org",
          "explore_access" => "edit",
          "forum_access" => "edit-topics",
          "forum_access_restricted_content" => "1",
          "macro_access" => "manage-personal",
          "business_rule_management" => "0",
          "manage_dynamic_content" => "1",
          "extensions_and_channel_management" => "1",
          "manage_facebook" => "1",
          "edit_organizations" => "0",
          "report_access" => "readonly",
          "ticket_access" => "within-groups",
          "comment_access" => "none",
          "ticket_deletion" => "0",
          "ticket_editing" => "0",
          "ticket_merge" => "0",
          "edit_ticket_tags" => "0",
          "view_twitter_searches" => "0",
          "user_view_access" => "manage-personal",
          "view_access" => "playonly",
          "view_deleted_tickets" => "0",
          "view_ticket_satisfaction_prediction" => "0",
          "voice_availability_access" => "0",
          "voice_dashboard_access" => "1",
          "side_conversation_create" => "1"
        }
      end

      describe "a GET to :index" do
        before do
          get :index
        end

        it("responds with success") { assert_response :success }
      end

      describe "a POST to :create" do
        it "creates a permission set" do
          assert_difference('PermissionSet.count(:all)', 1) do
            post :create, params: { custom_role: { name: 'test role', description: 'test desc', configuration: config_params } }
            assert_response :success
            ps = PermissionSet.find(JSON.parse(@response.body)['custom_role']['id'])
            assert_equal 'test role', ps.name
            assert_equal 'test desc', ps.description
            value_map = Hash[ps.permissions.map { |p| [p.name, p.value] }]
            assert_equal expected_permissions, value_map
          end
        end

        it "enqueues RoleSyncJob" do
          Omnichannel::RoleSyncJob.expects(:enqueue).with(has_entries(
            account_id: account.id,
            context: 'api_create',
            permissions_changed: []
          )).once
          post :create, params: { custom_role: { name: 'test role', description: 'test desc', configuration: config_params } }
        end

        describe "without custom_role" do
          before do
            ActionController::Parameters.action_on_unpermitted_parameters = :log
          end

          it 'returns 400' do
            post :create, params: { configuration: 'whatever', format: :json }
            assert_response :bad_request
          end
        end

        describe "when dual save is enabled on the account" do
          let(:subdomain) { 'test' }
          let(:user_id) { 10011 }
          let(:client_options) { { circuit_options: { failure_threshold: 10000, reset_timeout: 5 } } }
          let(:permissions_set_policies_helper) { Zendesk::ExternalPermissions::PermissionSetPoliciesHelper.new(subdomain, user_id, client_options) }
          let(:subdomain_policies_url) { permissions_set_policies_helper.policies_url }

          before do
            account.stubs(:has_permissions_custom_role_policy_dual_save?).returns(true)
          end

          it "creates a permission set and calls permissions service to create policy" do
            stub_request = stub_request(:post, subdomain_policies_url).to_return(status: 200)
            assert_difference('PermissionSet.count(:all)', 1) do
              post :create, params: { custom_role: { name: 'test role', description: 'test desc', configuration: config_params } }
              assert_response :success
              assert_requested stub_request, times: 1
              ps = PermissionSet.find(JSON.parse(@response.body)['custom_role']['id'])
              assert_equal 'test role', ps.name
              assert_equal 'test desc', ps.description
              value_map = Hash[ps.permissions.map { |p| [p.name, p.value] }]
              assert_equal expected_permissions, value_map
            end
          end

          it "undoes permission_set creation and returns internal server error if call to permission service throws an error" do
            stub_request(:post, subdomain_policies_url).to_raise(Kragle::ResponseError)
            assert_difference('PermissionSet.count(:all)', 0) do
              post :create, params: { custom_role: { name: 'test role', description: 'test desc', configuration: config_params } }
              assert_response :internal_server_error
            end
          end

          it "undoes permission_set creation and returns internal server error if call to permission service is not successful" do
            stub_request(:post, subdomain_policies_url).to_return(status: 300)
            assert_difference('PermissionSet.count(:all)', 0) do
              post :create, params: { custom_role: { name: 'test role', description: 'test desc', configuration: config_params } }
              assert_response :internal_server_error
            end
          end
        end
      end

      describe "a GET to :show" do
        before do
          @permission_set = PermissionSet.new(account: account, name: 'starting name')
          @permission_set.save!
        end

        it 'returns permission set info' do
          get :show, params: { id: @permission_set.id }
          assert_response :success
          role_info = JSON.parse(@response.body)['custom_role']
          assert_equal @permission_set.id, role_info['id']
          assert_equal 'starting name', role_info['name']
        end
      end

      describe "a PUT to :update" do
        before do
          @permission_set = PermissionSet.new(account: account, name: 'starting name')
          @permission_set.save!
        end

        it "updates the permission set" do
          put :update, params: { id: @permission_set.id, custom_role: { name: 'test role', description: 'test desc', configuration: config_params } }
          assert_response :success
          @permission_set.reload
          assert_equal 'test role', @permission_set.name
          assert_equal 'test desc', @permission_set.description
          value_map = Hash[@permission_set.permissions.map { |p| [p.name, p.value] }]
          assert_equal expected_permissions, value_map
        end

        it "enqueues RoleSyncJob" do
          Omnichannel::RoleSyncJob.expects(:enqueue).with(has_entries(
            account_id: account.id,
            context: 'api_update',
            permissions_changed: ['forum_access']
          )).once

          put :update, params: { id: @permission_set.id, custom_role: { name: 'test role', description: 'test desc', configuration: { forum_access: "edit-topics" } } }
        end

        describe "without custom_role" do
          before do
            ActionController::Parameters.action_on_unpermitted_parameters = :log
          end

          it 'returns 400' do
            put :update, params: { id: @permission_set.id, format: :json }
            assert_response :bad_request
          end
        end
      end

      describe "a DELETE to :destroy" do
        before do
          @permission_set = PermissionSet.new(account: account, name: 'starting name')
          @permission_set.save!
        end

        it "deletes the permission set" do
          assert_difference('PermissionSet.count(:all)', -1) do
            delete :destroy, params: { id: @permission_set.id }
            refute_operator PermissionSet, :exists?, @permission_set.id
          end
        end

        describe "users are still assigned the role" do
          let(:account) { accounts(:extra_large) }
          let(:agent) { users(:extra_large_end_user) }

          before do
            agent.email = 'test@zendesk.com'
            agent.roles = Role::AGENT.id
            agent.permission_set = @permission_set
            agent.save!
          end

          it "does not delete the permission set" do
            delete :destroy, params: { id: @permission_set.id }
            assert_operator PermissionSet, :exists?, @permission_set.id
          end

          it "returns a failed response" do
            delete :destroy, params: { id: @permission_set.id }
            assert_response :unprocessable_entity
          end
        end

        describe "when dual save is enabled on the account" do
          let(:subdomain) { 'test' }
          let(:user_id) { 10011 }
          let(:client_options) { { circuit_options: { failure_threshold: 10000, reset_timeout: 5 } } }
          let(:permissions_set_policies_helper) { Zendesk::ExternalPermissions::PermissionSetPoliciesHelper.new(subdomain, user_id, client_options) }
          let(:subdomain_policies_url) { permissions_set_policies_helper.policies_url }

          before do
            Account.any_instance.stubs(:has_permissions_custom_role_policy_dual_save?).returns(true)
          end

          it "calls permissions service to delete policy then deletes permission_set" do
            assert_difference('PermissionSet.count(:all)', -1) do
              stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
                  status: 200,
                  body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
                  headers: { "Content-Type" => "application/json" }
                )
              stub_delete_policy_request = stub_request(:delete, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_return(status: 204)
              delete :destroy, params: { id: @permission_set.id }
              assert_requested stub_get_policy_request, times: 1
              assert_requested stub_delete_policy_request, times: 1
              refute PermissionSet.exists?(@permission_set.id)
            end
          end

          it "calls permissions service to delete policy then deletes permission_set if policy resource_not_found on deletion" do
            assert_difference('PermissionSet.count(:all)', -1) do
              stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
                  status: 200,
                  body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
                  headers: { "Content-Type" => "application/json" }
                )
              stub_delete_policy_request = stub_request(:delete, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_raise(Kragle::ResourceNotFound)
              delete :destroy, params: { id: @permission_set.id }
              assert_requested stub_get_policy_request, times: 1
              assert_requested stub_delete_policy_request, times: 1
              refute PermissionSet.exists?(@permission_set.id)
            end
          end

          it "retains permission_set and returns internal server error if call to permission service to get policy (for deletion) throws an error" do
            assert_difference('PermissionSet.count(:all)', 0) do
              stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_raise(Kragle::ServerError)
              delete :destroy, params: { id: @permission_set.id }
              assert_requested stub_get_policy_request, times: 1
              assert PermissionSet.exists?(@permission_set.id)
              assert_response :internal_server_error
            end
          end

          it "retains permission_set and returns internal server error if call to permissions service to delete policy fails" do
            assert_difference('PermissionSet.count(:all)', 0) do
              stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
                  status: 200,
                  body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
                  headers: { "Content-Type" => "application/json" }
                )
              stub_delete_policy_request = stub_request(:delete, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_raise(Kragle::ServerError)
              delete :destroy, params: { id: @permission_set.id }
              assert_requested stub_get_policy_request, times: 1
              assert_requested stub_delete_policy_request, times: 1
              assert PermissionSet.exists?(@permission_set.id)
              assert_response :internal_server_error
            end
          end
        end

        it "enqueues RoleSyncJob" do
          Omnichannel::RoleSyncJob.expects(:enqueue).with(has_entries(
            account_id: account.id,
            context: 'api_destroy',
            permissions_changed: []
          )).once
          delete :destroy, params: { id: @permission_set.id }
        end
      end
    end
  end

  describe "zdp_metadata subsystem user" do
    as_a_subsystem_user(user: 'zdp_metadata', account: :extra_large) do
      should_be_forbidden :show, :create, :update, :destroy

      describe "a GET to :index" do
        before do
          get :index
        end

        it("responds with success") { assert_response :success }
      end
    end
  end

  as_an_agent do
    describe "for accounts with custom roles" do
      before { account.stubs(:has_permission_sets?).returns(true) }

      describe "a GET to :index" do
        describe "runs fine vanilla" do
          before { get :index }
          should_use_presenter Api::V2::Roles::CustomRolePresenter, status: :ok
        end

        describe "adds cache-control headers for apps" do
          before do
            request.headers.merge! "X-Zendesk-App-Id" => "b4dApp"
          end

          describe_with_arturo_enabled :custom_roles_cache_control_header do
            it "responds with success and with cache headers" do
              get :index
              assert_response :success
              assert_equal 'max-age=60, private', @response.headers['Cache-Control']
            end
          end

          describe_with_arturo_disabled :custom_roles_cache_control_header do
            it "responds with success and no cache headers" do
              get :index
              assert_response :success
              assert_nil @response.headers['Cache-Control']
            end
          end
        end

        describe 'with external domains' do
          it 'returns a valid allow-origin header with an allowed external domain as origin' do
            @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
            get :index
            assert_equal 'http://app.futuresimple.com', @response.headers['Access-Control-Allow-Origin']
          end

          it 'returns a not valid allow-origin header with an allowed external domain as origin' do
            @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
            get :index
            assert_nil @response.headers['Access-Control-Allow-Origin']
          end
        end
      end
    end

    describe "for accounts without custom roles" do
      before { @controller.send(:current_account).stubs(:has_permission_sets?).returns(false) }

      describe "a GET to :index" do
        before { get :index }
        it('responds with payment_required') { assert_response :payment_required }
      end

      describe "with zopim chat permission set" do
        before { @controller.send(:current_account).stubs(:has_chat_permission_set?).returns(true) }

        describe "a GET to :index" do
          before { get :index }
          should_use_presenter Api::V2::Roles::CustomRolePresenter, status: :ok
        end
      end

      describe "with light agents" do
        before { @controller.send(:current_account).stubs(:has_light_agents?).returns(true) }

        describe "a GET to :index" do
          before { get :index }
          should_use_presenter Api::V2::Roles::CustomRolePresenter, status: :ok
        end
      end
    end
  end
end
