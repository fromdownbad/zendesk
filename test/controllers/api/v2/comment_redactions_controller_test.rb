require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::CommentRedactionsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :users, :tickets, :events, :attachments

  let(:ticket) { tickets(:minimum_1) }
  let(:comment) { ticket.comments.first }
  let(:external_attachment) { attachments(:attachment_entry) }

  before do
    accept :json
    use_ssl
    stub_request(:delete, %r{\.amazonaws\.com/data/attachments/*})
    stub_request(:put, %r{\.amazonaws\.com/data/attachments/*})
  end

  with_options(controller: "api/v2/comment_redactions") do |request|
    request.should_route :put, "/api/v2/comment_redactions/2", action: "update", id: 2
  end

  as_an_anonymous_user do
    should_be_unauthorized([:put, :update, {ticket_id: 1, id: 2}])
  end

  as_an_agent do
    should_be_forbidden [:put, :update, {ticket_id: 1, id: 2}]
  end

  as_an_admin do
    describe_with_arturo_disabled :agent_workspace_omni_redaction do
      should_be_forbidden [:put, :update, {ticket_id: 1, id: 2}]
    end
  end

  as_an_admin do
    describe "a PUT to :update" do
      describe_with_arturo_enabled(:agent_workspace_omni_redaction) do
        let(:params) { {ticket_id: ticket.nice_id, id: comment.id, text: "minimum <redact>ti</redact>cket 1"} }
        let(:domain_event_publisher) { FakeEscKafkaMessage.new }

        describe "simple" do
          before { put :update, params: params }
          should_use_presenter Api::V2::Tickets::CommentPresenter
        end

        it "updates the comments text" do
          assert_equal "minimum ticket 1", comment.body
          put :update, params: params
          assert_response :success
          assert_equal "minimum ▇▇cket 1", comment.reload.body
        end

        it "updates a public comment without making it private" do
          User.any_instance.stubs(is_light_agent?: true)
          assert comment.reload.is_public?
          assert comment.author.is_light_agent?

          put :update, params: params
          assert comment.reload.is_public?
        end

        it "accepts external attachment urls and redacts them" do
          external_attachment.source = comment
          external_attachment.save!

          put :update, params: params.merge({ external_attachment_urls: [external_attachment.url] })
          audit = ticket.audits.last
          event = audit.events.detect { |e| e.class == AttachmentRedactionEvent }

          assert_equal comment.id, event.comment_id.to_i
          assert_response :success
        end

        it "adds a redaction audit" do
          put :update, params: params
          audit = ticket.audits.last
          event = audit.events.detect { |e| e.class == CommentRedactionEvent }
          assert_equal comment.id, event.comment_id.to_i
        end

        describe_with_arturo_enabled(:publish_ticket_events_to_bus) do
          it "publishes a domain event" do
            Ticket.any_instance.stubs(:domain_event_publisher).returns(domain_event_publisher)
            put :update, params: params

            refute_empty domain_event_publisher.events
            events = domain_event_publisher.events.map do |event|
              ::Zendesk::Protobuf::Support::Tickets::V2::TicketEvent.decode(event[:value]).event
            end
            assert_includes events, :comment_redacted
          end
        end

        it "fails with unfound text" do
          put :update, params: params.merge(text: "minimum ticket 1")
          assert_response :bad_request
          assert_equal "minimum ticket 1", ticket.comments.first.reload.body
        end
      end
    end
  end
end
