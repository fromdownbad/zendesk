require_relative '../../../support/test_helper'

SingleCov.covered!

describe Api::V2::GooddataIntegrationController do
  extend Api::V2::TestHelper

  fixtures :all

  before do
    use_ssl
    accept :json
  end

  with_options(controller: 'api/v2/gooddata_integration') do |request|
    request.should_route(
      :post, '/api/v2/gooddata_integration', action: :create
    )
    request.should_route(
      :get, '/api/v2/gooddata_integration', action: :show
    )
    request.should_route(
      :put, '/api/v2/gooddata_integration', action: :update
    )
    request.should_route(
      :post, '/api/v2/gooddata_integration/re_enable', action: :re_enable
    )
  end

  as_an_anonymous_user do
    should_be_unauthorized :create, [:get, :show, {}], [:put, :update, {}], [:post, :re_enable, {}]
  end

  as_an_end_user do
    should_be_forbidden :create, [:get, :show, {}], [:put, :update, {}], [:post, :re_enable, {}]
  end

  as_an_agent do
    should_be_forbidden :create, [:put, :update, {}]

    describe "when accessing the 'show' action with a GET" do
      describe "and an integration exists" do
        let(:gooddata_integration) { stub_everything('gooddata integration') }

        before do
          @account.stubs(:gooddata_integration).returns(gooddata_integration)
        end

        describe "and the integration version is greater than '1'" do
          before do
            get :show
          end

          should_use_presenter(Api::V2::GooddataIntegrationPresenter,
            status: :ok)
        end
      end

      describe "and an integration does not exist" do
        before do
          @account.stubs(:gooddata_integration).returns(nil)

          get :show
        end

        it('responds with not_found') { assert_response :not_found }
      end
    end

    describe "version" do
      describe "with a gooddata integration" do
        before do
          @integration = @account.create_gooddata_integration(project_id: 'my project')
        end

        describe "and the integration has a version" do
          before do
            @integration_version = 42
            @integration.update_attribute(:version, @integration_version)
            get :version
          end

          it "presents version the integration version" do
            assert_equal @integration_version, JSON.parse(response.body)["version"]
          end
        end

        describe "and the integration is missing a version" do
          before do
            @integration.update_attribute(:version, nil)
            get :version
          end

          it "presents version 2" do
            assert_equal 2, JSON.parse(response.body)["version"]
          end
        end
      end

      describe "without a gooddata integration" do
        before do
          get :version
        end

        it "presents version 2" do
          assert_equal 2, JSON.parse(response.body)["version"]
        end
      end
    end

    describe "GET :last_successful_process" do
      let(:last_successful_process) { "last_successful_process" }
      before do
        @integration = @account.create_gooddata_integration(project_id: 'my project')
      end

      describe "when a successful process exists" do
        before do
          GooddataIntegration.any_instance.stubs(:last_successful_process).returns(last_successful_process)
          get :last_successful_process
        end

        it "presents the process information" do
          assert_equal({"last_successful_process" => last_successful_process}, JSON.parse(response.body))
        end
      end

      describe "when a successful process doesn't exist" do
        before do
          GooddataIntegration.any_instance.stubs(:last_successful_process)
          get :last_successful_process
        end

        it "presents the process information as null" do
          assert_equal({"last_successful_process" => nil}, JSON.parse(response.body))
        end
      end
    end
  end

  as_an_admin do
    describe "when accessing the 'create' action" do
      before { GooddataIntegrationsJob.stubs(:enqueue) }

      describe "with a POST" do
        before { post :create }

        before_should "enqueue a job to create an integration" do
          GooddataIntegrationsJob.expects(:enqueue).with(@account.id, @user.id)
        end

        it('responds with accepted') { assert_response :accepted }
      end
    end

    describe "when accessing the 're_enable' action" do
      before { GooddataEnableJob.stubs(:enqueue) }

      describe "with a POST" do
        before { post :re_enable }

        before_should "enqueue a job to re-enable an integration" do
          GooddataEnableJob.expects(:enqueue).with(@account.id, @user.id)
        end

        it('responds with accepted') { assert_response :accepted }
      end
    end

    describe "when accessing the 'update' action with a PUT" do
      let(:gooddata_integration) { stub_everything('gooddata integration') }

      before do
        @account.stubs(:gooddata_integration).returns(gooddata_integration)
      end

      describe "when provided allowed attributes" do
        before do
          put :update, params: { gooddata_integration: {scheduled_at: '9am'} }
        end

        before_should "update the sync time for the integration" do
          gooddata_integration.expects(:update_attributes!).with(
            'scheduled_at' => '9am'
          )
        end

        should_use_presenter(Api::V2::GooddataIntegrationPresenter,
          status: :ok)
      end
    end
  end

  as_a_subsystem_user(user: "gooddata", account: :minimum) do
    should_be_forbidden :create, [:get, :show, {}], [:put, :update, {}], [:post, :re_enable, {}]
  end
end
