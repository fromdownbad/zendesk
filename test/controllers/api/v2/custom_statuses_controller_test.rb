require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::CustomStatusesController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper

  fixtures :accounts, :users, :custom_statuses

  let(:json_response) { JSON.parse(@response.body) }

  before do
    accept :json
  end

  with_options(controller: 'api/v2/custom_statuses') do |request|
    request.should_route :get, '/api/v2/custom_statuses', action: 'index'
    request.should_route :get, '/api/v2/custom_statuses/1', action: 'show', id: '1'
    request.should_route :post, "/api/v2/custom_statuses", action: 'create'
    request.should_route :put, "/api/v2/custom_statuses/123", action: 'update', id: '123'
    request.should_route :delete, "/api/v2/custom_statuses/123", action: 'destroy', id: '123'
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :show
  end

  as_an_end_user do
    should_be_forbidden :index, :show
  end

  as_an_agent do
    describe_with_arturo_disabled :custom_statuses do
      should_be_forbidden :index, :show
    end

    describe_with_arturo_enabled :custom_statuses do
      describe "a GET to :index" do
        let(:json) { json_response['custom_statuses'] }

        before { get :index }
        should_use_presenter Api::V2::CustomStatusPresenter, status: 200

        it "presents all custom statuses for the account" do
          assert_equal @account.custom_statuses.map(&:id), json.map { |cs| cs['id'] }
        end
      end

      describe "a GET to :show" do
        let(:custom_status) { custom_statuses(:minimum_open) }
        let(:json) { json_response['custom_status'] }

        before { get :show, params: { id: custom_status.id } }
        should_use_presenter Api::V2::CustomStatusPresenter, status: 200

        it "presents the specific custom status" do
          assert_equal custom_status.id, json['id']
        end
      end

      describe "with CTS enabled" do
        before do
          @account.update_attributes!(settings: {
            custom_statuses_enabled: true
          })
        end

        describe "a PUT to :update" do
          should_be_forbidden :update
        end

        describe "a PUT to :create" do
          should_be_forbidden :update
        end
      end
    end
  end

  as_an_admin do
    describe_with_arturo_enabled :custom_statuses do
      describe "a POST to :create with CTS disabled" do
        should_be_forbidden :create
      end

      describe "a PUT to :update with CTS disabled" do
        should_be_forbidden :update
      end

      describe "with CTS enabled" do
        before do
          @account.update_attributes!(settings: {
            custom_statuses_enabled: true
          })
        end

        describe "a POST to :create" do
          let(:custom_status_params) do
            {
              status_category: "open",
              agent_label: "In Development",
              end_user_label: "In Development",
              description: "The development team is working on this"
            }
          end

          describe "with valid parameters" do
            before { post :create, params: { custom_status: custom_status_params } }

            should_use_presenter Api::V2::CustomStatusPresenter, status: :created

            it "sets the correct position" do
              positions_for_status = @account.custom_statuses.reload.for_status(StatusType.OPEN).map(&:position)
              # 100
              initial = CustomStatus::POSITION_RANGES[StatusType.OPEN].first
              # 102
              last_position = CustomStatus.last.position
              # [100, 101, 102]
              assert_equal (initial..last_position).to_a, positions_for_status
            end
          end

          describe "with invalid category" do
            let(:params_without_category) { custom_status_params.merge(status_category: "invalid") }

            before { post :create, params: { custom_status: params_without_category } }

            it "fails with the correct invalid parameter error message" do
              assert_response :bad_request
              assert json_response['error']['message'].include? "You passed an invalid value for the status_category attribute"
            end
          end

          describe "without required information" do
            let(:params_without_required_information) { custom_status_params.except(:agent_label) }

            before { post :create, params: { custom_status: params_without_required_information } }

            it "fails with the correct validation error coming from the model" do
              assert_response :unprocessable_entity
              assert_equal json_response['error'], "RecordInvalid"
              assert json_response['details']['agent_label'].first['description'].include? "cannot be blank"
            end
          end
        end

        describe "a PUT to :update" do
          let(:custom_status) { custom_statuses(:minimum_in_progress) }
          let(:custom_status_params) do
            {
              agent_label: "In Development",
              end_user_label: "In Development",
              description: "The development team is working on this"
            }
          end

          describe "with valid parameters" do
            before do
              put :update, params: {
                id: custom_status.id,
                custom_status: custom_status_params
              }
              custom_status.reload
            end

            should_use_presenter Api::V2::CustomStatusPresenter, status: :ok

            it "updates the values" do
              assert_equal custom_status.agent_label, custom_status_params[:agent_label]
              assert_equal custom_status.end_user_label, custom_status_params[:end_user_label]
              assert_equal custom_status.description, custom_status_params[:description]
            end
          end

          describe "with status category" do
            let(:params_with_category) { custom_status_params.merge(status_category: "pending") }

            before do
              put :update, params: {
                id: custom_status.id,
                custom_status: params_with_category
              }
              custom_status.reload
            end

            should_use_presenter Api::V2::CustomStatusPresenter, status: :ok

            it "does not update category" do
              assert_equal custom_status.status_category, "open"
            end
          end

          describe "with active: false" do
            before do
              put :update, params: {
                id: custom_status.id,
                custom_status: { active: false }
              }
              custom_status.reload
            end

            should_use_presenter Api::V2::CustomStatusPresenter, status: :ok

            it "deactivates the custom status" do
              refute custom_status.active?
            end
          end

          describe "with active: true" do
            before do
              custom_status.update_attribute(:active, false)
              refute custom_status.active?

              put :update, params: {
                id: custom_status.id,
                custom_status: { active: true }
              }
              custom_status.reload
            end

            should_use_presenter Api::V2::CustomStatusPresenter, status: :ok

            it "activates the custom status" do
              assert custom_status.active?
            end
          end

          describe "with active: false in a default custom status" do
            let(:custom_status) { custom_statuses(:minimum_open) }

            before do
              put :update, params: {
                id: custom_status.id,
                custom_status: { active: false }
              }
              custom_status.reload
            end

            it "fails with the correct validation error coming from the model" do
              assert_response :unprocessable_entity
              assert_equal json_response['error'], "RecordInvalid"
              assert json_response['details']['default'].first['description'].include? "Can not set the default custom status to inactive"
            end
          end
        end

        describe "a DELETE to :destroy" do
          describe "with valid custom status" do
            let(:custom_status) { custom_statuses(:minimum_in_progress) }

            before do
              delete :destroy, params: { id: custom_status.id }
            end

            it "soft deletes the status" do
              assert_response :success
              assert custom_status.reload.deleted?
            end
          end

          describe "with default custom status" do
            let(:custom_status) { custom_statuses(:minimum_open) }

            before do
              delete :destroy, params: { id: custom_status.id }
            end

            it "fails" do
              assert_response :unprocessable_entity
              refute custom_status.reload.deleted?
            end
          end
        end
      end
    end
  end
end
