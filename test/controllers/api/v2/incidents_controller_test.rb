require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::IncidentsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: "api/v2/incidents") do |request|
    request.should_route :get, "/api/v2/tickets/1/incidents", action: "index", ticket_id: "1"
    request.should_route :get, "/api/v2/tickets/1/incidents/count", action: "count", ticket_id: "1"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :count, add_to_all: {ticket_id: 1}
  end

  as_an_end_user do
    should_be_forbidden :index, :count, add_to_all: {ticket_id: 1}
  end

  as_an_agent do
    describe "a GET to #index" do
      describe "presenter" do
        before { get :index, params: { ticket_id: tickets(:minimum_1).nice_id } }
        should_use_presenter Api::V2::Tickets::TicketPresenter
      end

      describe 'cursor based pagination v2' do
        let(:problem) { tickets(:minimum_1) }
        let(:page_size) { 2 }
        let(:page_params) { { size: page_size } }
        let(:sort) { nil }
        let(:params) { { ticket_id: problem.nice_id, page: page_params, sort: sort } }
        let(:json) do
          get :index, params: params
          JSON.parse(@response.body).with_indifferent_access
        end

        before do
          problem.update_column(:ticket_type_id, TicketType.PROBLEM)
          @incident_1 = add_incident(problem, tickets(:minimum_2))
          @incident_2 = add_incident(problem, tickets(:minimum_3))
          @incident_3 = add_incident(problem, tickets(:minimum_4))
          problem.reload
        end

        describe_with_arturo_disabled :cursor_pagination_v2_ticket_incidents_endpoint do
          it "still uses cbp v1" do
            @controller.expects(:paginate_with_cursor).never
            get :index, params: { ticket_id: problem.nice_id, limit: 2 }
            incidents_keys = JSON.parse(@response.body).with_indifferent_access.keys
            assert_equal 4, (incidents_keys & ['after_cursor', 'before_cursor', 'after_url', 'before_url']).size
          end

          it "returns 400 bad request if the after param is passed in" do
            get :index, params: { ticket_id: problem.nice_id, page: { after: 'xxx' } }
            assert_response :bad_request
            assert_equal 'Invalid attribute', json[:error][:title]
            assert_includes json[:error][:message], 'You passed an invalid value for the page attribute. Invalid parameter: page must be an integer'
          end
        end

        describe 'when there is only one page' do
          let(:page_size) { problem.incidents.size + 1 }

          it 'presents the correct keys' do
            # binding.pry
            refute json[:meta][:has_more]
            assert json[:meta][:before_cursor].present?
            assert json[:meta][:after_cursor].present?
            assert json[:links][:prev].present?
            assert json[:links][:next].present?
          end

          it 'presents the correct number of tickets' do
            assert_equal problem.incidents.size, json[:tickets].size
          end
        end

        describe 'when there is more than one page' do
          let(:page_size) { problem.incidents.size - 1 }

          it 'presents the correct keys' do
            assert json[:meta][:has_more]
            assert json[:meta][:before_cursor].present?
            assert json[:meta][:after_cursor].present?
            assert json[:links][:prev].present?
            assert json[:links][:next].present?
          end

          it 'presents the correct number of incidents' do
            assert_equal page_size, json[:tickets].size
          end
        end

        describe 'with a valid after cursor' do
          let(:page_size) { problem.incidents.size - 1 }
          let(:after_json) do
            JSON.parse(@response.body).with_indifferent_access
          end

          before do
            after_cursor = json[:meta][:after_cursor]

            # The same controller instance is reused within each test and this controller memoizes
            # incidents. To get to the second page of results we need to reach in and
            # clear the controller's @incidents variable. Outside of controller
            # tests we get a new instance of the controller on each request - this mimics
            # that behavior for only this variable.
            @controller.instance_variable_set(:@incidents, nil)

            get :index, params: {
              ticket_id: problem.nice_id,
              page: {
                size: page_size,
                after: after_cursor
              }
            }
          end

          it 'presents the correct keys' do
            refute after_json[:meta][:has_more]
            assert after_json[:meta][:before_cursor].present?
            assert after_json[:meta][:after_cursor].present?
            assert after_json[:links][:prev].present?
            assert after_json[:links][:next].present?
          end

          it 'presents the correct number of incidents' do
            assert_equal 1, after_json[:tickets].size
          end
        end

        describe 'with an invalid after cursor' do
          let(:page_params) do
            {
              size: page_size,
              after: 'invalid'
            }
          end

          it 'presents an error message' do
            assert_equal 'InvalidPaginationParameter', json[:error]
            assert_equal 'page[after] is not valid', json[:description]
            assert_response :bad_request
          end
        end
        describe 'when sort parameter has multiple fields' do
          let(:sort) { '-created_at,id' }

          it 'shows an error message' do
            json
            assert_response :bad_request
            assert @response.body =~ /sort is not valid/
          end
        end

        describe 'when sort parameter has invalid fields' do
          let(:sort) { '-invalid' }

          it 'shows an error message' do
            json
            assert_response :bad_request
            assert @response.body =~ /sort is not valid/
          end
        end
      end
    end

    describe "a GET to #count" do
      let(:iso_date) { '2020-12-15T03:21:55+00:00' }

      before do
        @problem = tickets(:minimum_1)
        @problem.update_column(:ticket_type_id, TicketType.PROBLEM)
        @incident_1 = add_incident(@problem, tickets(:minimum_2))
        @incident_2 = add_incident(@problem, tickets(:minimum_3))
        @problem.reload

        Zendesk::RecordCounter::Incidents.any_instance.stubs(:iso_date).returns(iso_date)
        get :count, params: { ticket_id: @problem.nice_id }
      end

      it "should respond with the correct format and values" do
        expected_response = {
          'count' => {
            'value' => @problem.incidents.count,
            'refreshed_at' => iso_date
          },
          'links' => {
            'url' => 'https://minimum.zendesk-test.com/api/v2/tickets/1/incidents/count'
          }
        }

        assert_equal expected_response, JSON.parse(@response.body)
        assert_response :success
      end
    end

    describe "sorting" do
      let(:params) { {} }

      before do
        @problem = tickets(:minimum_1)
        @problem.update_column(:ticket_type_id, TicketType.PROBLEM)
        @incident_1 = add_incident(@problem, tickets(:minimum_2))
        @incident_2 = add_incident(@problem, tickets(:minimum_3))
        @problem.reload
      end

      it "sorts asc" do
        get :index, params: { ticket_id: @problem.nice_id, sort_by: "subject", sort_order: "asc" }
        expected = [@incident_1, @incident_2].sort_by(&:subject)
        assert_equal expected.map(&:subject), JSON.parse(@response.body)['tickets'].map { |t| t['subject'] }
      end

      it "sorts desc" do
        get :index, params: { ticket_id: @problem.nice_id, sort_by: "subject", sort_order: "desc" }
        expected = [@incident_1, @incident_2].sort { |b, a| a.subject <=> b.subject }
        assert_equal expected.map(&:subject), JSON.parse(@response.body)['tickets'].map { |t| t['subject'] }
      end

      it "defaults to created_at asc" do
        @incident_2.update_column(:created_at, (@incident_1.created_at - 1.day))
        get :index, params: { ticket_id: @problem.nice_id }
        expected = [@incident_1, @incident_2].sort_by(&:created_at)
        assert_equal expected.map { |t| t.created_at.iso8601 }, JSON.parse(@response.body)['tickets'].map { |t| t['created_at'] }
      end

      it "defaults to offset based pagination" do
        get :index, params: { ticket_id: @problem.nice_id }

        response = JSON.parse(@response.body)
        assert_includes response, "next_page"
        assert_includes response, "previous_page"

        refute_includes response, "after_cursor"
        refute_includes response, "before_cursor"
      end

      it "includes cursor parameters, not offset params" do
        get :index, params: params.merge(ticket_id: @problem.nice_id, cursor: "")

        response = JSON.parse(@response.body)
        refute_includes response, "next_page"
        refute_includes response, "previous_page"

        assert_includes response, "after_cursor"
        assert_includes response, "before_cursor"
      end

      describe "by locale" do
        before { get :index, params: { sort_by: 'locale', ticket_id: @problem.nice_id } }

        it "uses the custom sort_by" do
          assert_equal "FIELD(locale_id,#{@controller.send(:current_account).available_languages.map(&:id).join(',')})", @controller.send(:sort_by)
        end
      end
    end
  end

  as_an_agent_with_permissions(ticket_access: 'assigned-only') do
    before do
      Account.any_instance.stubs(has_extended_ticket_types?: true)
      @owner = users(:minimum_admin)
      @agent = users(:minimum_agent)
      @problem = tickets(:minimum_1)
      @problem.will_be_saved_by(@owner)
      @problem.update_attributes(
        ticket_type_id: TicketType.PROBLEM,
        assignee_id: @owner.id
      )
      @problem.save!
      @incident_1 = add_incident(@problem, tickets(:minimum_2))
      @incident_2 = add_incident(@problem, tickets(:minimum_3))
    end

    describe "a GET to #index" do
      describe "with a problem ticket that is not assigned to the agent" do
        before { get :index, params: { ticket_id: @problem.nice_id } }

        it "responds with forbidden" do
          assert_response :forbidden
        end
      end

      describe "with a problem ticket that is assigned to the agent" do
        before do
          @problem.update_column(:assignee_id, @agent.id)
          @incident_1.update_column(:assignee_id, @agent.id)
        end

        it "only returns incidents the agent is permitted to see" do
          get :index, params: { ticket_id: @problem.nice_id }

          incidents = JSON.parse(@response.body)["tickets"]
          incidents.each { |t| assert_equal @agent.id, t["assignee_id"] }
          assert_not_equal incidents.count, @problem.reload.incidents.count
        end

        it "defaults to offset based pagination" do
          get :index, params: { ticket_id: @problem.nice_id }

          response = JSON.parse(@response.body)
          assert_includes response, "next_page"
          assert_includes response, "previous_page"

          refute_includes response, "after_cursor"
          refute_includes response, "before_cursor"
        end

        it "includes cursor parameters, not offset params" do
          get :index, params: {}.merge(ticket_id: @problem.nice_id, cursor: "")

          response = JSON.parse(@response.body)
          refute_includes response, "next_page"
          refute_includes response, "previous_page"

          assert_includes response, "after_cursor"
          assert_includes response, "before_cursor"
        end
      end
    end

    describe "a GET to #count" do
      before do
        # RecordCounter reloads current user from DB. Because of that,
        # we need to set real permissions in DB instead of the stubbing
        # in as_an_agent_with_permissions
        Account.any_instance.stubs(:has_permission_sets?).returns(true)
        @agent.permission_set = @account.permission_sets.create!(name: "Test")
        @agent.permission_set.permissions.ticket_access = 'assigned-only'
        @agent.permission_set.save!
        @agent.save!
      end

      describe "with a problem ticket that is not assigned to the agent" do
        before { get :count, params: { ticket_id: @problem.nice_id } }

        it "responds with forbidden" do
          assert_response :forbidden
        end
      end

      describe "with a problem ticket that is assigned to the agent" do
        before do
          @problem.update_column(:assignee_id, @agent.id)
          @incident_1.update_column(:assignee_id, @agent.id)
        end

        it "only returns a count of incidents the agent is permitted to see" do
          get :count, params: { ticket_id: @problem.nice_id }

          incident_count = JSON.parse(@response.body)["count"]["value"]
          assert_not_equal incident_count, @problem.reload.incidents.count
        end
      end
    end
  end

  def add_incident(problem, incident)
    incident.problem = problem
    incident.will_be_saved_by(accounts(:minimum).owner)
    incident.save
    incident
  end
end
