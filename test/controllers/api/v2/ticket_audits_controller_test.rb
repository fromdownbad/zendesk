require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TicketAuditsController do
  extend Api::Presentation::TestHelper
  extend Api::V2::TestHelper

  fixtures :all

  before do
    use_ssl
    accept :json

    @account = accounts(:minimum)
    @agent   = users(:minimum_agent)
  end

  with_options(controller: 'api/v2/ticket_audits') do |request|
    request.should_route :get, '/api/v2/ticket_audits', action: 'index'
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_agent do
    should_be_forbidden :index
  end

  as_an_admin do
    describe "index" do
      before do
        get :index, params: {}.merge(@add_params || {})
      end

      should_use_presenter Api::V2::Tickets::AuditPresenter

      it "should return ticket audits" do
        json = JSON.parse(@response.body)["audits"]
        assert json.size > 1
      end

      describe "cursor pagination" do
        it "should not include count of tickets" do
          ticket_count = JSON.parse(@response.body)["count"]
          refute ticket_count
        end
      end
    end

    describe "ticket audit index query" do
      it 'should force the index for the audits query' do
        audits_sql = @controller.send(:ticket_audits).to_sql
        assert_equal 1, audits_sql.scan(/force index \(index_account_id_created_at\)/).size
      end

      describe_with_arturo_disabled :ticket_audit_force_index do
        it 'should not force index for audit query' do
          audits_sql = @controller.send(:ticket_audits).to_sql
          assert_equal 1, audits_sql.scan(/use index \(index_account_id_created_at\)/).size
        end
      end
    end

    describe "index with cursor param" do
      describe "cursor value is not valid Base64" do
        before do
          @add_params = { cursor: "2018-05-02T02:14:38Z" }
        end

        it "should return bad_request" do
          get :index, params: {}.merge(@add_params)
          assert_equal 400, @response.status
        end
      end

      describe "decoded cursor value is not of the correct type" do
        before do
          @add_params = { cursor: "YmFzZTY0" }
        end

        it "should return bad_request" do
          get :index, params: {}.merge(@add_params)
          assert_equal 400, @response.status
        end
      end
    end

    describe "index with voice comments" do
      before do
        @ticket = @account.tickets.first

        recording_sid = "RE5d4706b2a7ce9c6085119352bc3f96c"
        recording_url = "http://api.twilio.com/2010-04-01/Accounts/AC987414f141979a090af362442ae4b864/Recordings/#{recording_sid}"
        call = mock('Call')
        call_id = 7890
        agent = users(:minimum_agent)

        call.stubs(:voice_comment_data).returns(
          recording_url: recording_url,
          recording_duration: "7",
          transcription_text: "foo: hi\nbar: bye\n",
          transcription_status: "completed",
          answered_by_id: agent.id,
          call_id: call_id,
          from: '+11234567890',
          to: '+11234567890',
          started_at: 'June 04, 2014 05:10:27 AM',
          outbound: false
        )
        call.stubs(:account_id).returns(@account.id)
        @ticket.add_voice_comment(call,
          author_id: agent.id,
          body: "Call recording")
        @ticket.will_be_saved_by(agent)

        @ticket.save!

        call_2 = mock('Call 2')
        call_2_id = 7891
        agent_2 = users(:minimum_admin)

        call_2.stubs(:voice_comment_data).returns(
          recording_url: recording_url,
          recording_duration: "7",
          transcription_text: "foo: hi\nbar: bye\n",
          transcription_status: "completed",
          answered_by_id: agent_2.id,
          call_id: call_2_id,
          from: '+11234567890',
          to: '+11234567890',
          started_at: 'June 04, 2014 05:10:27 AM',
          outbound: false
        )
        call_2.stubs(:account_id).returns(@account.id)
        @ticket.add_voice_comment(call_2,
          author_id: agent_2.id,
          body: "Call recording")
        @ticket.will_be_saved_by(agent_2)

        @ticket.save!
      end

      it "does not do n+1 queries when loading voice comments" do
        # One of these is not an n+1 - it is preloads on the notifications_with_ccs associations on audits
        assert_n_plus_ones 1 do
          # We need to disabled Kasket because it can cache the users for an account
          # (depending on the number of users) and hide the issue: n+1 requests to users
          Account.without_kasket do
            get :index, params: {}.merge(@add_params || {})
          end
        end
      end
    end

    describe "index with deleted tickets" do
      it "should not return ticket audits" do
        original_count = @account.ticket_audits.count
        t = @account.tickets.first
        t.will_be_saved_by(@account.owner)
        t.soft_delete!

        get :index
        ticket_count = JSON.parse(@response.body)["count"]

        refute_equal original_count, ticket_count.to_i
      end
    end

    describe "index with cursor pagination params" do
      # eg. [5, 4, 3, 2]
      let(:ids) { @account.ticket_audits.order('id desc').pluck(:id) }
      let(:timestamps) { @account.ticket_audits.order('created_at desc').pluck(:created_at).map(&:to_f) }

      it "should have cursor pagination attrs" do
        get :index, params: { limit: 10 }
        assert_response :ok
        response = JSON.parse(@response.body)
        %w[after_cursor after_url before_cursor before_url].each do |attr|
          v = response.fetch(attr)
          assert v.present?, "Key <#{attr}> is empty"
        end
      end

      it "should work with limit" do
        get :index, params: { limit: 2 }
        assert_response :ok
        assert_equal 2, JSON.parse(@response.body)['audits'].size
      end

      it "shows first page with equal timestamps" do
        assert_equal 1, timestamps.uniq.size

        get :index, params: { limit: 2 }
        assert_response :ok
        json = JSON.parse(@response.body)['audits']

        assert_equal ids[0], json[0]['id']
        assert_equal ids[1], json[-1]['id']

        assert_after_cursor([timestamps[0], nil, ids[0], nil], limit: 2)
        # Same timestamp because they are the same
        assert_before_cursor([nil, timestamps[0], nil, ids[1]], limit: 2)
      end

      it "shows empty collection on after first page" do
        get :index, params: { limit: 2, cursor: make_cursor([timestamps[0], nil, ids[0], nil]) }
        assert_response :ok
        json = JSON.parse(@response.body)['audits']

        assert_empty(json)

        assert_nil after_cursor
        assert_nil after_url

        assert_before_cursor([nil, timestamps[0] + 1, nil, nil], limit: 2)
      end

      it "shows second page" do
        get :index, params: { limit: 2, cursor: make_cursor([nil, timestamps[0], nil, ids[1]]) }
        assert_response :ok
        json = JSON.parse(@response.body)['audits']

        assert_equal ids[2], json[0]['id']
        assert_equal ids[3], json[-1]['id']

        assert_after_cursor([timestamps[2], nil, ids[2], nil], limit: 2)
        # Same timestamp because they are the same
        assert_before_cursor([nil, timestamps[3], nil, ids[3]], limit: 2)
      end

      it "shows last page of results" do
        get :index, params: { limit: 2, cursor: make_cursor([nil, timestamps[0], nil, ids[-3]]) }
        assert_response :ok
        json = JSON.parse(@response.body)['audits']
        assert_equal 2, json.size

        assert_equal ids[-2], json[0]['id']
        assert_equal ids[-1], json[-1]['id']

        assert_before_cursor([nil, timestamps[0], nil, ids[-1]], limit: 2)
        assert_after_cursor([timestamps[0], nil, ids[-2], nil], limit: 2)
      end

      it "shows empty result before last page of results" do
        get :index, params: { limit: 2, cursor: make_cursor([nil, timestamps[0], nil, ids[-1]]) }
        assert_response :ok
        json = JSON.parse(@response.body)['audits']
        assert_equal 0, json.size

        assert_nil before_cursor
        assert_nil before_url

        assert_after_cursor([timestamps[0] - 1, nil, nil, nil], limit: 2)
      end

      describe "when ids are not sequential" do
        before do
          # Takes the event with the smallest id (ie. created_first) and gives
          # it the highest created_at
          Event.find(ids[-1]).update_column :created_at, Time.at(timestamps[0]) + 1.year

          @timestamps = @account.ticket_audits.order('created_at desc').pluck(:created_at).map(&:to_f)
        end

        it "shows the right results" do
          assert_equal 2, @timestamps.uniq.size

          get :index, params: { limit: 2 }
          assert_response :ok
          json = JSON.parse(@response.body)['audits']

          assert_equal ids[-1], json[0]['id']
          assert_equal ids[0], json[-1]['id']

          assert_after_cursor([@timestamps[0], nil, json[0]['id'], nil], limit: 2)
          assert_before_cursor([nil, @timestamps[1], nil, json[-1]['id']], limit: 2)
        end
      end
    end

    # For a complete set of tests, go to `zendesk_cursor_pagination` gem.
    describe 'using cursor based pagination v2' do
      let(:account) { accounts(:minimum) }
      let(:page_size) { 2 }
      let(:page_params) { { size: page_size } }
      let(:json) do
        get :index, params: { page: page_params }
        JSON.parse(@response.body).with_indifferent_access
      end

      it 'still uses v1 if cursor_pagination_v2_ticket_audits_endpoint arturo is disabled' do
        Account.any_instance.stubs(:has_cursor_pagination_v2_ticket_audits_endpoint?).returns(false)
        @controller.expects(:paginate_with_cursor).never
        json
      end

      it 'generates the correct order clause' do
        assert_sql_queries(1, /ORDER BY `events`.`created_at` DESC, `events`.`id` DESC/) do
          json
        end
      end

      describe 'when there is only one page' do
        let(:page_size) { account.ticket_audits.size + 1 }

        it 'presents the correct keys' do
          refute json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of tags' do
          assert_equal account.ticket_audits.size, json[:audits].size
        end
      end

      describe 'when there are multiple pages' do
        let(:page_size) { account.ticket_audits.size - 1 }

        it 'presents the correct keys' do
          assert json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of tags' do
          assert_equal page_size, json[:audits].size
        end
      end

      describe 'with a valid after cursor' do
        let(:page_size) { account.ticket_audits.size - 1 }
        let(:after_json) { JSON.parse(@response.body).with_indifferent_access }

        before do
          after_cursor = json[:meta][:after_cursor]
          # reset memoized ticket audits
          @controller.remove_instance_variable(:@ticket_audits)

          get :index, params: { page: {
            size: page_size,
            after: after_cursor
          } }
        end

        it 'presents the correct keys' do
          refute after_json[:meta][:has_more]
          assert after_json[:meta][:before_cursor].present?
          assert after_json[:meta][:after_cursor].present?
          assert after_json[:links][:prev].present?
          assert after_json[:links][:next].present?
        end

        it 'presents the correct number of tags' do
          assert_equal 1, after_json[:audits].size
        end
      end

      describe 'with an invalid after cursor' do
        let(:page_params) do
          {
            size: page_size,
            after: 'invalid'
          }
        end

        it 'presents an error message' do
          assert_equal 'InvalidPaginationParameter', json[:error]
          assert_equal 'page[after] is not valid', json[:description]
          assert_response :bad_request
        end
      end
    end

    describe '#ticket_audits' do
      it 'returns audits' do
        ary = @controller.send(:ticket_audits)
        assert_equal @account.ticket_audits.count, ary.size
      end

      it 'does not return audits from deleted tickets' do
        t = @account.tickets.first
        t.will_be_saved_by(@account.owner)
        t.soft_delete!

        ary = @controller.send(:ticket_audits)
        expected_count = @account.ticket_audits.select(&:ticket).size

        assert expected_count < @account.ticket_audits.count
        assert_equal expected_count, ary.size
      end

      it 'does not return audits from archived tickets' do
        original_count = @account.ticket_audits.count

        t = @account.tickets.first
        # Also archives audits
        archive_and_delete(t)

        ary = @controller.send(:ticket_audits)
        expected_count = @account.ticket_audits.select(&:ticket).size

        assert expected_count < original_count
        assert_equal expected_count, ary.size
      end
    end
  end

  %w[after_cursor before_cursor after_url before_url].each do |attr|
    define_method(attr) do
      @json_body ||= JSON.parse(@response.body)
      @json_body.fetch(attr)
    end
  end

  def make_url(ary, limit: nil)
    cursor = make_cursor(ary)
    res = "https://#{@request.host}/api/v2/ticket_audits.json"
    res += "?cursor=#{CGI.escape cursor}"
    res += "&limit=#{limit}" if limit
    res
  end

  def make_cursor(ary)
    Base64.urlsafe_encode64(ary.map { |e| e.nil? ? '' : e }.join('|'))
  end

  def assert_cursor(ary:, direction:, limit: nil)
    expected_cursor = make_cursor(ary)
    expected_url = make_url(ary, limit: limit)

    cursor_value = direction == :before ? before_cursor : after_cursor
    url_value = direction == :before ? before_url : after_url

    err = %(<"#{expected_cursor}"> expected but was <"#{cursor_value}=">.\nExpected decoded value: #{Base64.urlsafe_decode64(expected_cursor)}. Was #{Base64.urlsafe_decode64(cursor_value)})
    assert_equal expected_cursor, cursor_value, err
    assert_equal expected_url, url_value
  end

  def assert_after_cursor(ary, limit: nil)
    assert_cursor(ary: ary, direction: :after, limit: limit)
  end

  def assert_before_cursor(ary, limit: nil)
    assert_cursor(ary: ary, direction: :before, limit: limit)
  end
end
