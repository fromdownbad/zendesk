require_relative "../../../support/test_helper"
require_relative "../../../support/search_controller_test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::SearchController do
  include SearchControllerTestHelper
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :subscriptions, :users, :organizations, :tickets, :role_settings

  before do
    ZendeskSearch::Client.any_instance.stubs(:search).returns(count: 0, results: [])
    @controller.stubs(:request_span).returns(Datadog::Span.new(Datadog.tracer, 'test-span'))
    accept :json
  end

  with_options(controller: "api/v2/search") do |request|
    request.should_route :get, "/api/v2/search", action: "index"
    request.should_route :get, "/api/v2/search/incremental", action: "incremental"
    request.should_route :post, "/api/v2/search", action: "index"
  end

  as_an_agent do
    describe "a successful GET to :index" do
      before do
        Zendesk::ClassicStats::SearchStringFact.expects(:store).with(
          type: 'search', origin: 'api', format: 'json', string: 'Ticket', user_id: 57888, account_id: 90538, shard_id: 1
        )
        get :index, params: { query: 'Ticket', per_page: 100, sort_order: 'asc', sort_by: 'priority' }
      end

      it "does not set incremental option on the query handler" do
        refute assigns(:query).incremental
      end

      it "does not default to API_MAX_PER_PAGE" do
        assert_equal 100, @controller.send(:query).per_page
      end

      it "uses :sort_order to specify the sorting order" do
        assert_equal :asc, assigns(:query).sort_mode
      end

      it "uses :sort_by to specify the sorting field" do
        assert_equal :priority_id, assigns(:query).order
      end

      describe "disabling the article results" do
        before do
          @account.stubs(:help_center_enabled?).with(any_parameters).returns(true)
          Zendesk::Search.expects(:search).returns(Zendesk::Search::Results.create(1, 1, 1) {})
          Zendesk::ClassicStats::SearchStringFact.expects(:store).with(
            type: 'search', origin: 'api', format: 'json', string: 'Ticket', user_id: 57888, account_id: 90538, shard_id: 1
          )
          stub_request(:get, "https://#{@account.subdomain}.zendesk-test.com/hc/api/internal/users/57888/access/user_segments.json").
            to_return(status: 200, body: '{"user_segment_ids": [111,126]}', headers: {})
        end

        describe_with_arturo_disabled :exclude_articles_from_search_results do
          it "includes article results" do
            get :index, params: { query: 'Ticket', per_page: 100, sort_order: 'asc', sort_by: 'priority' }
            assert @controller.send(:query).include_helpcenter
          end
        end

        describe_with_arturo_enabled :exclude_articles_from_search_results do
          it "does not include article results" do
            get :index, params: { query: 'Ticket', per_page: 100, sort_order: 'asc', sort_by: 'priority' }
            refute @controller.send(:query).include_helpcenter
          end
        end
      end
    end

    describe "a GET to :index with new format of per-presenter sideloads" do
      it "sets mixed collection includes for sideloads" do
        get :index, params: { query: 'Ticket', per_page: 100, sort_order: 'asc', sort_by: 'priority', include: 'tickets(last_comment,comment_count,dates),topics(users)' }
        mixed_collection_includes = @controller.send(:mixed_collection_includes)
        assert_equal [:last_comment, :comment_count, :dates], mixed_collection_includes[Ticket]
        assert_equal [:users], mixed_collection_includes[Entry]
      end

      describe "with 'end user'" do
        before do
          login('minimum_end_user')
        end

        it "does not set mixed collection includes for sideloads when 'disabled_search_sideload_for_end_users' feature is enabled" do
          Arturo.enable_feature! :disabled_search_sideload_for_end_users
          get :index, params: { query: 'Ticket', per_page: 100, sort_order: 'asc', sort_by: 'priority', include: 'tickets(last_comment,comment_count,dates),topics(users)' }

          mixed_collection_includes = @controller.send(:mixed_collection_includes)
          assert_nil mixed_collection_includes[Ticket]
          assert_nil mixed_collection_includes[Entry]
        end

        it "sets mixed collection includes for sideloads" do
          Arturo.disable_feature! :disabled_search_sideload_for_end_users
          get :index, params: { query: 'Ticket', per_page: 100, sort_order: 'asc', sort_by: 'priority', include: 'tickets(last_comment,comment_count,dates),topics(users)' }

          mixed_collection_includes = @controller.send(:mixed_collection_includes)
          assert_equal [:last_comment, :comment_count, :dates], mixed_collection_includes[Ticket]
          assert_equal [:users], mixed_collection_includes[Entry]
        end
      end
    end

    describe "custom comment presenter options" do
      # This is to prevent regressions such as the ones reported in https://zendesk.atlassian.net/browse/MSS-154
      # See https://github.com/zendesk/zendesk/pull/26939 for more context.
      # Basically we make sure that the controller always passes the correct comments presenter to the results presenter
      # not the best but at least this will break if somebody for somebody reason removes the option.
      it "must always pass an instance of 'Ap::V2::Tickets::GenericCommentPresenter' to Api::V2::Search::ResultsPresenter' initializer" do
        results_presenter = mock
        results_presenter.stubs(:present).with(any_parameters).returns(true)
        Api::V2::Search::ResultsPresenter.expects(:new).with(anything, has_entry(:comment_presenter, instance_of(Api::V2::Tickets::GenericCommentPresenter))).returns(results_presenter)
        get :index, params: { query: 'Ticket', per_page: 100, sort_order: 'asc', sort_by: 'priority', include: 'tickets(first_comment,last_comment)' }
      end
    end

    describe "a successful GET to :index with archived tickets" do
      before do
        @account.tickets.map { |t| archive_and_delete(t) }

        results = @account.tickets.map { |t| {'id' => t.id.to_s, 'type' => 'ticket'} }

        ZendeskSearch::Client.any_instance.
          expects(:search).
          returns('count' => results.size, 'results' => results).
          at_least_once
      end

      it "does not do n+1 queries" do
        assert_n_plus_ones 1 do
          get :index, params: { query: 'Ticket', per_page: 100, sort_order: 'asc', sort_by: 'priority' }
        end
      end
    end

    describe "a GET to :index with an invalid query" do
      before do
        Zendesk::Search.expects(:search).raises(ZendeskSearch::QueryError)
        get :index, params: { query: 'hi' }
      end

      should_use_presenter Api::V2::Search::ErrorsPresenter, status: :unprocessable_entity
    end

    describe "a GET to :index with an empty query" do
      before do
        get :index
      end

      should_use_presenter Api::V2::Search::ErrorsPresenter, status: :unprocessable_entity
    end

    describe "a GET to :index with a query that times out" do
      before do
        Zendesk::Search.expects(:search).raises(Timeout::Error)
        get :index, params: { query: 'hi' }
      end

      should_use_presenter Api::V2::Search::ErrorsPresenter, status: :service_unavailable
    end

    describe "a GET to :index with a relevance sorting order" do
      before do
        Zendesk::Search.expects(:search).returns(Zendesk::Search::Results.create(1, 1, 1) {})
        get :index, params: { query: 'Ticket', sort_order: 'relevance' }
      end

      it('responds with success') { assert_response :success }
    end

    describe_with_arturo_enabled :search_return_limit_in_search_service do
      before do
        get :index, params: { query: 'type:ticket created>5minutes fieldvalue:feedback_type', per_page: nil, page: 6667 }
      end
      it('responds with success') { assert_response :success }
    end

    describe "with a pagination limit" do
      before do
        get :index, params: { query: 'type:ticket created>5minutes fieldvalue:feedback_type', per_page: nil, page: 6667 }
      end

      it('responds with Invalid search: Reached pagination limit') { assert_response :unprocessable_entity }
    end

    describe_with_arturo_enabled :search_short_circuit_limiter do
      before do
        get :index, params: { query: 'type: ticket created>5minutes', per_page: nil, page: 2 }
      end
      it('responds with Too many requests') { assert_response 429 }
    end

    describe 'with valid external domains' do
      before do
        Zendesk::ClassicStats::SearchStringFact.expects(:store).with(
          type: 'search', origin: 'api', format: 'json', string: 'Ticket', user_id: 57888, account_id: 90538, shard_id: 1
        )
      end

      it 'returns a valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
        get :index, params: { query: 'Ticket', per_page: 100, sort_order: 'asc', sort_by: 'priority' }
        assert_equal response.headers['Access-Control-Allow-Origin'], 'http://app.futuresimple.com'
      end

      it 'returns a not valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
        get :index, params: { query: 'Ticket', per_page: 100, sort_order: 'asc', sort_by: 'priority' }
        assert_nil response.headers['Access-Control-Allow-Origin']
      end
    end

    describe "a GET to :incremental" do
      describe "regular query" do
        before do
          expect_search('hi', match_percent: '100%', incremental: true, facets: true, per_page: 30, include_helpcenter: false, problem_bias: 0.6)

          get :incremental, params: { query: 'hi' }
        end

        it('responds with success') { assert_response :success }
        should_use_presenter Api::V2::Search::LotusResultsPresenter

        it "sets incremental option on the query handler" do
          assert assigns(:query).incremental
        end
      end

      describe "with a relevance sorting order" do
        before do
          Zendesk::Search.expects(:search).returns(Zendesk::Search::Results.create(1, 1, 1) {})
          get :incremental, params: { query: 'Ticket', sort_order: 'relevance' }
        end

        it('responds with success') { assert_response :success }
      end

      describe "with a pagination limit" do
        before do
          get :incremental, params: { query: 'Hi', per_page: nil, page: 6667 }
        end
        it('responds with Invalid search: Reached pagination limit') { assert_response :unprocessable_entity }
      end

      describe_with_arturo_disabled :search_cache_control_header do
        before { Zendesk::Search.expects(:search).returns(Zendesk::Search::Results.create(1, 1, 1) {}) }
        it "does not set cache-control headers" do
          get :incremental, params: { query: 'Ticket', sort_order: 'relevance' }
          assert_nil @response.headers['Cache-Control']
        end
      end

      describe_with_arturo_enabled :search_cache_control_header do
        before { Zendesk::Search.expects(:search).returns(Zendesk::Search::Results.create(1, 1, 1) {}) }
        it "sets cache-control max-age 5 headers for Lotus requests" do
          @controller.stubs(:lotus_internal_request?).returns(true)
          get :incremental, params: { query: 'Ticket', sort_order: 'relevance' }
          assert_equal @response.headers['Cache-Control'], 'max-age=5, private'
        end

        it "sets cache-control max-age 10 headers for non-Lotus requests" do
          @controller.stubs(:lotus_internal_request?).returns(false)
          get :incremental, params: { query: 'Ticket', sort_order: 'relevance' }
          assert_equal @response.headers['Cache-Control'], 'max-age=10, private'
        end
      end
    end

    describe "a POST to :index" do
      describe "with the :api_allow_post_in_search flag" do
        before do
          Arturo.enable_feature!(:api_allow_post_in_search)
          expect_search('Ticket', per_page: 100, page: 1, sort_mode: :relevance,
                                  type: nil, include_helpcenter: false)
          post :index, params: { query: 'Ticket' }
        end

        it('responds with success') { assert_response :success }
      end

      describe "without the :api_allow_post_in_search flag" do
        before do
          post :index, params: { query: 'Ticket' }
        end

        it('responds with not_found') { assert_response :not_found }
      end
    end

    describe "a GET to :count" do
      before do
        Zendesk::Search.expects(:search).
          with(anything, 'Ticket', has_entry(endpoint: 'things/count')).
          returns(Zendesk::Search::Results.create(1, 1, 1) {})
        get :count, params: { query: 'Ticket', sort_order: 'relevance' }
      end

      it('responds with success') { assert_response :success }

      describe "disabling the article results" do
        before do
          @account.stubs(:help_center_enabled?).with(any_parameters).returns(true)
          Zendesk::Search.expects(:search).
            with(anything, 'Ticket', has_entry(endpoint: 'things/count')).
            returns(Zendesk::Search::Results.create(1, 1, 1) {})
          Zendesk::ClassicStats::SearchStringFact.expects(:store).with(
            type: 'search', origin: 'api', format: 'json', string: 'Ticket', user_id: 57888, account_id: 90538, shard_id: 1
          )
          stub_request(:get, "https://#{@account.subdomain}.zendesk-test.com/hc/api/internal/users/57888/access/user_segments.json").
            to_return(status: 200, body: '{"user_segment_ids": [111,126]}', headers: {})
        end

        describe_with_arturo_disabled :exclude_articles_from_search_results do
          it "includes article results" do
            get :count, params: { query: 'Ticket', sort_order: 'relevance' }
            assert @controller.send(:query).include_helpcenter
          end
        end

        describe_with_arturo_enabled :exclude_articles_from_search_results do
          it "does not include article results" do
            get :count, params: { query: 'Ticket', sort_order: 'relevance' }
            refute @controller.send(:query).include_helpcenter
          end
        end
      end
    end

    describe "a GET to :count, force hl=false" do
      before do
        Zendesk::Search.expects(:search).
          with(anything, 'Ticket', has_entries(endpoint: 'things/count', hl: false)).
          returns(Zendesk::Search::Results.create(1, 1, 1) {})
        get :count, params: { query: 'Ticket', sort_order: 'relevance', include: 'highlights' }
      end

      it('responds with success') { assert_response :success }
    end

    describe_with_arturo_enabled :search_grant_access_export_api do
      describe "a successful GET to :export" do
        before do
          Zendesk::Search.expects(:search).
            with(anything, 'export', has_entry(endpoint: 'export/things')).
            returns(Zendesk::Search::CBPResults.new([], {}))
          get :export, params: { query: 'export', filter: { type: 'export' }, page: { size: '1' } }
        end

        it('responds with success') { assert_response :success }
      end

      describe "a GET to :export with missing filter[type] param" do
        before do
          Zendesk::Search.expects(:search).raises(ZendeskSearch::QueryError)
          get :export, params: { query: 'export', page: { size: '1000' } }
        end

        should_use_presenter Api::V2::Search::ErrorsPresenter, status: :unprocessable_entity
      end

      describe "a GET to :export with missing page[size] and page[after] params" do
        before do
          Zendesk::Search.expects(:search).
            with(anything, 'export', has_entry(endpoint: 'export/things')).
            returns(Zendesk::Search::CBPResults.new([], {})) # needs to be CBP
          get :export, params: { query: 'export', filter: { type: 'export' } }
        end

        it('responds with success') { assert_response :success }
      end

      describe "a GET to :export with page[size] that exceeds limit" do
        before do
          Zendesk::Search.expects(:search).raises(ZendeskSearch::QueryError)
          get :export, params: { query: 'export', filter: { type: 'export' }, page: { size: '1001' } }
        end

        should_use_presenter Api::V2::Search::ErrorsPresenter, status: :unprocessable_entity
      end
    end

    describe_with_arturo_disabled :search_grant_access_export_api do
      before do
        get :export, params: { query: 'export', filter: { type: 'export' }, page: { size: '1' } }
      end

      it('responds with forbidden') { assert_response :forbidden }
    end
  end

  as_an_end_user do
    describe_with_arturo_enabled :end_user_search do
      describe "a successful GET to :index" do
        before do
          Zendesk::Search.expects(:search).returns(Zendesk::Search::Results.create(1, 1, 1) {})
        end

        it('responds with success') do
          get :index, params: { query: 'Ticket' }
          assert_response :success
        end

        describe_with_arturo_disabled :search_cache_control_header do
          it "does not set cache-control headers" do
            get :index, params: { query: 'Ticket' }
            assert_nil @response.headers['Cache-Control']
          end
        end

        describe_with_arturo_enabled :search_cache_control_header do
          it "sets cache-control max-age 5 headers for Lotus requests" do
            @controller.stubs(:lotus_internal_request?).returns(true)
            get :index, params: { query: 'Ticket' }
            assert_equal @response.headers['Cache-Control'], 'max-age=5, private'
          end

          it "sets cache-control max-age 10 headers for non-Lotus requests" do
            @controller.stubs(:lotus_internal_request?).returns(false)
            get :index, params: { query: 'Ticket' }
            assert_equal @response.headers['Cache-Control'], 'max-age=10, private'
          end
        end
      end
    end

    describe_with_arturo_disabled :end_user_search do
      should_be_forbidden :index
    end

    should_be_forbidden :incremental
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :incremental, :export
  end

  ['zopim', 'knowledge_api'].each do |subsystem_user|
    as_a_subsystem_user(user: subsystem_user, account: :minimum) do
      should_be_authorized { [:index, :export] }

      describe "a successful GET to :index" do
        before do
          Zendesk::Search.expects(:search).returns(Zendesk::Search::Results.create(1, 1, 1) {})
          get :index, params: { query: 'Ticket', per_page: 100, sort_order: 'asc', sort_by: 'priority' }
        end
        it('responds with success') { assert_response :success }
      end

      describe "a successful GET to :export" do
        before do
          Arturo.enable_feature!(:search_grant_access_export_api)
          Zendesk::Search.expects(:search).returns(Zendesk::Search::CBPResults.new([], {}))
          get :export, params: { query: 'export', filter: { type: 'export' }, page: { size: '1' } }
        end
        it('responds with success') { assert_response :success }
      end
    end
  end
end
