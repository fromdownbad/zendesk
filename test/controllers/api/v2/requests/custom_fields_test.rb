require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered! file: 'app/controllers/api/v2/requests_controller.rb'

# Split from test/controllers/api/v2/requests_controller_test.rb to speed up test suite runs
describe Api::V2::RequestsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper

  fixtures :accounts, :tickets, :users

  let(:account) { accounts(:minimum) }

  before do
    Rails.logger.stubs(:info)
    accept :json
  end

  with_options(controller: "api/v2/requests") do |request|
    request.should_route :post, "/api/v2/requests", action: :create
  end

  as_an_anonymous_user do
    describe "a POST to #create" do
      describe "validates custom fields" do
        let(:account)      { accounts(:minimum) }
        let(:ticket_field) { FactoryBot.create(:field_tagger, is_required: true, is_required_in_portal: true, account: account) }
        let(:ticket_form)  { Zendesk::TicketForms::Initializer.new(account, ticket_form: { ticket_field_ids: [ticket_field.id], position: 1, default: true, name: "foo", in_all_brands: true }).ticket_form }

        before do
          Arturo.enable_feature!(:help_center_agent_requests)
          ticket_form.save!
        end

        it "returns error with required ticket_field" do
          post :create, params: { request: {
            ticket_form_id: ticket_form.id,
            subject: "Help!",
            comment: { value: "I need somebody!" },
            requester: { email: "drhenner@yahoo.com", name: "Drhenner", locale_id: 1 }
          } }

          new_ticket = JSON.parse(@response.body)

          assert !!new_ticket["error"]
          assert_equal ticket_field.id, new_ticket["details"]["base"].first["field_key"]
          assert new_ticket["details"]["base"].first["description"].include?('cannot be blank')
        end

        describe "with agent_as_end_user_allow_anonymous_via enabled" do
          before do
            Arturo.enable_feature!(:agent_as_end_user_allow_anonymous_via)
          end

          [
            ["source.rel=help_center", { rel: "help_center" }],
            ["HELPCENTER via type", ViaType.HELPCENTER]
          ].each do |(description, source)|
            describe "with #{description} where requester email is an agent's email" do
              it "returns error with required ticket_field" do
                post :create, params: { request: {
                                         ticket_form_id: ticket_form.id,
                                         subject: "Help!",
                                         comment: { value: "I need somebody!" },
                                         requester: { email: @account.owner.email, name: "Drhenner", locale_id: 1 },
                                         via: { channel: "web", source: source }
                                       } }

                new_ticket = JSON.parse(@response.body)

                assert !!new_ticket["error"]
                assert_equal ticket_field.id, new_ticket["details"]["base"].first["field_key"]
                assert new_ticket["details"]["base"].first["description"].include?('cannot be blank')
              end
            end

            describe "with #{description} where requester email is not associated with an existing user" do
              it "also returns error with required ticket_field" do
                post :create, params: { request: {
                                         ticket_form_id: ticket_form.id,
                                         subject: "Help!",
                                         comment: { value: "I need somebody!" },
                                         requester: { email: "#{Time.now.to_i}@example.com", name: "Drhenner", locale_id: 1 },
                                         via: { channel: "web", source: source }
                                       } }

                new_ticket = JSON.parse(@response.body)

                assert !!new_ticket["error"]
                assert_equal ticket_field.id, new_ticket["details"]["base"].first["field_key"]
                assert new_ticket["details"]["base"].first["description"].include?('cannot be blank')
              end
            end
          end

          describe "with no via and where requester email is an agent's email" do
            it "is valid" do
              post :create, params: { request: {
                                       ticket_form_id: ticket_form.id,
                                       subject: "Help!",
                                       comment: { value: "I need somebody!" },
                                       requester: { email: @account.owner.email, name: "Drhenner", locale_id: 1 }
                                     } }

              assert_response 201
            end
          end
        end

        describe "with agent_as_end_user_allow_anonymous_via disabled" do
          before do
            Arturo.disable_feature!(:agent_as_end_user_allow_anonymous_via)
          end

          [
            ["source.rel=help_center", { rel: "help_center" }],
            ["HELPCENTER via type", ViaType.HELPCENTER]
          ].each do |(description, source)|
            # This is the existing behavior that was the case prior to AEU changes.
            # When anonymous requests are made to /api/v2/requests, `via` has always been ignored.
            describe "with #{description} where requester email is an agent's email" do
              it "is valid" do
                post :create, params: { request: {
                                         ticket_form_id: ticket_form.id,
                                         subject: "Help!",
                                         comment: { value: "I need somebody!" },
                                         requester: { email: @account.owner.email, name: "Drhenner", locale_id: 1 },
                                         via: { channel: "web", source: source }
                                       } }

                assert_response 201
              end
            end

            describe "with #{description} where requester email is not associated with an existing user" do
              it "returns error with required ticket_field" do
                post :create, params: { request: {
                                         ticket_form_id: ticket_form.id,
                                         subject: "Help!",
                                         comment: { value: "I need somebody!" },
                                         requester: { email: "#{Time.now.to_i}@example.com", name: "Drhenner", locale_id: 1 },
                                         via: { channel: "web", source: source }
                                       } }

                new_ticket = JSON.parse(@response.body)

                assert !!new_ticket["error"]
                assert_equal ticket_field.id, new_ticket["details"]["base"].first["field_key"]
                assert new_ticket["details"]["base"].first["description"].include?('cannot be blank')
              end
            end
          end

          describe "with no via and where requester email is an agent's email" do
            it "is valid" do
              post :create, params: { request: {
                                       ticket_form_id: ticket_form.id,
                                       subject: "Help!",
                                       comment: { value: "I need somebody!" },
                                       requester: { email: @account.owner.email, name: "Drhenner", locale_id: 1 }
                                     } }

              assert_response 201
            end
          end
        end
      end
    end
  end

  as_an_agent do
    describe "validates custom fields" do
      let(:account)      { accounts(:minimum) }
      let(:ticket_field) { FactoryBot.create(:field_tagger, is_required: true, is_required_in_portal: true, account: account) }
      let(:ticket_form)  { Zendesk::TicketForms::Initializer.new(account, ticket_form: { ticket_field_ids: [ticket_field.id], position: 1, default: true, name: "foo", in_all_brands: true }).ticket_form }

      before do
        Arturo.enable_feature!(:help_center_agent_requests)
        ticket_form.save!
      end

      [
        ["source.rel", { rel: "help_center" }],
        ["HELPCENTER via type", ViaType.HELPCENTER]
      ].each do |(description, source)|
        describe "with #{description}" do
          it "returns error with required ticket_field" do
            post :create, params: { request: {
                                     ticket_form_id: ticket_form.id,
                                     subject: "Help!",
                                     comment: { value: "I need somebody!" },
                                     requester: { email: "drhenner@yahoo.com", name: "Drhenner", locale_id: 1 },
                                     via: { channel: "web", source: source }
                                   } }

            new_ticket = JSON.parse(@response.body)

            assert !!new_ticket["error"]
            assert_equal ticket_field.id, new_ticket["details"]["base"].first["field_key"]
            assert new_ticket["details"]["base"].first["description"].include?('cannot be blank')
          end
        end
      end
    end
  end
end
