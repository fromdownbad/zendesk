require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"
require_relative "../../../../support/collaboration_settings_test_helper"
require_relative "../../../../support/agent_test_helper"

SingleCov.covered! file: 'app/controllers/api/v2/requests_controller.rb'

# Tests for email CCs, collaborators, and followers
# Split from test/controllers/api/v2/requests_controller_test.rb to speed up test suite runs
describe Api::V2::RequestsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  include AgentTestHelper

  fixtures :accounts, :tickets, :users, :role_settings, :recipient_addresses

  let(:account) { accounts(:minimum) }

  before do
    Rails.logger.stubs(:info)
    accept :json
  end

  with_options(controller: "api/v2/requests") do |request|
    request.should_route :post, "/api/v2/requests", action: :create
    request.should_route :get, "/api/v2/requests/open", action: :open
    request.should_route :put, "/api/v2/requests/1", action: :update, id: 1
  end

  as_an_end_user do
    describe "with verified email identities" do
      let(:ticket) { tickets(:minimum_2) }

      describe "on a POST to #create" do
        describe "with collaborators" do
          subject do
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, collaborators: [@user.email.to_s] } }
            assert_response :created, @response.body
          end

          before do
            @user = users(:minimum_end_user)
            account.reload
          end

          describe "uses the correct presenter" do
            before { subject }

            should_use_presenter Api::V2::RequestPresenter, status: :created
          end

          describe "when collaborators_addable_only_by_agents setting is false" do
            describe "with legacy CCs setting enabled" do
              before do
                CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
                enable_account_legacy_collaboration(true)
              end

              it "creates a ticket with collaborators" do
                subject
                collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                assert_equal [@user.id], collaborator_ids
              end
            end

            describe "with CCs/Followers settings enabled" do
              before do
                CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
                enable_account_legacy_collaboration(false)
              end

              it "creates a ticket with collaborators" do
                subject
                collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                assert_equal [@user.id], collaborator_ids
              end
            end
          end

          describe "when collaborators_addable_only_by_agents setting is true" do
            let(:account) do
              set_account_collaborators_addable_only_by_agents
            end

            describe "with legacy CCs setting enabled" do
              before do
                CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
                enable_account_legacy_collaboration(true)
              end

              it "creates a ticket without collaborators" do
                subject
                collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                assert_equal [], collaborator_ids
              end
            end

            describe "with CCs/Followers settings enabled" do
              before do
                CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
                enable_account_legacy_collaboration(false)
              end

              it "creates a ticket with collaborators" do
                subject
                collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                assert_equal [@user.id], collaborator_ids
              end
            end
          end
        end

        describe "with email CCs" do
          subject do
            post :create, params: { request: {
              subject: "Help!",
              comment: { value: "I need somebody!" },
              email_ccs: [{ user_email: users(:minimum_end_user).email }]
            } }
            assert_response :created, @response.body
          end

          let(:email_cc_ids) { JSON.parse(@response.body)["request"]["email_cc_ids"] }

          describe "when email CCs are enabled" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                email_ccs_setting: true
              ).apply

              account.reload
            end

            describe "uses the correct presenter" do
              before { subject }

              should_use_presenter Api::V2::RequestPresenter, status: :created
            end

            describe "when collaborators_addable_only_by_agents setting is false" do
              it "creates a ticket with email CCs" do
                subject
                assert_equal [@user.id], email_cc_ids
              end
            end

            describe "when collaborators_addable_only_by_agents setting is true" do
              let(:account) do
                set_account_collaborators_addable_only_by_agents
              end

              it "creates a ticket with email CCs" do
                subject
                assert_equal [@user.id], email_cc_ids
              end
            end
          end

          describe "when email CCs are disabled" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                email_ccs_setting: false
              ).apply
              subject
            end

            should_use_presenter Api::V2::RequestPresenter, status: :created

            it "creates a ticket without email CCs" do
              assert_empty email_cc_ids
            end
          end
        end
      end

      describe "on a GET to #show" do
        describe "email_cc_ids" do
          subject do
            get :show, params: { id: ticket.nice_id }
            JSON.parse(@response.body)["request"]["email_cc_ids"]
          end

          before do
            ticket.collaborations << Collaboration.new(
              collaborator_type: CollaboratorType.EMAIL_CC,
              user: users(:minimum_end_user)
            )

            ticket.collaborations << Collaboration.new(
              collaborator_type: CollaboratorType.LEGACY_CC,
              user: users(:minimum_agent)
            )

            ticket.will_be_saved_by(users(:minimum_admin))
            ticket.save!
          end

          describe "when follower_and_email_cc_collaborations is enabled" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true
              ).apply
            end

            describe "when comment_email_ccs is enabled" do
              it "should contain user ids for Email CC collaborations" do
                Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)

                assert_equal 1, subject.size
              end
            end

            describe "when comment_email_ccs is disabled" do
              it "should be an empty array" do
                Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)

                assert_empty subject
              end
            end
          end

          describe "when follower_and_email_cc_collaborations is disabled" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: false
              ).apply
            end

            it "should contain user ids for all collaborations" do
              assert_equal 2, subject.size
            end
          end
        end
      end

      describe "on a PUT to #update" do
        describe "with additional_collaborators" do
          subject do
            put :update, params: { id: ticket.nice_id, request: {
              comment: { body: "Hello!" },
              additional_collaborators: [users(:minimum_end_user).email]
            } }
          end

          before do
            ticket.set_collaborators = [users(:minimum_agent).id]
            ticket.will_be_saved_by(users(:minimum_admin))
            ticket.save!

            assert_equal 1, ticket.collaborators.size

            account.reload
          end

          describe "when collaborators_addable_only_by_agents setting is false" do
            describe "with legacy CCs setting enabled" do
              before do
                CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
                enable_account_legacy_collaboration(true)
              end

              it "adds collaborators" do
                subject
                assert_equal 2, ticket.collaborators.reload.size
              end
            end

            describe "with CCs/Followers settings enabled" do
              before do
                CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
                enable_account_legacy_collaboration(false)
              end

              it "adds collaborators" do
                subject
                assert_equal 2, ticket.reload.collaborators.size
              end
            end
          end

          describe "when collaborators_addable_only_by_agents setting is true" do
            let(:account) do
              set_account_collaborators_addable_only_by_agents
            end

            describe "with legacy CCs setting enabled" do
              before do
                CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
                enable_account_legacy_collaboration(true)
              end

              it "does not add collaborators" do
                subject
                assert_equal 1, ticket.collaborators.reload.size
              end
            end

            describe "with CCs/Followers settings enabled" do
              before do
                CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
                enable_account_legacy_collaboration(false)
              end

              it "adds collaborators" do
                subject
                assert_equal 2, ticket.reload.collaborators.size
              end
            end
          end
        end

        describe "with email CCs" do
          subject do
            put :update, params: { id: ticket.nice_id, request: {
              comment: { body: "Hello!" },
              email_ccs: [{ user_email: users(:minimum_end_user).email }]
            } }
          end

          let(:email_cc_ids) { JSON.parse(@response.body)["request"]["email_cc_ids"] }

          describe "with email CCs disabled" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                email_ccs_setting: false
              ).apply

              subject
            end

            it "does not add email CCs" do
              assert_equal 0, email_cc_ids.size
            end
          end

          describe "with email CCs enabled" do
            before do
              CollaborationSettingsTestHelper.new(
                feature_arturo: true,
                feature_setting: true,
                email_ccs_setting: true
              ).apply

              account.reload
            end

            describe "when collaborators_addable_only_by_agents setting is false" do
              it "adds email CCs" do
                subject
                assert_equal 1, email_cc_ids.size
              end
            end

            describe "when collaborators_addable_only_by_agents setting is true" do
              let(:account) do
                set_account_collaborators_addable_only_by_agents
              end

              it "adds email CCs" do
                subject
                assert_equal 1, email_cc_ids.size
              end
            end
          end
        end
      end
    end

    describe "with an unverified email identity" do
      let(:ticket) { tickets(:minimum_2) }
      let(:current_user) { users(:minimum_end_user) }
      let(:unverified_email_identity) do
        UserEmailIdentity.create(
          user: current_user,
          value: "EnduserUnverified@zendesk.com",
          account: account,
          is_verified: false
        )
      end

      before do
        unverified_email_identity
      end

      describe "on a POST to #create" do
        describe "with collaborators" do
          subject do
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, collaborators: [@user.email.to_s] } }
            assert_response :created, @response.body
          end

          before do
            @user = users(:minimum_end_user)

            account.reload
          end

          describe "uses the correct presenter" do
            before { subject }

            should_use_presenter Api::V2::RequestPresenter, status: :created
          end

          describe "when collaborators_addable_only_by_agents setting is false" do
            describe "with legacy CCs setting enabled" do
              before do
                CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
                enable_account_legacy_collaboration(true)
              end

              it "creates a ticket with collaborators" do
                subject
                collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                assert_equal [@user.id], collaborator_ids
              end
            end

            describe "with CCs/Followers settings enabled" do
              before do
                CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
                enable_account_legacy_collaboration(false)
              end

              it "creates a ticket with collaborators" do
                subject
                collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                assert_equal [@user.id], collaborator_ids
              end
            end
          end

          describe "when collaborators_addable_only_by_agents setting is true" do
            let(:account) do
              set_account_collaborators_addable_only_by_agents
            end

            describe "with legacy CCs setting enabled" do
              before do
                CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
                enable_account_legacy_collaboration(true)
              end

              it "creates a ticket without collaborators" do
                subject
                collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                assert_equal [], collaborator_ids
              end
            end

            describe "with CCs/Followers settings enabled" do
              before do
                CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
                enable_account_legacy_collaboration(false)
              end

              it "creates a ticket with collaborators" do
                subject
                collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                assert_equal [@user.id], collaborator_ids
              end
            end
          end
        end
      end

      describe "from a branded subdomain" do
        let(:brand) { FactoryBot.create(:brand, account_id: account.id) }
        let(:second_brand) { FactoryBot.create(:brand, account_id: account.id) }
        let(:branded_ticket) { @user.tickets.working.first }

        before do
          @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
          @controller.stubs(:current_brand).returns(brand)
          branded_ticket.will_be_saved_by(@user)
          branded_ticket.update_attribute(:brand, brand)
          branded_ticket.update_column(:is_public, true)
        end

        describe "on a POST to #create" do
          describe "with collaborators" do
            subject do
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, collaborators: [@user.email.to_s] } }
              assert_response :created, @response.body
            end

            before do
              @user = users(:minimum_end_user)

              account.reload
            end

            describe "uses the correct presenter" do
              before { subject }

              should_use_presenter Api::V2::RequestPresenter, status: :created
            end

            describe "when collaborators_addable_only_by_agents setting is false" do
              describe "with legacy CCs setting enabled" do
                before do
                  CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
                  enable_account_legacy_collaboration(true)
                end

                it "creates a ticket with collaborators" do
                  subject
                  collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                  assert_equal [@user.id], collaborator_ids
                end
              end

              describe "with CCs/Followers settings enabled" do
                before do
                  CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
                  enable_account_legacy_collaboration(false)
                end

                it "creates a ticket with collaborators" do
                  subject
                  collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                  assert_equal [@user.id], collaborator_ids
                end
              end
            end

            describe "when collaborators_addable_only_by_agents setting is true" do
              let(:account) do
                set_account_collaborators_addable_only_by_agents
              end

              describe "with legacy CCs setting enabled" do
                before do
                  CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
                  enable_account_legacy_collaboration(true)
                end

                it "creates a ticket without collaborators" do
                  subject
                  collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                  assert_equal [], collaborator_ids
                end
              end

              describe "with CCs/Followers settings enabled" do
                before do
                  CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
                  enable_account_legacy_collaboration(false)
                end

                it "creates a ticket with collaborators" do
                  subject
                  collaborator_ids = JSON.load(@response.body)["request"]["collaborator_ids"]
                  assert_equal [@user.id], collaborator_ids
                end
              end
            end
          end
        end
      end
    end
  end

  def self.describe_with_collaboration_settings_enabled(&block)
    describe 'when CCs/Followers settings are enabled' do
      before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

      class_eval(&block)
    end
  end

  describe_with_collaboration_settings_enabled do
    def self.test_requests_controller
      def post_create(request_body = {})
        post :create, params: { request: {
            subject: "Help!",
            comment: { value: "I need somebody!" }
          }.merge(request_body) }
        assert_response :created, @response.body
      end

      def put_update(request_body = {})
        put :update, params: { id: ticket.nice_id, request: {
            comment: {
              value: "I need somebody!"
            }
          }.merge(request_body) }
        assert_response :ok, @response.body
      end

      describe 'when creating a ticket' do
        describe 'with collaborators' do
          subject { post_create(collaborators: collaborators) }

          it 'creates a ticket without collaborators' do
            subject
            assert_empty collaborator_ids
          end
        end

        describe 'with email ccs' do
          subject { post_create(email_ccs: email_ccs) }

          it 'creates a ticket without email ccs' do
            subject
            assert_empty email_cc_ids
          end
        end
      end

      describe 'when updating a ticket' do
        before { ticket }

        describe 'with collaborators' do
          subject { put_update(additional_collaborators: collaborators) }

          it 'creates a ticket without collaborators' do
            subject
            assert_empty collaborator_ids
          end
        end

        describe 'with email ccs' do
          subject { put_update(email_ccs: email_ccs) }

          it 'creates a ticket without email ccs' do
            subject
            assert_empty email_cc_ids
          end
        end
      end
    end

    # NOTE: The arturo `email_restricted_agent_fix` is set to disabled because it is currently broken.
    # These specs should not be removed if this arturo is GA'd
    describe_with_arturo_disabled :email_restricted_agent_fix do
      let(:collaborators) { [users(:minimum_end_user).id, users(:minimum_agent).id] }
      let(:email_ccs) { [{ "user_id": users(:minimum_end_user).id }, { "user_id": users(:minimum_agent).id }] }
      let(:response_body) { JSON.parse(@response.body) }
      let(:collaborator_ids) { response_body["request"]["collaborator_ids"] }
      let(:email_cc_ids) { response_body["request"]["email_cc_ids"] }
      let(:ticket) do
        ticket_initializer = Zendesk::Tickets::Initializer.new(account,
          user,
          requester_id: user.id,
          ticket: { submitter: user,
                    subject: "Help",
                    comment: { public: false,
                               value: "I need help"}})
        ticket = ticket_initializer.ticket
        ticket.save!
        ticket
      end

      before { login(user) }

      describe 'as an agent who cannot comment publicly' do
        let(:user) { create_custom_role_agent(ticket_editing: false, comment_access: 'private') }

        test_requests_controller
      end

      describe 'as a light agent' do
        let(:user) { create_light_agent }

        test_requests_controller
      end
    end
  end

  private

  def set_account_collaborators_addable_only_by_agents
    account = accounts(:minimum)
    account.settings.collaborators_addable_only_by_agents = true
    account.save!
    account
  end

  def enable_account_legacy_collaboration(enabled)
    account = accounts(:minimum)
    account.settings.collaboration_enabled = enabled
    account.save!
    account
  end
end
