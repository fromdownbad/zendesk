require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered! file: 'app/controllers/api/v2/requests_controller.rb'

# Split from test/controllers/api/v2/requests_controller_test.rb to speed up test suite runs
describe Api::V2::RequestsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper

  fixtures :accounts, :tickets, :users

  let(:account) { accounts(:minimum) }
  let(:statsd_client) { @controller.send(:statsd_client) }

  before do
    ZendeskSearch::Client.any_instance.stubs(:search).returns(count: 0, results: [])
    Rails.logger.stubs(:info)
    accept :json
  end

  with_options(controller: "api/v2/requests") do |request|
    request.should_route :get, "/api/v2/requests/ccd", action: :ccd
  end

  describe "when using oauth tokens" do
    let(:ticket) { tickets(:minimum_2) }
    let(:new_comment) { {comment: {body: "New comment!"}} }
    let(:valid_params) do
      {
        subject: "Help!",
        comment: {value: "I need somebody!"}
      }
    end

    with_scopes('requests:read', 'read') do
      should_be_authorized { get :ccd }
    end

    with_scopes('read', 'write') do
      should_be_authorized { get :ccd }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :ccd
  end

  as_an_end_user do
    describe "with verified email identities" do
      let(:ticket) { tickets(:minimum_2) }

      describe "on a GET to #ccd" do
        describe "without extra parameters" do
          before { get :ccd }
          should_use_presenter Api::V2::RequestPresenter

          it "uses object pagination by default" do
            assert_response :ok
            requests_keys = JSON.parse(response.body).keys
            assert_equal 2, (requests_keys & ['next_page', 'previous_page']).size
          end
        end

        describe "with a valid status type filter" do
          it 'uses the supplied status type filter' do
            selected_requests = [stub, stub]
            @controller.expects(:ccd_tickets).with('new', 'open').returns(selected_requests)
            Api::V2::RequestPresenter.any_instance.expects(:present).with(selected_requests)

            get :ccd, params: { status: 'new,open' }
          end
        end

        describe "with an invalid status type filter" do
          it 'quietly reject invalid status types' do
            selected_requests = [stub, stub]
            @controller.expects(:ccd_tickets).with('new', 'open').returns(selected_requests)
            Api::V2::RequestPresenter.any_instance.expects(:present).with(selected_requests)

            get :ccd, params: { status: 'new,open' }
          end
        end

        describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it "uses the ccd_tickets scope" do
              @user.expects(:ccd_tickets).returns(@user.tickets).once
              @user.expects(:collaborated_tickets).never

              get :ccd
            end
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            it "returns an empty list" do
              @user.expects(:ccd_tickets).never
              @user.expects(:collaborated_tickets).never

              get :ccd
              assert_empty JSON.parse(@response.body)["requests"]
            end
          end
        end
      end
    end

    describe "on a GET to #search" do
      describe "without a query parameter" do
        let(:selected_requests) { [FactoryBot.create(:ticket)] }
        describe "with cc_id" do
          before { @controller.stubs(ccd_tickets: selected_requests) }

          it "returns tickets through the ccd_tickets method" do
            get :search, params: { cc_id: 'true' }
            assert_equal JSON.parse(@response.body)["requests"].map { |v| v["id"] }, selected_requests.map(&:nice_id)
          end
        end
      end
    end

    describe "with an unverified email identity" do
      let(:ticket) { tickets(:minimum_2) }
      let(:current_user) { users(:minimum_end_user) }
      let(:unverified_email_identity) do
        UserEmailIdentity.create(
          user: current_user,
          value: "EnduserUnverified@zendesk.com",
          account: account,
          is_verified: false
        )
      end

      before do
        unverified_email_identity
      end

      describe "on a GET to #ccd" do
        describe "without extra parameters" do
          before { get :ccd }

          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "with a valid status type filter" do
          before { get :ccd, params: { status: 'new,open' } }

          it('responds with forbidden') { assert_response :forbidden }
        end
      end
    end
  end
end
