require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::Requests::CommentsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :tickets, :events

  before do
    accept :json
  end

  with_options(controller: "api/v2/requests/comments") do |request|
    request.should_route :get, "/api/v2/requests/1/comments", action: :index, request_id: 1
    request.should_route :get, "/api/v2/requests/1/comments/1", action: :show, request_id: 1, id: 1
  end

  describe "when using oauth tokens" do
    let(:ticket) { tickets(:minimum_2) }

    with_scopes('requests:read', 'read') do
      should_be_authorized { get :index, params: { request_id: ticket.nice_id } }
      should_be_authorized { get :show, params: { request_id: ticket.nice_id, id: ticket.comments.first.id } }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, add_to_all: {request_id: 1}
  end

  as_an_end_user do
    let(:ticket) { tickets(:minimum_2) }

    describe "on a GET to #index" do
      describe "with an invalid ticket" do
        before { get :index, params: { request_id: tickets(:minimum_1).nice_id } }
        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "with a valid ticket" do
        before { get :index, params: { request_id: ticket.nice_id } }
        should_use_presenter Api::V2::Requests::CommentPresenter
      end

      describe "with sort_order" do
        before do
          Timecop.travel(2.minutes.from_now) do
            ticket.add_comment(body: 'ciao', is_public: true)
            ticket.will_be_saved_by(users(:minimum_agent))
            ticket.save!
          end
        end

        let(:older_comment) { ticket.comments.first }
        let(:newer_comment) { ticket.comments.last }

        it 'orders by created_at asc' do
          get :index, params: { request_id: ticket.nice_id, sort_order: 'asc', sort_by: 'created_at' }

          comments = JSON.parse(@response.body)['comments']

          assert_equal older_comment.id, comments.first["id"]
          assert_equal newer_comment.id, comments.last["id"]
        end

        it 'orders by created_at desc' do
          get :index, params: { request_id: ticket.nice_id, sort_order: 'desc', sort_by: 'created_at' }

          comments = JSON.parse(@response.body)['comments']

          assert_equal newer_comment.id, comments.first["id"]
          assert_equal older_comment.id, comments.last["id"]
        end
      end

      describe "with since" do
        before do
          Timecop.travel(2.days.from_now) do
            ticket.add_comment(body: 'ciao', is_public: true)
            ticket.will_be_saved_by(users(:minimum_agent))
            ticket.save!
          end
        end

        let(:older_comment) { ticket.comments.first }
        let(:newer_comment) { ticket.comments.last }

        it 'only includes comments equal and after given time' do
          get :index, params: { request_id: ticket.nice_id, since: (Time.now - 1.day).iso8601 }

          comments = JSON.parse(@response.body)['comments']
          refute comments.any? { |comment| comment['id'] == older_comment.id }, "Should not exist!"
        end
      end

      describe "with role" do
        before do
          ticket.add_comment(body: 'ciao', is_public: true)
          ticket.will_be_saved_by(users(:minimum_end_user))
          ticket.save!
        end

        it 'only includes comments belongs to `admin, agent, legacy agent` role' do
          get :index, params: { request_id: ticket.nice_id, role: 'agent' }

          users = JSON.parse(@response.body)['users']
          assert users.all? { |user| user['agent'] }, "All comments must be from agents!"
        end

        it 'only includes comments belongs to `end_user` role' do
          get :index, params: { request_id: ticket.nice_id, role: 'end_user' }

          users = JSON.parse(@response.body)['users']
          refute users.any? { |user| user['agent'] }, "All comments must be from end users!"
        end
      end

      describe "with since and role" do
        before do
          Timecop.travel(2.days.from_now) do
            ticket.add_comment(body: 'aiao', is_public: true)
            ticket.will_be_saved_by(users(:minimum_agent))
            ticket.save!
          end

          ticket.add_comment(body: 'eiao', is_public: true)
          ticket.will_be_saved_by(users(:minimum_end_user))
          ticket.save!
        end

        let(:older_comment) { ticket.comments.first }
        let(:newer_comment) { ticket.comments.last }

        it 'only includes comments equal and after given time' do
          get :index, params: { request_id: ticket.nice_id, since: (Time.now - 1.day).iso8601, role: 'agent' }

          comments = JSON.parse(@response.body)['comments']
          refute comments.any? { |comment| comment['id'] == older_comment.id }, "Should not exist!"
          assert users.all? { |user| user['agent'] }, "All comments must be from agents!"
        end

        it 'only includes comments equal and after given time' do
          get :index, params: { request_id: ticket.nice_id, since: (Time.now - 1.day).iso8601, role: 'end_user' }

          comments = JSON.parse(@response.body)['comments']
          refute comments.any? { |comment| comment['id'] == older_comment.id }, "Should not exist!"
          refute users.any? { |user| user['agent'] }, "All comments must be from end users!"
        end
      end

      describe "when end user has unverified emails" do
        let(:account) { accounts(:minimum) }
        let(:current_user) { users(:minimum_end_user) }

        before do
          UserEmailIdentity.create!(
            user: current_user,
            value: "EnduserUnverified@zendesk.com",
            account: account,
            is_verified: false
          )

          get :index, params: { request_id: ticket.nice_id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end
    end

    describe "on a GET to #show" do
      describe "with a valid comment" do
        before do
          Comment.without_arsi.update_all(is_public: true)
          get :show, params: { request_id: ticket.nice_id, id: ticket.comments.first.id }
        end

        should_use_presenter Api::V2::Requests::CommentPresenter
      end

      describe "with an invalid comment" do
        before do
          Comment.without_arsi.update_all(is_public: false)
          get :show, params: { request_id: ticket.nice_id, id: ticket.comments.first.id }
        end

        it('responds with not_found') { assert_response :not_found }
      end
    end
  end
end
