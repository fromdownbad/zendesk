require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered! file: 'app/controllers/api/v2/requests_controller.rb'

# Split from test/controllers/api/v2/requests_controller_test.rb to speed up test suite runs
describe Api::V2::RequestsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper

  fixtures :accounts, :tickets, :users

  let(:account) { accounts(:minimum) }
  let(:statsd_client) { @controller.send(:statsd_client) }

  before do
    ZendeskSearch::Client.any_instance.stubs(:search).returns(count: 0, results: [])
    Rails.logger.stubs(:info)
    accept :json
  end

  describe "when using oauth tokens" do
    let(:ticket) { tickets(:minimum_2) }
    let(:new_comment) { {comment: {body: "New comment!"}} }
    let(:valid_params) do
      {
        subject: "Help!",
        comment: {value: "I need somebody!"}
      }
    end

    with_scopes('requests:read', 'read') do
      should_be_authorized { get :search, params: { query: 'test' } }
    end

    with_scopes('read', 'write') do
      should_be_authorized { get :search, params: { query: 'test' } }
    end
  end

  as_an_end_user do
    describe "with verified email identities" do
      let(:ticket) { tickets(:minimum_2) }
      describe "on a GET to #search" do
        describe "in a multibrand account" do
          let(:brand) { FactoryBot.create(:brand, account_id: account.id) }

          before do
            @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
            @controller.stubs(:current_brand).returns(brand)
            get :search, params: { query: "camera" }
          end

          it "sets brand_id" do
            assert_equal assigns(:query).options[:with][:brand_id], brand.id
          end
        end

        describe "without extra parameters" do
          before do
            get :search, params: { query: "camera" }
          end

          it "sets field weights" do
            assert_equal(
              { nice_id_string: 100, subject: 100, comment: 50 },
              assigns(:query).options[:field_weights]
            )
          end

          it "sets requester_id" do
            assert_equal assigns(:query).options[:with][:requester_id], @user.id
          end

          it "sets is_public" do
            assert_equal assigns(:query).options[:with][:is_public], true
          end

          it "uses object pagination by default" do
            assert_response :ok
            requests_keys = JSON.parse(response.body).keys
            assert_equal 2, (requests_keys & ['next_page', 'previous_page']).size
          end
        end

        describe "setting an organization_id" do
          before { @user.stubs(end_user_viewable_organizations: @user.organizations) }

          it "sets the organization_id in the query" do
            get :search, params: { query: "camera", organization_id: @user.organizations.first.id }

            assert_equal assigns(:query).options[:with][:organization_id], @user.organizations.first.id
          end

          it "sets field weights" do
            get :search, params: { query: "camera", organization_id: @user.organizations.first.id }
            assert_equal(
              { nice_id_string: 100, subject: 100, comment: 50, requester_name: 100 },
              assigns(:query).options[:field_weights]
            )
          end
        end

        describe "setting a cc_id" do
          before { get :search, params: { query: "camera", cc_id: "true" } }

          it "sets the cc_id in the query" do
            assert_equal assigns(:query).options[:with][:cc_id], @user.id
          end

          it "sets field weights" do
            assert_equal(
              { nice_id_string: 100, subject: 100, comment: 50, requester_name: 100 },
              assigns(:query).options[:field_weights]
            )
          end
        end

        describe "with status parameter" do
          it "sets status_id in query" do
            get :search, params: { query: "camera", status: "open,closed" }
            assert_equal assigns(:query).options[:with][:status_id], [StatusType.OPEN, StatusType.CLOSED]
          end
        end

        describe "with ordering parameters" do
          it "sets sort_mode and order for the search query" do
            get :search, params: { query: "camera", sort_order: "desc", sort_by: "created_at" }
            assert_equal assigns(:query).options[:sort_mode], :desc
            assert_equal assigns(:query).options[:order], :created_at
          end
        end

        describe "without a query parameter" do
          let(:selected_requests) { [FactoryBot.create(:ticket)] }

          describe "without any extra parameters" do
            before { @controller.stubs(:tickets).returns(selected_requests) }
            before { get :search }

            it "returns tickets using the tickets method" do
              assert_equal JSON.parse(@response.body)["requests"].map { |v| v["id"] }, selected_requests.map(&:nice_id)
            end
          end

          describe "with organization_id" do
            before { @controller.stubs(:tickets).returns(selected_requests) }
            before { get :search, params: { organization_id: @user.organizations.first.id } }

            it "returns tickets using the tickets method" do
              assert_equal JSON.parse(@response.body)["requests"].map { |v| v["id"] }, selected_requests.map(&:nice_id)
            end
          end
        end
      end
    end

    describe "with an unverified email identity" do
      let(:ticket) { tickets(:minimum_2) }
      let(:current_user) { users(:minimum_end_user) }
      let(:unverified_email_identity) do
        UserEmailIdentity.create(
          user: current_user,
          value: "EnduserUnverified@zendesk.com",
          account: account,
          is_verified: false
        )
      end

      before do
        unverified_email_identity
      end

      describe "on a GET to #search" do
        describe "in a multibrand account" do
          let(:brand) { FactoryBot.create(:brand, account_id: account.id) }

          before do
            @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
            @controller.stubs(:current_brand).returns(brand)
            get :search, params: { query: "camera" }
          end

          it('responds with forbidden') { assert_response :forbidden }
        end
      end
    end
  end
end
