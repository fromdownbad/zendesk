require_relative "../../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::Zopim::AccountsController do
  extend Api::V2::TestHelper

  fixtures :accounts, :users, :role_settings

  before do
    accept :json
  end

  with_options(controller: "api/v2/internal/zopim/accounts") do |request|
    request.should_route :get, "/api/v2/internal/zopim/accounts/1", action: "show", id: "1"
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :show, { id: 1 }]
  end

  as_an_admin do
    should_be_forbidden [:get, :show, { id: 1 }]
  end

  as_an_agent do
    should_be_forbidden [:get, :show, { id: 1 }]
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    describe '#show' do
      describe 'when there is no zopim subscription with the given zopim account ID' do
        before do
          ZendeskBillingCore::Zopim::Subscription.
            expects(:find_by_zopim_account_id).with(1234).returns(nil)
          get :show, params: { id: 1234 }
        end

        it('responds with not found') do
          assert_response(:not_found)
        end
      end

      describe 'when there is a zopim subscription with the given zopim account ID' do
        before do
          ZendeskBillingCore::Zopim::Subscription.
            expects(:find_by_zopim_account_id).with(1234).
            returns(stub(account: stub(id: 9876)))
          get :show, params: { id: 1234 }
        end

        it('responds with success') do
          expected_response = {
            'account_id' => 9876
          }
          assert_equal expected_response, JSON.parse(@response.body)
        end

        should_use_presenter Api::V2::Internal::ZopimAccountPresenter, status: :ok
      end

      describe 'when the account is not present' do
        before do
          ZendeskBillingCore::Zopim::Subscription.
            expects(:find_by_zopim_account_id).with(1234).
            returns(stub(account: nil))
          get :show, params: { id: 1234 }
        end

        it('responds with not found') do
          assert_response(:not_found)
        end
      end
    end
  end
end
