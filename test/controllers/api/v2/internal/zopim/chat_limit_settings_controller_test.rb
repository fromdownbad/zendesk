require_relative "../../../../../support/test_helper"
require_relative "../../../../../support/zopim_test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"
require "zendesk/o_auth/testing/token_factory"

SingleCov.covered!

describe Api::V2::Internal::Zopim::ChatLimitSettingsController do
  extend Api::V2::TestHelper
  include ZopimTestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    accept :json

    @request.account = account

    setup_zopim_stubs
    setup_zopim_subscription(account)
  end

  with_options(controller: "api/v2/internal/zopim/chat_limit_settings") do |request|
    request.should_route :put, "/api/v2/internal/zopim/chat_limit_settings", action: "update"
  end

  as_an_anonymous_user do
    should_be_unauthorized [:put, :update]
  end

  as_an_admin do
    should_be_forbidden [:put, :update]
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    let(:current_user) { @controller.send(:current_user) }

    before do
      client = FactoryBot.create(:global_client, identifier: "zdg-zopim", user: current_user, account: account)
      token = FactoryBot.create(:token, global_client: client, user: nil, scopes: "zopim")
      @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token
    end

    describe 'params' do
      describe '#enabled' do
        it 'is required' do
          assert_raises ActiveRecord::StatementInvalid do
            put :update, params: { level: 'account', account_limit: 5 }
          end
        end
      end

      describe '#level' do
        describe 'when missing' do
          before do
            put :update, params: { enabled: 1, account_limit: 5 }
          end

          it('responds with succes status') { assert_response :ok }

          it 'defaults to account level' do
            assert_equal 5, account.zopim_subscription.account_chat_limit
          end
        end

        describe 'when "agent"' do
          before do
            put :update, params: { enabled: 1, level: 'agent' }
          end

          it('does not require account_limit param') { assert_response :ok }
        end

        describe 'when "account"' do
          before do
            put :update, params: { enabled: 1, level: 'account' }
          end

          it('does require account_limit param') { assert_response :unprocessable_entity }
        end

        describe 'when "foo"' do
          before do
            put :update, params: { enabled: 1, level: 'foo', account_limit: 5 }
          end

          it('responds with 422 status') { assert_response :unprocessable_entity }
        end
      end

      describe '#account_limit' do
        describe 'when negative' do
          before do
            put :update, params: { enabled: 1, level: 'account', account_limit: -1 }
          end

          it('responds with 422 status') { assert_response :unprocessable_entity }
        end

        describe 'when greater than 20' do
          before do
            put :update, params: { enabled: 1, level: 'account', account_limit: 21 }
          end

          it('responds with 422 status') { assert_response :unprocessable_entity }
        end
      end
    end

    describe 'update' do
      describe 'when enabling chat limits' do
        before do
          refute account.zopim_subscription.chat_limits_enabled
          put :update, params: { enabled: 1, level: 'account', account_limit: 5 }
        end

        it 'updates the zopim subscription' do
          assert account.zopim_subscription.chat_limits_enabled
        end
      end

      describe 'when chat limit level is account' do
        before do
          assert_equal 3, account.zopim_subscription.account_chat_limit
          put :update, params: { enabled: 1, level: 'account', account_limit: 5 }
        end

        it 'sets account_chat_limit to account_limit param value' do
          assert_equal 5, account.zopim_subscription.account_chat_limit
        end
      end

      describe 'when chat limit level is agent' do
        before do
          assert_equal 3, account.zopim_subscription.account_chat_limit
          put :update, params: { enabled: 1, level: 'agent', account_limit: 5 }
        end

        it 'sets account_chat_limit to 0' do
          assert_equal 0, account.zopim_subscription.account_chat_limit
        end
      end
    end
  end
end
