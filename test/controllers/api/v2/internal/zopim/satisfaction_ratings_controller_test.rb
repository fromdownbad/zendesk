require_relative "../../../../../support/test_helper"
require_relative "../../../../../support/satisfaction_test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"
require "zendesk/o_auth/testing/token_factory"

SingleCov.covered! uncovered: 1

describe Api::V2::Internal::Zopim::SatisfactionRatingsController do
  extend Api::V2::TestHelper
  include SatisfactionTestHelper
  fixtures :accounts, :tickets

  before do
    accept :json
    @account = accounts(:minimum)
    @request.account = @account
  end

  with_options(controller: "api/v2/internal/zopim/satisfaction_ratings") do |request|
    request.should_route :post, "/api/v2/internal/zopim/tickets/1/satisfaction_rating", action: "create", ticket_id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized [:post, :create, { ticket_id: 1 }]
  end

  as_an_admin do
    should_be_forbidden [:post, :create, { ticket_id: 1 }]
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    let(:ticket) { tickets(:minimum_4) }
    let(:rating) { { satisfaction_rating: { score: "bad", comment: "Terrible" }} }

    before do
      client = FactoryBot.create(:global_client, identifier: "zdg-zopim", user: @account.owner, account: @account)
      token = FactoryBot.create(:token, global_client: client, user: nil, scopes: "zopim")
      @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token
    end

    describe_with_arturo_setting_enabled :customer_satisfaction do
      describe 'when creating a satisfaction rating' do
        before { post :create, params: rating.merge(ticket_id: ticket.nice_id) }

        should_use_presenter Api::V2::SatisfactionRatingPresenter
        should_change("ratings", by: 1) { Satisfaction::Rating.count(:all) }

        describe "without a comment" do
          let(:rating) { { satisfaction_rating: { score: "good" }} }

          it "keeps the comment" do
            json = JSON.parse(@response.body)
            assert_nil json["satisfaction_rating"]["comment"]
          end

          it "uses the requester as the author on the audit" do
            assert_equal ticket.requester, ticket.audits.last.author
          end
        end

        describe "with an invalid score" do
          let(:rating) { { satisfaction_rating: { score: "HAHA" }} }
          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
        end

        describe "with no params" do
          let(:rating) { {} }
          it('responds with bad_request') { assert_response :bad_request }
        end
      end

      describe "when creating a second satisfaction rating" do
        let(:second_rating) { { satisfaction_rating: { score: "bad", comment: "Second sat rating" }} }
        let(:ticket_with_rating) { build_ticket_with_satisfaction_rating }

        before do
          assert_equal 1, ticket_with_rating.satisfaction_ratings.size
          assert_equal "I laughed. I cried. It was better than Cats.", ticket_with_rating.satisfaction_rating_comments.first.value

          post :create, params: second_rating.merge(ticket_id: ticket_with_rating.nice_id)
        end

        it "returns the more recent satisfaction rating" do
          assert_equal 2, ticket_with_rating.satisfaction_ratings.size

          json = JSON.parse(@response.body)
          assert_equal "Second sat rating", json["satisfaction_rating"]["comment"]
        end

        describe "and second rating has no comment" do
          let(:second_rating) { { satisfaction_rating: { score: "good", comment: "" }} }

          it "records the comment as an empty string" do
            json = JSON.parse(@response.body)
            assert_equal "", json["satisfaction_rating"]["comment"]
          end
        end
      end
    end

    describe_with_arturo_setting_disabled :customer_satisfaction do
      should_be_forbidden [:post, :create, { ticket_id: 1 }]
    end

    # TODO: This test fails as the response is a 400, 403 is expected
    # should_be_forbidden [:post, :create, { ticket_id: 1 }]
  end
end
