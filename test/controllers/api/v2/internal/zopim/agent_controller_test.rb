require_relative '../../../../../support/test_helper'
require_relative '../../../../../support/zopim_test_helper'
require_relative '../../../../../support/entitlement_test_helper'
require_relative '../../../../../support/suite_test_helper'

SingleCov.covered!

describe Api::V2::Internal::Zopim::AgentController do
  include ZopimTestHelper
  include EntitlementTestHelper
  include SuiteTestHelper

  fixtures :all

  let(:patch_status) { 200 }

  before do
    Account.any_instance.stubs(has_chat_permission_set?: true)
    ZopimIntegration.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      ensure_not_phase_four: nil
    )
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)

    Zopim::Reseller.client.stubs(
      account!: stub(trial_end_date: 30.days.from_now),
      create_account!: Hashie::Mash.new(id: 1),
      account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
      find_accounts!: []
    )

    accept :json

    stub_staff_service_patch_request('minimum', patch_status)
  end

  with_options(controller: 'api/v2/internal/zopim/agent') do |request|
    request.should_route :post, '/api/v2/internal/zopim/agent/transfer_ownership', action: 'transfer_ownership'
    request.should_route :delete, '/api/v2/internal/zopim/agent', action: 'destroy'
    request.should_route :put, '/api/v2/internal/zopim/agent/downgrade', action: 'downgrade'
  end

  as_a_subsystem_user(account: :minimum) do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }

    describe "when there is a chat-account" do
      before do
        setup_zopim_subscription(current_account)
      end

      let(:current_zopim_subscription) do
        current_account.zopim_subscription
      end

      describe "#transfer_ownership" do
        describe "when the recipient is eligible" do
          let(:recipient) do
            current_account.admins.detect { |admin| admin.zopim_identity.blank? }
          end

          before do
            Zopim::Reseller.client.stubs(create_account_agent!: stub(id: 1234))
            Zopim::Agent.stubs(:transfer_ownership!).with(anything, recipient).returns(true)
          end

          describe "and it is not yet a zopim-agent" do
            before do
              post :transfer_ownership, params: { user_id: recipient.id }
            end

            it('responds with success') { assert_response :success }
          end

          describe "and it is already a zopim-agent" do
            before do
              recipient.create_zopim_identity!
              post :transfer_ownership, params: { user_id: recipient.id }
            end

            it('responds with success') { assert_response :success }
          end
        end

        describe "when the recipient is not eligible" do
          describe "because it is not a zendesk-admin" do
            let(:recipient) do
              current_account.agents.detect { |agents| !agents.is_admin? }
            end

            before do
              post :transfer_ownership, params: { user_id: recipient.id }
            end

            it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
          end

          describe "because it is already an owner-agent" do
            let(:recipient) do
              current_account.zopim_subscription.owner_agent.user
            end

            before do
              post :transfer_ownership, params: { user_id: recipient.id }
            end

            it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
          end
        end

        describe "when the Zendesk/Zopim agents fell out of sync" do
          let(:recipient) do
            current_account.admins.detect { |admin| admin.zopim_identity.blank? }
          end

          before do
            Zopim::Agent.stubs(agents_in_sync?: false)
          end

          it "respond with 422" do
            post :transfer_ownership, params: { user_id: recipient.id }
            assert_response :unprocessable_entity
          end
        end
      end

      describe '#destroy' do
        before do
          Zopim::Agent.stubs(:destroy_agents).with(anything).returns(true)
        end

        it "responds with 204" do
          delete :destroy
          assert_response :no_content
        end
      end

      describe '#downgrade' do
        before do
          Zopim::Reseller.client.stubs(create_account_agent!: stub(id: 1234))
          current_account.agents.each do |agent|
            next if agent.zopim_identity.present?
            agent.create_zopim_identity!
          end
        end

        it "downgrades the given number of agents" do
          Zopim::Agent.any_instance.expects(:destroy).twice
          put :downgrade, params: { excess_agent_count: 2 }
          assert_response :success
        end
      end
    end
  end
end
