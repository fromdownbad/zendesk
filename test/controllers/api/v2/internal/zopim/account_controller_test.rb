require_relative "../../../../../support/test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"
require "zendesk/o_auth/testing/token_factory"

SingleCov.covered!

describe Api::V2::Internal::Zopim::AccountController do
  extend Api::V2::TestHelper
  fixtures :accounts

  before do
    accept :json
    @account = accounts(:minimum)
    @request.account = @account
  end

  with_options(controller: "api/v2/internal/zopim/account") do |request|
    request.should_route :get, "/api/v2/internal/zopim", action: "show"
  end

  as_an_anonymous_user do
    # TODO: This fails as the response is a 404, not a 403
    # should_be_unauthorized [:get, :show]
  end

  as_an_admin do
    # TODO: This fails as the response is a 404, not a 403
    # should_be_forbidden [:get, :show]
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    before do
      client = FactoryBot.create(:global_client, identifier: "zdg-zopim", user: @account.owner, account: @account)
      token = FactoryBot.create(:token, global_client: client, user: nil, scopes: "zopim")
      @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token

      get :show
    end

    should_use_presenter Api::V2::Internal::ZopimPresenter, status: :ok
  end
end
