require_relative "../../../../../support/test_helper"

SingleCov.covered! uncovered: 0

describe Api::V2::Internal::Zopim::OmnichannelTicketsController do
  fixtures :all

  let(:sample_ticket) do
    {
      ticket: {
        subject: 'hello world',
        description: 'hello world',
        requester_id: @request.account.users.first.id,
        system_metadata: {
          client: '',
          ip_address: ''
        }
      }
    }.freeze
  end
  let(:ticket) { tickets(:minimum_1) }

  before do
    accept :json
    @account = accounts(:minimum)
    @request.account = @account
  end

  describe '#create' do
    as_an_admin do
      should_be_forbidden :create
    end

    as_a_subsystem_user(user: 'zopim', account: :minimum) do
      it 'creates a new ticket' do
        post :create, params: sample_ticket

        assert_response :success
      end

      it 'disables triggers' do
        Ticket.
          expects(:with_triggers_disabled).
          yields.
          returns(ticket).
          once

        post :create, params: sample_ticket
      end
    end
  end
end
