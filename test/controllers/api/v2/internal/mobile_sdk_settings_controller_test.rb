require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::MobileSdkSettingsController do
  extend Api::Presentation::TestHelper
  fixtures :all

  def create_mobile_sdk_settings
    @account.create_mobile_sdk_settings!(
      auth_endpoint: @auth_endpoint,
      auth_token: @auth_token,
      enabled: true
    )
  end

  before do
    accept :json
    @request.account = @account = accounts(:minimum)
    @auth_endpoint = "https://sample.com/zd/sdk"
    @auth_token = SecureRandom.base64(2 * 48).gsub(/[^\w]/, '').slice(0, 48)
  end

  with_options(controller: "api/v2/internal/mobile_sdk_settings") do |request|
    request.should_route :get,    "/api/v2/internal/mobile_sdk_setting", action: "show"
    request.should_route :put,    "/api/v2/internal/mobile_sdk_setting", action: "update"
    request.should_route :post, "/api/v2/internal/mobile_sdk_setting", action: "create"
  end

  as_an_admin do
    should_be_forbidden :show, :update
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#show" do
      describe "on an account with mobile sdk settings" do
        before do
          assert create_mobile_sdk_settings
          get :show
        end

        should_use_presenter Api::V2::Account::MobileSdkSettingsPresenter, status: :ok
      end

      describe "on an account without mobile sdk settings" do
        before { get :show }
        it('responds with not_found') { assert_response :not_found }
      end
    end

    describe "#create" do
      describe "on an account without mobile sdk settings" do
        before do
          MobileSdkSettings.destroy_all
          post :create, params: { mobile_sdk_settings: {
            auth_endpoint: @auth_endpoint,
            auth_token: @auth_token
          } }
        end

        should_use_presenter Api::V2::Account::MobileSdkSettingsPresenter, status: :created
      end
    end

    describe "#update" do
      describe "on an account with mobile sdk settings" do
        before do
          create_mobile_sdk_settings
          @other_auth_endpoint = "https://new.example.com"
          put :update, params: { mobile_sdk_settings: { auth_endpoint: @other_auth_endpoint } }
        end

        should_use_presenter Api::V2::Account::MobileSdkSettingsPresenter, status: :ok
        it "updates the record" do
          json = JSON.parse(@response.body)
          assert_equal @other_auth_endpoint, json["mobile_sdk_settings"]["auth_endpoint"]
        end
      end

      describe "enable/disable" do
        before do
          create_mobile_sdk_settings
          @enabled = false
          put :update, params: { mobile_sdk_settings: { enabled: @enabled } }
        end

        should_use_presenter Api::V2::Account::MobileSdkSettingsPresenter, status: :ok
        it "disables the mobile sdk" do
          json = JSON.parse(@response.body)
          assert_equal @enabled, json["mobile_sdk_settings"]["enabled"]
        end
      end

      describe "on an account without mobile sdk settings" do
        before do
          put :update, params: { mobile_sdk_settings: { auth_endpoint: "https://new.example.com" } }
        end

        it('responds with not_found') { assert_response :not_found }
      end
    end
  end
end
