require_relative '../../../../support/test_helper'
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::Internal::AccountSettingsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper

  fixtures :accounts, :users

  before do
    accept :json
    @account = accounts(:minimum)
    @request.account = @account
  end

  with_options(controller: "api/v2/internal/account_settings") do |request|
    request.should_route :get, "/api/v2/internal/account/settings", action: "index"
    request.should_route :put, "/api/v2/internal/account/settings", action: "update"
    request.should_route :get, '/api/v2/internal/account/settings/show_many', action: 'show_many'
  end

  as_an_admin do
    should_be_forbidden [:put, :update], :update
  end

  describe "as a metropolis subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "metropolis") do
      should_be_authorized do
        get :index
        get :show_many, params: { ids: '' }, format: :json, as: :json
        put :update, params: { account_settings: [%w[agent_forwardable_emails 1]] }, format: :json, as: :json
      end
    end
  end

  describe "connect/outbound subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "outbound") do
      should_be_forbidden [:put, :update], :index, :show_many
    end
  end

  describe "zopim subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "zopim") do
      should_be_authorized do
        get :index
        get :show_many, params: { ids: '' }, format: :json, as: :json
        put :update, params: {}
      end
    end
  end

  describe "sell subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "sell") do
      should_be_forbidden [:put, :update], :index, :show_many
    end
  end

  describe 'passport subsystem user' do
    let(:settings_payload) { [%w[agent_forwardable_emails 1]] }

    as_a_subsystem_user(account: :minimum, user: 'passport') do
      should_be_authorized do
        get :index
        get :show_many, params: { ids: '' }, format: :json, as: :json
        put :update, params: { account_settings: settings_payload }
      end
    end
  end

  describe 'sunco configurator subsystem user' do
    let(:settings_payload) { [%w[native_messaging 1]] }

    as_a_subsystem_user(account: :minimum, user: 'sunco_configurator') do
      should_be_authorized do
        get :index
        get :show_many, params: {ids: ''}, format: :json, as: :json
        put :update, params: { account_settings: settings_payload }
      end

      describe "a PUT to settings" do
        before { put :update, params: { account_settings: settings_payload }, as: :json }

        it("responds with ok") { assert_response :ok }
      end
    end
  end

  describe "embeddable subsystem user" do
    let(:settings_payload) { [%w[agent_forwardable_emails 1]] }

    as_a_subsystem_user(account: :minimum, user: "embeddable") do
      should_be_authorized do
        get :index
        get :show_many, params: { ids: '' }, format: :json, as: :json
        put :update, params: { account_settings: settings_payload }
      end

      describe "a PUT to settings" do
        before { put :update, params: { account_settings: settings_payload }, as: :json }

        it("responds with ok") { assert_response :ok }
      end
    end
  end

  describe "answer bot flow composer subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "answer_bot_flow_composer") do
      should_be_authorized do
        get :index
        get :show_many, params: { ids: '' }, format: :json, as: :json
      end

      should_be_forbidden :update
    end
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#update" do
      describe "when account setting already exists" do
        describe "and no update or save problems occur" do
          it "updates" do
            cache_key = @account.cache_key
            assert_equal false, @account.settings.agent_forwardable_emails?

            put :update, params: { account_settings: [%w[agent_forwardable_emails 1]] }, format: :json, as: :json

            assert_response 200
            assert @request.account.reload.settings.agent_forwardable_emails?
            assert_not_equal cache_key, @account.cache_key
          end
        end

        describe "and new value is the same as default" do
          it "does not update, 422 with error message to the client" do
            cache_key = @account.cache_key
            assert_equal false, @account.settings.agent_forwardable_emails?

            put :update, params: { account_settings: [%w[agent_forwardable_emails 0]] }, format: :json, as: :json

            assert_response 422
            assert_equal false, @account.settings.agent_forwardable_emails?
            assert_includes response.body, "Could not update setting 'agent_forwardable_emails' for account ##{@account.id}: already set to 0"
            assert_equal cache_key, @account.cache_key
          end
        end

        describe "and account setting not marked as changed" do
          before { AccountSetting.any_instance.stubs(:changed?).returns(false) }

          it "does not update, 422 with error message to the client" do
            cache_key = @account.cache_key
            assert_equal false, @account.settings.agent_forwardable_emails?

            put :update, params: { account_settings: [%w[agent_forwardable_emails 1]] }, format: :json, as: :json

            assert_response 422
            assert_equal false, @account.reload.settings.agent_forwardable_emails?
            assert_includes response.body, "Could not update setting 'agent_forwardable_emails' for account ##{@account.id}"
            assert_equal cache_key, @account.cache_key
          end
        end

        describe "and account setting validation fails on update" do
          before do
            AccountSetting.any_instance.stubs(:save!).raises(ActiveRecord::RecordInvalid.new(@account.triggers.build))
          end

          it "does not update, 422 with error message to the client" do
            cache_key = @account.cache_key
            assert_equal false, @account.settings.agent_forwardable_emails?

            put :update, params: { account_settings: [%w[agent_forwardable_emails 1]] }, format: :json, as: :json

            assert_response 422
            assert_equal false, @account.reload.settings.agent_forwardable_emails?
            assert_includes response.body, 'Validation failed'
            assert_equal cache_key, @account.cache_key
          end
        end

        describe "and account setting before callbacks fail on update" do
          before do
            AccountSetting.any_instance.stubs(:save!).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))
          end

          it "does not update, 422 with error message to the client" do
            cache_key = @account.cache_key
            assert_equal false, @account.settings.agent_forwardable_emails?

            put :update, params: { account_settings: [%w[agent_forwardable_emails 1]] }, format: :json, as: :json

            assert_response 422
            assert_equal false, @account.reload.settings.agent_forwardable_emails?
            assert_includes response.body, 'RecordNotSaved'
            assert_equal cache_key, @account.cache_key
          end
        end
      end

      describe "when account setting does not exist" do
        describe "and new value is not the same as default" do
          it "creates account setting" do
            put :update, params: { account_settings: [["api_rate_limit", 500]] }, format: :json, as: :json
            assert_response 200
            assert_present @request.account.reload.settings.find_by_name('api_rate_limit')
          end
        end

        describe "and new value is the same as default" do
          it "creates account setting" do
            put :update, params: { account_settings: [["api_rate_limit", 700]] }, format: :json, as: :json
            assert_response 200
            assert_present @request.account.reload.settings.find_by_name('api_rate_limit')
          end
        end

        describe "and account setting not marked as changed" do
          before { AccountSetting.any_instance.stubs(:changed?).returns(false) }

          it "does not update, 422 with error message to the client" do
            put :update, params: { account_settings: [["api_rate_limit", 500]] }, format: :json, as: :json
            assert_response 422
            assert_includes response.body, "Could not update setting 'api_rate_limit' for account ##{@account.id}"
            assert_nil @request.account.reload.settings.find_by_name('api_rate_limit')
          end
        end

        describe "and account setting validation fails on update" do
          before do
            AccountSetting.any_instance.stubs(:save!).raises(ActiveRecord::RecordInvalid.new(@account.triggers.build))
          end

          it "does not update, 422 with error message to the client" do
            put :update, params: { account_settings: [["api_rate_limit", 500]] }, format: :json, as: :json
            assert_response 422
            assert_includes response.body, 'Validation failed'
            assert_nil @request.account.reload.settings.find_by_name('api_rate_limit')
          end
        end

        describe "and account setting before callbacks fail on update" do
          before do
            AccountSetting.any_instance.stubs(:save!).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))
          end

          it "does not update, 422 with error message to the client" do
            put :update, params: { account_settings: [["api_rate_limit", 500]] }, format: :json, as: :json
            assert_response 422
            assert_includes response.body, 'RecordNotSaved'
            assert_nil @request.account.reload.settings.find_by_name('api_rate_limit')
          end
        end
      end
    end

    describe "#index" do
      it "renders" do
        @account.settings.ticket_tagging = "x"
        @account.settings.lookup("ticket_tagging").save!

        get :index, format: :json, as: :json

        assert_response 200
        result = JSON.parse(@response.body)
        assert_equal "x", result["account_settings"]["ticket_tagging"]
      end

      it "includes defaults in the account settings" do
        assert_nil @account.settings.lookup_without_default(:anonymous_request_threshold)
        get :index, format: :json

        assert_response 200
        result = JSON.parse(@response.body)
        assert_equal AccountSetting.keys.size, result["account_settings"].keys.size
        assert_equal(
          ::AccountSetting.default(:anonymous_request_threshold),
          result["account_settings"]["anonymous_request_threshold"]
        )
      end
    end

    describe '#show_many' do
      let(:json_response) { JSON.parse(response.body) }

      it 'returns an error if required param is omitted' do
        get :show_many, params: {}, format: :json, as: :json

        assert_response :bad_request
      end

      it 'returns empty for empty query param' do
        get :show_many, params: { ids: '' }, format: :json, as: :json

        assert_response :ok
        assert_empty json_response['account_settings']
      end

      it 'returns requested settings' do
        expected_json_full_set = {
          'ticket_tagging' => '1',
          'polaris' => false,
          'cdn_provider' => 'default',
          'voice_maximum_queue_wait_time' => '1'
        }

        get :show_many, params: { ids: 'ticket_tagging,polaris,cdn_provider,voice_maximum_queue_wait_time' }, format: :json, as: :json

        assert_response :ok
        assert_equal expected_json_full_set, json_response['account_settings']
      end

      it 'ignores invalid setting ids' do
        get :show_many, params: { ids: 'polaris,some_invalid_settings' }, format: :json, as: :json

        assert_response 200
        refute_includes json_response['account_settings'], 'some_invalid_settings'
      end
    end
  end
end
