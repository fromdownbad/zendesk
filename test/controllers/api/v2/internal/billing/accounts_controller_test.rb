require_relative '../../../../../support/test_helper'

SingleCov.covered! uncovered: 1

describe Api::V2::Internal::Billing::AccountsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :subscriptions

  let(:account) { accounts(:minimum) }

  before do
    @request.account = account
    accept :json
  end

  with_options(controller: 'api/v2/internal/billing/accounts') do |request|
    request.should_route :put, '/api/v2/internal/billing/account', action: 'update'
  end

  as_an_anonymous_user do
    should_be_unauthorized :update
  end

  as_an_end_user do
    should_be_forbidden :update
  end

  as_an_agent do
    should_be_forbidden :update
  end

  as_an_admin do
    should_be_forbidden :update
  end

  as_an_owner do
    should_be_forbidden :update
  end

  as_a_subsystem_user(user: 'zendesk', account: :minimum) do
    describe "#update" do
      before do
        put :update, params: { account: {
            is_serviceable: false,
            is_active:      false,
            subscription:   {
              plan_type:              SubscriptionPlanType.SMALL,
              pricing_model_revision: 5,
              max_agents:             6,
              billing_cycle_type:     BillingCycleType.ANNUALLY,
              payment_method_type:    1,
              is_trial:               false,
              audit_message:          "cancelling subscription"
            }
          } }
      end

      it('responds with success') { assert_response :success }
    end
  end
end
