require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Billing::SubscriptionsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    Subscription.any_instance.stubs(:voice_account).returns(nil)
    Subscription.any_instance.stubs(:guide_preview).returns(nil)
    Subscription.any_instance.stubs(:suite_allowed?).returns(nil)

    @request.account = account
    accept :json
  end

  with_options(controller: 'api/v2/internal/billing/subscriptions') do |request|
    request.should_route :get,    '/api/v2/internal/billing/subscription', action: 'show'
    request.should_route :delete, '/api/v2/internal/billing/subscription', action: 'destroy'
  end

  as_an_anonymous_user do
    should_be_unauthorized :show, :destroy
  end

  as_an_end_user do
    should_be_forbidden :show, :destroy
  end

  as_an_agent do
    should_be_forbidden :show, :destroy
  end

  as_an_admin do
    should_be_forbidden :show, :destroy
  end

  as_an_owner do
    should_be_forbidden :show, :destroy
  end

  as_a_subsystem_user(user: 'billing', account: :minimum) do
    describe "#show" do
      before do
        get :show
      end

      it('responds with success') { assert_response :success }
      should_use_presenter Api::V2::Account::SubscriptionPresenter, status: :success
    end

    describe "#destroy" do
      describe "when able to cancel the subscription" do
        before do
          account.expects(cancel!: true).once
        end

        it "responds with 204" do
          delete :destroy
          assert_response :no_content
        end
      end

      describe "when unable to cancel the subscription" do
        before do
          account.expects(cancel!: false).once
          delete :destroy
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end
  end
end
