require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Billing::LatestTpeUsageController do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:account) { accounts(:trial) }

  before do
    @request.account = account
    accept :json
  end

  with_options(controller: 'api/v2/internal/billing/latest_tpe_usage') do |request|
    request.should_route :get, '/api/v2/internal/billing/latest_tpe_usage/42', action: :show, account_id: 42
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :show, { account_id: 42 }]
  end

  as_an_end_user do
    should_be_forbidden [:get, :show, { account_id: 42 }]
  end

  as_an_agent do
    should_be_forbidden [:get, :show, { account_id: 42 }]
  end

  as_an_admin do
    should_be_forbidden [:get, :show, { account_id: 42 }]
  end

  as_an_owner do
    should_be_forbidden [:get, :show, { account_id: 42 }]
  end

  as_a_subsystem_user(user: 'billing', account: :minimum) do
    describe "#show" do
      before { get :show, params: { account_id: account.id } }

      it('responds with success') { assert_response :success }
    end
  end
end
