require_relative '../../../../../../support/test_helper'
require_relative '../../../../../../support/api_scopes_helper'

SingleCov.covered!

describe Api::V2::Internal::Billing::Subscription::AddonsController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper

  fixtures :accounts

  let(:account) { accounts(:extra_large) }

  before do
    account.subscription.update_attribute(:pricing_model_revision, ZBC::Zendesk::PricingModelRevision::PATAGONIA)
    @request.account = account
    accept :json
  end

  with_options(controller: 'api/v2/internal/billing/subscription/addons') do |request|
    request.should_route :put, '/api/v2/internal/billing/subscription/addons', action: 'update'
  end

  as_an_anonymous_user do
    should_be_unauthorized :update
  end

  as_an_end_user do
    should_be_forbidden :update
  end

  as_an_agent do
    should_be_forbidden :update
  end

  as_an_admin do
    should_be_forbidden :update
  end

  as_an_owner do
    should_be_forbidden :update
  end

  as_a_subsystem_user(user: 'billing', account: :extra_large) do
    should_be_authorized { :update }
  end

  as_a_subsystem_user(user: 'billing', account: :extra_large) do
    describe '#update' do
      describe 'with a valid addon' do
        it 'adds the addon to the account' do
          feature_name = 'light_agents'
          assert_nil account.subscription_feature_addons.find { |a| a.name == feature_name }
          put :update, params: { addons: { 'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' } }, format: :json }
          assert account.subscription_feature_addons.find { |a| a.name == feature_name }.present?
        end
      end

      describe 'with an invalid addon' do
        it 'returns error for invalid addon name' do
          feature_name = 'foo_addon'
          put :update, params: { addons: { 'foo_addon' => { zuora_rate_plan_id: 'foo_rate_plan_id' } }, format: :json }
          assert_nil account.subscription_feature_addons.find { |a| a.name == feature_name }
          expected = /addon\(s\) \[.*\] not available for account .*/
          assert_match expected, JSON.parse(response.body)['error']
        end
      end

      describe 'with addon missing' do
        before do
          account.subscription_feature_addons.create!(
            name:               'light_agents',
            zuora_rate_plan_id: '1234567890'
          )
        end

        it 'removes the addon from the account' do
          assert account.subscription_feature_addons.find { |a| a.name == 'light_agents' }
          put :update, params: { addons: { }, format: :json }
          assert_nil account.subscription_feature_addons.find { |a| a.name == :light_agents }
        end
      end

      describe "Passing the same addons multiple times" do
        it "should succeed, but do nothing" do
          put :update, params: { addons: { 'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' } } }, session: { format: :json }
          assert_equal 200, @response.status, @response.body.inspect
          assert_equal 1, account.subscription_feature_addons.count(:all)
          put :update, params: { addons: { 'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' } } }, session: { format: :json }
          assert_equal 200, @response.status
          assert_equal 1, account.subscription_feature_addons.count(:all)
        end
      end

      describe 'with no addons parameter' do
        it 'treats it as no addons (rather than throwing an error)' do
          put :update, format: :json
          assert_equal 200, response.status
        end
      end
    end
  end
end
