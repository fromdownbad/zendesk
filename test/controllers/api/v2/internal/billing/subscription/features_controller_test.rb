require_relative '../../../../../../support/test_helper'
require_relative '../../../../../../support/api_scopes_helper'

SingleCov.covered!

describe Api::V2::Internal::Billing::Subscription::FeaturesController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    @request.account = account
    accept :json
  end

  with_options(controller: 'api/v2/internal/billing/subscription/features') do |request|
    request.should_route :put, '/api/v2/internal/billing/subscription/features', action: 'update'
  end

  as_an_anonymous_user do
    should_be_unauthorized :update
  end

  as_an_end_user do
    should_be_forbidden :update
  end

  as_an_agent do
    should_be_forbidden :update
  end

  as_an_admin do
    should_be_forbidden :update
  end

  as_an_owner do
    should_be_forbidden :update
  end

  as_a_subsystem_user(user: 'billing', account: :minimum) do
    should_be_authorized { :update }
  end

  as_a_subsystem_user(user: 'billing', account: :minimum) do
    describe '#update' do
      describe 'successful update' do
        it 'updates the record' do
          put :update, params: { zuora_data: { plan_type: SubscriptionPlanType.Large }, format: :json }
          expected = Zendesk::Features::Catalogs::LegacyCatalog.features_for_plan(SubscriptionPlanType.Large).map(&:name).sort
          actual = account.subscription.active_features.map { |f| f.name.to_sym }.sort
          assert_equal expected, actual
        end
      end

      describe 'with an invalid addon' do
        it 'returns error for invalid addon name' do
          put :update, params: { zuora_data: { addons: { 'foo_addon' => { zuora_rate_plan_id: 'foo_rate_plan_id' } } }, format: :json }
          assert_nil account.subscription_feature_addons.find { |a| a.name == :foo_addon }
          expected = /addon .* is not available for account .*/
          assert_match expected, JSON.parse(response.body)['error']
        end
      end
    end
  end

  as_a_subsystem_user(user: 'billing', account: :extra_large) do
    let(:account) { accounts(:extra_large) }

    describe '#update' do
      before do
        Subscription.any_instance.stubs(:pricing_model_revision).returns(ZBC::Zendesk::PricingModelRevision::PATAGONIA)
        Zendesk::Features::SubscriptionFeatureService.new(account).execute!
        @request.account = account
        accept :json
      end

      describe 'with addons' do
        it 'adds the addons as features' do
          assert_equal 0, account.subscription.active_features.select { |f| f.name == 'light_agents' || f.name == 'unlimited_multibrand' }.count
          put :update, params: { zuora_data: { addons: { 'light_agents' => { zuora_rate_plan_id: '1234'}, 'unlimited_multibrand' => { zuora_rate_plan_id: '4567' } } }, format: :json }
          assert_equal 2, account.subscription.active_features.select { |f| f.name == 'light_agents' || f.name == 'unlimited_multibrand' }.count
        end
      end

      describe 'with no addons parameter' do
        before do
          addons = {'light_agents' => { zuora_rate_plan_id: '1234' } }
          Zendesk::Features::AddonService.new(account, addons).execute!
          Zendesk::Features::SubscriptionFeatureService.new(account, nil, nil, addons).execute!
        end

        it 'treats it as no addons (i.e. deletes any existing addons)' do
          assert_equal 1, account.subscription.active_features.select { |f| f.name == 'light_agents' }.count
          Zendesk::Features::AddonService.new(account).execute!
          put :update, params: { zuora_data: {} }, session: { format: :json }
          assert_equal 0, account.subscription.active_features.select { |f| f.name == 'light_agents' }.count
        end
      end

      describe "Passing the same addons multiple times" do
        before do
          Zendesk::Features::AddonService.new(account, 'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' }).execute!
        end

        it "should succeed, but do nothing" do
          put :update, params: { zuora_data: { addons: { 'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' } } }, format: :json }
          assert_equal 200, @response.status
          assert account.subscription.active_features.any? { |f| f.name == 'light_agents' }
          put :update, params: { zuora_data: { addons: { 'light_agents' => { zuora_rate_plan_id: 'rate_plan_id' } } }, format: :json }
          assert_equal 200, @response.status
          assert account.subscription.active_features.any? { |f| f.name == 'light_agents' }
        end
      end
    end
  end
end
