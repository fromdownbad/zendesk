require_relative '../../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Billing::Subscription::PreviewsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    @request.account = account
    accept :json
  end

  with_options(controller: 'api/v2/internal/billing/subscription/previews') do |request|
    request.should_route :get, '/api/v2/internal/billing/subscription/preview', action: 'show'
  end

  as_an_anonymous_user do
    should_be_unauthorized :show
  end

  as_an_end_user do
    should_be_forbidden :show
  end

  as_an_agent do
    should_be_forbidden :show
  end

  as_an_admin do
    should_be_forbidden :show
  end

  as_an_owner do
    should_be_forbidden :show
  end

  as_a_subsystem_user(user: 'zendesk', account: :minimum) do
    let (:preview_params) do
      {
        subscription: {
          plan_type: SubscriptionPlanType.Large,
          billing_cycle_type: BillingCycleType.Annually,
          max_agents: 5
        }
      }
    end

    describe "#show" do
      before do
        Subscription.any_instance.stubs(:voice_account).returns(nil)
        Subscription.any_instance.stubs(:guide_preview).returns(nil)
        Subscription.any_instance.stubs(:suite_allowed?).returns(anything)

        get :show, params: preview_params
      end

      it('responds with success') { assert_response :success }
      should_use_presenter Api::V2::Account::SubscriptionPresenter, status: :success
    end
  end
end
