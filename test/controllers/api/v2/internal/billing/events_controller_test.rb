require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Billing::EventsController do
  extend Api::V2::TestHelper

  fixtures :all

  before do
    accept :json
    @request.account = accounts(:minimum)
  end

  with_options(controller: 'api/v2/internal/billing/events') do |request|
    request.should_route :post,
      '/api/v2/internal/billing/events',
      action: :create
  end

  as_an_anonymous_user { should_be_unauthorized :create }
  as_an_end_user       { should_be_forbidden :create }
  as_an_agent          { should_be_forbidden :create }
  as_an_admin          { should_be_forbidden :create }

  as_a_subsystem_user(user: 'anyone', account: :minimum) do
    describe '#create' do
      before do
        AccountSynchronizerJob.expects(:enqueue).
          with(bar: 'zoo').never
        post :create, params: { event: { topic: 'foo', payload: { bar: 'zoo' } } }
      end

      it { assert_response :forbidden }
    end
  end

  as_a_subsystem_user(user: 'billing', account: :minimum) do
    describe '#create' do
      before do
        AccountSynchronizerJob.expects(:enqueue).
          with(bar: 'zoo').once
        post :create, params: { event: { topic: 'foo', payload: { bar: 'zoo' } } }
      end

      it { assert_response :accepted }
    end
  end
end
