require_relative '../../../../../support/test_helper'
require_relative '../../../../../support/suite_test_helper'

SingleCov.covered!

describe Api::V2::Internal::Billing::AgentCountsController do
  extend Api::Presentation::TestHelper

  include SuiteTestHelper

  fixtures :accounts, :zopim_subscriptions

  before do
    setup_stubs_for_adding_agents(account)

    account.owner.account_id = account.id
    account.owner.save!(validate: false)

    @request.account = account
    @controller.stubs(:current_account).returns(account)
    accept :json
  end

  let(:account)             { accounts(:with_paid_zopim_subscription) }
  let(:owner)               { account.owner }
  let!(:chat_enabled_agent) { FactoryBot.create(:chat_enabled_agent, account: account) }
  let!(:chat_only_agent)    { FactoryBot.create(:chat_only_agent,    account: account) }
  let!(:support_only_agent) { FactoryBot.create(:agent,              account: account) }

  with_options(controller: 'api/v2/internal/billing/agent_counts') do |request|
    request.should_route :get, '/api/v2/internal/billing/agent_counts/42', action: :show, account_id: 42
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :show, { account_id: 42 }]
  end

  as_an_end_user do
    should_be_forbidden [:get, :show, { account_id: 42 }]
  end

  as_an_agent do
    should_be_forbidden [:get, :show, { account_id: 42 }]
  end

  as_an_admin do
    should_be_forbidden [:get, :show, { account_id: 42 }]
  end

  as_an_owner do
    should_be_forbidden [:get, :show, { account_id: 42 }]
  end

  as_a_subsystem_user(user: 'billing', account: :minimum) do
    describe "#show" do
      before { get :show, params: { account_id: account.id } }

      it('responds with success') { assert_response :success }
    end
  end

  def setup_stubs_for_adding_agents(account)
    PermissionSet.create_chat_agent!(account)

    User.any_instance.stubs(encrypt_password: true)
    User.any_instance.stubs(password_security_policy_enforced: true)

    Zopim::Agent.any_instance.stubs(:link_to_remote_account_record)

    SecurityMailer.stubs(send: true)
  end
end
