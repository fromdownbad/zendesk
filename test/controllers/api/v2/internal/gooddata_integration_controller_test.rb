require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 1

describe Api::V2::Internal::GooddataIntegrationController do
  fixtures :accounts, :users

  before do
    @request.account = accounts(:minimum)
  end

  with_options controller: 'api/v2/internal/gooddata_integration' do |request|
    request.should_route :get,    'api/v2/internal/gooddata_integration', action: 'show'
    request.should_route :put,    'api/v2/internal/gooddata_integration', action: 'update'
    request.should_route :delete, 'api/v2/internal/gooddata_integration', action: 'destroy'
    request.should_route :put,    'api/v2/internal/gooddata_integration/sync_configuration', action: 'sync_configuration'
    request.should_route :put,    'api/v2/internal/gooddata_integration/reprovision_user', action: 'reprovision_user'
  end

  as_a_subsystem_user account: :minimum do
    before do
      @controller.stubs(:current_registered_user).returns(Zendesk::SystemUser.find_subsystem_user_by_name('zendesk'))
      @integration = stub
      @integration.stubs(:project_id).returns(123)
      @integration.stubs(:version).returns(2)
      @integration.stubs(:as_json).returns(gooddata_integration: {project_id: 123, version: 2})
    end

    it "returns the correct representation" do
      @account.stubs(:gooddata_integration).returns(@integration)
      @integration.stubs(:subdomain_matches_project?).returns(true)
      @integration.expects(:as_json).with(only: [:project_id, :version]).returns(gooddata_integration: {project_id: 123, version: 2})

      get :show, format: :json

      @result = JSON.parse(@response.body)

      assert_equal({'project_id' => 123, 'version' => 2}, @result['gooddata_integration'])
    end

    describe "when no integration exists" do
      before do
        @account.stubs(:gooddata_integration).returns(nil)

        get :show, format: :json

        @result = JSON.parse(@response.body)
      end

      it "returns nil" do
        assert_nil @result['gooddata_integration']
      end
    end

    describe "#sync_configuration" do
      describe "when an integration is present" do
        before { @account.stubs(:gooddata_integration).returns(@integration) }

        it "schedules a configuration job" do
          GooddataConfigurationJob.expects(:enqueue).with(@account.id)
          put :sync_configuration, format: :json
        end
      end

      describe "when an integration is not present" do
        before { @account.stubs(:gooddata_integration).returns(nil) }

        it "does not schedule a configuration job" do
          GooddataConfigurationJob.expects(:enqueue).never
          put :sync_configuration, format: :json
        end
      end
    end

    describe "#reprovision_user" do
      let(:zendesk_user) { users(:minimum_agent) }
      let(:user_id) { zendesk_user.id }
      let(:params) do
        {
          format: :json,
          user_id: user_id
        }
      end

      describe "when an integration is present" do
        before { @account.stubs(:gooddata_integration).returns(@integration) }

        it 'schedules user reprovisioning job' do
          GooddataUserReprovisioningJob.expects(:enqueue).with(@account.id, zendesk_user.id)

          put :reprovision_user, params: params

          assert_equal 200, @response.status
        end

        describe 'but user not found' do
          let(:user_id) { 9999999 }

          it 'returns not found error' do
            put :reprovision_user, params: params
            assert_equal 404, @response.status
          end
        end

        describe 'but user not agent or admin' do
          let(:zendesk_user) { users(:minimum_end_user) }

          it 'returns not found error' do
            put :reprovision_user, params: params
            assert_equal 404, @response.status
          end
        end
      end

      describe "when an integration is not present" do
        before { @account.stubs(:gooddata_integration).returns(nil) }

        it 'does not check reprovision user' do
          GooddataUserReprovisioningJob.expects(:enqueue).never
          put :reprovision_user, params: params
        end
      end
    end

    describe "#show" do
      before { @account.stubs(:gooddata_integration).returns(@integration) }

      describe "basic project information" do
        before do
          @integration.stubs(:subdomain_matches_project?).returns(true)
          get :show, format: :json

          @result = JSON.parse(@response.body)
        end

        it "returns the project ID" do
          assert_equal 123, @result['gooddata_integration']['project_id']
        end

        it "returns the integration version" do
          assert_equal 2, @result['gooddata_integration']['version']
        end
      end

      describe "subdomain matches" do
        describe "when subdomain information is available" do
          before do
            @integration.stubs(:subdomain_matches_project?).returns(true)
            get :show, format: :json

            @result = JSON.parse(@response.body)
          end

          it "renders the result" do
            assert @result['subdomain_match']
          end
        end

        describe "when an exception is thrown" do
          before do
            @integration.stubs(:subdomain_matches_project?).raises
            get :show, format: :json

            @result = JSON.parse(@response.body)
          end

          it "is false" do
            get :show, format: :json
            refute @result['subdomain_match']
          end
        end
      end
    end

    describe "#update" do
      before do
        GooddataIntegrationsDestroyJob.stubs(:enqueue)
        @integration = @account.create_gooddata_integration(project_id: 456, invitation_id: 443, scheduled_at: '1am', version: 2)
        @integration.send("admin_role_id=", '1')
        @integration.send("editor_role_id=", '2')
        @integration.send("read_only_user_role_id=", '3')
      end

      describe "when the new project ID is different" do
        before do
          Zendesk::Gooddata::IntegrationProvisioning.any_instance.stubs(:project_role_mapping).returns(
            admin_role_id: '10',
            editor_role_id: '20',
            read_only_user_role_id: '30'
          )

          put :update, params: { gooddata_integration: {project_id: "123"}, format: :json }
          result = JSON.parse(@response.body)
          @new_integration = GooddataIntegration.find(result['gooddata_integration']['id'])
        end

        before_should "destroy the existing integration" do
          GooddataIntegrationsDestroyJob.expects(:enqueue).with(@account.id, @integration.id)
        end

        it "creates a new integration, retaining the old information" do
          excluded_attributes = [
            'project_id', 'invitation_id', 'id', 'updated_at', 'created_at',
            'admin_role_id', 'editor_role_id', 'read_only_user_role_id'
          ]
          copied_attributes = @new_integration.attributes.reject { |name, _| excluded_attributes.include?(name) }
          copied_attributes.keys.each do |name|
            assert_equal_with_nil @integration[name], @new_integration[name], "#{name} did not match"
          end
        end

        it "adds the project_id of the new project" do
          assert_equal "123", @new_integration.project_id
        end

        it "clears out the invitation ID" do
          assert_nil @new_integration.invitation_id
        end

        it "does not set deleted_at on the new integration" do
          assert_nil @new_integration.deleted_at
        end

        it "has new role ids" do
          assert_equal '10', @new_integration.admin_role_id
          assert_equal '20', @new_integration.editor_role_id
          assert_equal '30', @new_integration.read_only_user_role_id
        end
      end
    end

    describe "#destroy" do
      it "deletes the integration" do
        Zendesk::Gooddata::IntegrationProvisioning.expects(:destroy_for_account).with(@account)

        delete :destroy, format: :json
      end
    end
  end
end
