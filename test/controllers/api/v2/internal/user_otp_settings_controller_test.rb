require_relative '../../../../support/test_helper'
require_relative "../../../../support/multiproduct_test_helper"

SingleCov.covered!

describe Api::V2::Internal::UserOtpSettingsController do
  include Api::V2::TestHelper
  include MultiproductTestHelper

  fixtures :accounts
  fixtures :users

  before do
    accept :json
  end

  with_options(controller: 'api/v2/internal/user_otp_settings') do |request|
    request.should_route :delete, '/api/v2/internal/users/1/user_otp_settings', action: 'destroy', user_id: '1'
    request.should_route :get, '/api/v2/internal/users/1/user_otp_settings/configured', action: 'configured', user_id: '1'
    request.should_route :get, '/api/v2/internal/users/1/user_otp_settings/recovery_code', action: 'recovery_code', user_id: '1'
  end

  as_an_admin do
    should_be_forbidden [:delete, :destroy, { user_id: 1 }]
  end

  describe 'as centraladmin app on shell account without support' do
    let!(:shell_account_without_support) { setup_shell_account }
    let(:user)   { FactoryBot.create(:user, account: shell_account_without_support, roles: Role::ADMIN.id) }
    let(:actual) { JSON.parse(response.body) }

    before do
      accept :json
      @account         = shell_account_without_support
      @request.account = @account
      system_user      = Zendesk::SystemUser.find_subsystem_user_by_name('centraladminapp')
      assert system_user, "No subsystem user found, bad zendesk.yml?"

      system_user.account = @account

      @controller.stubs(:current_registered_user).returns(system_user)
    end

    describe 'DELETE destroy' do
      describe 'with an existing user otp_setting record' do
        before do
          Zendesk::Auth::Otp::Time.new(user).setup!
          assert user.reload.otp_setting

          delete :destroy, params: { user_id: user.id }
        end

        it 'responds with ok' do
          assert_response :ok
        end

        it 'removes otp setting record' do
          refute user.reload.otp_setting
        end
      end

      describe 'when otp_setting record does not exist' do
        before do
          refute user.otp_setting

          delete :destroy, params: { user_id: user.id }
        end

        it 'responds with 404' do
          assert_response :not_found
        end

        it 'responds with error' do
          assert_equal "RecordNotFound", actual["error"]
        end

        it 'responds with correct error message' do
          assert_includes actual["description"], "Couldn't find UserOtpSetting"
        end
      end

      describe 'when user does not exist' do
        before do
          refute @account.users.find_by_id(123)

          delete :destroy, params: { user_id: 123 }
        end

        it 'responds with 404' do
          assert_response :not_found
        end

        it 'responds with error' do
          assert_equal "RecordNotFound", actual["error"]
        end

        it 'responds with correct error message' do
          assert_includes actual["description"], "Couldn't find User"
        end
      end
    end

    describe 'GET recovery_code' do
      let (:recovery_code) { JSON.parse(response.body)['recovery_code'] }

      describe 'with a user that has otp set up' do
        before do
          otp = Zendesk::Auth::Otp::Time.new(user)
          assert otp.setup!
          assert user.otp_setting.update_attributes!(confirmed: true)
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('get_recovery_code', tags: ["caller:central_admin"])
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.Api::V2::Internal::UserOtpSettingsController.recovery_code', anything).at_least(1)
          get :recovery_code, params: { user_id: user.id }
        end

        it 'responds with ok' do
          assert_response :ok
        end

        it 'responds with json body with the first backup code' do
          assert_equal recovery_code, user.otp_setting.backup_codes['codes'].first
        end
      end

      describe 'with a user with an unconfirmed otp setup' do
        before do
          otp = Zendesk::Auth::Otp::Time.new(user)
          assert otp.setup!
          assert user.otp_setting.update_attributes!(confirmed: false)
          get :recovery_code, params: { user_id: user.id }
        end

        it 'responds with forbidden' do
          assert_response :forbidden
        end
      end

      describe 'with a user with no otp setup' do
        before do
          refute user.otp_setting

          get :recovery_code, params: { user_id: user.id }
        end

        it 'responds with forbidden' do
          assert_response :forbidden
        end
      end

      describe 'when user does not exist' do
        before do
          refute @account.users.find_by_id(123)

          get :recovery_code, params: { user_id: 123 }
        end

        it 'responds with not found' do
          assert_response :not_found
        end
      end
    end

    describe 'GET configured' do
      let (:otp_configured) { JSON.parse(response.body)['otp_configured'] }

      describe 'with an existing confirmed user otp_setting record' do
        before do
          otp = Zendesk::Auth::Otp::Time.new(user)
          assert otp.setup!
          assert user.otp_setting.update_attributes!(confirmed: true)
          get :configured, params: { user_id: user.id }
        end

        it 'responds with ok' do
          assert_response :ok
        end

        it 'responds with json body indicating otp is configured' do
          assert_equal otp_configured, true
        end
      end

      describe 'with an existing non confirmed user otp_setting record' do
        before do
          otp = Zendesk::Auth::Otp::Time.new(user)
          assert otp.setup!
          assert user.otp_setting.update_attributes!(confirmed: false)
          get :configured, params: { user_id: user.id }
        end

        it 'responds with ok' do
          assert_response :ok
        end

        it 'responds with json body indicating otp is not configured' do
          assert_equal otp_configured, false
        end
      end

      describe 'when otp_setting record does not exist' do
        before do
          refute user.otp_setting

          get :configured, params: { user_id: user.id }
        end

        it 'responds with ok' do
          assert_response :ok
        end

        it 'responds with json body indicating otp is not configured' do
          assert_equal otp_configured, false
        end
      end

      describe 'when user does not exist' do
        before do
          refute @account.users.find_by_id(123)

          get :configured, params: { user_id: 123 }
        end

        it 'responds with 404' do
          assert_response :not_found
        end
      end
    end
  end
end
