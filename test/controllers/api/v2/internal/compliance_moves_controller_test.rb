require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 6

describe Api::V2::Internal::ComplianceMovesController do
  extend Api::V2::TestHelper

  fixtures :accounts

  before do
    accept :json
    @account              = accounts(:minimum)
    @request.account      = @account
  end

  with_options(controller: "api/v2/internal/compliance_moves") do |request|
    request.should_route :get, "/api/v2/internal/compliance_moves", action: "index"
  end

  as_an_admin do
    should_be_forbidden :index
  end

  as_a_subsystem_user(account: :minimum) do
    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::Internal::ComplianceMovesPresenter, status: :ok
    end
  end
end
