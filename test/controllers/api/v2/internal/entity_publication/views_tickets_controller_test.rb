require_relative "../../../../../support/test_helper"

SingleCov.covered! uncovered: 1
SingleCov.covered! file: 'lib/zendesk/entity_publication/database_backoff_error.rb'
SingleCov.covered! file: 'app/observers/views_observers/api_producer.rb'

describe Api::V2::Internal::EntityPublication::ViewsTicketsController do
  extend Api::V2::TestHelper
  fixtures :all

  let(:escape_producer) { FakeEscKafkaMessage.new }

  before do
    accept :json
  end

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:deleted_ticket) { Ticket.unscoped { tickets(:minimum_deleted) } }
  let(:params) { { account_id: account.id, ids: [ticket.id] } }
  let(:deleted_ticket_params) { { account_id: account.id, ids: [deleted_ticket.id] } }
  let(:tombstone_params) { { account_id: account.id, tombstone_ids: [ticket.id] } }
  let(:entity_and_tombstone) { { account_id: account.id, ids: [ticket.id], tombstone_ids: [123] } }
  let(:tombstone_deleted_ticket) { { account_id: account.id, tombstone_ids: [99999] } }
  let(:partial_found_params) { { account_id: account.id, ids: [ticket.id, 99999] } }
  let(:no_tickets_params) { { account_id: account.id, ids: [99999] } }

  as_a_subsystem_user(user: 'entity_republisher', account: :minimum) do
    describe_with_arturo_enabled :views_entity_republisher_rollout do
      describe_with_arturo_enabled :views_entity_republisher do
        describe '#index' do
          it 'uses account id passed in params' do
            ActiveRecord::Base.expects(:on_shard).with(account.shard_id).once.yields

            get :index, params: params
          end

          describe 'publication' do
            let(:json_response) { JSON.parse(response.body)['entities'].first }

            it 'returns entities' do
              get :index, params: params

              assert account.id, json_response['account_id']
              assert 'support.views.tickets', json_response['topic']
              assert "#{ticket.account_id}/#{ticket.id}", json_response['key']
              assert_instance_of String, json_response['value']
              assert anything, json_response['metadata']
            end

            describe 'publishes tombstones' do
              it 'when asked expiliclty' do
                get :index, params: tombstone_params

                json_response = JSON.parse(response.body)['tombstones'].first
                assert account.id, json_response['account_id']
                assert 'support.views.tickets', json_response['topic']
                assert "#{ticket.account_id}/#{ticket.id}", json_response['key']
                assert_nil json_response['value']
                assert anything, json_response['metadata']
              end

              it 'when entity is tombstoned' do
                get :index, params: deleted_ticket_params

                assert account.id, json_response['account_id']
                assert 'support.views.tickets', json_response['topic']
                assert "#{ticket.account_id}/#{deleted_ticket.id}", json_response['key']
                assert_nil json_response['value']
                assert anything, json_response['metadata']
              end
            end

            it 'publishes tombstones and entities' do
              get :index, params: entity_and_tombstone

              entity_1 = JSON.parse(response.body)['entities'].first
              entity_2 = JSON.parse(response.body)['tombstones'].first

              assert_instance_of String, entity_1['value']
              assert_nil entity_2['value']
            end

            it 'does not need the entity to have changes' do
              get :index, params: params

              assert_empty ticket.changes
              assert_instance_of String, json_response['value']
            end

            it 'does not need tomstone entites to be deleted' do
              get :index, params: tombstone_params

              refute ticket.deleted?
              json_response = JSON.parse(response.body)['tombstones'].first
              assert_nil json_response['value']
            end

            describe 'entity lookup' do
              # Should I reach in and test more?
              it 'ignores the default scope' do
                # Maybe I can roll this up depending on how I test it
                ticket.status_id = 5
                ticket.will_be_saved_by(users(:minimum_agent), via_id: ViaType.WEB_SERVICE)
                ticket.save!

                get :index, params: params

                assert_nil json_response['value']
              end

              it 'does not lookup tombstones' do
                get :index, params: tombstone_deleted_ticket

                json_response = JSON.parse(response.body)['tombstones'].first
                assert_nil json_response['value']
                assert "#{ticket.account_id}/99999", json_response['key']
              end
            end
          end

          describe 'response' do
            let(:response_entity) { JSON.parse(response.body)['entities'].first }

            it 'returns encoded entities' do
              get :index, params: params

              assert_response 200
              assert_equal account.id, response_entity['account_id']
              assert_equal 'support.views.tickets', response_entity['topic']
              assert_equal "#{ticket.account_id}/#{ticket.id}", response_entity['key']
              assert response_entity['value'].is_a?(String)
            end

            it '404 on account not on pod' do
              ActiveRecord::Base.expects(:on_shard).raises(ActiveRecord::AdapterNotSpecified)

              get :index, params: params
              assert_response 404
            end

            it '429 with Retry-After if database above CPU limit' do
              ZendeskDatabaseSupport::Sensors::AuroraCpuSensor.any_instance.expects(:cpu_utilization).returns(60.0)

              get :index, params: params
              assert_response 429
              assert_equal 60.0, response.headers['X-DB-CPU']
            end
          end

          describe 'instruments' do
            it 'time taken' do
              Zendesk::StatsD::Client.any_instance.expects(:time).with('republish_time')

              get :index, params: params
            end

            it 'no of entities published' do
              Zendesk::StatsD::Client.any_instance.expects(:count).with('republished_entites', 1)
              Zendesk::StatsD::Client.any_instance.expects(:count).with('republished_tombstones', 1)

              get :index, params: entity_and_tombstone
            end
          end

          describe 'limits' do
            it 'enforces batch size' do
              get :index, params: { account_id: account.id, ids: Array.new(1001) { |i| i } }

              assert_response 413
            end
          end
        end

        describe '#update' do
          before do
            ViewsObservers::EscapeProducer.any_instance.stubs(domain_event_publisher: escape_producer)
          end

          it 'uses account id passed in params' do
            ActiveRecord::Base.expects(:on_shard).with(account.shard_id).once.yields

            put :update, params: params
          end

          describe 'publicaion' do
            it 'publishes entities to escape' do
              escape_producer.expects(:bulk_import!).with(
                includes(
                  has_entries(
                    account_id: account.id,
                    topic: 'support.views.tickets',
                    key: "#{ticket.account_id}/#{ticket.id}",
                    value: is_a(String),
                    metadata: anything
                  )
                )
              )

              put :update, params: params
            end

            it 'publishes tombstones to escape' do
              escape_producer.expects(:bulk_import!).with(
                includes(
                  has_entries(
                    account_id: account.id,
                    topic: 'support.views.tickets',
                    key: "#{ticket.account_id}/#{ticket.id}",
                    value: nil,
                    metadata: anything
                  )
                )
              )

              put :update, params: tombstone_params
            end

            it 'publishes tombstones and entities' do
              escape_producer.expects(:bulk_import!).with(includes(has_entries(value: is_a(String))))
              escape_producer.expects(:bulk_import!).with(includes(has_entries(value: nil)))

              put :update, params: entity_and_tombstone
            end

            it 'does not need the entity to have changes' do
              escape_producer.expects(:bulk_import!).with(includes(has_entries(value: is_a(String))))
              assert_empty ticket.changes

              put :update, params: params
            end

            it 'does not need tomstone entites to be deleted' do
              escape_producer.expects(:bulk_import!).with(includes(has_entries(value: nil)))
              refute ticket.deleted?

              put :update, params: tombstone_params
            end

            describe 'entity lookup' do
              # Should I reach in and test more?
              it 'ignores the default scope' do
                # Maybe I can roll this up depending on how I test it
                ticket.status_id = 5
                ticket.will_be_saved_by(users(:minimum_agent), via_id: ViaType.WEB_SERVICE)
                ticket.save!

                escape_producer.expects(:bulk_import!).with(includes(has_entries(value: nil)))

                put :update, params: params
              end

              it 'does not lookup tombstones' do
                escape_producer.expects(:bulk_import!).with(includes(has_entries(value: nil)))

                put :update, params: tombstone_deleted_ticket
              end
            end
          end

          describe 'response' do
            it '200 empty body when all entites republished' do
              put :update, params: tombstone_deleted_ticket

              assert_response 200
              assert_empty response.body
            end

            it '200 with missing ids true when not all entities found' do
              put :update, params: partial_found_params

              assert_response 200
              assert_equal({ 'missing_ids' => 'true' }.to_json, response.body)
            end

            it '200 when all entities not found' do
              put :update, params: no_tickets_params

              assert_response 200
              assert_equal({ 'missing_ids' => 'true' }.to_json, response.body)
            end

            it '404 on account not on pod' do
              ActiveRecord::Base.expects(:on_shard).raises(ActiveRecord::AdapterNotSpecified)

              put :update, params: params
              assert_response 404
            end

            it '429 with Retry-After if database above CPU limit' do
              ZendeskDatabaseSupport::Sensors::AuroraCpuSensor.any_instance.expects(:cpu_utilization).returns(60.0)

              put :update, params: params
              assert_response 429
              assert_equal 60.0, response.headers['X-DB-CPU']
            end
          end

          describe 'instruments' do
            it 'time taken' do
              Zendesk::StatsD::Client.any_instance.expects(:time).with('republish_time')

              put :update, params: params
            end

            it 'no of entities published' do
              Zendesk::StatsD::Client.any_instance.expects(:count).with('republished_entites', 1)
              Zendesk::StatsD::Client.any_instance.expects(:count).with('republished_tombstones', 1)

              put :update, params: entity_and_tombstone
            end
          end

          describe 'limits' do
            it 'enforces batch size' do
              put :update, params: { account_id: account.id, ids: Array.new(1001) { |i| i } }

              assert_response 413
            end
          end
        end
      end
    end

    describe_with_arturo_enabled :views_entity_republisher_rollout do
      describe_with_arturo_disabled :views_entity_republisher do
        describe '#update' do
          it 'returns 503' do
            put :update, params: params
            assert_response 503
          end

          it 'does not publish' do
            put :update, params: params
            assert_empty response.body
          end
        end

        describe '#index' do
          it 'returns 503' do
            get :index, params: params
            assert_response 503
          end

          it 'does not publish' do
            get :index, params: params
            assert_empty response.body
          end
        end
      end
    end

    describe_with_arturo_disabled :views_entity_republisher_rollout do
      describe '#update' do
        it 'returns 404' do
          put :update, params: params
          assert_response 404
        end

        it 'does not publish' do
          put :update, params: params
          assert_empty response.body
        end
      end

      describe '#index' do
        it 'returns 404' do
          get :index, params: params
          assert_response 404
        end

        it 'does not publish' do
          get :index, params: params
          assert_empty response.body
        end
      end
    end
  end

  def create_tickets(count, account, requester)
    FactoryBot.create_list(
      :ticket,
      count,
      account: account,
      requester: requester,
      via_id: ViaType.MOBILE_SDK
    )
  end
end
