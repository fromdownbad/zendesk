require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::BoostsController do
  extend Api::V2::TestHelper

  fixtures :all

  before do
    accept :json
    @request.account = accounts(:minimum)
  end

  with_options(controller: "api/v2/internal/boosts") do |request|
    request.should_route :get,    "/api/v2/internal/boost", action: "show"
    request.should_route :put,    "/api/v2/internal/boost", action: "update"
    request.should_route :delete, "/api/v2/internal/boost", action: "destroy"
  end

  as_an_admin do
    should_be_forbidden :show, :update, :destroy
  end

  describe "embeddable subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "embeddable") do
      should_be_forbidden :update, :destroy

      describe "a GET to :show" do
        before do
          @account.create_feature_boost(expires: 30.days.from_now)
          get :show
        end

        it("responds with success") { assert_response :success }
      end
    end
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#show" do
      describe "on an account with a feature boost record" do
        before do
          assert @account.create_feature_boost(expires: 30.days.from_now)
          get :show
        end

        should_use_presenter Api::V2::Internal::BoostPresenter, status: :ok
      end

      describe "on an account without a feature boost record" do
        before { get :show }
        it('responds with not_found') { assert_response :not_found }
      end
    end

    describe "#update" do
      describe "on an account with a feature boost record" do
        before do
          @account.create_feature_boost(expires: 30.days.from_now)
          @valid = 30.days.from_now.utc
          put :update, params: { boost: { expires: @valid } }
        end

        should_use_presenter Api::V2::Internal::BoostPresenter, status: :ok
        it "updates the record" do
          json = JSON.parse(@response.body)
          assert_equal @valid.iso8601, json["boost"]["expires"]
        end
      end

      describe "on an account without a feature boost record" do
        before do
          put :update, params: { boost: { expires: 30.days.from_now.utc.iso8601 } }
        end

        should_use_presenter Api::V2::Internal::BoostPresenter, status: :ok
      end
    end

    describe "#destroy" do
      describe "on an account with a feature boost record" do
        before do
          @account.create_feature_boost(expires: 30.days.from_now)
          delete :destroy
        end

        it('responds with ok') { assert_response :ok }
      end

      describe "on an account without a feature boost record" do
        before { delete :destroy }
        it('responds with not_found') { assert_response :not_found }
      end
    end
  end
end
