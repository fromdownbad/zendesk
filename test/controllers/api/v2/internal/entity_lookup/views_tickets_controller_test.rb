require_relative "../../../../../support/test_helper"

SingleCov.covered!
SingleCov.covered! file: 'app/presenters/api/v2/internal/entity_lookup/views_tickets_presenter.rb'

describe Api::V2::Internal::EntityLookup::ViewsTicketsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
  end

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_end_user) }
  let(:group) { groups(:minimum_group) }
  let(:brand) { brands(:minimum) }
  let(:organization) { organizations(:minimum_organization1) }

  let(:params) do
    ->(type, search_params) { { account_id: account.id, type: type, origin: 'views', search_params: search_params } }
  end

  let(:user_fanout_params) { params['user_fanout', { user_id: user.id }] }

  let(:response_ids) { JSON.parse(response.body)['views-tickets'].map { |entity| entity['id'] } }

  as_a_subsystem_user(user: 'entity_republisher', account: :minimum) do
    describe_with_arturo_enabled :views_entity_republisher_rollout do
      describe_with_arturo_enabled :views_entity_republisher do
        describe '#index' do
          it 'uses the accound id passed in params' do
            ActiveRecord::Base.expects(:on_shard).with(account.shard_id).once.yields

            get :index, params: user_fanout_params
          end

          it 'returns the accout shard_id' do
            get :index, params: user_fanout_params

            json = JSON.parse(response.body).with_indifferent_access
            assert 1, json[:shard_id]
          end

          it '404 on account not on pod' do
            ActiveRecord::Base.expects(:on_shard).raises(ActiveRecord::AdapterNotSpecified)

            get :index, params: user_fanout_params
            assert_response 404
          end

          it 'uses a query timeout' do
            # TODO: This can only be set on the connection
          end

          it 'instruments time taken' do
            Zendesk::StatsD::Client.any_instance.expects(:time).with('find_time').yields

            get :index, params: user_fanout_params
          end

          it 'returns 429 whe db cpu over capacity' do
            ZendeskDatabaseSupport::Sensors::AuroraCpuSensor.any_instance.expects(:cpu_utilization).returns(60.0)

            get :index, params: user_fanout_params
            assert_response 429
            assert_equal 60.0, response.headers['X-DB-CPU']
          end

          describe 'supports request type' do
            describe '#user_fanout' do
              describe 'when the user is a submitter on a ticket' do
                let(:user) { users(:minimum_agent) }
                let(:ticket_as_submitter) { tickets(:minimum_2) }

                it 'does not return that ticket' do
                  get :index, params: user_fanout_params

                  refute_includes response_ids, ticket_as_submitter.id
                end
              end

              it 'user_fanout' do
                get :index, params: user_fanout_params

                assert_equal [33637, 37804, 67806, 78889, 96799], response_ids
              end
            end

            it 'group fanout' do
              get :index, params: params['group_fanout', { group_id: group.id}]

              assert_equal [10624, 37804, 38472, 67806, 94818], response_ids
            end

            it 'brand fanout' do
              get :index, params: params['brand_fanout', { brand_id: brand.id}]

              assert_equal [10624, 33637, 37804, 38472, 67806, 78889, 94818, 96799], response_ids
            end

            it 'organization fanout' do
              get :index, params: params['organization_fanout', { organization_id: organization.id}]

              assert_equal [33637, 37804, 67806, 78889, 96799], response_ids
            end

            describe 'backfill' do
              let(:account_tickets) { account.tickets.order(:updated_at) }
              let(:first_ticket) { account_tickets.first }
              let(:last_ticket) { account_tickets.last }
              let(:time_from) { first_ticket.updated_at + 1.second }
              let(:time_to) { last_ticket.updated_at - 1.second }

              let(:expected_ids) do
                -> (clause) do
                  Ticket.unscoped.where("account_id = #{account.id}").where(clause).order(:id).map(&:id)
                end
              end

              it 'finds before a timestamp' do
                get :index, params: params['backfill', { from: time_from }]

                assert_equal expected_ids["updated_at >= '#{time_from}'"], response_ids
              end

              it 'finds after a timestamp' do
                get :index, params: params['backfill', { to: time_to }]

                assert_equal expected_ids["updated_at <= '#{time_to}'"], response_ids
              end

              it 'finds a time range' do
                get :index, params: params['backfill', { from: time_from, to: time_to }]
                assert_equal expected_ids["updated_at >= '#{time_from}' AND updated_at <= '#{time_to}'"], response_ids
              end
            end

            describe 'pagination' do
              let(:page_param) do
                ->(size) { { page: { size: size } } }
              end

              it 'limits per page param' do
                get :index, params: user_fanout_params.merge(page_param[1001])

                assert_response 400
              end

              describe 'when there is only one page' do
                let(:json) do
                  get :index, params: user_fanout_params.merge(page_param[10])
                  JSON.parse(response.body).with_indifferent_access
                end

                it 'presents the correct keys' do
                  refute json[:meta][:has_more]
                  assert json[:meta][:after_cursor].present?
                end

                it 'presents the correct number of entities' do
                  assert_equal 5, json['views-tickets'].count
                end
              end

              describe 'when there are multiple pages' do
                let(:json) do
                  get :index, params: user_fanout_params.merge(page_param[3])
                  JSON.parse(response.body).with_indifferent_access
                end

                it 'presents the correct keys' do
                  assert json[:meta][:has_more]
                  assert json[:meta][:after_cursor].present?
                end

                it 'presents the correct number of entities' do
                  assert_equal 3, json['views-tickets'].count
                end
              end

              describe 'with an after cursor' do
                let(:first_request) do
                  get :index, params: user_fanout_params.merge(page_param[3])
                  JSON.parse(response.body).with_indifferent_access
                end

                let(:json) do
                  after_cursor = first_request[:meta][:after_cursor]
                  get :index, params: user_fanout_params.merge(page: { after: after_cursor})
                  JSON.parse(response.body).with_indifferent_access
                end

                it 'presents the correct keys' do
                  refute json[:meta][:has_more]
                  assert json[:meta][:after_cursor].present?
                end

                it 'presents the correct number of entities' do
                  assert_equal 2, json['views-tickets'].count
                end
              end
            end
          end
        end

        describe_with_arturo_disabled :views_entity_republisher do
          it 'retruns 503' do
            get :index, params: user_fanout_params
            assert_response 503
          end
        end
      end
    end

    describe_with_arturo_disabled :views_entity_republisher_rollout do
      it 'retruns 404' do
        get :index, params: user_fanout_params
        assert_response 404
      end
    end
  end
end
