require_relative "../../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::Voice::RechargeSettingsController do
  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    accept :json
  end

  as_an_admin do
    should_be_forbidden :create
    should_be_forbidden :show
    should_be_forbidden :update
  end

  as_a_subsystem_user(account: :minimum) do
    let(:account) { accounts(:minimum) }

    let(:client) do
      stub(
        update_recharge_settings: true,
        get_voice_recharge_subscription!: nil,
        session: nil
      )
    end

    let(:synchronizer) { ZendeskBillingCore::Zuora::Synchronizer.new(nil) }

    before do
      zuora_subscription = stub(zuora_account_id: nil, client: client)
      Account.any_instance.stubs(:zuora_subscription).returns(zuora_subscription)
      synchronizer.stubs(zuora_account: stub)
      synchronizer.stubs(:zendesk_account).returns(account)
      ZendeskBillingCore::Zuora::Synchronizer.stubs(:new).returns(synchronizer)
    end

    describe "#create" do
      describe "when account already has voice_recharge_settings" do
        before do
          account.create_voice_recharge_settings(
            enabled: true,
            amount: 50.00,
            minimum_balance: 5.00
          )
          Voice::RechargeSettings.any_instance.expects(:update_attributes).never
          @controller.expects(:setup_voice_recharge_placeholder_subscription).never
          post :create, params: { recharge_settings: { enabled: true, amount: 100.00 } }
        end

        it('responds with not_found') { assert_response :not_found }
      end

      describe "when account does not have voice recharge settings" do
        before do
          synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_enabled__c).returns('Yes')
          synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_amount__c).returns(100.0)
          @controller.expects(:setup_voice_recharge_placeholder_subscription).once
          assert_nil account.voice_recharge_settings
          post :create, params: { recharge_settings: { enabled: true, amount: 100.00 } }
        end

        it('responds with created') { assert_response :created }

        should_use_presenter Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter

        it "creates the account's voice recharge settings" do
          assert account.voice_recharge_settings.enabled
          assert_equal 100.00, account.voice_recharge_settings.amount
        end
      end

      it 'defaults enabled param to true' do
        @controller.expects(:update_recharge_settings).with('enabled' => true, 'amount' => 50).once
        @controller.expects(:setup_voice_recharge_placeholder_subscription).once
        post :create, params: { recharge_settings: { amount: 50 } }
      end
    end

    describe "#show" do
      describe "when account has voice recharge settings" do
        before do
          account.create_voice_recharge_settings(
            enabled: true,
            amount: 50.00,
            minimum_balance: 5.00
          )
          get :show
        end

        it('responds with success') { assert_response :success }

        should_use_presenter Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter

        it "renders voice recharge settings details" do
          actual   = JSON.parse(response.body).deep_symbolize_keys
          expected = {
            enabled: true,
            minimum_balance: 5.00,
            amount: 50.00,
            balance: 0.00,
            payment_status: nil
          }
          assert_equal expected, actual[:recharge_settings]
        end
      end

      describe "when account does not have voice recharge settings" do
        before { get :show }

        it('responds with not_found') { assert_response :not_found }

        it "renders error message" do
          actual   = JSON.parse(response.body).symbolize_keys
          expected = {
            error: 'RecordNotFound',
            description: 'Not found'
          }
          assert_equal expected, actual
        end
      end
    end

    describe "#update" do
      describe 'params' do
        before do
          account.create_voice_recharge_settings(
            enabled: true,
            amount: 50.00,
            minimum_balance: 5.00
          )
        end

        describe 'when empty' do
          before do
            put :update, params: { recharge_settings: { } }
          end

          it('responds with bad_request') { assert_response :bad_request }
        end
      end

      describe "when account has voice recharge settings" do
        before do
          account.create_voice_recharge_settings(
            enabled: true,
            amount: 50.00,
            minimum_balance: 5.00
          )
        end

        describe "with enabled false" do
          before do
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_enabled__c).returns('No')
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_amount__c).returns(50.0)

            put :update, params: { recharge_settings: { enabled: false, amount: 50.0 } }
          end

          it('responds with success') { assert_response :success }

          should_use_presenter Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter

          it "sets enabled to false" do
            refute account.voice_recharge_settings.reload.enabled
          end
        end

        describe "with amount 100" do
          before do
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_enabled__c).returns('Yes')
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_amount__c).returns(100.0)

            put :update, params: { recharge_settings: { enabled: true, amount: 100.00 } }
          end

          it('responds with success') { assert_response :success }

          should_use_presenter Api::V2::Account::VoiceSubscription::RechargeSettingsPresenter

          it "sets amount to 100.00 and minimum balance to 10" do
            assert_equal 100.00, account.voice_recharge_settings.reload.amount.to_f
            assert_equal 10.00, account.voice_recharge_settings.reload.minimum_balance
          end
        end

        describe 'and update fails' do
          before do
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_enabled__c).returns('Yes')
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_amount__c).returns(100.0)
            synchronizer.stubs(:synchronize_voice_recharge_settings!).returns(false)

            put :update, params: { recharge_settings: { enabled: true, amount: 100.00 } }
          end

          it('returns unprocessable entity error') { assert_response :unprocessable_entity }
        end
      end

      describe "when account does not have voice recharge settings" do
        describe "with amount 100" do
          before do
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_enabled__c).returns('No')
            synchronizer.zuora_account.stubs(:get_custom_field).with(:voice_recharge_amount__c).returns(nil)

            Voice::RechargeSettings.any_instance.expects(:update_attributes).never
            put :update, params: { recharge_settings: { enabled: true, amount: 100.00 } }
          end

          it('responds with not_found error') { assert_response :not_found }
        end
      end
    end

    describe '#recharge' do
      describe 'when no recharge settings exist' do
        before do
          post :recharge
        end

        it('responds with not_found error') { assert_response :not_found }
      end

      describe 'when account has recharge settings' do
        before do
          account.create_voice_recharge_settings(
            enabled: true,
            amount: 50.00,
            minimum_balance: 5.00
          )
        end

        describe 'when no errors occur' do
          it 'initiates recharge' do
            @controller.expects(:retry_recharge!).once
            post :recharge
          end

          it 'returns created status' do
            post :recharge
            assert_response :created
          end
        end

        describe 'when an error occurs' do
          before do
            ZBC::Voice::UnprocessedUsage.expects(:create!).raises(ActiveRecord::RecordInvalid.new(ZBC::Voice::UnprocessedUsage.new))
            post :recharge
          end

          it('returns unprocessable_entity error status') { assert_response :unprocessable_entity }
        end
      end
    end

    describe '#setup_voice_recharge_placeholder_subscription' do
      subject { @controller.send(:setup_voice_recharge_placeholder_subscription) }

      describe 'when there is no existing subscription' do
        it 'creates voice recharge placeholder subscription' do
          ZBC::Zuora::VoiceRecharge.any_instance.expects(:subscribe!).returns('subscription created')
          subject.must_equal 'subscription created'
        end
      end

      describe 'when there is an existing subscription' do
        let(:expected_error) { ZBC::Zuora::VoiceRecharge::RechargeSubscriptionAlreadyPresent }
        let(:voice_recharge_subscription) { stub }

        before do
          ZBC::Zuora::VoiceRecharge.any_instance.expects(:voice_recharge_subscription).returns(voice_recharge_subscription)
        end

        it 'raises a SubscriptionAlreadyPresent error' do
          assert_raises expected_error do
            subject
          end
        end
      end
    end

    describe 'when account is trial' do
      describe '#create' do
        before do
          Account.any_instance.stubs(:zuora_subscription)
          Voice::RechargeSettings.any_instance.expects(:update_attributes).never
          post :create, params: { recharge_settings: { enabled: true, amount: 100.00 } }
        end

        it('responds with not_found error') { assert_response :not_found }
      end

      describe '#update' do
        before do
          account.create_voice_recharge_settings(
            enabled: true,
            amount: 50.00,
            minimum_balance: 5.00
          ) # required to avoid the 404 error response if calling update when no RechargeSettings exist
          Account.any_instance.stubs(:zuora_subscription)
          Voice::RechargeSettings.any_instance.expects(:update_attributes).never
          put :update, params: { recharge_settings: { enabled: true, amount: 100.00 } }
        end

        it('responds with not_found error') { assert_response :not_found }
      end
    end
  end
end
