require_relative "../../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::Voice::UsagesController do
  fixtures :all

  let(:statsd_client) { stub }

  before do
    accept :json
    @account = accounts(:minimum)
    @request.account = @account
    ZBC::Zuora::Jobs::UpdateVoiceSubAccountJob.stubs(:enqueue)
    Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
  end

  as_an_admin do
    should_be_forbidden :create
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#create" do
      describe 'when :serialize_voice_usage Arturo is OFF' do
        before do
          Arturo.disable_feature!(:serialize_voice_usage)
        end

        it "creates a voice usage record" do
          assert_difference 'ZBC::Zuora::VoiceUsage.count(:all)', 1 do
            post :create, params: { usage: {
              units: -100,
              usage_type: 'charge',
              zuora_reference_id: "abc123t",
              voice_reference_id: "voice_ref"
            } }
            assert_response :success
          end
        end

        describe "duplicate voice_reference_id" do
          before do
            @account.zuora_voice_usages.create!(
              units: 100,
              usage_type: 'credit',
              voice_reference_id: "DUPLICATE"
            )
          end

          it "is rejected" do
            post :create, params: { usage: {
              units: 100,
              usage_type: 'credit',
              voice_reference_id: "DUPLICATE"
            } }
            assert_response :unprocessable_entity
          end
        end

        describe "duplicate zuora_reference_id" do
          before do
            @account.zuora_voice_usages.create!(
              units: 100,
              usage_type: 'credit',
              zuora_reference_id: "DUPLICATE"
            )
          end

          it "is rejected" do
            post :create, params: { usage: {
              units: 100,
              usage_type: 'credit',
              zuora_reference_id: "DUPLICATE"
            } }
            assert_response :unprocessable_entity
          end
        end
      end

      describe 'when :serialize_voice_usage Arturo is ON' do
        before do
          Arturo.enable_feature!(:serialize_voice_usage)
        end

        it "creates a voice usage record" do
          assert_difference 'ZBC::Zuora::VoiceUsage.count(:all)', 1 do
            post :create, params: { usage: {
              units: -100,
              usage_type: 'charge',
              zuora_reference_id: "abc123t",
              voice_reference_id: "voice_ref"
            } }
            assert_response :success
          end
        end

        describe "duplicate voice_reference_id" do
          before do
            @account.zuora_voice_usages.create!(
              units: 100,
              usage_type: 'credit',
              voice_reference_id: "DUPLICATE"
            )
          end

          it "is rejected" do
            post :create, params: { usage: {
              units: 100,
              usage_type: 'credit',
              voice_reference_id: "DUPLICATE"
            } }
            assert_response :unprocessable_entity
          end
        end

        describe "duplicate zuora_reference_id" do
          before do
            @account.zuora_voice_usages.create(
              units: 100,
              usage_type: 'credit',
              zuora_reference_id: "DUPLICATE"
            )
          end

          it "is rejected" do
            post :create, params: { usage: {
              units: 100,
              usage_type: 'credit',
              zuora_reference_id: "DUPLICATE"
            } }
            assert_response :unprocessable_entity
          end
        end
      end
    end
  end
end
