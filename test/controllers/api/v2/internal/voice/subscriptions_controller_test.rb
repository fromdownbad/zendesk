require_relative "../../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::Voice::SubscriptionsController do
  extend Api::V2::TestHelper

  before do
    accept :json
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#show" do
      describe "on an account with voice" do
        before { get :show }
        should_use_presenter Api::V2::Account::VoiceSubscriptionPresenter, status: :ok
      end
    end
  end
end
