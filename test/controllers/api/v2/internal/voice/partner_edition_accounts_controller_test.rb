require_relative "../../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Internal::Voice::PartnerEditionAccountsController do
  extend Api::V2::TestHelper

  fixtures :all

  before do
    accept :json
    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
  end

  as_an_admin do
    should_be_forbidden :show
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#show" do
      describe "on an account without voice_partner_edition_account" do
        before { get :show }
        it "should return 404 response" do
          assert_response :not_found
        end
      end

      describe "on an account with voice_partner_edition_account" do
        before do
          FactoryBot.create(:voice_partner_edition_account, account_id: @account.id)
          get :show
        end

        should_use_presenter Api::V2::Account::VoicePartnerEditionAccountPresenter, status: :ok
      end
    end

    describe "#update" do
      describe "on an account without voice_partner_edition_account" do
        before { put :update }
        it "should return 404 response" do
          assert_response :not_found
        end
      end

      describe "on an account with voice_partner_edition_account" do
        before do
          FactoryBot.create(:voice_partner_edition_account, account_id: @account.id)
        end

        it 'returns 200 for a valid update' do
          response = put :update, params: { voice_partner_edition_account: { active: false } }
          response = JSON.parse response.body

          assert_response :ok
          assert_equal(response['voice_partner_edition_account']['active'], false)
        end
      end
    end
  end
end
