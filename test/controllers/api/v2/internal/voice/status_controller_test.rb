require_relative "../../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Internal::Voice::StatusController do
  fixtures :all

  before do
    accept :json
    @account = accounts(:minimum)
    @request.account = @account
  end

  as_an_admin do
    should_be_forbidden :update
  end

  as_a_subsystem_user(account: :minimum) do
    before do
      @settings = @account.settings
    end

    describe "update with enable = true" do
      before do
        @settings.voice = false
        @settings.save
      end

      it "enables voice" do
        put :update, params: { enabled: true }
        @settings.reload
        assert_equal "true", @settings.voice
      end
    end

    describe "update with enable = false" do
      before do
        @settings.voice = true
        @settings.save
      end

      it "enables voice" do
        put :update, params: { enabled: false }
        @settings.reload
        assert_equal "false", @settings.voice
      end
    end
  end
end
