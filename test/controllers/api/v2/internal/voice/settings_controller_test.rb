require_relative '../../../../../support/test_helper'

SingleCov.covered! uncovered: 1

describe Api::V2::Internal::Voice::SettingsController do
  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    accept :json
  end

  as_an_admin do
    should_be_forbidden :update
  end

  as_a_subsystem_user(account: :minimum) do
    let(:account) { accounts(:minimum) }

    describe '#update' do
      describe 'when saving works' do
        before do
          put :update, params: { settings: { voice: true, voice_outbound_enabled: false, voice_maximum_queue_size: 0 } }
        end

        it('responds with 404') { assert_response :not_found }
      end
    end

    describe '#cleanup_legacy_settings' do
      it 'deletes the voice settings' do
        account.settings.set(voice_agent_wrap_up_after_calls: true, voice_only_during_business_hours: true)
        account.settings.save!

        delete :cleanup_legacy_settings, params: { settings: %w[voice_agent_wrap_up_after_calls voice_only_during_business_hours] }

        assert_equal 200, response.status
        assert_equal 0, account.settings.where(name: %w[voice_agent_wrap_up_after_calls voice_only_during_business_hours]).count(:all)
      end

      it 'rejects an empty settings' do
        delete :cleanup_legacy_settings, params: { settings: '   ' }

        assert_equal 400, response.status
      end

      it 'rejects non-voice settings' do
        delete :cleanup_legacy_settings, params: { settings: 'property_sets' }

        assert_equal 422, response.status
      end
    end

    describe 'billing_periods' do
      before do
        Timecop.freeze('2015-11-04 10:00:00')
      end

      describe 'with invoice' do
        before do
          Account.any_instance.stubs(:zuora_managed?).returns(true)
          Subscription.any_instance.stubs(:invoicing?).returns(true)
          Subscription.any_instance.stubs(:pricing_model_revision).returns(2)
        end

        describe 'when the sub account is less than 5 months old' do
          it 'generates periods for each month starting with the sub account creation' do
            FactoryBot.create(:voice_sub_account, account: account, created_at: '2015-09-15 10:00:00')
            get :billing_periods

            assert_equal 200, response.status

            expected_periods = [
              {'label' => 'Nov 1 to Present', 'range' => [1446336000, 1448927999]},
              {'label' => 'Oct 1 to Nov 1',   'range' => [1443657600, 1446335999]},
              {'label' => 'Sep 1 to Oct 1',   'range' => [1441065600, 1443657599]},
              {'label' => 'Before Sep 1',     'range' => [0,          1441065600]}
            ]
            assert_equal expected_periods, assigns(:periods).map(&:stringify_keys)
          end
        end

        describe 'when the sub account is more than 5 months old' do
          it 'generates periods for each month starting 5 months ago' do
            FactoryBot.create(:voice_sub_account, account: account, created_at: '2015-01-01 10:00:00')

            get :billing_periods

            assert_equal 200, response.status

            expected_periods = [
              {'label' => 'Nov 1 to Present', 'range' => [1446336000, 1448927999]},
              {'label' => 'Oct 1 to Nov 1',   'range' => [1443657600, 1446335999]},
              {'label' => 'Sep 1 to Oct 1',   'range' => [1441065600, 1443657599]},
              {'label' => 'Aug 1 to Sep 1',   'range' => [1438387200, 1441065599]},
              {'label' => 'Jul 1 to Aug 1',   'range' => [1435708800, 1438387199]},
              {'label' => 'Jun 1 to Jul 1',   'range' => [1433116800, 1435708799]},
              {'label' => 'Before Jun 1',     'range' => [0,          1433116800]}
            ]
            assert_equal expected_periods, assigns(:periods).map(&:stringify_keys)
          end
        end
      end

      describe 'without invoice' do
        before do
          Account.any_instance.stubs(:zuora_managed?).returns(false)
          Subscription.any_instance.stubs(:invoicing?).returns(false)
        end

        it 'returns an empty array when there is no voice payments' do
          get :billing_periods

          assert_equal 200, response.status
          assert_equal [], assigns(:periods).map(&:stringify_keys)
        end
      end

      it 'returns voice payments and adds current period if missing' do
        FactoryBot.create(:voice_payment, account: account, period_begin_at: '2015-10-01', period_end_at: '2015-11-01')
        get :billing_periods

        assert_equal 200, response.status
        expected_periods = [
          {'label' => 'Nov 1 to Present', 'range' => [1446336000, 1446631200]},
          {'label' => 'Oct 1 to Nov 1',   'range' => [1443657600, 1446336000]},
          {'label' => 'Before Oct 1',     'range' => [0, 1443657600]}
        ]
        assert_equal expected_periods, assigns(:periods).map(&:stringify_keys)
      end

      it 'returns voice payments including the current one' do
        FactoryBot.create(:voice_payment, account: account, period_begin_at: '2015-10-01', period_end_at: '2015-11-01')
        FactoryBot.create(:voice_payment, account: account, period_begin_at: '2015-11-01', period_end_at: '2015-12-01')

        get :billing_periods

        assert_equal 200, response.status
        expected_periods = [
          {'label' => 'Nov 1 to Present', 'range' => [1446336000, 1448928000]},
          {'label' => 'Oct 1 to Nov 1',   'range' => [1443657600, 1446336000]},
          {'label' => 'Before Oct 1',     'range' => [0, 1443657600]}
        ]
        assert_equal expected_periods, assigns(:periods).map(&:stringify_keys)
      end
    end
  end
end
