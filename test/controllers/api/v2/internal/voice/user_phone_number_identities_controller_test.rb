require_relative "../../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::Voice::UserPhoneNumberIdentitiesController do
  fixtures :all

  before do
    accept :json
    @account = accounts(:minimum)
    @request.account = @account
  end

  as_an_admin do
    should_be_forbidden :update
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#update" do
      describe "when identity is found" do
        it "returns 200" do
          identity = UserPhoneNumberIdentity.new
          UserPhoneNumberIdentity.expects(:where).returns [identity]
          identity.expects(:update_sms_capability).with(capable: true)

          put :update, params: { id: 1, identity: {
            sms_capable: true
          } }

          assert_response :success
        end
      end

      describe "when identity is NOT found" do
        it "returns 404" do
          UserPhoneNumberIdentity.expects(:where).returns []

          put :update, params: { id: 1, identity: {
            sms_capable: true
          } }

          assert_response :not_found
        end
      end

      describe "when sms_capable is not passed" do
        it "returns 400" do
          put :update, params: { id: 1 }

          assert_response :bad_request
        end
      end
    end
  end
end
