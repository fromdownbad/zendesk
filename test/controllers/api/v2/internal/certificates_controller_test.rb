require_relative "../../../../support/test_helper"
require_relative "../../../../support/certificate_test_helper"

SingleCov.covered!

describe Api::V2::Internal::CertificatesController do
  extend Api::V2::TestHelper
  include CertificateTestHelper

  fixtures :accounts, :certificate_ips, :certificates

  before do
    use_ssl

    # All of these could happen when Exodus has locked an account
    certificates(:active1).account.lock!

    # Accept requests to https://pod-X.zendesk.com
    request.env['zendesk.account'] = nil
  end

  def assert_certificate(certificate_fixture, json)
    Account.without_locking do
      cert = certificates(certificate_fixture)
      assert_equal cert.account.subdomain, json['subdomain']
      assert_equal cert.key, json['key']
      assert_equal cert.crt_chain, json['crt']

      if cert.sni_enabled
        assert(json['sni'])
      else
        assert_equal false, json['sni']
        assert_includes json['conf'], cert.certificate_ips.map(&:ip).join
      end
    end
  end

  describe "routes" do
    with_options(controller: "api/v2/internal/certificates") do |request|
      request.should_route :get, "/api/v2/internal/certificates", action: "index"
      request.should_route :get, "/api/v2/internal/certificates/1", action: "show", id: "1"
      request.should_route :delete, "/api/v2/internal/certificates/1", action: "destroy", id: "1"
      request.should_route :post, "/api/v2/internal/certificates/1/activate", action: "activate", id: "1"
    end
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#index" do
      let(:sni_certificate) { certificates(:active_sni) }
      before do
        assert_equal 1, sni_certificate.account.pod_id
        assert_equal 1, sni_certificate.certificate_ips[0].pod_id
      end

      it "fails with missing pod_id" do
        get :index, format: :json
        assert_response :bad_request
      end

      it "downloads empty JSON since no active certificates will be returned for bad pod_id" do
        get :index, params: { pod_id: '3', format: :json }
        assert_response :success
        certs = JSON.load(@response.body)
        certs.reject! { |c| c['sni'] }
        assert_equal 0, certs.length
      end

      it "downloads JSON with 2 certificates given pod_id of 1" do
        get :index, params: { pod_id: '1', format: :json }

        assert_response :success
        certs = JSON.load(@response.body)
        assert_equal 2, certs.length

        # check the certs
        assert_certificate('active_sni', certs[0])
        assert_certificate('active1', certs[1])
      end

      it "downloads JSON with 2 certificates given pod_id of 1 and updated_since before the certificate's" do
        get :index, params: { pod_id: '1', updated_since: "2011-12-31T23:59:59+00:00", format: :json }

        assert_response :success
        certs = JSON.load(@response.body)
        assert_equal 2, certs.length

        # check the certs
        assert_certificate('active_sni', certs[0])
        assert_certificate('active1', certs[1])
      end

      it "downloads empty list given pod_id of 1 and updated_since after the certificate's" do
        get :index, params: { pod_id: '1', updated_since: "2012-01-02T00:00:00+00:00", format: :json }

        assert_response :success
        certs = JSON.load(@response.body)
        assert_equal 0, certs.length
      end

      it "downloads JSON with 1 certificate given pod_id of 2" do
        get :index, params: { pod_id: '2', format: :json }

        assert_response :success
        certs = JSON.load(@response.body)
        assert_equal 1, certs.length

        # check the certs
        assert_certificate('active2', certs[0])
      end

      it "releases scheduled ips" do
        CertificateIp.expects(:release_scheduled_ips!).with(1)
        get :index, params: { updated_since: "2012-01-02T00:00:00+00:00", pod_id: 1, format: :json }
      end

      it "includes SNI certificates" do
        sni_certificate = certificates(:active_sni)
        get :index, params: { pod_id: '1', format: :json }

        cert = json.detect { |j| j["subdomain"] == sni_certificate.account.subdomain }
        assert cert, "did not include SNI certificate"
      end

      describe 'when a certificate is corrupted' do
        before do
          sni_certificate = certificates(:active_sni)
          sni_certificate.crt = "abc"
          sni_certificate.save!
        end

        it "does not serve it to nginx" do
          get :index, params: { pod_id: '1', format: :json }
          cert = json.detect { |j| j["subdomain"] == sni_certificate.account.subdomain }

          assert_nil cert
        end
      end
    end

    describe "#destroy" do
      describe "without an active certificate" do
        let(:certificate) { accounts(:minimum).certificates.pending.first }

        it "returns 404" do
          delete :destroy, params: { id: certificate.id, format: :json }
          assert_response :not_found
        end
      end

      describe "with an active certificate" do
        let(:account) { accounts(:minimum) }
        let(:certificate) do
          c = certificates(:active1)
          c.account = account
          c.tap(&:save!)
        end

        it "revokes the certificate and removes the ips" do
          delete :destroy, params: { id: certificate.id, format: :json }
          assert_response :no_content

          certificate.reload
          assert_equal 'revoked', certificate.state
        end

        describe "with automatic_certificate_provisioning enabled" do
          before do
            account.settings.automatic_certificate_provisioning = true
            account.settings.save!
          end

          it "disables automatic_certificate_provisioning if the certificate is autoprovisioned" do
            certificate.update_attribute(:autoprovisioned, true)
            delete :destroy, params: { id: certificate.id, format: :json }
            assert_response :no_content

            certificate.reload
            assert_equal 'revoked', certificate.state
            assert_equal false, account.reload.settings.automatic_certificate_provisioning
          end

          it "does not disable automatic_certificate_provisioning for customer certs" do
            delete :destroy, params: { id: certificate.id, format: :json }
            assert_response :no_content

            certificate.reload
            assert_equal 'revoked', certificate.state
            assert(account.reload.settings.automatic_certificate_provisioning)
          end
        end
      end
    end

    describe "#activate" do
      let(:account) { accounts(:minimum) }

      describe "with an already active certificate" do
        let(:certificate) do
          c = certificates(:active1)
          c.account = account
          c.tap(&:save!)
        end

        it "raises Certificate::CertificateAlreadyActive" do
          assert_raises(Certificate::CertificateAlreadyActive) do
            post :activate, params: { id: certificate.id, format: :json }
          end
        end
      end

      describe "with a pending certificate" do
        let(:certificate) do
          c = certificates(:pending)
          c.account = account
          c.tap(&:save!)
        end
        let(:certificate_ip) { certificate_ips(:one) }

        before do
          ActionMailer::Base.perform_deliveries = true
          ActionMailer::Base.deliveries.clear
          post :activate, params: { id: certificate.id, certificate_ip_id: certificate_ip.id, format: :json }
        end

        it "activates the certificate" do
          assert_response :ok

          certificate.reload.state.must_equal 'active'
          certificate_ip.reload.certificate.must_equal certificate
        end

        it "notifies the customer" do
          ActionMailer::Base.deliveries.size.must_equal 1
          ActionMailer::Base.deliveries.last.subject.must_match /^Zendesk SSL certificate approved for/
        end
      end
    end

    describe "#show" do
      describe "without active certificate" do
        it "returns 404" do
          get :show, params: { id: certificates(:pending).account_id, format: :json }
          assert_response :not_found
        end
      end

      describe "with active certificate" do
        it "returns the certificate" do
          get :show, params: { id: certificates(:active1).account_id, format: :json }
          assert_response :success
          assert_equal json['certificate']['ip'], certificates(:active1).certificate_ips.first.ip
        end

        it "returns the host mapped routes that are included in the certificate" do
          cert = certificates(:active_sni)
          cert.account.update_attribute(:host_mapping, 'example.com')
          cert.account.routes.create!(subdomain: 'abc123', host_mapping: 'abc123.example.com')
          cert.crt = self_sign(new_csr('example.com')).to_pem
          cert.save!

          get :show, params: { id: cert.account_id, format: :json }
          assert_response :success
          assert_equal ['example.com'], json['certificate']['host_mappings']
          assert_equal ['example.com'], json['certificate']['crt_covered_host_mappings']
          assert_equal ['example.com', 'abc123.example.com'], json['certificate']['all_host_mappings']
        end
      end

      describe "with an sni cert" do
        before do
          get :show, params: { id: certificates(:active_sni).account_id, format: :json }
          assert_response :success
        end

        it "exposes the sni status" do
          assert(json['certificate']['sni'])
        end

        it "does not return an ip address or port" do
          assert_nil json['certificate']['ip']
          assert_nil json['certificate']['port']
        end
      end
    end
  end
end
