require File.expand_path '../../../../support/test_helper', File.dirname(__FILE__)

SingleCov.covered!

describe Api::V2::Internal::CouponsController do
  extend Api::V2::TestHelper

  fixtures :all

  before do
    accept :json
    @request.account = accounts(:minimum)
  end

  as_a_subsystem_user(account: :minimum) do
    describe 'with :deprecate_classic_coupons_controller' do
      before { Arturo.enable_feature!(:deprecate_classic_coupons_controller) }

      let(:options) do
        {
          type:   "classic",
          format: :json,
          id:     12313
        }
      end
      describe '#index action' do
        it 'returns `410 gone`' do
          get :index, params: { type: "classic", format: :json }
          assert_response :gone
        end
      end

      describe '#edit action' do
        it 'returns `410 gone`' do
          get :edit, params: options
          assert_response :gone
        end
      end

      describe '#show action' do
        it 'returns `410 gone`' do
          get :show, params: options
          assert_response :gone
        end
      end

      describe '#update action' do
        it 'returns `410 gone`' do
          put :update, params: options
          assert_response :gone
        end
      end

      describe '#new action' do
        it 'returns `410 gone`' do
          get :new, params: options
          assert_response :gone
        end
      end

      describe '#create action' do
        it 'returns `410 gone`' do
          post :create, params: options
          assert_response :gone
        end
      end

      describe '#destroy action' do
        it 'returns `410 gone`' do
          delete :destroy, params: options
          assert_response :gone
        end
      end
    end

    it 'complains if coupon type is not specified' do
      get :index, format: :json
      refute assigns(:coupon_type)
      assert_response :bad_request
    end

    it 'complains if coupon type is not classic or zuora' do
      get :index, params: { type: "other", format: :json }
      assert_response :bad_request
    end

    it 'identifys coupon type' do
      get :index, params: { type: "classic", format: :json }
      assert coupon_type = assigns(:coupon_type)
      assert_equal :classic, coupon_type
      assert_response :ok
    end

    describe 'when coupon_type is classic' do
      before do
        @coupon = coupons(:sample)
        @opts = { type: "classic", format: :json }
      end

      describe '#index action' do
        before do
          get :index, params: @opts
        end

        it 'responds with HTTP 200' do
          assert_response :ok
        end

        it 'fetchs list of coupons' do
          assert coupons = assigns(:coupons)
          assert coupons.is_a? Array
          assert coupons.first.is_a? Coupon
        end
      end

      describe '#show action' do
        before do
          get :show, params: @opts.merge(id: @coupon.id)

          assert_response 200, response.body
        end

        it 'responds with `HTTP 200`' do
          assert_response :ok
        end

        it 'finds the coupon' do
          assert coupon = assigns(:coupon)
          assert coupon.is_a? Coupon
          assert_equal coupon, @coupon
        end

        it 'renders the coupon' do
          coupon = JSON.parse(@response.body)['coupon']
          assert_equal @coupon.id, coupon['id']
        end
      end

      describe '#edit action' do
        it 'does not be allowed' do
          get :edit, params: @opts.merge(id: @coupon.id)
          refute assigns(:coupons)
          assert_response :method_not_allowed
        end
      end

      describe '#new action' do
        it 'does not be allowed' do
          get :new, params: @opts
          refute assigns(:coupons)
          assert_response :method_not_allowed
        end
      end

      describe '#destroy action' do
        before do
          delete :destroy, params: @opts.merge(id: @coupon.id)
        end

        it 'does not be allowed' do
          refute assigns(:coupons)
          assert_response :method_not_allowed
        end

        it 'does not delete the coupon' do
          assert Coupon.find_by_id(@coupon.id)
        end
      end

      describe '#create action' do
        before do
          data = coupons(:sample).attributes.with_indifferent_access
          data[:name] = 'XXX'
          data[:coupon_code] = 'XXX'
          post :create, params: @opts.merge(coupon: data)
        end

        it 'does not be allowed' do
          refute assigns(:coupons)
          assert_response :method_not_allowed
        end

        it 'does not create the coupon' do
          refute Coupon.find_by_name_and_coupon_code('XXX', 'XXX')
        end
      end

      describe '#update action' do
        before do
          data = @coupon.attributes.with_indifferent_access
          data[:coupon_code] = 'XXX'
          put :update, params: @opts.merge(coupon: data, id: 1)
        end

        it 'does not be allowed' do
          refute assigns(:coupons)
          assert_response :method_not_allowed
        end

        it 'does not update the coupon' do
          refute Coupon.find_by_coupon_code('XXX')
        end
      end
    end

    describe 'when coupon_type is zuora' do
      before do
        @coupon = zuora_coupons(:sample)
        @opts = { type: "zuora", format: :json }
      end

      describe '#index action' do
        before do
          get :index, params: @opts
        end

        it 'responds with `HTTP 200`' do
          assert_response :ok
        end

        it 'fetchs list of coupons' do
          assert coupons = assigns(:coupons)
          assert coupons.is_a? Array
          assert coupons.first.is_a? ZendeskBillingCore::Zuora::Coupon
        end
      end

      describe '#show action' do
        before do
          get :show, params: @opts.merge(id: @coupon.id)
        end

        it 'responds with `HTTP 200`' do
          assert_response :ok
        end

        it 'finds the coupon' do
          assert coupon = assigns(:coupon)
          assert coupon.is_a? ZendeskBillingCore::Zuora::Coupon
          assert_equal coupon, @coupon
        end

        it 'renders the coupon' do
          coupon = JSON.parse(@response.body)['coupon']
          assert_equal @coupon.id, coupon['id']
          assert_not_nil coupon['forced_inactive']
          assert_not_nil coupon['in_effective_window?']
          assert_not_nil coupon['after_effective_window?']
        end
      end

      describe '#edit action' do
        describe 'when coupon is editable' do
          before do
            ZendeskBillingCore::Zuora::Coupon.any_instance.stubs(:editable?).returns(true)
            get :edit, params: @opts.merge(id: @coupon.id)
          end

          it 'responds with `HTTP 200`' do
            assert_response :ok
          end

          it 'finds the coupon' do
            assert coupon = assigns(:coupon)
            assert coupon.is_a? ZendeskBillingCore::Zuora::Coupon
            assert_equal coupon, @coupon
          end

          it 'renders the coupon' do
            coupon = JSON.parse(@response.body)['coupon']
            assert_equal @coupon.id, coupon['id']
          end
        end

        describe 'when coupon is not editable' do
          before do
            ZendeskBillingCore::Zuora::Coupon.any_instance.stubs(:editable?).returns(false)
          end

          it 'does not fetch the coupon' do
            get :edit, params: @opts.merge(id: @coupon.id)
            assert_response :method_not_allowed
          end
        end
      end

      describe '#new action' do
        before do
          data = { name: 'XXX', coupon_code: 'XXX', start_date: Date.today, end_date: 1.year.from_now.to_date }
          get :new, params: @opts.merge(coupon: data)
        end

        it 'responds with `HTTP 200`' do
          assert_response :ok
        end

        it 'renders the coupon' do
          coupon = JSON.parse(@response.body)['coupon']
          assert_equal 'XXX', coupon['name']
          assert_equal 'XXX', coupon['coupon_code']
        end
      end

      describe '#destroy action' do
        describe 'when it succeeds' do
          before do
            @coupon.coupon_redemptions.each(&:delete)
            assert @coupon.editable?
          end

          it 'responds with `HTTP 204`' do
            delete :destroy, params: @opts.merge(id: @coupon.id)
            assert_response :no_content
          end

          it 'deletes the coupon' do
            delete :destroy, params: @opts.merge(id: @coupon.id)
            refute ZendeskBillingCore::Zuora::Coupon.find_by_id(@coupon.id)
          end

          it 'does not render the deleted coupon' do
            delete :destroy, params: @opts.merge(id: @coupon.id)
            assert @response.body.blank?
          end
        end

        describe 'when it fails' do
          before do
            ZendeskBillingCore::Zuora::Coupon.any_instance.stubs(:destroy).returns(false)
            delete :destroy, params: @opts.merge(id: @coupon.id)
          end

          it 'responds with `HTTP 422`' do
            assert_response :unprocessable_entity
          end

          it 'does not delete the coupon' do
            assert ZendeskBillingCore::Zuora::Coupon.find_by_id(@coupon.id)
          end
        end
      end

      describe '#create action' do
        before do
          @data = {
            name: 'XXX',
            coupon_code: 'XXX',
            discount_type: 'fixed',
            discount_amount: 1,
            duration: 1,
            start_date: 1.day.from_now,
            end_date: 2.days.from_now
          }
        end

        describe 'when it succeeds' do
          before do
            refute ZendeskBillingCore::Zuora::Coupon.find_by_coupon_code(@data[:coupon_code])
            post :create, params: @opts.merge(coupon: @data)
          end

          it 'responds with `HTTP 200`' do
            assert_response :created
          end

          it 'creates the coupon' do
            assert ZendeskBillingCore::Zuora::Coupon.find_by_coupon_code(@data[:coupon_code])
          end

          it 'renders the created coupon' do
            coupon = JSON.parse(@response.body)['coupon']
            assert_equal 'XXX', coupon['name']
            assert_equal 'XXX', coupon['coupon_code']
          end
        end

        describe 'when it fails' do
          before do
            @data[:discount_type] = :other
            post :create, params: @opts.merge(coupon: @data)
          end

          it 'responds with `HTTP 422`' do
            assert_response :unprocessable_entity
          end

          it 'does not create the coupon' do
            refute ZendeskBillingCore::Zuora::Coupon.find_by_coupon_code(@data[:coupon_code])
          end
        end
      end

      describe '#update action' do
        before do
          @data = { discount_type: 'fixed', coupon_code: 'YYY' }
        end

        describe 'when it succeeds' do
          before do
            ZendeskBillingCore::Zuora::Coupon.any_instance.stubs(:editable?).returns(true)
            put :update, params: @opts.merge(id: @coupon.id, coupon: @data)
          end

          it 'responds with `HTTP 200`' do
            assert_response :ok
          end

          it 'updates the coupon' do
            coupon =  ZendeskBillingCore::Zuora::Coupon.find_by_id(@coupon.id)
            assert_equal 'YYY', coupon.coupon_code
          end

          it 'returns the updated coupon' do
            assert coupon = assigns(:coupon)
            assert coupon.is_a? ZendeskBillingCore::Zuora::Coupon
            assert_equal coupon, @coupon
          end

          it 'renders the updated coupon' do
            coupon = JSON.parse(@response.body)['coupon']
            assert_equal 'YYY',   coupon['coupon_code']
            assert_equal 'fixed', coupon['discount_type']
          end
        end

        describe 'when it fails' do
          before do
            ZendeskBillingCore::Zuora::Coupon.any_instance.stubs(:editable?).returns(false)
            put :update, params: @opts.merge(id: @coupon.id, coupon: @data)
          end

          it 'responds with HTTP 422' do
            assert_response :unprocessable_entity
          end

          it 'does not update the coupon' do
            coupon = ZendeskBillingCore::Zuora::Coupon.find_by_id(@coupon.id)
            assert_not_equal 'YYY', coupon.coupon_code
          end
        end
      end

      describe 'when coupon id argument is invalid' do
        before do
          @opts.merge!(id: 42)
        end

        describe '#edit action' do
          it 'responds with `HTTP 404`' do
            get :edit, params: @opts
            assert_response :not_found
          end
        end

        describe 'in #destroy action' do
          it 'responds with `HTTP 404`' do
            delete :destroy, params: @opts
            assert_response :not_found
          end
        end

        describe 'in #update action' do
          it 'responds with `HTTP 404`' do
            put :update, params: @opts.merge(coupon: { name: 'XXX' })
            assert_response :not_found
          end
        end
      end
    end
  end
end
