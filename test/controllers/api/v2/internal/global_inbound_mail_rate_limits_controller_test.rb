require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 0

describe Api::V2::Internal::GlobalInboundMailRateLimitsController do
  extend Api::V2::TestHelper

  with_options(controller: 'api/v2/internal/global_inbound_mail_rate_limits') do |request|
    request.should_route :get, '/api/v2/internal/global_inbound_mail_rate_limits', action: 'index'
  end

  as_a_subsystem_user(account: :support) do
    describe "#index" do
      before do
        get :index, format: :json
      end

      it "responds with success" do
        assert_response :success
      end

      it "returns global inbound mail rate limits" do
        response_body = JSON.parse(@response.body)
        recipient_size = response_body['recipient_exceptions'].length
        sender_size = response_body['sender_exceptions'].length

        assert_equal 11, recipient_size
        assert_equal 52, sender_size
      end
    end
  end
end
