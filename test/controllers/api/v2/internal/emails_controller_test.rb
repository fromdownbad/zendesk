require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::EmailsController do
  extend Api::V2::TestHelper
  fixtures :all
  let(:account) { accounts(:multiproduct) }

  before do
    accept :json
    @request.account = account
  end

  as_an_admin do
    should_be_forbidden(
      [
        :put,
        :deliver_verify,
        { staff_id: 1, email_id: 1, requester_id: 1 }
      ],
      [
        :post,
        :create,
        { email: { type: 'x', value: 'y' }, requester_id: 1, staff_id: 1 }
      ]
    )
  end

  as_a_subsystem_user(user: 'metropolis', account: :multiproduct) do
    let(:email) { user.identities.first }
    let(:other_email) { UserEmailIdentity.create!(user: user, value: 'very_user@example.com') }
    let(:user) { account.owner }

    describe '#create' do
      let(:new_email_value) { 'new_email@example.com' }
      let(:create_payload) { { type: 'email', value: new_email_value } }
      it 'creates a new email identity' do
        post :create, params: { staff_id: user.id, email: create_payload, requester_id: user.id }
        assert_response :created

        created = JSON.parse(@response.body)['email']
        assert_equal new_email_value, created['value']
        assert_equal user.id, created['user_id']
      end

      it 'returns 404 when the user does not exist' do
        post :create, params: { staff_id: 0, email: create_payload, requester_id: user.id }
        assert_response :not_found
        assert_equal 'RecordNotFound', JSON.parse(@response.body)['error']
      end

      it 'returns 422 when creating a duplicate email' do
        duplicate_email = 'duplicate@example.com'
        UserEmailIdentity.create!(user: user, value: duplicate_email)
        create_payload = { type: 'email', value: duplicate_email }
        post :create, params: { staff_id: user.id, email: create_payload, requester_id: user.id }

        assert_response :unprocessable_entity
        assert_equal 'RecordInvalid', JSON.parse(@response.body)['error']
      end

      describe 'when missing the requester_id field' do
        it 'returns a 400 with MissingRequesterId error when missing the requester_id' do
          post :create, params: { staff_id: user.id }
          assert_response 400
          body = JSON.parse(@response.body)

          assert_equal 'MissingRequesterId', body['error']
          assert_equal "'requester_id' is a required field", body['description']
        end
      end
    end

    describe '#update' do
      let(:updated_value) { 'updated_email@example.com' }
      let(:updated_verified) { !email.is_verified }
      let(:update_payload) { { verified: updated_verified, value: updated_value } }

      it 'updates the email identity' do
        put :update, params: { staff_id: user.id, id: other_email.id, email: update_payload, requester_id: user.id }
        assert_response :ok

        updated = JSON.parse(@response.body)['email']
        assert_equal updated_value, updated['value']
        assert_equal updated_verified, updated['verified']
      end

      it 'does not trigger verification if the email is already verified' do
        verified_email = UserEmailIdentity.create!(user: user, value: 'verified@example.com', is_verified: false)
        verified_email.expects(:verify!).never
        put :update, params: { staff_id: user.id, id: verified_email.id, email: { verified: true }, requester_id: user.id }
        assert_response :ok
      end

      it 'returns 404 when the user does not exist' do
        put :update, params: { staff_id: 0, id: email.id, email: update_payload, requester_id: user.id }

        assert_response :not_found
        assert_equal 'RecordNotFound', JSON.parse(@response.body)['error']
      end

      it 'returns 404 when the email does not exist' do
        put :update, params: { staff_id: user.id, id: 0, email: update_payload, requester_id: user.id }
        assert_response :not_found
        assert_equal 'RecordNotFound', JSON.parse(@response.body)['error']
      end

      it 'returns 422 when updating the value to an already existing value' do
        duplicate_email = 'duplicate@example.com'
        UserEmailIdentity.create!(user: user, value: duplicate_email)
        update_payload = { value: duplicate_email }
        put :update, params: { staff_id: user.id, id: email.id, email: update_payload, requester_id: user.id }

        assert_response :unprocessable_entity
        assert_equal 'RecordInvalid', JSON.parse(@response.body)['error']
      end

      describe 'when missing the requester_id field' do
        it 'returns a 400 with MissingRequesterId error when missing the requester_id' do
          put :update, params: { staff_id: 0, id: email.id, email: update_payload }
          assert_response 400
          body = JSON.parse(@response.body)

          assert_equal 'MissingRequesterId', body['error']
          assert_equal "'requester_id' is a required field", body['description']
        end
      end

      describe 'when changing the primary identity and not skipping verification' do
        before do
          Account.any_instance.stubs(has_email_suppress_email_identity_update_notifications?: false)
          Arturo.enable_feature! :new_actor_via_requester_actions
        end

        it 'calls SecurityMailer.deliver_user_primary_email_changed' do
          SecurityMailer.expects(:deliver_user_primary_email_changed).with(user, user, anything)

          put :update, params: { staff_id: user.id, id: user.identities.first.id, email: update_payload, requester_id: user.id }
        end
      end
    end

    describe '#destroy' do
      it 'deletes the email identity' do
        delete :destroy, params: { staff_id: user.id, id: other_email.id, requester_id: user.id }

        assert_response :ok
        assert_equal other_email.id, JSON.parse(@response.body)['email']['id']
      end

      it 'returns 404 when the user does not exist' do
        delete :destroy, params: { staff_id: 0, id: email.id, requester_id: user.id }

        assert_response :not_found
        assert_equal 'RecordNotFound', JSON.parse(@response.body)['error']
      end

      it 'returns 404 when the email does not exist' do
        delete :destroy, params: { staff_id: user.id, id: 0, requester_id: user.id }

        assert_response :not_found
        assert_equal 'RecordNotFound', JSON.parse(@response.body)['error']
      end

      describe 'when missing the requester_id field' do
        it 'returns a 400 with MissingRequesterId error when missing the requester_id' do
          delete :destroy, params: { staff_id: email.id, id: email.id }
          assert_response 400
          body = JSON.parse(@response.body)

          assert_equal 'MissingRequesterId', body['error']
          assert_equal "'requester_id' is a required field", body['description']
        end
      end

      it 'returns 422 when attempting to destroy last email' do
        delete :destroy, params: { staff_id: user.id, id: user.identities.first.id, requester_id: user.id }

        assert_response :unprocessable_entity
        assert_equal 'RecordInvalid', JSON.parse(@response.body)['error']
      end
    end

    describe '#deliver_verify' do
      it 'returns ok when requesting an email verification delivery' do
        put :deliver_verify, params: { staff_id: user.id, email_id: email.id, requester_id: user.id }
        assert_response :ok
      end

      it 'returns 404 when requesting an email verification for a nonexistent user' do
        put :deliver_verify, params: { staff_id: 0, email_id: email.id, requester_id: user.id }
        assert_response :not_found
      end

      it 'returns 404 when requesting an email verification for a nonexistent email' do
        put :deliver_verify, params: { staff_id: user.id, email_id: 0, requester_id: user.id }
        assert_response :not_found
      end

      describe 'when missing the requester_id field' do
        it 'returns a 400 with MissingRequesterId error when missing the requester_id' do
          put :deliver_verify, params: { staff_id: user.id, email_id: 0 }
          assert_response 400
          body = JSON.parse(@response.body)

          assert_equal 'MissingRequesterId', body['error']
          assert_equal "'requester_id' is a required field", body['description']
        end
      end
    end
  end
end
