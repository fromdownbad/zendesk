require_relative "../../../../support/test_helper"
require 'channels/user_mapper'

SingleCov.covered!

describe Api::V2::Internal::UsersController do
  extend Api::V2::TestHelper
  fixtures :all

  let(:account) { accounts(:minimum) }

  before do
    accept :json
    @request.account = account
    @default_current_brand = FactoryBot.create(:brand, subdomain: 'default')
    @controller.stubs(:current_brand).returns(@default_current_brand)
  end

  with_options(controller: "api/v2/internal/users") do |request|
    request.should_route :post, "/api/v2/internal/users/find_or_create_user_by_social_identity", action: 'find_or_create_user_by_social_identity'
    request.should_route :get, "/api/v2/internal/users/find_user_identity_by_email", action: 'find_user_identity_by_email'
    request.should_route :post, "/api/v2/internal/users/create_or_update", action: 'create_or_update'
  end

  as_an_admin do
    should_be_forbidden :find_or_create_user_by_social_identity
    should_be_forbidden :find_user_identity_by_email
    should_be_forbidden :find_user_identity
  end

  as_a_subsystem_user(user: 'zendesk', account: :minimum) do
    describe '#find_user_identity_by_email' do
      describe 'when email identity exists' do
        let!(:user_identity) { user_identities(:minimum_end_user) }

        before { get :find_user_identity_by_email, params: { email: user_identity.value } }

        it 'finds the correct user identity' do
          assert_equal user_identity.id, JSON.parse(@response.body)['identity']['id']
        end

        it 'returns correct identity user_id attribute' do
          assert_equal user_identity.user_id, JSON.parse(@response.body)['identity']['user_id']
        end

        it 'returns correct identity type attribute' do
          assert_equal 'email', JSON.parse(@response.body)['identity']['type']
        end

        it 'returns correct identity value attribute' do
          assert_equal user_identity.value, JSON.parse(@response.body)['identity']['value']
        end

        it 'returns correct identity is_verified attribute' do
          assert_equal user_identity.is_verified, JSON.parse(@response.body)['identity']['verified']
        end

        it 'returns correct identity primary attribute' do
          assert(JSON.parse(@response.body)['identity']['primary'])
        end

        it 'returns correct identity undeliverable_count attribute' do
          assert_equal user_identity.undeliverable_count, JSON.parse(@response.body)['identity']['undeliverable_count']
        end

        it 'returns correct identity deliverable_state attribute' do
          assert_equal 'deliverable', JSON.parse(@response.body)['identity']['deliverable_state']
        end
      end

      describe 'when email identity does not exist' do
        before { get :find_user_identity_by_email, params: { email: 'doesntexist@example.com' } }

        it 'returns a 404 not found' do
          assert_response :not_found
        end
      end
    end

    describe '#find_or_create_user_by_social_identity' do
      let(:allow_creation) { true }

      describe 'facebook' do
        let(:fb_name) { 'Zendeskian Zen' }
        let(:external_id) { '769929266457813' }
        let(:access_token) { stubs(params: { user_id: external_id }) }
        let(:facebook_params) do
          {
            external_id: 'me',
            channel: 'facebook',
            allow_creation: allow_creation,
            access_token: access_token
          }
        end
        let(:fb_identity) do
          UserIdentity.where(value: external_id, type: UserFacebookIdentity)
        end

        before do
          assert_equal 0, fb_identity.count
          stub_request(:get, "https://graph.facebook.com/#{external_id}/picture?type=large")
        end

        describe 'when facebook user is not present' do
          before do
            Channels::FacebookApi::Client.any_instance.stubs(:fetch_user).with('me').returns("name" => fb_name, "id" => external_id)
            post :find_or_create_user_by_social_identity, params: { social_identity: facebook_params }
          end

          it 'creates a user when allowed' do
            assert_equal fb_identity.first.user_id, JSON.parse(@response.body)['user']['id']
          end

          describe 'when creation is not allowed' do
            let(:allow_creation) { false }

            it('does not create a user') { assert_nil JSON.parse(@response.body) }
            it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
          end
        end

        describe 'when already an end_user' do
          let!(:user_identity) { user_identities(:minimum_end_user_facebook) }
          let(:fb_name) { user_identity.user.name }

          before do
            Channels::FacebookApi::Client.any_instance.stubs(:fetch_user).with('me').returns("name" => fb_name, "id" => user_identity.value)
            ::Channels::UserMapper.any_instance.expects(:create_user).never
            post :find_or_create_user_by_social_identity, params: { social_identity: facebook_params }
          end

          it 'finds the end_user record' do
            assert_equal User.find_by_name(fb_name).id, JSON.parse(@response.body)['user']['id']
          end
        end
      end

      describe 'twitter' do
        let(:twitter_name) { 'twitter person' }

        let(:twitter_profile) do
          {
            id:          external_id,
            id_str:      external_id,
            name:        twitter_name,
            screen_name: twitter_name
          }
        end
        let(:twitter_params) do
          {
            external_id:    external_id,
            channel:        'twitter',
            allow_creation: allow_creation
          }
        end

        before do
          Channels::TwitterApi::Client.any_instance.stubs(:fetch_profile).with(external_id).returns(twitter_profile)
        end

        describe 'when twitter user is not present' do
          let(:external_id) { '123456' }
          let(:twitter_identity) do
            UserIdentity.where(value: external_id, type: UserTwitterIdentity)
          end

          before do
            assert_equal 0, twitter_identity.count
            post :find_or_create_user_by_social_identity, params: { social_identity: twitter_params }
          end

          it 'creates a user when allowed' do
            assert_equal twitter_identity.last.user_id, JSON.parse(@response.body)['user']['id']
          end

          describe 'when creation is not allowed' do
            let(:allow_creation) { false }

            it('does not create a user') { assert_nil JSON.parse(@response.body) }
            it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
          end
        end

        describe 'when already an enduser' do
          let(:external_id) { '1' }
          let!(:user_identity) { user_identities(:minimum_end_user_twitter) }
          let(:twitter_name) { user_identity.user.name }

          before do
            ::Channels::UserMapper.any_instance.expects(:create_user).never
            post :find_or_create_user_by_social_identity, params: { social_identity: twitter_params }
          end

          it 'finds the enduser' do
            assert_equal user_identity.user.id, JSON.parse(@response.body)['user']['id']
          end
        end
      end
    end

    describe '#create_or_update' do
      it 'when request has no data gets rejected' do
        post :create_or_update
        expected_response = { 'error' => 'UnknownAttributeError', 'description' => "Invalid attribute: missing user parameter" }
        assert_equal expected_response, JSON.parse(@response.body)
      end

      describe 'CREATE User:' do
        describe 'Without requester_id' do
          describe 'when non existent user id is given in the request' do
            it 'returns a 404 error' do
              post :create_or_update, params: { user: { id: 10000000 } }
              assert_response :not_found
            end

            it 'with name, returns a 404 error' do
              post :create_or_update, params: { user: { id: 10000000, name: 'abc' } }
              assert_response :not_found
            end

            it 'with external_id, returns a 404 error' do
              post :create_or_update, params: { user: { id: 10000000, external_id: 'abcd1234' } }
              assert_response :not_found
            end

            it 'with identities, returns a 404 error' do
              post :create_or_update, params: { user: { id: 1000000, identities: [{ type: 'email', value: 'abcd@example.com' }] } }
              assert_response :not_found
            end

            it 'with name, external_id and identities, returns a 404 error' do
              post :create_or_update, params: { user: { id: 1000000, name: 'abc', external_id: 'abcd1234', identities: [{ type: 'email', value: 'abcd@example.com' }] } }
              assert_response :not_found
            end
          end

          describe 'when name is given in the request' do
            it 'no identifiers given, responds with created ' do
              post :create_or_update, params: { user: { name: 'abc' } }
              assert_response :created
              assert_not_nil JSON.parse(@response.body)['user']['id']
              assert_equal 'abc', JSON.parse(@response.body)['user']['name']
            end

            it 'with external_id, responds with created' do
              user_name = external_id = 'abc_100'
              post :create_or_update, params: { user: { name: user_name, external_id: external_id } }
              assert_response :created
              user_id = JSON.parse(@response.body)['user']['id']
              assert_equal user_name, JSON.parse(@response.body)['user']['name']
              user = @controller.send(:current_account).users.find { |u| u['id'] == user_id }
              assert_not_nil user
              assert_equal external_id, user['external_id']
            end

            it 'with identity, responds with created' do
              post :create_or_update, params: { user: { name: 'abc_101', identities: [{ type: 'email', value: 'abc_101@test.com' }] } }
              assert_response :created
              check_for_identity_value('abc_101@test.com')
            end

            it 'with multiple identities, responds with created' do
              post :create_or_update, params: { user: { name: 'abc_102', identities: [{ type: 'email', value: 'abc_102@test.com' }, { type: 'phone_number', value: '1234567890' }] } }
              assert_response :created
              check_for_identity_value('abc_102@test.com')
              check_for_identity_value('1234567890')
            end

            it 'with external_id and identity, responds with created' do
              post :create_or_update, params: { user: { name: 'abc_103', external_id: 'abc_103', identities: [{ type: 'email', value: 'abc_103@test.com' }] } }
              assert_response :created
            end

            it 'with unknown type of identity, responds with 422 error' do
              error = {
                "error" => "RecordInvalid",
                "description" => "Record validation errors",
                "details" => {
                  "type" => [{ "description" => "Type: zendesk is not a valid identity type", "error" => "UnknownIdentityType" }]
                }
              }

              post :create_or_update, params: { user: { name: 'abc_104', identities: [{ type: 'zendesk', value: 'abc_104@test.com' }] } }
              assert_response :unprocessable_entity
              assert_equal error, JSON.parse(@response.body)
            end

            it 'with empty identity type, responds with 422 error' do
              error = {
                "error" => "RecordInvalid",
                "description" => "Record validation errors",
                "details" => {
                  "type" => [{ "description" => "Identity type is missing", "error" => "MissingValueError" }]
                }
              }

              post :create_or_update, params: { user: { name: 'abc_104', identities: [{ type: '', value: 'abc_104@test.com' }] } }
              assert_response :unprocessable_entity
              assert_equal error, JSON.parse(@response.body)
            end

            it 'with no identity value, responds with 422 error' do
              post :create_or_update, params: { user: { name: 'abc_104', identities: [{ type: 'email' }] } }
              assert_response :unprocessable_entity
            end

            it 'with empty identity value, responds with 422 error' do
              error = {
                "error" => "RecordInvalid",
                "description" => "Record validation errors",
                "details" => {
                  "identity" => [{ "description" => "Invalid value is given for identity", "error" => "InvalidValueError" }]
                }
              }

              post :create_or_update, params: { user: { name: 'abc_104', identities: [{ type: 'phone_number', value: '' }] } }
              assert_response :unprocessable_entity
              assert_equal error, JSON.parse(@response.body)
            end

            describe 'with overwrite_existing_name param' do
              it 'set to sunshine_user, responds with created' do
                post :create_or_update, params: { user: { name: 'abc_134', external_id: 'abc_134', overwrite_existing_name: 'sunshine_user' } }
                assert_response :created
                assert_equal 'abc_134', JSON.parse(@response.body)['user']['name']
              end
            end
          end

          describe 'when name is not given in the request' do
            it 'external_id given, responds with 422 error' do
              post :create_or_update, params: { user: { external_id: 'abc_105' } }
              assert_response :unprocessable_entity
            end

            it 'identity given, responds with 422 error' do
              post :create_or_update, params: { user: { identities: [{ type: 'email', value: 'abc_105@test.com' }] } }
              assert_response :unprocessable_entity
            end
          end
        end

        describe 'With requester_id' do
          it 'should return error when requester_id not found' do
            post :create_or_update, params: { user: { name: 'abc_130' }, requester_id: 100000 }
            assert_response :unprocessable_entity
          end

          describe 'as an admin' do
            before(:all) do
              @admin = users(:minimum_admin)
            end

            it 'when name is not given, responds with error' do
              post :create_or_update, params: { user: { external_id: 'abc_105' }, requester_id: @admin.id }
              assert_response :unprocessable_entity
            end

            it 'when only name is given, responds with created' do
              post :create_or_update, params: { user: { name: 'abc_130' }, requester_id: @admin.id }
              assert_response :created
              assert_not_nil JSON.parse(@response.body)['user']['id']
              assert_equal 'abc_130', JSON.parse(@response.body)['user']['name']
            end

            it 'when name is given with other identifiers, responds with created' do
              params = {
                user: {
                  name: 'abc_131', external_id: 'abc_131', identities: [{ type: 'email', value: 'abc_131@test.com' }], skip_verify_email: false
                },
                requester_id: @admin.id
              }

              post :create_or_update, params: params
              assert_response :created
            end
          end

          describe 'as an agent' do
            before(:all) do
              @agent = users(:minimum_agent)
            end

            it 'when name is not given, responds with error' do
              post :create_or_update, params: { user: { external_id: 'abc_105' }, requester_id: @agent.id }
              assert_response :unprocessable_entity
            end

            it 'when only name is given, responds with created' do
              post :create_or_update, params: { user: { name: 'abc_132' }, requester_id: @agent.id }
              assert_response :created
              assert_not_nil JSON.parse(@response.body)['user']['id']
              assert_equal 'abc_132', JSON.parse(@response.body)['user']['name']
            end
          end

          describe 'as an end-user' do
            before(:all) do
              @end_user = users(:minimum_end_user)
            end

            it 'should not be able to create user' do
              post :create_or_update, params: { user: { name: 'abc_133' }, requester_id: @end_user.id }
              assert_response :forbidden
            end
          end
        end
      end

      describe 'UPDATE User:' do
        describe 'Without requester_id' do
          describe 'when user name is not given' do
            before do
              @user_name = @external_id = 'name_not_given'
              post :create_or_update, params: { user: { name: @user_name, external_id: @external_id, identities: [{ type: 'email', value: "#{@user_name}@test.com" }] } }
              @user_id = JSON.parse(@response.body)['user']['id']
            end

            it 'update via id should not change name and return user object with id and name' do
              post :create_or_update, params: { user: { id: @user_id } }
              assert_response :ok
              assert_equal @user_id, JSON.parse(@response.body)['user']['id']
              assert_equal @user_name, JSON.parse(@response.body)['user']['name']
            end

            it 'update via external_id should not change name and return user object with id and name' do
              post :create_or_update, params: { user: { external_id: @external_id } }
              assert_response :ok
              assert_equal @user_id, JSON.parse(@response.body)['user']['id']
              assert_equal @user_name, JSON.parse(@response.body)['user']['name']
            end

            it 'update via identity should not change name and return user object with id and name' do
              post :create_or_update, params: { user: { identities: [{ type: 'email', value: "#{@user_name}@test.com" }] } }
              assert_response :ok
              assert_equal @user_id, JSON.parse(@response.body)['user']['id']
              assert_equal @user_name, JSON.parse(@response.body)['user']['name']
            end
          end

          describe 'update user name' do
            describe 'via user id' do
              let(:user_name) { 'abc_106' }
              let(:new_name) { 'new_abc_106' }
              before do
                post :create_or_update, params: { user: { name: user_name, external_id: user_name } }
                @user_id = JSON.parse(@response.body)['user']['id']
              end

              it 'should return error when empty user name is given' do
                post :create_or_update, params: { user: { id: @user_id, name: '' } }
                assert_response :unprocessable_entity
              end

              it 'should not change name when old name is given' do
                post :create_or_update, params: { user: { id: @user_id, name: user_name } }
                assert_response :ok
                assert_equal user_name, JSON.parse(@response.body)['user']['name']
              end

              it 'should update name' do
                post :create_or_update, params: { user: { id: @user_id, name: new_name } }
                assert_response :ok
                assert_equal new_name, JSON.parse(@response.body)['user']['name']
              end
            end

            describe 'via external_id' do
              let(:user_name) { 'abc_107' }
              let(:new_name) { 'new_abc_107' }
              before do
                post :create_or_update, params: { user: { name: user_name, external_id: user_name } }
              end

              it 'should return error when empty user name is given' do
                post :create_or_update, params: { user: { external_id: user_name, name: '' } }
                assert_response :unprocessable_entity
              end

              it 'should not change name when old name is given' do
                post :create_or_update, params: { user: { external_id: user_name, name: user_name } }
                assert_response :ok
                assert_equal user_name, JSON.parse(@response.body)['user']['name']
              end

              it 'should update name' do
                post :create_or_update, params: { user: { external_id: user_name, name: new_name } }
                assert_response :ok
                assert_equal new_name, JSON.parse(@response.body)['user']['name']
              end
            end

            describe 'via identity' do
              let(:user_name) { 'abc_108' }
              let(:new_name) { 'new_abc_108' }
              before do
                post :create_or_update, params: { user: { name: user_name, identities: [{ type: 'email', value: "#{user_name}@test.com" }] } }
              end

              it 'should return error when empty user name is given' do
                post :create_or_update, params: { user: { identities: [{ type: 'email', value: "#{user_name}@test.com" }], name: '' } }
                assert_response :unprocessable_entity
              end

              it 'should not change name when old name is given' do
                post :create_or_update, params: { user: { identities: [{ type: 'email', value: "#{user_name}@test.com" }], name: user_name } }
                assert_response :ok
                assert_equal user_name, JSON.parse(@response.body)['user']['name']
              end

              it 'should update user' do
                post :create_or_update, params: { user: { identities: [{ type: 'email', value: "#{user_name}@test.com" }], name: new_name } }
                assert_response :ok
                assert_equal new_name, JSON.parse(@response.body)['user']['name']
              end
            end

            describe 'via user id, external_id and identity' do
              let(:user_name) { 'abc_1000' }
              let(:new_name) { 'new_abc_1000' }
              before do
                post :create_or_update, params: { user: { name: user_name, external_id: user_name, identities: [{ type: 'email', value: "#{user_name}@test.com" }] } }
                @user_id = JSON.parse(@response.body)['user']['id']
              end

              it 'should not change name when old name is given' do
                post :create_or_update, params: { user: { id: @user_id, external_id: user_name, identities: [{ type: 'email', value: "#{user_name}@test.com" }], name: user_name } }
                assert_response :ok
                assert_equal user_name, JSON.parse(@response.body)['user']['name']
              end

              it 'should update name' do
                post :create_or_update, params: { user: { id: @user_id, external_id: user_name, identities: [{ type: 'email', value: "#{user_name}@test.com" }], name: new_name } }
                assert_response :ok
                assert_equal new_name, JSON.parse(@response.body)['user']['name']
              end
            end

            describe 'with overwrite_existing_name param' do
              before do
                post :create_or_update, params: { user: { name: 'sunshine_user', external_id: 'abc_136' } }
                assert_response :created
              end

              it 'when existing name is same as overwrite_existing_name, should update user name' do
                post :create_or_update, params: { user: { name: 'abc_136_new', external_id: 'abc_136', overwrite_existing_name: 'sunshine_user' } }
                assert_response :ok
                assert_equal 'abc_136_new', JSON.parse(@response.body)['user']['name']
              end

              it 'when existing name is not same as overwrite_existing_name, should not update user name' do
                post :create_or_update, params: { user: { name: 'abc_136_new', external_id: 'abc_136' } }
                assert_response :ok
                assert_equal 'abc_136_new', JSON.parse(@response.body)['user']['name']

                post :create_or_update, params: { user: { name: 'abc_136_new_new', external_id: 'abc_136', overwrite_existing_name: 'sunshine_user' } }
                assert_response :ok
                assert_equal 'abc_136_new', JSON.parse(@response.body)['user']['name']
              end
            end
          end

          describe 'when external_id is present in request' do
            describe "user doesn't have external_id" do
              before do
                post :create_or_update, params: { user: { name: 'abc_109', identities: [{ type: 'email', value: 'abc_109@test.com' }] } }
                @user_id_one = JSON.parse(@response.body)['user']['id']
                post :create_or_update, params: { user: { name: 'abc_110', external_id: 'abc_110' } }
                post :create_or_update, params: { user: { name: 'abc_114', identities: [{ type: 'email', value: 'abc_114@test.com' }] } }
                @user_id_two = JSON.parse(@response.body)['user']['id']
              end

              it 'should return error when empty external_id is given' do
                post :create_or_update, params: { user: { id: @user_id_one, external_id: '' } }
                assert_response :unprocessable_entity
              end

              it 'should return an error when updating external_id which belongs to other user via identity' do
                post :create_or_update, params: { user: { name: 'abc_109', external_id: 'abc_110', identities: [{ type: 'email', value: 'abc_109@test.com' }] } }
                error = {
                  "error" => "RecordInvalid",
                  "description" => "Record validation errors",
                  "details" => {
                    "identity" => [{ "description" => "The provided identifiers resolve to multiple existing users", "error" => "MultipleExistingUsers"}]
                  }
                }
                assert_response :unprocessable_entity
                assert_equal error, JSON.parse(@response.body)
              end

              it 'should return an error when updating external_id which belongs to other user via user id' do
                post :create_or_update, params: { user: { id: @user_id_one, external_id: 'abc_110' } }
                assert_response :unprocessable_entity
              end

              it 'should update external_id via identity' do
                post :create_or_update, params: { user: { external_id: 'abc_109', identities: [{ type: 'email', value: 'abc_109@test.com' }] } }
                assert_response :ok
                assert_not_nil @controller.send(:current_account).users.find { |user| user['external_id'] == 'abc_109' }
              end

              it 'should update external_id via user id' do
                post :create_or_update, params: { user: { id: @user_id_two, external_id: 'abc_114' } }
                assert_response :ok
                assert_not_nil @controller.send(:current_account).users.find { |user| user['external_id'] == 'abc_114' }
              end
            end

            describe "user has external_id" do
              before do
                post :create_or_update, params: { user: { name: 'abc_111', external_id: 'abc_111', identities: [{ type: 'email', value: 'abc_111@test.com' }] } }
                @user_id_one = JSON.parse(@response.body)['user']['id']
                post :create_or_update, params: { user: { name: 'abc_112', external_id: 'abc_112', identities: [{ type: 'email', value: 'abc_112@test.com' }] } }
              end

              it 'should return error when empty external_id is given' do
                post :create_or_update, params: { user: { id: @user_id_one, external_id: '' } }
                assert_response :unprocessable_entity
              end

              it 'should return ok when updating with the same external_id via identity' do
                post :create_or_update, params: { user: { external_id: 'abc_111', identities: [{ type: 'email', value: 'abc_111@test.com' }] } }
                assert_response :ok
              end

              it 'should return ok when updating with the same external_id via user id' do
                post :create_or_update, params: { user: { id: @user_id_one, external_id: 'abc_111' } }
                assert_response :ok
              end

              it 'should return an error when updating to different external_id via identity' do
                error = {
                  "error" => "RecordInvalid",
                  "description" => "Record validation errors",
                  "details" => {
                    "type" => [{ "description" => "A different external ID is present on the user", "error" => "ExternalIdPresent"}]
                  }
                }

                post :create_or_update, params: { user: { external_id: 'abc_113', identities: [{ type: 'email', value: 'abc_111@test.com' }] } }
                assert_response :unprocessable_entity
                assert_equal error, JSON.parse(@response.body)
              end

              it 'should return an error when updating to different external_id via user id' do
                post :create_or_update, params: { user: { id: @user_id_one, external_id: 'abc_113' } }
                assert_response :unprocessable_entity
              end

              it 'should return an error when updating with external_id which belongs to another user via identity' do
                post :create_or_update, params: { user: { external_id: 'abc_112', identities: [{ type: 'email', value: 'abc_111@test.com' }] } }
                assert_response :unprocessable_entity
              end

              it 'should return an error when updating external_id which belongs to another user via user id' do
                post :create_or_update, params: { user: { id: @user_id_one, external_id: 'abc_112' } }
                assert_response :unprocessable_entity
              end
            end
          end

          describe 'when identities are present in request' do
            before do
              post :create_or_update, params: { user: { name: 'abc_116', external_id: 'abc_116' } }
              @user_id_one = JSON.parse(@response.body)['user']['id']
              post :create_or_update, params: { user: { name: 'abc_117', external_id: 'abc_117' } }
              post :create_or_update, params: { user: { name: 'abc_118', external_id: 'abc_118', identities: [{ type: 'email', value: 'abc_118@test.com' }] } }
              @user_id_two = JSON.parse(@response.body)['user']['id']
              post :create_or_update, params: { user: { name: 'abc_119', external_id: 'abc_119', identities: [{ type: 'phone_number', value: '3030000000' }] } }
            end

            it 'returns error when invalid identity type is given' do
              post :create_or_update, params: { user: { id: @user_id_one, identities: [{ type: 'zendesk', value: 'abc_116@test.com' }] } }
              assert_response :unprocessable_entity
            end

            it 'should add new email identity to user via id' do
              new_email_address = 'abc_116@test.com'
              post :create_or_update, params: { user: { id: @user_id_one, identities: [{ type: 'email', value: new_email_address }] } }
              assert_response :ok
              check_for_identity_value(new_email_address)
            end

            it 'should add new phone identity to user via id' do
              new_phone_address = '2130000000'
              post :create_or_update, params: { user: { id: @user_id_one, identities: [{ type: 'phone_number', value: new_phone_address }] } }
              assert_response :ok
              check_for_identity_value(new_phone_address)
            end

            it 'should add new identity to user via external_id' do
              new_email_address = 'abc_117@test.com'
              post :create_or_update, params: { user: { external_id: 'abc_117', identities: [{ type: 'email', value: new_email_address }] } }
              assert_response :ok
              check_for_identity_value(new_email_address)
            end

            it 'should add new identity to user via existing identity' do
              new_email_address = 'abc_1180@test.com'
              post :create_or_update, params: { user: { identities: [{ type: 'email', value: 'abc_118@test.com' }, { type: 'email', value: new_email_address }] } }
              assert_response :ok
              check_for_identity_value(new_email_address)
            end

            it 'should add new identity via id when user already has identity' do
              new_email_address = 'abc_1181@test.com'
              post :create_or_update, params: { user: { id: @user_id_two, identities: [{ type: 'email', value: new_email_address }] } }
              assert_response :ok
              check_for_identity_value(new_email_address)
            end

            it 'update via id should fail when trying to add identity that belongs to another user' do
              post :create_or_update, params: { user: { id: @user_id_one, identities: [{ type: 'email', value: 'abc_118@test.com' }] } }
              assert_response :unprocessable_entity
            end

            it 'update via id should fail when trying to add/update multiple identity and one belongs to another user' do
              form = { id: @user_id_two, identities: [{type: 'email', value: 'abc_118@test.com'}, {type: 'phone_number', value: '3030000000'}] }
              post :create_or_update, params: { user: form }
              assert_response :unprocessable_entity
            end

            it 'update via external_id should fail when trying to add identity that belongs to another user' do
              post :create_or_update, params: { user: { external_id: 'abc_116', identities: [{type: 'email', value: 'abc_118@test.com'}] } }
              assert_response :unprocessable_entity
            end

            it 'update via external_id should fail when trying to add/update multiple identity and one belongs to another user' do
              form = { external_id: 'abc_118', identities: [{type: 'email', value: 'abc_118@test.com'}, {type: 'phone_number', value: '3030000000'}] }
              post :create_or_update, params: { user: form }
              assert_response :unprocessable_entity
            end

            it 'update via identity should fail when trying to add identity that belongs to another user' do
              post :create_or_update, params: { user: { identities: [{type: 'email', value: 'abc_118@test.com'}, {type: 'phone_number', value: '3030000000'}] } }
              assert_response :unprocessable_entity
            end
          end

          describe 'with multiple identifiers' do
            before do
              post :create_or_update, params: { user: { name: 'abc_115', external_id: 'abc_115', identities: [{type: 'email', value: 'abc_115@test.com'}] } }
              @user_id_one = JSON.parse(@response.body)['user']['id']
              post :create_or_update, params: { user: { name: 'abc_120', external_id: 'abc_120', identities: [{type: 'email', value: 'abc_120@test.com'}] } }
            end

            it 'should return ok when same identifiers are given' do
              post :create_or_update, params: { user: { name: 'abc_115', external_id: 'abc_115', identities: [{type: 'email', value: 'abc_115@test.com'}] } }
              assert_response :ok
            end

            it 'should update name and add new identity to user' do
              new_email_address = 'new_abc_115@test.com'
              form = {
                name: 'new_abc_115', external_id: 'abc_115',
                identities: [{ type: 'email', value: 'abc_115@test.com' }, { type: 'email', value: new_email_address }]
              }
              post :create_or_update, params: { user: form }
              assert_response :ok
              assert_equal 'new_abc_115', JSON.parse(@response.body)['user']['name']
              check_for_identity_value(new_email_address)
            end

            it 'should return an error when trying to update external_id with new value' do
              form = {
                name: 'abc_115',
                external_id: 'new_abc_115',
                identities: [{ type: 'email', value: 'abc_115@test.com' }, { type: 'email', value: 'new_abc_1150@test.com' }]
              }

              post :create_or_update, params: { user: form }
              assert_response :unprocessable_entity
            end

            it 'should return an error when identifiers resolve to multiple users' do
              form = { id: @user_id_one, external_id: 'abc_120', identities: [{ type: 'email', value: 'abc_120@test.com' }] }
              post :create_or_update, params: { user: form }
              assert_response :unprocessable_entity
            end
          end
        end

        describe 'With requester_id' do
          describe 'as an admin' do
            before(:all) do
              @admin = users(:minimum_admin)
            end

            it 'should be able to update own record' do
              post :create_or_update, params: { user: { id: @admin.id, name: 'new_admin_name' }, requester_id: @admin.id }
              assert_response :ok
              assert_equal @admin.id, JSON.parse(@response.body)['user']['id']
              assert_equal 'new_admin_name', JSON.parse(@response.body)['user']['name']
            end

            it 'should be able to update agent record' do
              agent = users(:minimum_agent)
              post :create_or_update, params: { user: { id: agent.id, name: 'new_agent_name' }, requester_id: @admin.id }
              assert_response :ok
              assert_equal agent.id, JSON.parse(@response.body)['user']['id']
              assert_equal 'new_agent_name', JSON.parse(@response.body)['user']['name']
            end

            it 'should be able to update end-user record' do
              end_user = users(:minimum_end_user)
              post :create_or_update, params: { user: { id: end_user.id, name: 'new_end_user_name' }, requester_id: @admin.id }
              assert_response :ok
              assert_equal end_user.id, JSON.parse(@response.body)['user']['id']
              assert_equal 'new_end_user_name', JSON.parse(@response.body)['user']['name']
            end
          end

          describe 'as an agent' do
            before(:all) do
              @agent = users(:minimum_agent)
            end

            it 'should be able to update own record' do
              post :create_or_update, params: { user: { id: @agent.id, name: 'new_agent_name' }, requester_id: @agent.id }
              assert_response :ok
              assert_equal @agent.id, JSON.parse(@response.body)['user']['id']
              assert_equal 'new_agent_name', JSON.parse(@response.body)['user']['name']
            end

            it 'should not be able to update admin record' do
              admin = users(:minimum_admin)
              post :create_or_update, params: { user: { id: admin.id, name: 'new_agent_name' }, requester_id: @agent.id }
              assert_response :forbidden
            end

            it 'should be able to update end-user record' do
              end_user = users(:minimum_end_user)
              post :create_or_update, params: { user: { id: end_user.id, name: 'new_end_user_name' }, requester_id: @agent.id }
              assert_response :ok
              assert_equal end_user.id, JSON.parse(@response.body)['user']['id']
              assert_equal 'new_end_user_name', JSON.parse(@response.body)['user']['name']
            end

            describe 'in different organization' do
              let(:permission_set) do
                account.permission_sets.new(
                  account: account,
                  name: 'test',
                  role_type: ::PermissionSet::Type::CUSTOM
                ).tap do |role|
                  role.permissions.set(end_user_profile: 'edit-within-org')
                end
              end
              let(:other_org) { organizations(:minimum_organization2) }

              before do
                Account.any_instance.stubs(:has_permission_sets?).returns(true)
                permission_set.save!
                @agent.permission_set = permission_set
                @agent.organization = other_org
                @agent.save!
              end

              it 'should not be able to update end-user record' do
                end_user = users(:minimum_end_user)
                post :create_or_update, params: { user: { id: end_user.id, name: 'new_end_user_name' }, requester_id: @agent.id }
                assert_response :forbidden
              end
            end
          end

          describe 'as an end-user' do
            before(:all) do
              @end_user = users(:minimum_end_user)
            end

            it 'should be able to update own record' do
              post :create_or_update, params: { user: { id: @end_user.id, name: 'new_end_user_name' }, requester_id: @end_user.id }
              assert_response :ok
              assert_equal @end_user.id, JSON.parse(@response.body)['user']['id']
              assert_equal 'new_end_user_name', JSON.parse(@response.body)['user']['name']
            end

            it 'should not be able to update admin record' do
              admin = users(:minimum_admin)
              post :create_or_update, params: { user: { id: admin.id, name: 'new_agent_name' }, requester_id: @end_user.id }
              assert_response :forbidden
            end

            it 'should not be able to update agent record' do
              agent = users(:minimum_agent)
              post :create_or_update, params: { user: { id: agent.id, name: 'new_agent_name' }, requester_id: @end_user.id }
              assert_response :forbidden
            end

            it 'should not be able to update other end-user record' do
              end_user = users(:minimum_end_user2)
              post :create_or_update, params: { user: { id: end_user.id, name: 'new_end_user_name' }, requester_id: @end_user.id }
              assert_response :forbidden
            end
          end
        end
      end

      def check_for_identity_value(value)
        user_id = JSON.parse(@response.body)['user']['id']
        user = @controller.send(:current_account).users.find { |u| u['id'] == user_id }

        user_identities = []
        user.reload.identities.each do |identity|
          user_identities << identity.value if identity.present?
        end

        assert_includes user_identities, value
      end
    end
  end

  as_a_subsystem_user(user: 'profile_service', account: :minimum) do
    describe '#find_user_identity' do
      describe 'when email identity exists' do
        let!(:user_identity) { user_identities(:minimum_end_user) }

        before { get :find_user_identity, params: { type: 'email', value: user_identity.value } }

        it 'finds the correct user identity' do
          assert_equal user_identity.id, JSON.parse(@response.body)['identity']['id']
        end

        it 'returns correct identity user_id attribute' do
          assert_equal user_identity.user_id, JSON.parse(@response.body)['identity']['user_id']
        end

        it 'returns correct identity type attribute' do
          assert_equal 'email', JSON.parse(@response.body)['identity']['type']
        end

        it 'returns correct identity value attribute' do
          assert_equal user_identity.value, JSON.parse(@response.body)['identity']['value']
        end

        it 'returns correct identity is_verified attribute' do
          assert_equal user_identity.is_verified, JSON.parse(@response.body)['identity']['verified']
        end

        it 'returns correct identity primary attribute' do
          assert(JSON.parse(@response.body)['identity']['primary'])
        end

        it 'returns correct identity undeliverable_count attribute' do
          assert_equal user_identity.undeliverable_count, JSON.parse(@response.body)['identity']['undeliverable_count']
        end

        it 'returns correct identity deliverable_state attribute' do
          assert_equal 'deliverable', JSON.parse(@response.body)['identity']['deliverable_state']
        end
      end

      describe 'when email identity does not exist' do
        before { get :find_user_identity, params: { type: 'email', value: 'doesntexist@example.com' } }

        it 'returns a 404 not found' do
          assert_response :not_found
        end
      end

      describe 'when specifying an invalid type' do
        before { get :find_user_identity, params: { type: 'doge', value: 'wow. such invalid.' } }

        it 'returns a 400 bad request' do
          assert_response :bad_request
        end
      end
    end

    describe '#create_or_update' do
      before do
        post :create_or_update, params: { user: { name: 'abc_121', external_id: 'abc_121', identities: [{ type: 'email', value: 'abc_121@test.com' }] } }
      end

      it ('when create new user responds with created') { assert_response :created }

      it 'when user already exists responds with ok' do
        post :create_or_update, params: { user: { name: 'abc_121', external_id: 'abc_121', identities: [{ type: 'email', value: 'abc_121@test.com' }] } }
        assert_response :ok
      end
    end
  end

  as_a_subsystem_user(user: 'collaboration', account: :minimum) do
    describe '#create_or_update' do
      before do
        post :create_or_update, params: { user: { name: 'abc_122', external_id: 'abc_122', identities: [{ type: 'email', value: 'abc_122@test.com' }] } }
      end

      it ('when create new user responds with created') { assert_response :created }

      it 'when user already exists responds with ok' do
        post :create_or_update, params: { user: { name: 'abc_122', external_id: 'abc_122', identities: [{ type: 'email', value: 'abc_122@test.com' }] } }
        assert_response :ok
      end
    end
  end

  as_a_subsystem_user(user: 'sunshine_conversations', account: :minimum) do
    describe '#create_or_update' do
      before do
        post :create_or_update, params: { user: { name: 'abc_123', external_id: 'abc_123', identities: [{ type: 'email', value: 'abc_123@test.com' }] } }
      end

      it ('when create new user responds with created') { assert_response :created }

      it 'when user already exists responds with ok' do
        post :create_or_update, params: { user: { name: 'abc_123', external_id: 'abc_123', identities: [{ type: 'email', value: 'abc_123@test.com' }] } }
        assert_response :ok
      end
    end
  end
end
