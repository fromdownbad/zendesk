require_relative "../../../../support/test_helper"
require_relative '../../../../support/deflection_helper'
require_relative '../../../../support/rule'

SingleCov.covered!

describe Api::V2::Internal::PredictionSettingsController do
  extend Api::V2::TestHelper
  include TestSupport::Rule::Helper
  include TestSupport::Rule::UsageHelper
  include DeflectionHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:create) { 'Create' }
  let(:update) { 'Change' }

  let(:deflection_trigger_1) { Trigger.where(title: 'deflection_trigger_1').first }
  let(:deflection_trigger_2) { Trigger.where(title: 'deflection_trigger_2').first }
  let(:deflection_trigger_3) { Trigger.where(title: 'deflection_trigger_3').first }

  let(:notification_trigger_1) { Trigger.where(title: 'notification_trigger_1').first }
  let(:notification_trigger_2) { Trigger.where(title: 'notification_trigger_2').first }
  let(:notification_trigger_3) { Trigger.where(title: 'notification_trigger_3').first }

  before do
    Zendesk::RedisStore.redis_client = FakeRedis::Redis.new
    Timecop.freeze(Time.local(2017, 1, 1))
    Trigger.destroy_all

    accept :json
    @request.account = account
  end

  with_options(controller: "api/v2/internal/prediction_settings") do |request|
    request.should_route :get, "/api/v2/internal/prediction_settings/automatic_answers_triggers", action: "automatic_answers_triggers"
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#automatic_answers_triggers" do
      before do
        account.stubs(:has_automatic_answers_enabled?).returns(true)
        account.stubs(:url).returns('https://non-hostmapped.zendesk.com')
      end

      describe "deflection triggers" do
        let(:deflection_triggers) { JSON.parse(@response.body).fetch('prediction_settings').fetch('deflection_triggers') }

        describe "when deflection triggers exist" do
          let(:expected) do
            [{
              "id" => deflection_trigger_1.id,
              "title" => deflection_trigger_1.title,
              "permalink" => deflection_trigger_1.lotus_permalink,
              "usage_30d" => deflection_trigger_1.usage.monthly.to_i
            }]
          end
          let (:usage_from_response) do
            deflection_triggers.first["usage_30d"]
          end

          before do
            create_trigger(title: 'deflection_trigger_1', active: true, definition: build_deflection_action)
            create_trigger(title: 'deflection_trigger_2', active: false, definition: build_deflection_action)
            create_trigger(title: 'deflection_trigger_3')

            [10.days.ago, 20.days.ago, 31.days.ago].each do |time|
              Timecop.travel(time) do
                record_usage(type: :trigger, id: deflection_trigger_1.id)
              end
            end

            get :automatic_answers_triggers, format: :json
          end

          it "returns a list of deflection triggers" do
            assert_response :ok
            assert_equal expected, deflection_triggers
          end

          it "returns triggers with correct monthly usage" do
            assert_response :ok
            assert_equal 2, usage_from_response
          end
        end

        describe 'sorting order' do
          before do
            create_trigger(title: 'deflection_trigger_1', active: true, definition: build_deflection_action)
            create_trigger(title: 'deflection_trigger_2', active: true, definition: build_deflection_action)
            create_trigger(title: 'deflection_trigger_3', active: true, definition: build_deflection_action)

            Timecop.travel(20.days.ago) do
              2.times { record_usage(type: :trigger, id: deflection_trigger_1.id) }
              4.times { record_usage(type: :trigger, id: deflection_trigger_2.id) }
              6.times { record_usage(type: :trigger, id: deflection_trigger_3.id) }
            end

            get :automatic_answers_triggers, format: :json
          end

          it 'returns a list sorted by monthly usage descending' do
            expected_ids = [deflection_trigger_3.id, deflection_trigger_2.id, deflection_trigger_1.id]

            assert_equal expected_ids, deflection_triggers.map { |t| t['id'] }
          end
        end

        describe "when deflection triggers do not exist" do
          before do
            create_trigger(title: 'deflection_trigger_3')
          end

          it "renders an empty list" do
            get :automatic_answers_triggers, format: :json

            assert_response :ok
            assert_empty deflection_triggers
          end
        end
      end

      describe 'potential deflection triggers' do
        let(:potential_deflection_triggers) { JSON.parse(@response.body).fetch('prediction_settings').fetch('potential_deflection_triggers') }

        describe 'when notification_user triggers exist' do
          let(:expected) do
            [{
              'id' => notification_trigger_1.id,
              'title' => notification_trigger_1.title,
              'permalink' => notification_trigger_1.lotus_permalink,
              'usage_30d' => notification_trigger_1.usage.monthly.to_i
            }]
          end

          describe 'and triggers are active and inactive' do
            before do
              create_trigger(title: 'notification_trigger_1', active: true, definition: build_notification_action_conditions_all(create))
              create_trigger(title: 'notification_trigger_2', active: false, definition: build_notification_action_conditions_all(create))

              get :automatic_answers_triggers, format: :json
            end

            it 'returns a list of active notification_user triggers' do
              assert_response :ok
              assert_equal expected, potential_deflection_triggers
            end
          end

          describe 'and notification_user recipients vary' do
            before do
              create_trigger(title: 'notification_trigger_1', active: true, definition: build_notification_action_conditions_all(create))
              create_trigger(title: 'notification_trigger_2', active: false, definition: build_notification_action_conditions_all(create))

              notification_trigger_2.definition.actions.first.value[0] = 'all_agents'

              get :automatic_answers_triggers, format: :json
            end

            it "returns only those with notification_user and 'requester_id'" do
              assert_response :ok
              assert_equal expected, potential_deflection_triggers
            end
          end

          describe 'and notification_user recipients are requester and cc' do
            before do
              create_trigger(title: 'notification_trigger_1', active: true, definition: build_notification_action_conditions_all(create, with_cc: true))

              get :automatic_answers_triggers, format: :json
            end

            it "returns the trigger which has recepients set as requester and cc" do
              assert_response :ok
              assert_equal expected, potential_deflection_triggers
            end
          end

          describe 'and triggers include usage counts' do
            let (:usage_from_response) { potential_deflection_triggers.first["usage_30d"] }

            before do
              create_trigger(title: 'notification_trigger_1', active: true, definition: build_notification_action_conditions_all(create))

              [10.days.ago, 20.days.ago, 31.days.ago].each do |time|
                Timecop.travel(time) do
                  record_usage(type: :trigger, id: notification_trigger_1.id)
                end
              end

              get :automatic_answers_triggers, format: :json
            end

            it 'returns triggers with correct monthly usage' do
              assert_response :ok
              assert_equal 2, usage_from_response
            end
          end

          describe "and triggers include a ticket update_type of 'Created' or 'Updated' in conditions_all" do
            before do
              create_trigger(title: 'notification_trigger_1', active: true, definition: build_notification_action_conditions_all(create))
              create_trigger(title: 'notification_trigger_2', active: true, definition: build_notification_action_conditions_all(update))

              get :automatic_answers_triggers, format: :json
            end

            it "returns only those with an update_type of 'Created'" do
              assert_response :ok
              assert_equal expected, potential_deflection_triggers
            end
          end

          describe "and triggers include a ticket update_type of 'Created' or 'Updated' in conditions_any" do
            before do
              create_trigger(title: 'notification_trigger_1', active: true, definition: build_notification_action_conditions_any(create))
              create_trigger(title: 'notification_trigger_2', active: true, definition: build_notification_action_conditions_any(update))

              get :automatic_answers_triggers, format: :json
            end

            it "returns only those with an update_type of 'Created'" do
              assert_response :ok
              assert_equal expected, potential_deflection_triggers
            end
          end

          describe 'sorting order' do
            before do
              create_trigger(title: 'notification_trigger_1', active: true, definition: build_notification_action_conditions_all(create))
              create_trigger(title: 'notification_trigger_2', active: true, definition: build_notification_action_conditions_all(create))
              create_trigger(title: 'notification_trigger_3', active: true, definition: build_notification_action_conditions_all(create))

              Timecop.travel(20.days.ago) do
                2.times { record_usage(type: :trigger, id: notification_trigger_1.id) }
                4.times { record_usage(type: :trigger, id: notification_trigger_2.id) }
                6.times { record_usage(type: :trigger, id: notification_trigger_3.id) }
              end

              get :automatic_answers_triggers, format: :json
            end

            it 'returns a list sorted by monthly usage descending' do
              expected_ids = [notification_trigger_3.id, notification_trigger_2.id, notification_trigger_1.id]

              assert_equal expected_ids, potential_deflection_triggers.map { |t| t['id'] }
            end
          end
        end

        describe 'when notification_user triggers do not exist' do
          let(:expected) { [] }

          before do
            create_trigger(title: 'other_trigger_1', active: true, definition: build_trigger_definition)

            get :automatic_answers_triggers, format: :json
          end

          it 'returns an empty list' do
            assert_response :ok
            assert_equal expected, potential_deflection_triggers
          end
        end
      end
    end
  end
end
