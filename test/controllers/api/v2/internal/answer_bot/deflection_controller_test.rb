require_relative '../../../../../support/test_helper'

SingleCov.covered! uncovered: 3

describe Api::V2::Internal::AnswerBot::DeflectionController do
  fixtures :ticket_deflections, :brands, :tickets
  let(:brand) { brands(:minimum) }
  # minimum_2 has no existing ticket deflection
  let(:ticket) { tickets(:minimum_2) }
  let(:enquiry) { 'Cats are a$$holes' }
  let(:account) { accounts(:minimum) }
  let(:base_article) do
    {
      "article_id" => 1, "brand_id" => brand.id, "locale" => "en-us", "score" => 0.11682176580071302,
      "url" => "www.example.zendesk.com/artciles.json", "html_url" => "www.example.zendesk.com",
      "title" => "article title"
    }
  end
  let(:recommended_article_one) do
    base_article.merge(
      "article_id" => 203663736,
      "score" => 0.11682176580071302,
      "clicked_at" => 30.days.ago.to_datetime
    )
  end
  let(:recommended_article_two) do
    base_article.merge(
      "article_id" => 203663737,
      "score" => 0.21682176580071302
    )
  end
  let(:articles) { [recommended_article_one, recommended_article_two] }
  let(:ticket_id)  { ticket.nice_id }
  let(:via_id) { ViaType.WEB_FORM }
  let(:deflection_channel_id) { ViaType.WEB_FORM }
  let(:deflection_channel_reference_id) { 1234 }
  let(:request_params) do
    {
      ticket_id: ticket_id,
      enquiry: enquiry,
      model_version: 1,
      articles: articles,
      via_id: via_id,
      deflection_channel_id: deflection_channel_id,
      brand_id: brand.id,
      language: 'en'
    }
  end

  before do
    accept :json
    @request.account = @current_account = account
    ticket.account_id = account.id
    ticket.will_be_saved_by(User.system)
    ticket.save!
  end

  as_a_subsystem_user(account: :minimum) do
    describe 'POST #create without ticket' do
      let(:via_id) { ViaType.WEB_WIDGET }
      let(:deflection_channel_id) { ViaType.WEB_WIDGET }
      let(:ticket_id) { nil }
      let(:request_params) do
        {
          enquiry: enquiry,
          model_version: 1,
          articles: articles,
          via_id: via_id,
          deflection_channel_id: deflection_channel_id,
          deflection_channel_reference_id: deflection_channel_reference_id,
          brand_id: brand.id,
          language: 'en'
        }
      end

      it 'returns successful response' do
        post :create, params: request_params

        assert_response :ok
      end

      it 'creates the ticket deflection record' do
        assert_difference 'TicketDeflection.count', 1 do
          post :create, params: request_params

          parsed_response = JSON.parse(response.body)
          assert_not_nil parsed_response["id"]
        end
      end

      describe 'attributes for ticket deflection record' do
        let(:deflection_channel_reference_id) { 1234 }
        let(:request_params_with_extra) do
          request_params.merge(
            interaction_reference_type: 1,
            interaction_reference: 'some_reference_code',
            deflection_channel_reference_id: deflection_channel_reference_id
          )
        end

        before do
          post :create, params: request_params_with_extra

          parsed_response = JSON.parse(response.body)
          @deflection = TicketDeflection.find(parsed_response['id'])
        end

        it 'sets deflection channel id' do
          assert_equal(
            request_params_with_extra[:deflection_channel_id],
            @deflection.deflection_channel_id
          )
        end

        it 'sets interaction reference type' do
          assert_equal(1, @deflection.interaction_reference_type)
        end

        it 'sets interaction reference' do
          assert_equal('some_reference_code', @deflection.interaction_reference)
        end

        it 'sets deflection channel reference id' do
          assert_equal(
            deflection_channel_reference_id,
            @deflection.deflection_channel_reference_id
          )
        end
      end

      it 'creates ticket deflection article records' do
        assert_difference 'TicketDeflectionArticle.count', 2 do
          post :create, params: request_params
        end
      end

      it 'returns correct information of ticket deflection articles' do
        expected_article_ids = articles.map { |a| a['article_id'] }

        post :create, params: request_params

        parsed_deflection_articles = JSON.parse(response.body)['ticket_deflection_articles']
        assert_equal(expected_article_ids, parsed_deflection_articles.map { |a| a['article_id'] })
      end

      describe 'when articles is empty' do
        let(:articles) { nil }

        it 'returns successful response' do
          post :create, params: request_params, as: :json

          assert_response :ok
        end

        it 'creates just the ticket deflection record' do
          assert_difference 'TicketDeflection.count', 1 do
            post :create, params: request_params, as: :json
          end
        end

        it 'returns empty ticket_deflection_articles' do
          post :create, params: request_params, as: :json

          parsed_response = JSON.parse(response.body)
          assert_empty parsed_response["ticket_deflection_articles"]
        end
      end

      describe 'auth token' do
        it 'is present' do
          post :create, params: request_params

          parsed_response = JSON.parse(response.body)
          assert_not_nil parsed_response['auth_token']
        end

        it 'contains account_id set to account' do
          post :create, params: request_params

          parsed_response = JSON.parse(response.body)
          token = AutomaticAnswersJwtToken.decode(parsed_response['auth_token'])

          assert_equal account.id, token.fetch('account_id')
        end

        it 'sets user_id to nil' do
          post :create, params: request_params

          parsed_response = JSON.parse(response.body)
          token = AutomaticAnswersJwtToken.decode(parsed_response['auth_token'])

          assert_nil token.fetch('user_id')
        end
      end
    end

    describe 'POST #create' do
      describe 'when the ticket_id is not locateable' do
        let(:ticket_id) { 12345678910 }

        it 'responds with unprocessable entity' do
          post :create, params: request_params

          assert_response :unprocessable_entity
        end

        it 'does not create a new ticket deflection record' do
          assert_no_difference 'TicketDeflection.count' do
            post :create, params: request_params
          end
        end
      end

      it 'should create the ticket deflection record' do
        assert_difference 'TicketDeflection.count', 1 do
          post :create, params: request_params

          assert_response :ok
          parsed_response = JSON.parse(response.body)
          assert_not_nil parsed_response["id"]
        end
      end

      it 'should set the deflection channel id with given value' do
        post :create, params: request_params

        assert_response :ok
        ticket_deflection = TicketDeflection.find_by_ticket_id(ticket.id)
        assert_equal ticket_deflection.deflection_channel_id, request_params[:deflection_channel_id]
      end

      it 'sets the brand_id with the given value' do
        post :create, params: request_params

        ticket_deflection = TicketDeflection.find_by_ticket_id(ticket.id)
        assert_equal request_params[:brand_id], ticket_deflection.brand_id
      end

      it 'should create two ticket deflection articles record' do
        assert_difference 'TicketDeflectionArticle.count', 2 do
          post :create, params: request_params

          assert_response :ok
          parsed_response = JSON.parse(response.body)
          assert_not_empty parsed_response["ticket_deflection_articles"]
        end
      end

      it 'returns correct information of ticket deflection articles' do
        expected_article_ids = articles.map { |a| a['article_id'] }

        post :create, params: request_params

        parsed_deflection_articles = JSON.parse(response.body)['ticket_deflection_articles']
        assert_equal expected_article_ids, parsed_deflection_articles.map { |a| a['article_id'] }
        assert_equal [brand.id], parsed_deflection_articles.map { |a| a['brand_id'] }.uniq
      end

      it 'creates an AutomaticAnswerSend event and appends it to a new audit' do
        send_event_count = lambda { ticket.audits.last.events.to_a.select { |e| e.type == "AutomaticAnswerSend" }.count }
        assert_difference send_event_count, 1 do
          post :create, params: request_params
          assert_response :ok

          ticket.reload
        end
      end

      describe "when Answer Bot automatic tagging is enabled" do
        before do
          Arturo.enable_feature!(:answer_bot_tag_automation)
        end

        it "adds tag to ticket" do
          AnswerBot::TicketDeflectionTaggingJob.expects(:enqueue).once

          post :create, params: request_params
          assert_response :ok
        end
      end

      describe 'when articles is empty' do
        let(:articles) { nil }
        it 'should create just the ticket deflection record' do
          assert_difference 'TicketDeflection.count', 1 do
            post :create, params: request_params, as: :json

            assert_response :ok
            parsed_response = JSON.parse(response.body)
            assert_empty parsed_response["ticket_deflection_articles"]
          end
        end

        it 'should not add AutomaticAnswerSend event to audit' do
          send_event_count = lambda { ticket.audits.first.events.to_a.select { |e| e.type == "AutomaticAnswerSend" }.count }
          assert_no_difference send_event_count do
            post :create, params: request_params, as: :json
            assert_response :ok

            ticket.reload
          end
        end

        describe "when Answer Bot automatic tagging is enabled" do
          before do
            Arturo.enable_feature!(:answer_bot_tag_automation)
          end

          it "adds tag to ticket" do
            AnswerBot::TicketDeflectionTaggingJob.expects(:enqueue).once
            post :create, params: request_params, as: :json

            assert_response :ok
          end
        end
      end

      describe 'when deflection_channel_id is answer_bot_for_agent' do
        let (:deflection_channel_id) { ViaType.ANSWER_BOT_FOR_AGENTS }

        describe 'ticket already has a deflection' do
          let(:ticket) { tickets(:minimum_1) }

          it 'creates another deflection' do
            assert_difference 'TicketDeflection.where(ticket_id: ticket.id).count', 1 do
              post :create, params: request_params

              assert_response :ok
            end
          end

          it 'creates deflection articles for the provided articles' do
            post :create, params: request_params

            assert_response :ok
            assert_equal articles.count, JSON.parse(response.body)['ticket_deflection_articles'].count
          end
        end

        it 'should not add AutomaticAnswerSend event to audit' do
          send_event_count = lambda { ticket.audits.first.events.to_a.select { |e| e.type == "AutomaticAnswerSend" }.count ticket.reload }
          assert_no_difference send_event_count do
            post :create, params: request_params
          end
        end

        it 'should return a successful response' do
          post :create, params: request_params

          assert_response :ok
        end
      end

      describe 'when the ticket has an existing deflection' do
        # minimum_1 ticket has an existing ticket_deflection
        let(:ticket) { tickets(:minimum_1) }

        it 'does not create a new ticket_delection record' do
          assert_difference 'TicketDeflection.count', 0 do
            post :create, params: request_params

            assert_response :ok
            parsed_response = JSON.parse(response.body)
            assert_equal ticket.ticket_deflection.id, parsed_response["id"]
          end
        end

        it 'does not add AutomaticAnswerSend event to audit' do
          send_event_count = lambda { ticket.audits.first.events.to_a.select { |e| e.type == "AutomaticAnswerSend" }.count }
          assert_no_difference send_event_count do
            post :create, params: request_params
            assert_response :ok

            ticket.reload
          end
        end

        it 'does not create additional ticket delection article records' do
          assert_difference 'TicketDeflectionArticle.count', 0 do
            post :create, params: request_params
          end
        end
      end

      describe 'auth token' do
        before do
          post :create, params: request_params

          assert_response :ok
          parsed_response = JSON.parse(response.body)
          @auth_token = parsed_response["auth_token"]
        end

        it 'should be present' do
          assert_not_nil @auth_token
        end

        it 'should have account_id set to account' do
          token = AutomaticAnswersJwtToken.decode(@auth_token)
          assert_equal account.id,  token.fetch("account_id")
        end

        it 'should have user_id set to requester' do
          token = AutomaticAnswersJwtToken.decode(@auth_token)
          assert_equal ticket.requester.id, token.fetch("user_id")
        end
      end
    end

    describe 'PUT #update' do
      let(:ticket) { tickets(:minimum_1) }
      let(:ticket_deflection) { ticket_deflections(:minimum_ticket_deflection) }
      let(:ticket_deflector) { mock('ticket_deflector') }
      let(:deflection_id) { ticket_deflection.id }
      let(:ticket_nice_id) { ticket.nice_id }
      let(:article_id) { base_article['article_id'] }
      let(:via_id) { ViaType.WEB_FORM }
      let(:state) { 'solve' }
      let(:resolution_channel_id) { ViaType.WEB_FORM }
      let(:request_params) do
        {
          id: deflection_id,
          ticket_id: ticket_nice_id,
          article_id: article_id,
          resolution_channel_id: resolution_channel_id,
          state: state
        }
      end

      before do
        TicketDeflector.stubs(:new).returns(ticket_deflector)
      end

      it 'set resolution channel with provided value' do
        ticket_deflector.stubs(:solve)
        TicketDeflector.expects(:new).with(
          account: account,
          deflection: ticket.ticket_deflection,
          resolution_channel_id: resolution_channel_id
        ).returns(ticket_deflector)

        put :update, params: request_params
        assert_response 200
      end

      describe 'given state is "solved"' do
        let(:state) { 'solve' }

        it 'calls deflector service solve ticket' do
          ticket_deflector.expects(:solve).
            with(article_id: article_id)

          put :update, params: request_params
          assert_response 200
        end
      end

      describe 'given state is "irrelevant"' do
        let(:state) { 'irrelevant' }

        it 'calls deflector service to record irrelevant deflection' do
          ticket_deflector.expects(:process_deflection_rejection).
            with(
              article_id: article_id,
              reason: nil
            )

          put :update, params: request_params
          assert_response 200
        end

        it 'updates reason if provided' do
          ticket_deflector.expects(:process_deflection_rejection).
            with(
              article_id: article_id,
              reason: 1
            )

          put :update, params: request_params.merge(reason: 1)
          assert_response 200
        end
      end

      describe 'ticket deflection not found' do
        let(:deflection_id) { 9876 }

        it 'returns not_found error' do
          put :update, params: request_params
          assert_response :not_found
        end
      end

      describe 'ticket not found' do
        let(:ticket_nice_id) { 99999 }

        it 'returns not_found error' do
          put :update, params: request_params
          assert_response :not_found
        end
      end

      describe 'deflection record exists but does not match with associated ticket' do
        let(:ticket) { tickets(:minimum_3) }

        it 'returns 422' do
          put :update, params: request_params
          assert_response 422
        end
      end

      describe 'given invalid state' do
        let(:state) { 'randomstuff' }

        it 'returns 422' do
          put :update, params: request_params
          assert_response 422
        end
      end
    end
  end
end
