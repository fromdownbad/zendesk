require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::AnswerBot::RejectionController do
  fixtures :accounts, :ticket_deflections

  let(:account) { accounts(:minimum) }
  let(:article_to_reject) do
    {
      "article_id" => 1, "brand_id" => 123, "locale" => "en-us", "score" => 0.11682176580071302,
      "url" => "www.example.zendesk.com/artciles.json", "html_url" => "www.example.zendesk.com",
      "title" => "article title"
    }
  end

  before do
    accept :json
    @current_account = account
  end

  as_a_subsystem_user(account: :minimum) do
    describe 'POST #reject' do
      let(:deflection) { ticket_deflections(:without_ticket) }
      let(:deflection_id) { deflection.id }
      let(:article_id) { article_to_reject['article_id'] }
      let(:resolution_channel_id) { ViaType.WEB_WIDGET }
      let(:reject_reason) { TicketDeflectionArticle::REASON_RELATED_NOT_ANSWERED }
      let(:ticket_deflector) { mock('ticket_deflector') }
      let(:request_params) do
        {
          deflection_id: deflection_id,
          article_id: article_id,
          resolution_channel_id: resolution_channel_id,
          reason: reject_reason
        }
      end

      before do
        TicketDeflector.stubs(:new).with(
          account: @current_account,
          deflection: deflection,
          resolution_channel_id: resolution_channel_id
        ).returns(ticket_deflector)
      end

      it 'calls deflector service to record rejection' do
        ticket_deflector.expects(:process_deflection_rejection).
          with(
            article_id: article_id,
            reason: reject_reason
          )

        post :reject, params: request_params
        assert_response 200
      end

      describe 'deflection not found for the given id' do
        let(:deflection_id) { 123321 }

        it 'returns 404 not found' do
          TicketDeflector.stubs(:new).returns(ticket_deflector)

          post :reject, params: request_params

          assert_response 404
        end
      end
    end
  end
end
