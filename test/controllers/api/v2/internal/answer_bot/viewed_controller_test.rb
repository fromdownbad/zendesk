require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::AnswerBot::ViewedController do
  fixtures :accounts, :ticket_deflections

  let(:account) { accounts(:minimum) }

  before do
    accept :json
    @current_account = account
  end

  as_a_subsystem_user(account: :minimum) do
    describe 'POST #viewed' do
      let(:deflection) { ticket_deflections(:without_ticket) }
      let(:deflection_id) { deflection.id }
      let(:article_id) { deflection.ticket_deflection_articles.first.article_id }
      let(:request_params) do
        {
          deflection_id: deflection_id,
          article_id: article_id,
        }
      end

      describe 'when an article exists' do
        it 'records viewed article params' do
          Timecop.freeze(2018, 5, 1, 13, 0) do
            TicketDeflectionArticle.any_instance.expects(:update_clicked_at).with(Time.now)

            post :viewed, params: request_params
          end
        end

        it 'responds with ok' do
          post :viewed, params: request_params

          assert_response :ok
        end
      end

      describe 'when a deflection record is not found for the given id' do
        let(:deflection_id) { 123321 }

        it 'responds with not found' do
          post :viewed, params: request_params

          assert_response :not_found
        end
      end

      describe 'when article id is not provided' do
        let (:article_id) { nil }

        it 'responds with bad request' do
          post :viewed, params: request_params

          assert_response :bad_request
        end
      end

      describe 'when the article id provided does not match an existing deflection article' do
        let(:article_id) { 999 }

        before do
          TicketDeflectionArticle.destroy_all
        end

        it 'does not update clicked at' do
          TicketDeflectionArticle.any_instance.expects(:update_clicked_at).never

          post :viewed, params: request_params
        end

        it 'responds with not found' do
          post :viewed, params: request_params

          assert_response :not_found
        end
      end
    end
  end
end
