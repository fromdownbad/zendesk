require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::AnswerBot::ResolutionController do
  fixtures :accounts, :ticket_deflections, :tickets

  let(:account) { accounts(:minimum) }

  before do
    accept :json
    @request.account = @current_account = account
  end

  as_a_subsystem_user(account: :minimum) do
    describe 'POST #resolve' do
      let(:deflection) { ticket_deflections(:without_ticket) }
      let(:deflection_id) { deflection.id }
      let(:article_id) { 11111 }
      let(:resolution_channel_id) { ViaType.ANSWER_BOT_FOR_WEB_WIDGET }
      let(:request_params) do
        {
          deflection_id: deflection_id,
          article_id: article_id,
          resolution_channel_id: resolution_channel_id
        }
      end

      before do
        AnswerBot::TicketCreationOnSolveJob.stubs(:enqueue)
      end

      it 'schedules job' do
        AnswerBot::TicketCreationOnSolveJob.expects(:enqueue).
          with(
            account.id,
            deflection_id,
            article_id,
            resolution_channel_id
          )

        post :resolve, params: request_params
      end

      it 'returns 202 Accepted' do
        post :resolve, params: request_params
        assert_response 202
      end

      describe 'deflection not found for the given id' do
        let(:deflection_id) { 9876 }

        it 'returns 404 not found' do
          post :resolve, params: request_params
          assert_response 404
        end
      end

      describe 'deflection already has a ticket' do
        let(:deflection) { ticket_deflections(:minimum_ticket_deflection) }
        let(:ticket) { tickets(:minimum_1) }
        let(:ticket_deflector) { stub('TicketDeflector', solve: true) }

        before do
          TicketDeflector.stubs(:new).returns(ticket_deflector)
        end

        it 'creates TicketDeflector' do
          TicketDeflector.expects(:new).with(
            account: account,
            deflection: deflection,
            resolution_channel_id: resolution_channel_id
          ).returns(ticket_deflector)

          post :resolve, params: request_params
        end

        it 'invokes ticket_deflector with correct arguments' do
          ticket_deflector.expects(:solve).with(article_id: article_id)

          post :resolve, params: request_params
        end

        it 'returns 202 Accepted' do
          post :resolve, params: request_params
          assert_response 202
        end
      end

      describe 'deflection is from Answer Bot for Slack' do
        let(:resolution_channel_id) { ViaType.ANSWER_BOT_FOR_SLACK }
        before do
          AnswerBot::DeflectionSolveJob.stubs(:enqueue)
        end

        it 'schedules job' do
          AnswerBot::DeflectionSolveJob.expects(:enqueue).
            with(
              account.id,
              deflection_id,
              article_id,
              resolution_channel_id
            )

          post :resolve, params: request_params
        end

        it 'returns 202 Accepted' do
          post :resolve, params: request_params
          assert_response 202
        end
      end
    end
  end
end
