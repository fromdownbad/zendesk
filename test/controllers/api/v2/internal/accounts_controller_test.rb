require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::AccountsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
    @request.account = accounts(:minimum)
  end

  with_options(controller: "api/v2/internal/accounts") do |request|
    request.should_route :get, "/api/v2/internal/accounts", action: "index"
  end

  as_an_admin do
    should_be_forbidden :index
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#index" do
      describe "when passing no parameters" do
        before do
          User.expects(:zendesk_search).never
          get :index
        end

        should_use_presenter Api::V2::AccountPresenter, status: :ok
      end

      describe "when passing an email parameter" do
        before do
          User.expects(:zendesk_search).once.returns(User.all)
          get :index, params: { email: users(:minimum_agent).email }
        end

        should_use_presenter Api::V2::AccountPresenter, status: :ok

        it "does not include subscription" do
          # See discussion here: https://github.com/zendesk/zendesk/pull/3132
          json = JSON.parse(@response.body)
          refute json["accounts"].first.key?("subscription")
        end
      end
    end

    describe "#clear_cache" do
      let(:account) { accounts(:minimum) }

      describe "with the correct shard available" do
        before do
          Account.any_instance.expects(:clear_kasket_cache!)
          Account.any_instance.expects(:clear_reflections_kasket_cache!)
        end

        it "clears the cache" do
          put :clear_cache, params: { account_id: account.id, format: :json }
          assert_response 200
        end

        # FIXME: this test is wrong ... it would blow up if we did not set request.account = above
        it "works with a locked account and doesn't bump cache version" do
          account.lock!

          # defeat recycle_with_rack_reload from raising

          Account.any_instance.stubs(:reload)
          put :clear_cache, params: { account_id: account.id, format: :json }
          assert_response 200
          assert_equal 0, account.settings.account_cache_version
        end
      end

      describe "with no shard available" do
        it "only clears kasket" do
          ActiveRecord::Base.shard_names.stubs(:include?).returns(false)

          Account.any_instance.expects(:clear_kasket_cache!)
          Account.any_instance.expects(:clear_reflections_kasket_cache!).never

          put :clear_cache, params: { account_id: account.id, format: :json }
        end
      end
    end

    describe "#sync_support_product" do
      let(:account) { accounts(:minimum) }

      it 'syncs support product for account' do
        BackfillAccountProducts.any_instance.expects(:perform).once
        post :sync_support_product, params: { account_id: account.id, format: :json }
        assert_response :ok
      end

      it "returns not_found when it can't find the account" do
        BackfillAccountProducts.any_instance.expects(:perform).never
        post :sync_support_product, params: { account_id: 0, format: :json }
        assert_response :not_found
      end
    end

    describe '#sync_account_roles' do
      let(:account) { accounts(:minimum) }

      it 'starts a sync job for account roles' do
        Omnichannel::AccountRolesSyncJob.expects(:enqueue).with(account_id: account.id)
        post :sync_account_roles, params: { account_id: account.id, format: :json }
        assert_response :ok
      end

      describe 'account does not exist' do
        it "returns not_found when it can't find the account" do
          Omnichannel::AccountRolesSyncJob.expects(:enqueue).never
          post :sync_account_roles, params: { account_id: 1234567890, format: :json }
          assert_response :not_found
        end
      end
    end

    describe '#sync_account_users_entitlements' do
      let(:account) { accounts(:minimum) }

      it 'starts a sync job for account users entitlements' do
        Omnichannel::AccountUsersEntitlementsSyncJob.expects(:enqueue).with(account_id: account.id)
        post :sync_account_users_entitlements, params: { account_id: account.id, format: :json }
        assert_response :ok
      end

      it 'starts a sync job for explore, guide and talk entitlements' do
        Omnichannel::ExploreAccountSyncJob.expects(:enqueue).with(account_id: account.id)
        Omnichannel::GuideAccountSyncJob.expects(:enqueue).with(account_id: account.id)
        Omnichannel::TalkAccountSyncJob.expects(:enqueue).with(account_id: account.id)
        post :sync_account_users_entitlements, params: { account_id: account.id, format: :json }
        assert_response :ok
      end

      describe 'account does not exist' do
        it "returns not_found when it can't find the account" do
          Omnichannel::AccountUsersEntitlementsSyncJob.expects(:enqueue).never
          Omnichannel::ExploreAccountSyncJob.expects(:enqueue).never
          Omnichannel::GuideAccountSyncJob.expects(:enqueue).never
          Omnichannel::TalkAccountSyncJob.expects(:enqueue).never
          post :sync_account_users_entitlements, params: { account_id: 3, format: :json }
          assert_response :not_found
        end
      end

      describe 'user is specified' do
        let(:user) { users(:minimum_agent) }
        let(:products) { ['support', 'guide', 'explore', 'talk'] }

        it 'starts a job to sync entitlements' do
          Omnichannel::EntitlementsSyncJob.expects(:enqueue).with(account_id: account.id, user_id: user.id, products: products)
          post :sync_account_users_entitlements, params: { account_id: account.id, user_id: user.id, format: :json }
          assert_response :ok
        end

        describe 'user does not exist' do
          it 'returns not_found' do
            Omnichannel::EntitlementsSyncJob.expects(:enqueue).never
            post :sync_account_users_entitlements, params: { account_id: account.id, user_id: 999, format: :json }
            assert_response :not_found
          end
        end
      end
    end
  end
end
