require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::ComplianceAgreementsController do
  extend Api::V2::TestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    accept :json
    @request.account = account
  end

  with_options(controller: "api/v2/internal/compliance_agreements") do |request|
    request.should_route :get, "/api/v2/internal/compliance_agreements", action: "index"
    request.should_route :post, "/api/v2/internal/compliance_agreements", action: "create"
    request.should_route :delete, "/api/v2/internal/compliance_agreements/46", action: "destroy", id: 46
  end

  as_an_admin do
    should_be_forbidden :index, :create, :destroy
  end

  as_a_subsystem_user(account: :minimum) do
    let(:compliance) { Compliance.create!(name: 'HIPPA') }

    describe "a GET to :index" do
      before { get :index }

      should_use_presenter Api::V2::Internal::ComplianceAgreementPresenter, status: :ok
    end

    describe "a POST to :create" do
      before do
        post :create, params: { compliance_agreement: { compliance_id: compliance.id } }
      end

      should_use_presenter Api::V2::Internal::ComplianceAgreementPresenter, status: :created

      should_change("the account agreements count", by: 1) { account.compliance_agreements.count(:all) }
    end

    describe "a DELETE to :destroy" do
      before do
        @compliance_agreement = account.compliance_agreements.create! do |ca|
          ca.compliance_id = compliance.id
        end
      end

      it "responds with 204" do
        delete :destroy, params: { id: @compliance_agreement.id }
        assert_response :no_content
      end

      it "deletes the item" do
        delete :destroy, params: { id: @compliance_agreement.id }
        assert_nil account.reload.compliance_agreements.find_by_id(@compliance_agreement.id)
      end
    end
  end
end
