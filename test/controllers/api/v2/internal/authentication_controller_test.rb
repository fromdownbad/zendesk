require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 11

describe Api::V2::Internal::AuthenticationController do
  extend Api::V2::TestHelper

  fixtures :all

  describe "configurations" do
    should_route :put, "/api/v2/internal/authentication", controller: "api/v2/internal/authentication", action: :update

    before do
      accept :json
    end

    as_a_subsystem_user(account: :minimum, user: 'classic') do
      describe "in production" do
        before do
          Rails.env.stubs(production?: true)
          put :update
        end

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "public forums" do
      end

      describe "google_login" do
      end

      describe "setting remote authentications" do
        let(:strategy) { remote_authentications(:minimum_remote_authentication) }

        let(:default_attributes) do
          {
            is_active: false,
            auth_mode: RemoteAuthentication::JWT
          }
        end

        let(:new_attributes) do
          {
            is_active: true
          }
        end

        describe "single strategy" do
          before do
            strategy.update_attributes!(default_attributes)

            put :update, params: { sso: new_attributes }

            strategy.reload
          end

          it('responds with ok') { assert_response :ok }

          it "updates the SSO strategy" do
            assert strategy.is_active?
          end
        end
      end

      describe "account settings" do
        [:prefer_lotus].each do |setting|
          describe "setting #{setting}" do
            before do
              @account.settings.send("#{setting}=", !setting_value)
              @account.settings.save!

              put :update, params: { setting => setting_value }
              @account.settings.reload
            end

            describe "enabled" do
              let(:setting_value) { true }

              it('responds with ok') { assert_response :ok }

              it "is enabled" do
                assert @account.settings.send("#{setting}?")
              end
            end

            describe "disabled" do
              let(:setting_value) { false }

              it('responds with ok') { assert_response :ok }

              it "is disabled" do
                refute @account.settings.send("#{setting}?")
              end
            end
          end
        end
      end

      describe "help center state" do
        ["enabled", "disabled", "restricted"].each do |state|
          describe "setting state to #{state}" do
            before do
              put :update, params: { help_center_state: state }
              @account.reload
            end

            it('responds with ok') { assert_response :ok }

            it "sets help center state" do
              assert_equal state, @account.send("help_center_state").to_s
            end
          end
        end
      end

      describe "web portal state" do
        ["enabled", "disabled", "restricted"].each do |state|
          describe "setting state to #{state}" do
            before do
              put :update, params: { web_portal_state: state }
              @account.reload
            end

            it('responds with ok') { assert_response :ok }

            it "sets help center state" do
              assert_equal state, @account.send("web_portal_state").to_s
            end
          end
        end
      end

      describe "ip limitations" do
        let(:ip_ranges) { "127.0.0.1/8, 192.168.0.1/32, no way" }

        describe "enabled" do
          before do
            put :update, params: { ip_ranges: ip_ranges }
            @account.reload
          end

          it('responds with ok') { assert_response :ok }

          it "updates ip_ranges" do
            assert_equal ip_ranges, @account.texts.ip_restriction
            assert @account.settings.ip_restriction_enabled?
          end
        end

        describe "disabled" do
          before do
            @account.texts.ip_restriction = "127.0.0.1/8"
            @account.texts.save!

            @account.settings.ip_restriction_enabled = true
            @account.settings.save!

            put :update, params: { ip_ranges: "" }
            @account.reload
          end

          it('responds with ok') { assert_response :ok }

          it "disables ip_restriction_enabled?" do
            assert_equal '127.0.0.1/8', @account.texts.ip_restriction
            refute @account.settings.ip_restriction_enabled?
          end
        end
      end
    end
  end
end
