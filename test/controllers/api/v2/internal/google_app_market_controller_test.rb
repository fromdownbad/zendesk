require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Api::V2::Internal::GoogleAppMarketController do
  extend Api::V2::TestHelper
  fixtures :accounts, :brands, :role_settings

  Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

  before do
    @access_token = 'access_token'
    @customer_id = "abc0123"
    @domain = 'zobuzendesk.com'
    @username = 'haabaato'
    @email = "user@#{@domain}"
    session['google_token'] = @access_token
  end

  def stub_email_forwarding_setup_calls_except(except_method: nil)
    private_methods = [:get_and_write_site_verification_token, :verify_site_token]
    client_methods = [:add_domain_alias, :create_email_forwarding_filter, :add_group_member, :enable_public_group_emails]
    job_methods = [:enqueue_in]

    private_methods.each do |method|
      if method != except_method
        Api::V2::Internal::GoogleAppMarketController.any_instance.stubs(method).returns({})
      end
    end

    client_methods.each do |method|
      if method != except_method
        Zendesk::Google::GAMClient.any_instance.stubs(method).returns({})
      end
    end

    job_methods.each do |method|
      if method != except_method
        GAMEmailForwardingJob.stubs(method).returns
      end
    end

    GAMEmailForwardingJob.stubs(:sleep)
  end

  def post_email_forwarding
    post 'email_forwarding_setup', params: { customer_id: @customer_id, domain: @domain, owner: @username, email: @email }
  end

  def post_group_email_forwarding
    post 'group_email_forwarding_setup', params: { email: @email }
  end

  describe "Email Settings API" do
    before do
      accept :json
    end

    with_options(controller: "api/v2/internal/google_app_market") do |request|
      request.should_route :post, "/api/v2/internal/google_app_market/email_forwarding_setup", action: "email_forwarding_setup"
    end

    describe "a POST to :email_forwarding_setup" do
      before do
        @account = accounts(:minimum)
        @brand = brands(:minimum)
        @account.default_brand = @brand
        @account.save!
        @controller.stubs(:current_brand).returns(@brand)
      end

      as_an_admin do
        describe "#GAMClient.add_domain_alias" do
          before do
            @url = "https://www.googleapis.com/admin/directory/v1/customer/#{@customer_id}/domainaliases"
            stub_email_forwarding_setup_calls_except(except_method: :add_domain_alias)
          end

          it "returns failure when it errors" do
            body = '{"error":{"errors":[{"domain":"global","reason":"badRequest","message":""}],"code":400,"message":""}}'
            stub_request(:post, @url).to_return(status: 400, body: body)
            post_email_forwarding

            assert_response 400
            expected = JSON.parse(body).to_json
            assert_equal expected, response.body
          end

          it "returns success when the error reason is duplicate" do
            body = '{"error":{"errors":[{"domain":"global","reason":"duplicate","message":""}],"code":200,"message":""}}'
            stub_request(:post, @url).to_return(status: 200, body: body)
            post_email_forwarding

            assert_response :success
          end

          it "returns success when it returns a valid response" do
            body = '{
                "kind": "admin#directory#domainAlias",
                "etag": "iwpzoDgSq9BJw-XzORg0bILYPVc/QVrDQ30kAV59H2-njqriXagvwFc",
                "domainAliasName": "zobuzendesk.zendesk.com",
                "parentDomainName": "zobuzendesk.com",
                "verified": false,
                "creationTime": "1457388655589"
              }'
            stub_request(:post, @url).to_return(status: 200, body: body)
            post_email_forwarding

            assert_response :success
          end
        end

        describe "#get_and_write_site_verification_token" do
          before do
            @url = "https://www.googleapis.com/siteVerification/v1/token"
            stub_email_forwarding_setup_calls_except(except_method: :get_and_write_site_verification_token)
          end

          it "returns failure when it errors" do
            body = '{"error":{"errors":[{"domain":"global","reason":"badRequest","message":""}],"code":400,"message":""}}'
            stub_request(:post, @url).to_return(status: 400, body: body)

            post_email_forwarding

            assert_response 400
            expected = JSON.parse(body).to_json
            assert_equal expected, response.body
          end

          it "returns success, returns token, and saves token into account settings and redis" do
            token = 'google82bd8ec382b139d4.html'
            body = '{"method":"FILE","token":"' + token + '"}'
            stub_request(:post, @url).to_return(status: 200, body: body)

            post_email_forwarding

            assert_response :success
            assert_equal token, @account.settings.google_site_verification_token
            redis_client = @controller.send(:redis_client)
            redis_key = @controller.send(:google_site_verification_token_redis_key, @account)
            assert_equal token, redis_client.get(redis_key)
          end
        end

        describe "#verify_site_token" do
          before do
            @url = "https://www.googleapis.com/siteVerification/v1/webResource?verificationMethod=FILE"
            stub_email_forwarding_setup_calls_except(except_method: :verify_site_token)
          end

          it "returns failure when it errors" do
            body = '{"error":{"errors":[{"domain":"global","reason":"badRequest","message":"The necessary verification token could not be found on your site."}],"code":400,"message":"The necessary verification token could not be found on your site."}}'
            stub_request(:post, @url).to_return(status: 400, body: body)
            post_email_forwarding

            assert_response 400
            expected = JSON.parse(body).to_json
            assert_equal expected, response.body
          end

          it "returns success when it returns a valid response" do
            body = '{
                "id":"http%3A%2F%2Fzobuzendesk.com%2F",
                "site":{"type":"SITE","identifier":"http://zobuzendesk.com/"},
                "owners":["haabaato@zobuzendesk.com"]
              }'
            stub_request(:post, @url).to_return(status: 200, body: body)
            post_email_forwarding

            assert_response :success
          end
        end

        describe "when all previous APIs succeed" do
          before do
            stub_email_forwarding_setup_calls_except(except_method: :enqueue)

            zendesk_subdomain = @controller.send(:zendesk_subdomain)
            @forward_to_email = "support@#{zendesk_subdomain}"
            body = <<~XML
              <?xml version='1.0' encoding='UTF-8'?>
              <entry xmlns='http://www.w3.org/2005/Atom' xmlns:apps='http://schemas.google.com/apps/2006'>
                <id>https://apps-apis.google.com/a/feeds/emailsettings/2.0/#{@domain}/#{@username}/filter/0</id>
                <updated>2016-04-27T22:27:53.369Z</updated>
                <link rel='self' type='application/atom+xml' href='https://apps-apis.google.com/a/feeds/emailsettings/2.0/#{@domain}/#{@username}/filter/0'/>
                <link rel='edit' type='application/atom+xml' href='https://apps-apis.google.com/a/feeds/emailsettings/2.0/#{@domain}/#{@username}/filter/0'/>
                <apps:property name='forwardTo' value='#{@forward_to_email}'/>
                <apps:property name='to' value='hello@zobuzendesk.com'/>
              </entry>
            XML
            stub_request(:post, "https://apps-apis.google.com/a/feeds/emailsettings/2.0/zobuzendesk.com/haabaato/filter").to_return(status: 204, body: body)
            @user = @controller.send(:current_user)
          end

          it "enqueues an email forwarding job" do
            GAMEmailForwardingJob.expects(:enqueue_in).with(
              5.minutes, @account.id, @user.id, @access_token, @domain,
              @username, @email, @forward_to_email
            )
            post_email_forwarding

            assert_response :success
          end

          it "creates a ticket to notify user about email forwarding setup" do
            Ticket.expects(:generate_email_forward_pending_ticket).with(@account, @user)

            post_email_forwarding
          end

          it "creates a non-default recipient address for the forwarded email; does not send verification email" do
            post_email_forwarding

            recipient_address = @account.recipient_addresses.find_by_email(@email)
            assert recipient_address
            refute recipient_address.default?
            refute recipient_address.forwarding_sent_at
          end

          it "returns success even if recipient address already exists" do
            @account.recipient_addresses << RecipientAddress.new(
              email: @email,
              default: false,
              name: @brand.name,
              brand: @brand
            )
            recipient_address = @account.recipient_addresses.find_by_email(@email)
            assert recipient_address

            post_email_forwarding

            assert_response :success
          end
        end
      end
    end
  end

  describe "Google Site Verification" do
    before do
      @token = '123abctoken'
      @html_token = "google#{@token}.html"
      accept :json
    end

    describe "a GET to :google_site_verification" do
      as_an_end_user do
        before do
          @account = accounts(:minimum)
        end

        it "returns error if token param is not alphanumeric" do
          get 'google_site_verification', params: { token: 'abc123!$' }

          assert_response 400
        end

        describe "with an account not found by zendesk_core_middleware" do
          before do
            Api::V2::Internal::GoogleAppMarketController.any_instance.
              stubs(:current_account).returns(nil)
          end

          it "returns 404 if account is not found" do
            get 'google_site_verification', params: { token: @token }

            assert_response 404
          end
        end

        describe "with token stored in account settings" do
          before do
            @account.settings.google_site_verification_token = @html_token
            @account.settings.save!
          end

          it "returns the site verification token in plain text" do
            get 'google_site_verification', params: { token: @token }

            assert_response :success
            assert_match @html_token, response.body
          end
        end

        describe "with token stored in redis" do
          before do
            redis_client = @controller.send(:redis_client)
            redis_key = @controller.send(:google_site_verification_token_redis_key, @account)
            redis_client.set(redis_key, @html_token)
          end

          it "returns the site verification token in plain text" do
            get 'google_site_verification', params: { token: @token }

            assert_response :success
            assert_match @html_token, response.body
          end
        end

        describe "with host_mapping as request.host" do
          before do
            host_mapping = 'minimum.example.com'
            request.host = host_mapping
            @account.update_attribute(:host_mapping, host_mapping)
            @account.settings.google_site_verification_token = @html_token
            @account.settings.save!
          end

          it "returns the site verification token in plain text" do
            get 'google_site_verification', params: { token: @token }

            assert_response :success
            assert_match @html_token, response.body
          end
        end
      end
    end
  end

  describe "Setting up email forwarding for a G Suite Groups email" do
    before do
      accept :json
    end

    describe "a POST to :group_email_forwarding_setup" do
      let(:account) { accounts(:minimum) }

      as_an_admin do
        describe "calls #GAMClient.add_group_member" do
          before do
            @url = "https://www.googleapis.com/admin/directory/v1/groups/#{@email}/members"
            stub_email_forwarding_setup_calls_except(except_method: :add_group_member)
          end

          it "returns failure when it errors" do
            body = '{"error":{"errors":[{"domain":"global","reason":"badRequest","message":""}],"code":400,"message":""}}'
            stub_request(:post, @url).to_return(status: 400, body: body)
            post_group_email_forwarding

            assert_response 400
            expected = JSON.parse(body).to_json
            assert_equal expected, response.body
          end

          it "returns success when the error reason is duplicate" do
            body = '{"error":{"errors":[{"domain":"global","reason":"duplicate","message":""}],"code":200,"message":""}}'
            stub_request(:post, @url).to_return(status: 200, body: body)
            post_group_email_forwarding

            assert_response :success
          end

          it "returns success on valid response" do
            stub_request(:post, @url).to_return(status: 200)
            post_email_forwarding
            assert_response :success
          end
        end

        describe "calls #GAMClient.enable_public_group_emails" do
          before do
            @url = "https://www.googleapis.com/groups/v1/groups/#{@email}"
            stub_email_forwarding_setup_calls_except(except_method: :enable_public_group_emails)
          end

          it "returns failure when it errors" do
            body = '{"error":{"errors":[{"domain":"global","reason":"badRequest","message":""}],"code":400,"message":""}}'
            stub_request(:patch, @url).to_return(status: 400, body: body)
            post_group_email_forwarding

            assert_response 400
            expected = JSON.parse(body).to_json
            assert_equal expected, response.body
          end

          it "returns success on valid response" do
            stub_request(:patch, @url).to_return(status: 200)
            post_email_forwarding
            assert_response :success
          end
        end

        describe "on successful API responses" do
          before do
            stub_email_forwarding_setup_calls_except
            @brand = brands(:minimum)
            @controller.stubs(:current_brand).returns(@brand)
          end

          it "creates a recipient address for the forwarded email" do
            post_email_forwarding
            assert @account.recipient_addresses.find_by_email(@email)
          end

          it "does not send a verification email" do
            post_email_forwarding

            recipient_address = @account.recipient_addresses.find_by_email(@email)
            assert recipient_address
            refute recipient_address.forwarding_sent_at
          end
        end
      end
    end
  end

  describe "Qualifies for GAM" do
    def stub_mx(domain)
      mx = Resolv::DNS::Resource::IN::MX.new(0, domain)
      Resolv::DNS.stubs(:open).returns(stub(getresources: [mx]))
    end

    def assert_rejection
      assert_response :success
      assert_equal false.to_json, response.body
      assert_equal '', @redis_client.get(@redis_key)
    end

    before do
      accept :json
    end

    describe "a GET to :qualifies_for_google_apps" do
      let(:account) { accounts(:minimum) }

      as_an_admin do
        before do
          @redis_client = @controller.send(:redis_client)
          @redis_key = @controller.send(:google_apps_domain_redis_key, account)
          Rails.logger.stubs(:info)
        end

        it "returns response cached in redis" do
          expected = true
          @redis_client.set(@redis_key, expected)

          get :qualifies_for_google_apps
          assert_equal expected.to_json, response.body
        end

        describe "multibrand enabled" do
          before do
            request.account.stubs(:has_multibrand?).returns(true)
            FactoryBot.create(:brand, account_id: account.id, active: false)
          end

          it "returns false when account has multiple brands" do
            get :qualifies_for_google_apps
            assert_rejection
          end
        end

        describe "is a hub" do
          before do
            spoke_account = accounts(:support)
            spoke_account.subscription.update_attributes!(hub_account_id: request.account.id)
          end

          it "returns false" do
            get :qualifies_for_google_apps
            assert_rejection
          end
        end

        describe "is a spoke" do
          before do
            hub_account = accounts(:support)
            request.account.subscription.update_attributes!(hub_account_id: hub_account.id)
          end

          it "returns false" do
            get :qualifies_for_google_apps
            assert_rejection
          end
        end

        it "returns false when account has remote login enabled for agents" do
          account.role_settings.update_attribute(:agent_zendesk_login, false)
          account.role_settings.update_attribute(:agent_google_login, false)
          account.role_settings.update_attribute(:agent_remote_login, true)

          Rails.logger.expects(:info).with('agent remote login cannot be enabled')
          get :qualifies_for_google_apps
          assert_rejection
        end

        describe "agent has zendesk or google login enabled" do
          before { account.role_settings.update_attribute(:agent_google_login, true) }

          it "returns false when owner only has gmail or aliased emails" do
            owner = account.owner
            owner.identities.delete_all
            owner.reload
            UserEmailIdentity.create!(user: owner, value: 'owner@gmail.com', is_verified: true)
            UserEmailIdentity.create!(user: owner, value: 'owner+foo@bar.com')

            Rails.logger.expects(:info).with(regexp_matches(/owner does not have one valid G Suite email/))
            get :qualifies_for_google_apps
            assert_rejection
          end

          describe "agent has a valid email" do
            it "returns false when owner has no G Suite domain in their emails" do
              stub_mx('outlook.com')
              Rails.logger.expects(:info).with(regexp_matches(/owner does not have one valid G Suite email/))
              get :qualifies_for_google_apps
              assert_rejection
            end

            describe "owner has a valid G Suite domain" do
              before do
                stub_mx('google.com')
                account.owner.identities.delete_all
                @owner_domain = 'googledomain.com'
                owner_email = "owner@#{@owner_domain}"
                UserEmailIdentity.create!(user: account.owner, value: owner_email)
              end

              it "returns false when owner has multiple G Suite domains in their emails" do
                UserEmailIdentity.create!(user: account.owner, value: 'owner@another-googledomain.com')
                Rails.logger.expects(:info).with(regexp_matches(/owner does not have one valid G Suite email/))
                get :qualifies_for_google_apps
                assert_rejection
              end

              it "returns false when another account route is associated with the G Suite domain" do
                other_account = accounts(:support)
                other_account.route.update_attribute(:gam_domain, @owner_domain)
                Rails.logger.expects(:info).with('gam_domain already in use')
                get :qualifies_for_google_apps
                assert_rejection
              end

              it "returns false when not all admins and agents are on the same G Suite domain" do
                Rails.logger.expects(:info).with('not all admins and agents are on the same G Suite domain')
                get :qualifies_for_google_apps
                assert_rejection
              end

              describe "all admins and agents are on same G Suite domain" do
                before do
                  account.users.each_with_index do |user, i|
                    next if user.is_end_user? || user == account.owner
                    UserEmailIdentity.create!(user: user, value: "user#{i}@googledomain.com")
                  end
                end

                it "returns true" do
                  get :qualifies_for_google_apps

                  assert_response :success
                  assert_equal true.to_json, response.body
                  assert_equal @owner_domain, @redis_client.get(@redis_key)
                end
              end
            end
          end
        end
      end
    end
  end

  describe "Enabling GAM for existing customers" do
    before do
      accept :json
    end

    describe "a POST to :enable_google_apps" do
      let(:account) { accounts(:minimum) }

      as_an_admin do
        before do
          @redis_client = @controller.send(:redis_client)
          @redis_key = @controller.send(:google_apps_domain_redis_key, account)
          @google_domain = 'googledomain.com'
        end

        it "returns an error if account already has GAM enabled" do
          account.route.update_attribute(:gam_domain, @google_domain)
          account.role_settings.update_attribute(:agent_google_login, true)
          post :enable_google_apps

          assert_response :unprocessable_entity
          expected = {
            error: 'AlreadyEnabled',
            description: 'Account already has G Suite integration'
          }
          assert_equal expected.to_json, response.body
        end

        describe "GAM is not already enabled" do
          it "returns an error when account does not qualify for GAM" do
            post :enable_google_apps

            assert_response :unprocessable_entity
            expected = {
              error: 'DoesNotQualify',
              description: 'Account does not qualify for G Suite integration'
            }
            assert_equal expected.to_json, response.body
          end

          describe "account qualifies for GAM" do
            before do
              @controller.stubs(eligible_google_apps_domain: @google_domain)
            end

            it "updates account route, settings, role_settings, and returns the GAM nav link with params " do
              post :enable_google_apps, params: { return_to: '/return/to' }
              assert_response :success

              assert_equal @google_domain, account.route.gam_domain
              assert_equal @google_domain, account.settings.google_apps_domain
              assert account.settings.switched_to_google_apps
              assert account.settings.has_google_apps
              refute account.role_settings.agent_zendesk_login
              assert account.role_settings.agent_google_login

              nav_link = response.body.delete '"'
              params = Rack::Utils.parse_nested_query(nav_link.split('?', 2).last)
              nav_link.must_include 'gam_universal_navigation_link'
              refute_nil params['return_to']
              assert_equal @account.subdomain, params['subdomain']
              assert_equal 'activate-google-apps', params['source']
              assert_equal @google_domain, params['hd']
            end
          end
        end
      end
    end
  end

  describe "QA endpoints for browser tests" do
    before do
      accept :json
      @gam_domain = "example.com"
    end

    describe "a POST to :unbind_google_apps_domain" do
      let(:account) { accounts(:minimum) }

      as_an_end_user do
        it "fails if environment is production" do
          Rails.env.stubs(:production?).returns(true)

          post 'unbind_google_apps_domain'
          assert_response 400
        end

        describe "in non-production environments" do
          before do
            account.route.gam_domain = @gam_domain
            account.route.save!
            session['google_token'] = 'google_access_token'
          end

          after do
            session.delete('google_token')
          end

          it "nils route.gam_domain" do
            assert_equal 1, Route.where(gam_domain: @gam_domain).count

            post 'unbind_google_apps_domain', params: { domain: @gam_domain }

            assert_response :success
            assert_equal 0, Route.where(gam_domain: @gam_domain).count
          end
        end
      end
    end

    describe "a POST to :purge_google_apps_domain_aliases" do
      let(:account) { accounts(:minimum) }

      as_an_end_user do
        it "fails if environment is production" do
          Rails.env.stubs(:production?).returns(true)

          post 'purge_google_apps_domain_aliases'
          assert_response 400
        end

        describe "in non-production environments" do
          before do
            @client_stub = stub(delete_domain_alias: nil, get_domain_aliases: {domainAliases: []})
            @controller.stubs(:gam_client).returns(@client_stub)
          end

          describe "domain alias deletion" do
            it "does not delete the domain alias if session is incomplete" do
              @client_stub.expects(:delete_domain_alias).never

              post 'purge_google_apps_domain_aliases', params: { domain: @gam_domain, customer_id: @customer_id }

              assert_response :success
            end

            describe "session contains valid google information" do
              before do
                session['google_token'] = @access_token
              end

              it "fetches all domain aliases" do
                @client_stub.expects(:get_domain_aliases).with(customer_id: @customer_id).returns(domainAliases: [])

                post 'purge_google_apps_domain_aliases', params: { domain: @gam_domain, customer_id: @customer_id }
              end

              it "deletes all previously created G Suite domain aliases if session contains all info" do
                body = {
                  domainAliases: [
                    # Default domain alias is ignored
                    { domainAliasName: "zobuzendesk.com.test-google-a.com" },
                    { domainAliasName: "herbtest25.zd-staging.com" },
                    { domainAliasName: "slick-lynx-2720.vagrantshare.com" }
                  ]
                }
                @client_stub.expects(:get_domain_aliases).with(customer_id: @customer_id).returns(body)

                @controller.send(:zendesk_subdomain)
                GsuiteDeleteDomainAliasJob.expects(:enqueue).with(@access_token, @gam_domain, @customer_id, 'zobuzendesk.com.test-google-a.com').never
                GsuiteDeleteDomainAliasJob.expects(:enqueue).with(@access_token, @gam_domain, @customer_id, 'herbtest25.zd-staging.com')
                GsuiteDeleteDomainAliasJob.expects(:enqueue).with(@access_token, @gam_domain, @customer_id, 'slick-lynx-2720.vagrantshare.com')

                post 'purge_google_apps_domain_aliases', params: { domain: @gam_domain, customer_id: @customer_id }

                assert_response :success
              end
            end
          end
        end
      end
    end
  end
end
