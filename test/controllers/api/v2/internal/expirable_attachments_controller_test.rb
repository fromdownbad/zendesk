require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::ExpirableAttachmentsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: "api/v2/internal/expirable_attachments") do |request|
    request.should_route :post, "/api/v2/internal/expirable_attachments", action: "create"
  end

  as_a_subsystem_user(account: :minimum) do
    it "creates an expirable attachment" do
      stub_request(:put, %r{\.amazonaws\.com/data/expirable_attachments/.*/report\.zip})
      assert_difference("ExpirableAttachment.count(:all)", 1) do
        post :create, params: { account_id: accounts(:minimum).id, user_id: users(:minimum_end_user).id, filename: "report.zip", uploaded_data: fixture_file_upload("small.png") }
        assert_response 201, url: ExpirableAttachment.last.url
      end
    end
  end
end
