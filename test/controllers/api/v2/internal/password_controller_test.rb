require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::PasswordController do
  extend Api::V2::TestHelper
  fixtures :all
  [:multiproduct, :shell_account_without_support].each do |account_type|
    describe "for account #{account_type}" do
      let(:account) { accounts(account_type) }

      before do
        accept :json
        @request.account = account
        @deliveries = ActionMailer::Base.perform_deliveries
        ActionMailer::Base.perform_deliveries = true
      end

      after { ActionMailer::Base.perform_deliveries = @deliveries }

      as_an_admin do
        should_be_forbidden [:post, :reset, staff_id: 1, requester_id: 1]
      end

      as_a_subsystem_user(user: 'metropolis', account: account_type) do
        let(:requester) { User.find(account.owner_id) }
        let(:user) do
          FactoryBot.create(:user, account: account, roles: Role::END_USER.id)
        end

        describe '#create' do
          it 'returns ok' do
            post :create, params: { staff_id: user.id, requester_id: requester.id, password: 'new_password' }
            assert_response :ok
          end

          it 'should deliver email for successfully setting password' do
            post :create, params: { staff_id: user.id, requester_id: requester.id, password: 'new_password' }
            assert_equal 1, ActionMailer::Base.deliveries.size
            assert_equal "User profile updated: password changed", ActionMailer::Base.deliveries.first.subject
          end
        end

        describe '#update' do
          let(:old_password) { 'old_password' }
          before { user.change_password!(old_password) }

          it 'returns ok' do
            put :update, params: { staff_id: user.id, requester_id: requester.id, previous_password: 'old_password', password: 'new_password' }
            assert_response :ok
          end

          it 'should deliver email for password change' do
            put :update, params: { staff_id: user.id, requester_id: requester.id, previous_password: 'old_password', password: 'new_password' }
            assert_equal 1, ActionMailer::Base.deliveries.size
            assert_equal 'User profile updated: password changed', ActionMailer::Base.deliveries.first.subject
          end

          it 'should return 422 when old password is incorrect' do
            put :update, params: { staff_id: user.id, requester_id: requester.id, previous_password: 'wrong_password', password: 'new_password' }
            body = JSON.parse(@response.body)

            assert_equal 0, ActionMailer::Base.deliveries.size
            assert_response :unprocessable_entity
            assert_equal 'RecordInvalid', body['error']
            assert_equal 'The current password you entered is not correct, your password has not been changed', body['details']['base'].first['description']
          end
        end

        describe '#reset' do
          it 'returns a 400 with MissingRequesterId error when missing the requester_id' do
            post :reset, params: { staff_id: user.id }
            assert_response 400
            body = JSON.parse(@response.body)

            assert_equal 'MissingRequesterId', body['error']
            assert_equal "'requester_id' is a required field", body['description']
          end

          it 'should deliver email for password reset' do
            post :reset, params: { staff_id: user.id, requester_id: requester.id }
            assert_equal 1, ActionMailer::Base.deliveries.size
            assert_equal "#{account.name} password reset", ActionMailer::Base.deliveries.first.subject
          end

          it('responds with ok') do
            post :reset, params: { staff_id: user.id, requester_id: requester.id }
            assert_response :ok
          end

          describe 'for a user without a password' do
            before do
              user.update_attribute(:crypted_password, nil)
              ActionMailer::Base.deliveries = [] # To remove email sent after a password is changed
              post :reset, params: { staff_id: user.id, requester_id: requester.id }
            end

            it 'should deliver email for password reset' do
              assert_equal 1, ActionMailer::Base.deliveries.size
              assert_equal "Create a password for #{account.name}", ActionMailer::Base.deliveries.first.subject
            end

            it('responds with ok') { assert_response :ok }
          end
        end
      end
    end
  end
end
