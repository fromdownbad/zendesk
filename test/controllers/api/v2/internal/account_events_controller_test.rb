require_relative '../../../../support/test_helper'
require_relative '../../../../support/account_products_test_helper'
require_relative '../../../../support/chat_phase_3_helper'
require_relative '../../../../support/entitlement_test_helper'
require_relative '../../../../support/zopim_test_helper'
require_relative '../../../../support/suite_test_helper'
require_relative '../../../../support/multiproduct_test_helper'

SingleCov.covered!

describe Api::V2::Internal::AccountEventsController do
  extend Api::V2::TestHelper
  include ZopimTestHelper
  include AccountProductsTestHelper
  include ChatPhase3Helper
  include EntitlementTestHelper
  include SuiteTestHelper
  include MultiproductTestHelper

  fixtures :all

  let(:subdomain) { account.subdomain }
  let(:has_synchronous_chat_provisioning) { false }
  let(:phase_three) { true }
  let(:account) { accounts(:multiproduct) }
  let(:products) { [] }
  let(:products_response) { make_generic_products_response(account, products) }
  let(:notification_type) { '' }
  let(:updated_product) { '' }
  let(:created_product) { '' }
  let(:patch_status) { 200 }
  let(:params) do
    {
      account_id: account.id,
      id: 1,
      name: updated_product,
      old_owner_id: account.owner.id,
      new_owner_id: account.owner.id,
      notification_type: notification_type
    }
  end
  let(:product_created_params) do
    {
      account_id: account.id,
      id: 1,
      name: created_product,
      old_owner_id: account.owner.id,
      new_owner_id: account.owner.id,
      notification_type: notification_type
    }
  end
  let(:cancelled_chat_product) do
    chat_product['active'] = false
    chat_product
  end

  before do
    Timecop.freeze
    accept :json
    @request.account = account
    account.stubs(
      has_light_agents?: true,
      has_chat_permission_set?: true,
      has_permission_sets?: true,
      has_synchronous_chat_provisioning?: has_synchronous_chat_provisioning,
      install_app: true,
      uninstall_app: true
    )
    ::PermissionSet.create_light_agent!(account)
    ::PermissionSet.create_chat_agent!(account)
    ZBC::Zopim::Subscription.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      ensure_not_phase_four: nil
    )
    ZopimIntegration.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      phase_three?: phase_three,
      ensure_not_phase_four: nil
    )
    stub_request(:get, "https://multiproduct.zendesk-test.com/api/v2/apps/installations")
    stub_request(:post, %r{embeddable/api/internal/config_sets.json})
    stub_request(
      :get,
      %r{#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/\d+/products}
    ).with(
      headers: {
        'Host' => "#{subdomain}.#{Zendesk::Configuration['host']}"
      }
    ).to_return(
      status: 200,
      body: products_response,
      headers: {"Content-Type" => "application/json"}
    )
    stub_request(
      :patch,
      %r{#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/\d+/products/chat}
    ).with(
      headers: {
        'Host' => "#{subdomain}.#{Zendesk::Configuration['host']}"
      }
    ).to_return(
      status: 200,
      body: "",
      headers: {}
    )
    stub_staff_service_patch_request('multiproduct', patch_status)
    stub_account_service_sync
    account.stubs(:has_ip_restriction?).returns(true)
  end

  as_an_admin do
    should_be_forbidden :create
  end

  as_a_subsystem_user(user: 'account_service', account: :multiproduct) do
    describe 'POST #create' do
      retryable_errors = [
        ActiveRecord::ConnectionNotEstablished,
        ActiveRecord::ConnectionTimeoutError
      ]

      describe 'when synchronous_chat_provisioning flag is turned on' do
        let(:has_synchronous_chat_provisioning) { true }

        describe 'when handling account notifications' do
          let(:notification_type) { 'account_updated' }

          it 'clears the cached account, reloads it and returns a 200' do
            Zendesk::Accounts::Client.any_instance.expects(:expire_account_client_account_cache).once
            account.expects(:clear_kasket_cache!)
            account.expects(:reload).at_least_once
            post :create, params: params
            assert_response :ok
          end
        end

        describe 'when handling chat notification' do
          let(:notification_type) { 'product_updated' }
          let(:updated_product) { 'chat' }

          it 'returns a 200' do
            post :create, params: params
            assert_response :ok
          end
        end

        describe 'when handling SPP product update' do
          let(:expected_billing_id) { '1c92c0f95f368207025s3bba7fbc6ebe' }
          let(:notification_type) { 'product_updated' }
          let(:updated_product) { Zendesk::Accounts::Client::SUPPORT_PRODUCT }
          let(:unchanged_trial_expiry) { account.subscription.trial_expires_on }
          let(:expected_trial_expiry) { unchanged_trial_expiry + 1 }
          let(:unchanged_plan_type) { account.subscription.plan_type }
          let(:expected_plan_type) { unchanged_plan_type + 1 }
          let(:unchanged_max_agents) { account.subscription.max_agents }
          let(:expected_max_agents) { unchanged_max_agents + 1 }
          let(:support_product) do
            make_product(product_name: Zendesk::Accounts::Client::SUPPORT_PRODUCT,
                         trial_expires_at: expected_trial_expiry,
                         plan_settings: { "plan_type" => expected_plan_type,
                                          "max_agents" => expected_max_agents })
          end
          let(:products_response) do
            { products: [support_product] }.to_json
          end
          before do
            stub_request(
              :get,
              %r{#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}$}
            ).with(
              headers: {
                'Host' => "#{subdomain}.#{Zendesk::Configuration['host']}"
              }
            ).to_return(
              status: 200,
              body: {
                account: {
                  id: account.id,
                  billing_id: expected_billing_id
                }
              }.to_json,
              headers: {"Content-Type" => "application/json"}
            )
            Account.any_instance.stubs(spp?: true)
          end

          describe 'updating trial accounts' do
            before do
              account.subscription_feature_addons.create!(
                name:               'multibrand',
                zuora_rate_plan_id: '123456'
              )
              Account.any_instance.stubs(is_trial?: true)
              post :create, params: params
            end

            it 'returns a 200' do
              assert_response :ok
            end

            it 'updates the plan_type' do
              assert_equal expected_plan_type, account.subscription.plan_type
            end

            it 'updates the trial expiry' do
              assert_equal expected_trial_expiry, account.subscription.trial_expires_on
            end

            it 'updates the max_agents cap' do
              assert_equal expected_max_agents, account.subscription.max_agents
            end

            it 'sets the rate_plan_id to "1"' do
              addon = account.subscription_feature_addons.by_name(:multibrand).first
              assert_equal "1", addon.zuora_rate_plan_id
            end

            it 'nullifies the boost_expires_at' do
              addon = account.subscription_feature_addons.by_name(:multibrand).first
              assert_nil addon.boost_expires_at
            end
          end

          describe 'updating plan_type and max_agents' do
            before do
              Zendesk::Features::SubscriptionFeatureService.any_instance.expects(:execute!).once
              post :create, params: params
            end

            it 'returns a 200' do
              assert_response :ok
            end

            it 'updates the plan_type' do
              assert_equal expected_plan_type, account.subscription.plan_type
            end

            it 'updates the max_agents cap' do
              assert_equal expected_max_agents, account.subscription.max_agents
            end
          end

          describe 'updating billing_id' do
            before do
              account.subscription_feature_addons.create!(
                name:               'multibrand',
                zuora_rate_plan_id: '123456'
              )
              account.subscription_feature_addons.create!(
                name:               'light_agents',
                boost_expires_at:   1.day.from_now
              )
              account.save!
              post :create, params: params
            end

            it 'returns a 200' do
              assert_response :ok
            end

            it 'updates the zuora_rate_plan_id for addons without boost_expires_at' do
              addon = account.subscription_feature_addons.by_name(:multibrand).first
              assert_equal expected_billing_id, addon.zuora_rate_plan_id
            end

            it 'does not update the zuora_rate_plan_id for addons with boost_expires_at' do
              addon = account.subscription_feature_addons.by_name(:light_agents).first
              assert_nil addon.zuora_rate_plan_id
            end
          end

          describe 'and an unexpected error is raised' do
            let(:support_product) do
              make_product(product_name: Zendesk::Accounts::Client::SUPPORT_PRODUCT,
                           plan_settings: { "plan_type" => 100000, # something nonsensical
                                            "max_agents" => expected_max_agents })
            end

            it 'logs the error' do
              Rails.logger.expects(:error).with do |m|
                m.starts_with?("Exception while processing support product (SPP) update")
              end
              post :create, params: params
            end

            it 'posts to statsd' do
              Zendesk::StatsD::Client.any_instance.expects(:event).once
              post :create, params: params
            end

            it 'returns ok' do
              post :create, params: params
              assert_response(200)
            end
          end
        end
      end

      describe 'when synchronous_chat_provisioning flag is turned off' do
        let(:has_synchronous_chat_provisioning) { false }

        describe 'when handling route notifications' do
          let(:notification_type) { 'route_updated' }

          it 'ignores the notiifcation and returns a 200' do
            post :create, params: params
            assert_response :ok
          end
        end
      end

      describe 'when handling product notifications' do
        let(:updated_product) { Zendesk::Accounts::Client::CHAT_PRODUCT }
        let(:notification_type) { 'product_updated' }

        describe 'answerbot product changes' do
          before do
            Rails.cache.fetch(account.scoped_cache_key(:answer_bot)) { 1 }
            Rails.cache.fetch(account.scoped_cache_key(:answer_bot_guide_plan)) { 2 }

            assert_equal 1, Rails.cache.read(account.scoped_cache_key(:answer_bot))
            assert_equal 2, Rails.cache.read(account.scoped_cache_key(:answer_bot_guide_plan))
          end

          describe 'and the product updated is answer_bot' do
            let(:updated_product) { 'answer_bot' }
            let(:products_response) do
              { products: [make_product(product_name: Zendesk::Accounts::Client::ANSWER_BOT_PRODUCT)] }.to_json
            end

            it 'should clear cache' do
              post :create, params: params
              assert_response :ok

              assert_nil Rails.cache.read(account.scoped_cache_key(:answer_bot))
              assert_nil Rails.cache.read(account.scoped_cache_key(:answer_bot_guide_plan))
            end
          end

          describe 'and the product updated is guide' do
            let(:updated_product) { 'guide' }
            let(:products_response) do
              { products: [make_product(product_name: Zendesk::Accounts::Client::GUIDE_PRODUCT)] }.to_json
            end

            it 'clears the account scoped cache for answer_bot and guide plan' do
              post :create, params: params
              assert_response :ok

              assert_nil Rails.cache.read(account.scoped_cache_key(:answer_bot))
              assert_nil Rails.cache.read(account.scoped_cache_key(:answer_bot_guide_plan))
            end
          end
        end

        describe 'answer_bot product_settings changes' do
          # NOTE: The notification_type "product_settings_updated" is no longer
          # sent out; in its stead the "product_updated" notification_type is
          # sent.
          let(:product_settings_updated) do
            {
              product_id: 2,
              account_id: 1,
              notification_type: "product_updated"
            }
          end

          before do
            Rails.cache.fetch(account.scoped_cache_key(:answer_bot)) { 1 }
            Rails.cache.fetch(account.scoped_cache_key(:answer_bot_guide_plan)) { 2 }

            assert_equal 1, Rails.cache.read(account.scoped_cache_key(:answer_bot))
            assert_equal 2, Rails.cache.read(account.scoped_cache_key(:answer_bot_guide_plan))
          end

          describe 'when automatic_answers feature is on' do
            before do
              account.stubs(:has_automatic_answers_enabled?).returns(true)
            end

            describe 'when product settings is updated' do
              it 'clears the acocount scoped cache for answer_bot and guide plan' do
                post :create, params: product_settings_updated
                assert_response :ok

                assert_nil Rails.cache.read(account.scoped_cache_key(:answer_bot))
                assert_nil Rails.cache.read(account.scoped_cache_key(:answer_bot_guide_plan))
              end
            end
          end

          describe 'when automatic_answers feature is off' do
            before do
              account.stubs(:has_automatic_answers_enabled?).returns(false)
            end

            describe 'when product settings is updated' do
              it 'does not clear account scoped cache for answer_bot and guide plan' do
                post :create, params: product_settings_updated
                assert_response :ok

                assert_equal 1, Rails.cache.read(account.scoped_cache_key(:answer_bot))
                assert_equal 2, Rails.cache.read(account.scoped_cache_key(:answer_bot_guide_plan))
              end
            end
          end
        end

        describe 'and Chat product was not updated' do
          let(:updated_product) { 'not-chat' }
          let(:notification_type) { 'product_updated' }

          it 'does not call account.settings.ip_restriction_enabled?' do
            account.settings.expects(:ip_restriction_enabled?).never

            post :create, params: params
            assert_response :ok
          end

          it 'ignores the notification and returns a 200' do
            post :create, params: params
            assert_response :ok
          end
        end

        describe 'and the product updated is chat' do
          let(:lite_plan_type) { 1 }
          let(:max_agents) { 100 }
          let(:trial_expiry) { 7.days.from_now }
          let(:chat_product_phase) { 3 }
          let(:updated_product) { Zendesk::Accounts::Client::CHAT_PRODUCT }
          let(:notification_type) { 'product_updated' }
          let(:chat_product) do
            plan_settings = default_plan_settings.merge('phase' => chat_product_phase, 'max_agents' => max_agents)
            make_product(
              product_name: Zendesk::Accounts::Client::CHAT_PRODUCT,
              plan_settings: plan_settings,
              trial_expires_at: trial_expiry
            )
          end

          let(:products_response) { { products: [chat_product] }.to_json }

          describe 'when ip restriction feature is not enabled' do
            before { refute account.settings.ip_restriction_enabled? }

            it 'does not call chat_phase_four?' do
              @controller.expects(:chat_phase_four?).never

              post :create, params: params
              assert_response :ok
            end
          end

          describe 'when ip restriction feature is enabled' do
            before do
              account.settings.ip_restriction_enabled = true
              account.settings.save!

              assert account.settings.ip_restriction_enabled
            end

            describe 'chat product record is chat phase 4' do
              let(:chat_product_phase) { 4 }

              describe 'account has ip_restriction feature' do
                before { account.stubs(:has_ip_restriction?).returns(true) }

                it 'does not set ip restrictions enabled setting to false' do
                  post :create, params: params
                  assert_response :ok
                  assert account.settings.ip_restriction_enabled?
                end
              end

              describe 'account does not have ip_restriction feature' do
                before { account.stubs(:has_ip_restriction?).returns(false) }

                it 'sets ip restrictions enabled setting to false' do
                  post :create, params: params
                  assert_response :ok
                  refute account.settings.ip_restriction_enabled?
                end
              end

              describe 'when it fails due to a non retryable error' do
                before { account.stubs(:has_ip_restriction?).raises(StandardError.new) }

                it 'logs an error and returns ok' do
                  Rails.logger.expects(:error).at_least_once.with { |m| m.starts_with?("Encountered an unexpected error") }
                  post :create, params: params

                  assert_response :ok
                end
              end

              describe 'when it fails due to a retryable error' do
                retryable_errors.each do |e|
                  it 'logs the retryable error and returns a 500' do
                    account.stubs(:has_ip_restriction?).raises(e.new)
                    Rails.logger.expects(:error).with { |m| m.starts_with?("Encountered a retryable error") }

                    post :create, params: params
                    assert_response(500)
                  end
                end
              end
            end

            describe 'chat product record is chat phase 3' do
              let(:chat_product_phase) { 3 }

              describe 'account has ip_restriction feature' do
                before { account.stubs(:has_ip_restriction?).returns(true) }

                it 'does not set ip restrictions enabled setting to false' do
                  post :create, params: params
                  assert_response :ok
                  assert account.settings.ip_restriction_enabled?
                end
              end

              describe 'account does not have ip_restriction feature' do
                before { account.stubs(:has_ip_restriction?).returns(false) }

                it 'sets ip restrictions enabled setting to false' do
                  post :create, params: params
                  assert_response :ok
                  refute account.settings.ip_restriction_enabled?
                end
              end
            end
          end

          describe 'when the chat product record is not phase 3' do
            let(:chat_product_phase) { 0 }

            it 'does nothing, and returns 200' do
              post :create, params: params
              assert_response :ok
              assert_nil account.reload.zopim_subscription
            end
          end

          describe 'when the account is a phase I chat customer' do
            before do
              setup_zopim_stubs
              ZopimIntegration.where(
                account_id:        account.id,
                external_zopim_id: account.id,
                phase: 1
              ).first_or_create!(
                zopim_key: account.id.to_s
              )
            end

            it 'does nothing, and returns 200' do
              post :create, params: params
              assert_response :ok
              assert_nil account.reload.zopim_subscription
            end
          end

          describe 'when the account is a phase II chat customer' do
            before do
              setup_zopim_stubs
              setup_zopim_subscription(account)
            end

            it 'does nothing, and returns 200' do
              post :create, params: params
              assert_response :ok
              assert_equal 2, account.reload.zopim_integration.phase
            end
          end

          describe 'when the account is a phase 3 chat customer' do
            describe 'for an account with no existing zopim_subscription' do
              before { delete_zopim_integration(account) }

              it 'creates a new zopim_subscription using the product record from account service' do
                post :create, params: params

                assert account.reload.zopim_subscription.present?, "ZopimSubscription was not created."
                assert_equal ZBC::Zopim::SubscriptionStatus::Active,
                  account.reload.zopim_subscription.status
                assert_equal 'lite',
                  account.zopim_subscription.plan_type
                assert_equal chat_product['plan_settings']['max_agents'],
                  account.zopim_subscription.max_agents
                assert_equal 7, account.zopim_subscription.trial_days
              end

              # There should already be retries built in to the default account client
              it 'logs an error and returns ok when the chat product record lookup fails even after many retries' do
                stub_request(
                  :get,
                  %r{#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/\d+/products}
                ).to_return(status: 500).
                  to_return(status: 500).
                  to_return(status: 500).
                  to_return(status: 500).
                  to_return(status: 500)

                post :create, params: params
                assert_response(200)
              end

              describe 'when the provisioning fails due to an unexpected error' do
                it 'logs the unexpected error and returns ok' do
                  ZBC::Zopim::Subscription.any_instance.stubs(:save!).raises(ArgumentError.new)
                  Rails.logger.expects(:error).with do |m|
                    m.starts_with?("Exception while processing chat product notification.")
                  end

                  post :create, params: params
                  assert_response(200)
                end
              end

              describe 'when the provisioning fails due to a retryable error' do
                retryable_errors.each do |e|
                  it 'logs the retryable error and returns a 500' do
                    ZBC::Zopim::Subscription.any_instance.stubs(:save!).raises(e.new)
                    Rails.logger.expects(:error).with do |m|
                      m.starts_with?("Encountered a retryable error")
                    end

                    post :create, params: params
                    assert_response(500)
                  end
                end
              end
            end

            describe 'when the chat record from the account service is inactive' do
              let(:products_response) do
                { products: [cancelled_chat_product] }.to_json
              end

              it 'does nothing and return a 200' do
                post :create, params: params
                assert_response :ok
                assert_nil account.zopim_subscription, "ZopimSubscription should not exist."
              end
            end
          end

          describe 'for an account with an existing zopim_subscription' do
            before do
              create_phase_3_zopim_subscription(account)
            end

            describe 'when the product max agents is updated' do
              let(:max_agents) { 88 }

              it 'updates the zopim_subscription max_agents field to match' do
                post :create, params: params
                assert account.reload.zopim_subscription.present?, "ZopimSubscription should be present."
                assert_equal ZBC::Zopim::SubscriptionStatus::Active,
                  account.zopim_subscription.status
                assert_equal 'lite',
                  account.zopim_subscription.plan_type
                assert_equal chat_product['plan_settings']['max_agents'],
                  account.zopim_subscription.reload.max_agents
                assert_equal 7, account.zopim_subscription.trial_days
              end
            end

            describe 'when the product status is updated to cancelled' do
              let(:products_response) do
                { products: [cancelled_chat_product] }.to_json
              end

              it 'destroys the zopim_subscription record' do
                post :create, params: params

                assert_nil account.reload.zopim_subscription, "ZopimSubscription should be deleted."
              end
            end

            describe 'when expires_at is nil' do
              let(:trial_expiry) { nil }

              it 'sets the zopim_subscription trial_days to default' do
                post :create, params: params
                assert_equal ZBC::Zopim::Subscription::TrialManagement::DEFAULT_TRIAL_LENGTH_IN_DAYS,
                  account.reload.zopim_subscription.trial_days
              end
            end

            describe 'when expires_at is updated' do
              let(:trial_expiry) { 11.days.from_now }

              it 'sets the zopim_subscription trial_days to match' do
                post :create, params: params
                assert_equal 11, account.reload.zopim_subscription.trial_days
              end

              describe 'and the trial is being reactivated' do
                let(:chat_product) do
                  plan_settings = default_plan_settings.merge('phase' => chat_product_phase, 'max_agents' => max_agents)
                  make_product(
                    product_name: Zendesk::Accounts::Client::CHAT_PRODUCT,
                    plan_settings: plan_settings,
                    trial_expires_at: trial_expiry,
                    created_at: Time.now - 10.days
                  )
                end

                it 'calculates the remaining trial days based on the current date and not when the trial was first created' do
                  post :create, params: params
                  assert_equal 11, account.reload.zopim_subscription.trial_days
                end
              end
            end

            describe 'when the update fails' do
              describe 'due to an unexpected error' do
                it 'logs the error and returns ok' do
                  ZBC::Zopim::Subscription.any_instance.stubs(:save!).raises(ArgumentError.new)
                  Rails.logger.expects(:error).with do |m|
                    m.starts_with?("Exception while processing chat product notification.")
                  end

                  post :create, params: params
                  assert_response(200)
                end
              end

              describe 'and the error is a retryable error' do
                retryable_errors.each do |e|
                  it 'logs the error and returns a 500' do
                    ZBC::Zopim::Subscription.any_instance.stubs(:save!).raises(e.new)
                    Rails.logger.expects(:error).with do |m|
                      m.starts_with?("Encountered a retryable error")
                    end

                    post :create, params: params
                    assert_response(500)
                  end
                end
              end
            end

            describe 'without an existing zopim_integration' do
              describe 'when the product max agents is updated' do
                let(:max_agents) { 88 }

                it 'notifies statsd, does not log as error, and returns ok' do
                  account.stubs(:zopim_integration).returns(nil)

                  Rails.application.config.statsd.client.
                    expects(:event).
                    once

                  Rails.logger.expects(:error).never

                  post :create, params: params
                  assert_response(200)
                end
              end
            end
          end
        end

        describe 'and a product is created' do
          let(:notification_type) { 'product_created' }
          describe_with_arturo_enabled :ocp_support_shell_account_creation do
            describe_with_arturo_enabled :accsrv_product_created_notification do
              let(:state) { :not_started }
              let(:created_product) { 'support' }
              let(:support_product) do
                make_product(
                  product_name: Zendesk::Accounts::Client::SUPPORT_PRODUCT,
                  state: state
                )
              end

              describe 'and the created product is not support' do
                let(:chat_product) { make_product(product_name: Zendesk::Accounts::Client::CHAT_PRODUCT) }
                let(:created_product) { 'chat' }
                let(:products_response) do
                  { products: [chat_product] }.to_json
                end

                it 'actvates support trial' do
                  account.expects(:start_support_trial).never
                  post :create, params: product_created_params
                end

                describe 'and the created product is guide' do
                  let(:guide_product) { make_product(product_name: Zendesk::Accounts::Client::GUIDE_PRODUCT) }
                  let(:created_product) { 'guide' }
                  let(:products_response) do
                    { products: [guide_product] }.to_json
                  end

                  it 'enqueues an account sync job' do
                    Omnichannel::GuideAccountSyncJob.expects(:enqueue).with(account_id: account.id)
                    post :create, params: product_created_params
                    assert_response :ok
                  end
                end
              end

              describe 'and the created product is support' do
                describe 'account only has support product' do
                  let(:products_response) do
                    { products: [support_product] }.to_json
                  end

                  it 'actvates support trial' do
                    account.expects(:start_support_trial)
                    post :create, params: product_created_params
                  end
                end
                describe 'account is a sandbox' do
                  let(:products_response) do
                    { products: [support_product] }.to_json
                  end
                  it 'does not activate support trial' do
                    account.stubs(:is_sandbox?).returns(true)
                    account.expects(:start_support_trial).never
                    post :create, params: product_created_params
                  end
                end
                describe 'account only has more than just support product' do
                  let(:chat_product) { make_product(product_name: Zendesk::Accounts::Client::CHAT_PRODUCT) }
                  let(:products_response) do
                    { products: [support_product, chat_product] }.to_json
                  end

                  it 'does activate the support trial' do
                    account.expects(:start_support_trial)
                    post :create, params: product_created_params
                  end
                end
              end
            end
          end
        end

        describe 'and the Support product was not updated' do
          let(:updated_product) { 'not-support' }

          it 'does not call arturo has_downgrade_support_trial_specific_support_features?' do
            account.expects(:has_downgrade_support_trial_specific_support_features?).never
            post :create, params: params
          end
        end

        describe 'and the product updated is support' do
          let(:updated_product) { 'support' }
          before { account.stubs(:deactivate_agent_workspace) }

          describe 'support trial needs to be activated' do
            describe 'with z3n prefix' do
              let(:subdomain) { 'z3ntest' }

              before do
                account.stubs(:subdomain).returns('z3ntest')
              end

              describe_with_arturo_enabled :ocp_support_shell_account_creation do
                let(:state) { :not_started }
                let(:support_product) do
                  make_product(
                    product_name: Zendesk::Accounts::Client::SUPPORT_PRODUCT,
                    state: state
                  )
                end

                describe 'account only has support product' do
                  let(:products_response) do
                    { products: [support_product] }.to_json
                  end

                  describe 'support not started' do
                    let(:state) { :not_started }

                    it 'actvates support trial' do
                      account.expects(:start_support_trial)
                      post :create, params: params
                    end
                  end

                  describe 'account is a sandbox' do
                    let(:state) { :not_started }
                    it 'does not activate support trial' do
                      account.stubs(:is_sandbox?).returns(true)
                      account.expects(:start_support_trial).never
                      post :create, params: params
                    end
                  end

                  describe 'support already started' do
                    let(:state) { :trial }

                    it 'does not actvate support trial' do
                      account.expects(:start_support_trial).never
                      post :create, params: params
                    end
                  end
                end

                describe 'account only has more than just support product' do
                  let(:chat_product) { make_product(product_name: Zendesk::Accounts::Client::CHAT_PRODUCT) }
                  let(:products_response) do
                    { products: [support_product, chat_product] }.to_json
                  end

                  it 'does not actvate support trial' do
                    account.expects(:start_support_trial).never
                    post :create, params: params
                  end
                end
              end

              describe_with_arturo_disabled :ocp_support_shell_account_creation do
                it 'does not actvate support trial' do
                  account.expects(:start_support_trial).never
                  post :create, params: params
                end
              end
            end
          end

          describe 'with downgrade_support_trial_specific_support_features Arturo ON' do
            describe 'with the support product expired' do
              before { account.stubs(:support_expired?).returns(true) }

              # trial_expiry_jobs only expires trials on the day *after* their trial expires
              describe 'when trial expired today' do
                before do
                  account.subscription.update_attribute(:trial_expires_on, Date.today)
                  assert Date.today, account.subscription.trial_expires_on
                end

                it 'does not call #account.downgrade_support_trial_specific_support_features!' do
                  account.expects(:downgrade_support_trial_specific_support_features!).never
                  post :create, params: params
                  assert_response :ok
                end
              end

              describe 'when trial expired 1 day ago' do
                before do
                  account.subscription.update_attribute(:trial_expires_on, 1.day.ago)
                  assert 1.day.ago, account.subscription.trial_expires_on
                end

                it 'calls #account.downgrade_support_trial_specific_support_features!' do
                  account.expects(:downgrade_support_trial_specific_support_features!).once
                  post :create, params: params
                  assert_response :ok
                end
              end
            end

            describe 'with an unexpired support product' do
              before { account.stubs(:support_expired?).returns(false) }

              it 'does not call #account.downgrade_support_trial_specific_support_features!' do
                account.expects(:downgrade_support_trial_specific_support_features!).never
                post :create, params: params
                assert_response :ok
              end
            end
          end

          describe 'with downgrade_support_trial_specific_support_features Arturo OFF' do
            before { Arturo.disable_feature!(:downgrade_support_trial_specific_support_features) }

            it 'does not call #trial_expired_within_a_day?' do
              account.expects(:downgrade_support_trial_specific_support_features!).never
              @controller.expects(:trial_expired_within_a_day?).never
              post :create, params: params
              assert_response :ok
            end
          end

          it 'expires the account service client product cache' do
            Zendesk::Accounts::Client.any_instance.expects(:expire_product_cache).with(updated_product).once
            post :create, params: params
            assert_response :ok
          end

          it 'deactivates agent workspace when support trial expires' do
            account.stubs(:products).returns [stub(name: :support, active?: false)]
            account.expects(:deactivate_agent_workspace).with
            post :create, params: params
            assert_response :ok
          end
        end

        describe 'and the product updated is explore' do
          let(:updated_product) { 'explore' }

          it 'enqueues an account sync job' do
            Omnichannel::ExploreAccountSyncJob.expects(:enqueue).with(account_id: account.id)
            post :create, params: params
            assert_response :ok
          end
        end

        describe 'and the Explore product was not updated' do
          let(:updated_product) { 'not-explore' }

          it 'does not enqueue an explore sync job' do
            Omnichannel::ExploreAccountSyncJob.expects(:enqueue).never
            post :create, params: params
          end
        end

        describe 'and the product updated is guide' do
          let(:updated_product) { 'guide' }

          it 'enqueues an account sync job' do
            Omnichannel::GuideAccountSyncJob.expects(:enqueue).with(account_id: account.id)
            post :create, params: params
            assert_response :ok
          end
        end

        describe 'and the Guide product was not updated' do
          let(:updated_product) { 'not-guide' }

          it 'does not enqueue an guide sync job' do
            Omnichannel::GuideAccountSyncJob.expects(:enqueue).never
            post :create, params: params
          end
        end

        describe 'and the product updated is Talk' do
          let(:updated_product) { 'talk' }

          it 'enqueues a talk account sync job' do
            Omnichannel::TalkAccountSyncJob.expects(:enqueue).with(account_id: account.id)
            post :create, params: params
            assert_response :ok
          end
        end

        describe 'and the Talk product was not updated' do
          let(:updated_product) { 'not-talk' }

          it 'does not enqueue an talk sync job' do
            Omnichannel::TalkAccountSyncJob.expects(:enqueue).never
            post :create, params: params
          end
        end

        describe 'and the product updated is Light Agent Addon' do
          let(:updated_product) { 'light_agents' }

          it 'performs a light agent downgrade' do
            Zendesk::Accounts::LightAgentDowngrader.expects(:perform).with(account: account)
            post :create, params: params
            assert_response :ok
          end
        end
      end

      describe 'multi-product shell account updated' do
        let(:notification_type) { 'account_updated' }

        before do
          account.stubs(:new_shell_account?).returns(true)
          account.stubs(:multiproduct?).returns(true)
          account.trial_extras = TrialExtra.from_hash("client_ip" => "10.21.19.192", "product_trial_selected" => "acme_app")
          account.save!
          account.reload
        end

        describe 'when account is newly created shell account' do
          describe_with_arturo_enabled :orca_classic_replace_fraud_report_job_with_fraud_score_job do
            it 'executes the fraud score job' do
              FraudScoreJob.expects(:account_creation_enqueue).with(account, Fraud::SourceEvent::SHELL_CREATED, 10.seconds)
              post :create, params: params
              assert_response :ok
            end
          end
        end

        describe 'when account is not multi-product' do
          before { account.stubs(:multiproduct?).returns(false) }
          it 'does not enqueue fraud score job' do
            FraudScoreJob.expects(:account_creation_enqueue).never
            post :create, params: params
            assert_response :ok
          end
        end

        describe 'when account is not a newly created shell account' do
          before { account.stubs(:new_shell_account?).returns(false) }
          it 'does not enqueue fraud score job' do
            FraudScoreJob.expects(:account_creation_enqueue).never
            post :create, params: params
            assert_response :ok
          end
        end
      end

      describe 'when account owner is changed' do
        let(:notification_type) { 'owner_changed' }

        before do
          @new_admin_user = User.new(name: 'New Admin', email: 'newadmin@foo.com', account: account)
          Zendesk::SupportUsers::User.new(@new_admin_user).assign_role(Role::AGENT.id)
          stub_account_service_account(account, response_body: { account: { owner_id: @new_admin_user.id } })
        end

        it 'the owners role is updated' do
          post :create, params: params
          assert_response :ok
          assert_equal @new_admin_user.reload.roles, Role::ADMIN.id
        end
      end

      describe 'when account name is changed' do
        let(:notification_type) { 'account_name_changed' }
        let(:new_name) { 'new name' }
        let(:params) do
          {
            account_id: account.id,
            old_account_name: account.name,
            new_account_name: new_name,
            notification_type: notification_type
          }
        end

        before do
          stub_account_service_account(account, response_body: { account: {
            old_account_name: account.name,
            new_account_name: new_name
          }})
        end

        it 'the account name updated' do
          AccountNamePropagator.expects(:call).with(account, name: new_name, name_was: account.name)
          account.expects(:propagate_name_to_brand)

          post :create, params: params
          assert_response :ok
        end
      end

      describe '#set_cancellation_date' do
        let(:params) do
          {
            account_id: account.id,
            notification_type: notification_type
          }
        end
        before do
          Timecop.freeze
          account.stubs('is_active').returns(is_active)
          account.stubs('is_serviceable').returns(is_serviceable)
        end

        describe 'when the account is cancelled' do
          let(:notification_type) { 'account_cancelled' }
          let(:is_active) { false }
          let(:is_serviceable) { false }

          it 'should set account cancellation date to now' do
            post :create, params: params
            assert_response :ok
            assert_equal Time.now, account.subscription.churned_on
          end
        end

        describe 'when the account is reactivated' do
          let(:notification_type) { 'account_reactivated' }
          let(:is_active) { true }
          let(:is_serviceable) { true }

          it 'should set account cancellation date to nil' do
            post :create, params: params
            assert_response :ok
            assert_nil account.subscription.churned_on
          end
        end
      end
    end
  end
end
