require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::Internal::AddonBoostsController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper

  before do
    accept :json
  end

  with_options(controller: 'api/v2/internal/addon_boosts') do |request|
    request.should_route :get,    '/api/v2/internal/addon_boosts',     action: 'index'
    request.should_route :post,   '/api/v2/internal/addon_boosts',     action: 'create'
    request.should_route :put,    '/api/v2/internal/addon_boosts/123', action: 'update',  id: 123
    request.should_route :delete, '/api/v2/internal/addon_boosts/123', action: 'destroy', id: 123
  end

  describe "embeddable subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "embeddable") do
      should_be_forbidden :create, :destroy, :update

      should_be_authorized { get :index }
    end
  end

  as_a_subsystem_user(account: :minimum) do
    let(:account) { @controller.send(:current_account) }

    before do
      account.subscription.update_column(:plan_type, SubscriptionPlanType.LARGE)
    end

    let (:catalog) { Zendesk::Features::Catalogs::Catalog }

    before do
      account.subscription.update_column(:pricing_model_revision, 7)
    end

    it 'uses the patagonia catalog' do
      expected = catalog
      actual   = @controller.send(:catalog)

      assert_equal expected, actual
    end

    describe '#index' do
      describe 'and the account has boostable add-on available' do
        before { get :index }

        it('responds with success') { assert_response :success }

        it 'returns an array of boostable add-ons from the catalog' do
          expected = catalog.addons_for_plan(SubscriptionPlanType.LARGE).select(&:boostable?).count
          actual   = JSON.parse(response.body)['addon_boosts'].count

          assert_equal expected, actual
          assert actual > 1
        end
      end
    end

    describe '#create' do
      let(:available_feature_names) do
        catalog.addons_for_plan(account.subscription.plan_type).select(&:boostable?).map(&:name)
      end

      let(:first_addon) do
        {
          name: available_feature_names[0],
          boost_expires_at: 5.days.from_now
        }
      end

      let(:second_addon) do
        {
          name: available_feature_names[1],
          boost_expires_at: 8.days.from_now
        }
      end

      it('responds with created') do
        post :create, params: { subscription_feature_addon: first_addon }
        assert_response :created
      end

      it 'creates a boosted addon' do
        post :create, params: { subscription_feature_addon: first_addon }
        expected = first_addon[:name].to_s
        actual   = account.subscription_feature_addons.boosted.first.name

        assert_equal expected, actual
      end

      it 'uses Zendesk::SupportAccounts::Product to save changes' do
        prod_stub = stub(:product)
        Zendesk::SupportAccounts::Product.expects(:retrieve).with(account).returns(prod_stub)
        prod_stub.expects(:update_plan_settings!)

        post :create, params: { subscription_feature_addon: first_addon }
      end

      describe 'add-ons must be co-terminus' do
        before do
          post :create, params: { subscription_feature_addon: first_addon }
          post :create, params: { subscription_feature_addon: second_addon }
        end

        it('responds with created') { assert_response :created }

        it 'updates all boosted add-ons expiry date to the latest' do
          expected = account.subscription_feature_addons.boosted.where(name: second_addon[:name]).first.boost_expires_at
          actual   = account.subscription_feature_addons.boosted.where(name: first_addon[:name]).first.boost_expires_at

          assert_equal expected, actual
        end
      end

      describe 'boost_expires_at must be present' do
        let(:invalid_addon) { { name: 'invalid_addon' } }

        before do
          post :create, params: { subscription_feature_addon: first_addon }
          post :create, params: { subscription_feature_addon: invalid_addon }
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end

      describe 'boost_expires_at must be a future date' do
        let(:outdated_addon) { { name: 'outdated_addon', boost_expires_at: 8.days.ago } }

        before do
          post :create, params: { subscription_feature_addon: first_addon }
          post :create, params: { subscription_feature_addon: outdated_addon }
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end

    describe '#update' do
      let(:available_feature_names) do
        catalog.addons_for_plan(account.subscription.plan_type).select(&:boostable?).map(&:name)
      end

      let(:first_addon) do
        account.subscription_feature_addons.create!(
          name: available_feature_names[0],
          boost_expires_at: 5.days.from_now
        )
      end

      let(:second_addon) do
        account.subscription_feature_addons.create!(
          name: available_feature_names[1],
          boost_expires_at: 5.days.from_now
        )
      end

      let(:new_time) { 8.days.from_now }

      before do
        first_addon
        second_addon
        assert_equal 2, account.subscription_feature_addons.boosted.count(:all)
      end

      describe "success" do
        before { put :update, params: { id: first_addon.id, subscription_feature_addon: { boost_expires_at: new_time } } }

        it('responds with success') { assert_response :success }

        it 'updates a boosted addon' do
          assert_equal new_time.to_i, first_addon.reload.boost_expires_at.to_i
        end

        it 'updates all boosted add-ons expiry date to the latest' do
          expected = second_addon.reload.boost_expires_at
          actual   = first_addon.reload.boost_expires_at

          assert_equal expected, actual
        end
      end

      describe 'boost_expires_at must be present' do
        let(:invalid_addon) { {} }

        before { put :update, params: { id: first_addon.id, subscription_feature_addon: invalid_addon } }

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end

      describe 'boost_expires_at must be a future date' do
        let(:outdated_addon) { { boost_expires_at: 8.days.ago } }

        before { put :update, params: { id: first_addon.id, subscription_feature_addon: outdated_addon } }

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end

    describe '#destroy' do
      let(:addon) { { name: 'my_addon', boost_expires_at: 3.days.from_now } }

      before do
        @boosted_addon = account.subscription_feature_addons.create!(addon)
        assert_equal 1, account.subscription_feature_addons.boosted.count(:all)
      end

      describe 'with a valid ID for a boosted add-on' do
        it "responds with 204" do
          delete :destroy, params: { id: @boosted_addon.id }
          assert_response :no_content
        end

        it 'removes the selected add-ons' do
          delete :destroy, params: { id: @boosted_addon.id }
          assert_equal 0, account.subscription_feature_addons.boosted.count(:all)
        end
      end

      describe 'with an invalid ID' do
        before { delete :destroy, params: { id: 123456 } }

        it('responds with not_found') { assert_response :not_found }
      end

      describe 'with a valid ID but for a purchased add-on' do
        before do
          @boosted_addon.update_attributes!(zuora_rate_plan_id: 123456, boost_expires_at: nil)
          delete :destroy, params: { id: @boosted_addon.id }
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end
  end

  as_a_subsystem_user(account: :shell_account_without_support) do
    describe '#index' do
      before { get :index }

      it 'returns an empty array (success status)' do
        assert_response :success
        assert_empty JSON.parse(response.body)['addon_boosts']
      end
    end
  end
end
