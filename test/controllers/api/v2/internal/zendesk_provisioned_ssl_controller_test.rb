require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::Internal::ZendeskProvisionedSslController do
  extend Api::V2::TestHelper
  fixtures :accounts, :acme_certificate_job_statuses

  let(:json) { JSON.parse(response.body) }
  before { use_ssl }

  with_options(controller: "api/v2/internal/zendesk_provisioned_ssl") do |request|
    request.should_route :get,    "/api/v2/internal/zendesk_provisioned_ssl/jobs",        action: 'jobs'
    request.should_route :post,   "/api/v2/internal/zendesk_provisioned_ssl/enqueue",     action: 'enqueue'
    request.should_route :delete, "/api/v2/internal/zendesk_provisioned_ssl/rate_limits", action: 'rate_limits'
    request.should_route :delete, "/api/v2/internal/zendesk_provisioned_ssl/jobs",        action: 'kill_jobs'
    request.should_route :post,   "/api/v2/internal/zendesk_provisioned_ssl/reset",       action: 'reset'
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#jobs" do
      before do
        get :jobs, format: "json"
        assert_response :ok
      end

      should_use_presenter Api::V2::Internal::AcmeCertificateJobStatusPresenter

      it "returns AcmeCertificateJobStatuses for an account" do
        assert_equal 1, json.fetch('acme_certificate_job_statuses').size
        assert_equal "abc", json.fetch('acme_certificate_job_statuses').first.fetch('enqueued_id')
      end
    end

    describe "#enqueue" do
      before do
        assert_equal 1, @account.acme_certificate_job_statuses.count

        post :enqueue, format: "json"
        assert_response :ok
      end

      it "enqueues a job" do
        assert_equal 2, @account.acme_certificate_job_statuses.count
      end

      it "won't enqueue a job if there's already a pending job" do
        @account.acme_certificate_job_statuses.create!
        post :enqueue, format: "json"

        assert_response :unprocessable_entity
        assert_includes json.fetch('error'), 'A job is already enqueued for this account'
      end
    end

    describe "#rate_limits" do
      it "removes the acme rate limit" do
        Prop.expects(:reset).with(AcmeCertificateJob::PROP_KEY, @account.id)
        delete :rate_limits, format: "json"
        assert_response :ok
      end
    end

    describe "#kill_jobs" do
      before do
        @uncompleted_job = @account.acme_certificate_job_statuses.create!

        delete :kill_jobs, format: "json"
        assert_response :ok
        @uncompleted_job.reload
      end

      it "marks uncompleted jobs as failed" do
        assert_equal 'Killed', @uncompleted_job.message
        assert_equal 'failed', @uncompleted_job.status
        refute_nil @uncompleted_job.completed_at
      end
    end

    describe "#reset" do
      before do
        @account.acme_registrations.create!
        @account.acme_registrations.first.acme_authorizations.create!(account: @account, identifier: 'abc')

        Prop.expects(:reset).with(AcmeCertificateJob::PROP_KEY, @account.id)
      end

      describe "with an active autoprovisioned certificate" do
        before do
          c = @account.certificates.create
          c.state = 'active'
          c.autoprovisioned = true
          c.save!

          post :reset, format: "json"
          assert_response :ok
          @account.reload
        end

        it "does not delete acme records" do
          refute_empty @account.acme_registrations
          refute_empty @account.acme_authorizations
        end
      end

      describe "without a certificate" do
        before do
          post :reset, format: "json"
          assert_response :ok
          @account.reload
        end

        it "deletes acme records" do
          assert_empty @account.acme_registrations
          assert_empty @account.acme_authorizations
        end
      end
    end
  end
end
