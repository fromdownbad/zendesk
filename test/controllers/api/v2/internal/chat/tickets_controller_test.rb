require_relative "../../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::Chat::TicketsController do
  fixtures :accounts, :account_settings, :users, :rules, :sequences, :role_settings, :tokens, :attachments, :ticket_fields, :custom_field_options, :groups, :translation_locales

  def json_response
    JSON.parse(response.body)
  end

  def close_ticket!(ticket, user:)
    ticket.status_id = StatusType.CLOSED
    ticket.will_be_saved_by(user)
    ticket.save!
  end

  let(:create_params) do
    {
      chat_started: {
        visitor: {
          id: '123',
          name: 'john doe',
          email: 'john_doe@example.com',
          phone: '999',
          sunco_integration: {
            conversation_id: 'deadbeefcafe'
          }
        },
        start_url: 'https://example.org',
        chat_id: '2101.123456.abc',
        tags: ['bar']
      },
      format: :json
    }.freeze
  end
  let(:update_params) { create_params.merge(id: @ticket.nice_id.to_s) }
  let(:new_conversation_title) { I18n.t('txt.chat.new_conversation_title', visitor_name: 'john doe') }
  let(:ticket_from_response) { Ticket.find_by_nice_id!(json_response['ticket']['id']) }
  let(:event_from_response) { ticket_from_response.events.find_by!(id: json_response['chat_event']['id'], type: 'ChatStartedEvent') }
  let(:requester_from_response) { User.find(json_response['ticket']['requester_id']) }
  let(:group_from_response) { Group.find(json_response['ticket']['group_id']) }
  let(:via_type_id) { ViaType.fuzzy_find(json_response['ticket']['via']['channel']) }

  before do
    accept :json
    @request.account = accounts(:minimum)
    Account.any_instance.stubs(use_status_hold?: true)
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    describe 'for newly created ticket' do
      it 'creates a new ticket with chat event' do
        post :create, params: create_params
        assert_response :success

        assert ticket_from_response.comments.none?(&:is_public?), 'Comment is not private'
        assert_equal '2101.123456.abc', event_from_response.chat_id
        assert_equal 'deadbeefcafe', event_from_response.conversation_id
        assert_equal new_conversation_title, ticket_from_response.subject
        assert_equal "#{new_conversation_title}\n\nURL: https://example.org", ticket_from_response.description
        assert_equal 'john doe', requester_from_response.name
        assert_equal 'john_doe@example.com', requester_from_response.email
        assert_equal '999', requester_from_response.phone
        assert_equal 'bar zopim_chat', ticket_from_response.current_tags
        assert_equal ViaType.CHAT, via_type_id
        assert_equal ticket_from_response.requester_id, ticket_from_response.submitter_id
      end

      it 'creates new ticket with chat start event with locale and create new user with given visitor locale' do
        spanish = translation_locales(:spanish)
        Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
        Account.any_instance.stubs(:allowed_translation_locales).returns([spanish, ENGLISH_BY_ZENDESK])
        post :create, params: create_params.deep_merge(chat_started: {
            visitor: {
                locale: spanish.locale
            }
        })

        assert_response :success
        assert_equal spanish.locale, requester_from_response.locale
        assert_equal ticket_from_response.requester_id, ticket_from_response.submitter_id
      end

      it 'creates new ticket with chat start event with locale and existing user locale not updated' do
        requester = users(:minimum_end_user)
        spanish = translation_locales(:spanish)
        Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
        Account.any_instance.stubs(:allowed_translation_locales).returns([spanish, ENGLISH_BY_ZENDESK])

        post :create, params: create_params.deep_merge(chat_started: {
            requester_id: requester.id,
            visitor: {
                locale: spanish.locale
            }
        })

        assert_response :success
        assert_equal requester, requester_from_response
        assert_equal ENGLISH_BY_ZENDESK.locale, requester_from_response.locale
      end

      it 'creates ticket with requester_id when given' do
        requester = users(:minimum_end_user)
        post :create, params: create_params.deep_merge(chat_started: { requester_id: requester.id })

        assert_response :success
        assert_equal requester, requester_from_response
        refute_equal 'john doe', requester_from_response.name
      end

      it 'creates ticket when merged requester_id given' do
        old_requester = users(:minimum_end_user)
        new_requester = users(:minimum_end_user2)
        Users::Merge.new(winner: new_requester, loser: old_requester).merge!

        post :create, params: create_params.deep_merge(chat_started: { requester_id: old_requester.id })

        assert_response :success
        assert_equal new_requester, requester_from_response
      end

      it 'creates ticket with group_id when given' do
        group = groups(:minimum_group)
        post :create, params: create_params.deep_merge(chat_started: { group_id: group.id })

        assert_response :success
        assert_equal group, group_from_response
      end

      describe 'Channel handling' do
        it 'creates new ticket with chat via channel by default' do
          post :create, params: create_params

          assert_response :success
          assert_equal ViaType.CHAT, via_type_id
          assert_equal new_conversation_title, ticket_from_response.subject
          assert_equal 'bar zopim_chat', ticket_from_response.current_tags
        end

        it 'creates new ticket with given via channel as CHAT' do
          post :create, params: create_params.deep_merge(chat_started: { via: { channel: 'CHAT' } })

          assert_response :success
          assert_equal ViaType.CHAT, via_type_id
          assert_equal new_conversation_title, ticket_from_response.subject
          assert_equal 'bar zopim_chat', ticket_from_response.current_tags
        end

        %w[LINE WECHAT WHATSAPP NATIVE_MESSAGING].each do |channel|
          it "creates new ticket with given via channel as #{channel}" do
            post :create, params: create_params.deep_merge(chat_started: { via: { channel: channel } })

            assert_response :success
            assert_equal ViaType.fuzzy_find(channel), via_type_id
            assert_equal new_conversation_title, ticket_from_response.subject
            assert_nil ticket_from_response.current_tags
          end
        end
      end

      describe 'with metadata' do
        def response_ticket_field_value(id)
          json_response['ticket']['fields'].find { |f| f['id'] == id }['value']
        end

        before { Arturo.enable_feature! :apply_sunco_ticket_data }

        it 'sets ticket field' do
          ticket_field = ticket_fields(:field_text_custom)
          post :create, params: create_params.deep_merge(chat_started: { sunco_meta: { "dataCapture.ticketField.#{ticket_field.id}" => 'bla' } })
          assert_equal 'bla', response_ticket_field_value(ticket_field.id)
        end

        it 'sets dropdown value by id' do
          ticket_field = ticket_fields(:field_tagger_custom)
          option = ticket_field.custom_field_options.first
          post :create, params: create_params.deep_merge(chat_started: { sunco_meta: { "dataCapture.ticketField.#{ticket_field.id}" => option.id.to_s } })
          assert_equal option.value, response_ticket_field_value(ticket_field.id)
        end

        it 'sets system field' do
          post :create, params: create_params.deep_merge(chat_started: {
            requester_id: users(:minimum_end_user).id,
            sunco_meta: { 'dataCapture.systemField.requester.name' => 'foobar' }
          })
          assert_predicate response, :created?
          assert_equal 'foobar', ticket_from_response.requester.name
        end

        it 'overwrites group_id' do
          groups(:support_group).activate!
          post :create, params: create_params.deep_merge(chat_started: { group_id: groups(:minimum_group).id, sunco_meta: { 'dataCapture.systemField.group_id' => groups(:support_group).id.to_s } })
          assert_equal groups(:support_group), ticket_from_response.group
        end

        it 'applies combined tags' do
          post :create, params: create_params.deep_merge(chat_started: { sunco_meta: { 'dataCapture.systemField.tags' => 'foo bar' } })
          assert_equal 'bar foo zopim_chat', ticket_from_response.current_tags
        end
      end

      describe 'with chat history' do
        let(:chat_history) do
          [{
             'type' => 'ChatMessage',
             'actor_id' => '123',
             'actor_type' => 'end-user',
             'actor_name' => 'jack',
             'timestamp' => '123000',
             'message' => 'testing'
           }]
        end

        it 'appends chat history when provided' do
          post :create, params: create_params.deep_merge(chat_started: { history: chat_history })
          assert_response :created
          transcripts = ticket_from_response.chat_transcripts
          assert_equal 1, transcripts.size
          assert_equal [chat_history], transcripts.map(&:chat_history)
          transcripts.each { |transcript| assert_predicate transcript, :transient_json? }
        end
      end
    end

    # Additional Sunshine's social types
    [
      {name: 'Mailgun', type: ViaType.MAILGUN, channel: 'MAILGUN'},
      {name: 'MessageBird SMS', type: ViaType.MESSAGEBIRD_SMS, channel: 'MESSAGEBIRD_SMS'},
      {name: 'SunCo Facebook Messenger', type: ViaType.TELEGRAM, channel: 'TELEGRAM'},
      {name: 'Integram', type: ViaType.SUNSHINE_CONVERSATIONS_FACEBOOK_MESSENGER, channel: 'SUNSHINE_CONVERSATIONS_FACEBOOK_MESSENGER'},
      {name: 'Twilio SMS', type: ViaType.TWILIO_SMS, channel: 'TWILIO_SMS'},
      {name: 'SunCo Twitter DM', type: ViaType.SUNSHINE_CONVERSATIONS_TWITTER_DM, channel: 'SUNSHINE_CONVERSATIONS_TWITTER_DM'},
      {name: 'Viber', type: ViaType.VIBER, channel: 'VIBER'},
      {name: 'Google RCS', type: ViaType.GOOGLE_RCS, channel: 'GOOGLE_RCS'},
      {name: 'Apple Business Chat', type: ViaType.APPLE_BUSINESS_CHAT, channel: 'APPLE_BUSINESS_CHAT'},
      {name: 'Google Business Messages', type: ViaType.GOOGLE_BUSINESS_MESSAGES, channel: 'GOOGLE_BUSINESS_MESSAGES'},
      {name: 'KakaoTalk', type: ViaType.KAKAOTALK, channel: 'KAKAOTALK'},
      {name: 'Instagram DM', type: ViaType.INSTAGRAM_DM, channel: 'INSTAGRAM_DM'},
      {name: 'SunCo API', type: ViaType.SUNSHINE_CONVERSATIONS_API, channel: 'SUNSHINE_CONVERSATIONS_API'}
    ].each do |setting|
      it "creates new ticket with given via channel as #{setting[:name]}" do
        post :create, params: create_params.deep_merge(chat_started: { via: { channel: setting[:channel] } })

        assert_response :success
        assert_equal setting[:type], via_type_id
        assert_equal new_conversation_title, ticket_from_response.subject
        assert_nil ticket_from_response.current_tags
      end
    end

    describe 'for a ticket with chat started event' do
      let(:chat_timestamp) { '456000' }
      let(:chat_history) do
        [{
           'type' => 'ChatMessage',
           'actor_id' => '123',
           'actor_type' => 'end-user',
           'actor_name' => 'jack',
           'timestamp' => chat_timestamp,
           'message' => 'testing'
         }]
      end

      before do
        post :create, params: create_params
        assert_response :success
        @ticket = ticket_from_response
        @chat_started_event_id = event_from_response.id
        @controller.instance_variable_set(:@chat_param, nil)
      end

      describe '#chats' do
        it 'returns no related chats' do
          get :chats, params: { id: @ticket.nice_id.to_s }
          assert_response :ok
          assert_equal [], json_response
        end
      end

      describe 'for a new chat ended event' do
        let(:params) do
          {
            id: @ticket.nice_id.to_s,
            chat_ended: {
              chat_id: '2101.123456.abc',
              visitor_id: '123',
              history: chat_history,
              webpath: [{ 'timestamp' => '1601514219000', 'url' => 'http://example.com/', 'referrer' => '', 'title' => 'title' }],
              tags: ['bar']
            },
            chat_event_id: @chat_started_event_id,
            format: :json
          }
        end
        let(:chat_started_event) { @ticket.events.find { |event| event.type == 'ChatStartedEvent' } }

        describe '#update' do
          after do
            transcripts = chat_started_event.chat_transcripts
            assert_equal [chat_history], transcripts.map(&:chat_history)
            assert_equal ['123'], transcripts.map(&:visitor_id)
            assert_equal ['2101.123456.abc'], transcripts.map(&:chat_id)
          end

          it 'returns chat ended event in response for operable ticket' do
            mock_notification = mock('radar notification')
            mock_notification.expects(:updated)
            ::RadarFactory.expects(:create_radar_notification).returns(mock_notification)

            put :update, params: params

            assert_response :success
            assert_equal 'ChatEndedEvent', json_response['chat_event']['type']
            assert_equal({ 'chat_id' => '2101.123456.abc', 'visitor_id' => '123', 'is_served' => true, 'tags' => ['bar'] }, json_response['chat_event']['value'])
          end

          it 'handles a closed ticket correctly' do
            close_ticket!(@ticket, user: @user)
            last_updated_at = @ticket.reload.updated_at

            mock_notification = mock('radar notification')
            mock_notification.expects(:updated)
            ::RadarFactory.expects(:create_radar_notification).returns(mock_notification)
            Timecop.travel(5.minutes) { put :update, params: params }

            assert_response :success
            assert_nil json_response['chat_event']
            assert @ticket.reload.updated_at > last_updated_at, 'ticket not updated'
          end

          it 'does not add any comments' do
            put :update, params: params
            assert_equal 1, @ticket.comments.count
          end

          it 'adds tags' do
            put :update, params: params
            assert_equal 'bar zopim_chat zopim_chat_ended', @ticket.reload.current_tags
          end

          describe 'for a messaging session' do
            let(:create_params) do
              {
                chat_started: {
                  visitor: {
                    id: '123',
                    name: 'john doe',
                    email: 'john_doe@example.com',
                    phone: '999'
                  },
                  chat_id: '2101.123456.abc',
                  via: {
                    channel: 'NATIVE_MESSAGING'
                  }
                }
              }.freeze
            end

            describe_with_arturo_enabled :public_chat_transcripts do
              it 'does not add any comments' do
                put :update, params: params.deep_merge(chat_ended: { public: true })
                assert_equal 1, @ticket.comments.count
              end
            end
          end

          describe 'with an existing tag' do
            before do
              @ticket.will_be_saved_by(@user)
              @ticket.additional_tags = ['foobar']
              @ticket.save!
            end

            it 'does not clobber existing tags' do
              put :update, params: params
              assert_equal 'bar foobar zopim_chat zopim_chat_ended', @ticket.reload.current_tags
            end
          end

          describe 'attachments' do
            let(:upload) do
              # force load association here, because after the attachments are added, they would have changed ownership
              tokens(:minimum_upload_token).tap { |u| u.attachments.to_a }
            end

            let(:chat_history) do
              [{
                 'type' => 'ChatMessage',
                 'actor_id' => '123',
                 'actor_type' => 'end-user',
                 'actor_name' => 'jack',
                 'timestamp' => (Time.zone.now.to_i * 1000).to_s,
                 'message' => 'testing'
               }, {
                 'type' => 'ChatFileAttachment',
                 'actor_id' => '123',
                 'actor_type' => 'agent',
                 'actor_name' => 'mr smith',
                 'filename' => 'sentinel.jpg',
                 'url' => 'https://example.org/matrix-assets/sentinel.jpg',
                 'mime_type' => 'image/jpeg',
                 'size' => '42433',
                 'timestamp' => (Time.zone.now.to_i * 1000).to_s,
                 'support_upload_token' => upload.value
               }]
            end

            describe_with_arturo_enabled :public_chat_transcripts do
              it 'does not add comment if transcript is private' do
                put :update, params: params
                assert_equal 1, @ticket.comments.count
              end

              it 'adds public comment if public param passed' do
                Timecop.freeze(Time.at(123)) { put :update, params: params.deep_merge(chat_ended: { public: true }) }

                assert_equal 2, @ticket.comments.count
                comment = @ticket.comments.order(:id).last
                expected = <<~CHAT.chomp
                  (12:02:03 AM) jack: testing
                  (12:02:03 AM) mr smith uploaded: sentinel.jpg
                  URL: https://example.org/matrix-assets/sentinel.jpg
                  Type: image/jpeg
                  Size: 42433
                CHAT
                assert_equal expected, comment.body
                assert_predicate comment, :is_public?
                assert_equal ViaType.CHAT_TRANSCRIPT, comment.via_id
                assert_equal 2, comment.attachments.length
                assert_equal ViaType.CHAT_TRANSCRIPT, comment.audit.via_id
              end
            end

            describe_with_arturo_enabled :chat_uploads do
              it 'does not add comment if transcript is public' do
                put :update, params: params.deep_merge(chat_ended: { public: true })

                assert_equal 1, @ticket.comments.count
              end

              it 'adds attachments to started event' do
                put :update, params: params.deep_merge(chat_ended: { public: true })
                assert_equal 2, chat_started_event.attachments.length
              end
            end
          end

          describe 'with rating in chat ended request body' do
            describe_with_arturo_setting_disabled :customer_satisfaction do
              it 'does not create CSAT' do
                put :update, params: params.deep_merge(chat_ended: { rating: { score: 'good', comment: 'fast & furious' } })

                assert_equal 0, @ticket.reload.satisfaction_ratings.count
              end
            end

            describe_with_arturo_setting_enabled :customer_satisfaction do
              it 'creates CSAT' do
                put :update, params: params.deep_merge(chat_ended: { rating: { score: 'good', comment: 'slow & steady' } })

                @ticket.reload

                assert_equal 1, @ticket.satisfaction_ratings.count
                assert_equal SatisfactionType.GOODWITHCOMMENT, @ticket.satisfaction_score
                assert_equal 'slow & steady', @ticket.satisfaction_comment
              end
            end
          end

          describe 'metrics collection' do
            let(:chat_timestamp) { @ticket.created_at.to_i * 1_000 }
            let(:chat_history) do
              [{
                 'type' => 'ChatMessage',
                 'actor_id' => '123',
                 'actor_type' => 'end-user',
                 'actor_name' => 'neo',
                 'timestamp' => chat_timestamp + 1_000,
                 'message' => 'i want my phone call'
               }, {
                 'type' => 'ChatMessage',
                 'actor_id' => '456',
                 'actor_type' => 'agent',
                 'actor_name' => 'smith',
                 'timestamp' => chat_timestamp + 61_000,
                 'message' => 'what good is a phone call if you are unable to speak?'
               }, {
                 'type' => 'ChatMessage',
                 'actor_id' => '456',
                 'actor_type' => 'agent',
                 'actor_name' => 'smith',
                 'timestamp' => chat_timestamp + 62_000,
                 'message' => 'you will cooperate with us, whether you like it or not'
               }]
            end
            let(:chat_metrics) do
              time_range = [Time.at(0, chat_timestamp + 1_000, :millisecond)..Time.at(0, chat_timestamp + 62_000, :millisecond)]
              @ticket.metric_events.where(time: time_range)
            end

            describe_with_arturo_disabled :chat_ticket_metrics do
              it 'does not record chat metrics' do
                put :update, params: params
                assert_nil @ticket.ticket_metric_set.first_reply_time_in_minutes
              end
            end

            describe_with_arturo_enabled :chat_ticket_metrics do
              before { Ticket.any_instance.stubs(:via_id).returns ViaType.NATIVE_MESSAGING }

              it 'records chat metrics' do
                put :update, params: params, as: :json

                assert_equal 1, @ticket.ticket_metric_set.first_reply_time_in_minutes
                assert_equal 1, @ticket.ticket_metric_set.replies
              end
            end
          end

          describe 'for chat with existing history' do
            let(:create_params) do
              {
                chat_started: {
                  visitor: {
                    id: '123',
                    name: 'john doe',
                    email: 'john_doe@example.com',
                    phone: '999'
                  },
                  start_url: 'https://example.org',
                  chat_id: '2101.123456.abc',
                  history: chat_history
                },
                format: :json
              }.freeze
            end

            describe 'for a closed ticket' do
              before { close_ticket!(@ticket, user: @user) }

              it 'sends radar notification' do
                mock_notification = mock('radar notification')
                mock_notification.expects(:updated)
                ::RadarFactory.expects(:create_radar_notification).returns(mock_notification)
                put :update, params: params
              end
            end

            it 'overwrites transient history when chat ends' do
              put :update, params: params
              assert_response :ok
              chat_started_event.chat_transcripts.each { |transcript| assert_predicate transcript, :json? }
            end

            it 'updates transient chat history' do
              put :update, params: {
                id: @ticket.nice_id.to_s,
                chat_history_updated: {
                  chat_id: '2101.123456.abc',
                  history: chat_history
                },
                chat_event_id: @chat_started_event_id,
                format: :json
              }
              assert_response :ok
              chat_started_event.chat_transcripts.each { |transcript| assert_predicate transcript, :transient_json? }
            end
          end
        end

        describe '#chats' do
          after do
            get :chats, params: { id: @ticket.nice_id }
            assert_response :success
            assert_equal [{ 'audit_id' => @chat_started_event_id, 'chat_id' => '2101.123456.abc', 'visitor_id' => '123' }], json_response
          end

          describe 'for an open ticket' do
            it 'returns related chats' do
              put :update, params: params
            end
          end

          describe 'for a deleted ticket' do
            it 'returns related chats' do
              put :update, params: params
              Zendesk::Scrub.scrub_ticket_and_associations(@ticket)
            end
          end
        end

        describe 'visitor update email' do
          it 'set requester to new user if email is not found' do
            previous_requester = @ticket.requester
            put :update, params: params.deep_merge(chat_ended: { visitor_email: 'hello@world.com' })

            @ticket.reload
            assert_equal 'hello@world.com', @ticket.requester.email
            assert_equal 'john doe', @ticket.requester.name
            refute_equal previous_requester, @ticket.requester
          end

          describe_with_arturo_setting_enabled :customer_satisfaction do
            it 'update enduser of csat' do
              previous_requester = @ticket.requester
              put :update, params: params.deep_merge(chat_ended: {
                visitor_email: 'foo@bar.com',
                rating: { score: 'good', comment: 'slow & steady' }
              })

              @ticket.reload
              assert_equal 'foo@bar.com', @ticket.satisfaction_rating.enduser.email
              refute_equal previous_requester, @ticket.satisfaction_rating.enduser
            end
          end

          it 'set requester to existing user if email is found' do
            previous_requester = @ticket.requester
            existing_user = users(:minimum_end_user)
            put :update, params: params.deep_merge(chat_ended: { visitor_email: existing_user.email })

            @ticket.reload
            assert_equal existing_user, @ticket.requester
            refute_equal previous_requester, @ticket.requester
          end
        end
      end

      describe 'for a new chat started event' do
        describe 'on a valid ticket' do
          before do
            put :update, params: update_params
            assert_response :success
            @chat_started_event_id_2 = json_response['chat_event']['id']
          end

          it 'creates another started event' do
            assert_equal 'ChatStartedEvent', json_response['chat_event']['type']
            assert_equal({ 'conversation_id' => 'deadbeefcafe', 'chat_id' => '2101.123456.abc', 'visitor_id' => '123', 'history' => [], 'webpath' => [], 'chat_start_url' => 'https://example.org', 'tags' => ['bar'] }, json_response['chat_event']['value'])
            # it doesn't care about duplicate chat ids
            assert_equal ['2101.123456.abc'] * 2, ChatStartedEvent.where(ticket: @ticket).map(&:chat_id)
          end

          it 'removes the chat ended tags' do
            assert_equal 'bar zopim_chat', @ticket.reload.current_tags
          end
        end

        it 'yields an error when a ticket is closed' do
          close_ticket!(@ticket, user: @user)

          put :update, params: update_params

          assert_response :unprocessable_entity
        end

        describe 'for ticket status updates' do
          it 'noop when no ticket status param (optional)' do
            put :update, params: update_params
            assert_response :ok
            assert_equal StatusType.NEW, @ticket.reload.status_id
          end

          [StatusType.OPEN, StatusType.HOLD, StatusType.PENDING].each do |status_id|
            ticket_status = StatusType.to_s(status_id)
            it "updates ticket status when param is #{ticket_status}" do
              put :update, params: update_params.merge(ticket_status: ticket_status)
              assert_response :ok
              assert_equal status_id, @ticket.reload.status_id
            end
          end

          [StatusType.NEW, StatusType.SOLVED, StatusType.CLOSED, StatusType.DELETED].each do |status_id|
            ticket_status = StatusType.to_s(status_id)
            it "does not update ticket status when param is #{ticket_status}" do
              # set ticket status to open
              @ticket.will_be_saved_by(@user)
              @ticket.status_id = StatusType.OPEN
              @ticket.save!

              put :update, params: update_params.merge(ticket_status: ticket_status)
              assert_response :ok
              assert_equal StatusType.OPEN, @ticket.reload.status_id
            end
          end
        end
      end

      describe 'with missing or invalid chat event id' do
        it 'yields error if event id is missing' do
          put :update, params: { id: @ticket.nice_id.to_s, chat_ended: {
                chat_id: '2101.123456.abc',
                visitor_id: '123',
                history: chat_history
              }, format: :json }

          assert_response :bad_request
        end

        it 'yields error in case of nonexistent event id' do
          put :update, params: { id: @ticket.nice_id.to_s, chat_ended: {
                chat_id: '2101.123456.abc',
                visitor_id: '123',
                history: chat_history
              }, chat_event_id: @ticket.events.find_by(parent_id: nil).id, format: :json }

          assert_response :bad_request
        end

        it 'yields error in case of chat id mismatch' do
          put :update, params: {id: @ticket.nice_id.to_s, chat_ended: {
              chat_id: '2101.123456.abcd',
              visitor_id: '123',
              history: chat_history
          }, chat_event_id: @chat_started_event_id, format: :json }

          assert_response :bad_request
        end
      end

      describe 'for a missed chat' do
        before do
          put :update, params: { id: @ticket.nice_id.to_s, chat_ended: {
                 chat_id: '2101.123456.abc',
                 visitor_id: '123',
                 history: chat_history,
                 is_served: false
               }, chat_event_id: @chat_started_event_id, format: :json }
          assert_response :ok
        end
        let(:updated_chat_title) { I18n.t('txt.chat.missed_conversation_title', visitor_name: 'john doe') }

        it 'sets is_missed correctly on the ended event' do
          assert_equal false, json_response['chat_event']['value']['is_served']
        end

        it 'updates the subject of the ticket' do
          assert_equal updated_chat_title, json_response['ticket']['subject']
          assert_equal updated_chat_title, @ticket.reload.subject
        end

        it 'sets missed ended tag' do
          assert_equal 'zopim_chat zopim_chat_ended zopim_chat_missed', @ticket.reload.current_tags
        end
      end
    end

    describe 'for a social messaging ticket' do
      before do
        post :create, params: create_params.deep_merge(chat_started: { via: { channel: 'WECHAT' } })
        assert_response :success
        @ticket = ticket_from_response
        @chat_started_event_id = event_from_response.id
        @controller.instance_variable_set(:@chat_param, nil)
      end

      describe 'when chat ends after agent serves' do
        before do
          put :update, params: { id: @ticket.nice_id.to_s, chat_ended: {
                chat_id: '2101.123456.abc',
                visitor_id: '123',
                history: [],
                is_served: true
              }, chat_event_id: @chat_started_event_id, format: :json }
          assert_response :success
        end
        let(:conversation_title) { I18n.t('txt.chat.new_conversation_title', visitor_name: 'john doe') }

        it 'does not add tags' do
          assert_nil @ticket.reload.current_tags
        end

        it 'does not update ticket subject' do
          assert_equal conversation_title, @ticket.reload.subject
        end

        it 'does not update ticket subject after agent has changed it' do
          @ticket.subject = 'Custom Subject'
          @ticket.will_be_saved_by(@user)
          @ticket.save!

          put :update, params: update_params
          assert_equal 'Custom Subject', @ticket.reload.subject

          put :update, params: { id: @ticket.nice_id.to_s, chat_ended: {
                chat_id: '2101.123456.xyz',
                visitor_id: '123'
              }, format: :json }
          assert_equal 'Custom Subject', @ticket.reload.subject
        end
      end

      describe 'when chat ends without being served' do
        before do
          put :update, params: { id: @ticket.nice_id.to_s, chat_ended: {
                chat_id: '2101.123456.abc',
                visitor_id: '123',
                history: [],
                is_served: false
              }, chat_event_id: @chat_started_event_id, format: :json }
          assert_response :success
        end
        let(:conversation_title) { I18n.t('txt.chat.new_conversation_title', visitor_name: 'john doe') }

        it 'does not update ticket subject' do
          assert_equal conversation_title, @ticket.reload.subject
        end
      end
    end

    describe 'triggers' do
      let(:rule_id) { rules(:trigger_notify_group_of_assignment).id }
      before do
        Account.any_instance.stubs(:has_rule_usage_stats?).returns(true)
      end

      describe_with_arturo_disabled :enable_triggers_for_new_chat_ticket do
        it 'does not run triggers' do
          post :create, params: create_params
          assert_response :success
          assert_empty TicketJoin.includes(:ticket).where(rule_id: rule_id, tickets: { nice_id: ticket_from_response.nice_id })
        end
      end

      describe_with_arturo_enabled :enable_triggers_for_new_chat_ticket do
        it 'runs triggers' do
          post :create, params: create_params
          assert_response :success
          refute_empty TicketJoin.includes(:ticket).where(rule_id: rule_id, tickets: { nice_id: ticket_from_response.nice_id })
        end
      end
    end

    describe 'with a social identity' do
      describe 'with facebook identity in request' do
        let(:fb_visitor) do
          {
            id: 'new_facebook_visitor_1',
            email: nil,
            social_identity: {
              type: 'facebook',
              value: 'fb123'
            }
          }
        end

        it 'creates new ticket & facebook user identity for new facebook visitor' do
          post :create, params: create_params.deep_merge(chat_started: { visitor: fb_visitor })
          assert_response :success

          identity = requester_from_response.identities.facebook.first
          assert_equal 'UserFacebookIdentity', identity.type
          assert_equal requester_from_response.id, identity.user_id
          assert_equal 'fb123', identity.value
          assert_predicate identity, :is_verified
        end

        describe 'with an existing facebook identity' do
          before { @account.user_identities.facebook.new(user: users(:minimum_end_user), value: 'fb123').save! }

          it 'does not create a new facebook identity' do
            post :create, params: create_params.deep_merge(chat_started: { visitor: fb_visitor })
            assert_response :success

            assert_predicate requester_from_response.identities.facebook, :empty?
          end
        end
      end

      describe 'twitter_identity' do
        let(:twitter_visitor) do
          {
            id: 'new_twitter_visitor_1',
            email: nil,
            social_identity: {
              type: 'twitter',
              value: 'twitter_screen_name'
            }
          }
        end

        it 'creates new ticket and twitter user identity for new twitter visitor' do
          stub_request(:post, 'https://api.twitter.com/oauth2/token').
            to_return(
              status: 200,
              body: { 'token_type' => 'bearer', 'access_token' => 'AAAA%2FAAA%3DAAAAAAAA' }.to_json,
              headers: { 'Content-Type' => 'application/json' }
            )
          stub_request(:get, 'https://api.twitter.com/1.1/users/show.json?screen_name=twitter_screen_name').
            to_return(status: 200,
              body: read_test_file('twitter_users_show_110542029_1.1.json'),
              headers: { 'Content-Type' => 'application/json' })

          post :create, params: create_params.deep_merge(chat_started: { visitor: twitter_visitor })

          assert_response :success
          identity = requester_from_response.identities.twitter.first
          assert_equal 'UserTwitterIdentity', identity.type
          assert_equal requester_from_response.id, identity.user_id
          assert_predicate identity, :is_verified
        end
      end
    end
  end
end
