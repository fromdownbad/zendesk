require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Voyager::VoyagerExportsController do
  fixtures :accounts, :users
  before do
    accept :json
    @request.account = accounts(:minimum)
  end

  as_a_subsystem_user(account: :minimum) do
    describe 'notify with non-existent user' do
      before { post :notify, params: { user_id: 99999 } }
      it 'responds with not_found' do
        must_respond_with :not_found
      end
    end

    describe '#notify' do
      it 'sends an email' do
        ActionMailer::Base.perform_deliveries = true
        assert_difference 'ActionMailer::Base.deliveries.size', +1 do
          post :notify, params: { user_id: users(:minimum_admin), url: 'http://www.test.com' }
        end
        must_respond_with :ok
      end
    end
  end
end
