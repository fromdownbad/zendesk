require_relative '../../../../../support/test_helper'
require_relative '../../../../../support/zopim_test_helper'

SingleCov.covered!

describe Api::V2::Internal::AppMarket::AccountsController do
  include ZopimTestHelper
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }

  before do
    @request.account = account
    accept :json
  end

  with_options(controller: 'api/v2/internal/app_market/accounts') do |request|
    request.should_route :get, '/api/v2/internal/app_market/account/billable_agents', action: 'billable_agents'
    request.should_route :get, '/api/v2/internal/app_market/account/app_recommendations_info', action: 'app_recommendations_info'
  end

  as_an_anonymous_user do
    should_be_unauthorized :billable_agents
    should_be_unauthorized :app_recommendations_info
  end

  as_an_end_user do
    should_be_forbidden :billable_agents
    should_be_forbidden :app_recommendations_info
  end

  as_an_agent do
    should_be_forbidden :billable_agents
    should_be_forbidden :app_recommendations_info
  end

  as_an_admin do
    should_be_forbidden :billable_agents
    should_be_forbidden :app_recommendations_info
  end

  as_an_owner do
    should_be_forbidden :billable_agents
    should_be_forbidden :app_recommendations_info
  end

  describe "#app_recommendations_info" do
    let(:account) { accounts(:minimum) }

    as_a_subsystem_user(account: :minimum) do
      before do
        apac_country = Country.where(region: "APAC").first
        account.address.update_attribute(:country_id, apac_country.id)
        account.settings.create(name: "onboarding_segments", value: "b2c")
        account.survey_response = Account::SurveyResponse.create(industry: "web_apps")

        get :app_recommendations_info
      end

      it 'responds with success' do
        assert_response :success
      end

      it 'contains a mapping for each onboarding segment' do
        segments = Users::OnboardingSupport::SEGMENTS
        known_segments = subject.class::ONBOARDING_SEGMENT_MAPPING.keys
        segments.each do |segment|
          assert_includes(known_segments, segment)
        end
      end

      it 'contains a mapping for each industry' do
        industries = Account::SurveyResponse::INDUSTRIES.values
        known_industries = subject.class::INDUSTRY_MAPPING.values.flatten
        industries.each do |industry|
          assert_includes(known_industries, industry)
        end
      end

      it 'responds with data about the account' do
        recommendation_hash = JSON.parse(response.body).fetch('app_recommendations_info')
        assert_equal(recommendation_hash,             "industry" => "SaaS",
                                                      "agent_count" => 4,
                                                      "target_audience" => "Customers",
                                                      "zendesk_plan" => "Medium",
                                                      "region" => "APAC")
      end
    end
  end

  describe "#billable_agents" do
    describe 'for an account with only support agents' do
      as_a_subsystem_user(account: :minimum) do
        before { get :billable_agents }

        let(:account) { accounts(:minimum) }

        it 'responds with success' do
          assert_response :success
        end

        it 'responds with a list of billable agents in support' do
          expected_support_agents = account.billable_agents
          support_agents = JSON.parse(response.body).fetch('support')

          assert_equal(support_agents.count, expected_support_agents.count)
          expected_support_agents.each do |agent|
            agent_hash = {
              'user_id' => agent.id,
              'role' => agent.roles,
              'custom_role_id' => agent.custom_role_id,
              'groups' => agent.groups.map(&:id)
            }
            assert_includes(support_agents, agent_hash)
          end
        end
      end
    end
  end
end
