require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::TicketsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
    @request.account = accounts(:minimum)
  end

  with_options(controller: "api/v2/internal/tickets") do |request|
    request.should_route :get,    "/api/v2/internal/tickets", action: 'index'
    request.should_route :get,    '/api/v2/internal/tickets/show_many_comments', action: 'show_many_comments'
  end

  as_an_admin do
    should_be_forbidden :index
    should_be_forbidden :show_many_comments
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#index" do
      before do
        @users = [users(:minimum_end_user), users(:minimum_author)]
        get :index, params: { emails: @users.map(&:email) }
      end
      should_use_presenter Api::V2::Tickets::TicketPresenter, status: :ok

      it "returns tickets requested by the specified users" do
        data = JSON.parse(@response.body)
        assert_equal Ticket.where(requester_id: @users.map(&:id)).count(:all), data["tickets"].size
      end
    end

    describe '#archive' do
      let(:ticket) { tickets(:minimum_1) }
      before do
        Ticket.stubs(:find_by_nice_id!).returns(ticket)
        ticket.expects(:archive!).once
        put :archive, params: { id: ticket.nice_id }
      end

      should_use_presenter Api::V2::Tickets::TicketPresenter, status: :ok
      it 'archives the ticket' do
        assert_response :success
        json = JSON.parse(@response.body)
        assert_equal ticket.nice_id, json['ticket']['id'].to_i
      end
    end

    describe '#permissions' do
      let(:ticket) { tickets(:minimum_1) }
      let(:user) { users(:minimum_end_user) }

      describe 'uses the correct presenter' do
        before do
          get :permissions, params: { id: ticket.nice_id, user_id: ticket.requester.id }
        end

        should_use_presenter Api::V2::Tickets::TicketPermissionsPresenter, status: :ok
      end

      it 'returns the permissions for the ticket and user specified' do
        get :permissions, params: { id: ticket.nice_id, user_id: ticket.requester.id }

        assert_response :success
        json = JSON.parse(@response.body)
        refute_nil json['permissions']
      end

      describe 'includes "can_view_ticket" attribute' do
        it 'is false if the user is unrelated to the ticket' do
          get :permissions, params: { id: ticket.nice_id, user_id: user.id }

          assert_response :success
          json = JSON.parse(@response.body)
          refute_nil json['permissions']
          assert_equal json['permissions']['can_view_ticket'], false
        end

        it 'is true if the user has access' do
          get :permissions, params: { id: ticket.nice_id, user_id: ticket.requester.id }

          assert_response :success
          json = JSON.parse(@response.body)
          refute_nil json['permissions']
          assert_equal json['permissions']['can_view_ticket'], true
        end
      end
    end

    describe '#show_many_comments' do
      before do
        @tickets = [tickets(:minimum_1), tickets(:minimum_2)]
      end

      describe 'default max_comments' do
        before do
          get :show_many_comments, params: { ticket_ids: @tickets.map(&:nice_id), id_waterline: 0, comment_id_waterline: 0 }
        end
        should_use_presenter Api::V2::Internal::CommentPresenter, status: :ok

        it 'returns comments for all submitted ticket ids' do
          assert_response :success
          json = JSON.parse(@response.body)
          assert_equal @tickets.map(&:comments).flatten.map(&:id).sort, json['comments'].map { |o| o['id'] }.sort
          json['comments'].each { |comment| refute_nil comment['ticket_id'] }
        end
      end

      describe 'explicit max_comments param' do
        it 'accepts a param max_comments to limit the amount of comments returned' do
          get :show_many_comments, params: { ticket_ids: @tickets.map(&:nice_id), id_waterline: 0, comment_id_waterline: 0, max_comments: 3 }

          assert_response :success
          json = JSON.parse(@response.body)

          assert_equal json['comments'].count, 3
          json['comments'].each { |comment| refute_nil comment['ticket_id'] }
        end

        describe 'when max_comments is present' do
          it 'paginates correctly if the first ticket has the same number of comments as max_comments' do
            max_comments = 3

            comment_counts = [max_comments, 2]

            ticket_fixtures = comment_counts.map { |count| ticket_with_comments(count) }
            ticket_ids = ticket_fixtures.map(&:nice_id)

            # retrieve first page of comments, limited to 3
            get :show_many_comments, params: { ticket_ids: ticket_ids, id_waterline: 0, comment_id_waterline: 0, max_comments: max_comments }

            assert_response :success
            json1 = JSON.parse(@response.body)

            assert_equal json1['comments'].count, max_comments
            json1['comments'].each { |comment| refute_nil comment['ticket_id'] }

            # retrieve the rest of the paginated comment list
            get :show_many_comments, params: { ticket_ids: ticket_ids, id_waterline: json1['id_waterline'], comment_id_waterline: json1['comment_id_waterline'], max_comments: max_comments }
            assert_response :success
            json2 = JSON.parse(@response.body)
            json2['comments'].each { |comment| refute_nil comment['ticket_id'] }

            # make sure we retrieved all the comments
            assert_equal ticket_fixtures.map(&:comments).flatten.map(&:id).sort, (json2['comments'] + json1['comments']).map { |o| o['id'] }.sort
          end

          it 'paginates correctly if the first ticket has fewer than max_comments' do
            max_comments = 2
            comment_counts = [1, 3, 5]

            ticket_fixtures = comment_counts.map { |count| ticket_with_comments(count) }
            ticket_ids = ticket_fixtures.map(&:nice_id)

            comments = collect_comments(ticket_ids, max_comments)

            # make sure we retrieved all the comments
            assert_equal comment_counts.inject(:+), comments.size
            assert_equal ticket_fixtures.map(&:comments).flatten.map(&:id).sort, comments.map { |o| o['id'] }.sort
          end

          it 'paginates correctly if the first ticket has more than max_comments' do
            max_comments = 4
            comment_counts = [10, 3, 5]

            ticket_fixtures = comment_counts.map { |count| ticket_with_comments(count) }
            ticket_ids = ticket_fixtures.map(&:nice_id)

            comments = collect_comments(ticket_ids, max_comments)

            # make sure we retrieved all the comments
            assert_equal comment_counts.inject(:+), comments.size
            assert_equal ticket_fixtures.map(&:comments).flatten.map(&:id).sort, comments.map { |o| o['id'] }.sort
          end

          it 'paginates correctly if a random ticket has many comments' do
            max_comments = 3

            comment_counts = [3, 10, 4]
            ticket_fixtures = comment_counts.map { |count| ticket_with_comments(count) }
            ticket_ids = ticket_fixtures.map(&:nice_id)

            comments = collect_comments(ticket_ids, max_comments)

            # make sure we retrieved all the comments
            assert_equal comment_counts.inject(:+), comments.size
            assert_equal ticket_fixtures.map(&:comments).flatten.map(&:id).sort, comments.map { |o| o['id'] }.sort
          end
        end
      end
    end

    describe '#errors' do
      before do
        Api::V2::Internal::TicketsController.any_instance.stubs(:max_ids).returns(2)
      end
      it 'returns a 422 error when the ticket_ids parameter is not present' do
        get :show_many_comments
        assert_response :bad_request
      end

      it 'returns 400 error when too many ticket ids are submitted' do
        get :show_many_comments, params: { ticket_ids: [1, 2, 3] }
        assert_response :bad_request
      end
    end

    def collect_comments(ticket_ids, max_comments)
      comments = []
      json = {'id_waterline' => nil, 'comment_id_waterline' => nil}
      loop do
        get :show_many_comments, params: { ticket_ids: ticket_ids, id_waterline: json['id_waterline'], comment_id_waterline: json['comment_id_waterline'], max_comments: max_comments }

        assert_response :success
        json = JSON.parse(@response.body)

        json['comments'].each { |comment| refute_nil comment['ticket_id'] }
        comments += json['comments']
        break if json['next_page'].nil?
      end
      comments
    end

    def ticket_with_comments(num_comments)
      ticket = @account.tickets.new(description: "Wombats")
      ticket.will_be_saved_by(users(:minimum_admin))
      ticket.expects(:validate_ticket_form_id).never
      ticket.save!

      diff = num_comments - ticket.comments.size
      if diff > 0
        diff.times do |i|
          comment_attrs = {
            body: "some comment #{i}",
            is_public: true,
            author_id: ticket.requester_id,
            via_id: 0,
            via_reference_id: 0
          }
          ticket.add_comment(comment_attrs).save
        end
        ticket.comments.reload
      end
      ticket
    end
  end

  describe "pigeon subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "pigeon") do
      describe "a GET to :permissions" do
        let(:ticket) { tickets(:minimum_1) }
        let(:user) { users(:minimum_end_user) }

        it 'returns the permissions for the ticket and user specified' do
          get :permissions, params: { id: ticket.nice_id, user_id: ticket.requester.id }

          assert_response :success
          json = JSON.parse(@response.body)
          refute_nil json['permissions']
        end
      end
    end
  end
end
