require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::AuditLogsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
    @request.account = accounts(:minimum)
    @params = {
        account_id: accounts(:minimum).id,
        actor_id: users(:minimum_end_user).id,
        actor_type: 'User',
        action: 'update',
        ip_address: '1.1.1.1',
        source_id: 1,
        source_type: 'Zendesk::AppMarket::App',
        source_display_name: 'JIRA',
        message: 'JIRA',
        visible: true
      }
  end

  as_a_subsystem_user(account: :minimum) do
    describe_with_and_without_arturo_enabled :audit_logs_refactor do
      it "creates audit log" do
        post :create, params: { event: @params, format: :json }

        assert_response :created
        assert CIA::Event.find_by_source_type('Zendesk::AppMarket::App').visible?
      end
    end

    describe 'bad input' do
      it 'rejects requests without the `event` param' do
        post :create, params: { action: 'update' }

        assert_response :bad_request
      end

      it 'rejects requests when `event` is not a hash' do
        post :create, params: { event: 'update' }

        assert_response :bad_request
      end
    end

    describe "with app_setting_changes" do
      let(:change) { CIA::AttributeChange.last }

      before do
        @params.merge!(
          source_type: 'Zendesk::AppMarket::AppInstallation',
          app_setting_changes: { 'awesomeness' => ['0', '11'] }
        )
      end

      it "creates setting audit for update events" do
        post :create, params: { event: @params, format: :json }
        assert_response :created

        assert_equal({ 'awesomeness' => '0' }.to_json, change.old_value)
        assert_equal({ 'awesomeness' => '11' }.to_json, change.new_value)
      end

      it "does not create invalid json" do
        @params[:app_setting_changes] = { 'awesomeness' => ['0', '11'], 'big' => ['0', 'a' * 300], 'small' => ['0', '11'] }

        post :create, params: { event: @params, format: :json }
        assert_response :created

        assert_equal "{\"awesomeness\":\"0\",\"big\":\"0\",\"small\":\"0\"}", change.old_value
        assert_equal "{\"awesomeness\":\"11\",\"small\":\"11\"}", change.new_value
      end

      it "does not create settings audit when app is created" do
        @params[:action] = "create"

        post :create, params: { event: @params, format: :json }
        assert_response :created

        assert_equal 0, CIA::AttributeChange.count(:all)
      end
    end

    describe "create_attribute_change" do
      let(:changes) { CIA::AttributeChange.last(2) }

      before do
        @params.merge!(
          source_type: 'VoiceAccount',
          changes: { 'awesomeness' => ['0', '11'], 'more_awesome' => [true, false] }
        )
      end

      it "creates cia_attribute_changes audit for update events" do
        post :create, params: { event: @params, format: :json }
        assert_response :created

        assert_equal('0', changes[0].old_value)
        assert_equal('11', changes[0].new_value)

        assert_equal('1', changes[1].old_value)
        assert_equal('0', changes[1].new_value)
      end

      it "creates cia_attribute_changes audit when object is created" do
        @params[:action] = "create"

        assert_difference 'CIA::AttributeChange.count(:all)', 2 do
          post :create, params: { event: @params, format: :json }
          assert_response :created
        end
      end

      it "does not create cia_attribute_changes audit when there are no changes" do
        @params[:changes] = {}

        assert_no_difference 'CIA::AttributeChange.count(:all)' do
          post :create, params: { event: @params, format: :json }
          assert_response :created
        end
      end
    end
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    it "creates audit log" do
      post :create, params: { event: {
        account_id: accounts(:minimum).id,
        actor_id: -1,
        actor_type: 'User',
        action: 'update',
        ip_address: '1.1.1.1',
        source_id: 1,
        source_type: 'Voltron::Chat::Account',
        source_display_name: 'MOFO',
        message: 'MOOOOOOOOOOO',
        visible: true
      }, format: :json }

      assert_response :created
      assert CIA::Event.find_by_source_type('Voltron::Chat::Account').visible?
    end
  end

  as_a_subsystem_user(user: 'account_service', account: :shell_account_without_support) do
    it "creates audit log" do
      post :create, params: { event: {
        account_id: accounts(:minimum).id,
        actor_id: -1,
        actor_type: 'User',
        action: 'update',
        ip_address: '1.1.1.1',
        source_id: 1,
        source_type: 'Voltron::Chat::Account',
        source_display_name: 'MOFO',
        message: 'MOOOOOOOOOOO',
        visible: true
      }, format: :json }

      assert_response :created
      assert CIA::Event.find_by_source_type('Voltron::Chat::Account').visible?
    end
  end

  as_a_subsystem_user(user: 'metropolis', account: :shell_account_without_support) do
    it "creates audit log" do
      post :create, params: { event: {
        account_id: accounts(:minimum).id,
        actor_id: -1,
        actor_type: 'User',
        action: 'update',
        ip_address: '1.1.1.1',
        source_id: 1,
        source_type: 'Voltron::Chat::Account',
        source_display_name: 'MOFO',
        message: 'MOOOOOOOOOOO',
        visible: true
      }, format: :json }

      assert_response :created
      assert CIA::Event.find_by_source_type('Voltron::Chat::Account').visible?
    end
  end
end
