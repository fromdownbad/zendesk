require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::AuditsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
    @request.account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
  end

  with_options(controller: 'api/v2/internal/audits') do |request|
    request.should_route :get, '/api/v2/internal/tickets/1/audits', action: 'index', ticket_id: 1
  end

  as_an_admin do
    should_be_forbidden :index, add_to_all: {ticket_id: 1}
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#index" do
      describe "for a non-existent ticket" do
        before { get :index, params: { ticket_id: 100000000 } }
        it('responds with not_found') { assert_response :not_found }
      end

      describe "for an existing ticket" do
        let (:get_args) { { ticket_id: @ticket.id } }

        before do
          @ticket.audits.each { |a| a.update_attribute(:created_at, Time.at(a.id)) }
          @sql_queries = sql_queries do
            get :index, params: get_args
          end
        end

        should_use_presenter Api::V2::Tickets::AuditPresenter, with: :audits

        it "does not run N+1 queries when listing audits" do
          # If you have a test failing here, make sure you're not just introducing an N+1
          assert_sql_queries 12
        end

        describe "sort ascending" do
          it "sorts correctly" do
            audits_json = JSON.parse(@response.body)["audits"]
            assert_equal audits_json.sort_by { |a| a['created_at'].to_datetime.to_i }, audits_json
          end
        end

        describe "sort descending" do
          let (:get_args) { { ticket_id: @ticket.id, sort_order: 'desc' } }

          it "sorts correctly" do
            audits_json = JSON.parse(@response.body)["audits"]
            assert_equal audits_json.sort_by { |a| -a['created_at'].to_datetime.to_i }, audits_json
          end
        end
      end

      describe "archived tickets" do
        let (:get_args) { { ticket_id: @ticket.id } }

        before do
          archive_and_delete(@ticket)
          get :index, params: get_args
        end

        it "can find archived audits" do
          JSON.parse(@response.body)["audits"].size.must_equal 2
        end

        describe "sort ascending" do
          it "sorts correctly" do
            audits_json = JSON.parse(@response.body)["audits"]
            assert_equal audits_json.sort_by { |a| a['created_at'].to_datetime.to_i }, audits_json
          end
        end

        describe "sort descending" do
          let (:get_args) { { ticket_id: @ticket.id, sort_order: 'desc' } }

          it "sorts correctly" do
            audits_json = JSON.parse(@response.body)["audits"]
            assert_equal audits_json.sort_by { |a| -a['created_at'].to_datetime.to_i }, audits_json
          end
        end
      end

      describe "filtering events" do
        before do
          events(:create_comment_for_minimum_ticket_1)
          events(:create_change_for_minimum_ticket_1)
          get :index, params: { ticket_id: @ticket.id, filter_events: ["Comment"] }
        end

        should_use_presenter Api::V2::Tickets::AuditPresenter, with: :audits

        it "only returns audit with events with the specified type" do
          events_json = JSON.parse(@response.body)["audits"].first["events"]
          assert_equal 2, events_json.size
          assert_equal "Comment", events_json.map { |e| e["type"] }.uniq.first
        end
      end
    end
  end
end
