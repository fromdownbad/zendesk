require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::ChallengeTokenController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users

  let(:account) { accounts(:minimum) }
  let(:ip_address) { '127.0.0.1' }
  let(:session_key) { '2:46:e54266850598213599620037522a6ca97c32fbd1b7bba2910688253a2d01afb1' }
  let(:user) { users(:minimum_end_user) }
  let(:actual_response) { JSON.parse(response.body) }
  let(:challenge_token) { Token.find_by_value(actual_response['challenge']) }
  let(:expected_response) { { 'challenge' => challenge_token.value } }

  before do
    accept :json
    @request.account = account
  end

  with_options(controller: 'api/v2/internal/challenge_token') do |request|
    request.should_route :post, 'api/v2/internal/challenge_token', action: 'create'
  end

  as_an_admin do
    should_be_forbidden :create
  end

  as_a_subsystem_user(user: 'zendesk', account: :minimum) do
    describe '#create' do
      before do
        assert_empty user.challenge_tokens
        post :create, params: { ip_address: ip_address, session_key: session_key, user_id: user.id }
      end

      it 'returns a succesful response' do
        assert_response :ok
      end

      it 'verifies that the actual response matches the expected' do
        assert_equal expected_response, actual_response
      end

      it "verifies that the user's challenge token matches the original parameters" do
        assert_equal 'User', challenge_token.source_type
        assert_equal user.id, challenge_token.source_id
        assert_equal account.id, challenge_token.account_id
        assert_equal ip_address, challenge_token.ip_address
      end
    end
  end

  describe 'as an unauthenticated user one should not be able to call this method' do
    before do
      assert_empty user.challenge_tokens
      post :create, params: { ip_address: ip_address, session_key: session_key, user_id: user.id }
    end

    it 'returns a failed response' do
      assert_response :unauthorized
    end

    it 'verifies no challenge_tokens were created' do
      assert_empty user.challenge_tokens
    end
  end
end
