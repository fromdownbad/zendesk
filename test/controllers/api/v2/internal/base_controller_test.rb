require_relative "../../../../support/test_helper"
require_relative "../../../../support/multiproduct_test_helper"

SingleCov.covered! uncovered: 4

describe Api::V2::Internal::BaseController do
  extend MultiproductTestHelper

  class ApiV2InternalTestController < Api::V2::Internal::BaseController
    allow_parameters :index, a: Parameters.integer
    def index
      render json: {"a" => params["a"] || 1}
    end

    allow_parameters :action_that_raises_temp_db_exception, {}
    def action_that_raises_temp_db_exception
      raise ZendeskDatabaseSupport::MappedDatabaseExceptions::LostConnection, "Lost connection to MySQL"
    end
  end

  tests ApiV2InternalTestController
  use_test_routes

  let (:account) { accounts(:minimum) }

  before do
    accept :json
    @request.account = account
  end

  should_be_unauthorized_when_not_logged_in([:index], "minimum_admin", 403)
  should_be_unauthorized_when_not_logged_in([:index], "minimum_end_user", 403)

  describe "when accessed as a system user" do
    before do
      @controller.stubs(:current_registered_user).returns(Zendesk::SystemUser.find_subsystem_user_by_name("zendesk"))
    end

    it "is able to access" do
      get :index
      assert_response :success
      assert_equal({"a" => 1}, JSON.parse(@response.body))
    end

    it "fails without json" do
      accept :xml
      get :index
      assert_response 415
    end

    it "responds with 503 for temporary database error" do
      get :action_that_raises_temp_db_exception
      assert_response 503
      assert_equal("Lost connection to MySQL", JSON.parse(@response.body)["description"])
    end

    should_eventually "not be able to send unregistered params" do
      get :index, params: { "a" => 2 }
      assert_response :success
      assert_equal({"a" => 1}, JSON.parse(@response.body))
    end

    it_allows_access_for_all_multiproduct_product_types
  end
end
