require_relative "../../../../support/test_helper"
require_relative "../../../../support/attachment_test_helper"

SingleCov.covered!

describe Api::V2::Internal::UserReportsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users, :user_settings, :subscriptions

  before do
    set_header('X-Zendesk-Via', "Monitor event")
    set_header('X-Zendesk-Via-Reference-ID', 1234)
    @request.account = @account = accounts(:minimum)
    @controller.stubs(:current_registered_user).returns(Zendesk::SystemUser.find_subsystem_user_by_name("zendesk"))
  end

  with_options(controller: "api/v2/internal/user_reports") do |request|
    request.should_route :post, "/api/v2/internal/user_reports", action: "new"
  end

  describe "as a SystemUser" do
    describe "a POST to #new" do
      before do
        BillingRelatedUserDataJob.expects(:enqueue)
        post :new, format: "json"
      end

      should_use_presenter Api::V2::JobStatusPresenter
    end
  end
end
