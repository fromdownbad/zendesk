require_relative "../../../../support/test_helper"
require_relative "../../../../support/attachment_test_helper"
require 'prop'

SingleCov.covered!

describe Api::V2::Internal::JobsController do
  fixtures :all

  with_options(controller: 'api/v2/internal/jobs') do |request|
    request.should_route :post, '/api/v2/internal/jobs/create', action: 'create'
  end

  before do
    accept :json
  end

  describe 'two_factor_export' do
    as_an_admin do
      should_be_forbidden :create
    end

    as_a_subsystem_user(user: 'centraladminapp', account: :support) do
      describe 'two factor export job' do
        it 'enqueues the two factor export job' do
          Resque.expects(:enqueue).with(TwoFactorCsvJob, @account.id, @account.users[0].id, anything)
          post :create, params: { type: 'two_factor_export', requester_id: @account.users[0].id }

          assert_response :ok
          assert_includes @response.body, 'We are building the requested CSV file for you'
        end

        describe "being throttled" do
          it "returns status 429" do
            Resque.expects(:enqueue).with(TwoFactorCsvJob, @account.id, @account.users[0].id, anything).raises(Resque::ThrottledError)
            post :create, params: { type: 'two_factor_export', requester_id: @account.users[0].id }

            assert_response :too_many_requests
            assert_includes @response.body, 'too many requests made to two_factor_export job'
          end
        end
      end
    end
  end
end
