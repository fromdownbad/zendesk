require File.expand_path "../../../../support/test_helper", File.dirname(__FILE__)

SingleCov.covered!

describe Api::V2::Internal::HelpCenterStatesController do
  extend Api::V2::TestHelper

  before do
    accept :json
    @account              = accounts(:minimum)
    @brand                = brands(:minimum)
    @request.account      = @account
    @request.env['zendesk.brand'] = @brand

    @state_changer = HelpCenterStateChanger.new(account: @account, brand: @brand)
    HelpCenterStateChanger.expects(:new).with(account: @account, brand: @brand).returns(@state_changer)
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#update" do
      before do
        HelpCenter.create(
          id: 12345,
          state: 'enabled',
          account_id: @account.id,
          brand_id: @brand.id
        )
      end

      describe "when directive is 'enable' and help_center_state is 'enabled'" do
        before do
          @params = {
            directive: 'enable',
            help_center_state: 'enabled',
            help_center_id: 500
          }
        end

        it "responds with HTTP 200" do
          put :update, params: @params
          assert_response :ok
        end

        it "calls #update on the state changer" do
          @state_changer.expects(:update).with(
            account_help_center_state: 'enable',
            brand_help_center_state: 'enabled',
            help_center_id: 500
          )

          put :update, params: @params
        end
      end

      describe "when directive is 'disable' and help_center_state is 'restricted'" do
        before do
          @params = {
            directive: 'disable',
            help_center_state: 'restricted',
            help_center_id: 500
          }
        end

        it "responds with HTTP 200" do
          put :update, params: @params
          assert_response :ok
        end

        it "calls #update on the state changer" do
          @state_changer.expects(:update).with(
            account_help_center_state: 'disable',
            brand_help_center_state: 'restricted',
            help_center_id: 500
          )

          put :update, params: @params
        end
      end

      describe "when directive is 'restrict' and help_center_state is 'archived'" do
        before do
          @params = {
            directive: 'restrict',
            help_center_state: 'archived',
            help_center_id: 500
          }
        end

        it "responds with HTTP 200" do
          put :update, params: @params
          assert_response :ok
        end

        it "calls #update on the state changer" do
          @state_changer.expects(:update).with(
            account_help_center_state: 'restrict',
            brand_help_center_state: 'archived',
            help_center_id: 500
          )

          put :update, params: @params
        end
      end

      describe "when directive is unknown" do
        before do
          @params = {
            directive: 'foo',
            help_center_state: 'enabled',
            help_center_id: 500
          }
        end

        it "responds with HTTP 400" do
          put :update, params: @params
          assert_response :bad_request
        end
      end

      describe "when help_center_state is unknown" do
        before do
          @params = {
            directive: 'enable',
            help_center_state: 'foo',
            help_center_id: 500
          }
        end

        it "responds with HTTP 400" do
          put :update, params: @params
          assert_response :bad_request
        end
      end
    end

    describe "#destroy" do
      before do
        HelpCenter.create(id: 12345, state: 'enabled', account_id: @account.id, brand_id: @brand.id)
      end

      describe "when directive is 'enable'" do
        before do
          @params = { directive: 'enable' }
        end

        it "responds with HTTP 200" do
          delete :destroy, params: @params
          assert_response :ok
        end

        it "calls #destroy on the state changer" do
          @state_changer.expects(:destroy).with(account_help_center_state: 'enable')

          delete :destroy, params: @params
        end
      end

      describe "when directive is 'disable'" do
        before do
          @params = { directive: 'disable' }
        end

        it "responds with HTTP 200" do
          delete :destroy, params: @params
          assert_response :ok
        end

        it "calls #destroy on the state changer" do
          @state_changer.expects(:destroy).with(account_help_center_state: 'disable')

          delete :destroy, params: @params
        end
      end

      describe "when directive is 'restrict'" do
        before do
          @params = { directive: 'restrict' }
        end

        it "responds with HTTP 200" do
          delete :destroy, params: @params
          assert_response :ok
        end

        it "calls #destroy on the state changer" do
          @state_changer.expects(:destroy).with(account_help_center_state: 'restrict')

          delete :destroy, params: @params
        end
      end

      describe "when directive is unknown" do
        before do
          @params = { directive: 'foo' }
        end

        it "responds with HTTP 400" do
          delete :destroy, params: @params
          assert_response :bad_request
        end
      end
    end
  end
end
