require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::BrandsController do
  extend Api::V2::TestHelper
  fixtures :accounts, :brands, :users, :routes

  before do
    accept :json
    @account = accounts(:minimum)
    @brand = @account.default_brand
    @request.account = @account
  end

  with_options(controller: "api/v2/internal/brands") do |request|
    request.should_route :put, "/api/v2/internal/brands/1/remove_host_mapping", action: "remove_host_mapping", id: 1
  end

  as_an_admin do
    should_be_forbidden [:put, :remove_host_mapping, id: 1]
  end

  as_a_subsystem_user(account: :minimum) do
    describe "PUT #remove_host_mapping" do
      it "removes host_mapping from the brand" do
        @route = Route.find(@brand.route_id)
        @route.update_attribute(:host_mapping, "example.com")

        put :remove_host_mapping, params: { id: @brand.id }
        assert_response :success

        @route.reload
        assert_nil @route.host_mapping
      end
    end
  end
end
