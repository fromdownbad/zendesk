require File.expand_path '../../../../support/test_helper', File.dirname(__FILE__)

SingleCov.covered! uncovered: 1

describe Api::V2::Internal::SubscriptionController do
  extend Api::V2::TestHelper

  fixtures :all

  let(:t1) { Time.parse('2014-01-01') }

  before do
    accept :json
    @account              = accounts(:minimum)
    @request.account      = @account
  end

  with_options(controller: 'api/v2/internal/subscription') do |request|
    request.should_route :put, '/api/v2/internal/accounts/1/subscription.json', action: 'update', id: 1, format: 'json'
  end

  as_a_subsystem_user(account: :minimum) do
    describe 'on a canceled account' do
      before do
        @account.cancel!
        put :update, params: { id: @account.id, subscription: { canceled_on: t1 } }
      end
      it 'sets the canceled_on date on the subscription model' do
        assert_equal t1, @account.subscription.reload.canceled_on
      end
      it 'does not set other attributes' do
        put :update, params: { id: @account.id, subscription: { plan_type: 10 } }
        assert_equal 2, @account.subscription.reload.plan_type
      end
      it 'responds with status ok' do
        assert_equal 200, @response.status
      end
    end
  end
end
