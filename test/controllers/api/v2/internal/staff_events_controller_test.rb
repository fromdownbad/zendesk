require_relative '../../../../support/test_helper'
require_relative '../../../../support/suite_test_helper'
require_relative '../../../../support/entitlement_test_helper'

SingleCov.covered! uncovered: 31

describe Api::V2::Internal::StaffEventsController do
  extend Api::V2::TestHelper
  include EntitlementTestHelper
  include SuiteTestHelper

  fixtures :all

  before do
    accept :json
    Account.any_instance.stubs(
      has_light_agents?: true,
      has_chat_permission_set?: true,
      has_permission_sets?: true
    )
  end

  as_an_admin do
    should_be_forbidden :create
  end

  describe 'POST #create' do
    describe 'when handling notifications' do
      let(:user) { users(:multiproduct_contributor) }
      let(:entitlements_get_response) { {}.to_json }
      let(:notification_type) { '' }
      let(:account) { accounts(:minimum) }
      let(:user_id) { user.id }
      let(:params) do
        {
          account_id: account.id,
          user_id: user_id,
          notification_type: notification_type
        }
      end

      before do
        stub_request(
          :get,
          /api\/services\/staff\/\d+\/entitlements/
        ).to_return(
          status: 200,
          body: entitlements_get_response,
          headers: {"Content-Type" => "application/json"}
        )
      end

      as_a_subsystem_user(user: 'metropolis', account: :minimum) do
        describe 'when account is not shell account' do
          describe 'and chat entitlement is changed' do
            let(:notification_type) { "entitlement_updated" }
            let(:entitlements_get_response) { make_entitlements_response(chat: 'admin') }
            let(:user) { users(:minimum_agent) }

            it 'does update user chat entitlement' do
              post :create, params: params
              assert_equal 'admin', user.settings.chat_entitlement
            end
          end
        end
      end

      as_a_subsystem_user(user: 'metropolis', account: :multiproduct) do
        describe 'that is a user notification' do
          let(:notification_type) { "staff_updated" }

          it('ignores the notification and returns a 200') do
            post :create, params: params
            assert_response :ok
          end
        end

        describe 'that is a shared_session notification' do
          let(:notification_type) { "shared_session_expired" }

          it('ignores the notification and returns a 200') do
            post :create, params: params
            assert_response :ok
          end
        end

        describe 'that is an entitlements notification' do
          let(:notification_type) { "entitlement_updated" }

          describe 'and chat entitlement is changed' do
            let(:entitlements_get_response) { make_entitlements_response(chat: 'admin') }

            it 'updates user chat entitlement' do
              post :create, params: params
              assert_equal 'admin', user.settings.chat_entitlement
            end
          end

          describe 'and connect entitlement is changed' do
            let(:entitlements_get_response) { make_entitlements_response(connect: 'agent') }

            it 'updates user connect entitlement' do
              post :create, params: params
              assert_equal 'agent', user.settings.connect_entitlement
            end
          end

          describe 'and support entitlement is changed' do
            let(:entitlements_get_response) { make_entitlements_response(support: 'agent') }

            describe_with_arturo_enabled :ocp_reverse_sync do
              it 'updates user support entitlement from staff service' do
                Zendesk::SupportUsers::EntitlementSynchronizer.expects(:perform).with(user, strategy: :pull)
                post :create, params: params
              end
            end

            describe_with_arturo_disabled :ocp_reverse_sync do
              it 'does not update user support entitlement from staff service' do
                Zendesk::SupportUsers::EntitlementSynchronizer.expects(:perform).with(user, strategy: :pull).never
                post :create, params: params
              end
            end
          end

          describe 'and connect and chat entitlements are changed' do
            let(:entitlements_get_response) { make_entitlements_response(connect: 'agent', chat: 'admin') }

            it 'updates user connect and chat entitlements' do
              post :create, params: params
              assert_equal 'agent', user.settings.connect_entitlement
              assert_equal 'admin', user.settings.chat_entitlement
            end
          end

          describe 'and user is soft deleted' do
            before { user.update_column(:is_active, false) }
            it 'does not call Staff Service' do
              Zendesk::StaffClient.any_instance.expects(:get_entitlements!).never
              post :create, params: params
            end

            it('ignores the notification and returns a 200') do
              post :create, params: params
              assert_response :ok
            end
          end

          describe 'and user is deleted' do
            let(:user_id) { 123456 }

            it 'does not call Staff Service' do
              Zendesk::StaffClient.any_instance.expects(:get_entitlements!).never
              post :create, params: params
            end

            it('ignores the notification and returns a 200') do
              post :create, params: params
              assert_response :ok
            end
          end

          describe 'and user is an end user' do
            let(:user) { users(:multiproduct_end_user) }

            it 'does not call Staff Service' do
              Zendesk::StaffClient.any_instance.expects(:get_entitlements!).never
              post :create, params: params
            end

            it('ignores the notification and returns a 200') do
              post :create, params: params
              assert_response :ok
            end
          end
        end
      end
    end
  end
end
