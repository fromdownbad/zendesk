require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::CsvExportsController do
  extend Api::V2::TestHelper

  fixtures :accounts, :users, :csv_export_records

  before do
    accept :json
    @request.account = accounts(:minimum)
  end

  with_options(controller: "api/v2/internal/csv_exports") do |request|
    request.should_route :post, "/api/v2/internal/csv_exports", action: "create"
    request.should_route :get, "/api/v2/internal/csv_exports/accounts", action: "accounts"
  end

  as_an_admin do
    should_be_forbidden [:post, :create, {}]
    should_be_forbidden [:get, :accounts, {}]
  end

  as_a_subsystem_user(account: :minimum) do
    describe "#accounts" do
      before do
        @request.env["zendesk.account"] = nil
      end

      describe "with an account 3 weeks old" do
        before do
          Timecop.travel(3.weeks.ago) do
            @account.csv_export_records.create!(user_id: -1, url: "test")
            accounts(:with_groups).csv_export_records.create!(user_id: -1, url: "test")
          end

          get :accounts
        end

        it('responds with ok') { assert_response :ok }

        # See csv_export_record fixtures
        it "returns all distinct accounts used in the last two weeks" do
          ids = JSON.parse(@response.body)["accounts"].map { |a| a["id"] }
          assert_equal [@account.id], ids
        end
      end

      describe "with an initial account" do
        before do
          @initial_account = accounts(:payment)
          @initial_account.csv_export_records.create!(user_id: 9999)

          get :accounts
        end

        it('responds with ok') { assert_response :ok }

        it "returns user ids of initial exports" do
          ids = JSON.parse(@response.body)["accounts"].map { |a| a["initial_exporter"] }.compact
          assert_equal [9999], ids
        end
      end

      describe "with a shard-moved account" do
        before do
          # Can't use minimum because of fixtures
          @payment_account = accounts(:payment)
          @payment_account.csv_export_records.create!(user_id: -1)

          # Bypass is_serviceable change from callbacks
          assert @payment_account.update_attribute(:shard_id, 2)

          get :accounts
        end

        it('responds with ok') { assert_response :ok }

        it "does not return moved account" do
          ids = JSON.parse(@response.body)["accounts"].map { |a| a["id"] }
          assert_equal [@account.id], ids
        end
      end
    end

    describe "#create with nonexistant user" do
      before { post :create, params: { user_id: 99999 } }
      it('responds with not_found') { assert_response :not_found }
    end

    describe "#create" do
      def perform
        post :create, params: { user_id: users(:minimum_admin), url: "http://www.test.com" }
      end

      it "creates" do
        assert_difference "@account.csv_export_records.count(:all)", +1 do
          perform
          assert_response :ok
        end
      end

      it "sends an email" do
        ActionMailer::Base.perform_deliveries = true
        assert_difference "ActionMailer::Base.deliveries.size", +1 do
          perform
        end
      end
    end
  end
end
