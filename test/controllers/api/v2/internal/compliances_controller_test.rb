require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::CompliancesController do
  extend Api::V2::TestHelper
  fixtures :accounts

  before do
    accept :json
    @request.account = accounts(:minimum)
  end

  with_options(controller: "api/v2/internal/compliances") do |request|
    request.should_route :get, "/api/v2/internal/compliances", action: "index"
  end

  as_an_admin do
    should_be_forbidden :index
  end

  as_a_subsystem_user(account: :minimum) do
    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::Internal::CompliancePresenter, status: :ok
    end
  end
end
