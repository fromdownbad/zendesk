require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Collaboration::EventsController do
  fixtures :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_2) }
  let(:agent) { users(:minimum_agent) }

  let(:request_params) do
    {
      ticket_id: ticket.nice_id,
      event: {
        subject: 'Can anyone in IT help us?',
        type: 'thread_created',
        thread_id: 'abcd123'
      }
    }
  end

  describe 'when unauthenticated' do
    describe 'POST /events' do
      before do
        accept :json
      end

      it 'returns 404' do
        post :create, params: request_params
        assert_response :not_found
      end
    end
  end

  describe 'when authenticated' do
    as_a_subsystem_user(user: 'collaboration', account: :minimum) do
      describe 'POST /events' do
        let(:event) do
          ticket.reload.audits.last.events.to_a.select do |e|
            e.type == event_type
          end.last
        end

        describe 'with type CollabThreadCreated' do
          let(:event_type) { 'CollabThreadCreated' }
          let(:request_params) do
            {
              ticket_id: ticket.nice_id,
              event: {
                subject: 'Can anyone in IT help us?',
                type: 'thread_created',
                thread_id: 'abcd123',
                recipient_count: 2,
                sc_type: 'email',
              }
            }
          end

          before do
            accept :json
            post :create, params: request_params
          end

          it 'responds with 200 ok' do
            assert_response :ok
            assert response.body.to_json["id"].present?
          end

          it 'creates a new collaboration event' do
            assert_equal 1, ticket.reload.audits.last.events.to_a.select { |e| e.type == event_type }.count
          end

          it 'sets is_public of the event to false' do
            refute event.is_public?
          end

          it 'sets the subject' do
            assert_equal 'Can anyone in IT help us?', event.subject
          end

          it 'sets the recipient_count' do
            assert_equal 2, event.recipient_count
          end

          it 'sets the sc_type' do
            assert_equal 'email', event.sc_type
          end

          describe 'when the event has already been created' do
            before do
              Arturo.enable_feature! :sc_skip_duplicate_events

              event = CollabThreadCreated.new
              event.subject = request_params[:event][:subject]
              event.thread_id = request_params[:event][:thread_id]
              event.recipient_count = request_params[:event][:recipient_count]
              event.sc_type = request_params[:event][:sc_type]
              ticket.will_be_saved_by(agent, via_id: ViaType.SIDE_CONVERSATION)
              ticket.audit.events << event
              ticket.save!
            end

            describe 'with the arturo on' do
              before do
                Arturo.enable_feature! :sc_skip_duplicate_events

                accept :json
              end

              it 'responds with 422 unprocessable entity' do
                post :create, params: request_params
                assert_response :unprocessable_entity
              end

              it 'creates replies just fine' do
                reply_request_params = {
                  ticket_id: ticket.nice_id,
                  event: {
                    type: 'thread_reply',
                    thread_id: 'abcd123'
                  }
                }

                post :create, params: reply_request_params
                assert_response :ok
              end
            end

            describe 'with the arturo off' do
              before do
                Arturo.disable_feature! :sc_skip_duplicate_events

                accept :json
                post :create, params: request_params
              end

              it 'responds with 200 ok' do
                assert_response :ok
              end
            end
          end
        end

        describe 'with type CollabThreadClosed' do
          let(:event_type) { 'CollabThreadClosed' }
          let(:request_params) do
            {
              ticket_id: ticket.nice_id,
              event: {
                subject: 'Can anyone in IT help us?',
                type: 'thread_closed',
                thread_id: 'abcd123',
                recipient_count: 2,
                sc_type: 'email',
              }
            }
          end
          before do
            accept :json
            post :create, params: request_params
          end

          it 'responds with 200 ok' do
            assert_response :ok
          end

          it 'creates a new collaboration event' do
            assert_equal 1, ticket.reload.audits.last.events.to_a.select { |e| e.type == event_type }.count
          end

          it 'sets is_public of the event to false' do
            refute event.is_public?
          end

          it 'sets the subject' do
            assert_equal 'Can anyone in IT help us?', event.subject
          end

          it 'sets the recipient_count' do
            assert_equal 2, event.recipient_count
          end

          it 'sets the sc_type' do
            assert_equal 'email', event.sc_type
          end
        end

        describe 'with type CollabThreadReopened' do
          let(:event_type) { 'CollabThreadReopened' }
          let(:request_params) do
            {
              ticket_id: ticket.nice_id,
              event: {
                subject: 'Can anyone in IT help us?',
                type: 'thread_reopened',
                thread_id: 'abcd123',
                recipient_count: 2,
                sc_type: 'email',
              }
            }
          end
          before do
            accept :json
            post :create, params: request_params
          end

          it 'responds with 200 ok' do
            assert_response :ok
          end

          it 'creates a new collaboration event' do
            assert_equal 1, ticket.reload.audits.last.events.to_a.select { |e| e.type == event_type }.count
          end

          it 'sets is_public of the event to false' do
            refute event.is_public?
          end

          it 'sets the subject' do
            assert_equal 'Can anyone in IT help us?', event.subject
          end

          it 'sets the recipient_count' do
            assert_equal 2, event.recipient_count
          end

          it 'sets the sc_type' do
            assert_equal 'email', event.sc_type
          end
        end

        describe 'with type CollabThreadReply' do
          let(:event_type) { 'CollabThreadReply' }
          let(:request_params) do
            {
              ticket_id: ticket.nice_id,
              event: {
                subject: 'Can anyone in IT help us?',
                type: 'thread_reply',
                thread_id: 'abcd123',
                recipient_count: 2,
                sc_type: 'email',
                identifier: 'somethingunique'
              }
            }
          end
          before do
            accept :json
            post :create, params: request_params
          end

          it 'responds with 200 ok' do
            assert_response :ok
          end

          it 'creates a new collaboration event' do
            assert_equal 1, ticket.reload.audits.last.events.to_a.select { |e| e.type == event_type }.count
          end

          it 'sets is_public of the event to false' do
            refute event.is_public?
          end

          it 'sets the subject' do
            assert_equal 'Can anyone in IT help us?', event.subject
          end

          it 'sets the recipient_count' do
            assert_equal 2, event.recipient_count
          end

          it 'sets the sc_type' do
            assert_equal 'email', event.sc_type
          end

          describe 'when the event has already been created' do
            before do
              Arturo.enable_feature! :sc_skip_duplicate_events

              event = CollabThreadCreated.new
              event.subject = request_params[:event][:subject]
              event.thread_id = request_params[:event][:thread_id]
              event.recipient_count = request_params[:event][:recipient_count]
              event.identifier = request_params[:event][:identifier]
              event.sc_type = request_params[:event][:sc_type]
              ticket.will_be_saved_by(agent, via_id: ViaType.SIDE_CONVERSATION)
              ticket.audit.events << event
              ticket.save!

              accept :json
            end

            it 'responds with 422 unprocessable entity' do
              post :create, params: request_params
              assert_response :unprocessable_entity
            end
          end
        end

        describe 'when the ticket does not exist' do
          let(:event_type) { 'CollabThreadReply' }
          let(:request_params) do
            {
              ticket_id: 888888,
              event: {
                subject: 'Can anyone in IT help us?',
                type: 'thread_reply',
                thread_id: 'abcd123',
                recipient_count: 2,
                sc_type: 'email',
              }
            }
          end
          before do
            accept :json
            post :create, params: request_params
          end

          it 'responds with 422 unprocessable entity' do
            assert_response :unprocessable_entity
          end
        end
      end
    end
  end
end
