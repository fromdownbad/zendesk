require_relative '../../../../../support/test_helper'
require_relative "../../../../../support/signed_auth_helper"

SingleCov.covered!

describe Api::V2::Internal::Collaboration::TicketsController do
  fixtures :all

  let(:account) { accounts(:minimum) }

  let(:create_params) do
    {
      ticket: {
        subject: 'Subject',
        external_id: 'zen:side_conversation:abc',
        group_id: groups(:minimum_group).id,
        comment: {
          html_body: '<div>test</div>',
          attachments: [
            {
              url: 'http://graphql/attachments/foo.txt',
              filename: 'foo.txt',
              content_type: 'text/plain'
            }, {
              url: 'http://graphql/attachments/foo.jpg',
              filename: 'foo.jpg',
              content_type: 'image/jpeg'
            }
          ]
        }
      }
    }
  end

  let(:update_params) do
    {
      id: tickets(:minimum_1).nice_id,
      ticket: {
        comment: {
          html_body: '<div>test</div>',
          attachments: [
            {
              url: 'http://graphql/attachments/foo.txt',
              filename: 'foo.txt',
              content_type: 'text/plain'
            }, {
              url: 'http://graphql/attachments/foo.jpg',
              filename: 'foo.jpg',
              content_type: 'image/jpeg'
            }
          ]
        }
      }
    }
  end

  before { accept :json }

  describe 'when unauthenticated' do
    describe 'POST /api/v2/internal/collaboration/tickets' do
      before { post :create, params: create_params }

      it 'returns 404' do
        assert_response :not_found
      end
    end

    describe 'PUT /api/v2/internal/collaboration/tickets/ID' do
      before { put :update, params: update_params }

      it 'returns 404' do
        assert_response :not_found
      end
    end
  end

  describe 'when authenticated' do
    as_an_agent do
      describe 'POST /api/v2/internal/collaboration/tickets' do
        before { post :create, params: create_params }

        it 'creates a ticket' do
          assert_response :success
          ticket_id = JSON.parse(response.body).dig('ticket', 'id')
          ticket = account.tickets.find_by!(nice_id: ticket_id)

          ticket.requester.must_equal users(:minimum_agent)
          ticket.subject.must_equal 'Subject'
          ticket.external_id.must_equal 'zen:side_conversation:abc'
          ticket.group_id.must_equal groups(:minimum_group).id

          ticket.via_id.must_equal Zendesk::Types::ViaType.SIDE_CONVERSATION
          ticket.events.each do |event|
            event.via_id.must_be :in?, [Zendesk::Types::ViaType.SIDE_CONVERSATION, Zendesk::Types::ViaType.RULE]
          end

          comment = ticket.comments.first
          comment.must_be :rich?
          comment.must_be :is_public?
          comment.html_body.must_equal '<div class="zd-comment" dir="auto"><div>test</div></div>'

          comment.attachments.size.must_equal 2

          comment.attachments[0].filename.must_equal 'foo.txt'
          comment.attachments[0].content_type.must_equal 'text/plain'
          comment.attachments[0].external_url.must_be :present?
          comment.attachments[0].external_url.url.must_equal 'http://graphql/attachments/foo.txt'

          comment.attachments[1].filename.must_equal 'foo.jpg'
          comment.attachments[1].content_type.must_equal 'image/jpeg'
          comment.attachments[1].external_url.must_be :present?
          comment.attachments[1].external_url.url.must_equal 'http://graphql/attachments/foo.jpg'
        end
      end

      describe 'PUT /api/v2/internal/collaboration/tickets/ID' do
        before { put :update, params: update_params }

        it 'adds a comment to the ticket' do
          assert_response :success

          ticket = tickets(:minimum_1).reload

          audit = ticket.audits.last
          audit.via_id.must_equal Zendesk::Types::ViaType.SIDE_CONVERSATION
          audit.author.must_equal users(:minimum_agent)

          comment = audit.comment
          comment.via_id.must_equal Zendesk::Types::ViaType.SIDE_CONVERSATION
          comment.author.must_equal users(:minimum_agent)
          comment.must_be :rich?
          comment.html_body.must_equal '<div class="zd-comment" dir="auto"><div>test</div></div>'

          comment.attachments.size.must_equal 2

          comment.attachments[0].filename.must_equal 'foo.txt'
          comment.attachments[0].content_type.must_equal 'text/plain'
          comment.attachments[0].external_url.must_be :present?
          comment.attachments[0].external_url.url.must_equal 'http://graphql/attachments/foo.txt'

          comment.attachments[1].filename.must_equal 'foo.jpg'
          comment.attachments[1].content_type.must_equal 'image/jpeg'
          comment.attachments[1].external_url.must_be :present?
          comment.attachments[1].external_url.url.must_equal 'http://graphql/attachments/foo.jpg'
        end
      end
    end
  end
end
