require_relative '../../../../support/test_helper'
require_relative '../../../../support/api_scopes_helper'

SingleCov.covered!

describe Api::V2::Internal::SubscriptionFeaturesController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper

  fixtures :accounts

  before do
    accept :json
  end

  with_options(controller: 'api/v2/internal/subscription_features') do |request|
    request.should_route :post, 'api/v2/internal/subscription_features/master',
      action: 'create_master_job'

    request.should_route :post, 'api/v2/internal/subscription_features/local',
      action: 'create_pod_job'
  end

  as_an_anonymous_user do
    should_be_unauthorized :create_master_job, :create_pod_job
  end

  as_an_end_user do
    should_be_forbidden :create_master_job, :create_pod_job
  end

  as_an_agent do
    should_be_forbidden :create_master_job, :create_pod_job
  end

  as_an_admin do
    should_be_forbidden :create_master_job, :create_pod_job
  end

  as_an_owner do
    should_be_forbidden :create_master_job, :create_pod_job
  end

  as_a_subsystem_user(account: :minimum) do
    should_be_authorized { [:create_master_job, :create_pod_job] }
  end

  as_a_subsystem_user(account: :minimum) do
    describe '#create_master_job' do
      describe 'with a valid feature name' do
        let(:pods) { Zendesk::Configuration::PodConfig.pod_ids }

        before do
          Account.stubs(:find_by_subdomain).returns(stub)

          pods.each do |pod_id|
            stub_request(:post, "https://pod#{pod_id}.zendesk-test.com/api/v2/"\
              "internal/subscription_features/local")
          end

          post :create_master_job, params: { feature_name: 'nps' }
        end

        it('responds with created') { assert_response :created }
      end

      describe 'with an invalid feature name' do
        before { post :create_master_job, params: { feature_name: 'invalid_feature' } }
        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end
    end

    describe '#create_pod_job' do
      before do
        CreateFeatureOnLocalPodJob.expects(:enqueue)
        post :create_pod_job, params: { pod_id: 1 }
      end

      it('responds with created') { assert_response :created }
    end
  end
end
