require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"
require_relative "../../../../support/multiproduct_test_helper"

SingleCov.covered!

describe Api::V2::Internal::SecuritySettingsController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper
  include MultiproductTestHelper

  fixtures :accounts, :remote_authentications

  before do
    accept :json
  end

  let(:current_account) { @controller.send(:current_account) }

  with_options(controller: 'api/v2/internal/security_settings') do |request|
    request.should_route :get, 'api/v2/internal/security_settings', action: 'show'
    request.should_route :put, 'api/v2/internal/security_settings', action: 'update'
  end

  as_an_admin do
    should_be_forbidden :show
  end

  describe 'with different accounts' do
    let(:expected) do
      {
        "admins_can_set_user_passwords" => false,
        "assumption_expiration" => nil,
        "assumable_account_type" => true,
        "assumable" => true,
        "assumption_duration" => "off",
        "custom_session_timeout_is_available" => false,
        "email_agent_when_sensitive_fields_changed" => true,
        "session_timeout" => '480',
        "authentication" => {
          "agent" => {
            "security_policy_id" => "100",
            "google_login" => false,
            "office_365_login" => false,
            "zendesk_login" => true,
            "remote_login" => false,
            "enforce_sso" => false,
            "remote_bypass" => "2",
            "password" => {
              "is_available" => false,
              "password_history_length" => "0",
              "password_length" => "5",
              "password_complexity" => "0",
              "password_in_mixed_case" => false,
              "failed_attempts_allowed" => "10",
              "max_sequence" => nil,
              "disallow_local_part_from_email" => false,
              "password_duration" => "0"
            },
          },
          "end_user" => {
            "security_policy_id" => "100",
            "google_login" => false,
            "office_365_login" => false,
            "zendesk_login" => true,
            "remote_login" => false,
            "twitter_login" => false,
            "facebook_login" => false,
            "enforce_sso" => false
          },
          "two_factor_enforce" => false
        },
        "remote_authentications" => {
          "saml" => {"id" => nil, "is_active" => false, "is_available" => false, "priority" => "1", "remote_login_url" => nil, "remote_logout_url" => nil, "fingerprint" => nil, "ip_ranges" => nil},
          "jwt" => {"id" => nil, "is_active" => false, "is_available" => false, "priority" => "1", "remote_login_url" => nil, "remote_logout_url" => nil, "shared_secret" => "not configured", "update_external_ids" => false, "ip_ranges" => nil, "alt_shared_secret" => "foo"}
        },
        "ip" => {
          "is_available" => true,
          "ip_ranges" => nil,
          "ip_restriction_enabled" => false,
          "enable_agent_ip_restrictions" => false
        }
      }
    end

    let(:pravda_chat_product) { FactoryBot.build(:chat_trial_product) }
    let(:response_body) { { products: [pravda_chat_product] }.to_json }

    describe 'shell_account_without_support from the multiproduct_test_helper' do
      let(:actual) { JSON.parse(response.body)['security_settings'] }

      before do
        @account         = setup_shell_account
        @request.account = @account
        system_user      = Zendesk::SystemUser.find_subsystem_user_by_name('centraladminapp')
        assert system_user, "No subsystem user found, bad zendesk.yml?"

        # The WardenStrategy in the zendesk_system_users gem sets the current account to the user account for subsystems
        system_user.account = @account

        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{@account.id}/products").
          to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })

        @controller.stubs(:current_registered_user).returns(system_user)

        Token.stubs(:generate).returns("foo")
        get :show
      end

      it 'returns json of account security settings' do
        actual.delete('two_factor_last_update')

        assert_equal expected, actual
      end
    end

    as_a_subsystem_user(user: 'bime', account: :shell_account_without_support) do
      before do
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{@account.id}/products").
          to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
        Zendesk::StaffClient.any_instance.stubs(:admin?).with(@account.owner.id).returns(true)
      end

      describe '#show' do
        it 'responds with ip restrictions' do
          get :show

          actual = JSON.parse(response.body)['security_settings']

          assert_not_nil actual['ip']
          assert_equal actual['ip'].keys, %w[is_available ip_ranges ip_restriction_enabled enable_agent_ip_restrictions]
        end
      end

      describe '#update' do
        it 'responds unauthorized' do
          put :update

          assert_equal 403, response.status
        end
      end
    end

    ['centraladminapp', 'zopim', 'metropolis'].each do |subsystem_user|
      [:shell_account_without_support, :minimum_jwt_saml].each do |account_type|
        as_a_subsystem_user(user: subsystem_user, account: account_type) do
          before do
            stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{@account.id}/products").
              to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
            Zendesk::StaffClient.any_instance.stubs(:admin?).with(@account.owner.id).returns(true)
          end

          describe "for user #{subsystem_user}" do
            describe '#show' do
              let (:actual) { JSON.parse(response.body)['security_settings'] }

              describe 'with two_factor_last_update' do
                describe 'when setting not present' do
                  before { get :show }

                  it 'responds with account created_at' do
                    assert_equal current_account.created_at.strftime('%Y-%m-%dT%H:%M:%SZ'), actual['two_factor_last_update']
                  end
                end

                describe 'when setting present' do
                  before do
                    @account.settings.two_factor_all_agents = true
                    @account.save!

                    get :show
                  end

                  it 'responds with setting updated_at' do
                    expected = @account.settings.find_by_name('two_factor_all_agents')[:updated_at].strftime('%Y-%m-%dT%H:%M:%SZ')

                    assert_equal expected, actual['two_factor_last_update']
                  end
                end
              end

              describe 'assumable parameter' do
                describe 'when account type is not assumable by default' do
                  before do
                    @account.stubs(:assumable_account_type?).returns(false)
                    SecurityMailer.expects(:deliver_assumption_expiration).once
                  end

                  describe 'when assumption_expiration is set in the past' do
                    before do
                      @account.settings.assumption_expiration = 1.week.ago
                      @account.settings.save!
                      get :show
                    end

                    it 'returns false' do
                      refute actual['assumable']
                    end
                  end

                  describe 'when assumption_expiration date is in the future' do
                    before do
                      @account.settings.assumption_expiration = 1.year.from_now
                      @account.settings.save!
                      get :show
                    end

                    it 'is assumable' do
                      assert actual['assumable']
                    end
                  end
                end
              end

              describe 'without two_factor_last_update' do
                before do
                  Token.stubs(:generate).returns("foo")
                  @account.stubs(:assumable?).returns(true)
                  get :show
                end

                should_use_presenter Api::V2::Internal::SecuritySettingsPresenter

                it('responds with success') { assert_response :success }

                it 'returns json of account security settings' do
                  actual.delete('two_factor_last_update')

                  if account_type == :minimum_jwt_saml
                    expected['assumable_account_type']               = false
                    expected['ip']['is_available']                   = true
                    expected['remote_authentications']['jwt']['id']  = '86254'
                    expected['remote_authentications']['saml']['id'] = '32353'
                  end

                  assert_equal expected, actual
                end

                describe 'agent #password' do
                  describe 'when agent security_policy_id has been previously updated to medium' do
                    before do
                      params = {
                        authentication: {
                          agent: {
                            security_policy_id: '200'
                          }
                        },
                        requester_id: @account.owner.id
                      }
                      put :update, params: params
                    end

                    it 'should show medium password settings in the response' do
                      @account.reload
                      assert_not_nil @account.custom_security_policy
                      get :show

                      assert_equal Zendesk::SecurityPolicy::Medium.password_length.to_s, actual['authentication']['agent']['password']['password_length']
                    end
                  end

                  describe 'when agent security_policy_id has never previously been updated' do
                    before do
                      @account.custom_security_policy.try(:destroy)
                      get :show
                    end

                    it 'should show low password settings in the response' do
                      @account.reload
                      assert_nil @account.custom_security_policy
                      assert_equal Zendesk::SecurityPolicy::Low.password_length.to_s, actual['authentication']['agent']['password']['password_length']
                    end
                  end
                end
              end
            end
          end
        end
      end

      # we are doing updates here on settings that are support specific. Hence, not using a shell account without support fixture is ok here.
      [:multiproduct, :minimum].each do |account_type|
        as_a_subsystem_user(user: subsystem_user, account: account_type) do
          describe "for user #{subsystem_user}" do
            describe '#show' do
              let (:actual_remote_auth_response) { JSON.parse(response.body)['security_settings']['remote_authentications'] }
              let (:expected_jwt_response) do
                {
                  "id"                  =>  nil,
                  "is_active"           =>  false,
                  "is_available"        =>  true,
                  "priority"            =>  "1",
                  "remote_login_url"    =>  nil,
                  "remote_logout_url"   =>  nil,
                  "shared_secret"       =>  "not configured",
                  "update_external_ids" =>  false,
                  "ip_ranges"           =>  nil,
                  "alt_shared_secret"   =>  "foo"
                }
              end

              let (:expected_saml_response) do
                {
                  "id"                =>  nil,
                  "is_active"         =>  false,
                  "is_available"      =>  false,
                  "priority"          =>  "1",
                  "remote_login_url"  =>  nil,
                  "remote_logout_url" =>  nil,
                  "fingerprint"       =>  nil,
                  "ip_ranges"         =>  nil
                }
              end

              before do
                @account.stubs(:has_jwt?).returns(true)
                @account.stubs(:has_saml?).returns(false)
                @account.stubs(:has_ip_restriction?).returns(false)
              end

              describe 'no remote authentications set up' do
                before do
                  @account.remote_authentications.each(&:destroy!)

                  @account.remote_authentications.reload

                  Token.stubs(:generate).returns("foo")
                  assert_empty @account.remote_authentications

                  get :show
                end

                it 'returns built jwt record with alt_shared_secret' do
                  assert_equal expected_jwt_response, actual_remote_auth_response['jwt']
                end

                it 'returns built saml record' do
                  assert_equal expected_saml_response, actual_remote_auth_response['saml']
                end
              end

              describe 'only jwt previously configured' do
                before do
                  Token.stubs(:generate).returns("foo")
                  @auth = @account.remote_authentications.create!(next_token: 'uVyPVcsmNiFkTAYWEe4p14bzOiGyAxOuPwMCvVRKie6p3LPI')
                  @auth.save!

                  get :show
                end

                it 'response contains redacted shared secret' do
                  assert_equal 'uVyPVc', actual_remote_auth_response['jwt']['shared_secret']
                end

                it 'returns alt_shared_secret' do
                  assert_equal 'foo', actual_remote_auth_response['jwt']['alt_shared_secret']
                end

                it 'returns built saml record' do
                  assert_equal expected_saml_response, actual_remote_auth_response['saml']
                end
              end

              describe 'only saml previously configured' do
                before do
                  Token.stubs(:generate).returns("foo")
                  @auth = @account.remote_authentications.create!
                  @auth.auth_mode = RemoteAuthentication::SAML
                  @auth.save!

                  get :show
                end

                it 'returns built jwt record with alt_shared_secret' do
                  assert_equal expected_jwt_response, actual_remote_auth_response['jwt']
                end

                it 'returns saml record' do
                  refute_nil actual_remote_auth_response['saml']
                end
              end
            end
          end
        end
      end

      # we are doing updates here on settings that are support specific. Hence, not using a shell account without support fixture is ok here.
      [:multiproduct, :minimum].each do |account_type|
        as_a_subsystem_user(user: subsystem_user, account: account_type) do
          before do
            Account.any_instance.stubs(:has_ip_restriction?).returns(true)
            Zendesk::StaffClient.any_instance.stubs(:admin?).with(@account.owner.id).returns(true)
          end

          describe "for user #{subsystem_user}" do
            describe '#update' do
              let (:actual) { JSON.parse(response.body)['security_settings'] }

              describe 'sending a long remote_login_url' do
                before do
                  Account.any_instance.stubs(:has_saml?).returns(true)
                  params = {
                      remote_authentications: {
                        saml: {
                          is_active: true,
                          remote_login_url: "https://#{"long" * 2048}.com",
                        }
                      },
                      requester_id: @account.owner.id
                    }

                  put :update, params: params
                  @errors = JSON.parse(response.body)['errors']
                  @account.reload
                end

                it 'adds the correct error' do
                  assert_response :unprocessable_entity
                  assert_equal I18n.t("txt.admin.models.remote_authentication.remote_login_url_is_too_long_v4",
                    char_limit: RemoteAuthentication::MAXIMUM_URL_LENGTH), @errors["remote_authentications"]["saml"]["remote_login_url"]
                end
              end

              describe 'when both saml and jwt are active' do
                before do
                  Account.any_instance.stubs(:has_saml?).returns(true)
                  Account.any_instance.stubs(:has_jwt?).returns(true)

                  @auth = @account.remote_authentications.new(
                    next_token: SecureRandom.hex(20),
                    remote_login_url: 'https://jwt-exists-login.com',
                    is_active: true,
                    priority: '1'
                  )
                  @auth.auth_mode = RemoteAuthentication::JWT
                  @auth.save!

                  @auth = @account.remote_authentications.new(
                    fingerprint: '55:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C',
                    remote_login_url: 'https://saml-exists-login.com',
                    is_active: true,
                    priority: '0'
                  )
                  @auth.auth_mode = RemoteAuthentication::SAML
                  @auth.save!
                end

                describe 'saml is de-activated' do
                  before do
                    params = {
                      remote_authentications: {
                        saml: {
                          is_active: false,
                          remote_login_url: 'https://www.saml-login-url.co.in',
                          fingerprint: '88:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C',
                          priority: '0'
                        }
                      },
                      requester_id: @account.owner.id
                    }

                    put :update, params: params

                    @account.reload
                  end

                  it('responds with success') { assert_response :success }
                  it('updates saml priority') { assert_equal 1, @account.remote_authentications.saml.first.priority }
                  it('updates jwt priority') { assert_equal 0, @account.remote_authentications.jwt.first.priority }
                  it('deactivates saml correctly') { assert_equal false, @account.remote_authentications.saml.first.is_active }
                  it('keeps jwt active') { assert(@account.remote_authentications.jwt.first.is_active) }
                end
              end

              describe 'general case' do
                before do
                  Account.any_instance.stubs(:has_saml?).returns(true)
                  Account.any_instance.stubs(:has_jwt?).returns(true)

                  assert_equal 2, @account.role_settings.agent_remote_bypass

                  params = {
                    admins_can_set_user_passwords: true,
                    email_agent_when_sensitive_fields_changed: true,
                    assumption_duration: 'always',
                    authentication: {
                      agent: {
                        security_policy_id: '300',
                        remote_bypass: '1'
                      },
                      end_user: {
                        security_policy_id: '300',
                      }
                    },
                    ip: {
                      ip_ranges: '128.0.0.1',
                      ip_restriction_enabled: true,
                      enable_agent_ip_restrictions: true,
                    },
                    remote_authentications: {
                      saml: {
                        is_active: true,
                        remote_login_url: 'https://www.saml-login-url.co.in',
                        fingerprint: '88:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C',
                        priority: '1'
                      },
                      jwt: {
                        is_active: true,
                        remote_login_url: 'https://www.jwt-login-url.co.in',
                        shared_secret: 'uVyPVcsmNiFkTAYWEe4p14bzOiGyAxOuPwMCvVRKie6p3LPI',
                        priority: '0'
                      }
                    },
                    requester_id: @account.owner.id
                  }
                  put :update, params: params

                  @account.reload
                end

                it('responds with success') { assert_response :success }
                it('responds with payload') { assert(actual['admins_can_set_user_passwords']) }

                it('changes admins_can_set_user_passwords') { assert(current_account.admins_can_set_user_passwords) }
                it('changes email_agent_when_sensitive_fields_changed') { assert(current_account.email_agent_when_sensitive_fields_changed) }
                it('changes the ip ranges') { assert_equal '128.0.0.1', current_account.ip_ranges }
                it('changes ip_restriction_enabled') { assert(current_account.settings.ip_restriction_enabled?) }
                it('changes enable_agent_ip_restrictions') { assert(current_account.settings.enable_agent_ip_restrictions?) }
                it('changes assumption_expiration') { assert(current_account.always_assumable?) }
                it('changes agent agent_remote_bypass') { assert_equal 1, current_account.role_settings.agent_remote_bypass }
                it('changes end_user security_policy_id') { assert_equal 300, current_account.role_settings.end_user_security_policy_id }

                it('changes agent security_policy_id') { assert_equal 300, current_account.role_settings.agent_security_policy_id }
                it('creates correct custom_security_policy') { assert_equal Zendesk::SecurityPolicy::High.password_length, current_account.custom_security_policy.password_length }

                it('changes saml is_active') { assert(current_account.remote_authentications.saml.first.is_active) }
                it('changes saml remote_login_url') { assert_equal 'https://www.saml-login-url.co.in', @account.remote_authentications.saml.first.remote_login_url }
                it('changes saml fingerprint') { assert_equal '88:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C', @account.remote_authentications.saml.first.token }
                it('changes saml priority') { assert_equal 1, @account.remote_authentications.saml.first.priority }

                it('changes jwt is_active') { assert(current_account.remote_authentications.jwt.first.is_active) }
                it('changes jwt remote_login_url') { assert_equal 'https://www.jwt-login-url.co.in', @account.remote_authentications.jwt.first.remote_login_url }
                it('changes jwt shared_secret') { assert_equal 'uVyPVcsmNiFkTAYWEe4p14bzOiGyAxOuPwMCvVRKie6p3LPI', @account.remote_authentications.jwt.first.token }
                it('changes jwt priority') { assert_equal 0, @account.remote_authentications.jwt.first.priority }
              end

              describe 'when all remote authentication active (i.e saml, jwt configurations AND agent/end-user remote login)' do
                before do
                  Account.any_instance.stubs(:has_saml?).returns(true)
                  Account.any_instance.stubs(:has_jwt?).returns(true)

                  @auth = @account.remote_authentications.new(
                    next_token: SecureRandom.hex(20),
                    remote_login_url: 'https://jwt-exists-login.com',
                    is_active: true,
                    priority: '1'
                  )
                  @auth.auth_mode = RemoteAuthentication::JWT
                  @auth.save!

                  @auth = @account.remote_authentications.new(
                    fingerprint: '55:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C',
                    remote_login_url: 'https://saml-exists-login.com',
                    is_active: true,
                    priority: '0'
                  )
                  @auth.auth_mode = RemoteAuthentication::SAML
                  @auth.save!

                  @account.role_settings.update_attributes!(end_user_zendesk_login: false, end_user_remote_login: true)
                  @account.role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_login: true)
                  @account.reload
                end

                describe 'deactivating both saml and jwt configurations' do
                  before do
                    params = {
                      remote_authentications: {
                        saml: {
                          is_active: false
                        },
                        jwt: {
                          is_active: false
                        }
                      },
                      requester_id: @account.owner.id
                    }

                    put :update, params: params

                    @errors = JSON.parse(response.body)['errors']

                    @account.reload
                  end

                  it 'adds the correct error' do
                    assert_response :unprocessable_entity
                    assert_equal "Cannot disable all remote auths with agent/end-user remote login enabled", @errors["remote_authentications"]["saml"]["is_active"]
                    assert_equal "Cannot disable all remote auths with agent/end-user remote login enabled", @errors["remote_authentications"]["jwt"]["is_active"]
                  end

                  it 'does not set remote auth configurations to false' do
                    assert @account.remote_authentications.saml.first.is_active
                    assert @account.remote_authentications.jwt.first.is_active
                  end
                end

                describe 'deactivating only one remote auth configuration' do
                  before do
                    params = {
                      remote_authentications: {
                        saml: {
                          is_active: false
                        }
                      },
                      requester_id: @account.owner.id
                    }

                    put :update, params: params

                    @account.reload
                  end

                  it 'sets saml to false' do
                    assert_response :success
                    refute @account.remote_authentications.saml.first.is_active
                  end
                end
              end

              describe 'with invalid parameters' do
                before do
                  params = {
                    authentication: {
                      agent: {
                        google_login: true,
                        office_365_login: true
                      }
                    },
                    requester_id: @account.owner.id
                  }
                  put :update, params: params
                end

                it('responds with error') do
                  assert_response :unprocessable_entity
                end
              end

              describe 'session_timeout' do
                before do
                  CustomSecurityPolicy.build_from_current_policy(current_account).save

                  params = { session_timeout: '20160', requester_id: @account.owner.id }
                  put :update, params: params
                end

                it('responds with success') { assert_response :success }
                it('responds with payload') { assert_equal '20160', actual['session_timeout'] }

                it('changes the session timeout for the account settings') { assert_equal 20160, current_account.settings.session_timeout }
              end

              describe 'custom security policy is already set' do
                before do
                  @account.role_settings.update_attribute(:agent_security_policy_id, 400)
                  CustomSecurityPolicy.build_from_current_policy(current_account).save

                  assert_equal 400, @account.role_settings.agent_security_policy_id
                  assert @account.custom_security_policy
                end

                describe 'updating two_factor_enforce' do
                  before do
                    refute @account.settings.two_factor_enforce
                    params = {
                      authentication: {
                        two_factor_enforce: true,
                      },
                      requester_id: @account.owner.id
                    }
                    put :update, params: params

                    @account.reload
                  end

                  it 'updates it correctly' do
                    assert_response :success
                    assert @account.settings.two_factor_enforce
                  end
                end

                describe 'updating email_agent_when_sensitive_fields_changed' do
                  before do
                    assert @account.email_agent_when_sensitive_fields_changed
                    params = {
                      email_agent_when_sensitive_fields_changed: false,
                      requester_id: @account.owner.id
                    }
                    put :update, params: params

                    @account.reload
                  end

                  it 'updates it correctly' do
                    assert_response :success
                    refute @account.email_agent_when_sensitive_fields_changed
                  end
                end

                describe 'updating end user security policy id' do
                  before do
                    params = {
                      authentication: {
                        end_user: {
                          security_policy_id: '200',
                        }
                      },
                      requester_id: @account.owner.id
                    }
                    put :update, params: params

                    @account.reload
                  end

                  it 'updates it correctly' do
                    assert_response :success
                    assert_equal 200, @account.role_settings.end_user_security_policy_id
                  end
                end
              end

              describe 'custom password' do
                before do
                  assert_nil @account.custom_security_policy

                  params = {
                    authentication: {
                      agent: {
                        security_policy_id: '400',
                        password: {
                          password_history_length: '7',
                          password_length: '5',
                          password_complexity: '2',
                          password_in_mixed_case: false,
                          failed_attempts_allowed: '9',
                          max_sequence: '5',
                          disallow_local_part_from_email: true,
                          password_duration: '180'
                        }
                      }
                    },
                    requester_id: @account.owner.id
                  }

                  put :update, params: params

                  @account.reload
                end

                it('responds with success') { assert_response :success }
                it('responds with payload') { assert_equal '400', actual['authentication']['agent']['security_policy_id'] }

                it('changes agent security_policy_id') { assert_equal 400, current_account.role_settings.agent_security_policy_id }

                it('changes the password_history_length') { assert_equal 7, @account.custom_security_policy.password_history_length }
                it('changes the password_length') { assert_equal 5, @account.custom_security_policy.password_length }
                it('changes the password_complexity') { assert_equal 2, @account.custom_security_policy.password_complexity }
                it('changes the password_in_mixed_case') { assert_equal false, @account.custom_security_policy.password_in_mixed_case }
                it('changes the failed_attempts_allowed') { assert_equal 9, @account.custom_security_policy.failed_attempts_allowed }
                it('changes the max_sequence') { assert_equal 5, @account.custom_security_policy.max_sequence }
                it('changes the disallow_local_part_from_email') { assert(@account.custom_security_policy.disallow_local_part_from_email) }
                it('changes the password_duration') { assert_equal 180, @account.custom_security_policy.password_duration }
              end

              describe 'with a requester that is an owner who is not an admin' do
                before do
                  Zendesk::StaffClient.any_instance.stubs(:admin?).with(@account.owner.id).returns(false)
                  @params = {
                    authentication: {
                      agent: {
                        google_login: true,
                        zendesk_login: false
                      }
                    },
                    requester_id: @account.owner.id
                  }
                  put :update, params: @params
                end

                it 'succeeds' do
                  assert_response 200
                end
              end

              describe 'with a requester that is not the owner' do
                before do
                  @requester = @account.agents.last
                  @params = {
                    authentication: {
                      agent: {
                        google_login: true,
                        zendesk_login: false
                      }
                    },
                    requester_id: @requester.id
                  }

                  Zendesk::StaffClient.any_instance.unstub(:admin?)
                end

                describe 'who is an admin in at least one product' do
                  before do
                    assert_equal 0, CIA::Event.for_account(@account).count
                    Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).with(@requester.id).returns("chat" => "admin", "support" => 'agent')
                    put :update, params: @params
                  end

                  it 'succeeds' do
                    assert_response 200
                  end

                  it 'sets requester' do
                    assert_equal @requester, @controller.send(:requester)
                  end

                  it 'generates CIA events with the requester as actor' do
                    events = CIA::Event.for_account(@account)
                    refute events.count == 0

                    events.each do |event|
                      assert_equal @requester, event.actor
                    end
                  end
                end

                describe 'when trying to change an owner-only setting requesting as an admin who is not owner' do
                  before do
                    @params[:admins_can_set_user_passwords] = true
                    Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).with(@requester.id).returns("chat" => "admin", "support" => nil)
                    put :update, params: @params
                  end

                  it 'fails' do
                    assert_response 403
                  end

                  it 'returns an InvalidRequesterId error' do
                    assert_equal 'InvalidRequesterId', JSON.parse(response.body)['error']
                  end

                  it 'returns an error description' do
                    assert_equal 'Requester is not owner, and is trying to change an owner-only setting: admins_can_set_user_passwords', JSON.parse(response.body)['description']
                  end
                end

                describe 'who is not an admin in any product' do
                  before do
                    Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).with(@requester.id).returns("chat" => nil, "support" => nil)

                    put :update, params: @params
                  end

                  it 'fails' do
                    assert_response 403
                  end

                  it 'returns an InvalidRequesterId error' do
                    assert_equal 'InvalidRequesterId', JSON.parse(response.body)['error']
                  end

                  it 'returns an error description' do
                    assert_equal 'Requester does not have entitlements to update security settings', JSON.parse(response.body)['description']
                  end
                end

                describe 'with a non-existing requester' do
                  before do
                    params = {
                      requester_id: 424242,
                      authentication: {
                        agent: {
                          google_login: true,
                          zendesk_login: false
                        }
                      }
                    }

                    put :update, params: params
                  end

                  it 'fails' do
                    assert_response 403
                  end

                  it 'returns an InvalidRequesterId error' do
                    assert_equal 'InvalidRequesterId', JSON.parse(response.body)['error']
                  end

                  it 'returns an error description' do
                    assert_equal 'Requester does not exist', JSON.parse(response.body)['description']
                  end
                end
              end

              describe 'causing errors' do
                describe 'updating agent to custom security_policy_id without any password customizations' do
                  let(:expected) do
                    {
                      "authentication" => {
                        "agent" => {
                          "security_policy_id" => "Cannot update to custom without any password customizations.",
                          "password" => {
                            "password_history_length"        => "Cannot update to custom without any password customizations.",
                            "password_length"                => "Cannot update to custom without any password customizations.",
                            "password_complexity"            => "Cannot update to custom without any password customizations.",
                            "password_in_mixed_case"         => "Cannot update to custom without any password customizations.",
                            "failed_attempts_allowed"        => "Cannot update to custom without any password customizations.",
                            "max_sequence"                   => "Cannot update to custom without any password customizations.",
                            "disallow_local_part_from_email" => "Cannot update to custom without any password customizations.",
                            "password_duration"              => "Cannot update to custom without any password customizations."
                          }
                        }
                      }
                    }
                  end

                  before do
                    assert_equal 100, @account.role_settings.agent_security_policy_id

                    params = {
                      authentication: {
                        agent: {
                          security_policy_id: '400',
                        }
                      },
                      requester_id: @account.owner.id
                    }

                    put :update, params: params
                    @errors = JSON.parse(response.body)['errors']

                    @account.reload
                  end

                  it 'causes errors on all password settings keys and security_policy_id' do
                    assert_equal expected, @errors
                  end

                  it 'does not update agent_security_policy_id' do
                    assert_equal 100, @account.role_settings.agent_security_policy_id
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
