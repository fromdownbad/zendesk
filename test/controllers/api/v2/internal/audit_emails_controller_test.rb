require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::AuditEmailsController do
  include MailTestHelper
  fixtures :all

  with_options(controller: 'api/v2/internal/audit_emails') do |request|
    request.should_route(:get, "/api/v2/internal/accounts/1/audits/123/email.json", action: "show", id: "123", account_id: '1', format: "json")
    request.should_route(:get, "/api/v2/internal/accounts/1/audits/123/email.eml",  action: "show", id: "123", account_id: '1', format: "eml")
    request.should_route(:get, "/api/v2/internal/accounts/1/audits/123/email.html", action: "show", id: "123", account_id: '1', format: "html")
  end

  before do
    @account = accounts(:minimum)
    @mail = FixtureHelper::Mail.read("minimum_ticket.json")
    @eml = FixtureHelper::Mail.raw("minimum_ticket.eml")
    @ticket = process_mail(@mail, account: @account, eml_fixture: @eml).ticket

    assert_instance_of Ticket, @ticket

    @comment = @ticket.comments.last
  end

  as_an_anonymous_user do
    it "is unauthorized" do
      get :show, params: { id: @comment.id, account_id: @account.id, ticket_id: @ticket.nice_id, format: "json" }
      assert_response :unauthorized
    end
  end

  as_an_end_user do
    it "is forbidden" do
      get :show, params: { id: @comment.id, account_id: @account.id, ticket_id: @ticket.nice_id, format: "json" }
      assert_response :forbidden
    end
  end

  as_an_agent do
    it "is forbidden" do
      get :show, params: { id: @comment.id, account_id: @account.id, ticket_id: @ticket.nice_id, format: "json" }
      assert_response :forbidden
    end
  end

  as_a_subsystem_user(account: :minimum) do
    describe "when requesting an audit email in JSON format" do
      it "responds with json" do
        get :show, params: { id: @comment.id, account_id: @account.id, ticket_id: @ticket.nice_id, format: "json" }

        assert_equal "application/json", @response.content_type
        assert JSON.parse(@response.body).is_a?(Hash)
      end
    end

    describe "when requesting an audit email in eml format" do
      it "responds with eml" do
        get :show, params: { id: @comment.id, account_id: @account.id, ticket_id: @ticket.nice_id, format: "eml" }

        assert_equal "message/rfc822", @response.content_type
        assert @response.body.include?("Message-ID: #{@mail.message_id}")
      end
    end

    describe "when requesting an audit email in text format" do
      it "responds with text" do
        get :show, params: { id: @comment.id, account_id: @account.id, ticket_id: @ticket.nice_id, format: "text" }

        assert_equal "text/plain", @response.content_type
        refute @response.body.include?("Message-ID: #{@mail.message_id}")
      end
    end

    describe "when requesting an audit email in html format" do
      it "responds with html" do
        get :show, params: { id: @comment.id, account_id: @account.id, ticket_id: @ticket.nice_id, format: "html" }

        assert_equal "text/html", @response.content_type
        assert_response :success
      end
    end

    it "responds with 404 with no email from storage returned" do
      Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:json).returns(nil)

      get :show, params: { id: @comment.id, account_id: @account.id, ticket_id: @ticket.nice_id, format: "html" }
      assert_equal "text/html", @response.content_type
      assert_response 404
    end

    it "responds with 410 for a deleted ticket" do
      @ticket.will_be_saved_by(@account.owner)
      @ticket.soft_delete!
      @comment.reload

      get :show, params: { id: @comment.id, account_id: @account.id, ticket_id: @ticket.nice_id, format: "html" }
      assert_equal "text/html", @response.content_type
      assert_response 410
    end

    it "returns data for an archived ticket" do
      archive_and_delete(@ticket)

      get :show, params: { id: @comment.id, account_id: @account.id, ticket_id: @ticket.nice_id, format: "html" }
      assert_equal "text/html", @response.content_type
      assert_response :success
    end

    it "returns data for a redacted ticket (no .eml file present)" do
      @ticket.comments.first.redact!("Microsoft Word")
      get :show, params: { id: @comment.id, account_id: @account.id, ticket_id: @ticket.nice_id, format: "html" }
      assert_equal "text/html", @response.content_type
      assert_response :success
      refute_match /<a href=\"#source\" class=\"\" data-title-value=\"Source\">Source<\/a>/, @response.body
    end
  end
end
