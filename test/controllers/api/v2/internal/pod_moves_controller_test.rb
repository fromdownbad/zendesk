require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::PodMovesController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
  end

  describe 'permission' do
    as_an_admin do
      should_be_forbidden [:put, :enable_feature]
      should_be_forbidden [:put, :disable_feature]
    end
  end

  describe 'routes' do
    with_options(controller: 'api/v2/internal/pod_moves') do |request|
      request.should_route :get, '/api/v2/internal/pod_moves/fetch_feature', action: 'fetch_feature'
      request.should_route :put, '/api/v2/internal/pod_moves/enable_feature', action: 'enable_feature'
      request.should_route :put, '/api/v2/internal/pod_moves/disable_feature', action: 'disable_feature'
    end
  end

  describe '#fetch_feature' do
    as_a_subsystem_user(user: 'pod_mover', account: :minimum) do
      let(:feature) { 'syncattachments_enabled' }

      it 'gets the state of syncattachments for the specified account' do
        account = accounts(:minimum)
        expected_response = { 'feature' => feature, 'feature_status' => Zendesk::ShardMover::Accounts::ENABLED_SETTING_VALUE}
        get :fetch_feature, params: { feature: feature, account_id: account.id.to_s, format: :json }
        assert_response :success
        assert_equal expected_response, JSON.parse(response.body)
      end

      it 'translates false to disabled in the api response' do
        account = accounts(:minimum)
        account.settings.set(feature.to_sym => Zendesk::ShardMover::Accounts::DISABLED_SETTING_VALUE)
        account.save!(validate: false)
        expected_response = { 'feature' => feature, 'feature_status' => Zendesk::ShardMover::Accounts::DISABLED_API_VALUE }
        get :fetch_feature, params: { feature: feature, account_id: account.id.to_s, format: :json }
        assert_response :success
        assert_equal expected_response, JSON.parse(response.body)
      end

      it 'returns 400 for invalid feature' do
        account = accounts(:minimum)
        get :fetch_feature, params: { feature: 'bogus_feature_key', account_id: account.id.to_s, format: :json }

        assert_response 400
      end
    end
  end

  describe '#enable_feature' do
    as_a_subsystem_user(user: 'pod_mover', account: :minimum) do
      it 'enables syncattachments for the specified account' do
        account = accounts(:minimum)
        put :enable_feature, params: { feature: 'syncattachments_enabled', account_id: account.id.to_s, format: :json }
        assert_response :success
      end

      it 'enables write to archiver for the specified account' do
        account = accounts(:minimum)
        put :enable_feature, params: { feature: 'write_to_archive_v2_enabled', account_id: account.id.to_s, format: :json }
        assert_response :success
      end

      it 'returns 400 for invalid feature' do
        account = accounts(:minimum)
        put :enable_feature, params: { feature: 'bogus_feature_key', account_id: account.id.to_s, format: :json }

        assert_response 400
      end
    end
  end

  describe '#disable_feature' do
    as_a_subsystem_user(user: 'pod_mover', account: :minimum) do
      it 'disables syncattachments for the specified accounts' do
        account = accounts(:minimum)

        put :disable_feature, params: { feature: 'syncattachments_enabled', account_id: account.id.to_s, format: :json }
        account.reload

        assert_response :success
        assert_equal({feature: 'syncattachments_enabled', feature_status: account.settings.syncattachments_enabled}.to_json, response.body)

        account.settings.syncattachments_enabled = false
        account.reload

        put :disable_feature, params: { feature: 'syncattachments_enabled', account_id: account.id.to_s, format: :json }
        assert_response :success
        assert_equal({feature: 'syncattachments_enabled', feature_status: account.settings.syncattachments_enabled}.to_json, response.body)
      end

      it 'disables archiver for the specified accounts' do
        account = accounts(:minimum)

        put :disable_feature, params: { feature: 'write_to_archive_v2_enabled', account_id: account.id.to_s, format: :json }
        account.reload

        assert_response :success
        assert_equal({feature: 'write_to_archive_v2_enabled', feature_status: account.settings.write_to_archive_v2_enabled}.to_json, response.body)

        account.settings.syncattachments_enabled = false
        account.reload

        put :disable_feature, params: { feature: 'write_to_archive_v2_enabled', account_id: account.id.to_s, format: :json }
        assert_response :success
        assert_equal({feature: 'write_to_archive_v2_enabled', feature_status: account.settings.write_to_archive_v2_enabled}.to_json, response.body)
      end

      it 'returns 400 for non valid feature' do
        account = accounts(:minimum)

        put :disable_feature, params: { feature: 'bogus_feature_key', account_id: account.id.to_s, format: :json }

        assert_response 400
      end
    end
  end
end
