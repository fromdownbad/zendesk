require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::SecondarySubscriptionsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:active_temporary_zendesk_agents) do
    {
      zuora_rate_plan_id: '4567890',
      name:               'temporary_zendesk_agents',
      quantity:           1,
      starts_at:          5.days.ago,
      expires_at:         2.months.from_now,
      status_id:          ::SubscriptionFeatureAddon::STATUS[:started]
    }
  end

  let(:expired_in_grace_period_temporary_zendesk_agents) do
    {
      zuora_rate_plan_id: '4567890',
      name:               'temporary_zendesk_agents',
      quantity:           1,
      starts_at:          3.month.ago,
      expires_at:         5.days.ago,
      status_id:          ::SubscriptionFeatureAddon::STATUS[:expired]
    }
  end

  before do
    accept :json
  end

  with_options(controller: 'api/v2/internal/secondary_subscriptions') do |req|
    req.should_route :get, '/api/v2/internal/secondary_subscriptions', action: 'index'
  end

  as_a_subsystem_user(account: :minimum) do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user)    { @controller.send(:current_user) }

    describe 'when there are no active secondary subscriptions' do
      describe '#index' do
        before { get :index }

        it('responds with success') { assert_response :success }
      end
    end

    describe 'when there are active secondary subscriptions' do
      before do
        current_account.subscription_feature_addons.create(
          [
            active_temporary_zendesk_agents,
            expired_in_grace_period_temporary_zendesk_agents
          ]
        )
      end

      describe '#index' do
        before { get :index }

        it('responds with success') { assert_response :success }

        it 'lists all active and in grace period secondary subscriptions for an account' do
          assert_equal 2, JSON.load(@response.body)['secondary_subscriptions'].size
        end
      end
    end
  end
end
