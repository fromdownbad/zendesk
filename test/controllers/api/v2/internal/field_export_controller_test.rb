require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 3

describe Api::V2::Internal::FieldExportController do
  fixtures :accounts, :ticket_fields, :cf_fields

  before do
    @request.account = accounts(:minimum)
  end

  as_a_subsystem_user account: :minimum do
    before do
      @controller.stubs(:current_registered_user).returns(Zendesk::SystemUser.find_subsystem_user_by_name('zendesk'))
    end

    describe '#ticket_fields' do
      it 'only return fields included in the incremental export' do
        get :ticket_fields, format: :json
        field_types = JSON.parse(@response.body)['ticket_fields'].map { |f| f['type'] }

        refute_includes field_types, 'FieldDescription'
      end
    end

    describe '#update_exportable_fields' do
      it 'updates ticket fields' do
        field = ticket_fields(:field_integer_custom)
        post :update_exportable_fields, params: { format: :json, type: 'ticket', id_value_map: { field.id.to_s => 'false' } }

        assert_response :success
        refute field.reload.is_exportable
      end

      it 'updates user fields' do
        field = cf_fields(:integer1)
        post :update_exportable_fields, params: { format: :json, type: 'user', id_value_map: { field.id.to_s => 'false' } }

        assert_response :success
        refute field.reload.is_exportable
      end

      it 'updates organization fields' do
        field = cf_fields(:org_integer1)
        post :update_exportable_fields, params: { format: :json, type: 'organization', id_value_map: { field.id.to_s => 'false' } }

        assert_response :success
        refute field.reload.is_exportable
      end
    end
  end
end
