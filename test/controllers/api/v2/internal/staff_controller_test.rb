require_relative '../../../../support/test_helper'
require_relative '../../../../support/suite_test_helper'
require_relative "../../../../support/multiproduct_test_helper"

SingleCov.covered!

describe Api::V2::Internal::StaffController do
  extend Api::V2::TestHelper
  include MultiproductTestHelper
  include SuiteTestHelper

  fixtures :all

  describe 'staff on shell account with support' do
    let(:shell_account_with_support) { accounts(:multiproduct) }
    let(:presenter) { Api::V2::Internal::StaffPresenter.new(shell_account_with_support.owner, url_builder: self) }

    before do
      accept :json
      @request.account = shell_account_with_support
    end

    as_an_admin do
      should_be_forbidden :create, :update, :destroy, :owner_welcome
    end

    as_an_end_user do
      should_be_forbidden :create, :update, :destroy, :owner_welcome
    end

    as_an_agent do
      should_be_forbidden :create, :update, :destroy, :owner_welcome
    end

    as_a_subsystem_user(user: 'account_service', account: :multiproduct) do
      should_be_forbidden :create, :update, :destroy, :owner_welcome
    end

    as_a_subsystem_user(user: 'metropolis', account: :multiproduct) do
      let(:requester_id) { FactoryBot.create(:user, account: shell_account_with_support, roles: Role::END_USER.id).id }

      describe 'POST #create' do
        describe 'with non multiproduct account' do
          let(:non_cp4_account) { accounts(:minimum) }
          let(:new_staff_email) { 'the.rod@example.com' }
          let(:user_payload) { { name: 'Rodney', email: new_staff_email } }
          let(:requester_id) { FactoryBot.create(:user, account: non_cp4_account, roles: Role::END_USER.id).id }

          before do
            @request.account = non_cp4_account
          end

          describe 'and account has contributor role' do
            before do
              PermissionSet.enable_contributor!(non_cp4_account)
              Timecop.freeze
              post :create, params: { user: user_payload, requester_id: requester_id }
            end

            it 'creates a new agent with Contributor role' do
              assert_response 201
              user = non_cp4_account.users.last
              expected = presenter.present(user)[:user]
              actual = JSON.parse(@response.body)['user'].with_indifferent_access
              expected.each do |key, v|
                if v.nil?
                  assert_nil actual[key]
                else
                  assert_equal v, actual[key]
                end
              end

              assert_equal ::Role::AGENT.id, user.roles
              assert_equal ::PermissionSet::Type::CONTRIBUTOR, user.permission_set.role_type
            end

            describe 'when a custom created_at is passed in' do
              let(:custom_datetime) { "2012-12-12T20:45:18Z" }
              let(:user_payload) { { name: 'Rodney', email: new_staff_email, custom_created_at: custom_datetime} }

              it 'creates a new active staff user with custom created_at' do
                assert_response 201
                user = non_cp4_account.users.last
                expected = presenter.present(user)[:user].merge(created_at: custom_datetime)
                actual = JSON.parse(@response.body)['user'].with_indifferent_access
                expected.each do |key, v|
                  if v.nil?
                    assert_nil actual[key]
                  else
                    assert_equal v, actual[key]
                  end
                end
              end
            end
          end

          it 'returns a 422 with MultiproductMissing error' do
            post :create, params: { user: user_payload, requester_id: requester_id }
            assert_response 422
            body = JSON.parse(@response.body)
            assert_equal 'MultiproductOrContributorRoleMissing', body['error']
            assert_equal "Account must have `multiproduct` set to true or have contributor role enabled", body['description']
          end
        end

        describe 'with multiproduct account' do
          let(:user_payload) { { name: 'Rodney', email: new_staff_email } }
          let(:new_staff_email) { 'the.rod@example.com' }

          it 'returns a 400 with MissingRequesterId error when missing the requester_id' do
            post :create, params: { id: shell_account_with_support.owner_id, user: user_payload }
            assert_response 400
            body = JSON.parse(@response.body)
            assert_equal 'MissingRequesterId', body['error']
            assert_equal "'requester_id' is a required field", body['description']
          end

          describe 'with correct parameters' do
            let(:user) { shell_account_with_support.users.order(:created_at).last }

            before do
              Timecop.freeze
            end

            it 'creates a new active staff user' do
              post :create, params: { user: user_payload, requester_id: requester_id }
              assert_response 201
              expected = presenter.present(user)[:user]
              actual = JSON.parse(@response.body)['user'].with_indifferent_access
              expected.each do |key, v|
                if v.nil?
                  assert_nil actual[key]
                else
                  assert_equal v, actual[key]
                end
              end
            end

            it 'creates the new user as an agent' do
              post :create, params: { user: user_payload, requester_id: requester_id }
              assert_equal ::Role::AGENT.id, user.roles
            end

            it 'creates the new user as a Contributor' do
              post :create, params: { user: user_payload, requester_id: requester_id }
              assert_equal ::PermissionSet::Type::CONTRIBUTOR, user.permission_set.role_type
            end

            it 'runs guide entitlement sync for this user' do
              Omnichannel::GuideEntitlementSyncJob.expects(:enqueue)
              post :create, params: { user: user_payload, requester_id: requester_id }
            end
          end

          describe 'with validation error: contributor already defined' do
            let(:user) { shell_account_with_support.users.order(:created_at).last }

            let(:permission_set) do
              PermissionSet.new(
                account: @account,
                role_type: PermissionSet::Type::CONTRIBUTOR,
                name: I18n.t('txt.default.roles.contributor.name'),
                description: I18n.t('txt.default.roles.contributor.description')
              )
            end

            let(:validation_error) do
              permission_set.errors.add(:base, I18n.t('txt.default.roles.contributor.error.already_defined'))
              ActiveRecord::RecordInvalid.new(permission_set)
            end

            let(:contributor) do
              @account.permission_sets.where(role_type: PermissionSet::Type::CONTRIBUTOR).first
            end

            before do
              Timecop.freeze
              PermissionSet.stubs(:find_contributor).returns(nil).then.returns(contributor)
              PermissionSet.stubs(:create).with(any_parameters).raises(validation_error)

              post :create, params: { user: user_payload, requester_id: requester_id }
            end

            it 'rescues exception and returns 201' do
              assert_response 201
            end

            it 'returns the user with Contributor role' do
              assert_equal ::PermissionSet::Type::CONTRIBUTOR, user.permission_set.role_type
            end
          end

          describe 'with custom created_at' do
            before do
              Timecop.freeze
              post :create, params: { user: user_payload, requester_id: requester_id }
            end

            describe 'with a date in the past' do
              let(:user) { shell_account_with_support.users.order(:updated_at).last }
              let(:custom_datetime) { "2012-12-12T20:45:18Z" }
              let(:user_payload) { { name: 'Rodney', email: new_staff_email, custom_created_at: custom_datetime} }

              it 'creates a new active staff user with custom created_at' do
                assert_response 201
                expected = presenter.present(user)[:user].merge(created_at: custom_datetime)
                actual = JSON.parse(@response.body)['user'].with_indifferent_access
                expected.each do |key, v|
                  if v.nil?
                    assert_nil actual[key]
                  else
                    assert_equal v, actual[key]
                  end
                end
              end
            end

            describe 'with a date in the future is passed in' do
              let(:user) { shell_account_with_support.users.order(:updated_at).last }
              let(:custom_datetime) { "3012-12-12T20:45:18Z" }
              let(:user_payload) { { name: 'Rodney', email: new_staff_email, custom_created_at: custom_datetime} }

              it 'returns a 422 with validation error' do
                body = JSON.parse(response.body)
                assert_response 422
                assert_equal 'CustomCreatedAtInvalid', body['error']
                assert_equal "custom_created_at must be in the future", body['description']
              end
            end
          end

          describe 'when email in payload is already used by an agent' do
            let(:new_staff_email) { users(:multiproduct_support_agent).email }

            it 'returns a 422 with validation error' do
              post :create, params: { user: user_payload, requester_id: requester_id }
              body = JSON.parse(response.body)
              assert_response 422
              assert_equal 'RecordInvalid', body['error']
              assert_equal 'DuplicateValue', body['details']['email'].first['error']
            end
          end

          describe 'when email in payload is already used by an end user' do
            let(:end_user) { users(:multiproduct_end_user) }
            let(:new_staff_email) { end_user.email }
            before { post :create, params: { user: user_payload, requester_id: requester_id } }

            it 'returns 201' do
              assert_response 201
            end

            it 'promotes end user to Contributor' do
              user_body = JSON.parse(@response.body)['user']
              assert_equal user_body['roles'], 4
              assert_equal ::PermissionSet::Type::CONTRIBUTOR, end_user.reload.permission_set.role_type
            end
          end
        end
      end

      describe 'PUT update' do
        let(:updated_locale) { TranslationLocale.find_by_locale('es').id }
        let(:updated_time_zone) { 'Europe/London' }
        let(:updated_name) { 'Stretch' }
        let(:user_payload) do
          { name: updated_name, locale_id: updated_locale, time_zone: updated_time_zone }
        end
        let(:has_time_zone_selection) { true }

        describe 'when missing the requester_id' do
          it 'returns a 400 with MissingRequesterId error' do
            put :update, params: { id: shell_account_with_support.owner_id, user: user_payload }
            assert_response 400
            body = JSON.parse(@response.body)
            assert_equal 'MissingRequesterId', body['error']
            assert_equal "'requester_id' is a required field", body['description']
          end
        end

        describe 'when there is a requester_id' do
          before do
            Account.any_instance.stubs(has_time_zone_selection?: has_time_zone_selection)
            put :update, params: { id: shell_account_with_support.owner_id, user: user_payload, requester_id: requester_id }
          end

          describe 'when the account has time zone selection enabled' do
            it 'updates the user, including the time_zone' do
              assert_response 200
              response = JSON.parse(@response.body)
              assert_equal updated_name, response['user']['name']
              assert_equal updated_time_zone, response['user']['time_zone']
              assert_equal updated_locale, response['user']['locale_id']
            end
          end

          describe 'when the shell_account has time zone selection disabled' do
            let(:has_time_zone_selection) { false }

            it 'updates the user, but not the time_zone field' do
              assert_response 200
              response = JSON.parse(@response.body)
              assert_equal updated_name, response['user']['name']
              assert_not_equal updated_time_zone, response['user']['time_zone']
              assert_equal updated_locale, response['user']['locale_id']
            end
          end
        end
      end

      describe 'DELETE destroy' do
        let(:user_to_delete) { FactoryBot.create(:user, account: shell_account_with_support) }

        describe 'when deleting a user' do
          before do
            ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
            delete :destroy, params: { id: user_to_delete.id, requester_id: requester_id }
          end

          it 'deletes the user and creates a compliance deletion record' do
            assert_response 200
            response = JSON.parse(@response.body)
            deletion_status = ComplianceDeletionStatus.where(account_id: shell_account_with_support.id, user_id: user_to_delete.id, executer_id: requester_id).first

            assert_equal user_to_delete.id, response['user']['id']
            assert_empty shell_account_with_support.users.where(id: user_to_delete.id)
            assert deletion_status.present?
          end
        end

        describe 'when deleting an owner' do
          let(:user_to_delete) { shell_account_with_support.owner }

          it 'returns a 422' do
            delete :destroy, params: { id: user_to_delete.id, requester_id: requester_id }
            assert_response 422
            body = JSON.parse(@response.body)
            assert_equal 'CannotDeleteOwner', body['error']
            assert_equal 'This staff is an owner and cannot be deleted', body['description']
          end
        end
      end

      describe '#owner_welcome' do
        let(:user) { shell_account_with_support.users.first }

        let(:account_request) { { id: shell_account_with_support.id } }
        let(:user_request) { { id: user.id } }
        let(:user_wrong_account_request) { { id: users(:minimum_admin).id } }

        let(:enqueue) { ShellOwnerWelcomeEmailJob.stubs(:enqueue) }

        before do
          enqueue
        end

        it 'returns 200' do
          put :owner_welcome, params: { account: account_request, user: user_request, format: :json }
          assert_response :ok
        end

        it 'returns 200 and enqueues the welcome email job' do
          enqueue.with(account_id: shell_account_with_support.id, user_id: user.id).once
          put :owner_welcome, params: { account: account_request, user: user_request, format: :json }
          assert_response :ok
        end

        describe 'when welcome email job already enqueued' do
          before do
            Rails.cache.write("owner-welcome-job-queued/#{shell_account_with_support.id}", true)
          end

          it 'returns 200 without enqueueing job' do
            enqueue.with(account_id: shell_account_with_support.id, user_id: user.id).never
            put :owner_welcome, params: { account: account_request, user: user_request, format: :json }
            assert_response :ok
          end
        end

        it 'clears the current account cache' do
          Account.any_instance.expects(:clear_kasket_cache!).once
          put :owner_welcome, params: { account: account_request, user: user_request, format: :json }
          assert_response :ok
        end

        describe 'when clearing the current account cache fails' do
          it 'returns 200' do
            Account.any_instance.expects(:clear_kasket_cache!).raises(StandardError).once
            put :owner_welcome, params: { account: account_request, user: user_request, format: :json }
            assert_response :ok
          end
        end

        describe 'when the current account is nil' do
          before do
            @request.account = nil
          end

          it 'returns 200' do
            put :owner_welcome, params: { account: account_request, user: user_request, format: :json }
            assert_response :ok
          end

          it 'does not clear the current account cache' do
            Account.any_instance.expects(:clear_kasket_cache!).never
            put :owner_welcome, params: { account: account_request, user: user_request, format: :json }
            assert_response :ok
          end
        end
      end
    end
  end

  describe 'staff on shell account without support' do
    let!(:shell_account_without_support) { setup_shell_account }
    let(:presenter) { Api::V2::Internal::StaffPresenter.new(shell_account_without_support.owner, url_builder: self) }
    let(:requester_id) { FactoryBot.create(:user, account: shell_account_without_support, roles: Role::ADMIN.id).id }

    before do
      accept :json
      @account         = shell_account_without_support
      @request.account = @account
      system_user      = Zendesk::SystemUser.find_subsystem_user_by_name('metropolis')
      assert system_user, "No subsystem user found, bad zendesk.yml?"

      # The WardenStrategy in the zendesk_system_users gem sets the current account to the user account for subsystems
      system_user.account = @account

      @controller.stubs(:current_registered_user).returns(system_user)
    end

    describe 'POST #create' do
      let(:user_payload) { { name: 'Rodney', email: 'the.rod@example.org'} }

      describe 'with correct parameters' do
        let(:user) { shell_account_without_support.users.order(:created_at).last }

        before do
          Timecop.freeze
          post :create, params: { user: user_payload, requester_id: requester_id }
        end

        it 'creates a new active staff user' do
          assert_response 201
          expected = presenter.present(user)[:user]
          actual = JSON.parse(@response.body)['user'].with_indifferent_access
          expected.each do |key, v|
            if v.nil?
              assert_nil actual[key]
            else
              assert_equal v, actual[key]
            end
          end
        end

        it 'creates the new user as an agent' do
          assert_equal ::Role::AGENT.id, user.roles
        end

        it 'creates the new user as a Contributor' do
          assert_equal ::PermissionSet::Type::CONTRIBUTOR, user.permission_set.role_type
        end
      end
    end
  end
end
