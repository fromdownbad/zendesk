require_relative "../../../../support/test_helper"
SingleCov.covered! uncovered: 1

describe Api::V2::Internal::FraudScoresController do
  extend Api::V2::TestHelper

  fixtures :accounts

  before do
    accept :json
  end

  let(:actions) { { auto_suspend: true } }
  let(:account) { accounts(:minimum) }
  let(:fraud_score) { FactoryBot.create(:fraud_score, account: account) }
  let(:json) { JSON.parse(@response.body).fetch('fraud_score') }
  let(:first_json) { JSON.parse(@response.body).fetch('fraud_scores').first }

  with_options(controller: 'api/v2/internal/fraud_scores') do |request|
    request.should_route :get, '/api/v2/internal/fraud_scores.json', action: 'index', format: 'json'
    request.should_route :get, '/api/v2/internal/fraud_scores/1.json', action: 'show', id: 1, format: 'json'
  end

  as_a_subsystem_user(account: :minimum) do
    describe "set up steps" do
      before do
        @opts = { format: :json }
      end

      describe "#index" do
        before do
          fraud_score # Create a FraudScore record using the factory.
          get :index, params: @opts
        end

        it "responds with HTTP 200" do
          assert_response :ok
        end

        it "renders the fraud_score" do
          assert first_json
          assert_equal fraud_score.account_id,                    first_json["account_id"]
          assert_equal fraud_score.verified_fraud,                first_json["verified_fraud"]
          assert_equal fraud_score.account_fraud_service_release, first_json["account_fraud_service_release"]
        end
      end

      describe "#show action" do
        before do
          get :show, params: @opts.merge(id: fraud_score.id)
        end

        it "responds with HTTP 200" do
          assert_response :ok
        end

        it "renders the fraud_score" do
          assert_equal fraud_score.account_id,                    json["account_id"]
          assert_equal fraud_score.verified_fraud,                json["verified_fraud"]
          assert_equal fraud_score.account_fraud_service_release, json["account_fraud_service_release"]
        end
      end

      describe "#create action" do
        describe "when it is a trial_signup source_event" do
          before do
            @data = {
              account_id: account.id,
              verified_fraud: true,
              subdomain: account.subdomain,
              actions: {
                auto_suspend: true,
              },
              source_event: Fraud::SourceEvent::TRIAL_SIGNUP.to_s,
              account_fraud_service_release: "1"
            }
          end

          describe "when it succeeds" do
            describe "with a response from AFS" do
              before do
                refute FraudScore.find_by_account_id(@data[:account_id])
                post :create, params: @opts.merge(fraud_score: @data)
              end

              it "responds with HTTP 200" do
                assert_response :created
              end

              it "creates the fraud_score" do
                new_fraud_score = FraudScore.find_by_account_id(@data[:account_id])
                assert_equal account.id, new_fraud_score.account_id
              end

              it "renders the created FraudScore" do
                assert_equal account.owner.email, json["owner_email"]
                assert_equal account.subdomain, json["subdomain"]
                assert_equal Fraud::SourceEvent::TRIAL_SIGNUP.to_s, json["source_event"]
              end
            end
          end

          describe "when it fails" do
            before do
              # Force a failure to test controller error presenter use.
              FraudScore.any_instance.stubs(:valid?).returns(false)
              post :create, params: @opts.merge(fraud_score: @data)
            end

            it "responds with HTTP 500" do
              assert_response :internal_server_error
            end

            it "does not create the FraudScore" do
              refute FraudScore.find_by_account_id(@data[:account_id])
            end
          end
        end

        describe "when it is another source_event" do
          describe_with_arturo_enabled :enable_captcha_via_afs do
            before do
              @data = {
                account_id: account.id,
                verified_fraud: false,
                subdomain: account.subdomain,
                is_whitelisted: false,
                source_event: Fraud::SourceEvent::SPAMMY_INBOUND_TICKET_ACTIVITY.to_s,
                actions: {
                  "enable_captcha": true
                },
                account_fraud_service_release: "1"
              }
              refute FraudScore.find_by_account_id(@data[:account_id])
              post :create, params: @opts.merge(fraud_score: @data)
            end

            it "performs fraud actions and auto-enables captcha for the account" do
              assert_response :created
              assert account.settings.captcha_required?
            end
          end
        end
      end

      describe "#update action" do
        before do
          @data = {
            verified_fraud: true,
            owner_email: 'test222@example.com',
            subdomain: 'hello'
          }
        end

        describe "when it succeeds" do
          before do
            assert_equal false, fraud_score.verified_fraud
            put :update, params: @opts.merge(id: fraud_score.id, fraud_score: @data)
          end

          it "responds with HTTP 200" do
            assert_response :ok
          end

          it "renders the updated fraud_score" do
            assert(json["verified_fraud"])
            assert_equal 'hello', json["subdomain"]
            assert_equal 'test222@example.com', json["owner_email"]
          end
        end

        describe "when it fails" do
          before do
            # Force a failure to test controller error presenter use.
            FraudScore.any_instance.stubs(:update_attributes).returns(false)
            put :update, params: @opts.merge(id: fraud_score.id, fraud_score: @data)
          end

          it "responds with HTTP 422" do
            assert_response :unprocessable_entity
          end

          it "does not create the FraudScore" do
            refute FraudScore.find_by_account_id(@data[:account_id])
          end
        end
      end

      describe "#vendor_review_prohibited" do
        it "responses with false by default" do
          get :vendor_review_prohibited
          assert_response :ok
          refute JSON.parse(@response.body)['vendor_review_prohibited']
        end

        it "responds with true if account.vendor_review_prohibited?" do
          account.stubs(:vendor_review_prohibited?).returns(true)
          get :vendor_review_prohibited
          assert_response :ok
          assert JSON.parse(@response.body)['vendor_review_prohibited']
        end
      end
    end
  end
end
