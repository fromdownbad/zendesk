require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Api::V2::Internal::DataDeletionAuditsController do
  extend Api::V2::TestHelper
  fixtures :accounts, :subscriptions

  before do
    # Accept requests to https://pod-X.zendesk.com
    request.env['zendesk.account'] = nil

    accept :json

    @account = accounts(:minimum)
    ActiveRecord::Base.connection.execute <<-MYSQL
    UPDATE `subscriptions`
    SET
      `manual_discount` = 0,
      `is_trial` = 0,
      `churned_on` = '#{130.days.ago.to_s(:db)}',
      `updated_at` = '#{Time.now.to_s(:db)}'
    WHERE
      `subscriptions`.`id` = #{@account.subscription.id}
    MYSQL
    @account.update_column(:deleted_at, 40.days.ago)
    Rails.cache.clear

    @audit = @account.data_deletion_audits.build_for_cancellation
    @audit.created_at = 40.days.ago
    @audit.save!

    Zendesk::Maintenance::Jobs::BaseDataDeleteJob.stubs(:enqueue_in).returns(true)
    Zendesk::DataDeletion::AccountDeletionManager.stubs(:enqueue_in)
  end

  with_options(controller: 'api/v2/internal/data_deletion_audits') do |request|
    request.should_route :get, '/api/v2/internal/accounts/1/data_deletion_audits/active.json', action: 'active', id: 1, format: 'json'
    request.should_route :get, '/api/v2/internal/data_deletion_audits/1.json', action: 'show', id: 1, format: 'json'
    request.should_route :get, '/api/v2/internal/data_deletion_audits.json', action: 'index', format: 'json'
    request.should_route :post, '/api/v2/internal/data_deletion_audits.json', action: 'create', format: :json
  end

  as_a_subsystem_user(account: :support) do
    describe 'a GET to :index' do
      let!(:audit_2) { accounts(:minimum).data_deletion_audits.create!(reason: 'moved') }

      it 'renders all account audits' do
        get :index, params: { id: accounts(:minimum) }
        json['data_deletion_audits'].length.must_equal 2
        json['data_deletion_audits'].first['id'].must_equal audit_2.id
        json['total_entries'].must_equal 2
      end
    end

    describe 'a GET to :active' do
      before do
        # Create a second audit to prove the pending audit isn't included
        @active = @audit.account.data_deletion_audits.build_for_cancellation
        @active.save!
        @active.start!
      end

      before { get :active, params: { id: @audit.account_id } }
      should_use_presenter Api::V2::Internal::DataDeletionAuditPresenter, status: :ok

      it 'renders all account audits' do
        assert_equal 1, JSON.parse(@response.body)['data_deletion_audits'].size
        assert_equal @active.id, JSON.parse(@response.body)['data_deletion_audits'].first['id']
      end
    end

    describe 'a GET to :show' do
      before { get :show, params: { id: @audit.id } }
      should_use_presenter Api::V2::Internal::DataDeletionAuditPresenter, status: :ok

      it 'renders the correct audit' do
        assert_equal @audit.id, JSON.parse(@response.body)['data_deletion_audit']['id']
      end
    end

    describe 'a POST to :create' do
      let(:enqueue) { false }
      before do
        @audit_count = @account.data_deletion_audits.count(:all)
        Zendesk::DataDeletion::AccountDeletionManager.stubs(:enqueue).returns(true)
        post :create, params: { reason: 'moved', account_id: '1', shard_id: 1, enqueue: enqueue }
      end

      it('responds with ok') { assert_response :ok }

      it 'should create a new data_deletion_audit' do
        @account.data_deletion_audits.count(:all).must_equal(@audit_count + 1)
        audit = @account.data_deletion_audits.find(json['data_deletion_audit']['id'])
        assert json['success']
        audit.shard_id.must_equal(1)
        audit.status.must_equal 'waiting'
        audit.reason.must_equal 'moved'
      end

      it 'should refuse to create a second data_deletion_audit' do
        post :create, params: { reason: 'moved', account_id: '1', shard_id: 1, enqueue: enqueue }

        refute json['success']
        assert json['error']
        @account.data_deletion_audits.count(:all).must_equal(@audit_count + 1)
      end

      describe 'with enqueue parameter' do
        let(:enqueue) { true }
        it 'enqueues the job after creation' do
          json['data_deletion_audit']['status'].must_equal 'enqueued'
        end
      end
    end

    describe 'a PUT to :enqueue' do
      describe 'on a successful enqueue' do
        before { put :enqueue, params: { id: @audit.id } }
        it('responds with accepted') { assert_response :accepted }

        it 'contains location header' do
          assert_equal api_v2_internal_data_deletion_audit_url(@audit), @response.headers['Location']
        end
      end

      describe 'on a failed enqueue' do
        before do
          DataDeletionAudit.any_instance.stubs(:enqueue!).returns(false)
          put :enqueue, params: { id: @audit.id }
        end
        it('responds with internal_server_error') { assert_response :internal_server_error }
      end
    end
  end
end
