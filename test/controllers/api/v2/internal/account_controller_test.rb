require_relative "../../../../support/test_helper"
require_relative "../../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::Internal::AccountController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :all

  before do
    accept :json
    @account = accounts(:minimum)
    @request.account = @account
  end

  with_options(controller: "api/v2/internal/account") do |request|
    request.should_route :post, "/api/v2/internal/account/enable_contributor", action: "enable_contributor"
  end

  describe "connect/outbound subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "outbound") do
      should_be_forbidden [:put, :route, {}]

      should_be_authorized do
        post :enable_contributor
      end

      describe "POST #enable_contributor" do
        before { post :enable_contributor }

        it "creates Contributor role for given account" do
          assert_equal 1, @account.permission_sets.count
          assert_equal PermissionSet::Type::CONTRIBUTOR, @account.permission_sets.first.role_type
        end

        it("responds with created") { assert_response :created }
      end
    end
  end

  describe "zopim subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "zopim") do
      should_be_forbidden [:put, :route, {}]

      should_be_authorized do
        post :enable_contributor
      end
    end
  end

  describe "sell subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "sell") do
      should_be_forbidden [:put, :route, {}]

      should_be_authorized do
        post :enable_contributor
      end

      describe "POST #enable_contributor" do
        before { post :enable_contributor }

        it "creates Contributor role for given account" do
          assert_equal 1, @account.permission_sets.count
          assert_equal PermissionSet::Type::CONTRIBUTOR, @account.permission_sets.first.role_type
        end

        it("responds with created") { assert_response :created }
      end
    end
  end

  describe "embeddable subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "embeddable") do
      should_be_forbidden [:put, :route, {}]
    end
  end

  as_a_subsystem_user(account: :minimum) do
    describe "PUT #route" do
      it "updates gam domain when unique" do
        gam_domain = "foo.com"
        put :route, params: { gam_domain: gam_domain, format: :json }
        assert_response 200

        assert_equal gam_domain, @account.route.gam_domain
      end

      it "nils the gam domain when gam_domain is empty string" do
        gam_domain = "foo.com"
        @account.route.update_attribute(:gam_domain, gam_domain)
        @account.reload

        assert_equal gam_domain, @account.route.gam_domain
        put :route, params: { gam_domain: "", format: :json }

        assert_response 200
        assert_nil @account.route.gam_domain
      end

      it "errors when gam domain is not unique" do
        gam_domain = "foo.com"
        support = accounts(:support)
        support.route.update_attribute(:gam_domain, gam_domain)

        put :route, params: { gam_domain: gam_domain, format: :json }
        @account.reload

        assert_response 409
        assert_nil @account.route.gam_domain
        result = JSON.parse(@response.body)
        assert_equal "Domain in use", result["error"]
      end
    end
  end
end
