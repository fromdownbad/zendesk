require File.expand_path "../../../../../support/test_helper", File.dirname(__FILE__)

SingleCov.covered!

describe Api::V2::Internal::Ipm::AlertsController do
  extend Api::V2::TestHelper

  before do
    use_ssl
    accept :json
  end

  as_a_subsystem_user(account: :minimum) do
    describe "GET index" do
      let!(:alert) { FactoryBot.create(:ipm_alert) }

      it "returns list of IPMs" do
        get :index
        assert_response :ok
        assert_equal json['alerts'].first['id'], alert['id']
        assert_equal json['count'], 1
      end
    end

    describe "POST create" do
      describe "valid parameters with constraints" do
        let(:valid_params) { FactoryBot.attributes_for(:ipm_alert_with_constraints) }
        let(:more_params) { { start_date: Date.today, end_date: Date.tomorrow, state: 2 } }
        it "creates the alert with constraints" do
          post :create, params: { alert: valid_params.merge(more_params) }
          assert_response :created
          alert = Ipm::Alert.find(json['alert']['id'])
          assert_equal alert.text,                valid_params[:text]
          assert_equal alert.link_url,            valid_params[:link_url]
          assert_equal alert.internal_name,       valid_params[:internal_name]
          assert_equal alert.pods,                valid_params[:pods].split(/\s/).map(&:to_i)
          assert_equal alert.shards,              valid_params[:shards].split(/\s/).map(&:to_i)
          assert_equal alert.roles,               valid_params[:roles]
          assert_equal alert.languages,           valid_params[:languages]
          assert_equal alert.account_types,       valid_params[:account_types]
          assert_equal alert.interfaces,          valid_params[:interfaces]
          assert_equal alert.plans,               valid_params[:plans]
          assert_equal alert.subdomains,          valid_params[:subdomains]
          assert_equal alert.exclude_subdomains,  valid_params[:exclude_subdomains]
          assert_equal alert.custom_factors,      [valid_params[:custom_factors]]
          assert_equal alert.state,                more_params[:state]
          assert_equal alert.start_date,           more_params[:start_date]
          assert_equal alert.end_date,             more_params[:end_date]
        end
      end

      describe "invalid parameters" do
        let(:invalid_params) { { text: '' } }
        it "rejects the request" do
          post :create, params: { alert: invalid_params }
          assert_response :unprocessable_entity
          assert_equal json['error'], 'RecordInvalid'
        end
      end
    end

    describe "PUT update" do
      let(:alert) { FactoryBot.create(:ipm_alert) }

      describe "valid parameters with constraints" do
        let(:valid_params) { FactoryBot.attributes_for(:ipm_alert_with_constraints) }
        let(:state) { 1 }

        it "updates the alert" do
          assert_nil alert.state
          valid_params[:state] = state
          put :update, params: { id: alert.id, alert: valid_params }
          assert_equal state, alert.reload.state
          assert_response :ok
        end
      end

      describe "invalid parameters" do
        let(:invalid_params) { { text: '' } }
        it "rejects the request" do
          put :update, params: { id: alert.id, alert: invalid_params }
          assert_response :unprocessable_entity
          assert_equal json['error'], 'RecordInvalid'
        end
      end

      describe "missing record" do
        it "404" do
          put :update, params: { id: 0 }
          assert_response :not_found
          assert_equal json['error'], 'RecordNotFound'
        end
      end
    end

    describe "GET show" do
      describe "record exists" do
        let(:alert) { FactoryBot.create(:ipm_alert) }
        it "updates the alert" do
          get :show, params: { id: alert.id }
          assert_response :ok
          assert_equal json['alert']['id'], alert.id
        end
      end

      describe "missing record" do
        it "404" do
          get :show, params: { id: 1 }
          assert_response :not_found
          assert_equal json['error'], 'RecordNotFound'
        end
      end
    end

    describe "DELETE destroy" do
      describe "record exists" do
        let(:alert) { FactoryBot.create(:ipm_alert) }

        it "updates the alert and responds with 204" do
          delete :destroy, params: { id: alert.id }
          assert_response :no_content
        end
      end

      describe "missing record" do
        it "404" do
          delete :destroy, params: { id: 1 }
          assert_response :not_found
          assert_equal json['error'], 'RecordNotFound'
        end
      end
    end
  end
end
