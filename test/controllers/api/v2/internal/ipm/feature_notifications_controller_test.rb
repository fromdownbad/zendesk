require File.expand_path "../../../../../support/test_helper", File.dirname(__FILE__)

SingleCov.covered!

describe Api::V2::Internal::Ipm::FeatureNotificationsController do
  extend Api::V2::TestHelper

  before do
    use_ssl
    accept :json
  end

  as_a_subsystem_user(account: :minimum) do
    describe "GET index" do
      let!(:feature_notification) { FactoryBot.create(:ipm_feature_notification) }

      it "returns list of IPMs" do
        get :index
        assert_response :ok
        assert_equal json['feature_notifications'].first['id'], feature_notification['id']
        assert_equal json['count'], 1
      end
    end

    describe "POST create" do
      describe "valid parameters with constraints" do
        let(:valid_params) { FactoryBot.attributes_for(:ipm_feature_notification_with_constraints) }
        it "creates the alert with constraints" do
          post :create, params: { feature_notification: valid_params }
          assert_response :created
          feature_notification = Ipm::FeatureNotification.find(json['feature_notification']['id'])
          assert_equal feature_notification.internal_name,       valid_params[:internal_name]
          assert_equal feature_notification.pods,                valid_params[:pods].split(/\s/).map(&:to_i)
          assert_equal feature_notification.shards,              valid_params[:shards].split(/\s/).map(&:to_i)
          assert_equal feature_notification.roles,               valid_params[:roles]
          assert_equal feature_notification.languages,           valid_params[:languages]
          assert_equal feature_notification.account_types,       valid_params[:account_types]
          assert_equal feature_notification.interfaces,          valid_params[:interfaces]
          assert_equal feature_notification.plans,               valid_params[:plans]
          assert_equal feature_notification.subdomains,          valid_params[:subdomains]
          assert_equal feature_notification.exclude_subdomains,  valid_params[:exclude_subdomains]
        end
      end

      describe "invalid parameters" do
        let(:invalid_params) { { title: '', body: '' } }
        it "rejects the request" do
          post :create, params: { feature_notification: invalid_params }
          assert_response :unprocessable_entity
          assert_equal json['error'], 'RecordInvalid'
        end
      end
    end

    describe "PUT update" do
      let(:feature_notification) { FactoryBot.create(:ipm_feature_notification) }

      describe "valid parameters with constraints" do
        let(:valid_params) { FactoryBot.attributes_for(:ipm_feature_notification_with_constraints) }
        it "updates the alert" do
          put :update, params: { id: feature_notification.id, feature_notification: valid_params }
          assert_response :ok
        end
      end

      describe "invalid parameters" do
        let(:invalid_params) { { title: '', body: '' } }
        it "rejects the request" do
          put :update, params: { id: feature_notification.id, feature_notification: invalid_params }
          assert_response :unprocessable_entity
          assert_equal json['error'], 'RecordInvalid'
        end
      end

      describe "missing record" do
        it "404" do
          put :update, params: { id: 1 }
          assert_response :not_found
          assert_equal json['error'], 'RecordNotFound'
        end
      end
    end

    describe "GET show" do
      describe "record exists" do
        let(:feature_notification) { FactoryBot.create(:ipm_feature_notification) }
        it "updates the feature_notification" do
          get :show, params: { id: feature_notification.id }
          assert_response :ok
          assert_equal json['feature_notification']['id'], feature_notification.id
        end
      end

      describe "missing record" do
        it "404" do
          get :show, params: { id: 1 }
          assert_response :not_found
          assert_equal json['error'], 'RecordNotFound'
        end
      end
    end

    describe "DELETE destroy" do
      describe "record exists" do
        let(:feature_notification) { FactoryBot.create(:ipm_feature_notification) }

        it "updates the feature_notification and responds with 204" do
          delete :destroy, params: { id: feature_notification.id }
          assert_response :no_content
        end
      end

      describe "missing record" do
        it "404" do
          delete :destroy, params: { id: 1 }
          assert_response :not_found
          assert_equal json['error'], 'RecordNotFound'
        end
      end
    end
  end
end
