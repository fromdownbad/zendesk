require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::SandboxesController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users, :user_settings, :subscriptions

  before do
    set_header('X-Zendesk-Via', "Monitor event")
    set_header('X-Zendesk-Via-Reference-ID', 1234)
    @request.account = @account = accounts(:minimum)
    @controller.stubs(:current_registered_user).returns(Zendesk::SystemUser.find_subsystem_user_by_name("zendesk"))
  end

  with_options(controller: "api/v2/internal/sandboxes") do |request|
    request.should_route :get, "/api/v2/internal/sandboxes", action: "index"
    request.should_route :put, "/api/v2/internal/sandboxes/10", action: "update", id: 10
    request.should_route :post, "/api/v2/internal/sandboxes/10/reactivate", action: "reactivate", id: 10
    request.should_route :post, "/api/v2/internal/sandboxes/10/product_addon_sync", action: "product_addon_sync", id: 10
    request.should_route :post, "/api/v2/internal/sandboxes/10/notification", action: "notification", id: 10
  end

  describe "as a SystemUser" do
    before do
      @first_sandbox = accounts(:with_groups)
      @first_sandbox.update_column(:sandbox_master_id, @account.id)
      @first_sandbox.update_column(:is_active, false)
      @first_sandbox.update_column(:updated_at, 1.days.ago)

      @second_sandbox = accounts(:trial)
      @second_sandbox.update_column(:sandbox_master_id, @account.id)
      @second_sandbox.update_column(:updated_at, 1.minutes.ago)
    end

    it "has two different sandboxes" do
      refute_equal @first_sandbox.id, @second_sandbox.id
    end

    describe "#list_sandboxes" do
      before { get :index, format: :json }

      should_use_presenter Api::V2::AccountPresenter

      it "lists all sandboxes for an account" do
        assert_response :success
        assert_equal 2, JSON.load(@response.body)["accounts"].size
      end

      it "shows active and inactive sandboxes" do
        assert_equal JSON.load(@response.body)["accounts"].map { |m| m["subdomain"] }.sort, [@first_sandbox.subdomain, @second_sandbox.subdomain].sort
      end

      it "lists most recently updated sandbox first" do
        assert_operator @second_sandbox.updated_at, :>, @first_sandbox.updated_at
        assert_equal @second_sandbox.subdomain, JSON.load(@response.body)["accounts"].first["subdomain"]
      end
    end

    describe "#reactivate_sandbox" do
      describe "when the sandbox exists" do
        before do
          put :update, params: { id: @first_sandbox.id, format: :json }
        end

        it "is successful" do
          assert_response :success
        end

        it "activates the current sandbox and cancels the old" do
          assert @first_sandbox.reload.is_active
          assert @first_sandbox.reload.is_serviceable
          refute @second_sandbox.reload.is_active
        end

        it "correctly link the first sandbox" do
          assert_equal @account.reload.sandbox.id, @first_sandbox.id
        end

        describe "using the new action" do
          before do
            post :reactivate, params: { id: @first_sandbox.id, format: :json }
          end

          it "is successful" do
            assert_response :success
          end
        end
      end

      describe "when the sandbox does not exist" do
        it "responds with not_found" do
          put :update, params: { id: 99, format: :json }
          assert_response :not_found
        end
      end
    end

    [:multiple_sandboxes, :admin_center_framework_view_sandbox].each do |feature|
      describe "#reactivate_sandbox with #{feature}" do
        before do
          Arturo.enable_feature! feature
          put :update, params: { id: @first_sandbox.id, format: :json }
        end

        it "activates the current sandbox and does not deactivate the old one" do
          assert @first_sandbox.reload.is_active
          assert @first_sandbox.reload.is_serviceable
          assert @second_sandbox.reload.is_active
        end
      end
    end

    describe "#product_addon_sync" do
      let(:pravda_client) { stub(product!: nil, create_product!: nil) }
      before do
        ::Zendesk::Accounts::Client.stubs(:new).returns(pravda_client)
        pravda_client.stubs(:product).with(::Account::FraudSupport::SELL_PRODUCT, use_cache: true).returns(nil)
        pravda_client.stubs(:products).returns(
          [
            ::Zendesk::Accounts::Product.new(
              FactoryBot.build(:support_subscribed_product)
            )
          ]
        )
      end

      it 'copies the product records from master to sandbox, setting to free state' do
        pravda_client.expects(:update_or_create_product).with(
          :support,
          {
            product: {
              state:         ::Zendesk::Accounts::Product::FREE,
              plan_settings: {
                'max_agents' => 5,
                'plan_type'  => 1
              }
            }
          },
          context: 'sandbox_creation'
        )
        Zendesk::Features::SubscriptionFeatureService.any_instance.expects(:create_sandbox_features!).with(@first_sandbox.sandbox_master.subscription)
        post :product_addon_sync, params: { id: @first_sandbox.id, format: :json }
        assert_response :success
      end

      describe "when the sandbox does not exist" do
        it "responds with not_found" do
          post :product_addon_sync, params: { id: 99, format: :json }
          assert_response :not_found
        end
      end
    end

    describe '#notification' do
      let(:user) { @account.admins.first }
      let(:notification_type) { 'success' }
      let(:user_id) { user.id }
      let(:params) do
        {
          id: @account.id,
          user_id: user_id,
          sandbox_id: @first_sandbox.id,
          notification_type: notification_type,
          format: :json
        }
      end

      describe 'success notification' do
        let(:notification_type) { 'success' }

        it 'delivers a notification to the user' do
          SandboxMailer.expects(:deliver_success_notification)
          post :notification, params: params
          assert_response :success
        end
      end

      describe 'failure notification' do
        let(:notification_type) { 'failure' }

        it 'delivers a notification to the user' do
          SandboxMailer.expects(:deliver_failure_notification)
          post :notification, params: params
          assert_response :success
        end
      end

      describe 'ready notification' do
        let(:notification_type) { 'ready' }

        it 'delivers a notification to the user' do
          SandboxMailer.expects(:deliver_ready_notification)
          post :notification, params: params
          assert_response :success
        end
      end

      describe 'unknown notification' do
        let(:notification_type) { 'unknown' }

        it 'responds with bad_request' do
          post :notification, params: params
          assert_response :bad_request
        end
      end

      describe 'user does not exist' do
        let(:user_id) { 348984384389349834894 }

        it 'responds with bad_request' do
          post :notification, params: params
          assert_response :bad_request
        end
      end
    end
  end
end
