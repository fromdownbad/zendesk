require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::ImportExportJobPolicyController do
  extend Api::V2::TestHelper
  fixtures :accounts

  before do
    accept :json
    @request.account = accounts(:minimum)
  end

  with_options(controller: "api/v2/internal/import_export_job_policy") do |request|
    request.should_route :get, "/api/v2/internal/import_export_job_policy/job_keys_by_type", action: "job_keys_by_type"
  end

  as_a_subsystem_user(account: :minimum) do
    it "renders a hash of job keys grouped by type" do
      get :job_keys_by_type, format: :json
      assert_response :ok
      json = JSON.parse(@response.body)

      assert_equal json["default"], ImportExportJobPolicy::DEFAULT_JOBS
      assert_equal json["export"], ImportExportJobPolicy::EXPORT_JOBS
      assert_equal json["view"], ImportExportJobPolicy::VIEW_JOBS
    end
  end
end
