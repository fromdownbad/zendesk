require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::RecipientAddressesController do
  extend Api::V2::TestHelper
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :recipient_addresses

  let(:recipient_address) { recipient_addresses(:not_default) }

  before do
    @account = accounts(:minimum)

    accept :json
  end

  should_route :get, '/api/v2/recipient_addresses', action: 'index', controller: 'api/v2/recipient_addresses'
  should_route :put, '/api/v2/recipient_addresses/1/verify', action: 'verify', controller: 'api/v2/recipient_addresses', id: 1

  as_a_subsystem_user(account: :minimum) do
    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::Internal::RecipientAddressPresenter, status: :ok
    end

    describe "a GET to :show" do
      before { get :show, params: { id: recipient_address.id } }
      should_use_presenter Api::V2::Internal::RecipientAddressPresenter
    end

    describe "a PUT to :verify" do
      resque_inline false

      it "performs forwarding verification" do
        put :verify, params: { id: recipient_address.id, type: "forwarding" }
        assert_response :success
        assert_equal "{}", response.body # empty json so jquery does not blow up
        assert_equal :waiting, recipient_address.reload.forwarding_status
      end

      it "performs SPF verification" do
        recipient_address.update_column(:spf_status_id, RecipientAddress::SPF_STATUS.fetch(:failed))
        RecipientAddress.any_instance.expects(default_host?: true).twice

        put :verify, params: { id: recipient_address.id, type: "spf" }

        assert_response :success
        assert_equal "{}", response.body # empty json so jquery does not blow up
        assert_equal :verified, recipient_address.reload.spf_status
      end

      describe_with_arturo_enabled :email_deprecate_cname_checks do
        it "performs DNS verification for TXT record" do
          recipient_address.update_column(:metadata, nil)
          RecipientAddress.any_instance.expects(default_host?: true).times(1)

          put :verify, params: { id: recipient_address.id, type: "dns" }

          assert_response :success
          assert_equal "{}", response.body # empty json so jquery does not blow up
          assert_equal "verified", recipient_address.reload.metadata["domain_verification"]["status"]
        end
      end

      describe_with_arturo_disabled :email_deprecate_cname_checks do
        it "performs DNS verification for TXT and CNAME records" do
          recipient_address.update_column(:metadata, nil)
          RecipientAddress.any_instance.expects(default_host?: true).times(3)

          put :verify, params: { id: recipient_address.id, type: "dns" }

          assert_response :success
          assert_equal "{}", response.body # empty json so jquery does not blow up
          assert_equal "verified", recipient_address.reload.metadata["domain_verification"]["status"]
        end
      end
    end
  end

  as_a_subsystem_user(user: "zopim", account: :minimum) do
    should_be_forbidden [:put, :verify, { id: 1 }, type: "forwarding"]

    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::Internal::RecipientAddressPresenter, status: :ok
    end

    describe "a GET to :show" do
      before { get :show, params: { id: recipient_address.id } }
      should_use_presenter Api::V2::Internal::RecipientAddressPresenter
    end
  end

  as_a_subsystem_user(user: "sell", account: :minimum) do
    should_be_forbidden [:put, :verify, {id: 1}, type: "forwarding"]

    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::Internal::RecipientAddressPresenter, status: :ok
    end

    describe "a GET to :show" do
      before { get :show, params: { id: recipient_address.id } }
      should_use_presenter Api::V2::Internal::RecipientAddressPresenter
    end
  end

  # For a complete set of tests, go to `zendesk_cursor_pagination` gem.
  as_a_subsystem_user(account: :minimum) do
    describe 'using cursor based pagination' do
      let(:account) { accounts(:minimum) }
      let(:page_size) { 2 }
      let(:page_params) { {size: page_size} }
      let(:json) do
        get :index, params: { page: page_params }
        JSON.parse(@response.body).with_indifferent_access
      end

      describe 'index' do
        before { get :index }
        it('responds with success') { assert_response :success }
      end

      it 'generates the correct order clause' do
        assert_sql_queries(1, /ORDER BY `recipient_addresses`.`id`/) do
          json
        end
      end

      describe 'when there is only one page' do
        let(:page_size) { account.recipient_addresses.size + 1 }

        it 'presents the correct keys' do
          refute json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of recipient addresses' do
          assert_equal account.recipient_addresses.size, json[:recipient_addresses].size
        end
      end

      describe 'when there are multiple pages' do
        let(:page_size) { account.recipient_addresses.size - 1 }

        it 'presents the correct keys' do
          assert json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of recipient addresses' do
          assert_equal page_size, json[:recipient_addresses].size
        end
      end

      describe 'with a valid after cursor' do
        let(:page_size) { account.recipient_addresses.size - 1 }
        let(:after_json) { JSON.parse(@response.body).with_indifferent_access }

        before do
          after_cursor = json[:meta][:after_cursor]
          # reset memoized recipient_addresses
          @controller.remove_instance_variable(:@recipient_addresses)

          get :index, params: { page: {
            size: page_size,
            after: after_cursor
          } }
        end

        it 'presents the correct keys' do
          refute after_json[:meta][:has_more]
          assert after_json[:meta][:before_cursor].present?
          assert after_json[:meta][:after_cursor].present?
          assert after_json[:links][:prev].present?
          assert after_json[:links][:next].present?
        end

        it 'presents the correct number of recipient addresses' do
          assert_equal 1, after_json[:recipient_addresses].size
        end
      end

      describe 'with an invalid after cursor' do
        let(:page_params) do
          {
            size: page_size,
            after: 'invalid'
          }
        end

        it 'presents an error message' do
          assert_equal 'InvalidPaginationParameter', json[:error]
          assert_equal 'page[after] is not valid', json[:description]
          assert_response :bad_request
        end
      end

      describe_with_arturo_disabled :email_cursor_pagination_recipient_addresses_index do
        it 'presents the same error message we were showing before adding the types' do
          assert_equal 'Invalid attribute', json[:error][:title]
          assert_equal 'You passed an invalid value for the page attribute. Invalid parameter: page must be an integer', json[:error][:message]
          assert_response :bad_request
        end
      end
    end
  end
end
