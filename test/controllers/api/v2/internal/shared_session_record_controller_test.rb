require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::SharedSessionRecordController do
  fixtures :accounts

  before do
    accept :json
  end

  with_options(controller: 'api/v2/internal/shared_session_record') do |request|
    request.should_route :post, 'api/v2/internal/shared_session_record', action: 'create'
  end

  as_an_admin do
    should_be_forbidden :create
  end

  as_a_subsystem_user(user: 'passport', account: :multiproduct) do
    describe 'POST #create' do
      let(:user) { @account.users.first }
      let(:params) do
        {
          session_id: '90556b71151ebc5a33cdf49ab8ede71904f822c76f5c1a54a748745881d60c5f',
          user_id: user.id,
          auth_via: 'password', # warden.winning_strategy.name
          invalidate_id: false, # warden.winning_strategy.invalidate_id?
          remember_me_requested: false, # warden.request.params[:remember_me].to_i != 0
          auth_duration: 28800, # passport will calculate
          user_role: 2, # passport will send in
          csrf_token: 'w5ZVRnESj40yaiWRVepnYRFAJ2YdT81u3hCg11zKFhfT05Nl2rZ50gntoY6hvuqr'
        }
      end

      describe 'sends correct params' do
        it 'creates the shared_session record' do
          assert_empty user.shared_sessions

          post :create, params: params

          assert_equal 1, user.shared_sessions.count
          assert_nil JSON.parse(response.body)["errors"]
          assert_response :created
        end
      end

      describe 'causing error' do
        let(:params) { { user_id: user.id } }

        it 'responds with unprocessable_entity error' do
          Rails.logger.expects(:error).with { |msg| msg =~ /internal_api_shared_session_record_creations error/ }
          post :create, params: params

          assert_not_nil JSON.parse(response.body)["errors"]
          assert_response :unprocessable_entity
        end
      end
    end
  end
end
