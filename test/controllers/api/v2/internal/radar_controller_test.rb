require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 2

class StubRadarClient
  def commands
    @commands ||= []
  end

  def method_missing(m, *args)
    commands.push(method: m, args: args)
    self
  end
end

describe Api::V2::Internal::RadarController do
  extend Api::V2::TestHelper

  fixtures :accounts, :account_settings
  before do
    use_ssl
  end
  let(:minimum_account) { accounts(:minimum) }

  as_a_subsystem_user(account: :minimum) do
    let(:kill_switch_feature) { Arturo::Feature.find_feature(:radar_kill_switch) }

    describe '#reconfigure' do
      before do
        # Accept requests to https://pod-X.zendesk.com
        request.env['zendesk.account'] = nil
      end
      describe 'valid request' do
        it '200 with non-empty params' do
          post :reconfigure, params: { shard_id: '1', account_subdomain: 'minimum', format: :json }
          assert_response :ok
          assert_equal json['status'], 'ok'
        end

        it 'sets radar reconfigure resource' do
          recorder = StubRadarClient.new
          @controller.expects(:reconfigure_radar_client).returns recorder

          post :reconfigure, params: { shard_id: '1', account_subdomain: 'minimum', format: :json }

          assert_equal :status, recorder.commands[0][:method]
          assert_equal 'reconfigure', recorder.commands[0][:args][0]

          assert_equal :set, recorder.commands[1][:method]
        end
      end

      it '400 with an empty account_subdomain' do
        post :reconfigure, params: { shard_id: '1', account_subdomain: '', format: :json }
        assert_response :bad_request
        assert_equal 'Couldn\'t find Account with subdomain = ', json['description']
      end

      it '400 with an nonexistent account_subdomain' do
        post :reconfigure, params: { shard_id: '1', account_subdomain: 'acct-doesnt-exist', format: :json }
        assert_response :bad_request
        assert_equal 'Couldn\'t find Account with subdomain = acct-doesnt-exist', json['description']
      end
    end

    describe '#suspend' do
      let(:recorder) { StubRadarClient.new }
      describe 'valid request' do
        before do
          Radar::Client.expects(:build_client).returns recorder
          post :suspend, params: { audit: {
            "actor_id" => -1,
            "actor_type" => "User",
            "ip_address" => "0.0.0.0",
            "message" => "Radar suspended for trial",
            "via_id" => 43,
            "via_reference_id" => 4645
          }, format: :json }
        end
        it '200 with non-empty params' do
          assert_response :ok
          assert_equal json['enabled'], false
        end

        it 'sets radar reconfigure resource' do
          assert_equal :status, recorder.commands[0][:method]
          assert_equal 'reconfigure', recorder.commands[0][:args][0]

          assert_equal :set, recorder.commands[1][:method]
        end

        it "sets the radar_enabled setting to false" do
          refute minimum_account.settings.reload.radar_enabled
        end

        it "creates audit log" do
          assert_present CIA::Event.find_by_source_type('Account')
        end
      end
    end

    describe '#unsuspend' do
      describe 'valid request when with subdomain given' do
        before do
          post :unsuspend, params: { audit: {
            "actor_id" => -1,
            "actor_type" => "User",
            "ip_address" => "0.0.0.0",
            "source_type" => "Account",
            "message" => "Radar unsuspended for trial",
            "via_id" => 43,
            "via_reference_id" => 4645
          }, format: :json }
        end
        it '200 with non-empty params' do
          assert_response :ok
          assert_equal json, "enabled" => true
        end

        it "sets the radar_enabled setting to true" do
          assert minimum_account.settings.reload.radar_enabled
        end

        it "creates audit log" do
          assert_present CIA::Event.find_by_source_type('Account')
        end
      end
    end
  end
end
