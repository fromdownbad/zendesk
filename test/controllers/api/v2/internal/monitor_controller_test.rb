require_relative "../../../../support/test_helper"
require_relative "../../../../support/voice_test_helper"

SingleCov.covered! uncovered: 35

describe Api::V2::Internal::MonitorController do
  include ZendeskBillingCore::ZuoraTestHelper
  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    set_header('X-Zendesk-Via', "Monitor event")
    set_header('X-Zendesk-Via-Reference-ID', 1234)
    @request.account = @account = accounts(:minimum)
    Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
  end

  describe "when accessed as the account service" do
    before do
      @controller.stubs(:current_registered_user).returns(Zendesk::SystemUser.find_subsystem_user_by_name("account_service"))
      put :account, params: { account: {multiproduct: true}, format: :json }
    end

    should_use_presenter Api::V2::AccountPresenter

    it "updates" do
      assert_response 200
    end
  end

  describe "when accessed as the bime service" do
    as_a_subsystem_user(user: 'bime', account: :minimum) do
      it 'returns ok' do
        get :account_stats, format: :json
        assert_response :ok
      end
    end
  end

  describe "when accessed as a system user" do
    before do
      @controller.stubs(:current_registered_user).returns(Zendesk::SystemUser.find_subsystem_user_by_name("zendesk"))
    end

    describe "ip logging" do
      it "logs real forwarded ips" do
        assert @request.account.reload.is_serviceable
        set_header "HTTP_X_FORWARD_FOR", "1.2.3.4"
        put :account, params: { account: {is_serviceable: 0}, format: :json }

        # updated
        assert_response 200
        refute @request.account.reload.is_serviceable

        # logged
        assert_equal "1.2.3.4", CIA::Event.last.ip_address
      end

      it "does not overwrite with empty ips" do
        assert @request.account.reload.is_serviceable
        put :account, params: { account: {is_serviceable: 0}, format: :json }

        # updated
        assert_response 200
        refute @request.account.reload.is_serviceable

        # logged
        assert_equal "0.0.0.0", CIA::Event.last.ip_address
      end
    end

    describe "#token" do
      it "makes a master token available for the given user" do
        @user = users(:minimum_admin)
        assert_difference("MasterToken.count(:all)", 1) do
          post :token, params: { user_id: @user.id, format: :json }
        end

        assert_response 201
        result = JSON.parse(@response.body)
        assert_equal ["expires_at", "id", "value"], result["verification_token"].keys.sort
        assert_equal 1234, MasterToken.last.assuming_monitor_user_id
        assert_match /T\d\d:\d\d:\d\dZ/, result["verification_token"]["expires_at"] # correct date format
      end
    end

    describe "PUT #account" do
      before { Account.any_instance.stubs(:is_trial?).returns(true) }
      it "updates" do
        assert @request.account.reload.is_serviceable
        put :account, params: { account: {is_serviceable: 0}, format: :json }
        assert_response 200
        refute @request.account.reload.is_serviceable
      end

      it "updates without harassing us about owner verification status" do
        @request.account.update_attributes!(is_serviceable: false)
        @request.account.owner.identities.each { |id| id.update_attributes!(is_verified: false) }
        FraudScoreJob.expects(:enqueue_account).with(@request.account, Fraud::SourceEvent::UNSUSPENDED, nil).once
        put :account, params: { account: {is_serviceable: true}, format: :json }
        assert_response 200
        assert @request.account.reload.is_serviceable
      end

      it 'enqueues manual suspension fraud score job when an account is suspended' do
        assert @request.account.reload.is_serviceable
        FraudScoreJob.expects(:enqueue_account).with(@request.account, Fraud::SourceEvent::MANUAL_SUSPENSION, nil).once
        put :account, params: { account: {is_serviceable: false}, format: :json }
        assert_response 200
      end

      it 'enqueues unsuspended fraud score job when an account is unsuspended' do
        assert @request.account.reload.is_serviceable
        FraudScoreJob.expects(:enqueue_account).with(@request.account, Fraud::SourceEvent::UNSUSPENDED, nil).once
        put :account, params: { account: {is_serviceable: true}, format: :json }
        assert_response 200
      end
    end

    describe "DELETE #account" do
      it "destroys the account, yeah." do
        assert Account.find_by_subdomain('minimum').is_active?
        delete :account, format: :json
        refute Account.find_by_name('minimum')
      end
    end

    describe "PUT #soft_delete_account" do
      it 'softs delete an account' do
        @request.account.expects(:soft_delete!).once
        put :soft_delete_account, format: :json
      end
      it 'responds with error on exception' do
        Account.any_instance.stubs(:soft_delete!).throws(:exception)
        put :soft_delete_account, format: :json
        assert_includes JSON.parse(@response.body).keys, 'error'
        assert_equal @response.status, 422 # unprocessable_entity
      end
    end

    describe 'PUT #release_account_subdomain' do
      let(:timestamp) { Time.now }

      before do
        Timecop.freeze
      end

      it 'renames original subdomain' do
        subdomain = @account.subdomain

        put :release_account_subdomain, params: { account_id: @request.account.id, format: :json }

        assert_response 200

        @account.reload

        assert_match %r{^#{subdomain}-[0-9]+-deleted$}, @account.subdomain
        assert_equal timestamp.to_i, @account.updated_at.to_i
      end

      it 'renames host_mapping' do
        @account.update_column(:host_mapping, "something")

        put :release_account_subdomain, params: { account_id: @request.account.id, format: :json }

        assert_response 200

        @account.reload
        assert_match %r{^something-[0-9]+-deleted$}, @account.host_mapping
        assert_equal timestamp.to_i, @account.updated_at.to_i
      end

      it 'renames route subdomains' do
        put :release_account_subdomain, params: { account_id: @request.account.id, format: :json }

        assert_response 200

        @account.reload
        @account.routes.each { |r| assert_match %r{-[0-9]+-deleted$}, r.subdomain }
      end

      it 'response with error on exception' do
        Account.any_instance.stubs(:update_columns).throws(:exception)

        put :release_account_subdomain, params: { account_id: @request.account.id, format: :json }

        assert_equal 422, @response.status, "responded with error json" # unprocessable_entity
        assert_includes JSON.parse(@response.body).keys, 'error'
      end
    end

    describe "#subscription" do
      describe "changing payment methods" do
        before do
          @subscription = @request.account.subscription
        end

        it "allows changing to invoicing mode" do
          assert_equal PaymentMethodType.CREDIT_CARD, @subscription.reload.payment_method_type

          put :subscription, params: {
            subscription: {
              payment_method_type: PaymentMethodType.MANUAL,
              hub_account_id: '' # Piggybacking for param validation
            },
            format: :json
          }

          assert_response 200
          assert_equal PaymentMethodType.MANUAL, @subscription.reload.payment_method_type
        end
      end

      describe "voice_sub_account" do
        describe "change opted_in" do
          describe "with voice subaccount" do
            it "shoulds allow the voice optin status to be changed" do
              FactoryBot.create(:voice_sub_account, account: @account, opted_in: true)
              ZendeskBillingCore::Zuora::Jobs::UpdateVoiceSubAccountJob.expects(:enqueue).with(@account.id, false, 'X-Forwarded-For' => nil, 'X-Zendesk-Via' => 'Monitor event', 'X-Zendesk-Via-Reference-ID' => 1234)

              put :subscription, params: {
                subscription: {
                  voice_sub_account: {
                    opted_in: "false"
                  },
                  hub_account_id: nil # Piggybacking for param validation
                },
                format: :json
              }
            end
          end

          describe "without voice subaccount" do
            before { assert_nil @account.voice_sub_account }

            it "creates a voice subaccount and optin for voice when enabled" do
              ZendeskBillingCore::Zuora::Jobs::UpdateVoiceSubAccountJob.expects(:enqueue).with(@account.id, true, 'X-Forwarded-For' => nil, 'X-Zendesk-Via' => 'Monitor event', 'X-Zendesk-Via-Reference-ID' => 1234)
              put :subscription, params: { subscription: { voice_sub_account: {opted_in: "true"} }, format: :json }
            end
          end
        end
      end

      describe "subscription fraud job" do
        it "sends a fraud job if enterprise plan type" do
          FraudScoreJob.expects(:enqueue_account).with(@account, Fraud::SourceEvent::ENTERPRISE_SUBSCRIPTION, nil).once
          put :subscription, params: { subscription: { plan_type: "4" }, format: :json }
        end

        it "does not send a fraud job if another plan type" do
          FraudScoreJob.expects(:enqueue_account).with(@account, Fraud::SourceEvent::ENTERPRISE_SUBSCRIPTION, nil).never
          put :subscription, params: { subscription: { plan_type: "3" }, format: :json }
        end

        it "does not send a fraud job if no plan type" do
          FraudScoreJob.expects(:enqueue_account).with(@account, Fraud::SourceEvent::ENTERPRISE_SUBSCRIPTION, nil).never
          put :subscription, params: { subscription: {} }, as: :json
        end

        it "does not send a fraud job if account whitelisted" do
          @account.stubs(:whitelisted?).returns(true)
          FraudScoreJob.expects(:enqueue_account).with(@account, Fraud::SourceEvent::ENTERPRISE_SUBSCRIPTION, nil).never
          put :subscription, params: { subscription: { plan_type: "4" }, format: :json }
        end

        it "sends a fraud job if trial extension" do
          FraudScoreJob.expects(:enqueue_account).with(@account, Fraud::SourceEvent::TRIAL_EXTENSION, nil).once
          put :subscription, params: { subscription: {trial_expires_on: (@account.subscription.trial_expires_on + 1).to_s}, format: :json }
        end

        it "does not send a fraud job if no trial extension" do
          FraudScoreJob.expects(:enqueue_account).with(@account, Fraud::SourceEvent::TRIAL_EXTENSION, nil).never
          put :subscription, params: { subscription: {trial_expires_on: @account.subscription.trial_expires_on.to_s}, format: :json }
        end

        it "does not send a fraud job if trial_expires_on is nil in request" do
          FraudScoreJob.expects(:enqueue_account).with(@account, Fraud::SourceEvent::TRIAL_EXTENSION, nil).never
          put :subscription, params: { subscription: {trial_expires_on: nil} }, as: :json
        end
      end

      describe "subscription fraud job nil test" do
        before { @account.subscription.update_attribute(:trial_expires_on, nil) }
        it "does not send a fraud job if account trial_expires_on is nil" do
          FraudScoreJob.expects(:enqueue_account).with(@account, Fraud::SourceEvent::TRIAL_EXTENSION, nil).never
          put :subscription, params: { subscription: {trial_expires_on: Date.today.to_s}, format: :json }
        end
      end
    end

    describe "#experiment_participations" do
      let(:account) { accounts(:minimum) }

      describe "when participating" do
        before do
          account.experiments.each(&:delete)
          account.experiments.create(
            [
              {name: 'getting-started', group: 'skilled', version: 4, finished: true},
              {name: 'buy-now', group: 'click', version: 4},
              {name: 'buy-now', group: 'click'},
              {name: 'rate-us', group: 'green', version: 4, finished: true},
              {name: 'red-black', group: 'red', finished: true}
            ]
          )
          get :experiment_participations, format: :json
        end

        it "renders all the experiments where it participates" do
          expected = {
            'experiment_participations' => [
              {
                'name'     => 'buy-now',
                'group'    => 'click',
                'version'  => '',
                'finished' => false
              },
              {
                'name'     => 'buy-now',
                'group'    => 'click',
                'version'  => '4',
                'finished' => false
              },
              {
                'name'     => 'getting-started',
                'group'    => 'skilled',
                'version'  => '4',
                'finished' => true
              },
              {
                'name'     => 'rate-us',
                'group'    => 'green',
                'version'  => '4',
                'finished' => true
              },
              {
                'name'     => 'red-black',
                'group'    => 'red',
                'version'  => '',
                'finished' => true
              }
            ]
          }
          actual = JSON.parse(response.body).slice('experiment_participations')

          assert_equal expected.keys.size, actual.keys.size

          sorter = ->(a) { [a['name'], a['version']] }
          assert_equal expected['experiment_participations'].sort_by(&sorter),
            actual['experiment_participations'].sort_by(&sorter)
        end
      end

      describe "when not participating" do
        before do
          account.experiments.each(&:delete)
          get :experiment_participations, format: :json
        end

        it "renders all the experiments where it participates" do
          expected = { 'experiment_participations' => [] }
          assert_equal expected, JSON.parse(response.body).
            slice('experiment_participations')
        end
      end
    end

    describe "#logout_all_users_and_devices" do
      before do
        @account = accounts(:minimum)
      end

      it "logouts all users and devices" do
        TerminateAllSessionsJob.expects(:enqueue).with(@account.id, false).once
        post :logout_all_users_and_devices, format: :json
        assert_response :success
      end
    end

    describe "#takeover_account" do
      before do
        @account = accounts(:minimum)
        Account.any_instance.stubs(:is_trial?).returns(true)
      end

      it 'enqueues a fraud score job when an account is taken over' do
        FraudScoreJob.expects(:enqueue_account).with(@account, Fraud::SourceEvent::TAKEN_OVER_ACCOUNT, nil).once
        assert Account.find_by_subdomain('minimum').is_active?
        post :takeover_account, format: :json
        assert_response :success
      end
    end

    describe "#send_one_time_login_to_user" do
      let(:account) { accounts(:minimum) }
      let(:admin) { account.owner }
      let(:agent) { users(:minimum_agent) }
      let(:email) { ActionMailer::Base.deliveries.last }
      let(:json) { JSON.parse(response.body) }

      before do
        Arturo.enable_feature! :access_token_sso_bypass
        account.settings.sso_bypass = false
        account.settings.save!
        ActionMailer::Base.perform_deliveries = true

        assert_empty admin.otp_tokens
        assert_empty agent.otp_tokens
      end

      it "requires the account to have sso_bypass disabled" do
        account.settings.sso_bypass = true
        account.settings.save!

        post :send_one_time_login_to_user, params: { user_id: admin.id, format: :json }
        assert_response :unprocessable_entity
        assert_equal "SSO bypass is enabled for this account", json['error']
        assert_empty admin.otp_tokens.reload
        assert_nil email
      end

      it "requires an admin" do
        post :send_one_time_login_to_user, params: { user_id: agent.id, format: :json }
        assert_response :unprocessable_entity
        assert_equal "User is not an administrator", json['error']
        assert_empty agent.otp_tokens.reload
        assert_nil email
      end

      it "requires a monitor_user_id" do
        request.env.delete 'HTTP_X_ZENDESK_VIA_REFERENCE_ID'
        request.env.delete 'HTTP_X_ZENDESK_VIA'

        post :send_one_time_login_to_user, params: { user_id: admin.id, format: :json }
        assert_response :unprocessable_entity
        assert_equal "Monitor user id required", json['error']
        assert_empty admin.otp_tokens.reload
        assert_nil email
      end

      it "sends an otp code to the admin" do
        post :send_one_time_login_to_user, params: { user_id: admin.id, format: :json }

        assert_response :ok
        assert_equal 1, admin.otp_tokens.reload.count
        token = admin.otp_tokens.first
        expected_link = "https://minimum.zendesk-test.com/access/access_token/#{token.value}"
        assert_match expected_link, email.text_part.body.decoded
      end
    end

    describe "#twitter_handles" do
      it "finds monitored twitter handles" do
        monitored_twitter_handles(:minimum_monitored_twitter_handle_1)
        get :twitter_handles, format: :json
        assert_response 200
        result = JSON.parse(@response.body)
        assert_equal [
          'id',
          'twitter_id',
          'twitter_screen_name',
          'since_ids',
          'can_reply',
          'active',
          'authorized',
          'direct_messages_enabled_at',
          'mentions_enabled_at',
          'favorites_enabled_at',
          'created_at'
        ].sort, result["twitter_handles"][0].keys.sort
      end
    end

    describe "#facebook_pages" do
      it "finds facebook pages" do
        get :facebook_pages, format: :json
        assert_response 200
        result = JSON.parse(@response.body)
        assert_equal ["debug_access_token", "id", "link", "name", "settings", "state_text"], result["facebook_pages"][0].keys.sort
      end
    end

    describe '#registered_integration_services' do
      before do
        ::Channels::AnyChannel::RegisteredIntegrationService.new(
          name:           'ris',
          search_name:    'ris',
          external_id:    1,
          manifest_url:   'manifest_url',
          admin_ui_url:   'admin_ui_url'
        ).tap do |ris|
          ris.account = @account
          ris.save!
        end
      end

      it 'finds registered integration services' do
        get :registered_integration_services, format: :json
        assert_response 200
        result = JSON.parse(@response.body)
        assert_equal ["author", "channelback_files", "create_followup_tickets", "external_id", "id", "manifest_url",
                      "name", "push_client_id"], result["registered_integration_services"][0].keys.sort
      end
    end

    describe '#integration_service_instances' do
      before do
        ::Channels::AnyChannel::IntegrationServiceInstance.new(
          registered_integration_service_id: 234,
          name: "ISI",
          metadata: '{}',
          state: ::Channels::AnyChannel::IntegrationServiceInstance::State::ACTIVE
        ).tap do |isi|
          isi.account = @account
          isi.save!
        end
      end

      it 'finds integration service instances' do
        get :integration_service_instances, format: :json
        assert_response 200
        result = JSON.parse(@response.body)
        assert_equal ["id", "last_polled_at", "metadata", "name", "needs_reinitialization", "pull_state",
                      "registered_integration_service_id", "state_text"], result["integration_service_instances"][0].keys.sort
      end
    end

    describe '#security_settings' do
      describe 'render' do
        let(:json) { JSON.parse(@response.body) }
        let(:expected) do
          %w[
            captcha_required content_type_header csp_header email_agent_when_sensitive_fields_changed
            end_user_comment_rate_limit password_reset_override
            remote_auth security_headers spam_prevention spam_processor
            spam_threshold_multiplier ssl_enabled
            x_frame_header xss_header
          ]
        end

        before { get :security_settings, format: :json }

        it 'outputs expected keys' do
          assert_equal expected, json['security_settings'].keys.sort
        end
      end
    end

    describe '#account_stats' do
      describe 'render' do
        let(:json) { JSON.parse(@response.body) }
        let(:expected) do
          %w[
            active_brand_count admin_count agent_count brand_count
            custom_field_options_count entry_count group_count inactive_brand_count
            organization_count owner rule_count settings sla_policies_count
            ticket_count ticket_field_conditions_count ticket_fields_count
            ticket_forms_count user_count
          ]
        end

        before { get :account_stats, format: :json }

        it 'outputs expected keys' do
          assert_equal expected, json['account_stats'].keys.sort
        end
      end

      describe "caching" do
        it "is cached" do
          Api::V2::Internal::AccountStatsPresenter.any_instance.expects(:present)
          get :account_stats, format: :json

          Api::V2::Internal::AccountStatsPresenter.any_instance.expects(:present).never
          get :account_stats, format: :json
        end

        it "busts cache when current account changes" do
          Api::V2::Internal::AccountStatsPresenter.any_instance.expects(:present).twice
          get :account_stats, format: :json

          @request.account.update_attribute(:updated_at, @request.account.updated_at + 1)
          get :account_stats, format: :json
        end

        it "busts cache when rate limit boost changes" do
          refute(@request.account.api_rate_limit_boosted?)
          Api::V2::Internal::AccountStatsPresenter.any_instance.expects(:present).twice
          get :account_stats, format: :json

          @request.account.settings.api_rate_limit_boost = 14.days.from_now.to_i
          @request.account.settings.save!
          assert(@request.account.api_rate_limit_boosted?)
          get :account_stats, format: :json
        end

        it "busts cache when cdn provider changes" do
          Api::V2::Internal::AccountStatsPresenter.any_instance.expects(:present).twice
          get :account_stats, format: :json

          @request.account.settings.cdn_provider = "fiskemanden"
          @request.account.settings.save!
          get :account_stats, format: :json
        end

        it "busts cache when account settings change" do
          Api::V2::Internal::AccountStatsPresenter.any_instance.expects(:present).twice
          get :account_stats, format: :json

          @request.account.settings.voice_trial_enabled = true
          @request.account.settings.save!
          get :account_stats, format: :json
        end
      end

      it "contains remote auth settings" do
        get :account_stats, format: :json
        json = JSON.parse(@response.body)
        assert_equal @account.remote_authentications.active.saml.exists?, json["account_stats"]["settings"]["remote_auth"]["saml"]
      end

      describe "for an account with a feature boost record" do
        before do
          @account.create_feature_boost(expires: 30.days.from_now, active: false)
          get :account_stats, format: :json
        end

        it "contains boost settings" do
          json = JSON.parse(@response.body)
          assert json["account_stats"]["settings"]["boost"].present?
        end
      end
    end

    describe "#cancel_account" do
      before { @request.account.stubs(:is_trial?).returns(true) }
      it "cancels the account" do
        assert Account.find_by_subdomain('minimum').is_active?
        post :cancel_account, format: :json
        refute @request.account.is_active
        refute Account.find_by_name('minimum')
      end

      it "cancels the account without harassing us about owner verification status" do
        @request.account.owner.identities.each { |id| id.update_attributes!(is_verified: false) }
        assert Account.find_by_subdomain('minimum').is_active?
        post :cancel_account, format: :json
        refute @request.account.is_active
        refute Account.find_by_name('minimum')
      end

      it 'enqueues a fraud score job when an account is cancelled' do
        FraudScoreJob.expects(:enqueue_account).with(@request.account, Fraud::SourceEvent::CANCELLED_ACCOUNT, nil).once
        assert Account.find_by_subdomain('minimum').is_active?
        post :cancel_account, params: { format: :json, human_verdict: 1 }
        refute @request.account.is_active
        refute Account.find_by_name('minimum')
      end

      it 'doesnt enqueue a fraud score job when an account is cancelled when not human verified' do
        FraudScoreJob.expects(:enqueue_account).with(@request.account, Fraud::SourceEvent::CANCELLED_ACCOUNT, nil).never
        assert Account.find_by_subdomain('minimum').is_active?
        post :cancel_account, format: :json
        refute @request.account.is_active
        refute Account.find_by_name('minimum')
      end
    end

    describe "#reactivate_account" do
      before do
        @request.account.update_attributes!(is_active: false, is_serviceable: false,
                                            settings: {
                                                is_abusive: true,
                                                risk_assessment: {level: 'suspended', product: 'support', reason: 'auto'}
                                            })
        refute @request.account.reload.is_active
      end

      it "enqueues an unsuspended fraud score job" do
        FraudScoreJob.expects(:enqueue_account).with(@request.account, Fraud::SourceEvent::UNSUSPENDED, nil).once
        post :reactivate_account, format: :json
      end
    end

    describe "#create_voice_usage" do
      let(:current_time_utc) { Time.now.to_i }

      before do
        Timecop.freeze
      end

      after do
        Timecop.return
      end

      describe 'when :serialize_voice_usage Arturo is OFF' do
        before do
          @request.account.stubs(:zuora_voice_usages).returns(stub)
          Arturo.disable_feature!(:serialize_voice_usage)
        end

        it 'adds a zuora voice usage directly' do
          @request.account.zuora_voice_usages.expects(:create!).with(
            units: 20000.0,
            voice_reference_id: "test-#{current_time_utc}",
            usage_type: "fixup"
          )
          put :create_voice_usage, params: { amount: "20", reference: "test", format: :json }
          assert_response 200
        end
      end

      describe 'when :serialize_voice_usage Arturo is ON' do
        before do
          Arturo.enable_feature!(:serialize_voice_usage)
        end

        it 'schedules usage to be processed sequentially in background' do
          ZBC::Voice::UnprocessedUsage.expects(:create!).with(
            account_id: @request.account.id,
            units: -5000.0,
            voice_reference_id: "test-#{current_time_utc}",
            usage_type: "fixup"
          )
          put :create_voice_usage, params: { amount: "-5", reference: "test", format: :json }
          assert_response 200
        end

        it 'ignores commas in usage' do
          ZBC::Voice::UnprocessedUsage.expects(:create!).with(
            account_id: @request.account.id,
            units: 2000000.0,
            voice_reference_id: "test-#{current_time_utc}",
            usage_type: "fixup"
          )
          put :create_voice_usage, params: { amount: "2,000", reference: "test", format: :json }
          assert_response 200
        end

        it 'ignores dollar signs in usage' do
          ZBC::Voice::UnprocessedUsage.expects(:create!).with(
            account_id: @request.account.id,
            units: 30000.0,
            voice_reference_id: "test-#{current_time_utc}",
            usage_type: "fixup"
          )
          put :create_voice_usage, params: { amount: "$30", reference: "test", format: :json }
          assert_response 200
        end

        it 'accepts floats for usage' do
          ZBC::Voice::UnprocessedUsage.expects(:create!).with(
            account_id: @request.account.id,
            units: 250.0,
            voice_reference_id: "test-#{current_time_utc}",
            usage_type: "fixup"
          )
          put :create_voice_usage, params: { amount: 0.25, reference: "test", format: :json }
          assert_response 200
        end

        it 'accepts an empty description' do
          ZBC::Voice::UnprocessedUsage.expects(:create!).with(
            account_id: @request.account.id,
            units: 30000.0,
            voice_reference_id: "fixup-#{current_time_utc}",
            usage_type: "fixup"
          )
          put :create_voice_usage, params: { amount: "30", format: :json }
          assert_response 200
        end

        describe "when entering multiple fixups with the same description" do
          it 'generates a unique voice id for subsequent calls' do
            put :create_voice_usage, params: { amount: "30", reference: "test", format: :json }

            Timecop.travel(1.second.from_now) do
              put :create_voice_usage, params: { amount: "30", reference: "test", format: :json }
            end
            assert_equal ZBC::Zuora::VoiceUsage.where(account_id: @request.account.id).count, 2
            reference_ids = ZBC::Zuora::VoiceUsage.all.map(&:voice_reference_id)
            assert_not_equal reference_ids.first, reference_ids.last
          end
        end
      end
    end

    describe "#generate_twilio_account" do
      it "calls find or create sub account" do
        ZendeskBillingCore::Zuora::Jobs::UpdateVoiceSubAccountJob.expects(:enqueue).with(
          @account.id,
          false,
          'X-Forwarded-For' => nil, 'X-Zendesk-Via' => 'Monitor event', 'X-Zendesk-Via-Reference-ID' => 1234
        )
        put :generate_twilio_account, format: :json
        assert_response 200
      end
    end

    describe "#enable_voice_trial" do
      it "enables voice trial" do
        @account.settings.voice_trial_enabled = false
        @account.save!
        put :enable_voice_trial, format: :json
        assert_response 200
        assert @account.settings.voice_trial_enabled?
      end
    end

    describe "#disable_voice_trial" do
      it "enables voice trial" do
        @account.settings.voice_trial_enabled = true
        @account.save!
        put :disable_voice_trial, format: :json
        assert_response 200
        refute @account.settings.voice_trial_enabled?
      end
    end

    describe "#audit_events" do
      before do
        stub_request(:get, "https://support.zendesk-test.com/api/v2/internal/monitor/agent_id_name_hash.json").to_return(body: "{}")
      end

      let(:api_attributes) { ["action", "message", "source_type", "source_id", "source_display_name", "created_at", "ip_address", "actor_id", "actor_type", "actor_name", "monitor_user_id", "attribute_changes", "zendesk_agent"] }

      describe "auditing system" do
        it "returns subscription audits" do
          subscription = @request.account.subscription
          audit = create_audit_for subscription
          audit.attribute_changes.create!(source: subscription, attribute_name: "foo", old_value: "A", new_value: "B", account: @request.account)

          get :audit_events, params: { source_type: "Subscription", format: :json }

          assert_response 200
          result = JSON.parse(@response.body)

          assert_equal 1, result["audit_events"].size
          assert_equal api_attributes.sort, result["audit_events"][0].keys.sort
          assert_equal({"foo" => ["A", "B"]}, result["audit_events"][0]["attribute_changes"])
          assert_match /T\d\d:\d\d:\d\dZ/, result["audit_events"][0]["created_at"] # correct date format
        end

        it "returns source_type in english even if account locale is set to spanish" do
          spanish = translation_locales(:spanish)
          @request.account.translation_locale = spanish

          subscription = @request.account.subscription
          audit = create_audit_for subscription
          audit.attribute_changes.create!(source: subscription, attribute_name: "foo", old_value: "A", new_value: "B", account: @request.account)

          get :audit_events, params: { source_type: "Subscription", format: :json }

          assert_response 200
          result = JSON.parse(@response.body)

          assert_equal 1, result["audit_events"].size
          assert_equal "Subscription", result["audit_events"][0]["source_type"]
        end

        it "allows me to restrict audits by date" do
          subscription = @request.account.subscription
          audit = create_audit_for subscription
          audit.attribute_changes.create!(source: subscription, attribute_name: "foo", old_value: "A", new_value: "B", account: @request.account)

          # Inside the date range
          get :audit_events, params: { source_type: ["Subscription"], from: "2014-01-01", to: Date.tomorrow.strftime }, format: :json
          assert_response 200
          result = JSON.parse(@response.body)
          assert_equal 1, result["audit_events"].size

          # Outside the date range
          get :audit_events, params: { source_type: ["Subscription"], from: "2011-01-01", to: "2011-01-02" }, format: :json
          assert_response 200
          result = JSON.parse(@response.body)
          assert_equal 0, result["audit_events"].size
        end

        it "returns zuora-subscription audits" do
          subscription = create_zuora_subscription(account: @request.account)
          audit = create_audit_for subscription
          audit.attribute_changes.create!(source: subscription, attribute_name: "foo", old_value: "A", new_value: "B", account: @request.account)

          get :audit_events, params: { source_type: ["ZendeskBillingCore::Zuora::Subscription"], format: :json }

          assert_response 200
          result = JSON.parse(@response.body)
          assert_equal 1, result["audit_events"].size
          assert_equal api_attributes.sort, result["audit_events"][0].keys.sort
          assert_equal('ZendeskBillingCore::Zuora::Subscription', result["audit_events"][0]["source_type"])
          assert_equal({"foo" => ["A", "B"]}, result["audit_events"][0]["attribute_changes"])
          assert_match /T\d\d:\d\d:\d\dZ/, result["audit_events"][0]["created_at"] # correct date format
        end

        it "does not blow up with unfound actor" do
          subscription = @request.account.subscription
          audit = create_audit_for subscription
          audit.update_column(:actor_id, 9999)
          audit.attribute_changes.create!(source: subscription, attribute_name: "foo", old_value: "A", new_value: "B", account: @request.account)

          get :audit_events, params: { source_type: ["Subscription"], format: :json }

          assert_response 200
          result = JSON.parse(@response.body)
          assert_equal 1, result["audit_events"].size
        end

        it "presents legacy agent names" do
          subscription = @request.account.subscription
          audit = create_audit_for subscription
          audit.update_columns(actor_id: -1, via_id: ViaType.IMPORT, via_reference_id: users(:minimum_end_user).id)
          stub_request(:get, "https://support.zendesk-test.com/api/v2/internal/monitor/agent_id_name_hash.json").to_return(body: JSON.dump(users(:minimum_end_user).id => 'Pete'), headers: { content_type: 'application/json' })

          get :audit_events, params: { source_type: ["Subscription"], format: :json }

          assert_response 200
          result = JSON.parse(@response.body)
          assert_equal "Pete", result["audit_events"][0]["actor_name"]
        end

        describe "when filtering by rule subclass" do
          let!(:macro) { @request.account.macros.first }
          let!(:trigger) { @request.account.triggers.first }

          before do
            macro_audit = create_audit_for macro
            macro_audit.attribute_changes.create!(
              source: macro, attribute_name: "foo", old_value: "A", new_value: "B", account: @request.account
            )

            trigger_audit = create_audit_for trigger
            trigger_audit.attribute_changes.create!(
              source: trigger, attribute_name: "foo", old_value: "A", new_value: "B", account: @request.account
            )
          end

          describe "and passing 'Macro' as source_type param" do
            it "presents only macros" do
              get :audit_events, params: { source_type: ['Macro'], format: :json }

              assert_response 200
              result = JSON.parse(@response.body)

              assert_equal 1, result['audit_events'].size
              audit_event = result['audit_events'].first
              assert_equal api_attributes.sort, audit_event.keys.sort
              assert_equal 'Macro', audit_event['source_type']
              assert_equal({'foo' => ['A', 'B']}, audit_event['attribute_changes'])
              assert_match /T\d\d:\d\d:\d\dZ/, audit_event['created_at'] # correct date format
            end
          end

          describe "and passing 'Trigger' as source_type param" do
            it "presents only triggers" do
              get :audit_events, params: { source_type: ['Trigger'], format: :json }

              assert_response 200
              result = JSON.parse(@response.body)

              assert_equal 1, result['audit_events'].size
              audit_event = result['audit_events'].first
              assert_equal api_attributes.sort, audit_event.keys.sort
              assert_equal 'Trigger', audit_event['source_type']
              assert_equal({'foo' => ['A', 'B']}, audit_event['attribute_changes'])
              assert_match /T\d\d:\d\d:\d\dZ/, audit_event['created_at'] # correct date format
            end
          end
        end

        describe 'when passing in page' do
          let!(:macro) { @request.account.macros.first }
          let!(:trigger) { @request.account.triggers.first }

          before do
            macro_audit = create_audit_for macro
            macro_audit.attribute_changes.create!(
              source: macro, attribute_name: "foo", old_value: "A", new_value: "B", account: @request.account
            )

            trigger_audit = create_audit_for trigger
            trigger_audit.attribute_changes.create!(
              source: trigger, attribute_name: "foo", old_value: "A", new_value: "B", account: @request.account
            )
          end

          it 'should present results with given page passed as string/integer' do
            get :audit_events, params: { limit: 1, page: "1", format: :json }

            assert_response 200
            result = JSON.parse(@response.body)

            assert_equal 1, result['audit_events'].size
          end
        end

        describe 'when passing in a limit' do
          let!(:macro) { @request.account.macros.first }
          let!(:trigger) { @request.account.triggers.first }

          before do
            macro_audit = create_audit_for macro
            macro_audit.attribute_changes.create!(
              source: macro, attribute_name: "foo", old_value: "A", new_value: "B", account: @request.account
            )

            trigger_audit = create_audit_for trigger
            trigger_audit.attribute_changes.create!(
              source: trigger, attribute_name: "foo", old_value: "A", new_value: "B", account: @request.account
            )
          end

          it 'should present results with given limit' do
            get :audit_events, params: { limit: 1, format: :json }

            assert_response 200
            result = JSON.parse(@response.body)

            assert_equal 1, result['audit_events'].size
          end

          it 'should present results even with given limit passed as string' do
            get :audit_events, params: { limit: "1", format: :json }

            assert_response 200
            result = JSON.parse(@response.body)

            assert_equal 1, result['audit_events'].size
          end
        end
      end
    end

    describe "#rename_subdomain" do
      it "renames an account's subdomain" do
        post :rename_subdomain, params: { subdomain: "foobarbazer", format: :json }
        assert_response 200
        assert_equal "{}", @response.body
        assert_equal "foobarbazer", @request.account.reload.subdomain
        assert_equal "foobarbazer", @request.account.route.reload.subdomain
      end

      it "fails to rename an account's subdomain to an invalid subdomain" do
        post :rename_subdomain, params: { subdomain: "f", format: :json }
        assert_response 422
        assert_equal '{"error":"subdomain unavailable"}', @response.body
        assert_not_equal "f", @request.account.reload.subdomain
        assert_not_equal "f", @request.account.route.reload.subdomain
      end

      it "fails to rename an account's subdomain to subdomain that's taken by another account" do
        assert Account.find_by_subdomain('support')
        post :rename_subdomain, params: { subdomain: "support", format: :json }
        assert_response 422
        assert_equal '{"error":"subdomain unavailable"}', @response.body
        assert_not_equal "support", @request.account.reload.subdomain
        assert_not_equal "support", @request.account.route.reload.subdomain
      end

      it "fails to rename an account's subdomain to a reserved subdomain" do
        refute Account.find_by_subdomain('admin')
        post :rename_subdomain, params: { subdomain: "admin", format: :json }
        assert_response 422
        assert_equal '{"error":"subdomain unavailable"}', @response.body
        assert_not_equal "admin", @request.account.reload.subdomain
        assert_not_equal "admin", @request.account.route.reload.subdomain
      end

      it "enqueues the Revere sync job" do
        RevereAccountUpdateJob.expects(:enqueue).with(@account.id)
        post :rename_subdomain, params: { subdomain: "foobarbazer", format: :json }
      end
    end

    describe "#rename_brand_subdomain" do
      before do
        @brand = FactoryBot.create(:brand, subdomain: "wombats", account_id: @request.account.id)
      end

      it "renames the brand's subdomain" do
        post :rename_brand_subdomain, params: { subdomain: "partywombat", brand_id: @brand.id, format: :json }
        assert_response :ok
        assert_equal "partywombat", @brand.reload.subdomain
      end

      it "fails to rename an account's subdomain to a subdomain already taken" do
        post :rename_brand_subdomain, params: { subdomain: "minimum", brand_id: @brand.id, format: :json }
        assert_response :unprocessable_entity
        assert_equal '{"error":"Subdomain unavailable"}', @response.body
        assert_not_equal "minimum", @brand.reload.subdomain
      end

      it "fails if the brand is not found" do
        post :rename_brand_subdomain, params: { subdomain: "partywombat", brand_id: 2, format: :json }
        assert_response :not_found
        assert_equal '{"error":"RecordNotFound","description":"Not found"}', @response.body
        assert_not_equal "monkeyfun", @brand.reload.subdomain
      end
    end

    describe "#remove_host_mapping" do
      it "removes host_mapping on an account" do
        @account.host_mapping = "www.example.com"
        @account.save(validate: false)
        @account.reload.host_mapping.must_equal "www.example.com"
        post :remove_host_mapping, format: :json
        @account.reload.host_mapping.must_be_nil
      end
    end

    describe "#zendesk_ids_match?" do
      describe "when accounts match" do
        it "is true" do
          assert @controller.send(:zendesk_ids_match?, 123, "123")
        end
      end

      describe "when ids do not match" do
        it "is false" do
          assert_equal false, @controller.send(:zendesk_ids_match?, 123, "321")
        end
      end

      describe "when any id is blank" do
        it "is false" do
          assert_equal false, @controller.send(:zendesk_ids_match?, nil, "123")
        end
        it "is false" do
          assert_equal false, @controller.send(:zendesk_ids_match?, 123, "")
        end
      end
    end

    describe "#burn_rates" do
      before do
        @account = accounts(:minimum)
      end

      describe "GET" do
        it "sends the request to billing_core and returns ok" do
          ZendeskBillingCore::Zuora::VoiceUsage.expects(:burn_rates).returns([])
          now = Time.zone.now.to_s
          get :burn_rates, params: { start_date_time: now, end_date_time: now, format: :json }
          assert_response :ok
        end
      end
    end

    describe "#update_zuora_subscription" do
      before do
        @account.subscription.create_zuora_subscription(
          zuora_account_id:       'zuora_account_id',
          plan_type:              @account.subscription.plan_type,
          pricing_model_revision: @account.subscription.pricing_model_revision,
          max_agents:             @account.subscription.max_agents
        )
      end

      describe "PUT" do
        describe "with valid zuora_account_id" do
          let(:pravda_client) { @controller.send(:pravda_client) }

          before { @controller.stubs(get_zendesk_id: @account.id) }

          it "changes the zuora_account_id" do
            ZendeskBillingCore::Zuora::Synchronizer.expects(:synchronize!)
            pravda_client.expects(:update_account).with(billing_id: 54321)
            put :update_zuora_subscription, params: { zuora_subscription: { zuora_account_id: 54321 }, format: :json }
            assert_equal '54321', @account.reload.subscription.zuora_subscription.zuora_account_id
          end
        end

        describe "with invalid zuora_account_id" do
          before { @controller.stubs(get_zendesk_id: @account.id.to_s + '1') }

          it "does not change the zuora_account_id" do
            put :update_zuora_subscription, params: { zuora_subscription: { zuora_account_id: 54321 }, format: :json }
            assert_not_equal '00000', @account.reload.subscription.zuora_subscription.zuora_account_id
          end
        end

        describe "with trial account" do
          before { @account.subscription.zuora_subscription.destroy }

          it "returns not_found" do
            put :update_zuora_subscription, params: { zuora_subscription: { zuora_account_id: 54321 }, format: :json }
            assert_response :not_found
          end
        end
      end

      describe "GET" do
        it "returns method_not_allowed" do
          get :update_zuora_subscription, params: { zuora_subscription: { zuora_account_id: 54321 }, format: :json }
          assert_response :method_not_allowed
        end
      end

      describe "POST" do
        it "returns reject action" do
          post :update_zuora_subscription, params: { zuora_subscription: { zuora_account_id: 54321 }, format: :json }
          assert_response :method_not_allowed
        end
      end
    end

    describe "PUT feature_boost" do
      before do
        refute @account.feature_boost
      end

      it "creates a new feature boost" do
        put :feature_boost, params: { feature_boost: {valid_until: 1.day.from_now.to_s(:db), boost_level: SubscriptionPlanType.Medium.to_s}, format: :json }

        assert_response 200
        assert @account.reload.feature_boost
        assert_equal SubscriptionPlanType.Medium, @account.feature_boost.boost_level
      end

      it "updates an existing feature boost" do
        old_boost = @account.create_feature_boost(valid_until: 1.day.from_now)
        put :feature_boost, params: { feature_boost: {boost_level: SubscriptionPlanType.Medium.to_s}, format: :json }
        assert_response 200

        assert_equal old_boost, @account.reload.feature_boost
        assert_equal SubscriptionPlanType.Medium, @account.feature_boost.boost_level
      end
    end

    describe "DELETE feature_boost" do
      it "disables the feature_boost" do
        @account.create_feature_boost(valid_until: 1.day.from_now)
        assert @account.reload.feature_boost.is_active
        delete :feature_boost, format: :json
        refute @account.reload.feature_boost.is_active
      end
    end

    describe "#zopim_subscription_boost" do
      before do
        @zopim_subscription = ZendeskBillingCore::Zopim::Subscription.new
        @zopim_subscription.stubs(
          zopim_account_id: 2000,
          trial_expires_on: '2015-12-01',
          final_trial_expire_date: '2016-03-01'
        )
        @account.stubs(:zopim_subscription).returns(@zopim_subscription)
      end

      describe "PUT zopim_subscription_boost" do
        it "uses the Zopim Reseller client to add a premium boost for basic plan" do
          @zopim_subscription.stubs(plan_type: 'basic')
          Zopim::Reseller.client.expects(:update_account!).with(id: @zopim_subscription.zopim_account_id, data: { premium_boost_date: 10.days.from_now.to_date.strftime("%Y-%m-%d") })

          put :zopim_subscription_boost, params: { premium_boost_date_expiry: 10.days.from_now.to_date.strftime("%Y-%m-%d"), format: :json }
          assert_response 200
        end

        it "does nothing if plan is premium" do
          @zopim_subscription.stubs(plan_type: 'premium')
          Zopim::Reseller.client.expects(:update_account!).never

          put :zopim_subscription_boost, params: { premium_boost_date_expiry: 10.days.from_now.to_date.strftime("%Y-%m-%d"), format: :json }
          assert_response 200
        end

        it "does nothing if plan is trial" do
          @zopim_subscription.stubs(plan_type: 'trial')
          Zopim::Reseller.client.expects(:update_account!).never

          put :zopim_subscription_boost, params: { premium_boost_date_expiry: 10.days.from_now.to_date.strftime("%Y-%m-%d"), format: :json }
          assert_response 200
        end
      end

      describe "DELETE zopim_subscription_boost" do
        before do
          Zopim::Reseller.client.expects(:update_account!).with(id: @zopim_subscription.zopim_account_id, data: { premium_boost_date: Date.tomorrow.midnight.to_date.strftime("%Y-%m-%d") })
        end

        it "uses the Zopim Reseller client to set existing boost to expire tomorrow at midnight" do
          delete :zopim_subscription_boost, format: :json
          assert_response 200
        end
      end
    end

    describe "POST zuora_subscription" do
      before do
        ZendeskBillingCore::Zuora::Jobs::AccountSyncJob.expects(:enqueue).with(1)
      end

      it "enqueues a ZuoraAccountSyncJob" do
        post :zuora_subscription, params: { zuora_account_id: 1, format: 'json' }
        assert_response :success
      end
    end

    describe "POST zuora_approve_subscription" do
      describe "when subscription with zuora_account_id does not exist" do
        it "responds with a HTTP 404 status" do
          nonexistent_id = 171717
          post :zuora_approve_subscription, params: { zuora_account_id: nonexistent_id, format: :json }
          assert_response :not_found
        end
      end

      describe "when subscription with zuora_account_id exists" do
        before do
          zs = stub
          zs.expects(:approval_required=).with(false)
          zs.expects(:save).returns(true)

          ZendeskBillingCore::Zuora::Subscription.expects(:find_by_zuora_account_id).returns(zs)
          post :zuora_approve_subscription, params: { zuora_account_id: 1, format: :json }
        end

        it "responds with a HTTP 200 status" do
          assert_response :ok
        end
      end

      describe "when subscription with zuora_account_id exists but unable to save the change" do
        before do
          zs = stub
          zs.expects(:approval_required=).with(false)
          zs.expects(:save).returns(false)

          ZendeskBillingCore::Zuora::Subscription.expects(:find_by_zuora_account_id).returns(zs)
          post :zuora_approve_subscription, params: { zuora_account_id: 1, format: :json }
        end

        it "responds with a HTTP 500 status" do
          assert_response :unprocessable_entity
        end
      end
    end

    describe "#zuora_sync_subscription" do
      it "enqueues a zuora subscription sync job" do
        ZendeskBillingCore::Zuora::Jobs::AccountSyncJob.expects(:enqueue).with(3)
        put :zuora_sync_subscription, params: { id: 3, format: "json" }
      end
    end

    describe "#agents" do
      describe "regular request" do
        before { get :agents, format: :json }
        should_use_presenter Api::V2::Users::AgentPresenter
      end

      describe "with sorting" do
        before do
          get :agents, params: { format: :json, sort_order: "DESC" }
          @agents = JSON.parse(@response.body)["users"]
        end

        it "is sorted" do
          assert_equal @account.agents.map(&:name).sort.reverse, @agents.map { |agent| agent["name"] }
        end
      end

      describe "with pagination" do
        before do
          get :agents, params: { format: :json, sort_order: "DESC", per_page: 2, page: 1 }
          @agents = JSON.parse(@response.body)["users"]
        end

        it "only returns a subset of agents" do
          assert_equal 2, @agents.count
        end
      end
    end

    describe "#agent_id_name_hash" do
      it "returns a id/name hash" do
        get :agent_id_name_hash, format: :json
        assert_equal(@account.agents.map { |a| [a.id.to_s, a.name] }.sort, JSON.parse(@response.body).to_a.sort)
      end
    end

    describe "#update_dns" do
      before { ActionMailer::Base.perform_deliveries = true }

      it "delivers notice to customers" do
        post :update_dns, params: { certificate_id: certificates(:active2).id, format: :json }
        assert_response :success
        ActionMailer::Base.deliveries.size.must_equal 1
      end

      it "does not deliver for inactive certificates" do
        post :update_dns, params: { certificate_id: certificates(:pending).id, format: :json }
        assert_response :success
        ActionMailer::Base.deliveries.size.must_equal 0
      end
    end

    describe "#reports" do
      before do
        Zendesk::S3ReportingStore.expects(:new).with(Zendesk::Export::BillingReport.filename).returns(stub(fetch: stub(url: 'billing_url')))
        Zendesk::S3ReportingStore.expects(:new).with(Zendesk::Export::BenchmarkingReport.filename).returns(stub(fetch: stub(url: 'benchmarking_url')))

        get :reports, format: :json

        @result = JSON.parse(@response.body)
      end

      it "returns a hash of report URLs" do
        assert_equal 'billing_url', @result['reports']['billing_report.zip']
        assert_equal 'benchmarking_url', @result['reports']['benchmarking.tsv']
      end
    end

    describe "#mobile_devices" do
      after do
        PushNotifications::FirstPartyDeviceIdentifier.without_arsi.delete_all
      end
      describe "regular request" do
        before do
          @agent = FactoryBot.create(:user, account: @account, roles: Role::AGENT.id)
          get :mobile_devices, params: { user_id: @agent.id, format: :json }
        end
        should_use_presenter Api::V2::MobileDevicePresenter
      end

      describe "view devices" do
        before do
          PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:register)
          @agent = FactoryBot.create(:user, account: @account, roles: Role::AGENT.id)
          @device = PushNotifications::FirstPartyDeviceIdentifier.create!(
            user: @agent,
            token: "kJYUTEHjyfu65uyfkht",
            device_type: "iPhone",
            mobile_app: mobile_apps(:com_zendesk_agent)
          )
        end

        it "has one device" do
          get :mobile_devices, params: { user_id: @agent.id, format: :json }

          assert_response :success
          @result = JSON.parse(@response.body)

          assert @result["mobile_devices"]
          assert_equal @device.id, @result["mobile_devices"].first["id"]
        end

        after do
          PushNotifications::FirstPartyDeviceIdentifier.without_arsi.delete_all
          @agent.destroy
        end
      end
    end

    describe "#mobile_device" do
      describe "delete device" do
        before do
          PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:register)
          PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:unregister)
          @agent = FactoryBot.create(:user, account: @account, roles: Role::AGENT.id)
          @device = PushNotifications::FirstPartyDeviceIdentifier.create!(
            user: @agent,
            token: "kJYUTEHjyfu65uyfkht",
            device_type: "iPhone",
            mobile_app: mobile_apps(:com_zendesk_agent)
          )
        end

        it "deletes one device" do
          delete :mobile_device, params: { device_id: @device.id, format: :json }

          assert_response 200
          assert_empty(@agent.devices)
        end

        after do
          PushNotifications::FirstPartyDeviceIdentifier.without_arsi.delete_all
          @agent.destroy
        end
      end
    end

    describe "#reset_mobile_device_push_notif_count" do
      describe "device does not exist" do
        before do
          @device_id = 1234
          put :reset_mobile_device_push_notif_count, params: { device_id: @device_id, format: :json }
        end

        it "responds with 404 Not Found HTTP status code and JSON payload" do
          assert_response 404
          expected_body = { "error" => "No device found by id #{@device_id}" }
          assert_equal JSON.parse(@response.body), expected_body
        end
      end

      describe "device exists" do
        before do
          PushNotificationUpdateJob.stubs(:work)
          PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:register)
          PushNotifications::FirstPartyDeviceIdentifier.any_instance.stubs(:unregister)

          @token = "kJYUTEHjyfu65uyfkht"
          @app = mobile_apps(:com_zendesk_agent)
          @device_type = "iphone"
          agent = FactoryBot.create(:user, account: @account, roles: Role::AGENT.id)

          @device = PushNotifications::FirstPartyDeviceIdentifier.create!(
            user: agent,
            token: @token,
            device_type: @device_type,
            mobile_app: @app
          )

          job_args = {
            account_id: @account.id,
            token: @token,
            device_type: @device_type,
            mobile_app_identifier: @app.identifier,
            badge: 0
          }

          PushNotificationUpdateJob.expects(:enqueue).with(job_args).returns(true)
          put :reset_mobile_device_push_notif_count, params: { device_id: @device.id, format: :json }
        end

        it "responds with HTTP success status and body" do
          assert_response 200
          assert_equal JSON.parse(@response.body), {}
        end
      end
    end

    describe '#append_intermediate_certificate' do
      let(:fake_cert) { 'FAKE' }

      before do
        @cert = @account.certificates.create
        refute @cert.sni_enabled?

        Certificate.any_instance.expects(:append_intermediate).with { |file| File.open(file).read == fake_cert }.returns(nil)

        post :append_intermediate_certificate, params: { cert_id: @cert.id, cert: fake_cert, format: :json }
        @cert.reload
      end

      it 'succeeds' do
        assert_response :success
      end

      it 'converts certificate to sni' do
        assert_equal 1, @cert.certificate_ips.count
        assert(@cert.sni_enabled?)
      end
    end

    describe '#delete_attachment' do
      let(:attachment) { attachments(:attachment_post) }

      it 'deletes the attachment' do
        delete :delete_attachment, params: { token: attachment.token, format: :json }
        assert_response :success
        refute Attachment.where(id: attachment.id).any?, "Attachment not deleted."
      end

      it 'returns 404 if the attachment is not found' do
        Attachment.where(id: attachment.id).destroy_all
        refute Attachment.where(id: attachment.id).any?
        delete :delete_attachment, params: { token: attachment.token, format: :json }
        assert_response :not_found
      end
    end

    describe '#temporary_agent_count' do
      let(:expected_temp_agent_count) { 3 }
      let(:temp_agent_subscription) do
        stub(
          addon_rateplan_info: {
            'temporary_zendesk_agents' => { quantity: expected_temp_agent_count }
          }
        )
      end

      before do
        ZBC::Zuora::Client.any_instance.stubs(:get_temporary_zendesk_agents_products).returns([temp_agent_subscription])
      end

      it "returns the number of temporary agents currently existing on the account" do
        get :temporary_agent_count, params: { zuora_account_id: 1, format: "json" }
        assert_response :success
        assert_equal JSON.parse(@response.body), 'temporary_agent_count' => expected_temp_agent_count
      end
    end

    describe '#reserved_subdomains' do
      let(:expected) do
        {
          'subdomains' => [
            {
              'pattern' => 'admin',
              'is_regex' => false
            }
          ]
        }
      end

      before do
        ReservedSubdomain.without_arsi.delete_all
        ReservedSubdomain.new.tap do |rs|
          rs.pattern = 'admin'
        end.save!

        accept :json
        get :reserved_subdomains
      end

      it 'returns a list of reserved subdomains' do
        assert_response :success
        assert_equal expected, JSON.parse(@response.body)
      end
    end

    describe '#add_reserved_subdomain' do
      let(:expected) do
        {
          'subdomain' => {
            'pattern' => "nosubdomain",
            'is_regex' => false,
          }
        }
      end

      before do
        accept :json
        ReservedSubdomain.without_arsi.delete_all
      end

      it 'requires a `pattern` parameter' do
        post :add_reserved_subdomain, params: { is_regex: false }

        assert_response :bad_request
      end

      it 'requires a `is_regex` parameter' do
        post :add_reserved_subdomain, params: { pattern: false }

        assert_response :bad_request
      end

      it 'returns the new record' do
        post :add_reserved_subdomain, params: { pattern: "nosubdomain", is_regex: false }

        assert_response :created
        assert_equal expected, JSON.parse(@response.body)
      end
    end
  end

  describe "#voice_failover_activated" do
    before do
      @controller.stubs(:current_registered_user).returns(Zendesk::SystemUser.find_subsystem_user_by_name("zendesk"))
    end

    it "enables" do
      @account.settings.voice_failover_activated = false
      @account.save!

      put :enable_voice_failover_activated, format: :json

      assert_response 200
      assert @account.settings.reload.voice_failover_activated
    end

    it "disables" do
      @account.settings.voice_failover_activated = true
      @account.save!

      put :disable_voice_failover_activated, format: :json

      assert_response 200
      refute @account.settings.reload.voice_failover_activated
    end

    it "responds 405 when method is not allowed" do
      post :disable_voice_failover_activated, format: :json
      assert_response 405
    end
  end
end
