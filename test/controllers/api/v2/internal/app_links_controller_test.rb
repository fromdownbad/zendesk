require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::AppLinksController do
  fixtures :accounts, :users, :role_settings, :app_links

  before do
    accept :json
  end

  with_options(controller: 'api/v2/internal/app_links') do |request|
    request.should_route :get, '/api/v2/internal/app_links', action: 'index', format: 'json'
    request.should_route :get, '/api/v2/internal/app_links/123', action: 'show', id: 123, format: 'json'
    request.should_route :post, '/api/v2/internal/app_links', action: 'create', format: 'json'
    request.should_route :put, '/api/v2/internal/app_links/123', action: 'update', id: 123, format: 'json'
    request.should_route :delete, '/api/v2/internal/app_links/123', action: 'destroy', id: 123, format: 'json'
  end

  let(:app) { app_links(:scarlett) }
  let(:app_json) do
    {
      'id' => app.id,
      'app_id' => app.app_id,
      'package_name' => app.package_name,
      'paths' => app.paths,
      'fingerprints' => app.fingerprints,
      'url' => api_v2_internal_app_link_url(app)
    }
  end

  should_be_unauthorized_when_not_logged_in([:index, :show, :update, :create, :destroy], nil, 404)
  should_be_unauthorized_when_not_logged_in([:index, :show, :update, :create, :destroy], 'minimum_admin', 403)
  should_be_unauthorized_when_not_logged_in([:index, :show, :update, :create, :destroy], 'minimum_agent', 403)
  should_be_unauthorized_when_not_logged_in([:index, :show, :update, :create, :destroy], 'minimum_end_user', 403)

  as_a_subsystem_user(account: :minimum) do
    describe '#index' do
      it 'returns all the app link data' do
        get :index
        app_links = JSON.parse(response.body)

        assert_response :ok
        assert_equal [app_json], app_links['app_links']
      end
    end

    describe '#show' do
      it 'returns one app link data' do
        get :show, params: { id: app.id }
        app_links = JSON.parse(response.body)

        assert_response :ok
        assert_equal app_json, app_links['app_link']
      end
    end

    describe '#update' do
      it 'updates an app link' do
        put :update, params: { id: app.id, app_link: { app_id: 'NEW', package_name: 'NEW', paths: %w[p1 p2], fingerprints: %w[f1 f2] } }
        app.reload

        assert_response :ok
        assert_equal 'NEW', app.app_id
        assert_equal 'NEW', app.package_name
        assert_equal %w[p1 p2], app.paths
        assert_equal %w[f1 f2], app.fingerprints
      end

      it 'fails to update an app link' do
        put :update, params: { id: app.id, app_link: { app_id: '', package_name: '' } }

        body = JSON.parse(response.body)

        assert_response :unprocessable_entity
        assert_equal 'RecordInvalid', body['error']
        assert_equal [{'description' => 'App cannot be blank', 'error' => 'BlankValue' }], body['details']['app_id']
        assert_equal [{'description' => 'Package name cannot be blank', 'error' => 'BlankValue' }], body['details']['package_name']
      end
    end

    describe '#create' do
      it 'creates a new app link' do
        post :create, params: { app_link: { app_id: 'NEW', package_name: 'NEW', paths: %w[p1 p2], fingerprints: %w[f1 f2] } }

        app = AppLink.last

        assert_response :ok
        assert_equal 'NEW', app.app_id
        assert_equal 'NEW', app.package_name
        assert_equal %w[p1 p2], app.paths
        assert_equal %w[f1 f2], app.fingerprints
      end

      it 'fails to create a new app link' do
        post :create, params: { app_link: { app_id: '', package_name: '' } }

        body = JSON.parse(response.body)

        assert_response :unprocessable_entity
        assert_equal 'RecordInvalid', body['error']
        assert_equal [{'description' => 'App cannot be blank', 'error' => 'BlankValue' }], body['details']['app_id']
        assert_equal [{'description' => 'Package name cannot be blank', 'error' => 'BlankValue' }], body['details']['package_name']
      end
    end

    describe '#delete' do
      it 'deletes an app link' do
        assert_difference -> { AppLink.count }, -1 do
          delete :destroy, params: { id: app.id }
        end
      end
    end
  end
end
