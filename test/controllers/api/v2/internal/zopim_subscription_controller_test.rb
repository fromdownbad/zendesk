require_relative '../../../../support/test_helper'
require_relative '../../../../support/account_products_test_helper'
require_relative '../../../../support/zopim_test_helper'
require_relative '../../../../support/chat_phase_3_helper'
require_relative '../../../../support/suite_test_helper'

SingleCov.covered! uncovered: 6

describe Api::V2::Internal::ZopimSubscriptionController do
  include ZopimTestHelper
  include ChatPhase3Helper
  include AccountProductsTestHelper
  include SuiteTestHelper

  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    Timecop.freeze('2014-02-24')

    Account.any_instance.stubs(has_chat_permission_set?: true)
    Zopim::AgentSyncJob.stubs(:work)

    ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.stubs(:enqueue)
    Zopim::Reseller.client.stubs(
      account!: stub(trial_end_date: 30.days.from_now),
      create_account!: Hashie::Mash.new(id: 1),
      account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
      find_accounts!: []
    )

    set_up_stubs

    accept :json
  end

  with_options(controller: 'api/v2/internal/zopim_subscription') do |request|
    request.should_route :get,    '/api/v2/internal/zopim_subscription',                              action: 'show'
    request.should_route :post,   '/api/v2/internal/zopim_subscription/phase_three',                  action: 'create_phase_three'
    request.should_route :put,    '/api/v2/internal/zopim_subscription/phase_three',                  action: 'update_phase_three'
    request.should_route :delete, '/api/v2/internal/zopim_subscription/reset',                        action: 'reset'
    request.should_route :put,    '/api/v2/internal/zopim_subscription/toggle_status',                action: 'toggle_status'
    request.should_route :put,    '/api/v2/internal/zopim_subscription/toggle_app_installation',      action: 'toggle_app_installation'
    request.should_route :post,   '/api/v2/internal/zopim_subscription/restore_integration',          action: 'restore_integration'
    request.should_route :post,   '/api/v2/internal/zopim_subscription/restore_phase_3_integration',  action: 'restore_phase_3_integration'
    request.should_route :put,    '/api/v2/internal/zopim_subscription/extend_trial_to_date',         action: 'extend_trial_to_date'
    request.should_route :put,    '/api/v2/internal/zopim_subscription/restore_trial_status',         action: 'restore_trial_status'
    request.should_route :put,    '/api/v2/internal/zopim_subscription/restore_phase',                action: 'restore_phase'
  end

  describe "embeddable subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "embeddable") do
      should_be_forbidden(
        [:delete, :reset, {}],
        [:post, :create_phase_three, {}],
        [:post, :restore_integration, {}],
        [:post, :restore_phase_3_integration, {}],
        [:put, :extend_trial_to_date, {}],
        [:put, :restore_phase, {}],
        [:put, :restore_trial_status, {}],
        [:put, :toggle_app_installation, {}],
        [:put, :toggle_status, {}],
        [:put, :update_phase_three, {}]
      )

      describe "a GET to :show" do
        let(:current_account) { @controller.send(:current_account) }

        before do
          setup_zopim_subscription(current_account)
          get :show
        end

        it "responds with success" do
          assert_response :success
        end
      end
    end
  end

  as_a_subsystem_user(account: :minimum) do
    let(:current_account) { @controller.send(:current_account) }
    let(:current_user) { @controller.send(:current_user) }
    let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 100, phase: 3, plan_type: 1 }) }

    def post_chat_phase_three(user_id = nil)
      if user_id.nil?
        post :create_phase_three, params: { product: chat_product_payload }
      else
        post :create_phase_three, params: { product: chat_product_payload, user_id: user_id }
      end
    end

    describe "when there is no chat-account" do
      describe "#show" do
        before { get :show }
        it('responds with not_found') { assert_response :not_found }
      end

      describe '#restore_phase' do
        before { put :restore_phase, params: { phase: 3 } }
        it('responds with bad_request') { assert_response :not_found }
      end

      describe '#create_phase_three' do
        let(:minimum_agent) { users(:minimum_agent) }

        describe 'when the request is valid' do
          describe 'and there is no phase 1 zopim integration' do
            before do
              post_chat_phase_three
            end

            it 'responds with 204 and creates zopim subscription and integration' do
              assert_response :no_content
              assert current_account.reload.zopim_subscription.present?, 'ZopimSubscription was not created.'
              assert_equal ZBC::Zopim::SubscriptionStatus::Active, current_account.reload.zopim_subscription.status
              assert_equal 'lite', current_account.zopim_subscription.plan_type
              assert_equal current_account.zopim_subscription.max_agents, 100
              assert_equal current_account.zopim_integration.phase, 3, 'ZopimIntegration was not created with phase three.'
            end
          end

          describe 'and there is already a phase 1 zopim integration' do
            before do
              create_zopim_integration(current_account, 1)
              post_chat_phase_three
            end

            it('responds with 422') do
              assert_response :unprocessable_entity
              assert_nil current_account.reload.zopim_subscription, 'ZopimSubscription should not be created.'
            end
          end
        end

        describe 'when the request is made with user_id' do
          let(:owner) { users(:minimum_admin) }

          describe 'set to the owner' do
            let(:activator) { users(:minimum_admin) }

            before { post_chat_phase_three(activator.id) }

            it 'creates zopim identity for the specified user' do
              assert_response :no_content
              assert current_account.reload.zopim_subscription.present?, 'ZopimSubscription was not created.'
              assert_equal ZBC::Zopim::SubscriptionStatus::Active, current_account.reload.zopim_subscription.status
              assert_equal 'lite', current_account.zopim_subscription.plan_type
              assert_equal current_account.zopim_subscription.max_agents, 100
              assert_equal current_account.zopim_integration.phase, 3, 'ZopimIntegration was not created with phase three.'
              assert owner.reload.zopim_identity.is_serviceable?
              assert activator.reload.zopim_identity.is_serviceable?
            end
          end

          describe 'set to an admin who is not the owner' do
            let(:activator) { users(:minimum_admin_not_owner) }

            before { post_chat_phase_three(activator.id) }

            it 'creates zopim identity for the specified user' do
              assert_response :no_content
              assert current_account.reload.zopim_subscription.present?, 'ZopimSubscription was not created.'
              assert_equal ZBC::Zopim::SubscriptionStatus::Active, current_account.reload.zopim_subscription.status
              assert_equal 'lite', current_account.zopim_subscription.plan_type
              assert_equal current_account.zopim_subscription.max_agents, 100
              assert_equal current_account.zopim_integration.phase, 3, 'ZopimIntegration was not created with phase three.'
              assert owner.reload.zopim_identity.is_serviceable?
              assert activator.reload.zopim_identity.is_serviceable?
            end
          end

          describe 'set to an agent' do
            let(:activator) { users(:minimum_agent) }

            before { post_chat_phase_three(activator.id) }

            it 'creates zopim identity for the specified user' do
              assert_response :no_content
              assert current_account.reload.zopim_subscription.present?, 'ZopimSubscription was not created.'
              assert_equal ZBC::Zopim::SubscriptionStatus::Active, current_account.reload.zopim_subscription.status
              assert_equal 'lite', current_account.zopim_subscription.plan_type
              assert_equal current_account.zopim_subscription.max_agents, 100
              assert_equal current_account.zopim_integration.phase, 3, 'ZopimIntegration was not created with phase three.'
              assert owner.reload.zopim_identity.is_serviceable?
              assert activator.reload.zopim_identity.is_serviceable?
            end
          end

          describe 'set to an end-user' do
            let(:activator) { users(:minimum_end_user) }

            before { post_chat_phase_three(activator.id) }

            it 'does not create zopim identity for the specified user' do
              assert_response :no_content
              assert current_account.reload.zopim_subscription.present?, 'ZopimSubscription was not created.'
              assert_equal ZBC::Zopim::SubscriptionStatus::Active, current_account.reload.zopim_subscription.status
              assert_equal 'lite', current_account.zopim_subscription.plan_type
              assert_equal current_account.zopim_subscription.max_agents, 100
              assert_equal current_account.zopim_integration.phase, 3, 'ZopimIntegration was not created with phase three.'
              assert owner.reload.zopim_identity.is_serviceable?
              assert_nil activator.reload.zopim_identity
            end
          end

          describe 'set to a bad user' do
            before { post_chat_phase_three(123456) }

            it 'does not create zopim identity for the specified user' do
              assert_response :no_content
              assert current_account.reload.zopim_subscription.present?, 'ZopimSubscription was not created.'
              assert_equal ZBC::Zopim::SubscriptionStatus::Active, current_account.reload.zopim_subscription.status
              assert_equal 'lite', current_account.zopim_subscription.plan_type
              assert_equal current_account.zopim_subscription.max_agents, 100
              assert_equal current_account.zopim_integration.phase, 3, 'ZopimIntegration was not created with phase three.'
              assert owner.reload.zopim_identity.is_serviceable?
            end
          end
        end

        describe 'when trial_expires_at is in the past' do
          let(:chat_product_payload) { make_product(product_name: 'chat', trial_expires_at: Date.yesterday, plan_settings: { max_agents: 100, phase: 3, plan_type: 1 }) }
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it('sets trial days to zero') do
            assert_equal current_account.zopim_subscription.trial_days, 0
            assert_response :no_content
          end
        end

        describe 'when trial_expires_at is nil' do
          let(:chat_product_payload) { make_product(product_name: 'chat', trial_expires_at: nil, plan_settings: { max_agents: 100, phase: 3, plan_type: 1 }) }
          before do
            post :create_phase_three, params: { product: chat_product_payload }
          end

          it 'sets trial days to default' do
            assert_response :no_content
            assert_equal current_account.zopim_subscription.trial_days, ZBC::Zopim::Subscription::TrialManagement::DEFAULT_TRIAL_LENGTH_IN_DAYS
          end
        end

        describe 'when plan_type is nil' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 100, phase: 3, plan_type: nil }) }
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end

        describe 'when max_agents is nil' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: nil, phase: 3, plan_type: 1 }) }
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end

        describe 'when phase is nil' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 100, phase: nil, plan_type: nil }) }
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end

        describe 'when phase is not 3' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 100, phase: 2, plan_type: 1 }) }
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end

        describe 'when the product is in subscribed state' do
          let(:chat_product_payload) { make_product(product_name: 'chat', state: 'subscribed', plan_settings: { max_agents: 100, phase: 3, plan_type: 1 }) }
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it 'responds with 201 and sets purchased_at' do
            assert_response :no_content
            assert current_account.zopim_subscription.purchased_at.present?, "ZopimSubscription#purchased_at should be set"
          end
        end
      end

      describe '#update_phase_three' do
        describe 'when the request is valid but there is no zopim_* records' do
          before { put :update_phase_three, params: { product: chat_product_payload } }
          it('responds with no_content and provisions zopim_* records') do
            assert_response :no_content
            assert current_account.reload.zopim_subscription.present?, "ZopimSubscription was not present."
            assert_equal ZBC::Zopim::SubscriptionStatus::Active, current_account.reload.zopim_subscription.status
            assert_equal 'lite', current_account.zopim_subscription.plan_type
            assert_equal 100, current_account.zopim_subscription.max_agents
            assert_equal current_account.zopim_integration.phase, 3, "ZopimIntegration was not present with phase three."
          end
        end
      end
    end

    describe 'when there is already a zopim subscription' do
      before do
        create_phase_3_zopim_subscription(current_account)
      end

      describe '#create_phase_three' do
        describe 'when the request is valid' do
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it('responds with no content') do
            assert_response :no_content
          end
        end

        describe 'when the account does not have a zopim integration' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 101, phase: 3, plan_type: 1 }) }
          before do
            Account.any_instance.stubs(:zopim_integration).returns(nil)
            put :update_phase_three, params: { product: chat_product_payload }
          end
          it('responds with 422') do
            assert_response :unprocessable_entity
          end
        end

        describe 'when plan_type is nil' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 100, phase: 3, plan_type: nil }) }
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end

        describe 'when max_agents is nil' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: nil, phase: 3, plan_type: 1 }) }
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end

        describe 'when phase is nil' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 100, phase: nil, plan_type: nil }) }
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end

        describe 'when phase is -1' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 100, phase: -1, plan_type: 1 }) }
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end

        describe 'when phase is not 3' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 100, phase: 2, plan_type: 1 }) }
          before { post :create_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end
      end

      describe '#update_phase_three' do
        describe 'when the request is valid' do
          describe 'and the current zopim_integration is phase 3' do
            let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 101, phase: 3, plan_type: 1 }) }
            before do
              put :update_phase_three, params: { product: chat_product_payload }
            end

            it 'responds with 204' do
              assert_response :no_content
              assert current_account.reload.zopim_subscription.present?, "ZopimSubscription was not present."
              assert_equal ZBC::Zopim::SubscriptionStatus::Active, current_account.reload.zopim_subscription.status
              assert_equal 'lite', current_account.zopim_subscription.plan_type
              assert_equal chat_product_payload['plan_settings'][:max_agents], current_account.zopim_subscription.max_agents
              assert_equal current_account.zopim_integration.phase, 3, "ZopimIntegration was not present with phase three."
            end
          end

          describe 'and the current zopim_integration is not phase 3' do
            let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 101, phase: 3, plan_type: 1 }) }
            before do
              put :update_phase_three, params: { product: chat_product_payload }
              current_account.zopim_integration.update_column(:phase, 0)
            end
            it('responds with 204') do
              assert_response :no_content
              assert current_account.reload.zopim_subscription.present?, "ZopimSubscription was not present."
              assert_equal ZBC::Zopim::SubscriptionStatus::Active, current_account.reload.zopim_subscription.status
              assert_equal 'lite', current_account.zopim_subscription.plan_type
              assert_equal chat_product_payload['plan_settings'][:max_agents], current_account.zopim_subscription.max_agents
              assert_equal current_account.zopim_integration.phase, 2
            end
          end
        end

        describe 'when zopim_subscription has created_at' do
          let(:chat_product_payload) { make_product(product_name: 'chat', trial_expires_at: Date.today, plan_settings: { max_agents: 100, phase: 3, plan_type: 1 }) }
          before do
            current_account.zopim_subscription.update_column(:created_at, Date.today - 10)
            current_account.zopim_subscription.reload
            put :update_phase_three, params: { product: chat_product_payload }
          end

          it 'calculates trial days using the zopim_subscription#created_at field' do
            assert_response :no_content
            assert_equal 10, current_account.zopim_subscription.trial_days
          end
        end

        describe 'when trial_expires_at is in the past' do
          let(:chat_product_payload) { make_product(product_name: 'chat', trial_expires_at: Date.yesterday, plan_settings: { max_agents: 100, phase: 3, plan_type: 1 }) }
          before { put :update_phase_three, params: { product: chat_product_payload } }
          it('sets trial days to zero') do
            assert_response :no_content
            assert_equal 0, current_account.zopim_subscription.trial_days
          end
        end

        describe 'when trial_expires_at is nil' do
          let(:chat_product_payload) { make_product(product_name: 'chat', trial_expires_at: nil, plan_settings: { max_agents: 100, phase: 3, plan_type: 1 }) }
          before { put :update_phase_three, params: { product: chat_product_payload } }
          it('sets trial days to default') do
            assert_equal current_account.zopim_subscription.trial_days, ZBC::Zopim::Subscription::TrialManagement::DEFAULT_TRIAL_LENGTH_IN_DAYS
            assert_response :no_content
          end
        end

        describe 'when the request updates the subscription with the deleted state' do
          let(:chat_product_payload) { make_product(product_name: 'chat', state: 'deleted', plan_settings: { max_agents: 101, phase: 3, plan_type: 1 }) }
          before { put :update_phase_three, params: { product: chat_product_payload } }
          it('responds with 204') do
            assert_response :no_content
            assert current_account.reload.zopim_subscription.nil?, "ZopimSubscription was not destroyed."
            assert current_account.zopim_integration.nil?, "ZopimIntegration was not destroyed."
          end
        end

        describe 'when the request updates the subscription with the cancelled state' do
          let(:chat_product_payload) { make_product(product_name: 'chat', state: 'cancelled', plan_settings: { max_agents: 101, phase: 3, plan_type: 1 }) }
          before { put :update_phase_three, params: { product: chat_product_payload } }
          it('responds with 204') do
            assert_response :no_content
            assert current_account.reload.zopim_subscription.nil?, "ZopimSubscription was not destroyed."
            assert current_account.zopim_integration.nil?, "ZopimIntegration was not destroyed."
          end
        end

        describe 'when the request updates the subscription with the cancelled state and the deleted phase' do
          let(:chat_product_payload) { make_product(product_name: 'chat', state: 'cancelled', plan_settings: { max_agents: 101, phase: -1, plan_type: 1 }) }
          before { put :update_phase_three, params: { product: chat_product_payload } }
          it('responds with 204') do
            assert_response :no_content
            assert current_account.reload.zopim_subscription.nil?, "ZopimSubscription was not destroyed."
            assert current_account.zopim_integration.nil?, "ZopimIntegration was not destroyed."
          end
        end

        describe 'when the request updates state to subscribed' do
          let(:chat_product_payload) { make_product(product_name: 'chat', state: 'subscribed', plan_settings: { max_agents: 100, phase: 3, plan_type: 2 }) }
          before { put :update_phase_three, params: { product: chat_product_payload } }
          it('responds with 204') do
            assert_response :no_content
            assert current_account.zopim_subscription.purchased_at.present?, "ZopimSubscription#purchased_at should be set"
          end
        end

        describe 'when the request updates state to trial' do
          let(:chat_product_payload) { make_product(product_name: 'chat', state: 'trial', plan_settings: { max_agents: 100, phase: 3, plan_type: 2 }) }
          before do
            current_account.zopim_subscription.update_column(:purchased_at, DateTime.yesterday)
            current_account.zopim_subscription.reload
            put :update_phase_three, params: { product: chat_product_payload }
          end

          it('responds with 204 and set zopim_subscription#purchased_at to nil') do
            assert_response :no_content
            assert current_account.zopim_subscription.purchased_at.nil?, "ZopimSubscription#purchased_at should be nil"
          end
        end

        describe 'when plan_type is nil' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 100, phase: 3, plan_type: nil }) }
          before { put :update_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end

        describe 'when max_agents is nil' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: nil, phase: 3, plan_type: 1 }) }
          before { put :update_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end

        describe 'when phase is nil' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 100, phase: nil, plan_type: nil }) }
          before { put :update_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end

        describe 'when phase is not 3' do
          let(:chat_product_payload) { make_product(product_name: 'chat', plan_settings: { max_agents: 100, phase: 2, plan_type: 1 }) }
          before { put :update_phase_three, params: { product: chat_product_payload } }
          it('responds with 400') { assert_response :bad_request }
        end
      end
    end

    describe 'when there is no zopim subscription' do
      describe '#update_phase_three' do
        describe 'when the request updates the subscription with the cancelled state' do
          let(:chat_product_payload) { make_product(product_name: 'chat', state: 'cancelled', plan_settings: { max_agents: 101, phase: 3, plan_type: 1 }) }

          before do
            put :update_phase_three, params: { product: chat_product_payload }
          end

          it('responds with 204') do
            assert_response :no_content
          end
        end

        describe 'when the request updates the subscription with the cancelled state and the deleted phase' do
          let(:chat_product_payload) { make_product(product_name: 'chat', state: 'cancelled', plan_settings: { max_agents: 101, phase: -1, plan_type: 1 }) }

          before do
            put :update_phase_three, params: { product: chat_product_payload }
          end

          it('responds with 204') do
            assert_response :no_content
          end
        end
      end
    end

    describe "when there is a chat-account" do
      before do
        setup_zopim_subscription(current_account)
      end

      describe "#show" do
        before { get :show }
        it('responds with success') { assert_response :success }
      end

      describe "#reset" do
        describe "purchased account" do
          before do
            ZendeskBillingCore::Zopim::Subscription.any_instance.
              stubs(
                purchased_at: '2016-01-01',
                is_cancelled?: false
              )
            delete :reset
          end

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
        end

        describe "when a trial or lite non-purchased subscription" do
          before do
            ZendeskBillingCore::Zopim::Subscription.any_instance.
              stubs(
                is_cancelled?: false,
                purchased_at: nil
              )
          end

          it "resets the chat-account" do
            ZendeskBillingCore::Zopim::Subscription.any_instance.expects(:destroy)
            delete :reset
          end
        end

        describe "when the zopim_subscription is cancelled after previous purchase" do
          before do
            ZendeskBillingCore::Zopim::Subscription.any_instance.
              stubs(
                is_cancelled?: true,
                purchased_at: '2016-01-01'
              )
          end

          it "resets the chat-account" do
            ZendeskBillingCore::Zopim::Subscription.any_instance.expects(:destroy)
            delete :reset
          end
        end
      end

      describe "#toggle_status" do
        describe "when it is in a suspended state" do
          before do
            ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(is_active?: false)
          end

          it "activates the chat-account" do
            ZendeskBillingCore::Zopim::Subscription.any_instance.expects(:activate!)
            put :toggle_status
          end
        end

        describe "when it is not in a suspended state" do
          before do
            ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(is_active?: true)
          end

          it "suspends the chat-account" do
            ZendeskBillingCore::Zopim::Subscription.any_instance.expects(:suspend!)
            put :toggle_status
          end
        end
      end

      describe "#toggle_app_installation" do
        describe "for agent_workspace account" do
          before do
            current_account.settings.polaris = true
            current_account.save!
          end

          describe "when the app is not installed" do
            before do
              ZopimIntegration.any_instance.stubs(app_installed?: false)
            end

            it "remains uninstalled" do
              ZopimIntegration.any_instance.expects(:install_app).never
              ZopimIntegration.any_instance.expects(:uninstall_app).never
              put :toggle_app_installation

              assert_response :not_modified
            end
          end

          describe "when the app is installed" do
            before do
              ZopimIntegration.any_instance.stubs(app_installed?: true)
            end

            it "remains installed" do
              ZopimIntegration.any_instance.expects(:install_app).never
              ZopimIntegration.any_instance.expects(:uninstall_app).never
              put :toggle_app_installation

              assert_response :not_modified
            end
          end
        end

        describe "for non-agent workspace account" do
          describe "when the app is not installed" do
            before do
              ZopimIntegration.any_instance.stubs(app_installed?: false)
            end

            it "installs the app" do
              ZopimIntegration.any_instance.expects(:install_app)
              put :toggle_app_installation

              assert_response :ok
            end
          end

          describe "when the app is installed" do
            before do
              ZopimIntegration.any_instance.stubs(app_installed?: true)
            end

            it "suspends the chat-account" do
              ZopimIntegration.any_instance.expects(:uninstall_app)
              put :toggle_app_installation

              assert_response :ok
            end
          end
        end
      end

      describe "#restore_integration" do
        let(:current_zopim_subscription) do
          current_account.zopim_subscription
        end

        describe "when the integration record is missing" do
          let(:zopim_account_data) do
            stub(id: 1, account_key: 'account-key')
          end

          before do
            Zopim::Reseller.client.stubs(:account!).
              with(id: current_zopim_subscription.zopim_account_id).
              returns(zopim_account_data)
            current_account.zopim_integration.destroy
            current_account.reload
            post :restore_integration
          end

          it('responds with success') { assert_response :success }

          it "restores the integration record" do
            assert current_account.zopim_integration.present?
          end
        end

        describe "when the integration record already exists" do
          before do
            assert current_account.zopim_integration.present?
            post :restore_integration
          end

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
        end
      end

      describe "#restore_phase_3_integration" do
        let(:current_zopim_subscription) do
          current_account.zopim_subscription
        end

        describe "when the integration record is missing" do
          before do
            current_account.zopim_integration.destroy
            current_account.reload
            post :restore_phase_3_integration, params: { zopim_key: 'abc' }
          end

          it "restores the integration record" do
            assert_response :success
            assert current_account.zopim_integration.present?
            assert_equal current_account.zopim_integration.phase, 3
          end
        end

        describe "when the integration record already exists" do
          before do
            assert current_account.zopim_integration.present?
            post :restore_phase_3_integration, params: { zopim_key: 'abc' }
          end

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
        end
      end

      describe "#extend_trial_to_date" do
        let(:current_zopim_subscription) do
          current_account.zopim_subscription
        end

        let(:zopim_account_id) do
          current_zopim_subscription.zopim_account_id
        end

        describe "when the trial is extended" do
          before do
            Zopim::Reseller.client.expects(:update_account!).
              with(id: zopim_account_id, data: { plan: 'trial', trial_days: 1000 })

            ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(trial_days: 30)

            ZendeskBillingCore::Zopim::Subscription.any_instance.expects(:update_column).
              with(:trial_days, 1000).once.returns(true)

            ZendeskBillingCore::Zopim::Subscription.any_instance.expects(:update_column).
              with(:trial_end_at, nil).once.returns(true)

            put :extend_trial_to_date, params: { expire_date: 1000.days.from_now }
          end

          it('responds with success') { assert_response :success }
        end

        describe "when the trial is not extended" do
          before do
            put :extend_trial_to_date, params: { expire_date: 1001.days.from_now }
          end

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }

          it "explains why in the response body" do
            expected = 'InvalidTrialExtensionDate'
            actual   = JSON.parse(response.body)['error']
            assert_equal expected, actual
          end
        end
      end

      describe "#restore_trial_status" do
        let(:current_zopim_subscription) do
          current_account.zopim_subscription
        end

        let(:zopim_account_id) do
          current_zopim_subscription.zopim_account_id
        end

        let(:default_payload) do
          {
            agents: current_zopim_subscription.max_agents,
            plan:   current_zopim_subscription.plan_type
          }
        end

        before do
          current_zopim_subscription.
            update_column(:plan_type, ZendeskBillingCore::Zopim::PlanType::Lite.name)
        end

        describe "when the trial status is restored" do
          before do
            payload = default_payload.merge(plan: 'trial', trial_days: 1000)

            Zopim::Reseller.client.expects(:update_account!).
              with(id: zopim_account_id, data: payload)

            put :restore_trial_status, params: { expire_date: 1000.days.from_now }
          end

          it('responds with success') { assert_response :success }

          it "should set the plan type to 'trial'" do
            expected = 'trial'
            actual   = current_zopim_subscription.reload.plan_type
            assert_equal expected, actual
          end

          it "should move the trial expiration date to expire_date" do
            expected = 1000.days.from_now.to_date
            actual   = current_zopim_subscription.reload.trial_expires_on
            assert_equal expected, actual
          end
        end

        describe "when the trial is not restored" do
          before do
            current_zopim_subscription.update_column(:purchased_at, Date.today)
            current_zopim_subscription.reload
            put :restore_trial_status, params: { expire_date: 1000.days.from_now }
          end

          it 'responds with unprocessable_entity' do
            assert_response :unprocessable_entity
          end

          it "should explain why in the response body" do
            expected = 'IneligibleForTrialRestoration'
            actual   = JSON.parse(response.body)['error']
            assert_equal expected, actual
          end
        end
      end
    end

    describe '#restore_phase' do
      let(:current_zopim_subscription) do
        current_account.zopim_subscription
      end

      let(:current_zopim_integration) do
        current_account.zopim_integration
      end

      let(:expected_payload) { {} }

      before do
        ZBC::Zopim::Subscription.include(ChatPhaseThree)
        setup_zopim_subscription(current_account)
      end

      describe 'when the phase is invalid' do
        let(:phase) { 2 }

        it 'responds with bad_request' do
          put :restore_phase, params: { phase: phase }
          assert_response :bad_request
        end
      end

      describe 'when the phase is 3' do
        let(:phase) { 3 }

        it 'sets the zopim_integration#phase to 3 and copies zopim_subscription info to chat product record' do
          ZBC::Zopim::Subscription.any_instance.expects(:make_phase_three).once
          put :restore_phase, params: { phase: phase }
          assert_equal current_zopim_integration.phase, 3
        end
      end
    end
  end
end
