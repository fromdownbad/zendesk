require_relative '../../../../support/test_helper'
require_relative '../../../../support/inbound_mail_rate_limit_test_helper'

SingleCov.covered!

describe Api::V2::Internal::InboundMailRateLimitsController do
  extend Api::V2::TestHelper

  include InboundMailRateLimitTestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:email) { 'akim@zendesk.com' }
  let(:user) { { name: 'Amy', email: email } }

  before do
    @request.account = account
    account.inbound_mail_rate_limits.destroy_all
    @limit1 = create_inbound_mail_rate_limit(is_sender: true, rate_limit: 10)
    @limit2 = create_inbound_mail_rate_limit(is_sender: false, rate_limit: 100)
    accept :json
  end

  with_options(controller: 'api/v2/internal/inbound_mail_rate_limits') do |request|
    request.should_route :get, '/api/v2/internal/accounts/1/inbound_mail_rate_limits', action: 'index', account_id: '1'
    request.should_route :get, '/api/v2/internal/accounts/1/inbound_mail_rate_limits/10', action: 'show', account_id: '1', id: 10
    request.should_route :post, '/api/v2/internal/accounts/1/inbound_mail_rate_limits', action: 'create', account_id: '1'
    request.should_route :patch, '/api/v2/internal/accounts/1/inbound_mail_rate_limits/10', action: 'update', account_id: '1', id: 10
    request.should_route :get, '/api/v2/internal/accounts/1/inbound_mail_rate_limits/10/audits', action: 'audits', account_id: '1', id: 10
  end

  as_a_subsystem_user(account: :support) do
    describe '#index' do
      before { get :index, params: { account_id: account_id } }

      describe 'when valid account id is provided' do
        let(:account_id) { account.id }

        it('responds with success') { assert_response :success }

        it 'returns inbound mail rate limits' do
          actual = JSON.parse(@response.body)['inbound_mail_rate_limits'].length
          assert_equal 2, actual
        end
      end

      describe 'when invalid account id is provided' do
        let(:account_id) { 17171717 }

        it('responds with not found') do
          refute Account.find_by_id(account_id)
          assert_response :not_found
        end
      end
    end

    describe '#show' do
      before { get :show, params: { account_id: account.id, id: id } }

      describe 'when retriving a inbound mail rate limit that exists' do
        let(:id) { @limit1.id }

        it('responds with success') { assert_response :success }

        it 'returns the corresponding rate limit' do
          actual = JSON.parse(@response.body)['inbound_mail_rate_limit']['rate_limit']
          assert_equal @limit1.rate_limit, actual
        end
      end

      describe 'when retriving a inbound mail rate limit that does not exist' do
        let(:id) { '123456789' }

        it('responds with not found') { assert_response :not_found }
      end
    end

    describe '#create' do
      describe 'when creating a new rate limit' do
        let(:rate_limit) { 20 }
        let(:inbound_mail_rate_limit_data) do
          {
            email: email,
            account_id: account.id,
            is_sender: true,
            description: 'test',
            rate_limit: rate_limit,
            deleted_at: nil,
            is_active: true
          }
        end

        before do
          post :create, params: { account_id: account.id, inbound_mail_rate_limit: inbound_mail_rate_limit_data, user: user }
        end

        it('responds with success') { assert_response :success }

        it 'returns the newly created rate limit' do
          actual = JSON.parse(@response.body)['inbound_mail_rate_limit']
          assert_equal rate_limit, actual['rate_limit']
          assert_equal 'akim@zendesk.com', actual['email']
          # assert the expected cia event is created
          cia_event = InboundMailRateLimit.find(actual['id']).cia_events.last
          assert_equal user.to_json, cia_event.message
          assert_equal 'create', cia_event.action
        end

        describe 'when invalid email is provided' do
          let(:email) { '@@@@@' }

          it('responds with bad request') { assert_response :bad_request }
        end

        describe 'when invalid rate limit is provided' do
          let(:rate_limit) { 'hi' }

          it('responds with bad request') { assert_response :bad_request }
        end
      end

      describe 'when the rate limit has been deleted' do
        before do
          delete :destroy, params: { account_id: account.id, id: @limit1.id }

          post :create, params: { account_id: account.id, user: user, inbound_mail_rate_limit: {
            email: @limit1.email,
            account_id: account.id,
            is_sender: @limit1.is_sender,
            description: 'test',
            rate_limit: 100,
          } }
        end

        it('responds with success') { assert_response :success }

        it 'undelete rate limit' do
          inbound_mail_rate_limit = JSON.parse(@response.body)['inbound_mail_rate_limit']
          assert_nil inbound_mail_rate_limit['deleted_at']
          # assert the expected cia event is created
          cia_event = InboundMailRateLimit.find(inbound_mail_rate_limit['id']).cia_events.last
          assert_equal user.to_json, cia_event.message
          assert_equal 'restore', cia_event.action
        end
      end
    end

    describe '#update' do
      before do
        patch :update, params: { account_id: account.id, id: @limit1.id, user: user, inbound_mail_rate_limit: { rate_limit: 20 } }
      end

      it('responds with success') { assert_response :success }

      it 'returns the updated rate limit' do
        inbound_mail_rate_limit = JSON.parse(@response.body)['inbound_mail_rate_limit']
        assert_equal 20, inbound_mail_rate_limit['rate_limit']
        # assert the expected cia event is created
        cia_event = InboundMailRateLimit.find(inbound_mail_rate_limit['id']).cia_events.last
        assert_equal user.to_json, cia_event.message
        assert_equal 'update', cia_event.action
      end
    end

    describe '#destroy' do
      let(:id) { @limit1.id }

      before { delete :destroy, params: { account_id: account.id, id: id, user: user } }

      it('responds with success') { assert_response :success }

      it 'return the deleted rate limit' do
        inbound_mail_rate_limit = JSON.parse(@response.body)['inbound_mail_rate_limit']
        assert_equal @limit1.email, inbound_mail_rate_limit['email']
        # assert the expected cia event is created
        cia_event = InboundMailRateLimit.with_deleted { InboundMailRateLimit.find(inbound_mail_rate_limit['id']).cia_events.last }
        assert_equal user.to_json, cia_event.message
        assert_equal 'destroy', cia_event.action
      end

      describe 'when deleting a rate limit that does not exist' do
        let(:id) { '123456789' }

        it('responds with not found') { assert_response :not_found }
      end
    end

    describe '#audits' do
      before do
        patch :update, params: { account_id: account.id, id: @limit2.id, inbound_mail_rate_limit: { rate_limit: 5, description: 'yay'} }
        patch :update, params: { account_id: account.id, id: @limit2.id, inbound_mail_rate_limit: { rate_limit: 10, description: 'day'} }
        get :audits, params: { account_id: account.id, id: @limit2.id }
      end

      it('responds with success') { assert_response :success }

      it 'returns the audits for the rate limit' do
        actual = JSON.parse(@response.body)['audit_logs'].length
        assert_equal 2, actual
      end
    end
  end
end
