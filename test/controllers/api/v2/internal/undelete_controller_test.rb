require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Internal::UndeleteController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :forums, :posts, :entries, :tickets, :events, :organizations

  before do
    accept :json
    @request.account = accounts(:minimum)
  end

  with_options(controller: "api/v2/internal/undelete") do |request|
    request.should_route :get, "/api/v2/internal/undelete", action: "index"
    request.should_route :put, "/api/v2/internal/undelete/123", action: "update", id: 123
  end

  as_an_admin do
    should_be_forbidden :index
  end

  as_a_subsystem_user(account: :minimum) do
    let(:forum) { forums(:solutions) }
    let(:ticket) { tickets(:minimum_1) }
    let(:organization) { organizations(:minimum_organization1) }
    let(:group) { groups(:minimum_group) }

    before do
      forum.soft_delete!
      ticket.will_be_saved_by ticket.account.owner
      ticket.soft_delete!
      organization.soft_delete!
      group.update_attributes(is_active: false)
    end

    def json
      JSON.parse(@response.body)
    end

    describe "#index" do
      it "scopes correctly" do
        get :index, params: { deleted_at: [1.day.ago.to_s(:db), Time.now.to_s(:db)], type: "forum" }
        assert_equal 1, json["models"].size

        get :index, params: { deleted_at: [1.day.ago.to_s(:db), 1.minute.ago.to_s(:db)], type: "forum" }
        assert_equal 0, json["models"].size

        get :index, params: { deleted_at: [1.minute.from_now.to_s(:db), 1.day.from_now.to_s(:db)], type: "forum" }
        assert_equal 0, json["models"].size
      end

      it "fails with incorrect type" do
        get :index, params: { deleted_at: [1.day.ago.to_s(:db), Time.now.to_s(:db)], type: "xxx" }
        assert_response :bad_request
      end

      it "fails without deleted_at" do
        get :index, params: { type: "xxx" }
        assert_response :bad_request
      end

      [Forum, Entry, Post, Organization].each do |type|
        describe type do
          before do
            get :index, params: { deleted_at: [1.day.ago.to_s(:db), Time.now.to_s(:db)], type: type.to_s.downcase }
          end

          should_use_presenter Api::V2::Internal::UndeletePresenter, status: :ok

          it "renders #{type.to_s.downcase.pluralize} with deleted at" do
            scope = type.to_s.downcase.pluralize
            result = type.with_deleted { accounts(:minimum).send(scope).where("deleted_at > ?", 1.day.ago).count(:all) }
            json["models"].size.must_equal result
            assert json["models"][0]["deleted_at"].presence
          end
        end
      end

      describe "tickets" do
        before do
          get :index, params: { deleted_at: [1.day.ago.to_s(:db), Time.now.to_s(:db)], type: "ticket" }
        end

        should_use_presenter Api::V2::Internal::UndeletePresenter, status: :ok

        it "renders tickets of StatusType deleted" do
          assert_operator 1, :<=, json["models"].size
          assert json["models"][0]["deleted_at"]
        end

        it "returns ticket nice_id instead of id" do
          json["models"][0]["id"].must_equal ticket.nice_id
        end

        describe "sorting" do
          before do
            @ticket2 = tickets(:minimum_2)
            @ticket3 = tickets(:minimum_3)

            @ticket2.will_be_saved_by @ticket2.account.owner
            @ticket3.will_be_saved_by @ticket3.account.owner

            Timecop.freeze(5.hours.ago.to_s(:db)) do
              @ticket2.soft_delete!
            end

            Timecop.freeze(3.hours.ago.to_s(:db)) do
              @ticket3.soft_delete!
            end
          end

          it "renders tickets in descending order when requested" do
            get :index, params: { deleted_at: [1.day.ago.to_s(:db), Time.now.to_s(:db)], sort_order: "desc", type: "ticket" }
            assert_operator json["models"][0]["deleted_at"], :>, json["models"][1]["deleted_at"]
            assert_operator json["models"][1]["deleted_at"], :>, json["models"][2]["deleted_at"]
          end

          it "renders tickets in ascending order when requested" do
            get :index, params: { deleted_at: [1.day.ago.to_s(:db), Time.now.to_s(:db)], sort_order: "asc", type: "ticket" }
            assert_operator json["models"][0]["deleted_at"], :<, json["models"][1]["deleted_at"]
            assert_operator json["models"][1]["deleted_at"], :<, json["models"][2]["deleted_at"]
          end

          it "renders tickets in ascending order by default" do
            get :index, params: { deleted_at: [1.day.ago.to_s(:db), Time.now.to_s(:db)], sort_order: "blah", type: "ticket" }
            assert_operator json["models"][0]["deleted_at"], :<, json["models"][1]["deleted_at"]
            assert_operator json["models"][1]["deleted_at"], :<, json["models"][2]["deleted_at"]
          end
        end
      end

      describe "groups" do
        before do
          get :index, params: { deleted_at: [1.day.ago.to_s(:db), Time.now.to_s(:db)], type: "group" }
        end

        should_use_presenter Api::V2::Internal::UndeletePresenter, status: :ok

        it "renders deleted groups" do
          assert_operator 1, :<=, json["models"].size
          assert json["models"][0]["deleted_at"]
        end
      end
    end

    describe "#update" do
      before do
        @ticket = tickets(:minimum_1)
        @ticket.will_be_saved_by @ticket.account.owner
        @ticket.soft_delete!
      end

      it "404 on missing" do
        put :update, params: { type: "forum", id: 1234 }
        assert_response :not_found
      end

      it "undeletes forum" do
        forum.reload
        assert_operator forum.entries.size, :==, 0
        assert_operator forum.posts.size, :==, 0

        put :update, params: { type: "forum", id: forum.id }

        forum.reload
        assert_operator forum.entries.size, :>=, 1
        assert_operator forum.posts.size, :>=, 1
      end

      it "undeletes post" do
        post = Post.with_deleted { forum.posts.first }
        put :update, params: { type: "post", id: post.id }
        assert_response :success
        assert Post.find(post.id)
      end

      it "undeletes entry" do
        entry = Entry.with_deleted { forum.entries.first }
        put :update, params: { type: "entry", id: entry.id }
        assert_response :success
        assert Entry.find(entry.id)
      end

      it "undeletes ticket too!" do
        @ticket.reload
        put :update, params: { type: "ticket", id: @ticket.nice_id }
        assert_response :success
        assert Ticket.find(@ticket.id)
      end
    end
  end
end
