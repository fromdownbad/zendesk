require File.expand_path '../../../../support/test_helper', File.dirname(__FILE__)

SingleCov.covered!

describe Api::V2::Internal::CouponRedemptionsController do
  extend Api::V2::TestHelper

  fixtures :all

  before do
    accept :json
    @account              = accounts(:minimum)
    @request.account      = @account
  end

  as_a_subsystem_user(account: :minimum) do
    describe 'with :deprecate_coupons_redemptions_controller' do
      before { Arturo.enable_feature!(:deprecate_coupons_redemptions_controller) }
      it 'renders 409 gone' do
        get :index, params: { account_id: @account.id, format: :json }

        assert_response :gone
      end
    end

    describe 'when account has zuora coupon redemptions' do
      before do
        # Ensure the coupon is replicated in billing
        zuora_coupons(:sample).send(:create_billing_coupon)

        Subscription.any_instance.stubs(:zuora_managed?).returns(true)
        assert @account.zuora_managed?

        redemption = zuora_coupon_redemptions(:sample)
        redemption.update_attributes!(account: @account)

        get :index, params: { account_id: @account.id, format: :json }
        assert_response :ok

        @redemptions = JSON.parse(@response.body)['coupon_redemptions']
      end

      it 'renders the redemptions associated with the account' do
        refute @redemptions.empty?
      end

      it 'renders the coupon associated with the redemption' do
        assert @redemptions.first.key? 'coupon'
      end

      it 'indicates that it is a zuora coupon redemption' do
        redemption = @redemptions.first
        assert redemption.key? 'type'
        assert_equal 'zuora', redemption['type']
      end

      it 'has an indicator if the redemption is still active' do
        redemption = @redemptions.first
        assert redemption.key? 'active?'
        assert redemption['active?']
      end
    end

    describe 'when account has classic coupon redemptions' do
      before do
        Subscription.any_instance.stubs(:zuora_managed?).returns(false)
        refute @account.zuora_managed?

        redemption = coupon_applications(:sample)
        redemption.update_attributes!(account: @account, subscription: @account.subscription)
        @account.subscription.coupon_applications << redemption

        get :index, params: { account_id: @account.id, format: :json }
        assert_response :ok

        @redemptions = JSON.parse(@response.body)['coupon_redemptions']
      end

      it 'renders the redemptions associated with the account' do
        refute @redemptions.empty?
      end

      it 'renders the coupon associated with the redemption' do
        assert @redemptions.first.key? 'coupon'
      end

      it 'indicates that it is a classic coupon redemption' do
        redemption = @redemptions.first
        assert redemption.key? 'type'
        assert_equal 'classic', redemption['type']
      end

      it 'has an indicator if the redemption is still active' do
        redemption = @redemptions.first
        assert redemption.key? 'active?'
        assert redemption['active?']
      end
    end

    describe 'when account has both zuora and classic coupon redemptions' do
      #
      # ... account should only have one or the other ...
      #
    end
  end
end
