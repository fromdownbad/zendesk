require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::AgentsController do
  fixtures :all

  let!(:account) do
    accounts(:minimum).tap do |a|
      a.users.where('id != ?', a.owner_id).delete_all
    end
  end

  let!(:admin1) do
    CIA.audit(actor: account.owner) do
      FactoryBot.create(:verified_admin, account: account, name: 'AAA')
    end
  end

  let!(:admin2) do
    CIA.audit(actor: account.owner) do
      Timecop.travel(Time.now + 1.seconds)
      FactoryBot.create(:unverified_admin, account: account, name: 'BBB')
    end
  end

  let!(:agent1) do
    CIA.audit(actor: account.owner) do
      Timecop.travel(Time.now + 1.seconds)
      FactoryBot.create(:agent, account: account, name: 'CCC')
    end
  end

  let!(:agent2) do
    CIA.audit(actor: account.owner) do
      Timecop.travel(Time.now + 1.seconds)
      FactoryBot.create(:agent, account: account, name: 'DDD')
    end
  end

  let!(:light_agent1) do
    CIA.audit(actor: account.owner) do
      Timecop.travel(Time.now + 1.seconds)
      FactoryBot.create(:light_agent, account: account, name: 'EEE')
    end
  end

  let!(:light_agent2) do
    CIA.audit(actor: account.owner) do
      Timecop.travel(Time.now + 1.seconds)
      FactoryBot.create(:light_agent, account: account, name: 'FFF')
    end
  end

  let!(:chat_agent1) do
    CIA.audit(actor: account.owner) do
      Timecop.travel(Time.now + 1.seconds)
      FactoryBot.create(:chat_agent, account: account, name: 'GGG')
    end
  end

  let!(:chat_agent2) do
    CIA.audit(actor: account.owner) do
      Timecop.travel(Time.now + 1.seconds)
      FactoryBot.create(:chat_agent, account: account, name: 'HHH')
    end
  end

  let!(:contributor1) do
    CIA.audit(actor: account.owner) do
      Timecop.travel(Time.now + 1.seconds)
      FactoryBot.create(:contributor, account: account, name: 'III')
    end
  end

  let!(:contributor2) do
    CIA.audit(actor: account.owner) do
      Timecop.travel(Time.now + 1.seconds)
      FactoryBot.create(:contributor, account: account, name: 'JJJ')
    end
  end

  before do
    @request.account = account
    @controller.stubs(:current_registered_user).
      returns(Zendesk::SystemUser.
      find_subsystem_user_by_name('zendesk'))
  end

  let(:response_body) { JSON.parse(response.body) }

  describe '#admins' do
    it 'returns the account admins' do
      get :admins, format: :json

      assert_response :ok

      assert_equal %w[AAA BBB],
        response_body['users'].map { |u| u['name'] }
    end

    it 'does not have any N+1 query' do
      # SELECT  `accounts`.* FROM `accounts`  WHERE `accounts`.`id` = XXXX LIMIT 1
      # SELECT `account_settings`.* FROM `account_settings` WHERE `account_settings`.`account_id` = XXXX
      # SELECT  `accounts`.* FROM `accounts`  WHERE `accounts`.`deleted_at` IS NULL AND `accounts`.`id` = -1 LIMIT 1
      # SELECT  `translation_locales`.* FROM `translation_locales`  WHERE `translation_locales`.`deleted_at` IS NULL AND `translation_locales`.`id` = 1 LIMIT 1
      # SELECT `routes`.* FROM `routes`  WHERE `routes`.`account_id` = XXXX
      # SELECT  `users`.* FROM `users`  WHERE `users`.`account_id` = XXXX AND `users`.`is_active` = 1 AND `users`.`roles` = 2 AND (`users`.`id` != YYYY) ORDER BY `users`.`name` asc LIMIT 30 OFFSET 0 /* Api::V2::Internal::Monitor::AgentsController#admins */
      # SELECT `user_identities`.* FROM `user_identities`  WHERE `user_identities`.`user_id` IN (AAA, BBB) ORDER BY priority /* Api::V2::Internal::Monitor::AgentsController#admins */
      assert_sql_queries(8) do
        get :admins, format: :json
      end
    end

    it 'filters based on the id parameter' do
      get :admins, params: { id: admin1.id.to_s, format: :json }

      assert_response :ok
      assert_equal %w[AAA],
        response_body['users'].map { |u| u['name'] }
    end

    it 'filters based on the name parameter' do
      get :admins, params: { name: 'aaa', format: :json }

      assert_response :ok

      assert_equal %w[AAA],
        response_body['users'].map { |u| u['name'] }
    end

    it 'filters based on the email parameter' do
      admin1.email = 'aaa@zendesk.com'
      admin2.email = 'bbb@zendesk.com'

      get :admins, params: { email: 'aaa', format: :json }

      assert_response :ok

      assert_equal %w[AAA],
        response_body['users'].map { |u| u['name'] }
    end

    it 'orders based on the sort_column and sort_order for user values' do
      CIA.audit(actor: account.owner) { admin2.touch }

      get :admins, params: { sort_column: 'updated_at', sort_order: 'desc', format: :json }

      assert_response :ok

      assert_equal %w[BBB AAA],
        response_body['users'].map { |u| u['name'] }
    end

    it 'orders based on the sort_column and sort_order for email' do
      get :admins, params: { sort_column: 'email', sort_order: 'desc', format: :json }

      assert_response :ok

      assert_equal %w[BBB AAA],
        response_body['users'].map { |u| u['name'] }
    end

    it 'returns the number of admins asked' do
      get :admins, params: { per_page: 1, format: :json }

      assert_response :ok

      assert_equal %w[AAA],
        response_body['users'].map { |u| u['name'] }

      assert_equal 1, response_body['page']
      assert_equal 1, response_body['per_page']
      assert_equal 2, response_body['count']
    end

    it 'returns the page asked' do
      get :admins, params: { page: 2, format: :json }

      assert_response :ok

      assert_equal [],
        response_body['users'].map { |u| u['name'] }
    end
  end

  describe '#agents' do
    it 'returns the account agents' do
      get :agents, format: :json

      assert_response :ok

      assert_equal %w[CCC DDD],
        response_body['users'].map { |u| u['name'] }
    end
  end

  describe '#light_agents' do
    it 'returns the account light agents' do
      get :light_agents, format: :json

      assert_response :ok

      assert_equal %w[EEE FFF],
        response_body['users'].map { |u| u['name'] }
    end
  end

  describe '#chat_agents' do
    it 'returns the account chat agents' do
      get :chat_agents, format: :json

      assert_response :ok

      assert_equal %w[GGG HHH],
        response_body['users'].map { |u| u['name'] }
    end
  end

  describe '#contributors' do
    it 'returns the account contributors' do
      get :contributors, format: :json

      assert_response :ok

      assert_equal %w[III JJJ],
        response_body['users'].map { |u| u['name'] }
    end
  end

  describe '#associated_accounts' do
    let!(:account) do
      accounts(:minimum)
    end

    let!(:inactive_account) do
      accounts(:multiproduct).tap do |account|
        account.is_active = false
        account.save!
      end
    end

    before do
      UserContactInformation.create! do |c|
        c.account_id = account.id
        c.user_id = account.owner_id
        c.name = 'Admin Extraordinaire'
        c.email = 'test@zendesk.com'
        c.role = 2
      end

      UserContactInformation.create! do |c|
        c.account_id = inactive_account.id
        c.user_id = inactive_account.owner_id
        c.name = 'Admin Extraordinaire'
        c.email = 'test@zendesk.com'
        c.role = 2
      end
    end

    it 'returns the list of accounts with user contact information' do
      get :associated_accounts, params: { email: 'test@zendesk.com', format: :json }

      assert_response :ok
      assert_equal([{ account_id: account.id, role: 2, name: 'Admin Extraordinaire' }].to_json, response.body)
    end
  end
end
