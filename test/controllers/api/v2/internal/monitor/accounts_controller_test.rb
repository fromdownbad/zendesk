require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::AccountsController do
  fixtures :all

  before do
    set_header('X-Zendesk-Via', 'Monitor event')
    set_header('X-Zendesk-Via-Reference-ID', 1234)
    @request.account = @account = accounts(:minimum)
    @controller.stubs(:current_registered_user).
      returns(Zendesk::SystemUser.
      find_subsystem_user_by_name('zendesk'))
  end

  describe '#settings' do
    before { Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns(false) }
    it 'returns the account settings' do
      get :settings, format: :json

      assert_response :ok
      assert_equal %w[settings], JSON.parse(response.body).keys
    end
  end

  describe '#owner' do
    it 'returns the account owner' do
      get :owner, format: :json

      assert_response :ok

      parsed_response = JSON.parse(response.body)
      assert_equal %w[id name last_login is_verified email], parsed_response.keys
      assert_equal @account.owner.is_verified?,  parsed_response['is_verified']
      assert_equal @account.owner.email,         parsed_response['email']
    end
  end

  describe 'agent_counts' do
    it 'returns the number of active agents' do
      get :agent_counts, format: :json

      assert_response :ok
      assert_equal %w[normal light chat legacy], JSON.parse(response.body).keys
    end
  end

  describe 'change_account_owner' do
    let!(:old_owner) { @account.owner }
    let!(:new_owner) { users(:minimum_admin_not_verified) }
    let!(:new_end_user_owner) { users(:minimum_end_user) }

    it 'changes the owner of the account' do
      put :change_account_owner, params: { new_owner_id: new_owner.id, demote_role_id: 0, format: :json }

      assert_response :ok

      assert_equal new_owner, @account.reload.owner
    end

    it 'renders an error message when the change fails' do
      Account.any_instance.stubs(:save).returns(false, true)
      put :change_account_owner, params: { new_owner_id: new_owner.id, demote_role_id: 0, format: :json }

      assert_response :unprocessable_entity

      assert_equal old_owner, @account.reload.owner

      assert_includes response.body, 'Update of the account failed'
    end

    it 'allows the promotion of an end-user to the new owner' do
      put :change_account_owner, params: { new_owner_id: new_end_user_owner.id, demote_role_id: 0, format: :json }

      assert_response :ok

      assert_equal new_end_user_owner, @account.reload.owner
    end
  end

  describe 'create_new_account_owner' do
    let!(:old_owner) { @account.owner }

    it 'creates a new owner for the account' do
      new_owner = users(:minimum_admin_not_verified)
      new_owner.name = 'John Doe'
      new_owner.email = 'jdoe@zendesk.com'
      new_owner.save!

      stub_request(:post, %r{/api/v2/users}).
        to_return(status: 201, body: {user: {id: new_owner.id}}.to_json, headers: { content_type: 'application/json' })

      put :create_new_account_owner, params: { new_owner: { name: 'John Doe', email: 'jdoe@zendesk.com', demote_role_id: 2 }, format: :json }

      assert_response :ok

      assert_equal new_owner, @account.reload.owner
    end

    it 'handles network errors' do
      stub_request(:post, %r{/api/v2/users}).
        to_return(status: 500)

      put :create_new_account_owner, params: { new_owner: { name: 'John Doe', email: 'jdoe@zendesk.com', demote_role_id: 2 }, format: :json }

      assert_equal old_owner, @account.reload.owner
    end
  end

  describe 'change_account_country_id' do
    let(:country_id) { 10 }
    let(:address) { { country_id: country_id } }

    it 'changes the country id of the account' do
      put :change_account_country_id, params: { address: address, format: :json }

      assert_response :ok
      assert_equal country_id, @account.address.country_id
    end

    it 'responds with error on exception' do
      Address.any_instance.stubs(:update_attributes).returns(false)

      put :change_account_country_id, params: { address: address, format: :json }

      assert_response 422
    end
  end
end
