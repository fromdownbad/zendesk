require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::SubscriptionsController do
  fixtures :all

  before do
    set_header('X-Zendesk-Via', 'Monitor event')
    set_header('X-Zendesk-Via-Reference-ID', 1234)
    @request.account = @account = accounts(:minimum)
    @controller.stubs(:current_registered_user).
      returns(Zendesk::SystemUser.
      find_subsystem_user_by_name('zendesk'))
  end

  describe '#settings' do
    it 'returns the account settings' do
      get :settings, format: :json

      assert_response :ok

      parsed_response = JSON.parse(response.body)

      assert_equal %w[settings], parsed_response.keys
      assert_equal %w[
        boost
        import_mode
        has_high_volume_api
        can_be_boosted
        default_api_rate_limit
        default_api_time_rate_limit
        api_rate_limit
        api_time_rate_limit
        default_api_incremental_exports_rate_limit
        api_incremental_exports_rate_limit
      ], parsed_response['settings'].keys
    end
  end
end
