require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::ConditionalRateLimitsController do
  before do
    set_header('X-Zendesk-Via', 'Monitor event')
    set_header('X-Zendesk-Via-Reference-ID', 1234)
    @request.account = @account = accounts(:minimum)
    @controller.stubs(:current_registered_user).
      returns(Zendesk::SystemUser.
      find_subsystem_user_by_name('zendesk'))
  end

  describe '#index' do
    it 'returns the conditional rate limits' do
      get :index, format: :json

      assert_response :ok
      assert_equal %w[conditional_rate_limits], JSON.parse(response.body).keys
    end
  end

  describe '#create' do
    let(:params) do
      {
        key: 'test',
        limit: '123',
        interval: '345',
        expires_on: '2020-01-21T02:58:43+00:00',
        owner_email: 'abc@zendesk.com',
        request_method: 'post',
        endpoint: 'api/v2/test',
        user_agent: 'test_agent',
        query_string: 'test_query',
        ip_address: '1.1.1.1',
        user_id: '789',
        enforce_lotus: 'true'
      }
    end

    it 'creates a new rate limit' do
      post :create, params: { format: :json, rate_limit: params }
      assert_response :created
      assert_equal @account.texts.conditional_rate_limits.first['prop_key'], 'test'
    end

    it 'sets the ip_address property' do
      post :create, params: { format: :json, rate_limit: params }
      assert_equal @account.texts.conditional_rate_limits.first['properties']['ip_address'], '1.1.1.1'
    end

    it 'sets the expire time' do
      post :create, params: { format: :json, rate_limit: params }
      assert_equal @account.texts.conditional_rate_limits.first['expires_on'], '2020-01-21T02:58:43+00:00'
    end

    it 'sets the owner email' do
      post :create, params: { format: :json, rate_limit: params }
      assert_equal @account.texts.conditional_rate_limits.first['owner_email'], 'abc@zendesk.com'
    end
  end

  describe '#destroy' do
    before do
      @account.add_conditional_rate_limit!(key: 'test', limit: 123, interval: 456, endpoint: '/test')
      @account.add_conditional_rate_limit!(key: 'other', limit: 1234, interval: 4567, endpoint: '/other')
    end

    it 'removes the rate limit' do
      delete :destroy, params: { format: :json, key: 'test' }
      assert_response :no_content
      assert_equal @account.texts.conditional_rate_limits.count, 1
    end
  end

  describe '#destroy_all' do
    before do
      @account.add_conditional_rate_limit!(key: 'test', limit: 123, interval: 456, endpoint: '/test')
      @account.add_conditional_rate_limit!(key: 'other', limit: 1234, interval: 4567, endpoint: '/other')
    end

    it 'removes all rate limits' do
      delete :destroy_all, format: :json
      assert_response :no_content
      assert_nil @account.texts.conditional_rate_limits
    end
  end

  describe "Ledger" do
    let(:arturo_enabled) { true }
    before do
      Account.any_instance.expects(:has_ocp_log_crl_events_to_ledger_enabled?).returns(arturo_enabled).at_least_once
    end

    describe "with :ocp_log_crl_events_to_ledger arturo enabled" do
      before do
        LedgerClient::ApiClient.any_instance.expects(:post_event).times(3)
        @account.add_conditional_rate_limit!(key: 'test1', limit: 123, interval: 456, endpoint: '/test1')
        @account.add_conditional_rate_limit!(key: 'test2', limit: 123, interval: 456, endpoint: '/test2')
        @account.add_conditional_rate_limit!(key: 'test3', limit: 123, interval: 456, endpoint: '/test3')
        LedgerClient::ApiClient.any_instance.expects(:post_event).once
      end

      it ':create event is logged' do
        post :create, params: { format: :json, rate_limit: {
          key: 'test',
          limit: '123',
          interval: '345',
          expires_on: '2020-01-21T02:58:43+00:00',
          owner_email: 'abc@zendesk.com',
          request_method: 'post',
          endpoint: 'api/v2/test',
          user_agent: 'test_agent',
          query_string: 'test_query',
          ip_address: '1.1.1.1',
          user_id: '789',
          enforce_lotus: 'true'
        } }
      end

      it ':delete event is logged' do
        delete :destroy, params: { format: :json, key: 'test1' }
      end

      describe ':destroy_all event' do
        before do
          LedgerClient::ApiClient.any_instance.expects(:post_event).times(2)
        end

        it 'is logged' do
          delete :destroy_all, format: :json
        end
      end
    end

    describe "with :ocp_log_crl_events_to_ledger arturo disabled" do
      let(:arturo_enabled) { false }

      before do
        @account.add_conditional_rate_limit!(key: 'test1', limit: 123, interval: 456, endpoint: '/test1')
        @account.add_conditional_rate_limit!(key: 'test2', limit: 123, interval: 456, endpoint: '/test2')
        @account.add_conditional_rate_limit!(key: 'test3', limit: 123, interval: 456, endpoint: '/test3')
        LedgerClient::ApiClient.any_instance.expects(:post_event).never
      end

      it ':create event is NOT logged' do
        post :create, params: { format: :json, rate_limit: {
          key: 'test',
          limit: '123',
          interval: '345',
          expires_on: '2020-01-21T02:58:43+00:00',
          owner_email: 'abc@zendesk.com',
          request_method: 'post',
          endpoint: 'api/v2/test',
          user_agent: 'test_agent',
          query_string: 'test_query',
          ip_address: '1.1.1.1',
          user_id: '789',
          enforce_lotus: 'true'
        } }
      end

      it ':delete event is NOT logged' do
        delete :destroy, params: { format: :json, key: 'test1' }
      end

      describe ':destroy_all event' do
        before do
          LedgerClient::ApiClient.any_instance.expects(:post_event).never
        end

        it 'is NOT logged' do
          delete :destroy_all, format: :json
        end
      end
    end
  end
end
