require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::MobileSdkAppSettingsController do
  extend Api::V2::TestHelper

  fixtures :mobile_sdk_apps, :mobile_sdk_auths, :oauth_clients, :mobile_sdk_app_settings

  before do
    accept :json
    set_header('X-Zendesk-Via', 'Monitor event')
    set_header('X-Zendesk-Via-Reference-ID', 1234)
    @request.account = @account = accounts(:minimum_sdk)

    @controller.
      stubs(:current_registered_user).
      returns(Zendesk::SystemUser.find_subsystem_user_by_name('zendesk'))
  end

  let(:app) { @account.mobile_sdk_apps.first }
  let(:json_response) { JSON.parse(@response.body) }

  describe '#show' do
    before do
      get :show
    end

    should_use_presenter Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter

    it { assert_response :ok }

    it "sets cache headers based on mobile_sdk_apps information" do
      etag = @response.headers["ETag"]

      assert etag.present?, "ETag is absent"
    end
  end

  describe "Cache is expired correctly" do
    it "will cache" do
      Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter.any_instance.expects(:present)
      get :show, format: :json

      Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter.any_instance.expects(:present).never
      get :show, format: :json
    end

    it "will expire the cache if the app count changes" do
      Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter.any_instance.expects(:present).twice

      get :show
      etag1 = @response.headers["ETag"]

      MobileSdkApp.create!(
        title: app.title,
        identifier: SecureRandom.hex(16),
        mobile_sdk_auth: app.mobile_sdk_auth,
        account: @account,
        brand: @account.brands.first,
        signup_source: app.signup_source
      )

      get :show
      etag2 = @response.headers["ETag"]

      assert_not_nil etag1
      assert_not_nil etag2
      refute etag1 == etag2, "Creating an app did not expire the cache"
    end

    it "will expire the cache if an app is updated" do
      Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter.any_instance.expects(:present).twice

      get :show
      etag1 = @response.headers["ETag"]

      app.title = "Let's Rebrand"
      app.save!

      get :show
      etag2 = @response.headers["ETag"]

      assert_not_nil etag1
      assert_not_nil etag2
      refute etag1 == etag2, "Updating an app itself did not expire the cache"
    end

    it "will expire the cache if an app settings is updated" do
      Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter.any_instance.expects(:present).twice

      get :show
      etag1 = @response.headers["ETag"]

      app.settings.contact_us_tags = ["yolo"]
      app.save!

      get :show
      etag2 = @response.headers["ETag"]

      assert_not_nil etag1
      assert_not_nil etag2
      refute etag1 == etag2, "Updating an app's settings did not expire the cache"
    end

    it "will expire the cache if an app's authentication is updated" do
      Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter.any_instance.expects(:present).twice

      get :show
      etag1 = @response.headers["ETag"]

      app.settings.authentication = mobile_sdk_app_settings(:authentication)
      app.mobile_sdk_auth = mobile_sdk_auths(:minimum_sdk)
      app.save!

      get :show
      etag2 = @response.headers["ETag"]

      assert_not_nil etag1
      assert_not_nil etag2
      refute etag1 == etag2, "Updating the authentication did not invalidate the cache"
    end
  end

  describe 'Using an account that has no sdk apps' do
    before do
      @request.account = @account = accounts(:support)
      get :show
    end

    should_use_presenter Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter

    it { assert_response :ok }

    it "should not cache" do
      Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter.any_instance.expects(:present)
      get :show, format: :json
      etag1 = @response.headers['ETag']

      Api::V2::Internal::Monitor::MobileSdkAppSettingsPresenter.any_instance.expects(:present)
      get :show, format: :json
      etag2 = @response.headers['ETag']
      assert etag1 == etag2, "cache was set"
    end
  end
end
