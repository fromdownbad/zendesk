require_relative '../../../../../support/test_helper'

SingleCov.covered! uncovered: 2

describe Api::V2::Internal::Monitor::FraudController do
  fixtures :accounts

  let (:account) { accounts(:minimum) }
  let (:ticket_action) { 'tag' }
  let (:tag) { 'spam-deletion-2018-10-10' }
  let (:days) { 7 }
  let (:days_out_of_range) { "366" }
  let (:spammy_strings_to_block) { "csv, spammy_strings, some_string" }
  let (:days_malformed) { "hello" }

  before do
    set_header('X-Zendesk-Via', 'Monitor event')
    set_header('X-Zendesk-Via-Reference-ID', 1234)

    @request.account = account
    @controller.stubs(:current_registered_user).
      returns(Zendesk::SystemUser.
      find_subsystem_user_by_name('zendesk'))
  end

  describe '#show' do
    before do
      get :show, format: :json
      json = JSON.parse(response.body)
      @fraud_presenter = json.fetch('fraud')
      assert @fraud_presenter.fetch('recent_tickets')
      assert @fraud_presenter.fetch('total_users')
    end
  end

  describe '#enqueue_fraud_score_job' do
    it 'enqueues a fraud score job with source event' do
      FraudScoreJob.expects(:enqueue_account).with(account, Fraud::SourceEvent::WHITELISTED, nil).once
      post :fraud_score_job, params: { source_event: 'whitelisted', format: :json }
      assert_response :success
    end
  end

  describe '#watchlist' do
    describe 'when is_watchlisted setting is true' do
      before do
        account.settings.is_watchlisted = true
        account.settings.save!
      end

      it 'changes is_watchlisted setting to false if true' do
        post :watchlist, format: :json
        assert_response :success
        refute account.settings.is_watchlisted
      end

      it 'does not enqueue fraud score job' do
        FraudScoreJob.expects(:enqueue_account).never
        post :watchlist, format: :json
        assert_response :success
      end
    end

    it 'enqueues a watchlist fraud score job with monitor action as default reason' do
      reason = 'unsuspend'
      FraudScoreJob.expects(:enqueue_account).with(account, Fraud::SourceEvent::WATCHLISTED, reason).once
      post :watchlist, params: { reason: reason, format: :json }
      assert_response :success
    end
  end

  describe '#spam_cleanup_job' do
    describe_with_arturo_enabled :orca_classic_spam_cleanup_job_enable do
      it 'enqueues a spam_cleanup_job' do
        SpamCleanupJob.expects(:enqueue).with(account.id, ticket_action, tag, days, spammy_strings_to_block).once
        post :spam_cleanup_job, params: { ticket_action: ticket_action, tag: tag, days: days.to_s, spammy_strings: spammy_strings_to_block, format: :json }
        assert_response :success
      end

      it 'uses the default days for an out of range days' do
        SpamCleanupJob.expects(:enqueue).with(account.id, ticket_action, tag, days, spammy_strings_to_block).once
        post :spam_cleanup_job, params: { ticket_action: ticket_action, tag: tag, days: days_out_of_range, spammy_strings: spammy_strings_to_block, format: :json }
        assert_response :success
      end

      it 'uses the default days for a malformed input days string' do
        SpamCleanupJob.expects(:enqueue).with(account.id, ticket_action, tag, days, spammy_strings_to_block).once
        post :spam_cleanup_job, params: { ticket_action: ticket_action, tag: tag, days: days_malformed, spammy_strings: spammy_strings_to_block, format: :json }
        assert_response :success
      end
    end

    describe_with_arturo_disabled :orca_classic_spam_cleanup_job_enable do
      it 'never enqueues a spam_cleanup_job' do
        SpamCleanupJob.expects(:enqueue).with(account.id, ticket_action, tag, days, spammy_strings_to_block).never
        post :spam_cleanup_job, params: { ticket_action: ticket_action, tag: tag, days: days.to_s, spammy_strings: spammy_strings_to_block, format: :json }
        assert_response :success
      end
    end
  end
end
