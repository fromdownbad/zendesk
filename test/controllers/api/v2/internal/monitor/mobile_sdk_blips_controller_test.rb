require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::MobileSdkBlipsController do
  extend Api::V2::TestHelper

  fixtures :mobile_sdk_auths
  let(:account) { accounts(:minimum) }
  let(:mobile_sdk_auth) { mobile_sdk_auths(:minimum_sdk) }
  let(:app) { @account.mobile_sdk_apps.create(title: "My App", identifier: "987654wsdfghjkliuytr65", mobile_sdk_auth: mobile_sdk_auth) }

  before do
    accept :json
    set_header('X-Zendesk-Via', 'Monitor event')
    set_header('X-Zendesk-Via-Reference-ID', 1234)
    @request.account = @account = account
    @controller.
      stubs(:current_registered_user).
      returns(Zendesk::SystemUser.find_subsystem_user_by_name('zendesk'))
  end

  describe '#show' do
    before { get :show }

    should_use_presenter Api::V2::Internal::Monitor::MobileSdkBlipsPresenter

    it { assert_response :ok }
  end

  describe '#update_account' do
    before do
      @account.settings.mobile_sdk_blips = false
      @account.settings.mobile_sdk_required_blips = false
      @account.settings.mobile_sdk_behavioural_blips = false
      @account.settings.mobile_sdk_pathfinder_blips = false
      @account.save!
    end

    it 'returns unprocessable entity when the update fails' do
      Account.any_instance.stubs(:save).returns(false)

      post :update_account, params: { blips: { enabled: true } }

      assert_response :unprocessable_entity
    end

    it 'updates the master blips switch' do
      post :update_account, params: { blips: { enabled: true } }

      assert @account.settings.reload.mobile_sdk_blips, 'Account Mobile SDK Blips settings should be enabled'
      assert_response :ok
    end

    it 'updates the required blips switch' do
      post :update_account, params: { blips: { permission: { key: 'required_blips', enabled: true } } }

      assert @account.settings.mobile_sdk_required_blips, 'Account Mobile SDK Required Blips settings should be enabled'
      assert_response :ok
    end

    it 'updates the behavioural blips switch' do
      post :update_account, params: { blips: { permission: { key: 'behavioural_blips', enabled: true } } }

      assert @account.settings.mobile_sdk_behavioural_blips, 'Account Mobile SDK Behavioural Blips settings should be enabled'
      assert_response :ok
    end

    it 'updates the pathfinder blips switch' do
      post :update_account, params: { blips: { permission: { key: 'pathfinder_blips', enabled: true } } }

      assert @account.settings.mobile_sdk_pathfinder_blips, 'Account Mobile SDK Pathfinder Blips settings should be enabled'
      assert_response :ok
    end
  end

  describe '#update_app' do
    before do
      app.settings.blips = false
      app.settings.required_blips = false
      app.settings.behavioural_blips = false
      app.settings.pathfinder_blips = false
      app.save!
    end

    it 'returns unprocessable entity when the update fails' do
      MobileSdkApp.any_instance.stubs(:save).returns(false)

      post :update_app, params: { id: app.id, blips: { enabled: true } }

      assert_response :unprocessable_entity
    end

    it 'updates the master blips switch' do
      post :update_app, params: { id: app.id, blips: { enabled: true } }

      assert app.settings.reload.blips, 'App Mobile SDK Blips settings should be enabled'
      assert_response :ok
    end

    it 'updates the required blips switch' do
      post :update_app, params: { id: app.id, blips: { permission: { key: 'required_blips', enabled: true } } }

      assert app.settings.reload.required_blips, 'App Mobile SDK Required Blips settings should be enabled'
      assert_response :ok
    end

    it 'updates the behavioural blips switch' do
      post :update_app, params: { id: app.id, blips: { permission: { key: 'behavioural_blips', enabled: true } } }

      assert app.settings.reload.behavioural_blips, 'App Mobile SDK Behavioural Blips settings should be enabled'
      assert_response :ok
    end

    it 'updates the pathfinder blips switch' do
      post :update_app, params: { id: app.id, blips: { permission: { key: 'pathfinder_blips', enabled: true } } }

      assert app.settings.reload.pathfinder_blips, 'App Mobile SDK Pathfinder Blips settings should be enabled'
      assert_response :ok
    end
  end
end
