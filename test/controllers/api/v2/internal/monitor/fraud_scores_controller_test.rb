require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Internal::Monitor::FraudScoresController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
    set_header('X-Zendesk-Via', 'Monitor event')
    set_header('X-Zendesk-Via-Reference-ID', 1234)
    @request.account = @account = accounts(:minimum)
    @controller.stubs(:current_registered_user).
      returns(Zendesk::SystemUser.
      find_subsystem_user_by_name('zendesk'))
  end

  let!(:account) { accounts(:minimum) }
  let(:data) do
    {
      account_id: account.id,
      verified_fraud: true,
      owner_email: "david@example.com",
      subdomain: "test",
      account_fraud_service_release: "1.0.0"
    }
  end

  with_options(controller: 'api/v2/internal/monitor/fraud_scores') do |request|
    request.should_route :get, '/api/v2/internal/monitor/fraud_scores.json', action: 'show', format: 'json'
    request.should_route :post, '/api/v2/internal/monitor/fraud_scores.json', action: 'create', format: 'json'
  end

  as_a_subsystem_user(account: :minimum) do
    describe "set up steps" do
      before do
        @opts = { format: :json }
      end

      describe "#show action" do
        let!(:fraud_score) { FactoryBot.create(:fraud_score, account: account) }
        let(:json) { JSON.parse(@response.body).fetch('fraud_score') }

        before do
          get :show, params: @opts
        end

        it "responds with HTTP 200" do
          assert_response :ok
        end

        it "renders the fraud_score" do
          assert_equal fraud_score.account_id, json["account_id"]
          assert_equal fraud_score.verified_fraud, json["verified_fraud"]
          assert_equal fraud_score.account_fraud_service_release, json["account_fraud_service_release"]
        end
      end

      describe "POST to #create action" do
        let(:json) { JSON.parse(@response.body).fetch('fraud_score') }

        describe "when it succeeds" do
          before do
            refute FraudScore.find_by_account_id(data[:account_id])
            post :create, params: @opts.merge(fraud_score: data)
          end

          it "responds with HTTP 200" do
            assert_response :created
          end

          it "creates the fraud_score" do
            new_fraud_score = FraudScore.find_by_account_id(data[:account_id])
            assert new_fraud_score, "Failed to create FraudScore."
            assert_equal account.id, new_fraud_score.account_id
          end

          it "returns the expected value in the FraudScore json" do
            assert_equal "david@example.com", json["owner_email"]
            assert_equal "test", json["subdomain"]
            assert_equal "1.0.0", json["account_fraud_service_release"]
          end
        end

        describe "when it fails" do
          before do
            # Force a failure to test controller error presenter use.
            FraudScore.any_instance.stubs(:valid?).returns(false)
            post :create, params: @opts.merge(fraud_score: data)
          end

          it "responds with HTTP 422" do
            assert_response :unprocessable_entity
          end

          it "does not create the FraudScore" do
            refute FraudScore.find_by_account_id(data[:account_id])
          end
        end
      end
    end
  end
end
