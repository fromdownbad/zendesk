require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::CertificateMovesController do
  extend Api::V2::TestHelper
  fixtures :accounts, :certificate_ips, :certificates

  before do
    use_ssl

    # Accept requests to https://pod-X.zendesk.com
    request.env['zendesk.account'] = nil
  end

  as_a_subsystem_user(account: :minimum) do
    before do
      @certificate = certificates(:active1)
      @sni_certificate = certificates(:active_sni)

      # All of these could happen when Exodus has locked an account
      @certificate.account.lock!
    end

    describe "#prepare" do
      it "404 if the account does not have a certificate" do
        post :prepare, params: { account_id: '123456789', target_shard_id: '3', format: :json }
        assert_response :not_found
        assert_equal json['description'], "Account '123456789' does not have any certificates."
      end

      it "500s with the exception message" do
        @controller.stubs(:prepare).raises(StandardError, 'the error message')
        post :prepare, params: { account_id: @certificate.account_id.to_s, target_shard_id: '3', format: :json }

        assert_response 500
        assert_equal 'StandardError', json['error']
        assert_equal 'the error message', json['description']
      end

      it "503 if no ips are available" do
        CertificateIp.unassigned.delete_all
        post :prepare, params: { account_id: @certificate.account_id.to_s, target_shard_id: '3', format: :json }
        assert_response :service_unavailable
        assert_equal 'CertificateIp::NoCertificateIpsAvailableInPod', json['error']
        json['description'].must_include "No IPs are available in pod 2."
      end

      it "assigns an available IP on the new pod" do
        post :prepare, params: { account_id: @certificate.account_id.to_s, target_shard_id: '3', format: :json }
        assert_response :success
        refute_nil json['certificate_ip']['ip']
        assert_equal false, json['certificate_ip']['sni']
        assert_equal json['certificate_ip']['pod_id'], 2
        assert json['certificate_ip']['release_at'] > 6.days.from_now
      end

      it "creates a new SNI record on the new pod for an sni_certificate" do
        post :prepare, params: { account_id: @sni_certificate.account_id.to_s, target_shard_id: '3', format: :json }
        assert_response :success
        assert_equal '127.0.0.2', json['certificate_ip']['ip']
        assert(json['certificate_ip']['sni'])
        assert_equal json['certificate_ip']['pod_id'], 2
        assert json['certificate_ip']['release_at'] > 6.days.from_now
      end
    end

    describe "#swap" do
      it "404 if an IP wasn't already acquired in the target pod (e.g. by prepare)" do
        post :swap, params: { account_id: @certificate.account_id.to_s, source_shard_id: '1', target_shard_id: '3', format: :json }
        assert_response :not_found
        assert_equal json['description'], "No certificate IP assigned on target pod 2. Please issue a `prepare` request to acquire an IP."
      end

      it "promotes the new ip to primary" do
        new_ip = certificate_ips(:two)
        new_ip.update_attribute :certificate_id, @certificate.id
        new_ip.schedule_release!(1.day.from_now)

        post :swap, params: { account_id: @certificate.account_id.to_s, source_shard_id: '1', target_shard_id: '3', format: :json }

        old_ip = certificate_ips(:three)
        assert old_ip.release_at > 23.hours.from_now
        assert_nil json['certificate_ip']['release_at']
        assert_equal json['certificate_ip']['ip'], new_ip.ip
      end

      it "promotes the new ip to primary for an sni_certificate" do
        post :prepare, params: { account_id: @sni_certificate.account_id.to_s, target_shard_id: '3', format: :json }
        post :swap, params: { account_id: @sni_certificate.account_id.to_s, source_shard_id: '1', target_shard_id: '3', format: :json }
        old_ip = @sni_certificate.certificate_ips.by_pod(1).first

        assert_equal '127.0.0.2', json['certificate_ip']['ip']
        assert(json['certificate_ip']['sni'])
        assert_equal json['certificate_ip']['pod_id'], 2
        assert_nil json['certificate_ip']['release_at']
        refute_nil old_ip.reload.release_at
      end
    end

    describe "#rollback" do
      before do
        # after a swap, but the old ip is still associated
        @old_ip = certificate_ips(:three)
        @old_ip.schedule_release!(1.day.from_now)

        @new_ip = certificate_ips(:two)
        @new_ip.old_ip         = @old_ip.ip
        @new_ip.certificate_id = @old_ip.certificate_id
        @new_ip.save!
      end

      it "promotes the old ip to primary" do
        post :rollback, params: { account_id: @certificate.account_id.to_s, source_shard_id: '1', target_shard_id: '3', format: :json }

        assert_equal @old_ip.id, @certificate.certificate_ips.where(pod_id: 1).first.id
      end

      it "promotes the old ip to primary for an sni_certificate" do
        post :prepare, params: { account_id: @sni_certificate.account_id.to_s, target_shard_id: '3', format: :json }
        post :rollback, params: { account_id: @sni_certificate.account_id.to_s, source_shard_id: '1', target_shard_id: '3', format: :json }
        new_ip = @sni_certificate.certificate_ips.by_pod(2).first

        assert_equal '127.0.0.1', json['certificate_ip']['ip']
        assert(json['certificate_ip']['sni'])
        assert_equal json['certificate_ip']['pod_id'], 1
        assert_nil json['certificate_ip']['release_at']
        refute_nil new_ip.release_at
      end
    end
  end
end
