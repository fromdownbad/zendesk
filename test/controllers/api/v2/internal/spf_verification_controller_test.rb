require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Internal::SpfVerificationController do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    @request.account = account
    accept :json
  end

  with_options(controller: "api/v2/internal/spf_verification") do |request|
    request.should_route :get, "/api/v2/internal/accounts/1/spf_verification", action: "index", id: "1"
    request.should_route :put, "/api/v2/internal/accounts/1/spf_verification", action: "update", id: "1"
  end

  as_an_admin do
    should_be_forbidden [:get, :index, id: 1]
    should_be_forbidden [:put, :update, id: 1]
  end

  as_a_subsystem_user(user: "monitor", account: :minimum) do
    describe "#index" do
      before { get :index, params: { id: account.id, email_suffix: email_suffix } }

      describe "when email suffix matches recipient addresses on the account" do
        let(:email_suffix) { "@example.net" }

        it "returns the matching spf records" do
          assert_response :success
          actual = JSON.parse(@response.body)["recipient_addresses"].map { |ra| ra["email"] }.sort
          expected = ["not_default@example.net", "without_name@example.net"].sort
          assert_equal expected, actual
        end
      end

      describe "when email suffix does not match recipient addresses on the account" do
        let(:email_suffix) { "@blah.net" }

        it "does not return spf records" do
          assert_response :success
          assert_empty JSON.parse(@response.body)["recipient_addresses"]
        end
      end
    end

    describe "#update" do
      let(:email_suffix) { "@example.net" }

      before do
        recipient_addresses = account.recipient_addresses.not_collaboration.where("email like ?", "%#{email_suffix}")
        assert_equal 2, recipient_addresses.length
        recipient_addresses.each { |ra| assert_not_equal RecipientAddress::SPF_STATUS.fetch(:verified), ra.spf_status_id }

        put :update, params: { id: account.id, email_suffix: email_suffix }
      end

      it "updates the spf records to verified" do
        assert_response :success
        recipient_addresses = JSON.parse(@response.body)["recipient_addresses"]
        actual = JSON.parse(@response.body)["recipient_addresses"].map { |ra| ra["email"] }.sort
        expected = ["not_default@example.net", "without_name@example.net"].sort
        assert_equal expected, actual
        assert recipient_addresses.all? { |ra| ra["spf_status"] == "verified" }
      end
    end
  end
end
