require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::SharingAgreementsController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :users

  with_options(controller: "api/v2/sharing_agreements") do |request|
    request.should_route :get, "/api/v2/sharing_agreements", action: "index"
  end

  before do
    accept :json
    Zendesk::Net::AddressUtil.stubs(:safe_url?).returns(true)
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_agent do
    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::SharingAgreementPresenter, status: :ok
    end

    describe "a GET to :show" do
      before do
        stub_request(:post, %r{/sharing/agreements/[0-9a-f]+})
        @agreement = accounts(:minimum).sharing_agreements.create!(subdomain: accounts(:support).subdomain)
        get :show, params: { id: @agreement.id }
      end

      should_use_presenter Api::V2::SharingAgreementPresenter, status: :ok

      it "presents the sharing_agreement requested" do
        res = JSON.parse(@response.body)['sharing_agreement']
        assert_equal @agreement.status, res['status'].to_sym
      end
    end
  end

  as_an_admin do
    describe "a POST to :create" do
      before do
        stub_request(:post, %r{/sharing/agreements/[0-9a-f]+})
        params = {
          sharing_agreement: {
            remote_subdomain: accounts(:support).subdomain,
            sync_tags: true,
            sync_custom_fields: true,
            allows_public_comments: false
          }
        }
        post :create, params: params
      end

      should_use_presenter Api::V2::SharingAgreementPresenter, status: :created
      should_change("sharing agreements count", by: 1) { @account.sharing_agreements.count(:all) }

      it "creates a sharing agreement" do
        agreement = @account.sharing_agreements.last
        assert_equal :pending, agreement.status
        assert(agreement.out?)

        assert_equal 'https://support.zendesk-test.com/sharing', agreement.remote_url
        assert(agreement.sync_tags)
        assert(agreement.sync_custom_fields)
        assert_equal false, agreement.allows_public_comments
      end
    end

    describe "Existing agreements" do
      before do
        stub_request(:post, %r{/sharing/agreements/[0-9a-f]+})
        stub_request(:put, %r{/sharing/agreements/[0-9a-f]+})
        @agreement = accounts(:minimum).sharing_agreements.create!(subdomain: accounts(:support).subdomain)
        assert_equal :pending, @account.sharing_agreements.last.status
      end

      describe "a PUT to :update" do
        before do
          put :update, params: { id: @agreement.to_param, sharing_agreement: {status: 'inactive'} }
        end

        should_use_presenter Api::V2::SharingAgreementPresenter, status: :ok

        it "updates a sharing agreement" do
          assert_equal :inactive, @agreement.reload.status
        end
      end

      describe "a DELETE to :destroy" do
        before do
          delete :destroy, params: { id: @agreement.to_param }
        end

        it('responds with no_content') { assert_response :no_content }
        should_change("sharing agreements count", by: -1) { @account.sharing_agreements.count(:all) }
      end
    end
  end

  ["gooddata", "bime"].each do |subsystem_user|
    as_a_subsystem_user(user: subsystem_user, account: :minimum) do
      describe "a GET to :index" do
        should_be_authorized do
          get :index, format: :json
        end
      end
    end
  end

  as_a_subsystem_user(user: 'gooddata', account: :minimum) do
    describe "a GET to :index" do
      before { get :index }
      it('responds with success') { assert_response :success }
    end
  end

  as_a_subsystem_user(user: 'collaboration', account: :minimum) do
    should_be_forbidden :create, :update, :show

    describe 'index' do
      before { get :index }
      it('responds with success') { assert_response :success }
    end
  end
end
