require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::MailInlineImagesController do
  extend Api::V2::TestHelper

  fixtures :users

  with_options(controller: "api/v2/mail_inline_images") do |request|
    request.should_route :get,
      "/api/v2/mail_resources/message_id/images/filename",
      action: "show",
      message_id: "message_id",
      filename: "filename"
  end

  describe "#show" do
    let(:filename) { "attachment_2_square.png" }
    let(:message_id) { "7aa47a71-fe7c-49be-824e-d9bab93761fe" }

    before do
      @minimum_admin = users(:minimum_admin)
      login(@minimum_admin)
      @request.accept = "*/*"
    end

    describe_with_arturo_enabled :email_no_delimiter do
      describe "and remote file is not found" do
        it "returns :not_found" do
          RemoteFiles::MemoryStore.any_instance.stubs(:options).returns(directory: 'zendesk-euc1-csv-export-production')

          get :show, params: { message_id: message_id, filename: filename }

          assert_response :not_found
        end
      end

      describe "and remote file is found" do
        let(:content) { '{"hello": "world"}' }
        let(:remote_file) { RemoteFiles::File.new('file_identifier.txt', content: content, content_type: 'text/plain') }

        it "returns the remote file" do
          Api::V2::MailInlineImagesController.any_instance.stubs(:retrieve_remote_file).returns(remote_file)

          get :show, params: { message_id: message_id, filename: filename }

          assert_response :success
          assert_equal JSON.parse(content), JSON.parse(@response.body)
        end
      end
    end
  end
end
