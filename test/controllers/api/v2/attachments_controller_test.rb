require_relative "../../../support/test_helper"
require_relative "../../../support/attachment_test_helper"

SingleCov.covered!

describe Api::V2::AttachmentsController do
  extend Api::V2::TestHelper
  fixtures :accounts, :tickets, :tokens, :attachments, :users

  let(:statsd_client) { mock('statsd_client') }

  before do
    accept :json
  end

  with_options(controller: 'api/v2/attachments') do |request|
    request.should_route :get, '/api/v2/attachments/1', action: 'show', id: 1
    request.should_route :delete, '/api/v2/attachments/1', action: 'destroy', id: 1
  end

  as_an_anonymous_user do
    # TODO: This test fails as the response is a 401, 403 is expected
    # should_be_forbidden [:get, :show, { id: 1 }], [:delete, :destroy, { id: 1 }]
  end

  as_an_end_user do
    # TODO: This test fails as the response is a 401, 403 is expected
    # should_be_forbidden [:get, :show, { id: 1 }], [:delete, :destroy, { id: 1 }]
  end

  when_logged_in_as(:serialization_agent) do
    describe "with an redacted attachment" do
      before do
        stub_request(:put, %r{\.amazonaws\.com/data/attachments/\d+/redacted\.txt})
        attachment = attachments(:serialization_comment_attachment).redact!
        get :show, params: { id: attachment.id }
      end

      it "is gone" do
        assert_response :gone
      end
    end
  end

  as_an_agent do
    describe "#show" do
      let(:ticket) { tickets(:minimum_1) }
      let(:attachment) do
        attachment = create_attachment("#{Rails.root}/test/files/normal_1.jpg", ticket.requester)
        attachment.update_attribute :ticket_id, ticket.id
        attachment.update_column :source_type, "Comment"
        attachment.update_column :source_id, ticket.comments.last.id
        attachment
      end

      describe "when the agent can view the attachment" do
        describe_with_and_without_arturo_enabled :sse_agent_attachment_view_policy do
          describe "with a thumbnail" do
            before { get :show, params: { id: attachment.thumbnails.first.id } }
            it { assert_response :not_found }
          end

          describe "with a valid attachment" do
            before { get :show, params: { id: attachment.id } }
            should_use_presenter Api::V2::AttachmentPresenter
          end
        end

        describe "when the agent cannot view the ticket" do
          before do
            admin = users(:minimum_admin)
            ticket.assignee = admin
            ticket.will_be_saved_by(admin)
            ticket.save!

            @user.update_column :restriction_id, RoleRestrictionType.ASSIGNED

            get :show, params: { id: attachment.id }
          end

          describe_with_arturo_enabled :sse_agent_attachment_view_policy do
            it { assert_response :forbidden }
          end

          describe_with_arturo_enabled :sse_agent_attachment_view_policy_log_only do
            before do
              stub_statsd_client
              stub_statsd_client('attachments')

              Rails.logger.stubs(:info)
              Rails.logger.expects(:info).with('[Agent attachment policy API] API request would be blocked. Agent cannot view ticket.').once

              statsd_client.stubs(:increment)
              statsd_client.stubs(:histogram)
              statsd_client.expects(:increment).with('agent_policy_change.api_request_denied')
            end

            it { get :show, params: { id: attachment.id } }
          end
        end
      end
    end

    describe "#destroy" do
      describe "with an entry attachment" do
        let(:attachment) { attachments(:attachment_entry) }

        describe "when the agent can delete the attachment" do
          before do
            # needed because, #destroy tries to remove an nonexistant
            # file and then fails
            Attachment.any_instance.expects(:destroy).returns(true)
          end

          it "responds with 204" do
            delete :destroy, params: { id: attachment.id }
            assert_response :no_content
          end
        end

        describe "when the agent cannot delete the attachment" do
          before do
            @user.expects(:can?).with(:delete, attachment).returns(false)
            Attachment.any_instance.expects(:destroy).never

            delete :destroy, params: { id: attachment.id }
          end

          it('responds with forbidden') { assert_response :forbidden }
        end
      end

      describe "with an upload token" do
        let(:attachment) { attachments(:upload_token_attachment) }

        before do
          Attachment.any_instance.expects(:destroy).returns(true)
          delete :destroy, params: { id: attachment.id }
        end

        it('responds with no_content') { assert_response :no_content }
      end

      describe "with a post attachment" do
        let(:attachment) { attachments(:attachment_post) }

        before do
          delete :destroy, params: { id: attachment.id }
        end

        it('responds with not_found') { assert_response :not_found }
      end
    end
  end

  def stub_statsd_client(namespace = 'attachment_fu')
    Zendesk::StatsD::Client.stubs(:new).with(namespace: namespace).returns(statsd_client)
  end
end
