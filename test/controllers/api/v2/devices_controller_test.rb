require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::DevicesController do
  extend Api::V2::TestHelper
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :devices, :role_settings, :tokens

  before do
    accept :json
    @account = accounts(:minimum)
    @user    = users(:minimum_agent)
    @device  = devices(:minimum)
    @device.update_attribute(:last_active_at, Time.now)
  end

  with_options(controller: 'api/v2/devices') do |request|
    request.should_route :get, '/api/v2/users/123/devices', action: 'index', user_id: '123'
    request.should_route :put, '/api/v2/users/456/devices/78', action: 'update', user_id: '456', id: '78'
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index, {user_id: 1}]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index, {user_id: 1}]
  end

  as_an_agent do
    describe "a GET to :index" do
      before do
        @old_device = devices(:complete)
        @old_device.update_attribute(:last_active_at, Time.now - 1.month)
        get :index, params: { user_id: @user.id }
      end

      should_paginate :devices
      should_use_presenter Api::V2::DevicePresenter, status: 200

      it "presents all devices for the current user" do
        results = JSON.parse(@response.body)['devices']
        assert_equal [@device.id, @old_device.id], results.map { |d| d['id'] }
      end
    end

    describe "a GET to :index with mobile devices" do
      before do
        @mobile_device = devices(:complete)
        @mobile_device.update_attribute(:type, "MobileDevice")
        @token = tokens(:token_one)
        @mobile_device.update_attribute(:oauth_token_id, @token.id)
      end

      describe_with_and_without_arturo_enabled :filter_mobile_devices do
        it "shows mobile devices with valid tokens" do
          get :index, params: { user_id: @user.id }
          results = JSON.parse(@response.body)['devices']

          assert @user.mobile_devices.count == 1
          assert_equal [@device.id, @mobile_device.id], results.map { |d| d['id'] }
        end

        it "does not show mobile devices without valid tokens" do
          @mobile_device.update_attribute(:oauth_token_id, nil)

          get :index, params: { user_id: @user.id }
          results = JSON.parse(@response.body)['devices']

          # assert that we have not deleted the device itself
          assert @user.mobile_devices.count == 1

          if @arturo_enabled
            assert_equal [@device.id], results.map { |d| d['id'] }
          else
            assert_equal [@device.id, @mobile_device.id], results.map { |d| d['id'] }
          end
        end

        # possible edge case
        it "does not show devices from another account that has a user with the same id" do
          @mobile_device.update_attribute(:account_id, (@account.id * 2))

          get :index, params: { user_id: @user.id }
          results = JSON.parse(@response.body)['devices']

          assert @user.mobile_devices.count == 1
          assert_equal [@device.id], results.map { |d| d['id'] }
        end
      end
    end

    describe "a PUT to :update" do
      it "updates the device name" do
        put :update, params: { user_id: @user.id, id: @device.id, device: { name: 'Ramen' } }
        assert_equal 'Ramen', @device.reload.name, "Expected the database record to be updated"

        assert_response :ok

        json = JSON.parse(@response.body)
        assert_not_nil json['device'], 'Expected the JSON response to have a "device"'
        assert_equal 'Ramen', json['device']['name'], "Expected the new name in the JSON response"
      end

      it 'does not be able to update other attributes' do
        @device.update_column(:ip, '1.2.3.4')
        put :update, params: { user_id: @user.id, id: @device.id, device: { ip: '127.0.0.1' } }
        assert_response :bad_request
        assert_equal '1.2.3.4', @device.reload.ip
      end
    end

    describe "a DELETE to :destroy" do
      it "removes that device" do
        delete :destroy, params: { user_id: @user.id, id: @device.id }
        assert_nil Device.find_by_id(@device.id)
        assert_response :no_content
      end

      it "does not remove a device from another user" do
        @device.update_attribute(:user_id, 9999)
        delete :destroy, params: { user_id: @user.id, id: @device.id }
        assert Device.find_by_id(@device.id)
        assert_response :not_found
      end

      it "responds with 204" do
        delete :destroy, params: { user_id: @user.id, id: @device.id }
        assert_response :no_content
      end
    end
  end
end
