require_relative "../../../support/test_helper"
require_relative "../../../support/collaboration_settings_test_helper"

SingleCov.covered!

describe Api::V2::CollaboratorsController do
  extend Api::Presentation::TestHelper
  fixtures :tickets, :accounts, :users

  before do
    @ticket   = tickets(:minimum_3)
    @agent    = users(:minimum_agent)
    @account  = accounts(:minimum)
    @collaboration = Collaboration.create!(user: @agent, ticket: @ticket, account: @account)

    accept :json
  end

  with_options(controller: 'api/v2/collaborators') do |request|
    request.should_route :get, '/api/v2/tickets/1/collaborators', action: 'index', ticket_id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index, {ticket_id: 1}]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index, {ticket_id: 1}]
  end

  as_an_agent do
    describe "a GET to :index" do
      before { get :index, params: { ticket_id: @ticket.nice_id } }

      should_use_presenter Api::V2::Users::AgentPresenter, status: :ok
    end

    describe 'a GET to :index, and the agent can only view assigned tickets' do
      before do
        @agent.restriction_id = RoleRestrictionType.ASSIGNED
        @agent.save!
      end

      describe 'when the ticket is not assigned to the agent' do
        before do
          @ticket = tickets(:minimum_2)
          get :index, params: { ticket_id: @ticket.nice_id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe 'when the ticket is assigned to the agent' do
        before do
          @ticket = tickets(:minimum_1)
        end

        it 'responds with success' do
          get :index, params: { ticket_id: @ticket.nice_id }
          assert_response :success
        end

        describe "transforming and filtering collaborators" do
          let(:end_user_legacy_cc) { FactoryBot.build_stubbed(:end_user_legacy_cc) }
          let(:agent_legacy_cc) { FactoryBot.build_stubbed(:agent_legacy_cc) }
          let(:end_user_email_cc) { FactoryBot.build_stubbed(:end_user_email_cc) }
          let(:agent_email_cc) { FactoryBot.build_stubbed(:agent_email_cc) }
          let(:agent_follower) { FactoryBot.build_stubbed(:agent_follower) }

          let(:legacy_collaborations) { [end_user_legacy_cc, agent_legacy_cc] }
          let(:email_ccs) { [end_user_email_cc, agent_email_cc] }
          let(:followers) { [agent_follower] }
          let(:new_collaborations) { email_ccs + followers }
          let(:all_collaborations) { legacy_collaborations + new_collaborations }

          def user_ids_from_response(response_body)
            JSON.parse(response_body)["users"].map { |user| user["id"] }
          end

          describe_with_arturo_enabled :email_ccs do
            describe "when follower_and_email_cc_collaborations setting is on" do
              before do
                Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: true)
                Collaboration.expects(:with_active_users).at_least_once.returns(all_collaborations)
              end

              it "calls the Legacy Type Transformer with active users" do
                Collaboration::LegacyTypeTransformer.expects(:filtered_collaborators).
                  with(
                    collaborations: all_collaborations,
                    settings: {
                      comment_email_ccs_allowed_enabled: true,
                      ticket_followers_allowed_enabled: true,
                      follower_and_email_cc_collaborations_enabled: true
                    }
                  ).returns([])
                get :index, params: { ticket_id: @ticket.nice_id }
              end

              describe "and followers setting is disabled" do
                before do
                  CollaborationSettingsTestHelper.new(
                    feature_arturo: true,
                    feature_setting: true,
                    followers_setting: false,
                    email_ccs_setting: true
                  ).apply
                end

                it "filters out followers and agent legacy CCs" do
                  get :index, params: { ticket_id: @ticket.nice_id }
                  expected_user_ids = (all_collaborations - followers - [agent_legacy_cc]).map(&:user_id)
                  assert_equal expected_user_ids.sort, user_ids_from_response(@response.body).sort
                end
              end

              describe "and email_ccs setting is disabled" do
                before do
                  CollaborationSettingsTestHelper.new(
                    feature_arturo: true,
                    feature_setting: true,
                    followers_setting: true,
                    email_ccs_setting: false
                  ).apply
                end

                it "filters out email CCs and end user legacy CCs" do
                  get :index, params: { ticket_id: @ticket.nice_id }
                  expected_user_ids = (all_collaborations - email_ccs - [end_user_legacy_cc]).map(&:user_id)
                  assert_equal expected_user_ids.sort, user_ids_from_response(@response.body).sort
                end
              end
            end

            describe "when follower_and_email_cc_collaborations setting is off" do
              before { Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: false) }

              it "calls the Legacy Type Transformer with active users" do
                Collaboration::LegacyTypeTransformer.expects(:filtered_collaborators).
                  with(
                    collaborations: [],
                    settings: {
                      comment_email_ccs_allowed_enabled: false,
                      ticket_followers_allowed_enabled: false,
                      follower_and_email_cc_collaborations_enabled: false
                    }
                  ).returns([])
                get :index, params: { ticket_id: @ticket.nice_id }
              end

              it "returns all collaborators" do
                Collaboration.expects(:with_active_users).at_least_once.returns(all_collaborations)
                get :index, params: { ticket_id: @ticket.nice_id }
                expected_user_ids = all_collaborations.map(&:user_id)
                assert_equal expected_user_ids.sort, user_ids_from_response(@response.body).sort
              end
            end
          end

          describe_with_arturo_disabled :email_ccs do
            it "does not call the Legacy Type Transformer" do
              Collaboration::LegacyTypeTransformer.expects(:transform).never
              get :index, params: { ticket_id: @ticket.nice_id }
            end

            it "returns all collaborators" do
              Ticket.any_instance.expects(:collaborators).returns([users(:minimum_end_user)])
              get :index, params: { ticket_id: @ticket.nice_id }
              assert_equal [users(:minimum_end_user).id], user_ids_from_response(@response.body)
            end
          end
        end
      end
    end
  end
end
