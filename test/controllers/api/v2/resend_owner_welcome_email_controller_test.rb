require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ResendOwnerWelcomeEmailController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users

  before do
    accept :json
  end

  with_options(controller: 'api/v2/resend_owner_welcome_email') do |request|
    request.should_route :post, 'api/v2/resend_owner_welcome_email', action: 'resend_owner_welcome_email'
  end

  as_an_anonymous_user do
    let(:actual_error_message) { JSON.parse(response.body).fetch('description') }

    before do
      @account = accounts(:minimum)
      Rails.logger.stubs(:info)
      @statsd_client = @controller.send(:statsd_client)
      Prop.reset(:resend_owner_welcome_email, @account.id)
    end

    describe 'when email is invalid' do
      before { post :resend_owner_welcome_email, params: { email: 'harry@hogwarts' } }

      it 'responds with error' do
        assert_response :unprocessable_entity
        assert_equal 'Email format is invalid', actual_error_message
      end
    end

    describe 'when account is a NOT a trial' do
      let(:expected_message) { 'Not accessible to a non-trial account' }

      before do
        @account.stubs(:is_trial?).returns(false)
        AccountsMailer.expects(:deliver_welcome_account_owner).never
        @statsd_client.expects(:increment).with('restricted', tags: ['reason:not_trial_account']).once
        post :resend_owner_welcome_email, params: { email: @account.owner.email }
      end

      it 'responds with error' do
        assert_response :unprocessable_entity
        assert_equal expected_message, actual_error_message
      end
    end

    describe 'when account is a trial' do
      before { @account.stubs(:is_trial?).returns(true) }

      describe 'if account owner verified' do
        let(:expected_message) { 'Your account owner is already verified' }

        before do
          assert @account.owner.is_verified?
          AccountsMailer.expects(:deliver_welcome_account_owner).never
          @statsd_client.expects(:increment).with('restricted', tags: ['reason:owner_verified']).once
          post :resend_owner_welcome_email, params: { email: @account.owner.email }
        end

        it 'responds with error' do
          assert_response :unprocessable_entity
          assert_equal expected_message, actual_error_message
        end
      end

      describe 'if account owner unverified' do
        before do
          @account.owner.is_verified = false
          @account.owner.save!

          refute @account.owner.is_verified?
        end

        describe 'with correct owner email' do
          before do
            AccountsMailer.expects(:deliver_welcome_account_owner).once
            @statsd_client.expects(:increment).with('welcome_account_owner_email_sent', tags: ['reason:unverified_owner_match']).once
            post :resend_owner_welcome_email, params: { email: @account.owner.email }
          end

          it 'sends email' do
            assert_response :ok
          end
        end

        describe 'rate limited' do
          let(:expected_message) { 'Number of allowed requests for sending owner welcome emails exceeded' }

          before do
            Timecop.freeze do
              Prop.throttle!(:resend_owner_welcome_email, @account.id, increment: 1)

              post :resend_owner_welcome_email, params: { email: @account.owner.email }
            end
          end

          it 'reponds with too many requests' do
            assert_response 429
            assert_equal expected_message, actual_error_message
          end
        end

        describe 'with incorrect owner email' do
          let(:provided_email) { 'incorrect@example.com' }
          let(:previous_owner_email) { @account.owner.email }

          before do
            AccountsMailer.expects(:deliver_owner_email_change).once
            AccountsMailer.expects(:deliver_welcome_account_owner).once

            @statsd_client.expects(:increment).with('welcome_account_owner_email_sent', tags: ['reason:unverified_owner_change']).once
            @statsd_client.expects(:increment).with('owner_email_change_email_sent', tags: ['reason:unverified_owner_change']).once

            Rails.logger.expects(:info).with("Account #{@account.subdomain} owner email changed from #{previous_owner_email} to #{provided_email}")
          end

          describe 'verification token exists on owner' do
            before do
              @original_verification_token = @account.owner.identities.first.verification_token
              refute_empty @account.owner.identities.first.verification_token

              post :resend_owner_welcome_email, params: { email: provided_email }
              @account.reload
            end

            it 'overwrites the original verification token' do
              assert_not_equal @original_verification_token, @account.owner.identities.first.verification_token
            end

            it 'changes the owner email to the provided email' do
              assert_equal provided_email, @account.reload.owner.email
            end

            it 'sends emails' do
              assert_response :ok
            end
          end

          describe 'verification token does not exist on owner' do
            before do
              assert_empty @account.owner.identities.first.verification_tokens

              post :resend_owner_welcome_email, params: { email: provided_email }
              @account.reload
            end

            it 'creates a verification token' do
              assert @account.owner.identities.first.verification_token
            end

            it 'changes the owner email to the provided email' do
              assert_equal provided_email, @account.reload.owner.email
            end

            it 'sends emails' do
              assert_response :ok
            end
          end
        end
      end
    end
  end
end
