require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::AuditLogsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :users, :role_settings, :rules

  before do
    accept :json
  end

  with_options(controller: "api/v2/audit_logs") do |request|
    request.should_route :get,  "/api/v2/audit_logs",     action: "index"
    request.should_route :get,  "/api/v2/audit_logs/123", action: "show", id: "123"
  end

  describe "when using oauth tokens" do
    let(:event) do
      CIA::Event.create!(
        account: account,
        actor: account.owner,
        source: account.owner,
        action: "create",
        visible: true
      )
    end

    with_scopes('auditlogs:read', 'read') do
      before do
        @token.user.stubs(can_see_audit_log?: true)
      end

      should_be_authorized { get :index }
      should_be_authorized { get :show, params: { id: event.id } }
      describe_with_arturo_enabled :audit_logs_export_api_enabled do
        should_be_authorized { post :export }
      end
    end

    with_scopes('write') do
      before do
        @token.user.stubs(can_see_audit_log?: true)
      end

      should_not_be_authorized { get :index }
      should_not_be_authorized { post :export }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :show, :export
  end

  as_an_end_user do
    should_be_forbidden :index, :show, :export
  end

  as_an_agent do
    should_be_forbidden :index, :show, :export
  end

  as_an_admin do
    before do
      Subscription.any_instance.expects(has_audit_log?: true).at_least_once
    end

    describe "a GET to :index" do
      let(:audit_logs) { JSON.parse(@response.body).with_indifferent_access[:audit_logs] }

      describe "with no parameters" do
        let(:account) { accounts(:minimum) }

        it 'uses correct presenter' do
          Api::V2::AuditLogPresenter.any_instance.expects(:present)
          get :index
          assert_response :ok
        end

        it 'returns empty audits with success' do
          get :index
          assert_response :ok
          assert_empty audit_logs
        end

        it 'returns an audit' do
          CIA::Event.create!(account: account, actor: account.owner, source: account.owner, action: "create", visible: true, message: 'foo bar')
          get :index
          assert_response :ok
          assert_equal 1, audit_logs.size
          assert_equal 'create', audit_logs[0][:action]
          assert_equal account.owner.id, audit_logs[0][:source_id]
          assert_equal 'user', audit_logs[0][:source_type]
          assert_equal account.owner.name, audit_logs[0][:source_label]
          assert_equal '', audit_logs[0][:change_description]
        end

        describe_with_arturo_disabled :audit_logs_api_include_login_events do
          it 'returns an audit not including the message for login events' do
            CIA::Event.create!(account: account, actor: account.owner, source: account.owner, action: "login", visible: true, message: "Agent logged in")
            get :index
            assert_response :ok
            assert_equal 1, audit_logs.size
            assert_equal '', audit_logs[0][:change_description]
          end
        end

        describe_with_arturo_enabled :audit_logs_api_include_login_events do
          it 'returns an audit containing the message for login events' do
            CIA::Event.create!(account: account, actor: account.owner, source: account.owner, action: "login", visible: true, message: "Agent logged in")
            get :index
            assert_response :ok
            assert_equal 1, audit_logs.size
            assert_equal 'Agent logged in', audit_logs[0][:change_description]
          end
        end
      end

      describe "with filter params" do
        it "accepts valid created_at" do
          get :index, params: { filter: { created_at: ['2013-01-01T00:00:00Z', '2013-01-02T00:00:00Z'] } }
          assert_response :ok
        end

        describe "filtering by source_type" do
          before do
            rules = [
              { type: "trigger", fixture: :trigger_notify_requester_of_received_request },
              { type: "macro", fixture: :macro_incident_escalation },
              { type: "view", fixture: :view_my_working_tickets }
            ]

            @rule_events = {}
            rules.each do |rule|
              @rule_events[rule[:type]] = CIA::Event.create!(
                account: @account,
                actor: @account.owner,
                source: rules(rule[:fixture]),
                action: "create",
                visible: true
              )
            end
          end

          it "accepts filtering by source_type rule" do
            get :index, params: { filter: { source_type: 'rule' } }
            assert_equal @rule_events.size, audit_logs.size
            assert_response :ok
          end

          ['trigger', 'macro', 'view'].each do |rule_subtype|
            it "accepts filtering by source_type trigger, macro or view" do
              get :index, params: { filter: { source_type: rule_subtype } }
              assert_equal @rule_events[rule_subtype].id, audit_logs[0]['id']
            end
          end
        end

        it "responds with error on invalid created_at" do
          get :index, params: { filter: { created_at: 'foo' } }
          assert_response :bad_request
          expected_response = {
            "error" => "InvalidValue",
            "description" => "Invalid filter[created_at] specified; format is ?filter[created_at][]=YYYY-MM-DDTHH:MM:SSZ&filter[created_at][]=YYYY-MM-DDTHH:MM:SSZ"
          }
          assert_equal expected_response, JSON.parse(@response.body)
        end

        it "responds with error on invalid length created_at" do
          get :index, params: { filter: { created_at: ['foo'] } }
          assert_response :bad_request
          expected_response = {
            "error" => "InvalidValue",
            "description" => "Invalid filter[created_at] specified; format is ?filter[created_at][]=YYYY-MM-DDTHH:MM:SSZ&filter[created_at][]=YYYY-MM-DDTHH:MM:SSZ"
          }
          assert_equal expected_response, JSON.parse(@response.body)
        end

        it "responds with error on invalid created_at dates" do
          get :index, params: { filter: { created_at: ['foo', 'bar'] } }
          assert_response :bad_request
          expected_response = {
            "error" => "InvalidValue",
            "description" => "Invalid filter[created_at] specified; format is ?filter[created_at][]=YYYY-MM-DDTHH:MM:SSZ&filter[created_at][]=YYYY-MM-DDTHH:MM:SSZ"
          }
          assert_equal expected_response, JSON.parse(@response.body)
        end
      end
    end

    describe "a GET to :show" do
      let(:event) do
        CIA::Event.create!(
          account: accounts(:minimum),
          actor: users(:minimum_agent),
          source: users(:minimum_agent),
          action: "create",
          visible: true
        )
      end

      before { get :show, params: { id: event.id } }
      should_use_presenter Api::V2::AuditLogPresenter, status: :ok
    end

    describe "a POST to :export" do
      let(:user) { users(:minimum_admin_not_owner) }

      describe_with_arturo_disabled :audit_logs_export_api_enabled do
        it "returns with access-denied" do
          AuditLogsExportJob.expects(:enqueue).with(user.account_id, user.id).never
          post :export
          assert_response :forbidden
        end
      end

      describe_with_arturo_enabled :audit_logs_export_api_enabled do
        before do
          Timecop.freeze # For rate-limit tests
        end

        it "successfully enqueues the export job" do
          AuditLogsExportJob.expects(:enqueue).with(user.account_id, user.id, filter: nil).once
          post :export
          assert_response :accepted
        end

        it "passes filters to the export job" do
          AuditLogsExportJob.expects(:enqueue).with(user.account_id, user.id, filter: { 'created_at' => ['2019-01-01T00:00:00Z', '2020-01-01T00:00:00Z'] }).once
          post :export, params: { filter: { created_at: ['2019-01-01T00:00:00Z', '2020-01-01T00:00:00Z'] } }
          assert_response :accepted
        end

        it "has a rate-limit" do
          post :export
          assert_response :accepted
          post :export
          assert_response :too_many_requests
        end
      end
    end
  end
end
