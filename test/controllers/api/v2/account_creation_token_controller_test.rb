require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::AccountCreationTokenController do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    accept :json
    @account = accounts(:minimum)
    @account.subdomain = "zendeskaccounts"
    @account.clients.create!(user_id: @account.users.admins.first.id, name: "Account Creation", identifier: "account_creation", secret: "fake_secret")
    @account.save!
  end

  with_options(controller: 'api/v2/account_creation_token') do |request|
    request.should_route :post, '/api/v2/account_creation_token', action: 'create'
  end

  as_an_anonymous_user do
    should_be_unauthorized :create
  end

  as_an_end_user do
    should_be_forbidden :create
  end

  as_an_agent do
    should_be_forbidden :create
  end

  as_an_admin do
    describe "a POST to :create with subdomain - zendeskaccounts" do
      let(:user) { @account.users.where(name: 'this_is_not_an_existing_user').first }
      let(:json) { JSON.parse(response.body).fetch('oauth_token') }

      describe "when the user does not exist" do
        describe "uses AccountCreationTokenPresenter" do
          before do
            post :create, params: { token_user_name: "this_is_not_an_existing_user" }
          end

          should_use_presenter Api::V2::AccountCreationTokenPresenter
        end

        it "creates a new user" do
          assert_difference '@account.users.count', 1 do
            post :create, params: { token_user_name: "this_is_not_an_existing_user" }
          end

          refute_nil user
          assert_response :ok
        end

        it 'creates the correct token for this user' do
          post :create, params: { token_user_name: "this_is_not_an_existing_user" }
          assert_response :ok

          token = user.tokens.first

          assert_equal 1, user.tokens.count
          assert_equal token.token(true), json.fetch('token')

          assert_equal(["accounts:write"], token.scopes)
          assert_equal "account_creation", token.client.identifier
        end
      end

      describe "when the user and token already exist" do
        before do
          post :create, params: { token_user_name: "this_is_not_an_existing_user" }
          @existing_token = JSON.parse(response.body).fetch('oauth_token')

          assert_response :ok
        end

        describe "when user has only one token" do
          it "does not create a new user" do
            assert_no_difference '@account.users.count' do
              post :create, params: { token_user_name: "this_is_not_an_existing_user" }
            end
          end

          it "returns the same token" do
            post :create, params: { token_user_name: "this_is_not_an_existing_user" }
            assert_equal @existing_token.fetch('token'), json.fetch('token')
          end
        end

        describe "when the user has multiple tokens" do
          before do
            Timecop.freeze(5.minutes.from_now)
            @latest_token = user.tokens.create!(account: @account, client: @account.clients.first, scopes: ['accounts:write'])
          end

          it "responds with the latest token" do
            post :create, params: { token_user_name: "this_is_not_an_existing_user" }

            assert_equal json.fetch('id'), @latest_token.id
            assert_equal json.fetch('user_id'), @latest_token.user_id
            assert_equal json.fetch('token'), @latest_token.token(true)
          end
        end
      end
    end

    describe "a POST to :create with subdomain other than zendeskaccounts" do
      before do
        @account.subdomain = "minimum"
        @account.save!
        post :create, params: { token_user_name: "this_is_not_an_existing_user" }
      end

      it ('responds with not_found') { assert_response :not_found }
    end
  end
end
