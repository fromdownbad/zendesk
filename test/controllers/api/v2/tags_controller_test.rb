require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::TagsController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :users, :tickets, :organizations, :entries, :role_settings, :account_settings

  with_options(controller: "api/v2/tags") do |request|
    request.should_route :get, "/api/v2/tags", action: "index"
    request.should_route :get, "/api/v2/tags/count", action: "count"

    request.should_route :get, "/api/v2/tickets/1/tags", action: "show", ticket_id: 1
    request.should_route :post, "/api/v2/tickets/1/tags", action: "create", ticket_id: 1
    request.should_route :put, "/api/v2/tickets/1/tags", action: "update", ticket_id: 1
    request.should_route :delete, "/api/v2/tickets/1/tags", action: "destroy", ticket_id: 1

    request.should_route :get, "/api/v2/organizations/1/tags", action: "show", organization_id: 1
    request.should_route :post, "/api/v2/organizations/1/tags", action: "create", organization_id: 1
    request.should_route :put, "/api/v2/organizations/1/tags", action: "update", organization_id: 1
    request.should_route :delete, "/api/v2/organizations/1/tags", action: "destroy", organization_id: 1

    request.should_route :get, "/api/v2/users/1/tags", action: "show", user_id: 1
    request.should_route :post, "/api/v2/users/1/tags", action: "create", user_id: 1
    request.should_route :put, "/api/v2/users/1/tags", action: "update", user_id: 1
    request.should_route :delete, "/api/v2/users/1/tags", action: "destroy", user_id: 1
  end

  before do
    accept :json
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :show, :create, :update, :destroy, add_to_all: {ticket_id: 1}
  end

  as_an_end_user do
    should_be_forbidden :index, :show, :create, :update, :destroy, add_to_all: {ticket_id: 1}
  end

  as_an_agent do
    [:organization, :ticket, :user].each do |model|
      describe "with #{model}" do
        before do
          send model
        end

        should_be_forbidden :index

        describe "a GET to :show for a thing that exists" do
          before do
            get :show, params: @params
          end

          it "renders tags" do
            assert_equal({ "tags" => %w[test] }, JSON.parse(@response.body))
          end
        end

        describe "when user has ability to modify tags" do
          before do
            @controller.expects(:can_modify_tags?).returns(true)
          end

          describe "a POST to :create" do
            before do
              post :create, params: @params.merge(tags: %w[blah 123])
            end

            it "creates the tags" do
              assert_response :created
              assert_match %r{api/v2/#{model}s/\d+/tags.json}, @response.headers['Location']
              assert_equal %w[123 blah], JSON.parse(@response.body)["tags"].sort
            end
          end

          describe "a PUT to :update" do
            before do
              put :update, params: @params.merge(tags: %w[blah])
            end

            it "renders tags" do
              assert_equal(%w[blah test], JSON.parse(@response.body)['tags'].sort)
            end
          end

          describe "a DELETE to :destroy" do
            before do
              delete :destroy, params: @params.merge(tags: %w[test])
            end

            it "renders tags" do
              assert_equal({ "tags" => [] }, JSON.parse(@response.body))
            end
          end
        end

        describe "when user does not have ability to modify tags" do
          before do
            @controller.expects(:can_modify_tags?).returns(false)
          end

          describe "a POST to :create" do
            before do
              post :create, params: @params.merge(tags: %w[blah 123])
            end

            it "denies access" do
              assert_response :forbidden
            end
          end

          describe "a PUT to :update" do
            before do
              put :update, params: @params.merge(tags: %w[blah])
            end

            it "denies access" do
              assert_response :forbidden
            end
          end

          describe "a DELETE to :destroy" do
            before do
              delete :destroy, params: @params.merge(tags: %w[test])
            end

            it "denys access" do
              assert_response :forbidden
            end
          end
        end
      end
    end

    describe "when passing via channel info along with update ticket's tags" do
      before do
        send :ticket
        @params_with_via_channel = @params.merge(tags: %w[blah], via: {'channel': 'chat'})

        put :update, params: @params_with_via_channel
      end

      it "audits via channel and renders tags" do
        @ticket.reload
        assert_equal({ "tags" => %w[blah test] }, JSON.parse(@response.body))
        assert_equal(
          @ticket.audits.last.via_id,
          Zendesk::Types::ViaType.find(@params_with_via_channel[:via][:channel])
        )
      end
    end

    describe "when using safe_update without an updated_stamp" do
      before do
        send :ticket
        @ticket.additional_tags = ['blah']
        @ticket.will_be_saved_by(@user)
        @ticket.save!

        assert_equal ['blah', 'test'], @ticket.reload.tag_array

        delete :destroy, params: @params.merge(tags: %w[test], safe_update: 'true')
      end

      it "returns with :conflict" do
        assert_response :conflict
      end
    end

    describe "when using safe_update to destroy ticket's tags" do
      before do
        send :ticket
        @ticket.additional_tags = ['blah']
        @ticket.will_be_saved_by(@user)
        @ticket.save!

        assert_equal ['blah', 'test'], @ticket.reload.tag_array

        delete :destroy, params: @params.merge(tags: %w[test], safe_update: 'true', updated_stamp: updated_stamp)
      end

      describe "with an outdated updated_stamp" do
        let(:updated_stamp) { (@ticket.updated_at - 2.minutes).iso8601 }

        it "returns with :conflict" do
          assert_response :conflict
        end
      end

      describe "with an up-to-date updated_stamp" do
        let(:updated_stamp) { @ticket.updated_at.iso8601 }

        it "renders tags" do
          assert_equal 'blah', @ticket.reload.current_tags
          assert_equal({ "tags" => %w[blah] }, JSON.parse(@response.body))
        end
      end
    end

    describe "when safe_update is set to false" do
      before do
        send :ticket

        delete :destroy, params: @params.merge(tags: %w[test], safe_update: 'false', updated_stamp: (@ticket.updated_at - 2.minutes).iso8601)
      end

      it "renders tags without checking updated_stamp" do
        assert_equal({ "tags" => [] }, JSON.parse(@response.body))
      end
    end

    describe "when using safe_update to update ticket's tags" do
      before do
        send :ticket

        put :update, params: @params.merge(tags: %w[blah], safe_update: 'true', updated_stamp: updated_stamp)
      end

      describe "with an outdated updated_stamp" do
        let(:updated_stamp) { (@ticket.updated_at - 2.minutes).iso8601 }

        it "returns with :conflict" do
          assert_response :conflict
        end
      end

      describe "with an up-to-date updated_stamp" do
        let(:updated_stamp) { @ticket.updated_at.iso8601 }

        it "renders tags" do
          assert_equal({ "tags" => %w[blah test] }, JSON.parse(@response.body))
        end
      end
    end

    describe "when safe_update is set to false" do
      before do
        send :ticket

        put :update, params: @params.merge(tags: %w[blah], safe_update: 'false', updated_stamp: (@ticket.updated_at - 2.minutes).iso8601)
      end

      it "renders tags without checking updated_stamp" do
        assert_equal({ "tags" => %w[blah test] }, JSON.parse(@response.body))
      end
    end
  end

  as_an_admin do
    describe "a GET to :index" do
      before { get :index }
      should_use_presenter Api::V2::TagScorePresenter, status: :ok
    end

    describe "a GET to :count" do
      let(:account) { accounts(:minimum) }
      let(:iso_date) { '2020-07-23T03:21:55+00:00' }

      before do
        Zendesk::RecordCounter::TagScores.any_instance.stubs(:iso_date).returns(iso_date)
        get :count
      end

      it "should respond with the correct format and values" do
        expected_response = {
          'count' => {
            'value' => account.tag_scores.count,
            'refreshed_at' => iso_date
          },
          'links' => {
            'url' => 'https://minimum.zendesk-test.com/api/v2/tags/count'
          }
        }

        assert_equal expected_response, JSON.parse(@response.body)
        assert_response :success
      end
    end

    describe "throttling the API" do
      let(:account) { accounts(:minimum) }

      before do
        Timecop.freeze(Time.zone.parse('2019-03-17 08:28:43'))
      end

      after { Timecop.return }

      describe "with throttling tags index" do
        before do
          account.settings.tags_index_threshold = 1
          account.save!
          Arturo.enable_feature!(:throttle_endpoint_limit_tag_index_api)
          Arturo.enable_feature!(:throttle_endpoint_limit_tag_index_api_master)
        end

        it "should not respond with success" do
          Arturo.enable_feature!(:use_scaling_strategies_for_rate_limiting)
          Arturo.disable_feature!(:suppress_sending_datadog_threshold_data)
          Account.any_instance.stubs(:created_at).returns(Time.parse('2021-07-02'))
          get :index
          assert_response :success

          statsd_client = @controller.send(:statsd_client)
          statsd_client.expects(:increment).with("throttled.v2_tags_index", tags: ["log_only:false", "account:#{account.subdomain}", 'trial:false', 'sandbox:false', 'grandfathered:false', 'page:-1', 'request_count:1', 'limit:1', 'plan_type:Medium', 'strategy:settings', 'high_volume_account:false']).once

          get :index
          assert_response 429
        end

        describe "limit types" do
          before do
            account.settings.tags_index_threshold = nil
            account.settings.save!
          end

          it 'should use the pagination limits' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2021-06-30'))
            Prop.expects(:throttle!).with(:v2_tags_index_pagination, account.id,
              threshold: 3000,
              interval: 1.minute,
              description: ::I18n.t("txt.admin.controllers.tags_controller.limited_tags_index_pagination")).returns(1)

            get :index, params: { page: 105 }
          end

          it 'should use the deep pagination limits' do
            Account.any_instance.stubs(:created_at).returns(Time.parse('2021-06-30'))
            Prop.expects(:throttle!).with(:v2_tags_index_deep_pagination, account.id,
              threshold: 100,
              interval: 1.minute,
              description: ::I18n.t("txt.admin.controllers.tags_controller.limited_tags_index_pagination")).returns(1)

            get :index, params: { page: 600 }
          end
        end
      end
    end

    describe 'without specific pagination parameters' do
      let(:account) { accounts(:minimum) }
      let(:json) do
        get :index
        JSON.parse(@response.body).with_indifferent_access
      end

      it 'uses offset based pagination by default' do
        assert json.key?(:next_page)
        assert json.key?(:previous_page)
      end
    end

    describe 'using offset based pagination' do
      let(:account) { accounts(:minimum) }
      let(:json) do
        get :index, params: { page: 2, per_page: 1 }
        JSON.parse(@response.body).with_indifferent_access
      end

      it 'presents the correct keys' do
        assert json[:previous_page].present?
        assert json[:next_page].present?
      end
    end

    # For a complete set of tests, go to `zendesk_cursor_pagination` gem.
    describe 'using cursor based pagination' do
      let(:account) { accounts(:minimum) }
      let(:page_size) { 2 }
      let(:page_params) { { size: page_size } }
      let(:json) do
        get :index, params: { page: page_params }
        JSON.parse(@response.body).with_indifferent_access
      end

      it 'generates the correct order clause' do
        assert_sql_queries(1, /ORDER BY `tag_scores`.`score` DESC, `tag_scores`.`id` ASC/) do
          json
        end
      end

      describe 'when there is only one page' do
        let(:page_size) { account.tag_scores.size + 1 }

        it 'presents the correct keys' do
          refute json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of tags' do
          assert_equal account.tag_scores.size, json[:tags].size
        end
      end

      describe 'when there are multiple pages' do
        let(:page_size) { account.tag_scores.size - 1 }

        it 'presents the correct keys' do
          assert json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of tags' do
          assert_equal page_size, json[:tags].size
        end
      end

      describe 'with a valid after cursor' do
        let(:page_size) { account.tag_scores.size - 1 }
        let(:after_json) { JSON.parse(@response.body).with_indifferent_access }

        before do
          after_cursor = json[:meta][:after_cursor]
          # reset memoized tag scores
          @controller.remove_instance_variable(:@tag_scores)

          get :index, params: { page: {
            size: page_size,
            after: after_cursor
          } }
        end

        it 'presents the correct keys' do
          refute after_json[:meta][:has_more]
          assert after_json[:meta][:before_cursor].present?
          assert after_json[:meta][:after_cursor].present?
          assert after_json[:links][:prev].present?
          assert after_json[:links][:next].present?
        end

        it 'presents the correct number of tags' do
          assert_equal 1, after_json[:tags].size
        end
      end

      describe 'with an invalid after cursor' do
        let(:page_params) do
          {
            size: page_size,
            after: 'invalid'
          }
        end

        it 'presents an error message' do
          assert_equal 'InvalidPaginationParameter', json[:error]
          assert_equal 'page[after] is not valid', json[:description]
          assert_response :bad_request
        end
      end
    end
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    should_be_forbidden :index, :show, :create, :destroy, add_to_all: {ticket_id: 1}
    should_be_authorized { put :update, params: { ticket_id: 1 } }

    describe "a PUT to :update" do
      before { put :update, params: { ticket_id: 1 } }
      it('responds with success') { assert_response :success }
    end
  end

  def organization
    @organization = organizations(:minimum_organization1)
    @organization.account.settings.has_user_tags = true
    @organization.account.save!

    @organization.reload
    @organization.set_tags = %w[test]
    @organization.save!

    @params = {organization_id: @organization.id}
  end

  def ticket
    @ticket = tickets(:minimum_1)
    @ticket.set_tags = %w[test]
    @ticket.will_be_saved_by(@user)
    @ticket.save!

    @params = {ticket_id: @ticket.nice_id}
  end

  def user
    @user = users(:minimum_agent)
    @user.account.settings.has_user_tags = true
    @user.account.save!

    @user.reload
    @user.set_tags = %w[test]
    @user.save!

    @params = {user_id: @user.id}
  end
end
