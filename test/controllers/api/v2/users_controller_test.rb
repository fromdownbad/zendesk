require_relative '../../../support/test_helper'
require_relative '../../../support/api_scopes_helper'
require_relative '../../../support/users_controller_update_session_locale_test_helper'
require_relative '../../../support/mailer_test_helper'
require_relative '../../../support/multiproduct_test_helper'
require_relative '../../../support/suite_test_helper'
require_relative "../../../support/fake_esc_kafka_message"

SingleCov.covered! uncovered: 5

require "zendesk/testing/factories/global_uid"

describe Api::V2::UsersController do
  extend Api::V2::TestHelper
  extend MultiproductTestHelper
  extend ApiScopesHelper

  include CustomFieldsTestHelper
  include UsersControllerUpdateSessionLocaleTestHelper
  include MailerTestHelper
  include SuiteTestHelper
  include DomainEventsHelper

  # See https://github.com/zendesk/zendesk/pull/23410
  # Ideally we need to find the correct fixtures to load. :subscriptions and :credit_cards
  # were added but tests are still failing.
  fixtures :all
  # fixtures :subscriptions, :credit_cards, :accounts, :users, :groups, :memberships, :organizations, :role_settings, :translation_locales

  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  before do
    Zendesk::RedisStore.client.flushall
    ZendeskSearch::Client.any_instance.stubs(:search).returns("count" => 0, "results" => [])
    # don't use these fixtures
    CustomField::Field.without_arsi.delete_all
    CustomField::Value.without_arsi.delete_all
    CustomField::Dropdown.without_arsi.delete_all
    accept :json
    Api::V2::UsersController.any_instance.stubs(:throttle_create_or_update).returns(true)
    @default_current_brand = FactoryBot.create(:brand, subdomain: 'default')
    @controller.stubs(:current_brand).returns(@default_current_brand)
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
    Omnichannel::EntitlementSyncJob.stubs(:enqueue)
  end

  after do
    Api::V2::UsersController.any_instance.unstub(:throttle_create_or_update)
  end

  with_options(controller: "api/v2/users") do |request|
    # Failed in Rails 2 with weird routing issues - fixed by not having users (with the same methods)
    # nested under more than one resource in the routes.
    request.should_route :get,    "/api/v2/users.json",            action: "index", format: :json

    request.should_route :get,    "/api/v2/users",                 action: "index"
    request.should_route :get,    "/api/v2/users/count",           action: "count"
    request.should_route :get,    "/api/v2/users/search",          action: "search"
    request.should_route :get,    "/api/v2/users/1",               action: "show", id: "1"
    request.should_route :post,   "/api/v2/users",                 action: "create"
    request.should_route :put,    "/api/v2/users/1",               action: "update",  id: "1"
    request.should_route :delete, "/api/v2/users/1",               action: "destroy", id: "1"
    request.should_route :get,    "/api/v2/users/1/related",       action: 'related', id: "1"
    request.should_route :put,    "/api/v2/users/me/merge",        action: 'merge_self'
    request.should_route :put,    "/api/v2/users/1/merge",         action: 'merge', id: "1"
    request.should_route :get,    "/api/v2/organizations/1/users", action: "index",   organization_id: "1"
    request.should_route :get,    "/api/v2/groups/1/users",        action: "index",   group_id: "1"
    request.should_route :get,    "/api/v2/organizations/1/users/count", action: "count",   organization_id: "1"
    request.should_route :get,    "/api/v2/groups/1/users/count",        action: "count",   group_id: "1"
    request.should_route :post,   "/api/v2/users/autocomplete",    action: "autocomplete"
    request.should_route :get,    "/api/v2/users/autocomplete",    action: "autocomplete"
    request.should_route :post,   "/api/v2/users/show_many",       action: "show_many"
    request.should_route :get,    "/api/v2/users/show_many",       action: "show_many"
    request.should_route :put,    "/api/v2/users/update_many",     action: "update_many"
    request.should_route :delete, "/api/v2/users/destroy_many",    action: "destroy_many"
  end

  describe "when using oauth tokens" do
    with_scopes('users:read', 'read') do
      should_be_authorized { get :index }
      should_be_authorized { get :count }
      should_be_authorized { get :show, params: { id: account.users.first.id } }
      should_be_authorized { get :show_many, params: { ids: account.users.first.id } }
      should_be_authorized { get :related, params: { id: account.users.first.id } }
      should_be_authorized { get :autocomplete, params: { name: "Bob" } }

      # TODO: These tests fail as the response is a 403, 401 is expected
      # should_be_unauthorized [:post, :create, user: { name: "Rodney", email: "the.rod@example.org", role: "end-user" }]
    end

    with_scopes('users:write', 'write') do
      should_be_authorized { post :create, params: { user: { name: "Rodney", email: "the.rod@example.org", role: "end-user" } } }
      should_be_authorized { post :create_many, params: { users: [{ name: "Rodney", email: "the.rod@example.org", role: "end-user" }] } }
      should_be_authorized { put :merge, params: { id: users(:minimum_end_user).id, user: { id: users(:minimum_author).id } } }
      should_be_authorized { put :update, params: { id: account.users.last.id, user: { role: "admin"} } }
      should_be_authorized { put :update_many, params: { ids: account.users.last.id, user: { role: "admin"} } }
      should_be_authorized { delete :destroy, params: { id: account.users.last.id } }
      should_be_authorized { delete :destroy_many, params: { ids: account.users.last.id } }
      should_be_authorized { get :search, params: { query: "test" } }

      # TODO: These tests fail as the response is a 403, 401 is expected
      # should_be_unauthorized :index
    end

    with_scopes('read', 'write') do
      should_be_authorized { post :create_many, params: { users: [{ name: "Rodney", email: "the.rod@example.org", role: "end-user" }] } }
      should_be_authorized { put :merge, params: { id: users(:minimum_end_user).id, user: { id: users(:minimum_author).id } } }
      should_be_authorized { put :update_many, params: { ids: account.users.last.id, user: { role: "admin"} } }
      should_be_authorized { delete :destroy_many, params: { ids: account.users.last.id } }
      should_be_authorized { get :search, params: { query: "test" } }
      should_be_authorized { get :show_many, params: { ids: account.users.first.id } }
      should_be_authorized { get :related, params: { id: account.users.first.id } }
      should_be_authorized { get :autocomplete, params: { name: "Bob" } }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :search, :create, :update, :destroy, :count, [:get, :merge, id: 1]
  end

  as_an_end_user do
    should_be_forbidden :index, :search, :create, :update, :destroy, :update_many, :destroy_many, :count, [:get, :merge, id: 1]

    describe "a PUT to :merge_self" do
      describe "with a valid user" do
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).with(users(:minimum_author), users(:minimum_end_user)).returns(:merge_complete)
          put :merge_self, params: { user: { email: users(:minimum_author).email, password: '123456' } }
        end

        it('responds with ok') { assert_response :ok }
      end

      describe "with an invalid user/password combination" do
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).never
          put :merge_self, params: { user: { email: users(:minimum_author).email, password: '654321' } }
        end

        it('responds with unauthorized') { assert_response :unauthorized }
      end

      describe "with an email that isn't registered" do
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).never
          put :merge_self, params: { user: { email: 'wrong_email', password: '123456' } }
        end

        it('responds with unauthorized') { assert_response :unauthorized }
      end

      describe "with a missing user parameter" do
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).never
          put :merge_self
        end

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with a user parameter set to nil" do
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).never
          put :merge_self, params: { user: nil }
        end

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with an end-user w/ remote auth and external_id" do
        let(:user) { users(:minimum_end_user) }
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).returns(:loser_has_sso_external_id)
          put :merge_self, params: { user: { email: users(:minimum_author).email, password: '123456' } }
        end

        it('responds with bad_request and error message') do
          assert_response :unprocessable_entity
          expected_response = {"error" => "You cannot merge #{user.name} while they have an external ID."}
          assert_equal expected_response, JSON.parse(@response.body)
        end
      end
    end

    describe "if the user can't be merged" do
      before do
        Users::Merge.expects(:validate_and_merge_with_tickets_now).returns(:loser_invalid)
        put :merge_self, params: { user: { email: users(:minimum_author).email, password: '123456' } }
      end

      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
    end
  end

  describe "as a restricted agent" do
    when_logged_in_as :minimum_agent do
      before do
        Account.any_instance.stubs(:has_permission_sets?).returns(false)
        @user.stubs(:is_restricted_agent?).returns(true)
      end

      should_be_forbidden :index
      should_be_forbidden :count
      describe "a GET to :search" do
        before do
          get :search, params: { query: "test" }
        end

        should_use_presenter Api::V2::Users::AgentPresenter
      end

      describe "a GET to :show" do
        before { get :show, params: { id: users(:minimum_end_user).id } }
        it('responds with ok') { assert_response :ok }
      end

      describe "a GET to :show_many" do
        before { get :show_many, params: { ids: users(:minimum_end_user).id.to_s } }
        it('responds with ok') { assert_response :ok }
      end
    end
  end

  as_an_agent do
    should_be_forbidden :destroy_many

    describe "a GET to :index" do
      describe 'using Api::V2::Users::AgentPresenter' do
        before do
          get :index
        end

        should_use_presenter Api::V2::Users::AgentPresenter
      end

      it 'should not generate n+1 queries' do
        FactoryBot.create(:voice_account, account: @account)
        Account.any_instance.stubs(:voice_feature_enabled?).with(:user_seats).returns(true)
        query_count = RAILS4 ? 30 : 31
        assert_sql_queries(query_count) do
          get :index, params: { include: 'abilities' }
        end
      end

      describe "Emergency rate limiting for GET users" do
        describe "with an integer page param" do
          let(:limiter) { mock }
          let(:statsd_client) { mock }

          before { limiter.stubs(:throttle_index_with_deep_offset_pagination) }

          it "calls the pagination limiter" do
            Zendesk::OffsetPaginationLimiter.expects(:new).with(anything).returns(limiter)
            limiter.expects(:throttle_index_with_deep_offset_pagination).once

            get :index, params: { page: 1 }
          end

          describe_with_arturo_disabled :bolt_throttle_index_with_deep_pagination_users do
            describe "after 99 calls within 1 second" do
              let(:rate_limit) { 100 }

              before do
                Timecop.freeze
                Prop.throttle!(:index_users_with_deep_offset_pagination_ceiling_limit, @account.id, increment: rate_limit - 1)
              end

              describe "for page=1 and per_page=100" do
                let(:page) { 1 }

                it "responds with success on calls 100 and 101" do
                  get :index, params: { page: page }
                  assert_response :success
                  get :index, params: { page: page }
                  assert_response :success
                end

                describe "metrics and logging" do
                  it "does not log any rate limit message" do
                    Rails.logger.expects(:info).with(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/)).never
                    # Once the above expectation is set up, you must specify that other calls might occur.
                    Rails.logger.expects(:info).with(Not(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/))).at_least(0)
                    2.times { get :index, params: { page: page } }
                  end

                  it "does not increment the 'users' ceiling rate limit metric" do
                    Zendesk::StatsD::Client.expects(:new).with(namespace: "users").at_least(0).returns(statsd_client)
                    statsd_client.expects(:increment).with("api_v2_users_index_ceiling_rate_limit_log_only", anything).never
                    statsd_client.expects(:increment).with("api_v2_users_index_ceiling_rate_limit", anything).never
                    2.times { get :index, params: { page: page } }
                  end
                end
              end

              describe "for page=10,001 and per_page=100" do
                let(:page) { 10_001 }

                it "responds with success on calls 100 and 101" do
                  get :index, params: { page: page }
                  assert_response :success
                  get :index, params: { page: page }
                  assert_response :success
                end

                describe "metrics and logging" do
                  it "logs the log_only message" do
                    Rails.logger.expects(:info).with(regexp_matches(/Log only: would have rate limited #index by account/)).once
                    Rails.logger.expects(:info).with(Not(regexp_matches(/Log only: would have rate limited #index by account/))).at_least(0)
                    2.times { get :index, params: { page: page } }
                  end

                  it "does not log the message for the active arturo" do
                    Rails.logger.expects(:info).with(regexp_matches(/Rate limiting #index by account/)).never
                    Rails.logger.expects(:info).with(Not(regexp_matches(/Rate limiting #index by account/))).at_least(0)
                    2.times { get :index, params: { page: page } }
                  end

                  it "does not increment the api_v2_users_index_ceiling_rate_limit metric" do
                    Zendesk::StatsD::Client.expects(:new).with(namespace: "users").once.returns(statsd_client)
                    statsd_client.expects(:increment).with("api_v2_users_index_ceiling_rate_limit", anything).never
                    statsd_client.expects(:increment).with(Not(equals("api_v2_users_index_ceiling_rate_limit")), anything).at_least(0)
                    2.times { get :index, params: { page: page } }
                  end

                  it "increments the log_only metric" do
                    Zendesk::StatsD::Client.expects(:new).with(namespace: "users").once.returns(statsd_client)
                    statsd_client.expects(:increment).with("api_v2_users_index_ceiling_rate_limit_log_only", anything).once
                    2.times { get :index, params: { page: page } }
                  end
                end
              end
            end
          end

          describe_with_arturo_enabled :bolt_throttle_index_with_deep_pagination_users do
            describe "after 99 calls within 1 second" do
              let(:rate_limit) { 100 }

              before do
                Timecop.freeze
                Prop.throttle!(:index_users_with_deep_offset_pagination_ceiling_limit, @account.id, increment: rate_limit - 1)
              end

              describe "for page=1 and per_page=100" do
                let(:page) { 1 }

                it "responds with success on calls 100 and 101" do
                  get :index, params: { page: page }
                  assert_response :success
                  get :index, params: { page: page }
                  assert_response :success
                end

                describe "metrics and logging" do
                  it "does not log any rate limit message" do
                    Rails.logger.expects(:info).with(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/)).never
                    Rails.logger.expects(:info).with(Not(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/))).at_least(0)
                    2.times { get :index, params: { page: page } }
                  end

                  it "does not increment the 'users' ceiling rate limit metric" do
                    Zendesk::StatsD::Client.expects(:new).with(namespace: "users").at_least(0).returns(statsd_client)
                    statsd_client.expects(:increment).with("api_v2_users_index_ceiling_rate_limit_log_only", anything).never
                    statsd_client.expects(:increment).with("api_v2_users_index_ceiling_rate_limit", anything).never
                    2.times { get :index, params: { page: page } }
                  end
                end
              end

              describe "for page=10,001 and per_page=100" do
                let(:page) { 10_001 }

                it "responds with success on call 100 and 429 on call 101" do
                  get :index, params: { page: page }
                  assert_response :success
                  get :index, params: { page: page }
                  assert_response 429
                end

                describe "metrics and logging" do
                  it "logs the controller and pagination info" do
                    Rails.logger.expects(:info).with(
                      "[UsersController] Rate limiting #index by account: minimum, page: 10001, per_page: 100, throttle key: index_users_with_deep_offset_pagination_ceiling_limit"
                    ).once
                    Rails.logger.expects(:info).with(Not(regexp_matches(/Rate limiting #index by account/))).at_least(0)
                    2.times { get :index, params: { page: page } }
                  end

                  it "does not log the log_only message" do
                    Rails.logger.expects(:info).with(regexp_matches(/Log only: would have rate limited #index by account/)).never
                    Rails.logger.expects(:info).with(Not(regexp_matches(/Log only: would have rate limited #index by account/))).at_least(0)
                    2.times { get :index, params: { page: page } }
                  end

                  it "increments the 'users' api_v2_users_index_ceiling_rate_limit metric" do
                    Zendesk::StatsD::Client.expects(:new).with(namespace: "users").once.returns(statsd_client)
                    statsd_client.expects(:increment).with("api_v2_users_index_ceiling_rate_limit", anything).once
                    2.times { get :index, params: { page: page } }
                  end

                  it "does not increment the log_only metric" do
                    Zendesk::StatsD::Client.expects(:new).with(namespace: "users").once.returns(statsd_client)
                    statsd_client.expects(:increment).with("api_v2_users_index_ceiling_rate_limit_log_only", anything).never
                    statsd_client.expects(:increment).with(Not(equals("api_v2_users_index_ceiling_rate_limit_log_only")), anything).at_least(0)
                    2.times { get :index, params: { page: page } }
                  end
                end
              end
            end
          end
        end
      end
    end

    describe "a POST to :request_create with user" do
      it "sends the deliver_update_subscription_request email to the account owner when passed user details" do
        AccountsMailer.expects(:deliver_update_subscription_request)
        post :request_create, params: { user: {name: 'taylor', email: 'taylor@tswift.com', role: 'agent'} }
        assert_response :ok
      end

      it "sends the deliver_low_seat_count_notification email to the account owner when passed the low_seat_count_notification boolean" do
        AccountsMailer.expects(:deliver_low_seat_count_notification)
        post :request_create, params: { low_seat_count_notification: true }
        assert_response :ok
      end
    end

    describe "a POST to :autocomplete with more than 64 search words" do
      it "returns a bad request error" do
        post :autocomplete, params: { name: "Word " * 65 }

        assert_response :bad_request
        expected_response = {'error' => 'Query Error', 'description' => "Number of search words exceeds the limit of 64"}
        assert_equal expected_response, JSON.parse(@response.body)
      end
    end

    describe "a POST to :autocomplete with less than 65 search words" do
      it "returns with success" do
        post :autocomplete, params: { name: "Word " * 64 }

        assert_response 200
        expected_response = {"users" => [], "next_page" => nil, "previous_page" => nil, "count" => 0}
        assert_equal expected_response, JSON.parse(@response.body)
      end
    end

    describe "a POST to :autocomplete with blank name param" do
      it "returns with success" do
        post :autocomplete, params: { name: nil }

        assert_response 200
        expected_response = {"users" => [], "next_page" => nil, "previous_page" => nil, "count" => 0}
        assert_equal expected_response, JSON.parse(@response.body)
      end
    end

    describe "a POST to :autocomplete when search is unavailable" do
      before do
        ZendeskSearch::Client.any_instance.expects(:search).with("Bob", is_a(Hash)).
          raises(Faraday::Error::ClientError, "Connection timed out")
      end

      it "returns an error" do
        post :autocomplete, params: { name: "Bob" }

        assert_response :internal_server_error
        expected_response = {'error' => 'Unavailable', 'description' => "Connection timed out"}
        assert_equal expected_response, JSON.parse(@response.body)
      end
    end

    describe "autocomplete with name" do
      before do
        users = accounts(:minimum).users.first(2)
        search_response = {"count" => 2, "results" => users.map { |u| {"id" => u.id, "type" => "user"} }}

        ZendeskSearch::Client.any_instance.expects(:search).with("Bob", is_a(Hash)).returns(search_response)
        Api::V2::Users::AgentPresenter.any_instance.expects(:present).with(users)
      end

      describe "a POST to :autocomplete with name" do
        it "should call Zendesk::Client.search" do
          post :autocomplete, params: { name: "Bob" }
        end
      end

      describe "a GET to :autocomplete with name" do
        it "should call Zendesk::Client.search" do
          get :autocomplete, params: { name: "Bob" }
        end

        describe_with_arturo_disabled :search_cache_control_header do
          it "does not set cache-control headers" do
            get :autocomplete, params: { name: "Bob" }
            assert_nil @response.headers['Cache-Control']
          end
        end

        describe_with_arturo_enabled :search_cache_control_header do
          it "sets cache-control max-age 5 headers for Lotus requests" do
            @controller.stubs(:lotus_internal_request?).returns(true)
            get :autocomplete, params: { name: "Bob" }
            assert_equal @response.headers['Cache-Control'], 'max-age=5, private'
          end

          it "sets cache-control max-age 10 headers for non-Lotus requests" do
            @controller.stubs(:lotus_internal_request?).returns(false)
            get :autocomplete, params: { name: "Bob" }
            assert_equal @response.headers['Cache-Control'], 'max-age=10, private'
          end
        end
      end
    end

    describe 'with filter' do
      let(:account) { accounts(:minimum) }
      let(:filter) { 'Admin' }

      before do
        users = account.users.first(2)
        search_response = {"count" => 2, "results" => users.map { |u| {"id" => u.id, "type" => "user"} }}
        ZendeskSearch::Client.any_instance.stubs(:search).returns(search_response)
      end

      describe_with_arturo_disabled :ocp_users_autocomplete_query do
        it "should call Zendesk::Client.search" do
          get :autocomplete, params: { name: "Bob", filter: 'assignable' }
          assert_equal 2, JSON.parse(response.body)["users"].count
        end
      end

      describe_with_arturo_enabled :ocp_users_autocomplete_query do
        it "should not call Zendesk::Client.search for assignable filter" do
          ZendeskSearch::Client.any_instance.expects(:search).never
          get :autocomplete, params: { name: filter, filter: 'assignable' }
          JSON.parse(response.body)['users'].each do |user|
            assert user['name'].include? filter
          end
        end

        it "should not call Zendesk::Client.search for requester filter" do
          ZendeskSearch::Client.any_instance.expects(:search).never
          get :autocomplete, params: { name: filter, filter: 'requester' }
          JSON.parse(response.body)['users'].each do |user|
            assert user['name'].include? filter
          end
        end

        it "should return 400 for invalid filter" do
          get :autocomplete, params: { name: "Bob", filter: 'lol' }
          assert_response :bad_request
        end
      end
    end

    describe "a POST to :autocomplete with highlights" do
      before do
        users = accounts(:minimum).users.first(1)
        search_response = {"count" => 1, "results" =>
          [{"id" => users[0].id, "type" => "user", "_highlights" => {"name" => "<em>Bob</em>by McGee"}}]}
        @highlights = { "users" => [{ "id" => users[0].id, "name" => "<em>Bob</em>by McGee" }]}

        ZendeskSearch::Client.any_instance.expects(:search).with("Bob", is_a(Hash)).returns(search_response)
      end

      it "should call Zendesk::Client.autocomplete_users 123" do
        post :autocomplete, params: { name: "Bob", include: "highlights" }
        response = JSON.parse(@response.body)
        assert_equal @highlights, response["highlights"]
      end
    end

    describe 'with valid external domain' do
      it 'returns a valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
        get :autocomplete, params: { name: 'Bob' }
        assert_equal response.headers['Access-Control-Allow-Origin'], 'http://app.futuresimple.com'
      end

      it 'returns a not valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
        get :autocomplete, params: { name: 'Bob' }
        assert_nil response.headers['Access-Control-Allow-Origin']
      end
    end

    describe "autocomplete with number" do
      before do
        users = accounts(:minimum).users.first(2)
        search_response = {"count" => 2, "results" => users.map { |u| {"id" => u.id, "type" => "user"} }}

        ZendeskSearch::Client.any_instance.expects(:search).with("555-43987",
          has_entry(endpoint: "users/autocomplete")).returns(search_response)
        Api::V2::Users::AgentPresenter.any_instance.expects(:present).with(users)
      end

      describe "a POST to :autocomplete with number" do
        it "should call Zendesk::Client.search" do
          post :autocomplete, params: { phone: '555-43987' }
        end
      end

      describe "a GET to :autocomplete with number" do
        it "should call Zendesk::Client.search" do
          get :autocomplete, params: { phone: '555-43987' }
        end
      end
    end

    describe "a GET to :index should respect max_per_page limit" do
      before do
        @controller.class_eval <<-RUBY
          def max_per_page; 1; end
        RUBY

        get :index, params: { per_page: 120 }
      end

      it "only returns 1 user" do
        assert_equal 1, JSON.parse(@response.body)["users"].size
      end
    end

    describe "users :count " do
      it 'count should come back' do
        get :count
        assert_response :ok
        users_json_keys = JSON.parse(response.body).keys
        assert_equal 1, (users_json_keys & ['count']).size
      end
    end

    describe "users :index pagination" do
      it 'should use offset pagination when no pagination params are present' do
        get :index
        assert_response :ok
        users_json_keys = JSON.parse(response.body).keys
        assert_equal 2, (users_json_keys & ['next_page', 'previous_page']).size
      end

      describe "with cursor pagination" do
        should_support_cursor_pagination :index
        should_support_cursor_pagination_conversion :index, 'users', :cursor_pagination_default_users, :remove_offset_pagination_users, cursor_pagination_version: :v1

        describe 'default sort parameter of id for v2' do
          it 'generates the correct order clause' do
            assert_sql_queries(1, /ORDER BY `users`.`id` DESC/) do
              get :index, params: { page: { size: 100 }, sort: '-id' }
            end
          end

          it 'shows an error message for invalid sort options' do
            get :index, params: { page: { size: 100 }, sort: '-invalid' }
            assert_response :bad_request
            assert @response.body =~ /sort is not valid/
          end
        end

        describe 'should always use OBP when \'query\' param is present no matter what the other params are' do
          it 'should use cursor pagination v2' do
            get :index, params: { query: 'test', page: { size: 100 } }
            assert_response :ok
            users_json_keys = JSON.parse(response.body).keys
            assert_equal 2, (users_json_keys & ['next_page', 'previous_page']).size
          end
        end

        describe_with_arturo_enabled :cursor_pagination_remove_v1_users do
          it 'should use cursor pagination v2' do
            get :index, params: { page: { size: 100 } }
            assert_response :ok
            json_response = JSON.parse(response.body)
            assert json_response['users'].size >= 1
            assert_not_nil json_response['links']['prev']
            assert_not_nil json_response['links']['next']
          end

          it 'should return an error when cursor pagination v1 params are given' do
            get :index, params: { limit: 100 }
            assert_response :ok
            json_response = JSON.parse(response.body)
            assert json_response['users'].size >= 1
            assert_not_nil json_response['links']['prev']
            assert_not_nil json_response['links']['next']
          end
        end

        describe "narrow results with group id" do
          it 'should use cursor pagination v2' do
            get :index, params: { page: { size: 100 }, group_id: @user.groups.first.id }
            assert_response :ok
            json_response = JSON.parse(response.body)
            assert json_response['users'].size >= 1
            assert_not_nil json_response['links']['prev']
            assert_not_nil json_response['links']['next']
          end
        end

        describe "narrow results with organization id" do
          before do
            @user.organization = organizations(:minimum_organization2)
            @user.save!
          end

          it 'should use cursor pagination v2' do
            get :index, params: { page: { size: 100 }, organization_id: @user.organization.id }
            assert_response :ok
            json_response = JSON.parse(response.body)
            assert json_response['users'].size >= 1
            assert_not_nil json_response['links']['prev']
            assert_not_nil json_response['links']['next']
          end
        end
      end

      describe "offset pagination count" do
        it 'does not use any index to count users' do
          assert_sql_queries(1, Regexp.new(Regexp.escape('SELECT COUNT(*) FROM `users` WHERE'))) do
            get :index
          end
        end

        it 'works when doing an email search query on index' do
          get :index, params: { query: 'abcd@example.com' }
          assert_response :ok
        end
      end

      describe_with_arturo_enabled :cursor_pagination_default_users do
        it 'should use cursor pagination v2 when the page parameter is present and is an Hash' do
          get :index, params: { page: { size: 100 } }
          assert_response :ok
          json_response = JSON.parse(response.body)
          assert json_response['users'].size >= 1
          assert_not_nil json_response['links']['prev']
          assert_not_nil json_response['links']['next']
        end

        describe_with_arturo_enabled :cursor_pagination_product_limit_users do
          it 'should set the pagination limit to 100 if the limit param is not present' do
            get :index
            assert_response :ok
            users_json = JSON.parse(response.body)
            assert_match 'limit=100&', users_json['after_url']
          end

          it 'should set the pagination limit to 100 if the limit sent is over the max limit of 100' do
            get :index, params: { limit: 321 }
            assert_response :ok
            users_json = JSON.parse(response.body)
            assert_match 'limit=100&', users_json['after_url']
          end
        end
      end
    end

    describe "a GET to :show" do
      describe "for a user with multiple organizations" do
        before do
          Account.any_instance.stubs(:has_multiple_organizations_enabled?).returns(true)

          @user = users(:minimum_end_user)
          @user.organizations << organizations(:minimum_organization2)

          @user.save!
        end

        describe "with the 'api_add_organization_ids_to_users' Arturo enabled" do
          before do
            Account.any_instance.stubs(:has_api_add_organization_ids_to_users?).returns(true)
            get :show, params: { id: @user.id }
          end

          it "returns the organization_ids as an array" do
            response = JSON.parse(@response.body)

            ids = response['user']['organization_ids'].sort
            assert_equal @user.organization_ids.sort, ids
          end
        end

        describe "with the 'api_add_organization_ids_to_users' Arturo disabled" do
          before do
            Account.any_instance.stubs(:has_api_add_organization_ids_to_users?).returns(false)
            get :show, params: { id: @user.id }
          end

          it "won't return the organization_ids" do
            response = JSON.parse(@response.body)
            assert_nil response['user']['organization_ids']
          end
        end
      end

      describe "with user_show_sideloads_cache_control_header on" do
        before do
          Arturo.enable_feature!(:user_show_sideloads_cache_control_header)
        end

        it 'sets cache-control to 5 seconds' do
          get :show, params: { id: users(:minimum_end_user).id, include: 'abilities' }

          assert_equal response.headers['Cache-Control'], 'max-age=5, private'
        end
      end
    end

    describe "a GET to :show_many" do
      describe "with ids" do
        before do
          @uids = accounts(:minimum).users.map(&:id).sort
        end

        it "gives me back the users I asked for" do
          get :show_many, params: { ids: @uids.join(",") }
          json = JSON.parse(@response.body)
          assert_equal @uids, json['users'].map { |u| u["id"] }.sort
        end
      end

      describe "with external_ids" do
        before do
          account = accounts(:minimum)
          @external_ids = ['external_1', 'external_2', 'external_3']
          @external_ids.each do |external_id|
            FactoryBot.create(:user, account: account, external_id: external_id)
          end
          get :show_many, params: { external_ids: @external_ids.join(",") }
        end

        it "gives me back the users I asked for" do
          json = JSON.parse(@response.body)
          assert_equal @external_ids, json['users'].map { |u| u["external_id"] }.sort
        end
      end

      describe "with ids including a deleted user" do
        let(:ids) { [users(:minimum_end_user).id, users(:inactive_user).id] }

        describe "with the Arturo enabled" do
          before do
            Arturo.enable_feature! :api_enable_show_many_deleted_users
            get :show_many, params: { ids: ids.join(",") }
          end

          it "gives me back all the users I asked for" do
            json = JSON.parse(@response.body)
            assert_equal ids.sort, json['users'].map { |u| u['id'] }.sort
          end
        end

        describe "with the Arturo disabled" do
          before do
            Arturo.disable_feature! :api_enable_show_many_deleted_users
            get :show_many, params: { ids: ids.join(",") }
          end

          it "gives me back just the active user" do
            json = JSON.parse(@response.body)
            assert_equal users(:minimum_end_user).id, json['users'][0]['id']
          end
        end
      end

      describe 'with valid external domain' do
        before do
          @uids = accounts(:minimum).users.map(&:id).sort
        end

        it 'returns a valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com'
          get :show_many, params: { ids: @uids.join(",") }
          assert_equal response.headers['Access-Control-Allow-Origin'], 'http://app.futuresimple.com'
        end

        it 'returns a not valid allow-origin header with an allowed external domain as origin' do
          @controller.request.env['HTTP_ORIGIN'] = 'http://app.futuresimple.com.wearehackers.com'
          get :show_many, params: { ids: @uids.join(",") }
          assert_nil response.headers['Access-Control-Allow-Origin']
        end
      end
    end

    describe "a GET to :search" do
      describe "AgentPresenter" do
        before { get :search, params: { query: "hello" } }
        should_use_presenter Api::V2::Users::AgentPresenter
      end

      it "searches for users using the finder" do
        Zendesk::Users::Finder.any_instance.expects(:find_by_database_scan).returns(@account.users.all)
        get :search, params: { query: "hello" }
      end

      it "instantiate a finder with the correct query" do
        finder = stub
        finder.expects(:users).returns([])
        Zendesk::Users::Finder.expects(:new).with(@account, @user, {'query' => 'hello'}, 1, 100).returns(finder)
        get :search, params: { query: "hello" }
      end

      describe_with_arturo_disabled :search_cache_control_header do
        it "does not set cache-control headers" do
          Zendesk::Users::Finder.any_instance.expects(:find_by_database_scan).returns(@account.users.all)
          get :search, params: { query: "hello" }
          assert_nil @response.headers['Cache-Control']
        end
      end

      describe_with_arturo_enabled :search_cache_control_header do
        it "sets cache-control max-age 5 headers for Lotus requests" do
          Zendesk::Users::Finder.any_instance.expects(:find_by_database_scan).returns(@account.users.all)
          @controller.stubs(:lotus_internal_request?).returns(true)
          get :search, params: { query: "hello" }
          assert_equal @response.headers['Cache-Control'], 'max-age=5, private'
        end

        it "sets cache-control max-age 10 headers for non-Lotus requests" do
          Zendesk::Users::Finder.any_instance.expects(:find_by_database_scan).returns(@account.users.all)
          @controller.stubs(:lotus_internal_request?).returns(false)
          get :search, params: { query: "hello" }
          assert_equal @response.headers['Cache-Control'], 'max-age=10, private'
        end
      end
    end

    describe "a GET to :search with error in the query" do
      before do
        ZendeskSearch::Client.any_instance.expects(:search).with("type:agent", is_a(Hash)).
          raises(ZendeskSearch::QueryError, "Invalid type:agent")
        get :search, params: { query: "type:agent" }
      end

      it ('responds with bad_request') { assert_response :bad_request }
    end

    describe "a GET to :show" do
      describe "an active user" do
        before { get :show, params: { id: users(:minimum_end_user).id } }
        should_use_presenter Api::V2::Users::AgentPresenter
      end

      describe "an inactive user" do
        before { get :show, params: { id: users(:inactive_user).id } }
        should_use_presenter Api::V2::Users::AgentPresenter
      end
    end

    describe 'a GET to :related' do
      before { get :related, params: { id: users(:minimum_end_user).id } }
      should_use_presenter Api::V2::UserRelatedPresenter
    end

    describe "a POST to :create" do
      describe "with invalid params" do
        before { post :create }

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

        it "does not create the user" do
          assert @controller.send(:new_user).new_record?
        end

        it "does not emit user_created domain events" do
          assert_empty decode_user_events(domain_event_publisher.events, :user_created)
        end
      end

      describe "when an invalid organization_id is provided" do
        before do
          post :create, params: { user: { name: "Rodney", email: "the.rod@example.org", role: "end-user", organization_id: 12345 } }
        end

        it "responds with a 422" do
          assert_response :unprocessable_entity
        end

        it "response includes erroneous org id" do
          assert_match '12345', response.body
        end

        it "does not emit user_created domain events" do
          assert_equal decode_user_events(domain_event_publisher.events, :user_created).size, 0
        end
      end

      describe "an end-user" do
        before { post :create, params: { user: { name: "Rodney", email: "the.rod@example.org", role: "end-user" } } }

        should_use_presenter Api::V2::Users::AgentPresenter, status: :created, location: /\/api\/v2\/users\/\d+\.json$/

        it "creates the user" do
          refute @controller.send(:new_user).new_record?
        end

        it "emits user_created domain events" do
          assert_equal decode_user_events(domain_event_publisher.events, :user_created).size, 1
        end
      end

      describe "an end-user that belongs to multiple organizations" do
        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
          @organization_ids = [organizations(:minimum_organization1).id, organizations(:minimum_organization2).id]
          post :create, params: { user: { name: "Argo", email: "the.argo@example.org", role: "end-user", organization_ids: @organization_ids } }
        end

        it("responds with created") { assert_response :created }

        it "emits user_organization_added domain events" do
          organization_added_events = decode_user_events(domain_event_publisher.events, :user_organization_added)
          organization_added_events.size.must_equal 2
          assert_includes @organization_ids, organization_added_events.first.user_organization_added.organization.id.value
          assert_includes @organization_ids, organization_added_events.second.user_organization_added.organization.id.value
        end
      end

      describe "an end-user with multiple identities specified" do
        # in web portal disabled state this is not happening
        it "does not send a single verification email" do
          UsersMailer.expects(:deliver_verify).never
          post :create, params: { user: { name: "Rodney", email: "the.rod@example.org", role: "end-user", identities: [{ type: "email", value: "the.rod@example.org" }] } }
        end

        it "emits user_created domain events" do
          assert_equal decode_user_events(domain_event_publisher.events, :user_created).size, 0
        end
      end

      describe "an agent" do
        before { post :create, params: { user: { name: "Rodney", email: "the.rod@example.org", role: "agent" } } }
        it('responds with forbidden') { assert_response :forbidden }

        it "does not emit user_created domain events" do
          assert_equal decode_user_events(domain_event_publisher.events, :user_created).size, 0
        end
      end

      describe "an admin" do
        before { post :create, params: { user: { name: "Rodney", email: "the.rod@example.org", role: "admin" } } }
        it('responds with forbidden') { assert_response :forbidden }

        it "does not emit user_created domain events" do
          assert_equal decode_user_events(domain_event_publisher.events, :user_created).size, 0
        end
      end
    end

    describe "a PUT to :update" do
      describe "an end-user" do
        before { put :update, params: { id: users(:minimum_end_user).id, user: { name: "some name" } } }

        should_use_presenter Api::V2::Users::AgentPresenter

        it "updates the user" do
          assert_equal "some name", users(:minimum_end_user).reload.name
        end

        it "emits an user_name_changed domain event" do
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_name_changed).size
        end
      end

      describe "a user that does not have any identity with verified: true" do
        before do
          post :create, params: { user: { name: "user without identity" } }
          @user_id = JSON.parse(@response.body)['user']['id']
        end

        it "response should return verified:false" do
          put :update, params: { id: @user_id, user: { verified: true } }
          refute JSON.parse(@response.body)['user']['verified']
          get :show, params: { id: @user_id }
          refute JSON.parse(@response.body)['user']['verified']
        end
      end

      describe "an end-user that belongs to multiple organizations" do
        let(:organization_ids) { [organizations(:minimum_organization3).id, organizations(:minimum_organization2).id] }
        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
          post :update, params: { id: users(:minimum_end_user).id, user: { name: "Argo", email: "the.argo1@example.org", role: "end-user", organization_ids: organization_ids } }
        end

        it("responds with ok") { assert_response :ok }

        it "emits user_organization_added and user_organization_removed domain events" do
          organization_added_events = decode_user_events(domain_event_publisher.events, :user_organization_added)
          organization_added_events.size.must_equal 2
          assert_includes organization_ids, organization_added_events.first.user_organization_added.organization.id.value
          assert_includes organization_ids, organization_added_events.second.user_organization_added.organization.id.value
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_organization_removed).size
        end
      end

      describe "a user's photo" do
        it "enqueues a job" do
          user = users(:minimum_end_user)
          FetchProfileImageJob.expects(:enqueue).with(user.account_id, user.id, "http://img.test.com/img.jpg")
          put :update, params: { id: users(:minimum_end_user).id, user: { remote_photo_url: "http://img.test.com/img.jpg" } }
        end
      end

      describe "on an enduser with a pre-existing unverified email" do
        before do
          @user = users(:minimum_end_user)
          @email = user_identities(:minimum_end_user)
          @email.update_attribute(:is_verified, false)
        end

        it "should not attempt to send another email when receiving a non-normalized email" do
          UsersMailer.expects(:deliver_verify).never
          put :update, params: { id: @user.id, user: { name: "Title Cased", email: "Minimum_End_User@aghassipour.com" } }
        end

        it "emits an user_name_changed domain event" do
          put :update, params: { id: @user.id, user: { name: "Title Cased", email: "Minimum_End_User@aghassipour.com" } }
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_name_changed).size
        end
      end

      describe "with another user's email" do
        it "should not attempt to send email for the invalid user" do
          UsersMailer.expects(:deliver_verify).never
          put :update, params: { id: users(:minimum_end_user).id, user: { email: user_identities(:minimum_search_user).value } }
          assert_response :unprocessable_entity
        end
      end

      describe "with a default group id" do
        before do
          @account = accounts(:with_groups)
          @controller.stubs(:current_account).returns(@account)
          @user = users(:with_groups_agent2)
          @controller.stubs(:current_user).returns(@user)

          @new_group = groups(:with_groups_group2)
          @current_default_group_id = @user.default_group_id
          put :update, params: { id: @user.id, user: { default_group_id: @new_group.id } }
        end

        it "changes the group" do
          @user.reload
          assert_equal @new_group.id, @user.default_group_id
        end
      end

      describe "updating a user from an agent that doesn't support JSON XHR uploads" do
        before do
          accept 'text/plain'
          @request.user_agent = 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)'
          FetchProfileImageJob.stubs(:enqueue)
          put :update, params: { id: users(:minimum_end_user).id, user: { remote_photo_url: "http://img.test.com/img.jpg" } }
        end

        it('responds with content_type text') { assert_equal Mime::Type.lookup_by_extension('text'), @controller.response.content_type.to_s }
      end

      describe "updating a user with a */* Accepts header" do
        before do
          @request.accept = '*/*'
          FetchProfileImageJob.stubs(:enqueue)
          put :update, params: { id: users(:minimum_end_user).id, user: { remote_photo_url: "http://img.test.com/img.jpg" } }
        end

        it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
      end

      describe "an admin" do
        before { put :update, params: { id: users(:minimum_admin).id, user: { name: "some name" } } }

        it('responds with forbidden') { assert_response :forbidden }

        it "does not emit user_name_changed domain event" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_name_changed).size
        end
      end

      describe "with invalid params" do
        before { put :update, params: { id: users(:minimum_end_user).id, user: { name: "" } } }

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

        it "does not update the user" do
          assert_not_equal "some name", users(:minimum_end_user).reload.name
        end

        it "does not emit user_name_changed domain event" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_name_changed).size
        end
      end

      describe "with invalid params" do
        describe "invalid name" do
          before { put :update, params: { id: users(:minimum_end_user).id, user: { name: "" } } }

          should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity

          it "does not update the user" do
            assert_not_equal "some name", users(:minimum_end_user).reload.name
          end

          it "does not emit user_name_changed domain event" do
            assert_equal 0, decode_user_events(domain_event_publisher.events, :user_name_changed).size
          end
        end

        describe "invalid custom role id" do
          before { put :update, params: { id: users(:minimum_end_user).id, user: { custom_role_id: 0 } } }

          it('responds with bad_request') do
            assert_response :bad_request
            expected_response = {"error" => "Invalid custom role id", "description" => "Custom role id does not exist"}
            assert_equal expected_response, JSON.parse(@response.body)
          end

          it "does not emit user_merged domain events" do
            assert_equal 0, decode_user_events(domain_event_publisher.events, :user_merged).size
          end
        end
      end

      describe "when adding or editing tags" do
        before do
          @account = users(:minimum_end_user).account
          @account.settings.has_user_tags = true
          @account.save!
          @user.account.settings.reload
        end

        it "updates end users tags" do
          put :update, params: { id: users(:minimum_end_user).id, user: { name: "some name", tags: "moose, goose"} }
          assert_equal ["goose", "moose"], users(:minimum_end_user).reload.tags.sort
        end

        it "updates user's updated_at" do
          user = users(:minimum_end_user)
          # doing this to clear out any other unsaved changes that need to happen
          Timecop.travel(33.minutes.ago) { user.save! }
          original_updated_at = user.reload.updated_at

          put :update, params: { id: user.id, user: { tags: ["fooboo"] } }
          assert_equal ["fooboo"], user.reload.tags
          assert_not_equal original_updated_at, user.updated_at
        end

        it "does not update own tags" do
          @user.current_tags = "goose"
          @user.save!

          put :update, params: { id: @user.id, user: { name: "some name", tags: "moose"} }
          assert_equal ["goose"], @user.reload.tags
        end

        it "does not update other agents tags" do
          agent = users(:with_groups_agent1)
          agent.current_tags = "goose"
          agent.save!

          put :update, params: { id: @user.id, user: { name: "some name", tags: "moose"} }
          assert_equal ["goose"], agent.reload.tags
        end
      end

      describe "when an invalid organization_id is provided" do
        before { put :update, params: { id: users(:minimum_end_user).id, user: { organization_id: 12345 } } }

        it "responds with a 422" do
          assert_response :unprocessable_entity
        end

        it "response includes erroneous org id" do
          assert_match '12345', response.body
        end
      end

      describe "a user's language" do
        let(:user) { users(:minimum_agent) }
        let(:user_params) { {} }
        let(:spanish) { translation_locales(:spanish) }
        let(:japanese) { translation_locales(:japanese) }

        before do
          user.update_attributes(locale_id: ENGLISH_BY_ZENDESK.id)
          put :update, params: { id: user.id, user: user_params }
        end

        describe "by passing in `locale_id`" do
          let(:user_params) { { locale_id: spanish.id } }

          it "correctly updates the locale" do
            assert_equal spanish, user.reload.translation_locale
          end
        end

        describe "by passing in `locale`" do
          let(:user_params) { { locale: spanish.locale } }

          it "correctly updates the locale" do
            assert_equal spanish, user.reload.translation_locale
          end
        end

        describe "by passing in both `locale_id` and `locale`" do
          let(:user_params) { { locale_id: japanese.id, locale: spanish.locale } }

          it "correctly updates the locale" do
            assert_equal spanish, user.reload.translation_locale
          end
        end
      end
    end

    describe "rate limit on updating a unique_user" do
      describe_with_arturo_enabled :rate_limit_create_or_update_on_unique_user do
        let(:rate_limit) { 5 }
        let(:current_time) { Time.utc(2020, 10, 21, 0, 0, 0) }

        before do
          Timecop.freeze(current_time) do
            rate_limit.times do
              put :update, params: { id: users(:minimum_end_user).id, user: { name: "some name" } }
              assert_response :success
            end
          end
        end

        it "rate limits updates on unique user" do
          Timecop.freeze(current_time) do
            put :update, params: { id: users(:minimum_end_user).id, user: { name: "some name" } }
            assert_response 429
          end
        end

        it "rate limits update or create_or_update on unique user and allows them after 1 minute on same user" do
          Timecop.freeze(current_time) do
            put :update, params: { id: users(:minimum_end_user).id, user: { name: "some name" } }
            assert_response 429
            post :create_or_update, params: { user: { id: users(:minimum_end_user).id } }
            assert_response 429
          end
          Timecop.freeze(current_time + 1.minute) do
            put :update, params: { id: users(:minimum_end_user).id, user: { name: "some name" } }
            assert_response :success
            post :create_or_update, params: { user: { id: users(:minimum_end_user).id } }
            assert_response :success
          end
        end

        it "rate limits update or create_or_update on unique user and allows them after 1 minute on different user" do
          Timecop.freeze(current_time) do
            (rate_limit + 1).times do
              post :create_or_update, params: { user: { name: "Different user", email: "iamanother@email.com", external_id: 100} }
            end
            post :create_or_update, params: { user: { name: "Different user", email: "iamanother@email.com", external_id: 100} }
            assert_response 429
          end
          Timecop.travel(current_time + 1.minute) do
            put :update, params: { id: users(:minimum_end_user).id, user: { name: "some name" } }
            assert_response :success
            post :create_or_update, params: { user: { name: "Different user", email: "iamanother@email.com", external_id: 100} }
            assert_response :success
          end
        end

        describe_with_arturo_disabled :rate_limit_create_or_update_on_unique_user do
          before do
            Rails.logger.stubs(:info)
            Timecop.freeze(current_time) do
              rate_limit.times do
                put :update, params: { id: users(:minimum_end_user).id, user: { name: "some name" } }
              end
            end
          end

          it "updates the user and logs the rate limit" do
            Timecop.freeze(current_time) do
              Rails.logger.expects(:info).with { |m| m.starts_with?("Would have rate limited create_or_updates by user") }
              put :update, params: { id: users(:minimum_end_user).id, user: { name: "some name" } }
              assert_response :success
            end
          end
        end
      end
    end

    describe "a POST to :create_many" do
      let(:statsd_client) { stub_for_statsd }
      let(:referrer) { "https://subdomain/api/v2/users/create_many" }

      before do
        @current_brand = FactoryBot.create(:brand, subdomain: 'lemurs')
        @controller.request.env['HTTP_REFERER'] = referrer
        @controller.stubs(:statsd_client).returns(statsd_client)
        @controller.stubs(:current_brand).returns(@current_brand)
        statsd_client.stubs(:increment)
      end

      describe "with valid params" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          UserBulkCreateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          post :create_many, params: { users: [{"name" => "Roger Wilco", "email" => "roge@example.org", "role" => "agent", "verified" => true}, {"name" => "Woger Rilco", "email" => "woge@example.org", "role" => "admin", "phone" => "+15553148109"}] }
        end

        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with invalid params" do
        before do
          statsd_client.expects(:increment).never
          post :create_many
        end

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "allows 100 users" do
        let(:tags) do
          ["source:#{referrer}", "users_count:100"]
        end

        before do
          job_id_stub = stub
          job_status_stub = stub
          UserBulkCreateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          statsd_client.expects(:increment).with('create_many', tags: tags)
          post_create_many(100)
        end

        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "disallows >100 users" do
        before do
          statsd_client.expects(:increment).never
          post_create_many(101)
        end

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with many_users_limit setting boosted" do
        before do
          @account = users(:minimum_end_user).account
          @account.settings.many_users_limit = 1000
          @account.save!
          @user.account.settings.reload
        end

        describe "allows 1000 users" do
          let(:tags) do
            ["source:#{referrer}", "users_count:1000"]
          end

          before do
            job_id_stub = stub
            job_status_stub = stub
            UserBulkCreateJob.expects(:enqueue).returns(job_id_stub)
            Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
            statsd_client.expects(:increment).with('create_many', tags: tags)
            post_create_many(1000)
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end

        describe "disallows >1000 users" do
          before do
            statsd_client.expects(:increment).never
            post_create_many(1001)
          end

          it('responds with bad_request') { assert_response :bad_request }
        end
      end
    end

    describe "a PUT to :update_many" do
      let(:statsd_client) { stub_for_statsd }
      let(:referrer) { "https://subdomain/api/v2/users/update_many" }

      before do
        @account = accounts(:minimum)
        @users = [
          FactoryBot.create(:user, account: @account, name: 'User 1', role: :end_user, external_id: 1),
          FactoryBot.create(:user, account: @account, name: 'User 2', role: :end_user, external_id: 2)
        ]
        @controller.request.env['HTTP_REFERER'] = referrer
        @controller.stubs(:statsd_client).returns(statsd_client)
        statsd_client.stubs(:increment)
      end

      describe "with valid params" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          UserBulkUpdateJobV2.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
        end

        describe "when referencing to ids" do
          let(:tags) do
            ["source:#{referrer}", "users_count:#{@users.count}"]
          end

          before do
            user_ids = @users.map(&:id).join(",")
            statsd_client.expects(:increment).with('update_many', tags: tags)
            put :update_many, params: { ids: user_ids, user: { verified: true } }
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end

        describe "when referencing to external_ids" do
          let(:tags) { ["source:#{referrer}", "users_count:#{@users.count}"] }

          before do
            user_ids = @users.map(&:external_id).join(",")
            statsd_client.expects(:increment).with('update_many', tags: tags)
            put :update_many, params: { external_ids: user_ids, user: { verified: true } }
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end
      end

      describe "with valid batch params" do
        let(:users_batch) do
          [
            { id: @users[0].id, name: 'New Name 1' },
            { id: @users[1].id, verified: true }
          ]
        end

        let(:tags) { ["source:#{referrer}", "users_count:#{users_batch.count}"] }

        before do
          job_id_stub = stub
          job_status_stub = stub
          UserBatchUpdateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          statsd_client.expects(:increment).with('update_many', tags: tags)
          put :update_many, params: { users: users_batch }
        end

        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with invalid params" do
        describe "when params not allowed" do
          before do
            statsd_client.expects(:increment).never
            put :update_many, params: { foo: "bar" }
          end
          it('responds with bad_request') { assert_response :bad_request }
        end

        describe "when params missing" do
          before do
            statsd_client.expects(:increment).never
            put :update_many
          end
          it('responds with bad_request') { assert_response :bad_request }
        end

        describe "with ids set to nil" do
          before do
            statsd_client.expects(:increment).never
            put :update_many, params: { ids: nil }
          end

          it('responds with bad request') { assert_response :bad_request }
        end

        describe "with external_ids set to nil" do
          before do
            statsd_client.expects(:increment).never
            put :update_many, params: { external_ids: nil }
          end

          it('responds with bad request') { assert_response :bad_request }
        end

        describe "with invalid custom_role_id" do
          before do
            user_ids = @users.map(&:external_id).join(",")
            statsd_client.expects(:increment).never
            put :update_many, params: { external_ids: user_ids, user: { custom_role_id: 0 } }
          end

          it('responds with bad request') { assert_response :bad_request }
        end
      end
    end

    describe "a POST to :create_or_update_many" do
      let(:statsd_client) { stub_for_statsd }
      let(:referrer) { "https://subdomain/api/v2/users/create_or_update_many" }

      before do
        @controller.request.env['HTTP_REFERER'] = referrer
        @controller.stubs(:statsd_client).returns(statsd_client)
        statsd_client.stubs(:increment)
      end

      describe "with valid params" do
        before do
          job_id_stub = stub
          job_status_stub = stub
          UserBulkCreateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          post :create_or_update_many, params: { users: [{"name" => "Roger Wilco", "email" => "roge@example.org", "role" => "agent", "verified" => true}, {"name" => "Woger Rilco", "email" => "woge@example.org", "role" => "admin", "phone" => "+15553148109"}] }
        end

        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with invalid params" do
        before do
          statsd_client.expects(:increment).never
          post :create_or_update_many
        end

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "allows 100 users" do
        let(:tags) do
          ["source:#{referrer}", "users_count:100"]
        end

        before do
          job_id_stub = stub
          job_status_stub = stub
          UserBulkCreateJob.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
          statsd_client.expects(:increment).with('create_or_update_many', tags: tags)
          post_create_or_update_many(100)
        end

        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "disallows >100 users" do
        before do
          statsd_client.expects(:increment).never
          post_create_or_update_many(101)
        end

        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with many_users_limit setting boosted" do
        before do
          @account = users(:minimum_end_user).account
          @account.settings.many_users_limit = 1000
          @account.save!
          @user.account.settings.reload
        end

        describe "allows 1000 users" do
          let(:tags) do
            ["source:#{referrer}", "users_count:1000"]
          end

          before do
            job_id_stub = stub
            job_status_stub = stub
            UserBulkCreateJob.expects(:enqueue).returns(job_id_stub)
            Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
            statsd_client.expects(:increment).with('create_or_update_many', tags: tags)
            post_create_or_update_many(1000)
          end

          should_use_presenter Api::V2::JobStatusPresenter
        end

        describe "disallows >1000 users" do
          before do
            statsd_client.expects(:increment).never
            post_create_or_update_many(1001)
          end

          it('responds with bad_request') { assert_response :bad_request }
        end
      end
    end

    describe "a DELETE to :destroy" do
      before { User.any_instance.stubs(:delete!).returns(true) }

      describe "himself" do
        before { delete :destroy, params: { id: @user.id } }
        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "another agent" do
        before do
          @other = users(:minimum_author)
          @other.update_attribute(:roles, Role::AGENT.id)
          assert @other.reload.is_agent?
          delete :destroy, params: { id: @other.id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "an admin" do
        before { delete :destroy, params: { id: users(:minimum_admin).id } }
        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "an end-user" do
        before { delete :destroy, params: { id: users(:minimum_end_user).id } }

        should_use_presenter Api::V2::Users::AgentPresenter
      end

      describe "a foreign user" do
        before do
          User.any_instance.stubs(:foreign?).returns(true)
          delete :destroy, params: { id: users(:minimum_end_user).id }
        end

        should_use_presenter Api::V2::Users::AgentPresenter
      end
    end
  end

  as_an_admin do
    describe "a POST to :create" do
      describe "an agent" do
        describe "normal" do
          before { post :create, params: { user: { name: "Rodney", email: "the.rod@example.org", role: "agent" } } }
          should_use_presenter Api::V2::Users::AgentPresenter, status: :created, location: /\/api\/v2\/users\/\d+\.json$/
          it "emits an user_created domain event" do
            assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          end
        end

        describe "creating a new user with more identities than allowed" do
          let(:new_user) { FactoryBot.build(:user, account: @account, roles: Role::AGENT.id) }

          before do
            Account.any_instance.stubs(:has_user_identity_limit?).returns(true)
            FactoryBot.create(:max_identities_account_setting, account: @account)
            # RAILS5UPGRADE: Try removing if making property_sets
            # bi-directional is successful
            @account.settings.reload
            User.any_instance.stubs(:account).returns(@account)
          end

          it 'should be invalid' do
            post :create, params: { user: {name: 'taylor', role: 'agent', identities: [{ type: "email", value: "test@user.com"}, { type: "email", value: "test2@user.com" }]} }
            assert_response :unprocessable_entity
            assert_equal "The number of identities for this user has exceeded the limit of 1. An identity can be an email address, a phone number, a Twitter account, or a Facebook account.", JSON.parse(response.body)['details']['base'].first['description']
            assert_equal 1, JSON.parse(response.body)['details']['base'].size
          end

          it 'should be valid' do
            post :create, params: { user: {name: 'taylor', role: 'agent', identities: [{ type: "email", value: "test3@user.com" }]} }
            assert_response 201
          end

          it "emits an user_created domain event" do
            post :create, params: { user: {name: 'taylor', role: 'agent', identities: [{ type: "email", value: "test3@user.com" }]} }
            assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          end
        end

        describe "with group_ids" do
          before do
            @group_ids = [groups(:minimum_group).id]
            post :create, params: { user: { name: "Rodney", email: "the.rod@example.org", role: "agent", group_ids: @group_ids } }
          end

          should_use_presenter Api::V2::Users::AgentPresenter, status: :created, location: /\/api\/v2\/users\/\d+\.json$/

          it "creates a user with groups" do
            result = JSON.parse(@response.body)
            user_id = result['user']['id']
            user = @controller.send(:current_account).users.find(user_id)

            assert_equal @group_ids, user.groups.map(&:id)
          end

          it "emits user domain event user_default_group_changed" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_default_group_changed).size, 1
          end
        end

        describe "with only_private_comments being nil" do
          before { post :create, params: { user: { name: "Rodney", email: "the.rod@example.org", role: "agent", only_private_comments: nil } } }

          should_use_presenter Api::V2::Users::AgentPresenter, status: :created, location: /\/api\/v2\/users\/\d+\.json$/

          it "defaults is_private_comments_only to false" do
            result = JSON.parse(@response.body)
            user_id = result['user']['id']
            user = @controller.send(:current_account).users.find(user_id)

            assert_equal false, user.is_private_comments_only
          end
        end

        describe "with a language" do
          let(:user_params) { {} }
          let(:spanish) { translation_locales(:spanish) }
          let(:japanese) { translation_locales(:japanese) }
          let(:user) { User.find(JSON.parse(response.body).dig('user', 'id')) }

          before do
            post :create, params: { user: user_params.merge(name: 'Language', role: 'agent', email: 'language@example.org') }
          end

          describe "by passing in `locale_id`" do
            let(:user_params) { { locale_id: spanish.id } }

            it "correctly updates the locale" do
              assert_equal spanish, user.translation_locale
            end
          end

          describe "by passing in `locale`" do
            let(:user_params) { { locale: spanish.locale } }

            it "correctly updates the locale" do
              assert_equal spanish, user.translation_locale
            end
          end

          describe "by passing in both `locale_id` and `locale`" do
            let(:user_params) { { locale_id: japanese.id, locale: spanish.locale } }

            it "correctly updates the locale" do
              assert_equal spanish, user.translation_locale
            end
          end

          describe "an invalid `locale`" do
            let(:user_params) { { locale_id: 11111 } }

            it "responds with a 422 RecordInvalid error" do
              assert_response 422
            end
          end

          describe "an invalid `locale_id`" do
            let(:user_params) { { locale: "fake" } }

            it "responds with a 422 RecordInvalid error" do
              assert_response 422
            end
          end
        end

        describe "an end-user that belongs to multiple organizations" do
          let(:organization_ids) { [organizations(:minimum_organization1).id, organizations(:minimum_organization2).id] }
          before do
            Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
            post :create, params: { user: { name: "Argo", email: "the.argo2@example.org", role: "end-user", organization_ids: organization_ids } }
          end

          it("responds with created") { assert_response :created }

          it "emits user_organization_added domain events" do
            organization_added_events = decode_user_events(domain_event_publisher.events, :user_organization_added)
            organization_added_events.size.must_equal 2
            assert_includes organization_ids, organization_added_events.first.user_organization_added.organization.id.value
            assert_includes organization_ids, organization_added_events.second.user_organization_added.organization.id.value
          end
        end
      end

      describe "an admin" do
        before { post :create, params: { user: { name: "Rodney", email: "the.rod@example.org", role: "admin" } } }
        should_use_presenter Api::V2::Users::AgentPresenter, status: :created, location: /\/api\/v2\/users\/\d+\.json$/
        it "emits an user_created domain event" do
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
        end
      end

      describe "an end user" do
        describe "normal" do
          before { post :create, params: { user: { name: "Rodney", email: "the.rod@example.org"} } }
          should_use_presenter Api::V2::Users::AgentPresenter, status: :created, location: /\/api\/v2\/users\/\d+\.json$/
          it "emits an user_created domain event" do
            assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          end
        end

        describe "an end-user that belongs to multiple organizations" do
          let(:organization_ids) { [organizations(:minimum_organization1).id, organizations(:minimum_organization2).id] }
          before do
            Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
            post :create, params: { user: { name: "Argo", email: "the.argo3@example.org", organization_ids: organization_ids } }
          end

          it("responds with created") { assert_response :created }

          it "emits user_organization_added domain events" do
            organization_added_events = decode_user_events(domain_event_publisher.events, :user_organization_added)
            organization_added_events.size.must_equal 2
            assert_includes organization_ids, organization_added_events.first.user_organization_added.organization.id.value
            assert_includes organization_ids, organization_added_events.second.user_organization_added.organization.id.value
          end
        end

        describe "skip welcome email" do
          before { post :create, params: { user: { name: "Rodney", email: "the.rod@example.org", skip_verify_email: true} } }
          should_use_presenter Api::V2::Users::AgentPresenter, status: :created, location: /\/api\/v2\/users\/\d+\.json$/
          it "emits an user_created domain event" do
            assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          end
        end

        describe "with a requested brand for the welcome email" do
          before do
            @brand = FactoryBot.create(:brand, subdomain: 'lemurs')
            ActionMailer::Base.perform_deliveries = true
            ActionMailer::Base.deliveries = []
            Zendesk::UserPortalState.any_instance.stubs(:send_verify_email?).returns(true)
            Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)
            post :create, params: { user: { name: "Wombat", email: "wombat@taz.com", active_brand_id: @brand.id} }
          end

          it 'should send the welcome email using the requested brand' do
            result = JSON.parse(@response.body)
            user_id = result['user']['id']
            user = @controller.send(:current_account).users.find(user_id)

            assert_equal user_id, user.id
            assert_match %r{lemurs.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
            assert_equal ["support@lemurs.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
            assert_includes ActionMailer::Base.deliveries.last.subject, "Welcome to"
          end

          it "emits an user_created domain event" do
            assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          end
        end

        describe "with no brand requested for welcome email" do
          before do
            @current_brand = FactoryBot.create(:brand, subdomain: 'lemurs')
            ActionMailer::Base.perform_deliveries = true
            ActionMailer::Base.deliveries = []
            Zendesk::UserPortalState.any_instance.stubs(:send_verify_email?).returns(true)
            Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)
            @controller.stubs(:current_brand).returns(@current_brand)
            post :create, params: { user: { name: "Wombat", email: "wombat@taz.com" } }
          end

          it 'should send the welcome email using the current brand' do
            result = JSON.parse(@response.body)
            user_id = result['user']['id']
            user = @controller.send(:current_account).users.find(user_id)

            assert_equal user_id, user.id
            assert_match %r{lemurs.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
            assert_equal ["support@lemurs.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
            assert_includes ActionMailer::Base.deliveries.last.subject, "Welcome to"
          end

          it "emits an user_created domain event" do
            assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          end
        end

        describe "phone number" do
          let(:domain_event_publisher) { FakeEscKafkaMessage.new }
          describe "when account setting is true" do
            before do
              User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
              Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
            end

            describe "when E164 validation is true" do
              describe "when shared_phone_number parameter is true" do
                describe "when the phone is unique" do
                  before do
                    post :create, params: { user: { name: "Wombat", phone: "+15551234567", shared_phone_number: true} }
                  end

                  it "create direct line" do
                    result = JSON.parse(@response.body)
                    assert(result['user']['shared_phone_number'])
                    assert_equal result['user']['phone'], "+15551234567"
                    @user = User.find(result['user']['id'])
                    assert_nil @user.identities.reload.phone.first
                  end

                  it "does not emit user identity events" do
                    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_created).size
                    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_removed).size
                    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_changed).size
                  end
                end

                describe "when the phone exists" do
                  before do
                    other_user_1 = FactoryBot.create(:user, account: @account, name: 'User 1', phone: '+15551234567')
                    other_user_1.update_phone_and_identity(other_user_1.phone, true) # enable direct line
                    put :update, params: { id: other_user_1.id, user: { phone: "+15551234567", shared_phone_number: true} }
                  end

                  it "remove phone number identity" do
                    result = JSON.parse(@response.body)
                    assert_equal result['user']['phone'], "+15551234567"
                    assert(result['user']['shared_phone_number'])
                    @user = User.find(result['user']['id'])
                    assert_nil @user.identities.reload.phone.first
                  end

                  it "emit event user_identity_removed" do
                    assert_equal 1, decode_user_events(domain_event_publisher.events, :user_identity_removed).size
                  end
                end

                describe "when the phone number is unique" do
                  before do
                    post :create, params: { user: { name: "Wombat", phone: "+15551234567", shared_phone_number: true} }
                  end

                  it "creates direct line" do
                    result = JSON.parse(@response.body)
                    assert(result['user']['shared_phone_number'])
                    assert_equal result['user']['phone'], "+15551234567"
                    @user = User.find(result['user']['id'])
                    assert_nil @user.identities.reload.phone.first
                  end

                  it "does not emit user identity events" do
                    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_created).size
                    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_removed).size
                    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_changed).size
                  end
                end

                describe "when the phone number exists" do
                  before do
                    other_user_1 = FactoryBot.create(:user, account: @account, name: 'User 1', phone: '+15551234567')
                    other_user_1.update_phone_and_identity(other_user_1.phone, true) # enable direct line
                    put :update, params: { id: other_user_1.id, user: { phone: "+15551234567", shared_phone_number: true} }
                  end

                  it "removes phone number identity" do
                    result = JSON.parse(@response.body)
                    assert_equal result['user']['phone'], "+15551234567"
                    assert(result['user']['shared_phone_number'])
                    @user = User.find(result['user']['id'])
                    assert_nil @user.identities.reload.phone.first
                  end

                  it "does not emit user identity events" do
                    assert_equal 2, decode_user_events(domain_event_publisher.events, :user_identity_created).size
                    assert_equal 1, decode_user_events(domain_event_publisher.events, :user_identity_removed).size
                    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_changed).size
                  end
                end

                describe "when more than one phone number identity exists" do
                  before do
                    other_user_1 = FactoryBot.create(:user, account: @account, name: 'User 1')
                    UserPhoneNumberIdentity.create(account: @account, user: other_user_1, value: '+15551234567')
                    UserPhoneNumberIdentity.create(account: @account, user: other_user_1, value: '+15559876543')
                    put :update, params: { id: other_user_1.id, user: { phone: "+15559876543", shared_phone_number: true} }
                  end

                  it "remove correct phone number identity" do
                    result = JSON.parse(@response.body)
                    assert(result['user']['shared_phone_number'])
                    assert_equal result['user']['phone'], "+15559876543"
                    @user = User.find(result['user']['id'])
                    assert_equal '+15551234567', @user.identities.reload.phone.first.value
                    assert_equal 1, @user.identities.reload.phone.count
                  end

                  it "emit event user_identity_removed" do
                    assert_equal 1, decode_user_events(domain_event_publisher.events, :user_identity_removed).size
                  end
                end
              end

              describe "when shared_phone_number parameter is false" do
                describe "when phone number identity is unique" do
                  before do
                    post :create, params: { user: { name: "Wombat", phone: "+15551234567", shared_phone_number: false } }
                  end
                  it "create direct line" do
                    result = JSON.parse(@response.body)
                    assert_equal false, result['user']['shared_phone_number']
                    @user = User.find(result['user']['id'])
                    assert_equal ["+15551234567"], @user.identities.phone.map(&:value)
                  end

                  it "emit event user_identity_created" do
                    assert_equal 1, decode_user_events(domain_event_publisher.events, :user_identity_created).size
                  end
                end

                describe "when phone number identity is not unique" do
                  before do
                    other_user_1 = FactoryBot.create(:user, account: @account, name: 'User 1', phone: '+15551234567')
                    other_user_1.update_phone_and_identity(other_user_1.phone, true)  # enable direct line
                    post :create, params: { user: { name: "Wombat2", phone: "+15551234567", shared_phone_number: false} }
                  end

                  it "does not create direct line" do
                    result = JSON.parse(@response.body)
                    assert_equal "RecordInvalid", result["error"]
                    refute result['details'].empty?
                  end

                  it "does not emit user identity events" do
                    assert_equal 2, decode_user_events(domain_event_publisher.events, :user_identity_created).size
                    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_removed).size
                    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_changed).size
                  end
                end
              end

              describe "when shared_phone_number is empty" do
                describe "when phone number identity is unique" do
                  before do
                    post :create, params: { user: { name: "Wombat", phone: "+15551234567"} }
                  end

                  it "create direct line" do
                    result = JSON.parse(@response.body)
                    assert_equal false, result['user']['shared_phone_number']
                    @user = User.find(result['user']['id'])
                    assert_equal ["+15551234567"], @user.identities.phone.map(&:value)
                  end

                  it "emit event user_identity_created" do
                    assert_equal 1, decode_user_events(domain_event_publisher.events, :user_identity_created).size
                  end
                end

                describe "when phone number identity is not unique" do
                  before do
                    other_user_1 = FactoryBot.create(:user, account: @account, name: 'User 1', phone: '+15551234567')
                    other_user_1.update_phone_and_identity(other_user_1.phone, true)  # enable direct line
                    post :create, params: { user: { name: "Wombat2", phone: "+15551234567"} }
                  end

                  it "does not create direct line" do
                    result = JSON.parse(@response.body)
                    assert(result['user']['shared_phone_number'])
                    assert_equal result['user']['phone'], "+15551234567"
                    @user = User.find(result['user']['id'])
                    assert_nil @user.identities.reload.phone.first
                  end

                  it "does not emit user identity events" do
                    assert_equal 2, decode_user_events(domain_event_publisher.events, :user_identity_created).size
                    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_removed).size
                    assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_changed).size
                  end
                end
              end
            end

            describe "when E164 validation is false" do
              before do
                post :create, params: { user: { name: "Wombat2", phone: "567"} }
              end

              it "shows an error" do
                result = JSON.parse(@response.body)
                assert_equal "RecordInvalid", result["error"]
                refute result['details'].empty?
              end

              it "does not emit user identity events" do
                assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_created).size
                assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_removed).size
                assert_equal 0, decode_user_events(domain_event_publisher.events, :user_identity_changed).size
              end
            end
          end
        end
      end

      describe "a chat-only agent" do
        let(:form) do
          {
            name: "Rodney",
            email: "the.rod@example.org",
            role: "agent",
            custom_role_id: @chat_permission_set.id
          }
        end

        before do
          Zopim::AgentSyncJob.stubs(:work)

          ZendeskBillingCore::Zuora::Jobs::ZopimIdSyncJob.stubs(:enqueue)
          ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)
          ZopimIntegration.stubs(
            where: stub(:first_or_create!),
            ensure_not_phase_four: nil
          )
          Zopim::Reseller.client.stubs(
            create_account!: Hashie::Mash.new(id: 1),
            account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
            create_account_agent!: stub(id: 1234),
            find_accounts!: []
          )

          Account.any_instance.stubs(
            has_permission_sets?: true,
            has_chat_permission_set?: true
          )

          @account = accounts(:minimum)
          @account.create_zopim_subscription!.tap do |record|
            record.zopim_account_id = record.id
            record.syncstate        = ZendeskBillingCore::Zopim::SyncState::Ready
            record.save!
          end

          @chat_permission_set = PermissionSet.create_chat_agent!(@account)
        end

        describe "when everything is kewl" do
          before do
            Account.any_instance.stubs(has_permission_sets?: true)
            post :create, params: { user: form }
          end

          it('responds with created') { assert_response :created }
          should_use_presenter Api::V2::Users::AgentPresenter,
            status: :created, location: /\/api\/v2\/users\/\d+\.json$/
          it "emits an user_created domain event" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_created).size, 1
          end
        end

        describe "with an unserviceable zopim subscription" do
          before do
            ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(is_serviceable?: false)
            post :create, params: { user: form }
          end

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
        end

        describe "without enough Zopim licenses available" do
          before do
            @account.zopim_subscription.update_column(:max_agents, 1)
          end

          describe "when the subscription is no longer in trial" do
            before do
              ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(is_trial?: false)
              post :create, params: { user: form }
            end

            it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
          end
        end

        describe "when the account does not have permission sets" do
          before do
            Account.any_instance.stubs(has_chat_permission_set?: true, has_permission_sets?: false)
            post :create, params: { user: form }
          end

          it('responds with created') { assert_response :created }

          it "emits an user_created domain event" do
            assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          end
        end
      end

      describe 'with is_verified' do
        describe 'as true' do
          describe 'with no identity' do
            it 'response should return verified:false' do
              post :create, params: { user: { name: 'new_user', is_verified: true } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end

          describe 'with valid identity' do
            describe 'is given' do
              it 'response should return verified:false' do
                post :create, params: { user: { name: 'new_user', is_verified: true, email: 'valid1@test.com' } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end

            describe 'is given in array' do
              it 'response should return verified:false' do
                post :create, params: { user: { name: 'new_user', is_verified: true, identities: [{type: 'email', value: 'valid2@test.com'}] } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end
          end

          describe 'with invalid identity' do
            describe 'is given' do
              it 'response should return verified:false' do
                post :create, params: { user: { name: 'new_user', is_verified: true, emails: 'invalid@test.com' } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end

            describe 'is given in array' do
              it 'response should return verified:false' do
                post :create, params: { user: { name: 'new_user', is_verified: true, identities: [{type: 'emails', value: 'invalid@test.com'}] } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end
          end
        end

        describe 'as false' do
          describe 'with no identity' do
            it 'response should return verified:false' do
              post :create, params: { user: { name: 'new_user', is_verified: false } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end

          describe 'with valid identity' do
            describe 'is given' do
              it 'response should return verified:false' do
                post :create, params: { user: { name: 'new_user', is_verified: false, email: 'valid3@test.com' } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end

            describe 'is given in array' do
              it 'response should return verified:false' do
                post :create, params: { user: { name: 'new_user', is_verified: false, identities: [{type: 'email', value: 'valid4@test.com'}] } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end
          end

          describe 'with invalid identity' do
            describe 'is given' do
              it 'response should return verified:false' do
                post :create, params: { user: { name: 'new_user', is_verified: false, emails: 'invalid@test.com' } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end

            describe 'is given in array' do
              it 'response should return verified:false' do
                post :create, params: { user: { name: 'new_user', is_verified: false, identities: [{type: 'emails', value: 'invalid@test.com'}] } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end
          end
        end
      end

      describe 'with verified' do
        describe 'as true' do
          describe 'with no identity' do
            it 'response should return verified:false' do
              post :create, params: { user: { name: 'new_user', verified: true } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end

          describe 'with valid identity' do
            describe 'is given' do
              it 'response should return verified:true' do
                post :create, params: { user: { name: 'new_user', verified: true, email: 'valid5@test.com' } }
                assert JSON.parse(@response.body)['user']['verified']
              end

              it 'with skip_verify_email true, should not send verification email' do
                post :create, params: { user: { name: 'new_user', role: 'agent', verified: true, email: 'valid16@test.com', skip_verify_email: true } }
                UsersMailer.expects(:deliver_verify).never
              end

              it 'with skip_verify_email false, should not send verification email' do
                post :create, params: { user: { name: 'new_user', role: 'agent', verified: true, email: 'valid20@test.com', skip_verify_email: false } }
                UsersMailer.expects(:deliver_verify).never
              end
            end

            describe 'is given in array' do
              it 'response should return verified:true' do
                post :create, params: { user: { name: 'new_user', verified: true, identities: [{type: 'email', value: 'valid6@test.com'}] } }
                assert JSON.parse(@response.body)['user']['verified']
              end
            end
          end

          describe 'with invalid identity is given in array' do
            it 'response should return verified:false' do
              post :create, params: { user: { name: 'new_user', verified: true, identities: [{type: 'emails', value: 'invalid@test.com'}] } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end
        end

        describe 'as false' do
          describe 'with no identity' do
            it 'response should return verified:false' do
              post :create, params: { user: { name: 'new_user', verified: false } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end

          describe 'with valid identity' do
            describe 'is given' do
              it 'response should return verified:false' do
                post :create, params: { user: { name: 'new_user', verified: false, email: 'valid7@test.com' } }
                refute JSON.parse(@response.body)['user']['verified']
              end

              it 'with skip_verify_email true, should not send verification email' do
                post :create, params: { user: { name: 'new_user', verified: false, email: 'valid18@test.com', skip_verify_email: true } }
                UsersMailer.expects(:deliver_verify).never
              end

              it 'with skip_verify_email false and agent is created, should send verification email' do
                UsersMailer.expects(:deliver_verify)
                post :create, params: { user: { name: 'new_user', role: 'agent', verified: false, email: 'valid20@test.com', skip_verify_email: false } }
              end
            end

            describe 'is given in array' do
              it 'response should return verified:false' do
                post :create, params: { user: { name: 'new_user', verified: false, identities: [{type: 'email', value: 'valid8@test.com'}] } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end
          end

          describe 'with invalid identity is given in array' do
            it 'response should return verified:false' do
              post :create, params: { user: { name: 'new_user', verified: false, identities: [{type: 'emails', value: 'invalid@test.com'}] } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end
        end
      end
    end

    describe "a POST to :create_or_update" do
      describe "when it has no data" do
        before { post :create_or_update }

        it "gets rejected" do
          expected_response = {'error' => 'UnknownAttributeError', 'description' => "Invalid attribute: missing user parameter"}
          assert_equal expected_response, JSON.parse(@response.body)
        end

        it "does not emit user domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events).size
        end
      end

      describe "create a new end-user that belongs to multiple organizations" do
        let(:organization_ids) { [organizations(:minimum_organization1).id, organizations(:minimum_organization2).id] }
        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
          post :create_or_update, params: { user: { name: "Argo", email: "the.argo4@example.org", role: "end-user", organization_ids: organization_ids } }
        end
        it("responds with created") { assert_response :created }
        it "emits user_organization_added domain events" do
          organization_added_events = decode_user_events(domain_event_publisher.events, :user_organization_added)
          organization_added_events.size.must_equal 2
          assert_includes organization_ids, organization_added_events.first.user_organization_added.organization.id.value
          assert_includes organization_ids, organization_added_events.second.user_organization_added.organization.id.value
        end
      end

      describe "when an invalid organization_id is provided" do
        before { post :create_or_update, params: { user: { name: "Rodney", email: "the.rod@example.org", organization_id: 12345 } } }

        it "responds with a 422" do
          assert_response :unprocessable_entity
        end

        it "response includes erroneous org id" do
          assert_match '12345', response.body
        end

        it "does not emit user domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events).size
        end
      end

      describe "updates an existing user via email" do
        let(:organization_ids) { [organizations(:minimum_organization2).id, organizations(:minimum_organization3).id] }
        let(:current_user) { @controller.send(:current_account).users.first }
        let(:new_name) { "Bobby Buttburger" }
        before do
          Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
          form = {
            email: current_user.email,
            name: new_name,
            organization_ids: organization_ids
          }
          post :create_or_update, params: { user: form }
        end

        it "sucessfully update the existing user" do
          assert_equal new_name, current_user.reload.name
        end

        it "emits correct user domain events" do
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_name_changed).size
          assert_equal 2, decode_user_events(domain_event_publisher.events, :user_organization_added).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_organization_removed).size
        end
      end

      it "updates an existing user via external ID" do
        current_user = @controller.send(:current_account).users.first
        current_user.update_attributes(external_id: 'abc')

        new_email_address = 'testing@zendeskfan.com'

        form = {
          external_id: 'abc',
          email: new_email_address
        }

        post :create_or_update, params: { user: form }

        email_addresses = []

        current_user.reload.identities.each do |identity|
          email_addresses << identity.value if identity.class == UserEmailIdentity
        end

        assert_includes email_addresses, new_email_address
      end

      describe "when create new user" do
        before do
          form = {
            email: "smurfette@gmail.com",
            name: "Smurfette"
          }
          post :create_or_update, params: { user: form }
        end

        it "successfully create user" do
          result = JSON.parse(@response.body)
          user_id = result['user']['id']
          user = @controller.send(:current_account).users.find(user_id)

          assert_not_nil user_id
          assert_equal user_id, user.id
        end

        it "emits correct user domain events" do
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_name_changed).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_role_changed).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :custom_role_changed).size
        end
      end

      describe "when creating user on multibrand account" do
        describe "with brand requested for welcome email" do
          before do
            @brand = FactoryBot.create(:brand, subdomain: 'lemurs')
            ActionMailer::Base.perform_deliveries = true
            ActionMailer::Base.deliveries = []
            Zendesk::UserPortalState.any_instance.stubs(:send_verify_email?).returns(true)
            Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)
            post :create_or_update, params: { user: { name: "Wombat", email: "wombat@taz.com", active_brand_id: @brand.id} }
          end

          it 'should send the welcome email using the requested brand' do
            result = JSON.parse(@response.body)
            user_id = result['user']['id']
            user = @controller.send(:current_account).users.find(user_id)

            assert_equal user_id, user.id
            assert_match %r{lemurs.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
            assert_equal ["support@lemurs.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
            assert_includes ActionMailer::Base.deliveries.last.subject, "Welcome to"
          end

          it "emits correct user domain events" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_created).size, 1
            assert_equal decode_user_events(domain_event_publisher.events, :user_name_changed).size, 0
          end
        end

        describe "with no brand requested for welcome email" do
          before do
            current_brand = FactoryBot.create(:brand, subdomain: 'lemurs')
            ActionMailer::Base.perform_deliveries = true
            ActionMailer::Base.deliveries = []
            Zendesk::UserPortalState.any_instance.stubs(:send_verify_email?).returns(true)
            Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)
            @controller.stubs(:current_brand).returns(current_brand)
            post :create, params: { user: { name: "Wombat", email: "wombat@taz.com" } }
          end

          it 'should send the welcome email using the requested brand' do
            result = JSON.parse(@response.body)
            user_id = result['user']['id']
            user = @controller.send(:current_account).users.find(user_id)

            assert_equal user_id, user.id
            assert_match %r{lemurs.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
            assert_equal ["support@lemurs.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
            assert_includes ActionMailer::Base.deliveries.last.subject, "Welcome to"
          end

          it "emits correct user domain events" do
            assert_equal decode_user_events(domain_event_publisher.events, :user_created).size, 1
            assert_equal decode_user_events(domain_event_publisher.events, :user_name_changed).size, 0
          end
        end
      end

      it "updates a single organization" do
        current_user = @controller.send(:current_account).users.first
        organization = organizations(:minimum_organization1)

        form = {
          id: current_user.id,
          organization: {name: organization.name}
        }

        post :create_or_update, params: { user: form }

        JSON.parse(@response.body)

        assert_equal 1, current_user.organizations.count(:all)
        assert_equal organization.id, current_user.organizations.first.id
      end

      it "updates multiple organizations" do
        Account.any_instance.stubs(:has_multiple_organizations_enabled?).returns(true)

        current_account = accounts(:minimum)
        @controller.stubs(:current_account).returns(current_account)

        current_user = current_account.users.first
        new_organization = organizations(:minimum_organization2)
        existing_organization = organizations(:minimum_organization1)

        current_user.organization = existing_organization
        current_user.save!

        form = {
          id: current_user.reload.id,
          organization: {name: new_organization.name}
        }

        post :create_or_update, params: { user: form }

        assert_equal 2, current_user.organizations.count(:all)
        assert_equal [new_organization, existing_organization].sort, current_user.organizations.sort
      end

      it "finds an existing user and adds an external ID on a subsequent call" do
        current_user = @controller.send(:current_account).users.first
        new_name = "Helen Hater"

        form = {
          email: current_user.email,
          name: new_name
        }

        post :create_or_update, params: { user: form }

        assert_equal new_name, current_user.reload.name

        form[:external_id] = 'abc123'

        post :create_or_update, params: { user: form }

        assert_equal 'abc123', current_user.reload.external_id
      end

      describe "rate limiting for creating or updating" do
        before do
          Api::V2::UsersController.any_instance.unstub(:throttle_create_or_update)
        end

        it "doesn't rate limit the requests if the throttle_user_create_or_update atruo is off" do
          Timecop.freeze

          form1 = {
            email: 'this_email_is_brand_new@example.com',
            name: 'I bet this name does not exist yet'
          }

          form2 = {
            email: 'brand_new_email_this_is@example.com',
            name: 'Yoda, I am'
          }

          post :create_or_update, params: { user: form1 }
          assert_response :created

          post :create_or_update, params: { user: form2 }
          assert_response :created
        end

        it "rate limits requests to 1/s and points the user to create_or_update_many" do
          Arturo.enable_feature!(:throttle_user_create_or_update)
          Timecop.freeze

          form1 = {
            email: 'this_email_is_brand_new@example.com',
            name: 'I bet this name does not exist yet'
          }

          form2 = {
            email: 'brand_new_email_this_is@example.com',
            name: 'Yoda, I am'
          }

          post :create_or_update, params: { user: form1 }
          assert_response :created

          post :create_or_update, params: { user: form2 }
          assert_response 429
          response.body.must_equal I18n.t('txt.api.v2.users.create_or_update.rate_limit')

          Arturo.disable_feature!(:throttle_user_create_or_update)
        end

        describe "rate_limit_create_or_update_on_unique_user arturo" do
          let(:rate_limit) { 5 }
          before do
            Rails.logger.stubs(:info)
            Timecop.freeze
            @user1 = {
              email: 'user1@example.com',
              name: 'Obi Wan Kenobi'
            }
          end

          describe "arturo is enabled" do
            before do
              Arturo.enable_feature!(:rate_limit_create_or_update_on_unique_user)
              post :create_or_update, params: { user: @user1 }
            end

            it "rate limits updates on unique user" do
              rate_limit.times do
                post :create_or_update, params: { user: @user1 }
                assert_response :success
              end
              post :create_or_update, params: { user: @user1 }
              assert_response 429
            end
          end

          describe "arturo is disabled" do
            before do
              Arturo.disable_feature!(:rate_limit_create_or_update_on_unique_user)
              (rate_limit + 1).times do
                post :create_or_update, params: { user: @user1 }
              end
            end

            describe "create_or_update the user multiple times" do
              before do
                Rails.logger.expects(:info).with { |m| m.starts_with?("Would have rate limited create_or_updates by user") }
                post :create_or_update, params: { user: @user1 }
              end

              it "updates the user and logs the rate limit" do
                assert_response :success
              end
            end
          end
        end
      end

      describe 'user_create_or_update_skip_update_last_seen Arturo' do
        before do
          Arturo.disable_feature!(:user_create_or_update_skip_update_last_seen)
          @account = @controller.send(:current_account)
          @user = @controller.send(:current_account).users.first

          CustomField::Date.new.tap do |cf|
            cf.account = @account
            cf.owner = 'User'
            cf.key = 'system::embeddable_last_seen'
            cf.title = 'txt.something'
            cf.is_system = true
            cf.save!
          end

          @form = {
            email: @user.email,
            user_fields: {
              'system::embeddable_last_seen' => '2018-07-20T20:06:42.042Z'
            }
          }

          @controller.send(:current_user).stubs(:is_system_user?).returns(true)
        end

        describe 'Arturo is disabled' do
          before do
            Arturo.disable_feature!(:user_create_or_update_skip_update_last_seen)
          end

          describe 'updating user last seen' do
            it 'records information' do
              assert_empty @user.reload.custom_field_values

              post :create_or_update, params: { user: @form }

              assert_equal '2018-07-20', @user.reload.custom_field_values.first.value
            end
          end
        end

        describe 'Arturo is enabled' do
          before do
            Arturo.enable_feature!(:user_create_or_update_skip_update_last_seen)
          end

          describe 'updating user last seen and other info' do
            it 'records information' do
              @form[:name] = 'test new name'

              post :create_or_update, params: { user: @form }

              @user.reload
              assert_equal '2018-07-20', @user.custom_field_values.first.value
              assert_equal 'test new name', @user.name
            end
          end

          describe 'updating only user last seen' do
            it 'does not record information' do
              post :create_or_update, params: { user: @form }

              assert_empty @user.reload.custom_field_values
            end
          end
        end
      end

      describe "rate_limit_create_leaky_bucket" do
        let(:account) { accounts(:minimum) }
        before do
          Account.any_instance.stubs(:default_api_rate_limit).returns(3)
          Zendesk::Users::UsersLeakyBucket.configure_rate_limit(account.default_api_rate_limit)
          Rails.logger.stubs(:info)
          Timecop.freeze
          @user1 = {
              email: 'user1@example.com',
              name: 'Obi Wan Kenobi'
          }
          @user2 = {
              email: 'user2@example.com',
              name: 'Kylo Ren'
          }
        end

        describe "arturo is enabled" do
          before do
            Arturo.enable_feature!(:throttle_user_writes_leaky_bucket)
          end

          it "rate limits on create leaky" do
            ((account.default_api_rate_limit / 2).to_i + 1).times do
              post :create_or_update, params: { user: @user1 }
              assert_response :success
            end
            post :create, params: { user: @user2 }
            assert_response 429
          end
        end

        describe "arturo is disabled leaky" do
          before do
            Arturo.disable_feature!(:throttle_user_writes_leaky_bucket)
            ((account.default_api_rate_limit / 2).to_i + 1).times do
              post :create_or_update, params: { user: @user1 }
            end
          end

          describe "create_or_update the user multiple times leaky" do
            before do
              Rails.logger.expects(:info).with { |m| m.starts_with?("Would have rate limited writes by leaky bucket") }
              post :create, params: { user: @user2 }
            end

            it "updates the user and logs the rate limit leaky" do
              assert_response :success
            end
          end
        end
      end

      describe 'with is_verified' do
        describe 'as true' do
          describe 'with no identity' do
            it 'response should return verified:false' do
              post :create_or_update, params: { user: { name: 'new_user', is_verified: true } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end

          describe 'with valid identity' do
            describe 'is given' do
              it 'response should return verified:false' do
                post :create_or_update, params: { user: { name: 'new_user', is_verified: true, email: 'valid9@test.com' } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end

            describe 'is given in array' do
              it 'response should return verified:false' do
                post :create_or_update, params: { user: { name: 'new_user', is_verified: true, identities: [{type: 'email', value: 'valid10@test.com'}] } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end
          end

          describe 'with invalid identity' do
            describe 'is given' do
              it 'response should return verified:false' do
                post :create_or_update, params: { user: { name: 'new_user', is_verified: true, emails: 'invalid@test.com' } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end

            describe 'is given in array' do
              it 'response should return verified:false' do
                post :create_or_update, params: { user: { name: 'new_user', is_verified: true, identities: [{type: 'emails', value: 'invalid@test.com'}] } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end
          end
        end

        describe 'as false' do
          describe 'with no identity' do
            it 'response should return verified:false' do
              post :create_or_update, params: { user: { name: 'new_user', is_verified: false } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end

          describe 'with valid identity' do
            describe 'is given' do
              it 'response should return verified:false' do
                post :create_or_update, params: { user: { name: 'new_user', is_verified: false, email: 'valid11@test.com' } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end

            describe 'is given in array' do
              it 'response should return verified:false' do
                post :create_or_update, params: { user: { name: 'new_user', is_verified: false, identities: [{type: 'email', value: 'valid12@test.com'}] } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end
          end

          describe 'with invalid identity' do
            describe 'is given' do
              it 'response should return verified:false' do
                post :create_or_update, params: { user: { name: 'new_user', is_verified: false, emails: 'invalid@test.com' } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end

            describe 'is given in array' do
              it 'response should return verified:false' do
                post :create_or_update, params: { user: { name: 'new_user', is_verified: false, identities: [{type: 'emails', value: 'invalid@test.com'}] } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end
          end
        end
      end

      describe "update a user that does not have any identity with verified: true" do
        before do
          post :create, params: { user: { name: "user without identity" } }
          @user_id = JSON.parse(@response.body)['user']['id']
        end

        it "response should return verified:false" do
          post :create_or_update, params: { id: @user_id, user: { name: 'updated_name', verified: true } }
          assert_equal 'updated_name', JSON.parse(@response.body)['user']['name']
          refute JSON.parse(@response.body)['user']['verified']
          get :show, params: { id: @user_id }
          refute JSON.parse(@response.body)['user']['verified']
        end
      end

      describe 'with verified' do
        describe 'as true' do
          describe 'with no identity' do
            it 'response should return verified:false' do
              post :create_or_update, params: { user: { name: 'new_user', verified: true } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end

          describe 'with valid identity' do
            describe 'is given' do
              it 'response should return verified:true' do
                post :create_or_update, params: { user: { name: 'new_user', verified: true, email: 'valid13@test.com' } }
                assert JSON.parse(@response.body)['user']['verified']
              end
            end

            describe 'is given in array' do
              it 'response should return verified:true' do
                post :create_or_update, params: { user: { name: 'new_user', verified: true, identities: [{type: 'email', value: 'valid14@test.com'}] } }
                assert JSON.parse(@response.body)['user']['verified']
              end
            end
          end

          describe 'with invalid identity is given in array' do
            it 'response should return verified:false' do
              post :create_or_update, params: { user: { name: 'new_user', verified: true, identities: [{type: 'emails', value: 'invalid@test.com'}] } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end
        end

        describe 'as false' do
          describe 'with no identity' do
            it 'response should return verified:false' do
              post :create_or_update, params: { user: { name: 'new_user', verified: false } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end

          describe 'with valid identity' do
            describe 'is given' do
              it 'response should return verified:false' do
                post :create_or_update, params: { user: { name: 'new_user', verified: false, email: 'valid15@test.com' } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end

            describe 'is given in array' do
              it 'response should return verified:false' do
                post :create_or_update, params: { user: { name: 'new_user', verified: false, identities: [{type: 'email', value: 'valid16@test.com'}] } }
                refute JSON.parse(@response.body)['user']['verified']
              end
            end
          end

          describe 'with invalid identity is given in array' do
            it 'response should return verified:false' do
              post :create_or_update, params: { user: { name: 'new_user', verified: false, identities: [{type: 'emails', value: 'invalid@test.com'}] } }
              refute JSON.parse(@response.body)['user']['verified']
            end
          end
        end
      end
    end

    describe "a DELETE to :destroy" do
      before { User.any_instance.stubs(:delete!).returns(true) }

      describe "himself" do
        before { delete :destroy, params: { id: @user.id } }
        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "another agent" do
        before do
          @other = users(:minimum_author)
          @other.update_attribute(:roles, Role::AGENT.id)
          assert @other.reload.is_agent?
          delete :destroy, params: { id: @other.id }
        end

        should_use_presenter Api::V2::Users::AgentPresenter
      end

      describe "an admin" do
        before { delete :destroy, params: { id: users(:minimum_admin_not_verified).id } }
        should_use_presenter Api::V2::Users::AgentPresenter
      end
    end

    describe "a DELETE to :destroy_many" do
      let(:statsd_client) { stub_for_statsd }
      let(:referrer) { "https://subdomain/api/v2/users/destroy_many" }

      before do
        @account = accounts(:minimum)
        @users = [
          FactoryBot.create(:user, account: @account, name: 'User 1', role: :end_user, external_id: 1),
          FactoryBot.create(:user, account: @account, name: 'User 2', role: :end_user, external_id: 2)
        ]
        @controller.request.env['HTTP_REFERER'] = referrer
        @controller.stubs(:statsd_client).returns(statsd_client)
        statsd_client.stubs(:increment)
      end

      describe "with valid params" do
        let(:tags) do
          ["source:#{referrer}", "users_count:#{@users.count}"]
        end

        before do
          job_id_stub = stub
          job_status_stub = stub
          UserBulkUpdateJobV2.expects(:enqueue).returns(job_id_stub)
          Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)
        end

        describe "when referencing to ids" do
          before do
            user_ids = @users.map(&:id).join(",")
            statsd_client.expects(:increment).with('destroy_many', tags: tags)
            delete :destroy_many, params: { ids: user_ids }
          end
          should_use_presenter Api::V2::JobStatusPresenter
        end

        describe "when referencing to external_ids" do
          before do
            user_ids = @users.map(&:external_id).join(",")
            statsd_client.expects(:increment).with('destroy_many', tags: tags)
            delete :destroy_many, params: { external_ids: user_ids }
          end
          should_use_presenter Api::V2::JobStatusPresenter
        end
      end

      describe "with invalid params" do
        before do
          statsd_client.expects(:increment).never
          delete :destroy_many, params: { foo: 'bar' }
        end
        it('responds with bad_request') { assert_response :bad_request }
      end
    end

    describe "a PUT to :merge" do
      describe "with a valid user" do
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).with(users(:minimum_author), users(:minimum_end_user)).returns(:merge_complete)
          put :merge, params: { id: users(:minimum_end_user).id, user: { id: users(:minimum_author).id } }
        end

        it('responds with ok') { assert_response :ok }
      end

      describe "with an invalid user" do
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).never
          put :merge, params: { id: users(:minimum_end_user).id, user: { id: 0 } }
        end

        it('responds with not_found') { assert_response :not_found }

        it "does not emit user_merged domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_merged).size
        end
      end

      describe "with a user object missing id" do
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).never
          put :merge, params: { id: users(:minimum_end_user).id, user: {} }
        end

        it('responds with bad_request') { assert_response :bad_request }

        it "does not emit user_merged domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_merged).size
        end
      end

      describe "with no user object" do
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).never
          put :merge, params: { id: users(:minimum_end_user).id }
        end

        it('responds with bad_request') { assert_response :bad_request }

        it "does not emit user_merged domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_merged).size
        end
      end

      describe "with user object set to null" do
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).never
          put :merge, params: { id: users(:minimum_end_user).id, user: nil }
        end

        it('responds with bad_request') { assert_response :bad_request }

        it "does not emit user_merged domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_merged).size
        end
      end

      describe "with an enduser to self" do
        before do
          put :merge, params: { id: users(:minimum_end_user).id, user: { id: users(:minimum_end_user).id } }
        end

        it('responds with bad_request') do
          assert_response :unprocessable_entity
          expected_response = {"error" => "You selected the same user to merge: #{users(:minimum_end_user).name}. You cannot merge the same user account into itself. Select a different user."}
          assert_equal expected_response, JSON.parse(@response.body)
        end

        it "does not emit user_merged domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_merged).size
        end
      end

      describe "with an agent to another agent" do
        before do
          put :merge, params: { id: users(:minimum_agent).id, user: { id: users(:minimum_admin).id } }
        end

        it('responds with bad_request') do
          assert_response :unprocessable_entity
          expected_response = {"error" => "You can merge end users only. #{users(:minimum_admin).name} is not an end user."}
          assert_equal expected_response, JSON.parse(@response.body)
        end

        it "does not emit user_merged domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_merged).size
        end
      end

      describe "with an agent to an end-user" do
        before do
          put :merge, params: { id: users(:minimum_agent).id, user: { id: users(:minimum_end_user).id } }
        end

        it('responds with bad_request') do
          assert_response :unprocessable_entity
          expected_response = {"error" => "You can merge end users only. #{users(:minimum_agent).name} is not an end user."}
          assert_equal expected_response, JSON.parse(@response.body)
        end

        it "does not emit user_merged domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_merged).size
        end
      end

      describe "with an end-user to an agent" do
        before do
          put :merge, params: { id: users(:minimum_end_user).id, user: { id: users(:minimum_agent).id } }
        end

        it('responds with bad_request') do
          assert_response :unprocessable_entity
          expected_response = {"error" => "You can merge end users only. #{users(:minimum_agent).name} is not an end user."}
          assert_equal expected_response, JSON.parse(@response.body)
        end

        it "does not emit user_merged domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_merged).size
        end
      end

      describe "with an end-user w/ remote auth and external_id" do
        let(:user) { users(:minimum_end_user) }
        before do
          Users::Merge.expects(:validate_and_merge_with_tickets_now).returns(:loser_has_sso_external_id)
          put :merge, params: { id: user.id, user: { id: users(:minimum_end_user2).id } }
        end

        it('responds with bad_request and error message') do
          assert_response :unprocessable_entity
          expected_response = {"error" => "You cannot merge #{user.name} while they have an external ID."}
          assert_equal expected_response, JSON.parse(@response.body)
        end

        it "does not emit user_merged domain events" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_merged).size
        end
      end
    end

    describe "a PUT to update a chat-only agent" do
      before do
        @chat_permission_set = PermissionSet.create_chat_agent!(@account)
        @user.permission_set_id = @chat_permission_set.id
        @user.save!
      end

      describe "to an admin" do
        before { put :update, params: { id: @user.id, user: { role: "admin" } } }

        it "removes any permission set id" do
          user = User.last
          assert_nil user.permission_set_id
        end

        it "sets custom role id again" do
          put :update, params: { id: @user.id, user: { custom_role_id: @chat_permission_set.id } }
          @user.reload
          assert_equal @user.permission_set_id, @chat_permission_set.id
        end
      end

      describe "to an agent" do
        before { put :update, params: { id: @user.id, user: { role: "agent", custom_role_id: nil } } }

        it "removes any permission set id" do
          user = User.last
          assert_nil user.permission_set_id
        end

        it "sets custom role id again" do
          put :update, params: { id: @user.id, user: { custom_role_id: @chat_permission_set.id } }
          @user.reload
          assert_equal @user.permission_set_id, @chat_permission_set.id
        end
      end

      describe "to an end-user" do
        before { put :update, params: { id: @user.id, user: { role: "end-user" } } }

        it "removes any permission set id" do
          user = User.last
          assert_nil user.permission_set_id
        end

        it "sets custom role id again" do
          put :update, params: { id: @user.id, user: { custom_role_id: @chat_permission_set.id } }
          @user.reload
          assert_equal @user.permission_set_id, @chat_permission_set.id
        end
      end
    end

    describe "if the user can't be merged" do
      before do
        Users::Merge.expects(:validate_and_merge_with_tickets_now).returns(:loser_non_enduser)
        put :merge, params: { id: users(:minimum_end_user).id, user: { id: users(:minimum_author).id } }
      end

      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      it "does not emit user_merged domain events" do
        assert_equal 0, decode_user_events(domain_event_publisher.events, :user_merged).size
      end
    end

    describe "with custom fields" do
      before do
        @account.settings.has_user_tags = true
        @account.save!

        @user = users(:minimum_end_user)
        @user.current_tags = nil
        @user.save!

        cf_cbox = create_user_custom_field!("field_checkbox", "title", "Checkbox")
        cf_cbox.tag = "toast"
        cf_cbox.save!
        create_user_custom_field!("field_text", "title", "Text")
        create_user_custom_field!("field_checkbox2", "title", "Checkbox") # not tag
      end

      describe "a POST to :create" do
        before do
          post :create, params: { user: {
            name: "Rodney", email: "the.rod@example.org", role: "end-user", tags: "moose",
            user_fields: {field_checkbox: true, field_checkbox2: true, field_text: "bar"}
          } }
        end

        it('responds with created') { assert_response :created }
        it "sets the custom fields on the user" do
          user = User.last
          assert_equal({"field_checkbox" => true, "field_checkbox2" => true, "field_text" => "bar"}, user.custom_field_values.as_json(account: @account))
          assert_equal(["moose", "toast"].sort, user.tags.sort)
        end

        it "emit correct custom_field_changed events" do
          custom_field_events = decode_user_events(domain_event_publisher.events, :custom_field_changed)
          assert_equal custom_field_events.size, 3
          expected_events = [true, true, "bar"]
          custom_field_events.each_with_index do |event, index|
            current = expected_events[index]
            event.custom_field_changed.previous.must_be_nil

            if current.is_a? TrueClass
              assert event.custom_field_changed.current.checkbox_value, current
            else
              assert event.custom_field_changed.current.text_value, current
            end
          end
        end
      end

      describe "a PUT to :update" do
        before do
          put :update, params: { id: @user.id, user: {
            name: "some name", tags: "moose",
            user_fields: {field_text: "foobar"}
          } }
        end

        it('responds with success') { assert_response :success }
        it "updates the user fields" do
          @user.reload
          assert_equal({"field_checkbox" => false, "field_checkbox2" => false, "field_text" => "foobar"}, @user.custom_field_values.as_json(account: @account))
          assert_equal(["moose"], @user.tags)
        end

        it "emit correct custom_field_changed events" do
          custom_field_events = decode_user_events(domain_event_publisher.events, :custom_field_changed)
          assert_equal 1, custom_field_events.size
          assert_equal "foobar", custom_field_events.first.custom_field_changed.current.text_value
        end
      end

      describe "updating a user with more identities allowed" do
        let(:user) { FactoryBot.create(:user, account: @account, roles: Role::AGENT.id, custom_role_id: nil) }

        before do
          Account.any_instance.stubs(:has_user_identity_limit?).returns(true)
          FactoryBot.create(:max_identities_account_setting, account: @account, value: 2)
          # RAILS5UPGRADE: Try removing if making property_sets
          # bi-directional is successful
          @account.settings.reload
          User.any_instance.stubs(:account).returns(@account)
          @permission_set = PermissionSet.create(account: @account, name: 'custom')
        end

        it 'should be invalid' do
          assert_equal 1, user.identities.size
          put :update, params: { id: user.id, user: {name: 'taylor', role: 'agent', identities: [{ type: "email", value: "test@user.com"}, { type: "email", value: "test2@user.com" }]} }
          assert_response :unprocessable_entity

          assert_equal "The number of identities for this user has exceeded the limit of 2. An identity can be an email address, a phone number, a Twitter account, or a Facebook account.", JSON.parse(response.body)['details']['base'].first['description']
          assert_equal 1, JSON.parse(response.body)['details']['base'].size
        end

        it 'should be valid' do
          put :update, params: { id: user.id, user: {name: 'taylor', role: 'agent', identities: [{ type: "email", value: "test3@user.com" }], custom_role_id: @permission_set.id} }
          assert_response 200
        end
      end

      describe "restricting bulk upload to risky accounts" do
        before do
          @user = users(:minimum_admin)
          login(@user)
          @account = @request.account
          @account.stubs(:trusted_bulk_uploader?).returns(false)
        end

        it "restricts if not trusted_bulk_uploader" do
          post_create_many(20)
          assert_response :forbidden
        end

        it "does not restrict if trusted_bulk_uploader" do
          @account.stubs(:trusted_bulk_uploader?).returns(true)
          post_create_many(20)
          assert_response 200
        end
      end

      describe "adding custom dropdowns" do
        before do
          # 3 taggers
          @custom_field = create_user_custom_field!("field_dropdown1", "title", "Dropdown")
          @custom_field.dropdown_choices.create(name: "test1", value: "first", position: 1)
          @custom_field.dropdown_choices.create(name: "test2", value: "second", position: 2)
          @custom_field = create_user_custom_field!("field_dropdown2", "title", "Dropdown")
          @custom_field.dropdown_choices.create(name: "prueba1", value: "primero", position: 1)
          @custom_field.dropdown_choices.create(name: "prueba2", value: "segundo", position: 2)
          @custom_field = create_user_custom_field!("field_dropdown3", "title", "Dropdown")
          @custom_field.dropdown_choices.create(name: "tesuto1", value: "hitotsu", position: 1)
          @custom_field.dropdown_choices.create(name: "tesuto2", value: "futatsu", position: 2)
        end

        describe "a POST to :create" do
          before do
            post :create, params: { user: {
              name: "Rodney", email: "the.rod@example.org", role: "end-user", tags: "moose",
              user_fields: {
                field_checkbox: true, field_checkbox2: true, field_text: "bar",
                field_dropdown1: "first", field_dropdown2: "segundo"
              }
            } }
          end

          it('responds with created') { assert_response :created }
          it "sets the custom fields on the user" do
            user = User.last
            assert_equal({"field_checkbox" => true, "field_checkbox2" => true, "field_text" => "bar",
                          "field_dropdown1" => "first", "field_dropdown2" => "segundo", "field_dropdown3" => nil}, user.custom_field_values.as_json(account: @account))
            assert_equal(["moose", "toast", "first", "segundo"].sort, user.tags.sort)
          end

          it "emit the correct custom_field_changed events" do
            custom_field_events = decode_user_events(domain_event_publisher.events, :custom_field_changed)
            assert_equal custom_field_events.size, 5
            custom_field_events.last.custom_field_changed.previous.must_be_nil
            assert custom_field_events.last.custom_field_changed.current.tagger_value, "segundo"
          end
        end

        describe "a PUT to :update" do
          before do
            @user.current_tags = "moose"
            @user.custom_field_values.update_from_hash(field_text: "foobar", field_checkbox2: true, field_dropdown1: "first", field_dropdown2: "primero", field_dropdown3: "hitotsu")
            @user.save!

            put :update, params: { id: @user.id, user: {user_fields: {field_checkbox: false, field_checkbox2: false, field_dropdown2: nil, field_dropdown3: "futatsu"} } } # update again
          end

          it('responds with success') { assert_response :success }
          it "updates the user custom fields correctly" do
            expected_fields = {
              "field_checkbox"  => false,
              "field_checkbox2" => false,
              "field_text"      => 'foobar',
              "field_dropdown1" => 'first',
              "field_dropdown2" => nil,
              "field_dropdown3" => 'futatsu'
            }
            expected_tags = ['first', 'futatsu', 'moose']
            json = JSON.parse(@response.body)['user']

            fields = json['user_fields']
            expected_fields.each do |key, val|
              assert_equal_with_nil(val, fields[key.to_s])
            end

            tags = json['tags']
            assert_equal(expected_tags, tags.sort)

            @user.reload
            assert_equal(expected_fields, @user.custom_field_values.as_json(account: @account))
            assert_equal(expected_tags, @user.tags.sort)
          end
        end

        describe "a PUT to :update with a bad value" do
          before do
            @user.current_tags = "moose"
            @user.custom_field_values.update_from_hash(field_text: "foobar", field_dropdown1: "first")
            @user.save!

            put :update, params: { id: @user.id, user: {user_fields: {field_dropdown1: "bad_value"} } } # update again
          end

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity } # 422 Unprocessable Entity (WebDAV; RFC 4918)
          it "updates the user custom fields correctly" do
            @user.reload
            assert_equal({"field_checkbox" => false, "field_checkbox2" => false, "field_text" => "foobar",
                          "field_dropdown1" => "first", "field_dropdown2" => nil, "field_dropdown3" => nil}, @user.custom_field_values.as_json(account: @account))
            assert_equal(["moose", "first"].sort, @user.tags.sort)
          end
        end

        describe "a PUT to :update with an even worse value" do
          before do
            @user.current_tags = "moose"
            @user.custom_field_values.update_from_hash(field_text: "foobar", field_dropdown1: "first")
            @user.save!

            put :update, params: { id: @user.id, user: {user_fields: [{field_dropdown1: "second"}] } }
          end

          it('responds with 400') { assert_response 400 }
          it "updates the user custom fields correctly" do
            @user.reload
            assert_equal({"field_checkbox" => false, "field_checkbox2" => false, "field_text" => "foobar",
                          "field_dropdown1" => "first", "field_dropdown2" => nil, "field_dropdown3" => nil}, @user.custom_field_values.as_json(account: @account))
            assert_equal(["moose", "first"].sort, @user.tags.sort)
          end
        end
      end
    end

    describe "when adding or editing tags" do
      before do
        @account = @user.account
        @account.settings.has_user_tags = true
        @account.save!
      end

      describe "updates end users tags" do
        before do
          put :update, params: { id: users(:minimum_end_user).id, user: { name: "some name", tags: "moose, goose"} }
        end

        it "emits correct user_tags_changed event" do
          user_tags_events = decode_user_events(domain_event_publisher.events, :tags_changed)
          assert_equal user_tags_events.size, 1
          assert_equal user_tags_events.first.tags_changed.tags_removed.tags, ["beta.test", "superuser"]
          assert_equal user_tags_events.first.tags_changed.tags_added.tags, ["moose", "goose"]
        end

        it "successfully update user's tags" do
          assert_equal ["goose", "moose"], users(:minimum_end_user).reload.tags.sort
        end
      end

      describe "updates own tags" do
        before do
          put :update, params: { id: @user.id, user: { name: "some name", tags: "moose, goose"} }
        end

        it "successfully update user's tags" do
          assert_equal ["goose", "moose"], @user.reload.tags.sort
        end

        it "emits correct user_tags_changed event" do
          user_tags_events = decode_user_events(domain_event_publisher.events, :tags_changed)
          assert_equal 1, user_tags_events.size
          assert_equal [], user_tags_events.first.tags_changed.tags_removed.tags
          assert_equal ["moose", "goose"], user_tags_events.first.tags_changed.tags_added.tags
        end
      end

      describe "a POST to :create" do
        before do
          post :create, params: { user: {
            name: "Argonauts", email: "argonauts+tags@example.org", role: "end-user", tags: "hero, super",
          } }
        end

        it "emits correct user_tags_changed event" do
          user_tags_events = decode_user_events(domain_event_publisher.events, :tags_changed)
          assert_equal 0, user_tags_events.size
        end
      end
    end

    describe "when update user alias" do
      describe "when updates user alias alone" do
        before { put :update, params: { id: users(:minimum_end_user).id, user: { alias: "Jame Bond"} } }

        it "emits correct user_alias_changed event" do
          user_alias_events = decode_user_events(domain_event_publisher.events, :user_alias_changed)
          assert_equal 1, user_alias_events.size
          assert_equal "Jame Bond", user_alias_events.first.user_alias_changed.current.value
          assert_equal "", user_alias_events.first.user_alias_changed.previous.value
        end
      end

      describe "when updates user alias with other fields" do
        before { put :update, params: { id: users(:minimum_end_user).id, user: { alias: "Jame Bond 1", name: "007", tags: "spy agent"} } }

        it "emits correct user_alias_changed event" do
          user_alias_events = decode_user_events(domain_event_publisher.events, :user_alias_changed)
          assert_equal 1, user_alias_events.size
          assert_equal "Jame Bond 1", user_alias_events.first.user_alias_changed.current.value
          assert_equal "", user_alias_events.first.user_alias_changed.previous.value
          assert_equal 3, decode_user_events(domain_event_publisher.events).size
        end
      end

      describe "when does not update alias field" do
        before { put :update, params: { id: users(:minimum_end_user).id, user: {name: "007", tags: "spy agent"} } }

        it "does not emit user_alias_changed event" do
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_alias_changed).size
        end
      end

      describe "a POST to :create" do
        before do
          post :create, params: { user: {
            name: "Argonauts", email: "argonauts+alias@example.org", role: "end-user", alias: "Mythical Team",
          } }
        end

        it "emit correct user_tags_changed event" do
          user_alias_events = decode_user_events(domain_event_publisher.events, :user_alias_changed)
          assert_equal user_alias_events.size, 0
        end
      end
    end

    describe "with user inferred domain events" do
      describe "a POST to :create" do
        before do
          @account.permission_sets.new(
              name:        I18n.t('txt.default.roles.staff.name'),
              description: I18n.t('txt.default.roles.staff.description'),
              role_type:   ::PermissionSet::Type::CUSTOM
            ).save

          post :create, params: { user: {
            name: "Argonauts",
            external_id: "external_id_1",
            email: "argonauts+inferred@example.org",
            role: "end-user",
            notes: "My notes",
            custom_role_id: @account.permission_sets.first.id
          } }
        end

        it "emits correct user domain events" do
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_name_changed).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :external_id_changed).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_notes_changed).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_role_changed).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :custom_role_changed).size
        end
      end

      describe "a PUT to :update" do
        before do
          @account.permission_sets.new(
              name:        I18n.t('txt.default.roles.staff.name'),
              description: I18n.t('txt.default.roles.staff.description'),
              role_type:   ::PermissionSet::Type::CUSTOM
            ).save
          put :update, params: { id: users(:minimum_end_user).id, user: {
            name: "Argonauts",
            external_id: "external_id_1",
            role: "agent",
            notes: "My notes",
            alias: "Mr. Zendesk",
            custom_role_id: @account.permission_sets.first.id
          } }
        end

        it "emits correct user domain events" do
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_name_changed).size
          assert_equal 1, decode_user_events(domain_event_publisher.events, :external_id_changed).size
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_notes_changed).size
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_alias_changed).size
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_role_changed).size
          assert_equal 1, decode_user_events(domain_event_publisher.events, :custom_role_changed).size
        end
      end
    end
  end

  describe "collaboration system user" do
    let(:user) { users(:minimum_end_user) }

    as_a_subsystem_user(user: 'collaboration', account: :minimum) do
      should_be_authorized { get :show, params: { id: user.id } }
      should_be_authorized { get :show_many, params: { ids: user.id } }
      should_be_authorized { get :search, params: { query: user.email } }

      describe "a POST to :create_or_update" do
        describe "when user does not exist" do
          before do
            post :create_or_update, params: { user: { name: "John Doe", email: "johndoe@example.com" } }
          end

          it('responds with created') { assert_response :created }
        end

        describe "when user already exists" do
          before do
            post :create_or_update, params: { user: { name: "John Doe", email: "johndoe@example.com" } }
          end

          it('responds with success') { assert_response :success }
        end
      end
    end
  end

  describe "metropolis system user" do
    as_a_subsystem_user(user: 'metropolis', account: :multiproduct) do
      should_be_authorized { put :update, params: { id: users(:multiproduct_contributor).id, user: { name: "Updated Staff name" } } }
      should_be_authorized { post :create, params: { user: { name: "Staff user", email: "newstaff@example.com" } } }
      should_be_authorized { post :create_or_update, params: { user: { name: "Staff user", email: "newstaff@example.com" } } }

      describe "a PUT to :update" do
        let(:user) { users(:multiproduct_contributor) }

        before do
          put :update, params: { id: user.id, user: { name: "Updated Staff name", role: "admin" } }
        end

        it('responds with success') { assert_response :success }

        it "emits user_name_changed domain events" do
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_name_changed).size
        end

        it "emits user_role_changed domain events" do
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_role_changed).size
        end

        it "emits custom_role_changed domain events" do
          assert_equal 1, decode_user_events(domain_event_publisher.events, :custom_role_changed).size
        end
      end

      describe "a POST to :create" do
        before do
          post :create, params: { user: { name: "Staff user", email: "newstaff@example.com" } }
        end

        it('responds with created') { assert_response :created }

        it "emits correct domain events" do
          assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_name_changed).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :user_role_changed).size
          assert_equal 0, decode_user_events(domain_event_publisher.events, :custom_role_changed).size
        end
      end

      describe "a POST to :create_or_update" do
        describe "when user does not exist" do
          before do
            post :create_or_update, params: { user: { name: "Staff user", email: "newstaff@example.com" } }
          end

          it('responds with created') { assert_response :created }

          it "emits correct domain events" do
            assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          end
        end

        describe "when user already exists" do
          before do
            post :create_or_update, params: { user: { name: "Staff user", email: "staff@example.com" } }
          end

          it('responds with success') { assert_response :success }

          it "emits correct domain events" do
            assert_equal 1, decode_user_events(domain_event_publisher.events, :user_created).size
          end

          it "not emits other domain events" do
            assert_equal 0, decode_user_events(domain_event_publisher.events, :user_name_changed).size
            assert_equal 0, decode_user_events(domain_event_publisher.events, :user_external_id_changed).size
            assert_equal 0, decode_user_events(domain_event_publisher.events, :user_notes_changed).size
            assert_equal 0, decode_user_events(domain_event_publisher.events, :user_time_zone_changed).size
            assert_equal 0, decode_user_events(domain_event_publisher.events, :user_only_private_comments_changed).size
            assert_equal 0, decode_user_events(domain_event_publisher.events, :user_tags_changed).size
            assert_equal 0, decode_user_events(domain_event_publisher.events, :user_details_changed).size
            assert_equal 0, decode_user_events(domain_event_publisher.events, :user_role_changed).size
            assert_equal 0, decode_user_events(domain_event_publisher.events, :custom_role_changed).size
          end

          it 'update to name and role should emit correct domain events' do
            post :create_or_update, params: { user: { name: "Update Staff user", email: "staff@example.com", role: "admin" } }
            assert_equal 1, decode_user_events(domain_event_publisher.events, :user_name_changed).size
            assert_equal 1, decode_user_events(domain_event_publisher.events, :user_role_changed).size
          end
        end
      end
    end
  end

  describe "embeddable system user" do
    as_a_subsystem_user(user: "embeddable", account: :multiproduct) do
      should_be_authorized { post :create_or_update, params: { user: { name: "Gaius Julius Caesar", email: "alea_iacta_est@example.com" } } }
      should_be_authorized { get :search, params: { query: "alea_iacta_est@example.com" } }

      describe "a POST to :create_or_update" do
        describe "when user does not exist" do
          before do
            post :create_or_update, params: { user: { name: "Cato the Elder", email: "delenda_est_carthago@example.com" } }
          end

          it("responds with created") { assert_response :created }
        end

        describe "when user already exists" do
          before do
            post :create_or_update, params: { user: { name: "Gaius Julius Caesar", email: "alea_iacta_est@example.com" } }
          end

          it("responds with success") { assert_response :success }
        end
      end

      describe "a GET to :search" do
        before do
          get :search, params: { query: "alea_iacta_est@example.com" }
        end

        it("responds with ok") { assert_response :ok }
      end
    end
  end

  describe "a zopim user" do
    as_a_subsystem_user(user: 'zopim', account: :minimum) do
      should_be_forbidden :search, :create, :update_many, :destroy_many, :search, :create_many, :create_or_update_many,
        [:get, :related, id: 1], :merge_self, :autocomplete

      should_be_authorized { get :index }
      should_be_authorized { get :show, params: { id: accounts(:minimum).users.first.id } }
      should_be_authorized { put :merge, params: { id: users(:minimum_end_user).id, user: { id: users(:minimum_author).id } } }
      should_be_authorized { put :update, params: { id: accounts(:minimum).users.last.id, user: { role: "admin"} } }

      describe "a GET to :index" do
        before { get :index }
        it('responds with success') { assert_response :success }
      end

      describe "a GET to :show" do
        before { get :show, params: { id: users(:minimum_end_user).id } }
        it('responds with success') { assert_response :success }
      end

      describe "a DELETE to :destroy" do
        before do
          User.any_instance.stubs(:delete!).returns(true)
          delete :destroy, params: { id: users(:minimum_end_user).id }
        end
        it('responds with success') { assert_response :success }
        should_be_authorized { delete :destroy, params: { id: users(:minimum_end_user).id } }
      end

      describe "a PUT to :merge" do
        before { put :merge, params: { id: users(:minimum_end_user).id, user: { id: users(:minimum_author).id } } }
        it('responds with success') { assert_response :success }
      end

      describe "a PUT to :update" do
        before { put :update, params: { id: accounts(:minimum).users.last.id, user: { role: "admin"} } }
        it('responds with success') { assert_response :success }
      end
    end
  end

  # TODO: Remove this account_fraud_service-related `describe` block as part of PA-1392
  describe "an account_fraud_service user" do
    as_a_subsystem_user(user: 'account_fraud_service', account: :minimum) do
      should_be_forbidden :search, :create, :update_many, :destroy_many, :search, :create_many, :create_or_update_many,
        [:get, :related, id: 1], :merge_self, :autocomplete, :show, :merge_self, :update

      should_be_authorized { get :index }

      describe "a GET to :index" do
        before { get :index }
        it('responds with success') { assert_response :success }
      end
    end
  end

  describe "an outbound user" do
    as_a_subsystem_user(user: 'outbound', account: :minimum) do
      should_be_forbidden :search, :create, :update_many, :destroy_many, :search, :create_many, :create_or_update_many,
        [:get, :related, id: 1], :merge_self, :autocomplete, :show, :merge_self, :update

      should_be_authorized { get :index }

      describe "a GET to :index" do
        before { get :index }
        it('responds with success') { assert_response :success }
      end
    end
  end

  describe "a knowledge_api user" do
    as_a_subsystem_user(user: 'knowledge_api', account: :minimum) do
      should_be_forbidden :index, :search, :create, :update_many, :destroy_many, :search, :create_many, :create_or_update_many,
        [:get, :related, id: 1], :merge_self, :autocomplete, :show, :merge_self, :update

      should_be_authorized { get :show_many }

      describe "a GET to :show_many" do
        before { get :show_many }
        it('responds with success') { assert_response :success }
      end
    end
  end

  describe "a profile_service user" do
    as_a_subsystem_user(user: 'profile_service', account: :multiproduct) do
      should_be_forbidden :index, :search, :update_many, :destroy_many, :search, :create_many, :create_or_update_many,
        [:get, :related, id: 1], :merge_self, :autocomplete, :merge_self

      should_be_authorized { get :show, params: { id: users(:multiproduct_contributor).id } }
      should_be_authorized { get :show_many }
      should_be_authorized { post :create, params: { user: { name: "Staff user", email: "newstaff@example.com" } } }
      should_be_authorized { put :update, params: { id: users(:multiproduct_contributor).id, user: { name: "Roger Wilco II" } } }

      describe "a GET to :show" do
        before { get :show, params: { id: users(:multiproduct_contributor).id } }
        it('responds with success') { assert_response :success }
      end

      describe "a GET to :show_many" do
        before { get :show_many }
        it('responds with success') { assert_response :success }
      end

      describe "a POST to :create" do
        before { post :create, params: { user: { name: "Staff user", email: "newstaff@example.com" } } }
        it('responds with created') { assert_response :created }
      end

      describe "a PUT to :update" do
        before { put :update, params: { id: users(:multiproduct_contributor).id, user: { name: "Roger Wilco II" } } }
        it('responds with success') { assert_response :success }
      end
    end
  end

  describe "a zdp_metadata user" do
    as_a_subsystem_user(user: 'zdp_metadata', account: :multiproduct) do
      should_be_forbidden :index, :search, :update_many, :destroy_many, :search, :create_many, :create_or_update_many,
        [:get, :related, id: 1], :merge_self, :autocomplete, :merge_self

      should_be_authorized { get :show, params: { id: users(:multiproduct_contributor).id } }
    end
  end

  # A bug in older versions of zendesk_api_client_rb sent the role as
  # a hash with either an id or a name with the right value.
  # It got fixed in v1.0.7 but older versions are still in the wild.
  # TODO: As of Nov 18, 2020, v1.0.7 is no longer seen in logs. This
  # should be removed, but needs more study due to the addition of the
  # case-insensitivity.
  describe 'corrects parameters from broken old zendesk_api_client_rb' do
    it 'when supplied as a hash with name' do
      params = { action: 'update', user: { role: { name: 'agent' } } }.with_indifferent_access

      @controller.params = ActionController::Parameters.new(params)
      @controller.send(:whitelist_parameters)

      assert_equal({'role' => 'agent'}, @controller.params['user'])
    end

    it 'when supplied as a hash with id' do
      params = { action: 'update', user: { role: { id: 'admin' } } }.with_indifferent_access

      @controller.params = ActionController::Parameters.new(params)
      @controller.send(:whitelist_parameters)

      assert_equal({'role' => 'admin'}, @controller.params['user'])
    end
  end

  as_an_agent do
    describe 'related data side load' do
      it 'does not include related data in index' do
        get :index, params: { include: 'related' }

        refute_includes JSON.parse(response.body).keys, 'related'
      end

      describe 'related data' do
        let(:user) { users(:minimum_end_user) }
        let(:full_payload) do
          {
            'assigned_tickets' => 0,
            'ccd_tickets' => 0,
            'followed_tickets' => 0,
            'entry_subscriptions' => 0,
            'forum_subscriptions' => 0,
            'organization_subscriptions' => 0,
            'requested_tickets' => 5,
            'subscriptions' => 0,
            'topics' => 8,
            'topic_comments' => 3,
            'votes' => 0
          }
        end
        let(:payload_without_followed_tickets) { full_payload.reject { |key| key == 'followed_tickets' } }

        before do
          Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: false)
          Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: false)
          Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: false)
        end

        describe 'with follower_and_email_cc_collaborations enabled' do
          before { Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: true) }

          describe 'with ticket_followers_allowed enabled' do
            before { Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true) }

            it 'includes followed ticket count' do
              get :show, params: { id: user.id, include: 'related' }

              related = JSON.parse(response.body)['related']
              assert_equal related, 'user_related' => full_payload
              assert_equal related['user_related']['followed_tickets'], 0
            end
          end

          describe 'with ticket_followers_allowed disabled' do
            it 'includes followed ticket count' do
              get :show, params: { id: user.id, include: 'related' }

              related = JSON.parse(response.body)['related']
              assert_equal related, 'user_related' => full_payload
              assert related['user_related'].keys.include? 'followed_tickets'
            end
          end

          describe 'with comment_email_ccs_allowed enabled' do
            before { Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true) }

            it 'includes CCed ticket count (as email CCs)' do
              get :show, params: { id: user.id, include: 'related' }

              related = JSON.parse(response.body)['related']
              assert_equal related, 'user_related' => full_payload
            end
          end

          describe 'with comment_email_ccs_allowed disabled' do
            it 'includes zero CCed ticket count' do
              get :show, params: { id: user.id, include: 'related' }

              related = JSON.parse(response.body)['related']
              assert_equal related, 'user_related' => full_payload
            end
          end
        end

        describe 'with follower_and_email_cc_collaborations disabled' do
          it 'does not include followed ticket count' do
            get :show, params: { id: user.id, include: 'related' }

            related = JSON.parse(response.body)['related']
            assert_equal related, 'user_related' => payload_without_followed_tickets
            refute related['user_related'].keys.include? 'followed_tickets'
          end

          it 'includes CCed ticket count (as collaborators)' do
            get :show, params: { id: user.id, include: 'related' }

            related = JSON.parse(response.body)['related']
            assert_equal related, 'user_related' => payload_without_followed_tickets
          end
        end
      end
    end
  end

  when_logged_in_as(:multiproduct_support_agent) do
    it_allows_access_for_all_multiproduct_product_types
  end

  describe "seats_remaining endpoint" do
    describe_with_arturo_disabled :allow_seats_remaining do
      let(:seats_remaining) do
        Account.any_instance.expects(:billable_agent_count).never
        @controller.expects(:staff_service_client).never
        get :seats_remaining
      end

      as_an_agent do
        it "responds with forbidden and does not execute expensive code paths" do
          seats_remaining
          assert_response :forbidden
        end
      end

      as_an_admin do
        it "responds with bad request and does not execute expensive code paths" do
          seats_remaining
          assert_response :bad_request
        end
      end
    end

    describe_with_arturo_enabled :allow_seats_remaining do
      let(:support_seats_remaining) do
        get :seats_remaining
        JSON.parse(@response.body)["seats_remaining"]["support"]
      end

      as_an_admin do
        describe "account is not multiproduct" do
          before :all do
            Account.any_instance.expects(:multiproduct).returns(false)
          end

          it "returns 0 seats_remaining when none available" do
            max_agent_count = @controller.send(:current_account).subscription.max_agents
            Account.any_instance.expects(:billable_agent_count).returns(max_agent_count)
            assert_equal 0, support_seats_remaining
          end

          it "returns 1 seats_remaining when 1 seat is available" do
            max_agent_count = @controller.send(:current_account).subscription.max_agents
            Account.any_instance.expects(:billable_agent_count).returns(max_agent_count - 1)
            assert_equal 1, support_seats_remaining
          end
        end

        describe "multiproduct account" do
          before :all do
            Account.any_instance.expects(:multiproduct).returns(true)
          end

          describe_with_arturo_enabled :staff_service_seats_remaining do
            it "returns 0 seats_remaining when none available" do
              Zendesk::StaffClient.any_instance.expects(:seats_remaining_support_or_suite!).returns(0)
              assert_equal 0, support_seats_remaining
            end
            it "returns 1 seats_remaining when 1 seat is available" do
              Zendesk::StaffClient.any_instance.expects(:seats_remaining_support_or_suite!).returns(1)
              assert_equal 1, support_seats_remaining
            end
          end

          describe_with_arturo_disabled :staff_service_seats_remaining do
            it "returns 0 seats_remaining when none available" do
              Zendesk::StaffClient.any_instance.expects(:seats_remaining_support_or_suite!).returns(0)
              assert_equal 0, support_seats_remaining
            end
            it "returns 1 seats_remaining when 1 seat is available" do
              Zendesk::StaffClient.any_instance.expects(:seats_remaining_support_or_suite!).returns(1)
              assert_equal 1, support_seats_remaining
            end
          end
        end

        describe "spp account" do
          before :all do
            Account.any_instance.expects(:spp?).returns(true)
          end

          describe_with_and_without_arturo_enabled :staff_service_seats_remaining do
            it "returns 0 seats_remaining when none available" do
              Zendesk::StaffClient.any_instance.expects(:seats_remaining_support_or_suite!).returns(0)
              assert_equal 0, support_seats_remaining
            end
            it "returns 1 seats_remaining when 1 seat is available" do
              Zendesk::StaffClient.any_instance.expects(:seats_remaining_support_or_suite!).returns(1)
              assert_equal 1, support_seats_remaining
            end
          end
        end
      end
    end
  end

  private

  def post_create_many(count)
    post :create_many, params: { users: Array.new(count) { |i| { "name" => "User #{i}", "email" => "user#{i}@foo.com" } } }
  end

  def post_create_or_update_many(count)
    post :create_or_update_many, params: { users: Array.new(count) { |i| { "name" => "User #{i}", "email" => "user#{i}@foo.com" } } }
  end
end
