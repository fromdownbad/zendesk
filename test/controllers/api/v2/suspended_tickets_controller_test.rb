require_relative "../../../support/test_helper"
require "zendesk/testing/factories/global_uid"
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

describe Api::V2::SuspendedTicketsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: "api/v2/suspended_tickets") do |request|
    request.should_route :get,    "/api/v2/suspended_tickets",                       action: "index"
    request.should_route :get,    "/api/v2/suspended_tickets/1",                     action: "show",         id: "1"
    request.should_route :put,    "/api/v2/suspended_tickets/1/recover",             action: "recover",      id: "1"
    request.should_route :delete, "/api/v2/suspended_tickets/1",                     action: "destroy",      id: "1"
    request.should_route :post,   "/api/v2/suspended_tickets/1/attachments",         action: "attachments",  id: "1"
    request.should_route :delete, "/api/v2/suspended_tickets/destroy_many",          action: "destroy_many"
    request.should_route :put,    "/api/v2/suspended_tickets/recover_many",          action: "recover_many"
    request.should_route :put,    "/api/v2/suspended_tickets/bulk_recover",          action: "bulk_recover"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :show, [:put, :recover, {id: 1}], [:post, :attachments, {id: 1}], :recover_many, :destroy, :destroy_many, :bulk_recover
  end

  as_an_end_user do
    should_be_forbidden :index, :show, [:put, :recover, {id: 1}], [:post, :attachments, {id: 1}], :recover_many, :destroy, :destroy_many, :bulk_recover
  end

  as_an_agent do
    describe "a GET to #index" do
      describe "without sorting" do
        before { get :index }
        should_use_presenter Api::V2::SuspendedTicketPresenter
      end

      describe "with sorting" do
        describe "sort_by = author" do
          before do
            get :index, params: { sort_by: "author" }
            @tickets = JSON.parse(@response.body)["suspended_tickets"]
          end

          it "sorts by author default" do
            ids = @tickets.map { |t| t["author"]["name"] }
            assert_equal ids.sort, ids
          end
        end

        describe "sort_by = cause" do
          before do
            get :index, params: { sort_by: "cause" }
            @tickets = JSON.parse(@response.body)["suspended_tickets"]
          end

          it "sorts by author default" do
            causes = @tickets.map { |t| t["cause"] }
            assert_equal causes.sort, causes
          end
        end

        describe "sort_by = RANDOMVALUE" do
          before do
            get :index, params: { sort_by: "RANDOMVALUE" }
          end

          it "does not sort by RANDOMVALUE" do
            assert_equal 'created_at ASC, id ASC', @controller.send(:sorting)
          end
        end

        describe "sort_order" do
          describe "when sort_by = author" do
            before do
              @call = lambda do |direction|
                get :index, params: { sort_by: "author", sort_order: direction }
                tickets = JSON.parse(@response.body)["suspended_tickets"]
                tickets.map { |t| t["author"]["name"] }
              end
            end

            it "sorts by author ASC" do
              ids = @call.call("ASC")
              assert_equal ids.sort, ids
            end

            it "sorts by author DESC" do
              ids = @call.call("DESC")
              assert_equal ids.sort.reverse, ids
            end
          end

          describe "when sort_by = cause" do
            before do
              @call = lambda do |direction|
                get :index, params: { sort_by: "cause", sort_order: direction }
                tickets = JSON.parse(@response.body)["suspended_tickets"]
                tickets.map { |t| t["cause"] }
              end
            end

            it "sorts by translated cause ASC" do
              causes = @call.call("ASC")
              assert_equal causes.sort, causes
            end

            it "sorts by translated cause DESC" do
              causes = @call.call("DESC")
              assert_equal causes.sort.reverse, causes
            end

            it "sorts by translated cause ASC when not specified" do
              causes = @call.call(nil)
              assert_equal causes.sort, causes
            end
          end

          describe "when sort_by = author.email" do
            before do
              @call = lambda do |direction|
                get :index, params: { sort_by: "author.email", sort_order: direction }
                tickets = JSON.parse(@response.body)["suspended_tickets"]
                tickets.map { |t| t["from_mail"] }
              end
            end

            it "sorts by from_mail ASC" do
              emails = @call.call("ASC")
              assert_equal emails.sort, emails
            end

            it "sorts by from_mail DESC" do
              emails = @call.call("DESC")
              assert_equal emails.sort.reverse, emails
            end

            it "sorts by from_mail ASC when not specified" do
              emails = @call.call(nil)
              assert_equal emails.sort, emails
            end
          end

          describe "when sort_by = author_email" do
            before do
              @call = lambda do |direction|
                get :index, params: { sort_by: "author_email", sort_order: direction }
                tickets = JSON.parse(@response.body)["suspended_tickets"]
                tickets.map { |t| t["from_mail"] }
              end
            end

            it "should sort by from_mail ASC" do
              emails = @call.call("ASC")
              assert_equal emails.sort, emails
            end

            it "should sort by from_mail DESC" do
              emails = @call.call("DESC")
              assert_equal emails.sort.reverse, emails
            end

            it "should sort by from_mail ASC when not specified" do
              emails = @call.call(nil)
              assert_equal emails.sort, emails
            end
          end
        end
      end
    end

    describe "a GET to #show" do
      before { get :show, params: { id: suspended_tickets(:minimum_spam).id } }
      should_use_presenter Api::V2::SuspendedTicketPresenter
    end

    describe "a PUT to #recover" do
      describe "with a valid suspended ticket" do
        before { put :recover, params: { id: suspended_tickets(:minimum_spam).id } }
        it('responds with success') { assert_response :success }

        it "returns id of suspended ticket" do
          parsed = JSON.parse(@response.body)
          assert parsed["ticket"]["id"]
        end
      end

      describe "with an author and a valid suspended ticket using the author id" do
        before do
          put :recover, params: { id: suspended_tickets(:minimum_spam).id, author: {id: users(:minimum_end_user).id } }
        end
        it('responds with success') { assert_response :success }

        it "sets author of new ticket by author id" do
          parsed = JSON.parse(@response.body)
          assert_equal users(:minimum_end_user).id, parsed["ticket"]["requester_id"]
        end
      end

      describe "with an author and a valid suspended ticket using the author email address" do
        before do
          @email = users(:minimum_end_user).email
          @name = users(:minimum_end_user).name
          put :recover, params: { id: suspended_tickets(:minimum_spam).id, author: { name: @name, email: @email } }
        end
        it('responds with success') { assert_response :success }

        it "sets author of new ticket by email address" do
          parsed = JSON.parse(@response.body)
          assert_equal users(:minimum_end_user).id, parsed["ticket"]["requester_id"]
        end
      end

      describe "with a valid suspended ticket, and feature enabled" do
        before do
          Arturo.enable_feature!(:preserve_recovered_ticket_owner)
          @suspended_ticket = suspended_tickets(:minimum_spam)
        end

        describe "without new author" do
          before do
            put :recover, params: { id: @suspended_ticket.id }
          end

          it('responds with success') { assert_response :success }

          it "recovers the ticket and preserves the author" do
            parsed = JSON.parse(@response.body)
            assert_equal @suspended_ticket.author.id, parsed["ticket"]["requester_id"]
          end
        end

        describe "with a bogus author email" do
          before do
            put :recover, params: { id: @suspended_ticket.id, author: { name: "wibble", email: "bogus@jogus.com" } }
          end

          it('responds with success') { assert_response :success }

          it "preserves the author" do
            parsed = JSON.parse(@response.body)
            assert_equal @suspended_ticket.author.id, parsed["ticket"]["requester_id"]
          end
        end

        describe "with a bogus author id" do
          before do
            put :recover, params: { id: @suspended_ticket.id, author: { id: 1234 } }
          end

          it('responds with success') { assert_response :success }

          it "preserves the author" do
            parsed = JSON.parse(@response.body)
            assert_equal @suspended_ticket.author.id, parsed["ticket"]["requester_id"]
          end
        end
      end

      describe "with a valid suspended ticket, and feature disabled" do
        before do
          Arturo.disable_feature!(:preserve_recovered_ticket_owner)
        end

        describe "without new author" do
          before do
            put :recover, params: { id: suspended_tickets(:minimum_spam).id }
          end

          it('responds with success') { assert_response :success }

          it "recovers the ticket and sets author as current_user" do
            parsed = JSON.parse(@response.body)
            assert_equal users(:minimum_agent).id, parsed["ticket"]["requester_id"]
          end
        end

        describe "with a bogus author email" do
          before do
            put :recover, params: { id: suspended_tickets(:minimum_spam).id, author: { name: "wibble", email: "bogus@jogus.com" } }
          end

          it('responds with success') { assert_response :success }

          it "sets author as current_user" do
            parsed = JSON.parse(@response.body)
            assert_equal users(:minimum_agent).id, parsed["ticket"]["requester_id"]
          end
        end

        describe "with a bogus author id" do
          before do
            put :recover, params: { id: suspended_tickets(:minimum_spam).id, author: { id: 1234 } }
          end

          it('responds with success') { assert_response :success }

          it "sets author as current_user" do
            parsed = JSON.parse(@response.body)
            assert_equal users(:minimum_agent).id, parsed["ticket"]["requester_id"]
          end
        end
      end

      describe "with an unrecoverable suspended ticket" do
        before do
          SuspendedTicket.any_instance.expects(:recover).returns(nil)
          put :recover, params: { id: suspended_tickets(:minimum_spam).id }
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end

      describe "with an author and a from_mail suspended ticket" do
        before do
          @email = users(:minimum_end_user).email
          @name = users(:minimum_end_user).name
          @author = { name: @name, email: @email }
        end

        it "recovers the ticket as the author" do
          put :recover, params: { id: suspended_tickets(:minimum_loop), author: @author }
          parsed = JSON.parse(@response.body)
          assert_response :success
          assert_equal users(:minimum_end_user).id, parsed["ticket"]["requester_id"]
        end

        it "recovers the ticket as the author" do
          put :recover, params: { id: suspended_tickets(:minimum_unknown_author), author: @author }
          parsed = JSON.parse(@response.body)
          assert_response :success
          assert_equal users(:minimum_end_user).id, parsed["ticket"]["requester_id"]
        end
      end
    end

    describe "on a POST to #attachments" do
      describe "with a valid suspended ticket" do
        before { post :attachments, params: { id: suspended_tickets(:minimum_spam) } }
        it('responds with success') { assert_response :success }

        it "returns an array with token and ticket attachment" do
          parsed = JSON.parse(@response.body)
          assert parsed["upload"]["token"]
          assert parsed["upload"]["attachments"]
        end
      end
    end

    describe "on a PUT to #bulk_recover" do
      describe "with many ids" do
        before do
          @ids = [suspended_tickets(:minimum_spam), suspended_tickets(:minimum_unverified)]
          put :bulk_recover, params: { ids: @ids.map(&:id).join(','), author: { id: users(:minimum_agent).id, email: users(:minimum_agent).email } }
        end
        it('responds with success') { assert_response :success }
        should_use_presenter Api::V2::JobStatusPresenter
      end

      describe "with nil params" do
        before do
          put :bulk_recover, params: { ids: nil, author: { id: nil, email: nil } }
        end
        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "with invalid params" do
        before do
          put :bulk_recover, params: { ids: 'not_ids', author: { id: nil, email: nil } }
        end
        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      end

      describe "with an unrecoverable suspended ticket" do
        before do
          @ids = [suspended_tickets(:minimum_loop)]
          put :bulk_recover, params: { ids: @ids.map(&:id).join(','), author: { id: users(:minimum_agent).id, email: users(:minimum_agent).email } }
        end
        it "respond with failure on suspeded ticket recovery" do
          parsed = JSON.parse(@response.body)
          assert parsed.to_s.include? "Failed recovering suspended ticket id:"
        end
      end
    end

    describe "on a PUT to #recover_many" do
      let(:suspended_ticket) { [suspended_tickets(:minimum_spam), suspended_tickets(:minimum_unverified)] }

      it "returns the ids of all the tickets recovered" do
        put :recover_many, params: { ids: suspended_ticket.map(&:id).join(',') }

        assert_response :success
        parsed = JSON.parse(response.body)
        assert_equal 2, parsed["tickets"].size
        assert parsed["tickets"].first["id"]
      end

      it "recovers tickets with ticket_id" do
        suspended_ticket.each { |st| st.update_column(:ticket_id, tickets(:minimum_1).nice_id) }
        put :recover_many, params: { ids: suspended_ticket.map(&:id).join(',') }
        assert_response :success
      end

      describe "without many ids" do
        before { put :recover_many }

        it('responds with success') { assert_response :success }
      end

      describe "with an unrecoverable suspended ticket" do
        before do
          SuspendedTicket.any_instance.expects(:recover).twice.returns(nil)
          @tickets = [suspended_tickets(:minimum_spam), suspended_tickets(:minimum_unverified)]
          put :recover_many, params: { ids: @tickets.map(&:id).join(',') }
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }

        it "returns failed tickets in json" do
          parsed = JSON.parse(@response.body)
          assert parsed["suspended_tickets"].any?
        end
      end

      describe "with suspended requester, new ticket" do
        before do
          suspended_ticket = suspended_tickets(:minimum_spam)
          suspended_ticket.update_attribute(:ticket_id, nil)
          suspended_ticket.author.suspend!
          suspended_ticket.author.save!

          @tickets = [suspended_ticket]
          put :recover_many, params: { ids: @tickets.map(&:id).join(',') }
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }

        it "returns unrecovered tickets in json with error" do
          parsed = JSON.parse(@response.body)
          assert_equal ["Requester:  Author Minimum is suspended."], parsed["suspended_tickets"][0]["error_messages"]
        end
      end

      describe "with suspended requester, existing ticket" do
        before do
          suspended_ticket = suspended_tickets(:minimum_spam)
          suspended_ticket.update_attribute(:ticket_id, tickets(:minimum_1).nice_id)
          suspended_ticket.author.suspend!
          suspended_ticket.author.save!

          @tickets = [suspended_ticket]
          put :recover_many, params: { ids: @tickets.map(&:id).join(',') }
        end

        it('responds with unprocessable_entity') { assert_response :unprocessable_entity }

        it "returns unrecovered tickets in json with error" do
          parsed = JSON.parse(@response.body)
          assert_equal ["Requester:  Author Minimum is suspended."], parsed["suspended_tickets"][0]["error_messages"]
        end
      end
    end

    describe "on a DELETE to #destroy_many" do
      before do
        @tickets = [suspended_tickets(:minimum_spam), suspended_tickets(:minimum_unverified)]
      end

      it "responds with 204" do
        delete :destroy_many, params: { ids: @tickets.map(&:id).join(',') }
        assert_response :no_content
      end

      it 'soft-deletes the tickets' do
        delete :destroy_many, params: { ids: @tickets.map(&:id).join(',') }
        @tickets.each do |ticket|
          assert ticket.reload.deleted?
        end
      end
    end

    describe "a DELETE to #destroy" do
      it "responds with 204" do
        delete :destroy, params: { id: suspended_tickets(:minimum_spam).id.to_s }
        assert_response :no_content
      end

      it "soft-deletes the ticket" do
        delete :destroy, params: { id: suspended_tickets(:minimum_spam).id.to_s }
        assert suspended_tickets(:minimum_spam).reload.deleted?
      end
    end

    describe 'without specific pagination parameters' do
      let(:account) { accounts(:minimum) }
      let(:json) do
        get :index
        JSON.parse(@response.body).with_indifferent_access
      end

      it 'uses offset based pagination by default' do
        assert json.key?(:next_page)
        assert json.key?(:previous_page)
      end
    end

    describe 'using offset based pagination' do
      let(:account) { accounts(:minimum) }
      let(:json) do
        get :index, params: { page: 2, per_page: 1 }
        JSON.parse(@response.body).with_indifferent_access
      end

      it 'presents the correct keys' do
        assert json[:previous_page].present?
        assert json[:next_page].present?
      end
    end

    # For a complete set of tests, go to `zendesk_cursor_pagination` gem.
    describe 'using cursor based pagination' do
      let(:account) { accounts(:minimum) }
      let(:page_size) { 2 }
      let(:page_params) { {size: page_size} }
      let(:json) do
        get :index, params: { page: page_params }
        JSON.parse(@response.body).with_indifferent_access
      end

      it 'generates the correct order clause' do
        assert_sql_queries(1, /ORDER BY `suspended_tickets`.`id`/) do
          json
        end
      end

      describe 'when there is only one page' do
        let(:page_size) { account.suspended_tickets.size + 1 }

        it 'presents the correct keys' do
          refute json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of suspended tickets' do
          assert_equal account.suspended_tickets.size, json[:suspended_tickets].size
        end
      end

      describe 'when there are multiple pages' do
        let(:page_size) { account.suspended_tickets.size - 1 }

        it 'presents the correct keys' do
          assert json[:meta][:has_more]
          assert json[:meta][:before_cursor].present?
          assert json[:meta][:after_cursor].present?
          assert json[:links][:prev].present?
          assert json[:links][:next].present?
        end

        it 'presents the correct number of suspended tickets' do
          assert_equal page_size, json[:suspended_tickets].size
        end
      end

      describe 'with a valid after cursor' do
        let(:page_size) { account.suspended_tickets.size - 1 }
        let(:after_json) { JSON.parse(@response.body).with_indifferent_access }

        before do
          after_cursor = json[:meta][:after_cursor]
          # reset memoized suspended_tickets
          @controller.remove_instance_variable(:@suspended_tickets)

          get :index, params: { page: {
            size: page_size,
            after: after_cursor
          } }
        end

        it 'presents the correct keys' do
          refute after_json[:meta][:has_more]
          assert after_json[:meta][:before_cursor].present?
          assert after_json[:meta][:after_cursor].present?
          assert after_json[:links][:prev].present?
          assert after_json[:links][:next].present?
        end

        it 'presents the correct number of suspended tickets' do
          assert_equal 1, after_json[:suspended_tickets].size
        end
      end

      describe 'with an invalid after cursor' do
        let(:page_params) do
          {
              size: page_size,
              after: 'invalid'
          }
        end

        it 'presents an error message' do
          assert_equal 'InvalidPaginationParameter', json[:error]
          assert_equal 'page[after] is not valid', json[:description]
          assert_response :bad_request
        end
      end

      describe_with_arturo_disabled :email_cursor_pagination_suspended_tickets_index do
        it 'presents the same error message we were showing before adding the types' do
          assert_equal 'Invalid attribute', json[:error][:title]
          assert_equal 'You passed an invalid value for the page attribute. Invalid parameter: page must be an integer', json[:error][:message]
          assert_response :bad_request
        end
      end
    end
  end
end
