require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"
require_relative "../../../support/agent_test_helper"

SingleCov.covered! uncovered: 35

# Some tests have been moved to the /requests folder to speed up test runs
describe Api::V2::RequestsController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  include AgentTestHelper

  fixtures :accounts, :custom_field_options, :organizations, :tickets, :ticket_fields, :users,
    :role_settings, :organization_memberships, :sequences, :recipient_addresses, :translation_locales

  let(:account) { accounts(:minimum) }
  let(:statsd_client) { @controller.send(:statsd_client) }

  before do
    Rails.logger.stubs(:info)
    accept :json
  end

  with_options(controller: "api/v2/requests") do |request|
    request.should_route :get,    "/api/v2/requests",         action: :index
    request.should_route :get,    "/api/v2/requests/open",    action: :open
    request.should_route :get,    "/api/v2/requests/solved",  action: :solved
    request.should_route :get,    "/api/v2/users/1/requests", action: :index, user_id: 1
    request.should_route :get,    "/api/v2/requests/1",       action: :show, id: 1
    request.should_route :post,   "/api/v2/requests",         action: :create
    request.should_route :put,    "/api/v2/requests/1",       action: :update, id: 1
    request.should_route :get,    "/api/v2/organizations/1/requests", action: :index, organization_id: 1
  end

  describe "when using oauth tokens" do
    let(:ticket) { tickets(:minimum_2) }
    let(:new_comment) { {comment: {body: "New comment!"}} }
    let(:valid_params) do
      {
        subject: "Help!",
        comment: {value: "I need somebody!"}
      }
    end

    with_scopes('requests:read', 'read') do
      should_be_authorized { get :index }
      should_be_authorized { get :show, params: { id: ticket.nice_id } }
      should_be_authorized { get :open }
      should_be_authorized { get :solved }
      should_not_be_authorized { post :create, params: { request: valid_params } }
      should_not_be_authorized { put :update, params: { id: ticket.nice_id, request: new_comment } }
    end

    with_scopes('requests:write', 'write') do
      should_be_authorized { post :create, params: { request: valid_params } }
      should_be_authorized { put :update, params: { id: ticket.nice_id, request: new_comment } }

      should_not_be_authorized { get :index }
      should_not_be_authorized { get :show, params: { id: ticket.nice_id } }
    end

    with_scopes('read', 'write') do
      should_be_authorized { get :open }
      should_be_authorized { get :solved }
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :show, :update, :open, :solved

    describe "a POST to #create" do
      let(:signup_required) { false }

      before do
        @account.stubs(is_signup_required?: signup_required)
      end

      describe "parameters" do
        let(:params) { { request: { subject: "Help!", comment: { body: "test" }, requester: { email: "test@example.com" } } } }

        it "is valid" do
          post :create, params: params
          assert_response 201
        end

        describe "without comment param" do
          before do
            params[:request].delete :comment
            post :create, params: params
          end

          it "responds with 422" do
            assert_response :unprocessable_entity
          end
        end

        describe "without request param" do
          before do
            params.delete :request
            post :create, params: params
          end

          it "responds with 422" do
            assert_response :unprocessable_entity
          end
        end

        describe "requester locale" do
          let(:params) { { request: { subject: "Help!", comment: { body: "test" }, requester: requester_params } } }
          let(:invalid_locale_id) { TranslationLocale.maximum(:id) + 10 }
          let(:japanese) { translation_locales(:japanese) }
          let(:spanish) { translation_locales(:spanish) }

          before do
            Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
            Account.any_instance.stubs(:available_languages).returns([japanese, spanish, ENGLISH_BY_ZENDESK])
          end

          describe "request from new user" do
            let(:user_locale) { UserEmailIdentity.find_by(value: "test-new-user@zendesk.com").user.locale }
            let(:user_identity) { UserEmailIdentity.find_by(value: "test-new-user@zendesk.com") }

            describe 'passing in valid `locale`' do
              let(:requester_params) { { email: "test-new-user@zendesk.com", locale: japanese.locale, name: "Brand New" } }

              it 'succeeds and updates requester locale' do
                refute user_identity

                post :create, params: params

                assert_response 201
                assert_equal japanese.locale, user_locale
              end
            end

            describe 'passing in invalid `locale`' do
              let(:requester_params) { { email: "test-new-user@zendesk.com", locale: 'invalid', name: "Brand New" } }

              it 'fails' do
                refute user_identity

                post :create, params: params

                assert_response 422
                assert_equal 'RecordInvalid', json['error']
              end
            end

            describe 'passing in valid `locale_id`' do
              let(:requester_params) { { email: "test-new-user@zendesk.com", locale_id: japanese.id, name: "Brand New" } }

              it 'succeeds and updates requester locale' do
                refute user_identity

                post :create, params: params

                assert_response 201
                assert_equal japanese.locale, user_locale
              end
            end

            describe 'passing in invalid `locale_id`' do
              let(:requester_params) { { email: "test-new-user@zendesk.com", locale_id: invalid_locale_id, name: "Brand New" } }

              it 'fails' do
                refute user_identity

                post :create, params: params

                assert_response 422
                assert_equal 'RecordInvalid', json['error']
              end
            end

            describe 'passing in both `locale` and `locale_id`' do
              let(:requester_params) { { email: "test-new-user@zendesk.com", locale_id: japanese.id, locale: spanish.locale, name: "Brand New" } }

              it 'succeeds and update requester locale' do
                refute user_identity

                post :create, params: params

                assert_response 201
                assert_equal spanish.locale, user_locale
              end
            end
          end

          describe "request from existent end user" do
            let(:user) { users(:minimum_end_user) }

            describe 'passing in `locale`' do
              let(:requester_params) { { email: user.email, locale: japanese.locale } }

              it 'succeeds and ignore locale' do
                assert_equal ENGLISH_BY_ZENDESK.locale, user.locale

                post :create, params: params

                assert_response 201
                assert_equal ENGLISH_BY_ZENDESK.locale, user.locale
              end
            end

            describe 'passing in `locale_id`' do
              let(:requester_params) { { email: user.email, locale_id: japanese.id } }

              it 'succeeds and ignores locale' do
                assert_equal ENGLISH_BY_ZENDESK.locale, user.locale

                post :create, params: params

                assert_response 201
                assert_equal ENGLISH_BY_ZENDESK.locale, user.locale
              end
            end
          end
        end
      end

      describe "with account_is_open set to true" do
        before do
          @controller.send(:current_account).update_attribute('is_open', true)
        end

        describe "with is_signup_required? equals to true" do
          let(:signup_required) { true }

          before do
            post :create, params: { request: { subject: "Help!", comment: { body: "I need somebody!" }, requester: { name: 'Peter', email: 'peter@zendesk.com' } } }
          end

          should_use_presenter Api::V2::SuspendedTicketPresenter
        end

        describe "with is_signup_required? equals to false" do
          before do
            Comment.any_instance.stubs(:validate_all_attachment_tokens).returns(true)
            post :create, params: { request: { subject: "Help!", comment: { body: "I need somebody!", uploads: "deadbeef" }, requester: { name: 'Peter' } } }
          end

          should_use_presenter Api::V2::RequestPresenter
        end

        it "returns an error when passed too many attachments" do
          post :create, params: { request: { subject: "Help!", comment: { body: "I need somebody!", uploads: ["deadbeef", "5ca1ab1e", "b01dface", "f01dab1e", "ba5eba11", "f005ba11"] }, requester: { name: 'Peter' } } }
          assert_equal "{\"error\":\"Anonymous uploads are limited to 5 files per request\"}", @response.body
        end

        it "doesn't return the 'Anonymous uploads are limited' error when the account has a bigger limit" do
          account.settings.anonymous_attachment_limit = 10
          @controller.send(:current_account).stubs(settings: account.settings)
          Comment.any_instance.stubs(:validate_all_attachment_tokens).returns(true)

          post :create, params: { request: { subject: "Help!", comment: { body: "I need somebody!", uploads: ["deadbeef", "5ca1ab1e", "b01dface", "f01dab1e", "ba5eba11", "f005ba11"] }, requester: { name: 'Peter' } } }
          assert_response :created
        end

        it "creates a ticket" do
          post :create, params: { request: { subject: "Help!", comment: { body: "I need somebody!" }, requester: { name: 'Peter' } } }
          assert_response :created
        end

        it "responds with a 422 when the comment is missing" do
          post :create, params: { request: { subject: "Help!", requester: { name: 'Peter' } } }

          assert_response :unprocessable_entity
        end

        it "should present the correct CORS header" do
          post :create, params: { request: { subject: "Help!", comment: { body: "I need somebody!" }, requester: { name: 'Peter' } } }
          assert_equal "*", response.headers["Access-Control-Allow-Origin"]
        end

        it "should assign tags to a new ticket" do
          ["foo,bar", ["foo", "bar"]].each do |tag_params|
            post :create, params: { request: { subject: "Help!", comment: { body: "I need somebody!" }, requester: { name: 'Peter' }, tags: tag_params } }
            request = JSON.parse(@response.body)['request']

            ticket = account.tickets.find_by_nice_id(request['id'])
            assert_equal "bar foo", ticket.current_tags
          end
        end

        describe_with_arturo_enabled :orca_classic_restrict_anon_requests_endpoint do
          describe_with_arturo_enabled :orca_classic_restrict_anon_requests_endpoint_log_only do
            before do
              Rails.logger.expects(:info).with("Anonymous_request_denied")
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: { name: 'Peter' }, via_id: ViaType.WEB_FORM } }
            end
            it "sends the request successfully" do
              assert_response :created
            end
          end

          describe_with_arturo_disabled :orca_classic_restrict_anon_requests_endpoint_log_only do
            before do
              Rails.logger.expects(:info).with("Anonymous_request_denied")
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: { name: 'Peter' }, via_id: ViaType.WEB_FORM } }
            end

            it "blocks ticket creation" do
              assert_response :unauthorized
            end
          end
        end
      end

      describe "with account_is_open set to false" do
        before { @controller.send(:current_account).update_attribute('is_open', false) }

        it "blocks ticket creation" do
          post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: { name: 'Peter' } } }
          assert_response :unauthorized
        end
      end

      def self.it_responds_with_legit_response_but_doesnt_create_ticket
        it "responds with a legit response, but *doesn't* create the ticket" do
          request = JSON.parse(@response.body)['request']
          assert_nil account.tickets.find_by_nice_id(request['id'])
        end
      end

      def self.it_responds_with_forbidden
        it "responds with :forbidden" do
          assert_response :forbidden
        end
      end

      describe "when making multiple requests" do
        describe "via any general API client" do
          before do
            Timecop.freeze do
              5.times.each do
                post :create, params: { request: { subject: "Help!", comment: { body: "I need somebody!" }, requester: { name: 'Peter' } } }
                assert_response :created
              end

              post :create, params: { request: { subject: "Help!", comment: { body: "I need somebody!" }, requester: { name: 'Peter' } } }
            end
          end

          it "is rate limited correctly" do
            assert_response 429
          end
        end

        describe "via the web widget" do
          before do
            Timecop.freeze(40.seconds.from_now) do
              5.times.each do
                post :create, params: { request: { subject: "Help!", comment: { body: "I need somebody!" }, requester: { name: 'Peter' }, via_id: ViaType.WEB_WIDGET } }
                assert_response :created
              end

              post :create, params: { request: { subject: "Help!", comment: { body: "I need somebody!" }, requester: { name: 'Peter' }, via_id: ViaType.WEB_WIDGET } }
            end
          end

          it "is rate limited correctly" do
            assert_response 429
          end
        end
      end

      describe "with a suspended user as requester" do
        before do
          @suspended_user = users(:minimum_end_user)
          @suspended_user.suspend
        end

        it "should return success created and make a suspended ticket" do
          assert_difference('SuspendedTicket.count(:all)', 1) do
            post :create, params: { request: { subject: "Help!", requester: { name: @suspended_user.name, email: @suspended_user.email }, comment: { value: "I need somebody!" } } }
          end
          assert_response :success
        end
      end

      describe 'when a ticket is spammy' do
        let(:spammy_ticket_params) { { request: { subject: "Help!", comment: { body: "longhu007.com spam" }, via_id: ViaType.WEB_WIDGET, requester: { email: "test@example.com", name: "Peter" } } } }

        before do
          Arturo.enable_feature! :spam_detector_inbound_spam
          Fraud::SpamDetector.any_instance.stubs(:suspend_ticket?).returns(true)
        end

        it "blocks the ticket in the spam detector" do
          post :create, params: spammy_ticket_params

          # "{ success: true }" is the "fake" response we return to spammers when we block their tickets
          # /app/controllers/api/v2/requests_controller.rb:72
          assert_equal response.body, "{\"success\":true}"
        end

        describe_with_arturo_enabled :orca_classic_rate_limit_master do
          describe_with_arturo_enabled :orca_classic_rate_limit_request_spam_challenge do
            it "populates the response header with the strong (challenge) header -- ZENDESK-EP = 5" do
              post :create, params: spammy_ticket_params

              assert_equal response.headers["ZENDESK-EP"], 5
            end
          end

          describe_with_arturo_disabled :orca_classic_rate_limit_request_spam_challenge do
            it "populates the response header with the simulate strong (challenge) header -- ZENDESK-EP = -5" do
              post :create, params: spammy_ticket_params

              assert_equal response.headers["ZENDESK-EP"], -5
            end
          end
        end

        describe_with_arturo_disabled :orca_classic_rate_limit_master do
          describe_with_arturo_enabled :orca_classic_rate_limit_request_spam_challenge do
            it "does not populates the rate limit response header" do
              post :create, params: spammy_ticket_params

              refute response.headers["ZENDESK-EP"]
            end
          end
        end
      end
    end
  end

  as_an_end_user do
    describe "with verified email identities" do
      let(:ticket) { tickets(:minimum_2) }

      describe "on a GET to #index" do
        describe "with a user" do
          before { get :index, params: { user_id: ticket.requester_id } }

          it('responds with ok') { assert_response :ok }
          should_use_presenter Api::V2::RequestPresenter

          it "should increment statsd with verified.email" do
            statsd_client.expects(:increment).with('verified.email')

            get :index, params: { user_id: ticket.requester_id }
          end
        end

        describe "with a user different than the logged in one" do
          before { get :index, params: { user_id: users(:minimum_search_user).id } }

          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "with a valid status type filter" do
          it 'uses the supplied status type filter' do
            selected_requests = [stub, stub]
            @controller.expects(:tickets).with('new', 'open').returns(selected_requests)
            Api::V2::RequestPresenter.any_instance.expects(:present).with(selected_requests)

            get :index, params: { status: 'new,open' }
          end
        end

        describe "with sort_order" do
          let!(:older_ticket) { tickets(:minimum_2) }
          let!(:newer_ticket) { tickets(:minimum_3) }

          before do
            older_ticket.update_column(:created_at, Time.now - 400.days)
            newer_ticket.update_column(:created_at, Time.now)
          end

          it 'orders by created_at asc' do
            get :index, params: { sort_order: 'asc', sort_by: 'created_at' }

            requests = JSON.parse(@response.body)['requests']

            assert_equal older_ticket.nice_id, requests.first["id"]
            assert_equal newer_ticket.nice_id, requests.last["id"]
          end

          it 'orders by created_at desc' do
            get :index, params: { sort_order: 'desc', sort_by: 'created_at' }

            requests = JSON.parse(@response.body)['requests']

            assert_equal newer_ticket.nice_id, requests.first["id"]
            assert_equal older_ticket.nice_id, requests.last["id"]
          end
        end

        describe "with an invalid status type filter" do
          it 'quietly reject invalid status types' do
            selected_requests = [stub, stub]
            @controller.expects(:tickets).with('new', 'open').returns(selected_requests)
            Api::V2::RequestPresenter.any_instance.expects(:present).with(selected_requests)

            get :index, params: { status: 'new,foobar,open' }
          end
        end

        describe "without extra parameters" do
          before { get :index }

          should_use_presenter Api::V2::RequestPresenter

          it "uses object pagination by default" do
            assert_response :ok
            requests_keys = JSON.parse(response.body).keys
            assert_equal 2, (requests_keys & ['next_page', 'previous_page']).size
          end
        end

        describe 'when cursor_pagination_v2_requests_endpoint arturo is disabled and after param is passed in' do
          before do
            Account.any_instance.stubs(:has_cursor_pagination_v2_requests_endpoint?).returns(false)
          end

          it "returns 400 bad request" do
            get :index, params: { page: { after: 'xxx' } }
            json = JSON.parse(@response.body).with_indifferent_access
            assert_response :bad_request
            assert_equal 'Invalid attribute', json[:error][:title]
            assert_includes json[:error][:message], 'You passed an invalid value for the page attribute. Invalid parameter: page must be an integer'
          end
        end

        describe 'cursor pagination v2' do
          let(:page_size) { @user.tickets.size }
          let(:page_params) { { size: page_size } }
          let(:sort) { nil }
          let(:params) { { page: page_params, sort: sort } }
          let(:json) do
            get :index, params: params
            JSON.parse(@response.body).with_indifferent_access
          end

          describe 'with a valid status type filter' do
            let(:params) { { page: page_params, sort: sort, status: 'pending' } }

            it 'returns the tickets matching the filter' do
              assert_equal 1, json[:requests].size
            end
          end

          describe "when there are private tickets" do
            let(:ticket) do
              ticket_initializer = Zendesk::Tickets::Initializer.new(account, users(:minimum_agent), requester_id: @user.id, ticket: { submitter: users(:minimum_agent), subject: "Private Wombats", comment: { public: false, value: "private wombat ticket about end user" } })
              private_ticket = ticket_initializer.ticket
              private_ticket.save!
              private_ticket
            end

            before do
              assert_equal false, ticket.is_public
            end

            it 'should not include the private tickets' do
              requests = json['requests']
              refute requests.map { |r| r["id"] }.include?(ticket.nice_id)
            end
          end

          describe 'branded tickets' do
            let(:brand) { FactoryBot.create(:brand, account_id: account.id) }
            let(:second_brand) { FactoryBot.create(:brand, account_id: account.id) }
            let(:branded_ticket) { @user.tickets.working.first }

            before do
              @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
              @controller.stubs(:current_brand).returns(brand)
              branded_ticket.will_be_saved_by(@user)
              branded_ticket.update_attribute(:brand, brand)
              branded_ticket.update_column(:is_public, true)
            end

            describe "on a GET to #index" do
              before do
                assert @user.tickets.working.size > 1
              end

              it "gets a list of branded tickets" do
                requests = json['requests']

                assert_equal 1, requests.size
                assert_equal branded_ticket.nice_id, requests.first["id"]
              end
            end
          end

          describe 'when there is only one page' do
            let(:page_size) { @user.tickets.size + 1 }

            it 'presents the correct keys' do
              refute json[:meta][:has_more]
              assert json[:meta][:before_cursor].present?
              assert json[:meta][:after_cursor].present?
              assert json[:links][:prev].present?
              assert json[:links][:next].present?
            end

            it 'presents the correct number of requests' do
              assert_equal @user.tickets.size, json[:requests].size
            end
          end

          describe 'when there is more than one page' do
            let(:page_size) { @user.tickets.size - 1 }

            it 'presents the correct keys' do
              assert json[:meta][:has_more]
              assert json[:meta][:before_cursor].present?
              assert json[:meta][:after_cursor].present?
              assert json[:links][:prev].present?
              assert json[:links][:next].present?
            end

            it 'presents the correct number of requests' do
              assert_equal page_size, json[:requests].size
            end
          end

          describe 'with a valid after cursor' do
            let(:page_size) { @user.tickets.size - 1 }
            let(:after_json) do
              JSON.parse(@response.body).with_indifferent_access
            end

            before do
              after_cursor = json[:meta][:after_cursor]

              # The same controller instance is reused within each test and this controller memoizes
              # tickets. To get to the second page of results we need to reach in and
              # clear the controller's @tickets variable. Outside of controller
              # tests we get a new instance of the controller on each request - this mimics
              # that behavior for only this variable.
              @controller.instance_variable_set(:@tickets, nil)

              get :index, params: { page: {
                size: page_size,
                after: after_cursor
              }}
            end

            it 'presents the correct keys' do
              get :index, params: { page: { after: 'xxx' } }
              refute after_json[:meta][:has_more]
              assert after_json[:meta][:before_cursor].present?
              assert after_json[:meta][:after_cursor].present?
              assert after_json[:links][:prev].present?
              assert after_json[:links][:next].present?
            end
            it 'presents the correct number of requests' do
              assert_equal 1, after_json[:requests].size
            end
          end

          describe 'with an invalid after cursor' do
            let(:page_params) do
              {
                size: page_size,
                after: 'invalid'
              }
            end

            it 'presents an error message' do
              assert_equal 'InvalidPaginationParameter', json[:error]
              assert_equal 'page[after] is not valid', json[:description]
              assert_response :bad_request
            end
          end

          describe 'when sort parameter is updated_at' do
            let(:sort) { 'updated_at' }

            it 'generates the correct order clause' do
              assert_sql_queries(1, /ORDER BY `tickets`.`updated_at` ASC, `tickets`.`id` ASC/) do
                json
              end
            end

            it 'uses the correct index' do
              assert_sql_queries(1, /#{Ticket::ACCOUNT_AND_STATUS_AND_UPDATED_INDEX}/) do
                json
              end
            end

            describe 'desc' do
              let(:sort) { '-updated_at' }

              it 'generates the correct order clause' do
                assert_sql_queries(1, /ORDER BY `tickets`.`updated_at` DESC, `tickets`.`id` DESC/) do
                  json
                end
              end

              it 'uses the correct index' do
                assert_sql_queries(1, /#{Ticket::ACCOUNT_AND_STATUS_AND_UPDATED_INDEX}/) do
                  json
                end
              end
            end
          end

          describe 'when sort parameter is -id' do
            let(:sort) { '-id' }

            it 'generates the correct order clause' do
              assert_sql_queries(1, /ORDER BY `tickets`.`nice_id` DESC/) do
                json
              end
            end

            it 'uses the correct index' do
              assert_sql_queries(1, /#{Ticket::ACCOUNT_AND_NICE_ID_INDEX}/) do
                json
              end
            end
          end

          describe 'when sort parameter has multiple fields' do
            let(:sort) { '-created_at,id' }

            it 'shows an error message' do
              json
              assert_response :bad_request
              assert @response.body =~ /sort is not valid/
            end
          end

          describe 'when sort parameter has invalid fields' do
            let(:sort) { '-invalid' }

            it 'shows an error message' do
              json
              assert_response :bad_request
              assert @response.body =~ /sort is not valid/
            end
          end
        end

        describe "when there are private tickets" do
          let(:ticket) do
            ticket_initializer = Zendesk::Tickets::Initializer.new(account, users(:minimum_agent), requester_id: users(:minimum_end_user).id, ticket: { submitter: users(:minimum_agent), subject: "Private Wombats", comment: { public: false, value: "private wombat ticket about end user" } })
            private_ticket = ticket_initializer.ticket
            private_ticket.save!
            private_ticket
          end

          before do
            assert_equal false, ticket.is_public
            get :index
          end

          it 'should not include the private tickets' do
            requests = JSON.parse(@response.body)['requests']
            refute requests.map { |r| r["id"] }.include?(ticket.nice_id)
          end
        end

        describe "with an organization" do
          describe "that the current user is a part of" do
            before { get :index, params: { organization_id: @user.organization_id } }

            should_use_presenter Api::V2::RequestPresenter
          end

          describe "that the current user isn't a part of" do
            before { get :index, params: { organization_id: organizations(:minimum_organization2) } }

            it('responds with forbidden') { assert_response :forbidden }
          end
        end
      end

      describe "on a GET to #open" do
        describe "without extra parameters" do
          before { get :open }
          should_use_presenter Api::V2::RequestPresenter

          it "uses object pagination" do
            assert_response :ok
            requests_keys = JSON.parse(response.body).keys
            assert_equal 2, (requests_keys & ['next_page', 'previous_page']).size
          end
        end

        describe 'using cursor pagination v2' do
          describe 'when cursor_pagination_v2_requests_endpoint arturo is disabled and after param is passed in' do
            before do
              Account.any_instance.stubs(:has_cursor_pagination_v2_requests_endpoint?).returns(false)
            end

            it "returns 400 bad request" do
              get :open, params: { page: { after: 'xxx' } }
              json = JSON.parse(@response.body).with_indifferent_access
              assert_response :bad_request
              assert_equal 'Invalid attribute', json[:error][:title]
              assert_includes json[:error][:message], 'You passed an invalid value for the page attribute. Invalid parameter: page must be an integer'
            end
          end
        end
      end

      describe "on a GET to #solved" do
        describe "without extra parameters" do
          before { get :solved }
          should_use_presenter Api::V2::RequestPresenter

          it "uses object-based pagination by default" do
            assert_response :ok
            requests_keys = JSON.parse(response.body).keys
            assert_equal 2, (requests_keys & ['next_page', 'previous_page']).size
          end
        end

        describe 'using cursor pagination v2' do
          describe 'when cursor_pagination_v2_requests_endpoint arturo is disabled and after param is passed in' do
            before do
              Account.any_instance.stubs(:has_cursor_pagination_v2_requests_endpoint?).returns(false)
            end

            it "returns 400 bad request" do
              get :solved, params: { page: { after: 'xxx' } }
              json = JSON.parse(@response.body).with_indifferent_access
              assert_response :bad_request
              assert_equal 'Invalid attribute', json[:error][:title]
              assert_includes json[:error][:message], 'You passed an invalid value for the page attribute. Invalid parameter: page must be an integer'
            end
          end
        end
      end

      describe "on a GET to #show" do
        before { get :show, params: { id: ticket.nice_id } }
        should_use_presenter Api::V2::RequestPresenter
      end

      describe "on a POST to #create" do
        it "should present the correct CORS header" do
          post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }
          assert_equal "*", response.headers["Access-Control-Allow-Origin"]
        end

        describe "as HC" do
          before do
            @request.user_agent = "HelpCenter"
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }
          end

          before_should "not call Prop.throttle!" do
            Prop.expects(:throttle!).never
          end
        end

        describe "with metadata" do
          it "saves the metadata in the first audit" do
            request_params = {
              subject: "This post to ticket thing doesn't work",
              comment: {
                value: "I really wish it did"
              },
              metadata: {
                post_id: 42,
                post_name: "Post to ticket doesn't work"
              }
            }

            post :create, params: { request: request_params }
            ticket_id = JSON.load(@response.body)["request"]["id"]
            ticket = account.tickets.find_by_nice_id(ticket_id)

            expected_metadata = {
              "post_id" => "42",
              "post_name" => "Post to ticket doesn't work"
            }
            assert_equal expected_metadata, ticket.initial_audit.metadata["custom"]
          end
        end

        describe "with valid parameters" do
          before do
            Account.any_instance.stubs(:has_ticket_forms?).returns(true)

            @tf = Zendesk::TicketForms::Initializer.new(account, ticket_form: {position: 1, default: true, name: "foo", in_all_brands: true}).ticket_form
            @tf.save!

            @tf1 = Zendesk::TicketForms::Initializer.new(account, ticket_form: {
              name: "Wombat", display_name: "All Wombats",
              default: false, active: true, end_user_visible: true, in_all_brands: true, position: 2
            }).ticket_form
            @tf1.save!

            @tf2 = Zendesk::TicketForms::Initializer.new(account, ticket_form: {
              name: "Wombat Runner", display_name: "Wombat Runner",
              default: false, active: true, end_user_visible: true, in_all_brands: true, position: 3
            }).ticket_form
            @tf2.save!

            @brand = FactoryBot.create(:brand, account_id: account.id, name: "Wombat Brand")
          end

          describe "presenter: " do
            before do
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, ticket_form_id: @tf.id } }
            end

            should_use_presenter Api::V2::RequestPresenter, status: :created
          end

          it "sets the ticket_form_id" do
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, ticket_form_id: @tf.id } }
            ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
            assert_equal @tf.id, ticket.ticket_form_id
          end

          describe "assigns the ticket a default ticket form if no ticket_form_id is passed in" do
            it "assigns the account's default ticket form if that form is available to all brands" do
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }

              ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
              assert(account.ticket_forms.default.first.in_all_brands)
              assert_equal account.ticket_forms.default.first.id, ticket.ticket_form.id
            end

            it "returns the highest positioned active ticket form that belongs to the tickets brand, if the default ticket form is not available in all brands" do
              @tf.update_attributes(in_all_brands: false)
              @tf1.update_attributes(active: false)
              @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
              @controller.stubs(:current_brand).returns(@brand)
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }

              ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
              assert_equal @tf2.id, ticket.ticket_form.id
            end

            it "returns default ticket form if the brand has no active forms, even if default form is not available in all brands" do
              @tf.update_attributes(in_all_brands: false)
              @tf1.update_attributes(active: false)
              @tf2.update_attributes(active: false)
              @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
              @controller.stubs(:current_brand).returns(@brand)
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }

              ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
              assert_equal @tf.id, ticket.ticket_form.id
            end
          end
        end

        describe "parameters" do
          let(:params) { { request: { subject: "Help!", comment: { body: "test" }, requester: { email: "test@example.com" } } } }

          it "is valid" do
            post :create, params: params
            assert_response 201
          end

          describe "without comment param" do
            before do
              params[:request].delete :comment
              post :create, params: params
            end

            it "responds with 422" do
              assert_response :unprocessable_entity
            end
          end

          describe "without request param" do
            before do
              params.delete :request
              post :create, params: params
            end

            it "responds with 422" do
              assert_response :unprocessable_entity
            end
          end

          describe "requester" do
            let(:requester_email) { User.find(json["request"]["requester_id"]).email }
            let(:user) { users(:minimum_end_user) } # logged in user

            it "ignores requester param" do
              post :create, params: params

              assert_response 201
              refute_equal params[:request][:requester][:email], requester_email
              assert_equal user.email, requester_email
            end
          end
        end

        describe "with a blank due_at" do
          before { post :create, params: { request: { subject: "Help!", due_at: "", comment: { value: "I need somebody!" } } } }
          should_use_presenter Api::V2::RequestPresenter, status: :created
        end

        describe "with invalid parameters" do
          before { post :create, params: { request: {} } }

          it { assert_response :unprocessable_entity }
        end

        describe "with followup parameters" do
          before do
            ticket.status_id = StatusType.CLOSED
            ticket.will_be_saved_by(ticket.account.owner)
            ticket.save!

            post :create, params: { request: { via_followup_source_id: ticket.nice_id, via: { channel: "closed ticket" }, subject: "Followup!", comment: { value: "Blah Followup" } } }
          end

          it "creates a ticket as a followup" do
            new_ticket = JSON.parse(@response.body)
            id = new_ticket["request"]["id"]
            new_ticket = ticket.account.tickets.find_by_nice_id(id)

            assert_equal ticket, new_ticket.followup_source
            assert_equal ViaType.CLOSED_TICKET, new_ticket.via_id
          end
        end

        describe "with recipient" do
          let(:recipient_address) { recipient_addresses(:not_default).email }

          before do
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, recipient: recipient_address } }
          end

          should_use_presenter Api::V2::RequestPresenter, status: :created

          it "creates a tickets with recipient" do
            recipient = JSON.load(@response.body)["request"]["recipient"]
            assert_equal recipient_address, recipient
          end
        end

        describe "with an allowed via_id" do
          before do
            @account.stubs(is_signup_required?: false)
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: { name: 'Peter' }, via_id: ViaType.WEB_WIDGET } }
          end

          should_use_presenter Api::V2::RequestPresenter, status: :created

          it "creates a tickets with correct via_id" do
            id = JSON.parse(@response.body)["request"]["id"]
            new_ticket = ticket.account.tickets.find_by_nice_id(id)

            assert_equal ViaType.WEB_WIDGET, new_ticket.via_id
          end
        end

        describe "with a not allowed via_id" do
          before do
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: { name: 'Peter' }, via_id: 999 } }
          end

          should_use_presenter Api::V2::RequestPresenter, status: :created

          it "creates a tickets with via_id of Web Service" do
            id = JSON.parse(@response.body)["request"]["id"]
            new_ticket = ticket.account.tickets.find_by_nice_id(id)

            assert_equal ViaType.WEB_SERVICE, new_ticket.via_id
          end
        end

        describe "with an allowed via_id" do
          before do
            @account.stubs(is_signup_required?: false)
            post :create, params: { request: { subject: "Help!", comment: { value: "I need help!" }, requester: { name: 'Peter' }, via_id: ViaType.ANSWER_BOT } }
          end

          should_use_presenter Api::V2::RequestPresenter, status: :created

          it "creates a ticket with correct via_id" do
            id = JSON.parse(@response.body)["request"]["id"]
            new_ticket = ticket.account.tickets.find_by_nice_id(id)

            assert_equal ViaType.ANSWER_BOT, new_ticket.via_id
          end
        end
      end

      describe "on a PUT to #update" do
        describe "with a valid ticket, adding a comment" do
          before { put :update, params: { id: ticket.nice_id, request: { comment: { body: "New comment!" } } } }
          should_use_presenter Api::V2::RequestPresenter
          it('responds with ok') { assert_response :ok }
        end

        describe "with a valid ticket, adding a comment + solved status" do
          before { put :update, params: { id: ticket.nice_id, request: { comment: { body: "New comment!" }, solved: true } } }
          should_use_presenter Api::V2::RequestPresenter
          it('responds with ok') { assert_response :ok }
        end

        describe "with a valid ticket, updating solved status only" do
          before { put :update, params: { id: ticket.nice_id, request: { solved: true } } }
          it('responds with ok') { assert_response :ok }
        end

        describe "with a valid ticket, adding an organization_id that the current user is part of" do
          before { put :update, params: { id: ticket.nice_id, request: { organization_id: @user.organization_id } } }
          should_use_presenter Api::V2::RequestPresenter
          it('responds with ok') { assert_response :ok }
        end

        describe "with a valid ticket, adding an organization_id that the current user is not part of" do
          before { put :update, params: { id: ticket.nice_id, request: { organization_id: organizations(:minimum_organization2) } } }
          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
        end

        describe "with a valid ticket, updating system_metadata" do
          before { put :update, params: { id: ticket.nice_id, request: { system_metadata: { client: "browser info", ip_address: "127.0.0.1" } } } }
          should_use_presenter Api::V2::RequestPresenter
          it('responds with ok') { assert_response :ok }
        end

        describe "with an invalid ticket, updating a whitelisted param" do
          before { put :update, params: { id: tickets(:minimum_1).nice_id, request: { comment: { body: "New comment!" } } } }
          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "updating organizations" do
          let(:invalid_org_for_user) { organizations(:minimum_organization2).id }

          before do
            put :update, params: { id: ticket.nice_id, request: { comment: { body: "Hello!" }, organization_id: invalid_org_for_user } }
          end

          it "attempts to update the ticket with the supplied organization id" do
            assert @response.body =~ /not a valid organization for the requester/
          end
        end

        describe "rich comments" do
          let(:comment) { { body: "New Comment", html_body: "New <strong>Comment</strong>" } }

          it "stores the rich comment" do
            put :update, params: { id: ticket.nice_id, request: { comment: comment } }
            ticket.reload
            assert_equal "rich", ticket.comments.last.format
            assert_equal "New <strong>Comment</strong>", ticket.comments.last.send(:value)
          end
        end
      end

      describe "with safe_update set to true" do
        let(:request_update) do
          {
            comment: {
              body: "New comment!"
            },
            safe_update: true,
            updated_stamp: updated_stamp
          }
        end

        before do
          put :update, params: { id: ticket.nice_id.to_s, request: request_update }
          @json = JSON.parse(@response.body)
        end

        describe "with an outdated updated_stamp" do
          let(:updated_stamp) { (ticket.updated_at - 2.minutes).iso8601 }

          it('responds with conflict') { assert_response :conflict }

          it "returns an error message" do
            assert_equal 'UpdateConflict', @json['error']
          end

          it "returns an error description" do
            refute_nil @json['description']
          end
        end

        describe "with an up-to-date updated_stamp" do
          let(:updated_stamp) { ticket.updated_at.iso8601 }

          it "updates the ticket (add comment)" do
            assert_response :success
            assert_equal 'New comment!', ticket.comments[1].body
          end
        end

        describe "without the required updated_stamp param" do
          let(:updated_stamp) { nil }

          it "returns 422 if updated_stamp param is not included" do
            assert_response :unprocessable_entity
          end

          it "returns an error message" do
            assert_equal 'UnprocessableEntity', @json['error']
          end

          it "returns an error description" do
            refute_nil @json['description']
          end
        end
      end

      describe "from a branded subdomain" do
        let(:brand) { FactoryBot.create(:brand, account_id: account.id) }
        let(:second_brand) { FactoryBot.create(:brand, account_id: account.id) }
        let(:branded_ticket) { @user.tickets.working.first }

        before do
          @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
          @controller.stubs(:current_brand).returns(brand)
          branded_ticket.will_be_saved_by(@user)
          branded_ticket.update_attribute(:brand, brand)
          branded_ticket.update_column(:is_public, true)
        end

        describe "on a POST to #create" do
          before do
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }
          end

          it "creates a branded ticket" do
            new_ticket = JSON.parse(@response.body)
            id = new_ticket["request"]["id"]
            new_ticket = account.tickets.find_by_nice_id(id)

            assert_equal brand, new_ticket.brand
          end
        end

        describe "on a GET to #index" do
          before do
            assert @user.tickets.working.size > 1
            get :index
          end

          it "gets a list of branded tickets" do
            requests = JSON.parse(@response.body)['requests']

            assert_equal 1, requests.size
            assert_equal branded_ticket.nice_id, requests.first["id"]
          end
        end

        describe "on a GET to #show" do
          describe "with a ticket that belongs to the brand" do
            before { get :show, params: { id: branded_ticket.nice_id } }
            should_use_presenter Api::V2::RequestPresenter
          end

          describe "with a ticket that is not in the brand" do
            before { get :show, params: { id: ticket.nice_id } }
            it('responds with not_found') { assert_response :not_found }
          end

          describe "archived tickets" do
            before do
              archive_and_delete(branded_ticket, ticket)
            end

            describe "with an archived ticket that belongs to the brand" do
              before { get :show, params: { id: branded_ticket.nice_id } }
              should_use_presenter Api::V2::RequestPresenter
            end

            describe "with an archived ticket that is not in the brand" do
              before { get :show, params: { id: ticket.nice_id } }
              it('responds with not_found') { assert_response :not_found }
            end

            describe "archived tickets that are missing brand ids" do
              before do
                Ticket.any_instance.expects(:brand_id).returns(nil)
              end

              describe "with an archived ticket whose stub's brand matches the current brand" do
                before { get :show, params: { id: branded_ticket.nice_id } }
                should_use_presenter Api::V2::RequestPresenter
              end

              describe "with an archived ticket whose stub's brand does not match the current brand" do
                before { get :show, params: { id: ticket.nice_id } }
                it('responds with not_found') { assert_response :not_found }
              end

              describe "with an archived ticket whose stub does not have a brand" do
                before do
                  TicketArchiveStub.any_instance.expects(:brand_id).returns(nil)
                  get :show, params: { id: ticket.nice_id }
                end

                it('responds with not_found') { assert_response :not_found }
              end

              describe "with an archived ticket without a stub" do
                before do
                  ticket.stubs(:ticket_archive_stub).returns(nil)
                  get :show, params: { id: ticket.nice_id }
                end

                it('responds with not_found') { assert_response :not_found }
              end
            end
          end
        end

        describe "on a PUT to #update" do
          describe "with a ticket that belongs to the brand, updating a whitelisted param" do
            before { put :update, params: { id: branded_ticket.nice_id, request: { comment: { body: "New comment!" } } } }
            should_use_presenter Api::V2::RequestPresenter
          end

          describe "with a ticket that is not in the brand, updating a whitelisted param" do
            before { put :update, params: { id: ticket.nice_id, request: { comment: { body: "New comment!" } } } }
            it('responds with not_found') { assert_response :not_found }
          end
        end

        describe "when the brand is inactive" do
          before do
            brand.update_attribute(:active, false)
            get :index
          end

          it('responds with not_found') { assert_response :not_found }
        end
      end

      describe "on a GET to #show" do
        describe "followup_source_id" do
          let(:source) { tickets(:minimum_1) }
          let(:target) { tickets(:minimum_5) }

          it "should be nil when not a followup ticket" do
            get :show, params: { id: target.nice_id }

            request = JSON.parse(@response.body)["request"]
            assert_nil request["followup_source_id"]
          end

          it "should be present when source is not archived" do
            TicketLink.create!(
              target: target,
              source: source,
              link_type: 'Followup'
            )
            get :show, params: { id: target.nice_id }
            request = JSON.parse(@response.body)["request"]
            assert_equal source.nice_id, request["followup_source_id"]
          end

          it "should be present when source is archived" do
            TicketLink.create!(
              target: target,
              source: source,
              link_type: 'Followup'
            )
            archive_and_delete(ticket)
            get :show, params: { id: target.nice_id }
            request = JSON.parse(@response.body)["request"]
            assert_equal source.nice_id, request["followup_source_id"]
          end
        end
      end
    end

    describe "for private tickets" do
      let(:account) { accounts(:minimum) }

      let(:ticket) do
        Arturo.enable_feature!(:first_comment_private)
        ticket_initializer = Zendesk::Tickets::Initializer.new(account, users(:minimum_agent), requester_id: users(:minimum_end_user).id, ticket: { submitter: users(:minimum_agent), subject: "Private Wombats", comment: { public: false, value: "private wombat ticket about end user" } })
        private_ticket = ticket_initializer.ticket
        private_ticket.save!
        private_ticket
      end

      before do
        assert_equal false, ticket.is_public
        get :show, params: { id: ticket.nice_id }
      end

      it 'responds with forbidden' do
        assert_response :forbidden
      end
    end

    describe "with an unverified email identity" do
      let(:ticket) { tickets(:minimum_2) }
      let(:current_user) { users(:minimum_end_user) }
      let(:unverified_email_identity) do
        UserEmailIdentity.create(
          user: current_user,
          value: "EnduserUnverified@zendesk.com",
          account: account,
          is_verified: false
        )
      end

      before do
        unverified_email_identity
      end

      describe "on a GET to #index" do
        describe_with_arturo_enabled "unverified_ticket_creations" do
          describe "when end user created tickets using the unverified email" do
            before do
              ticket.create_unverified_creation(
                account: account,
                user: current_user,
                user_identity: unverified_email_identity,
                from_address: unverified_email_identity.value
              )
            end

            it('responds with forbidden') do
              get :index, params: { user_id: ticket.requester_id }
              assert_response :forbidden
            end

            it "should increment statsd with 'unverified.ticket'" do
              statsd_client.expects(:increment).with('unverified.ticket')
              get :index, params: { user_id: ticket.requester_id }
            end
          end

          describe "when end user didn't create tickets using the unverified email" do
            it('responds with ok') do
              get :index, params: { user_id: ticket.requester_id }
              assert_response :ok
            end
          end
        end

        describe "with a user" do
          before { get :index, params: { user_id: ticket.requester_id } }

          it('responds with forbidden') { assert_response :forbidden }

          it "should increment statsd with 'unverified.email.blocked'" do
            statsd_client.expects(:increment).with('unverified.email.blocked')
            get :index, params: { user_id: ticket.requester_id }
          end

          it "should log" do
            message = "Unverified End User Email would be restricted ACCOUNT #{account.id} IDENTITIES IN QUESTION #{current_user.identities.unverified_emails.inspect}"
            Rails.logger.expects(:info).with(message)
            get :index, params: { user_id: ticket.requester_id }
          end
        end

        describe "with a user different than the logged in one" do
          before { get :index, params: { user_id: users(:minimum_search_user).id } }

          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "with a valid status type filter" do
          before { get :index, params: { status: 'new,open' } }

          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "with sort_order" do
          describe 'orders by created_at asc' do
            before { get :index, params: { sort_order: 'asc', sort_by: 'created_at' } }

            it('responds with forbidden') { assert_response :forbidden }
          end

          describe 'orders by created_at desc' do
            before { get :index, params: { sort_order: 'desc', sort_by: 'created_at' } }

            it('responds with forbidden') { assert_response :forbidden }
          end
        end

        describe "with an invalid status type filter" do
          before { get :index, params: { status: 'new,foobar,open' } }

          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "for all requests" do
          before { get :index }

          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "when there are private tickets" do
          let(:ticket) do
            ticket_initializer = Zendesk::Tickets::Initializer.new(account, users(:minimum_agent), requester_id: users(:minimum_end_user).id, ticket: { submitter: users(:minimum_agent), subject: "Private Wombats", comment: { public: false, value: "private wombat ticket about end user" } })
            private_ticket = ticket_initializer.ticket
            private_ticket.save!
            private_ticket
          end

          before do
            assert_equal false, ticket.is_public
            get :index
          end

          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "with an organization" do
          describe "that the current user is a part of" do
            before { get :index, params: { organization_id: @user.organization_id } }

            it('responds with forbidden') { assert_response :forbidden }
          end

          describe "that the current user isn't a part of" do
            before { get :index, params: { organization_id: organizations(:minimum_organization2) } }

            it('responds with forbidden') { assert_response :forbidden }
          end
        end
      end

      describe "on a GET to #open" do
        before { get :open }

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "on a GET to #solved" do
        before { get :solved }

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "on a GET to #show" do
        before { get :show, params: { id: ticket.nice_id } }

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "on a POST to #create" do
        it "should present the correct CORS header" do
          post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }
          assert_equal "*", response.headers["Access-Control-Allow-Origin"]
        end

        describe "as HC" do
          before do
            @request.user_agent = "HelpCenter"
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }
          end

          before_should "not call Prop.throttle!" do
            Prop.expects(:throttle!).never
          end
        end

        describe "with metadata" do
          it "saves the metadata in the first audit" do
            request_params = {
              subject: "This post to ticket thing doesn't work",
              comment: {
                value: "I really wish it did"
              },
              metadata: {
                post_id: 42,
                post_name: "Post to ticket doesn't work"
              }
            }

            post :create, params: { request: request_params }
            ticket_id = JSON.load(@response.body)["request"]["id"]
            ticket = account.tickets.find_by_nice_id(ticket_id)

            expected_metadata = {
              "post_id" => "42",
              "post_name" => "Post to ticket doesn't work"
            }
            assert_equal expected_metadata, ticket.initial_audit.metadata["custom"]
          end
        end

        describe "with valid parameters" do
          before do
            Account.any_instance.stubs(:has_ticket_forms?).returns(true)

            @tf = Zendesk::TicketForms::Initializer.new(account, ticket_form: {position: 1, default: true, name: "foo", in_all_brands: true}).ticket_form
            @tf.save!

            @tf1 = Zendesk::TicketForms::Initializer.new(account, ticket_form: {
              name: "Wombat", display_name: "All Wombats",
              default: false, active: true, end_user_visible: true, in_all_brands: true, position: 2
            }).ticket_form
            @tf1.save!

            @tf2 = Zendesk::TicketForms::Initializer.new(account, ticket_form: {
              name: "Wombat Runner", display_name: "Wombat Runner",
              default: false, active: true, end_user_visible: true, in_all_brands: true, position: 3
            }).ticket_form
            @tf2.save!

            @brand = FactoryBot.create(:brand, account_id: account.id, name: "Wombat Brand")
          end

          describe "presenter: " do
            before do
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, ticket_form_id: @tf.id } }
            end

            should_use_presenter Api::V2::RequestPresenter, status: :created
          end

          it "sets the ticket_form_id" do
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, ticket_form_id: @tf.id } }
            ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
            assert_equal @tf.id, ticket.ticket_form_id
          end

          describe "assigns the ticket a default ticket form if no ticket_form_id is passed in" do
            it "assigns the account's default ticket form if that form is available to all brands" do
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }

              ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
              assert(account.ticket_forms.default.first.in_all_brands)
              assert_equal account.ticket_forms.default.first.id, ticket.ticket_form.id
            end

            it "returns the highest positioned active ticket form that belongs to the tickets brand, if the default ticket form is not available in all brands" do
              @tf.update_attributes(in_all_brands: false)
              @tf1.update_attributes(active: false)
              @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
              @controller.stubs(:current_brand).returns(@brand)
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }

              ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
              assert_equal @tf2.id, ticket.ticket_form.id
            end

            it "returns default ticket form if the brand has no active forms, even if default form is not available in all brands" do
              @tf.update_attributes(in_all_brands: false)
              @tf1.update_attributes(active: false)
              @tf2.update_attributes(active: false)
              @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
              @controller.stubs(:current_brand).returns(@brand)
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }

              ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
              assert_equal @tf.id, ticket.ticket_form.id
            end
          end
        end

        describe "with a blank due_at" do
          before { post :create, params: { request: { subject: "Help!", due_at: "", comment: { value: "I need somebody!" } } } }
          should_use_presenter Api::V2::RequestPresenter, status: :created
        end

        describe "with invalid parameters" do
          before { post :create, params: { request: {} } }

          it { assert_response :unprocessable_entity }
        end

        describe "with followup parameters" do
          before do
            ticket.status_id = StatusType.CLOSED
            ticket.will_be_saved_by(ticket.account.owner)
            ticket.save!

            post :create, params: { request: { via_followup_source_id: ticket.nice_id, via: { channel: "closed ticket" }, subject: "Followup!", comment: { value: "Blah Followup" } } }
          end

          it "creates a ticket as a followup" do
            new_ticket = JSON.parse(@response.body)
            id = new_ticket["request"]["id"]
            new_ticket = ticket.account.tickets.find_by_nice_id(id)

            assert_equal ticket, new_ticket.followup_source
            assert_equal ViaType.CLOSED_TICKET, new_ticket.via_id
          end
        end

        describe "with recipient" do
          let(:recipient_address) { recipient_addresses(:not_default).email }

          before do
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, recipient: recipient_address } }
          end

          should_use_presenter Api::V2::RequestPresenter, status: :created

          it "creates a tickets with recipient" do
            recipient = JSON.load(@response.body)["request"]["recipient"]
            assert_equal recipient_address, recipient
          end
        end

        describe "with an allowed via_id" do
          before do
            @account.stubs(is_signup_required?: false)
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: { name: 'Peter' }, via_id: ViaType.WEB_WIDGET } }
          end

          should_use_presenter Api::V2::RequestPresenter, status: :created

          it "creates a tickets with correct via_id" do
            id = JSON.parse(@response.body)["request"]["id"]
            new_ticket = ticket.account.tickets.find_by_nice_id(id)

            assert_equal ViaType.WEB_WIDGET, new_ticket.via_id
          end
        end

        describe "with a not allowed via_id" do
          before do
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: { name: 'Peter' }, via_id: 999 } }
          end

          should_use_presenter Api::V2::RequestPresenter, status: :created

          it "creates a tickets with via_id of Web Service" do
            id = JSON.parse(@response.body)["request"]["id"]
            new_ticket = ticket.account.tickets.find_by_nice_id(id)

            assert_equal ViaType.WEB_SERVICE, new_ticket.via_id
          end
        end
      end

      describe "on a PUT to #update" do
        before { put :update, params: { id: ticket.nice_id, request: { comment: { body: "New comment!" } } } }

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "from a branded subdomain" do
        let(:brand) { FactoryBot.create(:brand, account_id: account.id) }
        let(:second_brand) { FactoryBot.create(:brand, account_id: account.id) }
        let(:branded_ticket) { @user.tickets.working.first }

        before do
          @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
          @controller.stubs(:current_brand).returns(brand)
          branded_ticket.will_be_saved_by(@user)
          branded_ticket.update_attribute(:brand, brand)
          branded_ticket.update_column(:is_public, true)
        end

        describe "on a POST to #create" do
          it "should present the correct CORS header" do
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }
            assert_equal "*", response.headers["Access-Control-Allow-Origin"]
          end

          describe "as HC" do
            before do
              @request.user_agent = "HelpCenter"
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }
            end

            before_should "not call Prop.throttle!" do
              Prop.expects(:throttle!).never
            end
          end

          describe "with metadata" do
            it "saves the metadata in the first audit" do
              request_params = {
                subject: "This post to ticket thing doesn't work",
                comment: {
                  value: "I really wish it did"
                },
                metadata: {
                  post_id: 42,
                  post_name: "Post to ticket doesn't work"
                }
              }

              post :create, params: { request: request_params }
              ticket_id = JSON.load(@response.body)["request"]["id"]
              ticket = account.tickets.find_by_nice_id(ticket_id)

              expected_metadata = {
                "post_id" => "42",
                "post_name" => "Post to ticket doesn't work"
              }
              assert_equal expected_metadata, ticket.initial_audit.metadata["custom"]
            end
          end

          describe "with valid parameters" do
            before do
              Account.any_instance.stubs(:has_ticket_forms?).returns(true)

              @tf = Zendesk::TicketForms::Initializer.new(account, ticket_form: {
                position: 1, default: true,
                name: "foo", in_all_brands: true
              }).ticket_form
              @tf.save!

              @tf1 = Zendesk::TicketForms::Initializer.new(account, ticket_form: {
                name: "Wombat", display_name: "All Wombats",
                default: false, active: true, end_user_visible: true,
                in_all_brands: true, position: 2
              }).ticket_form
              @tf1.save!

              @tf2 = Zendesk::TicketForms::Initializer.new(account, ticket_form: {
                name: "Wombat Runner", display_name: "Wombat Runner",
                default: false, active: true, end_user_visible: true,
                in_all_brands: true, position: 3
              }).ticket_form
              @tf2.save!

              @brand = FactoryBot.create(:brand, account_id: account.id, name: "Wombat Brand")
            end

            describe "presenter: " do
              before do
                post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, ticket_form_id: @tf.id } }
                assert_response :created
              end

              should_use_presenter Api::V2::RequestPresenter, status: :created
            end

            it "sets the ticket_form_id" do
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, ticket_form_id: @tf.id } }
              assert_response :created
              ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
              assert_equal @tf.id, ticket.ticket_form_id
            end

            describe "assigns the ticket a default ticket form if no ticket_form_id is passed in" do
              it "assigns the account's default ticket form if that form is available to all brands" do
                post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }

                ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
                assert(account.ticket_forms.default.first.in_all_brands)
                assert_equal account.ticket_forms.default.first.id, ticket.ticket_form.id
              end

              it "returns the highest positioned active ticket form that belongs to the tickets brand, if the default ticket form is not available in all brands" do
                @tf.update_attributes(in_all_brands: false)
                @tf1.update_attributes(active: false)
                @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
                @controller.stubs(:current_brand).returns(@brand)
                post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }

                ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
                assert_equal @tf2.id, ticket.ticket_form.id
              end

              it "returns default ticket form if the brand has no active forms, even if default form is not available in all brands" do
                @tf.update_attributes(in_all_brands: false)
                @tf1.update_attributes(active: false)
                @tf2.update_attributes(active: false)
                @controller.send(:current_account).stubs(:has_multibrand?).returns(true)
                @controller.stubs(:current_brand).returns(@brand)
                post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }

                ticket = account.tickets.find_by_nice_id(JSON.load(@response.body)["request"]["id"])
                assert_equal @tf.id, ticket.ticket_form.id
              end
            end
          end

          describe "with a blank due_at" do
            before { post :create, params: { request: { subject: "Help!", due_at: "", comment: { value: "I need somebody!" } } } }
            should_use_presenter Api::V2::RequestPresenter, status: :created
          end

          describe "with invalid parameters" do
            before { post :create, params: { request: {} } }

            it { assert_response :unprocessable_entity }
          end

          describe "with followup parameters" do
            before do
              ticket.status_id = StatusType.CLOSED
              ticket.will_be_saved_by(ticket.account.owner)
              ticket.save!

              post :create, params: { request: { via_followup_source_id: ticket.nice_id, via: { channel: "closed ticket" }, subject: "Followup!", comment: { value: "Blah Followup" } } }
            end

            it "creates a ticket as a followup" do
              new_ticket = JSON.parse(@response.body)
              id = new_ticket["request"]["id"]
              new_ticket = ticket.account.tickets.find_by_nice_id(id)

              assert_equal ticket, new_ticket.followup_source
              assert_equal ViaType.CLOSED_TICKET, new_ticket.via_id
            end
          end

          describe "with recipient" do
            let(:recipient_address) { recipient_addresses(:not_default).email }

            before do
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, recipient: recipient_address } }
            end

            should_use_presenter Api::V2::RequestPresenter, status: :created

            it "creates a tickets with recipient" do
              recipient = JSON.load(@response.body)["request"]["recipient"]
              assert_equal recipient_address, recipient
            end
          end

          describe "with an allowed via_id" do
            before do
              @account.stubs(is_signup_required?: false)
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: { name: 'Peter' }, via_id: ViaType.WEB_WIDGET } }
            end

            should_use_presenter Api::V2::RequestPresenter, status: :created

            it "creates a tickets with correct via_id" do
              id = JSON.parse(@response.body)["request"]["id"]
              new_ticket = ticket.account.tickets.find_by_nice_id(id)

              assert_equal ViaType.WEB_WIDGET, new_ticket.via_id
            end
          end

          describe "with a not allowed via_id" do
            before do
              post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: { name: 'Peter' }, via_id: 999 } }
            end

            should_use_presenter Api::V2::RequestPresenter, status: :created

            it "creates a tickets with via_id of Web Service" do
              id = JSON.parse(@response.body)["request"]["id"]
              new_ticket = ticket.account.tickets.find_by_nice_id(id)

              assert_equal ViaType.WEB_SERVICE, new_ticket.via_id
            end
          end
        end

        describe "on a GET to #index" do
          before do
            assert @user.tickets.working.size > 1
            get :index
          end

          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "on a GET to #show" do
          before { get :show, params: { id: branded_ticket.nice_id } }

          it('responds with forbidden') { assert_response :forbidden }
        end

        describe "on a PUT to #update" do
          describe "with a ticket that belongs to the brand, updating a whitelisted param" do
            before { put :update, params: { id: branded_ticket.nice_id, request: { comment: { body: "New comment!" } } } }

            it('responds with forbidden') { assert_response :forbidden }
          end
        end

        describe "when the brand is inactive" do
          before do
            brand.update_attribute(:active, false)
            get :index
          end

          it('responds with forbidden') { assert_response :forbidden }
        end
      end
    end
  end

  as_an_agent do
    describe "on a GET to #index" do
      describe "with a user" do
        before { get :index, params: { user_id: users(:minimum_end_user).id } }

        it('responds with ok') { assert_response :ok }
      end
    end

    describe "on a GET to #show" do
      let(:account) { accounts(:minimum) }

      describe "for private tickets" do
        let(:ticket) do
          Arturo.enable_feature!(:first_comment_private)
          ticket_initializer = Zendesk::Tickets::Initializer.new(account, users(:minimum_agent), requester_id: users(:minimum_end_user).id, ticket: { submitter: users(:minimum_agent), subject: "Private Wombats", comment: { public: false, value: "private wombat ticket about end user" } })
          private_ticket = ticket_initializer.ticket
          private_ticket.will_be_saved_by(users(:minimum_agent))
          private_ticket.save!
          private_ticket
        end

        before do
          assert_equal false, ticket.is_public
          get :show, params: { id: ticket.nice_id }
        end

        it 'responds with success' do
          assert_response :success
        end
      end
    end
  end

  as_a_subsystem_user(account: :minimum, user: 'zendesk') do
    describe "user restrictions on create" do
      before do
        account.stubs(is_signup_required?: false)
        @controller.send(:current_account).update_attribute('is_open', true)
      end

      describe_with_arturo_enabled :orca_classic_restrict_anon_requests_endpoint do
        describe_with_and_without_arturo_enabled :orca_classic_restrict_anon_requests_endpoint_log_only do
          before do
            Rails.logger.expects(:info).with("Anonymous_request_succeeded")
            post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: { name: 'Peter' }, via_id: ViaType.WEB_FORM } }
          end
          it "sends the request successfully" do
            assert_response :created
          end
        end
      end
    end
  end
end
