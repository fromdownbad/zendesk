require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::WorkspacesController do
  extend Api::V2::TestHelper
  extend ApiScopesHelper
  fixtures :tickets, :workspaces, :accounts, :ticket_fields

  let(:ticket) { tickets(:minimum_3) }
  let(:title) { 'workspace title 1' }
  let(:conditions) { { all: [{"field": "status_id", "operator": "is", "value": "0"}], any: []} }
  let(:account) { accounts(:minimum) }
  let(:apps) { [{ id: 1, expand: true, position: 1 }, { id: 2, expand: false, position: 2 }, { id: 3, expand: true, position: 3 }] }

  before do
    account.subscription.plan_type = 4
    account.subscription.save!
    accept :json
    use_ssl

    Zendesk::TicketForms::Initializer.new(account, ticket_form: {
      name: "Form 1",
      position: 1
    }).ticket_form.save!

    Account.any_instance.stubs(:has_contextual_workspaces?).returns(true)
    Workspace.stubs(:active_workspaces_count).returns(Workspace::MAX_ACTIVE_WORKSPACES - 1)
  end

  let(:ticket_form) { account.ticket_forms.first }

  describe 'routing' do
    # TODO: look into why `assert_routing`/`assert_generates` fails
    it 'routes correctly for api/v2/workspaces' do
      assert_recognizes({controller: 'api/v2/workspaces', action: 'index'}, path: 'api/v2/workspaces', method: :get)
      assert_recognizes({controller: 'api/v2/workspaces', action: 'show', id: '123'}, path: 'api/v2/workspaces/123', method: :get)
      assert_recognizes({controller: 'api/v2/workspaces', action: 'create'}, path: 'api/v2/workspaces', method: :post)
      assert_recognizes({controller: 'api/v2/workspaces', action: 'destroy', id: '123'}, path: 'api/v2/workspaces/123', method: :delete)
      assert_recognizes({controller: 'api/v2/workspaces', action: 'destroy_many'}, path: 'api/v2/workspaces/destroy_many', method: :delete)
      assert_recognizes({controller: 'api/v2/workspaces', action: 'update', id: '123'}, path: 'api/v2/workspaces/123', method: :put)
      assert_recognizes({controller: 'api/v2/workspaces', action: 'reorder'}, path: 'api/v2/workspaces/reorder', method: :put)
      assert_recognizes({controller: 'api/v2/workspaces', action: 'definitions'}, path: 'api/v2/workspaces/definitions', method: :get)
    end
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_admin do
    describe "a GET to #index" do
      before do
        get :index
      end

      should_use_presenter Api::V2::WorkspacePresenter

      it "responds with success" do
        assert_response :success
      end

      it "send workspaces with response" do
        @json_response = JSON.parse(@response.body)
        assert_not_nil @json_response["workspaces"]
      end

      it "returns 403 if the account does not have contextual workspaces" do
        Account.any_instance.unstub(:has_contextual_workspaces?)
        get :index
        assert_response(403)
      end

      it "returns only macro ids if no macros parameter is true" do
        get :index, params: { no_macros: true }

        @json_response = JSON.parse(@response.body)

        assert @json_response["workspaces"][0]["macro_ids"].class, Array
        assert_nil @json_response["workspaces"][0]["selected_macros"]
      end

      it "returns only macro ids and selected if no macros parameter is false" do
        get :index, params: { no_macros: false }

        @json_response = JSON.parse(@response.body)

        assert @json_response["workspaces"][0]["macro_ids"].class, Array
        assert @json_response["workspaces"][0]["selected_macros"].class, Array
      end
    end

    describe "a post to #create" do
      describe "when correct parameters are passed" do
        describe "when under the product limit" do
          before do
            post :create, params: { workspace: { title: "Workspace name", description: "Description of the workspace", macros: [1, 2, 3], apps: apps, ticket_form_id: ticket_form.id, conditions: conditions } }
          end

          it "responds with success" do
            assert_response :success
          end

          it "sends workspace with response" do
            @json_response = JSON.parse(@response.body)
            assert_not_nil @json_response["workspace"]
          end

          it "sets the new workspace to the last position" do
            @json_response = JSON.parse(@response.body)
            assert_equal Workspace.active_workspaces_count(account) + 1, @json_response["workspace"]["position"]
          end
        end

        describe "when over the product limit" do
          before do
            Workspace.stubs(:active_workspaces_count).returns(Workspace::MAX_ACTIVE_WORKSPACES)
            post :create, params: { workspace: { title: "Workspace name", description: "Description of the workspace", macros: [1, 2, 3], apps: apps, ticket_form_id: ticket_form.id, conditions: conditions } }
          end

          it "responds with an error message" do
            @json_response = JSON.parse(@response.body)
            assert_equal("unprocessable_entity", @json_response["status"])
          end
        end
      end

      describe "when required parameters 'title' is not passed" do
        before do
          post :create, params: { workspace: { description: "Description of the workspace", macros: [1, 2, 3], apps: apps, ticket_form_id: ticket_form.id, conditions: conditions } }
        end

        it "sends response with error message" do
          @json_response = JSON.parse(@response.body)
          assert_not_nil @json_response["errors"]
          assert_equal("unprocessable_entity", @json_response["status"])
        end
      end
    end

    describe "a DELETE to #destroy" do
      let(:workspace_1) { workspaces(:refund_workspace) } # position 2
      let(:workspace_2) { workspaces(:general_workspace) } # position 4
      let(:workspace_3) { workspaces(:general_workspace_2) } # position 5
      let(:workspace_4) { workspaces(:billing_workspace) } # position 9999

      it "responds with success" do
        delete :destroy, params: { id: workspace_1.id }
        assert_response :success
      end

      it "normalizes the workspace positions" do
        delete :destroy, params: { id: workspace_1.id }

        workspace_2.reload
        workspace_3.reload
        workspace_4.reload

        assert_equal 1, workspace_2.position
        assert_equal 2, workspace_3.position
        assert_equal 3, workspace_4.position
      end

      it "with an invalid workspace id returns unprocessable entity" do
        delete :destroy, params: { id: 20000 }

        @json_response = JSON.parse(@response.body)
        assert_equal "not_found", @json_response["status"]
        assert_response :success
      end
    end

    describe "a DELETE to #destroy_many" do
      let(:workspace_1) { workspaces(:refund_workspace) } # position 2
      let(:workspace_2) { workspaces(:general_workspace) } # position 4
      let(:workspace_3) { workspaces(:general_workspace_2) } # position 5
      let(:workspace_4) { workspaces(:billing_workspace) } # position 9999

      it "responds with success" do
        delete :destroy_many, params: { ids: [workspace_1.id, workspace_2.id].join(',') }
        assert_response :success
      end

      it "normalizes the workspace positions" do
        delete :destroy_many, params: { ids: [workspace_1.id, workspace_2.id].join(',') }

        workspace_3.reload
        workspace_4.reload

        assert_equal 1, workspace_3.position
        assert_equal 2, workspace_4.position
      end

      it "with an invalid workspace id returns not found" do
        delete :destroy_many, params: { ids: '20000' }

        @json_response = JSON.parse(@response.body)
        assert_equal "unprocessable_entity", @json_response["status"]
        assert_response :success
      end
    end

    describe "a PUT to #update" do
      describe "when correct parameters are passed" do
        describe "when activating a workspace" do
          let(:workspace) { workspaces(:refund_workspace) }
          let(:inactive_workspace) { workspaces(:inactive_workspace) }

          describe "when under the product limit" do
            before do
              put :update, params: { id: inactive_workspace.id, workspace: { title: "Workspace name", description: "Description of the workspace", macros: [1, 2, 3], apps: apps, ticket_form_id: ticket_form.id, conditions: conditions, activated: true } }
              @json_response = JSON.parse(@response.body)
            end

            it "responds with success" do
              assert_response :success
            end

            it "sends workspace with response" do
              assert_not_nil @json_response["workspace"]
            end

            it "sets the position to last" do
              assert_equal @json_response["workspace"]["position"], Workspace::MAX_ACTIVE_WORKSPACES
            end
          end

          describe "when over the product limit" do
            before do
              Workspace.stubs(:active_workspaces_count).returns(Workspace::MAX_ACTIVE_WORKSPACES)
              put :update, params: { id: inactive_workspace.id, workspace: { title: "Workspace name", description: "Description of the workspace", macros: [1, 2, 3], apps: apps, ticket_form_id: ticket_form.id, conditions: conditions, activated: true } }
            end

            it "responds with an error message" do
              @json_response = JSON.parse(@response.body)
              assert_equal("unprocessable_entity", @json_response["status"])
            end
          end
        end

        describe "when deactivating a workspace" do
          let(:workspace_1) { workspaces(:refund_workspace) } # position 2
          let(:workspace_2) { workspaces(:general_workspace) } # position 4
          let(:workspace_3) { workspaces(:general_workspace_2) } # position 5
          let(:workspace_4) { workspaces(:billing_workspace) } # position 9999

          it "normalizes the workspace positions" do
            put :update, params: { id: workspace_2.id, workspace: { title: "Workspace name", description: "Description of the workspace", macros: [1, 2, 3], apps: apps, ticket_form_id: ticket_form.id, conditions: conditions, activated: false } }

            workspace_1.reload
            workspace_3.reload
            workspace_4.reload

            assert_equal 1, workspace_1.position
            assert_equal 2, workspace_3.position
            assert_equal 3, workspace_4.position
          end
        end
      end

      describe "when required parameters 'title' is not passed" do
        let(:workspace) { workspaces(:refund_workspace) }

        before do
          put :update, params: { id: workspace.id, workspace: { title: nil, description: "Description of the workspace", macros: [1, 2, 3], apps: apps, ticket_form_id: ticket_form.id, conditions: conditions } }
        end

        it "sends response with error message" do
          @json_response = JSON.parse(@response.body)
          assert_not_nil @json_response["errors"]
          assert_equal "unprocessable_entity", @json_response["status"]
        end
      end
    end

    describe "a PUT to #reorder" do
      let(:refund_workspace) { workspaces(:refund_workspace) }
      let(:billing_workspace) { workspaces(:billing_workspace) }
      let(:invalid_workspace) { workspaces(:invalid_workspace) }

      describe "when valid ids are passed" do
        before do
          put :reorder, params: { ids: [billing_workspace.id, refund_workspace.id] }
        end

        it "responds with success" do
          assert_response :success
        end

        it "responds with the workspace with correct positions" do
          @json_response = JSON.parse(@response.body)
          billing_workspace_response = @json_response["workspaces"].find { |workspace| workspace["title"] == billing_workspace["title"] }
          refund_workspace_response = @json_response["workspaces"].find { |workspace| workspace["title"] == refund_workspace["title"] }
          billing_workspace.reload
          refund_workspace.reload

          assert_equal billing_workspace_response["position"], 1
          assert_equal refund_workspace_response["position"], 2
          assert_equal 1, billing_workspace.position
          assert_equal 2, refund_workspace.position
        end
      end

      describe "when invalid ids are passed" do
        it "responds with an error message when workspace is invalid" do
          put :reorder, params: { ids: [invalid_workspace.id, refund_workspace.id] }

          @json_response = JSON.parse(@response.body)
          assert_not_nil @json_response["errors"]
          assert_equal "unprocessable_entity", @json_response["status"]
        end

        it "responds with an error message when workspace cannot be found" do
          put :reorder, params: { ids: [12345, refund_workspace.id] }

          @json_response = JSON.parse(@response.body)
          assert_not_nil @json_response["errors"]
          assert_equal "unprocessable_entity", @json_response["status"]
        end
      end
    end

    describe "a GET to #show" do
      let(:workspace) { workspaces(:billing_workspace) }

      describe "when a valid workspace id" do
        before do
          get :show, params: { id: workspace.id }
        end

        it "responds with success" do
          assert_response :success
        end

        it "sends workspace with response" do
          @json_response = JSON.parse(@response.body)
          assert_not_nil @json_response["workspace"]
        end
      end

      describe "when invalid workspace id is passed" do
        before do
          get :show, params: { id: 10000 }
        end

        it "sends response with error message" do
          @json_response = JSON.parse(@response.body)
          assert_equal("not_found", @json_response["status"])
        end
      end
    end

    describe "A valid workspace with macros and apps" do
      before do
        post :create, params: { workspace: { title: "Workspace name", description: "Description of the workspace", macros: [1, 2, 3], apps: apps, ticket_form_id: ticket_form.id, conditions: conditions } }
      end

      let(:id) { JSON.parse(@response.body)["workspace"]["id"] }

      it "creates associated workspace elements" do
        assert_equal 6, WorkspaceElement.where(workspace_id: id).length
      end

      it "updates associated workspace elements" do
        apps.pop # removes the last app
        put :update, params: { id: id, workspace: { macros: [1, 2], apps: apps} }
        # Update only updates the value for passed parameters and keep the existing parameters value as is.
        assert_equal 4, WorkspaceElement.where(workspace_id: id).length
      end

      it "deletes the associated workspace elements" do
        delete :destroy, params: { id: id }
        assert_empty WorkspaceElement.where(workspace_id: id)
      end
    end

    describe 'a GET to `definitions`' do
      before do
        field_subject = ticket_fields(:minimum_field_subject)
        Account.any_instance.stubs(:field_subject).returns(field_subject)
        get :definitions
      end

      it 'does not perform N+1 queries' do
        assert_no_n_plus_one do
          get :definitions
        end
      end

      it 'responds with `200 OK`' do
        assert_response :ok
      end

      it "send definitions with response" do
        @json_response = JSON.parse(@response.body)
        assert_not_nil @json_response["definitions"]
      end

      should_use_presenter Api::V2::WorkspaceDefinitionsPresenter
    end

    describe 'workspaces API threshold' do
      before do
        account.settings.workspace_controller_threshold = 1
        account.save!
      end

      it 'fails when over the rate limit' do
        get :index
        assert_response :success

        get :index
        assert_response 429
      end
    end
  end

  as_an_agent do
    describe 'when an agent has permissions' do
      describe "a POST to create" do
        describe "valid requests" do
          before do
            Account.any_instance.stubs(:has_contextual_workspaces?).returns(true)
            Account.any_instance.stubs(:has_permissions_contextual_workspaces_manage?).returns(true)
            Access::ExternalPermissions::Permissions.any_instance.stubs(:allow?).returns(true)
            post :create, params: { workspace: { title: "Workspace name", description: "Description of the workspace", macros: [1, 2, 3], apps: apps, ticket_form_id: ticket_form.id, conditions: conditions } }
          end

          it "responds with success" do
            assert_response :success
          end

          it "sends workspace with response" do
            @json_response = JSON.parse(@response.body)
            assert_not_nil @json_response["workspace"]
          end

          it "manage_contestual_workspace is false" do
            Access::ExternalPermissions::Permissions.any_instance.stubs(:allow?).returns(false)
            get :index
            assert_response(200)
          end

          it "manage_contestual_workspace is false" do
            Account.any_instance.unstub(:has_permissions_contextual_workspaces_manage?)
            get :index
            assert_response(200)
          end
        end
      end
    end
  end
end
