require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::CurrentUserController do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :signatures, :role_settings

  before do
    accept :json
  end

  should_route :get, "/api/v2/users/me", action: "show", controller: "api/v2/current_user"
  should_route :get, "/api/v2/users/radar_token", action: "radar_token", controller: "api/v2/current_user"

  as_an_anonymous_user do
    describe_with_arturo_disabled :restrict_unauthenticated_user do
      describe "a GET to :show" do
        before { get :show }
        should_use_presenter Api::V2::Users::EndUserPresenter

        it "properly generates URL" do
          json = JSON.parse(@response.body)
          assert_not_nil json["user"]
          assert_nil json["url"]
        end

        it "is missing CORS Access-Control-Allow-Credentials header" do
          assert_nil response.headers["Access-Control-Allow-Origin"]
          assert_nil response.headers["Access-Control-Allow-Credentials"]
        end
      end

      it 'returns a valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://dashboard.zopim.com'
        get :show
        assert_equal @response.headers['Access-Control-Allow-Origin'], 'http://dashboard.zopim.com'
        assert_equal response.headers["Access-Control-Allow-Credentials"], "true"
      end

      it 'returns a not valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://dashboard.zopim.com.wearehackers.com'
        get :show
        assert_nil response.headers['Access-Control-Allow-Origin']
        assert_nil response.headers["Access-Control-Allow-Credentials"]
      end
    end
    describe "a GET to :show" do
      should_be_unauthorized [:get, :show]
    end
  end

  as_an_end_user do
    describe 'with valid external domain' do
      it 'returns a valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://dashboard.zopim.com'
        get :show
        assert_equal response.headers['Access-Control-Allow-Origin'], 'http://dashboard.zopim.com'
      end

      it 'returns a not valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://dashboard.zopim.com.wearehackers.com'
        get :show
        assert_nil response.headers['Access-Control-Allow-Origin']
      end
    end

    describe "a GET to :show" do
      before { get :show }
      should_use_presenter Api::V2::Users::EndUserPresenter

      it "properly generates URL" do
        json = JSON.parse(@response.body)

        assert_not_nil json["user"]
        assert_nil json["url"]
      end
    end
  end

  as_an_agent do
    describe 'with valid external domains' do
      it 'returns a valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://dashboard.zopim.com'
        get :show
        assert_equal response.headers['Access-Control-Allow-Origin'], 'http://dashboard.zopim.com'
      end

      it 'returns a not valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://dashboard.zopim.com.wearehackers.com'
        get :show
        assert_nil response.headers['Access-Control-Allow-Origin']
      end
    end

    describe "a GET to :show" do
      before do
        get :show
      end
      should_use_presenter Api::V2::Users::AgentPresenter

      it "is missing CORS Access-Control-Allow-Credentials header" do
        assert_nil response.headers["Access-Control-Allow-Origin"]
        assert_nil response.headers["Access-Control-Allow-Credentials"]
      end
    end

    describe "a GET to :show with a callback" do
      before { get :show, params: { callback: 'test' } }

      it 'does not wrap the response in a callback' do
        refute response.body.start_with?('test('), "#{response.body.inspect} is improperly returning JSONP"
      end
    end

    describe "a GET to :radar_token" do
      let(:expires_at) do # The next 30 minute boundary
        time_step = 30.minutes.to_i
        epoch = (Time.now.to_i / time_step).floor
        time_step * epoch + time_step
      end

      before { Timecop.freeze }

      describe "when token is valid" do
        before do
          Timecop.freeze(Time.at(expires_at))
          get :radar_token
        end

        it('responds with success') { assert_response :success }
        it('responds with content_type json') { assert_equal 'application/json', response.content_type.to_s }
        it "responds with a valid payload" do
          json = JSON.parse(@response.body)

          assert_not_nil json["auth"]
          assert_not_nil json["userId"]
        end
        it "sets cache-control header" do
          assert_match /max-age=([0-9]+)/, @response.headers['Cache-Control']
          assert_match /private(,|$)/, @response.headers['Cache-Control']
          assert_match /private=true/, @response.headers['Cache-Control']
          assert_match /must-revalidate=true/, @response.headers['Cache-Control']
        end
      end

      describe "when token is expiring" do
        before do
          Timecop.freeze(Time.at(expires_at + 29.minutes.to_i))
          get :radar_token
        end

        it('responds with success') { assert_response :success }
        it "does not set cache-control header" do
          assert_nil @response.headers['Cache-Control']
        end
      end

      describe 'with strip session cookie arturo' do
        describe_with_arturo_disabled :v2_users_radar_token_strip_session_set_cookie do
          it 'does not strip session set-cookie' do
            get :radar_token

            refute request.env['rack.session.options'][:skip]
          end
        end

        describe_with_arturo_enabled :v2_users_radar_token_strip_session_set_cookie do
          it 'strips session set-cookie' do
            get :radar_token

            assert request.env['rack.session.options'][:skip]
          end
        end
      end
    end
  end

  as_an_admin do
    describe 'with valid external domain' do
      it 'returns a valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://dashboard.zopim.com'
        get :show
        assert_equal response.headers['Access-Control-Allow-Origin'], 'http://dashboard.zopim.com'
      end

      it 'returns a not valid allow-origin header with an allowed external domain as origin' do
        @controller.request.env['HTTP_ORIGIN'] = 'http://dashboard.zopim.com.wearehackers.com'
        get :show
        assert_nil response.headers['Access-Control-Allow-Origin']
      end
    end

    describe "a GET to :show" do
      before { get :show }
      should_use_presenter Api::V2::Users::AgentPresenter
    end

    describe "a GET to :show" do
      describe_with_arturo_disabled :current_user_show_cache_control_header do
        it "does perform heavy SQL queries" do
          @sql_queries = sql_queries do
            get :show
          end
          assert_sql_queries 16..20
        end
      end
    end

    describe "a GET to :show" do
      describe_with_arturo_enabled :current_user_show_cache_control_header do
        it "does perform heavy SQL queries when sideloads present" do
          @sql_queries = sql_queries do
            get :show, params: { include: 'abilities,roles,groups' }
          end
          assert_sql_queries 16..20
        end

        describe "for requests from apps" do
          before { @controller.stubs(:app_request?).returns(true) }

          describe 'when etag is stale' do
            before do
              get :show
              etag = @response.headers['ETag']

              @sql_queries = sql_queries do
                @request.headers['HTTP_IF_NONE_MATCH'] = etag + 'STALE'
                get :show
              end
            end

            it "does not perform heavy SQL queries, only rack middleware ones" do
              assert_sql_queries 2..6
            end
            it "sets cache-control max-age 60" do
              get :show
              assert_match /max-age=60/, @response.headers['Cache-Control']
              assert_match /private(,|$)/, @response.headers['Cache-Control']
              assert_equal "origin", @response.headers['Vary']
            end
            it('responds with success') { assert_response :success }
          end

          describe 'when etag is not stale' do
            before do
              get :show
              etag = @response.headers['ETag']

              @sql_queries = sql_queries do
                @request.headers['HTTP_IF_NONE_MATCH'] = etag
                get :show
              end
            end

            it "does not perform heavy SQL queries, only rack middleware ones" do
              assert_sql_queries 2..6
            end
            it "sets cache-control max-age 60" do
              get :show
              assert_match /max-age=60/, @response.headers['Cache-Control']
              assert_match /private(,|$)/, @response.headers['Cache-Control']
              assert_equal "origin", @response.headers['Vary']
            end
            it('responds with not modified') { assert_response :not_modified }
          end
        end

        describe "for requests from Lotus or API" do
          before { @controller.stubs(:app_request?).returns(false) }

          it "does not set cache-control" do
            get :show
            assert_nil @response.headers['Cache-Control']
          end
        end
      end
    end
  end
end
