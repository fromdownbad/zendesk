require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::TargetFailuresController do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :targets, :role_settings

  let(:account) { accounts(:minimum) }
  let(:target) { targets(:url_valid) }
  let(:target_failure) { new_target_failure }

  before do
    accept :json
  end

  with_options(controller: "api/v2/target_failures") do |request|
    request.should_route :get, "/api/v2/target_failures", action: "index"
    request.should_route :get, "/api/v2/target_failures/1", action: "show", id: 1
  end

  as_an_end_user do
    should_be_forbidden :index, :show
  end

  describe_with_arturo_disabled :allow_agents_to_access_targets do
    as_an_agent do
      should_be_forbidden :index, :show
    end

    as_an_admin do
      describe "a GET to #index" do
        before do
          10.times.each { new_target_failure }
          get :index
        end

        should_use_presenter Api::V2::TargetFailurePresenter, status: :ok

        it "is ordered by created_at DESC" do
          target_failures = JSON.parse(@response.body)["target_failures"].map { |tf| tf["created_at"] }
          assert_equal target_failures.sort.reverse, target_failures
        end
      end

      describe "a GET to #show" do
        before do
          get :show, params: { id: target_failure.id }
        end

        should_use_presenter Api::V2::TargetFailurePresenter, status: :ok

        it "returns target_failure with raw_request and raw_response" do
          target_failure = JSON.parse(@response.body)["target_failure"]

          assert target_failure.key?("raw_request")
          assert target_failure.key?("raw_response")
        end
      end
    end
  end

  describe_with_arturo_enabled :allow_agents_to_access_targets do
    as_an_agent do
      describe "a GET to #index" do
        before do
          10.times.each { new_target_failure }
          get :index
        end

        should_use_presenter Api::V2::TargetFailurePresenter, status: :ok

        it "is ordered by created_at DESC" do
          target_failures = JSON.parse(@response.body)["target_failures"].map { |tf| tf["created_at"] }
          assert_equal target_failures.sort.reverse, target_failures
        end
      end

      describe "a GET to #show" do
        before do
          get :show, params: { id: target_failure.id }
        end

        should_use_presenter Api::V2::TargetFailurePresenter, status: :ok

        it "returns target_failure with raw_request and raw_response" do
          target_failure = JSON.parse(@response.body)["target_failure"]

          assert target_failure.key?("raw_request")
          assert target_failure.key?("raw_response")
        end
      end
    end
  end

  private

  def new_target_failure
    account.target_failures.create(
      target: target,
      raw_http_capture: target.raw_http_capture
    )
  end
end
