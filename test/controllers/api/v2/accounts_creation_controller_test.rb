require_relative '../../../support/test_helper'
require_relative '../../../support/pre_account_creation_helper'

SingleCov.covered!

describe Api::V2::AccountsCreationController do
  extend Api::V2::TestHelper
  include PreAccountCreationHelper

  fixtures :all

  let(:account_subdomain) { "wombatsnowboards#{(rand * 10000).round}" }
  let(:account_name) { 'Wombat Snowboards' }
  let(:language) { 'ja' }
  let(:account_params) do
    {
      account: {
        name:           account_name,
        subdomain:      account_subdomain,
        help_desk_size: 'Small Team',
        language:       language,
        source:         'classic'
      },
      owner: {
        name:  'Sir Wombat',
        email: 'wbsnowboards@tz.com'
      },
      address: {
        phone: '+1-123-456-7890'
      },
      trial_extras: {}
    }
  end
  let(:original_remote_ip) { '0.0.0.0' }
  let(:new_account_params) do
    account_params.merge(
      confirm_origin: 'zendesk_internal_origin',
      original_remote_ip: original_remote_ip,
      region: 'us'
    )
  end
  let(:params_with_force_pod_id) { new_account_params.merge 'force_pod_id' => '2' }
  let(:response_body) do
    {
      'account' => {
        'url'          => "https://#{params_subdomain}.zendesk-test.com",
        'name'         => account_name,
        'sandbox'      => false,
        'subdomain'    => params_subdomain,
        'time_format'  => 24,
        'time_zone'    => 'Eastern Time (US & Canada)',
        'owner_id'     => account.owner_id,
        'multiproduct' => account.multiproduct?
      },
      'success' => true,
      'owner_verification_link' => "https://#{params_subdomain}.zendesk-test.com/verification/challenge/#{challenge_token}"
    }
  end
  let(:params_subdomain) { account_params[:account][:subdomain] }
  let(:account) { Account.find_by_subdomain(params_subdomain) }
  let(:challenge_token) { account.owner.challenge_tokens.last.value }
  let(:statsd_client) { @controller.send(:statsd_client) }

  before do
    Timecop.freeze
    Rails.logger.stubs(:info)
    accept :json
    AccountCreationShard.stubs(:pluck).returns([1])
    Resque.stubs(:enqueue).returns(true)
  end

  with_options(controller: 'api/v2/accounts_creation') do |request|
    request.should_route :post, 'api/v2/internal/new_account', action: 'create_new_account'
  end

  describe 'POST :create_new_account' do
    describe 'without confirm_origin param' do
      before do
        statsd_client.expects(:increment).with('create_action', anything)
        statsd_client.expects(:increment).with('validation_failure', tags: ["reason: confirm_zendesk_origin"])
      end

      it 'returns status code forbidden' do
        post :create_new_account, params: account_params
        assert_response 403
      end
    end

    describe 'with confirm_origin param' do
      describe 'creates a new account' do
        before do
          statsd_client.expects(:increment).with('create_action', anything)
          statsd_client.expects(:increment).with('success', tags: ['fast_account_creation:false', 'is_serviceable:true'])
          post :create_new_account, params: new_account_params
        end

        it 'succeeds' do
          assert_response :success
        end

        it 'creates an account with appropriate params on the account' do
          assert_not_nil account
          assert_equal params_subdomain, account.subdomain
          assert_equal "wbsnowboards@tz.com", account.owner.email
          assert_equal 'classic', account.source
        end

        it 'returns the correct response body' do
          actual = JSON.load(@response.body)
          assert_equal response_body, actual
        end

        it "creates an account and set the i18n locale" do
          assert_equal 'ja-x-4', I18n.previous_request_locale.to_s
        end
      end

      describe 'with creation_channel param' do
        before do
          new_account_params[:creation_channel] = 'website123'
          statsd_client.expects(:increment).with('create_action', anything)
          statsd_client.expects(:increment).with('success', tags: ['fast_account_creation:false', 'is_serviceable:true'])
          post :create_new_account, params: new_account_params
        end

        it 'set the creation_channel setting on the new account' do
          assert_response :success
          assert_equal 'website123', account.settings.creation_channel
        end
      end

      describe 'with original_remote_ip' do
        let(:original_remote_ip) { '172.18.0.7' }

        before do
          statsd_client.expects(:increment).with('create_action', anything)
          statsd_client.expects(:increment).with('success', tags: ['fast_account_creation:false', 'is_serviceable:true'])
          post :create_new_account, params: new_account_params
        end

        it 'sets the new accounts created_from_ip_address correctly' do
          assert_equal original_remote_ip, account.settings.created_from_ip_address
        end
      end

      describe 'with force_pod_id' do
        let(:redirect_pod) { Zendesk::Configuration.fetch(:pod_id) + 1 }

        it 'when not specified, pod is selected dynamically' do
          Account.expects(:select_pod).returns(redirect_pod)

          post :create_new_account, params: new_account_params
          assert_response :success
          assert_match %r{@pod_#{redirect_pod}\D}, @response.headers['X-Accel-Redirect']
        end

        it 'when specified to a non-numeric value, pod is selected dynamically' do
          new_account_params[:force_pod_id] = 'asdf'
          Account.expects(:select_pod).returns(redirect_pod)

          post :create_new_account, params: new_account_params
          assert_response :success
          assert_match %r{@pod_#{redirect_pod}\D}, @response.headers['X-Accel-Redirect']
        end

        it 'when specified to an integer, it is used' do
          new_account_params[:force_pod_id] = redirect_pod.to_s
          Account.expects(:select_pod).never

          post :create_new_account, params: new_account_params
          assert_response :success
          assert_match %r{@pod_#{redirect_pod}\D}, @response.headers['X-Accel-Redirect']
        end
      end

      describe 'not serviceable error handling' do
        before do
          Accounts::Classic.any_instance.stubs(valid?: true, is_serviceable: false)
          statsd_client.expects(:increment).with('create_action', anything)
          statsd_client.expects(:increment).with('failure', tags: ['fast_account_creation:false', 'error:ActiveRecord::RecordInvalid'])
          post :create_new_account, params: new_account_params
        end

        it "validates and logs that the resulting account is not serviceable" do
          assert_response :unprocessable_entity
          response_body = JSON.parse(@response.body)
          assert_equal [{'description' => 'account is not serviceable'}], response_body['details']['base']
        end
      end
    end

    describe "with list of agents" do
      let(:agents) do
        [
          {name: "Goober McFaden", email: "intestinal@snacks.com", role: "Agent"},
          {name: "Willis Jethro", email: "wonton_soup@snacks.com", role: "Admin"},
          {name: "GG Whalen", email: "rolls@snacks.com", role: "Agent"}
        ]
      end

      it 'enqueues UserBulkCreateJob to create agents' do
        UserBulkCreateJob.expects(:enqueue).with do |arg|
          /\d+/.match(arg[:account_id].to_s) &&
          /\d+/.match(arg[:user_id].to_s) &&
          arg[:users].size == 3 &&
          arg[:users][0][:name] == "Goober McFaden" &&
          arg[:users][0][:via] == 'classic' &&
          /\d+/.match(arg[:users][0][:organization_id].to_s)
        end
        Zendesk::GeoLocation.stubs(:locate).returns(country_code: 'USA', region: 'Americas', timezone: "America/Los_Angeles")
        post :create_new_account, params: new_account_params.merge!(agents: agents)
      end

      it "sets the organization id to the default org on the created agents" do
        post :create_new_account, params: new_account_params.merge!(agents: agents)
        account_default_org_id = account.organizations.first.id
        account.agents.each { |a| assert_equal account_default_org_id, a.organization_id }
      end
    end

    describe "with invalid params" do
      let(:invalid_params) do
        {
          account: {
            name: account_name,
            subdomain: "testsub#{(rand * 1000).round}",
            help_desk_size: "Small team",
            source: Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET,
            language: "pt-BR"
          },
          owner: {
            name: "Yolo Gucci",
            email: "yolo@snacks.com",
            password: "yolo@snacks.com"
          },
          address: {
            phone: "+1-123-456-7890"
          },
          confirm_origin: 'zendesk_internal_origin',
          original_remote_ip: original_remote_ip,
          region: 'us'
        }
      end

      describe 'uses Api::V2::ErrorsPresenter' do
        let (:shards) { anything }

        before do
          AccountCreationShard.stubs(:where).returns(shards)
          shards.stubs(:reload).returns(shards)
          shards.stubs(:pluck).returns([1])
          ZendeskExceptions::Logger.expects(:record).with(instance_of(ActiveRecord::RecordInvalid), instance_of(Hash))
          post :create_new_account, params: invalid_params
        end

        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
        should_not_change("the number of accounts") { Account.count(:all) }
      end

      it 'returns failures without calling save to prevent after_commit calls on invalid accounts' do
        post :create_new_account, params: invalid_params

        assert_response :unprocessable_entity
        refute JSON.parse(@response.body)['success']

        stub_account = stub('new_account')
        @controller.stubs(:new_account).returns(stub_account)
        @controller.expects(:validate_new_account).never
        stub_account.expects(:save!).never

        assert_nil account
      end
    end

    describe "with partner settings params" do
      let(:params) do
        new_account_params.merge(
          partner: {
            name: 'Mini Me',
            url:  'http://minime.example.com',
          }
        )
      end

      before do
        post :create_new_account, params: params
      end

      it('responds with created') { assert_response :created }

      it "stores the partner setting values" do
        settings = account.settings
        assert_equal 'Mini Me', settings.partner_name
        assert_equal 'http://minime.example.com', settings.partner_url
      end
    end

    describe "with a bunch of marketing extras" do
      let(:params) do
        new_account_params.merge(
          elqFormName: "something",
          elqCustomerGUID: "",
          elqCookieWrite: "0",
          elqSiteID: "123456789",
          Web_Offer_Name__c: "Inbox by Zendesk ??? The Free Collaborative Email for Teams",
          GA_Landing: "https://www.zendesk.com/register/#getstarted",
          GA_Referrer: "https://www.zendesk.com/",
          GA_Medium: "(none)",
          GA_Source: "(direct)",
          Opti_ID: "abc1234567",
          Opti_Variation_ID: "{\"1234567\":\"98767652\"}",
          trial_extras: {"some_field" => "abc", "another_field" => "whatever"},
          ZOBU_1: "",
          ZOBU_2: "",
          ZOBU_3: "ZOBU 1A",
          FirstName: "unknown",
          LastName: "unknown",
          created: "true"
        )
      end

      before do
        post :create_new_account, params: params
      end

      it('responds with created') { assert_response :created }
    end

    describe "with suite_trial param" do
      before do
        SuiteTrialJob.stubs(:enqueue)
        post :create_new_account, params: params
      end

      describe "with boolean param" do
        let(:params) do
          new_account_params.tap do |params|
            params[:account][:suite_trial] = true
          end
        end

        it('responds with created') { assert_response :created }
      end

      describe "with valid string param" do
        let(:params) do
          new_account_params.tap do |params|
            params[:account][:suite_trial] = 'true'
          end
        end

        it('responds with created') { assert_response :created }
      end
    end

    describe "with remote authentication params" do
      let(:params) do
        new_account_params.tap do |params|
          params[:account][:remote_authentication] = {}
        end
      end

      describe "when auth type is SAML" do
        let(:remote_authentication_params) do
          {
            type: 'saml',
            remote_login_url: 'https://saml.example.com/login',
            remote_logout_url: 'https://saml.example.com/logout',
            fingerprint: 'SAML-SAML-SAML-SAML-SAML-SAML-SAML-SAML!'
          }
        end

        let(:remote_authentication) do
          account.remote_authentications.saml.first
        end

        describe "with valid remote auth settings" do
          before do
            params[:account][:remote_authentication].
              merge!(remote_authentication_params)
            post :create_new_account, params: params
          end

          it('responds with created') { assert_response :created }

          it "activates the SAML remote authentication" do
            assert remote_authentication.is_active?
          end

          it "configures the SAML remote authentication record" do
            assert_equal 'https://saml.example.com/login', remote_authentication.remote_login_url
            assert_equal 'https://saml.example.com/logout', remote_authentication.remote_logout_url
            assert_equal 'SAML-SAML-SAML-SAML-SAML-SAML-SAML-SAML!', remote_authentication.token
          end
        end

        describe "with invalid remote auth settings" do
          before do
            params[:account][:remote_authentication].
              merge!(remote_authentication_params).
              merge!(
                remote_login_url: '',
                fingerprint: 'too-short'
              )
            post :create_new_account, params: params
          end

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
        end
      end

      describe "when auth type is JWT" do
        let(:remote_authentication_params) do
          {
            type: 'jwt',
            remote_login_url: 'https://jwt.example.com/login',
          }
        end

        let(:remote_authentication) do
          account.remote_authentications.jwt.first
        end

        describe "with valid remote auth settings" do
          before do
            params[:account][:remote_authentication].
              merge!(remote_authentication_params)
            post :create_new_account, params: params
          end

          it('responds with created') { assert_response :created }

          it "activates the JWT remote authentication" do
            assert remote_authentication.is_active?
          end

          it "configures the JWT remote authentication record" do
            assert_equal 'https://jwt.example.com/login', remote_authentication.remote_login_url
          end

          it "uses a generated token value" do
            assert_equal 48, remote_authentication.token.length
          end

          it "enables api_token_access settings" do
            settings = account.settings
            assert settings.api_token_access
          end

          it "sets the api_token crypted_value" do
            refute_nil account.api_tokens.last.crypted_value
          end
        end

        describe "with invalid remote auth settings" do
          before do
            params[:account][:remote_authentication].
              merge!(remote_authentication_params)[:remote_login_url] = ''
            post :create_new_account, params: params
          end

          it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
        end
      end

      describe 'when auth type is not supported' do
        before do
          params[:account][:remote_authentication] = {
            type: 'not-supported'
          }
          post :create_new_account, params: params
        end

        it('responds with created') { assert_response :created }

        it "does not activate any of the remote authentications" do
          account.remote_authentications.each { |a| refute a.is_active? }
        end
      end

      describe 'not serviceable error handling' do
        before do
          Accounts::Classic.any_instance.stubs(valid?: true, is_serviceable: false)
          statsd_client.expects(:increment).with('create_action', anything)
          statsd_client.expects(:increment).with('failure', tags: ['fast_account_creation:false', 'error:ActiveRecord::RecordInvalid'])
          post :create_new_account, params: new_account_params
        end

        it "validates and logs that the resulting account is not serviceable" do
          assert_response :unprocessable_entity
          response_body = JSON.parse(@response.body)
          assert_equal [{'description' => 'account is not serviceable'}], response_body['details']['base']
        end
      end

      describe 'with precreated accounts' do
        let(:redis_client) { Zendesk::RedisStore.redis_client = FakeRedis::Redis.new }
        let(:support_trial) do
          account = accounts(:support)
          account.subscription.update_column(:is_trial, true)
          Accounts::Base.find(account.id)
        end
        let(:minimum_trial) do
          account = accounts(:minimum)
          account.subscription.update_column(:is_trial, true)
          Accounts::Base.find(account.id)
        end
        let(:us_locale) { DefaultLocaleChooser.choose('en-US') }
        let(:ja_locale) { DefaultLocaleChooser.choose('ja') }
        let(:precreated_us_account) do
          get_precreated_account(name: 'precreation_one', account_id: support_trial.id, target_locale_id: us_locale.id)
        end
        let(:precreated_ja_account) do
          get_precreated_account(name: 'precreation_two', account_id: minimum_trial.id, target_locale_id: ja_locale.id)
        end

        before do
          assert_equal 2, [precreated_us_account, precreated_ja_account].compact.count
          assert_equal 2, PreAccountCreation.pluck(:id).uniq.count
          Account.any_instance.stubs(:redis_client).returns(redis_client)
        end

        {
          'classic' => true,
          Zendesk::Accounts::Source::MAGENTO => false,
          Zendesk::Accounts::Source::GOOGLE_APP_MARKET => true
        }.each do |source_type, has_precreation|
          describe "with #{source_type} account params" do
            it "ensures only classic and Google App Market uses a pre-created account" do
              new_account_params[:account][:source] = source_type
              post :create_new_account, params: new_account_params

              assert_not_nil account
              assert_equal account_subdomain, account.subdomain
              assert_equal source_type, account.source

              pre_account_creation = PreAccountCreation.unscoped.where(account_id: account.id).first
              if has_precreation
                refute_nil pre_account_creation
              else
                assert_nil pre_account_creation
              end
            end
          end
        end

        it 'uses only redis_trial_extras' do
          AccountsPrecreationJob.stubs(:work)
          TrialExtrasJob.stubs(:work).returns(true)
          Account.any_instance.expects(:trial_extras).never
          Accounts::Base.any_instance.expects(:trial_extras).never
          Accounts::Base.any_instance.expects(:redis_trial_extras).returns(is_fast_account_creation: true).at_least_once

          post :create_new_account, params: new_account_params
          assert_not_nil account
        end

        it 'ensures ja account uses pre-created account with ja locale' do
          post :create_new_account, params: new_account_params

          refute_nil account
          pre_account_creation = PreAccountCreation.unscoped.where(account_id: account.id).first
          assert_equal pre_account_creation.id, precreated_ja_account.id
          assert_equal DefaultLocaleChooser.choose(language).id, account.locale_id
        end

        describe 'when account locale is en-US' do
          let(:language) { 'en-US' }

          it 'ensures us account uses pre-created account with us locale' do
            post :create_new_account, params: new_account_params

            refute_nil account
            pre_account_creation = PreAccountCreation.unscoped.where(account_id: account.id).first
            assert_equal pre_account_creation.id, precreated_us_account.id
            assert_equal DefaultLocaleChooser.choose(language).id, account.locale_id
          end
        end

        it 'ensures new account has 30 days of trials' do
          minimum_trial.subscription.update_column(:trial_expires_on, Date.today - 10.days)
          post :create_new_account, params: new_account_params

          assert_equal minimum_trial.id, account.id
          assert_equal Date.today + Subscription::TRIAL_DURATION, account.subscription.trial_expires_on
        end

        describe "and owner[is_verified]=true is in the params" do
          before do
            new_account_params[:owner][:is_verified] = true
          end

          it 'ignores the parameter if the arturo to do so is on' do
            Arturo.enable_feature!(:create_owner_as_unverified)
            post :create_new_account, params: new_account_params

            assert_response :created
            assert_equal false, account.owner.is_verified?
          end

          it 'creates the account with a verified owner if the arturo is off' do
            Arturo.disable_feature!(:create_owner_as_unverified)
            post :create_new_account, params: new_account_params

            assert_response :created
            assert(account.owner.is_verified?)
          end
        end

        describe "and owner[is_verified]=1 is in the params" do
          before do
            new_account_params[:owner][:is_verified] = 1
          end

          it 'ignores the parameter if the arturo to do so is on' do
            Arturo.enable_feature!(:create_owner_as_unverified)
            post :create_new_account, params: new_account_params.merge(format: :json)

            assert_response :created
            assert_equal false, account.owner.is_verified?
          end

          it 'creates the account with a verified owner if the arturo is off' do
            Arturo.disable_feature!(:create_owner_as_unverified)
            post :create_new_account, params: new_account_params.merge(format: :json)

            assert_response :created
            assert(account.owner.is_verified?)
          end
        end

        it 'calls new_account' do
          @controller.expects(:new_account).at_least(1).returns(minimum_trial)
          post :create_new_account, params: new_account_params
        end

        it 'never calls new account if pod redirect is required' do
          Account.expects(:select_pod).returns(-1)
          @controller.expects(:can_relay_request?).at_least_once.returns(true)
          @controller.expects(:new_account).never

          post :create_new_account, params: new_account_params
        end
      end

      describe "when setting currency" do
        describe "without a currency parameter" do
          describe "without a country_code" do
            before do
              post :create_new_account, params: new_account_params
              assert_response 201, @response.body
            end

            it "creates an account with CurrencyType.USD as the currency_type on the account subscription" do
              assert_equal CurrencyType.USD, account.subscription.currency_type
            end
          end

          describe "with a valid country_code" do
            before do
              Zendesk::GeoLocation.stubs(:locate).returns(country_code: "NL")
              post :create_new_account, params: new_account_params
              assert_response 201, @response.body
            end

            it "creates an account with CurrencyType.EUR as the currency_type on the account subscription" do
              assert_equal CurrencyType.EUR, account.subscription.currency_type
            end
          end

          describe "with an invalid country_code" do
            before do
              Zendesk::GeoLocation.stubs(:locate).returns(country_code: "invalid")
              post :create_new_account, params: new_account_params
              assert_response 201, @response.body
            end

            it "creates an account with CurrencyType.USD as the default currency type" do
              assert_equal CurrencyType.USD, account.subscription.currency_type
            end
          end

          describe "with a currency param" do
            before do
              new_account_params[:account][:currency] = 'GBP'
              post :create_new_account, params: new_account_params
              assert_response 201, @response.body
            end

            it "creates an account with CurrencyType.GBP as the currency_type on the account subscription" do
              assert_equal CurrencyType.GBP, account.subscription.currency_type
            end
          end

          describe "with an invalid currency param" do
            before do
              new_account_params[:account][:currency] = 'invalid'
              post :create_new_account, params: new_account_params
              assert_response 201, @response.body
            end

            it "creates an account with CurrencyType.USD as the default currency_type on the account subscription" do
              assert_equal CurrencyType.USD, account.subscription.currency_type
            end
          end
        end
      end
    end
  end
end
