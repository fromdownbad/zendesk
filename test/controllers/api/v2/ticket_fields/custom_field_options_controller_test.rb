require_relative "../../../../support/test_helper"
require_relative "../../../../support/custom_field_options_test_helper"

SingleCov.covered!

describe Api::V2::TicketFields::CustomFieldOptionsController do
  extend Api::V2::TestHelper
  include CustomFieldOptionsTestHelper

  fixtures :all

  before do
    accept :json

    @parent_field = ticket_fields(:field_tagger_custom)
    @custom_field_option = custom_field_options(:field_tagger_custom_option_1)
  end

  with_options(controller: "api/v2/ticket_fields/custom_field_options") do |request|
    request.should_route :get,    "/api/v2/ticket_fields/1/options",     action: "index", ticket_field_id: "1"
    request.should_route :get,    "/api/v2/ticket_fields/1/options/1",   action: "show", ticket_field_id: "1", id: "1"
    request.should_route :post,   "/api/v2/ticket_fields/1/options",     action: "create_or_update", ticket_field_id: "1"
    request.should_route :delete, "/api/v2/ticket_fields/1/options/1",   action: "destroy", ticket_field_id: "1", id: "1"
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index, {ticket_field_id: 1}]
    should_be_unauthorized [:get, :show, {ticket_field_id: 1, id: 1}]
    should_be_unauthorized [:post, :create_or_update, {ticket_field_id: 1}]
    should_be_unauthorized [:delete, :destroy, {ticket_field_id: 1, id: 1}]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index, {ticket_field_id: 1}]
    should_be_forbidden [:get, :show, {ticket_field_id: 1, id: 1}]
    should_be_forbidden [:post, :create_or_update, {ticket_field_id: 1}]
    should_be_forbidden [:delete, :destroy, {ticket_field_id: 1, id: 1}]
  end

  as_an_admin do
    describe "a GET to :index" do
      before do
        get :index, params: { ticket_field_id: @parent_field.id }
      end

      it "returns the custom field's options" do
        assert_matches_custom_field_options(@parent_field)
      end

      should_use_presenter Api::V2::CustomFieldOptionPresenter
    end

    describe "a GET to :show" do
      before do
        get :show, params: { ticket_field_id: @parent_field.id, id: @custom_field_option.id }
      end

      it "returns the custom field option" do
        assert_matches_custom_field_option(@custom_field_option)
      end

      should_use_presenter Api::V2::CustomFieldOptionPresenter
    end

    describe "a POST to :create_or_update" do
      describe "with an unused value" do
        before do
          Account.any_instance.expects(:expire_scoped_cache_key).with(:settings).once
          Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_fields).once
          Account.any_instance.expects(:expire_scoped_cache_key).with(:ticket_forms).once
          Account.any_instance.expects(:expire_scoped_cache_key).with(:field_taggers).once
          Account.any_instance.expects(:expire_scoped_cache_key).with([:field_tagger, @parent_field.id]).once

          post :create_or_update, params: { ticket_field_id: @parent_field.id, custom_field_option: { name: 'my name', value: 'my_value' } }
        end

        it "creates a new option" do
          assert_creates_custom_field_option
        end

        should_use_presenter Api::V2::CustomFieldOptionPresenter
      end

      describe "with a matching id" do
        before do
          @params = { name: 'my new name', value: 'my_new_value' }
          post :create_or_update, params: { ticket_field_id: @parent_field.id, custom_field_option: { id: @custom_field_option.id }.merge(@params) }
        end

        it "updates the existing option's value" do
          assert_updates_existing_custom_field_option(@custom_field_option, @params)
        end

        should_use_presenter Api::V2::CustomFieldOptionPresenter
      end

      describe "when sent the id of an existing CustomFieldOption" do
        it "should not allow spaces in the value" do
          @params = { name: 'my new name', value: 'my spaced value' }
          post :create_or_update, params: { ticket_field_id: @parent_field.id, custom_field_option: { id: @custom_field_option.id }.merge(@params) }

          assert_response :unprocessable_entity
        end
      end

      describe "with an invalid id" do
        before do
          post :create_or_update, params: { ticket_field_id: @parent_field.id, custom_field_option: { id: 999, name: 'my name', value: 'my_other_new_value' } }
        end

        it "returns an error" do
          assert_handles_invalid_custom_field_option_update(@custom_field_option)
        end
      end

      describe "without a given position" do
        before do
          @options_count = @parent_field.custom_field_options.count
          post :create_or_update, params: { ticket_field_id: @parent_field.id, custom_field_option: { name: 'my name', value: 'my_other_other_new_value' } }
        end

        it "correctly sets one for the caller" do
          assert_equal @options_count, JSON.parse(@response.body)['custom_field_option']['position']
        end
      end

      describe "with a deleted option" do
        before do
          @custom_field_option.mark_as_deleted
          @custom_field_option.save!
          post :create_or_update, params: { ticket_field_id: @parent_field.id, custom_field_option: { name: 'my name', value: @custom_field_option.value } }
        end

        it "restores the option" do
          assert_equal @custom_field_option.id, JSON.parse(@response.body)['custom_field_option']['id']
        end

        should_use_presenter Api::V2::CustomFieldOptionPresenter
      end

      describe "with invalid params" do
        describe "empty body" do
          before do
            post :create_or_update, params: { ticket_field_id: @parent_field.id }
          end

          it "responds with bad_request" do
            assert_response :bad_request
          end
        end

        describe "empty name" do
          before do
            post :create_or_update, params: { ticket_field_id: @parent_field.id, custom_field_option: { value: 'some value' } }
          end

          it "responds with bad_request" do
            assert_response :bad_request
          end
        end

        describe "invalid param" do
          before do
            post :create_or_update, params: { ticket_field_id: @parent_field.id, invalid_param: "invalid_param" }
          end

          it "responds with bad_request" do
            assert_response :bad_request
          end
        end
      end

      describe "when there are conditions using options" do
        let(:account) { accounts(:minimum) }
        let(:ticket_form) { ticket_forms(:minimum_ticket_form) }
        let(:parent_field) { ticket_fields(:field_tagger_custom) }
        let(:child_field) { ticket_fields(:field_textarea_custom) }
        let(:cfo1) { custom_field_options(:field_tagger_custom_option_1) }
        let(:cfo2) { custom_field_options(:field_tagger_custom_option_2) }
        let(:user) { users(:minimum_admin) }
        let(:condition) do
          condition = TicketFieldCondition.new.tap do |c|
            c.account = account
            c.ticket_form = ticket_form
            c.parent_field = parent_field
            c.child_field = child_field
            c.value = cfo2.value
            c.user_type = :agent
          end
          condition.save

          condition
        end

        before do
          ticket_form.account_id = account.id
          Account.any_instance.stubs(:has_ticket_forms?).returns(true)
          Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(true)

          TicketFormField.create(
            ticket_form: ticket_form,
            account: account,
            ticket_field: parent_field,
            position: 1
          )
          TicketFormField.create(
            ticket_form: ticket_form,
            account: account,
            ticket_field: child_field,
            position: 2
          )

          condition
        end

        it "doesn't allow value updates on options used in conditions" do
          post :create_or_update, params: { ticket_field_id: @parent_field.id, custom_field_option: {
            id: cfo2.id, name: cfo2.name, value: "changed"
          } }

          assert_response :unprocessable_entity
          response = JSON.parse(@response.body)
          assert_equal "Value #{I18n.t('txt.admin.models.ticket_field.ticket_field.option_cannot_be_changed_by_condition', option_name: cfo2.name)}", response["details"]["value"].first["description"]
        end

        it "allows name updates on options used in conditions" do
          post :create_or_update, params: { ticket_field_id: @parent_field.id, custom_field_option: {
            id: cfo2.id, name: "changed", value: cfo2.value
          } }
          assert_response :ok
        end

        it "allows value updates on other options not used in conditions" do
          post :create_or_update, params: { ticket_field_id: @parent_field.id, custom_field_option: {
            id: cfo1.id, name: cfo1.name, value: "changed"
          } }
          assert_response :ok
        end

        it "doesn't allow deleting an option in conditions" do
          delete :destroy, params: { ticket_field_id: @parent_field.id, id: cfo2.id }

          assert_response :unprocessable_entity
          response = JSON.parse(@response.body)
          assert_equal "Value #{I18n.t('txt.admin.models.ticket_field.ticket_field.option_cannot_be_changed_by_condition', option_name: cfo2.name)}", response["details"]["value"].first["description"]
        end

        it "allows deleting other options not used in conditions" do
          delete :destroy, params: { ticket_field_id: @parent_field.id, id: cfo1.id }
          assert_deletes_custom_field_option(cfo1, CustomFieldOption)
        end
      end
    end

    describe "a DELETE to :destroy" do
      before do
        delete :destroy, params: { ticket_field_id: @parent_field.id, id: @custom_field_option.id }
      end

      it "deletes the option" do
        assert_deletes_custom_field_option(@custom_field_option, CustomFieldOption)
      end
    end
  end
end
