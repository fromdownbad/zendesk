require_relative "../../../support/test_helper"
require_relative "../../../support/api_test_helper"
require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered! uncovered: 9

describe Api::V2::BaseController do
  fixtures :all

  should_route :get, "/api/v2/nonexistant", controller: "api/v2/errors", action: "routing", path: "nonexistant"

  it 'routes OPTIONS requests to the CORS controller' do
    assert_recognizes(
      { controller: 'api/v2/cors', action: 'preflight', path: 'users/me' },
      { path: 'api/v2/users/me', method: :options }
    )

    assert_recognizes(
      { controller: 'api/v2/cors', action: 'preflight', path: 'users/me' },
      { path: 'api/v2/users%2Fme', method: :options }
    )

    assert_generates('api/v2/users%2Fme', controller: 'api/v2/cors', action: 'preflight', path: 'users/me')
  end

  def self.should_map(symbol, from, to)
    it "maps #{symbol} #{from} -> #{to}" do
      @controller.stubs(params: { symbol => from })
      assert_equal_with_nil to, @controller.send(symbol)
    end
  end

  class ApiV2TestController < Api::V2::BaseController
    allow_anonymous_users :anonymous
    skip_before_action :require_agent!, only: :anonymous
    allow_cors :cors_error
    allow_system_bearer_user :zopim, only: :bearer_protected
    require_system_bearer_user :zopim, only: :bearer_required
    around_action :forbid_nil_parameters, only: :params_not_nil

    allow_parameters :index, {}
    def index
      render json: { title: "Hello", created_at: User.last.created_at }
    end

    allow_parameters :create, {}
    def create
      head :ok
    end

    allow_parameters :update, {}
    def update
      head :ok
    end

    allow_parameters :destroy, {}
    def destroy
      head :ok
    end

    allow_parameters :anonymous, {}
    def anonymous
      head :ok
    end

    allow_parameters :bearer_protected, {}
    def bearer_protected
      head :ok
    end

    allow_parameters :bearer_required, {}
    def bearer_required
      head :ok
    end

    allow_parameters :cors_error, {}
    def cors_error
      raise ActiveRecord::RecordNotFound
    end

    allow_parameters :i18n, {}
    def i18n
      render plain: I18n.t('txt.email.footer')
    end

    allow_parameters :raise_type_mismatch, {}
    def raise_type_mismatch
      raise ActiveRecord::AssociationTypeMismatch
    end

    allow_parameters :raise_readonly_error, {}
    def raise_readonly_error
      raise ActiveRecord::ReadOnlyRecord
    end

    allow_parameters :raise_race_condition_error, {}
    def raise_race_condition_error
      raise ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, 'test'
    end

    allow_parameters :raise_deadlock_found_error, {}
    def raise_deadlock_found_error
      raise ZendeskDatabaseSupport::MappedDatabaseExceptions::DeadlockFound, 'test'
    end

    allow_parameters :raise_lock_wait_timeout_error, {}
    def raise_lock_wait_timeout_error
      raise ZendeskDatabaseSupport::MappedDatabaseExceptions::LockWaitTimeout, 'test'
    end

    allow_parameters :raise_too_many_jobs_error, {}
    def raise_too_many_jobs_error
      raise JobWithStatusTracking::TooManyJobs.new 'BulkTest', ['foo', 'bar']
    end

    allow_parameters :raise_upload_failure_error, {}
    def raise_upload_failure_error
      raise ::Technoweenie::AttachmentFu::NoBackendsAvailableException
    end

    allow_parameters :raise_archived_tickets_failure_error, {}
    def raise_archived_tickets_failure_error
      raise ZendeskArchive::DisabledDatastoreError
    end

    allow_parameters :params_not_nil, {}
    def params_not_nil
      render plain: "allow_nil_for_everything = #{ActionController::Parameters.allow_nil_for_everything}"
    end

    allow_parameters :raise_invalid_pagination_parameter, {}
    def raise_invalid_pagination_parameter
      raise Zendesk::CursorPagination::InvalidPaginationParameter, 'test'
    end
  end

  class ApiV2CleanController < Api::V2::BaseController
  end

  tests ApiV2TestController
  use_test_routes

  include ApiTestHelper

  before do
    @account = @request.account = accounts(:minimum)
  end

  as_an_agent do
    it "does not have access to inactive accounts" do
      accept :json
      @account.expects(:is_active?).returns false
      get :index
      assert_response 404
    end

    it "has access to bearer action" do
      get :bearer_protected, format: :json
      assert_response :ok
    end

    it "does not have access to bearer required action" do
      get :bearer_required, format: :json
      assert_response :forbidden
    end
  end

  as_a_system_user do
    it "has access to inactive accounts" do
      Account.any_instance.stubs(:is_active?).returns(false)
      accept :json
      get :index
      assert_response 200, @response.body
    end
  end

  describe "anonymous user" do
    before { accept :json }

    it "has access to anonymous action" do
      get :anonymous
      assert_response :ok
    end

    it "does not have access to bearer action" do
      get :bearer_protected
      assert_response :unauthorized
    end

    it "does not have access to bearer required action" do
      get :bearer_required
      assert_response :unauthorized
    end
  end

  describe "system bearer user" do
    before do
      accept :json

      client = FactoryBot.create(:global_client, user: @account.owner, account: @account)
      client.update_columns(identifier: 'zopim')
      token = FactoryBot.create(:token, global_client: client, user: nil, scopes: 'zopim')

      @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token
    end

    it "does not have access to agent action" do
      get :index
      assert_response :unauthorized
    end

    it "has access to anonymous action" do
      get :anonymous
      assert_response :ok
    end

    it "has access to bearer action" do
      get :bearer_protected
      assert_response :ok
    end

    it "has access to bearer required action" do
      get :bearer_required
      assert_response :ok
    end
  end

  describe "authorized" do
    before do
      accept :json
      login("minimum_agent")
    end

    describe "x-on-behalf-of" do
      before do
        @request.env["HTTP_X_ON_BEHALF_OF"] = users("with_groups_agent1").email
        get :index
      end

      it "returns bad request" do
        assert_response :bad_request
      end

      it "returns error response" do
        assert_match /X-On-Behalf-Of header/, @response.body
      end
    end

    describe "secure headers" do
      before do
        accept :json
        login("minimum_agent")
        get :index
      end

      it "sets X-Frame-Options" do
        assert_not_nil @response.headers['X-Frame-Options'],
          "Didn't find X-Frame-Options in #{@response.headers.inspect}"
      end
    end

    describe "invalid JSON format" do
      before do
        get :raise_type_mismatch
      end

      it "returns bad request" do
        assert_response :bad_request
      end

      it "returns error response" do
        @response.body.must_include "properly formatted"
      end
    end

    describe "altering readonly record" do
      before do
        get :raise_readonly_error
      end

      it "returns bad request" do
        assert_response :bad_request
      end

      it "returns error response" do
        @response.body.must_include "cannot be altered"
      end
    end

    describe "race condition" do
      before do
        get :raise_race_condition_error
      end

      it "returns conflict response" do
        assert_response :conflict
      end

      it "returns the correct content type" do
        assert_equal Mime[:json].to_s, @response.content_type
      end

      it "includes a nice error message" do
        expected_response = {
          "error" => "DatabaseConflict",
          "description" => "Database collision"
        }
        assert_equal expected_response, JSON.parse(@response.body)
      end

      before_should "records the exception" do
        Rails.logger.expects(:error)
      end
    end

    describe "deadlock found" do
      before do
        get :raise_deadlock_found_error
      end

      it "returns service_unavailable response" do
        assert_response :service_unavailable
      end

      it "returns the correct content type" do
        assert_equal Mime[:json].to_s, @response.content_type
      end

      it "includes a nice error message" do
        expected_response = {
          "error" => "DatabaseDeadlock",
          "description" => "Database deadlock found; please retry your transaction"
        }
        assert_equal expected_response, JSON.parse(@response.body)
      end

      it "returns Retry-After" do
        (5..65).must_include @response.headers['Retry-After'].to_i
      end

      before_should "records the exception" do
        ZendeskExceptions::Logger.expects(:record)
      end
    end

    describe "lock wait timeout exceeded" do
      before do
        get :raise_lock_wait_timeout_error
      end

      it "returns service_unavailable response" do
        assert_response :service_unavailable
      end

      it "returns the correct content type" do
        assert_equal Mime[:json].to_s, @response.content_type
      end

      it "includes a nice error message" do
        expected_response = {
          "error" => "DatabaseTimeout",
          "description" => "Database lock wait timeout; please retry your transaction"
        }
        assert_equal expected_response, JSON.parse(@response.body)
      end

      it "returns Retry-After" do
        (5..65).must_include @response.headers['Retry-After'].to_i
      end

      before_should "records the exception" do
        ZendeskExceptions::Logger.expects(:record)
      end
    end

    describe "too many in flight jobs" do
      before do
        get :raise_too_many_jobs_error
      end

      it "provides a machine readable error" do
        assert_equal "TooManyJobs", JSON.parse(@response.body)['error']
      end

      it "tells you which job hit the limit" do
        assert_includes JSON.parse(@response.body)['description'], 'Too many BulkTest jobs'
      end

      it "gives you the job ids of the currently running jobs" do
        assert_equal %w[foo bar], JSON.parse(@response.body)['current_job_ids']
      end

      it "returns 429 too many requests" do
        assert_response 429
      end

      it "sets Retry-After" do
        assert_equal 60, @response.headers['Retry-After']
      end
    end

    describe "upload failure error" do
      before do
        get :raise_upload_failure_error
      end

      it "returns service_unavailable response" do
        assert_response :service_unavailable
      end

      it "returns the correct content type" do
        assert_equal Mime[:json].to_s, @response.content_type
      end

      it "includes a nice error message" do
        expected_response = {
          "error" => 'UploadServiceTemporarilyUnavailable',
          "description" => I18n.t('txt.errors.attachments.upload_service_temporarily_unavailable')
        }
        assert_equal expected_response, JSON.parse(@response.body)
      end

      it "sets Retry-After" do
        assert_equal 10, @response.headers['Retry-After']
      end
    end

    describe "archived tickets unavailable error" do
      before do
        get :raise_archived_tickets_failure_error
      end

      it "returns service_unavailable response" do
        assert_response :service_unavailable
      end

      it "returns the correct content type" do
        assert_equal Mime[:json].to_s, @response.content_type
      end

      it "includes a nice error message" do
        expected_response = {
          "error" => 'ArchivedTicketsTemporarilyUnavailable',
          "description" => I18n.t('txt.errors.attachments.ticketarchive_service_temporarily_unavailable')
        }
        assert_equal expected_response, JSON.parse(@response.body)
      end

      it "sets Retry-After" do
        assert_equal 300, @response.headers['Retry-After']
      end
    end

    describe "authorization logging" do
      it "does not log without the arturo" do
        Rails.logger.expects(:info).never
        @controller.send(:log_authorization_header)
      end

      describe "when enabled" do
        before do
          Arturo.enable_feature! :log_auth_header
        end

        it "logs a basic auth request" do
          @request.env['HTTP_AUTHORIZATION'] = 'Basic abc'
          Rails.logger.expects(:info).with("[auth header] account:minimum user_id:#{users(:minimum_agent).id} auth_method:basic")

          @controller.send(:log_authorization_header)
        end

        it "logs a basic oauth request" do
          @request.env['HTTP_AUTHORIZATION'] = 'Bearer abcdefghijklmnopqrstuvwxyz'
          Rails.logger.expects(:info).with("[auth header] account:minimum user_id:#{users(:minimum_agent).id} auth_method:oauth:Bearer abcd")

          @controller.send(:log_authorization_header)
        end

        it "logs a request with a malformed header" do
          @request.env['HTTP_AUTHORIZATION'] = 'abc'
          Rails.logger.expects(:info).with("[auth header] account:minimum user_id:#{users(:minimum_agent).id} auth_method:other")

          @controller.send(:log_authorization_header)
        end

        it "logs a request without a header" do
          @request.env['HTTP_AUTHORIZATION'] = nil
          Rails.logger.expects(:info).with("[auth header] account:minimum user_id:#{users(:minimum_agent).id} auth_method:none")

          @controller.send(:log_authorization_header)
        end

        it "logs a request with a blank header" do
          @request.env['HTTP_AUTHORIZATION'] = ''
          Rails.logger.expects(:info).with("[auth header] account:minimum user_id:#{users(:minimum_agent).id} auth_method:none")

          @controller.send(:log_authorization_header)
        end
      end
    end

    describe 'multiproduct Support account' do
      let(:response_status) { 200 }
      let(:response_body) { {}.to_json }

      before do
        @account = @request.account = accounts(:multiproduct)
        @controller.stubs(current_account: @account)
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{@account.id}/products").
          to_return(status: response_status, body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
      end

      describe 'with active support product' do
        let(:response_body) { { products: [FactoryBot.build(:support_subscribed_product)] }.to_json }

        it 'allows access' do
          get :index
          assert_response :ok
        end
      end

      describe 'with expired support product' do
        let(:response_body) { { products: [FactoryBot.build(:support_expired_trial_product)] }.to_json }

        it 'returns 403' do
          get :index
          assert_response :forbidden
        end
      end

      describe 'without support product status' do
        let(:response_status) { 404 }

        it 'returns 403' do
          get :index
          assert_response :forbidden
        end
      end
    end

    describe 'shell account without Support (Chat phase 4)' do
      let(:chat_product) { FactoryBot.build(:chat_subscribed_product) }
      let(:support_product) { nil }
      let(:products) { [chat_product, support_product].compact }

      before do
        @account = @request.account = accounts(:shell_account_without_support)
        @account.stubs(:is_trial?).returns(false)
        @controller.stubs(current_account: @account)
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{@account.id}/products").
          to_return(body: { products: products }.to_json, headers: { content_type: 'application/json; charset=utf-8' })
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{@account.id}/products/support").
          to_return(body: { product: support_product }.to_json, headers: { content_type: 'application/json; charset=utf-8' })
      end

      it 'returns 403' do
        get :index
        assert_response :forbidden
      end

      describe 'after purchasing Support' do
        let(:chat_product) { FactoryBot.build(:chat_subscribed_product) }
        let(:support_product) { FactoryBot.build(:support_subscribed_product) }

        describe 'BEFORE Zuora synchronizer has created Support subscription' do
          it 'returns 403' do
            get :index
            assert_response :forbidden
          end
        end

        describe 'AFTER Zuora synchronizer has created Support subscription' do
          before do
            @account.stubs(:subscription).returns(stub)
          end

          it 'allows access' do
            get :index
            assert_response :ok
          end
        end
      end
    end

    describe "invalid pagination parameter error" do
      before do
        get :raise_invalid_pagination_parameter
      end

      it "returns bad_request response" do
        assert_response :bad_request
      end

      it "returns the correct content type" do
        assert_equal Mime[:json].to_s, @response.content_type
      end

      it "includes a nice error message" do
        expected_response = {
          "error" => 'InvalidPaginationParameter',
          "description" => 'test'
        }
        assert_equal expected_response, JSON.parse(@response.body)
      end
    end
  end

  describe "#invalid_request_format?" do
    it "returns true for non-JSON formats" do
      request = stub(format: Mime[:xml], accepts: [])
      @controller.expects(:request).at_least_once.returns(request)
      assert @controller.send(:invalid_request_format?)
    end

    it "returns false for JSON formats" do
      request = stub(format: Mime[:json], accepts: [])
      @controller.expects(:request).at_least_once.returns(request)
      refute @controller.send(:invalid_request_format?)
    end
  end

  describe "#invalid_content_type?" do
    it "returns false for non-write requests" do
      request = stub(content_type: Mime[:xml], put?: false, post?: false)
      @controller.stubs(:request).returns(request)
      refute @controller.send(:invalid_content_type?)
    end

    it "returns false for writes of type JSON" do
      request = stub(content_type: Mime[:json], put?: false, post?: true)
      @controller.stubs(:request).returns(request)
      refute @controller.send(:invalid_content_type?)
    end

    it "returns true for writes that are not JSON" do
      request = stub(content_type: Mime[:xml], put?: true, post?: false)
      @controller.stubs(:request).returns(request)
      assert @controller.send(:invalid_content_type?)
    end
  end

  describe "ssl" do
    before do
      accept :json
      login("minimum_agent")
    end

    it "process secure requests" do
      get :index
      assert_response :success
    end

    it "formats time stamps as per ISO8601" do
      get :index
      assert_match /\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z/, JSON.parse(@response.body)["created_at"]
    end

    it "rejects non-secure requests" do
      https!(false)
      get :index
      assert_response :forbidden
    end
  end

  describe "CORS" do
    before do
      accept :json
      login(:minimum_agent)
    end

    it "adds CORS headers to error responses" do
      get :cors_error
      assert_equal "*", @response.headers["Access-Control-Allow-Origin"]
    end
  end

  describe "authorization" do
    before do
      accept :json
    end

    it "requires authentication" do
      get :index
      assert_response :unauthorized
    end

    it "allows agents" do
      login(:minimum_agent)
      get :index

      assert_response :success
    end

    # Current behavior doesn't make sense for the API/lotus, as it redirects to /password.
    it "does not yet deny agents when the password expires" do
      login(:minimum_agent)
      User.any_instance.stubs(:password_expired?).returns(true)
      get :index

      assert_response :success
    end

    it "does not blow up system user" do
      login(User.find(User.system_user_id))

      get :index
      get :index

      assert_response :success
    end

    it "forbids end-users" do
      login(:minimum_end_user)
      get :index

      assert_response :forbidden
    end

    it "allows access via " do
      @controller.stubs(:current_registered_user).returns(Zendesk::SystemUser.find_subsystem_user_by_name("zendesk"))
      get :index

      assert_response :success
    end

    describe 'sandbox_orchestrator system user authorization' do
      before do
        @controller.stubs(:current_registered_user).returns(Zendesk::SystemUser.find_subsystem_user_by_name("sandbox_orchestrator"))
      end

      it "allows access for get on a sandbox account" do
        @account.stubs(:is_sandbox?).returns(true)
        get :index

        assert_response :success
      end

      it "allows access for post on a sandbox account" do
        @account.stubs(:is_sandbox?).returns(true)
        post :create

        assert_response :success
      end

      it "allows access for get on a master account" do
        @account.stubs(:is_sandbox?).returns(false)
        get :index

        assert_response :success
      end

      it "denies access for post on a master account" do
        @account.stubs(:is_sandbox?).returns(false)
        post :create

        assert_response :forbidden
      end

      it "denies access for put on a master account" do
        @account.stubs(:is_sandbox?).returns(false)
        put :update

        assert_response :forbidden
      end

      it "denies access for post on a master account" do
        @account.stubs(:is_sandbox?).returns(false)
        delete :destroy

        assert_response :forbidden
      end
    end
  end

  describe "API version" do
    before do
      login(:minimum_agent)
      get :index, format: "json"
      assert_response :success
    end

    should_set_version_header("v2")
  end

  describe "I18n" do
    before do
      users(:minimum_agent).update_attribute(:translation_locale, translation_locales(:brazilian_portuguese))
      login(:minimum_agent)
      get :i18n, format: "json"
    end

    it "uses the real translation locale" do
      assert_equal "THIS IS A BRAZILIAN PORTUGUESE EMAIL FOOTER", @response.body
    end
  end

  describe "actions" do
    it "does not have any actions (besides allow_parameters related)" do
      action_whitelist = /^(_conditional_callback_around_|_callback_before_|parameter_violations_header)/
      assert_equal [], ApiV2CleanController.action_methods.to_a.reject { |a| a =~ action_whitelist } - ["flash", "paginate_with_cursor"]
    end
  end

  describe "sorting" do
    describe "#sort_order" do
      should_map :sort_order, nil, "ASC"
      should_map :sort_order, "Hello", "ASC"
      should_map :sort_order, "asC", "asC"
      should_map :sort_order, "DEsC", "DEsC"
      should_map :sort_order, "desc", "desc"
      should_map :sort_order, "descending", "ASC"
    end

    describe "#sort_by" do
      describe "given the controller does not define SORT_LOOKUP" do
        before { assert_nil ApiV2TestController::SORT_LOOKUP }

        should_map :sort_by, nil, nil
        should_map :sort_by, "something", nil
        should_map :sort_by, "created_at", nil
      end

      describe "given the controller defines SORT_LOOKUP" do
        before { ApiV2TestController::SORT_LOOKUP = { "created_at" => "created_at" }.freeze }
        after { ApiV2TestController.send(:remove_const, :SORT_LOOKUP) }

        should_map :sort_by, nil, nil
        should_map :sort_by, "something", nil
        should_map :sort_by, "created_at", "created_at"
      end
    end

    describe "#sorting" do
      before do
        @params = {}
        @opts = {}
        @controller.stubs(params: @params)
      end

      it "returns default sort" do
        assert_equal "created_at ASC, id ASC", @controller.send(:sorting)
      end

      describe "given an :order option is passed" do
        before { @opts[:order] = "order" }

        it "returns :order option" do
          assert_equal "order", @controller.send(:sorting, @opts)
        end

        describe "given sort_by is present" do
          before { @controller.stubs(sort_by: "column") }

          it "returns sort from params with default sort direction" do
            assert_equal "column ASC", @controller.send(:sorting, @opts)
          end

          describe "given :sort_order is present in params" do
            before { @params[:sort_order] = "DESC" }

            it "returns sort from params with specified sort direction" do
              assert_equal "column DESC", @controller.send(:sorting, @opts)
            end
          end
        end
      end
    end

    describe "#paginate" do
      it "always inject sorting order" do
        @controller.expects(:sorting) if @controller.class::SORT_LOOKUP
        @controller.send(:paginate, @account.users)
      end
    end
  end

  describe '#prevent_expired_trial_account_access' do
    describe 'when the trial account has expired' do
      before do
        @account.stubs(:is_serviceable?).returns(false)
        @account.stubs(:is_trial?).returns(true)
        accept :json
      end

      describe 'with arturo feature on' do
        it 'returns 401' do
          login(:minimum_agent)
          get :index

          assert_response :unauthorized
        end

        it "returns 200 when it's a system user" do
          login(:systemuser)
          get :index

          assert_response :ok
        end

        describe 'with session auth' do
          before do
            @request.env['zendesk.user'] = users(:minimum_agent)
          end

          it 'returns 200' do
            login(:minimum_agent)
            get :index

            assert_response :ok
          end
        end
      end

      describe 'with arturo feature off' do
        before do
          @account.stubs(:has_prevent_expired_trial_account_access?).returns(false)
        end

        it 'returns 200' do
          login(:minimum_agent)
          get :index

          assert_response :ok
        end
      end
    end
  end
  describe '#forbid_nil_parameters' do
    before do
      login(:minimum_agent)
    end

    it 'sets the StrongerParameters allow_nil_for_everything to false before an action and reset to false after the action' do
      assert(ActionController::Parameters.allow_nil_for_everything)
      get :params_not_nil, format: :json
      assert_equal "allow_nil_for_everything = false", response.body
      assert(ActionController::Parameters.allow_nil_for_everything)
    end
  end
end
