require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Api::V2::CrmDataController do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users, :tickets

  before do
    @agent = users(:minimum_agent)
    @ticket = tickets(:minimum_1)
    @crm_user_data = SalesforceData.create(user: @agent, sync_status: ExternalUserData::SyncStatus::SYNC_OK, data: { records: [:foo, :bar] })
    @crm_ticket_data = SalesforceTicketData.create(ticket: @ticket, sync_status: ExternalUserData::SyncStatus::SYNC_OK, data: { records: [:foo, :bar] })

    accept :json
  end

  with_options(controller: 'api/v2/crm_data') do |request|
    request.should_route :get, '/api/v2/users/123/crm_data', action: 'show', user_id: '123'
    request.should_route :get, '/api/v2/users/123/crm_data/status', action: 'status', user_id: '123'
    request.should_route :delete, '/api/v2/users/123/crm_data', action: 'destroy', user_id: '123'
    request.should_route :delete, '/api/v2/users/123/crm_data/all_active', action: 'all_active', user_id: '123'
  end

  as_an_anonymous_user do
    should_be_unauthorized :show, [:get, :status, {id: 1}], add_to_all: {user_id: 1}
  end

  as_an_end_user do
    should_be_forbidden :show, [:get, :status, {id: 1}], add_to_all: {user_id: 1}
  end

  as_an_agent do
    before do
      @controller.stubs(:user).returns(@agent)
      @controller.stubs(:ticket).returns(@ticket)
      @agent.stubs(:crm_data).returns(@crm_user_data)
      @ticket.stubs(:crm_data).returns(@crm_ticket_data)
      @controller.stubs(:require_enabled_integration!)
    end

    describe "a GET to :show" do
      describe "when the CRM data is recent" do
        before do
          @crm_user_data.expects(:needs_sync?).returns(false)
          get :show, params: { user_id: @agent.id }
        end

        should_use_presenter Api::V2::CrmDataPresenter, status: :ok
      end

      describe "when the CRM data is stale" do
        before do
          @crm_user_data.expects(:needs_sync?).returns(true)
          @crm_user_data.expects(:start_sync)
          get :show, params: { user_id: @agent.id }
        end

        should_use_presenter Api::V2::CrmDataPresenter, status: :accepted, location: /\/api\/v2\/users\/\d+\/crm_data\/status\.json$/
      end

      describe "when salesforce_configuration_enabled? and ticket_id is in the params" do
        before do
          @controller.stubs(:salesforce_configuration_enabled?).returns(true)
        end

        describe "and CRM ticket data is recent" do
          before do
            @crm_ticket_data.expects(:needs_sync?).returns(false)
            get :show, params: { user_id: @agent.id, ticket_id: @ticket.nice_id }
          end

          should_use_presenter Api::V2::CrmDataPresenter, status: :ok
        end

        describe "and CRM ticket data is stale" do
          before do
            @crm_ticket_data.expects(:needs_sync?).returns(true)
            @crm_ticket_data.expects(:start_sync)
            get :show, params: { user_id: @agent.id, ticket_id: @ticket.nice_id }
          end

          should_use_presenter Api::V2::CrmDataPresenter, status: :accepted, location: /\/api\/v2\/users\/\d+\/crm_data\/status\.json\?ticket_id\=1$/
        end
      end
    end

    describe "a GET to :status" do
      describe "when the CRM retrieval has succeeded" do
        before do
          @crm_user_data.expects(:sync_errored?).returns(false)
          @crm_user_data.expects(:sync_pending?).returns(false)

          get :status, params: { user_id: @agent.id }
        end

        should_use_presenter Api::V2::CrmDataPresenter, status: :see_other, location: /https:\/\/.+\/api\/v2\/users\/\d+\/crm_data\.json$/
      end

      describe "when the CRM retrieval is ongoing" do
        before do
          @crm_user_data.expects(:sync_errored?).returns(false)
          @crm_user_data.expects(:sync_pending?).returns(true)

          get :status, params: { user_id: @agent.id }
        end

        should_use_presenter Api::V2::CrmDataPresenter, status: :ok, location: /\/api\/v2\/users\/\d+\/crm_data\/status\.json$/
      end

      describe "when the CRM retrieval has failed" do
        before do
          @crm_user_data.expects(:sync_errored?).returns(true)

          get :status, params: { user_id: @agent.id }
        end

        should_use_presenter Api::V2::CrmDataPresenter, status: :ok
      end

      describe "when salesforce_configuration_enabled? and ticket_id is in the params" do
        before do
          @controller.stubs(:salesforce_configuration_enabled?).returns(true)
        end

        describe "and the CRM retrieval has succeeded" do
          before do
            @crm_ticket_data.expects(:sync_errored?).returns(false)
            @crm_ticket_data.expects(:sync_pending?).returns(false)

            get :status, params: { user_id: @agent.id, ticket_id: @ticket.nice_id }
          end

          should_use_presenter Api::V2::CrmDataPresenter, status: :see_other, location: /\/api\/v2\/users\/\d+\/crm_data\.json\?ticket_id\=1$/
        end

        describe "and the CRM retrieval is ongoing" do
          before do
            @crm_ticket_data.expects(:sync_errored?).returns(false)
            @crm_ticket_data.expects(:sync_pending?).returns(true)

            get :status, params: { user_id: @agent.id, ticket_id: @ticket.nice_id }
          end

          should_use_presenter Api::V2::CrmDataPresenter, status: :ok, location: /\/api\/v2\/users\/\d+\/crm_data\/status\.json\?ticket_id\=1$/
        end

        describe "and the CRM retrieval has failed" do
          before do
            @crm_ticket_data.expects(:sync_errored?).returns(true)

            get :status, params: { user_id: @agent.id, ticket_id: @ticket.nice_id }
          end

          should_use_presenter Api::V2::CrmDataPresenter, status: :ok
        end
      end
    end

    describe "a DELETE to destroy" do
      before do
        delete :destroy, params: { user_id: @agent.id, ticket_id: @ticket.nice_id }
      end

      it "returns :no_content" do
        assert_response :no_content
      end
    end

    describe "a DELETE to destroy_all_active" do
      before do
        delete :all_active, params: { user_id: @agent.id, ticket_id: @ticket.nice_id }
      end

      it "returns :no_content" do
        assert_response :no_content
      end
    end
  end
end
