require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Embeddable::ConfigSetsController do
  extend Api::V2::TestHelper

  before do
    accept :json
  end

  describe "#show_default" do
    as_an_admin do
      it "uses Faraday" do
        Faraday::Connection.any_instance.expects(:get).with('/embeddable/api/internal/config_sets/default.json', nil)
        get :show_default
      end
    end

    as_an_agent do
      it "is allowed for admins only" do
        get :show_default
        assert_response :forbidden
      end
    end
  end

  describe "#update_default" do
    before do
      @params = {
        config_set: {
          color: 'red',
          contact_form_button_text: 'contact'
        }
      }
    end

    as_an_admin do
      it "uses Faraday" do
        Faraday::Connection.any_instance.expects(:put).with('/embeddable/api/internal/config_sets/default.json', anything)
        put :update_default, params: @params
      end
    end

    as_an_agent do
      it "is allowed for admins only" do
        put :update_default, params: @params
        assert_response :forbidden
      end
    end
  end
end
