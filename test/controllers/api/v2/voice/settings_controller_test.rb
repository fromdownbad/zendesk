require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 2

describe Api::V2::Voice::SettingsController do
  extend Api::Presentation::TestHelper

  fixtures :all

  before do
    accept :json
  end

  as_an_end_user do
    should_be_forbidden :update
  end

  as_an_admin do
    let(:account) { accounts(:minimum) }

    describe '#update' do
      describe 'when saving works' do
        before do
          put :update, params: { settings: { voice: true, voice_outbound_enabled: false, voice_maximum_queue_size: 0 } }
        end

        it('responds with success') { assert_response :success }

        it 'updates the settings' do
          account.settings.reload

          assert account.settings.voice?
          refute account.settings.voice_outbound_enabled?

          assert_equal '0', account.settings.voice_maximum_queue_size
        end
      end
    end
  end
end
