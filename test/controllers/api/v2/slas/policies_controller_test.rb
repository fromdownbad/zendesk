require_relative '../../../../support/test_helper'
require_relative '../../../../support/ticket_metric_helper'

SingleCov.covered!

describe Api::V2::Slas::PoliciesController do
  include TicketMetricHelper

  extend Api::V2::TestHelper

  fixtures :accounts,
    :users,
    :role_settings

  let(:account)       { accounts(:minimum) }
  let(:policy)        { new_sla_policy }
  let(:response_body) { JSON.parse(@response.body) }

  before do
    @model = policy

    accept :json
  end

  with_options(controller: 'api/v2/slas/policies') do |request|
    request.should_route :get,    '/api/v2/slas/policies',             action: 'index'
    request.should_route :get,    '/api/v2/slas/policies/1',           action: 'show', id: '1'
    request.should_route :post,   '/api/v2/slas/policies',             action: 'create'
    request.should_route :put,    '/api/v2/slas/policies/1',           action: 'update',  id: '1'
    request.should_route :delete, '/api/v2/slas/policies/1',           action: 'destroy', id: '1'
    request.should_route :put,    '/api/v2/slas/policies/reorder',     action: 'reorder'
    request.should_route :get,    '/api/v2/slas/policies/definitions', action: 'definitions'
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get,    :index]
    should_be_unauthorized [:get,    :show, id: 1]
    should_be_unauthorized [:post,   :create]
    should_be_unauthorized [:put,    :update,  id: 1]
    should_be_unauthorized [:delete, :destroy, id: 1]
    should_be_unauthorized [:put,    :reorder]
    should_be_unauthorized [:get,    :definitions]
  end

  as_an_end_user do
    should_be_forbidden [:get,    :index]
    should_be_forbidden [:get,    :show, id: 1]
    should_be_forbidden [:post,   :create]
    should_be_forbidden [:put,    :update,  id: 1]
    should_be_forbidden [:delete, :destroy, id: 1]
    should_be_forbidden [:put,    :reorder]
    should_be_forbidden [:get,    :definitions]
  end

  as_an_agent do
    should_be_forbidden [:get,    :index]
    should_be_forbidden [:get,    :show, id: 1]
    should_be_forbidden [:post,   :create]
    should_be_forbidden [:put,    :update,  id: 1]
    should_be_forbidden [:delete, :destroy, id: 1]
    should_be_forbidden [:put,    :reorder]
    should_be_forbidden [:get,    :definitions]
  end

  as_an_agent_with_permissions(business_rule_management: true) do
    describe 'when the account does not have the SLA feature' do
      before { @account.stubs(has_service_level_agreements?: false) }

      should_be_forbidden [:get,    :index]
      should_be_forbidden [:get,    :show, id: 1]
      should_be_forbidden [:post,   :create]
      should_be_forbidden [:put,    :update,  id: 1]
      should_be_forbidden [:delete, :destroy, id: 1]
      should_be_forbidden [:put,    :reorder]
      should_be_forbidden [:get,    :definitions]
    end

    describe 'when the account has the SLA feature' do
      before { @account.stubs(has_service_level_agreements?: true) }

      describe 'GET to #index' do
        before { get :index }

        should_use_presenter Api::V2::Slas::PolicyPresenter

        it('responds with ok') { assert_response :ok }
      end

      describe 'a GET to #show' do
        before { get :show, params: { id: policy.id } }

        should_use_presenter Api::V2::Slas::PolicyPresenter

        it('responds with ok') { assert_response :ok }
      end

      describe 'a POST to #create' do
        let(:request_body) do
          {
            sla_policy: {
              title:          'test title',
              description:    'test desc',
              position:       89,
              filter:         filter,
              policy_metrics: %w[Low normal HIGH urgent].flat_map do |priority|
                Zendesk::Sla::TicketMetric.all.map do |metric|
                  {
                    priority:       priority,
                    metric:         metric.name,
                    target:         10,
                    business_hours: false
                  }
                end
              end
            }
          }
        end

        let(:filter) do
          {
            all: [{field: 'current_tags', operator: 'includes', value: '123'}],
            any: []
          }
        end

        describe 'and the sla policies limit has not been reached' do
          before { post :create, params: request_body }

          it('responds with created') { assert_response :created }
        end

        describe 'and the sla policies limit has been reached' do
          let!(:original_limit) do
            Sla::Policy::SLA_POLICIES_LIMIT
          end

          let(:limit) { 0 }

          before do
            Sla::Policy.const_set(:SLA_POLICIES_LIMIT, limit)

            post :create, params: request_body
          end

          after do
            Sla::Policy.const_set(:SLA_POLICIES_LIMIT, original_limit)
          end

          it('responds with forbidden') { assert_response :forbidden }

          it('includes an error message') do
            assert JSON.parse(response.body)['error'].present?
          end
        end
      end

      describe 'a PUT to #update' do
        before do
          put(:update, params: { id:         policy.id, sla_policy: {
              title:    'new title',
              filter:   {
                all:  [
                  {
                    field:    'current_tags',
                    operator: 'includes',
                    value:    ['updated_value']
                  }
                ]
              },
              policy_metrics: [
                {
                  priority:       'normal',
                  metric:
                    Zendesk::Sla::TicketMetric::FirstReplyTime.name,
                  target:         999,
                  business_hours: false
                }
              ]
            } })
        end

        it('responds with ok') { assert_response :ok }
      end

      describe 'a DELETE to #destroy' do
        describe 'when the policy is valid' do
          before { delete :destroy, params: { id: policy.id } }

          it 'responds with no content' do
            assert_response :no_content
          end
        end

        describe 'when the policy is invalid' do
          before do
            policy.update_attribute(:title, nil)

            delete :destroy, params: { id: policy.id }

            policy.reload
          end

          it 'marks the policy as deleted' do
            assert policy.deleted?
          end

          it 'responds with no content' do
            assert_response :no_content
          end
        end
      end

      describe 'a PUT to #reorder' do
        let(:second_policy) { new_sla_policy(position: 2) }

        before do
          second_policy.update_attributes!(position: policy.position - 1)

          account.sla_policies.
            active.
            reject { |p| [policy, second_policy].include?(p) }.
            each(&:destroy)

          put :reorder, params: { sla_policy_ids: [policy.id, second_policy.id] }
        end

        it('responds with ok') { assert_response :ok }
      end

      describe 'a GET to #definitions' do
        before do
          ZendeskRules::Definitions::Conditions.stubs(:definitions_as_json).
            with { |context| context.options[:condition_type] == :all }.
            returns('all_defn')
          ZendeskRules::Definitions::Conditions.stubs(:definitions_as_json).
            with { |context| context.options[:condition_type] == :any }.
            returns('any_defn')

          get :definitions
        end

        it('responds with ok') { assert_response :ok }
      end
    end
  end

  as_an_admin do
    describe 'when the account does not have the SLA feature' do
      before { @account.stubs(has_service_level_agreements?: false) }

      should_be_forbidden [:get,    :index]
      should_be_forbidden [:get,    :show, id: 1]
      should_be_forbidden [:post,   :create]
      should_be_forbidden [:put,    :update,  id: 1]
      should_be_forbidden [:delete, :destroy, id: 1]
      should_be_forbidden [:put,    :reorder]
      should_be_forbidden [:get,    :definitions]
    end

    describe 'when the account has the SLA feature' do
      before { @account.stubs(has_service_level_agreements?: true) }

      describe 'GET to #index' do
        describe 'when there is an active policy' do
          before { get :index }

          should_use_presenter Api::V2::Slas::PolicyPresenter

          it('responds with ok') { assert_response :ok }

          it 'returns policies' do
            assert(
              response_body['sla_policies'].any? { |p| p['id'] == policy.id }
            )
          end
        end

        describe 'when there is a deleted policy' do
          before do
            policy.soft_delete

            get :index
          end

          should_use_presenter Api::V2::Slas::PolicyPresenter

          it('responds with ok') { assert_response :ok }

          it 'does not return deleted policy' do
            refute(
              response_body['sla_policies'].any? { |p| p['id'] == policy.id }
            )
          end
        end
      end

      describe 'a GET to #show' do
        describe 'when the policy is active' do
          before { get :show, params: { id: policy.id } }

          should_use_presenter Api::V2::Slas::PolicyPresenter

          it('responds with ok') { assert_response :ok }
        end

        describe 'when the policy is deleted' do
          before do
            policy.soft_delete

            get :show, params: { id: policy.id }
          end

          it('responds with not_found') { assert_response :not_found }
        end
      end

      describe 'a POST to #create' do
        let(:request_body) do
          {
            sla_policy: {
              title:          'test title',
              description:    'test desc',
              position:       89,
              filter:         filter,
              policy_metrics: %w[Low normal HIGH urgent].flat_map do |priority|
                Zendesk::Sla::TicketMetric.all.map do |metric|
                  {
                    priority:       priority,
                    metric:         metric.name,
                    target:         10,
                    business_hours: false
                  }
                end
              end
            }
          }
        end

        let(:filter) do
          {
            all: [{field: 'current_tags', operator: 'includes', value: '123'}],
            any: []
          }
        end

        ['123', ['123']].each do |filter_params|
          let(:filter) do
            {
              all: [
                {
                  field:    'current_tags',
                  operator: 'includes',
                  value:    filter_params
                }
              ],
              any: []
            }
          end

          describe "with filter params like #{filter_params}" do
            before { post :create, params: request_body }

            should_use_presenter(Api::V2::Slas::PolicyPresenter,
              status:   :created,
              location: /\/api\/v2\/slas\/policies\/\d+\.json$/)

            should_change('policies', by: 1) { Sla::Policy.count(:all) }

            should_change('policy metrics', by: 24) do
              Sla::PolicyMetric.count(:all)
            end

            it 'includes the policy metrics in the response' do
              assert_equal(
                request_body[:sla_policy][:policy_metrics].length,
                response_body['sla_policy']['policy_metrics'].length
              )
            end
          end
        end

        describe 'when no metrics are provided' do
          before do
            request_body[:sla_policy][:policy_metrics] = nil

            post :create, params: request_body
          end

          it 'is successful' do
            assert_response :created
          end
        end

        describe 'when the description is null' do
          before do
            request_body[:sla_policy][:description] = nil

            post :create, params: request_body
          end

          it 'is successful' do
            assert_response :created
          end
        end

        describe 'when no data is provided' do
          before { post :create }

          it 'returns an error with explanation' do
            assert_equal(
              {'error' => 'param is missing or the value is empty: sla_policy'},
              response_body
            )
          end

          it 'is unprocessable' do
            assert_response :unprocessable_entity
          end
        end

        describe 'when fields are missing' do
          before { post :create, params: { sla_policy: {filter: {}} } }

          it 'returns an error with explanation' do
            assert_equal(
              {'error' => 'param is missing or the value is empty: title'},
              response_body
            )
          end

          it 'is unprocessable' do
            assert_response :unprocessable_entity
          end
        end
      end

      describe 'a PUT to #update' do
        describe 'when the parameters are valid' do
          before do
            put(:update, params: { id:         policy.id, sla_policy: {
                title:    'new title',
                filter:   {
                  all:  [
                    {
                      field:    'current_tags',
                      operator: 'includes',
                      value:    ['updated_value']
                    }
                  ]
                },
                policy_metrics: [
                  {
                    priority:       'normal',
                    metric:
                      Zendesk::Sla::TicketMetric::FirstReplyTime.name,
                    target:         999,
                    business_hours: false
                  }
                ]
              } })

            policy.reload
          end

          should_use_presenter(Api::V2::Slas::PolicyPresenter,
            status:   :ok,
            location: %r{/api/v2/slas/policies/\d+\.json$})

          should_not_change('policies') { Sla::Policy.count(:all) }

          should_change('policy metrics') { Sla::PolicyMetric.count(:all) }

          it 'updates policy' do
            assert_equal 'new title', policy.title
          end

          it 'updates the filter' do
            assert_equal(
              Definition.build(
                conditions_all: [
                  {
                    field:    'current_tags',
                    operator: 'includes',
                    value:    'updated_value'
                  }
                ]
              ),
              policy.filter
            )
          end

          it 'updates the policy metrics' do
            assert policy.policy_metrics.map(&:target).include?(999)
          end

          it 'includes the updated policy information in the response' do
            assert_equal 'new title', response_body['sla_policy']['title']
          end
        end

        describe 'when no data is provided' do
          before { put :update, params: { id: policy.id } }

          it 'returns an error with explanation' do
            assert_equal(
              {'error' => 'param is missing or the value is empty: sla_policy'},
              response_body
            )
          end

          it 'is unprocessable' do
            assert_response :unprocessable_entity
          end
        end

        describe 'when the policy does not exist' do
          before { put :update, params: { id: 123 } }

          it 'is not found' do
            assert_response :not_found
          end
        end
      end

      describe 'a DELETE to #destroy' do
        describe 'when the parameters are valid' do
          before do
            delete :destroy, params: { id: policy.id }

            policy.reload
          end

          it 'marks the policy as deleted' do
            assert policy.deleted?
          end

          should_change('policies', by: -1) do
            Sla::Policy.active.count(:all)
          end
        end
      end

      describe 'a PUT to #reorder' do
        let(:second_policy) { new_sla_policy(position: 2) }

        before do
          second_policy.update_attributes!(position: policy.position - 1)

          account.sla_policies.
            active.
            reject { |p| [policy, second_policy].include?(p) }.
            each(&:destroy)

          put :reorder, params: { sla_policy_ids: [policy.id, second_policy.id] }

          policy.reload
          second_policy.reload
        end

        it 'updates the position of the policy' do
          assert policy.position < second_policy.position
        end
      end

      describe 'a GET to #definitions' do
        before do
          ZendeskRules::Definitions::Conditions.stubs(:definitions_as_json).
            with { |context| context.options[:condition_type] == :all }.
            returns('all_defn')
          ZendeskRules::Definitions::Conditions.stubs(:definitions_as_json).
            with { |context| context.options[:condition_type] == :any }.
            returns('any_defn')

          get :definitions
        end

        it('responds with ok') { assert_response :ok }

        it "returns 'all' definitions" do
          assert_equal 'all_defn', response_body['definitions']['all']
        end

        it "returns 'any' definitions" do
          assert_equal 'any_defn', response_body['definitions']['any']
        end
      end
    end
  end

  ["gooddata", "bime"].each do |subsystem_user|
    as_a_subsystem_user(user: subsystem_user, account: :minimum) do
      describe 'when the account does not have the SLA feature' do
        before { @account.stubs(has_service_level_agreements?: false) }

        should_be_forbidden [:get,    :index]
        should_be_forbidden [:get,    :show, id: 1]
        should_be_forbidden [:post,   :create]
        should_be_forbidden [:put,    :update,  id: 1]
        should_be_forbidden [:delete, :destroy, id: 1]
        should_be_forbidden [:put,    :reorder]
        should_be_forbidden [:get,    :definitions]
      end

      describe 'when the account has the SLA feature' do
        before { @account.stubs(has_service_level_agreements?: true) }

        should_be_forbidden [:post,   :create]
        should_be_forbidden [:put,    :update,  id: 1]
        should_be_forbidden [:delete, :destroy, id: 1]
        should_be_forbidden [:put,    :reorder]
        should_be_forbidden [:get,    :definitions]

        describe 'GET to #index' do
          before { get :index }

          it('responds with ok') { assert_response :ok }
        end

        describe 'a GET to #show' do
          before { get :show, params: { id: policy.id } }

          it('responds with ok') { assert_response :ok }
        end
      end
    end
  end
end
