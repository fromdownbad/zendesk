require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::JobStatusesController do
  extend Api::V2::TestHelper

  fixtures :accounts, :users, :role_settings

  before do
    @account = accounts(:minimum)
    @user    = users(:minimum_agent)
    options = {"account_id" => @account.id, "results" => [1, 2, 3]}

    @key = "Zendesk-JobWithStatusQueue-#{@account.id}"
    Zendesk::RedisStore.client.flushall

    @status1 = make_status("uuid1", options)
    @status2 = make_status("uuid2", options)
    @foreign_status = make_status("uuid3", options.merge("account_id" => accounts(:support).id))
    @broken_status = make_status("uuid4", nil)
    Resque::Plugins::Status::Hash.stubs(:get).with('invalid').returns(nil)

    accept :json
  end

  with_options(controller: 'api/v2/job_statuses') do |request|
    request.should_route :get, '/api/v2/job_statuses', action: 'index'
    request.should_route :get, '/api/v2/job_statuses/123', action: 'show', id: '123'
    request.should_route :get, '/api/v2/job_statuses/show_many', action: 'show_many'
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :show, :show_many
  end

  as_an_end_user do
    should_be_forbidden :index, :show, :show_many
  end

  as_an_agent do
    describe "a GET to :index" do
      let(:uuids) { %w[uuid1 uuid2 uuid3 uuid4] }

      before do
        get :index
      end

      it 'responds with a list of job statuses' do
        json = JSON.parse(@response.body)

        uuids.each do |uuid|
          assert json['job_statuses'].find { |s| s['id'] == uuid }.present?, "A job with UUID '#{uuid}' was expected to be present in the :index response, but was not"
        end
      end
    end

    describe "a GET to :show" do
      describe "with an invalid id" do
        before { get :show, params: { id: 'invalid' } }
        it('responds with not_found') { assert_response :not_found }
      end

      describe "with invalid options" do
        before { get :show, params: { id: @broken_status.uuid } }
        it('responds with not_found') { assert_response :not_found }
      end

      describe "with a valid id" do
        before do
          get :show, params: { id: @status1.uuid }
        end

        should_use_presenter Api::V2::JobStatusPresenter, status: :ok
      end

      describe "with a foreign account's job id" do
        before do
          get :show, params: { id: @foreign_status.uuid }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end
    end

    describe "a GET to :show_many" do
      describe "with ids" do
        before do
          @uuids = [@status1.uuid, @status2.uuid]
          get :show_many, params: { ids: @uuids.join(",") }
        end

        it "gives me back the job statuses I asked for" do
          json = JSON.parse(@response.body)
          assert_equal @uuids, json['job_statuses'].map { |status| status["id"] }.sort
        end
      end

      describe "with an invalid id" do
        before { get :show_many, params: { ids: 'invalid' } }

        it "returns an empty result" do
          json = JSON.parse(@response.body)
          assert_equal [], json["job_statuses"]
        end
      end

      describe "with a foreign account's job id" do
        before do
          get :show_many, params: { ids: @foreign_status.uuid }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end
    end
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    describe '#show' do
      it 'returns job status' do
        get :show, params: { id: @status1.uuid }
        assert_response :ok
        json = JSON.parse(@response.body)
        assert_equal @status1.uuid, json['job_status']['id']
      end
    end

    describe '#show_many' do
      it 'returns job statuses' do
        ids = [@status1.uuid, @status2.uuid]
        get :show_many, params: { ids: ids.join(',') }
        assert_response :ok
        json = JSON.parse(@response.body)
        assert_equal ids, json['job_statuses'].map { |status| status['id'] }.sort
      end
    end
  end

  private

  def make_status(uuid, options)
    Zendesk::RedisStore.client.lpush(@key, uuid)
    Resque::Plugins::Status::Hash.new.tap do |status|
      status.stubs(:uuid).returns(uuid)
      status.stubs(:total).returns(10)
      status.stubs(:num).returns(5)
      status.stubs(:status).returns("hello")
      status['options'] = options
      Resque::Plugins::Status::Hash.stubs(:get).with(uuid).returns(status)
    end
  end
end
