require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::DeletedUsersController do
  include Api::V2::TestHelper
  extend Api::V2::TestHelper
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper

  fixtures :all

  let (:redacted_user) { users(:inactive_user_redacted) }
  let(:deleted_user) { users(:minimum_end_user2) }
  let(:active_user) { users(:minimum_end_user) }
  let(:current_account) { active_user.account }
  let(:current_user) { @controller.send(:current_user) }
  let(:zendesk_agent) do
    FactoryBot.create(:agent,
      account: current_account,
      name: "Double Agent")
  end

  before do
    @agent = users(:minimum_agent)
    accept :json
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index, { }]
    should_be_unauthorized [:get, :show, { id: 1 }]
    should_be_unauthorized [:delete, :destroy, { id: 1 }]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index, { }]
    should_be_forbidden [:get, :show, { id: 1 }]
    should_be_forbidden [:delete, :destroy, { id: 1 }]
  end

  as_a_subsystem_user(user: "gooddata", account: :minimum) do
    before do
      deleted_user.current_user = zendesk_agent
      deleted_user.delete!
    end

    should_be_authorized { get :index }
    should_be_authorized { get :show, params: { id: deleted_user.id } }
  end

  as_an_agent do
    should_support_cursor_pagination :index
  end

  as_an_admin do
    before do
      # Setup Account to have at least one soft-deleted user
      deleted_user.current_user = zendesk_agent
      deleted_user.delete!
    end

    should_support_cursor_pagination :index

    describe "#count" do
      it 'should render json' do
        get :count
        users_json = JSON.parse(response.body)
        assert users_json['count']['value']
      end

      it 'should render json with filter' do
        get :count, params: { include_redacted: true }
        users_json = JSON.parse(response.body)
        assert users_json['count']['value']
      end

      it 'should render json with filter false' do
        get :count, params: { include_redacted: false }
        users_json = JSON.parse(response.body)
        assert users_json['count']['value']
      end
    end

    describe "#index" do
      it 'should render json' do
        get :index
        users_json = JSON.parse(response.body)
        assert users_json['deleted_users'].size >= 1
        assert users_json['deleted_users'].all? { |user_json| user_json['active'] == false }
        assert users_json['deleted_users'].map { |user_json| user_json['id'].to_i }.include?(deleted_user.id)
        assert users_json['deleted_users'].map { |user_json| user_json['id'].to_i }.include?(redacted_user.id)
      end

      it 'should render json with redacted true' do
        get :index, params: { include_redacted: true }
        users_json = JSON.parse(response.body)
        assert users_json['deleted_users'].size >= 1
        assert users_json['deleted_users'].all? { |user_json| user_json['active'] == false }
        assert users_json['deleted_users'].map { |user_json| user_json['id'].to_i }.include?(deleted_user.id)
        assert users_json['deleted_users'].map { |user_json| user_json['id'].to_i }.include?(redacted_user.id)
      end

      describe_with_arturo_disabled :v2_users_allow_redacted_users_filter do
        it 'should render json with redacted false and return redacted user because arturo is disabled' do
          get :index, params: { include_redacted: false }
          users_json = JSON.parse(response.body)
          assert users_json['deleted_users'].size >= 1
          assert users_json['deleted_users'].all? { |user_json| user_json['active'] == false }
          assert users_json['deleted_users'].map { |user_json| user_json['id'].to_i }.include?(redacted_user.id)
        end
      end

      describe_with_arturo_enabled :v2_users_allow_redacted_users_filter do
        it 'should render json with redacted false and not return redacted user because arturo is enabled' do
          get :index, params: { include_redacted: false }
          users_json = JSON.parse(response.body)
          assert users_json['deleted_users'].size >= 1
          assert users_json['deleted_users'].all? { |user_json| user_json['active'] == false }
          refute users_json['deleted_users'].map { |user_json| user_json['id'].to_i }.include?(redacted_user.id)
        end
      end

      it 'should use offset pagination when no pagination params are present' do
        get :index
        users_json = JSON.parse(response.body)
        assert users_json['deleted_users'].size >= 1
        assert users_json['deleted_users'].all? { |user_json| user_json['active'] == false }
        assert_equal 2, (users_json.keys & ['next_page', 'previous_page']).size
      end

      describe "Emergency rate limiting for GET deleted_users" do
        describe "with an integer page param" do
          let(:limiter) { mock }
          let(:statsd_client) { mock }

          before { limiter.stubs(:throttle_index_with_deep_offset_pagination) }

          it "calls the pagination limiter" do
            Zendesk::OffsetPaginationLimiter.expects(:new).with(anything).returns(limiter)
            limiter.expects(:throttle_index_with_deep_offset_pagination).once

            get :index, params: { page: 1 }
          end

          describe "after 99 calls within 1 second" do
            let(:rate_limit) { 100 }

            before do
              Timecop.freeze
              Prop.throttle!(:index_deleted_users_with_deep_offset_pagination_ceiling_limit, @account.id, increment: rate_limit - 1)
            end

            describe "for page=1 and per_page=100" do
              let(:page) { 1 }

              it "responds with success on calls 100 and 101" do
                get :index, params: { page: page }
                assert_response :success
                get :index, params: { page: page }
                assert_response :success
              end

              describe "metrics and logging" do
                it "does not log any rate limit message" do
                  Rails.logger.expects(:info).with(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/)).never
                  # Once the above expectation is set up, you must specify that other calls might occur.
                  Rails.logger.expects(:info).with(Not(regexp_matches(/[R|r]ate limit[ed|ing] #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not increment the 'deleted_users' ceiling rate limit metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "deleted_users").at_least(0).returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_deleted_users_index_ceiling_rate_limit_log_only", anything).never
                  statsd_client.expects(:increment).with("api_v2_deleted_users_index_ceiling_rate_limit", anything).never
                  2.times { get :index, params: { page: page } }
                end
              end
            end

            describe "for page=10,001 and per_page=100" do
              let(:page) { 10_001 }

              it "responds with success on calls 100 and 101" do
                get :index, params: { page: page }
                assert_response :success
                get :index, params: { page: page }
                assert_response :success
              end

              describe "metrics and logging" do
                it "logs the log_only message" do
                  Rails.logger.expects(:info).with(regexp_matches(/Log only: would have rate limited #index by account/)).once
                  Rails.logger.expects(:info).with(Not(regexp_matches(/Log only: would have rate limited #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not log the message for the active arturo" do
                  Rails.logger.expects(:info).with(regexp_matches(/Rate limiting #index by account/)).never
                  Rails.logger.expects(:info).with(Not(regexp_matches(/Rate limiting #index by account/))).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "does not increment the api_v2_deleted_users_index_ceiling_rate_limit metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "deleted_users").once.returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_deleted_users_index_ceiling_rate_limit", anything).never
                  statsd_client.expects(:increment).with(Not(equals("api_v2_deleted_users_index_ceiling_rate_limit")), anything).at_least(0)
                  2.times { get :index, params: { page: page } }
                end

                it "increments the log_only metric" do
                  Zendesk::StatsD::Client.expects(:new).with(namespace: "deleted_users").once.returns(statsd_client)
                  statsd_client.expects(:increment).with("api_v2_deleted_users_index_ceiling_rate_limit_log_only", anything).once
                  2.times { get :index, params: { page: page } }
                end
              end
            end
          end
        end
      end

      describe_with_arturo_enabled :cursor_pagination_default_deleted_users do
        it 'should use cursor pagination when no pagination params are present' do
          get :index
          users_json = JSON.parse(response.body)
          assert users_json['deleted_users'].size >= 1
          assert users_json['deleted_users'].all? { |user_json| user_json['active'] == false }
          assert_equal 2, (users_json.keys & ['after_url', 'before_url']).size
        end

        it 'should set the pagination limit to 100 if the limit param is not present' do
          get :index
          assert_response :ok
          users_json = JSON.parse(response.body)
          assert_match 'limit=100&', users_json['after_url']
        end

        it 'should set the pagination limit to 100 if the limit sent is over the max limit of 100' do
          get :index, params: { limit: 321 }
          assert_response :ok
          users_json = JSON.parse(response.body)
          assert_match 'limit=100&', users_json['after_url']
        end

        it 'should use offset pagination when offset pagination params are present' do
          get :index, params: { page: 1 }
          users_json = JSON.parse(response.body)
          assert users_json['deleted_users'].size >= 1
          assert users_json['deleted_users'].all? { |user_json| user_json['active'] == false }
          assert_equal 2, (users_json.keys & ['next_page', 'previous_page']).size
        end

        describe_with_arturo_enabled :remove_offset_pagination_deleted_users do
          it 'should use cursor pagination even with offset pagination params present' do
            get :index, params: { page: 1 }
            users_json = JSON.parse(response.body)
            assert users_json['deleted_users'].size >= 1
            assert users_json['deleted_users'].all? { |user_json| user_json['active'] == false }
            assert_equal 2, (users_json.keys & ['after_url', 'before_url']).size
          end
        end
      end

      it 'should use cursor pagination v2 when v2 params are present' do
        get :index, params: { page: { size: 100 } }
        assert_response :ok
        json_response = JSON.parse(response.body)
        assert json_response['deleted_users'].size >= 1
        assert_not_nil json_response['links']['prev']
        assert_not_nil json_response['links']['next']
      end

      describe 'default sort parameter of id for v2' do
        it 'generates the correct order clause' do
          assert_sql_queries(1, /ORDER BY `users`.`id` DESC/) do
            get :index, params: { page: { size: 100 }, sort: '-id' }
          end
        end

        it 'shows an error message for invalid sort options' do
          get :index, params: { page: { size: 100 }, sort: '-invalid' }
          assert_response :bad_request
          assert @response.body =~ /sort is not valid/
        end
      end

      describe_with_arturo_enabled :cursor_pagination_remove_v1_deleted_users do
        it 'should use cursor pagination v2' do
          get :index, params: { page: { size: 100 } }
          assert_response :ok
          json_response = JSON.parse(response.body)
          assert json_response['deleted_users'].size >= 1
          assert_not_nil json_response['links']['prev']
          assert_not_nil json_response['links']['next']
        end

        it 'should respond with v2 pagintion when cursor pagination v1 params are given' do
          get :index, params: { limit: 100 }
          assert_response :ok
          json_response = JSON.parse(response.body)
          assert json_response['deleted_users'].size >= 1
          assert_not_nil json_response['links']['prev']
          assert_not_nil json_response['links']['next']
        end
      end

      it 'should render json with redacted nil' do
        get :index, params: { include_redacted: nil }
        users_json = JSON.parse(response.body)
        assert users_json['deleted_users'].size >= 1
        assert users_json['deleted_users'].all? { |user_json| user_json['active'] == false }
        assert users_json['deleted_users'].map { |user_json| user_json['id'].to_i }.include?(deleted_user.id)
        assert users_json['deleted_users'].map { |user_json| user_json['id'].to_i }.include?(redacted_user.id)
      end

      it 'should include the default limit and ordering params for cursor based pagination' do
        get :index, params: { limit: 1000 }
        assert_response :ok
        users_json = JSON.parse(response.body)
        assert users_json['deleted_users'].size >= 1
        assert users_json['after_url'].include? "limit=100"
        assert users_json['after_url'].include? "order=asc"
      end

      describe 'side load latest_tickets' do
        let(:ticket1) { tickets(:minimum_1) }
        let!(:comment1) { ticket1.comments.first }

        before do
          assert comment1.update_column(:author_id, deleted_user.id)
        end

        it 'should be ignored' do
          get :index, params: { include: 'latest_tickets' }

          users_json = JSON.parse(response.body)
          assert users_json['deleted_users'].all? { |user_json| user_json['latest_tickets'].nil? }
        end
      end
    end

    describe "#show" do
      it 'should render json' do
        get :show, params: { id: deleted_user.id }
        users_json = JSON.parse(response.body)

        assert_equal users_json['deleted_user']['id'].to_i, deleted_user.id
      end

      it 'should not show an active user' do
        delete :destroy, params: { id: active_user.id }
        assert_response :not_found
      end

      describe 'side load latest_tickets' do
        let(:ticket1) { tickets(:minimum_1) }
        let!(:comment1) { ticket1.comments.first }

        before do
          assert comment1.update_column(:author_id, deleted_user.id)
        end

        it 'should be accepted' do
          get :show, params: { id: deleted_user.id, include: 'latest_tickets' }

          latest_tickets = JSON.parse(response.body)['deleted_user']['latest_tickets']
          assert_equal latest_tickets.first['id'], ticket1.nice_id
        end
      end
    end

    describe "#destroy" do
      before do
        ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
      end

      it 'should render json' do
        delete :destroy, params: { id: deleted_user.id }
        assert_response :success
        users_json = JSON.parse(response.body)
        assert_equal users_json['deleted_user']['id'].to_i, deleted_user.id
      end

      it 'should not destroy an active user' do
        delete :destroy, params: { id: active_user.id }
        assert_response :not_found
      end

      describe "when rate limit is exceeded in an account with customized limit" do
        let(:rate_limit) { 5 }

        before do
          current_account.update_attributes!(settings: { user_permanent_deletions_threshold: rate_limit })

          # Reach the customized rate limit
          Timecop.freeze(1.second.ago) do
            rate_limit.times { Prop.throttle(:user_permanent_deletions, current_account.id) }
          end
          delete :destroy, params: { id: deleted_user.id }
        end

        it('responds with too many requests') { assert_response 429 }
      end

      describe "when rate limit is exceeded in an account without customized limit" do
        let(:rate_limit) { Prop.configurations[:user_permanent_deletions][:threshold] }

        before do
          # Reach the default rate limit
          rate_limit.times { Prop.throttle(:user_permanent_deletions, current_account.id) }
          delete :destroy, params: { id: deleted_user.id }
        end

        it('responds with too many requests') { assert_response 429 }
      end
    end
  end
end
