require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::SatisfactionReasonsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let!(:reason1) { account.satisfaction_reasons.create(reason_code: 1001, value: "Agent was too slow to respond") }
  let!(:reason2) { account.satisfaction_reasons.create(reason_code: 1002, value: "Agent was rude") }

  before do
    accept :json
  end

  with_options(controller: 'api/v2/satisfaction_reasons') do |request|
    request.should_route :get, '/api/v2/satisfaction_reasons', action: 'index'
    request.should_route :get, '/api/v2/satisfaction_reasons/1', action: 'show', id: 1
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index]
    should_be_unauthorized [:get, :show, id: 1]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index]
    should_be_forbidden [:get, :show, id: 1]
  end

  as_an_agent do
    should_be_forbidden [:get, :index]
    should_be_forbidden [:get, :show, id: 1]
  end

  as_an_admin do
    describe_with_arturo_setting_disabled :customer_satisfaction do
      describe_with_arturo_setting_disabled :csat_reason_code do
        should_be_forbidden [:get, :index]
        should_be_forbidden [:get, :show, id: 1]
      end
    end

    describe_with_arturo_setting_enabled :customer_satisfaction do
      describe_with_arturo_setting_disabled :csat_reason_code do
        should_be_forbidden [:get, :index]
        should_be_forbidden [:get, :show, id: 1]
      end

      describe_with_arturo_setting_enabled :csat_reason_code do
        describe "a GET to :index" do
          let(:json) { JSON.parse(@response.body) }
          before { get :index }

          should_use_presenter Api::V2::SatisfactionReasonPresenter
          it('responds with ok') { assert_response :ok }

          it "lists all the satisfaction rating reasons" do
            assert_equal 2, json["satisfaction_reasons"].size
          end
        end

        describe "a GET to :show" do
          let(:json) { JSON.parse(@response.body) }
          before { get :show, params: { id: reason1.id } }

          should_use_presenter Api::V2::SatisfactionReasonPresenter
          it('responds with ok') { assert_response :ok }

          it "shows the satisfaction rating reason specified by id" do
            assert_equal json["satisfaction_reason"]["id"], reason1.id
          end
        end
      end
    end
  end
end
