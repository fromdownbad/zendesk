require_relative "../../../support/test_helper"
require_relative "../../../support/api_scopes_helper"

SingleCov.covered!

describe Api::V2::CurrentAccountController do
  extend Api::Presentation::TestHelper
  extend ApiScopesHelper
  fixtures :accounts, :users

  before do
    use_ssl
    accept :json
  end

  should_route :get, "/api/v2/account", action: "show", controller: "api/v2/current_account"
  should_route :put, "/api/v2/account", action: "update", controller: "api/v2/current_account"

  as_an_anonymous_user do
    should_be_unauthorized :show
    should_be_unauthorized [:put, :update, {}]
  end

  as_an_end_user do
    should_be_forbidden :show
    should_be_forbidden [:put, :update, {}]
  end

  as_an_agent do
    should_be_forbidden [:put, :update, {}]

    describe "a GET to :show" do
      before { get :show }
      should_use_presenter Api::V2::AccountPresenter
    end
  end

  as_an_admin do
    should_be_forbidden [:put, :update, {}]

    describe "a GET to :show" do
      before { get :show }
      should_use_presenter Api::V2::AccountPresenter
    end

    describe "a GET to :show with includes" do
      describe "with external request" do
        before do
          Zendesk::Accounts::Client.any_instance.stubs(:products).returns([])
          Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
          Subscription.any_instance.stubs(:voice_account).returns(nil)
          Subscription.any_instance.stubs(:guide_preview).returns(nil)
          Subscription.any_instance.stubs(:suite_allowed?).returns(anything)
        end

        describe "with non internal client request" do
          before do
            get :show, params: { include: "settings,subscription" }
            @response = JSON.parse(@response.body)
          end

          it "presents included keys" do
            assert @response.key?("settings")
            assert @response.key?("subscription")
          end

          it "does not present account id" do
            refute @response['account'].key?("id")
          end
        end

        describe "with internal client" do
          before do
            ActionDispatch::Request.any_instance.expects(internal_client?: true)
            get :show, params: { include: "settings,subscription" }
            @response = JSON.parse(@response.body)
          end

          it "presents included keys" do
            assert @response.key?("settings")
            assert @response.key?("subscription")
          end

          it "presents account id" do
            assert @response['account'].key?("id")
          end
        end
      end
    end
  end

  as_an_owner do
    describe "a PUT to :update" do
      describe "when setting time_format" do
        describe "to 12 hours" do
          before do
            @account.uses_12_hour_clock = false
            @account.save!
            put :update, params: { account: { time_format: 12 } }
          end

          should_use_presenter Api::V2::AccountPresenter

          it "updates the account time_format to 12" do
            assert_equal 12, @account.time_format
          end
        end

        describe "to 24 hours" do
          before do
            @account.uses_12_hour_clock = true
            @account.save!
            put :update, params: { account: { time_format: 24 } }
          end

          should_use_presenter Api::V2::AccountPresenter

          it "updates the account time_format to 24" do
            assert_equal 24, @account.time_format
          end
        end

        describe "to something not 12 or 24" do
          before do
            @account.uses_12_hour_clock = true
            @account.save!
            put :update, params: { account: { time_format: 55 } }
          end

          it "does not update the account time_format" do
            assert_not_equal 55, @account.time_format
            assert_equal 12, @account.time_format
          end
        end
      end

      describe "when setting a new owner" do
        before do
          @new_owner = users(:minimum_admin_not_owner)
          put :update, params: { account: { owner_id: @new_owner.id } }
        end

        should_use_presenter Api::V2::AccountPresenter

        it "updates the account" do
          assert_equal @new_owner.id, accounts(:minimum).owner_id
        end
      end

      describe "when setting account name" do
        before do
          put :update, params: { account: { name: "Account 123" } }
        end

        should_use_presenter Api::V2::AccountPresenter

        it "updates the account" do
          assert_equal "Account 123", accounts(:minimum).name
        end
      end
    end
  end

  describe "embeddable subsystem user" do
    as_a_subsystem_user(account: :minimum, user: "embeddable") do
      should_be_forbidden :update

      describe "a GET to :show" do
        before { get :show }
        it("responds with success") { assert_response :success }
      end
    end
  end

  as_a_subsystem_user(user: 'zopim', account: :minimum) do
    should_be_forbidden [:put, :update, { account: { name: "Account 123" } }]
    should_be_authorized { get :show }

    describe "a GET to :show" do
      before { get :show }
      it('responds with success') { assert_response :success }
    end
  end

  as_a_subsystem_user(user: 'bime', account: :minimum) do
    should_be_forbidden [:put, :update, { account: { name: "Account 123" } }]
    should_be_authorized { get :show }

    describe "a GET to :show" do
      before { get :show }
      it('responds with success') { assert_response :success }
    end
  end

  as_a_subsystem_user(user: 'guide_rest', account: :minimum) do
    should_be_forbidden [:put, :update, { account: { name: "Account 123" } }]
    should_be_authorized { get :show }

    describe "a GET to :show" do
      before { get :show }
      it('responds with success') { assert_response :success }
    end
  end
  describe "sandbox_orchestrator subsystem user" do
    describe "updating production accounts" do
      as_a_subsystem_user(user: 'sandbox_orchestrator', account: :minimum) do
        should_be_forbidden [:put, :update, { account: { name: "Account 123" } }]
      end
    end
    describe "updating sandboxes" do
      before do
        accounts(:trial).update_column(:sandbox_master_id, 1)
      end
      as_a_subsystem_user(user: 'sandbox_orchestrator', account: :trial) do
        should_be_authorized { put :update, params: { account: { name: "Account 123" } } }
      end
    end
  end
end
