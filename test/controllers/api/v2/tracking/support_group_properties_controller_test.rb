require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tracking::SupportGroupPropertiesController do
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: 'api/v2/tracking/support_group_properties') do |request|
    request.should_route :get, '/api/v2/tracking/support_group_properties', action: 'index'
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index]
  end

  as_an_agent do
    describe "with arturo enabled" do
      it "responds with :ok status and valid JSON" do
        get :index

        assert_response :success

        props = JSON.parse(response.body)
        expected = {
          "account_id" => @account.id,
          "account_name" => "Minimum account",
          "language" => "English",
          "locale_id" => 1,
          "signup_source" => "",
          "subdomain" => "minimum",
          "company_size" => "Small team",
          "has_active_suite" => false
        }

        props.must_equal(expected)
      end
    end
  end
end
