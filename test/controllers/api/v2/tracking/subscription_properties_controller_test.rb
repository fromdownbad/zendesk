require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tracking::SubscriptionPropertiesController do
  fixtures :all
  let(:created_at) { DateTime.new(2012, 12, 12) }
  let(:zopim_subscription) do
    stub(
      created_at: created_at,
      max_agents: 3,
      present_plan_type:  1,
      present_plan_name:  'Trial'
    )
  end

  before do
    accept :json
  end

  with_options(controller: 'api/v2/tracking/subscription_properties') do |request|
    request.should_route :get, '/api/v2/tracking/subscription_properties', action: 'index'
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index]
  end

  as_an_agent do
    describe "with arturo enabled" do
      describe "with a Talk subscription" do
        let(:body)    { read_test_file('voice/voice_account.json') }
        let(:headers) { { content_type: 'application/json' } }
        let(:request_path) { %r{/api/v2/channels/voice/voice_account.json} }

        before do
          stub_request(:get, request_path).to_return(body: body, headers: headers)
        end

        it "responds with :ok status and valid JSON" do
          @account.stubs(:zopim_subscription).returns(nil)
          @account.stubs(:voice_enabled?).returns(true)
          get :index

          assert_response :success

          props = JSON.parse(response.body)
          expected = {
            "talk_account_created" => "2017-12-04T14:55:25",
            "talk_account_created_week" => "2017-49W",
            "talk_account_created_month" => "2017Dec",
            "talk_max_agents" => 4,
            "talk_plan_name" => "advanced",
            "talk_plan_type" => 2
          }

          props.slice(*expected.keys).must_equal(expected)
        end

        it "should not return talk data if none exists" do
          @account.stubs(:zopim_subscription).returns(nil)
          @account.stubs(:voice_enabled?).returns(false)
          get :index

          assert_response :success

          props = JSON.parse(response.body)
          talk = props.keys.select { |key| key.to_s.match(/^talk_/) }
          talk.must_be_empty
        end
      end

      describe "with a support subscription" do
        it "responds with :ok status and valid JSON" do
          @account.stubs(:zopim_subscription).returns(nil)
          get :index

          assert_response :success

          props = JSON.parse(response.body)
          expected = {
            "support_account_created" => "2008-10-10T10:00:00",
            "support_account_created_week" => "2008-40W",
            "support_account_created_month" => "2008Oct",
            "support_account_type" => "customer",
            "support_max_agents" => 10,
            "support_plan_name" => "Regular",
            "support_plan_type" => 2
          }

          props.slice(*expected.keys).must_equal(expected)
        end
      end

      describe "with a guide subscription" do
        it "responds with :ok status and valid JSON" do
          @account.stubs(:zopim_subscription).returns(nil)
          FactoryBot.create(:guide_subscription, account: @account, created_at: created_at)
          get :index

          assert_response :success

          props = JSON.parse(response.body)
          expected = {
            "guide_account_created" => "2012-12-12T00:00:00",
            "guide_account_created_week" => "2012-50W",
            "guide_account_created_month" => "2012Dec",
            "guide_max_agents" => 5,
            "guide_plan_name" => "Lite",
            "guide_plan_type" => 1
          }

          props.slice(*expected.keys).must_equal(expected)
        end
      end

      describe "with a chat subscription" do
        it "responds with :ok status and valid JSON" do
          @account.stubs(:zopim_subscription).returns(zopim_subscription)
          get :index

          assert_response :success

          props = JSON.parse(response.body)
          expected = {
            "chat_account_created" => "2012-12-12T00:00:00",
            "chat_account_created_week" => "2012-50W",
            "chat_account_created_month" => "2012Dec",
            "chat_max_agents" => 3,
            "chat_plan_name" => "Trial",
            "chat_plan_type" => 1
          }

          props.slice(*expected.keys).must_equal(expected)
        end
      end
    end
  end
end
