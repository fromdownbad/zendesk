require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tracking::SupportUserPropertiesController do
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: 'api/v2/tracking/support_user_properties') do |request|
    request.should_route :get, '/api/v2/tracking/support_user_properties', action: 'index'
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index]
  end

  as_an_agent do
    it 'shows assuming is false by default' do
      get :index

      JSON.parse(response.body)['support_assuming'].must_equal false
    end

    it 'shows assuming is true' do
      @controller.expects(is_assuming_user?: true).at_least(1)
      get :index

      JSON.parse(response.body)['support_assuming'].must_equal true
    end
  end

  as_an_agent do
    describe "with arturo enabled" do
      it "responds with :ok status and valid JSON" do
        get :index

        assert_response :success

        props = JSON.parse(response.body)
        expected = {
            "user_id" => 57888,
            "support_is_admin" => false,
            "support_is_owner" => false,
            "support_is_end_user" => false,
            "support_user_role" => "Agent",
            "support_language" => "English",
            "support_locale_id" => nil,
            "support_assuming" => false,
            "support_locale" => "en-us"
        }

        props.must_equal(expected)
      end
    end
  end
end
