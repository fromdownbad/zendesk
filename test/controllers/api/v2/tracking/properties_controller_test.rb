require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tracking::PropertiesController do
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: 'api/v2/tracking/properties') do |request|
    request.should_route :get, '/api/v2/tracking/properties', action: 'index'
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index]
  end

  as_an_agent do
    it 'shows assuming is false by default' do
      get :index

      JSON.parse(response.body)['assuming'].must_equal false
    end

    it 'shows assuming is true' do
      @controller.expects(is_assuming_user?: true).at_least(1)
      get :index

      JSON.parse(response.body)['assuming'].must_equal true
    end

    it 'shows assuming is true when assuming from monitor' do
      @controller.expects(is_assuming_user_from_monitor?: true).at_least(1)
      get :index

      JSON.parse(response.body)['assuming'].must_equal true
    end

    it 'shows the correct pod number' do
      get :index

      JSON.parse(response.body)['pod'].must_equal @account.pod_id
    end
  end

  as_an_agent do
    describe "with arturo enabled" do
      it "responds with :ok status and valid JSON" do
        get :index

        assert_response :success

        props = JSON.parse(response.body)
        expected = {
          "subdomain" => "minimum",
          "help_desk_size" => "Small team",
          "customer_type" => "VSB",
          "locale_id" => 1,
          "language" => "English",
          "signup_source" => "",
          "is_partner" => false,
          "user_role" => "Agent",
          "is_owner" => false,
          "is_end_user" => false,
          "account_type" => "customer",
          "plan_type" => 2,
          "plan_name" => "Regular",
          "is_trial" => false,
          "max_agents" => 10,
          "bc_type" => 1,
          "ab_buy-now" => "click",
          "ab_buy-now_finished" => false,
          "assuming" => false,
          "pod" => @account.pod_id
        }

        props.slice(*expected.keys).must_equal(expected)
      end
    end
  end
end
