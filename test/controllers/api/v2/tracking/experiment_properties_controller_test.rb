require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Tracking::ExperimentPropertiesController do
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: 'api/v2/tracking/experiment_properties') do |request|
    request.should_route :get, '/api/v2/tracking/experiment_properties', action: 'index'
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index]
  end

  as_an_agent do
    expected_response = {
      "ab_buy-now" => "click",
      "ab_buy-now_finished" => false
    }

    describe_with_arturo_disabled :experiment_properties_cache_control_header do
      it "responds with :ok status and valid JSON" do
        get :index
        assert_response :success
        assert_nil response.headers['Cache-Control']

        props = JSON.parse(response.body)
        assert_equal props, expected_response
      end
    end

    describe_with_arturo_enabled :experiment_properties_cache_control_header do
      it "responds with :ok status and valid JSON and cache headers" do
        get :index
        assert_response :success
        assert_equal response.headers['Cache-Control'], 'max-age=30, private'

        props = JSON.parse(response.body)
        assert_equal props, expected_response
      end
    end
  end
end
