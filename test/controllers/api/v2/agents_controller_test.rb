require_relative '../../../support/test_helper'
require 'radar_client_rb'

SingleCov.covered!

describe Api::V2::AgentsController do
  extend Api::Presentation::TestHelper

  fixtures :accounts, :users, :user_settings, :tickets, :events, :activities, :role_settings

  before do
    @agent = users(:minimum_agent)
    accept :json
  end

  with_options(controller: 'api/v2/agents') do |request|
    request.should_route :get, '/api/v2/agents', action: 'index'
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_agent do
    describe 'when lotus_agent_availability is enabled' do
      before do
        Arturo.enable_feature!(:lotus_agent_availability)
      end

      describe 'with no online agents' do
        before do
          presence = stub(get: {})
          ::Radar::Client.any_instance.expects(:presence).with('agents').returns(presence)
        end

        describe 'a GET to :index' do
          before { get :index }

          it 'renders the response' do
            agents = JSON.parse(@response.body)
            assert_equal(JSON.parse('{"agents":[],"next_page":null,"previous_page":null,"count":0}'), agents)
          end
        end
      end

      describe 'with online agent(s)' do
        before do
          presence = stub(get: JSON.parse(%({"#{@agent.id}":{"clients":{"l3tj1uJ4oFQzUBHLAAAN":{"name":"Agent Extraordinaire","id":#{@agent.id}}},"userType":2}})))
          ::Radar::Client.any_instance.expects(:presence).with('agents').returns(presence)
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('api/v2/agents')
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('api/v2/agents/cache_miss')
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.Api::V2::AgentsController.index', anything).at_least(1)
        end

        describe 'without an active shared session' do
          before do
            @agent.shared_sessions.unexpired.each(&:destroy)
            assert_empty @agent.shared_sessions.unexpired
          end

          describe 'a GET to :index' do
            before { get :index }

            should_use_presenter Api::V2::AgentAvailabilityPresenter, status: :ok

            it 'renders the response' do
              agents = JSON.parse(@response.body)
              assert_equal(0, agents['agents'].length)
            end
          end
        end

        describe 'an active shared session' do
          before do
            FactoryBot.create(:session_record, account: @agent.account, user: @agent)
            assert_not_empty @agent.shared_sessions.unexpired
          end

          describe 'a GET to :index' do
            before { get :index }

            should_use_presenter Api::V2::AgentAvailabilityPresenter, status: :ok

            it 'renders the response' do
              agents = JSON.parse(@response.body)
              assert_equal(1, agents['agents'].length)
            end
          end
        end

        describe 'with pagination' do
          before do
            @controller.expects(:paginate).with([@agent]).returns([]) # We trust paginate will do its thing
          end

          describe 'a GET to :index' do
            before { get :index }

            it 'renders the paginated response' do
              agents = JSON.parse(@response.body)
              assert_equal(0, agents['agents'].length)
            end
          end
        end
      end
    end

    describe 'when lotus_agent_availability is disabled' do
      before do
        Arturo.disable_feature!(:lotus_agent_availability)
      end

      describe 'a GET to :index' do
        should_be_forbidden :index
      end
    end
  end
end
