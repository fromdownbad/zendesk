require_relative "../../../../support/test_helper"
require_relative "../../../../support/custom_field_options_test_helper"

SingleCov.covered!

describe Api::V2::UserFields::CustomFieldOptionsController do
  extend Api::V2::TestHelper
  include CustomFieldOptionsTestHelper

  fixtures :accounts, :cf_fields, :cf_dropdown_choices, :users

  before do
    accept :json

    @parent_field = cf_fields(:dropdown1)
    @custom_field_option = cf_dropdown_choices(:v100)
  end

  with_options(controller: "api/v2/user_fields/custom_field_options") do |request|
    request.should_route :get,    "/api/v2/user_fields/1/options",     action: "index", user_field_id: "1"
    request.should_route :get,    "/api/v2/user_fields/1/options/1",   action: "show", user_field_id: "1", id: "1"
    request.should_route :post,   "/api/v2/user_fields/1/options",     action: "create_or_update", user_field_id: "1"
    request.should_route :delete, "/api/v2/user_fields/1/options/1",   action: "destroy", user_field_id: "1", id: "1"
  end

  as_an_anonymous_user do
    should_be_unauthorized [:get, :index, {user_field_id: 1}]
    should_be_unauthorized [:get, :show, {user_field_id: 1, id: 1}]
    should_be_unauthorized [:post, :create_or_update, {user_field_id: 1}]
    should_be_unauthorized [:delete, :destroy, {user_field_id: 1, id: 1}]
  end

  as_an_end_user do
    should_be_forbidden [:get, :index, {user_field_id: 1}]
    should_be_forbidden [:get, :show, {user_field_id: 1, id: 1}]
    should_be_forbidden [:post, :create_or_update, {user_field_id: 1}]
    should_be_forbidden [:delete, :destroy, {user_field_id: 1, id: 1}]
  end

  as_an_admin do
    describe "a GET to :index" do
      describe "should" do
        before do
          get :index, params: { user_field_id: @parent_field.id }
        end

        it "returns the custom field's options" do
          assert_matches_custom_field_options(@parent_field)
        end

        should_use_presenter Api::V2::CustomFieldOptionPresenter
      end

      describe "with cursor pagination" do
        it 'should include the default limit and ordering params for cursor based pagination' do
          get :index, params: { user_field_id: @parent_field.id, page: { size: 100 } }
          assert_response :ok
          json_response = JSON.parse(response.body)
          assert json_response['custom_field_options'].size >= 1
          assert_not_nil json_response['links']['prev']
          assert_not_nil json_response['links']['next']
        end

        describe 'default sort parameter of id for v2' do
          it 'generates the correct order clause' do
            assert_sql_queries(1, /ORDER BY `cf_dropdown_choices`.`id` DESC/) do
              get :index, params: { user_field_id: @parent_field.id, page: { size: 100 }, sort: '-id' }
            end
          end

          it 'shows an error message for invalid sort options' do
            get :index, params: { user_field_id: @parent_field.id, page: { size: 100 }, sort: '-invalid' }
            assert_response :bad_request
            assert @response.body =~ /sort is not valid/
          end
        end

        describe_with_arturo_enabled :cursor_pagination_custom_fields_options_index do
          it 'should default to cursor pagination when no params are present' do
            get :index, params: { user_field_id: @parent_field.id }
            assert_response :ok
            json_response = JSON.parse(response.body)
            assert json_response['custom_field_options'].size >= 1
            assert_not_nil json_response['links']['prev']
            assert_not_nil json_response['links']['next']
          end

          it 'should use offset pagination when offset pagination params are present' do
            get :index, params: { user_field_id: @parent_field.id, per_page: 100 }
            assert_response :ok
            json_response = JSON.parse(response.body)
            assert json_response['custom_field_options'].size >= 1
            assert_equal 2, (json_response.keys & ['next_page', 'previous_page']).size
          end
        end
      end

      describe_with_arturo_enabled :remove_offset_pagination_custom_fields_options_index do
        it 'should use cursor pagination' do
          get :index, params: { user_field_id: @parent_field.id, page: { size: 100 } }
          assert_response :ok
          json_response = JSON.parse(response.body)
          assert json_response['custom_field_options'].size >= 1
          assert_not_nil json_response['links']['prev']
          assert_not_nil json_response['links']['next']
        end

        it 'should use cursor pagination when offset pagination params are present' do
          get :index, params: { user_field_id: @parent_field.id, per_page: 100 }
          assert_response :ok
          json_response = JSON.parse(response.body)
          assert json_response['custom_field_options'].size >= 1
          assert_not_nil json_response['links']['prev']
          assert_not_nil json_response['links']['next']
        end
      end
    end

    describe "a GET to :show" do
      before do
        get :show, params: { user_field_id: @parent_field.id, id: @custom_field_option.id }
      end

      it "returns the custom field option" do
        assert_matches_custom_field_option(@custom_field_option)
      end

      should_use_presenter Api::V2::CustomFieldOptionPresenter
    end

    describe "a POST to :create_or_update" do
      describe "with an unused value" do
        before do
          post :create_or_update, params: { user_field_id: @parent_field.id, custom_field_option: { name: 'my name', value: 'my_value' } }
        end

        it "creates a new option" do
          assert_creates_custom_field_option
        end

        should_use_presenter Api::V2::CustomFieldOptionPresenter
      end

      describe "with a matching id" do
        before do
          @params = { name: 'my new name', value: 'my_new_value' }
          post :create_or_update, params: { user_field_id: @parent_field.id, custom_field_option: { id: @custom_field_option.id }.merge(@params) }
        end

        it "updates the existing option's value" do
          assert_updates_existing_custom_field_option(@custom_field_option, @params)
        end

        should_use_presenter Api::V2::CustomFieldOptionPresenter
      end

      describe "with an invalid id" do
        before do
          post :create_or_update, params: { user_field_id: @parent_field.id, custom_field_option: { id: 999, name: 'my name', value: 'my_other_new_value' } }
        end

        it "returns an error" do
          assert_handles_invalid_custom_field_option_update(@custom_field_option)
        end
      end

      describe "without a given position" do
        before do
          @options_count = @parent_field.custom_field_options.count
          post :create_or_update, params: { user_field_id: @parent_field.id, custom_field_option: { name: 'my name', value: 'my_other_other_new_value' } }
        end

        it "correctly sets one for the caller" do
          assert_equal @options_count, JSON.parse(@response.body)['custom_field_option']['position']
        end
      end

      describe "with a deleted option" do
        before do
          @custom_field_option.mark_as_deleted
          @custom_field_option.save!
          post :create_or_update, params: { user_field_id: @parent_field.id, custom_field_option: { name: 'my name', value: @custom_field_option.value } }
        end

        it "restores the option" do
          assert_equal @custom_field_option.id, JSON.parse(@response.body)['custom_field_option']['id']
        end

        should_use_presenter Api::V2::CustomFieldOptionPresenter
      end

      describe "with invalid params" do
        describe "empty body" do
          before do
            post :create_or_update, params: { user_field_id: @parent_field.id }
          end

          it "responds with bad_request" do
            assert_response :bad_request
          end
        end

        describe "empty name" do
          before do
            post :create_or_update, params: { user_field_id: @parent_field.id, custom_field_option: { value: 'some value' } }
          end

          it "responds with bad_request" do
            assert_response :bad_request
          end
        end

        describe "invalid param" do
          before do
            post :create_or_update, params: { user_field_id: @parent_field.id, invalid_param: "invalid_param" }
          end

          it "responds with bad_request" do
            assert_response :bad_request
          end
        end
      end
    end

    describe "a DELETE to :destroy" do
      before do
        delete :destroy, params: { user_field_id: @parent_field.id, id: @custom_field_option.id }
      end

      it "deletes the option" do
        assert_deletes_custom_field_option(@custom_field_option, CustomField::DropdownChoice)
      end
    end
  end
end
