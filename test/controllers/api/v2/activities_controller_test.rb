require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ActivitiesController do
  extend Api::V2::TestHelper
  fixtures :accounts, :users, :user_settings, :tickets, :events, :activities

  before do
    accept :json
  end

  with_options(controller: "api/v2/activities") do |request|
    request.should_route :get,  "/api/v2/activities",     action: "index"
    request.should_route :get,  "/api/v2/activities/123", action: "show", id: "123"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index, :show
  end

  as_an_end_user do
    should_be_forbidden :index, :show
  end

  as_an_agent do
    describe "a GET to :index" do
      describe "without since" do
        before { get :index }
        should_use_presenter Api::V2::TicketActivityPresenter, status: :ok

        it "renders all activities" do
          activities = JSON.parse(@response.body)
          assert_equal 3, activities["count"]
        end
      end

      describe "with since" do
        before { get :index, params: { since: "2012-02-10T10:00:00Z" } }
        should_use_presenter Api::V2::TicketActivityPresenter, status: :ok

        it "renders some activities" do
          activities = JSON.parse(@response.body)
          assert_equal 1, activities["count"]
        end
      end
    end

    describe "a GET to :show" do
      before { get :show, params: { id: activities(:minimum_activity_1).id } }
      should_use_presenter Api::V2::TicketActivityPresenter, status: :ok
    end
  end
end
