require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::V2::Portal::RequestsController do
  extend Api::V2::TestHelper

  fixtures :all

  before do
    accept :json
  end

  with_options(controller: "api/v2/portal/requests") do |request|
    request.should_route :post, "/api/v2/portal/requests", action: :create
  end

  as_an_agent do
    describe "on a POST to #create" do
      it "creates and returns ticket location" do
        post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: @user.email } }
        assert_response :created
        assert_match api_v2_tickets_url, @response.headers["Location"]
      end

      it "creates with empty due_at" do
        post :create, params: { request: { due_at: "", subject: "Help!", comment: { value: "I need somebody!" }, requester: @user.email } }
        assert_response :created
      end
    end
  end

  as_an_anonymous_user do
    describe "on a POST to #create" do
      before { Ticket.any_instance.stubs(:check_ticket_form_id).returns(true) }
      let(:params) { {request: { subject: "Help meeee!", ticket_form_id: "1234", comment: { value: "I need somebody!" }, requester: "test@test.com" }} }

      # Not true anymore with the anonymous request rate limiting
      # See https://github.com/zendesk/zendesk/pull/27782
      # describe "as HC" do
      #   before do
      #     @account.stubs(is_signup_required?: false)
      #     @request.user_agent = "HelpCenter"
      #     post :create, params
      #   end
      #
      #   before_should "not call Prop.throttle!" do
      #     Prop.expects(:throttle!).never
      #   end
      # end

      describe "with valid request parameters" do
        before do
          @account.stubs(has_ticket_forms?: true)
          post :create, params: params
        end

        it('responds with created') { assert_response :created }

        it "creates with the ticket_form_id" do
          assert_equal 1234, @account.tickets.last.ticket_form_id
        end

        it "returns request location" do
          assert_match api_v2_requests_url, @response.headers["Location"]
        end

        it "CORSs header should be present" do
          assert_equal "*", response.headers["Access-Control-Allow-Origin"]
        end

        describe "repeated more than 5 times an hour" do
          let(:ip_address) { "0.0.0.0" }

          before do
            Timecop.freeze do
              ActionController::TestRequest.any_instance.stubs(remote_ip: ip_address)

              4.times { Prop.throttle!(:anonymous_requests, [accounts(:minimum).id, ip_address]) }

              post :create, params: params
            end
          end

          it "raises a throttle exception" do
            assert_response 429
          end
        end
      end

      describe "with valid parameters and requester sent as a hash" do
        let(:locale_params) do
          { request: {
              subject: "Help meeee!",
              ticket_form_id: "1234",
              comment: { value: "I need somebody!" },
              requester: { email: "test@test.com", name: "Test", locale_id: 6 }
            }}
        end

        before do
          post :create, params: locale_params
        end

        it('responds with created') { assert_response :created }
      end

      describe "with valid suspended parameters" do
        before do
          @account.stubs(is_signup_required?: true)
          post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: "test@test.com" } }
        end

        it('responds with created') { assert_response :created }

        it "returns suspended location" do
          assert_match api_v2_suspended_tickets_url, @response.headers["Location"]
        end
      end

      describe "with valid suspended parameters from a branded subdomain" do
        let(:brand) { FactoryBot.create(:brand) }

        before do
          @controller.stubs(:current_brand).returns(brand)
          @account.stubs(is_signup_required?: true)

          post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" }, requester: "test@test.com" } }
        end

        it('responds with created') { assert_response :created }

        it "creates a branded suspended ticket" do
          assert_equal brand.id, SuspendedTicket.last.properties[:brand_id]
        end
      end

      describe "with invalid parameters" do
        before { post :create, params: { request: {} } }
        should_use_presenter Api::V2::ErrorsPresenter, status: :unprocessable_entity
      end
    end
  end
end
