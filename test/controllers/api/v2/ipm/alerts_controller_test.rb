require_relative "../../../../support/test_helper"
require_relative "../../../../support/brand_test_helper"

SingleCov.covered! uncovered: 1

describe Api::V2::Ipm::AlertsController do
  include BrandTestHelper
  extend Api::V2::TestHelper
  fixtures :accounts, :users

  before do
    accept :json
  end

  with_options(controller: "api/v2/ipm/alerts") do |request|
    request.should_route :get, "/api/v2/ipm/alerts", action: "index"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_agent do
    describe "a GET to #index" do
      describe "presenter" do
        before do
          @alert = FactoryBot.create(:ipm_alert)
          get :index
        end
        should_use_presenter Api::V2::Ipm::AlertPresenter
      end

      describe "with irrelevant alerts" do
        before do
          @alert = FactoryBot.create(:ipm_alert)

          # restricted to certain pods
          FactoryBot.create(:ipm_alert, pods: [123])
          # disabled
          FactoryBot.create(:ipm_alert, enabled: false)
          # dismissed
          ::Ipm::AlertDismissal.create user: @user, account: @account, alert: FactoryBot.create(:ipm_alert)
          # restricted to certain languages
          FactoryBot.create(:ipm_alert, languages: [2])
          @user.stubs(:locale_id).returns(1)
          # restricted to certain account ids
          FactoryBot.create(:ipm_alert, account_ids: [123])

          get :index
        end

        it "shows alerts for this user" do
          assert_equal 1, JSON.parse(@response.body)["alerts"].length
          assert_equal @alert.id, JSON.parse(@response.body)["alerts"][0]["id"]
        end

        it "shows provide the correct interface and custom factors to the model" do
          assert_equal 1, JSON.parse(@response.body)["alerts"].length
          assert_equal @alert.id, JSON.parse(@response.body)["alerts"][0]["id"]
        end
      end

      describe "with subdomain exclusions" do
        before do
          @alert = FactoryBot.create(
            :ipm_alert,
            exclude_account_ids: [123]
          )

          # restricted to certain pods
          FactoryBot.create(:ipm_alert, pods: [123])

          # disabled
          FactoryBot.create(:ipm_alert, enabled: false)

          # dismissed
          ::Ipm::AlertDismissal.create(
            user: @user,
            account: @account,
            alert: FactoryBot.create(:ipm_alert)
          )

          # restricted to certain languages
          FactoryBot.create(:ipm_alert, languages: [2])
          @user.stubs(account_id: 123)
          @user.locale_id = 2

          get :index
        end

        it "only include applicable alerts for this user" do
          assert_equal 1, JSON.parse(@response.body)['alerts'].length
        end

        it "does not include alerts for this user which excluded by subdomain" do
          assert_not_equal @alert.id, JSON.parse(@response.body)['alerts'][0]['id']
        end
      end

      describe "with custom factors and segmenting by interface" do
        before do
          @alert = FactoryBot.create(:ipm_alert)
          @controller.stubs(:build_custom_factors).returns(['boogaloo'])
          expected_options = { interface: 'classic', custom: ['boogaloo'] }
          Ipm::Alert.expects(:for_user).with(@user, expected_options).returns([@alert])
          get :index, params: { interface: 'classic' }
        end

        it "shows provide the correct interface and custom factors to the model" do
          assert_equal 1, JSON.parse(@response.body)["alerts"].length
          assert_equal @alert.id, JSON.parse(@response.body)["alerts"][0]["id"]
        end
      end

      describe 'with the owner_email_verification_required custom factor' do
        let(:json) { JSON.parse(response.body) }

        before do
          @alert = FactoryBot.create(:ipm_alert, custom_factors: 'owner_email_verification_required')
        end

        describe 'when current_user is the account owner' do
          before do
            @user.stubs(is_account_owner?: true)
          end

          it 'includes the alert when user is NOT verified' do
            @user.stubs(is_verified?: false)
            get :index

            assert_response :ok
            assert_includes json.fetch('alerts').map { |alert| alert.fetch("id") }, @alert.id
          end

          it 'does not include the alert when user is verified' do
            @user.stubs(is_verified?: true)
            get :index

            assert_response :ok
            assert_empty json.fetch('alerts')
          end
        end

        describe 'when current_user is not the account owner' do
          before do
            @user.stubs(is_account_owner?: false)
          end

          it 'does not include the alert' do
            get :index

            assert_response :ok
            assert_empty json.fetch('alerts')
          end
        end
      end

      describe "with the account_needs_ssl_certificate custom factor" do
        let(:json) { JSON.parse(response.body) }
        before do
          @user.stubs(is_admin?: true)
          @alert = FactoryBot.create(:ipm_alert, custom_factors: 'account_needs_ssl_certificate')
          refute @account.settings.ssl_enabled
        end

        describe "and the current user is an admin" do
          it "does not include the ssl alert without hostmapping" do
            get :index

            assert_response :ok
            assert_empty json.fetch('alerts')
          end

          it "includes the ssl alert if the account has a hostmapped brand" do
            create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'brand1.com')
            get :index

            assert_response :ok
            assert_includes json.fetch('alerts').map { |alert| alert.fetch("id") }, @alert.id
          end

          it "does not include the ssl alert if the account has a hostmapped brand but ssl is enabled" do
            create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'brand1.com')
            @account.settings.ssl_enabled = true
            @account.settings.save!
            get :index

            assert_response :ok
            assert_empty json.fetch('alerts')
          end

          it "does not include the ssl alert if the account has a hostmapped brand but has a cert" do
            create_brand(@account, name: 'brand1', subdomain: 'brand1', host_mapping: 'brand1.com')
            cert = @account.certificates.build
            cert.state = 'active'
            cert.save!
            get :index

            assert_response :ok
            assert_empty json.fetch('alerts')
          end
        end
      end

      describe "with the user_account_has_fb_pages_with_messages_enabled custom factor" do
        let(:json) { JSON.parse(response.body) }

        before do
          @alert = FactoryBot.create(
            :ipm_alert,
            custom_factors: 'user_account_has_fb_pages_with_messages_enabled',
            enabled: true,
            text: 'fb account'
          )
        end

        describe "and the current user's account has at least one active facebook page" do
          before do
            @fb_pages = ::Facebook::Page.where(account_id: @account.id)
            assert @fb_pages.present?
          end

          [true, 'true', 0].each do |truthy_value|
            describe "with the include message setting" do
              before do
                @fb_pages.each do |page|
                  page.state = ::Facebook::Page::State::ACTIVE
                  page.page_settings[:include_messages] = truthy_value
                  page.save!
                end
              end

              it 'includes the alert' do
                get :index

                assert_response :ok

                assert json.fetch('alerts').any? { |alert| alert['text'] == @alert.text }
              end
            end
          end

          [false, nil].each do |falsy_value|
            describe "and none of the pages have the include message setting" do
              before do
                @fb_pages.each do |page|
                  page.state = ::Facebook::Page::State::ACTIVE
                  page.page_settings[:include_messages] = falsy_value
                  page.save!
                end
              end

              it 'does not include any facebook alerts' do
                get :index

                assert_response :ok

                assert_empty json.fetch('alerts')
              end
            end
          end
        end

        describe "and the current user's account does not have an active facebook page" do
          before do
            ::Facebook::Page.where(account_id: @account.id).update_all(state: ::Facebook::Page::State::INACTIVE)
          end

          it 'does not include any facebook alerts' do
            get :index

            assert_response :ok

            assert_empty json.fetch('alerts')
          end
        end
      end
    end

    describe "DELETE" do
      before do
        @alert = FactoryBot.create(:ipm_alert)
      end

      it "responds with 204" do
        delete :destroy, params: { id: @alert.id }
        assert_response :no_content
      end

      it "creates a AlertDismissal" do
        delete :destroy, params: { id: @alert.id }
        assert_equal @alert, Ipm::AlertDismissal.last.alert
      end
    end

    describe "on a DELETE to #destroy_many" do
      before do
        @alert1 = FactoryBot.create(:ipm_alert)
        @alert2 = FactoryBot.create(:ipm_alert, custom_factors: 'foo')
      end

      it "responds with 204" do
        delete :destroy_many, params: { ids: [@alert1.id, @alert2.id].join(',') }
        assert_response :no_content
      end

      it 'dismiss the alerts' do
        delete :destroy_many, params: { ids: [@alert1.id, @alert2.id].join(',') }
        @user.reload
        assert_equal [], Ipm::Alert.for_user(@user)
        assert_equal [@alert1.id, @alert2.id], Ipm::AlertDismissal.all.map(&:ipm_alert_id)
      end

      it "does not create dismissals for alerts not visible to user" do
        delete :destroy_many, params: { ids: [@alert1.id, @alert2.id].join(',') }
        Ipm::AlertDismissal.without_arsi.delete_all
        @alert = FactoryBot.create(:ipm_alert, pods: [123])

        delete :destroy_many, params: { ids: @alert.id }
        assert_equal [], @user.ipm_alert_dismissals
        @alert.update_attributes! pods: ''

        delete :destroy_many, params: { ids: @alert.id }
        assert_equal 1, @user.reload.ipm_alert_dismissals.length
      end
    end
  end
end
