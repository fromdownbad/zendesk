require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Api::V2::Ipm::FeatureNotificationsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
  end

  with_options(controller: "api/v2/ipm/feature_notifications") do |request|
    request.should_route :get, "/api/v2/ipm/feature_notifications", action: "index"
  end

  as_an_anonymous_user do
    should_be_unauthorized :index
  end

  as_an_end_user do
    should_be_forbidden :index
  end

  as_an_agent do
    describe "a GET to #index" do
      describe "presenter" do
        before do
          @feature_notification = FactoryBot.create(:ipm_feature_notification)
          get :index
        end
        should_use_presenter Api::V2::Ipm::FeatureNotificationPresenter
      end

      describe "with irrelevant feature_notifications" do
        before do
          @feature_notification = FactoryBot.create(:ipm_feature_notification)

          # restricted to certain pods
          FactoryBot.create(:ipm_feature_notification, pods: [123])
          # disabled
          FactoryBot.create(:ipm_feature_notification, enabled: false)
          # dismissed
          ::Ipm::FeatureNotificationDismissal.create user: @user, account: @account, feature_notification: FactoryBot.create(:ipm_feature_notification)
          # restricted to certain languages
          FactoryBot.create(:ipm_feature_notification, languages: [2])
          @user.stubs(:locale_id).returns(1)
          # restricted to certain account ids
          FactoryBot.create(:ipm_feature_notification, account_ids: [123])

          get :index
        end

        it "shows feature_notifications for this user" do
          assert_equal 1, JSON.parse(@response.body)["feature_notifications"].length
          assert_equal @feature_notification.id, JSON.parse(@response.body)["feature_notifications"][0]["id"]
        end
      end

      describe "with subdomain exclusion" do
        before do
          @feature_notification = FactoryBot.create(
            :ipm_feature_notification,
            exclude_account_ids: [2, 3, 4]
          )

          # restricted to certain pods
          FactoryBot.create(:ipm_feature_notification, pods: [123])

          # disabled
          FactoryBot.create(:ipm_feature_notification, enabled: false)

          # dismissed
          ::Ipm::FeatureNotificationDismissal.create(
            user: @user,
            account: @account,
            feature_notification: FactoryBot.create(:ipm_feature_notification)
          )

          # restricted to certain languages
          FactoryBot.create(:ipm_feature_notification, languages: [2])
          @user.stubs(locale_id: 2)

          # restricted to certain account ids
          FactoryBot.create(:ipm_feature_notification, account_ids: [1])
          @user.stubs(account_id: 3)
          @user.locale_id = 2

          get :index
        end

        it "only include applicable feature_notifications for this user" do
          assert_equal 1, JSON.parse(@response.body)['feature_notifications'].length
        end

        it "does not show feature_notifications for this user if subdomain is excluded" do
          assert_not_equal @feature_notification.id, JSON.parse(@response.body)['feature_notifications'][0]['id']
        end
      end
    end

    describe "DELETE" do
      before do
        @feature_notification = FactoryBot.create(:ipm_feature_notification)
      end

      it "responds with 204" do
        delete :destroy, params: { id: @feature_notification.id }
        assert_response :no_content
      end

      it "creates a FeatureNotificationDismissal" do
        delete :destroy, params: { id: @feature_notification.id }
        assert_equal @feature_notification, Ipm::FeatureNotificationDismissal.last.feature_notification
      end

      describe "when assumed" do
        before do
          @controller.stubs(:is_assumed?).returns(true)
          delete :destroy, params: { id: @feature_notification.id }
        end

        it('responds with not_modified') { assert_response :not_modified }
      end

      describe "when not feature notification record does not exist" do
        before do
          @controller.stubs(:feature_notification).returns(nil)
          delete :destroy, params: { id: @feature_notification.id }
        end

        it('responds with not_found') { assert_response :not_found }
      end
    end
  end
end
