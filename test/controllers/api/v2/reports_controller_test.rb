require_relative "../../../support/test_helper"

SingleCov.covered!

describe Api::V2::ReportsController do
  fixtures :accounts, :addresses, :account_property_sets, :subscriptions, :users, :user_identities

  before do
    accept :json
    @account = accounts(:minimum)
    @request.account = @account
    @minimum_admin   = users(:minimum_admin)
  end

  should_be_unauthorized_when_not_logged_in([[:get, :result, id: 111]], "minimum_end_user", 403)

  describe "#result" do
    before do
      login(@minimum_admin)

      params  = { sets: { "1" => { conditions: {}, legend: 'Working tickets', state: 'working' }}}
      @report = Report.create!(
        title: "report title", author: @minimum_admin, relative_interval_in_days: 30,
        sets: params[:sets], account: @account
      )
    end

    it "responds" do
      get :result, params: { id: @report.id }
      assert_response :ok
      assert_includes @response.body, 'legend":"Working tickets"'
    end

    describe "account has more than 20,000 tickets" do
      before do
        Zendesk::Reports::Processor.any_instance.stubs(:runs_in_background?).returns(true)
      end

      it "runs the job in the background" do
        Resque.expects(:enqueue).with(ReportJob, @account.id, @minimum_admin.id, @report.id, anything)
        get :result, params: { id: @report.id }
        assert_response :accepted
      end

      it "limits the run to once every 30 minutes" do
        Resque.expects(:enqueue).with(ReportJob, @account.id, @minimum_admin.id, @report.id, anything).raises(Resque::ThrottledError)
        get :result, params: { id: @report.id }
        assert_response :accepted
      end

      it "tells the user to check back in a few minutes if there isn't a previous run stored of the report" do
        assert @report.result.blank?
        Resque.expects(:enqueue).with(ReportJob, @account.id, @minimum_admin.id, @report.id, anything)
        get :result, params: { id: @report.id }
        assert_response :accepted
      end

      it "displays the last run of the report if available" do
        ReportJob.perform(@account.id, @minimum_admin.id, @report.id)
        assert @report.reload.result
        Resque.expects(:enqueue).with(ReportJob, @account.id, @minimum_admin.id, @report.id, anything)
        get :result, params: { id: @report.id }
        assert_response :accepted
      end
    end
  end
end
