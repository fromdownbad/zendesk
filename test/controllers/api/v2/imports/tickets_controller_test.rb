require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Api::V2::Imports::TicketsController do
  extend Api::V2::TestHelper
  fixtures :tickets, :ticket_fields, :users, :accounts, :sequences, :role_settings

  should_route :post, '/api/v2/imports/tickets', controller: 'api/v2/imports/tickets', action: 'create'
  should_route :post, '/api/v2/imports/tickets/create_many', controller: 'api/v2/imports/tickets', action: 'create_many'

  before do
    @request.account = accounts(:minimum)
    @admin = users(:minimum_admin)

    accept :json
    login(@admin)
  end

  describe "POST'ing ticket data" do
    it "creates a new ticket" do
      assert_difference 'Ticket.count(:all)' do
        post :create, params: { ticket: ticket_attributes }
        assert_response :created
        assert_not_nil JSON.parse(@response.body)['ticket']['id']
      end
    end
  end

  describe "POST'ing ticket and comment data" do
    before do
      comments_data = [comment_attrs(value: "comment 1"), comment_attrs(value: "comment 2")]
      ticket_data = ticket_attributes(comments: comments_data)

      post :create, params: { ticket: ticket_data }
    end

    it "creates the new ticket" do
      assert_response :created
      assert_not_nil JSON.parse(@response.body)['ticket']['id']
    end
  end

  describe "POST'ing with a malformed request" do
    before do
      @controller.send(:importer).stubs(:import).raises(Zendesk::Tickets::Importer::InvalidParams)
      post :create, params: { ticket: ticket_attributes }
    end

    it('responds with bad_request') { assert_response :bad_request }
  end

  describe "POST'ing with a incorrect request" do
    before do
      @controller.send(:importer).stubs(:import).raises(Zendesk::Tickets::Importer::InvalidParams.new(User.new))
      post :create, params: { ticket: ticket_attributes }
    end

    it('responds with unprocessable_entity') { assert_response :unprocessable_entity }

    it "returns error structure" do
      assert_not_empty JSON.parse(@response.body)
    end
  end

  describe "POST'ing and something bad happens" do
    before do
      @controller.send(:importer).stubs(:import).raises(Zendesk::Tickets::Importer::ImportFailed)
      post :create, params: { ticket: ticket_attributes }
    end

    it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
  end

  describe "POST'ing and requester race happens" do
    before do
      @controller.send(:importer).stubs(:import).raises(ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, 'test')
      post :create, params: { ticket: ticket_attributes }
    end

    it('responds with conflict') { assert_response :conflict }

    it "responds with text" do
      assert_equal Mime[:text].to_s, @response.content_type, @response.body
    end
  end

  describe "a POST to :create_many" do
    describe "with valid params" do
      before do
        ticket_data = tickets(:minimum_2).attributes.symbolize_keys.slice(*Api::V2::TicketsController::TICKET_PARAMETER.keys)

        job_id_stub = stub
        job_status_stub = stub
        TicketBulkImportJob.expects(:enqueue).returns(job_id_stub)
        Resque::Plugins::Status::Hash.expects(:get).with(job_id_stub).returns(job_status_stub)

        post :create_many, params: { tickets: [ticket_attributes, ticket_data] }
      end

      should_use_presenter Api::V2::JobStatusPresenter
    end

    describe "with invalid params" do
      before do
        post :create_many
      end

      it('responds with bad_request') { assert_response :bad_request }
    end

    describe "disallows > 100 tickets" do
      before do
        post :create_many, params: { tickets: Array.new(101) { ticket_attributes } }
      end

      it('responds with bad_request') { assert_response :bad_request }
    end

    describe "disallows payloads > 5MB" do
      before do
        post :create_many, params: { tickets: [ticket_attributes(subject: "a" * 5.megabyte)] }
      end

      it('responds with bad_request') { assert_response 413 }
    end
  end

  private

  def ticket_attributes(options = {})
    tickets(:minimum_1).attributes.update(options).symbolize_keys.slice(*Api::V2::TicketsController::TICKET_PARAMETER.keys)
  end

  def comment_attrs(options = {})
    attrs = {
      value: "some comment",
      author_id: users(:minimum_author).id,
      public: true,
      created_at: 2.weeks.ago
    }

    attrs.update(options)
  end
end
