require_relative "../../support/test_helper"
require_relative "../../support/api_test_helper"

SingleCov.covered! uncovered: 40

describe Api::BaseController do
  fixtures :accounts, :translation_locales, :users, :role_settings

  should_include_module Zendesk::CommentOnQueries

  class ApiBaseTestController < Api::BaseController
    caches_action :cached

    def set_api_version_header
      response.headers["X-Zendesk-API-Version"] = "test"
    end

    def cached
      respond_to do |format|
        format.json { render json: [].to_json }
        format.xml  { render xml: [].to_xml }
      end
    end

    def index
      render json: 'cool'
    end

    def i18n
      render plain: I18n.t('txt.email.footer')
    end

    def xml_as_default
      respond_to do |format|
        format.json { render json: [].to_json }
        format.xml  { render xml: [].to_xml }
      end
    end

    def raise_race_condition_error
      raise ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, 'test'
    end

    def failure
      render_failure(status: 410)
    end

    def denied_no_user
      stubs(:current_registered_user).returns(nil)
      deny_access
    end

    def denied
      deny_access
    end

    def skip_session_persistence; end
  end

  class ApiBaseCleanController < Api::BaseController
  end

  tests ApiBaseTestController
  use_test_routes
  include ApiTestHelper

  describe "missing account" do
    before do
      get :index
    end

    it('responds with not_found') { assert_response :not_found }
  end

  describe "with an account" do
    before do
      @current_account = accounts(:minimum)
      @request.account = @current_account
      login(@current_account.owner)
    end

    describe "inactive account" do
      before do
        @current_account.stubs(is_active?: false)
        get :index
      end

      it('responds with not_found') { assert_response :not_found }
    end

    describe "filter_parameter_logging" do
      it "has #filter_parameters" do
        assert_includes Rails.application.config.filter_parameters, :"card[verification_value]"
      end
    end

    describe 'for an account without ssl' do
      before do
        @current_account.stubs(:is_ssl_enabled?).returns(false)
      end

      it 'allows requests on HTTPS' do
        @request.stubs(:ssl?).returns(true)
        get :index, format: 'json'
        assert_response :success
      end
    end

    describe "race condition" do
      before do
        get :raise_race_condition_error
      end

      it "returns conflict response" do
        assert_response :conflict
      end
    end

    describe 'for a host-mapped, SSL-enabled account with no valid certificates' do
      before do
        @controller.stubs(:is_cname_request?).returns(true)
        @current_account.stubs(:is_ssl_enabled?).returns(true)
        @current_account.stubs(:certificates).returns(stub(active: []))
      end

      describe 'a reasonable request' do
        before do
          https!(false)
          get :index, format: 'json'
        end

        it 'redirects to the non-host-mapped URL' do
          assert_response :redirect
          assert_redirected_to(
            controller: 'api_base_test',
            action: 'index',
            format: 'json',
            host: @current_account.host_name(mapped: false),
            protocol: 'https'
          )
        end
      end
    end

    describe "#per_page" do
      it "allows values in range 1..100" do
        [10, 50, 100].each do |per_page|
          @controller.stubs(:params).returns(per_page: per_page.to_s)
          assert_equal per_page, @controller.send(:per_page)
        end
      end

      it "defaults to 100" do
        [0, -5, "hest", 200].each do |per_page|
          @controller.stubs(:params).returns(per_page: per_page.to_s)
          assert_equal 100, @controller.send(:per_page)
        end
      end
    end

    describe "I18n" do
      before do
        users(:minimum_agent).update_attribute(:translation_locale, translation_locales(:brazilian_portuguese))
        login(:minimum_agent)
        get :i18n, format: "json"
      end

      it "uses the real translation locale" do
        assert_equal "THIS IS A BRAZILIAN PORTUGUESE EMAIL FOOTER", @response.body
      end
    end

    describe "CSRF token within the shared session" do
      it "calls #ensure_csrf_token_into_shared_session" do
        @controller.expects(:ensure_csrf_token_into_shared_session)

        get :index
      end
    end

    describe "#render_failure" do
      it "renders a failure message for xml" do
        get :failure
        assert_response 410
        assert_includes @response.body, "<error>"
      end

      it "renders a failure message for json" do
        get :failure, format: "json"
        assert_response 410
        assert_includes @response.body, '"message":'
      end

      it "renders a failure message for other" do
        get :failure, format: "foo"
        assert_response 410
        assert_includes @response.body, "<error>"
      end
    end

    describe '#deny_access' do
      describe "if current_registered_user is not present" do
        let(:basic_auth_challenge) { false }
        let(:user_agent) { "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_3) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31" }

        before do
          @request.env['HTTP_USER_AGENT'] = user_agent
          Arturo.enable_feature!(:basic_auth_challenge) if basic_auth_challenge
          get :denied_no_user
        end

        it "responds with 401 Unauthorized" do
          assert_response :unauthorized
        end

        it "returns Zendesk challenge for browser requests" do
          assert_equal 'Zendesk realm="API"', @response.headers['WWW-Authenticate']
        end

        describe "when client is not a browser" do
          let(:user_agent) { "I AM NOT A BROWSER" }

          it "responds with Basic authentication challenge" do
            assert_equal 'Basic realm="Web Password"', @response.headers['WWW-Authenticate']
          end
        end

        describe "when basic_auth_challenge Arturo is enabled" do
          let(:basic_auth_challenge) { true }

          it "responds with Basic authentication challenge" do
            assert_equal 'Basic realm="Web Password"', @response.headers['WWW-Authenticate']
          end
        end
      end

      describe "if user is logged in" do
        it "responds with 403 Forbidden" do
          get :denied
          assert_response :forbidden
        end
      end
    end

    describe "response format" do
      before do
        @responses = {
          "xml" => "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<nil-classes type=\"array\" count=\"0\"/>\n",
          "json" => "[]"
        }
      end

      it "defaults to xml" do
        get :xml_as_default
        assert_equal @responses["xml"], @response.body
      end

      [["xml", "json"], ["json", "xml"]].each do |format, other|
        it "responds with cached #{format} when #{format} is accepted but #{other} is cached" do
          # write cache with other
          @request.format = nil

          accept other.to_sym
          get :cached
          assert_response :success

          assert_includes @response.content_type.to_s, other
          assert_equal @responses[other], @response.body

          # read cache with format
          @request.format = nil

          accept format.to_sym
          get :cached
          assert_response :success

          assert_includes @response.content_type.to_s, format
          assert_equal @responses[format], @response.body
        end

        it "responds with #{format} when asked for #{format} format" do
          get :xml_as_default, format: format
          assert_equal @responses[format], @response.body
        end

        it "responds with #{format} when #{format} is accepted" do
          accept format.to_sym
          get :xml_as_default
          assert_equal @responses[format], @response.body
        end

        it "responds with #{format} when #{other} is accepted but asked for #{format} format" do
          accept other.to_sym
          get :xml_as_default, format: format
          assert_equal @responses[format], @response.body
        end
      end
    end

    describe '#set_application_version_header' do
      it 'sets the application version' do
        get :index, format: 'json'

        assert_equal GIT_HEAD_TAG, response['X-Zendesk-Application-Version']
      end
    end

    should_not_have_extra_actions(ApiBaseCleanController)

    describe "instruments rate limiting" do
      before do
        login(:minimum_agent)
        @controller.stubs(:params).raises(
          Prop::RateLimited.new(
            handle: :api_time,
            threshold: 10,
            interval: 60,
            cache_key: 'foo',
            description: 'bar',
            strategy: Prop::IntervalStrategy,
            first_throttled: false
          )
        )
      end

      it "records the rate limit" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('rate_limited', tags: ["handle:api_time"])
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.ApiBaseTestController.index', anything).at_least(1)
        get :index
      end
    end

    describe "various lotus/app request helpers" do
      describe "app requests" do
        before do
          @controller.request.stubs(:headers).returns("X-Zendesk-Lotus-Version" => "123", "X-Zendesk-App-Id" => "abc")
        end

        it "detects app requests" do
          assert @controller.send(:lotus_request?)
          assert @controller.send(:app_request?)
          refute @controller.send(:lotus_internal_request?)
        end
      end

      describe "lotus internal requests" do
        before do
          @controller.request.stubs(:headers).returns("X-Zendesk-Lotus-Version" => "123")
          @request.env["X-Zendesk-Lotus-Version"] = "123"
        end

        it "detects lotus internal requests" do
          assert @controller.send(:lotus_request?)
          refute @controller.send(:app_request?)
          assert @controller.send(:lotus_internal_request?)
        end
      end

      describe "api requests" do
        before do
          @controller.request.stubs(:headers).returns({})
        end
        it "detects api requests" do
          refute @controller.send(:lotus_request?)
          refute @controller.send(:app_request?)
          refute @controller.send(:lotus_internal_request?)
        end
      end
    end
  end
end
