require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Crm::Private::UsersController do
  extend Api::V2::TestHelper

  fixtures :all

  before do
    @request.env["HTTPS"] = "on"
    accept :json

    @controller.stubs(:current_account).returns(accounts(:minimum))
  end

  with_options(controller: "api/crm/private/users") do |request|
    request.should_route :post,   "/api/crm/private/users/create_or_update",      action: "create_or_update"
    request.should_route :post,   "/api/crm/private/users/create_or_update_many", action: "create_or_update_many"
  end

  as_an_agent do
    describe "a POST to :create_or_update" do
      before do
        @current_brand = FactoryBot.create(:brand, subdomain: 'lemurs')
        @controller.stubs(:current_brand).returns(@current_brand)
      end

      describe "user creation" do
        it "creates a user if it doesn't match the params" do
          payload = {
            name: 'New person',
            email: 'new.person.email@zendesk.com',
          }

          post :create_or_update, params: { user: payload }
          response = JSON.parse(@response.body)

          assert_response :created

          user = User.find(response['user']['id'])

          assert_equal payload[:name], user.name
          assert_equal payload[:email], user.email
        end

        it "sends welcome email on the current brand if no brand is provided" do
          payload = {
            name: 'New person',
            email: 'new.person.email@zendesk.com',
          }
          ActionMailer::Base.perform_deliveries = true
          ActionMailer::Base.deliveries = []
          Zendesk::UserPortalState.any_instance.stubs(:send_verify_email?).returns(true)
          Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)

          post :create_or_update, params: { user: payload }

          result = JSON.parse(@response.body)
          user_id = result['user']['id']
          user = @controller.send(:current_account).users.find(user_id)

          assert_equal user_id, user.id
          assert_match %r{lemurs.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
          assert_equal ["support@lemurs.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
          assert_includes ActionMailer::Base.deliveries.last.subject, "Welcome to"
        end

        it "creates a user on the provided brand" do
          @brand = FactoryBot.create(:brand, subdomain: 'notlemurs')
          payload = {
            name: 'New person',
            email: 'new.person.email@zendesk.com',
            active_brand_id: @brand.id,
          }
          ActionMailer::Base.perform_deliveries = true
          ActionMailer::Base.deliveries = []
          Zendesk::UserPortalState.any_instance.stubs(:send_verify_email?).returns(true)
          Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)

          post :create_or_update, params: { user: payload }

          result = JSON.parse(@response.body)
          user_id = result['user']['id']
          user = @controller.send(:current_account).users.find(user_id)

          assert_equal user_id, user.id
          assert_match %r{notlemurs.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
          assert_equal ["support@notlemurs.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
          assert_includes ActionMailer::Base.deliveries.last.subject, "Welcome to"
        end
      end

      it "updates an existing user" do
        current_user = @controller.send(:current_account).users.first

        payload = {
          email: current_user.email,
          name: "CRM User"
        }

        post :create_or_update, params: { user: payload }

        assert_equal payload[:name], current_user.reload.name
      end

      it "updates the organization of the user if old organization matches the current in Zendesk" do
        current_organization = organizations(:minimum_organization1)
        new_organization = organizations(:minimum_organization2)
        current_user = @controller.send(:current_account).users.first
        account = accounts(:minimum)

        current_user.organization_memberships.create!(
          account: account,
          organization: current_organization,
          created_at: 1.month.ago
        )

        payload = {
          name: 'Existing user',
          email: current_user.email,
          old_organization_id: current_organization.id,
          organization_id: new_organization.id
        }

        post :create_or_update, params: { user: payload }
        response = JSON.parse(@response.body)

        user = User.find(response['user']['id'])
        assert_equal payload[:organization_id], user.organization_id
      end

      describe "with invalid editing permission" do
        before do
          User.any_instance.stubs(:can?).returns(false)
        end

        it "rejects the request" do
          payload = {
            name: 'New person',
            email: 'new.person.email@zendesk.com',
          }

          post :create_or_update, params: { user: payload }
          assert_response :forbidden
        end
      end

      describe "with multi-org enabled" do
        before do
          Account.any_instance.stubs(:has_multiple_organizations?).returns(true)
          @existing_user = users(:minimum_end_user)
        end

        describe "for an existing user" do
          describe "with an organization they're not a member of" do
            it "creates the membership" do
              @other_organization = organizations(:minimum_organization2)

              payload = {
                email: @existing_user.email,
                organization_id: @other_organization.id
              }

              post :create_or_update, params: { user: payload }

              assert_response :ok

              org_ids = @existing_user.reload.organization_memberships.map(&:organization_id)
              assert_includes org_ids, @other_organization.id
            end
          end
        end
      end
    end
  end
end
