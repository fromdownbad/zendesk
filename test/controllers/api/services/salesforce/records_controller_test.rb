require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Services::Salesforce::RecordsController do
  extend Api::V2::TestHelper
  fixtures :all

  before do
    accept :json
    @test_ticket_id = tickets(:minimum_1)
    @result = Faraday::Response.new
    salesforce_integration = SalesforceIntegration.new
    salesforce_integration.stubs(:http_post).returns(@result)
    @account = accounts(:minimum)
    @account.stubs(:salesforce_integration).returns(salesforce_integration)
    @account.stubs(:salesforce_configuration_enabled?).returns(true)
    @controller.stubs(:current_account).returns(@account)
  end

  with_options(controller: 'api/services/salesforce/records') do |request|
    request.should_route :post, '/api/services/salesforce/records', action: 'create_record'
  end

  as_an_agent do
    describe_with_arturo_disabled :salesforce_record_creation do
      describe "a POST to :records arturo off" do
        it "handles success response without error" do
          post :create_record, params: { ticket_id: @test_ticket_id, object_name: 'leads' }
          assert_equal 403, @response.status
        end
      end
    end

    describe_with_arturo_enabled :salesforce_record_creation do
      describe "a POST to :records" do
        it "handles success response" do
          @result.stubs(:body).returns(Api::Services::Salesforce::RecordsController::SUCCESS_MSG)
          post :create_record, params: { ticket_id: @test_ticket_id, object_name: 'leads' }
          assert_equal 200, @response.status
        end

        it "handles dupe error" do
          @result.stubs(:body).returns(Api::Services::Salesforce::RecordsController::DUPE_RESULT)
          post :create_record, params: { ticket_id: @test_ticket_id, object_name: 'leads' }
          assert_equal 409, @response.status
        end

        it "handles already exists error" do
          @result.stubs(:body).returns(Api::Services::Salesforce::RecordsController::RECORD_ALREADY_EXISTING)
          post :create_record, params: { ticket_id: @test_ticket_id, object_name: 'leads' }
          assert_equal 409, @response.status
        end

        it "handles other unknown errors" do
          @result.stubs(:body).returns('Unknown error')
          post :create_record, params: { ticket_id: @test_ticket_id, object_name: 'leads' }
          assert_equal 422, @response.status
        end

        it "handles nil results" do
          @result = nil
          post :create_record, params: { ticket_id: @test_ticket_id, object_name: 'leads' }
          assert_equal 422, @response.status
        end

        it "handles empty results" do
          @result.stubs(:body).returns('')
          post :create_record, params: { ticket_id: @test_ticket_id, object_name: 'leads' }
          assert_equal 422, @response.status
        end

        it "handles old sfdc package" do
          @result.stubs(:body).returns('[{"errorCode":"NOT_FOUND","message":"Could not find a match for URL"}]')
          @result.stubs(:status).returns(404)
          post :create_record, params: { ticket_id: @test_ticket_id, object_name: 'leads' }
          assert_equal 412, @response.status
          assert_equal Api::Services::Salesforce::RecordsController::SFDC_ENDPOINT_UNAVAILABLE, @response.body
        end
      end
    end
  end
end
