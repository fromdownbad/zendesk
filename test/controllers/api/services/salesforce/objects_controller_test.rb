require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Services::Salesforce::ObjectsController do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    accept :json
  end

  with_options(controller: 'api/services/salesforce/objects') do |request|
    request.should_route :get, '/api/services/salesforce/objects', action: 'index'
  end

  let(:minimum_account) { accounts(:minimum) }
  let(:objects) do
    [
      { name: 'Account', label: 'Account' },
      { name: 'Lead', label: 'Lead' }
    ]
  end

  describe "agent has no salesforce configured" do
    as_an_agent do
      describe "a GET to :index" do
        before { get :index }

        it("responds with unauthorized") { assert_response :unauthorized }
      end
    end
  end

  describe "admin has salesforce configured" do
    as_an_admin do
      before do
        salesforce_support = Zendesk::Salesforce::ControllerSupport.new(account: :minimum_account, params: { objects: :objects })
        salesforce_support.stubs(:objects).returns(objects)

        @controller.stubs(:salesforce_support).returns(salesforce_support)
        @controller.stubs(:require_salesforce_configuration!)
      end

      describe "a GET to :index" do
        before { get :index }

        should_use_presenter Api::Services::Salesforce::ObjectsPresenter, status: :ok
      end
    end
  end

  describe "when a Salesforce::NextGeneration::RefreshTokenError exception is raised" do
    before do
      @controller.stubs(:require_salesforce_configuration!)
      @controller.stubs(:objects).raises(Salesforce::NextGeneration::RefreshTokenError)
    end

    as_an_agent do
      describe "a GET to :show" do
        before { get :index }

        it("responds with 401 Unauthorized") { assert_response :unauthorized }

        it "responds with a RefreshTokenError exception" do
          assert_equal({ errors: 'RefreshTokenError Exception' }.to_json, response.body)
        end
      end
    end
  end

  describe "when a Faraday::ClientError exception is raised" do
    before do
      faraday_client_error = Faraday::ClientError.new('any client error')
      faraday_client_error.stubs(:response).returns(body: 'message', status: 403)

      @controller.stubs(:require_salesforce_configuration!)
      @controller.stubs(:objects).raises(faraday_client_error)
    end

    as_an_agent do
      describe "a GET to :show" do
        before { get :index }

        it "responds with a status and an error message" do
          assert_equal(403, response.status)
          assert_equal({ errors: 'message' }.to_json, response.body)
        end
      end
    end
  end
end
