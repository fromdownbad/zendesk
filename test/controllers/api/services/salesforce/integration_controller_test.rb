require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Services::Salesforce::IntegrationController do
  extend Api::V2::TestHelper
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    accept :json
    @controller.stubs(:current_account).returns(account)
  end

  with_options(controller: 'api/services/salesforce/integration') do |request|
    request.should_route :get, '/api/services/salesforce/integration/status', action: 'status'
  end

  describe "a GET to :status" do
    describe "when the user is unauthorized" do
      before do
        Salesforce::NextGeneration::Integration.any_instance.stubs(:configured_and_enabled?).returns(true)
      end

      it "handles success response" do
        get :status
        assert_equal 401, @response.status
      end
    end
  end

  as_an_agent do
    describe "a GET to :status" do
      describe "when salesforce_configuration_enabled? is true" do
        before do
          Salesforce::NextGeneration::Integration.any_instance.stubs(:configured_and_enabled?).returns(true)
        end

        it "handles success response" do
          get :status
          assert_equal 200, @response.status
        end
      end

      describe "when salesforce_configuration_enabled? is false" do
        before do
          Salesforce::NextGeneration::Integration.any_instance.stubs(:configured_and_enabled?).returns(false)
        end

        it "handles success response" do
          get :status
          assert_equal 200, @response.status
        end
      end
    end
  end
end
