require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Api::Services::Salesforce::FieldsController do
  extend Api::Presentation::TestHelper
  fixtures :accounts, :users

  before do
    accept :json
  end

  with_options(controller: 'api/services/salesforce/fields') do |request|
    request.should_route :get, '/api/services/salesforce/fields', action: 'show'
  end

  let(:minimum_account) { accounts(:minimum) }
  let(:object) { 'Account' }
  let(:relationship_name) { 'Opportunities' }
  let(:type) { 'zendesk' }
  let(:primary) do
    [
      {
        key: "Name",
        title: "Name",
        type: "string",
        picklistValues: [],
        length: 255,
        updateable: true,
        scale: 0
      },
      {
        key: "Salutation",
        title: "Salutation",
        type: "picklist",
        picklistValues: [
          {
            active: true,
            defaultValue: false,
            label: "Mr.",
            validFor: nil,
            value: "Mr."
          }
        ],
        length: 40,
        updateable: true,
        scale: 0
      }
    ]
  end
  let(:related) do
    [
      {
        key: "Contact::LastName",
        title: "Last Name",
        type: "string",
        picklistValues: [],
        length: 255,
        updateable: true,
        scale: 0
      },
      {
        key: "Contact::Salutation",
        title: "Salutation",
        type: "picklist",
        picklistValues: [
          {
            active: true,
            defaultValue: false,
            label: "Mr.",
            validFor: nil,
            value: "Mr."
          }
        ],
        length: 40,
        updateable: true,
        scale: 0
      }
    ]
  end
  let(:related_object) do
    [
      { title: "LastModifiedBy", type: "child", object: "User", relationship: "LastModifiedBy", fields: [] }
    ]
  end

  describe "agent has no salesforce configured" do
    before do
      @salesforce_support = Zendesk::Salesforce::ControllerSupport.new(account: :minimum_account, params: {})
    end
    as_an_agent do
      describe "a GET to :show" do
        before { get :show, params: { type: type } }

        it("responds with unauthorized") { assert_response :unauthorized }
      end
    end
  end

  describe "admin has salesforce configured" do
    as_an_admin do
      before do
        @salesforce_support = Zendesk::Salesforce::ControllerSupport.new(account: :minimum_account, params: {})
        @controller.stubs(:salesforce_support).returns(@salesforce_support)

        @controller.stubs(:require_salesforce_configuration!)
      end

      describe "a GET to :show" do
        describe "without request parameters" do
          before { get :show }

          it("responds with bad_request") { assert_response :bad_request }
        end

        describe "Salesforce object requested" do
          before do
            @salesforce_support.stubs(:fields).returns(primary + related_object)

            @app_format_fields = {
              fields: {
                fields: [
                  {
                    api_name: "Name",
                    label: "Name",
                    data_type: "string",
                    picklist_values: [],
                    length: 255,
                    updateable: true,
                    scale: 0
                  },
                  {
                    api_name: "Salutation",
                    label: "Salutation",
                    data_type: "picklist",
                    picklist_values: [
                      {
                        active: true,
                        defaultValue: false,
                        label: "Mr.",
                        validFor: nil,
                        value: "Mr."
                      }
                    ],
                    length: 40,
                    updateable: true,
                    scale: 0
                  }
                ],
                related_objects: [
                  {
                    object: "User",
                    label: "LastModifiedBy",
                    type: "child",
                    relationship: "LastModifiedBy"
                  }
                ]
              }
            }

            get :show, params: { object: object }
          end

          it "returns objects' Salesforce fields" do
            assert_equal @app_format_fields.to_json, response.body
          end
        end

        describe "Salesforce object and relationship requested" do
          before do
            @salesforce_support.stubs(:relationship_fields).returns(related)

            @app_format_related_object_fields = {
              fields: [
                {
                  api_name: "LastName",
                  label: "Last Name",
                  data_type: "string",
                  picklist_values: [],
                  length: 255,
                  updateable: true,
                  scale: 0
                },
                {
                  api_name: "Salutation",
                  label: "Salutation",
                  data_type: "picklist",
                  picklist_values: [
                    {
                      active: true,
                      defaultValue: false,
                      label: "Mr.",
                      validFor: nil,
                      value: "Mr."
                    }
                  ],
                  length: 40,
                  updateable: true,
                  scale: 0
                }
              ]
            }
            get :show, params: { object: object, relationship_name: relationship_name }
          end

          it "returns related objects' Salesforce fields" do
            assert_equal @app_format_related_object_fields.to_json, response.body
          end
        end

        describe "Zendesk field requested" do
          before { get :show, params: { type: type } }
          it "returns related objects' Salesforce fields" do
            assert_not_nil response.body
          end
        end
      end
    end
  end

  describe "when a Salesforce::NextGeneration::RefreshTokenError exception is raised" do
    before do
      @controller.stubs(:require_salesforce_configuration!)
      @controller.stubs(:fields).raises(Salesforce::NextGeneration::RefreshTokenError)
    end

    as_an_agent do
      describe "a GET to :show" do
        before { get :show, params: { type: type } }

        it("responds with 401 Unauthorized") { assert_response :unauthorized }

        it "responds with a RefreshTokenError exception" do
          assert_equal({ errors: 'RefreshTokenError Exception' }.to_json, response.body)
        end
      end
    end
  end

  describe "when a Faraday::ClientError exception is raised" do
    before do
      faraday_client_error = Faraday::ClientError.new('any client error')
      faraday_client_error.stubs(:response).returns(body: 'message', status: 403)

      @controller.stubs(:require_salesforce_configuration!)
      @controller.stubs(:fields).raises(faraday_client_error)
    end

    as_an_agent do
      describe "a GET to :show" do
        before { get :show, params: { type: type } }

        it "responds with a status and an error message" do
          assert_equal(403, response.status)
          assert_equal({ errors: 'message' }.to_json, response.body)
        end
      end
    end
  end
end
