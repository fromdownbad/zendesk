require_relative "../../../../support/test_helper"
require 'zendesk/salesforce/configuration'

SingleCov.covered!

describe Api::Services::Salesforce::ConfigurationController do
  extend Api::Presentation::TestHelper
  fixtures :accounts

  before do
    accept :json
  end

  with_options(controller: 'api/services/salesforce/configuration') do |request|
    request.should_route :get, '/api/services/salesforce/configuration', action: 'show'
    request.should_route :post, '/api/services/salesforce/configuration/objects', action: 'save_object'
    request.should_route :put, '/api/services/salesforce/configuration/objects/remove', action: 'remove_object'
    request.should_route :put, '/api/services/salesforce/configuration/objects/reorder', action: 'reorder_objects'
    request.should_route :put, '/api/services/salesforce/configuration/objects/filter', action: 'save_object_filter'
  end

  let(:minimum_account) { accounts(:minimum) }
  let(:configuration) { Salesforce::Configuration.new(minimum_account) }
  let(:salesforce_integration) { SalesforceIntegration.new }
  let(:object_name) { 'Account' }

  describe "agent has no salesforce configured" do
    as_an_agent do
      before do
        salesforce_integration.stubs(:configuration).returns(configuration)
        minimum_account.stubs(:salesforce_integration).returns(salesforce_integration)

        @controller.stubs(:current_account).returns(minimum_account)
      end

      describe "a GET to :show" do
        before { get :show }
        it("responds with unauthorized") { assert_response :unauthorized }
      end

      describe "a POST to :save_object" do
        before { post :save_object }
        it("responds with unauthorized") { assert_response :unauthorized }
      end

      describe "a PUT to :remove_object" do
        before { put :remove_object }
        it("responds with unauthorized") { assert_response :unauthorized }
      end

      describe "a PUT to :reorder_objects" do
        before { put :reorder_objects }
        it("responds with unauthorized") { assert_response :unauthorized }
      end

      describe "a PUT to :save_object_filter" do
        before { put :save_object_filter }
        it("responds with unauthorized") { assert_response :unauthorized }
      end
    end
  end

  describe "admin has salesforce configured" do
    as_an_admin do
      before do
        salesforce_integration.stubs(:configuration).returns(configuration)
        minimum_account.stubs(:salesforce_integration).returns(salesforce_integration)

        @controller.stubs(:current_account).returns(minimum_account)
        @controller.stubs(:require_salesforce_configuration!)
        @controller.stubs(:require_extensions_permission!)
      end

      describe "a GET to :show" do
        before { get :show }

        should_use_presenter Api::Services::Salesforce::ConfigurationPresenter, status: :ok
      end

      describe "a POST to :save_object" do
        describe "without complete parameters" do
          before { post :save_object }

          it("responds with bad_request") { assert_response :bad_request }
        end

        describe "with complete parameters" do
          before do
            @object = {
                api_name: 'Account',
                label: 'Account',
                mapping: {
                  sf: {
                    api_name: 'Name',
                    label: 'Name'
                  },
                  zd: {
                    key: 'Organization',
                    label: 'organization'
                  },
                },
                fields: [],
                related_objects: []
            }

            @controller.stubs(:save_app_format_object).returns({})
            post :save_object, params: { object: @object }
          end

          should_use_presenter Api::Services::Salesforce::ConfigurationPresenter, status: :ok
        end
      end

      describe "a PUT to :remove_object" do
        describe "without complete parameters" do
          before { put :remove_object }

          it("responds with bad_request") { assert_response :bad_request }
        end

        describe "with complete parameters" do
          before do
            configuration.stubs(:remove).returns({})
            put :remove_object, params: { object_name: object_name }
          end

          should_use_presenter Api::Services::Salesforce::ConfigurationPresenter, status: :ok
        end
      end

      describe "a PUT to :reorder_objects" do
        describe "without complete parameters" do
          before { put :reorder_objects }

          it("responds with bad_request") { assert_response :bad_request }
        end

        describe "with complete parameters" do
          before do
            @controller.stubs(:reorder_app_format_objects).returns({})
            put :reorder_objects, params: { objects: [{ api_name: 'Account' }] }
          end

          should_use_presenter Api::Services::Salesforce::ConfigurationPresenter, status: :ok
        end
      end

      describe "a PUT to :save_object_filter" do
        describe "without complete params" do
          before { put :save_object_filter }

          it("responds with bad_request") { assert_response :bad_request }
        end

        describe "with complete params" do
          before do
            @controller.stubs(:save_app_format_object_filter).returns({})
            put :save_object_filter, params: { filters: { parent: 'Account' } }
          end

          should_use_presenter Api::Services::Salesforce::ConfigurationPresenter, status: :ok
        end
      end
    end
  end
end
