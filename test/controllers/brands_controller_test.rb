require_relative '../support/test_helper'

SingleCov.covered! uncovered: 9

describe BrandsController do
  fixtures :accounts, :role_settings, :users, :brands, :account_settings

  before do
    @request.account = @account = accounts(:minimum)
    @account.stubs(:has_multibrand?).returns(true)
    use_ssl
  end

  ['minimum_end_user', 'minimum_agent'].each do |user|
    describe user do
      before { login user }

      describe 'index' do
        it 'is forbidden' do
          get :index
          assert_response :forbidden
        end
      end
    end
  end

  describe 'admins' do
    before { login 'minimum_admin' }

    describe 'index' do
      before do
        @default_minimum  = @account.default_brand
        @active_wombat    = FactoryBot.create(:brand, name: 'Wombat',  active: true,  account_id: @account.id)
        @inactive_lemur   = FactoryBot.create(:brand, name: 'Lemur',   active: false, account_id: @account.id)
        @inactive_giraffe = FactoryBot.create(:brand, name: 'Giraffe', active: false, account_id: @account.id)
        @active_cheetar   = FactoryBot.create(:brand, name: 'Cheetar', active: true,  account_id: @account.id)
        @account.settings.multibrand_includes_help_centers = true
        @account.save!

        get :index
      end

      it 'sorts inactive and active brands alphabetically' do
        assert_equal [@inactive_giraffe, @inactive_lemur], @controller.instance_variable_get(:@inactive_brands)
        assert_equal [@active_cheetar, @default_minimum, @active_wombat], @controller.instance_variable_get(:@active_brands)
      end

      it 'allows help-center creation on active brands' do
        [@active_cheetar, @default_minimum, @active_wombat].each do |brand|
          assert_select "button.create[data-brand-url=\"#{brand.url}\"]", true
        end

        [@inactive_giraffe, @inactive_lemur].each do |brand|
          assert_select "button.create[data-brand-url=\"#{brand.url}\"]", false
        end
      end
    end

    describe 'create' do
      describe 'given valid brand parameters' do
        it 'creates a new brand and redirect with success flash' do
          assert_difference 'Brand.count(:all)', 1 do
            post :create, params: { brand: {
              name: 'valid brand',
              subdomain: 'validbrand'
            } }
          end
          assert_redirected_to brands_path
          assert_not_nil flash[:notice]
        end
      end

      describe 'given invalid brand parameters' do
        before do
          post :create, params: { brand: {
            name: 'asdf'
          } }
        end

        it 'redirects with error flash' do
          assert_redirected_to brands_path
          assert_not_nil flash[:error]
        end
      end
    end

    describe 'update' do
      before do
        @wombat = FactoryBot.create(:brand, name: 'Wombat', active: true, account_id: @account.id)
      end

      describe 'given valid brand parameters' do
        it 'updates the brand and redirect with success flash' do
          put :update, params: { id: @wombat.id, brand: {
            name: 'New Wombat'
          } }
          assert_redirected_to brands_path
          assert_not_nil flash[:notice]
        end
      end

      describe 'given invalid brand parameters' do
        it 'redirects with error flash' do
          put :update, params: { id: @wombat.id, brand: {
            name: ''
          } }

          assert_redirected_to brands_path
          assert_not_nil flash[:error]
        end
      end

      describe 'given a valid subdomain change' do
        before { Brand.any_instance.stubs(invalid_subdomain_change?: false) }

        it 'updates the brand and redirect with success flash' do
          put :update, params: { id: @account.route.brand.id, brand: {
            subdomain: 'newsubdomain'
          } }
          assert_redirected_to brands_path
          assert_not_nil flash[:notice]
          assert_equal 'newsubdomain', @account.route.reload.subdomain
        end
      end

      describe 'given an invalid subdomain change' do
        before { Brand.any_instance.stubs(invalid_subdomain_change?: true) }

        it 'redirects with error flash' do
          put :update, params: { id: @account.route.brand.id, brand: {
            subdomain: 'newsubdomain'
          } }
          assert_redirected_to brands_path
          assert_not_nil flash[:error]
        end
      end
    end

    describe 'destroy' do
      before do
        @wombat = FactoryBot.create(:brand, name: 'Wombat', active: true, account_id: @account.id)
      end

      it 'softs delete the brand and redirect with success flash' do
        @controller.expects(:brand_name)
        delete :destroy, params: { id: @wombat.id }
        assert @wombat.reload.deleted?
        assert_redirected_to brands_path
        assert_not_nil flash[:notice]
      end
    end

    describe 'make default' do
      before do
        @wombat = FactoryBot.create(:brand, name: 'Wombat', active: true, account_id: @account.id)
        assert_not_equal @account.default_brand, @wombat
      end

      it 'updates the account setting and redirect with success flash' do
        @controller.expects(:brand_name)
        put :make_default, params: { id: @wombat.id }
        assert_redirected_to brands_path
        assert_equal @wombat, @account.default_brand
        assert_not_nil flash[:notice]
      end

      it 'only show the confirmation modal once' do
        user = @controller.send(:current_user)
        assert(user.settings.show_new_default_brand_modal)
        put :make_default, params: { id: @wombat.id }
        assert_equal false, user.settings.show_new_default_brand_modal
      end
    end

    describe 'toggle' do
      before do
        @wombat = FactoryBot.create(:brand, name: 'Wombat', active: true, account_id: @account.id)
      end

      it 'toggles the state of the brand and redirect with success flash' do
        @controller.expects(:brand_name).at_least_once
        put :toggle, params: { id: @wombat.id }
        assert_redirected_to brands_path
        refute @wombat.reload.active, 'should deactivate the brand'
        assert_not_nil flash[:notice]

        put :toggle, params: { id: @wombat.id }
        assert_redirected_to brands_path
        assert @wombat.reload.active, 'should activate the brand'
        assert_not_nil flash[:notice]
      end
    end

    describe '#switch_require_brand' do
      it 'switches the require_brand_on_new_tickets setting' do
        refute @account.settings.require_brand_on_new_tickets?
        put :switch_require_brand, params: { settings: { require_brand_on_new_tickets: true } }

        assert_response :ok
        assert @account.reload.settings.require_brand_on_new_tickets?
      end
    end
  end
end
