require_relative "../support/test_helper"
require_relative "../support/attachment_test_helper"
require 'prop'

SingleCov.covered!

describe JobsController do
  fixtures :all

  def self.enqueueable_jobs
    ImportExportJobPolicy::JOB_CLASS_MAPPING.except(:report_feed)
  end

  def self.throttled_jobs
    ImportExportJobPolicy::EXPORT_JOBS
  end

  def accessible_jobs
    ImportExportJobPolicy::DEFAULT_JOBS
  end

  with_options(controller: "jobs") do |request|
    request.should_route :post, '/jobs/create', action: 'create'
  end

  before do
    accounts(:support).settings.export_accessible_types = accessible_jobs
    accounts(:support).settings.save
    @request.account = accounts(:minimum)
    @request.env['HTTP_REFERER'] = '/'
    @request.env['Accepts'] = 'text/html'
  end

  describe "when the current_account's subscription does not include the export" do
    let(:account) { accounts(:minimum) }
    before do
      @controller.stubs(:current_account).returns(account)
      login('minimum_admin')
    end

    describe "and the requested job type is: report_feed, user_xml_export or xml_export" do
      %w[report_feed user_xml_export xml_export].each do |job_name|
        it "does not enqueue a #{job_name} job and redirect with a helpful rejection flash message" do
          @controller.expects(:enqueue).never
          post :create, params: { type: job_name.to_s }

          assert_response :redirect
          assert_equal "This feature is not available for your plan.", flash[:error]
        end
      end
    end
  end

  describe "as an admin with a non-existent job name" do
    it "is not found and display a helpful error message" do
      login('minimum_admin')
      post :create, params: { type: "fake_job_name" }

      assert_response 404
      @response.body.must_include "No such type of job"
    end
  end

  let(:support_account) { accounts(:support) }
  let(:support_admin)   { users(:support_admin) }
  let(:view)            { rules(:trigger_notify_requester_of_received_request) }

  def support_account_setup_for(user)
    login(user)

    support_account.subscription.update_attributes(plan_type: SubscriptionPlanType.Large)
    Zendesk::Features::SubscriptionFeatureService.new(support_account).execute!

    @request.account = support_account
    Subscription.any_instance.stubs(
      has_xml_export?: true,
      has_report_feed?: true,
      has_user_xml_export?: true
    )
    Account.any_instance.stubs(:role_settings).returns(role_settings(:minimum))
  end

  enqueueable_jobs.slice(*ImportExportJobPolicy::IMPORT_JOBS).each do |job_name, job_class|
    job_class = job_class.constantize
    describe "#create" do
      describe job_class do
        describe "as an end user" do
          it "is forbidden and display a helpful rejection message" do
            login('minimum_end_user')
            post :create, params: { type: job_name.to_s }

            assert_response 403
            @response.body.must_include "You do not have access to this page. Please contact the account owner of this Zendesk for further help"
          end
        end

        describe "as an agent" do
          it "displays a helpful rejection message" do
            login('minimum_agent')
            post :create, params: { type: job_name.to_s }

            assert_response 403
            @response.body.must_include "You do not have access to this page. Please contact the account owner of this Zendesk for further help"
          end
        end

        describe "as an admin" do
          before do
            support_account_setup_for(support_admin)
            Zendesk::Export::Configuration.any_instance.expects(:whitelisted?).never
          end

          it "enqueues a job to perform the action" do
            expect_enqueue_with(job_class, support_admin, view: view, token: "123456789", update_records: true)
            post_create(job_name, view: view, token: "123456789", update_records: "true")

            redirect_url =
              case job_class.name
              when "OrganizationsJob"
                "/organizations"
              when "UsersJob"
                "/people"
              when *ImportExportJobPolicy::EXPORT_JOB_CLASS_NAMES
                "/reports#export"
              else
                "/home"
              end
            assert_redirected_to redirect_url
            flash[:notice].must_include "You will receive an email with a download link shortly."
          end

          it "enqueues a job to perform the action without updating records" do
            expect_enqueue_with(job_class, support_admin, view: view, token: "123456789", update_records: false)
            post_create(job_name, view: view, update_records: "", token: "123456789")

            redirect_url =
              case job_class.name
              when "OrganizationsJob"
                "/organizations"
              when "UsersJob"
                "/people"
              when *ImportExportJobPolicy::EXPORT_JOB_CLASS_NAMES
                "/reports#export"
              else
                "/home"
              end
            assert_redirected_to redirect_url
            flash[:notice].must_include "You will receive an email with a download link shortly."
          end
        end
      end
    end
  end

  describe ViewCsvJob do
    let(:job_class) { ViewCsvJob }
    let(:job_name)  { "view_csv" }

    describe "as an end user" do
      it "is forbidden and display a helpful rejection message" do
        login('minimum_end_user')
        post :create, params: { type: job_name.to_s }

        assert_response 403
        @response.body.must_include "You do not have access to this page. Please contact the account owner of this Zendesk for further help"
      end
    end

    describe "as an agent without manage Report permission" do
      let(:support_agent) { users(:support_reports_agent1) }

      before do
        support_account_setup_for(support_agent)
        refute support_agent.can?(:manage, ::Report)
      end

      it "enqueues a job to perform the action" do
        expect_enqueue_with(job_class, support_agent, view: view, token: "123456789", update_records: true)
        post_create(job_name, view: view, token: "123456789", update_records: "true")

        assert_redirected_to "https://support.zendesk-test.com/agent"
        flash[:notice].must_include "You will receive email with a download link shortly."
      end

      it "enqueues a job to perform the action without updating records" do
        expect_enqueue_with(job_class, support_agent, view: view, token: "123456789", update_records: false)
        post_create(job_name, view: view, update_records: "", token: "123456789")

        assert_redirected_to "https://support.zendesk-test.com/agent"
        flash[:notice].must_include "You will receive email with a download link shortly."
      end

      it "does not append a link to the latest csv file when displaying a throttle error" do
        create_expirable_attachment("#{Rails.root}/test/files/ticket_export.csv", support_agent.account, created_via: job_class.name)
        expect_enqueue_raises_with(job_class, support_agent, view: view)
        post_create(job_name, view: view)

        assert_redirected_to "https://support.zendesk-test.com/agent"
        assert_equal("Your CSV file request frequency limit has been exceeded. Try again later.", flash[:error])
      end
    end

    describe "should be throttled after current_id reaches threshold during interval" do
      let(:support_agent) { users(:support_reports_agent1) }

      before do
        support_account_setup_for(support_agent)
      end

      it "allow each account export 100,000 tickets per hour." do
        expect_enqueue_with(job_class, support_agent, view: view)
        post_create(job_name, view: view)
        assert_redirected_to "https://support.zendesk-test.com/agent"
      end

      it "throttle while reaches limit; job will not be enqueued" do
        Prop.throttle(:csv_export_throttle, ViewCsvJob.throttle_key(support_agent.account_id, support_agent.id), increment: 100001)
        expect_enqueue_with(job_class, support_agent, view: view).never
        post_create(job_name, view: view)
        assert_redirected_to "https://support.zendesk-test.com/agent"
        assert_match(/Try again later/, flash[:error])
      end
    end
  end

  enqueueable_jobs.slice(*throttled_jobs).each do |job_name, job_class|
    job_class = job_class.constantize
    describe job_class do
      describe "as an end user" do
        it "is forbidden and display a helpful rejection message" do
          login('minimum_end_user')
          post :create, params: { type: job_name.to_s }

          assert_response 403
          @response.body.must_include "You do not have access to this page. Please contact the account owner of this Zendesk for further help"
        end
      end

      describe "and the agent has valid permissions to enqueue" do
        let(:agent)          { users(:support_reports_agent1) }
        let(:permission_set) { build_valid_permission_set }

        before do
          agent.account.stubs(has_permission_sets?: true)
          permission_set.permissions.report_access = "full"
          agent.stubs(permission_set: permission_set)
          support_account_setup_for(agent)
        end

        it "enqueues and redirect" do
          assert agent.can?(:manage, ::Report)
          @controller.expects(:enqueue)
          post :create, params: { type: job_name.to_s }

          assert_response :redirect
        end
      end

      describe "as an admin whose subscription level does not offer this report" do
        it "does not enqueue anything and display a helpful message" do
          Resque.expects(:enqueue).never
          login('minimum_admin')
          post :create, params: { type: job_name.to_s }

          assert_redirected_to "/reports#export"
          flash[:error].must_include "This feature is not available for your plan"
        end
      end

      describe "as an admin" do
        before { support_account_setup_for(support_admin) }

        it "enqueues a job to perform the action" do
          expect_enqueue_with(job_class, support_admin)
          post_create(job_name)

          assert_redirected_to "/reports#export"
          assert_match(/[Y|y]ou will receive an email with a download link shortly/, flash[:notice])
        end

        describe "being throttled" do
          it "only allow one job run every #{job_class.can_run_every} seconds based on the account" do
            expect_enqueue_with(job_class, support_admin)
            post_create(job_name)
            assert_redirected_to "/reports#export"

            expect_enqueue_raises_with(job_class, support_admin)
            post_create(job_name)

            assert_redirected_to "/reports#export"
            assert_match(/Try again later/, flash[:error])
          end

          it "appends a link to the latest xml file if there is one " do
            attachment = create_expirable_attachment("#{Rails.root}/test/files/harvest.xml", support_account, created_via: job_class.name)
            expect_enqueue_raises_with(job_class, support_admin)
            post_create(job_name)

            assert_redirected_to "/reports#export"
            assert_match(
              "<a class=\"title\" href=\"https://support.zendesk-test.com/expirable_attachments/token/#{attachment.token}/?name=harvest.xml\">",
              flash[:error]
            )
          end
        end
      end
    end
  end

  describe XmlExportJob do
    describe "when the account has more tickets than the allowable amount" do
      let(:mock_ticket_association)       { stub(count_with_archived: support_account.settings.export_ticket_max + 1) }
      let(:export_configuration)          { Zendesk::Export::Configuration.new(support_account) }
      let(:export_configuration_notifier) { Zendesk::Export::ConfigurationNotifier.new(support_account) }

      before do
        support_account_setup_for(support_admin)
        support_account.stubs(tickets: mock_ticket_association)
        export_configuration.stubs(maximum_exceeded?: true)
      end

      it "logs the number of tickets" do
        expect_enqueue_with(XmlExportJob, support_admin)
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with(
          "#{XmlExportJob} enqueued with more than #{Zendesk::Export::TicketExportOptions::MAXIMUM_COUNT} tickets. Total number of tickets: #{support_account.tickets.count_with_archived} for #{support_account.id}."
        )
        post :create, params: { type: "xml_export" }
      end
    end
  end

  def post_create(job_name, opts = {})
    if %w[organization_import user_import].include?(job_name)
      post :create, params: { type: job_name.to_s, job: { update_records: opts[:update_records], token: opts[:token] } }
    elsif job_name == "view_csv"
      post :create, params: { type: job_name.to_s, job: { rule: opts[:view].id.to_s } }
    elsif %w[xml_export user_xml_export report_feed].include?(job_name)
      post :create, params: { type: job_name.to_s }
    else
      raise "Unexpected job_name #{job_name}"
    end
  end

  def expect_enqueue_raises_with(job_class, current_user, opts = {})
    expect_enqueue_with(job_class, current_user, opts).raises(Resque::ThrottledError)
  end

  def expect_enqueue_with(job_class, current_user, opts = {})
    if OrganizationsJob == job_class
      Resque.expects(:enqueue).with(job_class, current_user.account_id, current_user.id, !!opts[:update_records], opts[:token].to_s, '/', anything)
    elsif UsersJob == job_class
      Resque.expects(:enqueue).with(job_class, current_user.account_id, current_user.id, !!opts[:update_records], opts[:token].to_s, nil, '/', !!opts[:allow_agent_downgrade], anything, anything)
    elsif ViewCsvJob == job_class
      Resque.expects(:enqueue).with(job_class, current_user.account_id, current_user.id, opts[:view].id.to_s, {}, anything)
    elsif [XmlExportJob, UserXmlExportJob].include?(job_class)
      Resque.expects(:enqueue).with(job_class, current_user.account_id, current_user.id, anything)
    else
      raise "Unhandled job_class #{job_class}"
    end
  end
end
