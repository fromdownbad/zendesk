require_relative "../support/test_helper"
require_relative "../support/attachment_test_helper"

SingleCov.covered!

describe ExpirableAttachmentsController do
  fixtures :accounts, :attachments, :users, :role_settings

  before do
    @account = accounts(:minimum)
    @request.account = @account
    @file_path = "#{Rails.root}/test/files/normal_1.jpg"
    assert File.exist?(@file_path)
    stub_request(:put, %r{\.amazonaws\.com/data/expirable_attachments/.*\.jpg})
    @controller.stubs(:current_brand).returns(@account.brands.first)
  end

  should_route :get, "/expirable_attachments/token/42", controller: "expirable_attachments", action: :token, id: 42

  describe "#token" do
    before do
      login(:minimum_admin)
    end

    it "redirects to agent login if not logged in as an agent" do
      logout
      get :token
      assert_response 401
    end

    it "returns 401 if not logged in as an admin" do
      logout
      login(:minimum_agent)
      attachment = create_expirable_attachment(@file_path, @account)
      ExpirableAttachment.any_instance.stubs(:public_filename).returns(@file_path)
      get :token, params: { id: attachment.token }
      assert_response 401
    end

    it "returns 404 if no id is specified" do
      get :token
      assert_response 404
    end

    it "returns the attachment if found" do
      attachment = create_expirable_attachment(@file_path, @account)

      get :token, params: { id: attachment.token }

      assert_equal "image/jpeg", response.headers["Content-Type"]
      assert_equal '116346', response.headers["Content-Length"]
      assert_equal "inline; filename=\"normal_1.jpg\"", response.headers["Content-Disposition"]
    end

    describe "for an attachment created_via ZendeskMonitor" do
      before { ExpirableAttachment.any_instance.stubs(:created_via).returns("ZendeskMonitor") }
      it "returns 401" do
        logout
        login(:minimum_admin)
        attachment = create_expirable_attachment(@file_path, @account)
        ExpirableAttachment.any_instance.stubs(:public_filename).returns(@file_path)
        get :token, params: { id: attachment.token }
        assert_response 401
      end
    end
  end

  describe "#show" do
    before do
      login(:minimum_agent)
    end

    it "redirects to agent login if not logged in as an agent" do
      logout
      get :show
      assert_response 401
    end

    it "returns 404 if no id is specified" do
      get :show
      assert_response 404
    end

    it "returns 404 if the id is not found" do
      get :show
      assert_response 404, id: 12345563535
    end

    it "returns a 401 if the user isn't authorized to view" do
      attachment = create_expirable_attachment(@file_path, @account)
      User.any_instance.stubs(:can?).returns(true)
      User.any_instance.expects(:can?).with(:view, attachment).returns(false)
      get :show, params: { id: attachment.id }
      assert_response 401
    end

    it "returns the attachment if found" do
      attachment = create_expirable_attachment(@file_path, @account)
      users(:minimum_agent).abilities.expects(:can?).with(:view, attachment).returns(true)

      get :show, params: { id: attachment.id }

      assert_equal "image/jpeg", response.headers["Content-Type"]
      assert_equal '116346', response.headers["Content-Length"]
      assert_equal "inline; filename=\"normal_1.jpg\"", response.headers["Content-Disposition"]
    end

    describe "for an attachment created_via ZendeskMonitor" do
      before { ExpirableAttachment.any_instance.stubs(:created_via).returns("ZendeskMonitor") }
      it "returns 401 when not logged in via monitor" do
        logout
        login(:minimum_admin)
        attachment = create_expirable_attachment(@file_path, @account)
        ExpirableAttachment.any_instance.stubs(:public_filename).returns(@file_path)
        get :show, params: { id: attachment.id }
        assert_response 401
      end
    end

    describe "attachments in the cloud" do
      fixtures :expirable_attachments

      before do
        Timecop.freeze

        @attachment = expirable_attachments(:report_csv)
        @controller.expects(:via_nginx?).returns(true)
        login(:minimum_agent)
        users(:minimum_agent).abilities.expects(:can?).with(:view, @attachment).returns(true)
      end

      it "has the X-S3-Storage-Url set" do
        get :show, params: { id: @attachment.id }
        assert_equal @attachment.header_for_s3, @response.headers['X-S3-Storage-Url']
      end
    end
  end
end
