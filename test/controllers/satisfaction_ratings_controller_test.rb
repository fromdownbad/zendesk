require_relative "../support/test_helper"

SingleCov.covered! uncovered: 3

describe SatisfactionRatingsController do
  fixtures :accounts, :tickets

  def self.describe_with_and_without_csr_token_retention_feature(context_name, &block)
    [:with, :without].each do |w|
      describe("#{w} csr_token_retention feature") do
        before do
          if w == :with
            Account.any_instance.stubs(:has_csr_token_retention?).returns(true)
          else
            Account.any_instance.stubs(:has_csr_token_retention?).returns(false)
          end
        end

        describe(context_name, &block)
      end
    end
  end

  before do
    Ticket.any_instance.stubs(:ever_solved?).returns(true)
    Zendesk::UserPortalState.any_instance.stubs(:show_anonymous_modify_csat_link?).returns(true)
  end

  describe_with_and_without_csr_token_retention_feature "routing" do
    should_route :post, "/requests/12/satisfaction",
      controller: :satisfaction_ratings,
      action: :create,
      ticket_id: '12'

    should_route :get, "/satisfaction",
      controller: :satisfaction_ratings,
      action: :latest

    should_route :get, "/requests/12/satisfaction/new/123456abcdef",
      controller: :satisfaction_ratings,
      action: :new,
      ticket_id: '12',
      token: '123456abcdef'

    should_route :post, "/requests/12/satisfaction/123456abcdef",
      controller: :satisfaction_ratings,
      action: :create_via_token,
      ticket_id: '12',
      token: '123456abcdef'

    should_route :get, "/satisfaction/good_this_week",
      controller: :satisfaction_ratings,
      action: :good_this_week

    should_route :get, "/satisfaction/bad_this_week",
      controller: :satisfaction_ratings,
      action: :bad_this_week
  end

  describe 'while not signed in' do
    describe_with_arturo_setting_enabled :customer_satisfaction do
      before do
        @request.account = @current_account = accounts(:minimum)
        @ticket = tickets(:minimum_2)
        @token  = Tokens::SatisfactionRatingToken.create!(ticket: @ticket)
      end

      describe 'and public customer satisfaction disabled' do
        before do
          @current_account.settings.disable(:public_customer_satisfaction)
          @current_account.save
        end

        describe 'a GET to :latest' do
          before { get :latest, format: "js" }
          it('responds with forbidden') { assert_response :forbidden }
        end
      end

      describe 'and public customer satisfaction enabled' do
        before do
          @current_account.settings.enable(:public_customer_satisfaction)
          @current_account.save
        end

        describe 'a GET to :latest with format js' do
          describe 'when there are less than 100 ratings' do
            before do
              Satisfaction::Rating.expects(:distribution).returns(Array.new(40, 0) + Array.new(40, 1))
              get :latest, format: "js"
            end

            it "sends a JS comment" do
              assert_response :success
              assert_equal "text/javascript; charset=utf-8", @response.headers["Content-Type"].downcase
              assert_equal "// There must be at least 100 ratings in order to present the distribution\n", @response.body
            end
          end

          describe 'when there are at least 100 ratings' do
            before do
              Satisfaction::Rating.expects(:distribution).returns(Array.new(40, 0) + Array.new(60, 1))
            end

            it "sends a JS driven div structure" do
              get :latest, format: "js"
              assert_response :success
              assert_equal "text/javascript; charset=utf-8", @response.headers["Content-Type"].downcase
              assert @response.body.index("good:             60")
              assert @response.body.index("bad:              40")
            end
          end
        end

        describe 'a GET to :latest with format json' do
          before do
            Satisfaction::Rating.expects(:distribution).returns([1, 0, 1, 0, 1])
            get :latest, format: "json"
          end

          it "sends a valid JSON response" do
            assert_response :success
            assert_equal "application/json; charset=utf-8", @response.headers["Content-Type"].downcase
            assert_equal [1, 0, 1, 0, 1], JSON.parse(@response.body)
          end
        end
      end

      describe_with_and_without_csr_token_retention_feature "a GET to :new with invalid token" do
        before { get :new, params: { ticket_id: @ticket.nice_id, token: '12345' } }

        it('responds with unauthorized') { assert_response :unauthorized }
      end

      describe_with_and_without_csr_token_retention_feature 'a GET to :new with an invalid token' do
        before do
          get :new, params: { ticket_id: @ticket.nice_id, token: '12345' }
        end

        it('responds with unauthorized') { assert_response :unauthorized }
        should_render_template nil
      end

      describe_with_and_without_csr_token_retention_feature 'a Get to :new with a valid token but for the wrong request' do
        before { get :new, params: { ticket_id: @ticket.nice_id + 1, token: @token.value } }

        it('responds with not authorized') { assert_response :unauthorized }
      end

      describe_with_and_without_csr_token_retention_feature 'a GET to :new with a valid token' do
        before { get :new, params: { ticket_id: @ticket.nice_id, token: @token.value } }

        # it('responds with success') { assert_response :success }
        # should_render_template :new
        should_not set_flash
      end

      describe "with csr_token retention feature and rerating by clicking on email link with changed intention" do
        before do
          Account.any_instance.stubs(:has_csr_token_retention?).returns(true)
          @ticket.update_column(:satisfaction_score, SatisfactionType.BAD)
          get :new, params: { ticket_id: @ticket.nice_id, token: @token.value, intention: SatisfactionType.GOOD.to_s }
        end

        it "shows the updated intention on landing page" do
          assert_equal SatisfactionType.GOOD.to_s, assigns(:latest_satisfaction_rating)[:updated_score]
        end
      end

      describe_with_and_without_csr_token_retention_feature "a GET to #new with a valid token for a closed ticket" do
        before do
          closed_ticket = tickets(:minimum_5)
          closed_token  = Tokens::SatisfactionRatingToken.create!(ticket: closed_ticket)

          get :new, params: { ticket_id: closed_ticket.nice_id, token: closed_token.value }
        end

        it "renders a request closed message" do
          @response.body.must_include I18n.t('txt.satisfaction.request_closed')
          assert_response :gone
        end
      end

      describe_with_and_without_csr_token_retention_feature 'a mobile GET to :new with a valid token' do
        before do
          set_header('HTTP_USER_AGENT', 'IPhone')
          get :new, params: { ticket_id: @ticket.nice_id, token: @token.value }
        end

        it('responds with success') { assert_response :success }
        should_render_template :new
      end

      describe_with_and_without_csr_token_retention_feature "with help center disabled" do
        it "does not render any login link" do
          Zendesk::UserPortalState.any_instance.stubs(:show_anonymous_modify_csat_link?).returns(false)

          get :new, params: { ticket_id: @ticket.nice_id, token: @token.value }
          refute_includes @response.body, '<a href="/login">'
        end
      end

      describe_with_and_without_csr_token_retention_feature "with help center enabled" do
        before do
          https!
          @current_account.stubs(:help_center_enabled?).returns(true)
        end

        describe "with a valid token" do
          it "renders a satisfaction vote page" do
            get :new, params: { ticket_id: @ticket.nice_id, token: @token.value }
            assert_includes @response.body, '<a href="#/satisfaction/new">'
          end

          describe "unable to rate ticket" do
            before do
              logout
              @ticket.requester = @current_account.users.agents.first
              @ticket.save
            end

            it "returns to access/unauthenticated" do
              @controller.expects(:no_permission_to_rate?).twice.returns(true)
              get :new, params: { ticket_id: @ticket.nice_id, token: @token.value }
              assert_response :unauthorized
            end
          end
        end

        describe "with an invalid token" do
          it "does not render a satisfaction vote page" do
            get :new, params: { ticket_id: @ticket.nice_id, token: '12345' }
            refute_includes @response.body, '<a href="#/satisfaction/new">'
          end
        end
      end

      describe_with_and_without_csr_token_retention_feature "serving the correct layout" do
        it "renders a neutral csat" do
          get :new, params: { ticket_id: @ticket.nice_id, token: @token.value }
          refute_includes @response.body, 'id="header_container"'
        end
      end

      describe 'a POST to :create_via_token with an invalid token' do
        describe 'when there is no rate limit applied' do
          before do
            @controller.expects(:load_token)
            post :create_via_token, params: { ticket_id: @ticket.nice_id, token: '12345' }
          end

          it('responds with unauthorized') { assert_response :unauthorized }
        end

        describe 'when rate limited' do
          before do
            Prop.stubs(:throttled?).with(
              :satisfaction_token_attempts,
              [@current_account.id, @request.remote_ip]
            ).returns(true)
            post :create_via_token, params: { ticket_id: @ticket.nice_id, token: '12345' }
          end

          it('responds with not_found') { assert_response :not_found }
          should_render_template :no_such_token

          before_should "log a message" do
            Rails.logger.expects(:warn).
              with(
                "[SECURITY] Satisfaction token attempt refused for " \
                "#{@request.remote_ip} on account #{@current_account.id}"
              )
          end
        end

        describe "when the rate limit is reached" do
          before do
            Prop.stubs(:throttle).with(
              :satisfaction_token_attempts,
              [@current_account.id, @request.remote_ip]
            ).returns(true)

            post :create_via_token, params: { ticket_id: @ticket.nice_id, token: '12345' }
          end

          it('responds with not_found') { assert_response :not_found }
          should_render_template :no_such_token

          before_should "log a message" do
            Rails.logger.expects(:warn).
              with(
                "[SECURITY] Satisfaction token attempts rate limited for " \
                "#{@request.remote_ip} on account #{@current_account.id}"
              )
          end
        end
      end

      describe 'a POST to :create_via_token with a valid token and valid satisfaction rating parameters' do
        before do
          Account.any_instance.stubs(:csat_reason_code_enabled?).returns(true)

          post(:create_via_token, params: { ticket_id: @ticket.nice_id, token: @token.value, ticket: {
              satisfaction_score: SatisfactionType.BAD,
              satisfaction_reason_code: Satisfaction::Reason::OTHER,
              satisfaction_comment: 'Not fine'
            }, format: 'json' })
        end

        it "rates the ticket" do
          @ticket.reload
          assert_equal SatisfactionType.BADWITHCOMMENT, @ticket.satisfaction_score
          assert_equal Satisfaction::Reason::OTHER, @ticket.satisfaction_reason_code
          assert_equal 'Not fine', @ticket.satisfaction_comment
        end

        it 'consumes the token' do
          assert Tokens::SatisfactionRatingToken.find_by_id(@token.id).blank?
        end

        it('responds with created') { assert_response :created }
      end

      describe "a POST to #create_via_token with a valid token for a closed ticket" do
        before do
          closed_ticket = tickets(:minimum_5)
          closed_token  = Tokens::SatisfactionRatingToken.create!(ticket: closed_ticket)

          post :create_via_token, params: { ticket_id: closed_ticket.nice_id, token: closed_token.value }
        end

        it "renders a request closed message" do
          @response.body.must_include I18n.t('txt.satisfaction.request_closed')
          assert_response :gone
        end
      end

      describe "satisfaction_rating_intentions" do
        let(:valid_params) { {ticket_id: @ticket.nice_id, token: @token.value, intention: SatisfactionType.GOOD.to_s} }

        it "creates a satisfaction_rating_intention with intended param" do
          assert_difference "SatisfactionRatingIntention.count(:all)", +1 do
            get :new, params: valid_params
          end
          assert_equal SatisfactionType.GOOD, SatisfactionRatingIntention.last.score
        end

        it "creates a satisfaction_rating_intention with intended param and being logged in" do
          login(:minimum_end_user)
          assert_difference "SatisfactionRatingIntention.count(:all)", +1 do
            get :new, params: valid_params
          end
          assert_equal SatisfactionType.GOOD, SatisfactionRatingIntention.last.score
        end

        it "overwrites previous satisfaction_rating_intention" do
          assert_difference "SatisfactionRatingIntention.count(:all)", +1 do
            get :new, params: valid_params
            get :new, params: valid_params.merge(intention: SatisfactionType.BAD.to_s)
          end
          assert_equal SatisfactionType.BAD, SatisfactionRatingIntention.last.score
        end

        it "creates no satisfaction_rating_intention without intended param" do
          refute_difference "SatisfactionRatingIntention.count(:all)" do
            get :new, params: valid_params.except(:intention)
          end
        end

        it "returns 422 for invalid intention score" do
          refute_difference "SatisfactionRatingIntention.count(:all)" do
            get :new, params: valid_params.merge(intention: "1")
            assert_response :unprocessable_entity
          end
        end

        it "returns 422 if the intention is valid but fails to save" do
          SatisfactionRatingIntention.any_instance.stubs(:save).returns(false)
          refute_difference "SatisfactionRatingIntention.count(:all)" do
            get :new, params: valid_params
            assert_response :unprocessable_entity
          end
        end

        it "does not return the invalid value" do
          refute_difference "SatisfactionRatingIntention.count(:all)" do
            get :new, params: valid_params.merge(intention: "bad_intentions")
            @response.body.wont_include 'bad_intentions'
          end
        end

        it "creates no satisfaction_rating_intention with known bot header" do
          refute_difference "SatisfactionRatingIntention.count(:all)" do
            request.stubs(user_agent: "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)")
            get :new, params: valid_params
          end
        end

        it "creates no satisfaction_rating_intention for HTTP HEAD requests" do
          refute_difference "SatisfactionRatingIntention.count(:all)" do
            head :new, params: valid_params
          end
        end

        it "does not blow up when no ticket is found" do
          get :new, params: valid_params.merge!(ticket_id: valid_params[:ticket_id] + 1)
        end

        it "creates no satisfaction_rating_intention if user cannot rate the ticket" do
          other_user = users(:minimum_agent)
          @ticket.requester_id = other_user.id
          @ticket.will_be_saved_by(other_user)
          @ticket.save!

          refute_difference "SatisfactionRatingIntention.count(:all)" do
            get :new, params: valid_params
          end
        end

        it "does not rate the ticket" do
          get :new, params: valid_params
          @ticket.reload
          assert_equal SatisfactionType.UNOFFERED, @ticket.satisfaction_score
        end

        it "rates the ticket after SatisfactionRatingIntentionJob is run" do
          Timecop.travel 1.hour.ago do
            get :new, params: valid_params
          end

          assert_difference "SatisfactionRatingIntention.count(:all)", -1 do
            Zendesk::Maintenance::Jobs::SatisfactionRatingIntentionJob.work
          end

          @ticket.reload
          assert_equal SatisfactionType.GOOD, @ticket.satisfaction_score
        end

        it "removes the intentions when creating" do
          assert_difference "SatisfactionRatingIntention.count(:all)", +1 do
            get :new, params: valid_params
          end
        end

        it "removes the intentions when creating via token" do
          assert_difference "SatisfactionRatingIntention.count(:all)", 0 do
            get :new, params: valid_params
            post :create_via_token, params: valid_params.merge(ticket: { satisfaction_score: SatisfactionType.GOOD}, format: 'json')
            assert_response :success
          end
        end

        describe "redirection" do
          describe "when logged in as end user" do
            before do
              login(users(:minimum_end_user))
              get :new, params: valid_params
            end

            should_redirect_to("request path with intention parameter") { @ticket.url_path(user: @current_user, params: { intention: "good" }) }
          end

          describe "when not an end user" do
            before { get :new, params: valid_params }

            should respond_with(:success)
          end
        end
      end
    end
  end

  describe 'while signed in as an end-user' do
    let(:agent) { users(:minimum_agent) }

    before do
      @request.account = @current_account = accounts(:minimum)
      @current_user = users(:minimum_end_user)
      @ticket = tickets(:minimum_2)
      @ticket.assignee = agent
      @ticket.will_be_saved_by(agent)
      @ticket.save!
      login(@current_user)
    end

    [:good_this_week, :bad_this_week].each do |endpoint|
      describe "and trying to access #{endpoint}" do
        before do
          get(endpoint)
        end
        should respond_with(:forbidden)
      end
    end

    describe_with_arturo_setting_disabled :customer_satisfaction do
      describe 'a POST to :create on their own ticket' do
        before { post :create, params: { ticket_id: @ticket.nice_id } }
        it('responds with forbidden') { assert_response :forbidden }
      end
    end

    describe_with_arturo_setting_enabled :customer_satisfaction do
      describe 'a GET to :new with a valid token' do
        before do
          @token = Tokens::SatisfactionRatingToken.create!(ticket: @ticket)
        end

        describe "with csr_token retention feature" do
          let(:ticket) { @ticket }

          before do
            Account.any_instance.stubs(:has_csr_token_retention?).returns(true)
            get :new, params: { ticket_id: @ticket.nice_id, token: @token.value }
          end

          it 'retains the token' do
            assert Tokens::SatisfactionRatingToken.find_by_id(@token.id).present?
          end

          it "doesn't redirect" do
            assert_response 200
          end

          it "has a link to change rating" do
            assert_includes @response.body, "Update"
          end

          describe "private ticket" do
            let(:private_ticket) do
              ticket_initializer = Zendesk::Tickets::Initializer.new(ticket.account, agent, requester_id: @current_user.id, ticket: { submitter: agent, subject: "Private ", comment: { public: false, value: "private ticket about end user" } })
              private_ticket1 = ticket_initializer.ticket
              private_ticket1.will_be_saved_by(agent)
              private_ticket1.save!
              private_ticket1
            end

            before do
              @token = Tokens::SatisfactionRatingToken.create!(ticket: private_ticket)
              Arturo.enable_feature!(:first_comment_private)
              @ticket.account.settings.first_comment_private_enabled = true
              @ticket.account.settings.save!
            end

            it "doesn't redirect" do
              assert_response 200
            end

            it "has a link to change rating" do
              assert_includes @response.body, "Update"
            end
          end
        end

        describe "without csr_token_retention feature" do
          before do
            Account.any_instance.stubs(:has_csr_token_retention?).returns(false)
            get :new, params: { ticket_id: @ticket.nice_id, token: @token.value }
          end
          should_redirect_to('the show-request page') { @ticket.url_path(user: @current_user) }

          it 'consumes the token' do
            refute Tokens::SatisfactionRatingToken.find_by_id(@token.id).present?
          end
        end

        describe "with csat_page_refresh feature" do
          before do
            Account.any_instance.stubs(:has_csr_token_retention?).returns(true)
            Account.any_instance.stubs(:has_csat_page_refresh?).returns(true)
            get :new, params: { ticket_id: @ticket.nice_id, token: @token.value }
          end

          it('responds with success') { assert_response :success }
          should_render_template :new_rating
        end

        describe "without csat_page_refresh feature" do
          before do
            Account.any_instance.stubs(:has_csr_token_retention?).returns(true)
            Account.any_instance.stubs(:has_csat_page_refresh?).returns(false)
            get :new, params: { ticket_id: @ticket.nice_id, token: @token.value }
          end

          it('responds with success') { assert_response :success }
          should_render_template :new
        end

        describe "with csat_hide_description setting" do
          before do
            Account.any_instance.stubs(:has_csr_token_retention?).returns(true)
            Account.any_instance.stubs(:has_csat_description_configure_visibility?).returns(true)
          end

          [true, false].each do |has_csat_page_refresh|
            it "shows description by default when has_csat_page_refresh returns #{has_csat_page_refresh}" do
              Account.any_instance.stubs(:has_csat_page_refresh?).returns(has_csat_page_refresh)
              get :new, params: { ticket_id: @ticket.nice_id, token: @token.value }
              @response.body.must_include @ticket.description
            end

            it "hides the description with the setting disabled" do
              @ticket.account.settings.csat_hide_description = true
              @ticket.account.save
              @ticket.account.reload
              Account.any_instance.stubs(:has_csat_page_refresh?).returns(has_csat_page_refresh)
              get :new, params: { ticket_id: @ticket.nice_id, token: @token.value }
              @response.body.wont_include @ticket.description
              @response.body.wont_include @ticket.subject
            end
          end
        end
      end

      describe_with_and_without_csr_token_retention_feature "when creating" do
        before do
          token         = Tokens::SatisfactionRatingToken.create!(ticket: @ticket)
          @valid_params = { ticket_id: @ticket.nice_id, token: token.value, intention: SatisfactionType.GOOD.to_s }
          attributes    = { ticket: @ticket, score: SatisfactionType.GOOD.to_s, user: @current_user }
          @ticket.account.satisfaction_rating_intentions.create!(attributes)
        end

        it "removes the intentions" do
          assert_difference "SatisfactionRatingIntention.count(:all)", -1 do
            post :create, params: @valid_params.slice(:ticket_id).merge(ticket: { satisfaction_score: SatisfactionType.GOOD }, format: 'json')
            assert_response :success
          end
        end
      end

      describe_with_and_without_csr_token_retention_feature 'a POST to :create with an invalid ticket ID' do
        before do
          @controller.stubs(:current_brand).returns(@request.account.brands.first)
          post :create, params: { ticket_id: 57893202735 }
        end

        it('responds with not_found') { assert_response :not_found }
      end

      describe_with_and_without_csr_token_retention_feature "a POST to :create for another user's ticket" do
        before do
          @other_ticket = tickets(:minimum_1)
          assert_not_equal @other_ticket.requester, @ticket.requester
          post :create, params: { ticket_id: @other_ticket.nice_id }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe_with_and_without_csr_token_retention_feature 'a POST to :create on their own ticket with a score, reason_code and comment' do
        before do
          Account.any_instance.stubs(:has_csat_reason_code_enabled?).returns(true)

          post(:create, params: { ticket_id: @ticket.nice_id, ticket: {
              satisfaction_score: SatisfactionType.BAD,
              satisfaction_reason_code: Satisfaction::Reason::OTHER,
              satisfaction_comment: 'Not fine'
            }, format: 'json' })
        end

        it "rates the ticket as 'BadWithComment'" do
          @ticket.reload
          assert_equal SatisfactionType.BADWITHCOMMENT, @ticket.satisfaction_score
          assert_equal Satisfaction::Reason::OTHER, @ticket.satisfaction_reason_code
          assert_equal 'Not fine', @ticket.satisfaction_comment
        end

        it('responds with created') { assert_response :created }

        it 'has a JSON body' do
          json = JSON.parse(@response.body).with_indifferent_access
          assert_equal SatisfactionType.BADWITHCOMMENT, json[:score].to_i
          assert_equal 'Not fine', json[:comment]
          assert_equal @ticket.assignee.id, json[:assignee_id]
        end
      end
    end
  end

  describe 'while signed in as an agent' do
    describe_with_arturo_setting_enabled :customer_satisfaction do
      before do
        @request.account = @current_account = accounts(:minimum)
        @agent = users(:minimum_agent)
        @end_user = users(:minimum_end_user)

        login(@agent)
      end

      [:good_this_week, :bad_this_week].each do |endpoint|
        describe "and trying to access #{endpoint}" do
          before do
            get(endpoint, params: { agent_id: @agent.id })
          end
          should respond_with(:ok)
        end
      end

      describe "for tickets requested by end-users" do
        before do
          @ticket = tickets(:minimum_6)
          @ticket.requester_id = @end_user.id
          @ticket.will_be_saved_by(@agent)
          @ticket.save!
          @token = Tokens::SatisfactionRatingToken.create!(ticket: @ticket)
        end

        describe 'a GET to #new' do
          before { get :new, params: { ticket_id: @ticket.nice_id, token: @token.value } }
          it "renders an agent cannot rate tickets message" do
            assert_response :forbidden
            @response.body.must_include "Agents cannot rate tickets"
          end
        end

        describe 'a POST to #create' do
          before do
            post(:create, params: { ticket_id: @ticket.nice_id, ticket: {
                satisfaction_score: SatisfactionType.GOOD,
                satisfaction_comment: 'Fine'
              } })
          end
          should respond_with(:forbidden)

          it "gives the agents cannot rate tickets message" do
            @response.body.must_include "Agents cannot rate tickets"
          end
        end
      end

      describe "for tickets requested by agents" do
        before do
          @ticket = tickets(:minimum_6)
          @ticket.requester_id = @agent.id
          @ticket.will_be_saved_by(@agent)
          @ticket.save!
        end

        describe 'a POST to #create' do
          before do
            post(:create, params: { ticket_id: @ticket.nice_id, ticket: {
                satisfaction_score: SatisfactionType.GOOD,
                satisfaction_comment: 'Fine'
              } })
          end
          it 'renders an agent cannot rate tickets message' do
            assert_response :forbidden
            @response.body.must_include "Agents cannot rate tickets"
          end
        end
      end

      describe 'and assuming an end-user' do
        before do
          @ticket = tickets(:minimum_6)
          @ticket.requester_id = @end_user.id
          @ticket.will_be_saved_by(@agent)
          @ticket.save!

          SatisfactionRatingsController.any_instance.expects(:current_user).at_least_once.returns(@end_user)
          SatisfactionRatingsController.any_instance.expects(:is_assuming_user?).at_least_once.returns(true)
          SatisfactionRatingsController.any_instance.expects(:original_user).at_least_once.returns(@agent)

          post(:create, params: { ticket_id: @ticket.nice_id, ticket: {
              satisfaction_score: SatisfactionType.GOOD,
              satisfaction_comment: 'Fine'
            } })
        end

        it 'renders an agent cannot rate tickets message' do
          assert_response :forbidden
          @response.body.must_include "Agents cannot rate tickets"
        end
      end
    end
  end
end
