require_relative '../support/test_helper'
require 'zopim_chat_redirection_mixin'

SingleCov.covered!

describe 'ZopimChatRedirectionMixin' do
  include ZopimChatRedirectionMixin

  # stronger_parameters will throw errors unless allow_parameters is
  # set for public methods of the controller where this is used.
  # This test originally tested these as public methods but the
  # visibility was changed to avoid specifying allow_parameters for
  # methods that don't use parameters.
  let(:mixin) do
    Object.new.extend(ZopimChatRedirectionMixin).tap do |mix|
      ZopimChatRedirectionMixin.private_instance_methods.each do |meth|
        mix.singleton_class.send(:public, meth)
      end
    end
  end
  let(:integration) { mock('zopim_integration') }
  let(:account) { mock('current_account') }

  before do
    integration.stubs(:present?).returns(true)
    account.stubs(:zopim_integration).returns(integration)
    account.stubs(:id).returns(1)
    mixin.stubs(:current_account).returns(account)
  end

  describe '#zopim_integration' do
    it 'fetches a zopim integration' do
      assert_equal mixin.zopim_integration, integration
    end
  end

  describe '#zopim_integration?' do
    describe 'with an integration' do
      it 'returns true' do
        assert mixin.zopim_integration?
      end
    end

    describe 'without an integration' do
      before { integration.stubs(:present?).returns(false) }

      it 'returns false' do
        refute mixin.zopim_integration?
      end
    end
  end

  describe '#app_enabled_without_integration?' do
    describe 'with an integration' do
      it 'returns false' do
        refute mixin.app_enabled_without_integration?
      end
    end

    describe 'without an integration' do
      before { integration.stubs(:present?).returns(false) }

      describe 'with the zopim app enabled' do
        before do
          integration.stubs(:app_enabled?).returns(true)
          ZopimIntegration.stubs(:new).returns(integration)
        end

        it 'returns true' do
          assert mixin.app_enabled_without_integration?
        end
      end

      describe 'without the zopim app enabled' do
        before do
          integration.stubs(:app_enabled?).returns(false)
          ZopimIntegration.stubs(:new).returns(integration)
        end

        it 'returns false' do
          refute mixin.app_enabled_without_integration?
        end
      end
    end
  end

  describe '#app_disabled_without_integration?' do
    describe 'with an integration' do
      it 'returns false' do
        refute mixin.app_disabled_without_integration?
      end
    end

    describe 'without an integration' do
      before { integration.stubs(:present?).returns(false) }

      describe 'with the zopim app disabled' do
        before do
          integration.stubs(:app_installed?).returns(true)
          integration.stubs(:app_enabled?).returns(false)
          ZopimIntegration.stubs(:new).returns(integration)
        end

        it 'returns true' do
          assert mixin.app_disabled_without_integration?
        end
      end

      describe 'with the zopim app enabled' do
        before do
          integration.stubs(:app_installed?).returns(true)
          integration.stubs(:app_enabled?).returns(true)
          ZopimIntegration.stubs(:new).returns(integration)
        end

        it 'returns false' do
          refute mixin.app_disabled_without_integration?
        end
      end

      describe 'without the zopim app installed' do
        before do
          integration.stubs(:app_installed?).returns(false)
          ZopimIntegration.stubs(:new).returns(integration)
        end

        it 'returns false' do
          refute mixin.app_disabled_without_integration?
        end
      end
    end
  end

  describe '#zopim_setup_required?' do
    describe 'with an integration' do
      let(:user) { mock('agent or admin') }

      describe 'with the zopim app enabled' do
        before { integration.stubs(:app_enabled?).returns(true) }

        it 'returns false' do
          refute mixin.zopim_setup_required?(user)
        end
      end

      describe 'with the zopim app not enabled' do
        before { integration.stubs(:app_enabled?).returns(false) }

        it 'returns true' do
          assert mixin.zopim_setup_required?(user)
        end
      end
    end

    describe 'without an integration' do
      before { integration.stubs(:present?).returns(false) }

      describe 'as an agent' do
        let(:agent) { mock('agent', is_admin?: false) }

        it 'returns true' do
          assert mixin.zopim_setup_required?(agent)
        end
      end

      describe 'as an admin' do
        let(:admin) { mock('admin', is_admin?: true) }

        describe 'with the zopim app enabled' do
          before do
            integration.stubs(:app_enabled?).returns(true)
            ZopimIntegration.stubs(:new).returns(integration)
          end

          it 'returns false' do
            refute mixin.zopim_setup_required?(admin)
          end
        end

        describe 'without the zopim app enabled' do
          before do
            integration.stubs(:app_enabled?).returns(false)
            ZopimIntegration.stubs(:new).returns(integration)
          end

          it 'returns true' do
            assert mixin.zopim_setup_required?(admin)
          end
        end
      end
    end
  end

  describe '#zopim_setup_url' do
    describe 'with an integration' do
      describe 'in phase 1' do
        let(:user) { mock('agent or admin') }

        before { integration.stubs(:phase).returns(ZopimIntegration::PHASE_I) }

        it 'returns the appropriate url' do
          assert_equal ZopimChatRedirectionMixin::ZOPIM_PHASE_ONE_APP_NOT_INSTALLED_OR_ENABLED,
            mixin.zopim_setup_url(user)
        end
      end

      describe 'in phase 2' do
        before { integration.stubs(:phase).returns(ZopimIntegration::PHASE_II) }

        describe 'as an agent' do
          let(:agent) { mock('agent', is_admin?: false) }

          it 'returns the appropriate url' do
            assert_equal ZopimChatRedirectionMixin::ZOPIM_PHASE_TWO_APP_NOT_INSTALLED,
              mixin.zopim_setup_url(agent)
          end
        end

        describe 'as an admin' do
          let(:admin) { mock('admin', is_admin?: true) }

          describe 'with the zopim app installed' do
            before { integration.stubs(:app_installed?).returns(true) }

            it 'returns the appropriate url' do
              assert_equal ZopimChatRedirectionMixin::ZOPIM_APP_DISABLED_FOR_ADMIN,
                mixin.zopim_setup_url(admin)
            end
          end

          describe 'without the zopim app installed' do
            before { integration.stubs(:app_installed?).returns(false) }

            it 'returns the appropriate url' do
              assert_equal ZopimChatRedirectionMixin::ZOPIM_PHASE_TWO_APP_NOT_INSTALLED,
                mixin.zopim_setup_url(admin)
            end
          end
        end
      end
    end

    describe 'without an integration' do
      before { integration.stubs(:present?).returns(false) }

      describe 'as an agent' do
        let(:agent) { mock('agent', is_admin?: false) }

        it 'returns the appropriate url' do
          assert_equal ZopimChatRedirectionMixin::NO_ZOPIM_AGENT,
            mixin.zopim_setup_url(agent)
        end
      end

      describe 'as an admin' do
        let(:admin) { mock('admin', is_admin?: true) }

        describe 'with the zopim app disabled' do
          before do
            integration.stubs(:app_installed?).returns(true)
            integration.stubs(:app_enabled?).returns(false)
            ZopimIntegration.stubs(:new).returns(integration)
          end

          it 'returns the appropriate url' do
            assert_equal ZopimChatRedirectionMixin::ZOPIM_APP_DISABLED_FOR_ADMIN,
              mixin.zopim_setup_url(admin)
          end
        end

        describe 'without the zopim app installed' do
          before do
            integration.stubs(:app_installed?).returns(false)
            ZopimIntegration.stubs(:new).returns(integration)
          end

          it 'returns the appropriate url' do
            assert_equal ZopimChatRedirectionMixin::NO_ZOPIM_ADMIN,
              mixin.zopim_setup_url(admin)
          end
        end
      end
    end
  end
end
