require_relative "../support/test_helper"
require_relative "../support/attachment_test_helper"

SingleCov.covered! uncovered: 9

describe AttachmentsController do
  fixtures :accounts, :attachments, :users, :events, :tickets

  def ignore_missing_file
    yield
  rescue ActionController::MissingFile
  end

  before do
    @account = accounts(:minimum)
    @request.account = @account
    @file_path = "#{Rails.root}/test/files/normal_1.jpg"
    assert File.exist?(@file_path)
    login(:minimum_agent)
    stub_request(:put, %r{\.amazonaws\.com/data/attachments/.*})
  end

  it "recognizes legacy routes" do
    assert_recognizes({ controller: 'attachments', action: 'create', format: 'json' }, path: '/attachments/create.json', method: :post)
    assert_recognizes({ controller: 'attachments', action: 'show',   id: '1'        }, path: '/attachments/show/1',      method: :get)
  end

  describe "#index" do
    it "does not be available for end users" do
      login(:minimum_end_user)
      get :index
      assert_response 403
    end

    describe "html and js formats" do
      it "lists image attachments for an account by updating an element on the page" do
        get :index
        assert_response :ok
        @response.body.must_include "Element.update(\"dynamic_images_list\""

        get :index, format: "js"
        assert_response :ok
        @response.body.must_include "Element.update(\"dynamic_images_list\""
      end
    end

    describe "xml format" do
      it "renders in xml format" do
        get :index, format: "xml"
        assert_response :ok
        @response.body.must_include "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
      end
    end

    describe "json format" do
      it "renders json format" do
        get :index, format: "json"
        assert_response :ok
        assert JSON.parse(@response.body)
      end
    end
  end

  describe "#create" do
    describe "html format" do
      it "shoulds create a new image and redirect to index" do
        assert_difference("Attachment.count(:all)") do
          post :create, params: { image: {uploaded_data: fixture_file_upload("hello.html")} }
        end
        assert_equal "Image was successfully created.", flash[:notice]
        assert_redirected_to attachment_url(Attachment.last)
      end

      it "renders the new template if save was not successful" do
        refute_difference("Attachment.count(:all)") do
          post :create
        end
        assert_response :success
        response.body.must_equal "failed to save attachment"
      end
    end

    describe "with a REST upload" do
      describe "using format XML" do
        before do
          @request.env['RAW_POST_DATA'] = fixture_file_upload("small.png").read
          post :create, params: { format: 'xml', filename: 'small.png', content_type: 'image/png' }
          assert_response :ok, response.body
        end

        should_change("the account's attachments") { accounts(:minimum).attachments.size }

        it "creates a properly content-typed file" do
          attach = accounts(:minimum).attachments.last
          assert_equal "image/png", attach.content_type
        end

        it "responses in XML" do
          assert(Nokogiri::XML(@response.body))
        end
      end

      describe "using format JSON" do
        before do
          @request.env['RAW_POST_DATA'] = fixture_file_upload("small.png").read
          post :create, params: { format: 'json', filename: 'small.png', content_type: 'image/png' }
          assert_response :ok, response.body
        end

        should_change("the account's attachments") { accounts(:minimum).attachments.size }

        it "creates a properly content-typed file" do
          attach = accounts(:minimum).attachments.last
          assert_equal "image/png", attach.content_type
        end

        it "responses in JSON" do
          json = JSON.parse(@response.body)
          assert json["created_at"]
          assert json["url"] =~ /small.png$/
        end
      end
    end
  end

  describe "#show" do
    before do
      @controller.stubs(:current_brand).returns(@account.brands.first)
    end

    describe_with_arturo_enabled :sse_remove_attachment_show do
      describe "when attachment is present" do
        let(:attachment) { Attachment.last }

        before { ignore_missing_file { get :show, params: { id: attachment.id } } }

        after { delete_file attachment.public_filename }

        it { assert_response :gone }
      end
    end

    describe_with_arturo_disabled :sse_remove_attachment_show do
      it "returns 404 if the id is not found" do
        get :show, params: { id: 12345563535 }
        assert_response 404
      end

      it "returns 404 if the attachment stores is empty" do
        attachment = Attachment.last
        Attachment.any_instance.stubs(:stores).returns([])
        get :show, params: { id: attachment.id }
        assert_response 404
      end

      it "returns 404 if the attachment stores is invalid" do
        attachment = Attachment.last
        Attachment.any_instance.stubs(:stores).returns([:invalid_store])
        @controller.expects(:via_nginx?).returns(:true)
        get :show, params: { id: attachment.id }
        assert_response 404
      end

      it "returns a 401 if the user isn't authorized to view" do
        attachment = Attachment.last
        User.any_instance.stubs(:can?).returns(true)
        User.any_instance.expects(:can?).with(:view, attachment).returns(false)
        get :show, params: { id: attachment.id }
        assert_response 401
      end

      describe "when attachment is found" do
        let(:attachment) { Attachment.last }

        before { write_file attachment.public_filename, 'x' }

        after { delete_file attachment.public_filename }

        it "returns the attachment" do
          User.any_instance.expects(:can?).with(:view, attachment).returns(true)

          get :show, params: { id: attachment.id }

          assert_equal "image/png", response.headers["Content-Type"]
          assert_equal '1000', response.headers["Content-Length"]
          assert_equal "inline; filename=\"s3.png\"", response.headers["Content-Disposition"]
        end
      end

      describe "with the attachments_on_separate_domain feature" do
        before { Arturo.enable_feature! :attachments_on_separate_domain }
        it "redirects to the attachment domain with a token" do
          attachment = Attachment.last
          get :token, params: { id: attachment.token }
          assert_response :redirect

          url = "https://p1.zdusercontent.com/attachment/90538/20140415012?token="
          assert response.location.start_with?(url), "expected #{response.location} to start with #{url}"
          assert_valid_attachment_domain_url(response.location)
        end
      end

      describe_with_arturo_disabled :play_audio_inline do
        it "has Content-Disposition set to 'inline' if content-type == audio/mp3" do
          attachment = Attachment.new(
            token: "foo_token",
            account: @account,
            uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("test/files/dtmf-1.mp3")
          )
          attachment.content_type = "audio/mp3"
          attachment.save!
          Attachment.any_instance.stubs(:audio?).returns(true)
          User.any_instance.expects(:can?).with(:view, attachment).returns(true)

          get :show, params: { id: attachment.id }

          assert_equal "audio/mp3", response.headers["Content-Type"]
          assert_equal '8613', response.headers["Content-Length"]
          assert_equal "inline; filename=\"dtmf-1.mp3\"", response.headers["Content-Disposition"]
        end

        it "has Content-Disposition set to 'attachment' if content-type == audio/wav" do
          attachment = Attachment.new(
            token: "foo_token",
            account: @account,
            uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("test/files/Message.wav")
          )
          attachment.content_type = "audio/wav"
          attachment.save!
          User.any_instance.expects(:can?).with(:view, attachment).returns(true)

          get :show, params: { id: attachment.id }

          assert_equal "audio/wav", response.headers["Content-Type"]
          assert_equal '9996', response.headers["Content-Length"]
          assert_equal "attachment; filename=\"Message.wav\"", response.headers["Content-Disposition"]
        end
      end

      describe_with_arturo_enabled :play_audio_inline do
        it "has Content-Disposition set to 'inline' if content-type == audio/mp3" do
          attachment = Attachment.new(
            token: "foo_token",
            account: @account,
            uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("test/files/dtmf-1.mp3")
          )
          attachment.content_type = "audio/mp3"
          attachment.save!
          Attachment.any_instance.stubs(:audio?).returns(true)
          User.any_instance.expects(:can?).with(:view, attachment).returns(true)

          get :show, params: { id: attachment.id }

          assert_equal "audio/mp3", response.headers["Content-Type"]
          assert_equal '8613', response.headers["Content-Length"]
          assert_equal "inline; filename=\"dtmf-1.mp3\"", response.headers["Content-Disposition"]
        end

        it "has Content-Disposition set to 'inline' if content-type == audio/wav and content is valid audio" do
          attachment = Attachment.new(
            token: "foo_token",
            account: @account,
            uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("test/files/Message.wav")
          )
          attachment.content_type = "audio/wav"
          attachment.save!
          Attachment.any_instance.stubs(:audio?).returns(true)
          User.any_instance.expects(:can?).with(:view, attachment).returns(true)

          get :show, params: { id: attachment.id }

          assert_equal "audio/wav", response.headers["Content-Type"]
          assert_equal '9996', response.headers["Content-Length"]
          assert_equal "inline; filename=\"Message.wav\"", response.headers["Content-Disposition"]
        end

        it "has Content-Disposition set to 'attachment' if content-type == audio/wav and content is not valid audio" do
          attachment = Attachment.new(
            token: "foo_token",
            account: @account,
            uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("test/files/Message.wav")
          )
          attachment.content_type = "audio/wav"
          attachment.save!
          Attachment.any_instance.stubs(:audio?).returns(false)
          User.any_instance.expects(:can?).with(:view, attachment).returns(true)

          get :show, params: { id: attachment.id }

          assert_equal "audio/wav", response.headers["Content-Type"]
          assert_equal '9996', response.headers["Content-Length"]
          assert_equal "attachment; filename=\"Message.wav\"", response.headers["Content-Disposition"]
        end
      end

      it "has Content-Disposition set to 'attachment' if content-type == html" do
        Attachment.any_instance.expects(:size).with.at_least_once.returns(1000)
        Attachment.any_instance.stubs(:public_filename).returns(@file_path)

        attachment = Attachment.create!(content_type: Mime[:html].to_s, filename: "malicious.html", token: "foo_token", display_filename: "malicious.html", account: @account, uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("test/files/somefile.txt"))
        attachment.stubs(:author).returns(users(:minimum_agent))

        assert_equal "attachment", @controller.send(:determine_disposition, attachment)
      end

      it "strips questionable whitespace from filenames" do
        attachment = Attachment.new(content_type: Mime[:text].to_s, filename: "Too much\ncrazy\rwhite\tspace.txt", token: "hello", display_filename: "Too much\ncrazy\rwhite\tspace.txt", account: @account, uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("test/files/somefile.txt"))
        attachment.filename = "Too much\ncrazy\rwhite\tspace.txt"
        attachment.save!

        ignore_missing_file { get :show, params: { id: attachment.id } }

        assert_equal "text/plain", response.headers["Content-Type"]
        assert_equal '1035', response.headers["Content-Length"]
        assert_equal "attachment; filename=\"Too_much_crazy_white_space.txt\"", response.headers["Content-Disposition"]
      end

      it "has Content-Disposition specify filename* for non-ascii filenames" do
        attachment = Attachment.new(content_type: Mime[:text].to_s, filename: "Test 山田大輔 please!.txt", token: "hello", display_filename: "Test 山田大輔 please!.txt", account: @account, uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("test/files/somefile.txt"))
        attachment.filename = "Test 山田大輔 please!.txt"
        attachment.save!

        ignore_missing_file { get :show, params: { id: attachment.id } }

        assert_equal "text/plain", response.headers["Content-Type"]
        assert_equal '1035', response.headers["Content-Length"]
        assert_equal "attachment; filename=\"Test_山田大輔_please_.txt\"", response.headers["Content-Disposition"]
      end

      it "has Content-Disposition set to 'attachment' for PDF files" do
        attachment = Attachment.create!(
          content_type: Mime[:pdf].to_s,
          token: "hello",
          account: @account,
          uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("test/files/receipt.pdf")
        )

        User.any_instance.stubs(:can?).with(:view, attachment).returns(true)
        User.any_instance.stubs(:is_agent?).returns(false)

        get :show, params: { id: attachment.id }

        assert_equal "attachment; filename=\"receipt.pdf\"", response.headers['Content-Disposition']
        assert_equal '36300', response.headers['Content-Length']
        assert_equal "application/pdf", response.headers['Content-Type']
      end

      describe "external_url attachments" do
        it "redirects to the external url" do
          url = 'http://www.gimbo.net/foo.jpg'
          attachment = Attachment.from_external_url(@account, url, 'foo.jpg', 'image/jpeg', true)
          attachment.save!

          # It's not stored anywhere
          assert_equal [], attachment.stores

          get :show, params: { id: attachment.id }
          assert_response 302
          assert_equal url, response["Location"]
        end
      end
    end
  end

  describe "#token" do
    it "returns 404 if no valid id is specified" do
      get :token, params: { id: 1234 }
      assert_response 404
    end

    describe "attachment from comment of a ticket with archived status" do
      before do
        comment = events(:create_comment_for_minimum_ticket_1)
        Attachment.any_instance.stubs(:public_filename).returns(@file_path)
        @attachment = attachments(:attachment_entry)
        @attachment.source = comment
        @attachment.save!
        Ticket.any_instance.stubs(:status_id).returns(StatusType.ARCHIVED)
        ticket = @attachment.source.ticket
        ticket.archive!
      end

      it "returns the attachment" do
        get :token, params: { id: @attachment.token }
        assert_response 200
      end
    end

    describe "with public attachments" do
      before do
        @account.settings.stubs(:private_attachments?).returns(false)
        @attachment = attachments(:attachment_entry_thumb)
        write_file(@attachment.public_filename, 'x')
      end

      after do
        delete_file(@attachment.public_filename)
      end

      it "supports additional params" do
        get :token, params: { id: @attachment.token, name: 'test' }
      end

      describe "repeated more than 30 times" do
        before { Timecop.freeze }

        it "raises a throttle exception if using incorrect token" do
          get :token, params: { id: @attachment.token }
          assert_response :ok

          get :token, params: { id: 1337 }
          assert_response :not_found

          29.times { Prop.throttle!(:attachment_requests_by_ip, [@request.remote_ip, 57888, @account.id]) }

          get :token, params: { id: @attachment.token }
          assert_response :forbidden
        end
      end

      it "returns the attachment if found" do
        get :token, params: { id: @attachment.token }

        assert_equal "image/png", response.headers["Content-Type"]
        assert_equal '1000', response.headers["Content-Length"]
        assert_equal "inline; filename=\"a_thumb.png\"", response.headers["Content-Disposition"]
      end

      it "allows CORS" do
        get :token, params: { id: @attachment.token }

        assert_equal '*', @response.headers['Access-Control-Allow-Origin']
        assert_nil @response.headers['Access-Control-Allow-Credentials']
        assert_equal 'X-Zendesk-API-Warn', @response.headers['Access-Control-Expose-Headers']
      end

      it "does not render XML if user agent looks like an Android browser" do
        Attachment.any_instance.expects(:image?).returns(true) # this is horrible and I hate myself for having to do this...
        @controller.expects(:send_attachment_content)
        @request.stubs(:user_agent).returns("Mozilla/5.0 (Linux; U; Android 2.2.2; en-us; Nexus One Build/FRG83G) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1")
        get :token, params: { id: @attachment.token, format: 'xml' }
      end

      describe "Filenames with non-alpha-numeric characters" do
        it "returns the attachment with the same name as it was uploaded" do
          attachment = attachments(:foreign_file_attachment)
          User.any_instance.expects(:can?).with(:view, attachment).returns(true)

          ignore_missing_file { get :token, params: { id: attachment.token } }

          assert_equal "text/plain", response.headers["Content-Type"]
          assert_equal '1000', response.headers["Content-Length"]
          assert_equal "attachment; filename=\"☃.txt\"; filename*=utf-8''%E2%98%83.txt", response.headers["Content-Disposition"]
        end
      end
    end

    describe "with private attachments" do
      before do
        @attachment = Attachment.last
        Attachment.any_instance.stubs(:source).returns(events(:create_comment_for_minimum_ticket_2))
        @account.settings.enable(:private_attachments)
        @account.save
      end

      it "returns a 401 if logged in but unauthorized" do
        User.any_instance.stubs(:can?).returns(true)
        User.any_instance.expects(:can?).with(:view, @attachment).returns(false)

        get :token, params: { id: @attachment.token }
        assert_response 401
      end

      describe "as an anonymous user" do
        before do
          @controller.stubs(:current_user).returns(accounts(:minimum).anonymous_user)
        end

        it "redirects to the login page for normal attachments" do
          warden.expects(:authenticate!).with(return_to: nil)
          get :token, params: { id: @attachment.token }
        end

        it "redirects to the login with download_landing for non-inline attachments" do
          @attachment.author.roles = Role::END_USER.id
          @attachment.author.save
          @attachment.update_attributes!(content_type: "foo/bar")

          warden.expects(:authenticate!).with(return_to: "https://#{@account.subdomain}.zendesk-test.com/attachments/#{@attachment.token}/download_landing")
          get :token, params: { id: @attachment.token }
        end
      end
    end

    describe "EU" do
      before do
        @request.account = accounts(:minimum)
        @controller.stubs(via_nginx?: true)
      end

      describe "pod in US, attachment in US and EU" do
        it "serves from US" do
          attachment = attachments(:on_s3_and_s3eu)
          Attachment.any_instance.stubs(:audio?).returns(false)
          get :token, params: { id: attachment.token }
          assert_match(
            %r{us-west-2\.amazonaws\.com},
            @controller.response.headers['X-S3-Storage-Url']
          )
        end
      end

      describe "pod in EU, attachment in US and EU" do
        it "serves from EU" do
          Zendesk::Stores::StoresSynchronizerHelper.any_instance.expects(:preferred_stores).returns([:s3eu])
          attachment = attachments(:on_s3_and_s3eu)
          Attachment.any_instance.stubs(:audio?).returns(false)
          get :token, params: { id: attachment.token }
          assert_match(
            %r{eu-west-1\.amazonaws\.com},
            @controller.response.headers['X-S3-Storage-Url']
          )
        end
      end

      describe "pod in US, attachment in US" do
        it "serves from US" do
          attachment = attachments(:on_s3)
          Attachment.any_instance.stubs(:audio?).returns(false)
          get :token, params: { id: attachment.token }
          assert_match(
            %r{us-west-2\.amazonaws\.com},
            @controller.response.headers['X-S3-Storage-Url']
          )
        end
      end
    end
  end

  describe "#destroy" do
    it "returns a 404 if the attachment is not found" do
      @controller.stubs(:current_brand).returns(@account.brands.first)
      delete :destroy, params: { id: 1234567890 }
      assert_response 404
    end

    it "destroys the attachment" do
      attachment = Attachment.last
      Attachment.any_instance.expects(:destroy)
      delete :destroy, params: { id: attachment.id }
      assert_response 200
    end
  end

  describe "#download_landing" do
    it "returns 404 if no id is specified" do
      @controller.stubs(:current_brand).returns(@account.brands.first)
      get :download_landing, params: { id: 1234 }
      assert_response 404
    end

    describe "when attachment is found" do
      let(:attachment) { attachments(:attachment_entry) }

      before { write_file attachment.public_filename, 'x' }

      after { delete_file attachment.public_filename }

      it "renders the attachment" do
        get :download_landing, params: { id: attachment.token }
        assert_response :success
      end
    end
  end
end
