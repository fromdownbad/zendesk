require_relative "../support/test_helper"

SingleCov.covered! uncovered: 8

describe AccountsController do
  fixtures :translation_locales

  before do
    Account.any_instance.stubs(:verification_link).returns('verification_link')
    @request.account = accounts(:minimum)
    Zendesk::Configuration::PodConfig.stubs(pod_ids: [1])
  end

  let(:statsd_client) { @controller.send(:statsd_client) }

  describe "#new" do
    describe "on GET" do
      it "renders" do
        get :new
        assert_response :success
        assert_template :new
      end

      describe "Rails.env is production" do
        it "redirects to signup" do
          Rails.env.stubs(:production?).returns(true)
          get :new
          assert_response :redirect
          assert_redirected_to "http://www.zendesk.com/signup"
        end
      end
    end
  end

  describe "#reminder" do
    describe "on GET" do
      it "enqueues an AccountsReminderJob" do
        email = "hello@example.com"
        AccountsReminderJob.expects(:enqueue).with(email, I18n.locale, nil)
        get :reminder, params: { email: email }
        assert(JSON.parse(@response.body)['success'])
      end
    end
  end

  describe "#available" do
    it "is available if domain is free" do
      get :available, params: { subdomain: "foobarbazers", callback: "foo" }
      assert_response 200
      assert_equal '/**/foo({"success":true})', @response.body
    end

    it "is unavailable if domain is not available" do
      get :available, params: { subdomain: "a", callback: "foo" }
      assert_response 200
      assert_equal '/**/foo({"success":false})', @response.body
    end
  end
end
