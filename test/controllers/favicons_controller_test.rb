require_relative "../support/test_helper"

SingleCov.covered!

describe FaviconsController do
  fixtures :accounts, :users, :logos

  let(:account) { accounts(:minimum) }

  as_an_anonymous_user do
    describe 'when the account is using help center' do
      before do
        @controller.stubs(should_redirect_to_help_center?: true)
        account.stubs(help_center_disabled?: false)
      end

      describe 'redirect' do
        before { get :show }

        should_redirect_to('help center') { 'https://minimum.zendesk-test.com/hc/favicon' }
      end

      describe_with_arturo_disabled :assets_strip_response_cookies_photos_and_logos do
        it 'does not strip response cookies' do
          get :show

          refute request.env['rack.session.options'][:skip]
        end
      end

      describe_with_arturo_enabled :assets_strip_response_cookies_photos_and_logos do
        it 'strip response cookies' do
          get :show

          assert request.env['rack.session.options'][:skip]
        end
      end

      # Should not be affected by voice/uploads arturo

      describe_with_arturo_disabled :assets_strip_response_cookies_voice_uploads do
        it 'does not strip response cookies' do
          get :show

          refute request.env['rack.session.options'][:skip]
        end
      end

      describe_with_arturo_enabled :assets_strip_response_cookies_voice_uploads do
        it 'does not strip response cookies' do
          get :show

          refute request.env['rack.session.options'][:skip]
        end
      end
    end

    describe 'when using web portal' do
      describe 'and a favicon is uploaded' do
        before do
          @cf_location_header = '/zendesk_test/public/system/logos/45254/minimum_favicon.jpeg'
          @cf_auth_header = 'cf-token'
          @s3_header = %r{zendesk-test.*/public/system/logos/45254/minimum_favicon.jpeg}
        end

        it 'sets s3 location header' do
          get :show

          assert_match @s3_header, response.headers['X-S3-Storage-Url']
        end

        describe_with_arturo_disabled :assets_strip_response_cookies_photos_and_logos do
          it 'does not strip response cookies' do
            get :show

            refute request.env['rack.session.options'][:skip]
          end
        end

        describe_with_arturo_enabled :assets_strip_response_cookies_photos_and_logos do
          it 'strip response cookies' do
            get :show

            assert request.env['rack.session.options'][:skip]
          end
        end

        # Should not be affected by voice/uploads arturo

        describe_with_arturo_disabled :assets_strip_response_cookies_voice_uploads do
          it 'does not strip response cookies' do
            get :show

            refute request.env['rack.session.options'][:skip]
          end
        end

        describe_with_arturo_enabled :assets_strip_response_cookies_voice_uploads do
          it 'does not strip response cookies' do
            get :show

            refute request.env['rack.session.options'][:skip]
          end
        end
      end

      describe "when no favicon is uploaded" do
        before do
          account.stubs(favicon: nil)
        end

        it 'sets the Content-type header' do
          get :show

          response.headers['Content-Type'].must_include "image/x-icon"
        end

        it 'sets the Content-disposition header' do
          get :show

          response.headers['Content-Disposition'].must_include 'attachment; filename="favicon.ico"'
        end

        describe_with_arturo_disabled :assets_strip_response_cookies_photos_and_logos do
          it 'does not strip response cookies' do
            get :show

            refute request.env['rack.session.options'][:skip]
          end
        end

        describe_with_arturo_enabled :assets_strip_response_cookies_photos_and_logos do
          it 'strip response cookies' do
            get :show

            assert request.env['rack.session.options'][:skip]
          end
        end

        # Should not be affected by voice/uploads arturo

        describe_with_arturo_disabled :assets_strip_response_cookies_voice_uploads do
          it 'does not strip response cookies' do
            get :show

            refute request.env['rack.session.options'][:skip]
          end
        end

        describe_with_arturo_enabled :assets_strip_response_cookies_voice_uploads do
          it 'does not strip response cookies' do
            get :show

            refute request.env['rack.session.options'][:skip]
          end
        end
      end
    end
  end
end
