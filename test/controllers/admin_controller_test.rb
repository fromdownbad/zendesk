require_relative '../support/test_helper'

SingleCov.covered!

describe AdminController do
  fixtures :all

  describe 'admin routes' do
    it 'routes to the automations list page' do
      assert_recognizes(
        {controller: 'admin', action: 'rule'},
        '/support/admin/automations'
      )
    end

    it 'routes to the automations edit page' do
      assert_recognizes(
        {controller: 'admin', action: 'rule', id: '123'},
        '/support/admin/automations/123'
      )
    end

    it 'routes to the automations reorder modal' do
      assert_recognizes(
        {controller: 'admin', action: 'rule', id: '123'},
        '/support/admin/modal/automations/123/reorder'
      )
    end

    it 'routes to the automations reorder modal' do
      assert_recognizes(
        {controller: 'admin', action: 'rule'},
        '/support/admin/modal/automations/reorder'
      )
    end

    it 'routes to the macros list page' do
      assert_recognizes({controller: 'admin', action: 'rule'}, '/support/admin/macros')
    end

    it 'routes to the macros edit page' do
      assert_recognizes(
        {controller: 'admin', action: 'rule', id: '123'},
        '/support/admin/macros/123'
      )
    end

    it 'routes to the macros reorder modal' do
      assert_recognizes(
        {controller: 'admin', action: 'rule', id: '123'},
        '/support/admin/modal/macros/123/reorder'
      )
    end

    it 'routes to the macros reorder modal' do
      assert_recognizes(
        {controller: 'admin', action: 'rule'},
        '/support/admin/modal/macros/reorder'
      )
    end

    it 'routes to the macros settings modal' do
      assert_recognizes(
        {controller: 'admin', action: 'rule'},
        '/support/admin/modal/macros/settings'
      )
    end

    it 'routes to the SLAs page' do
      assert_recognizes({controller: 'admin', action: 'rule'}, '/support/admin/slas')
    end

    it 'routes to the triggers list page' do
      assert_recognizes({controller: 'admin', action: 'rule'}, '/support/admin/triggers')
    end

    it 'routes to the triggers edit page' do
      assert_recognizes(
        {controller: 'admin', action: 'rule', id: '123'},
        '/support/admin/triggers/123'
      )
    end

    it 'routes to the trigger revisions page' do
      assert_recognizes(
        {controller: 'admin', action: 'rule', id: '123'},
        '/support/admin/triggers/123/revisions'
      )
    end

    it 'routes to the trigger revisions show page' do
      assert_recognizes(
        {controller: 'admin', action: 'rule', id: '123', revision_id: '1'},
        '/support/admin/triggers/123/revisions/1'
      )
    end

    it 'routes to the triggers reorder modal' do
      assert_recognizes(
        {controller: 'admin', action: 'rule', id: '123'},
        '/support/admin/modal/triggers/123/reorder'
      )
    end

    it 'routes to the triggers reorder modal' do
      assert_recognizes(
        {controller: 'admin', action: 'rule'},
        '/support/admin/modal/triggers/reorder'
      )
    end

    it 'routes to the views list page' do
      assert_recognizes({controller: 'admin', action: 'rule'}, '/support/admin/views')
    end

    it 'routes to the views edit page' do
      assert_recognizes(
        {controller: 'admin', action: 'rule', id: '123'},
        '/support/admin/views/123'
      )
    end

    it 'routes to the views reorder modal' do
      assert_recognizes(
        {controller: 'admin', action: 'rule', id: '123'},
        '/support/admin/modal/views/123/reorder'
      )
    end

    it 'routes to the views reorder modal' do
      assert_recognizes(
        {controller: 'admin', action: 'rule'},
        '/support/admin/modal/views/reorder'
      )
    end

    it 'routes to an attribute value on the routing attributes list page' do
      assert_recognizes(
        {controller: 'admin', action: 'routing', id: '123', value_id: '456'},
        '/support/admin/routing/attributes/123/values/456'
      )
    end

    it 'routes to the routing attributes list page' do
      assert_recognizes(
        {controller: 'admin', action: 'routing'},
        '/support/admin/routing/attributes'
      )
    end

    it 'routes to the edit attribute value agents modal' do
      assert_recognizes(
        {controller: 'admin', action: 'routing', id: '123', value_id: '456'},
        '/support/admin/modal/routing/attributes/123/values/456/agents'
      )
    end

    it 'routes to the delete attribute value confirmation modal' do
      assert_recognizes(
        {controller: 'admin', action: 'routing', id: '123', value_id: '456'},
        '/support/admin/modal/routing/attributes/123/values/456/delete'
      )
    end

    it 'routes to the delete attribute confirmation modal' do
      assert_recognizes(
        {controller: 'admin', action: 'routing', id: '123'},
        '/support/admin/modal/routing/attributes/123/delete'
      )
    end

    it 'routes to the view select confirmation modal' do
      assert_recognizes(
        {controller: 'admin', action: 'routing', id: '123'},
        '/support/admin/modal/routing/views/123/update'
      )
    end

    it 'routes to the new view select confirmation modal' do
      assert_recognizes(
        {controller: 'admin', action: 'routing'},
        '/support/admin/modal/routing/view/update'
      )
    end

    it 'routes to the ticket skills setting modal' do
      assert_recognizes(
        {controller: 'admin', action: 'routing'},
        '/support/admin/modal/routing/settings/ticket_skills'
      )
    end
  end

  describe '#rule' do
    let(:current_user) { users(:minimum_admin) }
    let(:consul_url)   { 'localhost:8500' }
    let(:manifest) do
      {
        version: {sha: 'sha', tag: 'tag', timestamp: '2018-04-20T00:00:00Z'},
        js:      {path: 'rule-admin/sha.js', fingerprint: 'js-fingerprint'},
        css:     {path: 'rule-admin/sha.css', fingerprint: 'css-fingerprint'}
      }
    end

    before do
      login(current_user)

      stub_request(
        :get,
        "#{consul_url}/v1/kv/rule-admin/" \
          "pod#{Zendesk::Configuration.fetch(:pod_id)}/manifest.json?raw=true"
      ).to_return(status: 200, body: manifest.to_json)
    end

    describe 'when `CONSUL_HTTP_ADDR` is set' do
      let(:consul_url) { 'localhost:4200' }

      before do
        ENV['CONSUL_HTTP_ADDR'] = consul_url

        get :rule
      end

      after { ENV.delete('CONSUL_HTTP_ADDR') }

      describe 'and the user is an admin' do
        let(:current_user) { users(:minimum_admin) }

        it 'responds with `200 OK`' do
          assert_response :ok
        end
      end

      describe 'and the user is an agent' do
        let(:current_user) { users(:minimum_agent) }

        it 'responds with `200 OK`' do
          assert_response :ok
        end
      end

      describe 'and the user is an end user' do
        let(:current_user) { users(:minimum_end_user) }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end
    end

    describe 'when `CONSUL_HTTP_ADDR` is not set' do
      before { get :rule }

      describe 'and the user is an admin' do
        let(:current_user) { users(:minimum_admin) }

        it 'responds with `200 OK`' do
          assert_response :ok
        end
      end

      describe 'and the user is an agent' do
        let(:current_user) { users(:minimum_agent) }

        it 'responds with `200 OK`' do
          assert_response :ok
        end
      end

      describe 'and the user is an end user' do
        let(:current_user) { users(:minimum_end_user) }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end
    end

    describe 'when presenting admin assets' do
      before { get :rule }

      it 'sets the script `src`' do
        assert_select(
          selector_for('script', 'src'),
          'https://admin-assets.zendesk.com/rule-admin/sha.js'
        )
      end

      it 'sets the script `integrity`' do
        assert_select 'script[integrity=?]', 'sha384-js-fingerprint'
      end

      it 'sets the script `crossorigin`' do
        assert_select 'script[crossorigin=?]', 'anonymous'
      end

      it 'sets the stylesheet `href`' do
        assert_select(
          selector_for('link', 'href'),
          'https://admin-assets.zendesk.com/rule-admin/sha.css'
        )
      end

      it 'sets the stylesheet `integrity`' do
        assert_select 'link[integrity=?]', 'sha384-css-fingerprint'
      end

      it 'sets the stylesheet `crossorigin`' do
        assert_select 'script[crossorigin=?]', 'anonymous'
      end
    end
  end

  describe '#routing' do
    let(:consul_url) { 'localhost:8500' }
    let(:manifest) do
      {
        version: {sha: 'sha', tag: 'tag', timestamp: '2018-04-20T00:00:00Z'},
        js:      {path: 'routing-admin/sha.js', fingerprint: 'js-fingerprint'},
        css:     {path: 'routing-admin/sha.css', fingerprint: 'css-fingerprint'}
      }
    end

    before do
      login(current_user)

      stub_request(
        :get,
        "#{consul_url}/v1/kv/routing-admin/" \
          "pod#{Zendesk::Configuration.fetch(:pod_id)}/manifest.json?raw=true"
      ).to_return(
        status: 200,
        body: (defined?(manifest_override) ? manifest_override : manifest).to_json
      )
    end

    describe 'when `CONSUL_HTTP_ADDR` is set' do
      let(:consul_url) { 'localhost:8500' }

      before do
        ENV['CONSUL_HTTP_ADDR'] = consul_url
        get :routing
      end

      after { ENV.delete('CONSUL_HTTP_ADDR') }

      describe 'and the user is an admin' do
        let(:current_user) { users(:minimum_admin) }

        it 'responds with `200 OK`' do
          assert_response :ok
        end
      end

      describe 'and the user is an agent' do
        let(:current_user) { users(:minimum_agent) }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe 'and the user is an end user' do
        let(:current_user) { users(:minimum_end_user) }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end
    end

    describe 'when `CONSUL_HTTP_ADDR` is not set' do
      before { get :routing }

      describe 'and the user is an admin' do
        let(:current_user) { users(:minimum_admin) }

        it 'responds with `200 OK`' do
          assert_response :ok
        end
      end

      describe 'and the user is an agent' do
        let(:current_user) { users(:minimum_agent) }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end

      describe 'and the user is an end user' do
        let(:current_user) { users(:minimum_end_user) }

        it 'responds with `403 Forbidden`' do
          assert_response :forbidden
        end
      end
    end

    describe 'when presenting routing admin assets' do
      before { get :routing }

      let(:current_user) { users(:minimum_admin) }

      describe 'when css is present in manifest' do
        it 'sets the script `src`' do
          assert_select(
            selector_for('script', 'src'),
            'https://static.zdassets.com/routing-admin/sha.js'
          )
        end

        it 'sets the script `integrity`' do
          assert_select 'script[integrity=?]', 'sha384-js-fingerprint'
        end

        it 'sets the script `crossorigin`' do
          assert_select 'script[crossorigin=?]', 'anonymous'
        end

        it 'sets the stylesheet `href`' do
          assert_select(
            selector_for('link', 'href'),
            'https://static.zdassets.com/routing-admin/sha.css'
          )
        end

        it 'sets the stylesheet `integrity`' do
          assert_select 'link[integrity=?]', 'sha384-css-fingerprint'
        end

        it 'sets the stylesheet `crossorigin`' do
          assert_select 'link[crossorigin=?]', 'anonymous'
        end
      end

      describe 'when css is absent from manifest' do
        let(:manifest_override) do
          {
            version: {sha: 'sha', tag: 'tag', timestamp: '2018-04-20T00:00:00Z'},
            js:      {path: 'routing-admin/sha.js', fingerprint: 'js-fingerprint'},
            css:     {}
          }
        end

        it 'sets the script `src`' do
          assert_select(
            selector_for('script', 'src'),
            'https://static.zdassets.com/routing-admin/sha.js'
          )
        end

        it 'sets the script `integrity`' do
          assert_select 'script[integrity=?]', 'sha384-js-fingerprint'
        end

        it 'sets the script `crossorigin`' do
          assert_select 'script[crossorigin=?]', 'anonymous'
        end

        it 'does not set the stylesheet `href`' do
          assert_select(
            selector_for('link', 'href'),
            'https://admin-assets.zendesk.com/rule-admin/sha.css',
            false
          )
        end

        it 'does not set the stylesheet `integrity`' do
          assert_select 'link[integrity=?]', 'sha384-css-fingerprint', false
        end

        it 'does not set the stylesheet `crossorigin`' do
          assert_select 'link[crossorigin=?]', 'anonymous', false
        end
      end
    end

    describe 'when the user is an admin' do
      before { get :routing }

      let(:current_user) { users(:minimum_admin) }

      it 'responds with `200 OK`' do
        assert_response :ok
      end
    end

    describe 'when the user is an agent' do
      before { get :routing }

      let(:current_user) { users(:minimum_agent) }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'when the user is an end user' do
      before { get :routing }

      let(:current_user) { users(:minimum_end_user) }

      it 'responds with `403 Forbidden`' do
        assert_response :forbidden
      end
    end

    describe 'when presenting routing assets' do
      before { get :routing }

      let(:current_user) { users(:minimum_admin) }

      it 'sets the script `src`' do
        assert_select(
          selector_for('script', 'src'),
          'https://static.zdassets.com/routing-admin/sha.js'
        )
      end

      it 'sets the stylesheet `href`' do
        assert_select(
          selector_for('link', 'href'),
          'https://static.zdassets.com/routing-admin/sha.css'
        )
      end
    end
  end

  private

  # http://edgeguides.rubyonrails.org/4_2_release_notes.html#assert-select
  def selector_for(tag, attribute)
    "#{tag}:match('#{attribute}', ?)"
  end
end
