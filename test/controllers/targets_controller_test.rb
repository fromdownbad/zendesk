require_relative "../support/test_helper"

SingleCov.covered! uncovered: 55

describe TargetsController do
  fixtures :accounts, :targets, :users, :user_identities, :subscriptions, :addresses, :role_settings, :tickets

  should_include_module Zendesk::LotusEmbeddable

  before do
    @request.account = @account = accounts(:minimum)
  end

  def valid_basecamp_target_params
    {
      title: 'Basecamp Target',
      password: "12345",
      token: "abc",
      url: "https://xxx.com",
      project_id: "55555",
      resource: "todo"
    }
  end

  def valid_get_satisfaction_target_params
    {
      title: 'Get Satisfaction Target',
      email: "someone@example.com",
      password: "123456",
      account_name: "the_account"
    }
  end

  should_route :get,  "/targets/select_target_to_add", controller: :targets, action: :select_target_to_add
  should_route :post, "/targets/test/url_target",      controller: :targets, action: :test_target, id: "url_target"
  should_route :post, "/targets/test/twitter_target",  controller: :targets, action: :test_target, id: "twitter_target"
  should_route :post, "/targets/new/email_target",     controller: :targets, action: :new, id: "email_target"
  should_route :post, "/targets/new/url_target",       controller: :targets, action: :new, id: "url_target"
  should_route :post, "/targets/new/twitter_target",   controller: :targets, action: :new, id: "twitter_target"

  describe "as an agent" do
    before { login('minimum_agent') }

    describe "#index" do
      before { get :index }

      it('responds with forbidden') { assert_response :forbidden }
    end

    describe "#create" do
      before { post :create }

      it('responds with forbidden') { assert_response :forbidden }
    end

    describe "#destroy" do
      before { delete :destroy, params: { id: 1 } }

      it('responds with forbidden') { assert_response :forbidden }
    end

    describe "#test_target" do
      before { get :test_target, params: { id: 1 } }

      it('responds with forbidden') { assert_response :forbidden }
    end
  end

  describe "#index" do
    before do
      login('minimum_admin')
      get :index
    end

    should_redirect_to("the targets list") { settings_extensions_path(anchor: "targets") }
  end

  describe "#new" do
    before { login('minimum_admin') }

    describe "for twitter_target when twitter is not available and returns 502 Bad Gateway" do
      before do
        ::OAuth::Consumer.any_instance.expects(:request).returns(::Net::HTTPBadGateway.new("1.1", "502", "Bad Gateway"))
        get :new, params: { id: 'twitter_target', oauth: "aaaa", oauth_verifier: "bbbb" }
      end

      should_redirect_to("the new twitter target page") { new_target_path(id: "twitter_target") }
      should_not_change("The number of targets") { Target.count(:all) }
    end

    describe "for twitter_target when user clicks 'Deny'" do
      before do
        exception = ::OAuth::Unauthorized.new(stub(code: 401, message: 'Unauthorized'))
        TwitterTarget.any_instance.expects(:authorize).raises(exception)
        get :new, params: { id: 'twitter_target', oauth: "aaaa", oauth_verifier: "bbbb" }
      end

      should_redirect_to("the new twitter target page") { new_target_path(id: "twitter_target") }
      should_not_change("The number of targets") { Target.count(:all) }
    end

    describe "for twitter_target when user requests for target" do
      before do
        TwitterTarget.any_instance.stubs(authorized?: false, authorize_url: "XXX")
        @controller.stubs(:setup_oauth)
        get :new, params: { id: 'twitter_target' }
      end

      it "builds a twitter_target" do
        assert_response :success
      end
    end

    it "builds a salesforce_target" do
      SalesforceTarget.any_instance.stubs(authorized?: false, authorize_url: "XXX")
      @controller.stubs(:setup_oauth)
      get :new, params: { id: 'salesforce_target' }
      assert_response :success
    end
  end

  describe "#create with admin account" do
    before { login('minimum_admin') }

    describe "with a target that requires authorization" do
      before do
        @initializer = stub("initializer")
        @target = stub("target", :save => true, :title => "Some Target", :current_user= => nil, :requires_authorization? => true, :creates_integration? => false, :use_oauth2? => false, :encrypt! => true)
        Zendesk::Targets::Initializer.stubs(:new).returns(@initializer)
      end

      it "sets the access token and secret" do
        @initializer.expects(:build).returns(@target)
        @target.expects(:token= => "token", :secret= => "secret")
        post :create, params: { id: 'url_target', target: {} }, session: { access_token: "token", access_secret: "secret" }
      end
    end

    it "shows an integration creation error message" do
      GetSatisfactionTarget.any_instance.expects(:create_view).raises("XXX")
      ZendeskExceptions::Logger.expects(:record).with do |exception, _p|
        assert_includes(exception.message, "XXX")
      end
      post :create, params: { id: 'get_satisfaction_target', target: valid_get_satisfaction_target_params }

      assert_response :success
      assert_template :edit
      assert flash[:error].include?("There was a problem creating the necessary triggers and views")
    end

    describe "campfire_target" do
      before do
        post :create, params: { id: 'campfire_target', target: { title: 'Campfire Target', subdomain: 'subdomain', token: '123456', room: 'Room', ssl: "0" } }
      end

      should_redirect_to("the targets list") { targets_path }
      should_create :campfire_target

      it "creates the target with correct values" do
        target = Target.last
        assert_equal 'Campfire Target', target.title
        assert_equal 'subdomain', target.subdomain
        assert_equal '123456', target.token
        assert_equal 'Room', target.room
        assert_equal false, target.ssl
      end
    end

    describe "campfire_target failed" do
      before do
        post :create, params: { id: 'campfire_target', target: { title: 'Campfire Target', subdomain: '', token: '123456', room: 'Room', ssl: "0" } }
      end

      should_render_template :edit
      should_not_change("The number of targets") { Target.count(:all) }
    end

    describe "basecamp_target" do
      before do
        post :create, params: { id: 'basecamp_target', target: valid_basecamp_target_params }
      end

      should_redirect_to("the targets list") { targets_path }
      should_create :basecamp_target

      it "creates the target with correct values" do
        target = Target.last
        assert_equal 'Basecamp Target', target.title
        assert_equal '12345', target.password
      end
    end

    describe "url_target" do
      before do
        post :create, params: { id: 'url_target', target: { title: 'Little Bobby Tables <img src="http://imgs.xkcd.com/comics/exploits_of_a_mom.png" onerror=prompt(document.cookie)>', url: 'http://www.google.com/', method: 'get', attribute: 'q' } }
      end

      should_redirect_to("the targets list") { targets_path }
      should_create :url_target

      it "creates the target with correct values" do
        target = Target.last
        assert_equal 'Little Bobby Tables <img src="http://imgs.xkcd.com/comics/exploits_of_a_mom.png" onerror=prompt(document.cookie)>', target.title
        assert_equal 'http://www.google.com/', target.url
        assert_equal 'get', target.method
        assert_equal 'q', target.attribute
        assert @controller.flash[:notice].present?
      end

      it "escapes any raw HTML in the title when rendering the flash" do
        assert @controller.flash[:notice].include?("Little Bobby Tables &lt\;img src=&quot\;http:\/\/imgs.xkcd.com\/comics\/exploits_of_a_mom.png&quot\; onerror=prompt(document.cookie)&gt\;")
      end
    end

    describe "url_target failed" do
      before do
        post :create, params: { id: 'url_target', target: { title: 'Url Target', url: 'www.google.com', method: 'get', attribute: 'q' } }
      end

      should_render_template :edit
      should_not_change("The number of targets") { Target.count(:all) }
    end

    describe "twitter_target" do
      before do
        session[:access_token] = "token"
        session[:access_secret] = "secret"
        post :create, params: { id: 'twitter_target', target: { title: 'Twitter Target' } }
      end

      should_redirect_to("the targets list") { targets_path }
      should_create :twitter_target

      it "create the target with correct values" do
        target = Target.last
        assert_equal 'Twitter Target', target.title
      end
    end

    describe "pivotal target" do
      before do
        post :create, params: { id: 'pivotal_target', target: { title: 'Pivotal Target', token: "foo", project_id: 1234, story_type: "feature" } }
      end

      should_redirect_to("the targets list") { targets_path }
      should_create :pivotal_target

      it "creates the target with correct values" do
        target = Target.last
        assert_equal 'Pivotal Target', target.title
        assert_equal "foo", target.token
      end
    end

    describe "get_satisfaction_target" do
      before do
        post :create, params: { id: 'get_satisfaction_target', target: valid_get_satisfaction_target_params }
      end

      should_redirect_to("the targets list") { targets_path }
      should_create :view
      should_change("the number of triggers", by: 2) { Trigger.count(:all) }

      it "creates the target with correct values" do
        target = Target.last
        assert_equal 'Get Satisfaction Target', target.title
        assert_equal 'someone@example.com', target.email
        assert_equal '123456', target.password
        assert_equal 'the_account', target.account_name
      end
    end
  end

  describe "#create with trial account" do
    describe_with_arturo_enabled :trial_account_target_limit do
      describe "create url_target when account have two targets already" do
        before do
          [1, 2].each do |index|
            target = UrlTargetV2.new(
              title: "Url Target #{index}",
              account: accounts(:trial),
              url: "http://www.example.com/#{index}",
              method: "get"
            )
            target.save!
          end

          login(:trial_admin)

          @request.account = @account = accounts(:trial)

          post :create, params: { id: 'url_target', target: { title: 'Hello 123', url: 'http://www.google.com/', method: 'get', attribute: 'q' } }
        end

        should_redirect_to("the targets list") { targets_path }

        it "escapes any raw HTML in the title when rendering the flash of the warning message" do
          assert @controller.flash[:beware].include?("<p>Trial accounts are limited to creating a maximum of 2 targets. This limit will be removed on upgrading to a full account.</p>\n<p>If you believe this is an error, <a target=\"_blank\" href=\"https://support.zendesk.com/hc/en-us/articles/360026614173\">contact Zendesk Customer Support</a>.</p>")
        end

        it "should target count remain same" do
          assert_equal 2, accounts(:trial).targets.count
        end
      end
    end

    describe_with_arturo_disabled :trial_account_target_limit do
      describe "create url_target when account have two targets already" do
        before do
          [1, 2].each do |index|
            target = UrlTargetV2.new(
              title: "Url Target #{index}",
              account: accounts(:trial),
              url: "http://www.example.com/#{index}",
              method: "get"
            )
            target.save!
          end

          login(:trial_admin)

          @request.account = @account = accounts(:trial)

          post :create, params: { id: 'url_target', target: { title: 'Hello 456', url: 'http://www.google.com/', method: 'get', attribute: 'q' } }
        end

        should_redirect_to("the targets list") { targets_path }

        it "creates the target with correct values" do
          target = Target.last
          assert_equal 'Hello 456', target.title
          assert_equal 'http://www.google.com/', target.url
          assert_equal 'get', target.method
          assert_equal 'q', target.attribute
          assert @controller.flash[:notice].present?
        end

        it "escapes any raw HTML in the title when rendering the flash" do
          assert @controller.flash[:notice].include?("Hello 456")
        end

        it "should target count be increased by one" do
          assert_equal 3, accounts(:trial).targets.count
        end
      end
    end
  end

  describe "#update" do
    before { login('minimum_admin') }

    describe "basecamp_target" do
      let(:params) { { password: 'ppp', token: 'bbb'} }

      before do
        post :create, params: { id: 'basecamp_target', target: valid_basecamp_target_params }
        @basecamp_target = Target.last
      end

      it "overwrites the password" do
        put :update, params: { id: @basecamp_target.id, target: params }

        assert_response :redirect
        @basecamp_target.reload
        assert_equal 'bbb', @basecamp_target.token
        assert_equal 'ppp', @basecamp_target.password
      end

      it "does not overwrite the password with a blank password" do
        put :update, params: { id: @basecamp_target.id, target: params.merge(password: '') }

        assert_response :redirect
        @basecamp_target.reload
        assert_equal 'bbb', @basecamp_target.token
        assert_equal '12345', @basecamp_target.password
      end

      it 'uses the TargetInitializer to update the target' do
        Zendesk::Targets::Initializer.any_instance.expects(:set).with(
          instance_of(BasecampTarget),
          params.transform_keys(&:to_s)
        ).once.returns(@basecamp_target)
        @basecamp_target.expects(:save).once

        put :update, params: { id: @basecamp_target.id, target: params }
      end
    end
  end

  describe "#deactivate" do
    before do
      login('minimum_admin')

      @target = targets(:twitter_valid)
      @target.update_attribute(:is_active, true)

      post :deactivate, params: { id: @target.id }

      @target.reload
    end

    should_redirect_to("the targets list") { settings_extensions_path(anchor: "targets") }

    it "deactivates the target" do
      refute @target.is_active?
    end

    describe "a target that is now invalid" do
      before do
        @target.update_attribute(:is_active, false)
        Target.any_instance.expects(:valid?).returns(false)
        post :activate, params: { id: @target.id }
      end

      should_redirect_to("the edit target page") { edit_target_path(@target) }

      it "has an error flash" do
        assert @controller.flash[:error].present?
      end
    end
  end

  describe "#activate" do
    before do
      login('minimum_admin')

      @target = targets(:twitter_valid)
      @target.update_attribute(:is_active, false)
      @target.failures = 3

      post :activate, params: { id: @target.id }

      @target.reload
    end

    should_redirect_to("the targets list") { settings_extensions_path(anchor: "targets") }

    it "activates the target" do
      assert @target.is_active?
    end

    it "resets the number of failures" do
      assert_equal 0, @target.failures
    end

    describe "a target that is now invalid" do
      before do
        @target.update_attribute(:is_active, false)
        Target.any_instance.expects(:valid?).returns(false)
        post :activate, params: { id: @target.id }
      end

      should_redirect_to("the edit target page") { edit_target_path(@target) }

      it "has an error flash" do
        assert @controller.flash[:error].present?
      end
    end
  end

  describe "#destroy" do
    describe "destroy a normal target" do
      before do
        login('minimum_admin')
        @target = targets(:twitter_valid)
        delete :destroy, params: { id: @target.id }
      end

      should_destroy :twitter_target
      should_redirect_to("the targets list") { targets_path }
    end

    describe "destroy a target with raw HTML in the title" do
      describe "succeeds" do
        before do
          login('minimum_admin')
          @target = targets(:url_valid)
          @target.update_attribute(:title, 'Little Bobby Tables <img src="http://imgs.xkcd.com/comics/exploits_of_a_mom.png" onerror=prompt(document.cookie)>')
          delete :destroy, params: { id: @target.id }
        end

        should_destroy :url_target

        it "escapes the raw HTML in the flash" do
          assert @controller.flash[:notice].include?("Little Bobby Tables &lt\;img src=&quot\;http:\/\/imgs.xkcd.com\/comics\/exploits_of_a_mom.png&quot\; onerror=prompt(document.cookie)&gt\;")
        end
      end

      describe "fails" do
        before do
          login('minimum_admin')
          @target = targets(:url_valid)
          @target.update_attribute(:title, 'Little Bobby Tables <img src="http://imgs.xkcd.com/comics/exploits_of_a_mom.png" onerror=prompt(document.cookie)>')
          Target.any_instance.expects(:destroy).returns(false)
          delete :destroy, params: { id: @target.id }
        end

        should_render_template :edit
        should_not_change("The number of targets") { Target.count(:all) }

        it "escapes the raw HTML in the flash" do
          assert @controller.flash[:error].include?("Little Bobby Tables &lt\;img src=&quot\;http:\/\/imgs.xkcd.com\/comics\/exploits_of_a_mom.png&quot\; onerror=prompt(document.cookie)&gt\;")
        end
      end
    end
  end

  describe "#select_target_to_add" do
    before do
      login('minimum_admin')
      get :select_target_to_add
    end

    should_render_template :select_target_to_add
  end

  describe "#test_target" do
    before do
      login('minimum_admin')
    end

    describe "twitter target" do
      before do
        session[:access_token] = "token"
        session[:access_secret] = "secret"
        Target.any_instance.stubs(send_test_message: nil)
        post :test_target, params: { format: 'js', id: 'twitter_target', target: { title: 'Twitter Target' } }
      end

      it "tests a target" do
        assert @response.body.include?("The message was successfully sent")
      end
    end

    describe "url target" do
      before do
        Target.any_instance.stubs(send_test_message: nil)
        post :test_target, params: { format: 'js', id: 'url_target', target: { title: 'Url Target', url: 'http://www.google.com/', method: 'post', attribute: 'q' } }
      end

      it "tests a target" do
        assert @response.body.include?("The message was successfully sent")
      end
    end

    describe "when testing with basic auth" do
      let(:params) { {format: 'js', id: 'url_target', target: { 'title' => 'Url Target', 'url' => 'http://www.google.com/', 'method' => 'post', 'attribute' => 'q' }} }

      before do
        Target.any_instance.stubs(send_test_message: nil)
      end

      describe "when the password is unchanged" do
        it "uses the saved password" do
          params[:target]['password'] = ""
          UrlTarget.any_instance.expects(:attributes=).with(params[:target].except('password'))
          post :test_target, params: params
        end
      end

      describe "when the password is changed" do
        it "uses the new password" do
          params[:target]['password'] = "asdf"
          UrlTarget.any_instance.expects(:attributes=).with(params[:target])
          post :test_target, params: params
        end
      end
    end

    describe "when testing is not supported for the target" do
      before do
        exception = Zendesk::Targets::Tester::TestingNotSupported.new("smurfs are too small")
        Zendesk::Targets::Tester.any_instance.stubs(:test).raises(exception)

        post :test_target, params: { format: 'js', id: 'url_target', target: {} }
      end

      it "renders the correct alert" do
        assert @response.body.include?("Test is not supported for this target")
      end

      it "includes the reason why testing isn't supported" do
        assert @response.body.include?("smurfs are too small")
      end
    end

    describe "when the target is invalid" do
      before do
        exception = Zendesk::Targets::Tester::TargetInvalid.new
        exception.stubs(:error_messages).returns(["Smurfs"])

        Zendesk::Targets::Tester.any_instance.stubs(:test).raises(exception)

        post :test_target, params: { format: 'js', id: 'url_target', target: {} }
      end

      it "renders the correct alert" do
        assert_includes @response.body, "Invalid target configuration"
      end

      it "includes the errors on the target" do
        assert_includes @response.body, "Smurfs"
      end
    end

    describe "when used more than 20 times a minute with a valid target" do
      it "responds with forbidden" do
        Timecop.freeze do
          # stub hitting the endpoint 20 times
          20.times { Prop.throttle(:test_target, @controller.send(:current_user).id) }

          post :test_target, params: { format: 'js', id: 'url_target', target: { title: 'Url Target', url: 'http://www.google.com/', method: 'post', attribute: 'q' } }

          assert_response :forbidden
          assert_equal "Forbidden by rate limit\nRate limit for test target requests.", @response.body
        end
      end
    end
  end

  describe "A DELETE to :delete_inactive" do
    before do
      login('minimum_admin')
      assert @account.targets.inactive.any?
      delete :delete_inactive
    end

    it "is deprecated" do
      assert_response :gone, @response.body
    end
  end

  describe 'Permissions' do
    before do
      @user = users(:minimum_agent)
      @user.permission_set = @user.account.permission_sets.create!(name: "Test")
      @user.account.stubs(:has_permission_sets?).returns(true)
      @request.account = @user.account

      login(@user)
    end

    describe 'Given an agent with the ability to edit extensions' do
      before do
        @user.permission_set.permissions.enable(:extensions_and_channel_management)
        assert @user.can?(:edit, Access::Settings::Extensions)
      end

      it 'manipulates the targets pages' do
        get :new, params: { id: 'campfire_target' }
        assert_response :success, @response.body
      end
    end

    describe 'Given an agent without the ability to edit extensions' do
      before do
        @user.permission_set.permissions.disable(:extensions_and_channel_management)
        refute @user.can?(:edit, Access::Settings::Extensions)
      end

      it 'does not manipulate targets pages' do
        get :new, params: { id: 'campfire_target' }
        assert_response :forbidden, @response.body
      end
    end

    describe 'Given an admin' do
      before do
        login(:minimum_admin)
      end

      it 'manipulates the targets pages' do
        get :new, params: { id: 'campfire_target' }
        assert_response :success, @response.body
      end
    end
  end

  describe "#remove_password_param?" do
    let(:controller) { TargetsController.new.tap { |controller| controller.stubs(params: params) } }

    describe "when params does not have a root target object" do
      let(:params) { {} }

      it "returns false" do
        refute controller.send(:remove_password_param?)
      end
    end

    describe "when params has a root target object" do
      describe "when the password is blank" do
        describe "when there is no password_overwrite param present" do
          let(:params) { { target: { username: '', password: '' } } }

          it "returns true" do
            assert controller.send(:remove_password_param?)
          end
        end

        describe "when there is a password_overwrite param present" do
          let(:params) { { target: { username: '', password: '', password_overwrite: 'true' } } }

          it "returns false" do
            refute controller.send(:remove_password_param?)
          end
        end
      end

      describe "when the password is not blank" do
        let(:params) { { target: { username: 'henry', password: '123456' } } }

        it "returns false" do
          refute controller.send(:remove_password_param?)
        end
      end
    end
  end

  describe "#message" do
    before { @controller.stubs(target: stub) }

    describe "when the target uses parameters" do
      before { @controller.send(:target).stubs(uses_parameters?: true) }

      describe "and the a message parameter is present" do
        before { @controller.stubs(params: { message: {0 => {0 => "key", 1 => "value"}} }) }

        it "returns an array of arrays" do
          assert_equal [["key", "value"]], @controller.send(:message)
        end
      end

      describe "and the a message parameter is not present" do
        before { @controller.stubs(params: {}) }

        it "returns an empty array" do
          assert_equal [], @controller.send(:message)
        end
      end
    end

    describe "when the target does not use parameters" do
      before do
        @controller.send(:target).stubs(uses_parameters?: false)
        @controller.stubs(params: { message: "message" })
      end

      it "returns message" do
        assert_equal "message", @controller.send(:message)
      end
    end
  end
end
