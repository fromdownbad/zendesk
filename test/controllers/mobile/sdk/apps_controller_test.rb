require_relative "../../../support/test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"

SingleCov.covered! uncovered: 13

describe Mobile::Sdk::AppsController do
  fixtures :accounts, :brands, :role_settings, :users, :account_settings, :mobile_sdk_apps, :mobile_sdk_auths, :oauth_clients

  def get_create_parameters(auth_type = MobileSdkAuth::TYPE_ANONYMOUS)
    params = {
      "mobile_sdk_app" => {
        "title" => "My First App",
        "settings" => {
          "contact_us_tags" => "mobile, iphone",
          "conversations_enabled" => "0",
          "rma_tags" => "",
          "authentication" => auth_type.dup,
          "connect_enabled" => "0"
        },
        "identifier" => SecureRandom.hex(24),
      },
      "blank_client_identifier" => "mobile_sdk_client_#{SecureRandom.hex(10)}"
    }
    if auth_type == MobileSdkAuth::TYPE_JWT
      params["mobile_sdk_auth"] = {
        "endpoint" => "https://endpoint",
        "shared_secret" => "js6gJwjIclbnuUzOXjG84vzDo9foeVuzDgVkmGXGcfGzJUVC"
      }
    end
    params
  end

  def get_update_parameters(initial_jwt = false, auth_type = MobileSdkAuth::TYPE_ANONYMOUS)
    app = mobile_sdk_app
    app.settings.authentication = initial_jwt ? MobileSdkAuth::TYPE_JWT : MobileSdkAuth::TYPE_ANONYMOUS
    app.save

    params = {
      "id" => app.id,
      "mobile_sdk_app" => {
        "title" => app.title,
        "settings" => {
          "contact_us_tags" => app.settings.contact_us_tags.join(","),
          "conversations_enabled" => app.settings.conversations_enabled,
          "rma_tags" => app.settings.rma_tags.join(","),
          "authentication" => auth_type.dup,
          "connect_enabled" => app.settings.connect_enabled
        },
        "identifier" => app.identifier,
      },
      "mobile_sdk_auth" => {
        "endpoint" => app.mobile_sdk_auth.endpoint,
        "shared_secret" => "WRONG"
      }
    }
    [app, params]
  end

  def inject_invalid_endpoint(params)
    params["mobile_sdk_auth"]["endpoint"] = "http://endpoint"
    params
  end

  def stub_fetch_groups_phone_numbers_request
    body = {
      'groups' => [
        {
          'id' => 1,
          'phone_numbers' => [
            {
              'id' => 1,
              'display_number' => '+353 01010101'
            }
          ]
        }
      ]
    }.to_json
    @talk_request = stub_request(
      :get,
      "https://minimumsdk.zendesk-test.com/api/v2/channels/voice/groups_phone_numbers.json"
    ).to_return(status: 200, body: body, headers: { 'Content-Type' => 'application/json' })
  end

  def stub_create_sdk_embeddable(status: 200)
    body = {
      'talk_embeddable' => {
        'id' => 1
      }
    }.to_json
    @talk_request = stub_request(
      :post,
      'https://minimumsdk.zendesk-test.com/api/v2/channels/voice/talk_embeddables/sdk.json'
    ).to_return(status: status, body: body, headers: { 'Content-Type' => 'application/json' })
  end

  def stub_update_sdk_embeddable(status: 200)
    body = {
      'talk_embeddable' => {
        'id' => 1
      }
    }.to_json
    @talk_request = stub_request(
      :put,
      'https://minimumsdk.zendesk-test.com/api/v2/channels/voice/talk_embeddables/sdk/1.json'
    ).to_return(status: status, body: body, headers: { 'Content-Type' => 'application/json' })
  end

  def stub_delete_sdk_embeddable(status: 200)
    body = {}.to_json

    @talk_request = stub_request(
      :delete,
      'https://minimumsdk.zendesk-test.com/api/v2/channels/voice/talk_embeddables/sdk/1.json'
    ).to_return(status: status, body: body, headers: { 'Content-Type' => 'application/json' })
  end

  let(:mobile_sdk_app) { FactoryBot.create(:mobile_sdk_app, account: @account, mobile_sdk_auth: mobile_sdk_auths(:minimum_sdk)) }

  before do
    @minimum_sdk_admin = users(:minimum_sdk_admin)
    @account = accounts(:minimum_sdk)
    @controller.stubs(:current_brand).returns(@account.brands.first)
    @request.account = @account
    use_ssl

    login(@minimum_sdk_admin)
  end

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: "mobile/sdk/apps") do |request|
    request.should_route :get, "/mobile/sdk/apps", action: "index"
    request.should_route :post, "/mobile/sdk/apps", action: "create"
    request.should_route :get, "/mobile/sdk/apps/new", action: "new"
    request.should_route :get, "/mobile/sdk/apps/1/edit", action: "edit", id: 1
    request.should_route :put, "/mobile/sdk/apps/1", action: "update", id: 1
    request.should_route :delete, "/mobile/sdk/apps/1", action: "destroy", id: 1
  end

  ['minimum_end_user', 'minimum_agent'].each do |user|
    describe user do
      before { login user }

      describe 'index' do
        it 'is forbidden' do
          get :index
          assert_response :forbidden
        end
      end
    end
  end

  describe "#onboarding" do
    describe "terms not accepted" do
      before do
        @account.settings.allowed_mobile_sdk = false
        @account.save
      end

      describe "no apps available" do
        before do
          @account.mobile_sdk_apps = []
          get :index
        end

        it "redirects to onboarding" do
          assert_response 302
          assert_redirected_to action: 'onboarding'
        end
      end

      describe "some apps available" do
        before do
          mobile_sdk_app
          get :index
        end

        it "shows index" do
          assert_response 200
        end

        it 'blank_app has the right default values' do
          blank_app = assigns(:blank_app)
          brand = @account.brands.first

          refute blank_app.settings.connect_enabled, 'Connect must be disabled by default'
          assert blank_app.settings.conversations_enabled, 'Conversations must be enabled by default'
          refute blank_app.settings.rma_enabled, 'RMA must be disabled by default'
          assert_equal brand.id, blank_app.brand_id
        end
      end
    end

    describe "terms accepted" do
      before do
        @account.settings.allowed_mobile_sdk = true
      end

      describe "no apps available" do
        before do
          @account.mobile_sdk_apps = []
          @account.save
          get :index
        end

        it "does not redirect to onboarding" do
          assert_response 200
        end
      end

      describe "some apps available" do
        before do
          mobile_sdk_app
          @account.save
          get :index
        end

        it "shows index" do
          assert_response 200
        end
      end
    end

    describe "hostmapped" do
      before do
        @account.update_attribute(:host_mapping, "foo.com")
        get :onboarding
      end

      it "shoulds not redirect to http" do
        assert_response 200
      end
    end
  end

  describe "#accept_tos" do
    before do
      @account.settings.allowed_mobile_sdk = false
      @account.save
      put :accept_tos, params: { agree_mobile_sdk_tos: true }
    end

    it "redirects to onboarding" do
      assert_response 302
      assert_redirected_to action: 'index'
      @account.reload
      assert @account.settings.allowed_mobile_sdk
    end
  end

  describe "#create" do
    describe "with no identity check selected" do
      before do
        mobile_sdk_app
        @minimum_params = get_create_parameters
        @minimum_params["mobile_sdk_app"]["settings"]["authentication"] = "other"

        post :create, params: @minimum_params
      end

      it "returns a 400 status code" do
        assert_response 400
      end

      it "does not display an error" do
        assert_nil flash[:error]
      end

      it "does not create app" do
        assert_nil @account.mobile_sdk_apps.find_by_identifier(@minimum_params["mobile_sdk_app"]["identifier"])
      end
    end

    describe "with JWT disabled" do
      before do
        mobile_sdk_app
        @minimum_params = get_create_parameters

        post :create, params: @minimum_params
      end

      it "creates app" do
        assert_response 302
        assert_not_nil msa = @account.mobile_sdk_apps.find_by_identifier(@minimum_params["mobile_sdk_app"]["identifier"])
        assert_equal MobileSdkAuth::TYPE_ANONYMOUS, msa.settings.authentication
        assert_not_nil msa.mobile_sdk_auth.client
        assert_equal @minimum_params["blank_client_identifier"], msa.mobile_sdk_auth.client.identifier
      end
    end

    describe "with JWT enabled" do
      before do
        mobile_sdk_app
        @minimum_params = get_create_parameters(MobileSdkAuth::TYPE_JWT)

        post :create, params: @minimum_params
      end

      it "creates app" do
        assert_response 302
        assert_not_nil msa = @account.mobile_sdk_apps.find_by_identifier(@minimum_params["mobile_sdk_app"]["identifier"])
        assert_not_nil msa.mobile_sdk_auth.client
        assert_equal @minimum_params["blank_client_identifier"], msa.mobile_sdk_auth.client.identifier
        assert_equal MobileSdkAuth::TYPE_JWT, msa.settings.authentication
        assert_equal @minimum_params["mobile_sdk_auth"]["endpoint"], msa.mobile_sdk_auth.endpoint
        assert_equal @minimum_params["mobile_sdk_auth"]["shared_secret"], msa.mobile_sdk_auth.shared_secret(true)
      end
    end

    describe "with `mobile_sdk_rma_tab_removal` feature enabled" do
      before do
        Arturo.enable_feature!(:mobile_sdk_rma_tab_removal)
        mobile_sdk_app
        @minimum_params = get_create_parameters
        @minimum_params["mobile_sdk_app"]["settings"]["rma_enabled"] = true

        post :create, params: @minimum_params
      end

      it "ignores `rma_enabled` true" do
        assert_response 302
        assert_not_nil msa = @account.mobile_sdk_apps.find_by_identifier(@minimum_params["mobile_sdk_app"]["identifier"])
        refute msa.settings.rma_enabled
      end
    end

    describe_with_and_without_arturo_enabled :mobile_sdk_help_center_article_voting_enabled do
      before do
        @minimum_params = get_create_parameters
        @minimum_params_settings = @minimum_params["mobile_sdk_app"]["settings"]
      end

      [true, false].each do |help_center_article_voting_enabled|
        it "has the `help_center_article_voting_enabled` setting set to #{help_center_article_voting_enabled}" do
          @minimum_params_settings["help_center_article_voting_enabled"] = help_center_article_voting_enabled
          post :create, params: @minimum_params

          assert_response 302
          msa = @account.mobile_sdk_apps.find_by_identifier(@minimum_params["mobile_sdk_app"]["identifier"])
          assert_not_nil msa

          assert_equal @arturo_enabled, @account.has_mobile_sdk_help_center_article_voting_enabled?

          if @arturo_enabled
            assert_equal msa.settings.help_center_article_voting_enabled, help_center_article_voting_enabled
          else
            assert_equal msa.settings.help_center_article_voting_enabled, false
          end
        end
      end
    end

    describe_with_arturo_enabled :mobile_sdk_talk_settings do
      before do
        @account.settings.voice = true
        @account.save

        @minimum_params = get_create_parameters
        @minimum_params_settings = @minimum_params["mobile_sdk_app"]["settings"]

        # default talk settings values
        @minimum_params_settings[:talk_enabled] = false
        @minimum_params_settings[:talk_button_id] = ''
        @minimum_params_settings[:talk_group_id] = nil
        @minimum_params_settings[:talk_phone_number_id] = nil

        stub_create_sdk_embeddable
      end

      [true, false].each do |valid_settings|
        it "when talk settings are #{valid_settings ? 'NOT' : ''} set to nil values" do
          if valid_settings
            @minimum_params_settings[:talk_button_id] = 'abcdefghijklmnopqrstuvwxyz123456'
            @minimum_params_settings[:talk_group_id] = 1
            @minimum_params_settings[:talk_phone_number_id] = 1
          end

          post :create, params: @minimum_params

          assert_response 302
          assert_nil flash[:error]
          msa = @account.mobile_sdk_apps.find_by_identifier(@minimum_params["mobile_sdk_app"]["identifier"])
          assert_not_nil msa

          if valid_settings
            assert_requested(@talk_request)
            assert_not_nil msa.settings.talk_embeddable_id
          else
            assert_not_requested(@talk_request)
            assert_nil msa.settings.talk_embeddable_id
          end
        end
      end
    end

    describe_with_and_without_arturo_enabled :mobile_sdk_talk_settings do
      before do
        @minimum_params = get_create_parameters
        @minimum_params_settings = @minimum_params["mobile_sdk_app"]["settings"]

        # default talk settings values
        @minimum_params_settings[:talk_enabled] = false
        @minimum_params_settings[:talk_button_id] = ''
        @minimum_params_settings[:talk_group_id] = nil
        @minimum_params_settings[:talk_phone_number_id] = nil

        stub_create_sdk_embeddable
      end

      [true, false].each do |voice_enabled|
        it "has the `mobile_sdk_talk_settings` setting set to #{@arturo_enabled} AND `voice_enabled` set to #{voice_enabled}" do
          @minimum_params_settings[:talk_enabled] = false
          @minimum_params_settings[:talk_button_id] = 'abcdefghijklmnopqrstuvwxyz123456'
          @minimum_params_settings[:talk_group_id] = 1
          @minimum_params_settings[:talk_phone_number_id] = 1

          @account.settings.voice = voice_enabled
          @account.save

          post :create, params: @minimum_params

          assert_response 302
          assert_nil flash[:error]
          msa = @account.mobile_sdk_apps.find_by_identifier(@minimum_params["mobile_sdk_app"]["identifier"])
          assert_not_nil msa

          if @arturo_enabled && voice_enabled
            assert_requested(@talk_request)
            assert_not_nil msa.settings.talk_embeddable_id
          else
            assert_not_requested(@talk_request)
            assert_nil msa.settings.talk_embeddable_id
          end
        end
      end
    end

    describe_with_arturo_enabled :mobile_sdk_talk_settings do
      describe 'when POST request to Talk fails' do
        it 'does not create an app if POST request fails' do
          @minimum_params = get_create_parameters
          @minimum_params_settings = @minimum_params["mobile_sdk_app"]["settings"]
          @minimum_params_settings[:talk_enabled] = false
          @minimum_params_settings[:talk_button_id] = 'abcdefghijklmnopqrstuvwxyz123456'
          @minimum_params_settings[:talk_group_id] = 1
          @minimum_params_settings[:talk_phone_number_id] = 1

          @account.settings.voice = true
          @account.save

          stub_create_sdk_embeddable(status: 422)
          post :create, params: @minimum_params

          assert_response 302
          assert_not_nil flash[:error]
          msa = @account.mobile_sdk_apps.find_by_identifier(@minimum_params["mobile_sdk_app"]["identifier"])
          assert_nil msa

          assert_requested(@talk_request)
        end
      end
    end

    describe "with no terms of service accepted" do
      before do
        mobile_sdk_app
        @minimum_params = get_create_parameters
        @account.settings.allowed_mobile_sdk = false
        @account.save
        @minimum_params["agree_mobile_sdk_tos"] = true
        post :create, params: @minimum_params
      end

      it "creates app and accept terms" do
        assert_response 302
        @account.reload
        assert @account.settings.allowed_mobile_sdk
        assert_not_nil msa = @account.mobile_sdk_apps.find_by_identifier(@minimum_params["mobile_sdk_app"]["identifier"])
        assert_equal MobileSdkAuth::TYPE_ANONYMOUS, msa.settings.authentication
        assert_not_nil msa.mobile_sdk_auth.client
        assert_equal @minimum_params["blank_client_identifier"], msa.mobile_sdk_auth.client.identifier
      end
    end

    describe "http endpoint" do
      before do
        mobile_sdk_app
        @minimum_params = get_create_parameters(MobileSdkAuth::TYPE_JWT)
        @minimum_params = inject_invalid_endpoint(@minimum_params)

        post :create, params: @minimum_params
      end

      it "redirects to index" do
        assert_redirected_to mobile_sdk_apps_path
      end

      it "displays an error" do
        assert_not_nil flash[:error]
      end

      it "does not create app" do
        assert_nil @account.mobile_sdk_apps.find_by_identifier(@minimum_params["mobile_sdk_app"]["identifier"])
      end
    end
  end

  describe "#index" do
    describe "old JWT app" do
      before do
        @endpoint = "https://somesite.com/zd/auth"
        @shared_secret = "aub7qw64ci7467c64738iubyret"
        @app = mobile_sdk_apps(:minimum)
        @app.account = @account
        @app.save
        @msdk_settings = @account.build_mobile_sdk_settings(
          auth_endpoint: @endpoint,
          auth_token: @shared_secret
        )
        @msdk_settings.save

        get :index
      end

      it "migrates the JWT settings on save" do
        assert_response 200
        assert_template :index
      end
    end

    describe "old simple app" do
      before do
        @app = mobile_sdk_apps(:minimum)
        @app.account = @account
        @app.save

        get :index
      end

      it "migrates the JWT settings on save" do
        assert_response 200
        assert_template :index
      end
    end

    describe_with_and_without_arturo_enabled :mobile_sdk_help_center_article_voting_enabled do
      before do
        get :index
      end

      it "sets the correct value for help_center_article_voting_enabled" do
        assert_response 200
        assert_template :index

        assert_equal @arturo_enabled, assigns(:blank_app).settings.help_center_article_voting_enabled
      end
    end

    describe_with_and_without_arturo_enabled :mobile_sdk_talk_settings do
      before do
        @account.settings.voice = true
        @account.save

        stub_fetch_groups_phone_numbers_request
        get :index
      end

      it "sets the correct values for @groups_phone_numbers" do
        assert_response 200
        assert_template :index

        groups_phone_numbers = assigns(:groups_phone_numbers)
        if @arturo_enabled
          assert_not_nil groups_phone_numbers
        else
          assert_nil groups_phone_numbers
        end
      end
    end
  end

  describe "#update" do
    describe "an app with JWT disabled" do
      describe "enable JWT" do
        before do
          @app, @update_params = get_update_parameters(false, MobileSdkAuth::TYPE_JWT)
          @update_params["mobile_sdk_auth"]["endpoint"] = "https://new-endpoint"
          put :update, params: @update_params
          @app.reload
        end

        it "enables JWT" do
          assert_response 302
          assert_nil flash[:error]
          msa = @account.mobile_sdk_apps.find(@app.id)
          assert_equal MobileSdkAuth::TYPE_JWT, msa.settings.authentication
          assert_equal @update_params["mobile_sdk_auth"]["endpoint"], msa.mobile_sdk_auth.endpoint
          assert_not_equal @update_params["mobile_sdk_auth"]["shared_secret"], msa.mobile_sdk_auth.shared_secret(true)
        end
      end

      describe "general updates" do
        before do
          @app, @update_params = get_update_parameters(false, MobileSdkAuth::TYPE_ANONYMOUS)
          @update_params["mobile_sdk_app"]["title"] = "New Title"
          put :update, params: @update_params
          @app.reload
        end

        it "updates app" do
          assert_response 302
          msa = @account.mobile_sdk_apps.find(@app.id)
          assert_equal MobileSdkAuth::TYPE_ANONYMOUS, msa.settings.authentication
          assert_equal @update_params["mobile_sdk_app"]["title"], msa.title
          assert_not_equal @update_params["mobile_sdk_auth"]["shared_secret"], msa.mobile_sdk_auth.shared_secret(true)
        end
      end

      describe "update when `mobile_sdk_rma_tab_removal` feature enabled" do
        before do
          Arturo.enable_feature!(:mobile_sdk_rma_tab_removal)
          @app, @update_params = get_update_parameters(false, MobileSdkAuth::TYPE_ANONYMOUS)
          @update_params["mobile_sdk_app"]["settings"]["rma_enabled"] = true
          @app.settings.rma_enabled = true
          @app.save!

          # A bug was found where the action would crash because when this feature is enabled
          # params[:settings][:rma_tags] is not sent by the UI and we crash trying to do stuff
          # on nil. Here we remove it to test the fix.
          @update_params["mobile_sdk_app"]["settings"].delete("rma_tags")

          put :update, params: @update_params
          @app.reload
        end

        it "keeps `rma_enabled` true" do
          assert_response 302
          assert @app.settings.rma_enabled
        end
      end

      describe "updates when `mobile_sdk_rma_tab_removal` feature enabled and rma enabled for the app" do
        before do
          Arturo.enable_feature!(:mobile_sdk_rma_tab_removal)
          @app, @update_params = get_update_parameters(false, MobileSdkAuth::TYPE_ANONYMOUS)
          @update_params["mobile_sdk_app"]["settings"]["rma_enabled"] = true
          @app.settings.rma_enabled = true
          @app.save!
          put :update, params: @update_params
          @app.reload
        end

        it "does not ignore `rma_enabled` true" do
          assert_response 302
          msa = @account.mobile_sdk_apps.find(@app.id)
          assert msa.settings.rma_enabled
        end
      end
    end

    describe "an app with JWT enabled" do
      describe "disable JWT" do
        before do
          @app, @update_params = get_update_parameters(true, MobileSdkAuth::TYPE_ANONYMOUS)
          put :update, params: @update_params
          @app.reload
        end

        it "disables JWT" do
          assert_response 302
          msa = @account.mobile_sdk_apps.find(@app.id)
          assert_equal MobileSdkAuth::TYPE_ANONYMOUS, msa.settings.authentication
          assert_not_nil msa.mobile_sdk_auth.shared_secret
          assert_not_equal @update_params["mobile_sdk_auth"]["shared_secret"], msa.mobile_sdk_auth.shared_secret(true)
        end
      end

      describe "general updates" do
        before do
          @app, @update_params = get_update_parameters(true, MobileSdkAuth::TYPE_JWT)
          @update_params["mobile_sdk_app"]["title"] = "New Title"
          put :update, params: @update_params
          @app.reload
        end

        it "updates app" do
          assert_response 302
          msa = @account.mobile_sdk_apps.find(@app.id)
          assert_equal MobileSdkAuth::TYPE_JWT, msa.settings.authentication
          assert_equal @update_params["mobile_sdk_app"]["title"], msa.title
          assert_not_equal @update_params["mobile_sdk_auth"]["shared_secret"], msa.mobile_sdk_auth.shared_secret(true)
        end
      end

      describe "http endpoint" do
        before do
          @app, @update_params = get_update_parameters(true, MobileSdkAuth::TYPE_JWT)
          @update_params["mobile_sdk_auth"]["endpoint"] = "http://new-endpoint"
          put :update, params: @update_params
          @app.reload
        end

        it "does not update app" do
          assert_response 302
          assert flash[:error]
        end
      end
    end

    describe_with_and_without_arturo_enabled :mobile_sdk_talk_settings do
      [true, false].each do |voice_enabled|
        describe "when the `voice` Account setting is set to #{voice_enabled}" do
          before do
            @account.settings.voice = voice_enabled
            @account.save

            @app, @update_params = get_update_parameters

            @update_params_settings = @update_params["mobile_sdk_app"]["settings"]
            @update_params_settings[:talk_enabled] = false
            @update_params_settings[:talk_button_id] = 'abcdef0123456789ABCDEF0123456789'
            @update_params_settings[:talk_group_id] = '1'
            @update_params_settings[:talk_phone_number_id] = '1'
          end

          [true, false].each do |talk_settings_exist|
            describe "when that Mobile SDK app #{talk_settings_exist ? 'has existing Talk settings' : 'does not have existing talk settings'}" do
              before do
                if talk_settings_exist
                  @app.settings.talk_enabled = false
                  @app.settings.talk_group_id = 1
                  @app.settings.talk_phone_number_id = 1
                  @app.settings.talk_button_id = '0123456789abcdef0123456789ABCDEF'
                  @app.settings.talk_embeddable_id = 1
                  stub_create_sdk_embeddable
                  @app.save!

                  @update_params_settings[:talk_enabled] = true
                  stub_update_sdk_embeddable
                else
                  stub_create_sdk_embeddable
                end
              end

              describe 'when the `talk_button_id` setting is not valid' do
                it "does not send a #{talk_settings_exist ? 'PUT' : 'POST'} request" do
                  @update_params_settings.delete(:talk_button_id)
                  put :update, params: @update_params
                  assert_not_requested @talk_request
                end
              end

              describe 'when the `talk_group_id` setting is not valid' do
                it "does not send a #{talk_settings_exist ? 'PUT' : 'POST'} request" do
                  @update_params_settings.delete(:talk_group_id)
                  put :update, params: @update_params
                  assert_not_requested @talk_request
                end
              end

              describe 'when the `talk_phone_number_id` setting is not valid' do
                it "does not send a #{talk_settings_exist ? 'PUT' : 'POST'} request" do
                  @update_params_settings.delete(:talk_phone_number_id)
                  put :update, params: @update_params
                  assert_not_requested @talk_request
                end
              end

              describe 'when the `talk_button_id`, `talk_group_id` and `talk_phone_number_id` are valid`' do
                it "sends a #{talk_settings_exist ? 'PUT' : 'POST'} request" do
                  put :update, params: @update_params
                  if voice_enabled && @arturo_enabled
                    assert_requested @talk_request
                  else
                    assert_not_requested @talk_request
                  end
                end
              end

              [true, false].each do |request_success|
                describe "when the #{talk_settings_exist ? 'PUT' : 'POST'} request #{request_success ? 'succeeds' : 'fails'}" do
                  before do
                    @update_params['mobile_sdk_app']['title'] = 'Updated Title App'
                    @update_params_settings[:talk_enabled] = true
                    status = request_success ? 200 : 422

                    if talk_settings_exist
                      stub_update_sdk_embeddable(status: status)
                    else
                      stub_create_sdk_embeddable(status: status)
                    end
                    put :update, params: @update_params
                  end

                  it "#{request_success ? 'does not' : ''} display an error message" do
                    if voice_enabled && @arturo_enabled && !request_success
                      assert_not_nil flash[:error]
                    else
                      assert_nil flash[:error]
                    end
                  end

                  it 'redirects to #index' do
                    assert_response 302
                    assert_redirected_to mobile_sdk_apps_path
                  end

                  it 'does not update the app' do
                    @app.reload
                    if voice_enabled && @arturo_enabled && !request_success
                      assert_not_equal @app.title, @update_params['mobile_sdk_app']['title']
                      assert_not_equal @app.settings.talk_enabled, @update_params_settings[:talk_enabled]
                    else
                      assert_equal @app.title, @update_params['mobile_sdk_app']['title']
                    end
                  end
                end
              end
            end
          end
        end
      end
    end

    describe "#update_talk_config" do
      describe_with_arturo_enabled :mobile_sdk_talk_settings do
        before do
          @account.settings.voice = true
          @account.save

          @app, @update_params = get_update_parameters
          @settings = @update_params["mobile_sdk_app"]["settings"]

          # set Talk settings values
          @app.settings.talk_embeddable_id = 1
          @update_params["mobile_sdk_app"]["settings"]["talk_enabled"] = @app.settings.talk_enabled = false
          @update_params["mobile_sdk_app"]["settings"]["talk_button_id"] = @app.settings.talk_button_id = 'abcdef0123456789ABCDEF0123456789'
          @update_params["mobile_sdk_app"]["settings"]["talk_group_id"] = @app.settings.talk_group_id = '1'
          @update_params["mobile_sdk_app"]["settings"]["talk_phone_number_id"] = @app.settings.talk_phone_number_id = '1'

          @app.save

          stub_update_sdk_embeddable
        end

        describe "when none of the Talk settings has changed" do
          it "does not send a PUT request" do
            put :update, params: @update_params
            assert_not_requested @talk_request
          end
        end

        {
          "talk_enabled" => true,
          "talk_button_id" => "abcdef0123456789ABCDEF012345678A",
          "talk_group_id" => 2,
          "talk_phone_number_id" => 2
        }.each do |setting, new_value|
          describe "when the setting `#{setting}` value has changed" do
            before do
              @update_params["mobile_sdk_app"]["settings"][setting] = new_value
            end

            it "sends a PUT request" do
              put :update, params: @update_params
              assert_requested @talk_request
            end
          end
        end
      end
    end

    describe "accept terms of service" do
      before do
        @app, @update_params = get_update_parameters(false, MobileSdkAuth::TYPE_ANONYMOUS)
        @account.settings.allowed_mobile_sdk = false
        @account.save
        @update_params["agree_mobile_sdk_tos"] = true
        put :update, params: @update_params
        @app.reload
      end

      it "updates app" do
        assert_response 302
        @account.reload
        assert @account.settings.allowed_mobile_sdk
      end
    end

    describe "push notifications" do
      before do
        @app, @update_params = get_update_parameters(false, MobileSdkAuth::TYPE_ANONYMOUS)
      end

      describe "configuration" do
        describe "setup webhook" do
          before do
            @webhook_url = "http://webhook"
            @update_params["mobile_sdk_app"]["settings"]["push_notifications_type"] = MobileSdkApp::PUSH_NOTIF_TYPE_WEBHOOK.dup
            @update_params["mobile_sdk_app"]["settings"]["push_notifications_callback"] = @webhook_url
            put :update, params: @update_params
            @app.reload
          end

          it "enables push notifications" do
            assert_response 302
            msa = @account.mobile_sdk_apps.find(@app.id)
            assert msa.settings.push_notifications_enabled
            assert_equal @webhook_url, msa.settings.push_notifications_callback
            assert_equal MobileSdkApp::PUSH_NOTIF_TYPE_WEBHOOK, msa.settings.push_notifications_type
          end
        end

        describe "setup urban airship" do
          before do
            @ua_key = "ua_key"
            @ua_master_secret = "ua_master_secret"
            @update_params["mobile_sdk_app"]["settings"]["push_notifications_type"] = MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP.dup
            @update_params["mobile_sdk_app"]["settings"]["urban_airship_key"] = @ua_key
            @update_params["mobile_sdk_app"]["settings"]["urban_airship_master_secret"] = @ua_master_secret
            put :update, params: @update_params
            @app.reload
          end

          it "enables push notifications" do
            assert_response 302
            msa = @account.mobile_sdk_apps.find(@app.id)
            assert msa.settings.push_notifications_enabled
            assert_equal @ua_key, msa.settings.urban_airship_key
            assert_equal @ua_master_secret, msa.settings.urban_airship_master_secret
            assert_equal MobileSdkApp::PUSH_NOTIF_TYPE_URBAN_AIRSHIP, msa.settings.push_notifications_type
          end
        end

        describe "ignore push notifications" do
          before do
            @update_params["mobile_sdk_app"]["settings"]["push_notifications_type"] = "empty"
            put :update, params: @update_params
            @app.reload
          end

          it "keeps push notifications disabled" do
            assert_response 302
            msa = @account.mobile_sdk_apps.find(@app.id)
            refute msa.settings.push_notifications_enabled
            assert_nil msa.settings.push_notifications_type
          end
        end
      end
    end
  end

  describe "#destroy" do
    before do
      @app = mobile_sdk_app
    end

    describe "when the app setting `talk_embeddable_id` is set" do
      before do
        @app.settings.talk_embeddable_id = 1
        @app.save
      end

      [true, false].each do |request_success|
        describe "when the DELETE request #{request_success ? 'succeeds' : 'fails'}" do
          before do
            status = request_success ? 200 : 400
            stub_delete_sdk_embeddable(status: status)
            delete :destroy, params: { id: @app.id }
          end

          it 'sends a DELETE request' do
            assert_requested @talk_request
          end

          it "#{request_success ? 'does not display' : 'displays'} an error message" do
            if request_success
              assert_nil flash[:error]
            else
              assert_not_nil flash[:error]
            end
          end

          it "the app is #{request_success ? '' : 'NOT'} deleted" do
            if request_success
              assert_empty @account.mobile_sdk_apps.where(id: @app.id)
            else
              assert_not_empty @account.mobile_sdk_apps.where(id: @app.id)
            end
          end

          it 'redirects to #index' do
            assert_redirected_to mobile_sdk_apps_path
          end
        end
      end
    end

    describe "when the app setting `talk_embeddable_id` is NOT set" do
      before do
        @app.save
        delete :destroy, params: { id: @app.id }
      end

      it "does not display an error message" do
        assert_nil flash[:error]
      end

      it "the app is deleted" do
        assert_empty @account.mobile_sdk_apps.where(id: @app.id)
      end

      it 'redirects to #index' do
        assert_redirected_to mobile_sdk_apps_path
      end
    end
  end
end
