require_relative "../../../support/test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"

SingleCov.covered! uncovered: 13

describe Mobile::Sdk::AuthsController do
  fixtures :accounts, :role_settings, :users, :account_settings

  let(:account) { accounts(:minimum) }
  let(:oauth_client) { FactoryBot.create(:client, account: account) }
  let(:mobile_sdk_auth) { FactoryBot.create(:mobile_sdk_auth, account: account, client: oauth_client) }

  before do
    @minimum_admin = users(:minimum_admin)
    @account = accounts(:minimum)
    @request.account = @account
    use_ssl

    ActionController::Parameters.action_on_unpermitted_parameters = false

    login(@minimum_admin)
  end

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: "mobile/sdk/auths") do |request|
    request.should_route :get, "/mobile/sdk/auths", action: "index"
    request.should_route :post, "/mobile/sdk/auths", action: "create"
    request.should_route :get, "/mobile/sdk/auths/new", action: "new"
    request.should_route :get, "/mobile/sdk/auths/1/edit", action: "edit", id: 1
    request.should_route :put, "/mobile/sdk/auths/1", action: "update", id: 1
    request.should_route :delete, "/mobile/sdk/auths/1", action: "destroy", id: 1
    request.should_route :put, "/mobile/sdk/auths/1/generate_secret", action: "generate_secret", id: 1
  end

  ['minimum_end_user', 'minimum_agent'].each do |user|
    describe user do
      before { login user }

      describe 'index' do
        it 'is forbidden' do
          get :index
          assert_response :forbidden
        end
      end
    end
  end

  describe "#create" do
    before do
      @auth_name = "New Auth"
      @client = oauth_client
      @client.name = "Client for #{@auth_name}"
      @client.identifier = @client.name.downcase.gsub(/\s/, "_")
      @client.save!
      @params = {
        mobile_sdk_auth: {
          name: @auth_name,
          endpoint: "https://new-endpoint",
          shared_secret: "sdfa4gv5wey6eh7"
        },
        client: {
          secret: "de6d45e78d67a097af"
        }
      }

      put :create, params: @params
    end

    it "creates new auth with an unique client identifier" do
      assert_response 302
      sdk_auth = account.mobile_sdk_auths.find_by_name(@auth_name)
      assert sdk_auth
      assert_not_equal sdk_auth.client.identifier, @client.identifier
    end
  end

  describe "#update" do
    before do
      @sdk_auth = mobile_sdk_auth

      @params = {
        mobile_sdk_auth: {
          name: "New Name",
          endpoint: "https://new-endpoint",
          shared_secret: "00000000000000000"
        },
        client: {
          secret: "00000000000000"
        },
        id: @sdk_auth.id
      }

      put :update, params: @params
      @sdk_auth.reload
    end

    it "does not update the secrets" do
      assert_response 302

      assert_not_equal @sdk_auth.shared_secret, @params[:mobile_sdk_auth][:shared_secret]
      assert_not_equal @sdk_auth.client.secret, @params[:client][:secret]
      assert_equal @params[:mobile_sdk_auth][:endpoint], @sdk_auth.endpoint
      assert_equal @params[:mobile_sdk_auth][:name], @sdk_auth.name
    end
  end
end
