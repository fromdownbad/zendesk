require_relative "../../support/test_helper"

SingleCov.covered!

describe Mobile::LandingPageController do
  describe "LandingPageController" do
    fixtures :accounts, :users, :role_settings

    before do
      @request.account = @account = accounts(:minimum)
      @request.stubs(:has_mobile_switch_enabled?).returns(true)
      login('minimum_agent')
      get :index, format: 'mobile_v2'
    end

    describe "#index" do
      it('responds with ok') { assert_response :ok }
    end
  end
end
