require_relative "../../support/test_helper"

SingleCov.covered!

describe Mobile::SwitchController do
  describe "SwitchController" do
    fixtures :accounts, :users

    before do
      @return_to = "https://minimum.zendesk-test.com/agent/tickets/1234"
      @request.account = @account = accounts(:minimum)
      @request.user_agent = 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en)'
      login('minimum_agent')
    end

    describe "on first visit" do
      describe "without return_to" do
        before do
          get :index
        end

        it "redirects to the home controller" do
          assert_redirected_to "https://minimum.zendesk-test.com/agent"
        end

        it "does not set the session as mobile" do
          assert_equal false, session[Schmobile::IS_MOBILE]
        end
      end

      describe "with return_to" do
        before do
          get :index, params: { return_to: @return_to }
        end

        it "redirects to the return_to url" do
          assert_redirected_to @return_to
        end

        it "does not set the session as mobile" do
          assert_equal false, session[Schmobile::IS_MOBILE]
        end
      end

      describe "with illegal return_to" do
        it "redirects to /home and not the return_to value" do
          get :index, params: { return_to: 'http://some-illegal-url.com/tickets/1234' }
          assert_redirected_to "https://minimum.zendesk-test.com/agent"
        end
      end
    end

    describe "when mobile_switch setting is disabled" do
      before do
        @account.settings.mobile_switch_enabled = false
        @account.save!
      end

      describe "without return_to" do
        describe "on second visit" do
          before do
            get :index
            get :index
          end

          it "redirects to the home controller" do
            assert_redirected_to "https://minimum.zendesk-test.com/agent"
          end

          it "sets the session as mobile" do
            assert session[Schmobile::IS_MOBILE]
          end
        end
      end

      describe "with return_to" do
        describe "on second visit" do
          before do
            get :index, params: { return_to: @return_to }
          end

          it "redirects to the home controller" do
            get :index, params: { return_to: @return_to }
            assert_redirected_to "https://minimum.zendesk-test.com/agent"
          end

          it "sets the session as mobile" do
            get :index, params: { return_to: @return_to }
            assert session[Schmobile::IS_MOBILE]
          end

          describe "with return_to being an illegal url" do
            it "redirects to /home and not the return_to value" do
              get :index, params: { return_to: 'http://some-illegal-url.com/tickets/1234' }
              assert_redirected_to "https://minimum.zendesk-test.com/agent"
            end
          end
        end
      end
    end

    describe "when mobile_switch setting is enabled" do
      before do
        @account.settings.mobile_switch_enabled = true
        @account.save!
      end

      describe "on second visit" do
        describe "without return_to" do
          before do
            get :index
            get :index
          end

          it "redirects to the home controller" do
            assert_redirected_to "https://minimum.zendesk-test.com/agent"
          end

          it "sets the session as mobile" do
            assert session[Schmobile::IS_MOBILE]
          end
        end

        describe "with return_to" do
          before do
            get :index, params: { return_to: @return_to }
          end

          it "redirects to the mobile controller" do
            get :index, params: { return_to: @return_to }
            assert_redirected_to @return_to
          end

          it "sets the session as mobile" do
            get :index, params: { return_to: @return_to }
            assert session[Schmobile::IS_MOBILE]
          end

          describe "with return_to being an illegal url" do
            before do
              get :index, params: { return_to: 'http://some-illegal-url.com/tickets/1234' }
            end

            it "redirects to /agent and not the return_to value" do
              assert_redirected_to "https://minimum.zendesk-test.com/agent"
            end
          end
        end
      end
    end
  end
end
