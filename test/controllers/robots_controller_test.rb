require_relative "../support/test_helper"

SingleCov.covered!

describe RobotsController do
  fixtures :accounts, :brands

  before do
    @account = accounts(:minimum)
    @brand = brands(:minimum)
    @account.default_brand = @brand
    @account.save!

    @request.account = @account
    @controller.stubs(:current_brand).returns(@brand)
  end

  it "routes properly" do
    # We cannot use the `should_route` wrapper because it does not expose
    # the `extras` param necessary to specify the request format
    options = { controller: "robots", action: "index", "format" => "txt" }
    extras = { "format" => "txt" }
    assert_routing("/robots", options, {}, extras)
  end

  describe "#index" do
    it "responds with 200 OK" do
      get :index, format: :txt
      assert_response :ok
    end

    it "responds with a content type of 'text/plain'" do
      get :index, format: :txt
      assert_equal @response.headers["Content-Type"], "text/plain; charset=UTF-8"
    end

    it "includes a sitemap entry" do
      get :index, format: :txt
      assert_match(/sitemap\.xml/, @response.body)
    end

    it "allows indexing Help Center" do
      get :index, format: :txt
      assert_no_match(/^Disallow: \/hc$/, @response.body)
    end

    describe "on a trial" do
      before do
        @account = accounts(:trial)
        @brand = brands(:trial)
        @account.default_brand = @brand
        @account.save!

        @request.account = @account
        @controller.stubs(:current_brand).returns(@brand)
      end

      it "disallows indexing Help Center" do
        get :index, format: :txt
        assert_match(/^Disallow: \/hc$/, @response.body)
      end
    end
  end
end
