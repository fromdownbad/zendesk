require_relative "../support/test_helper"

SingleCov.covered!

describe AssetsController do
  fixtures :accounts, :users, :subscriptions, :translation_locales, :logos, :photos, :voice_uploads

  as_an_anonymous_user do
    before do
      Rails.cache.write("english_in_the_database_reload", [])
    end

    describe "assets in the cloud" do
      before do
        @s3_header = %r{zendesk-test.*/public/system/logos/20090591/minimum_logo.jpg}
        get :show, params: { id: "/system/logos/2009/0591/minimum_logo.jpg" }
      end

      describe "assets uri" do
        it "sets the correct header" do
          get :show, params: { id: "/system/logos/2009/0591/minimum_logo.jpg" }
          @response.headers['X-S3-Storage-Url'].must_match(
            %r{\.amazonaws\.com/public/system/logos/20090591/minimum_logo\.jpg}
          )
        end

        it "sets the correct header with large ids" do
          logos(:minimum_logo).update_column(:id, 120090591)
          get :show, params: { id: "/system/logos/0001/2009/0591/minimum_logo.jpg" }
          @response.headers['X-S3-Storage-Url'].must_match(
            %r{\.amazonaws\.com/public/system/logos/120090591/minimum_logo\.jpg}
          )
        end
      end

      it "sets s3 location header" do
        @response.headers['X-S3-Storage-Url'].must_match @s3_header
      end

      it "does not have ;charset=utf8 in the Content-Type header" do
        @response.headers["Content-Type"].wont_include "charset"
      end
    end

    describe "voice assets in the cloud" do
      let(:s3_url) do
        %r{\.amazonaws\.com/public/system/voice/uploads/20090589/railroad\.mp3}
      end

      before do
        # welcome to the EU!
        AssetsController.any_instance.stubs(:default_stores).returns([:s3eu])
      end

      it "sets s3 location header" do
        get :show, params: { id: "/system/voice/uploads/2009/0589/railroad.mp3" }
        @response.headers['X-S3-Storage-Url'].must_match s3_url
      end

      it "uses class-preferred s3 store if available" do
        Voice::Upload.where(filename: 'railroad.mp3').first.update_column(:stores, "fs,s3apne,s3eu,s3")
        get :show, params: { id: "/system/voice/uploads/2009/0589/railroad.mp3" }
        @response.headers['X-S3-Storage-Url'].must_match s3_url
      end

      it "uses region-preferred s3 store if necessary" do
        Voice::Upload.where(filename: 'railroad.mp3').first.update_column(:stores, "fs,s3apne,s3eu")
        get :show, params: { id: "/system/voice/uploads/2009/0589/railroad.mp3" }
        @response.headers['X-S3-Storage-Url'].must_match s3_url
      end

      it "uses non-preferred s3 store if necessary" do
        Voice::Upload.where(filename: 'railroad.mp3').first.update_column(:stores, "fs,s3apne")
        get :show, params: { id: "/system/voice/uploads/2009/0589/railroad.mp3" }
        @response.headers['X-S3-Storage-Url'].must_match s3_url
      end
    end

    describe 'response cookies' do
      describe 'photos and logos' do
        describe_with_arturo_disabled :assets_strip_response_cookies_photos_and_logos do
          it 'does not strip response cookies' do
            get :show, params: { id: "/system/logos/2009/0591/minimum_logo.jpg" }

            refute request.env['rack.session.options'][:skip]
          end
        end

        describe_with_arturo_enabled :assets_strip_response_cookies_photos_and_logos do
          it 'strip response cookies' do
            get :show, params: { id: "/system/logos/2009/0591/minimum_logo.jpg" }

            assert request.env['rack.session.options'][:skip]
          end
        end

        # Should not be affected by voice/uploads arturo

        describe_with_arturo_disabled :assets_strip_response_cookies_voice_uploads do
          it 'does not strip response cookies' do
            get :show, params: { id: "/system/logos/2009/0591/minimum_logo.jpg" }

            refute request.env['rack.session.options'][:skip]
          end
        end

        describe_with_arturo_enabled :assets_strip_response_cookies_voice_uploads do
          it 'does not strip response cookies' do
            get :show, params: { id: "/system/logos/2009/0591/minimum_logo.jpg" }

            refute request.env['rack.session.options'][:skip]
          end
        end
      end

      describe 'voice uploads' do
        describe_with_arturo_disabled :assets_strip_response_cookies_voice_uploads do
          it 'does not strip response cookies' do
            get :show, params: { id: "/system/voice/uploads/2009/0589/railroad.mp3" }

            refute request.env['rack.session.options'][:skip]
          end
        end

        describe_with_arturo_enabled :assets_strip_response_cookies_voice_uploads do
          it 'strip response cookies' do
            get :show, params: { id: "/system/voice/uploads/2009/0589/railroad.mp3" }

            assert request.env['rack.session.options'][:skip]
          end
        end

        # Should not be affected by photos/logos arturo

        describe_with_arturo_disabled :assets_strip_response_cookies_photos_and_logos do
          it 'does not strip response cookies' do
            get :show, params: { id: "/system/voice/uploads/2009/0589/railroad.mp3" }

            refute request.env['rack.session.options'][:skip]
          end
        end

        describe_with_arturo_enabled :assets_strip_response_cookies_photos_and_logos do
          it 'does not strip response cookies' do
            get :show, params: { id: "/system/voice/uploads/2009/0589/railroad.mp3" }

            refute request.env['rack.session.options'][:skip]
          end
        end
      end
    end

    describe "invalid url" do
      it "returns 404" do
        get :show, params: { id: "some/booboo" }
        assert_response 404
      end
    end

    describe "bad stores" do
      it "does not throw exception with 1 bad store" do
        logos(:minimum_logo).update_column(:stores, 'bad_store,fs')
        get :show, params: { id: "/system/logos/2009/0591/minimum_logo.jpg" }
      end

      it "does not throw exception with only bad stores" do
        logos(:minimum_logo).update_column(:stores, 'bad_store')
        get :show, params: { id: "/system/logos/2014/0428/minimum_logo.jpg" }
      end
    end

    it "returns 404 when the asset doesn't belong to the current account" do
      other_account = accounts(:support)
      logos(:minimum_logo).update_column(:account_id, other_account.id)
      get :show, params: { id: "/system/logos/2009/0591/minimum_logo.jpg" }

      assert_response 404
    end
  end
end
