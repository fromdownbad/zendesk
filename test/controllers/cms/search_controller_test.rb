require_relative "../../support/test_helper"

SingleCov.covered!

describe Cms::SearchController do
  fixtures :accounts, :users, :subscriptions

  with_options(controller: 'cms/search') do |request|
    request.should_route :get, '/cms/search', action: 'index'
  end

  before do
    @request.account = accounts(:minimum)
  end

  describe "Searching for content" do
    it "succeeds for admins" do
      admin = users(:minimum_admin)
      login(admin)
      options = { page: 1, per_page: 25, order: [:created_at],
                  endpoint: 'cms', without: {type: :article}, type: 'cms' }

      Zendesk::Search.expects(:search).with(admin, 'hi', options).returns(Zendesk::Search::Results.empty)
      get :index, params: { query: 'hi' }
      assert_response :success
    end

    it "is denied to non-admins" do
      login('minimum_agent')
      get :index, params: { query: 'hi' }

      assert_response :forbidden
    end

    describe "For agents whose role give access to dynamic content" do
      before do
        @agent = users(:minimum_agent)
        @permission_set = PermissionSet.new
        @permission_set.permissions.enable(:manage_dynamic_content)
        @agent.stubs(:permission_set).returns(@permission_set)
        login(@agent)

        options = { page: 1, per_page: 25, order: [:created_at],
                    endpoint: 'cms', without: {type: :article}, type: 'cms' }
        Zendesk::Search.expects(:search).with(@agent, 'hi', options).returns(Zendesk::Search::Results.empty)
      end

      it "succeeds" do
        get :index, params: { query: 'hi' }
        assert_response :success
      end
    end
  end
end
