require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 8

describe Cms::VariantsController do
  fixtures :accounts, :users

  before do
    Account.any_instance.stubs(:has_cms?).returns(true)

    @account = accounts(:minimum)

    @request.account = @account
    @user = login(:minimum_admin)

    @locale  = stub(id: 1, name: "English", locale: "en", localized_name: "English")
    @locale2 = stub(id: 2, name: "Deutsch", locale: "de", localized_name: "German")
    @account.stubs(:cms_texts).returns(Cms::Text)
    Cms::Text.any_instance.stubs(:account).returns(@account)
    @account.stubs(:available_languages).returns([@locale, @locale2])
    unless @text = Cms::Text.find_by_name_and_account_id("Wombat", @account.id)
      params = { name: "Wombat",
                 fallback_attributes: { is_fallback: "true", nested: true, value: "wibble", translation_locale_id: 1 }}
      @text = Cms::Text.new(params)
      @text.account = @account
      @text.save!
    end
    @variant = @text.fallback
    @second_variant = @text.variants.create!(value: "Content B", translation_locale_id: 4, active: true, is_fallback: false)
    Cms::VariantsController.any_instance.stubs(:current_account).returns(@account)
    Cms::Text.any_instance.stubs(:locales).returns([@locale, @locale2])
    Cms::Variant.any_instance.stubs(:name).returns(@locale.name)
    Cms::Variant.any_instance.stubs(:localized_name).returns(@locale.localized_name(display_in: I18n.translation_locale))
  end

  it "gets new" do
    get :new, params: { cms_text_id: @text.id }
    assert_response :success
  end

  it "creates a text variant" do
    assert_difference('Cms::Variant.count(:all)') do
      post :create, params: { cms_text_id: @text.id, cms_variant: {
        active: true,
        value: "wha?",
        translation_locale_id: 2
      } }
    end

    assert_redirected_to cms_text_path(@text)
  end

  it "gets edit" do
    get :edit, params: { cms_text_id: @text.id, id: @variant.to_param }
    assert_response :success
  end

  it "updates an existing text variant" do
    put :update, params: { cms_text_id: @text.id, id: @variant.to_param, variant: { value: "wobble" } }
    assert_redirected_to cms_text_path(@text)
  end

  it "invalidates ticket_fields cache on update" do
    original_cache_key = @variant.account.scoped_cache_key(:ticket_fields)
    Timecop.travel(Time.now + 1.seconds)
    put :update, params: { cms_text_id: @text.id, id: @variant.to_param, variant: { value: "wobble" } }
    assert_not_equal original_cache_key, @variant.account.scoped_cache_key(:ticket_fields)
  end

  it "destroys an existing text variant" do
    assert_difference('Cms::Variant.count(:all)', -1) do
      delete :destroy, params: { cms_text_id: @text.id, id: @second_variant.to_param }
    end

    assert_redirected_to cms_text_path(@text)
  end
end
