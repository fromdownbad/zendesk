require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 40

describe Cms::TextsController do
  fixtures :accounts, :users

  before do
    Account.any_instance.stubs(:has_cms?).returns(true)

    @account = accounts(:minimum)
    @request.account = @account
    @user = login(:minimum_admin)

    @account.stubs(:available_languages).returns(
      [
        stub(name: "English by Zendesk", localized_name: "English", id: 1),
        stub(name: "English (UK)", localized_name: "British English", id: 2)
      ]
    )
    @account.stubs(:cms_texts).returns(Cms::Text)
    Cms::Text.any_instance.stubs(:account).returns(@account)
    Cms::TextsController.any_instance.stubs(:current_account).returns(@account)
    Cms::Text.any_instance.stubs(:remaining_locales).returns([stub(name: "en-UK", id: 2)])
    Cms::Variant.any_instance.stubs(:name).returns("English")
    Cms::Text.any_instance.stubs(:references).returns([])
    String.any_instance.stubs(:strip_tags).returns("")
    @fbattrs = { is_fallback: "true", value: "wibble", translation_locale_id: 1 }
    unless @text = Cms::Text.find_by_name_and_account_id("OMGWTFBBQ", @account.id)
      params = { name: "OMGWTFBBQ", fallback_attributes: @fbattrs}
      @text = Cms::Text.new(params)
      @text.account = @account
      @text.save!
    end
    @variant = @text.fallback
    String.any_instance.stubs(:truncate).returns(String)
  end

  it "gets index" do
    @controller.expects(:register_device_activity)
    get :index
    assert_response :success
    assert_not_nil assigns(:cms_texts)
  end

  it "gets new" do
    get :new
    assert_response :success
  end

  it "creates the text resource and the first variant/fallback" do
    assert_difference('Cms::Text.count(:all)') do
      post :create, params: { cms_text: {
        name: "weee!",
        fallback_attributes: @fbattrs
      } }
    end
    assert_redirected_to cms_text_path(assigns(:cms_text))
  end

  it "shows text" do
    get :show, params: { id: @text.to_param }
    assert_response :success
  end

  it "updates text" do
    put :update, params: { id: @text.to_param, cms_text: { name: "wut" } }
    assert_redirected_to cms_text_path(assigns(:cms_text))
  end

  it "destroys text" do
    assert_difference('Cms::Text.count(:all)', -1) do
      delete :destroy, params: { id: @text.to_param }
    end

    assert_redirected_to cms_texts_path
  end
end
