require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 5

describe Cms::BaseController do
  should_include_module Zendesk::LotusEmbeddable
end
