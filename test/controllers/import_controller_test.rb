require_relative "../support/test_helper"

SingleCov.covered! uncovered: 13

describe ImportController do
  fixtures :all

  with_options(controller: "import") do |request|
    request.should_route :get, "/import", action: 'index'
    request.should_route :post, '/import/create', action: 'create'
  end

  before do
    @request.account = accounts(:minimum)
  end

  describe "as an agent" do
    before { login('minimum_agent') }
    describe "#index" do
      before { get :index }
      it('responds with forbidden') { assert_response :forbidden }
    end
    describe "#create" do
      before { post :create }
      it('responds with forbidden') { assert_response :forbidden }
    end
  end

  describe "#index" do
    before { login('minimum_admin') }
    describe "for users" do
      before { get :index, params: { kind: 'user' } }
      should_assign_to(:kind) { 'user' }
    end
    describe "for organizations" do
      before { get :index, params: { kind: 'organization' } }
      should_assign_to(:kind) { 'organization' }
    end
    describe "with no kind" do
      before { get :index }
      should_assign_to(:kind) { 'user' }
    end
    describe "with unknown kind" do
      before { get :index, params: { kind: 'unknown' } }
      it('responds with bad_request') { assert_response :bad_request }
    end
  end

  describe "#create" do
    describe 'with raw data' do
      before do
        @user = users(:minimum_admin)
        login(@user)
        post :create, params: { kind: 'user', update_records: "true", raw_data: raw_data, password_email_change_csv_import: false }
        @token = UploadToken.last
      end

      should_create :upload_token

      should_create :attachment

      it "creates the token with the correct attachment associated" do
        assert_equal @user, @token.source
        assert_equal @token, @user.account.upload_tokens.find_by_value(@token.value)
        assert_equal 1, @token.attachments.size
        attachment = @token.attachments.first
        assert_equal 'text/csv', attachment.content_type
        assert_equal raw_data, attachment.current_data
      end
    end

    describe 'with uploaded data' do
      before do
        @user = users(:minimum_admin)
        login(@user)
        uploaded_data = fixture_file_upload('users_import.csv', 'text/csv')
        post :create, params: { kind: 'user', update_records: "true", uploaded_data: uploaded_data, password_email_change_csv_import: false }
        @token = UploadToken.last
      end

      should_create :upload_token

      should_create :attachment

      it "creates the token with the correct attachment associated" do
        assert_equal @user, @token.source
        assert_equal @token, @user.account.upload_tokens.find_by_value(@token.value)
        assert_equal 1, @token.attachments.size
        attachment = @token.attachments.first
        assert_equal 'text/csv', attachment.content_type
        assert_equal raw_data, attachment.current_data
      end
    end
  end

  describe "jobs enqueue" do
    let(:referrer) { "https://subdomain/import?kind=#{kind}" }

    before do
      @user = users(:minimum_admin)
      login(@user)
      @controller.request.stubs(:referrer).returns(referrer)
    end

    describe "when reporting external id" do
      let(:kind) { 'user' }

      it 'enqueues UsersJob with correct params with enabled and with report_external_id false' do
        UsersJob.expects(:enqueue).with(@request.account.id, @user.id, true, anything, false, referrer, false, false)
        post :create, params: { kind: kind, update_records: "true", raw_data: raw_data, password_email_change_csv_import: false, allow_agent_downgrade: "false", report_external_id: 'false'}
      end

      it 'enqueues UsersJob with correct params with enabled and with report_external_id true' do
        UsersJob.expects(:enqueue).with(@request.account.id, @user.id, true, anything, false, referrer, false, true)
        post :create, params: { kind: kind, update_records: "true", raw_data: raw_data, password_email_change_csv_import: false, allow_agent_downgrade: "false", report_external_id: 'true'}
      end

      it 'enqueues UsersJob with correct params with enabled and with report_external_id omitted' do
        UsersJob.expects(:enqueue).with(@request.account.id, @user.id, true, anything, false, referrer, false, false)
        post :create, params: { kind: kind, update_records: "true", raw_data: raw_data, password_email_change_csv_import: false, allow_agent_downgrade: "false"}
      end
    end

    describe "when kind is organization" do
      let(:kind) { 'organization' }

      it 'enqueues OrganizationsJob with correct params' do
        OrganizationsJob.expects(:enqueue).with(@request.account.id, @user.id, true, anything, referrer)
        post :create, params: { kind: kind, update_records: "true", raw_data: raw_data }
      end
    end
  end

  describe "redirects to the correct location for certain jobs" do
    before do
      @user = users(:minimum_admin)
      login(@user)
    end

    describe 'password_email_change_csv_import' do
      it 'sets the password_email_change_csv_import parameter for the job if we are doing a user import and update_records is set to true' do
        post :create, params: { kind: 'user', update_records: "true", raw_data: raw_data, password_email_change_csv_import: false }
        assert_redirected_to people_path
      end

      it 'does not set the password_email_change_csv_import parameter for the job if we are not doing a user import and update_records is set to true' do
        post :create, params: { kind: 'organization', update_records: "true", raw_data: raw_data, password_email_change_csv_import: '0' }
        assert_redirected_to organizations_path
      end

      it 'does not set the password_email_change_csv_import parameter for the job if we are doing user import but the update_records is set to false' do
        post :create, params: { kind: 'user', update_records: "false", raw_data: raw_data, password_email_change_csv_import: '0' }
        assert_redirected_to people_path
      end
    end

    it "organization imports" do
      post :create, params: { kind: 'organization', update_records: "true", raw_data: raw_data }
      assert_redirected_to organizations_path
    end

    it "user imports" do
      post :create, params: { kind: 'user', update_records: "true", raw_data: raw_data, password_email_change_csv_import: false }
      assert_redirected_to people_path
    end
  end

  describe "forbids bulk user uploads for risky accounts" do
    before do
      @user = users(:minimum_admin)
      login(@user)
      @account = @request.account
      @account.stubs(:trusted_bulk_uploader?).returns(false)
    end

    it "restricts if not trusted_bulk_uploader" do
      post :create, params: { kind: 'user', update_records: "true", raw_data: raw_data, password_email_change_csv_import: false }
      assert_response :forbidden
    end

    it "does not restrict if organization upload" do
      post :create, params: { kind: 'organization', update_records: "true", raw_data: raw_data, password_email_change_csv_import: false }
      assert_response :redirect
    end

    it "does not restrict if trusted_bulk_uploader" do
      @account.stubs(:trusted_bulk_uploader?).returns(true)
      post :create, params: { kind: 'user', update_records: "true", raw_data: raw_data, password_email_change_csv_import: false }
      assert_response :redirect
    end
  end

  describe "#create - empty data" do
    before do
      @user = users(:minimum_admin)
      login(@user)
    end

    it "displays error message when no file is attached for import" do
      @controller.expects(:enqueue_job).never
      post :create, params: { kind: 'user', update_records: "true", commit: "ok" }
      assert_equal "There was an error in uploading. Please provide a file to upload.", flash[:error]
    end

    it "enqueues for raw posts" do
      @controller.expects(:enqueue_job).once
      raw_post :create, { kind: 'user', update_records: "true" }, raw_data
    end
  end

  # this is helper to test the post method for the api call
  def raw_post(action, params, body)
    @request.env["RAW_POST_DATA"] = body
    response = post(action, params: params)
    @request.env.delete("RAW_POST_DATA")
    response
  end

  def raw_data
    <<~DATA
      \"name\",\"email\",\"password\",\"phone\",\"notes\",\"external_id\",\"details\",\"role\",\"organization\",\"restriction\"
      \"John Doe\",\"john@company.com\",123456,\"555-43987\",\"Some notes about John\",11111,\"Some details about John\",\"Agent\",#{organizations(:minimum_organization1).name},\"Assigned\""
    DATA
  end
end
