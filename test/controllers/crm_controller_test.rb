require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe CrmController do
  fixtures :accounts, :users, :salesforce_integrations, :tickets, :external_user_datas, :external_ticket_datas

  before do
    @request.account = accounts(:minimum)
    salesforce_integration = SalesforceIntegration.new(is_sandbox: false, access_token: "access", refresh_token: "refresh", instance_url: "https://na2.salesforce.com", app_latency: 60)
    Account.any_instance.stubs(:salesforce_integration).returns(salesforce_integration)
    SalesforceIntegration.any_instance.stubs(:enabled?).returns(true)
  end

  should_be_unauthorized_when_not_logged_in([:sync_user_info])
  should_be_unauthorized_when_not_logged_in([:sync_user_info], "minimum_end_user", 403)
  should_be_unauthorized_when_not_logged_in([:sync_ticket_info])
  should_be_unauthorized_when_not_logged_in([:sync_ticket_info], "minimum_end_user", 403)

  describe "#sync_user_info" do
    before do
      login('minimum_agent')
      @user = users(:minimum_end_user)
    end

    it "is accessible via route" do
      assert_equal "/crm/sync_user_info", sync_user_info_crm_path
    end

    it "responds properly when called on an account with no crm integration configured" do
      Account.any_instance.stubs(:crm_integration).returns(nil)

      get :sync_user_info, params: { id: @user.id, format: 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"status" => "errored"}, result)
    end

    it "responds properly when called on a user with no salesforce data and start sync" do
      @user.salesforce_data = nil

      SalesforceData.any_instance.expects(:start_sync)

      get :sync_user_info, params: { id: @user.id, format: 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"status" => "pending"}, result)
    end

    it "responds with a pending status when a sync is pending" do
      @user.salesforce_data = SalesforceData.create(user: @user, sync_status: ExternalUserData::SyncStatus::SYNC_PENDING)
      @user.salesforce_data.save

      SalesforceData.any_instance.stubs(:start_sync)

      get :sync_user_info, params: { id: @user.id, format: 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"status" => "pending"}, result)
    end

    it "responds with data when called on a user with salesforce data" do
      @user.salesforce_data = SalesforceData.create(user: @user, sync_status: ExternalUserData::SyncStatus::SYNC_OK, data: {records: [:foo, :bar]})
      @user.salesforce_data.save

      get :sync_user_info, params: { id: @user.id, format: 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"status" => "ok", "records" => ["foo", "bar"]}, result)
    end

    it "responds with error status if an error happened on the last fetch and start another sync" do
      @user.salesforce_data = SalesforceData.create(user: @user)
      @user.salesforce_data.save
      @user.salesforce_data.sync_errored!(data: [])

      SalesforceData.any_instance.expects(:start_sync)

      get :sync_user_info, params: { id: @user.id, format: 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"status" => "errored"}, result)
    end
  end

  describe "#sync_ticket_info" do
    before do
      login('minimum_agent')
      @ticket = tickets(:minimum_1)
    end

    it "is accessible via route" do
      assert_equal "/crm/sync_ticket_info", sync_ticket_info_crm_path
    end

    it "responds properly when called on a ticket with no salesforce data and start sync" do
      @ticket.salesforce_ticket_data.try(:destroy)

      SalesforceTicketData.any_instance.expects(:start_sync)

      get :sync_ticket_info, params: { ticket_id: @ticket.nice_id, format: 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"status" => "pending"}, result)
    end

    it "responds with a pending status when a sync is pending" do
      @ticket.salesforce_ticket_data.update_attributes(sync_status: ExternalTicketData::SyncStatus::SYNC_PENDING)

      SalesforceTicketData.any_instance.stubs(:start_sync)

      get :sync_ticket_info, params: { ticket_id: @ticket.nice_id, format: 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"status" => "pending"}, result)
    end

    it "responds with data when called on a user with salesforce data" do
      @ticket.salesforce_ticket_data.update_attributes(sync_status: ExternalTicketData::SyncStatus::SYNC_OK, data: {records: [:foo, :bar]})

      get :sync_ticket_info, params: { ticket_id: @ticket.nice_id, format: 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"status" => "ok", "records" => ["foo", "bar"]}, result)
    end

    it "responds with error status if an error happened on the last fetch and start another sync" do
      @ticket.salesforce_ticket_data.sync_errored!(data: [])

      SalesforceTicketData.any_instance.expects(:start_sync)

      get :sync_ticket_info, params: { ticket_id: @ticket.nice_id, format: 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"status" => "errored"}, result)
    end

    it "responds with data when called from the test widget" do
      SalesforceIntegration.any_instance.expects(:fetch_ticket_info).returns(records: [:foo, :bar])

      get :sync_ticket_info, params: { "ticket.requester.name" => "Some Name", :format => 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"status" => "ok", "records" => ["foo", "bar"]}, result)
    end

    it "handles connection problems during testing" do
      SalesforceIntegration.any_instance.expects(:fetch_ticket_info).raises(Salesforce::Integration::LoginFailed)

      get :sync_ticket_info, params: { "ticket.requester.name" => "Some Name", :format => 'json' }
      assert_response :ok
      assert_equal Mime[:json].to_s, @response.content_type

      result = JSON.parse(@response.body)
      assert_equal({"status" => "errored"}, result)
    end
  end
end
