require_relative "../../support/test_helper"
require_relative "../../support/billing_test_helper"
require_relative "../../support/voice_test_helper"

SingleCov.covered!

describe Account::SubscriptionController do
  include BillingTestHelper

  fixtures :all

  with_options(controller: "account/subscription") do |request|
    request.should_route :put, "/account/subscription/upgrade_from_trial", action: 'upgrade_from_trial'
    request.should_route :get, "/account/subscription/upgrade_from_trial", action: 'upgrade_from_trial'
  end

  should_include_module Zendesk::LotusEmbeddable

  describe "authorization" do
    describe "end users" do
      before do
        @account = accounts(:minimum)
        @request.account = @account
        login('minimum_end_user')
      end

      it "is not able to view :show" do
        get :show
        assert_response 403
      end
    end

    describe "account admins" do
      before do
        @account = accounts(:minimum)
        @request.account = @account
        login('minimum_admin_not_owner')
        get :show
      end

      it "is redirected" do
        assert_redirected_to @controller.send(:subscription_settings_path)
      end
    end
  end

  # testing that all templates render, logging in as owner to minimize pain
  describe 'Action' do
    before do
      @account = accounts(:minimum)
      @request.account = @account
      login('minimum_admin')
    end

    describe '#show' do
      it 'redirects' do
        get :show

        assert_response 302
      end
    end

    describe '#upgrade_from_trial' do
      before do
        # use the trial account instead of the default minimum account so we don't get redirected to show
        @account = accounts(:trial)
        @account.stubs(:is_being_migrated_from_braintree?).returns(false)
        @request.account = @account
        login('trial_admin')
      end
    end
  end

  describe '#show' do
    before do
      @account = accounts(:minimum)
      @request.account = @account
      login('minimum_admin')
    end

    describe 'when no subscription exists (multiproduct)' do
      before do
        @account.subscription.destroy
        get :show
      end

      should_redirect_to('the lotus subscription page') { @controller.send :subscription_settings_path }
    end

    describe 'when subscription exists' do
      describe 'and account is multiproduct' do
        before do
          @account.update_attributes(multiproduct: true)
          get :show
        end

        should_redirect_to('the Admin Center billing page') { '/admin/billing' }
      end

      describe 'and account is not multiproduct' do
        before do
          get :show
        end

        should_redirect_to('the lotus subscription page') { @controller.send :subscription_settings_path }
      end
    end
  end

  describe "During the :upgrade_from_trial action" do
    before { @params = {subscription: {plan_type: SubscriptionPlanType.Large, billing_cycle_type: BillingCycleType.Monthly, max_agents: 19}} }

    describe "a not-trial user" do
      before do
        @account = accounts(:minimum)
        @request.account = @account
        login('minimum_admin')

        get :upgrade_from_trial, params: @params
      end

      it "redirects to #show" do
        assert_redirected_to @controller.send(:subscription_settings_path)
      end
    end

    describe "a trial user" do
      before do
        @account = accounts(:trial)
        @request.account = @account
        login('trial_admin')
      end

      describe "without subscription parameters" do
        it "redirects to #show" do
          get :upgrade_from_trial
          assert_redirected_to @controller.send(:subscription_settings_path)
        end
      end
    end
  end

  describe "Manually billed subscriptions" do
    before do
      @account = accounts(:trial)
      @account.subscription.update_attributes! manual_discount: 100, manual_discount_expires_on: 10.days.from_now.to_s(:db), payment_method_type: PaymentMethodType.MANUAL
      @request.account = @account
      login('trial_admin')
    end
    it "is redirected to #sponsorship" do
      get :show
      assert_redirected_to action: 'sponsorship'
    end
  end

  describe "Sponsored accounts" do
    before do
      @account = accounts(:trial)
      @account.subscription.managed_update = true
      @account.subscription.update_attributes! manual_discount: 100, manual_discount_expires_on: 10.days.from_now.to_s(:db)
      @request.account = @account
      login('trial_admin')
    end

    it "does not go into an infinite loop" do
      get :sponsorship
      assert_response 200
    end

    it "is redirected to #sponsorship" do
      get :show
      assert_redirected_to action: 'sponsorship'
    end
  end
end
