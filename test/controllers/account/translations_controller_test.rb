require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::TranslationsController do
  fixtures :accounts, :users, :subscriptions, :payments, :addresses

  before do
    @current_account = accounts(:minimum)
    @request.account = @current_account
    login(:minimum_admin)
  end

  describe "#index" do
    describe "with has_individual_language_selection" do
      before do
        Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
        get :index, format: "xml"
      end
      it('responds with content_type xml') { assert_equal 'application/xml', @controller.response.content_type.to_s }
      should_assign_to(:locales) { @current_account.available_languages }
    end

    describe "without has_individual_language_selection" do
      before do
        Subscription.any_instance.stubs(:has_individual_language_selection?).returns(false)
        get :index, format: "xml"
      end
      it('responds with forbidden') { assert_response :forbidden }
    end
  end
end
