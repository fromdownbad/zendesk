require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Account::BaseController do
  fixtures :accounts, :users

  should_be_unauthorized_when_not_logged_in([:update])

  before do
    @minimum_account = accounts(:minimum)
    @request.account = @minimum_account
  end

  describe "#update" do
    before do
      login('minimum_admin')
      refute accounts(:minimum).settings.no_short_url_tweet_append?
      set_header "HTTP_REFERRER", "/home"
    end

    it "updates an account feature" do
      put :update, params: { account: { settings: {no_short_url_tweet_append: '1'}} }
      assert_redirected_to "#{@minimum_account.url(mapped: false, ssl: true)}/agent"
      assert accounts(:minimum).reload.settings.no_short_url_tweet_append?
    end

    it "updates an account feature and do a proper redirect" do
      put :update, params: { account: { settings: {no_short_url_tweet_append: '1'}}, return_to: '/account/channels' }
      assert_redirected_to '/account/channels'
      assert accounts(:minimum).reload.settings.no_short_url_tweet_append?
    end
  end
end
