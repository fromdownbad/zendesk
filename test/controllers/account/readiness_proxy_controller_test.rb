require_relative "../../support/test_helper"
require_relative "../../support/consul_lookup_support"

SingleCov.covered!

describe Account::ReadinessProxyController do
  include ConsulLookupSupport
  let(:account_id) { '1' }
  let(:verify_url) { 'https://test.zd-test.com/verification/token/asdf' }
  let(:current_pod_id) { Zendesk::Configuration.fetch(:pod_id) }
  let(:statsd_client) { Rails.application.config.statsd.client }

  ENV['ACCOUNT_SERVICE_JWT_SECRET'] = '2e54d0782643e9d4fbb8606984b74ffd7f43ffdd9175184364df457e4f671d4ecf8f9cafdb5a0d0e5fcce68400da4838e719d6a84140f0b350d367d340572cb'

  def stub_account_service_jwt(jwt_claims)
    JWT.encode(jwt_claims, ENV['ACCOUNT_SERVICE_JWT_SECRET'])
  end

  def stub_account_service_readiness_request(status: 200, state: 'pending')
    stub_request(:get, /\/api\/services\/accounts\/[0-9]+\/ready\?jwt=.+/).
      to_return(status: status, body: JSON.dump('state' => state), headers: {'Content-Type' => 'application/json'})
  end

  def valid_jwt_claims(timestamp: Time.now)
    {
      iat: timestamp.to_i.to_s,
      exp: (timestamp.to_i + 300).to_s,
      account_id: :account_id,
      subdomain: 'test',
      pod_id: current_pod_id,
      owner_id: '10001',
      products_activated: ['chat'],
      owner_verify_link: verify_url
    }
  end

  let(:invalid_jwt_claims) { valid_jwt_claims(timestamp: Time.now - 2.days) }

  before do
    Timecop.freeze(Time.now)
    stub_account_service_lookup
    stub_auth_service_lookup
  end

  with_options(controller: 'account/readiness_proxy') do |request|
    request.should_route :get, '/account/1/ready', action: 'ready', id: 1
  end

  describe '#ready' do
    it 'returns completed response if account service responds with completed' do
      stub_account_service_readiness_request(state: 'complete')
      statsd_client.expects(:increment).with('account_setup.state.complete').once
      get :ready, params: { format: :json, id: account_id, jwt: stub_account_service_jwt(valid_jwt_claims) }
      assert_response :success
      assert_equal "{\"state\":\"complete\"}", @response.body
    end

    it 'returns pending response if account service responds with pending' do
      stub_account_service_readiness_request(state: 'pending')
      statsd_client.expects(:increment).with('account_setup.state.pending').once
      get :ready, params: { format: :json, id: account_id, jwt: stub_account_service_jwt(valid_jwt_claims) }
      assert_response :success
      assert_equal "{\"state\":\"pending\"}", @response.body
    end

    it 'returns pending response if account service response is not OK (200)' do
      stub_account_service_readiness_request(status: 503, state: 'pending')
      statsd_client.expects(:increment).with('account_setup.state.pending').once
      get :ready, params: { format: :json, id: account_id, jwt: stub_account_service_jwt(valid_jwt_claims) }
      assert_response :success
      assert_equal "{\"state\":\"pending\"}", @response.body
    end

    it 'returns pending if JWT provided is invalid account service response is not OK (200)' do
      stub_account_service_readiness_request(status: 503, state: 'pending')
      statsd_client.expects(:increment).with('account_setup.state.pending').once
      get :ready, params: { format: :json, id: account_id, jwt: stub_account_service_jwt(valid_jwt_claims) }
      assert_response :success
      assert_equal "{\"state\":\"pending\"}", @response.body
    end

    it 'returns 406 response for non-json formats' do
      get :ready, params: { id: account_id, jwt: stub_account_service_jwt(valid_jwt_claims) }
      assert_equal 406, @response.status
    end
  end
end
