require_relative "../../support/test_helper"

SingleCov.covered!

describe Account::FeatureBoostController do
  describe "with existing feature boost" do
    before do
      @account = accounts(:minimum)
      FeatureBoost.create!(account: @account, valid_until: 30.days.from_now, boost_level: 4)
      @request.account = @account
      login('minimum_admin')
    end

    describe "#show" do
      it "is redirected" do
        get :show
        assert_redirected_to @controller.send(:subscription_settings_path)
      end
    end

    describe "#create" do
      it "is redirected" do
        post :create
        assert_redirected_to @controller.send(:subscription_settings_path)
      end
    end
  end

  describe "without existing feature boost" do
    before do
      @account = accounts(:minimum)
      @request.account = @account
      login('minimum_admin')
    end

    describe "#create" do
      it "creates a feature boost and then is redirected" do
        post :create, params: { valid_until: 10.days.from_now }

        assert_response :redirect, @response.body
        assert_redirected_to @controller.send(:subscription_settings_path)
        assert @account.feature_boost.present?
      end
    end
  end
end
