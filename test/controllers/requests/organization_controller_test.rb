require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 10

describe Requests::OrganizationController do
  fixtures :all

  before do
    @request.account = accounts(:minimum)
  end

  let(:user)                  { users(:minimum_end_user) }
  let(:default_organization)  { organizations(:minimum_organization1) }
  let(:unshared_organization) { organizations(:minimum_organization2) }

  it "generates organization_request_path" do
    assert_equal "/organizations/#{default_organization.id}/requests", organization_requests_path(default_organization)
  end

  describe "#index" do
    describe "without an organization" do
      # This should be safe to remove once the /organization_requests
      # route is removed from config/routes.rb (line 195 as of 2013-05-08)
      describe "and the current_user has a default_organization" do
        before do
          assert user.can?(:view, default_organization)
          login(user)
        end

        it "is successful" do
          get :index
          assert_response :success
          assert_template :index
        end

        it "scopes requests to the default_organization" do
          get :index
          assert_equal(
            default_organization, @controller.send(:current_organization)
          )
        end
      end

      describe "and the current_user does not have a default_organization" do
        it "is forbidden" do
          login(users(:minimum_author))
          get :index
          assert_response :forbidden
        end
      end
    end

    describe "with an organization" do
      describe "and it is one of the current_users' organizations" do
        describe "and it is not the default organization" do
          before do
            Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
            unshared_organization.update_attribute(:is_shared, true)
            user.update_attribute(:organizations, [default_organization, unshared_organization])
            assert user.can?(:view, unshared_organization)
            login(user)
          end

          it "is success" do
            get :index, params: { organization_id: unshared_organization.id }
            assert_response :success
            assert_template :index
          end

          it "scopes requests to the organization" do
            get :index, params: { organization_id: unshared_organization.id }
            assert_equal(
              unshared_organization, @controller.send(:current_organization)
            )
          end

          it "renders rss" do
            get :index, params: { organization_id: unshared_organization.id, format: :rss }
            assert_response :success
            assert_template 'requests/shared/index'
          end
        end

        describe "but the current_user can't view the organization" do
          before do
            user.update_attribute(:organization, unshared_organization)
            refute user.can?(:view, unshared_organization)
            login(user)
          end

          it "is forbidden" do
            get :index, params: { organization_id: unshared_organization.id }
            assert_response 403
          end
        end
      end
    end
  end

  describe "#find_tickets" do
    describe "with a Ticket in hold" do
      before do
        login(user)
        Account.any_instance.stubs(:use_status_hold?).returns(true)
        ticket = tickets(:minimum_1)
        ticket.will_be_saved_by(users(:minimum_agent))
        ticket.update_attributes!(status_id: StatusType.HOLD,
                                  requester_id: users(:minimum_end_user).id)
      end

      describe "for accounts with #has_status_hold? enabled" do
        it "includes StatusType.HOLD for filters other than 'solved' and 'requester'" do
          get :index, params: { filter: "sdasd" }
          tickets, = @controller.send(:find_tickets)

          assert tickets.map(&:status_id).include?(StatusType.HOLD)
        end
      end

      describe "for accounts with #has_status_hold? disabled" do
        before { Account.any_instance.stubs(:use_status_hold?).returns(false) }

        it "does not include StatusType.HOLD for filters other than 'solved' and 'requester'" do
          get :index, params: { filter: "sdasd" }
          tickets, = @controller.send(:find_tickets)

          refute tickets.map(&:status_id).include?(StatusType.HOLD)
        end
      end
    end
  end

  describe "#search" do
    before do
      login(user)
      ZendeskSearch::Client.any_instance.stubs(:search).returns(count: 0, results: [])
    end

    it "succeeds" do
      Zendesk::Search.expects(:search).with(user, 'hi', default_search_options.merge(type: 'ticket'))
      get :search, params: { query: 'hi' }
    end

    it "redirects to the ticket when the ticket is associated with one of the user's organizations" do
      Account.any_instance.stubs(has_multiple_organizations_enabled?: true)
      unshared_organization.update_attribute(:is_shared, true)
      user.update_attribute(:organizations, [default_organization, unshared_organization])
      assert user.can?(:view, unshared_organization)

      ticket = tickets(:minimum_3)
      ticket.will_be_saved_by(user)
      ticket.update_attribute(:organization_id, unshared_organization.id)

      get :search, params: { organization_id: unshared_organization.id, query: ticket.nice_id }
      assert_redirected_to request_path(ticket.nice_id)
    end

    it "does not redirect to tickets that are not associated with the organization" do
      ticket = tickets(:minimum_1)
      assert_nil ticket.organization

      get :search, params: { query: ticket.nice_id }
      assert_response :success
    end

    it "supports ordering by updated at" do
      updated_by_options = { sort_mode: :desc, order: :updated_at, type: 'ticket' }

      Zendesk::Search.expects(:search).with(anything, 'hi', default_search_options.merge(updated_by_options))
      get :search, params: { query: 'hi', by_updated: '1' }
    end
  end

  describe "#sidebar_users" do
    it "renders" do
      login(user)
      get :sidebar_users, params: { organization_id: default_organization.id }
      assert_response :success
    end
  end

  protected

  def default_search_options
    { per_page: 15, page: 1, without: {type: :article} }
  end
end
