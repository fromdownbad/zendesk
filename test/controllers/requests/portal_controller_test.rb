require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 97

describe Requests::PortalController do
  fixtures :users, :tickets, :accounts

  before do
    @account = @minimum_account = @request.account = accounts(:minimum)
    login('minimum_end_user')
  end

  describe "#new" do
    it "redirects to help center request creation" do
      get :new
      assert_redirected_to '/hc/requests/new'
    end
  end

  describe "#index" do
    it "redirects to help center request list" do
      get :index
      assert_redirected_to '/hc/requests'
    end
  end

  describe "#show" do
    it "redirects to help center request view" do
      get :show, params: { id: 1 }
      assert_redirected_to '/hc/requests/1'
    end
  end

  describe "#update" do
    it "redirects to help center request view" do
      ticket_id = tickets(:minimum_2).nice_id
      put :update, params: { id: ticket_id, comment: {value: "Blergh"} }
      assert_redirected_to "/hc/requests/#{ticket_id}"
    end
  end
end
