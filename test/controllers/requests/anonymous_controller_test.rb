require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Requests::AnonymousController do
  fixtures :all

  describe "#new" do
    before do
      @minimum_account = accounts(:minimum)
      @request.account = @minimum_account
      @field = ticket_fields(:field_tagger_custom)
    end

    describe "anonymous user" do
      where_web_portal_is_restricted do
        describe "where help center disabled" do
          before do
            @request.account.settings.help_center_state = :disabled
            @request.account.settings.save!

            get :new
          end

          it('responds with 404') { assert_response :not_found }
        end

        describe "where help center not disabled" do
          before { activate_hc! }

          new_redirects_to_help_center_new_request
        end
      end

      where_web_portal_is_disabled do
        describe "where help center disabled" do
          before do
            @request.account.settings.help_center_state = :disabled
            @request.account.settings.save!

            get :new
          end

          it('responds with 404') { assert_response :not_found }
        end

        describe "where help center not disabled" do
          before { activate_hc! }

          new_redirects_to_help_center_new_request
        end
      end
    end
  end

  describe "#create" do
    before do
      @request.account = accounts(:minimum)
    end

    describe "with a standard user agent" do
      before do
        @request.user_agent = 'Mozilla/5.0 (X11; U; Linux; en-US) AppleWebKit/527+ (KHTML, like Gecko'
        5.times { Prop.throttle!(:anonymous_requests, [@request.account.id, @request.remote_ip], threshold: @request.account.settings.anonymous_request_threshold) }
        post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }
      end

      before_should "not be rate limited" do
        # We're only logging throttled requests for the time being.
        Requests::AnonymousController.any_instance.expects(:anonymous_requests_throttled!).never
      end
    end

    describe "with a Help Center user agent" do
      before do
        @request.user_agent = 'HelpCenter'
        5.times { Prop.throttle!(:anonymous_requests, [@request.account.id, @request.remote_ip], threshold: @request.account.settings.anonymous_request_threshold) }
        post :create, params: { request: { subject: "Help!", comment: { value: "I need somebody!" } } }
      end

      before_should "not be rate limited" do
        Requests::AnonymousController.any_instance.expects(:anonymous_requests_throttled!).never
      end
    end
  end

  describe "unknown user with account allowing anonymous tickets" do
    before do
      @minimum_account = accounts(:minimum)
      @minimum_account.stubs(:domain_whitelist).returns("*")
      @request.account = @minimum_account
      @field = ticket_fields(:field_tagger_custom)
    end

    describe "with email" do
      before do
        post :create, params: { comment: {value: "akschashc"}, ticket: { description: 'hellows', subject: 'there', via_followup_source_id: "" }, email: "test@test.zen" }
      end
      should_create :ticket
      should_not_change("the number of suspended tickets") { SuspendedTicket.count(:all) }
      should_redirect_to('access') { '/access' }
    end

    describe "with blank email" do
      before do
        post :create, params: { comment: {value: "akschashc"}, ticket: { description: 'hellows', subject: 'there', via_followup_source_id: "" }, email: "" }
      end

      should_not_change("the number of tickets") { Ticket.count(:all) }
    end

    describe "with non-standard email value" do
      before do
        post :create, params: { comment: {value: "akschashc"}, ticket: { description: 'hellows', subject: 'there', via_followup_source_id: "" }, email: "سنخيث" }
      end

      should_not_change("the number of tickets") { Ticket.count(:all) }
    end
  end

  describe "unknown user with account not allowing anonymous tickets" do
    before do
      @minimum_account = accounts(:minimum)
      @minimum_account.stubs(:is_signup_required?).returns(true)
      @request.account = @minimum_account
      @field = ticket_fields(:field_tagger_custom)
    end

    it "shoulds only be able to submit a suspended ticket" do
      assert_difference 'SuspendedTicket.count(:all)', 1 do
        refute_difference 'Ticket.count(:all)' do
          post :create, params: { comment: {value: "akschashc"}, ticket: { description: 'hellows', subject: 'there', via_followup_source_id: "" }, email: "test@test.com" }
        end
      end
      assert flash[:notice] =~ /We sent you an email to verify your request because you are not signed in./
      refute (flash[:notice] =~ /sign in now to verify your request/)
      assert_equal SuspensionType.SIGNUP_REQUIRED, SuspendedTicket.last.cause
      assert_redirected_to '/access'
    end
  end

  describe "suspended user" do
    before do
      @minimum_account = accounts(:minimum)

      @minimum_author_suspend = users(:minimum_author_suspend)
      UserEmailIdentity.any_instance.stubs(:user).returns(@minimum_author_suspend)
      @minimum_author_suspend.stubs(:suspended?).returns(true)

      @request.account = @minimum_account
      @field = ticket_fields(:field_tagger_custom)
    end

    it "does not be allowed to submit a ticket" do
      refute_difference 'Ticket.count(:all)' do
        post :create, params: { comment: {value: "akschashc"}, ticket: { description: 'hellows', subject: 'there', via_followup_source_id: "" }, email: "minimum_author_suspend@aghassipour.com" }
      end
      assert flash[:error] =~ /You have been suspended from this account. You are not allowed to submit requests at this time./
    end
  end

  describe "known user not logged in" do
    before do
      @request.account = accounts(:minimum)
      @user = users(:minimum_end_user)
    end

    it "submits a flagged ticket" do
      assert_difference 'Ticket.count(:all)', 1 do
        post :create, params: { comment: {value: "akschashc"}, ticket: { description: 'hellows', subject: 'there', via_followup_source_id: "" }, email: @user.email }
      end
      assert flash[:notice] =~ /Request .* created/
      refute Ticket.order('created_at desc').first.trusted?
      assert_redirected_to '/access'
    end
  end

  describe "moved from test/functional/requests_anonymous_controller_test.rb" do
    fixtures :users, :translation_locales

    before do
      @request.account = accounts(:minimum)
      @request.account.stubs(:has_public_forums?).returns(false)
    end

    should_route :get, "anonymous_requests/new", controller: 'requests/anonymous', action: :new
    should_route :post, "anonymous_requests", controller: 'requests/anonymous', action: :create

    describe "#new" do
      describe "as an end user" do
        before do
          login('minimum_end_user')
          get :new
        end
        should_redirect_to("the portal controller") { new_request_path }
      end
    end

    describe "#index" do
      describe "as an anonymous user" do
        before { get :index }
        it('responds with unauthorized') { assert_response :unauthorized }
      end
      describe "as an end user" do
        before do
          login('minimum_end_user')
          get :index
        end
        should_redirect_to("the portal controller") { requests_path }
      end
    end

    describe "#create" do
      before do
        ActionMailer::Base.perform_deliveries = true
        @attributes = { email: 'anonymous@test.zen', comment: { value: 'Hello' }, ticket: { subject: "The Subject" } }
        accounts(:minimum).update_attribute(:domain_whitelist, 'test.zen')
        assert_nil accounts(:minimum).user_identities.email.find_by_value(@attributes[:email])
      end

      describe "repeated more than 5 times an hour" do
        let(:params) { { comment: { value: "comment" }, ticket: {} } }
        let(:ip_address) { "0.0.0.0" }
        before do
          Timecop.freeze
          ActionController::TestRequest.any_instance.stubs(remote_ip: ip_address)
          Requests::AnonymousController.any_instance.stubs(validate_blacklist: true)
        end

        it "raises a throttle exception" do
          4.times { Prop.throttle!(:anonymous_requests, [accounts(:minimum).id, ip_address]) }

          post :create, params: params
          assert_response :found

          post :create, params: params
          assert_response :forbidden
        end
      end

      describe "before_actions" do
        before do
          @user = users(:minimum_end_user)
        end

        let(:params) do
          { comment: { value: "comment" },
            ticket: {}, email: @user.email }
        end

        describe "with a disallowed user" do
          before do
            @request.account.expects(:allows?).with(@user.email).returns(false)

            post :create, params: params
          end

          should respond_with(:found)
        end

        describe "with an suspended user" do
          before do
            @user.suspended = true
            @user.save!

            post :create, params: params
          end

          should respond_with(:found)
        end

        describe "with an invalid captcha" do
          before do
            @controller.expects(:verify_recaptcha).returns(false)

            @request.account.settings.captcha_required = true
            @request.account.settings.save!

            post :create, params: params
          end

          should respond_with(:found)
        end
      end

      describe "when signup is required" do
        before do
          accounts(:minimum).enable_help_center!
          accounts(:minimum).stubs(:help_center_disabled?).returns(false)
          accounts(:minimum).update_attribute(:is_signup_required, true)
          add_attachment
          token = Token.last.value

          attrs = @attributes.dup

          accounts(:minimum).field_priority.update_attribute(:is_editable_in_portal, true)
          attrs[:ticket][:priority_id] = PriorityType.HIGH

          attrs[:ticket] = attrs[:ticket].merge(fields: {
            ticket_fields(:field_textarea_custom).id.to_s => "I'm random",
            ticket_fields(:field_text_custom).id.to_s     => "ORLY?",
            ticket_fields(:field_tagger_custom).id.to_s   => "hilarious"
          }, uploads: token)

          post :create, params: attrs
          assert_response :redirect
        end

        should_not_change("the number of users") { User.count(:all) }

        should_create :suspended_ticket
        should_not_change("the number of tickets") { Ticket.count(:all) }

        it "sends the ticket verification email" do
          ActionMailer::Base.deliveries.last.joined_bodies.include? 'verification/ticket'
        end

        it "sets custom fields and editable ticket system properties fields" do
          ticket = SuspendedTicket.last

          assert ticket.properties

          assert_equal ticket.properties[:fields][ticket_fields(:field_textarea_custom).id], "I'm random"
          assert_equal ticket.properties[:fields][ticket_fields(:field_text_custom).id], "ORLY?"
          assert_equal ticket.properties[:fields][ticket_fields(:field_tagger_custom).id], "hilarious"

          assert_equal ticket.properties[:priority_id], PriorityType.HIGH
          assert_nil ticket.properties[:ticket_type_id]
        end

        it "creates attachments on the suspended ticket" do
          ticket = SuspendedTicket.last
          assert !ticket.attachments.empty?
        end
      end

      describe "when signup is not required" do
        before do
          post :create, params: @attributes
          assert_response :redirect
        end

        before_should("not send a welcome email") { UsersMailer.expects(:deliver_verify).never }
        should_create :user
        should_create :ticket
        should_not_change("the number of suspended tickets") { SuspendedTicket.count(:all) }

        before_should "store a search event when the user comes from a search" do
          Requests::AnonymousController.any_instance.expects(:store_search_string)
          @attributes.merge!(ticket_from_search: 'foo bar')
        end
      end

      describe "when unknown user has selected a language" do
        before do
          @english = translation_locales(:english_by_zendesk)
          @selected_language = translation_locales(:japanese)
          I18n.locale = @selected_language

          Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
          Account.any_instance.stubs(:available_languages).returns([@english, @selected_language])
          @request.shared_session[:locale_id] = @selected_language.id
          post :create, params: @attributes
          assert_response :redirect
        end

        it "creates the requester with the selected language" do
          assert_equal @selected_language.id, accounts(:minimum).find_user_by_email(@attributes[:email]).locale_id
        end
      end

      describe "with verified email but not logged in" do
        before do
          @attributes[:email] = users(:minimum_end_user).email
        end

        it "submits a ticket with an untrusted comment" do
          refute_difference  'SuspendedTicket.count(:all)' do
            assert_difference 'Ticket.count(:all)', 1 do
              post :create, params: @attributes
            end
          end

          ticket = Ticket.last
          audit  = ticket.audits.last

          refute ticket.comments.last.trusted?
          refute ticket.trusted?
          assert_equal AuditFlags.new([EventFlagType.REGISTERED_USER_LOGGED_OUT]), audit.flags
          assert_redirected_to '/access'
        end
      end

      describe "with unverified email and not logged in" do
        before do
          @user = users(:minimum_end_user)
          @user.identities.email.last.update_attribute(:is_verified, false)
          @attributes[:email] = @user.email
          post :create, params: @attributes
          assert_response :redirect
        end

        should_create :ticket
      end
    end
  end
end
