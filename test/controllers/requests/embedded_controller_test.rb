require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 19

describe Requests::EmbeddedController do
  fixtures :all

  with_options(controller: "requests/embedded") do |request|
    request.should_route :get,  "/requests/embedded/create",      action: 'create'
    request.should_route :post, "/requests/embedded/create",      action: 'create'
    request.should_route :post, "/requests/embedded/create.json", action: 'create', format: 'json'
    request.should_route :get,  "/requests/embedded/create.json", action: 'create', format: 'json'
  end

  with_options(controller: "api/v2/cors") do |request|
    request.should_route :options, "/requests/embedded/create", action: 'preflight'
  end

  describe "#create" do
    before do
      @request.account = @account = accounts(:minimum)
      @request.env['zendesk.brand'] = @account.default_brand
      @account.update_attribute(:domain_blacklist, nil)
    end

    describe 'with valid parameters within the :ticket scope' do
      before do
        @account.stubs(:is_signup_required?).returns(false)
        post :create, params: { ticket: {
          email: 'clara@example.org',
          subject: 'A burning question...',
          description: "Whats 'in' space?",
          submitted_from: 'http://foo.example.com/bar'
        } }
      end

      it('responds with created') { assert_response :created }

      it 'uses the ticket-scoped parameters' do
        ticket = @account.tickets.last
        assert_equal 'clara@example.org', ticket.requester.email
        assert_match %r{Whats 'in' space\?}, ticket.description
      end

      it 'supports CORS' do
        assert_equal '*', @response.headers['Access-Control-Allow-Origin']
        assert_equal 'true', @response.headers['Access-Control-Allow-Credentials']
        assert_equal 'X-Zendesk-API-Warn,X-Zendesk-User-Id,X-Zendesk-User-Session-Expires-At', @response.headers['Access-Control-Expose-Headers']
      end
    end

    describe "repeated more than 5 times an hour" do
      let(:ip_address) { "0.0.0.0" }
      let(:params) do
        {
          email: 'clara@example.org',
          subject: 'A burning question...',
          description: "Whats 'in' space?",
          submitted_from: 'http://foo.example.com/bar'
        }
      end
      before do
        @account.stubs(:is_signup_required?).returns(false)
        Timecop.freeze
        ActionController::TestRequest.any_instance.stubs(remote_ip: ip_address)

        4.times { Prop.throttle!(:anonymous_requests, [accounts(:minimum).id, ip_address]) }

        post :create, params: { ticket: params }
        assert_response :created
      end

      it "raises a throttle exception" do
        post :create, params: { ticket: params }
        assert_response :forbidden
      end

      describe "when anonymous_requests_throttled arturo off" do
        before { @account.stubs(:has_anonymous_requests_throttled?).returns(false) }

        describe "when account is a trial" do
          before { Subscription.any_instance.stubs(:is_trial?).returns(true) }

          it "raises throttle exception" do
            post :create, params: { ticket: params }
            assert_response :forbidden
          end
        end

        describe "when account is not a trial" do
          it "does not raise throttle exception" do
            post :create, params: { ticket: params }
            assert_response :created
          end
        end
      end
    end

    describe 'with a submitted_from URL' do
      before do
        @account.stubs(:is_signup_required?).returns(false)
        post :create, params: { email: 'jens@example.org', subject: 'Hiya', description: "It's me! Jens!", submitted_from: 'http://foo.example.com/bar?token=baz&challenge=quux' }
      end

      it('responds with created') { assert_response :created }
      it 'appends the URL to the description' do
        @account.tickets.last.description.must_include "Submitted from: http://foo.example.com/bar"
      end

      it 'strips sensitive parameters from the referrer' do
        assert_no_match %r{token=baz},      @account.tickets.last.description
        assert_no_match %r{challenge=quux}, @account.tickets.last.description
      end
    end

    describe "blacklist" do
      it "sends out verification mails for new users when signup is required" do
        @account.stubs(is_signup_required?: true)

        assert_difference "SuspendedTicket.count(:all)", 1 do
          post :create, params: { email: 'hans@hansen.com', subject: 'Hello...', description: "Is it me you're looking for?" }
        end

        assert_response :accepted
        assert_responded_with(:json)
        result = ActiveSupport::JSON.decode(@response.body)
        assert result['message'] =~ /Check your email/
      end

      it "blacklists users rather than send our verification emails when domain is blacklisted" do
        account = accounts(:minimum)
        account.update_attribute(:domain_blacklist, '*')

        assert_difference "SuspendedTicket.count(:all)", 1 do
          post :create, params: { email: 'hans@hansen.com', subject: 'Hello...', description: "Is it me you're looking for?" }
        end

        assert_response :accepted
        assert_responded_with(:json)
        result = ActiveSupport::JSON.decode(@response.body)
        assert result['message'] =~ /Requests from your domain are suspended/
      end

      it "does not create a ticket if blacklist matches and whitelist does not match and no signup is required" do
        @account.stubs(:is_signup_required?).returns(false)

        account = accounts(:minimum)
        account.update_attribute(:domain_blacklist, '*')
        account.update_attribute(:domain_whitelist, 'zendesk.com')

        refute_difference "Ticket.count(:all)" do
          post :create, params: { email: 'hans@hansen.com', subject: 'Hello...', description: "Is it me you're looking for?" }
        end

        assert_response :accepted
        assert_responded_with(:json)
        result = ActiveSupport::JSON.decode(@response.body)
        assert result['message'] =~ /Requests from your domain are suspended/
      end

      it "creates a ticket if whitelist matches and no signup is required" do
        @account.stubs(:is_signup_required?).returns(false)

        account = accounts(:minimum)
        account.update_attribute(:domain_blacklist, '*')
        account.update_attribute(:domain_whitelist, 'zendesk.com')

        assert_difference "Ticket.count(:all)" do
          post :create, params: { email: 'hans@zendesk.com', subject: 'Hello...', description: "Is it me you're looking for?" }
        end

        assert_response :created
        assert_responded_with(:json)
        result = ActiveSupport::JSON.decode(@response.body)
        assert result['message'] =~ /Request .+ created/
      end
    end

    describe "when signup is not required" do
      before do
        @account.is_signup_required = false
        @account.save!
        User.any_instance.stubs(:should_clear_locale?).returns(false)
      end

      it "shoulds create new users on the fly" do
        post :create, params: { email: 'lionel@hansen.com', subject: 'I can see it in your eyes', description: 'Is it me youre looking for?' }
        assert_response :created

        assert_responded_with(:json)
        result = ActiveSupport::JSON.decode(@response.body)
        assert result['message'] =~ /Request .+ created/
      end

      it "shoulds create new users on the fly with locale" do
        post :create, params: { email: 'lionel@hansen.com', subject: 'I can see it in your eyes', description: 'Is it me youre looking for?', locale_id: 4 }
        assert_response :created

        assert @account.find_user_by_email('lionel@hansen.com').locale_id == 4
      end

      it "shoulds create new users on the fly with account locale" do
        post :create, params: { email: 'lionel@hansen.com', subject: 'I can see it in your eyes', description: 'Is it me youre looking for?' }
        assert_response :created

        assert @account.find_user_by_email('lionel@hansen.com').locale_id == 1
      end
    end

    describe 'without an email address' do
      before do
        post :create, params: { email: nil, subject: 'Hi', description: 'Hello, there' }
      end
      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      it 'gives an error message' do
        assert_match %r{email}i, @response.body
      end
      before_should 'not create a ticket' do
        @account.tickets.expects(:new).never
        @account.suspended_tickets.expects(:new).never
      end
    end

    describe 'without a description' do
      before do
        post :create, params: { email: 'lionel@example.org', subject: 'Hi' }
      end
      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      it 'gives an error message' do
        assert_match %r{description}i, @response.body
      end
      before_should 'not create a ticket' do
        @account.tickets.expects(:new).never
        @account.suspended_tickets.expects(:new).never
      end
    end

    describe 'without a subject on an account that requires subjects' do
      before do
        @account.stubs(:is_signup_required?).returns(false)
        @account.stubs(:field_subject).returns(stub(is_active?: true, is_required_in_portal?: true))
        post :create, params: { email: 'lionel@example.org', subject: nil, description: 'Hello, there' }
      end
      it('responds with created') { assert_response :created }
      it 'uses the description for the subject' do
        assert_equal 'Hello, there', @account.tickets.last.subject
      end
    end

    describe 'with a priority' do
      before do
        @account.field_priority.update_attributes!(
          title_in_portal: 'Priority',
          is_editable_in_portal: true,
          is_visible_in_portal: true,
          is_required_in_portal: true
        )
        @ticket_params = {
          email: 'jens@example.org',
          subject: 'Hiya',
          description: "It's me! Jens!",
          submitted_from: 'http://foo.example.com/bar',
          priority_id: '2'
        }
      end

      describe 'when priority is editable by end users' do
        before { post :create, params: @ticket_params }
        it 'sets the priority on the ticket' do
          assert_equal '2', @account.tickets.last.priority_id.to_s
        end
      end

      describe 'when priority is not editable by end users' do
        before do
          @account.field_priority.update_attributes!(is_editable_in_portal: false)
          post :create, params: @ticket_params
        end
        it 'does not set the priority on the ticket' do
          assert_not_equal '2', @account.tickets.last.priority_id
        end
      end
    end

    describe 'with some custom fields' do
      before do
        @editable_custom_field1 = FieldText.create!(
          account: accounts(:minimum),
          title: 'Favorite Cheese',
          title_in_portal: 'Favorite Cheese',
          is_active: true,
          is_editable_in_portal: true,
          is_visible_in_portal: true,
          is_required_in_portal: false
        )

        @editable_custom_field2 = FieldTagger.create!(
          account: accounts(:minimum),
          title: 'About',
          title_in_portal: 'About',
          is_active: true,
          is_editable_in_portal: true,
          is_visible_in_portal: true,
          is_required_in_portal: false,
          custom_field_options: [
            CustomFieldOption.new(name: 'Channels::Feedback Tab', value: 'feedback_tab'),
            CustomFieldOption.new(name: 'Channels::Web Portal', value: 'web_portal')
          ]
        )

        @non_editable_custom_field = FieldText.create!(
          account: accounts(:minimum),
          title: 'How you feel about this customer',
          title_in_portal: 'How you feel about this customer',
          is_active: true,
          is_editable_in_portal: false,
          is_visible_in_portal: false,
          is_required_in_portal: false
        )
        @ticket_params = {
          email: 'jens@example.org',
          subject: 'Hiya',
          description: "It's me! Jens!",
          submitted_from: 'http://foo.example.com/bar',
          fields: {
            @editable_custom_field1.id.to_s => 'Cheddar',
            @non_editable_custom_field.id.to_s => "I'm awesome!"
          },
          ticket: {
            fields: {
              @editable_custom_field2.id.to_s => 'feedback_tab'
            }
          }
        }
      end

      describe 'when creating a full ticket' do
        before do
          @account.stubs(:is_signup_required?).returns(false)
          post :create, params: @ticket_params
        end

        it('responds with created') { assert_response :created }

        it 'sets editable fields on the created ticket' do
          assert_equal 'Cheddar', @editable_custom_field1.value(@account.tickets.last)
          assert_equal 'feedback_tab', @editable_custom_field2.value(@account.tickets.last)
        end

        it 'does not set non-editable fields on the created ticket' do
          assert @non_editable_custom_field.value(@account.tickets.last).blank?, "Expected non-editable-custom field value to be blank"
        end

        describe 'with a text custom field that is not editable by end users' do
          before do
            @editable_custom_field1.update_attributes!(is_editable_in_portal: false)
            post :create, params: @ticket_params
          end

          it 'does not set the inactive field on the created ticket' do
            assert_equal "", @editable_custom_field1.value(@account.tickets.last)
          end

          it 'sets the active field on the created ticket' do
            assert_equal 'feedback_tab', @editable_custom_field2.value(@account.tickets.last)
          end
        end

        describe 'with a nested dropdown field that is not editable by end users' do
          before do
            @editable_custom_field2.update_attributes!(is_editable_in_portal: false)
            post :create, params: @ticket_params
          end

          it 'does not set the inactive field on the created ticket' do
            assert_equal "", @editable_custom_field2.value(@account.tickets.last)
          end

          it 'sets the active field on the created ticket' do
            assert_equal 'Cheddar', @editable_custom_field1.value(@account.tickets.last)
          end
        end
      end

      describe 'when trying to create a ticket, but it gets suspended' do
        before do
          @account.stubs(:is_signup_required?).returns(true)
          post :create, params: @ticket_params.merge(set_tags: 'wee woo wot')
          @custom_field_properties = (@account.suspended_tickets.last.properties || {})[:fields] || {}
          @retained_tags           = (@account.suspended_tickets.last.properties || {})[:set_tags]
        end

        it 'sets editable fields on the created ticket' do
          assert_equal 'Cheddar', @custom_field_properties[@editable_custom_field1.id.to_s]
          assert_equal 'feedback_tab', @custom_field_properties[@editable_custom_field2.id.to_s]
        end

        it 'sets tags on the created suspended ticket' do
          assert_equal 'wee woo wot', @retained_tags
        end

        it 'does not set non-editable fields on the created ticket' do
          assert @custom_field_properties[@non_editable_custom_field.id.to_s].blank?, "Expected non-editable-custom field value to be blank"
        end
      end
    end
  end
end
