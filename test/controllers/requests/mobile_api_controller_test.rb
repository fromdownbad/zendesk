require_relative "../../support/test_helper"

SingleCov.covered!

describe Requests::MobileApiController do
  fixtures :accounts, :users, :user_identities, :tickets, :events, :groups, :sequences, :taggings, :ticket_fields, :organizations

  with_options(controller: "requests/mobile_api") do |request|
    request.should_route :post, "/requests/mobile_api/create",      action: 'create'
    request.should_route :post, '/requests/mobile_api/create.json', action: 'create', format: 'json'
  end

  before do
    Arturo.enable_feature! :mobile_dropbox_api
    @account = @request.account = accounts(:minimum)
    @account.update_attribute(:domain_blacklist, nil)
    @account.stubs(:is_signup_required?).returns(false)
    @request.env['zendesk.brand'] = @account.default_brand
  end

  describe "create" do
    before do
      @valid_params = {email: 'hans@hansen.com', subject: 'Hello...', description: 'Is it me youre looking for?'}
    end

    it "creates" do
      set_header('X-Zendesk-Mobile-API', '1.0')
      xhr :post, :create, params: @valid_params

      assert_response :success, response.body
      assert_responded_with(:json)
      result = ActiveSupport::JSON.decode(@response.body)
      assert_match(/Request .+ created/, result['message'])
      assert_nil result['error']
    end

    it "is access denied if I do not provide mobile api header" do
      xhr :post, :create, params: @valid_params

      assert_response :success, response.body
      assert_responded_with(:json)
      result = ActiveSupport::JSON.decode(@response.body)
      assert_match(/Access denied/, result['error'])
    end

    it "is allowed if I use a mobile app User Agent" do
      set_header('X-Zendesk-Mobile-API', '1.0')
      set_header('User-Agent', 'Zendesk SDK for iOS')
      xhr :post, :create, params: @valid_params

      assert_response :success, response.body
      assert_responded_with(:json)
      result = ActiveSupport::JSON.decode(@response.body)
      assert_match(/Request .+ created/, result['message'])
      assert_nil result['error']
    end

    describe "Sunset of the endpoint" do
      it "returns HTTP 404 (NOT FOUND) if 'mobile_dropbox_api' feature is disabled" do
        Arturo.disable_feature! :mobile_dropbox_api

        set_header('X-Zendesk-Mobile-API', '1.0')
        xhr :post, :create, params: @valid_params

        assert_response :not_found, response.body
      end
    end
  end
end
