require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 4

describe Zuora::CreditCardController do
  include ZendeskBillingCore::ZuoraTestHelper

  fixtures :all

  with_options(controller: 'zuora/credit_card') do |request|
    request.should_route :get, '/zuora/credit_card/zendesk_iframe', action: 'zendesk_iframe'
  end

  let(:terms_of_service_link) { I18n.t('txt.admin.views.account.credit_card._form.i_agree_to_the_terms_of_service_link') }
  let(:privacy_policy_link) { I18n.t('txt.admin.views.account.credit_card._form.i_agree_to_the_privacy_policy_link') }
  let(:zopim_terms_and_conditions_link) { I18n.t('txt.admin.views.account.credit_card._form.i_agree_to_the_terms_and_conditions_link') }

  let(:zendesk_and_zopim_terms_and_conditions_text) do
    I18n.t(
      'txt.admin.views.account.credit_card._form.i_agree_to_zendesk_and_zopim_terms_and_conditions_text',
      terms_of_service_link: terms_of_service_link,
      privacy_policy_link: privacy_policy_link,
      zopim_terms_and_conditions_link: zopim_terms_and_conditions_link
    )
  end

  let(:zendesk_terms_and_conditions_text) do
    I18n.t(
      'txt.admin.views.account.credit_card._form.agree_to_the_terms_and_conditions_text',
      terms_of_service_link: terms_of_service_link,
      privacy_policy_link: privacy_policy_link
    )
  end

  before do
    ZendeskBillingCore::Zuora::Page.any_instance.stubs(:hosted_page_id).returns(stub(page_id: 'test'))
    @account = accounts(:support)
    @account.settings.security_headers = true
    @account.settings.save!
    @request.account = @account
    Subscription.any_instance.stubs(:currency_type).returns(CurrencyType.USD)
  end

  describe '#zendesk_iframe' do
    before do
      @customer_account = accounts(:trial)
      @params = {
        method: 'upgrade_from_trial',
        subscription: {
          subdomain: @customer_account.subdomain,
          account_id: @customer_account.id,
          account_url: @customer_account.url
        },
        credit_card_encrypted_form: {
          payload: Base64.urlsafe_encode64('encrypted-subdomain'),
          nonce:   Base64.urlsafe_encode64('random-iv-generated-by-encryption')
        }
      }
    end

    before do
      ZBC::Zendesk::HostedPage::Decrypt.expects(:call).with(
        shared_key:      ZBC::Zuora::Configuration.page['api_security_key'],
        iv:              'random-iv-generated-by-encryption',
        encrypted:       'encrypted-subdomain',
        given_subdomain: 'trial'
      ).returns(decrypt_stub)
    end

    let(:decrypt_stub) { stub(valid?: true) }

    it 'renders the iframe successfully using the zuora_hosted_page template' do
      get :zendesk_iframe, params: @params
      assert_response :success
      assert_template 'zuora_hosted_page'
    end

    describe '#instrument_due_to_referrer' do
      describe 'nil referrer' do
        it 'sends the metric to datadog' do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(
            'rendered_with_subdomain_mismatch',
            tags: ['referrer:', 'subdomain:trial']
          )
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(
            'rendered',
            tags: ['subdomain:trial']
          )
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.Zuora::CreditCardController.zendesk_iframe', anything).at_least(1)
          get :zendesk_iframe, params: @params
        end
      end

      describe 'referrer mismatch' do
        before do
          @request.env['HTTP_REFERER'] = 'https://wrong-subdomain.zendesk-test.com'
        end

        it 'sends the metric to datadog' do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(
            'rendered_with_subdomain_mismatch',
            tags: ['referrer:wrong-subdomain', 'subdomain:trial']
          )

          Zendesk::StatsD::Client.any_instance.expects(:increment).with(
            'rendered',
            tags: ['subdomain:trial']
          )
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.Zuora::CreditCardController.zendesk_iframe', anything).at_least(1)
          get :zendesk_iframe, params: @params
        end
      end

      describe 'subdomain matches referrer' do
        before do
          @request.env['HTTP_REFERER'] = 'https://trial.zendesk-test.com'
        end

        it 'does not send the metric to datadog' do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(
            'rendered',
            tags: ['subdomain:trial']
          )
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.Zuora::CreditCardController.zendesk_iframe', anything).at_least(1)
          get :zendesk_iframe, params: @params
        end
      end
    end

    describe '#throttle_iframe_load' do
      let(:throttle_threshold) { 5 }

      before do
        Zendesk::StatsD::Client.any_instance.expects(:increment).
          with('rendered_with_subdomain_mismatch', tags: ['referrer:', 'subdomain:trial']).
          times(throttle_threshold + 1)
      end

      describe 'the account is flagged as support_risky' do
        before do
          Account.any_instance.stubs(:support_risky?).returns(true)

          # Only count the rendered when it actually happens, not when throttled
          Zendesk::StatsD::Client.any_instance.expects(:increment).
            with('rendered', tags: ['subdomain:trial']).
            times(throttle_threshold)
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.Zuora::CreditCardController.zendesk_iframe', anything).at_least(1)
        end

        it 'throttles the no. of times you can render the iframe' do
          throttle_threshold.times do
            get :zendesk_iframe, params: @params
            assert_response :success
          end

          # Send datadog a metric about rate being limited for the iframe
          Zendesk::StatsD::Client.any_instance.expects(:increment).
            with('rate_limited', tags: ['subdomain:trial']).once
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.Zuora::CreditCardController.zendesk_iframe', anything).at_least(1)

          # Forbidden after `throttle_threshold` attempts
          get :zendesk_iframe, params: @params
          assert_response :forbidden
        end
      end

      describe 'the account is not flagged as support risky' do
        before do
          Account.any_instance.stubs(:support_risky?).returns(false)

          Zendesk::StatsD::Client.any_instance.expects(:increment).
            with('rendered', tags: ['subdomain:trial']).
            times(throttle_threshold + 1)
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.Zuora::CreditCardController.zendesk_iframe', anything).at_least(1)
        end

        it 'does not throttles the no. of times you can render the iframe' do
          (throttle_threshold + 1).times do
            get :zendesk_iframe, params: @params
            assert_response :success
          end
        end
      end
    end

    describe '#verify_encrypted_subdomain' do
      describe 'subdomain matches the encrypted one' do
        let(:decrypt_stub) { stub(valid?: true) }

        it 'is successful' do
          get :zendesk_iframe, params: @params
          assert_response :success
        end
      end

      describe 'subdomain does not match the encrypted one' do
        let(:decrypt_stub) { stub(valid?: false) }

        it 'is forbidden' do
          get :zendesk_iframe, params: @params
          assert_response :forbidden
        end
      end
    end
  end
end
