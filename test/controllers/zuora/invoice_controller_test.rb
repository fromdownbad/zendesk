require_relative "../../support/test_helper"
require 'zendesk_billing_core/zuora/client'

SingleCov.covered! uncovered: 6

describe Zuora::InvoiceController do
  with_options(controller: 'zuora/invoice') do |request|
    request.should_route :get, '/zuora/invoice/invoice_items.json', action: 'invoice_items', format: 'json'
    request.should_route :get, '/zuora/invoice/invoices.json',      action: 'invoices',      format: 'json'
  end

  before do
    Subscription.any_instance.stubs(:zuora_subscription).returns(stub(zuora_account_id: "qwerty"))
    @request.account = accounts(:minimum)
    @session = stub
  end

  describe "as an admin" do
    before do
      login('minimum_admin')
      ZendeskBillingCore::Zuora::Client.any_instance.stubs(:session).returns(@session)
    end

    describe "#invoices" do
      before do
        accept :json
        @finder = stub(find!: [])
        @session.expects(:invoice).returns(@finder)
        get :invoices, format: :json
      end

      it('responds with success') { assert_response :success }
    end

    describe "#invoice_items" do
      before do
        accept :json
        @finder = stub
        @finder.expects(:find!).with(invoice_id: 12345)
        @session.expects(:invoice_item).returns(@finder)
        get :invoice_items, params: { invoice_id: 12345, format: :json }
      end

      it('responds with success') { assert_response :success }
    end
  end

  describe "as an agent" do
    before do
      login('minimum_agent')
    end

    describe "#invoices" do
      before do
        get :invoices, format: :json
      end

      it('responds with forbidden') { assert_response :forbidden }

      before_should "never construct a client session" do
        ZendeskBillingCore::Zuora::Client.any_instance.expects(:session).never
      end
    end
  end

  describe "as an end user" do
    before do
      login('minimum_agent')
    end

    describe "#invoices" do
      before do
        get :invoices, format: :json
      end

      it('responds with forbidden') { assert_response :forbidden }

      before_should "never construct a client instance" do
        ZendeskBillingCore::Zuora::Client.any_instance.expects(:session).never
      end
    end
  end
end
