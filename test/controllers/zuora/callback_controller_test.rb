require_relative "../../support/test_helper"
require 'zendesk_billing_core/zuora/client'

SingleCov.covered! uncovered: 4

describe Zuora::CallbackController do
  fixtures :all

  with_options(controller: "zuora/callback") do |request|
    request.should_route :get, "/zuora/callback/ping",        action: "ping"
    request.should_route :get, "/zuora/callback/callback",    action: "callback"
    request.should_route :put, "/zuora/callback/inline_sync", action: "inline_sync"
  end

  let(:zuora_account) do
    stub('zuora-account')
  end

  before do
    @account = accounts(:minimum)
    ZBC::Zuora::Client.any_instance.stubs(
      zendesk_account: @account,
      zuora_account:   zuora_account
    )
    ZBC::Billing::Zuora::Ping.stubs(:request)
  end

  describe "#ping" do
    let(:remote_synchronizer) do
      stub('remote-synchronizer', synchronize!: true)
    end

    let(:statsd_client) { stub_for_statsd }

    before do
      Zendesk::StatsD::Client.stubs(:new).
        returns(statsd_client)
      ZBC::Zuora::RemoteSynchronizerClient.stubs(:new).
        with('abcd1234', 'minimum').
        returns(remote_synchronizer)
    end

    it 'updates the statsd counter for the action' do
      statsd_client.expects(:increment).with('sli.zuora_sync_request', anything)
      get :ping, params: { format: 'json', zid: 'abcd1234' }
    end

    describe 'when the `zid` parameter is present' do
      let(:params) { { format: 'json', zid: 'abcd1234' } }

      it 'responds with a 200 status code' do
        get :ping, params: params
        assert_response :success
      end

      describe 'when account is enabled for billing primary sync' do
        before do
          Arturo.enable_feature!(:billing_primary_sync)
        end

        it 'initiates a multi-product sync' do
          ZBC::Billing::Zuora::Ping.expects(:request).
            with('abcd1234', multi_product: true).once
          remote_synchronizer.expects(:synchronize!).never
          get :ping, params: params
        end
      end

      describe 'when account is enabled for billing primary sync' do
        before do
          Arturo.disable_feature!(:billing_primary_sync)
        end

        describe 'when account is multi-product participant' do
          before do
            @account.stubs(:billing_multi_product_participant?).returns(true)
          end

          describe 'when the account is self-service' do
            before do
              ZBC::Zuora::Client.any_instance.stubs(:get_sales_model).
                returns(ZBC::States::SalesModel::SELF_SERVICE)
            end

            describe 'and the account is not originating from salesforce' do
              let(:zuora_subscription) { stub }

              before do
                ZBC::Zuora::Subscription.stubs(:find_by_zuora_account_id).
                  with('abcd1234').returns(zuora_subscription)
              end

              it 'initiates a multi-product sync' do
                ZBC::Billing::Zuora::Ping.expects(:request).
                  with('abcd1234', multi_product: true).once
                remote_synchronizer.expects(:synchronize!).never
                get :ping, params: params
              end
            end

            describe 'and the account is originating from salesforce' do
              before do
                ZBC::Zuora::Subscription.stubs(:find_by_zuora_account_id).
                  with('abcd1234').returns(nil)
              end

              it 'initiates a pod-aware sync' do
                ZBC::Billing::Zuora::Ping.expects(:request).never
                remote_synchronizer.expects(:synchronize!).once
                get :ping, params: params
              end
            end
          end

          describe 'when the account is not self-service' do
            before do
              ZBC::Zuora::Client.any_instance.stubs(:get_sales_model).
                returns(ZBC::States::SalesModel::ASSISTED)
            end

            it 'initiates a pod-aware sync' do
              ZBC::Billing::Zuora::Ping.expects(:request).never
              remote_synchronizer.expects(:synchronize!).once
              get :ping, params: params
            end
          end
        end

        describe 'when account is not a multi-product participant' do
          before do
            @account.stubs(:billing_multi_product_participant?).returns(false)
          end

          describe 'when the account is self-service' do
            before do
              ZBC::Zuora::Client.any_instance.stubs(:get_sales_model).
                returns(ZBC::States::SalesModel::SELF_SERVICE)
            end

            describe 'and the account is not originating from salesforce' do
              let(:zuora_subscription) { stub }

              before do
                ZBC::Zuora::Subscription.stubs(:find_by_zuora_account_id).
                  with('abcd1234').returns(zuora_subscription)
              end

              describe 'and the `:billing_sync_all_self_service Arturo` is enabled' do
                before do
                  Arturo.enable_feature!(:billing_sync_all_self_service)
                end

                it 'initiates a multi-product sync' do
                  ZBC::Billing::Zuora::Ping.expects(:request).
                    with('abcd1234', classic: true).once
                  remote_synchronizer.expects(:synchronize!).never
                  get :ping, params: params
                end
              end

              describe 'and the `:billing_sync_all_self_service` Arturo is disabled' do
                before do
                  Arturo.disable_feature!(:billing_sync_all_self_service)
                end

                it 'initiates a pod-aware sync' do
                  ZBC::Billing::Zuora::Ping.expects(:request).never
                  remote_synchronizer.expects(:synchronize!).once
                  get :ping, params: params
                end
              end
            end
          end

          describe 'when the account is not self-service' do
            before do
              ZBC::Zuora::Client.any_instance.stubs(:get_sales_model).
                returns(ZBC::States::SalesModel::ASSISTED)
            end

            it 'initiates a pod-aware sync' do
              ZBC::Billing::Zuora::Ping.expects(:request).never
              remote_synchronizer.expects(:synchronize!).once
              get :ping, params: params
            end
          end
        end
      end

      describe 'with an invalid Zuora account configuration' do
        before do
          ZBC::Zuora::Client.any_instance.expects(:zendesk_account).
            raises(ZBC::Zuora::Errors::InvalidConfiguration)
        end

        it 'does not synchronize the account' do
          ZBC::Zuora::RemoteSynchronizerClient.expects(:new).never
          get :ping, params: params
        end

        it 'responds with a 404 status code' do
          get :ping, params: params
          assert_response :missing
        end
      end
    end

    describe 'when the `zid` parameter is not present' do
      let(:params) { {} }

      it 'does not synchronize the account' do
        ZBC::Zuora::RemoteSynchronizerClient.expects(:new).never
        get :ping, params: params
        assert_response :bad_request
      end
    end
  end

  describe '#inline_sync' do
    it 'directly calls the Synchronizer with the Zuora acct id' do
      ZBC::Zuora::Synchronizer.expects(:synchronize!).once.with('abcd1234')
      put :inline_sync, params: { format: 'json', zid: 'abcd1234' }
    end

    describe 'when zuora acct id is not passed' do
      it 'responds with bad_request status' do
        assert_raises(ActionController::ParameterMissing) do
          put :inline_sync, format: 'json'
          assert_response :bad_request
        end
      end
    end

    describe 'when sync succeeds' do
      before do
        ZBC::Zuora::Synchronizer.stubs(:synchronize!).returns(true)
      end

      it 'responds with ok status' do
        put :inline_sync, params: { format: 'json', zid: 'abcd1234' }
        assert_response :ok
      end
    end

    describe 'when sync fails' do
      before do
        ZBC::Zuora::Synchronizer.stubs(:synchronize!).returns(false)
      end

      it 'responds with not_modified status' do
        put :inline_sync, params: { format: 'json', zid: 'abcd1234' }
        assert_response :not_modified
      end
    end

    describe 'when in production' do
      before do
        Rails.env.stubs(:production?).returns(true)
      end

      it 'is forbidden' do
        put :inline_sync, params: { format: 'json', zid: 'abcd1234' }
        assert_response :forbidden
      end
    end
  end

  describe "#callback" do
    before do
      Timecop.freeze(Time.new(2015, 07, 15, 7, 0, 0, '+00:00').utc)
      @params = {
        id: '2c92c0f8385d9f350138775869543830',
        tenantId: 'XXXXX',
        timestamp: '1436943600000',
        token: '070f09bcdbe74ec71a801d5a3d78f41f',
        responseSignature: 'MzYxOTZkNWM3MzkxYTNkOTc1MTNiNjIyNzUyOThjNzk=',
        success: 'true',
        refId: '2c92c0f939072aca01390881697018e7',
        field_passthrough1: 'trial_upgrade',
        field_passthrough2: 'http://minimum.zendesk-test.com',
        field_passthrough3: 'billing_cycle_type:1,max_agents:5,plan_type:3,promo_code:COUPONCODE',
        field_passthrough4: '',
        field_passthrough5: '',
        signature: 'NTNiYmNlOTYwOWFhMDE5MGJhNzJiYjk0ZDIwMDcyNzA='
      }
      ZBC::Zuora::CreditCardFormCalloutHandler.any_instance.stubs(:verify_timestamp!)
      ZBC::Zuora::CreditCardFormCalloutHandler.any_instance.stubs(:verify_signature!)
    end

    describe "Zuora response handler" do
      before do
        get :callback, params: @params
        assert_response :success
      end

      it "creates a Zuora response handler with the correct information" do
        handler = assigns(:credit_card_form_callout_handler)
        expected = {
          billing_cycle_type: '1',
          max_agents: '5',
          plan_type: '3',
          promo_code: 'COUPONCODE',
          payment_method_reference_id: '2c92c0f939072aca01390881697018e7'
        }
        actual = handler.subscription_details
        assert handler.valid?
        assert_equal expected, actual
      end
    end
  end
end
