require_relative '../support/test_helper'
require 'zendesk/account_cdn'

SingleCov.covered!

describe LotusBootstrapController do
  fixtures :all

  let(:account) do
    account = accounts(:minimum)
    account.settings.ssl_enabled = true
    account.save!
    account
  end

  let(:current_user) { @controller.send(:current_user) }
  let(:products_response) { [] }

  let(:manifest_data) do
    {
      'createdAt' => '',
      'css' => [],
      'js' => [
        {
          'name' => 'app.js',
          'path' => '/agent/assets/lr/app.d07c795669440a45.js'
        }
      ],
      'sha' => '0e795391b7f859e99f6b32e492ff6e689e227565',
      'version' => 1234
    }
  end
  let(:manifest_sha) { manifest_data['sha'] }
  let(:manifest_react_js) { manifest_data['js'][0] }
  let(:asset_manifest) { Lotus::AssetManifest.new(account, manifest_data) }
  let(:dev_asset_manifest) { Lotus::AssetManifest.new(account, manifest_data, develop: true) }

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:products).returns(products_response)
    Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])
    Subscription.any_instance.stubs(:voice_account).returns(nil)
    Subscription.any_instance.stubs(:guide_preview).returns(nil)
    Subscription.any_instance.stubs(:suite_allowed?).returns(anything)
    Access::ExternalPermissions::Permissions.any_instance.stubs(:allowed_scopes).returns(["user_fields:manage"])

    @request.account = account
    use_ssl

    Lotus::ManifestManager.any_instance.stubs(:find).returns(asset_manifest)
  end

  with_options(controller: 'lotus_bootstrap') do |request|
    request.should_route :get, '/agent', action: 'index'
    request.should_route :get, '/agent/tickets/2', action: 'ticket', ticket_id: 2
    request.should_route :get, '/agent/catch/all', action: 'show', route: 'catch/all'
  end

  as_an_anonymous_user do
    describe '#index' do
      it 'renders the login template' do
        get :index
        assert_template 'login'
      end
    end
  end

  as_an_end_user do
    describe '#index' do
      it 'is forbidden' do
        get :index
        assert_response :forbidden
      end
    end

    describe '#ticket' do
      it 'redirects end users to the end user portal' do
        @ticket = tickets(:minimum_1)
        get :ticket, params: { ticket_id: @ticket }
        assert_response :redirect
        assert_match(/requests\/#{@ticket.nice_id}/, @response['Location'])
      end
    end
  end

  as_an_agent do
    describe 'with Arturo access' do
      describe '#index' do
        it 'is allowed' do
          get :index
          assert_response :success, @response.code
        end

        it 'defers script tags' do
          get :index
          assert_includes @response.body, 'defer="defer"'
        end

        it 'allows mobile requests' do
          get :index, format: :mobile_v2
          assert_response :success, @response.code
        end
      end

      describe 'Arturo feature :redirect_from_lotus_according_agent_entitlements' do
        before do
          Arturo.enable_feature! :redirect_from_lotus_according_agent_entitlements
        end

        describe 'Account has support and Chat, agent is not entitled to support but is entitled chat' do
          let(:products_response) do
            [
              Zendesk::Accounts::Product.new(FactoryBot.build(:chat_trial_product)),
              Zendesk::Accounts::Product.new(FactoryBot.build(:support_trial_product))
            ]
          end

          before do
            stub_request(:get, "metropolis:8888/api/services/staff/#{current_user.id}/entitlements").
              to_return(body: {entitlements: {chat: 'agent'}}.to_json, headers: { content_type: 'application/json; charset=utf-8' })
          end

          it 'redirects to chat' do
            get :index
            assert_redirected_to '/chat'
          end
        end

        describe 'Account has only Chat, agent is entitled to chat' do
          let(:products_response) do
            [Zendesk::Accounts::Product.new(FactoryBot.build(:chat_trial_product))]
          end
          before do
            stub_request(:get, "metropolis:8888/api/services/staff/#{current_user.id}/entitlements").
              to_return(body: {entitlements: {chat: 'agent'}}.to_json, headers: { content_type: 'application/json; charset=utf-8' })
          end

          it 'redirects to chat' do
            get :index
            assert_redirected_to '/chat'
          end
        end
      end

      describe 'cdn hosts in the settings api' do
        let(:expected) { '{"name":"default","url":"https://edgecast.example.com"},{"name":"cloudfront","url":"https://cloudfront.example.com"}' }

        it 'includes https cdn hosts' do
          get :index
          assert_response :success
          assert_includes response.body, expected
        end
      end

      describe '#ticket' do
        before do
          @ticket = tickets(:minimum_1)
          @preload_url = Regexp.escape("/api/v2/tickets/#{@ticket.nice_id}?include=")
        end

        it 'resolves' do
          get :ticket, params: { ticket_id: @ticket.nice_id }
          assert_response :success
          assert_template layout: 'lotus_bootstrap'
          ticket = assigns(:ticket)
          assert_equal @ticket.nice_id, ticket.nice_id, 'Finds the ticket by id'
        end

        it 'includes preload-id' do
          get :ticket, params: { ticket_id: @ticket.nice_id }
          assert_match /data-preload-id=\"#{@preload_url}/, @response.body
        end

        it 'includes preload-secondary-id' do
          get :ticket, params: { ticket_id: @ticket.nice_id }
          assert_match /data-preload-secondary-id=\"#{@preload_url}/, @response.body
        end

        describe 'given an agent without access to the ticket' do
          before do
            @user.stubs(:can?).returns(true)
            @user.stubs(:can?).with(:view, @ticket).returns(false)
          end

          it 'is denied access' do
            get :ticket, params: { ticket_id: @ticket.nice_id }
            assert_response :forbidden
          end

          it 'does not add any recent tickets' do
            get :ticket, params: { ticket_id: @ticket.nice_id }
            assert_empty(@controller.send(:recent_ticket_manager).recent_tickets)
          end
        end

        it 'allows mobile requests' do
          get :ticket, params: { ticket_id: @ticket.nice_id, format: :mobile_v2 }
          assert_response :success
        end

        describe 'slas' do
          describe 'when the account has the SLA feature' do
            before do
              Account.any_instance.stubs(:has_service_level_agreements?).returns(true)
              policy = stub(policy_metrics: stub(with_priority: []))
              Ticket.any_instance.stubs(sla_policy: policy)
              get :ticket, params: { ticket_id: @ticket.nice_id }
            end

            it 'includes slas in the preload secondary id' do
              assert_match /data-preload-secondary-id="#{@preload_url}[\w,]+slas"/, @response.body
            end

            it 'does not include slas in the preload id' do
              assert_no_match /data-preload-id="#{@preload_url}[\w,]+slas"/, @response.body
            end

            it 'sideloads SLA info' do
              match = @response.body.match(/<script.*?#{@preload_url}.*?>(.*?)<\/script>/m)
              json  = JSON.parse(match[1])['ticket']

              assert json.key?('slas')
              assert json['slas'].key?('policy_metrics')
            end
          end

          describe 'when the account does not have the SLA feature' do
            before do
              account.stubs(:has_service_level_agreements?).returns(false)
              get :ticket, params: { ticket_id: @ticket.nice_id }
            end

            it 'include slas in the preload secondary id' do
              assert_match /data-preload-secondary-id="#{@preload_url}[\w,]+slas"/, @response.body
            end

            it 'does not include slas in the preload id' do
              assert_no_match /data-preload-id="#{@preload_url}[\w,]+slas"/, @response.body
            end

            it 'does not sideload SLA info' do
              match = @response.body.match(/<script.*?#{@preload_url}.*?>(.*?)<\/script>/m)
              json  = JSON.parse(match[1])['ticket']

              refute json.key?('slas')
            end
          end
        end

        it 'handles 404s' do
          @controller.stubs(:current_brand).returns(account.brands.first)
          get :ticket, params: { ticket_id: 9999999 }
          assert_response :not_found
        end

        describe 'agent cannot view private content' do
          describe 'help_center_agent_requests Arturo enabled' do
            before do
              Arturo.enable_feature!(:help_center_agent_requests)
            end

            it 'redirects to end user portal' do
              User.any_instance.stubs(:can?).returns(true)
              User.any_instance.expects(:can?).with(:view_private_content, instance_of(Ticket)).returns(false)
              get :ticket, params: { ticket_id: @ticket.nice_id }
              assert_response :redirect
              assert_match(/requests\/#{@ticket.nice_id}/, @response['Location'])
            end
          end

          describe 'help_center_agent_requests Arturo disabled' do
            before do
              Arturo.disable_feature!(:help_center_agent_requests)
            end

            it 'forbids access' do
              User.any_instance.stubs(:can?).returns(true)
              User.any_instance.expects(:can?).with(:view_private_content, instance_of(Ticket)).returns(false)
              get :ticket, params: { ticket_id: @ticket.nice_id }
              assert_response :forbidden
            end
          end
        end

        describe 'deeplinking to splash screen supported' do
          let(:has_ios_mobile_deeplinking) { false }
          let(:is_ios) { false }
          let(:has_android_mobile_deeplinking) { false }
          let(:is_android) { false }
          let(:has_windows_mobile_deeplinking) { false }
          let(:is_windows_phone) { false }
          before do
            @controller.stubs(:show_deeplinking_splash?).returns(true)
            Account.any_instance.stubs(:has_ios_mobile_deeplinking?).returns(has_ios_mobile_deeplinking)
            Account.any_instance.stubs(:has_android_mobile_deeplinking?).returns(has_android_mobile_deeplinking)
            Account.any_instance.stubs(:has_windows_mobile_deeplinking?).returns(has_windows_mobile_deeplinking)
            @controller.stubs(:is_ios?).returns(is_ios)
            @controller.stubs(:is_android?).returns(is_android)
            @controller.stubs(:is_windows_phone?).returns(is_windows_phone)
          end

          describe 'IOS mobile deeplinking supported' do
            let(:has_ios_mobile_deeplinking) { true }

            describe 'caller is on IOS' do
              let(:is_ios) { true }

              it 'does not authorize agent' do
                @controller.expects(:authorize_agent_with_return_to).never
                get :ticket, params: { ticket_id: @ticket.nice_id }
              end
            end

            describe 'caller is not on IOS' do
              it 'authorizes agent' do
                @controller.expects(:authorize_agent_with_return_to)
                get :ticket, params: { ticket_id: @ticket.nice_id }
              end
            end
          end

          describe 'Android mobile deeplinking supported' do
            let(:has_android_mobile_deeplinking) { true }

            describe 'caller is on Android' do
              let(:is_android) { true }

              it 'does not authorize agent' do
                @controller.expects(:authorize_agent_with_return_to).never
                get :ticket, params: { ticket_id: @ticket.nice_id }
              end
            end

            describe 'caller is not on Android' do
              it 'authorizes agent' do
                @controller.expects(:authorize_agent_with_return_to)
                get :ticket, params: { ticket_id: @ticket.nice_id }
              end
            end
          end

          describe 'Windows Mobile mobile deeplinking supported' do
            let(:has_windows_mobile_deeplinking) { true }

            describe 'caller is on Windows Mobile' do
              let(:is_windows_phone) { true }

              it 'does not authorize agent' do
                @controller.expects(:authorize_agent_with_return_to).never
                get :ticket, params: { ticket_id: @ticket.nice_id }
              end
            end

            describe 'caller is not on Windows Mobile' do
              it 'authorizes agent' do
                @controller.expects(:authorize_agent_with_return_to)
                get :ticket, params: { ticket_id: @ticket.nice_id }
              end
            end
          end
        end

        describe 'agent cannot view ticket' do
          it 'forbids access; does not double-render' do
            Arturo.enable_feature!(:help_center_agent_requests)
            User.any_instance.stubs(:can?).returns(true)
            User.any_instance.stubs(:can?).with(:view, instance_of(Ticket)).returns(false)
            User.any_instance.expects(:can?).with(:view_private_content, instance_of(Ticket)).never
            get :ticket, params: { ticket_id: @ticket.nice_id }
            assert_response :forbidden
          end
        end
      end

      describe '#talk' do
        describe 'with the necessary arturo bits on' do
          before do
            Arturo.enable_feature! :talk_product_tray
            get :talk
          end

          it 'responds successfully' do
            assert_response :success
          end

          it 'skips apps in the output' do
            assert_no_match /installed\.js/, @response.body
          end
        end

        describe 'without the necessary arturo bits on' do
          before do
            Arturo.disable_feature! :talk_product_tray
          end

          it 'forbids access' do
            get :talk
            assert_response :forbidden
          end
        end
      end

      describe '#chat' do
        describe 'with the necessary arturo bits on' do
          before do
            Arturo.enable_feature! :chat_product_tray
          end

          describe 'without a zopim integration' do
            as_an_admin do
              describe 'with the app enabled' do
                before do
                  ZopimIntegration.any_instance.stubs(:app_enabled?).returns(true)
                end

                describe 'with voltron_chat disabled' do
                  before do
                    Account.any_instance.stubs(has_voltron_chat?: false)
                    get :chat
                  end

                  it 'responds with success' do
                    assert_response :success
                  end
                end

                describe 'with voltron_chat enabled' do
                  before do
                    Account.any_instance.stubs(has_voltron_chat?: true)
                    get :chat
                  end

                  it 'redirects to new Zopim start page' do
                    assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
                  end
                end
              end

              describe 'without the app enabled' do
                before do
                  ZopimIntegration.any_instance.stubs(:app_enabled?).returns(false)
                end

                describe 'with voltron_chat disabled' do
                  before do
                    Account.any_instance.stubs(has_voltron_chat?: false)
                    get :chat
                  end

                  it 'redirects to ZopimChatStartController' do
                    assert_redirected_to 'https://minimum.zendesk-test.com/zendeskchat/start'
                  end
                end

                describe 'with voltron_chat enabled' do
                  before do
                    Account.any_instance.stubs(has_voltron_chat?: true)
                    get :chat
                  end

                  it 'redirects to new Zopim start page' do
                    assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
                  end
                end
              end
            end
          end

          describe 'with a zopim integration' do
            let(:zopim_integration) { mock('zopim_integration') }

            before do
              Account.any_instance.stubs(:zopim_integration).returns(zopim_integration)
            end

            describe 'with the app enabled' do
              before do
                zopim_integration.stubs(:app_enabled?).returns(true)
              end

              describe 'with voltron_chat disabled' do
                before do
                  Account.any_instance.stubs(has_voltron_chat?: false)
                  get :chat
                end

                it 'responds with success' do
                  assert_response :success
                end
              end

              describe 'with voltron_chat enabled' do
                before do
                  Account.any_instance.stubs(has_voltron_chat?: true)
                  get :chat
                end

                it 'redirects to new Zopim start page' do
                  assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
                end
              end
            end

            describe 'with the app disabled' do
              before do
                zopim_integration.stubs(:app_installed?).returns(true)
                zopim_integration.stubs(:app_enabled?).returns(false)
              end

              describe 'with voltron_chat disabled' do
                before do
                  Account.any_instance.stubs(has_voltron_chat?: false)
                  get :chat
                end

                it 'redirects to ZopimChatStartController' do
                  assert_redirected_to 'https://minimum.zendesk-test.com/zendeskchat/start'
                end
              end

              describe 'with voltron_chat enabled' do
                before do
                  Account.any_instance.stubs(has_voltron_chat?: true)
                  get :chat
                end

                it 'redirects to new Zopim start page' do
                  assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
                end
              end
            end

            describe 'without the app installed' do
              before do
                zopim_integration.stubs(:app_enabled?).returns(false)
              end

              describe 'with voltron_chat disabled' do
                before do
                  Account.any_instance.stubs(has_voltron_chat?: false)
                  get :chat
                end

                it 'redirects to ZopimChatStartController' do
                  assert_redirected_to 'https://minimum.zendesk-test.com/zendeskchat/start'
                end
              end

              describe 'with voltron_chat enabled' do
                before do
                  Account.any_instance.stubs(has_voltron_chat?: true)
                  get :chat
                end

                it 'redirects to new Zopim start page' do
                  assert_redirected_to ZopimChatRedirectionMixin::NEW_ZOPIM_START_PAGE
                end
              end
            end
          end
        end

        describe 'without the necessary arturo bits on' do
          before { Arturo.disable_feature! :chat_product_tray }

          it 'forbids access' do
            get :chat
            assert_response :forbidden
          end
        end
      end

      describe 'url validation' do
        it 'permanently redirects non-SSL requests to SSL' do
          @request.stubs(:scheme).returns('http')
          get :index
          assert_redirected_to 'https://minimum.zendesk-test.com/agent'
        end

        it 'permanently redirects host-mapped requests to the non-host-mapped version' do
          request.stubs(:host_with_port).returns('somewhere.else.com')
          get :index, params: { challenge: 'abc123' }
          assert_redirected_to 'https://minimum.zendesk-test.com/agent?challenge=abc123'
        end
      end

      describe 'wildcard routing' do
        let(:ticket) { tickets(:minimum_1) }

        it 'responds successfully' do
          get :show, params: { route: "tickets/#{ticket.id}" }
          assert_response :success
        end
      end

      describe 'static asset domain' do
        before do
          ENV['ZENDESK_STATIC_ASSETS_DOMAIN'] = 'https://foo.com'
        end

        it 'works' do
          get :index
          assert_response :success
        end

        it 'sets asset-host as defined in ZENDESK_STATIC_ASSETS_DOMAIN' do
          get :index
          assert_includes @response.body, '<meta name="asset-host" content="https://foo.com">'
        end
      end

      describe 'CDN fallback' do
        before do
          @controller.config[:asset_host] = Zendesk::AccountCdn.asset_host
          ENV['ZENDESK_STATIC_ASSETS_DOMAIN'] = 'https://edgecast.example.com'
          ENV['ZENDESK_FALLBACK_STATIC_ASSETS_DOMAIN'] = 'https://cloudfront.example.com'
        end

        it 'resolves URLs to default CDN if fallback_cdn not set' do
          get :index
          assert_includes @response.body, "<script src=\"https://edgecast.example.com"
          refute_includes @response.body, "<script src=\"https://cloudfront.example.com"
        end

        it 'returns a fallback CDN' do
          get :index
          assert_includes @response.body, '<input type="hidden" name="fallback_cdn" id="fallback_cdn" value="cloudfront"'
        end

        describe 'with translation api fallback arturo enabled' do
          before do
            # Required Arturos for `translations_fallback_rosetta_api`
            Arturo.enable_feature! :translations_from_default_cdn
            Arturo.enable_feature! :translations_from_cdn
            Arturo.enable_feature! :translations_fallback_rosetta_api
          end

          it 'initial page load' do
            get :index
            assert_includes @response.body, "<script src=\"https://edgecast.example.com"
            refute_includes @response.body, "<script src=\"https://cloudfront.example.com"
            assert_includes @response.body, 'onerror="Zendesk.onScriptLoadError(this, \'app.js\')"'
            assert_includes @response.body, 'onerror="Zendesk.onScriptLoadError(this, \'translations\')"'

            assert_includes @response.body, '<input type="hidden" name="translations_asset_fallback_to_api" id="translations_asset_fallback_to_api"'
          end

          describe 'fallback_cdn set to alternate CDN' do
            describe 'translation asset fails' do
              it 'resolves URLs to alternate CDN if fallback_cdn set' do
                post :index, params: { fallback_cdn: 'cloudfront', fallback_asset_name: 'translations' }
                refute_includes @response.body, "<script src=\"https://edgecast.example.com"
                assert_includes @response.body, "<script src=\"https://cloudfront.example.com"
                assert_includes @response.body, "onerror=\"Zd.onFatalError(\'cdn_failure\', \'https://cloudfront.example.com#{manifest_react_js['path']}\', \'#{manifest_react_js['name']}\')\""
                assert_includes @response.body, 'onerror="Zendesk.onScriptLoadError(this, \'translations\')"'

                assert_includes @response.body, '<input type="hidden" name="translations_asset_fallback_to_api" id="translations_asset_fallback_to_api" value="true"'
              end

              it 'return a fallback CDN if translation falling back' do
                post :index, params: { fallback_cdn: 'cloudfront', fallback_asset_name: 'translations' }
                assert_includes @response.body, "onerror=\"Zd.onFatalError(\'cdn_failure\', \'https://cloudfront.example.com#{manifest_react_js['path']}\', \'#{manifest_react_js['name']}\')\""
                assert_includes @response.body, 'onerror="Zendesk.onScriptLoadError(this, \'translations\')"'
                assert_includes @response.body, '<input type="hidden" name="fallback_cdn" id="fallback_cdn" value="cloudfront"'
                assert_includes @response.body, '<input type="hidden" name="translations_asset_fallback_to_api" id="translations_asset_fallback_to_api" value="true"'
              end

              it 'increments metrics cdn.fallback and cdn.fallback_from' do
                statsd_client = @controller.send(:statsd_client)
                statsd_client.expects(:increment).with('cdn.fallback', tags: ['cdn:cloudfront', 'asset_name:translations'])
                statsd_client.expects(:increment).with('cdn.fallback_from', tags: ['cdn:default', 'fallback:cloudfront', 'asset_name:translations'])
                post :index, params: { fallback_cdn: 'cloudfront', fallback_asset_name: 'translations' }
                assert_includes @response.body, "onerror=\"Zd.onFatalError(\'cdn_failure\', \'https://cloudfront.example.com#{manifest_react_js['path']}\', \'#{manifest_react_js['name']}\')\""
                assert_includes @response.body, 'onerror="Zendesk.onScriptLoadError(this, \'translations\')"'
                assert_includes @response.body, '<input type="hidden" name="translations_asset_fallback_to_api" id="translations_asset_fallback_to_api" value="true"'
              end
            end

            describe 'different asset fails to load' do
              it 'resolves URLs to alternate CDN if fallback_cdn set' do
                post :index, params: { fallback_cdn: 'cloudfront', fallback_asset_name: 'vendor' }
                refute_includes @response.body, "<script src=\"https://edgecast.example.com"
                assert_includes @response.body, "<script src=\"https://cloudfront.example.com"
                assert_includes @response.body, "onerror=\"Zd.onFatalError(\'cdn_failure\', \'https://cloudfront.example.com#{manifest_react_js['path']}\', \'#{manifest_react_js['name']}\')\""
                assert_includes @response.body, 'onerror="Zendesk.onScriptLoadError(this, \'translations\')"'
                refute_includes @response.body, '<input type="hidden" name="translations_asset_fallback_to_api" id="translations_asset_fallback_to_api"'
              end

              it 'does not return a fallback CDN' do
                post :index, params: { fallback_cdn: 'cloudfront', fallback_asset_name: 'vendor' }
                refute_includes @response.body, '<input id="fallback_cdn" name="fallback_cdn" type="hidden" value="cloudfront"'
                assert_includes @response.body, "onerror=\"Zd.onFatalError(\'cdn_failure\', \'https://cloudfront.example.com#{manifest_react_js['path']}\', \'#{manifest_react_js['name']}\')\""
                assert_includes @response.body, 'onerror="Zendesk.onScriptLoadError(this, \'translations\')"'
                refute_includes @response.body, '<input type="hidden" name="translations_asset_fallback_to_api" id="translations_asset_fallback_to_api"'
              end

              it 'increments metrics cdn.fallback and cdn.fallback_from' do
                statsd_client = @controller.send(:statsd_client)
                statsd_client.expects(:increment).with('cdn.fallback', tags: ['cdn:cloudfront', 'asset_name:vendor'])
                statsd_client.expects(:increment).with('cdn.fallback_from', tags: ['cdn:default', 'fallback:cloudfront', 'asset_name:vendor'])
                post :index, params: { fallback_cdn: 'cloudfront', fallback_asset_name: 'vendor' }
                assert_includes @response.body, "onerror=\"Zd.onFatalError(\'cdn_failure\', \'https://cloudfront.example.com#{manifest_react_js['path']}\', \'#{manifest_react_js['name']}\')\""
                assert_includes @response.body, 'onerror="Zendesk.onScriptLoadError(this, \'translations\')"'
                refute_includes @response.body, '<input type="hidden" name="translations_asset_fallback_to_api" id="translations_asset_fallback_to_api"'
              end
            end
          end

          describe 'fallback to the rosetta translation api' do
            it 'increments metrics cdn.fallback and cdn.fallback_from' do
              statsd_client = @controller.send(:statsd_client)
              statsd_client.expects(:increment).with('cdn.fallback', tags: ['cdn:translation_api', 'asset_name:translations'])
              statsd_client.expects(:increment).with('cdn.fallback_from', tags: ['cdn:cloudfront', 'fallback:translation_api', 'asset_name:translations'])
              post :index, params: { fallback_cdn: 'cloudfront', fallback_asset_name: 'translations', translations_asset_fallback_to_api: true }
              assert_includes @response.body, "onerror=\"Zd.onFatalError(\'cdn_failure\', \'https://cloudfront.example.com#{manifest_react_js['path']}\', \'#{manifest_react_js['name']}\')\""
              assert_includes @response.body, 'onerror="Zd.onFatalError(\'translations_failure\''
              refute_includes @response.body, '<input type="hidden" name="translations_asset_fallback_to_api" id="translations_asset_fallback_to_api"'
            end
          end
        end

        describe 'fallback_cdn set to alternate CDN' do
          it 'resolves URLs to alternate CDN if fallback_cdn set' do
            post :index, params: { fallback_cdn: 'cloudfront' }
            refute_includes @response.body, "<script src=\"https://edgecast.example.com"
            assert_includes @response.body, "<script src=\"https://cloudfront.example.com"
          end

          it 'resolves to alternate CDN on dashboard method' do
            post :dashboard, params: { fallback_cdn: 'cloudfront' }
            refute_includes @response.body, "<script src=\"https://edgecast.example.com"
            assert_includes @response.body, "<script src=\"https://cloudfront.example.com"
          end

          it 'resolves to alternate CDN on ticket method' do
            post :ticket, params: { fallback_cdn: 'cloudfront', ticket_id: '2' }
            refute_includes @response.body, "<script src=\"https://edgecast.example.com"
            assert_includes @response.body, "<script src=\"https://cloudfront.example.com"
          end

          it 'does not return a fallback CDN' do
            post :index, params: { fallback_cdn: 'cloudfront' }
            refute_includes @response.body, '<input id="fallback_cdn" name="fallback_cdn" type="hidden" value="cloudfront"'
          end

          it 'increments metrics cdn.fallback and cdn.fallback_from' do
            statsd_client = @controller.send(:statsd_client)
            statsd_client.expects(:increment).with('cdn.fallback', tags: ['cdn:cloudfront', 'asset_name:application'])
            statsd_client.expects(:increment).with('cdn.fallback_from', tags: ['cdn:default', 'fallback:cloudfront', 'asset_name:application'])
            post :index, params: { fallback_cdn: 'cloudfront', fallback_asset_name: 'application' }
          end
        end

        [:index, :show, :dashboard].each do |method|
          describe "for #{method}" do
            describe 'crashing when error is set to cdn_failure' do
              it 'returns 503 error' do
                cdn_url = 'https://cdn.example/api/v2/locales/ja.json?bidi_chars=true&callback=__zendesk_config._cb&include=translations&package=lotus'
                post method, params: { error_identifier: 'cdn_failure', error_asset_name: 'application', error_data: cdn_url }
                assert_response :service_unavailable
              end

              it 'increments the cdn_failure statsd metric' do
                statsd_client = @controller.send(:statsd_client)
                statsd_client.expects(:increment).with('lotus.crash.cdn_failure', tags: ['cdn:cloudfront', 'asset_name:application'])
                post method, params: { error_identifier: 'cdn_failure', error_asset_name: 'application' }
              end

              describe 'translations failure' do
                it 'increments the cdn_failure statsd metric and returns 503 error' do
                  statsd_client = @controller.send(:statsd_client)
                  statsd_client.expects(:increment).with('lotus.crash.translations_failure', tags: ['cdn:n/a', 'asset_name:translations'])

                  post method, params: { error_identifier: 'translations_failure', error_asset_name: 'translations' }

                  assert_response :service_unavailable
                end
              end
            end
          end
        end
      end

      describe 'invalid manifest' do
        it 'returns an error page' do
          Lotus::AssetManifest.any_instance.stubs(:empty?).returns(true)
          get :index
          assert_response :bad_request, @response.code
        end
      end

      describe 'with a specified sha' do
        before do
          @manager = Lotus::ManifestManager.new(account)
          @manager.stubs(:find).with(manifest_sha).returns(@asset_manifest)
        end

        describe 'in a VPN environment' do
          before { @request.env['REMOTE_ADDR'] = '127.0.0.1' }

          it 'allows access' do
            get :index, params: { sha: manifest_sha }
            assert_response :success, @response.code
          end
        end

        describe 'in a integrated network' do
          before { @request.env['REMOTE_ADDR'] = '127.0.0.2' }

          it 'allows access' do
            get :index, params: { sha: manifest_sha }
            assert_response :success, @response.code
          end
        end

        describe 'outside the VPN' do
          it 'restricts access' do
            get :index, params: { sha: manifest_sha }
            assert_response :forbidden, @response.code
          end
        end

        describe 'when using development lotus' do
          before do
            @request.env['REMOTE_ADDR'] = '127.0.0.1'
            Lotus::ManifestManager.any_instance.stubs(:find).returns(dev_asset_manifest)
          end

          it 'works' do
            get :index, params: { sha: 'develop' }
            assert_response :success, @response.code
          end

          it 'creates a development manifest manager' do
            get :index, params: { sha: 'develop' }
            assert @controller.send(:manifest_manager).instance_variable_get(:@develop)
          end

          it 'sets an ivar for use in the view' do
            get :index, params: { sha: 'develop' }
            assert(assigns(:local_dev_assets))
          end
        end

        describe 'handling tags' do
          before { @request.env['REMOTE_ADDR'] = '127.0.0.2' }

          describe 'when provided a valid tag (vXX)' do
            it 'accepts the tag' do
              get :index, params: { sha: 'v1234' }
              assert_response :success, @response.code
            end
          end

          describe 'when provided an unknown tag' do
            before do
              Lotus::ManifestManager.any_instance.stubs(:find).returns(Lotus::AssetManifest.new(account, {}))
            end

            it 'responds with an error' do
              get :index, params: { sha: 'v1.25.0' }
              assert_response :bad_request, @response.code
            end
          end
        end
      end

      describe 'showing error for incompatible browsers' do
        it 'happens when error_identifier is incompatible_browser' do
          post :index, params: { error_identifier: 'incompatible_browser' }
          assert_response :bad_request
        end
      end

      describe '#deeplink_params' do
        it 'returns ticket_id' do
          @controller.params = { ticket_id: 123 }
          expected = { ticket_id: 123 }
          assert_equal expected, @controller.send(:deeplink_params)
        end
      end
    end
  end
end
