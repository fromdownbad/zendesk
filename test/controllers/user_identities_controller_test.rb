require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe UserIdentitiesController do
  fixtures :account_settings, :accounts, :role_settings, :users
  include UserIdentitiesHelper

  before do
    @request.account = accounts(:minimum)
    @admin = users(:minimum_admin)
  end

  describe "On GET to :index" do
    before do
      @user = users(:minimum_end_user)
      login(@user)
    end

    describe "with xml format" do
      before { get :index, params: { user_id: @user.id, format: "xml" } }
      it('responds with content_type xml') { assert_equal 'application/xml', @controller.response.content_type.to_s }
      it "returns the user identities xml" do
        expected_response = Time.use_zone(@user.time_zone) { @user.identities.to_xml }
        assert_equal expected_response, @response.body
      end
    end
  end

  describe "On GET to :new" do
    before do
      @user = users(:minimum_end_user)
      login(@user)
    end

    describe "with type=twitter_readonly" do
      before do
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).with(
          "twitter_readonly", successful_twitter_auth_user_url(@user, state: Zendesk::Auth::Warden::OAuthStrategy.state({}, Zendesk::Configuration.dig!(:google, :key)), anchor: 'identities')
        ).
          returns('THE_TWITTER_URL')

        get :new, params: { user_id: @user.id, type: "twitter_readonly" }
      end
      it('responds with success') { assert_response :success }

      it "includes a link to the generated oauth url" do
        assert_select "a[href='THE_TWITTER_URL']"
      end
    end

    describe "with type=manual_twitter" do
      before do
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).never
        get :new, params: { user_id: @user.id, type: "manual_twitter" }
      end
      it('responds with success') { assert_response :success }

      it "shows the lightbox to add a twitter account manually" do
        assert_select "div#add_twitter_manually_lightbox"
      end
    end

    describe "with type=facebook" do
      before do
        token = @user.challenge_tokens.lookup_by_ip_address(@request.ip)
        real_domain = CGI.escape("https://#{@request.host}/users/#{@user.id}/user_identities/new?type=facebook")
        callback_url = "https://minimum.zendesk-test.com/users/#{@user.id}/successful_facebook_auth?challenge=#{token.value}&real_domain=#{real_domain}&state=#{Zendesk::Auth::Warden::OAuthStrategy.state({}, Zendesk::Configuration.dig!(:google, :key))}"
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).with("facebook", callback_url).returns('THE_FACEBOOK_URL')
        get :new, params: { user_id: @user.id, type: "facebook" }
      end

      it('responds with success') { assert_response :success }

      it "includes a link to the generated oauth url" do
        assert_select "a[href='THE_FACEBOOK_URL']"
      end
    end

    describe "with an invalid profile type should not call oauth" do
      before do
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).never
        get :new, params: { user_id: @user.id, type: "google" }
      end
      it('responds with success') { assert_response :success }
    end

    describe "with type=twitter but twitter is not currently available and sends 502 Bad Gateway" do
      before do
        OAuth::Consumer.any_instance.expects(:request).returns(Net::HTTPBadGateway.new("1.1", "502", "Bad Gateway"))
        get :new, params: { user_id: @user.id, type: "twitter" }
      end
      it('responds with bad_gateway') { assert_response :bad_gateway }
    end

    it "does not accept evil types" do
      get :new, params: { user_id: @user.id, type: "../../foo" }
      assert_response :bad_request
    end
  end

  describe "On DELETE to :destroy" do
    before do
      login(users(:minimum_admin))
    end

    describe "for a user with one identity" do
      before do
        @user = users(:minimum_agent)
        assert_equal 1, @user.identities.size

        delete :destroy, params: { id: @user.identities.first.id.to_s, user_id: @user.id.to_s }
      end
      should_not_change("the number of identities") { UserIdentity.count(:all) }
    end

    describe "for a user with two identities" do
      before do
        @user = users(:minimum_end_user)
        assert @user.identities.size > 1

        delete :destroy, params: { id: @user.identities.first.id.to_s, user_id: @user.id.to_s }
      end
      should_destroy :user_identity
    end

    describe "for a user with two identities with js format" do
      before do
        @user = users(:minimum_end_user)
        assert @user.identities.size > 1

        delete :destroy, params: { id: @user.identities.first.id.to_s, user_id: @user.id.to_s, format: 'js' }
      end
      should_destroy :user_identity
      it('responds with content_type js') { assert_equal 'text/javascript', @controller.response.content_type.to_s }
    end
  end

  describe "On POST to :make_primary" do
    describe "when editing is authorized" do
      before do
        @user = users(:minimum_end_user)
        @user.update_attribute(:locale_id, 1)
        login(@user)

        @user.identities.map(&:id)
        @new_primary_identity = @user.identities.last
        assert_not_equal @new_primary_identity, @user.identities.first
        @user.identities.update_all(priority: 2)
        post :make_primary, params: { user_id: @user.id.to_s, id: @new_primary_identity.id.to_s }
        assert_response :redirect
      end

      it "makes the specified identity the primary one" do
        assert_equal @new_primary_identity, @user.identities(:reload).first
      end
    end

    describe "when editing is notauthorized" do
      before do
        @user = users(:minimum_agent)
        login(users(:minimum_end_user))

        post :make_primary, params: { user_id: @user.id.to_s, id: Time.now.to_i.to_s }
      end
      it('responds with forbidden') { assert_response :forbidden }
    end
  end

  [:email, :google].each do |type|
    describe "On POST to :create for #{type}" do
      before do
        login(@admin)
      end

      it "adds a new user identity (#{type}) to the user" do
        assert_difference("UserIdentity.count(:all)") do
          UsersMailer.expects(:deliver_verify)
          post :create, params: { :user_id => @admin.id, type => new_email = "a_new_valid_email@email.com" }
          assert_equal new_email, @admin.reload.identities.email.last.value
          flash[:notice].must_include "identity has been added."
        end
      end

      it "does not allow a duplicate email to be added" do
        post :create, params: { :user_id => @admin.id.to_s, type => @admin.identities.email.first.value }
        flash[:error].must_include "#{@admin.identities.email.first.value} is already being used by another user"
      end

      it "does not allow a poorly formatted email to be added" do
        post :create, params: { :user_id => @admin.id.to_s, type => "bad_email_format" }
        flash[:error].must_include "is not properly formatted"
      end

      it "does not allow an email to be added with the same address as the account's reply to address" do
        post :create, params: { :user_id => @admin.id.to_s, type => @admin.account.reply_address }
        flash[:error].must_include "#{@admin.account.reply_address} cannot be used; it is in use as a support address"
      end
    end
  end

  it "creates a Google profile for Google emails" do
    login(@admin)

    assert_difference("UserIdentity.count(:all)") do
      post :create, params: { user_id: @admin.id, google: new_email = "a_new_valid_email@email.com" }
      identity = @admin.reload.identities.email.last

      assert_equal new_email, identity.value
      assert_equal false,     identity.is_verified?
      assert identity.google?
    end
  end

  describe "On POST to :create for a twitter handle manually" do
    before do
      login(@admin)
      stub_request(:get, "https://api.twitter.com/1.1/users/show.json?screen_name=testhandle").
        to_return(
          status: 200,
          body: read_test_file('twitter_users_show_110542029_1.1.json'),
          headers: {'Content-Type' => 'application/json'}
        )
      post :create, params: { user_id: users(:minimum_end_user).id.to_s, twitter: "testhandle" }
    end

    should_set_the_flash_to "The Twitter identity has been added."
    should_change("the number of Twitter identities", by: 1) { UserTwitterIdentity.count(:all) }
  end

  describe "On POST to :create with no data" do
    before do
      @user = users(:minimum_end_user)
      login(@admin)

      post :create, params: { user_id: @user.id.to_s }
    end

    it "fails with a not acceptable" do
      assert_response :bad_request
    end

    should_not_change("the number of User identities") { UserIdentity.count(:all) }
  end

  %w[xml json].each do |desired_format|
    describe "On a successful POST to :create with a desired #{desired_format.upcase} response" do
      before do
        login(@admin)
        post :create, params: { format: desired_format, user_id: @admin.id.to_s, email: "a_new_valid_email@email.com" }
      end

      it('responds with created') { assert_response :created }
      it("responds with content_type #{desired_format}") { assert_equal Mime::Type.lookup_by_extension(desired_format.to_s), @controller.response.content_type.to_s }
    end

    describe "On an unsuccessful POST to :create with a desired #{desired_format.upcase} response" do
      before do
        login(@admin)
        post :create, params: { format: desired_format, user_id: @admin.id.to_s, email: "bad_email_format" }
      end

      it('responds with not_acceptable') { assert_response :not_acceptable }
      it("responds with content_type #{desired_format}") { assert_equal Mime::Type.lookup_by_extension(desired_format.to_s), @controller.response.content_type.to_s }
    end
  end
end
