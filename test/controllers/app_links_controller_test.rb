require_relative "../support/test_helper"

SingleCov.covered!

describe AppLinksController do
  fixtures :accounts, :app_links

  before do
    Arturo.enable_feature! :mobile_app_links_ui
    @request.account = accounts(:minimum)
  end

  with_options(controller: 'app_links') do |request|
    request.should_route :get, '/.well-known/apple-app-site-association', controller: :app_links, action: :apple, format: 'json'
    request.should_route :get, '/.well-known/assetlinks.json', controller: :app_links, action: :android, format: 'json'
  end

  describe 'with :mobile_app_links_ui feature disabled' do
    before do
      Arturo.disable_feature! :mobile_app_links_ui
    end

    describe '#apple' do
      it 'returns the app link data for apple devices' do
        expected = {
          'applinks' => {
            'apps' => [],
            'details' => [
              {
                'appID' => 'com.zendesk.agent.scarlett',
                'paths' => ['tickets/*']
              }
            ]
          }
        }

        get :apple, format: :json

        assert_equal expected, JSON.parse(response.body)
      end
    end

    describe '#android' do
      it 'returns the app link data for android devices' do
        expected = [
          {
            'relation' => ['delegate_permission/common.handle_all_urls'],
            'target' => {
              'namespace' => 'android_app',
              'package_name' => 'com.zendesk.scarlett.zendesk',
              'sha256_cert_fingerprints' => ['2C:E2:3E:60:9E:E7:E9:8D:2E:71:BE:E9:86:B8:B6:11:3F:01:0D:B8:E3:CA:77:FA:7B:0C:F6:56:2D:FC:D3:EB']
            }
          }
        ]

        get :android, format: :json

        assert_equal expected, JSON.parse(response.body)
      end
    end
  end

  describe 'with :mobile_app_links_ui feature enabled' do
    before do
      Arturo.enable_feature! :mobile_app_links_ui
    end

    describe '#apple' do
      it 'returns the app link data for apple devices' do
        expected = {
          'applinks' => {
            'apps' => [],
            'details' => [
              {
                'appID' => 'com.zendesk.agent.scarlett',
                'paths' => ['tickets/*']
              }
            ]
          }
        }

        get :apple, format: :json

        assert_equal expected, JSON.parse(response.body)
      end
    end

    describe '#android' do
      it 'returns the app link data for android devices' do
        expected = [
          {
            'relation' => ['delegate_permission/common.handle_all_urls'],
            'target' => {
              'namespace' => 'android_app',
              'package_name' => 'com.zendesk.scarlett.zendesk',
              'sha256_cert_fingerprints' => ['2C:E2:3E:60:9E:E7:E9:8D:2E:71:BE:E9:86:B8:B6:11:3F:01:0D:B8:E3:CA:77:FA:7B:0C:F6:56:2D:FC:D3:EB']
            }
          }
        ]

        get :android, format: :json

        assert_equal expected, JSON.parse(response.body)
      end
    end
  end
end
