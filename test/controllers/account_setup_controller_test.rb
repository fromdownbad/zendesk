require_relative "../support/test_helper"
require_relative "../support/consul_lookup_support"

SingleCov.covered!

describe AccountSetupController do
  let(:current_pod_id) { Zendesk::Configuration.fetch(:pod_id) }
  let(:redirect_pod_id) { current_pod_id + 1 }
  let(:verify_url) { 'https://test.zd-test.com/verification/token/asdf' }
  let(:statsd_client) { Rails.application.config.statsd.client }

  ENV['ACCOUNT_SERVICE_JWT_SECRET'] = '2e54d0782643e9d4fbb8606984b74ffd7f43ffdd9175184364df457e4f671d4ecf8f9cafdb5a0d0e5fcce68400da4838e719d6a84140f0b350d367d340572cb'

  def stub_readiness_check(state:)
    Zendesk::AccountReadiness.any_instance.stubs(:readiness_check).returns(state)
  end

  def stub_account_service_jwt(jwt_claims)
    JWT.encode(jwt_claims, ENV['ACCOUNT_SERVICE_JWT_SECRET'])
  end

  def stub_doorman_jwt(jwt_claims)
    JWT.encode(jwt_claims, ENV['ZENDESK_DOORMAN_SECRET'])
  end

  def doorman_jwt_claims(account_active: true)
    timestamp = Time.now.to_i
    exp = timestamp + 300

    {
      "iat" => timestamp,
      "exp" => exp,
      "via" => "system_user",
      "ip_restriction" => "no_restriction",
      "system_user" => { "name" => "zendesk" },
      "account" => {
        "id" => 107,
        "is_active" => account_active,
        "route_id" => 68,
        "shard_id" => 1,
        "subdomain" => "acme-393806",
        "multiproduct" => true,
        "lock_state" => 0,
        "products" => { "acme_app" => { "state" => "trial", "plan" => "0" }},
        "plans" => { "acme_app" => "0" }
      },
      "user" => {"id" => -1, "role" => nil, "locale" => "en-US", "timezone" => "Etc/UTC", "entitlements" => {}, "owner" => false }
    }
  end

  def valid_jwt_claims(timestamp: Time.now)
    {
      iat: timestamp.to_i.to_s,
      exp: (timestamp.to_i + 300).to_s,
      account_id: '1',
      subdomain: 'test',
      pod_id: current_pod_id,
      owner_id: '10001',
      products_activated: ['chat'],
      owner_verify_link: verify_url
    }
  end

  let(:invalid_jwt_claims) { valid_jwt_claims(timestamp: Time.now - 2.days) }

  before do
    Timecop.freeze(Time.now)
  end

  describe '#show' do
    describe 'for valid JWT' do
      describe 'subdomain check' do
        let(:host_subdomain) { 'incorrect-subdomain' }
        before { stub_readiness_check(state: 'pending') }

        it 'renders 404 page if JWT claims has a subdomain that does not match the host subdomain' do
          @request.host = 'incorrect-subdomain.zendesk-test.com'
          statsd_client.expects(:increment).with('account_setup.invalid_subdomain').once
          get :show, params: { jwt: stub_account_service_jwt(valid_jwt_claims.merge(subdomain: 'correct-subdomain')) }
          assert_response :not_found
          assert_template(file: "#{Rails.root}/public/404.html")
        end

        it 'renders show page if JWT claims has a subdomain that matches the host subdomain' do
          @request.host = 'correct-subdomain.zendesk-test.com'
          get :show, params: { jwt: stub_account_service_jwt(valid_jwt_claims.merge(subdomain: 'correct-subdomain')) }
          assert_response :success
          assert_template :show
        end
      end

      describe 'when relaying request to a different pod' do
        it 'relays request to specified pod' do
          get :state, params: { format: :json, jwt: stub_account_service_jwt(valid_jwt_claims.merge(pod_id: redirect_pod_id)) }
          assert_response :success
          assert_match %r{@pod_#{redirect_pod_id}\D}, @response.headers['X-Accel-Redirect']
        end
      end

      describe 'when account readiness check state is pending' do
        before { stub_readiness_check(state: 'pending') }
        it 'renders show page' do
          get :show, params: { jwt: stub_account_service_jwt(valid_jwt_claims) }
          assert_response :success
          assert_template :show
        end
      end

      describe 'when account readiness check state is complete' do
        before { stub_readiness_check(state: 'complete') }
        it 'redirects to verification url' do
          get :show, params: { jwt: stub_account_service_jwt(valid_jwt_claims) }
          assert_redirected_to verify_url
        end
      end
    end

    describe 'when relaying request to a different pod' do
      before { stub_readiness_check(state: 'complete') }
      it 'relays request to specified pod' do
        get :show, params: { jwt: stub_account_service_jwt(valid_jwt_claims.merge(pod_id: redirect_pod_id)) }
        assert_response :success
        assert_match %r{@pod_#{redirect_pod_id}\D}, @response.headers['X-Accel-Redirect']
      end
    end
  end

  describe 'for invalid JWT' do
    before do
      @controller.expects(:redirect_to_pod).never
    end

    it 'renders 404 page' do
      get :show, params: { jwt: stub_account_service_jwt(invalid_jwt_claims.merge(pod_id: redirect_pod_id)) }
      assert_response :not_found
      assert_template(file: "#{Rails.root}/public/404.html")
    end
  end

  describe '#state' do
    describe 'for valid JWT' do
      before { stub_readiness_check(state: 'complete') }
      it 'returns completed response for json format if all checks pass' do
        get :state, params: { format: :json, jwt: stub_account_service_jwt(valid_jwt_claims) }
        assert_response :success
        assert_equal "{\"state\":\"complete\"}", @response.body
      end

      it 'records completion time when all checks pass' do
        statsd_client.expects(:histogram).with('account_setup.completion_time', is_a(Float)).once
        get :state, params: { format: :json, jwt: stub_account_service_jwt(valid_jwt_claims) }
      end

      it 'returns 406 response for non-json formats' do
        get :state, params: { jwt: stub_account_service_jwt(valid_jwt_claims) }
        assert_equal 406, @response.status
      end

      describe 'when setting forced_wait_seconds' do
        it 'returns pending state when current time is within the wait time window' do
          fake_timestamp = Time.new(2002, 10, 31)
          Timecop.freeze(fake_timestamp) do
            statsd_client.expects(:increment).with('account_setup.state.pending', tags: ['cause:forced_wait']).once
            get :state, params: { format: :json, jwt: stub_account_service_jwt(valid_jwt_claims(timestamp: fake_timestamp).merge(forced_wait_seconds: 60)) }
            assert_response :success
            assert_equal "{\"state\":\"pending\"}", @response.body
          end
        end

        it 'returns success state when current time is no longer in the lag time window and other readiness checks have succeeded' do
          fake_timestamp = Time.new(2002, 10, 31)
          Timecop.freeze(fake_timestamp) do
            get :state, params: { format: :json, jwt: stub_account_service_jwt(valid_jwt_claims(timestamp: fake_timestamp - 11.seconds).merge(forced_wait_seconds: 10)) }
            assert_response :success
            assert_equal "{\"state\":\"complete\"}", @response.body
          end
        end
      end
    end

    describe 'for invalid JWT' do
      before do
        @controller.expects(:redirect_to_pod).never
      end

      it 'returns 404' do
        statsd_client.expects(:increment).with('account_setup.invalid_jwt').once
        get :state, params: { format: :json, jwt: stub_account_service_jwt(invalid_jwt_claims.merge(pod_id: redirect_pod_id)) }
        assert_response :not_found
      end
    end
  end
end
