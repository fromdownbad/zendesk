require_relative "../support/test_helper"

SingleCov.covered! uncovered: 19

describe UnverifiedEmailAddressesController do
  fixtures :accounts, :subscriptions, :remote_authentications, :account_property_sets, :addresses

  before do
    @request.account = @account = accounts(:minimum)
    @user = FactoryBot.create(:end_user, account: @account)
  end

  describe "Given a logged in user" do
    before { login @user }

    describe "GET #new" do
      before { get :new, params: { user_id: @user.id } }
      should_assign_to(:user) { @user }
      should_render_template :new
    end

    describe "POST #create" do
      describe "as an end-user" do
        before { post :create, params: { user_id: @user.id, email: 'newemail@example.com' } }
        should_change(by: 1) { UnverifiedEmailAddress.count(:all) }

        describe_with_arturo_enabled :disable_legacy_people_users_id_page do
          before do
            @controller.stubs(:current_brand).returns(@account.brands.first)
            post :create, params: { user_id: @user.id, email: 'newemail_1@example.com' }
          end

          it ('responds with 404') { assert_response :not_found }
        end
      end

      describe "as an admin" do
        before do
          @agent = users(:minimum_admin)
          login @agent
          ActionMailer::Base.deliveries = []
          ActionMailer::Base.perform_deliveries = true
        end

        describe "adding another email for themself" do
          it "sends a verification mail" do
            post :create, params: { user_id: @agent.id, email: 'newemail2@example.com' }
            assert_equal 1, ActionMailer::Base.deliveries.size
            mail = ActionMailer::Base.deliveries.last
            assert_equal(['newemail2@example.com'], mail.to)
          end
        end

        describe "adding another email for another user" do
          it "does not send a verification mail" do
            Account.any_instance.stubs(:is_welcome_email_when_agent_register_enabled?).returns(false)
            post :create, params: { user_id: @user.id, email: 'newemail2@example.com' }
            assert_equal 0, ActionMailer::Base.deliveries.size
          end

          describe 'if is_welcome_email_when_agent_register_enabled? is true' do
            before do
              Account.any_instance.stubs(:is_welcome_email_when_agent_register_enabled?).returns(true)
            end

            it 'does not send a verification email if the user is an end-user' do
              post :create, params: { user_id: @user.id, email: 'newemail2@example.com' }
              assert_equal 0, ActionMailer::Base.deliveries.size
            end

            it 'does not send a verification email if the user is an agent' do
              post :create, params: { user_id: @agent.id, email: 'newemail2@example.com' }
              assert_equal 1, ActionMailer::Base.deliveries.size
            end
          end
        end
      end
    end

    describe "GET #verify" do
      describe 'with an invalid token' do
        before do
          get :verify, params: { token: 'not a valid token' }
        end
        it('responds with bad_request') { assert_response :bad_request }
      end

      describe "With a new and unused email address" do
        before do
          @uea = UnverifiedEmailAddress.create(user: @user, email: 'newemail@example.com')
          get :verify, params: { token: @uea.token }
        end
        should_change(by: 1) { @user.reload.identities.email.size }
        should_redirect_to("The edit profile page") { edit_user_path(@user) }
      end

      describe "With a new and already used email address" do
        before do
          @loser = FactoryBot.create(:end_user, email: 'usedemail@example.com', account: accounts(:minimum))
          UserEmailIdentity.create!(user: @loser, value: 'unverified@example.com')
          UserEmailIdentity.create!(user: @loser, value: 'verified@example.com', is_verified: true)
          @uea = UnverifiedEmailAddress.create(user: @user, email: 'usedemail@example.com')
        end

        describe "Without confirmation" do
          before do
            get :verify, params: { token: @uea.token }
          end
          should_not_change("the users identities") { @user.reload.identities.email.size }
          should_render_template :verify
        end

        describe "With confirmation" do
          before do
            get :verify, params: { token: @uea.token, unverified_email_address: {confirmed: 'true'} }
          end
          should_change(by: 3) { @user.reload.identities.email.size }
          should_redirect_to("The edit profile page") { edit_user_path(@user) }
        end

        describe "With confirmation and no password" do
          before do
            @user.update_attribute(:crypted_password, nil)
            @loser_id = @loser.identities.first
            get :verify, params: { token: @uea.token, unverified_email_address: {confirmed: 'true'} }
          end
          should_change(by: 3) { @user.reload.identities.email.size }
          should_redirect_to("The verification path") { email_token_verification_path(token: @loser_id.verification_tokens.first.value) }
        end
      end
    end

    describe "GET #manual_agent_verify" do
      describe "With a new and unused email address" do
        before do
          @uea = UnverifiedEmailAddress.create(user: @user, email: 'newemail@example.com')
          get :manual_agent_verify, params: { id: @uea.id, user_id: @user.id }
        end
        should_change(by: 1) { @user.reload.identities.email.size }
        should_redirect_to("The profile page") { user_path(@user) }
      end

      describe "With a new and already used email address" do
        before do
          @loser = FactoryBot.create(:end_user, email: 'usedemail@example.com', account: accounts(:minimum))
          @uea = UnverifiedEmailAddress.create(user: @user, email: 'usedemail@example.com')
        end

        describe "With confirmation" do
          before do
            get :manual_agent_verify, params: { id: @uea.id, user_id: @user.id }
          end
          should_change(by: 1) { @user.reload.identities.email.size }
          should_redirect_to("The profile page") { user_path(@user) }
        end
      end

      describe "When an exception is raised during verification" do
        before do
          @uea = UnverifiedEmailAddress.create(user: @user, email: 'usedemail@example.com')
          UnverifiedEmailAddress.any_instance.stubs(:verify!).raises(ActiveRecord::RecordInvalid.new(@uea))
          get :manual_agent_verify, params: { id: @uea.id, user_id: @user.id }
        end
        should_set_the_flash_to /Validation failed/
        should_redirect_to("The profile page") { user_path(@user) }
      end
    end

    describe "GET #resend_email" do
      before do
        ActionMailer::Base.perform_deliveries = true
        # Mails are only sent to end-users if account has HC
        @user.account.enable_help_center!
        @uea = UnverifiedEmailAddress.create(user: @user, email: 'newemail@example.com')
        get :resend_email, params: { user_id: @user.id, id: @uea.id }
      end

      should_change(by: 1) { ActionMailer::Base.deliveries.size }

      it "sends an email to the user" do
        mail = ActionMailer::Base.deliveries.last
        assert_equal([@uea.email], mail.to)
      end

      it "redirects to the user profile page" do
        assert_redirected_to user_path(@user)
      end
    end

    describe "DELETE #destroy" do
      before { @uea = UnverifiedEmailAddress.create(user: @user, email: 'newemail@example.com') }

      describe "with html format" do
        before { delete :destroy, params: { user_id: @user.id, id: @uea.id } }
        should_destroy :unverified_email_address
        should_redirect_to("the user profile page") { edit_user_path(@user) }
      end

      describe "with js format" do
        before { delete :destroy, params: { user_id: @user.id, id: @uea.id, format: "js" } }
        should_destroy :unverified_email_address
      end

      describe "with json format" do
        before { delete :destroy, params: { user_id: @user.id, id: @uea.id, format: "json" } }
        should_destroy :unverified_email_address
        it('responds with ok') { assert_response :ok }
      end

      describe "with invalid id" do
        before { delete :destroy, params: { user_id: @user.id, id: 1, format: "json" } }
        it('responds with not_found') { assert_response :not_found }
      end
    end
  end

  describe "Given a non logged in user" do
    describe "clicking the verification link of second email address" do
      it "redirects the user to authentication" do
        @uea = UnverifiedEmailAddress.create(user: @user, email: 'newemail@example.com')
        get :verify, params: { token: @uea.token }
        assert_response :unauthorized
      end
    end
  end

  describe "Given a logged in user with a verified identity" do
    describe "clicking the verification link of another user" do
      it "blocks the request" do
        @uea = UnverifiedEmailAddress.create(user: FactoryBot.create(:user), email: 'newemail@example.com')
        get :verify, params: { token: @uea.token }
        assert_response :bad_request
      end
    end
  end
end
