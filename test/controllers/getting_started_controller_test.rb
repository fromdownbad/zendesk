require_relative "../support/test_helper"

SingleCov.covered! uncovered: 3

describe GettingStartedController do
  fixtures :all

  before do
    ActionMailer::Base.perform_deliveries = true
    @account = accounts(:minimum)
    @request.account = @account
    @admin = users(:minimum_admin)
    @agent = users(:minimum_agent)
    @group = groups(:minimum_group)
    Account.any_instance.stubs(:field_ticket_type).returns(FieldTicketType.new)
  end

  should_be_unauthorized_when_not_logged_in([:invite_agents], 'minimum_end_user', 403)

  should_be_unauthorized_when_not_logged_in([:invite_agents], 'minimum_agent', 403)

  with_options(controller: 'getting_started') do |request|
    request.should_route :get, '/getting_started/test_ticket', action: 'test_ticket'
  end

  describe "#invite_agents" do
    before do
      login(@admin)
      @subject = @controller.send(:agent_invitation_subject)
    end

    describe "with a valid email address" do
      before do
        @email = "new_agent@example.com"
        @text = "This is a special text"
        post :invite_agents, params: { email: @email, text: @text, format: 'json' }
      end

      should_create :user
      should_change("the number of mail deliveries", by: 1) { ActionMailer::Base.deliveries.size }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }

      it "creates an agent that is member of all groups and with all access" do
        user = User.last
        assert_equal @email, user.email
        assert_equal Role::AGENT.id, user.roles
        assert_equal RoleRestrictionType.NONE, user.restriction_id
        assert_equal user.groups, @account.groups
        assert user.can?(:moderate, Entry)
      end

      it "sends the verification mail with the given text" do
        email = ActionMailer::Base.deliveries.last
        assert_equal [@email], email.to
        assert email.joined_bodies.include?(@text)
        assert_equal @subject, email.subject
      end
    end

    describe "with a valid email address and no text in parameters" do
      before do
        @email = "new_agent@example.com"
        post :invite_agents, params: { email: @email, format: 'json' }
      end
      should_create :user
      should_change("the number of mail deliveries", by: 1) { ActionMailer::Base.deliveries.size }

      it "sends the verification mail with the default text" do
        email = ActionMailer::Base.deliveries.last
        assert_equal [@email], email.to
        assert email.joined_bodies.include?("#{@admin.name} has just created an account for you in Zendesk")
        assert_equal @subject, email.subject
      end
    end

    describe "with more than one valid email addresses" do
      before do
        @email1 = "new_agent1@example.com"
        @email2 = "new_agent2@example.com"
        @emails = [@email1, @email2]
        @text = "This is a special text"
        post :invite_agents, params: { email: @emails.join(", "), text: @text, format: 'json' }
      end
      should_change("the number of Users", by: 2) { User.count(:all) }
      should_change("the number of mail deliveries", by: 2) { ActionMailer::Base.deliveries.size }

      it "creates an agent that is member of all groups and with all access" do
        @emails.each do |email|
          user = @account.find_user_by_email(email)
          assert_equal Role::AGENT.id, user.roles
          assert_equal RoleRestrictionType.NONE, user.restriction_id
          assert_equal user.groups, @account.groups
          assert user.can?(:moderate, Entry)
        end
      end

      it "sends the verification mail with the given text" do
        email = ActionMailer::Base.deliveries.last
        assert_equal [@email2], email.to
        assert email.joined_bodies.include?(@text)
        assert_equal @subject, email.subject
      end
    end

    describe "with a non valid email address" do
      before do
        @email = "not valid@example.com"
        @text = "This is a special text"
        post :invite_agents, params: { email: @email, text: @text, format: 'json' }
      end
      should_not_change("the number of users") { User.count(:all) }
      should_not_change("the number of mail deliveries") { ActionMailer::Base.deliveries.size }
      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
    end

    describe "with a support address" do
      before do
        Arturo.enable_feature!(:recipient_addresses)
        @email = @account.recipient_addresses.first.email
        @text = "This is a special text"
        post :invite_agents, params: { email: @email, text: @text, format: 'json' }
      end
      should_not_change("the number of users") { User.count(:all) }
      should_not_change("the number of mail deliveries") { ActionMailer::Base.deliveries.size }
      it('responds with unprocessable_entity') { assert_response :unprocessable_entity }
      it "returns errors" do
        expected_response = {
          "error" => "RecordInvalid",
          "description" => "Record validation errors",
          "details" => {
            "email" => [
              { "error" => "ReservedValue", "description" => "Email: #{@account.recipient_addresses.first.email} cannot be used; it is in use as a support address" }
            ]
          }
        }
        assert_equal expected_response, JSON.parse(response.body)
      end
    end

    describe "with an existent email address" do
      before do
        @email = users(:minimum_agent).email
        @text = "This is a special text"
        post :invite_agents, params: { email: @email, text: @text, format: 'json' }
      end
      should_not_change("the number of users") { User.count(:all) }
      should_not_change("the number of mail deliveries") { ActionMailer::Base.deliveries.size }
      it('responds with created') { assert_response :created }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end

    describe "with one valid and another existent email addresses" do
      before do
        @email1 = users(:minimum_agent).email
        @email2 = "new_agent2@example.com"
        @emails = [@email1, @email2]
        @text = "This is a special text"
        post :invite_agents, params: { email: @emails.join(", "), text: @text, format: 'json' }
      end
      should_change("the number of Users", by: 1) { User.count(:all) }
      should_change("the number of mail deliveries", by: 1) { ActionMailer::Base.deliveries.size }
      it('responds with created') { assert_response :created }
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    end
  end

  describe "#test_ticket" do
    before do
      login(@agent)
      ticket = stub(id: 1, nice_id: 2)
      Ticket.expects(:create_test_ticket!).with(@account, @agent).
        returns(ticket)
    end

    before do
      post :test_ticket, format: 'json'
    end

    it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
    it('responds with created') { assert_response :created }

    it "renders a json response with the test ticket id" do
      expected = { 'ticket_url' => '/tickets/2' }.to_json
      assert_equal expected, @response.body
    end
  end
end
