require_relative "../support/test_helper"

SingleCov.covered!

describe HomeController do
  fixtures :all

  before do
    @request.account = accounts(:minimum)
  end

  should_route :get,  '/home',           controller: :home, action: :index
  should_route :get,  '/home/abcd',      controller: :home, action: :index, id: 'abcd'

  it "recognizes /home/index.rss" do
    assert_recognizes({controller: 'home', action: 'index', format: 'rss'}, '/home/index.rss')
  end

  describe "An anonymous user with public forums" do
    before do
      @request.account.stubs(:has_public_forums?).returns(true)
    end

    describe "should show the home index" do
      before { get :index }
      should_assign_to(:tab) { :home }
      should_render_template :index
    end

    describe "getting a cached page" do
      before { Rails.cache.clear }

      it "uses the same cache key for same formats" do
        @controller.expects(:index).once
        get :index
        get :index
      end

      it "uses different cache keys for different formats" do
        @controller.expects(:index).twice
        get :index
        accept :xml
        get :index
      end

      it "uses different cache keys for show_version" do
        @controller.expects(:index).twice
        get :index, params: { show_version: true }
        get :index, params: { show_version: true }
      end

      it "uses different cache keys if locale changes" do
        @controller.expects(:index).twice
        spanish = translation_locales(:spanish)
        @request.account.stubs(:allowed_translation_locales).returns([spanish, ENGLISH_BY_ZENDESK])
        get :index
        I18n.stubs(:locale).returns(spanish)
        get :index
      end
    end

    describe "with home_page_search and incremental_forum_search enabled" do
      before do
        Arturo.enable_feature!(:incremental_forum_search)
        account = @request.account
        account.settings.incremental_forum_search = true
        account.settings.home_page_search = true
        account.save
      end

      describe "on :index" do
        before { get :index }
        it('responds with success') { assert_response :success }
        it "sees no search" do
          assert_select '#incremental_search', 0
          assert_select '#search_box', 0
        end
      end
    end

    describe "where help_center disabled" do
      before do
        @request.account.settings.help_center_state = :disabled
        @request.account.settings.save!
      end

      where_web_portal_is_restricted do
        before { get :index }
        it('responds with ok') { assert_response :ok }
      end

      where_web_portal_is_disabled do
        before { get :index }
        it('responds with ok') { assert_response :ok }
      end
    end

    describe "where help_center not disabled" do
      before do
        @request.account.settings.help_center_state = :enabled
        @request.account.default_brand.stubs(:help_center_in_use?).returns(true)
        @request.env['zendesk.brand'] = @request.account.default_brand
        @request.account.settings.save!
      end

      where_web_portal_is_restricted do
        index_redirects_to_help_center_root
      end

      where_web_portal_is_disabled do
        index_redirects_to_help_center_root
      end
    end
  end

  describe "An anonymous user with no public forums" do
    before do
      @request.account.stubs(:has_public_forums?).returns(false)
      @request.account.default_brand.stubs(:help_center_in_use?).returns(true)
      @request.env['zendesk.brand'] = @request.account.default_brand
    end

    describe "where web portal is enabled" do
      before do
        @request.account.enable_web_portal!
        get :index
      end

      should_show_login_page
    end

    describe "where help_center disabled" do
      before do
        @request.account.settings.help_center_state = :disabled
        @request.account.settings.save!
      end

      where_web_portal_is_restricted do
        before { get :index }
        it('responds with unauthorized') { assert_response :unauthorized }
      end

      where_web_portal_is_disabled do
        before { get :index }
        it('responds with unauthorized') { assert_response :unauthorized }
      end
    end

    describe "where help_center not disabled" do
      before do
        @request.account.settings.help_center_state = :enabled
        @request.account.settings.save!
      end

      where_web_portal_is_restricted do
        index_redirects_to_help_center_root
      end

      where_web_portal_is_disabled do
        index_redirects_to_help_center_root
      end
    end
  end

  describe "A logged in agent" do
    before do
      @minimum_agent = login('minimum_agent')
      @minimum_account = accounts('minimum')
    end

    describe "where help_center disabled" do
      before do
        @request.account.settings.help_center_state = :disabled
        @request.account.settings.save!
      end

      where_web_portal_is_restricted do
        before { get :index }
        it('responds with ok') { assert_response :ok }
      end

      where_web_portal_is_disabled do
        before { get :index }
        it('responds with ok') { assert_response :ok }
      end
    end

    describe "where help_center not disabled" do
      before do
        @request.account.settings.help_center_state = :enabled
        @request.account.default_brand.stubs(:help_center_in_use?).returns(true)
        @request.env['zendesk.brand'] = @request.account.default_brand
        @request.account.settings.save!
      end

      where_web_portal_is_restricted do
        index_redirects_to_help_center_root
      end

      where_web_portal_is_disabled do
        index_redirects_to_help_center_root
      end
    end

    describe "with a minimum subscription" do
      before do
        @minimum_subscription = subscriptions('minimum')
      end

      it "does not see the agent dashboard" do
        get :index
        assert_select "div#dashboard", count: 0 # no longer show dashboard, all lotus all the time
      end

      it "is not able to asynchronously load the dashboard" do
        get :index
        assert_select "div#dashboard", count: 0 # no longer show dashboard, all lotus all the time
      end
    end

    describe "on a trial subscription" do
      before do
        @trial_subscription = subscriptions('trial')
      end

      describe "with assigned tickets" do
        it "does not see the agent dashboard" do
          get :index
          assert_select "div#dashboard", count: 0 # no longer show dashboard, all lotus all the time
        end
      end
    end
  end

  describe "A logged in user" do
    before do
      @minimum_end_user = login('minimum_end_user')
      @minimum_account = accounts('minimum')
      @entries = @minimum_account.entries.for_user(@minimum_end_user).pinned.paginate(page: 1, per_page: Entry::PINNED_PER_PAGE)
      @entries.length # trigger preload or get crazy IFNULL(pinned_index DESC, 999999) from will_paginate actual helper does the same
    end

    describe "where help_center disabled" do
      before do
        @request.account.settings.help_center_state = :disabled
        @request.account.settings.save!
      end

      where_web_portal_is_restricted do
        before { get :index }
        it('responds with ok') { assert_response :ok }
      end

      where_web_portal_is_disabled do
        before { get :index }
        it('responds with ok') { assert_response :ok }
      end
    end

    describe "where help_center not disabled" do
      before do
        @request.account.settings.help_center_state = :enabled
        @request.account.default_brand.stubs(:help_center_in_use?).returns(true)
        @request.env['zendesk.brand'] = @request.account.default_brand
        @request.account.settings.save!
      end

      where_web_portal_is_restricted do
        index_redirects_to_help_center_root
      end

      where_web_portal_is_disabled do
        index_redirects_to_help_center_root
      end
    end

    describe "on GET to :index" do
      describe "render" do
        before { get :index }

        should_assign_to(:tab) { :home }
        should_render_template :index
      end

      it "only render entries of the account is so configured" do
        @minimum_account.update_attribute(:display_pinned_entries, false)
        @controller.expects(:find_pinned_entries).never
        get :index
      end

      it "does not see the agent dashboard" do
        get :index
        assert_select "div#dashboard", count: 0
      end
    end

    describe "on GET to :index with an expired password" do
      before do
        shared_session[:auth_password_expired] = true
      end

      it "redirects to password reset page" do
        get :index
        assert_redirected_to password_path
      end

      it "populates flash[:error] with the appropriate message" do
        get :index
        assert_equal flash[:error], I18n.t('txt.access.login.password_expired', locale: ENGLISH_BY_ZENDESK)
      end

      describe "when user has selected a different locale" do
        before do
          @spanish = translation_locales(:spanish)
          @minimum_end_user.stubs(translation_locale: @spanish)
        end

        it "populates flash[:error] with a translated message" do
          get :index
          expected = I18n.t('txt.access.login.password_expired', locale: @spanish)
          assert_equal flash[:error], expected
        end
      end
    end

    describe "on GET to :index with RSS format" do
      describe "render" do
        before { get :index, format: 'rss' }

        should_render_template 'home/index'
        it('responds with content_type rss') { assert_equal 'application/rss+xml', @controller.response.content_type.to_s }
        should_not render_with_layout
      end
    end

    describe "on GET to :index with iPad" do
      before do
        @request.user_agent = 'Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10'
        get :index
      end

      should_render_template :index
      it('responds with content_type html') { assert_equal 'text/html', @controller.response.content_type.to_s }
    end

    describe "on GET to :index with mobile device and is_mobile=false" do
      before do
        @request.user_agent = 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en)'
        get :index, params: { Schmobile::IS_MOBILE => 'false' }
      end

      it('responds with content_type html') { assert_equal 'text/html', @controller.response.content_type.to_s }
      it "does not mark as mobile" do
        session[Schmobile::IS_MOBILE].must_equal false
      end
    end

    describe "on GET to :index with mobile device and force_classic_layout=true" do
      before do
        @request.user_agent = 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en)'
        get :index, params: { "force_classic_layout" => "true" }
      end

      it('responds with content_type html') { assert_equal 'text/html', @controller.response.content_type.to_s }

      it "does not mark as mobile" do
        session[Schmobile::IS_MOBILE].must_equal false
      end
    end
  end

  describe "Unserviceable accounts" do
    before do
      Timecop.freeze
      @request.account = accounts(:minimum)
      login('minimum_admin')
    end

    it "gets a display notice" do
      Account.any_instance.expects(:is_serviceable?).at_least_once.returns(false)
      get :index
      assert @response.body.index('This account has been suspended.')
    end
  end

  describe "Payment failure notices" do
    before do
      @request.account = accounts(:minimum)
      login('minimum_admin')
    end

    describe "for accounts with problems" do
      before do
        Timecop.freeze
        Account.any_instance.expects(:is_failing?).at_least_once.returns(true)
        User.any_instance.stubs(time_zone: 'UTC')
        get :index
      end

      it "gets displayed" do
        assert_select "h1", "Warning: Payment problems"
        assert_select "p",  "We are unable to charge the credit card on file."
      end

      it "sets a cookie that the notice was displayed" do
        assert_equal 1.day.from_now.to_i, session[:show_payment_notice_at]
      end
    end

    describe "for accounts without problems" do
      before do
        Account.any_instance.expects(:is_failing?).at_least_once.returns(false)
        get :index
      end

      it "does not get displayed" do
        refute @response.body.index("<h1>Warning: Payment problems</h1>")
      end
    end

    describe "for initial payment failure" do
      before do
        voice_sub_account = stub
        Account.any_instance.expects(:voice_sub_account).at_least_once.returns(voice_sub_account)
        voice_sub_account.expects(:suspended?).at_least_once.returns(true)

        Account.any_instance.expects(:is_failing?).at_least_once.returns(true)
        Account.any_instance.expects(:has_zero_paid_payments?).at_least_once.returns(true)
        get :index
      end

      it "displays voice suspension notice" do
        assert_select "h1", "Warning: Payment problems"
        assert_select "p",  "We are unable to charge the credit card on file. Zendesk Voice has been temporarily disabled in your account."
      end
    end
  end

  describe "Agent layout" do
    before do
      login(:minimum_agent)
    end

    it "includes embedded lotus script" do
      get :index
      assert_template "layouts/_lotus_only_agent"
      assert response.body.include? %(<script src="/assets/embedded_lotus)
    end
  end
end
