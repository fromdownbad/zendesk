require_relative '../support/test_helper'

SingleCov.covered!

describe SurveyController do
  fixtures :users

  let(:owner) { users(:minimum_admin) }
  let(:admin) { users(:minimum_admin_not_owner) }

  describe '#survey_allowed_parameters' do
    subject { SurveyController.new }
    before  { subject.instance_variable_set(:@question_names, %w[one two]) }

    it 'builds the correct allowed parameters schema for arbitrary names' do
      pp = SurveyController::Parameters
      subject.send(:survey_allowed_parameters).
        must_equal(
          'one' => pp.array(pp.string),
          'two' => pp.array(pp.string)
        )
    end
  end

  describe 'when user is owner' do
    before do
      login(owner)
    end

    describe '#show' do
      it 'succeeds in rendering for a supported survey type' do
        get :show, params: { id: 'support_churn' }
        assert_response :ok
      end

      it 'fails to render for a non-supported survey type' do
        get :show, params: { id: 'nonexistent_survey_type' }
        assert_response :bad_request
      end
    end

    describe '#create' do
      describe 'when cancellation reason is not given' do
        it 'does not set in redis or enqueue SurveyPersistenceJob, but still cancels account when survey type is valid' do
          Zendesk::RedisStore.client.expects(:set).never
          SurveyPersistenceJob.expects(:enqueue).never
          owner.account.expects(:cancel!)
          post :create, params: { type: 'support_churn' }
          assert_response :created
        end
      end

      describe 'when cancellation reason is given' do
        it 'sets in redis, enqueues SurveyPersistenceJob, and cancels account, when survey type is valid' do
          Zendesk::RedisStore.client.expects(:set)
          SurveyPersistenceJob.expects(:enqueue).with(includes(:redis_key))
          owner.account.expects(:cancel!)
          post :create, params: { type: 'support_churn', answer: {whyCancel: ['foo']} }
          assert_response :created
        end

        it 'returns :bad_request, when survey type is invalid' do
          owner.account.expects(:cancel!).never
          post :create, params: { type: 'foobar' }
          assert_response :bad_request
        end

        it 'returns :created, even when redis fails' do
          Zendesk::RedisStore.client.stubs(:set).raises(Redis::CannotConnectError)
          post :create, params: { type: 'support_churn', answer: {whyCancel: ['foo']} }
          assert_response :created
        end

        it 'does not cancel that account if the request includes cancel=false' do
          Zendesk::RedisStore.client.expects(:set)
          SurveyPersistenceJob.expects(:enqueue).with(includes(:redis_key))
          owner.account.expects(:cancel!).never
          post :create, params: { type: 'support_churn', answer: {whyCancel: ['foo']}, cancel: false }
        end
      end

      describe 'when the survey is a churn survey' do
        it 'includes canceled as true if the request includes cancel=true' do
          Zendesk::RedisStore.client.expects(:set).with do |_, p|
            JSON.parse(p).fetch('canceled', false)
          end
          SurveyPersistenceJob.expects(:enqueue).with(includes(:redis_key))
          post :create, params: { type: 'support_churn', answer: {whyCancel: ['foo']}, cancel: true }
        end

        it 'includes canceled as false if the request includes cancel=false' do
          Zendesk::RedisStore.client.expects(:set).with do |_, p|
            JSON.parse(p).fetch('canceled', true) == false
          end
          SurveyPersistenceJob.expects(:enqueue).with(includes(:redis_key))
          post :create, params: { type: 'support_churn', answer: {whyCancel: ['foo']}, cancel: false }
        end

        it 'includes canceled as true if the request does not include cancel' do
          Zendesk::RedisStore.client.expects(:set).with do |_, p|
            JSON.parse(p).fetch('canceled', false)
          end
          SurveyPersistenceJob.expects(:enqueue).with(includes(:redis_key))
          post :create, params: { type: 'support_churn', answer: {whyCancel: ['foo']} }
        end

        it 'sets training_offer_save to true if the request includes redeem_training_offer=true' do
          Zendesk::RedisStore.client.expects(:set).with do |_, p|
            JSON.parse(p).fetch('training_offer_save', false)
          end
          SurveyPersistenceJob.expects(:enqueue).with(includes(:redis_key))
          post :create, params: { type: 'support_churn', answer: {whyCancel: ['foo']}, redeem_training_offer: true }
        end

        it 'sets training_offer_save to false if the request includes redeem_training_offer=false' do
          Zendesk::RedisStore.client.expects(:set).with do |_, p|
            JSON.parse(p).fetch('training_offer_save', true) == false
          end
          SurveyPersistenceJob.expects(:enqueue).with(includes(:redis_key))
          post :create, params: { type: 'support_churn', answer: {whyCancel: ['foo']}, redeem_training_offer: false }
        end

        it 'sets training_offer_save to false if the request does not include redeem_training_offer=true' do
          Zendesk::RedisStore.client.expects(:set).with do |_, p|
            JSON.parse(p).fetch('training_offer_save', true) == false
          end
          SurveyPersistenceJob.expects(:enqueue).with(includes(:redis_key))
          post :create, params: { type: 'support_churn', answer: {whyCancel: ['foo']} }
        end
      end
    end
  end

  describe 'when user is admin' do
    before do
      login(admin)
    end

    describe '#show' do
      it 'responds with :forbidden' do
        get :show, params: { id: 'support_churn' }
        assert_response :forbidden

        get :show, params: { id: 'nonexistent_survey_type' }
        assert_response :forbidden
      end
    end

    describe '#create' do
      it 'responds with :forbidden' do
        post :create, params: { type: 'support_churn', answer: {whyCancel: ['foo']} }
        assert_response :forbidden
      end
    end
  end
end
