require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

# Please be advised there are multiple "agreements controllers":
#
#   app/controllers/sharing/agreements_controller.rb
#   app/controllers/sharing_agreements_controller.rb
#   app/controllers/api/**/sharing_agreements_controller.rb
#
# the controller in the app/controllers directory (sharing_agreements.controller.rb) is user facing
# the controller in the app/controllers/sharing directory (agreements_controller.rb) is for backend work
# the controllers in the app/controllers/api/** directories are for api work

describe SharingAgreementsController do
  fixtures :accounts, :users, :role_settings

  resque_inline false

  before do
    @request.account = @account = accounts(:minimum)
    @sharing_partner = accounts(:support)
    @current_user = login(:minimum_admin)
  end

  describe "creating a valid agreement html" do
    ["", "1"].each do |shared_with_id|
      it "with shared_with_id '#{shared_with_id}'" do
        assert_difference('Sharing::Agreement.count(:all)') do
          post :create, params: {
            sharing_agreement: {
              shared_with_id: shared_with_id,
              subdomain: @sharing_partner.subdomain
            }
          }
        end

        assert_redirected_to settings_tickets_path(anchor: 'ticket_sharing')
        assert agreement = assigns(:agreement)
        assert_equal(@current_user, agreement.local_admin)
      end
    end
  end

  it "prevents creating an agreement if not admin" do
    @current_user = login(:minimum_agent)
    refute_difference('Sharing::Agreement.count(:all)') do
      post :create, params: { sharing_agreement: {subdomain: @sharing_partner.subdomain} }
    end
  end

  it "creating an invalid agreement html" do
    refute_difference('Sharing::Agreement.count(:all)') do
      post :create, params: { sharing_agreement: {subdomain: nil} }
    end

    assert_match(/The agreement was not saved because/, flash[:error])
    assert_redirected_to settings_tickets_path(anchor: 'ticket_sharing')
  end

  it "creating a valid agreement json" do
    assert_difference('Sharing::Agreement.count(:all)') do
      accept :json
      post :create, params: { sharing_agreement: {subdomain: @sharing_partner.subdomain} }
    end

    assert_response :created, @response.body
    assert_match(/settings\/tickets.*#ticket_sharing/, @response['Location'])
  end

  it "creating an invalid agreement json" do
    refute_difference('Sharing::Agreement.count(:all)') do
      accept :json
      post :create, params: { sharing_agreement: {subdomain: @account.subdomain} }
    end

    assert_response :unprocessable_entity, @response.body
  end

  describe "updating" do
    describe "a valid agreement" do
      before do
        # the :status starts at :pending
        @agreement = create_agreement
      end

      it "actually update the agreement" do
        put :update, params: { id: @agreement.to_param, sharing_agreement: {subdomain: @account.subdomain} }

        assert_redirected_to settings_tickets_path(anchor: 'ticket_sharing')
        assert @agreement = assigns(:agreement)
        assert_equal(@current_user, @agreement.local_admin)
      end

      it "rejects update if not admin" do
        @current_user = login(:minimum_agent)
        put :update, params: { id: @agreement.to_param, sharing_agreement: {subdomain: @account.subdomain} }

        assert_response :forbidden
      end

      describe "with a :pending status" do
        it "returns a resent notice in response body" do
          put :update, params: { id: @agreement.to_param, sharing_agreement: {
              status: :pending
            } }
          assert_equal("Agreement was successfully resent.", flash[:notice])
        end

        describe "twice in less than a minute" do
          it "returns a failed to resend notice" do
            Sharing::Agreement.any_instance.stubs(:resend_invite).returns(nil)
            put :update, params: { id: @agreement.to_param, sharing_agreement: {
                status: :pending
              } }
            assert_equal("Failed to resend. Limit 1 request per minute.", flash[:error])
            assert_nil(flash[:notice])
          end
        end
      end

      describe "with an :accepted status" do
        it "returns an accepted notice in response body" do
          put :update, params: { id: @agreement.to_param, sharing_agreement: {
              status: :accepted
            } }
          assert_equal("Agreement was successfully accepted.", flash[:notice])
        end
      end

      describe "with an :inactive status" do
        it "returns a deactivated notice in response body" do
          put :update, params: { id: @agreement.to_param, sharing_agreement: {
              status: :inactive
            } }
          assert_equal("Agreement was successfully deactivated.", flash[:notice])
        end
      end

      describe "with a :failed status" do
        it "returns a cancelled notice in response body" do
          put :update, params: { id: @agreement.to_param, sharing_agreement: {
              status: :failed
            } }
          assert_equal("Agreement was successfully cancelled.", flash[:notice])
        end
      end

      describe "with a :declined status" do
        it "returns a declined notice in response body" do
          put :update, params: { id: @agreement.to_param, sharing_agreement: {
              status: :declined
            } }
          assert_equal("Agreement was successfully declined.", flash[:notice])
        end
      end
    end

    describe "an invalid agreement" do
      before do
        @agreement = FactoryBot.create(
          :agreement,
          account: @account, direction: :out,
          status: :accepted, subdomain: @sharing_partner.subdomain
        )
      end

      it "does not update anything" do
        refute_difference('Sharing::Agreement.count(:all)') do
          post :update, params: { id: @agreement.id, sharing_agreement: {
              status: :accepted
            } }
        end
        assert_response :redirect, @response.body
      end
    end
  end

  it "should destroy agreement" do
    agreement = create_agreement
    assert_difference('Sharing::Agreement.count(:all)', -1) do
      delete :destroy, params: { id: agreement.to_param }
    end

    assert_redirected_to settings_tickets_path(anchor: 'ticket_sharing')
  end

  it "should prevent agents from destroying" do
    agreement = create_agreement
    @current_user = login(:minimum_agent)
    refute_difference('Sharing::Agreement.count(:all)') do
      delete :destroy, params: { id: agreement.to_param }
    end
  end

  it "should allow users to destroy invalid agreements" do
    agreement = create_agreement
    Sharing::Agreement.any_instance.stubs(:remote_url).returns("invalid-url:123123")
    assert_difference('Sharing::Agreement.count(:all)', -1) do
      delete :destroy, params: { id: agreement.to_param }
    end
  end

  it "should destroy all inactive outgoing agreements" do
    create_inactive_agreements
    assert_difference('Sharing::Agreement.count(:all)', -1) do
      delete :destroy_inactive
    end

    assert_redirected_to settings_tickets_path(anchor: 'ticket_sharing')
  end

  describe "#jira_projects" do
    before do
      @agreement = create_agreement
    end

    it "renders jira projects" do
      Sharing::Agreement.any_instance.expects(:retrieve_jira_projects).returns [1, 2, 3]
      get :jira_projects, params: { id: @agreement.to_param }
      assert_response :success
      assert_equal "[1,2,3]", @response.body
    end

    it "renders bad_gateway when jira projects cannot be retrieved" do
      Sharing::Agreement.any_instance.expects(:retrieve_jira_projects).returns nil
      get :jira_projects, params: { id: @agreement.to_param }
      assert_response :bad_gateway
    end
  end

  private

  def create_agreement
    Sharing::Agreement.create!(
      account: @account,
      subdomain: @sharing_partner.subdomain
    )
  end

  def create_inactive_agreements
    [
      Sharing::Agreement.create!(
        account: @account,
        status: :inactive,
        direction: :out,
        subdomain: @sharing_partner.subdomain
      ),
      Sharing::Agreement.create!(
        account: @account,
        status: :inactive,
        direction: :in,
        subdomain: accounts(:billing).subdomain
      )
    ]
  end
end
