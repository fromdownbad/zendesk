require_relative "../../support/test_helper"

SingleCov.covered!

describe Settings::ChannelsController do
  fixtures :accounts, :users

  should_include_module Zendesk::LotusEmbeddable

  describe 'Permissions' do
    before do
      @user = users(:minimum_agent)
      @user.permission_set = @user.account.permission_sets.create!(name: "Test")
      @user.account.stubs(:has_permission_sets?).returns(true)
      @request.account = @user.account

      login(@user)
    end

    describe 'Given an agent with the ability to manage channel settings' do
      before do
        @user.permission_set.permissions.enable(:extensions_and_channel_management)
        assert @user.can?(:edit, Access::Settings::Extensions)
      end

      it 'has access to manage channel settings pages' do
        get :show
        assert_response :success, @response.body
      end
    end

    describe 'Given an agent without the ability to manage channel settings' do
      before do
        @user.permission_set.permissions.disable(:extensions_and_channel_management)
        refute @user.can?(:edit, Access::Settings::Extensions)
      end

      it 'does not have access to manage channel settings pages' do
        get :show
        assert_response :forbidden, @response.body
      end
    end
  end
end
