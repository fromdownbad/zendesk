require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Settings::RecipientAddressesController do
  fixtures :accounts, :recipient_addresses, :users

  let(:account)           { accounts(:minimum) }
  let(:admin)             { account.owner }
  let(:recipient_address) { recipient_addresses(:not_default) }
  let(:user)              { users(:minimum_agent) }

  def assert_render_without_layout
    assert_response :success
    refute_layout
  end

  should_include_module Zendesk::LotusEmbeddable
  should_route :delete, "/settings/recipient_addresses/1", action: :destroy, id: "1"

  before do
    @request.account = account
    login(admin)
  end

  describe "Permissions" do
    describe "Given an admin" do
      it "renders" do
        xhr :get, :table
        assert_response :success
      end
    end

    describe "Given a non-admin" do
      before do
        user.permission_set = user.account.permission_sets.create!(name: "Test")
        user.account.stubs(:has_permission_sets?).returns(true)
        @request.account = user.account
        login(user)
      end

      describe "Given an agent with the ability to manage channel settings" do
        before do
          user.permission_set.permissions.enable(:extensions_and_channel_management)
          assert user.can?(:edit, Access::Settings::Extensions)
        end

        it "has access to manage channel settings pages" do
          xhr :get, :table
          assert_response :success
        end
      end

      describe "Given an agent without the ability to manage channel settings" do
        before do
          user.permission_set.permissions.disable(:extensions_and_channel_management)
          refute user.can?(:edit, Access::Settings::Extensions)
        end

        it "has access to manage channel settings pages" do
          xhr :get, :table
          assert_response :forbidden
        end
      end
    end
  end

  describe "#new" do
    it "renders without layout" do
      xhr :get, :new
      assert_render_without_layout
      assert_template "new"
    end

    it "prefills account" do
      xhr :get, :new
      assert_equal account.name, assigns(:recipient_address).name
    end

    it "prefills brand" do
      brand = FactoryBot.create(:brand, account_id: account.id)
      xhr :get, :new, params: { recipient_address: { brand_id: brand.id } }
      assert_equal brand.id, assigns(:recipient_address).brand.id
      assert_equal brand.name, assigns(:recipient_address).name
    end
  end

  describe "#edit" do
    it "renders" do
      xhr :get, :edit, params: { id: recipient_address.id }
      assert_render_without_layout
      assert_template "edit"
    end
  end

  describe "#update" do
    it "flashes a notice and redirect on success with valid parameters" do
      put :update, params: { id: recipient_address.id, recipient_address: {
        name: "Fooo",
        default: false,
        brand_id: account.brands.last.id
      } }
      assert_redirected_to settings_email_path
      assert flash[:notice]
      assert_equal "Fooo", recipient_address.reload.name
    end

    it "renders on success for json" do
      xhr :put, :update, params: { id: recipient_address.id, recipient_address: { name: "Fooo" }, format: "json" }
      assert_response :success
      assert_nil flash[:notice]
      assert_equal "Fooo", recipient_address.reload.name
    end

    it "flashes an error and redirect on failure" do
      put :update, params: { id: recipient_address.id, recipient_address: { email: "" } }
      assert_redirected_to settings_email_path
      assert flash[:error]
      assert_equal "not_default@example.net", recipient_address.reload.email
    end

    it "renders not_acceptable on failure for a json request" do
      xhr :put, :update, params: { id: recipient_address.id, recipient_address: { email: "foo@bar.com" }, format: "json" }
      assert_response :not_acceptable
      refute flash[:error]
      # if this changes you have to fix app/assets/javascripts/views/settings/email/recipient_address.js
      assert_equal({
        "description" => "Record validation errors",
        "details" => {"email" => [{"error" => "ChangeNotAllowed", "description" => "Email: change not allowed"}]},
        "error" => "RecordInvalid"
      }, JSON.parse(response.body))
      assert_equal "not_default@example.net", recipient_address.reload.email
    end
  end

  describe "#create" do
    it "adds the recipient address with allowed parameters" do
      assert_difference("RecipientAddress.count(:all)", +1) do
        post :create, params: { recipient_address: {
          email: "test@test.com",
          name: "Test",
          default: false,
          brand_id: account.brands.first.id,
        } }
      end

      assert flash[:notice]
      assert_redirected_to settings_email_path
    end

    it "responds with a json success response so jquery is happy" do
      assert_difference("RecipientAddress.count(:all)", +1) do
        xhr :post, :create, params: { recipient_address: { email: "test@test.com" }, format: "json" }
      end

      assert_nil flash[:notice]
      assert_response :success
      assert_not_nil JSON.parse(response.body)
    end

    it "fails to save with invalid payload" do
      refute_difference("RecipientAddress.count(:all)") do
        post :create, params: { recipient_address: { name: "" } }
      end

      assert flash[:error]
      assert_redirected_to settings_email_path
    end

    it "renders not_acceptable on failure for a json request" do
      refute_difference("RecipientAddress.count(:all)") do
        xhr :post, :create, params: { recipient_address: { email: "foo" }, format: "json" }
      end

      refute flash[:error]
      assert_response :not_acceptable
      assert_equal({
        "description" => "Record validation errors",
        "details" => {"email" => [{"error" => "InvalidFormat", "description" => "Email: is not properly formatted"}]},
        "error" => "RecordInvalid"
      }, JSON.parse(response.body))
    end
  end

  describe "#destroy" do
    it "removes the recipient address" do
      assert_difference("RecipientAddress.count(:all)", -1) do
        delete :destroy, params: { id: recipient_address.id }
      end

      assert_redirected_to settings_email_path
    end

    it "flashes an error when destroy fails" do
      RecipientAddress.any_instance.expects(:destroy).returns(false)
      delete :destroy, params: { id: recipient_address.id }

      assert_equal "Failed to save.", flash[:error]
      assert_redirected_to settings_email_path
    end
  end

  describe "#preview" do
    it "renders preview partial" do
      get :preview, params: { recipient_address: {email: "foo@bar.com", name: "123456AA"} }
      assert_response :success
      refute_layout
      assert_includes @response.body, "foo@bar.com"
    end
  end

  describe "#table" do
    it "renders" do
      xhr :get, :table
      assert_response :success
    end
  end
end
