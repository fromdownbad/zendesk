require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Settings::ChatController do
  fixtures :all

  should_include_module Zendesk::LotusEmbeddable

  let(:current_account) { accounts(:minimum) }
  let(:zopim_integration) { current_account.build_zopim_integration }

  before do
    Account.any_instance.stubs(:chat_available?).returns(true)
    ZopimIntegration.stubs(:app_market_id).returns(55)
    current_account.stubs(:web_portal_and_help_center_disabled?).returns(true)

    zopim_integration.stubs(
      app_path:      'some-path',
      app_installed?: true
    )

    Account.any_instance.stubs(:zopim_integration).
      returns(zopim_integration)

    @request.account = current_account
    login(current_account.owner)
  end

  describe 'a GET to :show' do
    before do
      get :show
    end

    it('responds with success') { assert_response :success }

    it "does not render the chat about my ticket field" do
      assert_select 'form input[name=?]', 'account[settings][chat_about_my_ticket]', 0
    end

    describe "zendesk customer/trial without zopim" do
      before do
        Account.any_instance.stubs(:zopim_integration).returns(nil)
        Account.any_instance.stubs(:zopim_subscription).returns(nil)
      end

      describe "and rendering in lotus" do
        before do
          get :show
        end

        it "renders the zopim marketing tab" do
          assert_select '.zopim-marketing'
        end

        it "starts with an inactive zopim get started button" do
          assert_select '.cta', I18n.t('txt.admin.views.account.chat_settings._zopim.get_started')
        end

        it "does not get the explore zopim button" do
          assert_select '.explorezop-phaseI', 0
        end
      end
    end

    describe "phaseII zopim subscription" do
      describe "trial rendering in lotus" do
        let(:zopim_subscription) { zopim_subscriptions(:with_trial_zopim_subscription) }

        before do
          zopim_subscription.stubs(trial_expires_on: Date.today + 14.days)
        end

        describe "active trial zopim subscription" do
          before do
            current_account.stubs(:zopim_subscription).returns(zopim_subscription)
            get :show
          end

          it "renders zopim TRIAL subscription information" do
            assert_equal zopim_subscription, assigns(:zopim_subscription)
            assert_select '.trial-header h2 span', I18n.t('txt.admin.views.account.chat_settings.zopim_chat_settings.trial')
            assert_select '.zopim-features'
          end

          describe "trialing zendesk and trialing zopim" do
            before do
              Subscription.any_instance.stubs(:is_trial?).returns(true)
              get :show
            end

            it "shows the 'Buy Now' button" do
              assert_select '.buy-zendesk', I18n.t('txt.admin.views.account.chat_settings.zopim_chat_settings.buy_now')
              assert_select '.adjust-plan', 0
              assert_select '.buy-zopim', 0
            end
          end

          describe "zendesk customer and trialing zopim" do
            it "shows the 'Buy Now' button" do
              assert_select '.buy-zopim', I18n.t('txt.admin.views.account.chat_settings.zopim_chat_settings.buy_now')
              assert_select '.adjust-plan', 0
              assert_select '.buy-zendesk', 0
            end
          end

          it "does not show the trial expired message" do
            assert_select '.trial-expired', 0
          end
        end

        describe "the zopim trial expired" do
          before do
            zopim_subscription.stubs(
              plan_type:    ZendeskBillingCore::Zopim::PlanType::Lite.name,
              trial_end_at: 2.days.ago,
              purchased_at: nil
            )

            current_account.expects(:zopim_subscription).returns(zopim_subscription)
          end

          it "renders zopim trial expired message" do
            get :show
            assert_equal zopim_subscription, assigns(:zopim_subscription)
            assert_select '.trial-expired', 'Your trial has expired'
          end
        end
      end

      describe "lite, basic or advanced rendering in lotus" do
        let(:current_account) { accounts(:with_paid_zopim_subscription) }
        let(:zopim_subscription) { current_account.zopim_subscription }

        describe "advanced zopim subscription" do
          before do
            get :show
          end

          it "renders the zopim features" do
            assert_equal zopim_subscription, assigns(:zopim_subscription)
            assert_select '.zopim-features p', I18n.t('txt.admin.views.account.chat_settings.zopim_chat_settings.subscription')
            assert_select '.zopim-feature-status p', zopim_subscription.plan_type.capitalize
          end

          it "shows 'Adjust Plan' button" do
            assert_select '.adjust-plan', I18n.t('txt.admin.views.account.chat_settings.zopim_chat_settings.adjust_plan')
          end

          it "does not render zopim TRIAL subscription information" do
            assert_select '.trial-header h2 span', 0
          end
        end
      end

      describe "cancelled zopim subscription" do
        let(:zopim_subscription) { zopim_subscriptions(:with_cancelled_zopim_subscription) }

        before do
          @request.env['HTTP_REFERER'] = "/agent"
          zopim_integration.stubs(:app_installed?).returns(false)
          current_account.expects(:zopim_subscription).returns(zopim_subscription)

          get :show
        end

        it "does not render the zopim features" do
          assert_select '.zopim-settings p', 0
        end

        it "renders marketing page" do
          assert_select '.zopim-marketing'
        end
      end
    end

    describe "with voltron_chat enabled" do
      before do
        current_account.stubs(:has_voltron_chat?).returns(true)
      end

      describe "with zopim_integration" do
        before do
          current_account.expects(:zopim_integration).returns(zopim_integration).twice

          get :show
        end

        it "returns 200" do
          assert_response :success
        end
      end

      describe "without zopim_integration" do
        before do
          current_account.expects(:zopim_integration).returns(nil)

          get :show
        end

        it "returns 404" do
          assert_response :not_found
        end
      end
    end
  end

  describe 'a PUT to :update' do
    before do
      put :update, params: { account: {
        maximum_chat_requests: '6',
        settings: { chat: '1' }
      } }
    end

    it "sets the account's chat settings propertly" do
      current_account.reload
      assert current_account.settings.chat?
      assert_equal '6', current_account.maximum_chat_requests.to_s
    end
  end

  describe 'Permissions' do
    before do
      @user = users(:minimum_agent)
      @user.permission_set = @user.account.permission_sets.create!(name: "Test")

      @user.account.stubs(:has_permission_sets?).returns(true)

      @request.account = @user.account
      login(@user)
    end

    describe 'Given an agent with the ability to manage chat settings' do
      before do
        @user.permission_set.permissions.enable(:extensions_and_channel_management)
        assert @user.can?(:edit, Access::Settings::Extensions)
      end

      it 'has access to manage chat settings pages' do
        get :show
        assert_response :success, @response.body
      end
    end

    describe 'Given an agent without the ability to manage chat settings' do
      before do
        @user.permission_set.permissions.disable(:extensions_and_channel_management)
        refute @user.can?(:edit, Access::Settings::Extensions)
      end

      it 'does not have access to manage chat settings pages' do
        get :show
        assert_response :forbidden, @response.body
      end
    end
  end

  describe 'Given an account whose subscription plan does not have chat' do
    it 'does not have access to chat settings page' do
      current_account.stubs(:chat_available?).returns(false)
      get :show
      assert_response :forbidden, @response.body
    end
  end
end
