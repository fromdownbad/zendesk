require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Settings::CustomersController do
  fixtures :accounts, :users, :role_settings, :subscriptions

  before do
    @minimum_admin   = users(:minimum_admin)
    @minimum_account = accounts(:minimum)
    @request.account = @minimum_account

    login(@minimum_admin)
  end

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: "settings/customers") do |request|
    request.should_route :put, "/settings/customers/update_settings", action: 'update_settings'
    request.should_route :put, '/settings/customers/update_satisfaction', action: 'update_satisfaction'
  end

  describe "#show" do
    it "responds with the page" do
      get :show
      assert_response :ok
    end

    describe "web_portal and help_center are disabled" do
      before do
        @request.account.stubs(:web_portal_disabled?).returns(true)
        @request.account.stubs(:help_center_enabled?).returns(false)
      end

      it "shows/hides options that are/aren't accessible in that state" do
        get :show
        refute_includes @response.body, "CAPTCHA"
        refute_includes @response.body, "Ask users to register"
        refute_includes @response.body, "User registration message"
        assert_includes @response.body, "Also send a welcome email when a new user is created by an agent or administrator."
      end
    end

    describe "web_portal is disabled and help_center is enabled" do
      before do
        https!
        @request.account.stubs(:web_portal_disabled?).returns(true)
        @request.account.stubs(:help_center_enabled?).returns(true)
        @request.account.stubs(:has_verification_captcha?).returns(true)
      end

      it "displays all the options" do
        get :show
        assert_includes @response.body, "CAPTCHA"
        assert_includes @response.body, "Ask users to register"
        assert_includes @response.body, "User registration message"
        assert_includes @response.body, "Also send a welcome email when a new user is created by an agent or administrator."
      end
    end

    describe "when csat reason code arturo is enabled" do
      before do
        @request.account.stubs(:has_csat_reason_code?).returns(true)
        get :show
      end

      it "displays csat reason code option" do
        assert_includes @response.body, "Ask a follow-up question after a bad rating"
      end
    end

    describe "when csat reason code arturo is disabled" do
      before do
        @request.account.stubs(:has_csat_reason_code?).returns(false)
        get :show
      end

      it "does not display csat reason code option" do
        refute_includes @response.body, "Ask a follow-up question after a bad rating"
      end
    end

    describe 'satisfaction prediction settings' do
      describe 'when arturo feature is enabled' do
        let(:dc_url) { "http://localhost:8500/v1/kv/global/pod/1/dc?raw" }

        before do
          Arturo.enable_feature!(:satisfaction_prediction)
        end

        it "displays satisfaction prediction option" do
          stub_request(:get, dc_url).to_return(body: "my-dc")
          stub_request(:get, "http://pod-1.satisfaction-prediction-app.service.consul:3000/api/v2/satisfaction_prediction/private/prediction/account/#{@minimum_account.id}/available")
          get :show
          assert_includes @response.body, I18n.t('txt.admin.views.settings.customers._satisfaction.prediction_score_enable')
        end

        describe 'for enterprise customers' do
          before do
            # Let the account subscription act like an extra large account.
            Subscription.any_instance.stubs(:has_satisfaction_prediction?).returns(true)
          end

          it 'displays associated description if model is available' do
            TicketPrediction.stubs(:satisfaction_prediction_model_available?).with(@request.account).returns(true)
            get :show

            assert_includes @response.body, I18n.t('txt.admin.views.settings.customers._satisfaction.prediction_descriptions.is_enterprise.has_model')
          end

          it 'displays associated description if model is not available' do
            TicketPrediction.stubs(:satisfaction_prediction_model_available?).with(@request.account).returns(false)
            get :show

            assert_includes @response.body, I18n.t('txt.admin.views.settings.customers._satisfaction.prediction_descriptions.is_enterprise.no_model')
          end
        end

        describe 'for non enterprise customers' do
          before do
            # Let the account subscription act like a small account.
            Subscription.any_instance.stubs(:has_satisfaction_prediction?).returns(false)
          end

          it 'displays associated description if model is available' do
            TicketPrediction.stubs(:satisfaction_prediction_model_available?).with(@request.account).returns(true)
            get :show

            assert_includes @response.body, I18n.t('txt.admin.views.settings.customers._satisfaction.prediction_descriptions.not_enterprise.has_model')
          end

          it 'displays associated description if model is not available' do
            TicketPrediction.stubs(:satisfaction_prediction_model_available?).with(@request.account).returns(false)
            get :show

            assert_includes @response.body, I18n.t('txt.admin.views.settings.customers._satisfaction.prediction_descriptions.not_enterprise.no_model')
          end
        end
      end

      describe 'when arturo feature is disabled' do
        before do
          Arturo.disable_feature!(:satisfaction_prediction)
        end

        it "does not display satisfaction prediction option" do
          get :show
          refute_includes @response.body, "Enable satisfaction rating prediction"
        end
      end
    end

    describe "when account has user_and_organization_fields feature" do
      before do
        @request.account.stubs(:has_user_and_organization_fields?).returns(true)
        get :show
      end

      it "displays user/organization tags setting" do
        assert_includes @response.body, "Tags on users and organizations"
      end
    end

    describe "when account does not have user_and_organization_fields feature" do
      before do
        @request.account.stubs(:has_user_and_organization_fields?).returns(false)
        get :show
      end

      it "does not display user/organization tags setting" do
        refute_includes @response.body, "Tags on users and organizations"
      end
    end
  end

  describe "#update_settings" do
    before do
      @minimum_account.update_attribute(:is_open, false)
    end

    it "allows setting of account attributes" do
      put :update_settings, params: { account: { is_open: true, domain_whitelist: "wibble.com" } }
      assert(@minimum_account.reload.is_open?)
      assert_equal "wibble.com", @minimum_account.domain_whitelist
    end

    it "refuses to create whitelists & blacklists which are more than 10,000 bytes" do
      whitelist = "a" * 10_001
      put :update_settings, params: { account: { domain_whitelist: whitelist } }
      refute_equal whitelist, @minimum_account.domain_whitelist
    end

    describe "update to welcome email and verification email text" do
      describe "when allow_email_template_customization? returns false" do
        before do
          Account.any_instance.stubs(:allow_email_template_customization?).returns(false)
        end

        it "does not update the text" do
          put :update_settings, params: { account: {
            signup_email_text: "change",
            verify_email_text: "change"
          } }
          refute_equal "change", @minimum_account.signup_email_text
          refute_equal "change", @minimum_account.verify_email_text
        end
      end

      describe "when allow_email_template_customization? returns true" do
        before do
          Account.any_instance.stubs(:allow_email_template_customization?).returns(true)
        end

        it "updates the text" do
          put :update_settings, params: { account: {
            signup_email_text: "change",
            verify_email_text: "change"
          } }
          assert_equal "change", @minimum_account.signup_email_text
          assert_equal "change", @minimum_account.verify_email_text
        end
      end
    end
  end

  describe "when an account setting is passed " do
    it "updates the corresponding field" do
      put :update_settings, params: { account: {
          settings: {
              list_help_centers_in_account_emails: false,
              captcha_required: true
          },
          is_end_user_phone_number_validation_enabled: true,
          signup_page_text: "change",
          is_end_user_profile_visible: true,
          is_end_user_password_change_visible: true,
          is_signup_required: false,
          is_welcome_email_when_agent_register_enabled: false
      } }
      assert @minimum_account.is_end_user_phone_number_validation_enabled
      assert_equal "change", @minimum_account.signup_page_text
      assert @minimum_account.is_end_user_profile_visible
      assert @minimum_account.is_end_user_password_change_visible
      assert_equal false, @minimum_account.is_signup_required
      assert_equal false, @minimum_account.is_welcome_email_when_agent_register_enabled
      assert @minimum_account.settings.captcha_required?
      assert_equal false, @minimum_account.settings.list_help_centers_in_account_emails?
    end
  end

  describe "#update_satisfaction" do
    before do
      @minimum_account.automations.delete_all
      @minimum_account.settings.customer_satisfaction = 0
    end

    describe "when csat reason code and customer satisfaction is enabled" do
      before do
        @minimum_account.settings.customer_satisfaction = 1
        @minimum_account.settings.csat_reason_code      = true
        @minimum_account.save!
      end

      it "allows disabling csat reason code" do
        assert(@minimum_account.reload.settings.customer_satisfaction?)
        assert(@minimum_account.reload.settings.csat_reason_code?)
        put :update_satisfaction, params: { account: { settings: { customer_satisfaction: 1, csat_reason_code: false } } }
        assert(@minimum_account.reload.settings.customer_satisfaction?)
        assert_equal false, @minimum_account.reload.settings.csat_reason_code?
      end

      describe "when csat_reason_code_admin is enabled" do
        let(:reason) { FactoryBot.create(:satisfaction_reason, account: @minimum_account, value: "some reason") }
        let(:system_reason) { FactoryBot.create(:satisfaction_reason, account: @minimum_account, system: true) }

        before do
          @minimum_account.stubs(:has_csat_reason_code_admin?).returns(true)
          put :update_satisfaction, params: { account: {
            settings: {
              customer_satisfaction: 1,
              csat_reason_code: true
            },
            satisfaction_reasons: JSON.generate(satisfaction_reasons)
          } }
        end

        describe "when there is a new reason" do
          describe "and it is active and has a value" do
            let(:satisfaction_reasons) { [{active: true, id: -1, value: "a value"}] }

            it "adds it to the account and activates it" do
              assert_equal @minimum_account.default_brand.satisfaction_reasons.last.value, "a value"
            end

            it "doesn't store validation messages in flash[:error]" do
              assert flash[:error].nil?
            end
          end

          describe "and it is invalid" do
            let(:satisfaction_reasons) { [{active: true, id: -1, value: nil}] }

            it "stores the validation messages in flash[:error]" do
              flash[:error].must_include I18n.t('txt.admin.controllers.settings.customers_controller.failed_to_update_satisfaction_settings')
            end
          end

          describe "and it is inactive" do
            let(:satisfaction_reasons) { [{active: false, id: -1, value: "a value"}] }

            it "does not activate on the account" do
              refute_equal @minimum_account.default_brand.satisfaction_reasons.last.value, "a value"
            end
          end

          describe "when it when contains html" do
            let(:satisfaction_reasons) { [{active: true, id: -1, value: "a value<svg/onLoad=alert(document.domain)>"}] }

            it "strip tags" do
              assert_equal "a value", @minimum_account.default_brand.satisfaction_reasons.last.value
            end
          end
        end

        describe "when there is a reason that has been edited" do
          describe "and it is not a system reason" do
            let(:satisfaction_reasons) do
              [{ id: reason.id, active: true, edited: true, value: "new value" }]
            end

            it "updates the value of the reason" do
              assert_equal @minimum_account.satisfaction_reasons.find(reason.id).value, "new value"
            end
          end

          describe "and it is a system reason" do
            let(:satisfaction_reasons) do
              [{ id: system_reason.id, active: true, edited: true, value: "new value" }]
            end

            it "does not update the value of the reason" do
              assert_equal @minimum_account.satisfaction_reasons.find(system_reason.id).value, system_reason.value
            end
          end

          describe "when contains html" do
            let(:satisfaction_reasons) do
              [{ id: reason.id, active: true, edited: true, value: "new value<svg/onLoad=alert(document.domain)>" }]
            end
            it "strip tags" do
              assert_equal "new value", @minimum_account.satisfaction_reasons.find(reason.id).value
            end
          end
        end

        describe "when there is a reason that has been deleted" do
          describe "and it is not a system reason" do
            let(:satisfaction_reasons) do
              [{ id: reason.id, deleted: true }]
            end

            it "sets the status of reason to deleted" do
              assert(Satisfaction::Reason.with_deleted { @minimum_account.satisfaction_reasons.find_by_reason_code(reason.reason_code) }.deleted?)
            end
          end

          describe "and it is a system reason" do
            let(:satisfaction_reasons) do
              [{ id: system_reason.id, deleted: true }]
            end

            it "does not set the status of reason to deleted" do
              assert_equal false, @minimum_account.satisfaction_reasons.find(system_reason.id).deleted?
            end
          end
        end

        describe "when there is an active reason" do
          let(:satisfaction_reasons) { [{active: true, id: reason.id}] }

          it "activates the reason" do
            assert_includes @minimum_account.default_brand.satisfaction_reasons, reason
          end
        end

        describe "when there is an active system reason" do
          let(:reason) { FactoryBot.create(:satisfaction_reason, account: @minimum_account, value: "some reason", system: true) }
          let(:satisfaction_reasons) { [{active: true, id: reason.id}] }

          it "activates the reason" do
            assert_includes @minimum_account.default_brand.satisfaction_reasons, reason
          end
        end

        describe "when there is a deactivated reason" do
          let(:satisfaction_reasons) { [{active: false, id: reason.id}] }

          before do
            @minimum_account.satisfaction_reason_brand_restrictions.create(satisfaction_reason: reason, brand: @minimum_account.default_brand)
            put :update_satisfaction, params: { account: {
              settings: {
                customer_satisfaction: 1,
                csat_reason_code: true
              },
              satisfaction_reasons: JSON.generate(satisfaction_reasons)
            } }
          end

          it "deactivates the reason" do
            refute_includes @minimum_account.default_brand.satisfaction_reasons, reason
          end
        end
      end
    end

    it "allows enabling customer satisfaction" do
      put :update_satisfaction, params: { account: { settings: { customer_satisfaction: 1 }} }
      assert(@minimum_account.reload.settings.customer_satisfaction?)
      assert_equal false, @minimum_account.reload.settings.public_customer_satisfaction?
      put :update_satisfaction, params: { account: { settings: { customer_satisfaction: 0 }} }
      assert_equal false, @minimum_account.reload.settings.customer_satisfaction?
      assert_equal false, @minimum_account.reload.settings.public_customer_satisfaction?
      put :update_satisfaction, params: { account: { settings: { customer_satisfaction: 1, public_customer_satisfaction: 1 }} }
      assert(@minimum_account.reload.settings.customer_satisfaction?)
      assert(@minimum_account.reload.settings.public_customer_satisfaction?)
    end

    it "allows enabling csat reason code" do
      assert_equal false, @minimum_account.reload.settings.customer_satisfaction?
      assert_equal false, @minimum_account.reload.settings.csat_reason_code?
      put :update_satisfaction, params: { account: { settings: { customer_satisfaction: 1, csat_reason_code: 1 }} }
      assert(@minimum_account.reload.settings.customer_satisfaction?)
      assert(@minimum_account.reload.settings.csat_reason_code?)
    end

    it "allows enabling satisfaction prediction" do
      assert_equal false, @minimum_account.reload.settings.customer_satisfaction?
      assert_equal false, @minimum_account.reload.settings.satisfaction_prediction?
      put :update_satisfaction, params: { account: { settings: { customer_satisfaction: 1, satisfaction_prediction: 1 }} }
      assert(@minimum_account.reload.settings.customer_satisfaction?)
      assert(@minimum_account.reload.settings.satisfaction_prediction?)
    end

    it 'causes an automation and view to be created if necessary' do
      assert_equal(0, @minimum_account.automations.count(:all))
      assert_difference('@minimum_account.automations.count(:all)') do
        put :update_satisfaction, params: { account: { settings: { customer_satisfaction: 1 }} }
      end
    end

    it "does not cause removed automation or view to be recreated if setting is resubmitted" do
      assert_equal(0, @minimum_account.automations.count(:all))
      put :update_satisfaction, params: { account: { settings: { customer_satisfaction: 1 }} }
      assert_equal(1, @minimum_account.automations.count(:all))
      @minimum_account.reload.automations[0].delete
      assert_no_difference('@minimum_account.automations.count(:all)') do
        put :update_satisfaction, params: { account: { settings: { customer_satisfaction: 1 }} }
      end
    end
  end

  describe "#ensure_reasons_present" do
    before do
      @minimum_account.stubs(:has_csat_reason_code_admin?).returns(true)
    end

    describe "when there are no satisfaction reasons" do
      it "creates the system reasons for the account" do
        refute_equal Satisfaction::Reason::SYSTEM_REASONS.count, @minimum_account.satisfaction_reasons.count(:all)
        get :show
        assert_equal Satisfaction::Reason::SYSTEM_REASONS.count, @minimum_account.satisfaction_reasons.count(:all)
      end
    end
  end
end
