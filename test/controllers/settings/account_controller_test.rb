require_relative "../../support/test_helper"
require_relative "../../support/brand_test_helper"
require 'zendesk_billing_core/zuora/client'

SingleCov.covered! uncovered: 34

describe Settings::AccountController do
  include BrandTestHelper
  fixtures :accounts, :users, :payments, :translation_locales, :addresses,
    :user_identities, :credit_cards, :subscriptions, :tickets, :rules,
    :role_settings, :recipient_addresses, :subscription_features

  before do
    @minimum_account = accounts(:minimum)
    @request.account = @minimum_account
    @minimum_admin   = users(:minimum_admin)

    @japanese = translation_locales(:japanese)
    ZendeskExceptions::Logger.stubs(:record_or_raise)
  end

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: "settings/account") do |request|
    request.should_route :put,  "/settings/account/update_account_owner", action: 'update_account_owner'
    request.should_route :put,  "/settings/account/update_address",       action: 'update_address'
    request.should_route :put,  "/settings/account/update_branding",      action: 'update_branding'
    request.should_route :put,  "/settings/account/update_localization",  action: 'update_localization'
    request.should_route :put,  "/settings/account/update_benchmark",     action: 'update_benchmark'
    request.should_route :get,  "/settings/account/invoice",              action: 'invoice'
  end

  should_be_unauthorized_when_not_logged_in([:update_branding])

  describe "#zuora_managed_and_not_via_partner?" do
    describe "when account is zuora managed" do
      before { @minimum_account.stubs(zuora_managed?: true) }

      describe "when account is via partner" do
        before { Account.any_instance.stubs(via_partner?: true) }
        it { refute @controller.send(:zuora_managed_and_not_via_partner?) }
      end

      describe "when account is not via partner" do
        before { Account.any_instance.stubs(via_partner?: false) }
        it { assert @controller.send(:zuora_managed_and_not_via_partner?) }
      end
    end

    describe "when account is not zuora managed" do
      before { @minimum_account.stubs(zuora_managed?: false) }
      it { refute @controller.send(:zuora_managed_and_not_via_partner?) }
    end
  end

  describe "#audits" do
    before do
      @request.account.subscription.update_attribute(:plan_type, SubscriptionPlanType.ExtraLarge) # make it an enterprise account
      Zendesk::Features::SubscriptionFeatureService.new(@request.account).execute!
    end

    as_an_admin do
      it "shows" do
        get :audits
        assert_response :success
        assert assigns(:events)
      end

      it "shows deleted tickets" do
        deleted_ticket = Ticket.with_deleted { tickets(:minimum_deleted) }

        CIA::Event.create(
          account: accounts(:minimum),
          actor: users(:minimum_agent),
          source: deleted_ticket,
          action: "destroy"
        )

        get :audits

        assert_response :success
        assert_equal "Ticket", assigns(:events).first.source_type
        assert_equal deleted_ticket.id, assigns(:events).first.source_id
      end

      it "shows exact type for rules" do
        CIA::Event.create(
          account: accounts(:minimum),
          actor: users(:minimum_agent),
          source: Trigger.first,
          action: "destroy"
        )

        get :audits

        assert_response :success
        assert_equal "Trigger", assigns(:events).map(&:source_type).first
      end

      it "shows labels correctly" do
        CIA::Event.create(
          account: accounts(:minimum),
          actor: users(:minimum_agent),
          source: Trigger.first,
          action: "destroy",
          source_display_name: "Incidenti con priorità < urgente"
        )

        get :audits

        assert_response :success
        assert_includes @response.body, "Incidenti con priorità &lt; urgente"
        assert_equal "Trigger", assigns(:events).map(&:source_type).first
      end

      it "shows rule source_type for deleted rules" do
        CIA::Event.create(
          account: accounts(:minimum),
          actor: users(:minimum_agent),
          source_type: "Rule",
          source_id: 122345,
          action: "destroy"
        )

        get :audits

        assert_response :success
        assert_equal "Rule", assigns(:events).map(&:source_type).first
      end

      it "accepts a created_at date range" do
        params = {created_at: ["2016-01-11 05:00:00", "2016-01-12 04:59:59"]}

        get :audits, params: { filter: params }
        assert_response :success
      end
    end

    it "does not show to agents" do
      login('minimum_agent')
      get :audits
      assert_response :forbidden
    end
  end

  describe '#show' do
    it 'requires SSL' do
      https!(false)
      get :show
      assert_response :redirect
    end

    describe 'when not logged in' do
      before { get :show }
      it('responds with unauthorized') { assert_response :unauthorized }
    end

    describe 'when logged in as an admin' do
      before do
        login(@minimum_admin)
        get :show
      end

      it('responds with ok') { assert_response :ok }
      should_assign_to :account
      should_assign_to :branding
      should_assign_to :locales
      should_assign_to :benchmark
      should_assign_to :subscription

      should_render_template :show
    end

    describe "when logged in" do
      before do
        login(@minimum_admin)
      end

      describe "Branding tab" do
        before do
          @minimum_account.cms_texts.create!(
            name: "html",
            fallback_attributes: {
              is_fallback: true,
              nested: true,
              account: @minimum_account,
              value: "<div>some dynamic content</div>",
              translation_locale_id: 1
            }
          )
        end

        describe "when lotus branding is not active" do
          before do
            get :show
          end

          it "shows the activate branding description" do
            assert_includes @response.body, '<p id="activate_branding_description">'
            assert_not_includes @response.body, '<a id="preview_color_link">'
          end

          it "should not render 'dynamic_content_data' in json blob" do
            assert_not_includes @response.body, "some dynamic content"
          end
        end

        describe "when lotus branding is active" do
          before do
            @minimum_account.settings.activate_lotus_branding = true
            @minimum_account.save!
            get :show
          end

          it "shows the preview color link" do
            assert_not_includes @response.body, '<p id="activate_branding_description">'
            assert_includes @response.body, '<a id="preview_color_link">'
          end

          it "should render 'dynamic_content_data' in json blob" do
            assert_includes @response.body, "some dynamic content"
          end
        end
      end

      describe "Localization tab" do
        describe "timezone" do
          it "shows the same timezone offset as lotus" do
            time_zone = ActiveSupport::TimeZone["Melbourne"]
            get :show

            assert_includes @response.body, "(GMT#{time_zone.now.formatted_offset}) #{time_zone.name}"
          end
        end

        describe "languagues for selection" do
          let(:spanish) { translation_locales(:spanish) }
          let(:english) { translation_locales(:english_by_zendesk) }
          let(:french)  { translation_locales(:french) }

          before do
            Account.any_instance.stubs(:locales_for_selection).returns([spanish, english, french])
            Account.any_instance.stubs(:allowed_translation_locales).returns([spanish, english, @japanese])
          end

          it "shows the default languages and any additionally enabled languages" do
            get :show

            assert_includes @controller.instance_variable_get(:@locales), spanish
            assert_includes @controller.instance_variable_get(:@locales), english
            assert_includes @controller.instance_variable_get(:@locales), french
            assert_includes @controller.instance_variable_get(:@locales), @japanese
            assert_equal 4, @controller.instance_variable_get(:@locales).count
          end
        end
      end

      describe "invoices and contacts section" do
        before do
          Account.any_instance.stubs(via_partner?: via_partner)
        end

        describe "address section" do
          let(:via_partner) { true }
          before { get :show }

          it "is hidden" do
            assert_template partial: "_address", count: 0
          end
        end

        describe "invoices section" do
          describe 'when account is via partner' do
            let(:via_partner) { true }
            before { get :show }

            it "is hidden" do
              assert_template partial: "_invoices", count: 0
            end
          end

          describe 'when account is not via partner' do
            let(:via_partner) { false }
            before { get :show }

            it "is hidden" do
              assert_template partial: "_invoices", count: 0
            end
          end
        end

        describe "owner section" do
          let(:via_partner) { true }
          before { get :show }

          it "is visible" do
            assert_template partial: "_owner_multiproduct", count: 1
          end
        end

        describe "address and invoices section" do
          let(:via_partner) { false }

          before do
            Account.any_instance.stubs(zuora_managed?: zuora_managed)
          end

          describe 'when zuora managed and not via parter' do
            let(:zuora_managed) { true }
            before { get :show }

            it "is visible" do
              assert_template partial: "_address_and_invoices", count: 1
            end
          end

          describe 'when not (zuora managed and not via parter)' do
            let(:zuora_managed) { false }
            before { get :show }

            it "is hidden" do
              assert_template partial: "_address_and_invoices", count: 0
            end
          end
        end
      end

      describe "when obtaining a reference to the account's zuora_subscription record" do
        describe "and account subscription is not zuora managed" do
          before do
            Subscription.any_instance.stubs(zuora_managed?: false)
            get :show
            assert_response :ok
          end

          should_not_assign_to :zuora_subscription
        end

        describe "and account subscription is zuora managed" do
          before do
            Account.any_instance.stubs(via_partner?: via_partner)

            zuora_subscription = stub(zuora_account_id: anything, in_dunning?: false)
            Subscription.any_instance.stubs(zuora_managed?: true, zuora_subscription: zuora_subscription)

            invoices = [
              stub(id: "123", invoice_number: "123", invoice_date: "2013-01-01", due_date: "2013-01-01", amount: "0"),
              stub(id: "456", invoice_number: "456", invoice_date: "2013-01-01", due_date: "2011-01-01", amount: "0"),
              stub(id: "789", invoice_number: "789", invoice_date: "2013-01-01", due_date: "2014-01-01", amount: "0")
            ]
            ZendeskBillingCore::Zuora::Client.any_instance.stubs(get_invoices!: invoices)
          end

          describe 'and not via_partner' do
            let(:via_partner) { false }

            before do
              get :show
              assert_response :ok
            end

            should_not_assign_to :zuora_subscription
          end

          describe 'and via_partner' do
            let(:via_partner) { true }

            before do
              get :show
              assert_response :ok
            end

            should_assign_to :zuora_subscription
          end
        end
      end

      describe "when account is not zuora managed" do
        before do
          Subscription.any_instance.stubs(zuora_managed?: false)
          get :show
        end

        it('responds with ok') { assert_response :ok }
        should_not_assign_to :zuora_subscription
      end

      describe "when account is zuora managed" do
        before do
          Account.any_instance.stubs(via_partner?: via_partner)
          live_zuora_subscription = stub(japanese: anything, plan_type: anything, plan_name: anything, max_agents: anything)
          zuora_subscription      = stub(get_live_zuora_subscription!: live_zuora_subscription, zuora_account_id: anything, in_dunning?: false)
          Subscription.any_instance.stubs(zuora_managed?: true, zuora_subscription: zuora_subscription)
          ZendeskBillingCore::Zuora::Client.any_instance.stubs(get_invoices!: [])
        end

        describe 'and not via_partner' do
          let(:via_partner) { false }

          before { get :show }

          it('responds with ok') { assert_response :ok }
          should_not_assign_to :zuora_subscription

          it "displays the redirect notice to Lotus' agent account subscription page" do
            assert_select "#lotus_agent_admin_subscription_redirect_notice > a#redirect_link", count: 1
          end

          it "does not display the subscription details" do
            assert_select "#subscription_details", count: 0
          end
        end

        describe 'and via_partner' do
          let(:via_partner) { true }

          before { get :show }

          it('responds with ok') { assert_response :ok }
          should_assign_to :zuora_subscription

          it "displays the redirect notice to Lotus' agent account subscription page" do
            assert_select "#lotus_agent_admin_subscription_redirect_notice > a#redirect_link", count: 1
          end

          it "does not display the subscription details" do
            assert_select "#subscription_details", count: 0
          end
        end
      end
    end

    describe "for an account with a manual discount" do
      before do
        @subscription = @minimum_account.subscription
        @subscription.update_attribute(:manual_discount, 20)
        login(@minimum_admin)
        get :show
      end

      it('responds with ok') { assert_response :ok }
    end
  end

  describe "updates" do
    before do
      login(@minimum_admin)
      @subscription = subscriptions(:minimum)
      @subscription.update_attribute(:pricing_model_revision, ZBC::Zendesk::PricingModelRevision::NEW_FORUMS)
      @subscription.reload
    end

    describe "#update_subdomain" do
      before do
        @old_subdomain = @minimum_account.subdomain
      end

      describe "given a sandbox account" do
        before do
          @minimum_account.stubs(is_sandbox?: true)
          @minimum_account.stubs(sandbox_master: @minimum_account) # sandboxes reference their master
          put :update_subdomain, params: { account: { subdomain: 'ramen' } }
        end

        it "does not update the subdomain" do
          @minimum_account.reload
          assert_equal @old_subdomain, @minimum_account.subdomain
          assert_equal @old_subdomain, @minimum_account.route.subdomain
        end
      end

      describe "given an available subdomain" do
        before { put :update_subdomain, params: { account: { subdomain: 'Ramen' } } }

        it "updates the subdomain and backup address" do
          @minimum_account.reload
          assert_equal 'ramen', @minimum_account.subdomain
          assert_equal 'ramen', @minimum_account.route.subdomain
          assert_equal 'support@ramen.zendesk-test.com', @minimum_account.recipient_addresses.detect(&:backup?).email
        end
      end

      describe "given an unavailable subdomain" do
        before { put :update_subdomain, params: { account: { subdomain: 'thankyoumachine' } } }

        it "does not update the subdomain" do
          @minimum_account.reload
          assert_equal @old_subdomain, @minimum_account.subdomain
          assert_equal @old_subdomain, @minimum_account.route.subdomain
        end
      end

      describe "given the same subdomain" do
        before { put :update_subdomain, params: { account: { subdomain: @old_subdomain } } }

        it "does not update the subdomain" do
          @minimum_account.reload
          assert_equal @old_subdomain, @minimum_account.subdomain
          assert_equal @old_subdomain, @minimum_account.route.subdomain
        end

        it "does not render any error in the show page" do
          assert_redirected_to settings_account_url(anchor: 'branding')
          assert_nil flash[:error]
        end
      end

      describe "given no params" do
        before { put :update_subdomain }

        it "does not update the subdomain" do
          @minimum_account.reload
          assert_equal @old_subdomain, @minimum_account.subdomain
          assert_equal @old_subdomain, @minimum_account.route.subdomain
        end
      end
    end

    describe "#update_branding" do
      it "supports changing the account branding" do
        assert @minimum_account.branding.header_color != "FEDABE"
        put :update_branding, params: { branding: { header_color: "FEDABE" } }
        assert_equal "FEDABE", @minimum_account.reload.branding.header_color
      end

      it "supports changing the account branding via SSL" do
        assert @minimum_account.branding.header_color != "FEDACE"
        put :update_branding, params: { branding: { header_color: "FEDACE"} }
        assert_equal "FEDACE", @minimum_account.reload.branding.header_color
      end

      it "allows changing the account name" do
        put :update_branding, params: { account: { name: "FEDABE" } }
        assert_equal "FEDABE", @minimum_account.reload.name
      end

      it "allows changing the account agent route" do
        @minimum_account.subscription.update_attribute(:plan_type, SubscriptionPlanType.ExtraLarge)
        brand = create_brand(@minimum_account, name: 'New Brand', subdomain: 'newbrand')

        put :update_branding, params: { account: { route_id: brand.route_id } }
        assert_equal brand.route_id, @minimum_account.reload.route_id
        assert_equal brand.subdomain, @minimum_account.reload.subdomain
      end

      it "renders the show page if there were logo errors" do
        HeaderLogo.expects(:set_for_account).raises(ActiveRecord::RecordInvalid.new(HeaderLogo.new))
        put :update_branding
        assert_template :show
        assert_equal "Failed to set one or more logos. Try another format for: header logo", flash[:error]
      end

      it "renders a retry error if upload fails" do
        HeaderLogo.expects(:set_for_account).raises(::Technoweenie::AttachmentFu::NoBackendsAvailableException.new)
        put :update_branding
        assert_template :show
        assert_equal "Failed to set one or more logos temporarily. Please retry.", flash[:error]
      end

      it "renders the show page if there were errors saving the account" do
        I18n.locale = nil
        put :update_branding, params: { account: { name: "" } }
        assert_template :show
        assert_equal "Failed to update account<ul><li><strong>Name</strong> cannot be blank</li></ul>", flash[:error].sub("can't", "cannot"), "Current locale is #{I18n.locale}"
      end
    end

    describe "#update_localization" do
      before do
        @minimum_account.update_attributes(time_zone: "Melbourne", uses_12_hour_clock: true)
      end

      it "allows setting time zone and format" do
        put :update_localization, params: { account: { time_zone: "Brasilia", uses_12_hour_clock: "false" } }
        assert_redirected_to settings_account_url(anchor: "localization")
        @minimum_account.reload

        assert_equal "Brasilia", @minimum_account.time_zone
        assert_equal false, @minimum_account.uses_12_hour_clock
      end

      it "allows setting the default language" do
        other_locale_id = translation_locales(:brazilian_portuguese).id
        assert other_locale_id != @minimum_account.locale_id, 'account already has the locale_id'

        put :update_localization, params: { account: { locale_id: other_locale_id } }

        @minimum_account.reload
        assert_equal other_locale_id, @minimum_account.locale_id
      end

      it "allows setting additional languages" do
        assert_equal [], @minimum_account.allowed_translation_locales
        put :update_localization, params: { account: { allowed_translation_locale_ids: [@japanese.id].map(&:to_s) } }
        assert_redirected_to settings_account_url(anchor: "localization")
        assert_equal [@japanese], @minimum_account.allowed_translation_locales
      end

      it "allows deselecting all languages" do
        english = translation_locales(:english_by_zendesk)
        portuguese = translation_locales(:brazilian_portuguese)
        japanese = translation_locales(:japanese)

        @minimum_account.allowed_translation_locales = [english, portuguese, japanese]
        @minimum_account.locale_id = english.id
        @minimum_account.save!

        assert_equal [english, portuguese, japanese], @minimum_account.allowed_translation_locales
        put :update_localization, params: { account: { locale_id: english.id} }
        assert_equal [english], @minimum_account.allowed_translation_locales
      end
    end

    describe "#update_address" do
      it "updates address fields and call update_zuora_address_and_vat" do
        @controller.expects(:update_zuora_address_and_vat)
        put :update_address, params: { address: { zip: "123" } }
        assert_equal "123", @minimum_account.address(true).zip
      end
    end

    describe "#update_account_owner" do
      before do
        login(@minimum_account.owner)
        put :update_account_owner, params: { account: { owner_id: 12345678 } }
      end

      it "updates account owner" do
        assert_equal 12345678, @minimum_account.owner_id
      end
    end

    describe "#update_benchmark" do
      before do
        @minimum_account.create_survey_response unless @minimum_account.survey_response.present?
        @industry = Account::SurveyResponse::INDUSTRIES.values.last

        put :update_benchmark, params: { account_survey_response: { industry: @industry } }
      end

      should_redirect_to("the edit survey-response page") { settings_account_path(anchor: "benchmark") }

      it "sets the accounts survey response" do
        assert_equal @industry, @minimum_account.survey_response(true).industry
      end
    end
  end
end
