require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 6

describe Settings::ApiTokensController do
  fixtures :accounts, :users, :role_settings

  before do
    @minimum_admin   = users(:minimum_admin)
    @minimum_account = accounts(:minimum)
    @request.account = @minimum_account

    login(@minimum_admin)
  end

  should_include_module Zendesk::LotusEmbeddable

  describe "#create" do
    it "creates a new api token" do
      assert_difference @minimum_account.api_tokens do
        post :create
      end
    end

    it "fails gracefully" do
      bad_response = stub(create: false)
      Account.any_instance.stubs(:api_tokens).returns(bad_response)
      post :create
      assert_response :not_acceptable
    end

    it "returns the full length API token (needed for displaying in UI)" do
      post :create
      json = JSON.parse(response.body)
      assert_equal json["token"].size, 40
    end

    describe "when API token access is disabled" do
      it "enables api token access" do
        refute @minimum_account.settings.reload.api_token_access?
        post :create
        assert @minimum_account.settings.reload.api_token_access?
      end
    end
  end

  describe "#destroy" do
    it "does not be valid and not blow up with inexistent id" do
      @controller.stubs(:current_brand).returns(@request.account.brands.first)
      delete :destroy, params: { id: '1234567' }
      assert_response 404
    end

    describe "with db token" do
      before do
        @token = @minimum_account.api_tokens.create!
        @token_value = @token.temp_full_value
        delete :destroy, params: { id: @token.id }
      end

      it "destroys api token" do
        assert_nil @minimum_account.api_tokens.find_by_id(@token.id)
      end
    end
  end
end
