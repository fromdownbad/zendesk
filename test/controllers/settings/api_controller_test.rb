require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Settings::ApiController do
  fixtures :all

  before do
    @minimum_admin   = users(:minimum_admin)
    @minimum_account = accounts(:minimum)
    @request.account = @minimum_account

    login(@minimum_admin)
  end

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: "settings/api") do |request|
    request.should_route :put, "/settings/api/update_settings", action: 'update_settings'
  end

  describe "#show" do
    it "responds with the page" do
      get :show
      assert_response :ok
    end

    describe 'Permissions' do
      before do
        @user = users(:minimum_agent)
        @user.permission_set = @user.account.permission_sets.create!(name: "Test")

        @user.account.stubs(:has_permission_sets?).returns(true)

        @request.account = @user.account
        login(@user)
      end

      describe 'Given an agent with the ability to manage API settings' do
        before do
          @user.permission_set.permissions.enable(:extensions_and_channel_management)
          assert @user.can?(:edit, Access::Settings::Extensions)
        end

        it 'has access to manage API settings pages' do
          get :show
          assert_response :success, @response.body
        end
      end

      describe 'Given an agent without the ability to manage API settings' do
        before do
          @user.permission_set.permissions.disable(:extensions_and_channel_management)
          refute @user.can?(:edit, Access::Settings::Extensions)
        end

        it 'does not have access to manage API settings pages' do
          get :show
          assert_response :forbidden, @response.body
        end
      end
    end
  end

  describe "#update_settings" do
    before do
      @minimum_account.settings.api_token_access    = false
      @minimum_account.settings.api_password_access = false
      @minimum_account.save!
    end

    it "allows setting the portal attributes" do
      put :update_settings, params: { account: {
        settings: { api_token_access: "1"}
      } }
      assert(@minimum_account.reload.settings.api_token_access?)
      assert_equal false, @minimum_account.settings.api_password_access?
    end
  end
end
