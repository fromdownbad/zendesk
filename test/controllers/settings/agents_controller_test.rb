require_relative "../../support/test_helper"
require 'zendesk/radar_factory'

SingleCov.covered! uncovered: 3

describe Settings::AgentsController do
  fixtures :accounts, :users, :account_settings

  before do
    @minimum_admin   = users(:minimum_admin)
    @minimum_account = accounts(:minimum)
    @request.account = @minimum_account

    login(@minimum_admin)
  end

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: "settings/agents") do |request|
    request.should_route :put, "/settings/agents/update_settings", action: 'update_settings'
  end

  describe "#show" do
    it "responds with the page" do
      get :show
      assert_response :ok
    end
  end

  describe "#update_settings" do
    before do
      @minimum_account.settings.ticket_delete_for_agents = false
    end

    it "allows changing the agent settings" do
      assert_equal false, @minimum_account.reload.settings.ticket_delete_for_agents?
      put :update_settings, params: { account: { signature_template: "Blargh", settings: { ticket_delete_for_agents: "1"} } }
      assert(@minimum_account.reload.settings.ticket_delete_for_agents?)
      assert_equal "Blargh", @minimum_account.signature_template
      assert_equal "Blargh", @minimum_account.default_brand.signature_template
      @minimum_account.brands.each do |brand|
        assert_equal "Blargh", brand.signature_template
      end
    end

    it 'allows enabling polaris' do
      Apps::ChatAppInstallation.any_instance.stubs(:handle_disable_installation)
      login('minimum_admin')
      refute_predicate @minimum_account.settings, :polaris?

      put :update_settings, params: { account: { settings: { polaris: '1' }} }

      assert_redirected_to settings_agents_path(anchor: "settings")
      assert_predicate @minimum_account.settings, :polaris?
    end

    it 'allows disabling polaris' do
      Apps::ChatAppInstallation.any_instance.stubs(:handle_disable_installation)
      login('minimum_admin')
      @minimum_account.settings.enable(:polaris)
      @minimum_account.save!
      assert_predicate @minimum_account.settings, :polaris?

      put :update_settings, params: { account: { settings: { polaris: '0' } } }

      assert_redirected_to settings_agents_path(anchor: "settings")
      refute_predicate @minimum_account.settings, :polaris?
    end

    it "allows setting the customer context as default feature" do
      login('minimum_admin')
      refute @minimum_account.settings.customer_context_as_default?
      put :update_settings, params: { account: { settings: { customer_context_as_default: '1' }} }
      assert_redirected_to settings_agents_path(anchor: "settings")
      assert @minimum_account.settings.customer_context_as_default?
    end

    it "allows removing the customer context as default feature" do
      login('minimum_admin')
      @minimum_account.settings.enable(:customer_context_as_default)
      @minimum_account.save!
      assert @minimum_account.settings.customer_context_as_default?
      put :update_settings, params: { account: { settings: { customer_context_as_default: '0' }} }
      assert_redirected_to settings_agents_path(anchor: "settings")
      refute @minimum_account.settings.customer_context_as_default?
    end

    describe "#handle_polaris_onboarding" do
      def update_polaris_setting!(value)
        method_name = value == "on" ? :enable : :disable
        @minimum_account.settings.send(method_name, :polaris)
        @minimum_account.settings.save!
      end

      def get_onboarding_values(agents, setting)
        agents.map { |agent| agent.settings.send(setting) }
      end

      around do |t|
        Timecop.freeze { t.call }
      end

      before do
        Apps::ChatAppInstallation.any_instance.stubs(:handle_disable_installation)

        # to create elements for association @minimum_account.settings
        update_polaris_setting!("off")
        refute @minimum_account.settings.polaris

        # to create the UserSetting that will be later queried
        @minimum_account.agents.each do |agent|
          agent.settings.show_zero_state_tour_ticket = true
          agent.settings.save!
        end
      end

      describe 'conflicting onboarding settings' do
        let(:conflicting_settings) { [:show_zero_state_tour_ticket] }

        it "turns off conflicting onboarding experiences whenever polaris is enabled" do
          put :update_settings, params: { account: { settings: { polaris: "1" } } }

          conflicting_settings.each do |setting|
            assert_equal [false, false, false, false], get_onboarding_values(@minimum_account.agents, setting)
          end
        end

        it "does NOT change conflicting onboarding experiences whenever polaris is disabled" do
          put :update_settings, params: { account: { settings: { polaris: "0" } } }

          conflicting_settings.each do |setting|
            assert_equal [true, true, true, true], get_onboarding_values(@minimum_account.agents, setting)
          end
        end
      end

      describe 'polaris onboarding settings' do
        let(:setting) { :show_agent_workspace_onboarding }

        def update_polaris_updated_time!(time_updated_at)
          polaris_account_setting = @minimum_account.settings.find_by(name: "polaris")
          polaris_account_setting.updated_at = time_updated_at

          @minimum_account.settings = [polaris_account_setting]
          @minimum_account.settings.save!
        end

        def first_agent_finish_onboarding!
          first_agent_setting = @minimum_account.agents.first.settings
          first_agent_setting.send("#{setting}=", false)
          first_agent_setting.save!
          assert_equal false, first_agent_setting.send(setting)
        end

        it "does NOT change any agents' settings when another @ changes" do
          old_polaris_onboard_settings = get_onboarding_values(@minimum_account.agents, setting)
          put :update_settings, params: { account: { settings: { customer_context_as_default: '1' } } }

          assert_equal get_onboarding_values(@minimum_account.agents, setting), old_polaris_onboard_settings
        end

        it "does NOT change agents' settings if polaris is being disabled" do
          update_polaris_setting!("on")
          first_agent_finish_onboarding!
          old_agent_polaris_onboarding_settings = get_onboarding_values(@minimum_account.agents, setting)
          assert_equal [false, true, true, true], old_agent_polaris_onboarding_settings

          put :update_settings, params: { account: { settings: { polaris: "0" } } }
          assert_equal get_onboarding_values(@minimum_account.agents, setting), old_agent_polaris_onboarding_settings
        end

        it "does NOT change agents' settings if account disabled polaris 24 hours ago OR less" do
          update_polaris_updated_time!(12.minutes.ago)
          first_agent_finish_onboarding!
          assert_equal [false, true, true, true], get_onboarding_values(@minimum_account.agents, setting)

          put :update_settings, params: { account: { settings: { polaris: "1" } } }
          assert_equal false, @minimum_account.agents.first.settings.send(setting)
          assert_equal [false, true, true, true], get_onboarding_values(@minimum_account.agents, setting)
        end

        it "does change agents' settings if account disabled polaris MORE than 24 hours ago" do
          update_polaris_updated_time!(2.days.ago)
          first_agent_finish_onboarding!
          assert_equal false, @minimum_account.agents.first.settings.send(setting)
          assert_equal [false, true, true, true], get_onboarding_values(@minimum_account.agents, setting)

          put :update_settings, params: { account: { settings: { polaris: "1" } } }
          assert @minimum_account.agents.first.settings.send(setting), "The first agent should have had their onboarding for `show_agent_workspace_onboarding` toggled back to true."
          assert_equal [true, true, true, true], get_onboarding_values(@minimum_account.agents, setting)
        end
      end
    end
  end
end
