require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Settings::BaseController do
  fixtures :accounts, :users
  use_test_routes

  should_include_module Zendesk::LotusEmbeddable

  before do
    @request.account = accounts(:minimum)
  end

  should_be_unauthorized_when_not_logged_in([:show])

  describe "mobile requests" do
    before do
      login(:minimum_admin)
      get :show, format: "mobile_v2"
    end

    it('responds with bad_request') { assert_response :bad_request }
    it "responds with a mobile error message" do
      @response.body.must_include "Mobile agents are not supported for the settings pages"
    end
  end

  describe "access" do
    it "is prohibited for users" do
      login(:minimum_end_user)
      get :show

      assert_response :forbidden
    end

    it "is prohibited for agents" do
      login(:minimum_agent)
      get :show

      assert_response :forbidden
    end

    it "is allowed for admins" do
      login(:minimum_admin)
      get :show

      assert_redirected_to "/settings/account"
    end
  end
end
