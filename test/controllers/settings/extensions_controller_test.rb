require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 25

describe Settings::ExtensionsController do
  fixtures :accounts, :users, :account_property_sets, :subscriptions, :addresses, :groups, :role_settings, :user_identities

  before do
    # this is for targets, when testing if is created by app requirements
    stub_request(:get, "https://minimum.zendesk-test.com/api/v2/apps/requirements.json?includes=installation").
      to_return(body: { "requirements" => [] }.to_json, status: 200)

    @minimum_admin   = users(:minimum_admin)
    @minimum_account = accounts(:minimum)
    @request.account = @minimum_account

    login(@minimum_admin)
  end

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: "settings/extensions") do |request|
    request.should_route :post,   '/settings/extensions/update_ms_dynamics',        action: 'update_ms_dynamics'
    request.should_route :post,   "/settings/extensions/update_sugar_crm",          action: 'update_sugar_crm'
    request.should_route :delete, '/settings/extensions/remove_crm',                action: 'remove_crm'
    request.should_route :get,    '/settings/extensions/edit_salesforce_fields',    action: 'edit_salesforce_fields'
    request.should_route :post,   '/settings/extensions/save_salesforce_object',    action: 'save_salesforce_object'
    request.should_route :post,   '/settings/extensions/update_salesforce',         action: 'update_salesforce'
    request.should_route :put,    '/settings/extensions/update_salesforce',         action: 'update_salesforce'
    request.should_route :get,    '/settings/extensions/salesforce_related_fields', action: 'salesforce_related_fields'
    request.should_route :post,   '/settings/extensions/sort_salesforce_objects',   action: 'sort_salesforce_objects'
  end

  describe "ssl_allowed" do
    [:show, :edit_salesforce_object, :edit_salesforce_fields, :salesforce_related_fields, :edit_object_filter, :update_salesforce, :update_sugar_crm, :update_ms_dynamics, :remove_crm].each do |action|
      it "is true for #{action}" do
        assert @controller.class.ssl_allowed_actions.include?(action)
      end
    end
  end

  describe "ssl_required" do
    it "is true for salesforce_oauth_callback" do
      assert @controller.class.ssl_required_actions.include?(:salesforce_oauth_callback)
    end
  end

  describe "#show" do
    it "responds with the page" do
      get :show
      assert_response :ok
    end

    it "shows the crm tab" do
      get :show
      assert_select "div#crm", true
    end

    describe "#crm" do
      it "shows the pulldown to select crm" do
        get :show
        assert_response :ok
        assert_select "select#crm_integration", true
      end

      it "shows the salesforce init" do
        get :show
        assert_select 'div#salesforce.show'
        assert_select 'form#salesforce_integration_form'
      end

      describe "when crm_integrations are not configured" do
        before do
          Account.any_instance.stubs(:crm_integration).returns(nil)
        end

        it "shows the salesforce init" do
          get :show
          assert_select 'div#sugar.hide'
          assert_select 'div#salesforce.show'
          assert_select 'form#salesforce_integration_form'
        end
      end

      describe "when sugar and salesforce integrations are configured" do
        before do
          salesforce = SalesforceIntegration.new(account: @minimum_account)
          Account.any_instance.stubs(:crm_integration).returns(salesforce)
          Account.any_instance.stubs(:salesforce_integration).returns(salesforce)
          salesforce.stubs(:configured?).returns(true)
        end

        it "shows the salesforce form" do
          get :show
          assert_response :ok
          assert_select 'form#salesforce_integration_form', true
        end

        it "has a link to reconnect the integration" do
          get :show
          assert_response :ok
          assert_select 'a[href="/settings/extensions/connect_to_salesforce?salesforce_environment=production"]', true
        end
      end
    end
  end

  describe "#update_sugar_crm" do
    before do
      @sugar_attributes = { url: "http://instance.sugar.com", username: "user", password: "123456", active: true }
      SugarCrmIntegration.any_instance.stubs(:fetch_info_for).returns(true)
    end

    describe "when there was no sugar crm integration" do
      before do
        @request.account.sugar_crm_integration.try(:destroy)
        post :update_sugar_crm, params: { sugar_crm_integration: @sugar_attributes }
      end
      it "creates the sugar crm integration" do
        sugar = @request.account.sugar_crm_integration
        assert_equal @sugar_attributes[:url], sugar.url
        assert_equal @sugar_attributes[:username], sugar.username
        assert_equal @sugar_attributes[:password], sugar.password
        assert_equal @sugar_attributes[:active], sugar.active
      end
    end

    describe "when there was a sugar crm integration" do
      before do
        @request.account.create_sugar_crm_integration(@sugar_attributes)
        @sugar_attributes[:url] = "http://instance2.sugar.com"
        post :update_sugar_crm, params: { sugar_crm_integration: @sugar_attributes }
      end
      it "updates the sugar crm integration" do
        sugar = @request.account.sugar_crm_integration
        assert_equal "http://instance2.sugar.com", sugar.url
      end
    end

    describe "when there's an error in the params" do
      before do
        @sugar_attributes[:url] = nil
        @request.account.sugar_crm_integration.try(:destroy)
        post :update_sugar_crm, params: { sugar_crm_integration: @sugar_attributes }
      end
      should_render_template :show
      it "does not create the sugar crm integration" do
        assert_nil @request.account.reload.sugar_crm_integration
      end
    end

    describe "when there's an error in the connection test" do
      before do
        @request.account.sugar_crm_integration.try(:destroy)
        SugarCrmIntegration.any_instance.expects(:fetch_info_for).raises(RuntimeError, 'Bad login test')
        post :update_sugar_crm, params: { sugar_crm_integration: @sugar_attributes }
      end
      should_render_template :show
      it "does not create the sugar crm integration" do
        refute @request.account.reload.sugar_crm_integration
      end
    end

    describe "when used more than 20 times a minute with a valid target" do
      before do
        Timecop.freeze do
          20.times do
            Prop.throttle(:update_sugar_crm, @controller.send(:current_user).id)
          end

          SugarCrmIntegration.any_instance.stubs(send_test_message: nil)

          post :update_sugar_crm, params: { format: 'js', sugar_crm_integration: @sugar_attributes }
        end
      end

      it('responds with forbidden') { assert_response :forbidden }

      it "responds with rate limit text" do
        assert_equal "Forbidden by rate limit\nRate limit for sugar crm integration update requests exceeded, please wait before trying again.", @response.body
      end
    end
  end

  describe "#remove_crm" do
    before do
      @minimum_account.create_sugar_crm_integration(@sugar_attributes)
      delete :remove_crm
    end
    it "removes the selected crm" do
      assert_nil @minimum_account.reload.crm_integration
    end
  end

  describe "#update_salesforce" do
    let(:invalid_configured_delay) { '1' }

    before do
      salesforce_integration = SalesforceIntegration.new(account: @minimum_account)
      @minimum_account.stubs(:salesforce_integration).returns(salesforce_integration)
    end

    it "enables salesforce integration" do
      post :update_salesforce, params: { account: { settings: { salesforce_integration: "0", use_salesforce_configuration: "0" }} }

      assert_response 302
      refute @request.account.settings.salesforce_integration?
    end

    it "disables salesforce integration" do
      post :update_salesforce, params: { account: { settings: {salesforce_integration: "1", use_salesforce_configuration: "1" }} }

      assert_response 302
      assert(@request.account.settings.salesforce_integration)
      assert_equal '1', @request.account.settings.use_salesforce_configuration
    end

    CrmHelper::APP_LATENCY_VALUES.each do |latency|
      it "updates salesforce integration attributes with valid latency (#{latency})" do
        post :update_salesforce, params: { account: { salesforce_integration_attributes: { app_latency: latency }} }

        @request.account.salesforce_integration.app_latency.must_equal latency.to_i
        flash[:notice].must_include I18n.t("txt.admin.controllers.settings.extensions_controller.Salesforce_settings_updated")
      end
    end

    it "fails to updates salesforce integration attributes with invalid latency" do
      post :update_salesforce, params: { account: { salesforce_integration_attributes: { app_latency: invalid_configured_delay }} }
      assert_response 400
    end
  end

  describe "#salesforce_oauth_callback" do
    it "handles successful oauth callback properly" do
      Salesforce::Oauth::ConnectionHandler.any_instance.expects(:callback)

      get :salesforce_oauth_callback, params: { code: "foo" }
      assert_redirected_to "/settings/extensions#crm"
      assert_equal "Salesforce configuration completed", flash[:notice]
    end

    it "handles unsuccessful oauth callback properly" do
      Salesforce::Oauth::ConnectionHandler.any_instance.expects(:callback).raises(Salesforce::Integration::LoginFailed)

      get :salesforce_oauth_callback
      assert_response :redirect
      assert_redirected_to "/settings/extensions#crm"
      assert_equal "Failed to authenticate with Salesforce. Try again later.", flash[:error]
    end
  end

  describe "#connect_to_salesforce" do
    before do
      Salesforce::Oauth::ConnectionHandler.any_instance.expects(:authorize_url).returns("http://www.example.com")
    end

    it "initiates the oauth flow" do
      post :connect_to_salesforce, params: { salesforce_environment: "production" }
      assert_response :redirect
      assert_redirected_to "http://www.example.com"
    end
  end

  describe "Object Management" do
    before do
      @salesforce_integration = SalesforceIntegration.new(account: @minimum_account)
      @minimum_account.stubs(:salesforce_integration).returns(@salesforce_integration)
    end

    describe "#edit_salesforce_object" do
      it "lists available objects except those already selected if no specific object is requested and exclude objects that can't be used in queries" do
        available_objects = [
          {
            "name" => "Contact",
            "label" => "The Contact",
            "queryable" => true,
          },
          {
            "name" => "Case",
            "label" => "Case",
            "queryable" => true,
          },
          {
            "name" => "Account",
            "label" => "Account",
            "queryable" => true,
          },
          {
            "name" => "Account",
            "label" => "Account",
            "queryable" => false,
          }
        ]
        @salesforce_integration.expects(:list_objects).returns(available_objects)
        get :edit_salesforce_object
        objects = [
          {
            "name" => "Case",
            "label" => "Case",
            "queryable" => true
          },
          {
            "name" => "Account",
            "label" => "Account",
            "queryable" => true
          }
        ]
        assert_equal objects, assigns(:objects)
      end

      it "assigns only the given object" do
        get :edit_salesforce_object, params: { object: "Account" }
        objects = [
          {
            "name" => "Account",
            "label" => "Account"
          }
        ]
        assert_equal objects, assigns(:objects)
      end

      it "handles connection problems while listing objects" do
        @salesforce_integration.expects(:list_objects).raises(Salesforce::Integration::LoginFailed)
        get :edit_salesforce_object
        assert_response :success
        assert_equal [], assigns(:objects)
      end
    end

    describe "#edit_salesforce_fields" do
      before do
        @primary_fields = [{ title: "Contact ID", key: "Id", select: false }]
        @related_fields = [{
          fields: [],
          isFolder: true,
          object: "Account",
          relationship: "Account",
          title: "Account",
          type: "child"
        }]
        @object_name = "Contact"
      end

      it "assigns primary and related fields" do
        @salesforce_integration.expects(:fields).with(@object_name, anything, anything).returns(@primary_fields)
        @salesforce_integration.expects(:related_fields).with(@object_name, anything).returns(@related_fields)

        get :edit_salesforce_fields, params: { object: @object_name }
        assert_equal (@primary_fields + @related_fields).map(&:with_indifferent_access), assigns(:fields).map(&:with_indifferent_access)
      end

      it "handles connection problems" do
        @salesforce_integration.expects(:fields).raises(Salesforce::Integration::LoginFailed)

        get :edit_salesforce_fields, params: { object: @object_name }
        assert_response :success
        assert_equal [], assigns(:fields)
      end
    end

    describe "#salesforce_related_fields" do
      before do
        @fields = [{ title: "Contact ID", key: "Id", select: false }]
        @object_name = "Contact"
        @relationship = "Contacts"
        @salesforce_integration.expects(:fields).with(@object_name, anything, @relationship).returns(@fields)
      end

      it "gets the fields from the integration using the params" do
        get :salesforce_related_fields, params: { object: @object_name, relationship_name: @relationship }
      end
    end

    describe "#save_salesforce_object" do
      before do
        @fields = [{ title: "Contact ID", key: "Id", select: false }]
        @salesforce_object = "Contact"
        @fields = ["Name", "Email", "Account::Name"]
        @labels = "{\"Name\":\"Full Name\",\"Email\":\"Email\",\"Account::Name\":\"Account Name\"}"
        @relationships = "{\"Account\":{\"object\":\"Account\",\"type\":\"child\"}}"
        @mapping_zendesk_field = "ticket.requester.email"
        @mapping_salesforce_field = "Email"
        @mapping_salesforce_field_type = "email"
        @object_label = "The Contact"

        @salesforce_integration.configuration.expects(:add).with(@salesforce_object,
          @fields,
          JSON.parse(@labels),
          JSON.parse(@relationships),
          @mapping_salesforce_field,
          @mapping_zendesk_field,
          @mapping_salesforce_field_type,
          @object_label).returns(true)
      end

      it "saves the object using the params and the correct method in the integration" do
        post :save_salesforce_object, params: { salesforce_object: @salesforce_object, fields: @fields, labels: @labels, relationships: @relationships, mapping_salesforce_field: @mapping_salesforce_field, mapping_zendesk_field: @mapping_zendesk_field, mapping_salesforce_field_type: @mapping_salesforce_field_type, object_label: @object_label }
      end
    end

    describe "#remove_salesforce_object" do
      before do
        @object_name = "Contact"
        @salesforce_integration.configuration.expects(:remove).with(@object_name).returns(true)
      end

      it "deletes the object using the params and the correct method in the integration" do
        delete :remove_salesforce_object, params: { object: @object_name }
      end
    end

    describe "#sort_salesforce_objects" do
      before do
        @objects_list = ["Account", "Contact"]
        @salesforce_integration.configuration.expects(:sort).with(@objects_list).returns(true)
      end

      it "calls the correct method in the integration to sort the objects" do
        post :sort_salesforce_objects, params: { "salesforce_selected_objects_sort_list" => @objects_list }
      end
    end
  end

  describe 'Permissions' do
    before do
      @user = users(:minimum_agent)
      @user.permission_set = @user.account.permission_sets.create!(name: "Test")

      account = @user.account
      account.stubs(:has_permission_sets?).returns(true)

      @request.account = account
      login(@user)
    end

    describe 'Given an agent with the ability to edit extensions' do
      before do
        @user.permission_set.permissions.enable(:extensions_and_channel_management)
        assert @user.can?(:edit, Access::Settings::Extensions)
      end

      it 'is able to view extensions pages' do
        get :show
        assert_response :success, @response.body
      end
    end

    describe 'Given an agent without the ability to edit extensions' do
      before do
        @user.permission_set.permissions.disable(:extensions_and_channel_management)
        refute @user.can?(:edit, Access::Settings::Extensions)
      end

      it 'is able to view extensions pages' do
        get :show
        assert_response :forbidden, @response.body
      end
    end

    describe 'Given an admin' do
      before do
        login(:minimum_admin)
      end

      it 'is able to view extensions pages' do
        get :show
        assert_response :success, @response.body
      end
    end
  end
end
