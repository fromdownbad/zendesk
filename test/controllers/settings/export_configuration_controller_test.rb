require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Settings::ExportConfigurationController do
  fixtures :accounts, :users

  let(:account)  { accounts(:minimum) }
  let(:settings) { account.settings   }

  with_options(controller: "settings/export_configuration") do |request|
    request.should_route :put,  "/settings/export_configuration/update_whitelisted_domain", action: "update_whitelisted_domain"
    request.should_route :put,  "/settings/export_configuration/disable", action: "disable"
    request.should_route :post, "/settings/export_configuration/enable_request", action: "enable_request"
  end

  before { @request.account = account }

  describe "as an admin that is not the account owner" do
    before { login(users(:minimum_admin_not_owner)) }

    it "forbids PUT #disable" do
      put :disable
      assert_response :forbidden
    end

    it "forbids POST #enable_request" do
      post :enable_request
      assert_response :forbidden
    end

    it "forbids PUT #update_whitelisted_domain" do
      put :update_whitelisted_domain
      assert_response :forbidden
    end
  end

  describe "as the account owner" do
    let(:export_configuration) { Zendesk::Export::Configuration.new(account) }

    before do
      login(users(:minimum_admin))
      @controller.stubs(export_configuration: export_configuration)
    end

    describe "PUT #disable" do
      describe "when successful" do
        before do
          account.stubs(settings: settings)
          settings.expects(:set).with(export_accessible_types: ImportExportJobPolicy::VIEW_JOBS)
          put :disable
        end

        it "sets the flash notice with an appropriate success message" do
          assert_equal(
            "Exports have been disabled. You can request to re-enable them on the <a href=\"/agent#/admin/subscription\">subscription</a> page.",
            flash[:notice]
          )
        end

        it "redirects to the reports tab" do
          assert_redirected_to reports_path
        end
      end

      describe "when unsuccessful" do
        before do
          account.stubs(settings: settings)
          settings.expects(:set).with(export_accessible_types: ImportExportJobPolicy::VIEW_JOBS)
          settings.expects(save: false)
          put :disable
        end

        it "sets the flash error with an appropriate failure message" do
          assert_equal(
            "There was a problem disabling exports.",
            flash[:error]
          )
        end

        it "redirects to the exports tab" do
          assert_redirected_to reports_path(anchor: "export")
        end
      end
    end

    describe "POST #enable_request" do
      let(:notifier) { Zendesk::Export::ConfigurationNotifier.new(account) }
      before { @controller.stubs(export_configuration_notifier: notifier) }

      describe "when successful" do
        before do
          export_configuration.expects(:all_accessible?).returns(false)
          notifier.expects(:create_enable_exports_request).returns(true)
          post :enable_request
        end

        it "redirects to the account settings page" do
          assert_redirected_to settings_account_path
        end

        it "sets the flash notice to an appropriate success message" do
          assert_equal(
            "Request to enable data exports received. You will receive an email confirmation with further details.",
            flash[:notice]
          )
        end
      end

      describe "when unsuccessful" do
        before do
          export_configuration.expects(:all_accessible?).returns(true)
          notifier.expects(:create_enable_exports_request).never
          post :enable_request
        end

        it "redirects to the account settings page" do
          assert_redirected_to settings_account_path
        end

        it "sets the flash error to an appropriate failure message" do
          assert_equal(
            "Data exports are currently enabled for your account.",
            flash[:error]
          )
        end
      end
    end

    describe "PUT #update_whitelisted_domain" do
      before do
        account.settings.export_whitelisted_domain = "current.com"
        account.save!
      end

      describe "when successful" do
        before { put :update_whitelisted_domain, params: { "account" => {"export_whitelisted_domain" => "example.com"} } }

        it "sets the flash notice to an appropriate success message" do
          assert_equal "Updated whitelisted domain.", flash[:notice]
        end

        it "updates the setting" do
          assert_equal "example.com", account.settings.reload.export_whitelisted_domain
        end

        it "redirects to the exports tab" do
          assert_redirected_to reports_path(anchor: "export")
        end
      end

      describe "when unsuccessful" do
        describe "using bad domains" do
          before do
            Socket.stubs(:gethostbyname).with("foo.blar").raises(SocketError)
            put :update_whitelisted_domain, params: { "account" => {"export_whitelisted_domain" => "foo.blar"} }
            settings.reload
          end

          it "sets the flash notice to an appropriate failure message" do
            assert_includes flash[:error], "Enter only one domain."
          end

          it "redirects to the exports tab" do
            assert_redirected_to reports_path(anchor: "export")
          end

          it "does not change the value of the setting" do
            refute_equal "foo.blar", settings.export_whitelisted_domain
            assert_equal "current.com", settings.export_whitelisted_domain
          end
        end

        describe "too many domains" do
          before { put :update_whitelisted_domain, params: { "account" => {"export_whitelisted_domain" => "example.com foo.org"} } }

          it "presents an error flash message" do
            assert_includes flash[:error], "Enter only one domain."
          end

          it "does not change the value of the setting" do
            refute_equal "foo.blar", account.settings.export_whitelisted_domain
            assert_equal "current.com", settings.export_whitelisted_domain
          end
        end
      end
    end
  end
end
