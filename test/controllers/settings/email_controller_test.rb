require_relative "../../support/test_helper"

SingleCov.covered!

describe Settings::EmailController do
  fixtures :accounts, :users
  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_admin) }

  before do
    @request.account = account
  end

  should_include_module Zendesk::LotusEmbeddable

  should_route :put, "/settings/email", action: 'update'

  should_be_unauthorized_when_not_logged_in([:show, :update])

  describe "#show" do
    describe "basic" do
      before do
        login user
        get :show
      end

      it('responds with ok') { assert_response :ok }
      should_assign_to :account
      should_render_template :show
    end

    it "is on forced to ssl so api v2 requests always work" do
      https!(false)
      get :show
      assert_redirected_to "https://minimum.zendesk-test.com/settings/email"
    end
  end

  describe "#update" do
    before do
      login user
    end

    describe "on failure" do
      before do
        post :update, params: { account: { mail_delimiter: "Invalid" } }
      end

      it('responds with ok') { assert_response :ok }
      should_assign_to :account
      should_render_template :show
      should_set_the_flash_to("Failed to update email settings<ul><li><strong>Mail delimiter</strong> is too short (minimum is 20 characters)</li></ul>")
    end

    describe "on success" do
      before do
        post :update, params: { account: { mail_delimiter: "== This is valid because it's more than 20 chars ==" } }
      end

      it('responds with redirect') { assert_response :redirect }
      should_assign_to :account
      should_set_the_flash_to("Email settings updated")
    end

    describe "settings" do
      it "updates accept_wildcard_emails settings" do
        assert account.settings.accept_wildcard_emails # ON by default
        post :update, params: { account: { settings: {accept_wildcard_emails: false} } }
        assert_response :redirect
        refute account.reload.settings.accept_wildcard_emails # Turned OFF
      end

      it "updates rich_content_in_emails settings" do
        refute account.settings.rich_content_in_emails # OFF by default
        post :update, params: { account: { settings: {rich_content_in_emails: true} } }
        assert_response :redirect
        assert account.reload.settings.rich_content_in_emails # Turned ON
      end
    end

    describe "updating email template" do
      describe "when allow_email_template_customization? returns false" do
        before do
          Account.any_instance.stubs(:allow_email_template_customization?).returns(false)
        end

        it "doesn't update html_email_template" do
          original_html_mail_template = account.html_mail_template
          original_text_mail_template = account.text_mail_template
          original_mail_delimiter     = account.mail_delimiter

          post :update, params: { account: {
            html_mail_template: "change {{delimiter}} {{header}} {{content}} {{footer}}",
            text_mail_template: "change {{content}}\n\n{{footer}}",
            mail_delimiter: "change {{txt.email.delimiter}}"
          } }
          assert_equal original_html_mail_template, account.reload.html_mail_template
          assert_equal original_text_mail_template, account.reload.text_mail_template
          assert_equal original_mail_delimiter, account.reload.mail_delimiter
        end
      end

      describe "when allow_email_template_customization? returns true" do
        before do
          Account.any_instance.stubs(:allow_email_template_customization?).returns(true)
        end

        it "updates html_email_template" do
          original_html_mail_template = account.html_mail_template
          original_text_mail_template = account.text_mail_template
          original_mail_delimiter     = account.mail_delimiter

          post :update, params: { account: {
            html_mail_template: "change {{delimiter}} {{header}} {{content}} {{footer}}",
            text_mail_template: "change {{content}}\n\n{{footer}}",
            mail_delimiter: "change {{txt.email.delimiter}}"
          } }
          refute_equal original_html_mail_template, account.reload.html_mail_template
          refute_equal original_text_mail_template, account.reload.text_mail_template
          refute_equal original_mail_delimiter, account.reload.mail_delimiter
        end
      end
    end
  end

  describe 'Permissions' do
    let(:user) { users(:minimum_agent) }

    before do
      user.permission_set = account.permission_sets.create!(name: "Test")
      user.account.stubs(:has_permission_sets?).returns(true)
      login user
    end

    describe 'Given an agent with the ability to manage email settings' do
      before do
        user.permission_set.permissions.enable(:extensions_and_channel_management)
        assert user.can?(:edit, Access::Settings::Extensions)
      end

      it 'has access to manage email settings pages' do
        get :show
        assert_response :success, response.body
      end
    end

    describe 'Given an agent without the ability to manage email settings' do
      before do
        user.permission_set.permissions.disable(:extensions_and_channel_management)
        refute user.can?(:edit, Access::Settings::Extensions)
      end

      it 'does not have access to manage email settings pages' do
        get :show
        assert_response :forbidden, response.body
      end
    end
  end
end
