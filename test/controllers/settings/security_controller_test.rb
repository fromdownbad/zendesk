require_relative '../../support/test_helper'
require_relative '../../support/certificate_test_helper'

SingleCov.covered! uncovered: 3

describe Settings::SecurityController do
  include CertificateTestHelper

  fixtures :accounts, :users, :role_settings, :remote_authentications

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: 'settings/security') do |request|
    request.should_route :get, 'settings/security', action: 'show'
    request.should_route :put, 'settings/security', action: 'update'
    request.should_route :post, 'settings/security/enqueue_certificate_job', action: 'enqueue_certificate_job'
    request.should_route :post, 'settings/security/disable_zendesk_provisioned_ssl', action: 'disable_zendesk_provisioned_ssl'
    request.should_route :get, 'settings/security/ssl_provisioning_status', action: 'ssl_provisioning_status'
  end

  before do
    use_ssl

    @account = accounts(:minimum)
    @request.account = @account

    @role_settings = @account.role_settings
    @account.stubs(:role_settings).returns(@role_settings)
  end

  describe 'helpers' do
    describe '#any_active_remote_authentications?' do
      it 'returns true if there is an active remote authentication' do
        refute @controller.send(:any_active_remote_authentications?)
        @account.remote_authentications.update_all(is_active: true)
        @account.remote_authentications(:reload)
        assert @controller.send(:any_active_remote_authentications?)
      end
    end
  end

  describe "as an end user" do
    before do
      @end_user = users(:minimum_end_user)
      login(@end_user)
    end

    it "does not allow access to the page" do
      get :show
      assert_response :forbidden
    end

    it "does not allow enqueing a certificate job" do
      post :enqueue_certificate_job
      assert_response :forbidden
    end

    it "does not allow disabling zendesk provisioned ssl" do
      post :disable_zendesk_provisioned_ssl
      assert_response :forbidden
    end

    it "does not allow checking the status of zendesk provisioned ssl" do
      post :ssl_provisioning_status
      assert_response :forbidden
    end
  end

  describe "as an admin" do
    before do
      @admin = users(:minimum_admin)
      login(@admin)
    end

    describe "#transition_to_sni" do
      before { Arturo.disable_feature! :hosted_ip_ssl }

      describe "when account has active certificates" do
        before { create_certificate!(state: :active) }

        it "sets SNI enabled" do
          assert_equal false, @account.certificates.active.first.sni_enabled

          post :transition_to_sni
          @account.reload

          assert(@account.certificates.active.first.sni_enabled)
          assert_redirected_to settings_security_url(anchor: "ssl")
        end
      end

      describe "when account doesn't have active certificates" do
        it "doesn't set SNI enabled for inactive certs" do
          assert_equal false, @account.certificates.first.sni_enabled

          post :transition_to_sni
          @account.reload

          assert_equal false, @account.certificates.first.sni_enabled
          assert_redirected_to settings_security_url(anchor: "ssl")
        end
      end
    end

    describe "#enqueue_certificate_job" do
      it "enqueues a job and redirects back to ssl settings" do
        @account.settings.automatic_certificate_provisioning = true
        @account.save!

        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(@account.id).once
        post :enqueue_certificate_job
        assert_redirected_to settings_security_url(anchor: "ssl")
      end

      it "doesn't enqueue a job without the setting" do
        refute @account.settings.automatic_certificate_provisioning?

        AcmeCertificateJobStatus.expects(:enqueue_with_status!).never
        post :enqueue_certificate_job
        assert_redirected_to settings_security_url(anchor: "ssl")
      end
    end

    describe "#disable_zendesk_provisioned_ssl" do
      before do
        @account.settings.automatic_certificate_provisioning = true
        @account.save!
      end

      it "disables the setting and redirects back to ssl settings" do
        post :disable_zendesk_provisioned_ssl

        assert_equal false, @account.reload.settings.automatic_certificate_provisioning
        assert_redirected_to settings_security_url(anchor: "ssl")
      end
    end

    describe "#ssl_provisioning_status" do
      before do
        @account.acme_certificate_job_statuses.create!(status: 'working')
        get :ssl_provisioning_status
      end

      it "returns the status" do
        assert_response :ok
        assert_equal 'application/json', response.content_type
        assert_equal 'pending', JSON.parse(response.body).fetch('provisioning_status')
      end
    end

    describe "#show" do
      before do
        remote_authentications = stub(
          'remote authentications',
          saml: [@saml = remote_authentication(2)],
          jwt: [@jwt = remote_authentication(3)]
        )

        remote_authentications.stubs(:count).returns(3)
        remote_authentications.stubs(:map).returns(remote_authentications)
        remote_authentications.stubs(:select).returns(remote_authentications)
        remote_authentications.stubs(:first).returns(@jwt)
        remote_authentications.stubs(:inject).returns(remote_authentications)
        remote_authentications.stubs(:join).returns('')
        remote_authentications.stubs(:any?).returns(true)

        @account.stubs(:available_security_policies).returns(@policies = policies(3))
        @account.stubs(:remote_authentications).returns(remote_authentications)
        @account.stubs(:has_saml?).returns(true)
      end

      it "sets the required variables" do
        get :show

        assert_equal @jwt, assigns(:jwt)
      end
    end

    describe "#show with no remote_authentications" do
      before do
        @account.remote_authentications.clear
        get :show
      end

      it "is ok" do
        assert_response :ok
      end
    end

    describe "with optional features" do
      before do
        Arturo.enable_feature! :credit_card_sanitization
        Arturo.enable_feature! :audioeye_in_auth
        @account.settings.two_factor_all_agents = true
        @account.settings.save!
      end

      it "renders the view" do
        get :show
        assert_response :ok
      end
    end

    describe "#update" do
      before do
        @params = {
          account: {settings: {}},
          tab: 'agents'
        }
      end

      describe "for global settings" do
        before do
          Zendesk::Accounts::Security::AgentSettingsUpdate.any_instance.stubs(:perform)
        end

        describe 'for email_agent_when_sensitive_fields_changed' do
          it "disables the setting" do
            @account.settings.two_factor_enforce = true
            @account.save!

            assert @account.settings.two_factor_enforce
            assert @account.email_agent_when_sensitive_fields_changed

            @params = {
              account: {
                email_agent_when_sensitive_fields_changed: '0',
                settings: { two_factor_enforce: '0' }
              },
              tab: 'global'
            }

            @admin.account.settings.reload

            put :update, params: @params

            @account.reload
            refute @account.email_agent_when_sensitive_fields_changed
            refute @account.settings.two_factor_enforce
          end

          it "enables the setting" do
            @account.email_agent_when_sensitive_fields_changed = false
            @account.settings.two_factor_enforce = false
            @account.save!
            @account.reload

            refute @account.email_agent_when_sensitive_fields_changed
            refute @account.settings.two_factor_enforce

            @params = {
              account: {
                email_agent_when_sensitive_fields_changed: '1',
                settings: { two_factor_enforce: '1' }
              },
              tab: 'global'
            }

            @admin.account.settings.reload

            put :update, params: @params

            @account.reload
            assert @account.email_agent_when_sensitive_fields_changed
            assert @account.settings.two_factor_enforce
          end
        end
      end

      describe "when successful" do
        before do
          Zendesk::Accounts::Security::AgentSettingsUpdate.any_instance.
            stubs(:perform)
          Zendesk::Accounts::Security::AgentSettingsUpdate.any_instance.
            stubs(:success?).returns(true)
        end

        it "provides a notice message" do
          put :update, params: @params

          assert flash[:notice]
        end

        describe "and updating agent settings" do
          before { @params[:tab] = 'agents' }

          it "disables agent social login settings" do
            @account.role_settings.expects(:disable_agent_social_login!)

            put :update, params: @params
          end
        end
        describe "and updating non-agent settings" do
          before { @params[:tab] = 'global' }

          it "does not disable agent social login settings" do
            @account.role_settings.expects(:update_attributes!).never

            put :update, params: @params
          end
        end
      end

      describe "when unsuccessful" do
        before do
          Zendesk::Accounts::Security::AgentSettingsUpdate.any_instance.
            stubs(:perform)
          Zendesk::Accounts::Security::AgentSettingsUpdate.any_instance.
            stubs(:success?).returns(false)
          @controller.stubs(:remote_authentication).returns(remote_authentication)
        end

        it "provides an error message" do
          put :update, params: @params

          assert flash[:error]
        end

        it "renders the show template" do
          put :update, params: @params

          assert_template 'settings/security/show'
        end

        describe "and updating agent settings" do
          before { @params[:tab] = 'agents' }

          it "does not disable agent social login settings" do
            @account.role_settings.expects(:disable_agent_social_login!).never

            put :update, params: @params
          end
        end
      end

      describe "for admin settings" do
        describe "when updating to login services ( other than remote_login )" do
          before do
            @params = {
              account: {
                role_settings: {
                    agent_security_policy_id: 200,
                    agent_zendesk_login: false,
                    agent_google_login: true
                }
              },
              tab: 'agents'
            }
          end

          it 'does not show remote authentication errors' do
            put :update, params: @params
            refute flash[:error]
          end
        end

        describe "when there are no exising remote_authentication records" do
          before do
            Zendesk::Accounts::Security::AgentSettingsUpdate.any_instance.
              stubs(:success?).returns(true)
            RemoteAuthentication.any_instance.
              stubs(:notify_on_activation)

            @params = {
              account: {
                role_settings: {
                    agent_security_policy_id: 200,
                    agent_zendesk_login: false,
                    agent_google_login: false,
                    agent_remote_login: true
                  },
                remote_authentications: {
                  'jwt' => {
                    id: nil,
                    is_active: true,
                    remote_logout_url: 'https://jwt.com'
                  },
                  'saml' => {
                    id: nil,
                    is_active: false,
                    remote_logout_url: 'https://saml.com'
                  }
                }
              },
              tab: 'agents'
            }
            @account.remote_authentications.delete_all
            @account.save!
          end

          it "builds remote_authentication records with params" do
            put :update, params: @params

            assert_equal @params[:account][:remote_authentications]["saml"][:remote_logout_url],
              @account.remote_authentications.saml.first.remote_logout_url
            assert_equal @params[:account][:remote_authentications]["jwt"][:remote_logout_url],
              @account.remote_authentications.jwt.first.remote_logout_url
          end
        end
      end
    end
  end

  private

  def policies(number = 1)
    policies = []
    number.times do
      policies << stub(
        id: 1,
        level?: false,
        title: 'High',
        display_name: 'High Policy',
        password_requirements: ['a', 'b', 'c']
      )
    end

    policies
  end

  def remote_authentication(auth_mode = RemoteAuthentication::JWT)
    @remote_authentication ||= stub(
      'remote_authentication',
      auth_mode: auth_mode,
      id: 10,
      is_active: true,
      remote_login_url: 'https://in.com',
      remote_logout_url: 'https://out.com',
      ip_ranges: '255.255.255.255',
      update_external_ids: false,
      fingerprint: 'a' * 40,
      has_native_token?: false,
      to_json: '',
      name: 'name',
      priority: auth_mode
    )
  end
end
