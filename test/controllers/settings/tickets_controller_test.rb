require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 7

describe Settings::TicketsController do
  describe "Settings::BaseController" do
    fixtures :accounts, :users, :subscriptions, :account_property_sets, :sequences,
      :addresses, :groups, :user_identities

    should_include_module Zendesk::LotusEmbeddable

    with_options(controller: "settings/tickets") do |request|
      request.should_route :put, "/settings/tickets/update_settings", action: 'update_settings'
      request.should_route :put, "/settings/tickets/update_ticket_sharing", action: 'update_ticket_sharing'
    end

    before do
      Account.any_instance.stubs(:is_serviceable?).returns(true)
      Account.any_instance.stubs(:is_failing?).returns(false)

      @minimum_admin   = users(:minimum_admin)
      @minimum_account = accounts(:minimum)
      @request.account = @minimum_account

      login(@minimum_admin)
    end

    describe "#show" do
      it 'reloads the account' do
        @minimum_account.settings.expects(:reload).at_least_once
        @minimum_account.expects(:reload).at_least_once.returns(@minimum_account)

        get :show
      end

      it "creates a suspended ticket notification if none exists" do
        assert @minimum_account.suspended_ticket_notification.nil?
        get :show
        assert @minimum_account.suspended_ticket_notification.present?
        record_id = @minimum_account.suspended_ticket_notification.id
        get :show
        assert_equal record_id, @minimum_account.reload.suspended_ticket_notification.id
      end

      it "displays bcc archive address when available" do
        get :show
        refute @response.body.include?('account[settings][bcc_archive_address]')

        Subscription.any_instance.expects(:has_bcc_archiving?).at_least_once.returns(true)
        get :show
        @response.body.must_include "account[settings][bcc_archive_address]"

        @minimum_account.settings.bcc_archive_address = 'mentor@example.com'
        Sharing::Agreement.any_instance.stubs(:send_agreement_request_to_partner).returns(true)
        @minimum_account.save(validate: false)
        get :show
        @response.body.must_include "mentor@example.com"
      end

      describe "screencasts for tickets option" do
        it "does not be shown if Screenr has not been provisioned" do
          Subscription.any_instance.stubs(:has_screencasts_for_tickets?).returns(true)

          get :show
          assert_no_match /account\[settings\]\[screencasts_for_tickets\]/, @response.body
        end

        it "does not be shown if option is not available to plan" do
          Subscription.any_instance.stubs(:has_screencasts_for_tickets?).returns(false)

          get :show
          assert_no_match /account\[settings\]\[screencasts_for_tickets\]/, @response.body
        end
      end

      describe "private attachments on tickets" do
        it "is always shown" do
          get :show
          @response.body.must_include I18n.t('txt.admin.views.settings.tickets._settings.secure_attachment_download_title')
        end
      end
    end

    describe "#update_settings" do
      it 'returns json when format is json' do
        login('minimum_admin')
        put :update_settings, params: { format: 'json', account: { settings: { }} }

        assert_response :ok
        assert_equal "{}", @response.body
      end

      it 'allows enabling tags to be sent via web widget' do
        login('minimum_admin')
        refute @minimum_account.settings.ticket_tags_via_widget?
        put :update_settings, params: { account: { settings: { ticket_tags_via_widget: '1' }} }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        assert @minimum_account.settings.ticket_tags_via_widget?
      end

      it 'allows disabling tags to be sent via web widget' do
        login('minimum_admin')
        @minimum_account.settings.enable(:ticket_tags_via_widget)
        @minimum_account.save!
        assert @minimum_account.settings.ticket_tags_via_widget?
        put :update_settings, params: { account: { settings: { ticket_tags_via_widget: '0' }} }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        refute @minimum_account.settings.ticket_tags_via_widget?
      end

      it "allows enabling settable CCs in Help Center" do
        login('minimum_admin')
        refute @minimum_account.settings.collaborators_settable_in_help_center
        put :update_settings, params: { account: { settings: { collaborators_settable_in_help_center: true }} }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        assert @minimum_account.settings.collaborators_settable_in_help_center?
      end

      it "allows setting the forwardable emails feature" do
        login('minimum_admin')
        refute @minimum_account.settings.agent_forwardable_emails?
        put :update_settings, params: { account: { settings: { agent_forwardable_emails: '1' }} }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        assert @minimum_account.settings.agent_forwardable_emails?
      end

      it "allows removing the forwardable emails feature" do
        login('minimum_admin')
        @minimum_account.settings.enable(:agent_forwardable_emails)
        @minimum_account.save!
        assert @minimum_account.settings.agent_forwardable_emails?
        put :update_settings, params: { account: { settings: { agent_forwardable_emails: '0' }} }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        refute @minimum_account.settings.agent_forwardable_emails?
      end

      it "allows setting the suspended tickets notification frequency" do
        login('minimum_admin')
        assert @minimum_account.create_suspended_ticket_notification
        put :update_settings, params: { suspended_ticket_notification: { email_list: "someone@example.com", frequency: SuspendedTicketNotification::Frequency::SOONISH.last } }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        @minimum_account.reload
        assert_equal @minimum_account.suspended_ticket_notification.frequency, SuspendedTicketNotification::Frequency::SOONISH.last
        assert_equal @minimum_account.suspended_ticket_notification.email_list, "someone@example.com"
      end

      it "allows setting the BCC archiving email address" do
        login('minimum_admin')
        put :update_settings, params: { account: { settings: { bcc_archive_address: "someone@example.com" } } }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        @minimum_account.reload
        assert_equal 'someone@example.com', @minimum_account.settings.bcc_archive_address
      end

      it "allows setting the screencasts for tickets setting" do
        login('minimum_admin')
        put :update_settings, params: { account: { settings: { screencasts_for_tickets: "1" } } }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        @minimum_account.reload
        assert @minimum_account.settings.screencasts_for_tickets
      end

      it "allows setting the render custom uri hyperlinks feature" do
        login('minimum_admin')
        assert_equal "", @minimum_account.texts.render_custom_uri_hyperlinks
        put :update_settings, params: { account: { render_custom_uri_hyperlinks: "dlr, ldr" } }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        @minimum_account.reload
        assert_equal "dlr, ldr", @minimum_account.texts.render_custom_uri_hyperlinks
      end

      it "allows setting the ccs_requester_excluded_public_comments setting" do
        login('minimum_admin')
        assert_equal false, @minimum_account.settings.ccs_requester_excluded_public_comments
        put :update_settings, params: { account: { settings: { ccs_requester_excluded_public_comments: true } } }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        @minimum_account.reload
        assert @minimum_account.settings.ccs_requester_excluded_public_comments
      end

      describe_with_arturo_disabled :email_end_user_comment_privacy_settings do
        it "does not allow setting the ccs_requester_excluded_public_comments setting" do
          login('minimum_admin')
          assert_equal false, @minimum_account.settings.ccs_requester_excluded_public_comments
          put :update_settings, params: { account: { settings: { ccs_requester_excluded_public_comments: true } } }
          assert_redirected_to settings_tickets_path(anchor: "settings")
          @minimum_account.reload
          refute @minimum_account.has_ccs_requester_excluded_public_comments_enabled?
        end
      end

      it "allows setting the third_party_end_user_public_comments setting" do
        login("minimum_admin")
        assert_equal false, @minimum_account.settings.third_party_end_user_public_comments
        put :update_settings, params: { account: { settings: { third_party_end_user_public_comments: true } } }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        @minimum_account.reload
        assert @minimum_account.has_third_party_end_user_public_comments_enabled?
      end

      describe_with_arturo_disabled :third_party_end_user_public_comments_setting do
        it "does not allow setting the third_party_end_user_public_comments setting" do
          login("minimum_admin")
          assert_equal false, @minimum_account.settings.third_party_end_user_public_comments
          put :update_settings, params: { account: { settings: { third_party_end_user_public_comments: true } } }
          assert_redirected_to settings_tickets_path(anchor: "settings")
          @minimum_account.reload
          refute @minimum_account.settings.third_party_end_user_public_comments
        end
      end

      it 'allows enabling re-engagement settings' do
        login('minimum_admin')
        refute @minimum_account.settings.reengagement
        put :update_settings, params: { account: { settings: { reengagement: true }} }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        @minimum_account.reload
        assert @minimum_account.settings.reengagement
      end

      it 'allows disabling re-engagement settings' do
        login('minimum_admin')
        @minimum_account.settings.enable(:reengagement)
        @minimum_account.save!
        assert @minimum_account.settings.reengagement?
        put :update_settings, params: { account: { settings: { reengagement: false }} }
        assert_redirected_to settings_tickets_path(anchor: "settings")
        refute @minimum_account.settings.reengagement
      end

      def setting_before_and_after_update_via_api(account:, setting:, new_value:)
        current_actual = account.settings.send(setting)
        put :update_settings, params: { account: { settings: { setting => new_value } } }
        updated_actual = account.settings.send(setting)
        [current_actual, updated_actual]
      end

      def assert_setting_changable_via_api(account:, setting:, current_value:, new_value:, expected_value:)
        before_value, after_value = setting_before_and_after_update_via_api(account: account, setting: setting, new_value: new_value)
        assert_equal current_value, before_value, "'#{setting}' setting has wrong initial value"
        assert_equal expected_value, after_value, "Expected '#{setting}' setting to change after API update but it didn't change"
      end

      def refute_setting_changable_via_api(account:, setting:, current_value:, new_value:, expected_value:)
        before_value, after_value = setting_before_and_after_update_via_api(account: account, setting: setting, new_value: new_value)
        assert_equal current_value, before_value, "'#{setting}' setting has wrong initial value"
        assert_equal expected_value, after_value, "Expected '#{setting}' setting to not change after API update but it did change"
      end

      it 'allows setting modern collaboration' do
        login('minimum_admin')
        [
          { setting: :follower_and_email_cc_collaborations, current: false, new: true, expected: true },
          { setting: :comment_email_ccs_allowed, current: true, new: false, expected: false },
          { setting: :ticket_followers_allowed, current: true, new: false, expected: false },
          { setting: :agent_can_change_requester, current: true, new: false, expected: false },
          { setting: :auto_updated_ccs_followers_rules, current: false, new: true, expected: true }
        ].each do |scenario|
          assert_setting_changable_via_api(account: @minimum_account,
                                           setting: scenario[:setting],
                                           current_value: scenario[:current],
                                           new_value: scenario[:new],
                                           expected_value: scenario[:expected])
        end
      end

      describe_with_arturo_disabled :email_ccs_light_agents_v2 do
        it 'does not permit setting `light_agent_email_ccs_allowed`' do
          login('minimum_admin')
          assert_setting_changable_via_api(account: @minimum_account,
                                           setting: :light_agent_email_ccs_allowed,
                                           current_value: false,
                                           new_value: true,
                                           expected_value: false)
        end
      end

      describe_with_arturo_disabled :email_ccs do
        it 'does not permit setting modern collaboration' do
          login('minimum_admin')
          [
            { setting: :follower_and_email_cc_collaborations, current: false, new: true, expected: false },
            { setting: :comment_email_ccs_allowed, current: true, new: false, expected: true },
            { setting: :ticket_followers_allowed, current: true, new: false, expected: true },
            { setting: :agent_can_change_requester, current: true, new: false, expected: true },
            { setting: :auto_updated_ccs_followers_rules, current: false, new: true, expected: false },
            { setting: :light_agent_email_ccs_allowed, current: false, new: true, expected: false }
          ].each do |scenario|
            refute_setting_changable_via_api(account: @minimum_account,
                                             setting: scenario[:setting],
                                             current_value: scenario[:current],
                                             new_value: scenario[:new],
                                             expected_value: scenario[:expected])
          end
        end
      end

      describe "with a :ticket_id_sequence parameter present" do
        before do
          @next_ticket_id = Time.now.to_i
          put :update_settings, params: { ticket_id_sequence: @next_ticket_id.to_s }
        end

        it "sets the current account's nice_id_sequence to one less than the param's value" do
          assert_equal @next_ticket_id - 1, @minimum_account.nice_id_sequence.value
        end
      end

      describe "with a :ticket_id_sequence parameter out of range" do
        before do
          @next_ticket_id = NiceIdSequence::MAX_ALLOWED_SETTING + 1
          put :update_settings, params: { ticket_id_sequence: @next_ticket_id.to_s }
        end

        should_set_the_flash_to :error
        should_render_template :show
      end

      describe "when the update fails" do
        it "renders the errors on same page" do
          Account.any_instance.stubs(:update_attributes).returns(false)
          put :update_settings
          assert_response :ok
        end
      end

      describe "when suspended_ticket_notification frequency is not set to 'Never'" do
        before do
          @notification = @minimum_account.build_suspended_ticket_notification
          Account.any_instance.stubs(:suspended_ticket_notification).returns(@notification)
        end

        describe "when email_list is blank" do
          it "shows the appropriate error message" do
            put :update_settings, params: { suspended_ticket_notification: { email_list: "", frequency: "60" } }
            @response.body.must_include I18n.t('txt.admin.controllers.settings.tickets_controller.suspended_ticket_notification_requires_emails')
          end

          it "sets the frequency to 'Never'" do
            assert @notification.frequency == 0
          end
        end

        describe "when email_list contains at least one address" do
          it "updates the account's suspended_ticket_notification settings" do
            put :update_settings, params: { suspended_ticket_notification: { email_list: "nobody_in_particular@example.com", frequency: "60" } }
            assert_equal 60, @minimum_account.suspended_ticket_notification.frequency
            assert_equal "nobody_in_particular@example.com", @minimum_account.suspended_ticket_notification.email_list
          end
        end
      end
    end
  end
end
