require_relative "../../support/test_helper"

SingleCov.covered!

describe Settings::SlasController do
  with_options(controller: "settings/slas") do |request|
    request.should_route :get, "/settings/slas", action: "show"
  end

  describe '#show' do
    as_an_agent do
      describe 'when the account does not have the SLA feature' do
        before do
          @account.stubs(has_service_level_agreements?: false)
        end

        it 'disallows access to slas admin settings page' do
          get :show
          assert_response :forbidden, @response.body
        end
      end

      describe 'when the account has the SLA feature' do
        before do
          @account.stubs(has_service_level_agreements?: true)
        end

        it 'disallows access to slas admin settings page' do
          get :show
          assert_response :forbidden, @response.body
        end
      end
    end

    as_an_agent_with_permissions(business_rule_management: false) do
      describe 'when the account does not have the SLA feature' do
        before do
          @account.stubs(has_service_level_agreements?: false)
        end

        it 'should disallow access' do
          get :show
          assert_response :forbidden, @response.body
        end
      end

      describe 'when the account has the SLA feature' do
        before do
          @account.stubs(has_service_level_agreements?: true)
        end

        it 'should disallow access' do
          get :show
          assert_response :forbidden, @response.body
        end
      end
    end

    as_an_agent_with_permissions(business_rule_management: true) do
      describe 'when the account does not have the SLA feature' do
        before do
          @account.stubs(has_service_level_agreements?: false)
        end

        it 'should not find page' do
          get :show
          assert_response :not_found, @response.body
        end
      end

      describe 'when the account has the SLA feature' do
        before do
          @account.stubs(has_service_level_agreements?: true)
        end

        it 'should allow access' do
          get :show
          assert_response :success, @response.body
        end
      end
    end

    as_an_admin do
      describe 'when the account does not have the SLA feature' do
        before do
          @account.stubs(has_service_level_agreements?: false)
        end

        it 'disallows access to slas admin settings page' do
          get :show
          assert_response :not_found, @response.body
        end
      end

      describe 'when the account has the SLA feature' do
        before do
          @account.stubs(has_service_level_agreements?: true)
        end

        it 'allows access to slas admin settings page' do
          get :show
          assert_response :success, @response.body
        end
      end
    end
  end
end
