require_relative "../support/test_helper"

SingleCov.covered! uncovered: 10

describe SuspendedTicketsController do
  fixtures :all

  def assert_recovers(count, &block)
    assert_difference 'Ticket.count(:all)', +count do
      assert_difference 'SuspendedTicket.count(:all)', -count, &block
    end
  end

  def assert_recovers_comment(count, &block)
    assert_no_difference 'Ticket.count(:all)' do
      assert_difference 'Comment.count(:all)', +count do
        assert_difference 'SuspendedTicket.count(:all)', -count, &block
      end
    end
  end

  with_options(controller: "suspended_tickets") do |request|
    request.should_route :post, "/suspended_tickets/bulk", action: 'bulk'
  end

  describe "on an account with agent restrictions" do
    before do
      @request.account = accounts(:with_groups)
    end

    should_be_unauthorized_when_not_logged_in([:index, :destroy], 'with_groups_agent_groups_restricted', 403)
    should_be_unauthorized_when_not_logged_in([:index, :destroy], 'with_groups_agent_organization_restricted', 403)
    should_be_unauthorized_when_not_logged_in([:index, :destroy], 'with_groups_agent_assigned_restricted', 403)
  end

  before do
    @request.account = accounts(:minimum)
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
  end

  should_be_unauthorized_when_not_logged_in([:index, :destroy])
  should_be_unauthorized_when_not_logged_in([:index, :destroy], 'minimum_end_user', 403)

  describe "#bulk" do
    let(:suspended_ticket) { suspended_tickets(:minimum_spam) }

    before { login(:minimum_agent) }

    it "recovers all" do
      suspended_count = SuspendedTicket.count(:all)
      tickets_count = Ticket.count(:all)
      post :bulk, params: { submit_type: 'recover_all' }
      # suspended(:minimum_loop) is unrecoverable
      assert_equal 1, SuspendedTicket.count(:all)
      assert_equal (tickets_count + suspended_count - 1), Ticket.count(:all)
    end

    it "recovers selected" do
      assert_recovers 1 do
        post :bulk, params: { submit_type: 'recover_selected', bulk_ids: [suspended_ticket] }
      end
    end

    it "deletes all" do
      post :bulk, params: { submit_type: 'delete_all' }
      assert_equal 0, SuspendedTicket.count(:all)
    end

    it "deletes selected" do
      assert_difference 'SuspendedTicket.count(:all)', -1 do
        post :bulk, params: { submit_type: 'delete_selected', bulk_ids: [suspended_ticket] }
      end
    end

    describe "with invalid authors in the list" do
      let(:suspended_ticket) { suspended_tickets(:minimum_loop) }

      it "does not raise validation errors" do
        assert_difference 'SuspendedTicket.count(:all)', 0 do
          post :bulk, params: { submit_type: 'recover_selected', bulk_ids: [suspended_ticket] }
        end
      end

      it "does not raise validation errors when recovering as user" do
        assert_difference 'SuspendedTicket.count(:all)', 0 do
          post :bulk, params: { submit_type: 'recover_selected', bulk_ids: [suspended_ticket], recover_as: "user:#{users(:minimum_end_user).id}" }
        end
      end
    end
  end

  describe "#recover" do
    before { login(:minimum_agent) }

    it "recovers spam" do
      suspended = suspended_tickets(:minimum_spam)

      assert_recovers(1) { post :recover, params: { id: suspended.id } }

      ticket = Ticket.last
      assert_equal suspended.send(:full_content), ticket.description
      assert_equal suspended.author, ticket.requester
      assert_equal users(:minimum_agent), ticket.submitter
      refute ticket.is_follower?(suspended.author)
      audit = ticket.audits.last
      assert_equal suspended.author, audit.author
    end

    it "recovers without subject" do
      suspended = suspended_tickets(:minimum_spam)
      suspended.update_attribute(:subject, nil)
      assert_recovers(1) { post :recover, params: { id: suspended.id } }
      ticket = Ticket.last
      assert_equal '', ticket.subject
    end

    it "recovers other user update" do
      suspended = suspended_tickets(:minimum_other_user_update)
      suspended.update_attribute(:ticket_id, tickets(:minimum_2).nice_id)

      assert_recovers_comment(1) { post :recover, params: { id: suspended.id } }

      ticket = suspended.ticket.reload
      comment = ticket.comments.last
      assert_equal suspended.send(:full_content), comment.body
      assert_equal suspended.author, comment.author
      assert_equal false, comment.is_public
      assert_equal tickets(:minimum_2).requester, ticket.requester
      audit = ticket.audits.last
      assert_equal suspended.author, audit.author
    end

    it "recovers other user update without collaboration enabled" do
      accounts(:minimum).update_attribute(:is_collaboration_enabled, false)
      suspended = suspended_tickets(:minimum_other_user_update)
      suspended.update_attribute(:ticket_id, tickets(:minimum_2).nice_id)
      post :recover, params: { id: suspended.id }
      refute suspended.ticket.reload.is_follower?(suspended.author)
    end

    it "recovers other user update with closed ticket" do
      suspended = suspended_tickets(:minimum_other_user_update)
      suspended.update_attribute(:ticket_id, tickets(:minimum_2).nice_id)
      ticket = suspended.ticket
      ticket.status_id = StatusType.CLOSED
      ticket.will_be_saved_by(users(:minimum_agent))
      ticket.save

      assert_recovers(1) { post :recover, params: { id: suspended.id } }

      new_ticket = Ticket.last
      refute_equal new_ticket, ticket

      ticket.reload
      refute ticket.is_follower?(suspended.author)
    end

    it "recovers unknown author without welcome email" do
      UsersMailer.expects(:deliver_verify).never
      accounts(:minimum).update_attribute(:is_welcome_email_when_agent_register_enabled, false)
      suspended = suspended_tickets(:minimum_unknown_author)

      assert_difference "User.count(:all)", +1 do
        assert_recovers(1) { post :recover, params: { id: suspended.id } }
      end

      ticket = Ticket.last
      assert_equal suspended.send(:full_content), ticket.description
      assert_equal suspended.from_mail, ticket.requester.email
      assert_equal suspended.from_name, ticket.requester.name
      assert_equal users(:minimum_agent), ticket.submitter
      assert_nil ticket.current_collaborators
    end

    it "recovers other user update with unknown author" do
      suspended = suspended_tickets(:minimum_other_user_update)
      suspended.update_attribute(:ticket_id, tickets(:minimum_2).nice_id)
      suspended.update_attribute(:author_id, nil)
      suspended.reload
      assert_nil suspended.author
      ticket = suspended.ticket

      assert_recovers_comment(1) { post :recover, params: { id: suspended.id } }

      ticket.reload
      comment = ticket.comments.last
      assert_equal suspended.send(:full_content), comment.body
      assert_equal suspended.from_mail, comment.author.email
      assert_equal suspended.from_name, comment.author.name
      assert_equal false, comment.is_public
    end

    it "recovers unknown author no welcome email" do
      UsersMailer.expects(:deliver_verify).never
      accounts(:minimum).update_attribute(:is_welcome_email_when_agent_register_enabled, false)
      suspended = suspended_tickets(:minimum_unknown_author)

      assert_difference "User.count(:all)", +1 do
        assert_recovers(1) { post :recover, params: { id: suspended.id } }
      end

      ticket = Ticket.last
      assert_equal suspended.send(:full_content), ticket.description
      assert_equal suspended.from_mail, ticket.requester.email
      assert_equal suspended.from_name, ticket.requester.name
      assert_equal users(:minimum_agent), ticket.submitter
      assert_nil ticket.current_collaborators
    end

    it "recovers signup required" do
      UsersMailer.expects(:deliver_verify).never
      suspended = suspended_tickets(:minimum_signup_required)

      assert_difference "User.count(:all)", +1 do
        assert_recovers(1) { post :recover, params: { id: suspended.id } }
      end

      ticket = Ticket.last
      assert_equal suspended.send(:full_content), ticket.description
      assert_equal suspended.from_mail, ticket.requester.email
      assert_equal suspended.from_name, ticket.requester.name
      assert_equal users(:minimum_agent), ticket.submitter
      assert_nil ticket.current_collaborators
    end

    it "can assign unrecoverable ticket to requester" do
      suspended = suspended_tickets(:minimum_loop)
      suspended.update_attributes!(
        ticket: tickets(:minimum_2),
        author: nil
      )

      # not recovered
      assert_no_difference "User.count(:all)" do
        assert_recovers(0) { post :recover, params: { id: suspended.id } }
      end

      assert flash[:error]
      flash.clear

      # recovered when assigning to different requester
      assert_no_difference "User.count(:all)" do
        assert_recovers_comment(1) { put :assign_to_requester, params: { id: suspended.id } }
      end

      refute flash[:error]
    end

    describe "recovering a ticket suspended due to an unknown author" do
      let(:suspended) {  suspended_tickets(:minimum_unknown_author) }

      it "recovers as a new ticket" do
        suspended = suspended_tickets(:minimum_unknown_author)
        put :recover, params: { id: suspended.id, recover_as: "new_ticket" }
        assert_redirected_to new_ticket_path(suspended: suspended.id)
      end

      it "recovers using the default automatic process" do
        login('minimum_agent')
        suspended = suspended_tickets(:minimum_unknown_author)
        assert_recovers(1) { put :recover, params: { id: suspended.id, recover_as: "default" } }
      end

      describe "sent in from an agent and specifying 'recover as'" do
        before do
          @agent = users(:minimum_agent)
          @ticket = tickets(:minimum_1)
          suspended = @agent.account.suspended_tickets.create!(ticket: @ticket, cause: 2, from_name: @agent.name, from_mail: @agent.email, subject: "foo", content: "bar")
          @params = { id: suspended.id, recover_as: "user:#{@ticket.requester.id}" }
        end

        it "does not strip email from agent" do
          assert_not_equal @ticket.requester, @ticket.comments.last.author
          refute_difference '@agent.identities.count(:all)' do
            put :recover, params: @params
          end
          @ticket.reload
          assert_equal @ticket.requester, @ticket.comments.last.author
        end
      end

      describe "with 'recover as' specified" do
        before do
          @recover_as_user = users(:minimum_end_user)
          open_ticket = tickets(:minimum_1)
          suspended.update_attributes!(ticket: open_ticket)
          @params = { id: suspended.id, recover_as: "user:#{@recover_as_user.id}" }
        end

        it "adds the ticket requester's email to the selected user's identities" do
          assert_difference '@recover_as_user.identities.count(:all)' do
            put :recover, params: @params
          end

          assert @recover_as_user.identities.find_by_value(suspended.from_mail), UserIdentity.last.inspect
        end

        it "recovers the ticket" do
          assert_difference 'suspended.ticket.comments.count(:all)' do
            put :recover, params: @params
          end
          assert_equal suspended.send(:full_content), suspended.ticket.comments.last.body
          assert_equal @recover_as_user, suspended.ticket.comments.last.author
        end

        it "flashs a notice that other suspended ticket's with this identity were recovered" do
          put :recover, params: @params
          message = /#{@recover_as_user.name} is now the verified owner of #{suspended.from_mail}. Suspended tickets from #{suspended.from_mail} were recovered/

          assert_response :redirect
          assert_match message, flash[:notice]
        end
      end

      it "flashs the default notice when recover as user is a new user" do
        params = { id: suspended.id, recover_as: "user:#{User.new.id}" }
        put :recover, params: params

        assert_redirected_to suspended_tickets_path
        flash[:notice].must_include "1 suspended ticket"
      end

      it "sets the flash when no tickets were recovered" do
        recoverer = stub("recoverer")
        Zendesk::Tickets::Recoverer.stubs(:new).returns(recoverer)
        recoverer.stubs(:recover).returns([])
        post :recover, params: { id: suspended.id, recover_as: "default" }
        assert_equal "No tickets were recovered.", flash[:notice]
      end

      describe "with welcome email enabled" do
        it "unsuspends the ticket" do
          assert_difference "User.count(:all)", +1 do
            assert_recovers(1) { post :recover, params: { id: suspended.id } }
          end

          ticket = Ticket.last
          assert_equal suspended.send(:full_content), ticket.description
          assert_equal suspended.from_mail, ticket.requester.email
          assert_equal suspended.from_name, ticket.requester.name
          assert_equal users(:minimum_agent), ticket.submitter
        end

        describe "with web portal restricted" do
          before do
            Account.any_instance.stubs(:web_portal_restricted?).returns(true)
            post :recover, params: { id: suspended.id }
          end

          it "sends a welcome email" do
            ticket = Ticket.last

            assert_equal "Welcome to #{accounts(:minimum).route.brand.name}", ActionMailer::Base.deliveries[0].subject
            assert_nil ticket.current_collaborators
          end
        end

        describe "with web portal in default disabled state" do
          before do
            post :recover, params: { id: suspended.id }
          end

          it "sends a welcome email" do
            ticket = Ticket.last

            refute_equal "Welcome to #{accounts(:minimum).route.brand.name}", ActionMailer::Base.deliveries[0].subject
            assert_nil ticket.current_collaborators
          end
        end
      end
    end
  end

  describe "#destroy" do
    it "works" do
      login('minimum_agent')
      assert_difference "SuspendedTicket.count(:all)", -1 do
        post :destroy, params: { id: suspended_tickets(:minimum_spam).id }
      end
    end
  end

  describe "#show" do
    let(:suspended) { suspended_tickets(:minimum_unknown_author) }

    before { login(:minimum_agent) }

    describe "showing a suspended ticket with recover as users" do
      before do
        suspended.update_attributes!(from_mail: users(:minimum_end_user).email, ticket: tickets(:minimum_1))
        get :show, params: { id: suspended.id }
      end

      it "provides an option to recover as an unspecified user" do
        assert_select 'select[name=recover_as]' do
          assert_select "option[value='user:']"
        end
      end
    end

    describe "showing a suspended ticket without recoverable users" do
      before do
        SuspendedTicket.any_instance.stubs(:recover_as_users).returns([])
        get :show, params: { id: suspended.id }
      end

      it "does not provide a 'recover as user' option" do
        assert_select 'select[name=recover_as]' do
          assert_select "option[value='user:']", count: 0
        end
      end

      it "provides a basic recover option" do
        assert_select 'select[name=recover_as]' do
          assert_select "option[value='default']"
        end
      end
    end

    describe "when coming from a mobile device" do
      it "redirects to the desktop version of the site" do
        get :show, params: { id: suspended.id, format: 'mobile_v2' }
        assert_response :redirect
        assert_redirected_to suspended_ticket_path(suspended.id)
      end
    end
  end

  describe "#index" do
    before { login(:minimum_agent) }

    it "renders" do
      get :index
      assert_response :ok
    end

    describe "rss" do
      before do
        accounts(:minimum).suspended_tickets.first.update_attribute(:created_at, 5.days.ago)
      end

      it "renders rss" do
        accept :rss
        get :index
        assert_response :ok
        response.body.must_include "rss version=\"2.0\""
      end

      it "does not double escape HTML characters" do
        accept :rss
        get :index
        assert_response :ok
        response.body.wont_include "&amp;amp;"
      end
    end

    describe "mobile" do
      it "redirects to the desktop version of the site" do
        get :index, format: 'mobile_v2'
        assert_response :redirect
        assert_redirected_to suspended_tickets_path
      end
    end
  end

  describe "#order" do
    describe "cause" do
      it "gives the right ordering in English" do
        @controller.stubs(:params).returns("order_by" => "cause")
        assert_equal "FIELD(cause,28,3,7,8,19,14,4,0,9,26,32,16,31,27,11,15,23,13,24,18,29,25,30,20,10,22,17,1,21,2,5,6,12)", @controller.send(:order)
      end

      it "gives the right ordering in Japanese" do
        @controller.stubs(:params).returns("order_by" => "cause")
        japanese = translation_locales(:japanese)
        ::I18n.config.stubs(translation_locale: japanese, locale: japanese.to_sym)
        assert_equal "FIELD(cause,28,3,7,8,19,14,4,9,26,32,16,31,27,11,15,23,13,24,18,29,25,30,20,10,22,17,1,21,2,5,6,12,0)", @controller.send(:order)
      end
    end

    describe "garbage" do
      it "returns date ordering" do
        @controller.stubs(:params).returns("order_by" => "garbage")
        assert_equal @controller.send(:order), created_at: :desc
      end
    end
  end
end
