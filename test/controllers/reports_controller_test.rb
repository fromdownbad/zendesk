require_relative "../support/test_helper"
require_relative "../support/attachment_test_helper"

SingleCov.covered! uncovered: 22

describe ReportsController do
  fixtures :all

  def report_cache_key(token = assigns(:token))
    "#{@account.id}/report_data_#{token}"
  end

  should_include_module Zendesk::LotusEmbeddable

  before do
    @account = accounts(:minimum)
    @min_subscription = subscriptions(:minimum)
    @request.account  = @account
  end

  should_be_unauthorized_when_not_logged_in([:index, :new, :create, :edit, :update, :destroy], 'minimum_end_user', 403)

  describe "When the current user is an admin" do
    before { login(:minimum_admin) }

    describe "Requesting the index page" do
      before { get :index }

      it "is accessible for admins" do
        assert_response :ok
      end

      before_should "register activity on the current device" do
        @controller.expects(:register_device_activity)
      end
    end
  end

  describe "#index" do
    describe "without analytics access" do
      before do
        @controller.stubs(analytics_access?: false)
        login(:minimum_agent)
        get :index
      end

      it "is denied access" do
        assert_response :forbidden
      end
    end

    describe 'deprecated tabs' do
      describe 'when the account has Classic reporting' do
        before do
          @account.created_at = Time.new(2013, 6, 1)
          @account.save!
        end

        ['Overview', 'Reports'].each do |tab_name|
          it "shows the #{tab_name} tab" do
            login(:minimum_agent)

            get :index
            assert_match(/data-title-value="#{tab_name}"/, @response.body)
          end
        end
      end

      describe "when the account doesn't have classic reporting" do
        before do
          @account.created_at = Time.new(2015, 6, 1)
          @account.save!
        end

        ['Overview', 'Reports', 'Forum', 'Search'].each do |tab_name|
          it "does not show the #{tab_name} tab" do
            login(:minimum_agent)

            get :index
            refute_match(/data-title-value="#{tab_name}"/, @response.body)
          end
        end
      end
    end

    describe "export tab" do
      let(:admin_not_owner) { users(:minimum_admin_not_owner) }
      let(:owner)           { users(:minimum_admin) }

      it "shows for the owner" do
        assert owner.is_account_owner?
        login(owner)

        get :index
        assert_match(/Export/, @response.body)
      end

      it "does not show for admins not in the whitelisted domain" do
        Zendesk::Export::Configuration.any_instance.
          expects(:whitelisted?).
          with(admin_not_owner).returns(false)

        login(admin_not_owner)

        get :index
        assert_no_match(/Export/, @response.body)
      end

      it "shows for admins in the whitelisted domain" do
        Zendesk::Export::Configuration.any_instance.
          expects(:whitelisted?).
          with(admin_not_owner).returns(true)

        login(admin_not_owner)

        get :index
        assert_match(/Export/, @response.body)
      end

      it "does not show #export tab if not admin" do
        login(:minimum_agent)
        get :index
        assert_no_match /Export/, @response.body
      end

      it "renders normal page if I am not allowed to see export tab" do
        login(:minimum_agent)
        get :index, params: { tab: "export" }
        @response.body.must_include "<head>"
      end

      it "renders bad request with unrecognized tab param" do
        @controller.stubs(:current_brand).returns(@account.brands.first)
        login(:minimum_agent)
        get :index, params: { tab: "bad_param" }
        assert_response 404
      end

      it "displays a link to the latest xml report if available" do
        login('minimum_admin')
        XmlExportJob.stubs(:available_for?).returns(true)
        attachment = create_expirable_attachment("#{Rails.root}/test/files/highrise.xml", @account, klass: ExpirableAttachment)
        get :index, params: { tab: "export" }
        assert_response :ok
        assert_equal attachment, assigns(:latest_xml_export)
        assert_no_match(/<head>/, @response.body)
      end

      it 'displays xml report warning for accounts with ticket count greater than limit' do
        login('minimum_admin')
        @account.subscription.update_attribute(:plan_type, SubscriptionPlanType.ExtraLarge)
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
        @account.reload
        @controller.stubs(:total_ticket_count).returns(200_000)
        stub_request(:get, 'http://localhost:5000/api/v2/exports').
          to_return(status: 200, body: { 'export_jobs' => [] }.to_json, headers: { 'Content-Type' => 'application/json' })
        stub_request(:get, "http://voyager-service:5000/api/v2/exports").
          to_return(status: 200, body: { 'export_jobs' => [] }.to_json, headers: { 'Content-Type' => 'application/json' })

        get :index, params: { tab: "export" }
        assert_response :ok
        assert_select '#xml_export_warning'
      end

      describe 'voyager enabled export tab' do
        let(:previous_exports) { [{ start_time: 1, end_time: 100, export_type: 'tickets', state: 'finished', result_file: 'http://some_file_url.com' }] }

        before do
          Timecop.freeze
          login(owner)
          Arturo.enable_feature!(:voyager_csv_export_history)
          @account.subscription.update_attribute(:plan_type, SubscriptionPlanType.ExtraLarge)
          Zendesk::Features::SubscriptionFeatureService.new(@account).execute!
          @account.reload
          stub_request(:get, 'http://localhost:5000/api/v2/exports').
            to_return(status: 200, body: { 'export_jobs' => [] }.to_json, headers: { 'Content-Type' => 'application/json' })
          stub_request(:get, "http://voyager-service:5000/api/v2/exports").
            to_return(status: 200, body: { 'export_jobs' => [] }.to_json, headers: { 'Content-Type' => 'application/json' })
        end

        describe 'with json_exports enabled' do
          before do
            Arturo.enable_feature!(:voyager_json_export)
            Arturo.disable_feature!(:voyager_ranged_json_exports)
          end

          describe 'default behaviour' do
            it 'shows the json exports pannel' do
              get :index, params: { tab: "export" }
              assert_template partial: 'reports/tabs/_export'
              assert_template partial: 'reports/tabs/exports/_voyager'
              assert_select '#json_export' do
                assert_select '.item-actions' do
                  assert_select "a[href='/voyager/voyager_exports?export_format=json&export_type=tickets']", count: 1
                  assert_select "a[href='/voyager/voyager_exports?export_format=json&export_type=users']", count: 1
                  assert_select "a[href='/voyager/voyager_exports?export_format=json&export_type=organizations']", count: 1
                end
              end
            end
          end

          describe 'with voyager_ranged_json_exports enabled' do
            before do
              Arturo.enable_feature!(:voyager_ranged_json_exports)
            end

            it 'does not show the default export links' do
              get :index, params: { tab: "export" }
              assert_template partial: 'reports/tabs/_export'
              assert_template partial: 'reports/tabs/exports/_voyager'

              assert_select "a[href='/voyager/voyager_exports?export_format=json&export_type=tickets']", count: 0
              assert_select "a[href='/voyager/voyager_exports?export_format=json&export_type=users']", count: 0
              assert_select "a[href='/voyager/voyager_exports?export_format=json&export_type=organizations']", count: 0
            end

            it 'shows the ranged export selector in the json exports section' do
              get :index, params: { tab: "export" }
              assert_template partial: 'reports/tabs/_export'
              assert_template partial: 'reports/tabs/exports/_voyager'
              assert_select '#json_export' do
                assert_select '#voyager_json_export' do
                  assert_select "select#export_type"
                  assert_select ".button[value='Export']"
                  assert_select "#json_start_date[value='0']"
                  assert_select "#json_end_date[value='#{Time.now.to_i}']"
                  assert_select "#export_format[value='json']"
                end
              end
            end

            it 'shows a list of previous exports indicating type of each export' do
              stub_request(:get, 'http://localhost:5000/api/v2/exports').
                to_return(status: 200, body: { 'export_jobs' => previous_exports }.to_json, headers: { 'Content-Type' => 'application/json' })
              stub_request(:get, "http://voyager-service:5000/api/v2/exports").
                to_return(status: 200, body: { 'export_jobs' => previous_exports }.to_json, headers: { 'Content-Type' => 'application/json' })

              get :index, params: { tab: "export" }
              assert_template partial: 'reports/tabs/_export'
              assert_template partial: 'reports/tabs/exports/_voyager'

              assert_select '#json_export' do
                assert_select '#json_export_history > .item', count: 1
                assert_select '#json_export_history > .item span:last-child' do |elem|
                  elem.html_must_include "tickets"
                end
              end
            end
          end
        end

        describe 'voyager history enabled but no previous exports' do
          it 'shows the ranged export selector but no previous export list' do
            @account.stubs(extended_ticket_metrics_visible?: false)
            get :index, params: { tab: "export" }
            assert_template partial: 'reports/tabs/_export'
            assert_template partial: 'reports/tabs/exports/_voyager'

            assert_select '#voyager_csv_export' do
              assert_select ".button[value='Export']"
              assert_select "#csv_start_date[value='0']"
              assert_select "#csv_end_date[value='#{Time.now.to_i}']"
            end
          end
        end

        describe 'with both ranged json and csv exports enabled' do
          before do
            Arturo.enable_feature!(:voyager_json_export)
            Arturo.enable_feature!(:voyager_ranged_json_exports)
          end

          let(:previous_json_exports) { [{ start_time: 1, end_time: 100, export_type: 'organizations', state: 'finished', result_file: 'http://some_file_url.com' }] }
          let(:previous_csv_exports) do
            [
              { start_time: 1, end_time: 100, export_type: 'tickets', state: 'finished', result_file: 'http://some_file_url.com' },
              { start_time: 1, end_time: 100, export_type: 'tickets', state: 'finished', result_file: 'http://some_file_url_2.com' }
            ]
          end

          it 'shows both lists of previous exports' do
            query_string = {
              account_id: @account.id,
              per_page: 10,
              sort_order: 'DESC',
              state: 'finished'
            }

            # For local tests
            stub_request(:get, "http://voyager-service:5000/api/v2/exports").with(body: query_string.merge(format_type: 'csv').to_query).
              to_return(status: 200, body: { export_jobs: previous_csv_exports }.to_json, headers: { 'Content-Type' => 'application/json' })

            stub_request(:get, "http://voyager-service:5000/api/v2/exports").with(body: query_string.merge(format_type: 'json').to_query).
              to_return(status: 200, body: { export_jobs: previous_json_exports }.to_json, headers: { 'Content-Type' => 'application/json' })

            # For Travis
            stub_request(:get, "http://localhost:5000/api/v2/exports").with(body: query_string.merge(format_type: 'csv').to_query).
              to_return(status: 200, body: { export_jobs: previous_csv_exports }.to_json, headers: { 'Content-Type' => 'application/json' })

            stub_request(:get, "http://localhost:5000/api/v2/exports").with(body: query_string.merge(format_type: 'json').to_query).
              to_return(status: 200, body: { export_jobs: previous_json_exports }.to_json, headers: { 'Content-Type' => 'application/json' })

            get :index, params: { tab: "export" }
            assert_template partial: 'reports/tabs/_export'
            assert_template partial: 'reports/tabs/exports/_voyager'

            assert_select '#json_export' do
              assert_select '#json_export_history > .item', count: 1
              assert_select '#json_export_history > .item span:last-child' do |elem|
                elem.html_must_include "organizations"
              end
            end

            assert_select '#csv_export' do
              assert_select '#csv_export_history > .item', count: 2
            end
          end
        end

        it 'shows the basic request csv file link if voyager enabled but history arturo disabled' do
          Arturo.disable_feature!(:voyager_csv_export_history)
          stub_request(:get, 'http://localhost:5000/api/v2/exports').
            to_return(status: 200, body: { 'export_jobs' => previous_exports }.to_json, headers: { 'Content-Type' => 'application/json' })
          stub_request(:get, "http://voyager-service:5000/api/v2/exports").
            to_return(status: 200, body: { 'export_jobs' => previous_exports }.to_json, headers: { 'Content-Type' => 'application/json' })
          get :index, params: { tab: "export" }
          assert_template partial: 'reports/tabs/exports/_voyager'
          assert_select '.csv-export-history a', 0
          assert_select '#voyager_csv_export', 0
        end

        it 'shows the basic request csv file link and legacy metrics link if history arturo disabled but has extended_metrics_visible' do
          Arturo.disable_feature!(:voyager_csv_export_history)
          @account.stubs(extended_ticket_metrics_visible?: true)
          get :index, params: { tab: "export" }
          assert_template partial: 'reports/tabs/_export'
          assert_template partial: 'reports/tabs/exports/_voyager'
          assert_select '#legacy_metrics_link', 'request file with metrics'
        end

        it 'shows the latest export link if voyager enabled but history arturo disabled' do
          Arturo.disable_feature!(:voyager_csv_export_history)
          stub_request(:get, 'http://localhost:5000/api/v2/exports').
            to_return(status: 200, body: { 'export_jobs' => [previous_exports.first] }.to_json, headers: { 'Content-Type' => 'application/json' })
          stub_request(:get, "http://voyager-service:5000/api/v2/exports").
            to_return(status: 200, body: { 'export_jobs' => [previous_exports.first] }.to_json, headers: { 'Content-Type' => 'application/json' })
          get :index, params: { tab: "export" }
          assert_template partial: 'reports/tabs/exports/_voyager'
          assert_select ".csv-export-request-link a[href='#{previous_exports.first[:result_file]}']", 'latest'
        end

        it 'shows historical items in list' do
          stub_request(:get, 'http://localhost:5000/api/v2/exports').
            to_return(status: 200, body: { 'export_jobs' => previous_exports }.to_json, headers: { 'Content-Type' => 'application/json' })
          stub_request(:get, "http://voyager-service:5000/api/v2/exports").
            to_return(status: 200, body: { 'export_jobs' => previous_exports }.to_json, headers: { 'Content-Type' => 'application/json' })

          get :index, params: { tab: 'export' }
          assert_template partial: 'reports/tabs/exports/_voyager'
          assert_select '.csv-export-history a', count: 1, text: 'download'
          assert_select '#voyager_csv_export' do
            assert_select ".button[value='Export']"
            assert_select "#csv_start_date[value='100']"
            assert_select "#csv_end_date[value='#{Time.now.to_i}']"
          end
        end
      end
    end
  end

  describe '#preview' do
    let(:valid_parameters) do
      {
        report: {
          title: '"><img src="http://cdn.zendesk.com/something.png" alt="Test">',
          is_relative_interval: "true",
          relative_interval_in_days: "7",
          "from_date(1i)": "2016",
          "from_date(2i)": "3",
          "from_date(3i)": "28",
          "to_date(1i)": "2016",
          "to_date(2i)": "4",
          "to_date(3i)": "4"
        },
        sets: {
          "1": {
            legend: "blah \"'<script>alert(1)</script>",
            state: "any",
            conditions: {
              "1": {
                source: "ticket_type_id",
                operator: "is",
                value: {
                  "0": "3"
                }
              }
            }
          }
        }
      }
    end

    before do
      @minimum_admin = users(:minimum_admin)
      login(@minimum_admin)
    end

    it 'renders' do
      post :preview, params: valid_parameters

      assert_includes @response.body, %{so.addVariable("data_file", "/reports/#{assigns(:token)}/data.xml");}
    end

    it 'escapes the report title' do
      post :preview, params: valid_parameters

      @response.body.must_include "Preview for report \"&quot;&gt;&lt;img src=&quot;http://cdn.zendesk.com/something.png&quot; alt=&quot;Test&quot;&gt;"
    end

    it 'stores escaped data in cache' do
      post :preview, params: valid_parameters

      data = Rails.cache.read(report_cache_key)
      assert data
      assert_includes data, "title=\"blah \\&quot;\\'&amp;lt;script&gt;alert(1)&amp;lt;/script&gt;\""
    end

    it 'renders correct error messages' do
      post :preview, params: { report: { "is_relative_interval" => "true", "relative_interval_in_days" => "7" } }

      assert_template 'reports/_preview_errors'
      assert_match /Title:\s*#{I18n.t("activerecord.errors.messages.blank")}/, @response.body
      assert_match /Definition:\s*#{I18n.t('txt.errors.report.definition_errors.must_contain_at_least_one_data_series')}/, @response.body
    end
  end

  describe "#show" do
    before do
      @minimum_admin = users(:minimum_admin)
      login(@minimum_admin)

      params  = { sets: { "1" => { conditions: {}, legend: 'Working tickets', state: 'working' }}}
      @report = Report.create!(
        title: "report title", author: @minimum_admin, relative_interval_in_days: 30,
        sets: params[:sets], account: @account
      )
    end

    it "renders the show template" do
      get :show, params: { id: @report.id }
      assert_response :ok
      assert_includes @response.body, %{so.addVariable("data_file", "/reports/#{assigns(:token)}/data.xml");}
    end

    it 'stores data in cache' do
      get :show, params: { id: @report.id }

      data = Rails.cache.read(report_cache_key)
      assert data
      assert_includes data, "title=\"Working tickets\""
    end

    it "shows the export links when the user has 'manage' access" do
      User.any_instance.stubs(:can?).returns(true)

      get :show, params: { id: @report.id }

      assert_select "a[href='#{report_path(@report, format: 'csv')}']"
      assert_select "a[href='#{report_path(@report, format: 'xml')}']"
    end

    it "does not show the export links when the user has no 'manage' access" do
      User.any_instance.stubs(:can?).returns(true)
      User.any_instance.stubs(:can?).with(:manage, Report).returns(false)

      get :show, params: { id: @report.id }

      assert_select "a[href='#{report_path(@report, format: 'csv')}']", count: 0
      assert_select "a[href='#{report_path(@report, format: 'xml')}']", count: 0
    end

    it "responds with XML" do
      get :show, params: { id: @report.id, format: "xml" }
      assert_response :ok
      assert_match /<legend>Working tickets<\/legend>/, @response.body
    end

    describe "account has more than 20,000 tickets" do
      before do
        Zendesk::Reports::Processor.any_instance.stubs(:runs_in_background?).returns(true)
      end

      it "runs the job in the background" do
        Resque.expects(:enqueue).with(ReportJob, @account.id, @minimum_admin.id, @report.id, anything)
        get :show, params: { id: @report.id }
      end

      it "limits the run to once every 30 minutes" do
        Resque.expects(:enqueue).with(ReportJob, @account.id, @minimum_admin.id, @report.id, anything).raises(Resque::ThrottledError)
        get :show, params: { id: @report.id }
        flash[:notice].must_include "This report can only be rebuilt every 30 minutes."
      end

      it "tells the user to check back in a few minutes if there isn't a previous run stored of the report" do
        assert @report.result.blank?
        Resque.expects(:enqueue).with(ReportJob, @account.id, @minimum_admin.id, @report.id, anything)
        get :show, params: { id: @report.id }
        flash[:notice].must_include "Check back in a few minutes."
      end

      it "displays the last run of the report if available" do
        ReportJob.perform(@account.id, @minimum_admin.id, @report.id)
        assert @report.reload.result
        Resque.expects(:enqueue).with(ReportJob, @account.id, @minimum_admin.id, @report.id, anything)
        get :show, params: { id: @report.id }
        flash[:notice].must_include "Now presenting the previous result"
      end
    end
  end

  describe "#destroy" do
    before do
      @minimum_admin = users(:minimum_admin)
      login(@minimum_admin)

      params  = { sets: { "1" => { conditions: {}, legend: 'Working tickets', state: 'working' }}}
      @report = Report.create!(
        title: '"><img src="http://cdn.zendesk.com/something.png" alt="Test">',
        author: @minimum_admin, relative_interval_in_days: 30,
        sets: params[:sets], account: @account
      )
      delete :destroy, params: { id: @report.id }
    end

    it "properly escape the report title" do
      flash[:notice].must_include "&quot;&gt;&lt;img src=&quot;http://cdn.zendesk.com/something.png&quot; alt=&quot;Test&quot;&gt;\" deleted"
    end

    should_redirect_to('reports index') { reports_path(anchor: 'reports') }
  end

  describe "#create" do
    before do
      @minimum_admin = users(:minimum_admin)
      login(@minimum_admin)

      params = { report: { is_relative_interval: "true", relative_interval_in_days: 30,
                           title: '"><img src="http://cdn.zendesk.com/something.png" alt="Test">' },
                 sets: { "1" => { legend: 'Legendary', state: 'created_at' }} }
      post :create, params: params
    end

    it "properly escape the report title" do
      # RAILS5UPGRADE: rails 4 returns &quot; whereas rails 5 provides an actual "
      title = if RAILS4
        "&quot;&gt;&lt;img src=&quot;http://cdn.zendesk.com/something.png&quot; alt=&quot;Test&quot;&gt;</a> created"
      else
        "\"&gt;&lt;img src=\"http://cdn.zendesk.com/something.png\" alt=\"Test\"&gt;</a> created"
      end
      assert_match title, flash[:notice]
    end

    should_redirect_to('reports index') { reports_path(anchor: 'reports') }
  end

  describe "#amline" do
    before do
      @minimum_admin = users(:minimum_admin)
      login(@minimum_admin)
    end

    it "renders file when requested without bad params" do
      get :amline
      assert_equal 'inline; filename="amline.swf"', response.headers["Content-Disposition"]
      assert_equal "application/x-shockwave-flash", response.headers["Content-Type"]
    end

    it "redirects to self when requested with bad params" do
      get :amline, params: { "settings_file" => "foo", "data_file" => "bar" }
      response.body.must_include "redirected"
      response.location.must_include "reports/amline"
    end

    it "redirects to self when requested with dangerous" do
      get :amline, params: { "chart_settings" => "foo", "chart_data" => "bar" }
      response.body.must_include "redirected"
      response.location.must_include "reports/amline"
    end
  end

  describe "#data" do
    before do
      @minimum_admin = users(:minimum_admin)
      login(@minimum_admin)
      params = { sets: { "1" => { conditions: {}, legend: "Working \"'<script>alert(1)</script> tickets", state: 'working' }}}
      @report = Report.create!(
        title: "report title", author: @minimum_admin, relative_interval_in_days: 30,
        sets: params[:sets], account: @account
      )
      @token = "foobar"
      Rails.cache.write(report_cache_key(@token), "xml data goes here")
      get :data, params: { id: @token, format: "xml" }
      assert_response :ok
    end

    it "returns the correct data for the report from the cache" do
      assert_equal @response.body.strip, "xml data goes here"
    end

    it 'does not delete token after data retrieval because of SWF reloading' do
      assert Rails.cache.read(report_cache_key(@token))
    end
  end

  describe "#insights" do
    before do
      @gooddata_integration = stub('gooddata integration')
      @account.stubs(:gooddata_integration).returns(@gooddata_integration)

      @provisioning = stub('provisioning')
      Zendesk::Gooddata::UserProvisioning.stubs(:new).with(@account).returns(@provisioning)

      @user = users(:minimum_agent)
      login(:minimum_agent)
    end

    describe "the user doesn't have access to insights" do
      before do
        @controller.stubs(analytics_access?: false)
        get :insights
      end

      it "is denied access" do
        assert_response :forbidden
      end
    end

    describe "the user hasn't been provisioned with a gooddata user" do
      before do
        @gooddata_integration.stubs(:portal_sso_url).returns('gooddata_sso_url')

        get :insights
      end

      before_should "provision a gooddata user" do
        @provisioning.expects(:create_gooddata_user).with(@user)
      end
    end

    describe "the user has already been provisioned with a gooddata user" do
      before do
        user = stub('gooddata user')
        GooddataUser.stubs(:for_user).returns(user)
        @gooddata_integration.stubs(:portal_sso_url).with(user).returns('gooddata_sso_url')

        get :insights
      end

      it "redirects to gooddata" do
        assert_redirected_to 'gooddata_sso_url'
      end
    end
  end
end
