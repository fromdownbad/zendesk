require_relative "../support/test_helper"

SingleCov.covered! uncovered: 36

describe AttachmentTokenController do
  # tested via test/integration/attachment_controller_test.rb
  with_options(controller: 'attachment_token') do |request|
    request.should_route :get, '/attachment/123/abc123', account_id: 123, attachment_token: 'abc123', action: 'show'
  end

  describe '#attachment_domain_host' do
    describe 'in production/test' do
      it 'returns a pX.zdusercontent.com URL' do
        subject.send(:attachment_domain_host, pod: 2).must_equal "https://p2.zdusercontent.com"
      end
    end

    describe 'in staging/master' do
      it 'returns a assetN.zendesk-ENV.com URL' do
        Rails.env.stubs(:test?).returns(false)
        subject.send(:attachment_domain_host, pod: 2).must_equal "https://asset2.zendesk-test.com"
      end
    end
  end
end
