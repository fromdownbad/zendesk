require_relative "../support/test_helper"

SingleCov.covered! uncovered: 5

VerificationController.class_eval do
  public :current_device
end

describe VerificationController do
  fixtures :all

  before do
    Account.any_instance.stubs(:use_status_hold?).returns(true)
    account = accounts(:minimum)
    account.stubs(:use_status_hold?).returns(true)
    @request.account = account
    brand = brands(:minimum)
    account.default_brand = brand
    account.save!
    @route = brand.route

    @controller.stubs(:current_brand).returns(@request.account.default_brand)
    @controller.stubs(:current_route).returns(@route)
    @request.account.stubs(has_mobile_switch_enabled?: true)

    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
  end

  describe "Verifying details for a user without a password, on a security policy that enforces a unique password history" do
    let(:account) { accounts(:minimum) }

    before do
      Account.any_instance.stubs(:security_policy).returns(Zendesk::SecurityPolicy::High.new)
      @user = FactoryBot.create(:agent, account: account, password: nil, skip_verification: nil)
      @user.reload
      refute @user.is_verified?
      assert @user.password.blank?
    end

    it "succeeds when providing a new valid password" do
      token          = @user.verification_tokens.create(expires_at: 3.days.from_now)
      valid_password = '123456A-a'

      post :details, params: { token: token.value, user: { name: 'Buddhy', password: valid_password } }
      assert_nil flash[:error]
      assert flash[:notice]

      @user.reload
      assert_empty(ActionMailer::Base.deliveries)
      assert @user.is_verified?
      assert @user.authenticated?(valid_password)

      expected_audit = {
        "actor_id" => @user.id,
        "actor_type" => "User",
        "source_id" => @user.id,
        "source_type" => "User"
      }
      assert_equal expected_audit, @user.cia_events.last.attributes.slice("actor_id", "actor_type", "source_id", "source_type")
    end

    it "provides an error if a user illegally changes their name" do
      Access::Policies::UserPolicy.any_instance.stubs(:can_update?).returns(false)

      token = @user.verification_tokens.create(expires_at: 3.days.from_now)
      user_name = @user.name
      valid_password = '123456A-a'

      post :details, params: { token: token.value, user: { name: 'Buddhy', password: valid_password } }
      assert_equal I18n.t('txt.auth.v2.verification.illegal_user_change'), flash[:error]
      refute flash[:notice]

      @user.reload
      assert_empty(ActionMailer::Base.deliveries)
      refute @user.is_verified?
      refute @user.authenticated?(valid_password)
      assert_equal @user.name, user_name
    end
  end

  describe "#challenge" do
    before do
      @account = accounts(:minimum)
      @user    = @account.owner
      @user.update_attribute(:last_login, nil)
      @token = @account.owner.challenge_tokens.create
    end

    it "does not recognize an invalid token" do
      get :challenge, params: { token: "hest" }
      assert_response :bad_request
    end

    it "redirects to root" do
      get :challenge, params: { token: @token.value }
      assert_response :redirect
      assert_equal "https://minimum.zendesk-test.com/agent", @response.headers["Location"]
    end

    describe "create a new device" do
      before do
        get :challenge, params: { token: @token.value }
      end

      should_change("the number of devices", by: 1) do
        @user.devices(:reload).size
      end

      should_change("the number of mail deliveries", by: 0) do
        ActionMailer::Base.deliveries.size
      end

      it "sets device attributes" do
        assert_equal request.remote_ip, @controller.current_device.ip
        assert_equal request.user_agent, @controller.current_device.user_agent
      end
    end

    it "redirects to the small plan page for causeware signups" do
      get :challenge, params: { token: @token.value, causeware: "hi" }
      assert_response :redirect
      assert_match(/\/agent\/dashboard\/plan_selection\/small$/, @response.headers["Location"])

      assert_not_nil @account.trial_extras.where(key: 'mixpanel_location', value: 'buy from marketing').first
      assert_not_nil @account.trial_extras.where(key: 'mixpanel_subscription_plan_name', value: 'Starter').first
    end

    it "redirects to the specified plan if a valid plan_redirect value is passed" do
      get :challenge, params: { token: @token.value, plan_redirect: "extra_large" }
      assert_response :redirect
      assert_match(/\/agent\/dashboard\/plan_selection\/extra_large$/, @response.headers["Location"])

      assert_not_nil @account.trial_extras.where(key: 'mixpanel_location', value: 'buy from marketing').first
      assert_not_nil @account.trial_extras.where(key: 'mixpanel_subscription_plan_name', value: 'Enterprise').first
    end

    it "redirects to landing page if invalid plan_redirect is provided" do
      get :challenge, params: { token: @token.value, plan_redirect: "invalid_plan" }
      assert_response :redirect
      assert_equal "https://minimum.zendesk-test.com/agent", @response.headers["Location"]

      assert_equal 0, @account.trial_extras.where('trial_extras.key like ?', 'mixpanel_%').count(:all)
    end

    it 'saves trial extras' do
      get :challenge, params: { token: @token.value, plan_redirect: "extra_large" }

      view_grp_extra = @account.trial_extras.where(key: 'mixpanel_location').first
      plan_name_extra = @account.trial_extras.where(key: 'mixpanel_subscription_plan_name').first

      assert_not_nil view_grp_extra
      assert_equal 'buy from marketing', view_grp_extra.value
      assert_not_nil plan_name_extra
      assert_equal 'Enterprise', plan_name_extra.value
    end

    it 'saves features trial extras' do
      get :challenge, params: { token: @token.value, features: 'embeddables' }

      features = @account.trial_extras.where(key: "features").pluck(:value).first
      assert_equal 'embeddables', features
    end

    describe 'when Google signup' do
      before do
        @account.route.update_attribute(:gam_domain, 'foo.com')
        @account.stubs(:role_settings).returns(role_settings(:minimum))
        @account.role_settings.update_attribute(:agent_google_login, true)
      end

      it "redirects to universal navigation link with a login hint for G Suite accounts" do
        get :challenge, params: { token: @token.value }

        assert_response :redirect

        location = @response.headers["Location"]
        params = Rack::Utils.parse_nested_query(location.split('?', 2).last)
        location.must_include 'gam_universal_navigation_link'
        assert_equal @user.email, params['login_hint']
      end

      it 'redirects to onboarding_url if available' do
        onboarding_url = '/agent/onboarding_url'
        @account.stubs(:onboarding_url).returns(onboarding_url)
        get :challenge, params: { token: @token.value }

        assert_response :redirect
        assert_match Regexp.new(CGI.escape(onboarding_url)), @response.headers["Location"]
      end
    end

    describe 'when non-Google signup' do
      it 'redirects to onboarding_url if available' do
        onboarding_url = '/agent/onboarding_url'
        @account.stubs(:onboarding_url).returns(onboarding_url)
        get :challenge, params: { token: @token.value }

        assert_response :redirect
        assert_match Regexp.new(onboarding_url), @response.headers["Location"]
      end
    end

    describe 'with a shell account without support' do
      let(:account) { accounts(:shell_account_without_support) }
      let(:user) { users(:shell_owner_without_support) }

      before do
        @request.account = account
        user.identities.first.update_attribute(:is_verified, false)
        refute user.reload.is_verified?
      end

      describe 'sell_only_after_verification Arturo is disabled' do
        before do
          Arturo.disable_feature!(:sell_only_after_verification)
          token = user.challenge_tokens.create
          get :challenge, params: { token: token.value }
        end

        it 'redirects to root url /' do
          assert_response :redirect
          assert_equal "https://#{account.subdomain}.zendesk-test.com/", @response.headers["Location"]
        end
      end

      describe 'sell_only_after_verification Arturo is enabled' do
        before do
          Arturo.enable_feature!(:sell_only_after_verification)
          token = user.challenge_tokens.create
          get :challenge, params: { token: token.value }
        end

        it 'redirects to root url /?after_verification=1' do
          assert_response :redirect
          assert_equal "https://#{account.subdomain}.zendesk-test.com/?after_verification=1", @response.headers["Location"]
        end
      end

      it 'does not verify the user' do
        refute user.reload.is_verified?
      end

      describe 'post to details with sell_only_after_verification is disabled' do
        before do
          Arturo.disable_feature!(:sell_only_after_verification)
          token = user.verification_tokens.create(expires_at: 3.days.from_now)
          valid_password = '123456A-a'
          post :details, params: { token: token.value, user: { name: 'Buddhy', password: valid_password } }
        end

        it "is allowed" do
          assert_response :redirect
          assert_redirected_to 'https://shellaccountwithoutsupport.zendesk-test.com/'
          assert user.reload.is_verified?
        end
      end

      describe 'post to details with sell_only_after_verification is enabled' do
        before do
          Arturo.enable_feature!(:sell_only_after_verification)
          token = user.verification_tokens.create(expires_at: 3.days.from_now)
          valid_password = '123456A-a'
          post :details, params: { token: token.value, user: { name: 'Buddhy', password: valid_password } }
        end

        it "is allowed" do
          assert_response :redirect
          assert_redirected_to 'https://shellaccountwithoutsupport.zendesk-test.com/?after_verification=1'
          assert user.reload.is_verified?
        end
      end
    end

    describe "mass-assignment" do
      before do
        @user = users(:minimum_end_user)

        token          = @user.verification_tokens.create(expires_at: 3.days.from_now)
        valid_password = '123456A-a'

        post :details, params: { user: { name: 'Test User', password: valid_password, roles: Role::ADMIN.id }, token: token.value }
      end

      it('responds with redirect') { assert_response :redirect }

      it "does not reset the user role" do
        assert_equal Role::END_USER.id, @user.reload.roles
      end
    end

    describe "As an end-user trying to verify their email address" do
      before do
        @user = users(:to_be_verified)
        @user.update_attribute(:roles, Role::AGENT.id)
        @user.update_attribute(:last_login, nil)
        @user.update_attribute(:is_verified, false)
        @token = @user.verification_tokens.first.value
      end

      describe "and an identity already exists" do
        before do
          @secondary_identity = UserEmailIdentity.new(user: @user, value: 'aa@example.com')
          @user.identities << @secondary_identity
          @user.save
          @controller.send(:current_user=, @user)
        end

        it "does not set the identity as primary if make_primary is missing" do
          post :email, params: { token: @secondary_identity.verification_token }
          refute @secondary_identity.reload.primary?
        end

        it "sets the identity as primary if make_primary parameter is true" do
          post :email, params: { token: @secondary_identity.verification_token, make_primary: "true" }
          assert @secondary_identity.reload.primary?
        end

        it "removes the old primary identity of the user" do
          post :email, params: { token: @secondary_identity.verification_token, make_primary: "true" }
          assert_equal @user.identities.count(:all), 1
          assert_equal @user.identities(:reload).first, @secondary_identity
        end
      end

      describe "invalid fields" do
        describe "blank name" do
          before { post :details, params: { token: @token, user: { name: '', password: '123456' } } }

          it "fails" do
            flash[:error].must_include "Failed to update user"
            refute @user.reload.is_verified?
          end
        end

        describe "blank password" do
          before { post :details, params: { token: @token, user: { name: 'Buddhy', password: '' } } }

          it "fails" do
            flash[:error].must_include "Failed to update user"
            refute @user.reload.is_verified?
          end
        end
      end

      describe "after choosing a new password" do
        describe "from a regular browser" do
          before { post :details, params: { token: @token, user: { name: 'Buddhy', password: '123456' } } }
          should_redirect_to("root") { "https://minimum.zendesk-test.com/agent" }
        end

        describe "2FA is enforced on account" do
          before do
            @request.account.settings.create(name: 'two_factor_enforce', value: true)
            assert @request.account.settings.two_factor_enforce
          end

          it "redirects to  2FA login" do
            post :details, params: { token: @token, user: { name: 'Buddhy', password: '123456' } }

            assert_redirected_to "https://minimum.zendesk-test.com/auth/v2/login"
            assert_equal "Your account has been verified.", flash[:notice]
          end
        end

        describe "2FA is OFF on account" do
          before do
            @request.account.settings.create(name: 'two_factor_enforce', value: false)
            @request.account.settings.reload
            refute @request.account.settings.two_factor_enforce
          end

          it "logs in user" do
            post :details, params: { token: @token, user: { name: 'Buddhy', password: '123456' } }

            assert_redirected_to "https://minimum.zendesk-test.com/agent"
            assert_equal "Your account has been verified.", flash[:notice]
          end
        end

        describe "when not from a human and account has verification_captcha" do
          before do
            Account.any_instance.stubs(:has_verification_captcha_enabled?).returns(true)
            @controller.stubs(:verify_recaptcha).returns(false)
          end

          it "fails" do
            post :details, params: { token: @token, user: { name: 'Buddhy', password: '123456' } }

            flash[:error].must_include "Verification failed"
            assert_equal '123456', assigns(:user).password
            refute @user.reload.is_verified?
          end

          it "redirect_to_return_tos" do
            url = @request.account.url + '/failure'

            post :details, params: { token: @token, user: { name: 'Buddhy', password: '123456' }, return_to_on_failure: url }

            location = @response.headers['Location']

            assert location.start_with?(url), "#{location} is not return_to_on_failure"
          end

          describe "render sso template" do
            it "renders v2" do
              @controller.stubs(:current_brand).returns(@request.account.default_brand)
              @controller.stubs(:current_route).returns(@route)

              post :details, params: { token: @token, user: { name: 'Buddhy', password: '123456' } }
              assert_template :sso_v2_index
            end
          end
        end

        describe "when not from a human account does not have verification_captcha" do
          before do
            Account.any_instance.stubs(:has_verification_captcha_enabled?).returns(false)
            @controller.stubs(:verify_recaptcha).returns(false)
          end

          it "redirects to root" do
            post :details, params: { token: @token, user: { name: 'Buddhy', password: '123456' } }
            assert_redirected_to 'https://minimum.zendesk-test.com/agent'
          end

          describe "when entering a pasword that doesn't fit the password policy" do
            before do
              User.any_instance.stubs(:save).returns(false)
            end

            describe "render sso template" do
              it "renders v2" do
                @controller.stubs(:current_brand).returns(@request.account.default_brand)
                @controller.stubs(:current_route).returns(@route)

                post :details, params: { token: @token, user: { name: 'Buddhy', password: '123456' } }
                assert_template :sso_v2_index
              end
            end
          end
        end

        describe "when not from a human and is in whitelist" do
          before do
            Account.any_instance.stubs(:has_verification_captcha_enabled?).returns(true)
            @controller.stubs(:verify_recaptcha).returns(false)
            Account.any_instance.stubs(:domain_whitelist).returns("*")

            post :details, params: { token: @token, user: { name: 'Buddhy', password: '123456' } }
          end

          should_redirect_to("root") { "https://minimum.zendesk-test.com/agent" }
        end
      end
    end

    describe "As a new agent trying to verify their email address" do
      before do
        @user = users(:to_be_verified)
        @user.update_attribute(:roles, Role::AGENT.id)
        @user.update_attribute(:last_login, nil)
        @user.update_attribute(:is_verified, false)
        @token = @user.verification_tokens.first.value
      end

      describe "after choosing a new password" do
        describe "from a regular browser" do
          before { post :details, params: { token: @token, password: "123456" } }
          should_redirect_to("root") { "https://minimum.zendesk-test.com/agent" }
        end

        describe "when agent is a light agent" do
          before do
            User.any_instance.stubs(:is_light_agent?).returns(true)
            post :details, params: { token: @token, password: "123456" }
          end

          it "redirects to root" do
            assert_redirected_to 'https://minimum.zendesk-test.com/agent'
          end
        end

        describe "from a mobile device" do
          before do
            @request.user_agent = 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en)'
            assert @request.is_mobile?
            post :details, params: { token: @token, password: "123456" }
          end

          it "redirects to root" do
            assert_redirected_to 'https://minimum.zendesk-test.com/agent'
          end
        end
      end

      describe "with a '/verification/email' link" do
        before do
          @request.account = accounts(:trial)
          @token = tokens(:trial_token).value
          @identity = tokens(:trial_token).source
          @user = @identity.user
          @user.update_attribute(:roles, Role::AGENT.id)
          @user.update_attribute(:last_login, nil)
          @user.update_attribute(:is_verified, false)
          @controller.send(:current_user=, @user)
        end

        describe "and help_center_enabled, with expired token" do
          before do
            Account.any_instance.stubs(:help_center_enabled?).returns(true)
          end

          it "redirects to /hc/signin" do
            @controller.stubs(:current_brand).returns(@request.account.default_brand)
            @controller.stubs(:current_route).returns(@route)

            post :email, params: { token: SecureRandom.hex }

            assert_redirected_to '/hc/signin'
            assert_equal I18n.t('txt.admin.controllers.verification_controller.the_activation_link_you_followed_to_get_here_is_no_longer_valid_v2', reset_password_url: '/auth/v2/login/password_reset'), flash[:error]
          end
        end

        describe "and a password was already selected for him" do
          before do
            post :email, params: { token: @token }
          end

          should_redirect_to("root") { "https://trial.zendesk-test.com/agent" }
        end

        describe "and account_owner signing up from a specific product" do
          before do
            Account.any_instance.stubs(:onboarding_url).returns('/hc/start')
            post :email, params: { token: @token }
          end

          should_redirect_to("onboarding_url") { "https://trial.zendesk-test.com/hc/start" }
        end

        describe "and no password was selected for him yet" do
          before do
            User.any_instance.stubs(:crypted_password).returns(nil)
            Account.any_instance.stubs(:owner).returns(nil)
          end

          describe "and the request is from a mobile device" do
            before do
              @request.user_agent = 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en)'
              assert @request.is_mobile?
            end

            it "renders mobile SSO template" do
              post :email, params: { token: @token }
              assert_template :sso_v2_index
            end
          end

          describe "render the SSO template" do
            it "renders v2" do
              @controller.stubs(:current_brand).returns(@request.account.default_brand)
              @controller.stubs(:current_route).returns(@route)
              post :email, params: { token: @token }
              assert_template :sso_v2_index
            end
          end
        end

        describe "and the user is already verified" do
          before do
            UserEmailIdentity.any_instance.stubs(:is_verified?).returns(true)
            Account.any_instance.stubs(:owner).returns(nil)
          end

          it "redirects to login page" do
            post :email, params: { token: @token }
            assert_redirected_to login_path
          end

          it "displays an error message" do
            post :email, params: { token: @token }
            assert_equal I18n.t('txt.verification.already_verified'), flash[:error]
          end
        end

        describe "from a mobile device" do
          before do
            @request.account.stubs(:has_mobile_switch_enabled?).returns(true)
            @request.user_agent = 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en)'
            post :email, params: { token: @token }
          end

          it "redirects to root" do
            assert_redirected_to "https://trial.zendesk-test.com/agent"
          end
        end

        describe "with a shell account without support" do
          let(:account) { accounts(:shell_account_without_support) }
          let(:user) { users(:shell_owner_without_support) }

          before do
            @request.account = account
            @controller.send(:current_user=, user)
            stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
              to_return(body: { products: [] }.to_json, headers: { content_type: 'application/json; charset=utf-8' })
            user.identities.first.update_attribute(:is_verified, false)
            refute user.reload.is_verified?
          end

          describe 'with sell_only_after_verification disabled' do
            before do
              Arturo.disable_feature!(:sell_only_after_verification)
              token = user.first_valid_token.value
              post :email, params: { token: token }
            end

            should_redirect_to("root") { "https://#{account.subdomain}.zendesk-test.com/" }

            it "verifies the user" do
              assert user.reload.is_verified?
            end
          end

          describe "with sell_only_after_verification enabled" do
            before do
              Arturo.enable_feature!(:sell_only_after_verification)
              token = user.first_valid_token.value
              post :email, params: { token: token }
            end

            should_redirect_to("root") { "https://#{account.subdomain}.zendesk-test.com/?after_verification=1" }

            it "verifies the user" do
              assert user.reload.is_verified?
            end
          end
        end
      end
    end

    describe "As the owner trying to verify their email address" do
      before do
        @request.account = accounts(:trial)
        @token = tokens(:trial_token).value
        @identity = tokens(:trial_token).source
        @user = @identity.user
        @user.update_attribute(:is_verified, false)
        User.any_instance.stubs(:crypted_password).returns(nil)
        @controller.stubs(:current_brand).returns(@request.account.default_brand)
        @controller.stubs(:current_route).returns(@route)

        post :email, params: { token: @token }
      end

      should_render_template :sso_v2_index
    end

    describe "verification with a valid suspended ticket token" do
      before do
        @ticket_count = Ticket.count(:all)
        @suspended_ticket = suspended_tickets(:minimum_unknown_author)

        get :ticket, params: { token: "foo" }
      end

      it "responds with 200 OK" do
        assert_response :success
      end

      it "finds the suspended ticket" do
        assert_equal @suspended_ticket, assigns(:suspended_ticket)
      end

      it "recovers the ticket" do
        assert_equal @ticket_count + 1, Ticket.count(:all)
      end

      should_render_template 'sso_v2_index'
    end

    describe "verification with a valid suspended ticket token with hc enabled" do
      before do
        @suspended_ticket = suspended_tickets(:minimum_unverified)
        @suspended_ticket.token = "my_secret_token"
        @suspended_ticket.save!

        @request.account.stubs(:help_center_enabled?).returns(true)

        get :ticket, params: { token: "my_secret_token" }
      end

      it "redirects to hc login form" do
        @response.redirected_to.must_include "/login"
        @response.redirected_to.must_include "theme=hc"
      end
    end

    describe "verification of a ticket fails" do
      before { @suspended_ticket = suspended_tickets(:minimum_unknown_author) }

      it "trys to recover the suspended ticket" do
        Zendesk::Tickets::Recoverer.any_instance.expects(:recover_ticket).raises(Zendesk::Tickets::Recoverer::RecoveryFailed)

        get :ticket, params: { token: @suspended_ticket.token }
      end

      it "responds with 200 OK" do
        assert_response :success
      end
    end

    describe "verification with invalid suspended ticket token" do
      it "redirects to password reset url if there is no SuspendedTicket and no verified user is logged in" do
        get :ticket, params: { token: "foo1" }
        assert_redirected_to '/access/help'
        assert_equal I18n.t('txt.verification_controller.thanks_for_verifying_your_request'), flash[:notice]
      end

      it "redirects to /hc if there is no SuspendedTicket and a verified used is logged in" do
        login(:minimum_author)
        get :ticket, params: { token: "foo1" }
        assert_redirected_to "/hc"
      end
    end

    describe "an unknown user with a submitted suspended ticket" do
      before do
        @suspended_ticket = suspended_tickets(:minimum_unverified_by_unknown_author)
        @suspended_ticket.token = "my_secret_token"
        @suspended_ticket.save!
      end

      it "recovers ticket and automatically verify email" do
        assert_difference('Ticket.count(:all)', 1) do
          get :ticket, params: { token: @suspended_ticket.token }
        end
        user = @controller.send(:current_account).find_user_by_email(@suspended_ticket.from_mail)
        assert user.is_verified?
        refute @suspended_ticket.deleted
        assert_response :success
      end

      describe "when without a help center" do
        before do
          get :ticket, params: { token: @suspended_ticket.token }
        end

        should_render_template 'sso_v2_index'

        it "renders HC layout anyway" do
          assert_template layout: 'layouts/help_center'
        end
      end

      describe "when using sso_login" do
        before do
          get :ticket, params: { token: @suspended_ticket.token }
        end

        should_render_template 'sso_v2_index'
      end
    end

    describe "a known non-authenticated user with a submitted suspended  ticket" do
      before do
        @suspended_ticket = suspended_tickets(:minimum_unverified)
        @suspended_ticket.token = "my_secret_token"
        @suspended_ticket.save!
      end

      it "recovers ticket via  emailed link with token" do
        assert_difference('Ticket.count(:all)', 1) do
          get :ticket, params: { token: @suspended_ticket.token }
        end
        refute @suspended_ticket.deleted
        assert_redirected_to "/access"
      end

      it "does not be able to  recover it via email twice" do
        assert_difference('Ticket.count(:all)', 1) do
          get :ticket, params: { token: @suspended_ticket.token }
        end
        refute_difference('Ticket.count(:all)') do
          get :ticket, params: { token: @suspended_ticket.token }
        end
        refute @suspended_ticket.deleted
        assert_redirected_to '/access/help'
      end
    end

    describe "a known authenticated user with a submitted suspended  ticket" do
      before do
        @suspended_ticket = suspended_tickets(:minimum_unverified_by_known_author)
        @suspended_ticket.token = "my_secret_token"
        @suspended_ticket.save!
        login(users(:minimum_author_suspend))
      end

      it "displays an error when recovering a non recoverable ticket" do
        SuspendedTicket.any_instance.stubs(recoverable?: false)
        get :ticket, params: { token: @suspended_ticket.token }
        assert_redirected_to "/hc"
        assert_equal flash[:error], "Failed to recover suspended ticket"
      end
    end
  end

  describe "#email" do
    let(:token) { tokens(:trial_token) }
    let(:identity) { token.source }
    let(:user) { identity.user }
    let(:account) { user.account }

    before do
      @request.account = account
    end

    describe "when identity is already verified and user has a password" do
      it "flashes error and redirects to login" do
        get :email, params: { token: token.value }

        assert_not_nil @request.flash[:error]
        assert_nil @request.flash[:notice]
        assert_equal @controller.send(:current_user), account.anonymous_user
        assert_redirected_to "/login"
      end
    end

    describe "when identity is not yet verified" do
      before do
        identity.update_attribute(:is_verified, false)
      end

      describe "when user has no password" do
        before do
          user.update_attribute(:crypted_password, nil)
        end

        it "does not set the current user, does not verify the email identity, and renders the SSO template" do
          get :email, params: { token: token.value }

          refute identity.reload.is_verified?
          assert_nil @request.flash[:error]
          assert_nil @request.flash[:notice]
          assert_equal @controller.send(:current_user), account.anonymous_user
          assert_template :sso_v2_index
        end
      end

      describe "when user has a password" do
        describe "when token user is owner, has never logged in, token identity is primary, account is < 7 days old" do
          it "flashes notice, sets the current user, verifies the email identity, and redirects to root" do
            user.update_attribute(:last_login, nil)
            assert user.is_account_owner?
            assert identity.primary?
            assert (Time.now - user.account.created_at) < 7.days

            get :email, params: { token: token.value }

            assert identity.reload.is_verified?
            assert_nil @request.flash[:error]
            assert_not_nil @request.flash[:notice]
            assert_equal @controller.send(:current_user), user
            assert_redirected_to '/agent'
          end
        end

        describe "when token user is owner, has never logged in, token identity is primary, account is > 7 days old" do
          it "verifies the email identity, flashes notice, and redirects to root" do
            user.account.update_attribute(:created_at, 8.days.ago)
            user.update_attribute(:last_login, nil)
            assert user.is_account_owner?
            assert identity.primary?
            assert (Time.now - user.account.created_at) >= 7.days

            get :email, params: { token: token.value }

            assert identity.reload.is_verified?
            assert_nil @request.flash[:error]
            assert_not_nil @request.flash[:notice]
            assert_equal @controller.send(:current_user), account.anonymous_user
            assert_redirected_to "/"
          end
        end

        describe "when token user is owner, has logged in, token identity is primary, account is < 7 days old" do
          it "verifies the email identity, flashes notice, and redirects to root" do
            refute user.last_login.nil?
            User.any_instance.stubs(is_account_owner?: false)
            assert identity.primary?
            assert (Time.now - user.account.created_at) < 7.days

            get :email, params: { token: token.value }

            assert identity.reload.is_verified?
            assert_nil @request.flash[:error]
            assert_not_nil @request.flash[:notice]
            assert_equal @controller.send(:current_user), account.anonymous_user
            assert_redirected_to "/"
          end
        end

        describe "when token user is owner, has never logged in, token identity is secondary, account is < 7 days old" do
          it "verifies the email identity, flashes notice, and redirects to root" do
            user.update_attribute(:last_login, nil)

            secondary_identity = UserEmailIdentity.new(user: user, value: 'aa@example.com', is_verified: false)
            secondary_identity.save!
            secondary_token = secondary_identity.verification_token

            assert user.is_account_owner?
            refute secondary_identity.primary?
            assert (Time.now - user.account.created_at) < 7.days

            get :email, params: { token: secondary_token }

            assert secondary_identity.reload.is_verified?
            assert_nil @request.flash[:error]
            assert_not_nil @request.flash[:notice]
            assert_equal @controller.send(:current_user), account.anonymous_user
            assert_redirected_to "/"
          end
        end

        describe "when token user is not owner, has never logged in, identity is primary, account is < 7 days old" do
          it "verifies the email identity, flashes notice, and redirects to root" do
            user.update_attribute(:last_login, nil)
            User.any_instance.stubs(is_account_owner?: false)
            assert identity.primary?
            assert (Time.now - user.account.created_at) < 7.days

            get :email, params: { token: token.value }

            assert identity.reload.is_verified?
            assert_nil @request.flash[:error]
            assert_not_nil @request.flash[:notice]
            assert_equal @controller.send(:current_user), account.anonymous_user
            assert_redirected_to "/"
          end
        end

        describe "when a user is logged in" do
          describe "who is the same as the token user" do
            it "flashes notice, verifies the email identity, and redirects to root" do
              @controller.send(:current_user=, user)

              get :email, params: { token: token.value }

              assert identity.reload.is_verified?
              assert_nil @request.flash[:error]
              assert_not_nil @request.flash[:notice]
              assert_equal @controller.send(:current_user), user
              assert_redirected_to '/agent'
            end
          end

          describe "who is different than the token user" do
            let(:other_user) { FactoryBot.create(:user, account: account) }

            it "does not verify the email identity and returns 400" do
              @controller.send(:current_user=, other_user)

              get :email, params: { token: token.value }

              refute identity.reload.is_verified?
              assert_nil @request.flash[:error]
              assert_nil @request.flash[:notice]
              assert_equal @controller.send(:current_user), other_user
              assert_response :bad_request
            end
          end
        end

        describe "when no user is logged in" do
          describe "when user is not verified" do
            it "verifies the email identity, flashes notice, and redirects to root" do
              refute user.reload.is_verified?

              get :email, params: { token: token.value }

              assert identity.reload.is_verified?
              assert_nil @request.flash[:error]
              assert_not_nil @request.flash[:notice]
              assert_equal @controller.send(:current_user), account.anonymous_user
              assert_redirected_to "/"
            end
          end

          describe "when user is verified" do
            it "does not verify the email identity, flashes notice, and redirects to login with return to the verification URL" do
              UserEmailIdentity.new(user: user, value: 'aa@example.com', is_verified: true).save!
              assert user.reload.is_verified?

              get :email, params: { token: token.value }

              refute identity.reload.is_verified?
              assert_nil @request.flash[:error]
              assert_not_nil @request.flash[:notice]
              assert_equal @controller.send(:current_user), account.anonymous_user
              assert_redirected_to "/login?return_to=#{CGI.escape(@request.path)}"
            end
          end
        end
      end
    end
  end

  describe "#details" do
    let(:token) { tokens(:trial_token) }
    let(:identity) { token.source }
    let(:user) { identity.user }
    let(:account) { user.account }

    before do
      @request.account = account
    end

    describe "evil user makes post call with evil password with secondary unverified identity" do
      it "returns a 403" do
        user.update_attribute(:last_login, nil)
        secondary_identity = UserEmailIdentity.new(user: user, value: 'aa@example.com', is_verified: false)
        secondary_identity.save!
        secondary_token = secondary_identity.verification_token
        refute secondary_identity.primary?

        post :details, params: { token: secondary_token, user: { name: 'Evil', password: 'evil_password' } }
        assert_response :forbidden
        assert_equal @response.code, "403"
      end
    end
  end
end
