require_relative "../support/test_helper"

SingleCov.covered!

describe EntriesController do
  fixtures :entries, :forums

  before do
    @minimum_account = accounts(:minimum)
    @request.account = @minimum_account
  end

  describe "with help center enabled" do
    let(:entry) { entries(:sticky) }
    let(:entry2) { entries(:ponies) }
    let(:forum) { entry.forum }
    let(:old_visibility) { forum.visibility_restriction_id }

    before do
      @request.account.stubs(:has_public_forums?).returns(true)
      activate_hc!
      ContentMapping.create! do |cm|
        cm.account_id = entry.account_id
        cm.classic_object_type = "Legacy::entry"
        cm.classic_object_id = entry.id
        cm.hc_object_type = "Foo"
        cm.hc_object_id = 42
        cm.url = "/foo"
      end

      entry.forum.update_attributes!(visibility_restriction_id: VisibilityRestriction::AGENTS_ONLY.id)
      entry.forum.save!
    end

    it "redirects to HelpCenter" do
      get :show, params: { id: entry.id }

      entry.forum.update_attributes!(visibility_restriction_id: old_visibility)
      assert_redirected_to "https://minimum.zendesk-test.com/foo"
    end

    describe 'when the entry id belongs to another account' do
      before do
        ContentMapping.create! do |cm|
          cm.account_id = accounts(:with_ticket_form).id
          cm.classic_object_type = "Legacy::entry"
          cm.classic_object_id = entry2.id
          cm.hc_object_type = "Foo"
          cm.hc_object_id = 42
          cm.url = "/foo"
        end

        entry2.forum.update_attributes!(visibility_restriction_id: VisibilityRestriction::AGENTS_ONLY.id)
        entry2.forum.save!
      end

      it 'returns 404' do
        get :show, params: { id: entry2.id }
        assert_response 404
      end
    end
  end

  describe "with help center disabled" do
    it "redirects to help center closed url" do
      entry = entries(:sticky)
      get :show, params: { id: entry.id }
      assert_match %r{app/help-center-closed/}, @response.location
      assert_equal 301, @response.status.to_i
    end
  end
end
