require_relative "../support/test_helper"

SingleCov.covered!

describe PasswordResetRequestsController do
  fixtures :accounts, :remote_authentications, :users, :certificates, :brands, :routes, :role_settings, :account_settings

  should_route :get,  "/access/help", controller: "password_reset_requests", action: :index
  should_route :post, "/access/help", controller: "password_reset_requests", action: :create

  before do
    @account = accounts(:minimum)
    @request.account = @account
  end

  describe "#index" do
    before do
      @account.update_attribute(:host_mapping, "mapped.com")
      @account.update_attribute(:is_ssl_enabled, true)
      @account.certificates.first.update_attribute(:state, "active")
      @account.certificates.first.update_attribute(:valid_until, 10.days.from_now.to_date)

      assert @account.certificates.active.any?
      assert @account.is_ssl_enabled?

      @controller.stubs(:current_brand).returns(@account.default_brand)
      @controller.stubs(:current_route).returns(@account.default_brand.route)
    end

    it 'does not redirect and renders the iframe if we are in the hostmapped url' do
      request.stubs(host: @account.host_mapping)
      get :index

      assert_template 'sso_v2_index'
    end

    it 'redirects to the host mapped domain if we are not in the hostmapped url' do
      request.stubs(host: "minimum.zendesk-test.com")
      get :index

      assert_redirected_to "https://mapped.com/access/help"
    end
  end

  describe "and the user is deleted but still has an identity somehow" do
    before do
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries.clear
    end

    it "betrays nothing and doesn't email the user" do
      @user = users(:minimum_end_user2)
      @user.current_user = @account.owner
      @user.delete!

      # deleting a user removes all identities and prevents you from add one
      # yet sometimes a deleted user has an identity anyway
      # and we should handle that

      @user.email = 'minimum_end_user2@example.com'
      @user.identities[0].is_verified = true
      @user.identities[0].save(validate: false)

      post :create, params: { email: @user.email, brand_id: 123, auth_origin: '123,false,true' }

      assert_empty ActionMailer::Base.deliveries
      assert_redirected_to '/auth/v2/login/password_reset_end_page?auth_origin=123%2Cfalse%2Ctrue&brand_id=123'
    end
  end

  describe "#create" do
    before do
      @user = users(:minimum_end_user)
      @identity = stub("identity", user: @user)
      @user_email_identities = stub("user_email_identities", find_by_value: @identity)
      @account.stubs(:user_identities).returns(stub(email: @user_email_identities))
    end

    describe "with valid return_to_on_failure parameter" do
      before do
        @brand = FactoryBot.create(:brand, account_id: @user.account_id, subdomain: 'wombat', active: true)
        post :create, params: { email: "", return_to_on_failure: @account.url + '/failure' }
      end

      it "redirects there with a flash" do
        location = @response.headers['Location']

        assert location.start_with?(@account.url + '/failure'),
          "#{location} is not the return_to_on_failure"

        location.must_include "flash_digest"
        "#{location} has no flash_digest"
      end
    end

    describe "with sso_login" do
      before do
        @controller.stubs(:current_brand).returns(@account.default_brand)
        @controller.stubs(:current_route).returns(@account.default_brand.route)
        post :create, params: { email: "", return_to_on_failure: @account.default_brand.url + '/failure' }
      end

      it "redirects to the return_to_on_failure" do
        location = @response.headers['Location']
        assert location.start_with?(@account.url + '/failure')
      end
    end

    describe "with the :email parameter scoped under :user" do
      before do
        @identity.stubs(:is_verified?).returns(true)
      end

      it "assigns the user to @user" do
        post :create, params: { user: { email: @user.email } }
        assert_equal @user, assigns(:user)
      end
    end

    describe "with a :brand_id parameter" do
      before do
        @identity.stubs(:is_verified?).returns(true)
        @brand = FactoryBot.create(:brand, account_id: @user.account_id, subdomain: 'wombat', active: true)
        token = stub("token", value: "rar_token")
        password_reset_tokens = stub("password_reset_tokens")
        password_reset_tokens.stubs(:create).returns(token)
        @user.stubs(:password_reset_tokens).returns(password_reset_tokens)
      end

      it "sends the brand to the users mailer" do
        UsersMailer.expects(:deliver_password_reset).with(@user, 'rar_token', as_id(@brand.id))
        post :create, params: { email: @user.email, brand_id: @brand.id, auth_origin: "#{@brand.id},false,true" }
      end
    end

    describe "without an :email parameter" do
      before do
        @controller.stubs(:current_brand).returns(@account.default_brand)
        @controller.stubs(:current_route).returns(@account.default_brand.route)
        post :create, params: { email: "", return_to_on_failure: @account.default_brand.url + '/failure' }
      end

      it "asks to provide a valid email address" do
        assert_equal I18n.t("txt.access.help.blank_email"), flash[:error]
      end

      it "redirects to the return_to_on_failure" do
        location = @response.headers['Location']
        assert location.start_with?(@account.url + '/failure')
      end
    end

    [
      {account_open: true,  user_can_reset_password: true,  identity: :verified,   expecting: :reset },
      {account_open: true,  user_can_reset_password: true,  identity: :unverified, expecting: :verification },
      {account_open: true,  user_can_reset_password: true,  identity: :unknown,    expecting: :nothing },

      {account_open: true,  user_can_reset_password: false, identity: :verified,   expecting: :nothing },
      {account_open: true,  user_can_reset_password: false, identity: :unverified, expecting: :nothing },
      {account_open: true,  user_can_reset_password: false, identity: :unknown,    expecting: :nothing },

      {account_open: false, user_can_reset_password: true,  identity: :verified,   expecting: :reset },
      {account_open: false, user_can_reset_password: true,  identity: :unverified, expecting: :verification },
      {account_open: false, user_can_reset_password: true,  identity: :unknown,    expecting: :nothing },

      {account_open: false, user_can_reset_password: false, identity: :verified,   expecting: :nothing },
      {account_open: false, user_can_reset_password: false, identity: :unverified, expecting: :nothing },
      {account_open: false, user_can_reset_password: false, identity: :unknown,    expecting: :nothing },
    ].each do |config|
      describe "with #{config.inspect}" do
        before do
          case config[:identity]
          when :verified
            @controller.expects(:user_can_reset_password?).at_least_once.returns(config[:user_can_reset_password])
            @controller.expects(:identity_present?).at_least_once.returns(true)
            Timecop.freeze
            @identity.stubs(:is_verified?).returns(true)
            token = stub("token", value: "foo_token")
            password_reset_tokens = stub("password_reset_tokens")
            password_reset_tokens.stubs(:create).returns(token)
            @user.stubs(:password_reset_tokens).returns(password_reset_tokens)
            @email = @user.email

          when :unverified
            @controller.expects(:user_can_reset_password?).at_least_once.returns(config[:user_can_reset_password])
            @identity.stubs(:is_verified?).returns(false)
            @controller.expects(:identity_present?).at_least_once.returns(true)
            @email = @user.email

          when :unknown
            @controller.expects(:user_can_reset_password?).never
            @controller.expects(:identity_present?).at_least_once.returns(false)
            @email = "not_found@example.com"

          else
            raise "Unexpected identity configuration"
          end

          case config[:expecting]
          when :reset
            UsersMailer.expects(:deliver_password_reset).with(@user, "foo_token", as_id(123))
            @identity.expects(:send_verification_email).never

          when :verification
            UsersMailer.expects(:deliver_password_reset).never
            @identity.expects(:send_verification_email)

          when :nothing
            UsersMailer.expects(:deliver_password_reset).never
            @identity.expects(:send_verification_email).never

          else
            raise "Unexpected expectation configuration"
          end

          post :create, params: { email: @email, brand_id: 123, auth_origin: '123,false,true' }
        end

        it "betrays nothing" do
          config[:expecting]
          assert_redirected_to '/auth/v2/login/password_reset_end_page?auth_origin=123%2Cfalse%2Ctrue&brand_id=123'
        end
      end
    end

    def no_email_sent_message(user, reason)
      "Preventing password reset email for " \
      "account_id: #{user.account_id}, " \
      "user_id: #{user.id}, " \
      "email: #{user.email.inspect}, " \
      "reason: #{reason}."
    end

    describe 'logging when we do not send a reset password email' do
      before do
        # Blanket expect here so we can be specific with logging messages for
        # each `it` block.
        Rails.logger.expects(:info).at_least_once
      end

      describe 'when the user is deleted' do
        before do
          # Deleting a user is kind of hard as there a lot of things that can
          # prevent it from ocurring. Stubbing here to make this more future
          # proof.
          @user.stubs(:deleted?).returns(true)
        end

        it 'logs that no email was sent' do
          Rails.logger.expects(:info).with(
            no_email_sent_message(@user, 'user is deleted')
          )

          post :create, params: { email: @email, brand_id: 123, auth_origin: '123,false,true' }
        end
      end

      describe 'when the user is not capable of reseting their password' do
        before do
          @controller.expects(:user_can_reset_password?).at_least_once.returns(false)
        end

        it 'logs that no email was sent' do
          Rails.logger.expects(:info).with(
            no_email_sent_message(@user, 'user is not capable of resetting password')
          )

          post :create, params: { email: @email, brand_id: 123, auth_origin: '123,false,true' }
        end
      end
    end

    describe "#identity_present?" do
      before do
        @identity = user_identities(:minimum_end_user)
      end

      it "returns true when the identity is present?" do
        @controller.expects(:identity).with.at_least_once.returns(@identity)
        assert @controller.send(:identity_present?)
      end

      it "returns false when identity not present?" do
        @controller.expects(:identity).with.at_least_once.returns(nil)
        refute @controller.send(:identity_present?)
      end

      it "returns false when user is suspended" do
        @user = users(:minimum_end_user)
        @controller.expects(:identity).with.at_least_once.returns(@identity)
        @identity.stubs(:user).returns(@user)
        @user.expects(:suspended?).returns(true)

        refute @controller.send(:identity_present?)
      end
    end

    describe "#validate_rate_limit" do
      before do
        Timecop.freeze
        @identity.stubs(:is_verified?).returns(true)
      end

      describe "after 10 requests for the same user" do
        before do
          10.times { Prop.throttle!(:password_reset_requests, [@account.id, @user.email]) }
          post :create, params: { email: @user.email }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "after 10 requests with the same ip address" do
        before do
          10.times { Prop.throttle!(:password_reset_requests, [@account.id, '0.0.0.0']) }
          post :create, params: { email: @user.email }
        end

        it('responds with forbidden') { assert_response :forbidden }
      end
    end

    describe "#user_can_reset_password?" do
      let(:agent) { users(:minimum_agent) }
      describe "as an end user" do
        it "checks the account setting" do
          @account.expects(:is_end_user_password_change_visible?).once.returns(true)
          assert(@controller.send(:user_can_reset_password?))
        end

        describe "when end user passwords are allowed" do
          it "allows the password reset request" do
            @account.role_settings.update_attributes!(end_user_zendesk_login: true, end_user_password_allowed: true)
            assert(@controller.send(:user_can_reset_password?))
          end
        end

        describe "when end user passwords are not allowed" do
          it "prevents the password reset request" do
            @account.role_settings.update_attributes!(end_user_zendesk_login: false, end_user_password_allowed: false)
            assert_equal false, @controller.send(:user_can_reset_password?)
          end
        end
      end

      describe "as a staff user" do
        before { @controller.stubs(user: agent) }

        describe "when staff passwords are allowed" do
          it "allows the password reset request" do
            @account.role_settings.update_attributes!(agent_zendesk_login: true, agent_password_allowed: true)
            assert(@controller.send(:user_can_reset_password?))
          end
        end

        describe "when staff passwords are not allowed" do
          it "prevents the password reset request" do
            @account.role_settings.update_attributes!(agent_zendesk_login: false, agent_password_allowed: false)
            assert_equal false, @controller.send(:user_can_reset_password?)
          end
        end

        describe "with use_user_policy_for_password_resets" do
          before do
            Arturo.enable_feature! :use_user_policy_for_password_resets
          end

          it "checks user policy" do
            agent.expects(:can?).with(:request_password_reset, agent).once.returns(true)
            assert(@controller.send(:user_can_reset_password?))
          end

          describe "when staff passwords are allowed" do
            it "allows the password reset request" do
              agent.expects(:can?).with(:request_password_reset, agent).once.returns(true)
              @account.role_settings.update_attributes!(agent_zendesk_login: true, agent_password_allowed: true)
              assert(@controller.send(:user_can_reset_password?))
            end
          end

          describe "when staff passwords are not allowed" do
            it "prevents the password reset request" do
              agent.expects(:can?).with(:request_password_reset, agent).never
              @account.role_settings.update_attributes!(agent_zendesk_login: false, agent_password_allowed: false)
              assert_equal false, @controller.send(:user_can_reset_password?)
            end
          end
        end
      end

      describe "with arturo feature user_can_reset_password disabled" do
        it "does not check user policy" do
          @user.expects(:can?).with(:request_password_reset, @user).never
          assert @controller.send(:user_can_reset_password?)
        end

        it "checks account settings" do
          @account.expects(:is_end_user_password_change_visible?).once.returns(true)
          @controller.send(:user_can_reset_password?)
        end

        it "allows agents to reset their password" do
          @user = users(:minimum_agent)
          @controller.stubs(:user).returns(@user)
          assert @controller.send(:user_can_reset_password?)
        end

        describe "when end users can reset passwords" do
          it "allows end users to reset passwords" do
            @account.expects(:is_end_user_password_change_visible?).returns(true)
            assert @controller.send(:user_can_reset_password?)
          end
        end

        describe "when end users can't reset passwords" do
          before do
            @account.stubs(:is_end_user_password_change_visible?).returns(false)
          end

          it "prevents end users from resetting their passwords" do
            refute @controller.send(:user_can_reset_password?)
          end

          it "allows agents to reset their password" do
            @user = users(:minimum_agent)
            @controller.expects(:user).returns(@user)
            assert @controller.send(:user_can_reset_password?)
          end
        end
      end
    end
  end
end
