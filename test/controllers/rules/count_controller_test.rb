require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 7

describe Rules::CountController do
  fixtures :accounts, :users

  with_options(controller: "rules/count") do |request|
    request.should_route :get, "/rules/count", action: "index"
  end
end
