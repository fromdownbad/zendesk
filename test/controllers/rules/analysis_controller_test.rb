require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Rules::AnalysisController do
  fixtures :accounts, :users, :role_settings

  should_include_module Zendesk::LotusEmbeddable

  before do
    @current_account = accounts(:minimum)
    @request.account = @current_account
  end

  describe "#index" do
    before do
      login("minimum_admin");
      @params = {}
      Subscription.any_instance.expects(:has_rule_analysis?).returns(true)
    end

    it "displays the index template" do
      get :index
      assert_response :ok
      assert_template 'index'
    end
  end
  describe "#show" do
    before { login("minimum_admin") }

    it "always set rules filter to active if user has not selected from active/inactive field" do
      get :show, params: { id: 'group_id', select: 'views' }
      @controller.show
      assert_equal 'active', @controller.params[:filter]
    end

    it "routes POST requests to rule analysis page" do
      assert_routing({method: 'post', path: '/rules/analysis/group_id'}, controller: 'rules/analysis', action: 'show', id: 'group_id')
    end

    it "routes GET requests (after submission of a rule) back to rule analysis page" do
      assert_routing({method: 'get', path: '/rules/analysis/group_id'}, controller: 'rules/analysis', action: 'show', id: 'group_id')
    end

    describe "when the subscription has the rule_analysis feature enabled" do
      before { Subscription.any_instance.expects(:has_rule_analysis?).returns(true) }

      it "displays the show template for item" do
        get :show, params: { id: 'group_id' }
        assert_response :ok
      end

      it "displays the show template for item and key" do
        get :show, params: { id: 'group_id', key: 'current_groups' }
        assert_response :ok
      end

      describe_with_arturo_disabled :use_get_for_rules_analysis_show do
        it "displays the show template for item and tag key" do
          get :show, params: { id: 'tag', key: 'hilarious' }
          assert_includes response.body, '[&quot;hilarious&quot;]'
          assert_response :ok
        end
      end

      describe_with_arturo_enabled :use_get_for_rules_analysis_show do
        it "displays the show template for item and valid tag key" do
          get :show, params: { id: 'tag', key: 'recipient_detected' }
          assert_includes response.body, '[&quot;recipient_detected&quot;]'
          assert_response :ok
        end

        it "displays the show template for item but not invalid tag key" do
          get :show, params: { id: 'tag', key: 'hilarious' }
          refute_includes response.body, '[&quot;hilarious&quot;]'
          assert_response :ok
        end
      end
    end

    describe "when the subscription does not have the rule_analysis feature enabled" do
      before { Subscription.any_instance.expects(:has_rule_analysis?).returns(false) }
      it "denys access" do
        get :show, params: { id: 'group_id' }
        assert_response :forbidden
      end
    end
  end
end
