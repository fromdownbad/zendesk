require_relative "../../support/test_helper"
require_relative "../../support/rules_test_helper"

# THIS IS NOT A TEST
# only included in other tests like the views_controller_test
module RulesControllerTests
  include RulesHelper
  include RulesTestHelper

  def rule_type
    self.class.rule_type
  end

  module ClassMethods
    def rule_type
      @rule_type ||= controller_class.name.demodulize.tableize.split('_').first.singularize
    end

    # Specify if there is any arturo that controls the deprecation of the consumer class
    # This can be overriden by consumer class to return an Arturo feature flag name
    # When overriden this will make sure all the specs in this module run with
    # arturo disabled.
    def deprecate_with_feature
      nil
    end
  end

  def self.included(test)
    test.fixtures :all

    test.before do
      @account = @request.account = accounts(:minimum)
      @model   = rule_type.classify.constantize

      stub_request(:get, %r{/api/v2/apps/requirements.json})
    end

    test.extend(ClassMethods)

    Arturo.disable_feature! test.deprecate_with_feature if test.deprecate_with_feature.present?

    test.describe(test.controller_class) do
      should_be_unauthorized_when_not_logged_in([:index, :new, :create, :edit, :update, :destroy, [:get, :activate, {id: 1}], :destroy_inactive])
      should_be_unauthorized_when_not_logged_in([:index, :new, :create, :edit, :update, :destroy, [:get, :activate, {id: 1}], :destroy_inactive], 'minimum_end_user', 403)

      describe "On GET to :index" do
        before { login(:minimum_admin) }

        describe "when all are inactive" do
          before do
            @model.any_instance.stubs(:is_active?).returns(false)
            get :index, params: { filter: rule_type }
          end
          it('responds with ok') { assert_response :ok }

          before_should "register activity on the current device" do
            @controller.expects(:register_device_activity)
          end
        end

        it "includes both active and inactive" do
          get :index, params: { filter: rule_type }
          assert_select '.item-info', @account.send(rule_type.pluralize).length
        end
      end

      describe "On GET to :new" do
        before { login("minimum_admin") }

        describe "with filter=#{rule_type}" do
          before { get :new, params: { filter: rule_type } }
          it('responds with ok') { assert_response :ok }
          should_render_template :edit

          it "defaults the #{rule_type}'s owner to the current account" do
            assert_equal @controller.send(:current_account), assigns(:rule).owner
          end

          it "includes a hidden input field for the rule's type" do
            assert_select "input#rule_type[name='rule[type]'][value='#{rule_type.capitalize}']"
          end
        end

        describe "cloning" do
          describe "a rule that exists" do
            before do
              login(:minimum_admin)
              @rule = @account.send(rule_type.pluralize).first
              @rule.expects(:prepare_for_editing!).returns(@rule.prepare_for_editing!)
              get :new, params: { copy_id: @rule }
            end
            it('responds with ok') { assert_response :ok }
            should_render_template :edit

            it "sets the new rule's owner to that of the rule being copied" do
              assert_equal @rule.owner, assigns(:rule).owner
            end
          end

          describe "a rule that the logged in user is not allowed to edit" do
            before do
              User.any_instance.stubs(:can?).returns(true)
              User.any_instance.expects(:can?).with(:edit, kind_of(Rule)).returns(false)
              get :new, params: { copy_id: Rule.first.id.to_s }
            end
            it('responds with forbidden') { assert_response :forbidden }
          end

          describe "a rule that that does not exist" do
            before do
              @controller.stubs(:current_brand).returns(@request.account.brands.first)
              get :new, params: { copy_id: Time.now.to_i.to_s }
            end
            it('responds with not_found') { assert_response :not_found }
          end
        end
      end

      describe "On DELETE to :destroy" do
        before do
          @rule = @account.send(rule_type.pluralize).first
        end

        describe "as an admin" do
          before do
            login(:minimum_admin)
          end

          it "softs delete the rule" do
            delete :destroy, params: { id: @rule.id }

            assert_response 302
            assert @rule.reload.deleted?
          end
        end
      end

      describe "A POST to :sort" do
        before do
          login(:minimum_admin)
          @sorted_rules = @account.send(rule_type.pluralize).sort_by(&:id).reverse
          post :sort, params: { 'active-rules-sort-list': @sorted_rules.map(&:id) }
        end

        it('responds with ok') { assert_response :ok }

        it "sorts the rules as specified" do
          assert_equal @sorted_rules.map(&:id), @account.send(rule_type.pluralize).order(position: :asc).pluck(:id)
        end

        before_should "expire cache key" do
          Account.any_instance.stubs(:expire_scoped_cache_key)
          Account.any_instance.expects(:expire_scoped_cache_key).at_least_once.with(:rules)
        end
      end

      describe "A POST to :create" do
        before do
          login(:minimum_admin)
        end

        it "does not set a flash message" do
          params = get_valid_rule_parameters(rule_type)

          post :create, params: params.update(type: rule_type)

          assert @controller.flash[:notice].blank?
        end

        it "sets a notice cookie" do
          params = get_valid_rule_parameters(rule_type)

          post :create, params: params.update(type: rule_type)

          assert_equal(
            {
              action: 'create',
              id:     Rule.last.id,
              result: 'success',
              type:   rule_type
            }.to_json,
            cookies[:zd_admin_notice]
          )
        end

        it "creates a rule" do
          params = get_valid_rule_parameters(rule_type)

          assert_difference('@account.all_rules.count(:all)') do
            post :create, params: params.update(type: rule_type)
          end
          assert_response :redirect
        end

        it "does not prepare for editing" do
          Rule.any_instance.expects(:prepare_for_editing!).never
          params = get_valid_rule_parameters(rule_type)
          post :create, params: params.update(type: rule_type)
        end

        it "validates parameters" do
          params = get_valid_rule_parameters(rule_type)
          params[:sets] = nil

          post :create, params: params
          assert_response 406
        end

        it "does not redirect to a non-relative url" do
          params = get_valid_rule_parameters(rule_type)
          params[:return_to] = "https://maliciouswebsite.com"

          RulesController.any_instance.expects(:redirect_to).with("#{root_url}https://maliciouswebsite.com")
          post :create, params: params
        end

        # https://support.zendesk.com/tickets/87620 describes a problem
        # wherein the value of a full-text match is getting its spaces stripped.
        # This test shows the breakage, but fixing it would break many more things.
        # One possible solution would be to add some logic to Definition and
        # DefinitionItem that exempts "coment_includes_word/is" and
        # "comment_includes_word/is_not" matches from the space-stripping.
        should_eventually "retain text match spaces" do
          params = get_valid_rule_parameters(rule_type)
          params[:sets]['1'][:conditions] = {
            "0" => {
              "operator" => "is",
              "value" => { "0" => ' with spaces ' },
              "source" => "comment_includes_word"
            }
          }
          post :create, params: params
          assert_response 302
          rule = Rule.find(Rule.maximum('id'))
          assert_equal ' with spaces ', rule.definition.conditions_all.first.value
        end
      end

      describe "A PUT to :update" do
        before do
          login(:minimum_admin)
          @rule = @account.send(rule_type.pluralize).active.first
          @rule.class.any_instance.stubs(:valid?).returns true
        end

        it "updates a rule" do
          assert(@rule.is_active?)
          put :update, params: { id: @rule.id, rule: { is_active: false } }

          assert_response :redirect
          assert_equal false, @rule.reload.is_active?
        end

        it "returns a link to the view in the flash message" do
          put :update, params: { id: @rule.id, rule: { is_active: false } }

          assert @controller.flash[:notice].blank?
        end

        it "sets a notice cookie" do
          put :update, params: { id: @rule.id, rule: {is_active: false} }

          assert_equal(
            {
              action: 'update',
              id:     @rule.id,
              result: 'success',
              type:   rule_type
            }.to_json,
            cookies[:zd_admin_notice]
          )
        end

        it "activate/deactivates a rule while updating" do
          put :update, params: { id: @rule.id, submit_type: "deactivate", rule: { title: "bah" } }

          @rule.reload
          assert_equal false, @rule.is_active?
          assert_equal "bah", @rule.title

          put :update, params: { id: @rule.id, submit_type: "activate", rule: { title: "foo" } }

          @rule.reload
          assert(@rule.is_active?)
          assert_equal "foo", @rule.title
        end

        it "deletes a rule if submit_type is 'delete'" do
          assert_difference "CIA::Event.count(:all)", +1 do
            put :update, params: { id: @rule.id, submit_type: "delete", rule: {title: "misdirection"} }
          end

          assert_raises(ActiveRecord::RecordNotFound) { Rule.find(@rule.id) }
          assert_equal({
            "source_id" => @rule.id,
            "source_type" => "Rule",
            "action" => "destroy"
          }, CIA::Event.last.attributes.slice("source_type", "source_id", "action"))
        end
      end

      describe "A PUT to :activate" do
        let(:rule) { @account.send(rule_type.pluralize).first }

        describe "as an authorized user" do
          before do
            login(:minimum_admin)
          end

          describe "when valid" do
            before do
              rule.class.any_instance.stubs(:valid?).returns true
            end

            it "activates an inactive rule" do
              rule.update_attribute(:is_active, false)
              put :activate, params: { id: rule.id }

              assert_response 302
              assert flash[:notice].blank?
              assert rule.reload.is_active
            end

            it "deactivates an active rule" do
              rule.update_attribute(:is_active, true)
              put :activate, params: { id: rule.id }

              assert_response 302
              assert flash[:notice].blank?
              refute rule.reload.is_active
            end
          end

          describe "when invalid" do
            before do
              rule.class.any_instance.stubs(:valid?).returns false
            end

            it "does not activate an inactive rule" do
              rule.update_attribute(:is_active, false)
              put :activate, params: { id: rule.id }

              assert_response 302
              assert flash[:error]
              refute rule.reload.is_active
            end

            it "deactivates an active rule" do
              rule.update_attribute(:is_active, true)
              put :activate, params: { id: rule.id }

              assert_response 302
              assert flash[:notice].blank?
              refute rule.reload.is_active
            end
          end
        end
      end

      describe "On GET to :edit" do
        let(:account) { @account }
        let(:rule)    { account.send(rule_type.pluralize.to_sym).first }

        before do
          account.
            organizations.
            new(name: "Buddhys <3 & stuff\u2028ed \newts").
            save!
        end

        describe "when the user can edit the rule" do
          let(:installations) { {} }

          before do
            login(:minimum_admin)

            Rule.stubs(apps_installations: installations)
          end

          describe "when the rule is not an app requirement" do
            before { get :edit, params: { id: rule.id } }

            should_render_template :edit

            it "responds with ok" do
              assert_response :ok
            end

            it "includes a hidden input field for the rule's type" do
              assert_select(
                "input#rule_type[name='rule[type]']" \
                  "[value='#{rule_type.capitalize}']"
              )
            end

            it "does not generate busted javascript" do
              @response.body.must_include(
                "\"Buddhys &lt;3 &amp; stuff ed \\newts\""
              )
            end

            it "does not include the app installation" do
              assert_select '.app_installation', 0
            end

            it 'does not disable the fields' do
              assert_select 'input#rule_title[disabled="disabled"]', 0
            end
          end

          describe "when the rule is an app requirement" do
            let(:installations) do
              {rule.id => {'settings' => {'title' => 'Test app'}}}
            end

            before { get :edit, params: { id: rule.id } }

            it "responds with ok" do
              assert_response :ok
            end

            it "includes the app installation" do
              assert_select '.app_installation', 1
            end

            it "includes the requiring app's name" do
              assert_select(
                '.app_installation__title',
                count: 1,
                text:  'Required by Test app'
              )
            end

            it 'disables the fields' do
              assert_select 'input#rule_title[disabled="disabled"]', 1
            end
          end
        end

        describe "when the user cannot edit the rule" do
          before do
            login(:minimum_agent)

            get :edit, params: { id: rule.id }
          end

          it "responds with ok" do
            assert_response :ok
          end
        end
      end
    end
  end

  module BusinessRules
    def rule
      raise "You need to implement the #rule method when including the RulesControllerTests::BusinessRules module"
    end

    def self.included(test)
      test.describe "#{test.rule_type.capitalize} business rules" do
        describe "index sorting/selecting" do
          before do
            login(:minimum_admin)

            Account.any_instance.stubs(:has_rule_usage_stats?).returns(true)
            Subscription.any_instance.stubs(:has_group_rules?).returns(true)
            @controller.stubs(:allow_rule_manipulations?).returns(true)
            @params = { filter: rule_type }
            @total_rule_count = @account.send("all_#{rule_type.pluralize}").count(:all)
          end

          it "sorts by created_at" do
            get :index, params: @params.merge(sort: :created_at)
            assert_select "div[class='item-info title']", count: @total_rule_count
            assert_select "div[class*='rule-sorter']",    count: 0
          end

          it "sorts by updated_at" do
            get :index, params: @params.merge(sort: :updated_at)
            assert_select "div[class='item-info title']", count: @total_rule_count
            assert_select "div[class*='rule-sorter']",    count: 0
          end
        end

        describe "permissions" do
          before do
            login(:minimum_agent)
          end

          describe "if the agent has permission to edit business rule settings" do
            before do
              Access::Policies::RulePolicy.any_instance.stubs(:edit?).returns(true)
            end

            it "is allowed to list" do
              get :index, params: { filter: rule_type }
              assert_response :ok
            end

            it "is allowed to edit" do
              get :edit, params: { id: rule.id }
              assert_response :ok
            end

            it "is allowed to update" do
              put :update, params: { id: rule.id, rule: {title: "Foo"} }
              assert_response :redirect
            end

            it "is allowed to activate" do
              get :activate, params: { id: rule.id }
              assert_response :redirect
            end

            it "is allowed to destroy" do
              delete :destroy, params: { id: rule.id }
              assert_response :redirect
            end

            it "is allowed to destroy_inactive" do
              delete :destroy_inactive, params: { filter: "triggers" }
              assert_response :redirect
            end

            it "is allowed to create" do
              get :new, params: { filter: rule_type }
              assert_response :ok

              params = get_valid_rule_parameters(rule_type)

              post :create, params: params.update(type: rule_type.singularize)
              assert_response :redirect
            end

            describe "destroy_inactive" do
              it "deletes inactive rules" do
                assert @model.where(account_id: @account).count(:all)
                assert @model.inactive.where(account_id: @account).count(:all)
                delete :destroy_inactive

                assert @model.where(account_id: @account).count(:all)
                assert_equal 0, @model.inactive.where(account_id: @account).count(:all)
              end
            end
          end

          describe "if the agent doesn't have permission to edit business rule settings" do
            before do
              Access::Policies::RulePolicy.any_instance.stubs(:edit?).returns(false)
            end

            it "should be allowed to list" do
              get :index, params: { filter: rule_type }
              assert_response :ok
            end

            it "should be allowed to edit" do
              get :edit, params: { id: rule.id }
              assert_response :ok
            end

            it "does not be allowed to update" do
              put :update, params: { id: rule.id, rule: {title: "Foo"} }
              assert_response :forbidden
            end

            it "does not be allowed to destroy" do
              delete :destroy, params: { id: rule.id }
              assert_response :forbidden
            end

            it "does not be allowed to destroy_inactive" do
              delete :destroy_inactive, params: { filter: "triggers" }
              assert_response :forbidden
            end

            it "does not be allowed to create" do
              get :new, params: { filter: rule_type.singularize }
              assert_response :forbidden

              params = get_valid_rule_parameters(rule_type)

              post :create, params: params.update(type: rule_type.singularize)
              assert_response 406
            end
          end
        end
      end
    end
  end

  module PersonalTests
    def self.included(test)
      test.before do
        stub_request(:get, %r{/api/v2/apps/requirements.json\?includes=installation})
      end

      test.describe "#{test.rule_type.capitalize} personal test" do
        describe "On DELETE to :destroy #{rule_type}" do
          describe "as an admin" do
            before do
              login(:minimum_admin)
            end

            it "softs delete the rule" do
              rule = @model.where(account_id: @account.id).first
              assert rule

              delete :destroy, params: { id: rule.id }
              assert_response 302
              assert rule.reload.deleted?
            end
          end
        end

        describe "On DELETE to :destroy_inactive #{rule_type}" do
          before do
            @controller.stubs(:current_account).returns(accounts(:minimum))
            Subscription.any_instance.stubs(:has_group_rules?).returns(true)
            @params = {filter: rule_type.pluralize, return_to: '/'}
          end

          describe "as an admin" do
            before do
              login("minimum_admin")
            end

            it "deletes inactive on account level" do
              account = accounts(:minimum)
              model   = rule_type.singularize.classify.constantize
              assert model.where(account_id: account).count(:all)
              assert model.inactive.where(account_id: account).count(:all)
              delete :destroy_inactive, params: @params

              assert model.where(account_id: account).count(:all)
              assert_equal 0, model.inactive.where(account_id: account).count(:all)
            end

            it "deletes inactive on group level" do
              assert_difference("accounts(:minimum).all_#{rule_type.pluralize}.inactive.size", -1) do
                delete :destroy_inactive, params: @params.merge(select: "group_id_#{groups(:minimum_group).id}")
              end
            end
          end

          describe "as an agent" do
            before do
              login("minimum_agent")
            end

            it "does not delete any inactive on account level" do
              delete :destroy_inactive, params: @params
              assert_response :forbidden
            end

            it "deletes inactive on personal level" do
              user        = users(:minimum_agent)
              rule        = rules("#{rule_type.singularize}_for_minimum_agent")
              rule.update_attributes!(is_active: false)
              assert user.send(rule_type.pluralize).size
              user.reload

              delete :destroy_inactive, params: @params.merge(select: 'personal')
              assert_equal 0, user.send(rule_type.pluralize).size
            end
          end
        end

        describe "On PUT to :update" do
          before do
            @model_name    = rule_type.singularize.capitalize
            @existing_rule = rules("#{rule_type.singularize}_for_minimum_admin")
          end

          describe "as an account admin" do
            before do
              login("minimum_admin")
            end

            describe "making it an account rule" do
              before do
                put :update, params: { id: @existing_rule.id.to_s, rule: { owner_type: "Account", type: @model_name } }
              end
              it('responds with redirect') { assert_response :redirect }

              it "shoulds set the rule's owner to the current account" do
                assert_equal @controller.send(:current_account), assigns(:rule).owner
              end
            end

            describe "making it a group rule" do
              before do
                Subscription.any_instance.stubs(:has_group_rules?).returns(true)
                @expected_group = groups(:minimum_group)
                put :update, params: { id: @existing_rule.id.to_s,
                                       rule: { owner_type: "Group", owner_id: @expected_group.id.to_s, type: @model_name } }
              end
              it('responds with redirect') { assert_response :redirect }

              it "shoulds set the rule's owner to the given group" do
                assert_equal @expected_group, assigns(:rule).owner
              end
            end

            describe "making it a personal rule" do
              before do
                put :update, params: { id: @existing_rule.id.to_s, rule: {owner_type: "User", type: @model_name } }
              end
              it('responds with redirect') { assert_response :redirect }

              it "shoulds set the rule's owner to the current user" do
                assert_equal @controller.send(:current_user), assigns(:rule).owner
              end
            end
          end

          describe "as an account agent" do
            before do
              @existing_rule = rules("#{rule_type.singularize}_for_minimum_agent")
              login("minimum_agent")
            end

            it "does not be able to create account rules" do
              put :update, params: { id: @existing_rule.id.to_s, rule: { owner_type: "Account", type: @model_name } }
              assert_equal @controller.send(:current_user), assigns(:rule).owner
            end
          end
        end

        describe "On GET to :index with filter=#{rule_type.pluralize}" do
          before do
            @params = {filter: rule_type.pluralize}
            subscriptions(:minimum).update_attribute(:plan_type, SubscriptionPlanType.Large)
            Zendesk::Features::SubscriptionFeatureService.new(accounts(:minimum)).execute!
          end

          describe "as an admin" do
            before do
              login('minimum_admin')
              @user = users(:minimum_admin)
            end

            it "displays shared" do
              get :index
              assert_select '.item-info', @user.send("shared_#{rule_type.pluralize}").length
            end

            it "displays personal" do
              get :index, params: { select: 'personal' }
              assert_select '.item-info', @user.send("personal_#{rule_type.pluralize}").length
            end
          end

          describe "the link to Add a rule of the appropriate type" do
            before do
              @add_link_selector = "a[href*='/rules/new?filter=#{rule_type.singularize}']"
            end

            describe "for an agent" do
              before { login('minimum_agent') }

              it "is present for accounts that support personal rules" do
                Subscription.any_instance.stubs(:has_personal_rules?).returns(true)
                get :index, params: @params.merge(select: 'personal')

                assert_select @add_link_selector
              end
            end

            describe "for an admin" do
              before { login('minimum_admin') }

              it "is present even for accounts that do not support personal rules" do
                Subscription.any_instance.stubs(:has_personal_rules?).returns(false)
                get :index, params: @params

                assert_select @add_link_selector
              end
            end
          end

          describe "for an agent looking at shared rules" do
            before_should "get the current user's shared #{rule_type.pluralize}" do
              current_user_mock = users(:minimum_agent)

              @controller.stubs(:current_user).returns(current_user_mock)
            end

            before do
              login('minimum_agent')
              get :index, params: @params
            end
            it('responds with ok') { assert_response :ok }

            it "does not display a link to the sort dropdown" do
              assert_select "select[id=\"rule-select\"]", count: 0
            end

            it "does not display any inactive shared #{rule_type.pluralize}" do
              inactive_shared_rules = users(:minimum_agent).send("shared_#{rule_type.pluralize}").reject(&:is_active?)
              assert inactive_shared_rules.present?, "For this test, the user must have access to some inactive shared #{rule_type.pluralize}"

              inactive_shared_rules.each do |rule|
                assert_select "a[href$='/rules/#{rule.id}']", count: 0
              end
            end

            it "does not include control links for any active shared #{rule_type.pluralize}" do
              active_shared_rules = users(:minimum_agent).send("shared_#{rule_type.pluralize}").select(&:is_active?)
              assert active_shared_rules.present?, "For this test, the user must have access to some active shared #{rule_type.pluralize}"

              active_shared_rules.each do |rule|
                assert_select "a[href*='/rules/#{rule.id}/activate']", count: 0
                assert_select "a[href*='/rules/new?copy_id=#{rule.id}']", count: 0
                assert_select "a[href='/rules/#{rule.id}/edit']", count: 0
              end
            end
          end

          describe "for an agent looking at personal rules" do
            before_should "get the current user's personal #{rule_type.pluralize}" do
              current_user_mock = users(:minimum_agent)

              @controller.stubs(:current_user).returns(current_user_mock)
            end

            before do
              login('minimum_agent')
              get :index, params: @params.merge(select: 'personal')
            end
            it('responds with ok') { assert_response :ok }

            it "includes control links for all personal #{rule_type.pluralize}" do
              personal_rules = users(:minimum_agent).send("personal_#{rule_type.pluralize}")
              assert personal_rules.present?,
                "For this test, the user must have at least one shared view in their available #{rule_type.pluralize}"

              personal_rules.each do |rule|
                assert_select "a[href*='/rules/#{rule.id}/activate']"
              end
            end
          end

          describe "for an agent with 'full' permission looking at shared rules" do
            before_should "get the current account's shared #{rule_type.pluralize}" do
              Account.any_instance.stubs(:has_permission_sets?).returns(true)
              current_user_mock    = users(:minimum_agent)
              current_account_mock = accounts(:minimum)
              permission_set       = build_valid_permission_set
              current_user_mock.stubs(:permission_set).returns(permission_set)

              # current_account_mock.expects("shared_#{rule_type.pluralize}").at_least_once.returns([])

              @controller.stubs(:current_user).returns(current_user_mock)
              @controller.stubs(:current_account).returns(current_account_mock)
            end

            before do
              login('minimum_agent')
              get :index, params: @params
            end
            it('responds with ok') { assert_response :ok }
          end

          %w[manage-group manage-personal readonly].each do |access|
            describe "for an agent with '#{access}' permission looking at shared rules" do
              before_should "get the current user's shared #{rule_type.pluralize}" do
                Account.any_instance.stubs(:has_permission_sets?).returns(true)
                current_user_mock = users(:minimum_agent)
                permission_set = build_valid_permission_set
                current_user_mock.stubs(:permission_set).returns(permission_set)

                permission_set.permissions.view_access = access

                @controller.stubs(:current_user).returns(current_user_mock)
              end

              before do
                login('minimum_agent')
                get :index, params: @params
              end
              it('responds with ok') { assert_response :ok }
            end
          end

          describe "for an admin" do
            before_should "get the #current account's shared #{rule_type.pluralize}" do
              mock_account = accounts(:minimum)
              @controller.stubs(:current_account).returns(mock_account)
            end

            before do
              Account.any_instance.stubs(:has_rule_usage_stats?).returns(true)
              login('minimum_admin')
              get :index, params: @params
            end
            it('responds with ok') { assert_response :ok }

            it "displays a link to the sort dropdown and the reorder button" do
              assert_select "select[id=\"rule-select\"]", count: 1
              assert_select "ul[class*='rule-sorter']", count: 1
            end

            it "includes control links for all shared #{rule_type.pluralize}" do
              shared_rules = accounts(:minimum).send("shared_#{rule_type.pluralize}")
              assert shared_rules.present?, "For this test, the user's account must have some shared #{rule_type.pluralize}"

              shared_rules.each do |rule|
                assert_select "a[href*='/rules/#{rule.id}/activate']"
                assert_select "a[href*='/rules/new?copy_id=#{rule.id}']"
                assert_select "a[href*='/agent/admin/#{rule.type.downcase.pluralize}/#{rule.id}']"
              end
            end
          end

          describe "for an end user" do
            before do
              login("minimum_end_user")
              get :index, params: @params
            end
            it('responds with forbidden') { assert_response :forbidden }
          end
        end

        describe "sorting/selecting" do
          before do
            login(:minimum_admin)

            Account.any_instance.stubs(:has_rule_usage_stats?).returns(true)
            Subscription.any_instance.stubs(:has_group_rules?).returns(true)
            @controller.stubs(:allow_rule_manipulations?).returns(true)
            @params = { filter: rule_type.pluralize }
            @total_rule_count = @account.send("shared_#{rule_type.pluralize}").count(:all)
          end

          it "sorts by created_at" do
            get :index, params: @params.merge(sort: :created_at)
            assert_select "div[class='item-info title']", count: @total_rule_count
            assert_select "div[class*='rule-sorter']",    count: 0
          end

          it "sorts by updated_at" do
            get :index, params: @params.merge(sort: :updated_at)
            assert_select "div[class='item-info title']", count: @total_rule_count
            assert_select "div[class*='rule-sorter']",    count: 0
          end
        end

        describe "On GET to :edit" do
          describe "for #{rule_type.pluralize}" do
            describe "with an agent logged in" do
              before do
                @rule = users(:minimum_agent).send(rule_type.pluralize).first
                assert @rule.present?, "In this context, the user must have an existing #{rule_type.singularize}"

                login("minimum_agent")
              end

              it "does not show any ownership controls" do
                get :edit, params: { id: @rule }
                assert_select ".ownership-controls", count: 0
              end
            end

            describe "with an agent from enterprise plan logged in" do
              before do
                Account.any_instance.stubs(:has_permission_sets?).returns(true)
                Subscription.any_instance.stubs(:has_group_rules?).returns(true)
                Subscription.any_instance.stubs(:has_personal_rules?).returns(true)
                @agent = users(:minimum_agent)
                @permission_set = build_valid_permission_set
                @agent.stubs(:permission_set).returns(@permission_set)
                @rule = @agent.send(rule_type.pluralize).first
                login(@agent)
              end

              describe "and 'full' access to #{rule_type.pluralize}" do
                before do
                  @permission_set.permissions.send("#{rule_type.singularize}_access=", "full")
                end

                it "shows all ownership controls" do
                  get :edit, params: { id: @rule }
                  assert_select ".ownership-controls"
                  assert_select "input#rule_owner_type_account"
                  assert_select "input#rule_owner_type_group"
                  assert_select "input#rule_owner_type_user"
                end

                describe "and tries to edit a rule defined for a group he does not belong to" do
                  before do
                    @request.account = accounts(:with_groups)

                    @groups_agent = users(:with_groups_agent1)
                    @groups_agent.stubs(:permission_set).returns(@permission_set)
                    @group_rule = rules("active_#{rule_type.singularize}_for_minimum_group")
                    @group_rule.account_id = @groups_agent.account_id
                    # assigning a group, to this rule, which is different that this agent's rule
                    @group_rule.owner = groups(:with_groups_group2)

                    refute @groups_agent.groups.include?(@group_rule.owner)

                    @group_rule.save!
                    login(@groups_agent)
                    get :edit, params: { id: @group_rule }
                  end

                  it "shows all ownership controls" do
                    assert_response :success
                    assert_select ".ownership-controls"
                    assert_select "input#rule_owner_type_account"
                    assert_select "input#rule_owner_type_group"
                    assert_select "input#rule_owner_type_user"
                  end
                end
              end

              describe "and 'manage-group' access to #{rule_type.pluralize}" do
                before do
                  @permission_set.permissions.send("#{rule_type.singularize}_access=", "manage-group")
                  get :edit, params: { id: @rule }
                end

                it "shows all ownership controls" do
                  assert_select ".ownership-controls"
                  assert_select "input#rule_owner_type_account", count: 0
                  assert_select "input#rule_owner_type_group"
                  assert_select "input#rule_owner_type_user"
                end
              end

              describe "and 'manage-personal' access to #{rule_type.pluralize}" do
                before do
                  @permission_set.permissions.send("#{rule_type.singularize}_access=", "manage-personal")
                  get :edit, params: { id: @rule }
                end
                it "dos not show ownership controls" do
                  assert_select ".ownership-controls", count: 0
                end
              end
            end

            describe "with an admin logged in" do
              before do
                Subscription.any_instance.stubs(:has_group_rules?).returns(true)
                Subscription.any_instance.stubs(:has_personal_rules?).returns(true)

                @rule = @account.send(rule_type.pluralize).first
                assert @rule.present?, "In this context, the user must have an existing #{rule_type.pluralize}"

                login(:minimum_admin)
              end

              it "shows group ownership controls if the account supports group rules" do
                get :edit, params: { id: @rule }

                assert_select "input#rule_owner_type_group"
              end

              it "shows personal ownership controls if the account supports personal rules" do
                get :edit, params: { id: @rule }

                assert_select "input#rule_owner_type_user"
              end

              it "Only show all (global shared) ownership controls if the account does not support group or personal rules" do
                Subscription.any_instance.stubs(:has_group_rules?).returns(false)
                Subscription.any_instance.stubs(:has_personal_rules?).returns(false)

                get :edit, params: { id: @rule }

                assert_select ".ownership-controls", count: 1
              end

              describe "with a solo or regular account" do
                it "displays upsell message" do
                  Subscription.any_instance.stubs(:has_group_rules?).returns(false)
                  Subscription.any_instance.stubs(:has_personal_rules?).returns(false)

                  get :edit, params: { id: @rule }
                  assert_select "p.plan-upgrade"
                end
              end

              describe "with a plus account" do
                before do
                  subscriptions(:minimum).update_attribute(:plan_type, SubscriptionPlanType.Large)
                  login("minimum_agent")
                end

                it "does not display upsell message" do
                  get :edit, params: { id: @rule }
                  assert_select 'p.plan-upgrade', count: 0
                end
              end
            end
          end # on GET to :edit

          describe "for a rule that the logged in user is not allowed to edit" do
            before do
              User.any_instance.stubs(:can?).returns(true)
              User.any_instance.expects(:can?).with(:edit, kind_of(Rule)).returns(false)
              login("minimum_admin")

              get :edit, params: { id: @account.send(rule_type.pluralize).first }
            end
            it('responds with ok') { assert_response :ok }
          end

          describe "for a non-existing rule" do
            before do
              @controller.stubs(:current_brand).returns(@request.account.brands.first)
              login("minimum_admin")
              get :edit, params: { id: Time.now.to_i.to_s }
            end
            it('responds with not_found') { assert_response :not_found }
          end
        end
      end
    end
  end
end
