require_relative 'rules_controller_tests'
require_relative '../../support/rule'

SingleCov.covered!

describe Rules::ViewsController do
  include RulesControllerTests
  include RulesControllerTests::PersonalTests

  include TestSupport::Rule::Helper

  let(:account) { accounts(:minimum) }

  fixtures :all

  before do
    stub_occam_find(Array(tickets(:minimum_1).id))
  end

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: "rules/views") do |request|
    request.should_route :post, "/rules/views/sort", action: 'sort'
    request.should_route :get, "/rules/views/preview", action: 'preview'
    request.should_route :get, "/rules/views/1/edit", action: 'edit', id: 1
  end

  should_be_unauthorized_when_not_logged_in([[:get, :index, {override_controller: "rules/views"}], :new, :create, :edit, :update, :destroy])
  should_be_unauthorized_when_not_logged_in([[:get, :index, {override_controller: "rules/views"}], :new, :create, :edit, :update, :destroy], 'minimum_end_user', 403)

  describe "#show" do
    before do
      @agent = login(:minimum_agent)
    end

    it "provides matching tickets" do
      rule = rules(:view_my_working_tickets)
      get :show, params: { id: rule.id }

      assert_response :success
      assert @agent.assigned.working.any?
      assert_equal @agent.assigned.working.map(&:nice_id), assigns(:tickets).map(&:nice_id)
    end
  end

  describe "In a prefer lotus context" do
    before do
      @agent = login(:minimum_admin)
      @account.settings.prefer_lotus = true
      @account.settings.save

      @request.account = @account
      @controller.stubs(:current_account).returns(@account)
    end

    describe "On GET to :new" do
      it "includes a hidden input field for the rule's output type" do
        get :new, params: { filter: 'view' }
        assert_response :success

        assert_select "input#output_type[name='output[type]'][value='table']"
      end

      it "does not include sla_next_breach_at as a group by choice" do
        Account.any_instance.stubs(:has_service_level_agreements?).returns(true)
        get :new, params: { filter: 'view' }
        assert_response :success
        assert_select "select#output_group[name='output[group]'] option[value='sla_next_breach_at']", 0
      end
    end

    describe "On GET to :edit" do
      before do
        rules(:view_for_minimum_agent).output.type = "list"
        rules(:view_for_minimum_agent).save
      end

      it "includes a hidden input field for the rule's output type" do
        get :edit, params: { id: rules(:view_for_minimum_agent).id }
        assert_response :success

        assert_select "input#output_type[name='output[type]'][value='table']"
      end

      it "does not include sla_next_breach_at as a group by choice" do
        Account.any_instance.stubs(:has_service_level_agreements?).returns(true)
        get :edit, params: { id: rules(:view_for_minimum_agent).id }
        assert_response :success
        assert_select "select#output_group[name='output[group]'] option[value='sla_next_breach_at']", 0
      end
    end
  end

  describe "#safe_request?" do
    describe "on a POST" do
      before { ActionController::TestRequest.any_instance.stubs(post?: true) }

      it "returns true" do
        assert @controller.send(:safe_request?)
      end
    end

    describe "on a non POST" do
      before { ActionController::TestRequest.any_instance.stubs(post?: false) }

      describe "when the authenticity_token parameter matches the current session's csrf token" do
        before { @controller.params[:authenticity_token] = @controller.send(:form_authenticity_token) }

        it "returns true" do
          assert @controller.send(:safe_request?)
        end
      end

      describe "when there is no authenticity token" do
        it "returns false" do
          refute @controller.send(:safe_request?)
        end
      end

      describe "when the authenticity token comes from the return_to_parser (with an overly escaped +)" do
        before do
          @controller.stubs(form_authenticity_token: "yOVN3MfOwizqMbf0HhpO9mUQ+aCDWWpj4JUYoECNKJg")
          @controller.params[:authenticity_token] = "yOVN3MfOwizqMbf0HhpO9mUQ aCDWWpj4JUYoECNKJg"
        end

        it "returns true" do
          assert @controller.send(:safe_request?)
        end
      end

      describe "when the authenticity token has a = in it" do
        before do
          @controller.stubs(form_authenticity_token: "yOVN3MfOwizqMbf0HhpO9mUQdaCDWWpj4JUYoECNK==")
          @controller.params[:authenticity_token] = "yOVN3MfOwizqMbf0HhpO9mUQdaCDWWpj4JUYoECNK=="
        end

        it "returns true" do
          assert @controller.send(:safe_request?)
        end
      end
    end
  end

  describe "#preview" do
    describe "simple" do
      before do
        login(:minimum_admin)
        @group = groups(:minimum_group)

        stub_occam_find(@group.tickets.map(&:id).reverse)

        get :preview, format: 'js', params: { sets: { '1' => { conditions: { '0' => { source: 'group_id', operator: 'is', value: { '0' => @group.id }}}}} }
        assert_response :success
      end

      it "provides the matching tickets" do
        assert @group.tickets.any?
        assert_equal @group.tickets.map(&:nice_id).sort.reverse, assigns(:tickets).map(&:nice_id)
      end

      it "uses the provided output" do
        assert_equal [:description, :requester, :created, :status, :group, :assignee], assigns(:output).columns
      end

      it "redirects to the last page of results when results are out of range" do
        # Simulate returning 0 results from occam
        stub_occam_find([])

        Comment.stubs(:map_latest_comment_to_tickets).
          raises(Zendesk::Rules::Preview::ResultsOutOfRange, 'message')

        get :preview, format: 'js', params: { page: 2, order: 'test', desc: 't', sets: { '1' => { conditions: { '0' => { source: 'group_id', operator: 'is', value: { '0' => @group.id }}}}} }
        assert_response :redirect
        assert_match /page=1/, @response.location
      end
    end

    it "does not break with invalid user_widget value" do
      login(:minimum_agent)
      @group = groups(:minimum_group)
      get :preview, format: 'js', params: { sets: { '1' => { conditions: { '0' => { source: 'group_id', operator: 'is', value: { '0' => @group.id }}}}}, user_widget: "RandomText" }

      assert_response :success
    end

    it "returns a bad request status when encountering an invalid tickets table column name" do
      login(:minimum_agent)

      put :preview,
        params: {
          "output" => {"group" => "", "order_asc" => "false", "order" => "nice_id", "type" => "list", "group_asc" => "false", "columns" => "scoredescriptionrequestercreatedstatusassignee"},
          "sets" => {"1" => {"conditions" => {"1" => {"operator" => "greater_than", "value" => {"0" => "1"}, "source" => "BANANAS"}}}},
          "rule" => {"title" => "Test ticket escaping", "per_page" => "15", "owner_type" => "Account"}, :override_controller => "rules/views", :rule_type => "view"
        },
        format: "js"

      @response.body.must_include "Invalid rule target: BANANAS"
    end
  end

  describe "#show" do
    before do
      collection = Zendesk::Rules::RuleExecuter::Collection.new(1, 1, 0)
      View.any_instance.stubs(:find_tickets).returns(collection)
    end

    describe "csv" do
      before do
        login(:minimum_agent)
      end

      it "success" do
        get :show, params: { id: rules(:view_for_minimum_agent).id }, format: "csv"
        assert_response :success
        assert_match /^("[^"]+?",?)+$/m, @response.body
      end

      it "does not send back duplicate tickets ZD#243083" do
        ZendeskExceptions::Logger.expects(:record)
        View.any_instance.expects(:find_tickets).returns [tickets(:minimum_1), tickets(:minimum_2), tickets(:minimum_1)]

        get :show, params: { id: rules(:view_for_minimum_agent).id }, format: "csv"
        assert_response :success
        assert_equal 3, @response.body.split("\n").size # header + 2 tickets
      end
    end

    describe "when accessing shared group views" do
      before do
        @user = users(:minimum_agent)
        @group_rule = rules(:active_view_for_minimum_group)
        assert @user.groups.include?(@group_rule.owner)

        @other_group = groups(:minimum_group).dup.tap { |group| group.update!(name: "#{group.name}2") }

        @other_group_rule = @group_rule.dup.tap do |rule|
          rule.owner = @other_group
          rule.save!
        end
      end

      describe "an account agent" do
        before { login(:minimum_agent) }

        it "is allowed to view rules owned by groups that the user belongs to" do
          get :show, params: { id: @group_rule.id }
          assert_response :success
        end

        it "is prevented from viewing to rules owned by groups that the user does not belong to" do
          get :show, params: { id: @other_group_rule.id }
          assert_response :forbidden
        end
      end

      describe "an account admin" do
        before { login(:minimum_admin) }

        it "is allowed to access to all group rules" do
          get :show, params: { id: @other_group_rule.id }
          assert_response :success
        end
      end
    end

    describe "private rules" do
      before do
        login(:minimum_agent)
      end

      it "allows access to private rules belonging to the user" do
        get :show, params: { id: rules(:view_for_minimum_agent).id }
        assert_response :success
      end

      it "prevents access to private rules belonging to other users" do
        get :show, params: { id: rules(:view_for_minimum_admin).id }
        assert_response :forbidden
      end
    end

    describe "for account rules" do
      before do
        @account_rule = @account.views.first
        assert @account_rule

        @other_account_rule = @account_rule.dup
        @other_account_rule.account = accounts(:with_groups)
        @other_account_rule.owner   = @other_account_rule.account
        @other_account_rule.save!

        @other_account_rule.reload
        assert_not_equal @account, @other_account_rule.account
        assert_not_equal @account, @other_account_rule.owner

        login(:minimum_agent)
      end

      it "allows access to shared rules belonging to the user's Account" do
        get :show, params: { id: @account_rule.id }
        assert_response :success
      end

      it "prevents access to shared rules belonging to other accounts" do
        @controller.stubs(:current_brand).returns(@account.brands.first)
        get :show, params: { id: @other_account_rule.id }
        assert_response :not_found
      end
    end

    describe "RSS" do
      before do
        @minimum_admin = login("minimum_admin")
        @rule = rules(:view_assigned_working_tickets)
        ticket = tickets(:minimum_1)
        ticket.stubs(:comments).returns([])
        collection = WillPaginate::Collection.create(1, 10) do |pager|
          pager.replace([ticket])
          pager.total_entries = 1
        end

        @rule.expects(:find_tickets).returns(collection)
        View.stubs(:find_by_sql).returns([])
        View.expects(:find_by_sql).returns([@rule])
      end

      it "renders in RSS format" do
        get :show, params: { id: @rule.id }, format: 'rss'
        assert_template "rules/show"
        assert_includes @response.body, "<rss version=\"2.0\""
        assert_response :success
      end
    end
  end

  describe "On GET to :index with filter=views and sorting/selecting" do
    before do
      @account.stubs(:has_rule_usage_stats?).returns(true)
      @controller.stubs(:current_account).returns(@account)
      Subscription.any_instance.stubs(:has_group_rules?).returns(true)
      login("minimum_admin")
      @params = {filter: rule_type }
    end

    it "selects by group" do
      group_id = groups(:minimum_group).id
      get :index, params: @params.merge(select: "group_id_#{group_id}")
      assert_select "div[class='item-info title']", count: 2
      assert_select "div[class*='rule-sorter']",    count: 0
    end

    it "selects by no group" do
      get :index, params: @params.merge(select: "group_none")
      assert_select "div[class='item-info title']", count: 9
      assert_select "div[class*='rule-sorter']",    count: 0
    end
  end

  describe "as an agent" do
    before do
      login('minimum_agent')
      # Skip routing, since it requires a call out to Deco, which we're not mocking
      Arturo.disable_feature!(:routing)
    end

    it "displays the rule with related tickets (1)" do
      # Populate a view with all available columns
      output = rules(:view_assigned_working_tickets).output
      context = Zendesk::Rules::Context.new(accounts(:minimum), users(:minimum_admin),
        rule_type: :view, component_type: :selection)

      output.columns = ZendeskRules::Definitions::Conditions.definitions_as_json(context).map do |t|
        t[:output_key]
      end

      rules(:view_assigned_working_tickets).output = output
      rules(:view_assigned_working_tickets).current_user = users(:minimum_admin)
      rules(:view_assigned_working_tickets).save(validate: false)

      get :show, params: { id: rules(:view_assigned_working_tickets).id }
      assert_response :ok
      assert_template 'show'
      assert_equal 'text/html', @response.content_type
      assert_select '.linked', 1
      assert_select '.group_by', 1
    end

    it "displays the rule with related tickets (2)" do
      get :show, params: { id: rules(:view_my_working_tickets).id }
      assert_response :ok
      assert_template 'show'
      assert_equal 'text/html', @response.content_type
    end

    it "displays the rule with related tickets (3)" do
      stub_occam_find([tickets(:minimum_1).id])

      get :show, params: { id: rules(:view_my_solved_tickets).id }
      assert_response :ok
      assert_template 'show'
      assert_equal 'text/html', @response.content_type
      assert_select '.linked', 1
      assert_select '.group_by', 1
    end

    it "displays the rule with related tickets (4)" do
      stub_occam_find([tickets(:minimum_1).id, tickets(:minimum_2).id, tickets(:minimum_3).id])

      get :show, params: { id: rules(:view_unassigned_to_group).id }
      assert_response :ok
      assert_template 'show'
      assert_equal 'text/html', @response.content_type
      assert_select '.linked', 3
      assert_select '.group_by', 0
    end

    it "is able to view RSS" do
      accept :rss
      @request.env['HTTP_IF_MODIFIED_SINCE'] = 2000.days.ago.to_s
      get :show, params: { id: rules(:view_my_working_tickets).id }
      assert_response :ok
      assert_template 'rules/show'
      assert_includes @response.body, "<rss version=\"2.0\""
      assert_xml_select 'item', 1
    end
  end

  describe "an out of range query from the browser" do
    before do
      @rule = rules(:view_assigned_working_tickets)
      login(:minimum_agent)

      # Simulate returning 0 results from occam
      stub_occam_find([])

      Comment.stubs(:map_latest_comment_to_tickets).
        raises(Zendesk::Rules::Preview::ResultsOutOfRange, 'message')
    end

    it "redirects to the last page of results" do
      get :show, params: { id: rules(:view_assigned_working_tickets), page: 2 }
      assert_response :redirect
      assert_match /page=1/, @response.location
    end
  end

  describe 'DELETE #destroy_inactive' do
    before { login(:minimum_admin) }
    it "redirects as asked" do
      delete :destroy_inactive, params: { filter: "views", return_to: '/somewhere' }
      assert_redirected_to '/somewhere'
    end
  end

  it "tests actions for agent" do
    login('minimum_agent')

    # Submit view
    params = get_valid_rule_parameters
    params[:rule].merge!(type: 'View', title: 'View test', owner_type: "User", owner_id: users(:minimum_agent).id)
    view_columns = 'satisfaction_score,description,requester,created,status,priority,type,group,assignee,solved,submitter,assigned,nice_id'
    post :create, params: params.merge(output: {columns: view_columns, type: 'table', group_asc: false, group: :created})
    assert_response 406  # Too many fields

    view_columns = 'satisfaction_score,description,requester,created,status,group,assignee,submitter,assigned'
    post :create, params: params.merge(output: {columns: view_columns, type: 'table', group_asc: false, group: :created})
    assert_response 302  # created

    rule = Rule.find(Rule.maximum('id'))
    assert_equal 'View test', rule.title
    assert_equal view_columns.split(',').sort, rule.output.columns.sort.map(&:to_s)
    assert_equal rule.output.type, 'table'
    assert_equal 15, rule.per_page
    assert_equal rule.output.group, :created
    refute rule.output.group_asc
    assert rule.owner.is_a?(User)
    assert_equal rule.owner, users(:minimum_agent)
    assert rule.is_active

    # Update view
    post :update, params: params.merge(id: rule.id, output: {columns: 'satisfaction_score,requester', type: 'list', order_asc: true, order: :satisfaction_score})
    assert_response 302
    rule.reload
    assert_equal 2, rule.output.columns.length
    assert_equal rule.output.type, 'list'
    assert_equal rule.output.order, :satisfaction_score
    assert rule.output.order_asc
  end

  it "tests actions for admin" do
    # RSS
    login('minimum_admin')
    accept :rss
    get :show, params: { id: rules(:view_assigned_working_tickets).id, is_public: true }
    assert_responded_with(:rss)
    assert_response :ok
  end

  describe "On a POST to :sort" do
    describe "with shared views" do
      before do
        login('minimum_admin')
        @rules = accounts(:minimum).views.sort_by(&:id)
        post :sort, params: { 'active-rules-sort-list': @rules.reverse.map(&:id) }
      end
      it('responds with ok') { assert_response :ok }
      it "sorts the rules as specified" do
        assert_equal @rules.reverse, accounts(:minimum).views(order: "position DESC")
      end
    end

    describe "with personal views" do
      before do
        @admin = users(:minimum_admin)
        login(@admin)
        @rules = @admin.views.sort_by(&:id)
        post :sort, params: { 'active-rules-sort-list': @rules.reverse.map(&:id) }
      end
      it('responds with ok') { assert_response :ok }
      it "sorts the rules as specified" do
        assert_equal @rules.reverse, @admin.views(order: "position DESC")
      end
    end
  end

  describe 'on a PUT to :update' do
    let(:rule) { rules(:view_for_minimum_agent) }

    before do
      login(:minimum_agent)

      account.update_attribute(:host_mapping, 'test.it.com')
    end

    describe 'when the `return_to` parameter is provided' do
      before do
        put(
          :update,
          params: {
            id:        rule.id,
            rule:      {title: 'Views test updated'},
            return_to: '/some/url'
          }
        )
      end

      it 'updates the view title' do
        assert_equal 'Views test updated', rule.reload.title
      end

      it 'redirects to the `return_to` url' do
        assert_equal "https://#{request.host}/some/url", response['Location']
      end
    end

    describe 'when the `return_to` parameter is not provided' do
      before { put :update, params: { id: rule.id, rule: {title: 'Views test updated'} } }

      it 'redirects to the correct url' do
        assert_equal(
          "https://#{request.host}/support/admin/views",
          response['Location']
        )
      end
    end
  end

  describe 'on a DELETE to :destroy' do
    let(:rule) { create_view }

    before do
      login(:minimum_admin)

      account.update_attribute(:host_mapping, 'test.it.com')
    end

    describe 'when the `return_to` parameter is provided' do
      before { delete :destroy, params: { id: rule.id, return_to: '/some/url' } }

      it 'soft-deletes the rule' do
        assert rule.reload.deleted?
      end

      it 'redirects to the `return_to` url' do
        assert_equal "https://#{request.host}/some/url", response['Location']
      end
    end

    describe 'when the `return_to` parameter is not provided' do
      before { delete :destroy, params: { id: rule.id } }

      it 'soft-deletes the rule' do
        assert rule.reload.deleted?
      end

      it 'redirects to the correct url' do
        assert_equal(
          "https://#{request.host}/support/admin/views",
          response['Location']
        )
      end
    end
  end
end
