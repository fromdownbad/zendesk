require_relative 'rules_controller_tests'
require_relative '../../support/rule'

SingleCov.covered!

describe Rules::TriggersController do
  include RulesControllerTests
  include RulesControllerTests::BusinessRules

  include TestSupport::Rule::Helper
  include TestSupport::Rule::UsageHelper

  let(:account) { accounts(:minimum) }

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: "rules/triggers") do |request|
    request.should_route :post, "/rules/triggers/sort", action: 'sort'
  end

  def rule
    rules(:trigger_notify_requester_of_received_request)
  end

  it "tests actions for admin" do
    login('minimum_admin')
    # Submit trigger
    params = get_valid_rule_parameters
    post :create, params: params
    assert_response 302
    rule = Rule.find(Rule.maximum('id'))
    assert_equal 'Trigger test', rule.title
    assert rule.owner.is_a?(Account)
    assert rule.is_active
  end

  it "fails when creating a rule with wrong update_type" do
    login('minimum_admin')
    # Submit trigger
    params = get_valid_rule_parameters
    params[:sets]['1'][:conditions]['1'] = {
      'source' => 'update_type',
      'operator' => 'is',
      'value' => 'foo'
    }

    post :create, params: params
    assert_response 406
  end

  it "tests actions for agent" do
    login(:minimum_agent)
    # Attempt to submit trigger
    post :create, params: get_valid_rule_parameters
    assert_response 406
  end

  describe "A PUT to :update" do
    before do
      login(:minimum_admin)
      @rule = @account.send(rule_type.pluralize).active.first
      @rule.class.any_instance.stubs(:valid?).returns true
    end

    it "updates a rule" do
      assert @rule.is_active?
      put :update, params: { id: @rule.id, rule: { is_active: false } }

      assert_response 302
      refute @rule.reload.is_active?
    end

    it "activate/deactivates a rule while updating" do
      put :update, params: { id: @rule.id, submit_type: "deactivate", rule: { title: "bah" } }

      assert_response 302
      refute @rule.reload.is_active?
    end

    it "deletes a rule if submit_type is 'delete'" do
      put :update, params: { id: @rule.id, submit_type: "delete", rule: {title: "misdirection"} }

      assert_response 302
      assert @rule.reload.deleted?
    end

    it 'updates conditions' do
      params = get_valid_rule_parameters(rule_type)
      params[:sets]["1"].delete(:actions)
      put :update, params: params.merge(id: @rule.id)

      assert_response 302
    end
  end

  describe 'when sorting by execution count' do
    let(:rule)    { rules(:trigger_notify_requester_of_received_request) }
    let(:ticket)  { tickets(:minimum_1) }

    let(:selector) { "div[class='item-info title']" }

    before do
      login('minimum_admin')

      Account.any_instance.stubs(has_rule_usage_stats?: rule_usage_stats)

      rule.update_attribute(:title, "Trigger test #{timeframe}")

      Timecop.freeze

      Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

      [30.minutes.ago, 10.hours.ago, 4.days.ago].each do |time|
        Timecop.travel(time) do
          record_usage(type: :trigger, id: rule.id)
        end
      end

      get :index, params: { filter: 'triggers', sort: "execution_count_#{timeframe}" }
    end

    describe "and the account does not have the `rule_usage_stats` feature" do
      let(:rule_usage_stats) { false }

      describe "and sorting by 'hourly' execution count" do
        let(:timeframe) { 'hourly' }

        it 'does not include the usage' do
          assert_select selector, 'Trigger test hourly'
        end
      end
    end

    describe "and the account has the `rule_usage_stats' feature" do
      let(:rule_usage_stats) { true }

      describe "and sorting by 'hourly' execution count" do
        let(:timeframe) { 'hourly' }

        it 'includes usage' do
          assert_select selector, 'Trigger test hourly (1)'
        end
      end

      describe "and sorting by 'daily' execution count" do
        let(:timeframe) { 'daily' }

        it 'includes usage' do
          assert_select selector, 'Trigger test daily (2)'
        end
      end

      describe "and sorting by 'weekly' execution count" do
        let(:timeframe) { 'weekly' }

        it 'includes usage' do
          assert_select selector, 'Trigger test weekly (3)'
        end
      end
    end
  end

  describe '#update' do
    let(:rule) { account.triggers.active.first }

    before do
      login(:minimum_admin)

      account.update_attribute(:host_mapping, 'test.it.com')
    end

    describe 'when the `return_to` parameter is provided' do
      before do
        put(
          :update,
          params: {
            id:        rule.id,
            rule:      {title: 'Trigger test updated'},
            return_to: '/some/url'
          }
        )
      end

      it 'updates the trigger title' do
        assert_equal 'Trigger test updated', rule.reload.title
      end

      it 'redirects to the `return_to` url' do
        assert_equal "https://#{request.host}/some/url", response['Location']
      end
    end

    describe 'when the `return_to` parameter is not provided' do
      before do
        put(
          :update,
          params: {
            id:   rule.id,
            rule: {title: 'Trigger test updated'}
          }
        )
      end

      it 'redirects to the correct url' do
        assert_equal(
          "https://#{request.host}/support/admin/triggers",
          response['Location']
        )
      end
    end
  end

  describe '#destroy' do
    let(:rule) { create_trigger }

    before do
      login(:minimum_admin)

      account.update_attribute(:host_mapping, 'test.it.com')
    end

    describe 'when the `return_to` parameter is provided' do
      before { delete :destroy, params: { id: rule.id, return_to: '/some/url' } }

      it 'soft-deletes the rule' do
        assert rule.reload.deleted?
      end

      it 'redirects to the `return_to` url' do
        assert_equal "https://#{request.host}/some/url", response['Location']
      end
    end

    describe 'when the `return_to` parameter is not provided' do
      before { delete :destroy, params: { id: rule.id } }

      it 'soft-deletes the rule' do
        assert rule.reload.deleted?
      end

      it 'redirects to the correct url' do
        assert_equal(
          "https://#{request.host}/support/admin/triggers",
          response['Location']
        )
      end
    end
  end
end
