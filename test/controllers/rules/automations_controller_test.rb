require_relative 'rules_controller_tests'
require_relative '../../support/rule'

SingleCov.covered!

describe Rules::AutomationsController do
  include RulesControllerTests
  include RulesControllerTests::BusinessRules

  include TestSupport::Rule::Helper
  include TestSupport::Rule::UsageHelper

  let(:account) { accounts(:minimum) }

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: 'rules/automations') do |request|
    request.should_route :post, '/rules/automations/sort', action: 'sort'
  end

  def rule
    rules(:automation_close_ticket)
  end

  describe "#create" do
    it "creates as agent" do
      login(:minimum_agent)

      # Attempt to submit automation
      params = get_valid_rule_parameters
      params[:rule][:type] = 'Automation'
      post :create, params: params
      assert_response 406
      rule = Rule.find(Rule.maximum('id'))
      rule.title.wont_equal 'Automation'
    end

    it "creates as admin" do
      login(:minimum_admin)
      params = get_valid_rule_parameters

      # Submit automation
      params[:rule].merge!(type: 'Automation', title: 'Automation test')
      post :create, params: params
      assert_response 302
      rule = Rule.find(Rule.maximum('id'))
      assert_equal 'Automation test', rule.title
      assert rule.is_active
    end
  end

  describe "#update" do
    let(:rule) { rules(:automation_for_custom_user_model) }

    before do
      login(:minimum_admin)

      account.update_attribute(:host_mapping, 'test.it.com')
    end

    describe 'when the `return_to` parameter is provided' do
      before do
        put(
          :update,
          params: {
            id:        rule.id,
            rule:      {title: 'Automation test updated'},
            return_to: '/some/url'
          }
        )
      end

      it 'updates the automation title' do
        assert_equal 'Automation test updated', rule.reload.title
      end

      it 'redirects to the `return_to` url' do
        assert_equal "https://#{request.host}/some/url", response['Location']
      end
    end

    describe 'when the `return_to` parameter is not provided' do
      before do
        put :update, params: { id: rule.id, rule: {title: 'Automation test updated'} }
      end

      it 'redirects to the correct url' do
        assert_equal(
          "https://#{request.host}/support/admin/automations",
          response['Location']
        )
      end
    end
  end

  describe '#destroy' do
    let(:rule) { create_automation }

    before do
      login(:minimum_admin)

      account.update_attribute(:host_mapping, 'test.it.com')
    end

    describe 'when the `return_to` parameter is provided' do
      before { delete :destroy, params: { id: rule.id, return_to: '/some/url' } }

      it 'soft-deletes the rule' do
        assert rule.reload.deleted?
      end

      it 'redirects to the `return_to` url' do
        assert_equal "https://#{request.host}/some/url", response['Location']
      end
    end

    describe 'when the `return_to` parameter is not provided' do
      before { delete :destroy, params: { id: rule.id } }

      it 'soft-deletes the rule' do
        assert rule.reload.deleted?
      end

      it 'redirects to the correct url' do
        assert_equal(
          "https://#{request.host}/support/admin/automations",
          response['Location']
        )
      end
    end
  end

  describe "#index" do
    describe 'when sorting by execution count' do
      let(:rule)   { rules(:automation_close_ticket) }
      let(:ticket) { tickets(:minimum_1) }

      let(:selector) { "div[class='item-info title']" }

      before do
        login('minimum_admin')

        Account.any_instance.stubs(has_rule_usage_stats?: rule_usage_stats)

        rule.update_attribute(:title, "Automation test #{timeframe}")

        Timecop.freeze

        Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

        [30.minutes.ago, 10.hours.ago, 4.days.ago].each do |time|
          Timecop.travel(time) do
            record_usage(type: :automation, id: rule.id)
          end
        end

        get :index, params: { filter: 'automations', sort: "execution_count_#{timeframe}" }
      end

      describe "and the account does not have the `rule_usage_stats` feature" do
        let(:rule_usage_stats) { false }

        describe "and sorting by 'hourly' execution count" do
          let(:timeframe) { 'hourly' }

          it 'does not include the usage' do
            assert_select selector, 'Automation test hourly'
          end
        end
      end

      describe "and the account has the `rule_usage_stats` feature`" do
        let(:rule_usage_stats) { true }

        describe "and sorting by 'hourly' execution count" do
          let(:timeframe) { 'hourly' }

          it 'includes usage' do
            assert_select selector, 'Automation test hourly (1)'
          end
        end

        describe "and sorting by 'daily' execution count" do
          let(:timeframe) { 'daily' }

          it 'includes usage' do
            assert_select selector, 'Automation test daily (2)'
          end
        end

        describe "and sorting by 'weekly' execution count" do
          let(:timeframe) { 'weekly' }

          it 'includes usage' do
            assert_select selector, 'Automation test weekly (3)'
          end
        end
      end
    end
  end
end
