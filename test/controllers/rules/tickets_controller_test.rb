require_relative "../../support/test_helper"

SingleCov.covered!

describe Rules::TicketsController do
  fixtures :accounts, :subscriptions, :addresses, :users, :user_identities, :tickets, :rules,
    :ticket_fields, :custom_field_options, :ticket_field_entries, :account_property_sets

  should_include_module Zendesk::LotusEmbeddable

  describe "When rule usage stats are enabled" do
    before do
      Account.any_instance.stubs(:has_rule_usage_stats?).returns(true)       # Arturo
      Subscription.any_instance.stubs(:has_rule_usage_stats?).returns(true)  # Pricing model
      @ticket = tickets(:minimum_1)
      @rule = rules(:trigger_notify_requester_of_comment_update)
    end

    describe "an agent" do
      before do
        @current_account = accounts(:minimum)
        @request.account = @current_account
        login("minimum_agent")
      end

      describe "viewing the Rules:TicketController" do
        before { get :index, params: { rule_id: @rule.id } }
        it('responds with forbidden') { assert_response :forbidden }
      end
    end

    describe "an admin user" do
      before do
        @current_account = accounts(:minimum)
        @request.account = @current_account
        login("minimum_admin")
      end

      describe "viewing the Rules:TicketController" do
        describe "when no affected tickets exist" do
          before { get :index, params: { rule_id: @rule.id } }
          it "does not display a list of tickets" do
            assert_select 'table#tickets', count: 0
          end
        end

        describe "when affected tickets exist" do
          before do
            @rule.apply_actions(@ticket)
            assert_difference('TicketJoin.count(:all)') { @ticket.save }
            get :index, params: { rule_id: @rule.id }
          end

          it "displays a list of affected tickets" do
            assert_select 'table.tickets'
            assert_select "tr[data-zd-ticket-nice-id='#{@ticket.nice_id}']"
          end
        end
      end
    end
  end
end
