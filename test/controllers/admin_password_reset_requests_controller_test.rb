require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe AdminPasswordResetRequestsController do
  fixtures :accounts, :users

  should_route :post, "/password/admin_reset_request/42", controller: "admin_password_reset_requests", action: :create, user_id: 42

  before do
    Token.any_instance.stubs(generate: "foo")
    @account          = accounts(:minimum)
    @request.account  = @account
    @user             = users(:minimum_end_user)
    @admin            = users(:minimum_admin)

    login(@admin)
  end

  describe "audits and tokens" do
    before do
      @token = @account.password_reset_tokens.create!(source_type: "User", source_id: @user.id)
      @token.value = "bar"
      @token.save!
    end

    it "does not immediately consume the token" do
      refute_difference '@account.password_reset_tokens.count(:all)' do
        post :create, params: { user_id: @user.id.to_s }
        assert_response :redirect
      end

      refute_equal @account.users.find(@user.id).password_reset_tokens.last.value, @token.value
      assert_equal @account.users.find(@user.id).password_reset_tokens.last.value, "foo"
    end

    it "should make a password_change audit" do
      assert_difference '@user.password_changes.count(:all)', 1 do
        post :create, params: { user_id: @user.id.to_s }
      end
    end
  end

  describe "as an end user" do
    before do
      logout
      login(@user)
    end

    it "blocks access" do
      post :create, params: { user_id: @admin.id.to_s }
      assert_response :forbidden
    end

    it "does not scramble the admin password" do
      password = @admin.crypted_password

      post :create, params: { user_id: @admin.id.to_s }
      assert_equal @admin.reload.crypted_password.to_s, password.to_s
    end
  end

  describe "#create" do
    before do
      Timecop.freeze
      @token = @account.password_reset_tokens.create!(source_type: "User", source_id: @user.id)
      @token.value = "bar"
      @token.save!
      UsersMailer.expects(:deliver_admin_password_reset).at_least(1).with(@user, "foo")
      post :create, params: { user_id: @user.id.to_s }
    end

    it "assigns the user to @user" do
      assert_equal @user, assigns(:user)
    end

    describe "audit" do
      before do
        @audit = @user.cia_attribute_changes.last
        @old_audit = @user.password_changes.last # should not audit end-user changes
      end

      it "audits the changed password" do
        assert @old_audit
        assert_nil @audit
      end

      it "has the admin as actor" do
        assert_equal @admin, @old_audit.actor
        assert_nil @audit
      end
    end

    it "responds with 302 redirect OK" do
      assert_response :redirect
    end

    it "does not show any error" do
      assert_nil flash[:error]
    end

    it "shows a notice" do
      assert flash[:notice]
    end

    it "does not render template :reset_request_success" do
      assert_redirected_to user_path(@user)
    end

    describe "with a user who has no email address" do
      before do
        User.any_instance.expects(:email).at_least(1).returns(nil)
        post :create, params: { user_id: @user.id.to_s }
      end

      it "responds with 302 redirect OK" do
        assert_response :redirect
      end

      it "shows an error" do
        assert flash[:error]
      end
    end
  end

  describe 'logging when an email is prevented from being sent' do
    before do
      User.
        any_instance.
        expects(:can?).
        with(:request_password_reset, instance_of(User)).
        returns(false)
    end

    it 'logs the reason no email was sent' do
      # Blanket expect here so we can be specific with a logging message.
      Rails.logger.expects(:info).at_least_once

      Rails.logger.expects(:info).with(
        "Preventing password reset email for " \
        "account_id: #{@user.account_id}, " \
        "user_id: #{@user.id}, " \
        "email: #{@user.email.inspect}, " \
        "reason: the current_user (#{@controller.send(:current_user).id}) is " \
        "not capable of " \
        "requesting a password reset for user (#{@user.id})."
      )

      post :create, params: { user_id: @user.id.to_s }
    end
  end
end
