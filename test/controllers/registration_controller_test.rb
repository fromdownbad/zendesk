require_relative "../support/test_helper"

SingleCov.covered! uncovered: 8

describe RegistrationController do
  fixtures :accounts, :users, :remote_authentications, :subscriptions,
    :account_property_sets, :addresses, :translation_locales, :role_settings

  with_options(controller: "registration") do |request|
    request.should_route :get,  "/registration",          action: 'index'
    request.should_route :get,  "/registration/register", action: 'register'
    request.should_route :post, "/registration/register", action: 'register'
  end

  before do
    @account         = accounts(:minimum)
    @route           = @account.route
    @user_params     = { email: 'mentor@example.com', name: 'men tor', password: 'mentor', timezone_name: "America/Los_Angeles" }

    @request.account = @account
    @account.enable_help_center!
    @controller.stubs(:current_brand).returns(@request.account.default_brand)
    @controller.stubs(:current_route).returns(@route)
  end

  describe "sanitizing an html injected email address" do
    it "returns html escaped" do
      assert_equal "foobar&lt;img src=&quot;http://i.imgur.com/SPk1X.jpg&quot;", RegistrationController.new.send(:sanitize, "foobar<img src=\"http://i.imgur.com/SPk1X.jpg\"")
    end
  end

  it "denys access when registration is disabled" do
    @account.stubs(:is_open?).returns(false)
    @account.role_settings.update_attributes!(end_user_zendesk_login: true, end_user_remote_login: false)

    post :register, params: { user: @user_params }
    assert_response 401
    assert_match /registration not enabled for account #{@account.subdomain}/, @response.body

    get :index
    assert_response 401
    assert_match /registration not enabled for account #{@account.subdomain}/, @response.body
  end

  it "denies access when end users cannot login" do
    @account.stubs(:is_open?).returns(true)
    @account.role_settings.update_attributes!(end_user_zendesk_login: false, end_user_remote_login: true, end_user_password_allowed: false)

    post :register, params: { user: @user_params }
    assert_response 401
    assert_match /registration not enabled for account #{@account.subdomain}/, @response.body

    get :index
    assert_response 401
    assert_match /registration not enabled for account #{@account.subdomain}/, @response.body
  end

  describe "user registration is allowed when" do
    before do
      @controller.stubs(:verify_recaptcha).returns(true)
      @account.stubs(:whitelists?).returns(@user_params[:email])
    end

    describe "end_user_zendesk_login is enabled, end_user_password_allowed is disabled" do
      it "and remote login is disabled" do
        @account.stubs(:is_open?).returns(true)
        @account.role_settings.update_attributes!(end_user_zendesk_login: true, end_user_remote_login: false, end_user_password_allowed: false)

        post :register, params: { user: @user_params }
        assert_response :redirect
        assert_redirected_to "#{@account.url}/registration/success?user%5Bname%5D=men+tor"

        get :index
        assert_response 200
      end

      it "and remote login is enabled" do
        @account.stubs(:is_open?).returns(true)
        @account.role_settings.update_attributes!(end_user_zendesk_login: true, end_user_remote_login: true, end_user_password_allowed: false)

        post :register, params: { user: @user_params }
        assert_response :redirect
        assert_redirected_to "#{@account.url}/registration/success?user%5Bname%5D=men+tor"

        get :index
        assert_response 200
      end
    end

    describe "end_user_zendesk_login is disabled, end_user_password_allowed is enabled" do
      it "and remote login is disabled" do
        @account.stubs(:is_open?).returns(true)
        @account.role_settings.update_attributes!(end_user_zendesk_login: false, end_user_remote_login: false, end_user_password_allowed: true)

        post :register, params: { user: @user_params }
        assert_response :redirect
        assert_redirected_to "#{@account.url}/registration/success?user%5Bname%5D=men+tor"

        get :index
        assert_response 200
      end

      it "and remote login is enabled" do
        @account.stubs(:is_open?).returns(true)
        @account.role_settings.update_attributes!(end_user_zendesk_login: false, end_user_remote_login: true, end_user_password_allowed: true)

        post :register, params: { user: @user_params }
        assert_response :redirect
        assert_redirected_to "#{@account.url}/registration/success?user%5Bname%5D=men+tor"

        get :index
        assert_response 200
      end
    end
  end

  it "renders the sso_login template if enabled" do
    get :index
    assert_template 'auth/sso_v2_index'
  end

  describe "#index" do
    describe "when web_portal is disabled and help_center is not enabled" do
      it "renders 404" do
        @account.stubs(:help_center_enabled?).returns(false)
        @account.stubs(:web_portal_disabled?).returns(true)

        get :index

        assert_response 404
      end
    end

    describe "when web_portal is restricted and help_center is enabled" do
      it "renders the sso_index registration page" do
        @account.stubs(:help_center_enabled?).returns(true)
        @account.stubs(:web_portal_disabled?).returns(false)

        get :index, params: { theme: 'hc' }

        assert_template 'auth/sso_v2_index'
      end
    end

    it "redirects to account_home if user is already logged in" do
      login('minimum_end_user')
      get :index
      assert_redirected_to "https://minimum.zendesk-test.com/hc"
    end

    describe "with hostmapping" do
      before do
        @account.update_attribute(:host_mapping, "wibble.example.com")
        @controller.stubs(:current_brand).returns(@request.account.default_brand)
        @controller.stubs(:current_route).returns(@request.account.default_brand.route)
      end

      it "renders the page on the subdomain" do
        get :index

        assert_response :ok
        assert_template 'auth/sso_v2_index'
      end

      it "renders the page on the hostmapped domain" do
        @request.host = 'wibble.example.com'
        get :index

        assert_response :ok
        assert_template 'auth/sso_v2_index'
      end
    end
  end

  describe "#success" do
    before do
      @controller.stubs(:current_brand).returns(@account.default_brand)
      @controller.stubs(:current_route).returns(@account.default_brand.route)
    end

    it "renders the sso_login template if enabled" do
      login(@request.account.owner)
      @request.account.stubs(:is_ssl_enabled?).returns(true)

      get :success

      assert_template 'sso_v2_index'
    end

    it "clears the session and logout when ssl is not required" do
      login(@request.account.owner)
      @request.account.stubs(:is_ssl_enabled?).returns(false)
      get :success
      response.body.must_include "/assets/zendesk/auth/v2/host"
    end

    it "does not show unsupported media type from mobile device LIBO" do
      login(@request.account.owner)
      @request.account.stubs(:is_ssl_enabled?).returns(true)

      get :success, format: 'mobile_v2'
      assert_response :ok

      response.body.must_include "/assets/zendesk/auth/v2/host"
      response.body.must_include "successful_registration"
    end
  end

  describe "#register" do
    before do
      @account.stubs(:help_center_enabled?).returns(true)
      @account.stubs(:whitelists?).returns(@user_params[:email])
    end

    it "allows the user to view the registration form" do
      get :index
      assert_response :success
    end

    it "renders the sso index" do
      get :index
      assert_template 'auth/sso_v2_index'
    end

    it "creates the user" do
      assert_difference '@account.users.count(:all)' do
        post :register, params: { user: @user_params }
      end
      assert_not_nil session[:user_name]
      assert_not_nil session[:user_email]
      assert_redirected_to "#{@account.url}/registration/success?user%5Bname%5D=men+tor"
    end

    describe "timezone" do
      before do
        Subscription.any_instance.stubs(:has_individual_time_zone_selection?).returns(true)
      end

      it "sets timezone if timezone_name is present and it is valid" do
        post :register, params: { user: @user_params }

        assert_equal "Pacific Time (US & Canada)", @account.users.last.time_zone
      end

      it "does not fail if timezone_name is not present" do
        @user_params.delete(:timezone_name)

        post :register, params: { user: @user_params }

        assert_equal "Copenhagen", @account.users.last.time_zone
      end

      it "does not fail if timezone_name is not valid" do
        @user_params[:timezone_name] = "not a real timezone"
        post :register, params: { user: @user_params }

        assert_equal "Copenhagen", @account.users.last.time_zone
      end
    end

    it "redirects to the return_to" do
      assert_difference '@account.users.count(:all)' do
        post :register, params: { user: @user_params, return_to: 'https://minimum.zendesk-test.com/foo' }
      end
      assert_response :redirect
      assert_equal 'https://minimum.zendesk-test.com/foo?user%5Bname%5D=men+tor', response.location
    end

    it "gos to twitter auth when I enter my twitter handle" do
      post :register, params: { user: @user_params.merge(twitter_screen_name: "foobar"), return_to: "/somewhere" }
      assert_redirected_to "/registration/request_oauth" # going to twitter auth
      assert @controller.send(:current_user) # logged in so I'm logged in when coming back
      assert @controller.send(:return_to_parser).user # logged in to return_to so it builds challenge when needed
      assert_equal "/somewhere", session[:return_to] # filled session so users_controller can use it after twitter auth
      assert_equal session[:create_twitter_profile_user_id], User.find_by_name(@user_params[:name]).id # stores user ID in session
    end

    it "sends a verification email" do
      assert_difference 'ActionMailer::Base.deliveries.size' do
        ActionMailer::Base.perform_deliveries = true
        post :register, params: { user: @user_params }
      end
    end

    it "sets the active_brand_id on a user if brand_id is passed in" do
      ActionMailer::Base.perform_deliveries = true
      brand = FactoryBot.create(:brand, account_id: @account.id, subdomain: 'wombats', active: true)
      post :register, params: { user: @user_params, brand_id: brand.id }
      ActionMailer::Base.deliveries.last.joined_bodies.must_include "https://wombats.zendesk-test.com/verification/email/"
    end

    it "redirects when not a POST" do
      get :register, params: { user: @user_params }
      assert_response :redirect
    end

    it "fails when not from a human" do
      @controller.stubs(:verify_recaptcha).returns(false)

      post :register, params: { user: @user_params }

      flash[:error].must_include "Verification failed"
    end

    it "redirects to return_to_on_failure" do
      @controller.stubs(:verify_recaptcha).returns(false)
      url = @request.account.url + '/failure'

      post :register, params: { user: @user_params, return_to_on_failure: url }

      location = @response.headers['Location']
      assert location.start_with?(url), "#{location} is not return_to_on_failure"
    end

    it "does not echo message parameter markup" do
      get :index, params: { message: "Please try a new set of words7f768<a>0d342f7e17d" }

      assert_select "#error", 0
    end

    it "does not allow setting roles" do
      post :register, params: { user: @user_params.merge(roles: 2) }
      assert_response :redirect
      user = User.order("created_at").last
      assert_equal 0, user.roles

      # Don't crash on just a get
      get :register
      assert_response :redirect
    end

    describe "redirection security on failures" do
      it "redirects to valid hosts" do
        post :register, params: { user: @user_params.merge(email: ' '), return_to: "http://minimum.zendesk-test.com/foo" }
        assert_includes @controller.url_for(@response.redirected_to), "minimum.zendesk-test.com"
      end

      it "does not redirect to evil stuff" do
        post :register, params: { user: @user_params.merge(email: ' '), return_to: "http://evil.com/" }
        assert_includes @controller.url_for(@response.redirected_to), "minimum.zendesk-test.com"
      end

      it "does not redirect to broken stuff" do
        post :register, params: { user: @user_params.merge(email: ' '), return_to: "Mooooh" }
        assert_includes @controller.url_for(@response.redirected_to), "minimum.zendesk-test.com"
      end
    end

    it "fails when the email is blank" do
      post :register, params: { user: @user_params.merge(email: ' ') }
      flash[:error].must_include "provide an email address"
    end

    it "fails when the email is already in use" do
      2.times { post :register, params: { user: @user_params } }
      flash[:error].must_include "already signed up"
    end

    it "succeeds when the email is already in use via Web Widget" do
      ActionMailer::Base.perform_deliveries = true
      widget_user = FactoryBot.create(:unverified_end_user, via: 'Web Widget')
      email = widget_user.identities.email.first.value
      ActionMailer::Base.deliveries = []
      post :register, params: { user: {name: 'John Smith', email: email} }
      assert_redirected_to "#{@account.url}/registration/success?user%5Bname%5D=John+Smith", response.location
      assert_equal 1, ActionMailer::Base.deliveries.size
    end

    it "fails when the email is already in use with multibrand" do
      @account.stubs(has_multiple_active_brands?: true)
      2.times { post :register, params: { user: @user_params } }
      flash[:error].must_include "has multiple portals"
    end

    it "fails when the email has invalid formatting" do
      post :register, params: { user: @user_params.merge(email: 'i am not valid') }
      flash[:error].must_include "Invalid email address: i am not valid"
    end

    it "fails when the email is blacklisted" do
      @account.expects(:allows?).with(@user_params[:email]).returns(false)
      post :register, params: { user: @user_params }
      assert_match /email address #{@user_params[:email]} are not allowed/, flash[:error]
    end

    it "fails when the user is invalid" do
      post :register, params: { user: @user_params.merge(name: ' ') }
      flash[:error].must_include "Failed to create"
    end

    describe "when the user choose a language before signing in" do
      before do
        @english = translation_locales(:english_by_zendesk)
        @selected_language = translation_locales(:japanese)
        @account.stubs(:available_languages).returns([@english, @selected_language])
        @request.shared_session[:locale_id] = @selected_language.id
      end

      it "creates the user with the selected language and account is not starter" do
        Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
        post :register, params: { user: @user_params }
        assert_equal @selected_language, User.last.translation_locale
      end

      # ZD#346487
      it "creates the user with and empty locale if the account is a starter" do
        Subscription.any_instance.stubs(:plan_type).returns(SubscriptionPlanType.Small)
        post :register, params: { user: @user_params }
        assert_nil User.last.locale_id
      end

      it "does not create the user with the selected language if locale_id comes in the params" do
        post :register, params: { user: @user_params.merge(locale_id: @english) }
        assert_equal @english, User.last.translation_locale
      end
    end
  end

  describe "#verify_recaptcha" do
    it "uses recaptcha_rate_limited?" do
      @controller.expects(recaptcha_rate_limited?: false)
      assert @controller.verify_recaptcha
      @controller.expects(recaptcha_rate_limited?: true)
      refute @controller.verify_recaptcha
    end
  end

  describe "#request_oauth" do
    before do
      login(users(:minimum_admin))
      @account.stubs(:whitelists?).returns(@user_params[:email])
      @request.stubs(:ssl?).returns(true)
      @some_url = @account.url
    end

    describe "after registering with twitter handle" do
      before do
        post :register, params: { user: @user_params.merge(twitter_screen_name: "foobar"), return_to: "/somewhere" }
        @user = User.last
      end

      it "uses twitter readonly profile" do
        callback_url = @controller.send(:create_twitter_profile_user_url, @user)
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).with('twitter_readonly', callback_url, false).returns(@some_url)
        get :request_oauth
      end
    end

    describe "with HTTPFatalError when requesting the token" do
      before do
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).raises(Net::HTTPFatalError.new(503, "Service Temporarily Unavailable"))
        get :request_oauth
      end
      should_set_the_flash_to :error
    end

    describe "twitter being a timing out little piggy" do
      before do
        Zendesk::Auth::OauthDelegator.stubs(:request_oauth!).with { sleep 5; true } # rubocop:disable Lint/Sleep
        get :request_oauth
      end
      should_set_the_flash_to :error
    end

    describe "with Zendesk::Auth::InvalidProfileException when requesting the token" do
      before do
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).raises(Zendesk::Auth::InvalidProfileException.new("twitter_readonly"))
        get :request_oauth
      end
      should_set_the_flash_to :error
    end
  end
end
