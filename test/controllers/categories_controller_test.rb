require_relative "../support/test_helper"

SingleCov.covered!

describe CategoriesController do
  fixtures :categories

  before do
    @account = accounts(:minimum)
    @request.account = @account
    @category = categories(:minimum_category)
  end

  describe "with help center enabled and web portal restricted" do
    before do
      @account.settings.web_portal_state = :restricted
      @account.save!
      @account.stubs(:has_public_forums?).returns(true)
      activate_hc!
    end

    it "redirects to HelpCenter" do
      ContentMapping.create! do |cm|
        cm.account_id = @category.account_id
        cm.classic_object_type = "Legacy::Category"
        cm.classic_object_id = @category.id
        cm.hc_object_type = "Foo"
        cm.hc_object_id = 42
        cm.url = "/foo"
      end
      get :show, params: { id: @category.id }
      assert_redirected_to "https://minimum.zendesk-test.com/foo"
    end
  end

  describe "with help center disabled" do
    it "redirects to help center closed url" do
      get :show, params: { id: @category.id }
      assert_match %r{app/help-center-closed/}, @response.location
      assert_equal 301, @response.status.to_i
    end
  end
end
