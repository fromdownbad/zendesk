require_relative "../support/test_helper"
require "zendesk/inbound_mail"

SingleCov.covered! uncovered: 1

describe AuditEmailsController do
  include MailTestHelper
  fixtures :all

  with_options(controller: "audit_emails") do |request|
    request.should_route :get, "/audits/123/email.json", action: "show", id: "123", format: "json"
    request.should_route :get, "/audits/123/email.eml",  action: "show", id: "123", format: "eml"
  end

  before do
    @account = accounts(:minimum)
    @mail    = FixtureHelper::Mail.read("minimum_ticket.json")
    @eml     = FixtureHelper::Mail.raw("minimum_ticket.eml")
    @ticket  = process_mail(@mail, account: @account, eml_fixture: @eml).ticket

    assert_instance_of Ticket, @ticket

    @audit = @ticket.audits.last
  end

  as_an_anonymous_user do
    it "is unauthorized" do
      get :show, params: { id: @audit.id, format: "json" }
      assert_response :unauthorized
    end
  end

  as_an_end_user do
    it "is unauthorized" do
      get :show, params: { id: @audit.id, format: "json" }
      assert_response :forbidden
    end
  end

  as_an_agent do
    # e.g. "x-spam-status" from
    # @mail.headers.keys
    # # => ["subject", "from", "message-id", "to", "cc", "content-type", "received", "user-agent", "delivered-to", "x-spam-status", "references", "bcc"]
    let(:mail_and_json_x_header_pattern) { /\Ax-[a-z]+/i }
    # e.g. "X-Spam-Status:" from an .eml file
    let(:eml_x_header_pattern) { /^x-[a-zA-Z\-]*[a-zA-Z]:/i }

    before do
      assert @mail.headers.keys.any? { |key| key.match?(mail_and_json_x_header_pattern) }
      assert @eml.match?(eml_x_header_pattern)
    end

    describe "when requesting an audit email in JSON format" do
      it "responds with json scrubbed of all X-* headers" do
        get :show, params: { id: @audit.id, format: "json" }
        header_keys = JSON.parse(@response.body)["headers"].keys

        assert_equal "application/json", @response.content_type
        assert_match /^\{.*\}$/m, @response.body
        refute header_keys.any? { |header_key| header_key.match?(mail_and_json_x_header_pattern) }
      end

      describe "when json X-* header scrubbing has failed" do
        before do
          Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:json_without_x_headers).returns(nil)
        end

        it "responds with a 404" do
          get :show, params: { id: @audit.id, format: "json" }

          assert_equal "text/html", @response.content_type
          assert_response 404
        end
      end
    end

    describe "when requesting an audit email in eml format" do
      it "responds with eml scrubbed of all X-* headers" do
        get :show, params: { id: @audit.id, format: "eml" }

        assert_equal "message/rfc822", @response.content_type
        assert @response.body.include?("Message-ID: #{@mail.message_id}")
        refute @response.body.match?(eml_x_header_pattern)
      end

      describe "when eml X-* header scrubbing has failed" do
        before do
          Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:eml_without_x_headers).returns(nil)
        end

        it "responds with a 404" do
          get :show, params: { id: @audit.id, format: "eml" }

          assert_equal "text/html", @response.content_type
          assert_response 404
        end
      end
    end

    describe "when requesting an audit email in text format" do
      it "responds with eml scrubbed of all X-* headers" do
        get :show, params: { id: @audit.id, format: "text" }

        assert_equal "text/plain", @response.content_type
        assert @response.body.include?("Message-ID: #{@mail.message_id}")
        refute @response.body.match?(eml_x_header_pattern)
      end

      describe "when eml X-* header scrubbing has failed" do
        before do
          Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:eml_without_x_headers).returns(nil)
        end

        it "responds with a 404" do
          get :show, params: { id: @audit.id, format: "text" }

          assert_equal "text/html", @response.content_type
          assert_response 404
        end
      end
    end

    describe "when requesting an audit email in html format" do
      it "responds with html" do
        get :show, params: { id: @audit.id, format: "html" }

        assert_equal "text/html", @response.content_type
        assert_response :success
      end
    end

    it "responds with 404 with no email from storage returned" do
      Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:json).returns(nil)

      get :show, params: { id: @audit.id, format: "html" }
      assert_equal "text/html", @response.content_type
      assert_response 404
    end

    it "does not blow up with deleted ticket when the ticket id is not passed in" do
      @ticket.will_be_saved_by(@account.owner)
      @ticket.soft_delete!
      @audit.reload

      get :show, params: { id: @audit.id, format: "html" }
      assert_equal "text/html", @response.content_type
      assert_response 404
    end

    it "does not blow up with deleted ticket when the ticket id is passed in" do
      @ticket.will_be_saved_by(@account.owner)
      @ticket.soft_delete!
      @audit.reload

      get :show, params: { id: @audit.id, ticket_id: @ticket.nice_id, format: "html" }
      assert_equal "text/html", @response.content_type
      assert_response 404
    end

    it "does not blow up without a comment" do
      Audit.any_instance.stubs(:comment).returns(nil)

      get :show, params: { id: @audit.id, format: "html" }
      assert_equal "text/html", @response.content_type
      assert_match /<p class=\"eml\">.*?<\/p>/, @response.body
    end

    it "does not blow up with an archived ticket" do
      archive_and_delete(@ticket)

      get :show, params: { id: @audit.id, ticket_id: @ticket.nice_id, format: "html" }
      assert_equal "text/html", @response.content_type
      assert_response :success
    end

    it "does not blow up with a redacted ticket (no .eml file present)" do
      @ticket.comments.first.redact!("Microsoft Word")
      get :show, params: { id: @audit.id, format: "html" }
      assert_equal "text/html", @response.content_type
      assert_response :success
      refute_match /<a href=\"#source\" class=\"\" data-title-value=\"Source\">Source<\/a>/, @response.body
    end

    describe "cache control headers" do
      describe_with_arturo_disabled :email_audit_email_cache_control_header do
        it "are not present" do
          get :show, params: { id: @audit.id, format: "html" }
          assert_equal 'max-age=0, private, must-revalidate', @response.headers['Cache-Control']
        end
      end

      describe_with_arturo_enabled :email_audit_email_cache_control_header do
        it "are present" do
          get :show, params: { id: @audit.id, format: "html" }
          assert_equal 'max-age=3600, private', @response.headers['Cache-Control']
        end
      end
    end

    describe "render Reply-To field" do
      let(:email_json_with_same_originator_addresses) { FixtureHelper::Mail.read("email_with_same_originator_addresses.json") }
      let(:email_eml_with_same_originator_addresses) { FixtureHelper::Mail.raw("email_with_same_originator_addresses.eml") }
      let(:email_json_with_different_originator_addresses) { FixtureHelper::Mail.read("email_with_different_originator_addresses.json") }
      let(:email_eml_with_different_originator_addresses) { FixtureHelper::Mail.raw("email_with_different_originator_addresses.eml") }

      before { FactoryBot.create(:user, email: 'agent@example.com', account: @account, roles: Role::AGENT.id) }

      describe "when an incoming email contains non-matching mail.from and reply-to" do
        before do
          @reply_to = email_json_with_different_originator_addresses.reply_to
          ticket = process_mail(email_json_with_different_originator_addresses, account: @account, eml_fixture: email_eml_with_different_originator_addresses).ticket
          @audit_with_different_originator_addresses = ticket.audits.last
        end

        it "should display Reply-To label with address same as the reply-to address" do
          get :show, params: { id: @audit_with_different_originator_addresses.id, format: "html" }

          assert_match /<label>Reply-to:<\/label>\n          <div class=\"value\">&quot\;#{@reply_to.name}&quot\; &lt\;#{@reply_to.address}&gt\;<\/div>/, @response.body
        end
      end

      describe "when an incoming email contains matching mail.from and reply-to" do
        before do
          ticket = process_mail(email_json_with_same_originator_addresses, account: @account, eml_fixture: email_eml_with_same_originator_addresses).ticket
          @audit_with_same_originator_addresses = ticket.audits.last
        end

        it "should not display Reply-To field" do
          get :show, params: { id: @audit_with_same_originator_addresses.id, format: "html" }

          refute_match /<label>Reply-To:<\/label>\n/, @response.body
        end
      end

      describe "when reply-to header is not present in the incoming email" do
        it "should not display Reply-To field" do
          get :show, params: { id: @audit.id, format: "html" }

          refute_match /<label>Reply-To:<\/label>\n/, @response.body
        end
      end

      describe_with_arturo_disabled :email_reply_to_vulnerability_surface_reply_to_address do
        describe "when an incoming email contains non-matching mail.from and reply-to" do
          before do
            ticket = process_mail(email_json_with_different_originator_addresses, account: @account, eml_fixture: email_eml_with_different_originator_addresses).ticket
            @audit_with_different_originator_addresses = ticket.audits.last
          end

          it "should not display Reply-To field" do
            get :show, params: { id: @audit_with_different_originator_addresses.id, format: "html" }
            refute_match /<label>Reply-To:<\/label>\n/, @response.body
          end
        end

        describe "when an incoming email contains matching mail.from and reply-to" do
          before do
            ticket = process_mail(email_json_with_same_originator_addresses, account: @account, eml_fixture: email_eml_with_same_originator_addresses).ticket
            @audit_with_same_originator_addresses = ticket.audits.last
          end

          it "should not display Reply-To field" do
            get :show, params: { id: @audit_with_same_originator_addresses.id, format: "html" }

            refute_match /<label>Reply-To:<\/label>\n/, @response.body
          end
        end

        describe "when reply-to header is not present in the incoming email" do
          it "should not display Reply-To field" do
            get :show, params: { id: @audit.id, format: "html" }

            refute_match /<label>Reply-To:<\/label>\n/, @response.body
          end
        end
      end
    end
  end
end
