require_relative "../support/test_helper"

SingleCov.covered!

describe AutomaticAnswersEmbedController do
  fixtures :tickets, :users, :accounts, :brands, :ticket_deflections

  let(:account) { accounts(:minimum) }
  let(:article_id) { 2233445566 }
  let(:ticket) { tickets(:minimum_1) }
  let(:brand) { brands(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:article_content) { { id: 1, title: 'title', html_url: 'http://whatever', body: 'This is so rad bro.'} }
  let(:ticket_deflection) { ticket_deflections(:minimum_ticket_deflection) }
  let(:article_info) { { 'article_id' => article_id, 'score' => 0.192819, 'locale' => 'en-us' } }
  let(:ticket_deflection_article) do
    TicketDeflectionArticle.build_from_deflection_and_article(ticket_deflection, article_info)
  end
  let(:base_time) { Time.utc(2016, 3, 31) }
  let(:token) { stub('token', ticket: ticket, value: 'CRC32VALUE') }
  let(:decrypted_auth_token) do
    {
      account_id: account.id,
      user_id: ticket.requester_id,
      ticket_id: ticket.nice_id,
      articles: [article_id.to_i],
      token: token.value,
      exp: 30.days.from_now.to_i
    }
  end
  let(:encrypted_auth_token) { JWT.encode(decrypted_auth_token, AUTOMATIC_ANSWERS_JWT_SECRET) }
  let(:statsd_client_stub) { stub_for_statsd }

  before do
    Timecop.freeze(base_time)
    Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:default_locale).returns('en-us')
    Zendesk::HelpCenter::InternalApiClient.any_instance.
      stubs(:fetch_articles_for_ids_and_locales).
      with([{'article_id' => article_id, 'locale' => 'en-us'}]).
      returns([article_content])

    ticket_deflection.ticket_deflection_articles.destroy_all
    ticket_deflection_article.save!
    @request.account = @current_account = accounts(:minimum)
    @controller.stubs(:current_brand).returns(@current_account.brands.first)
  end

  describe 'routing' do
    with_options(controller: 'automatic_answers_embed') do |request|
      request.should_route :get, "/requests/automatic-answers/embed/ticket/fetch",
        action: 'fetch_ticket', format: 'json'
    end
  end

  describe 'GET /fetch_ticket' do
    describe 'success' do
      let(:title_full) do
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ' \
        'tristique quis nisi a lobortis. Proin et malesuada orci, ut ' \
        'malesuada risus malesuad CHARACTERS THAT WILL BE TRUNCATED.'
      end
      let(:title_truncated) do
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ' \
        'tristique quis nisi a lobortis. Proin et malesuada orci, ut ' \
        'malesuada risus malesuad...'
      end
      let(:sample_body) do
        {
          'ticket' => {
            'nice_id' => ticket.nice_id,
            'title' => title_truncated,
            'status_id' => ticket.status_id,
            'is_solved_pending' => ticket.ticket_deflection.solved_pending?
          }
        }
      end

      before do
        ticket.subject = title_full
        ticket.will_be_saved_by user
        ticket.save

        get :fetch_ticket, params: { auth_token: encrypted_auth_token }
      end

      it 'returns a success response' do
        assert_response :success
      end

      it 'returns a valid json response' do
        assert_equal sample_body, JSON.parse(response.body)
      end

      it 'returns a ticket title truncated to 150 characters' do
        assert_equal title_truncated, JSON.parse(response.body)['ticket']['title']
      end

      it 'returns a response with Access-Control-Allow-Origin: *' do
        assert_equal '*', response.headers['Access-Control-Allow-Origin']
      end
    end

    describe 'lookup deflection' do
      it 'falls back to ticket.ticket_deflection to be backward compatible' do
        get :fetch_ticket, params: { auth_token: encrypted_auth_token }
        assert_equal false, JSON.parse(response.body)['ticket']['is_solved_pending']
      end

      describe 'when deflection_id is provided' do
        let(:another_deflection) { ticket_deflections(:solved_pending) }
        let(:decrypted_auth_token) do
          {
            account_id: account.id,
            user_id: ticket.requester_id,
            ticket_id: ticket.nice_id,
            deflection_id: another_deflection.id,
            articles: [article_id.to_i],
            token: token.value,
            exp: 30.days.from_now.to_i
          }
        end

        it 'finds deflection with the provided deflection_id' do
          ticket.expects(:ticket_deflection).never

          get :fetch_ticket, params: { auth_token: encrypted_auth_token }
          assert(JSON.parse(response.body)['ticket']['is_solved_pending'])
        end
      end
    end

    describe 'update article clicked_at' do
      let(:clicked_time) { Time.now }

      before { Timecop.freeze(Time.now) }

      it 'records article clicked_at if article_id matches a deflected article id' do
        get :fetch_ticket, params: { auth_token: encrypted_auth_token, article_id: article_id }
        assert_equal clicked_time, ticket_deflection_article.reload.clicked_at
      end

      it 'does not record article clicked_at if article_id does not match a deflected article id' do
        get :fetch_ticket, params: { auth_token: encrypted_auth_token, article_id: 9999 }

        ticket_deflection_article.expects(:clicked_at).never
      end

      it 'returns success when article_id not provided' do
        get :fetch_ticket, params: { auth_token: encrypted_auth_token, article_id: nil }
        assert_response :success
      end
    end

    describe 'logging' do
      before do
        AutomaticAnswersEmbedController.any_instance.stubs(:statsd_client).returns(statsd_client_stub)
      end

      it 'logs when the embed fetches a ticket' do
        statsd_client_stub.expects(:increment).
          with('ticket_fetched', tags: ['subdomain:minimum', 'channel:embeddable'])

        get :fetch_ticket, params: { auth_token: encrypted_auth_token }
      end

      describe "when a 'mobile' query param is present" do
        it 'adds a tag to indicate if the channel device was mobile' do
          statsd_client_stub.expects(:increment).
            with('ticket_fetched', tags: ['subdomain:minimum', 'channel:embeddable', 'mobile:true'])

          get :fetch_ticket, params: { auth_token: encrypted_auth_token, mobile: true }
        end
      end
    end

    describe 'failure' do
      describe 'when auth_token decoding fails' do
        it 'returns a forbidden response' do
          get :fetch_ticket, params: { auth_token: 'INV@LID.JWT.T0KEN' }

          assert_response :forbidden
        end
      end

      describe 'when article not found for the given id' do
        it 'return 200 success' do
          get :fetch_ticket, params: { auth_token: encrypted_auth_token, article_id: 9999 }
          assert_response :success
        end
      end
    end
  end

  describe 'POST /solve_ticket' do
    let(:subdomain) { account.subdomain }
    let(:ticket_deflector) { mock('ticket_deflector') }
    let(:mobile) { true }

    before do
      ticket_deflector.
        stubs(:solve).
        returns(true)

      TicketDeflector.
        stubs(:new).
        with(account: account, deflection: ticket.ticket_deflection, resolution_channel_id: ViaType.WEB_WIDGET).
        returns(ticket_deflector)
    end

    it 'calls TicketDeflector to solve ticket' do
      ticket_deflector.
        expects(:solve).
        with(
          has_entries(
            article_id: as_id(article_id),
            mobile: mobile
          )
        )

      post :solve_ticket, params: { auth_token: encrypted_auth_token, article_id: article_id, mobile: mobile }
    end

    describe 'authentication behaviour' do
      it 'returns a response with Access-Control-Allow-Origin: *' do
        post :solve_ticket, params: { auth_token: encrypted_auth_token, article_id: article_id, mobile: mobile }

        assert_equal '*', response.headers['Access-Control-Allow-Origin']
      end
    end

    describe 'allowed CSRF controller actions' do
      it 'includes the solve_ticket action' do
        post :solve_ticket, params: { auth_token: encrypted_auth_token, article_id: article_id, mobile: mobile }

        assert_includes @controller.class::VERIFIED_CSRF_POST_ACTIONS, @controller.action_name
      end
    end

    it 'responds with a success code' do
      post :solve_ticket, params: { auth_token: encrypted_auth_token, article_id: article_id, mobile: mobile }

      assert_response :success
    end
  end

  describe 'POST /reject_article' do
    let(:reason_not_related_to_question) { TicketDeflectionArticle::REASON_RELATED_NOT_ANSWERED }

    describe 'authentication behaviour' do
      it 'returns a response with Access-Control-Allow-Origin: *' do
        post :reject_article, params: { auth_token: encrypted_auth_token, article_id: article_id, reason: reason_not_related_to_question }

        assert_equal '*', response.headers['Access-Control-Allow-Origin']
      end
    end

    describe 'given missing data' do
      describe 'when the article_id is not supplied' do
        before do
          post :reject_article, params: { auth_token: encrypted_auth_token }
        end

        it 'responds with unprocessable_entity' do
          assert_response :unprocessable_entity
        end
      end

      describe 'when the reason is not supplied' do
        before do
          post :reject_article, params: { auth_token: encrypted_auth_token, article_id: article_id }
        end

        it 'responds with unprocessable_entity' do
          assert_response :unprocessable_entity
        end
      end
    end

    describe 'given extra data' do
      before do
        post :reject_article, params: { auth_token: encrypted_auth_token, extra: 123 }
      end

      it 'responds with unprocessable_entity' do
        assert_response :unprocessable_entity
      end
    end

    describe 'given non-existing irrelevant reason' do
      before do
        post :reject_article, params: { auth_token: encrypted_auth_token, article_id: article_id, reason: 16 } # reason is tinyint(4)
      end

      it 'responds with unprocessable_entity' do
        assert_response :unprocessable_entity
      end
    end

    describe 'allowed CSRF controller actions' do
      it 'includes the reject_article action' do
        post :reject_article, params: { auth_token: encrypted_auth_token, article_id: article_id, reason: reason_not_related_to_question }

        assert_includes @controller.class::VERIFIED_CSRF_POST_ACTIONS, @controller.action_name
      end
    end
  end
end
