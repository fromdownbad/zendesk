require_relative "../../support/test_helper"
require_relative "../../support/users_controller_update_session_locale_test_helper"
require 'zendesk/radar_factory'

SingleCov.covered! uncovered: 45

describe People::UsersController do
  include UsersControllerUpdateSessionLocaleTestHelper

  fixtures :all
  should_be_unauthorized_when_not_logged_in(
    [
      :index, :new, :create, :edit, :update, :destroy, :revert,
      [:get, :verify, {id: 1}],
      [:post, :assume, {id: 1}],
      [:put, :update_notes, {id: 1}],
      [:post, :resend_welcome_email, {id: 1}]
    ]
  )

  should_include_module Zendesk::LotusEmbeddable

  before do
    @request.account = @account = accounts(:with_groups)
    @with_groups_end_user = users('with_groups_end_user')
    @with_groups_admin = users(:with_groups_admin)
    ActionMailer::Base.deliveries = []
    @request.account.settings.prefer_lotus = true
    @request.account.settings.save!
    Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(true)
  end

  with_options(controller: "people/users") do |request|
    request.should_route :get, "users/1", action: :show, id: 1
    request.should_route :get, "organizations/1/users", action: :index, organization_id: 1
    request.should_route :get, "groups/1/users", action: :index, group_id: 1
    request.should_route :get, "users", action: :index
    request.should_route :get, "users/1/edit", action: :edit, id: 1
    request.should_route :post, "users/1/assume", action: :assume, id: 1
    request.should_route :get, "users/revert", action: :revert
    request.should_route :post, "users/revert", action: :revert
    request.should_route :post, "users/1/merge_complete", action: :merge_complete, id: 1
    request.should_route :get,  "/users/autocomplete", action: 'autocomplete'
    request.should_route :post, "/users/find_or_create.json", action: 'find_or_create', format: 'json'
    request.should_route :get,  "/users/multivalue_autocomplete", action: 'multivalue_autocomplete'
    request.should_route :get,  "/users/multivalue_autocomplete.json", action: 'multivalue_autocomplete', format: 'json'
    request.should_route :post, "/users/select_bulk_action", action: 'select_bulk_action'
    request.should_route :get, "/users/1/profiles_with_the_same_phone_number.json", action: 'profiles_with_the_same_phone_number', id: 1, format: 'json'
  end

  it "recognizes find_or_create" do
    assert_recognizes({controller: "people/users", action: "find_or_create", format: "json"}, path: "/users/find_or_create.json", method: :post)
  end

  describe "#index" do
    before do
      @account = accounts(:minimum)
      @request.account = @account
    end

    describe "for a restricted agent" do
      before do
        @agent = users(:minimum_agent)
        @agent.restriction_id = RoleRestrictionType.ORGANIZATION
        @agent.organization = organizations(:minimum_organization1)
        @agent.save!

        login(@agent)
      end

      it "allows access to users of own organization" do
        get :index, params: { organization_id: organizations(:minimum_organization1).id }
        assert_response :success
      end

      it "denys access to users of other organization" do
        get :index, params: { organization_id: organizations(:minimum_organization2).id }
        assert_response :forbidden
      end
    end

    describe "for an admin" do
      before do
        @admin = users(:minimum_admin)
        login(@admin)
        @request.account.stubs(:has_user_and_organization_tags?).returns(true)
      end

      it "renders the index template" do
        get :index
        assert_response :success
        assert_template 'index'
      end

      describe "filtering by end users" do
        it "applies span styling to the 'end-users' option" do
          get :index, params: { role: '0' }
          assert response.body.include?("<div id=\"people-browse\">... or browse <span style=\"color:#888;\">end users</span><span class=\"delim\">|</span><a href=\"/users?role%5B%5D=4&amp;role%5B%5D=2\">agents</a><span class=\"delim\">|</span><a href=\"/users?role=2\">admins</a><span class=\"delim\">|</span><a href=\"/groups\">groups</a><span class=\"delim\">|</span><a href=\"/organizations\">organizations</a><span class=\"delim\">|</span><a href=\"/people/tags\">tags</a></div>")
        end
      end

      describe "filtering by agents" do
        it "applies span styling to the 'agents' option" do
          get :index, params: { role: ["4", "2"] }
          assert response.body.include?("<div id=\"people-browse\">... or browse <a href=\"/users?role=0\">end users</a><span class=\"delim\">|</span><span style=\"color:#888;\">agents</span><span class=\"delim\">|</span><a href=\"/users?role=2\">admins</a><span class=\"delim\">|</span><a href=\"/groups\">groups</a><span class=\"delim\">|</span><a href=\"/organizations\">organizations</a><span class=\"delim\">|</span><a href=\"/people/tags\">tags</a></div>")
        end
      end

      describe "filtering by admins" do
        it "applies span styling to the 'admins' option" do
          get :index, params: { role: '2' }
          assert response.body.include?("<div id=\"people-browse\">... or browse <a href=\"/users?role=0\">end users</a><span class=\"delim\">|</span><a href=\"/users?role%5B%5D=4&amp;role%5B%5D=2\">agents</a><span class=\"delim\">|</span><span style=\"color:#888;\">admins</span><span class=\"delim\">|</span><a href=\"/groups\">groups</a><span class=\"delim\">|</span><a href=\"/organizations\">organizations</a><span class=\"delim\">|</span><a href=\"/people/tags\">tags</a></div>")
        end
      end

      describe "fuzzed inputs" do
        it "returns empty set for nil results" do
          get :index, params: { permission_set: 'blahblah' }
          assert_response :success
        end
      end

      describe "Organization" do
        before do
          @organization = organizations(:minimum_organization1)
          @user = users(:minimum_end_user)
        end

        it "Finds users in organization via id" do
          get :index, params: { organization: @organization.id }
          assert_template 'index'
          assert_select "a[href='#{user_path(@user)}']"
        end

        it "Finds users in organization via name" do
          get :index, params: { organization: @organization.name }
          assert_template 'index'
          assert_select "a[href='#{user_path(@user)}']"
        end

        it "returns 15 records when requested via a non API call" do
          Zendesk::Users::Finder.expects(:new).with(@account, @admin, {'organization_id' => @organization.id.to_s}, 1, 15).returns(stub(users: WillPaginate::Collection.new(1, 1, 0)))

          get :index, params: { organization_id: @organization.id }
        end

        it "returns 100 user records when requested via an API call" do
          Zendesk::Users::Finder.expects(:new).with(@account, @admin, {'organization_id' => @organization.id.to_s}, 1, 100).returns(stub(users: WillPaginate::Collection.new(1, 1, 0)))

          accept :json
          get :index, params: { organization_id: @organization.id }
        end
      end

      describe "offset-based pagination" do
        before do
          5.times do |time|
            @admin.account.users.create!(name: "Testy McTestface the #{time}")
          end
        end

        it "does not allow pagination past the max allowed page" do
          get :index, params: { page: 1001, format: "json" }
          assert_response :not_found
        end

        it "does not display the pagination numbers besides the current page" do
          get :index
          # ONLY the next_page link
          assert_select ".pagination a", count: 1
        end

        it "displays an information panel with information about pagination limits" do
          stub_const(People::UsersController, :OFFSET_PAGINATION_MAX_OFFSET, 30) do
            get :index, params: { page: 2 }
            assert response.body.include?(I18n.t("txt.admin.views.people.search.index.max_page_info"))
          end
        end
      end

      describe "total entries" do
        let(:max_offset) { 30 }

        describe "when number of users is below the max" do
          before do
            assert @account.users.count < max_offset
          end

          it "displays the actual count using a query limited to the max" do
            stub_const(People::UsersController, :OFFSET_PAGINATION_MAX_OFFSET, max_offset) do
              assert_sql_queries(1, Regexp.new(Regexp.escape("SELECT COUNT(count_column) FROM (SELECT  1 AS count_column FROM `users` WHERE `users`.`account_id` = #{@account.id} AND `users`.`is_active` = 1 AND `users`.`roles` IN (0, 4, 2) LIMIT #{max_offset}) subquery_for_count"))) do
                get :index, params: { page: 2 }
                assert response.body.include?("(#{@account.users.count})")
              end
            end
          end
        end

        describe "when number of users is equal to or greater than the max" do
          before do
            max_offset.times do |time|
              @account.users.create!(name: "Testy McTestface the #{time}")
            end

            assert @account.users.count >= max_offset
          end

          it "displays the max using a query limited to the max" do
            stub_const(People::UsersController, :OFFSET_PAGINATION_MAX_OFFSET, max_offset) do
              assert_sql_queries(1, Regexp.new(Regexp.escape("SELECT COUNT(count_column) FROM (SELECT  1 AS count_column FROM `users` WHERE `users`.`account_id` = #{@account.id} AND `users`.`is_active` = 1 AND `users`.`roles` IN (0, 4, 2) LIMIT #{max_offset}) subquery_for_count"))) do
                get :index, params: { page: 2 }
                assert response.body.include?("(#{max_offset}+)")
              end
            end
          end
        end
      end
    end

    describe "Group" do
      before do
        @group = groups(:minimum_group)
        @user  = users(:minimum_agent)
        @admin = users(:minimum_admin)
        login(@admin)
      end

      it "Finds agents in group via id as group" do
        get :index, params: { group: @group.id }
        assert_response :success
        assert_template 'index'
        assert_select "a[href='#{user_path(@user)}']"
      end

      it "Finds agents in group via id as group_id" do
        get :index, params: { group_id: @group.id }
        assert_response :success
        assert_template 'index'
        assert_select "a[href='#{user_path(@user)}']"
      end

      it "Finds agents in group via name" do
        get :index, params: { group: @group.name }
        assert_template 'index'
        assert_select "a[href='#{user_path(@user)}']"
      end

      it "returns 15 records when requested via a non API call" do
        Zendesk::Users::Finder.expects(:new).with(@account, @admin, {'group_id' => @group.id.to_s}, 1, 15).returns(stub(users: WillPaginate::Collection.new(1, 1, 0)))

        get :index, params: { group_id: @group.id }
      end

      it "returns 100 user records when requested via an API call" do
        Zendesk::Users::Finder.expects(:new).with(@account, @admin, {'group_id' => @group.id.to_s}, 1, 100).returns(stub(users: WillPaginate::Collection.new(1, 1, 0)))

        accept :json
        get :index, params: { group_id: @group.id }
      end
    end

    describe "search results" do
      describe "agent logged in" do
        it "shoulds include admins in the search result" do
          @request.account = accounts(:with_groups)
          logout
          login('with_groups_admin')
          get :index, params: { query: "admin", format: "json" }
          result = JSON.parse(@response.body)
          assert_equal ["with_groups_admin"], result.collect { |admin| admin["name"] }
        end
      end
    end
  end

  describe "#merge_complete" do
    let(:user) { users(:with_groups_admin) }

    before do
      login(user)
      post :merge_complete, params: { id: user.id }
    end

    it "redirects to lotus user page" do
      assert_redirected_to "/agent/users/#{user.id}"
    end
  end

  describe "#new" do
    before do
      @request.account = accounts(:minimum)
      login('minimum_admin')
    end

    it 'returns 410 gone' do
      get :new
      assert_response :gone
    end
  end

  describe "#show" do
    describe "with groups admin" do
      before do
        login('with_groups_admin')
        @user = users(:with_groups_end_user)
        get :show, params: { id: @user.id }
      end

      should_redirect_to("lotus user profile") { "/agent/users/#{@with_groups_end_user.id}" }
    end

    describe "when redirected from UserMergeController" do
      before do
        login('minimum_agent')
        @user = users(:minimum_agent)
      end

      it "does not raise an exception" do
        get :show, params: { id: @user.id, loser_id: users(:minimum_end_user) }
      end
    end

    describe "owner" do
      before do
        @account = accounts(:minimum)
        @request.account = @account
        @owner = @account.owner
        assert @owner.is_account_owner?
        login(@owner)
      end

      it "gets redirected to owners lotus page" do
        @uea = UnverifiedEmailAddress.create(user: @owner, email: 'unverifiable@example.com')
        get :show, params: { id: @owner.id }
        assert_redirected_to "/agent/users/#{@owner.id}"
      end

      it "gets redirected to user lotus page" do
        @user = users(:minimum_end_user)
        @uea = UnverifiedEmailAddress.create(user: @user, email: 'verifiable@example.com')
        get :show, params: { id: @user.id }
        assert_redirected_to "/agent/users/#{@user.id}"
      end
    end

    describe "end-user" do
      describe_with_arturo_enabled :disable_legacy_people_users_id_page do
        before do
          @controller.stubs(:current_brand).returns(@account.brands.first)
          login('with_groups_end_user')
        end

        describe 'with help_center enabled' do
          before do
            @account.stubs(:help_center_enabled?).returns(true)
            get :show, params: { id: @with_groups_end_user.id }
          end

          it ('gets redirected to /hc/profiles page') { assert_redirected_to "/hc/profiles/#{@with_groups_end_user.id}" }
        end

        describe 'without help center' do
          before do
            @account.stubs(:help_center_enabled?).returns(false)
            get :show, params: { id: @with_groups_end_user.id }
          end

          it ('responds with 404') { assert_response :not_found }
        end
      end
    end
  end

  describe '#autocomplete' do
    before do
      @end_user = users(:with_groups_end_user)
      @end_user.name = %(all",'funked_up)
      @end_user.save!

      ZendeskSearch::Client.any_instance.
        stubs(:autocomplete_users).
        returns('count' => 1, 'results' => ['id' => @end_user.id])
    end

    describe 'as an end-user' do
      before do
        login('with_groups_end_user')
        get :autocomplete
      end

      it('responds with forbidden') { assert_response :forbidden }
    end

    describe 'as an unrestricted agent' do
      before do
        login('with_groups_agent1')
        get :autocomplete, params: { name: 'user' }
      end

      it('responds with ok') { assert_response :ok }

      it 'includes all matching end-users with parseable addresses' do
        json = JSON.parse(@response.body)
        assert_equal [%(#{@end_user.name.to_json} <#{@end_user.email}>)], json
        assert Zendesk::Mail::Address.parse(json[0])
      end
    end

    describe 'as an agent restricted by organization' do
      before do
        login('with_groups_agent_organization_restricted')
        get :autocomplete, params: { name: 'user' }
      end

      it('responds with ok') { assert_response :ok }

      it 'includes only end-users in the same organization' do
        json = JSON.parse(@response.body)
        assert_equal [], json
      end
    end
  end

  describe "#choose" do
    before do
      @japanese = translation_locales(:japanese)
    end

    it "allows anyone to access this action, regardless of open or closed portal or being logged in" do
      assert_not_equal @japanese.id, shared_session[:locale_id]
      Account.any_instance.stubs(:has_public_forums?).returns(false)
      post :choose_locale, params: { id: @japanese.id }
      assert_equal @japanese.id, shared_session[:locale_id].to_i
    end

    it "redirects back to the home path when no referer is given" do
      post :choose_locale, params: { id: @japanese.id, locale: 88 }
      assert_equal "https://#{@request.host}/home", @response.location
    end

    it "redirects back to referer URL minus locale parameter" do
      set_header("REFERER", "/hello?locale=22")
      post :choose_locale, params: { id: @japanese.id, locale: 88 }
      assert_equal "https://#{@request.host}/hello?", @response.location

      set_header("REFERER", "/hello?foo=bar&moo=hoo&locale=22&goo=too")
      post :choose_locale, params: { id: @japanese.id, locale: 88 }
      assert_equal "https://#{@request.host}/hello?foo=bar&moo=hoo&goo=too", @response.location
    end

    it 'updates the user cache_key' do
      user = users(:minimum_end_user)
      @request.account = user.account
      login(user)
      old_key = user.cache_key
      post :choose_locale, params: { id: @japanese.id, locale: 88 }
      assert_not_equal old_key, user.reload.cache_key
    end
  end

  describe '#profiles_with_the_same_phone_number' do
    before do
      @user = users(:minimum_agent)
      @user.phone = '+15556439870'
      @user.save!
      @request.account = @user.account
      login('minimum_agent')
    end

    it('responds with forbidden when user is not an agent') do
      login('with_groups_end_user')
      get :profiles_with_the_same_phone_number, params: { id: @user.id, format: 'json' }
      assert_response :forbidden
    end

    it 'returns empty response when there is no other user with the same phone number' do
      get :profiles_with_the_same_phone_number, params: { id: @user.id, format: 'json' }
      assert_nil JSON.parse(@response.body)
    end

    it 'returns one random user with the same phone number' do
      other_user1 = FactoryBot.create(:user, account: @user.account, phone: '+15556439870')
      other_user2 = FactoryBot.create(:user, account: @user.account, phone: '+15556439870')

      get :profiles_with_the_same_phone_number, params: { id: @user.id, format: 'json' }
      assert_equal     other_user1.id, JSON.parse(@response.body)['id']
      assert_not_equal other_user2.id, JSON.parse(@response.body)['id']
    end

    it 'returns one random user with the same phone number with priority to direct-line numbers' do
      other_user1 = FactoryBot.create(:user, account: @user.account, phone: '+15556439870')
      other_user2 = FactoryBot.create(:user, account: @user.account, phone: '+15556439870')

      other_user1.update_phone_and_identity(other_user1, true)               # enable direct line
      get :profiles_with_the_same_phone_number, params: { id: @user.id, format: 'json' }
      first_response = JSON.parse(@response.body)['id']

      other_user1.reload.update_phone_and_identity(other_user1.phone, false) # disable direct line
      other_user2.reload.update_phone_and_identity(other_user2.phone, true)  # enable direct line
      get :profiles_with_the_same_phone_number, params: { id: @user.id, format: 'json' }
      second_response = JSON.parse(@response.body)['id']

      assert_not_equal first_response, second_response
    end

    it 'returns a user with the same phone number regardless of the format' do
      @user.update_attribute(:phone, '+1(555) 643-9870')
      other_user1 = FactoryBot.create(:user, account: @user.account, phone: '+15556439870')

      get :profiles_with_the_same_phone_number, params: { id: @user.id, format: 'json' }
      assert_equal other_user1.id, JSON.parse(@response.body)['id']
    end

    it 'returns only active user with the same phone number' do
      other_user1 = FactoryBot.create(:user, account: @user.account, phone: '+15556439870')
      other_user2 = FactoryBot.create(:user, account: @user.account, phone: '+15556439870')
      other_user2.is_active = false
      other_user2.save!

      get :profiles_with_the_same_phone_number, params: { id: @user.id, format: 'json' }
      assert_equal other_user1.id, JSON.parse(@response.body)['id']
    end
  end

  describe "An end user accessing #show" do
    before { login('with_groups_end_user') }

    describe "with letters in the id, xml formatted" do
      before do
        @controller.stubs(:current_brand).returns(@account.brands.first)
        get :show, params: { id: "abcd", format: "xml" }
      end

      it "gets a 404" do
        assert_response :not_found
      end
    end

    describe "with own id, xml formatted" do
      before { get :show, params: { id: users(:with_groups_end_user).id, format: "xml" } }

      it "does not see agent only fields" do
        assert_no_match(/<details/, @response.body)
        assert_no_match(/<notes/, @response.body)
      end
    end

    describe "with own id, json formatted" do
      before { get :show, params: { id: users(:with_groups_end_user).id, format: "json" } }

      it "does not see agent only fields" do
        assert_no_match(/"details":/, @response.body)
        assert_no_match(/"notes":/, @response.body)
      end
    end
  end

  describe "An admin" do
    before { login('with_groups_admin') }

    describe "view /show/:id" do
      before do
        @user = users(:with_groups_end_user)
        @user.update_attribute(:organization_id, organizations(:group_organization).id)
      end

      describe "as XML" do
        it "don't care redirects to lotus" do
          get :show, params: { id: @user.id, format: "xml" }
          assert_redirected_to "/agent/users/#{@user.id}"
        end

        it "when requesting identities, it don't care, redirects to lotus" do
          get :show, params: { id: @user.id, include: ["identities"], format: "xml" }
          assert_redirected_to "/agent/users/#{@user.id}"
        end

        it "when requesting identities and organization, it still don't care and still redirects to lotus" do
          get :show, params: { id: @user.id, include: ["identities", "organization"], format: "xml" }
          assert_redirected_to "/agent/users/#{@user.id}"
        end
      end

      describe "as JSON" do
        it "don't care redirects to lotus" do
          get :show, params: { id: @user.id, format: "json" }
          assert_redirected_to "/agent/users/#{@user.id}"
        end

        it "when requesting identities, it don't care redirects to lotus" do
          get :show, params: { id: @user.id, include: ["identities"], format: "json" }
          assert_redirected_to "/agent/users/#{@user.id}"
        end

        it "when requesting identities and organization, it still don't care and redirects to lotus" do
          get :show, params: { id: @user.id, include: ["identities", "organization"], format: "json" }
          assert_redirected_to "/agent/users/#{@user.id}"
        end
      end
    end
  end

  describe "tabs, as an end-user" do
    before do
      @user = users(:with_groups_end_user)
      login('with_groups_end_user')
    end

    it "renders tickets in the ticket tab" do
      get :show, params: { id: @user.id, select: 'tickets' }
      assert_response :ok
      assert_select '.linked', 4
    end

    it "renders ccs in the ccs tab" do
      tickets(:with_groups_1).requester = nil
      Collaboration.create!(user: @user, ticket: tickets(:with_groups_1))
      get :show, params: { id: @user.id, select: 'ccs' }

      assert_select '.item', 1
    end
  end

  describe "#edit" do
    it "renders not an email form field" do
      login('with_groups_admin')
      get :edit, params: { id: @with_groups_end_user.id }
      assert_select 'input#user_email[type=text]', count: 0
    end

    it "autocompletes for many organizations" do
      login('with_groups_admin')
      Account.any_instance.stubs(:many_organizations?).returns(true)
      get :edit, params: { id: @with_groups_end_user.id }
      assert_select 'input#user_organization_name[type=text]'
    end

    describe "user tags" do
      before do
        login('with_groups_admin')
      end

      it "is visible if the account has them enabled in arturo" do
        get :edit, params: { id: @with_groups_end_user.id }
        assert_response :success
        assert_select 'input#user-tags-input'
        assert_select 'label[for=user_organization]'
      end

      it "is not visible when not enabled in arturo" do
        @request.account.settings.has_user_tags = false
        @request.account.settings.save!

        get :edit, params: { id: @with_groups_end_user.id }
        assert_response :success
        assert_select 'input#user-tags-input', count: 0
        assert_select 'label[for=user_organization]'
      end
    end

    it "uses a drop down for few organizations" do
      login('with_groups_admin')
      Account.any_instance.stubs(:many_organizations?).returns(false)
      get :edit, params: { id: @with_groups_end_user.id }
      assert_select 'select#user_organization_id'
    end

    describe "with a Zendesk Voice incoming phone number" do
      describe "present on the account" do
        before do
          Account.any_instance.stubs(:phone_numbers).returns([mock])
          login('with_groups_admin')
        end

        it "shows instructional text for agents with regards to agent phone number routing" do
          get :edit, params: { id: @with_groups_admin.id }
          assert_response :success
          assert_select 'p#agent_voice_routing_instructions'
        end

        # should "never show instructional text for agents with regards to agent phone number routing if the agent's role does not permit it" do
        #   @with_groups_admin.abilities.stubs(:can?).with(:accept, ::Voice::Call).returns(false)
        #   get :edit, :id => @with_groups_admin.id
        #   assert_response :success
        #   assert_select 'p#agent_voice_routing_instructions', count: 0
        # end
      end

      describe "missing from the account" do
        before do
          Account.any_instance.stubs(:phone_numbers).returns([])
          login('with_groups_admin')
        end

        it "nevers show instructional text for agents with regards to agent phone number routing" do
          get :edit, params: { id: @with_groups_admin.id }
          assert_response :success
          assert_select 'p#agent_voice_routing_instructions', count: 0
        end
      end
    end

    describe "language selector drop down" do
      before do
        @account = accounts(:minimum)
        @japanese = translation_locales(:japanese)
        @spanish = translation_locales(:spanish)
        Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
        Account.any_instance.stubs(:available_languages).returns([@japanese, @spanish])
      end
      it "is not present when current user is an end user" do
        login('with_groups_end_user')
        get :edit, params: { id: @with_groups_end_user.id }
        assert_select 'select#user_locale_id', count: 0
      end
      it "is present for end users when the account has more than 1 available language and current_user is an agent" do
        login('with_groups_admin')
        get :edit, params: { id: @with_groups_end_user.id }
        assert_select 'select#user_locale_id'
      end
      it "is not present for end users when the account has only 1 avaialble language" do
        login('with_groups_admin')
        Account.any_instance.stubs(:available_languages).returns([@japanese])
        get :edit, params: { id: @with_groups_end_user.id }
        assert_select 'select#user_locale_id', count: 0
      end
    end

    describe 'end-user' do
      describe_with_arturo_enabled :disable_legacy_people_users_id_page do
        before do
          @controller.stubs(:current_brand).returns(@account.brands.first)
          login('with_groups_end_user')
          get :edit, params: { id: @with_groups_end_user.id }
        end

        it ('responds with 404') { assert_response :not_found }
      end
    end
  end

  describe "#update_notes" do
    it "allows an agent to update notes" do
      login('with_groups_admin')
      assert @with_groups_end_user.notes.to_s.index('hello there').nil?
      put :update_notes, params: { id: @with_groups_end_user.id, user: { notes: 'hello there' }, field: 'i-am-special', format: 'js' }
      assert_response :ok
      assert_match(/i-am-special-show/, @response.body)
      assert @with_groups_end_user.reload.notes.index('hello there')
    end

    it "does not allow a restricted agent to update notes" do
      login(users(:with_groups_agent_groups_restricted))
      put :update_notes, params: { id: @with_groups_end_user.id, user: { notes: 'hello' }, format: 'js' }
      assert_response 403
    end

    it "does not allow end-users to update notes" do
      login(users(:minimum_end_user))
      put :update_notes, params: { id: @with_groups_end_user.id, user: { notes: 'hello' }, format: 'js' }
      assert_response 403
    end
  end

  describe "#update" do
    before do
      @account = accounts(:with_groups)
      @request.account = @account
    end

    describe "owner" do
      before do
        @owner = @account.owner
        assert @owner.is_account_owner?
        refute @owner.settings.suspended?
        login(@owner)
      end

      it "does not allow suspension of owner" do
        put :update, params: { user: { suspended: true }, id: @owner.id }
        refute @owner.reload.settings.suspended?
      end
    end

    describe "agent" do
      before do
        @agent = users(:with_groups_agent1)
        refute @agent.settings.suspended?
        login(@agent)
      end

      it "does not allow suspension of oneself" do
        put :update, params: { user: { suspended: true }, id: @agent.id }
        refute @agent.reload.settings.suspended?
      end

      it "does not allow promoting of oneself" do
        put :update, params: { user: { roles: Role::ADMIN.id }, id: @agent.id }
        assert_response :forbidden
        refute @agent.reload.is_admin?
      end

      describe "with a permission set" do
        before do
          Account.any_instance.stubs(has_permission_sets?: true)
          permission_set = PermissionSet.create!(account: @account, name: "things")
          @agent.permission_set = permission_set
          @agent.save!
        end

        it "does not allow promoting of oneself" do
          put :update, params: { user: { roles: Role::ADMIN.id }, id: @agent.id }
          assert_response :forbidden
          refute @agent.reload.is_admin?
        end
      end

      it "updates user's cache_key when changing related data" do
        @old_cache_key = @agent.cache_key
        @group = groups(:with_groups_group2)
        put :update, params: { user: { groups: (@agent.groups << @group) }, id: @agent.id }

        @agent.reload
        assert_not_equal @old_cache_key, @agent.cache_key
      end

      describe "setting a password for another user" do
        it "is allowed if admins_can_set_user_passwords is true" do
          Account.any_instance.stubs(:admins_can_set_user_passwords).returns(true)
          put :update, params: { user: { password: "abcdef" }, id: @agent.id }
          assert @agent.reload.authenticated?('abcdef')
        end

        it "is not allowed if admins_can_set_user_passwords is false" do
          Account.any_instance.stubs(:admins_can_set_user_passwords).returns(false)
          put :update, params: { user: { password: "abcdef" }, id: @agent.id }
          refute @agent.reload.authenticated?('abcdef')
        end
      end

      describe "when creating a voice forwarding number" do
        it "saves a valid forwarding number" do
          put :update, params: { user: { voice_number: "14155556666" }, id: @agent.id }
          assert_response :redirect
          assert_equal('+14155556666', @agent.reload.voice_number_instance.value)
        end

        it "saves a valid forwarding number with extension" do
          put :update, params: { user: { voice_number: "14155556666", voice_extension: "123" }, id: @agent.id }
          assert_response :redirect
          assert_equal('+14155556666x123', @agent.reload.voice_number_instance.value)
        end

        it "does not save an empty forwarding number" do
          put :update, params: { user: { voice_number: "", voice_extension: "" }, id: @agent.id }
          refute @agent.reload.has_voice_number?
        end

        it "does not save an invalid forwarding number" do
          put :update, params: { user: { voice_number: 'invalid' }, id: @agent.id }
          refute @agent.reload.has_voice_number?
        end
      end

      describe "with a voice forwarding number already present" do
        before do
          @agent.identities << UserVoiceForwardingIdentity.new(value: "+14155556666x555", user: @agent)
          assert @agent.reload.has_voice_number?
        end

        it "updates a valid forwarding number" do
          put :update, params: { user: { voice_number: "14155556666" }, id: @agent.id }
          assert_response :redirect
          assert_equal('+14155556666x555', @agent.reload.voice_number_instance.value)
        end

        it "updates a valid forwarding extension" do
          put :update, params: { user: { voice_number: "14155556666", voice_extension: "111" }, id: @agent.id }
          assert_response :redirect
          assert_equal('+14155556666x111', @agent.reload.voice_number_instance.value)
        end

        it "does not update an invalid forwarding number" do
          put :update, params: { user: { voice_number: 'invalid' }, id: @agent.id }
          assert_equal('+14155556666x555', @agent.reload.voice_number_instance.value)
        end

        it "removes extension if invalid extension is passed" do
          put :update, params: { user: { voice_extension: 'invalid' }, id: @agent.id }
          assert_response :redirect
          assert_equal('+14155556666', @agent.reload.voice_number_instance.value)
        end

        it "removes extension if empty extension is passed" do
          put :update, params: { user: { voice_extension: '' }, id: @agent.id }
          assert_response :redirect
          assert_equal('+14155556666', @agent.reload.voice_number_instance.value)
        end

        describe "when deleting a number" do
          it "deletes a number if empty number value is passed" do
            put :update, params: { user: {voice_number: ''}, id: @agent.id }
            assert_response :redirect
            refute @agent.reload.has_voice_number?
          end

          it "deletes a number if empty number value and any extension is passed" do
            put :update, params: { user: {voice_number: '', voice_extension: '123'}, id: @agent.id }
            assert_response :redirect
            refute @agent.reload.has_voice_number?
          end

          it "deletes a number if an agent is downgraded to end user" do
            login(:with_groups_admin)
            put :update, params: { user: { roles: Role::END_USER.id }, id: @agent.id }
            refute @agent.reload.has_voice_number?
          end

          it "deletes a number if an agent is downgraded to light agent" do
            login(:with_groups_admin)
            @agent.assigned = []
            Account.any_instance.stubs(:has_light_agents?).returns(true)
            PermissionSet.create_light_agent!(@account)
            put :update, params: { user: {roles: Role::AGENT.id, role_or_permission_set: @account.permission_sets.find_by_role_type(PermissionSet::Type::LIGHT_AGENT).id }, id: @agent.id }
            refute @agent.reload.has_voice_number?
          end
        end
      end
    end

    describe "admin" do
      before do
        login('with_groups_admin')
        @user = users(:with_groups_end_user)
        @user.phone = "510-555-5555"
        @user.save!
        @account = @user.account
        @locale = translation_locales(:spanish)
        @request.account = @account
      end

      it "is able to change an end user's password if admins_can_set_user_passwords is true" do
        Account.any_instance.stubs(:admins_can_set_user_passwords).returns(true)
        put :update, params: { user: { password: "abcdef" }, id: @user.id }
        assert flash[:notice]
        assert @user.reload.authenticated?('abcdef')
        assert_equal "510-555-5555", @user.phone
      end

      it "is not able to change an end user's password if admins_can_set_user_passwords is false" do
        Account.any_instance.stubs(:admins_can_set_user_passwords).returns(false)
        put :update, params: { user: { password: "abcdef" }, id: @user.id }
        assert flash[:notice]
        refute @user.reload.authenticated?('abcdef')
        assert_equal "510-555-5555", @user.phone
      end

      describe "setting profile image via URL" do
        describe "with a valid URL" do
          it "enqueues a job to fetch the remote image" do
            url = "http://foo.com/a.png"
            FetchProfileImageJob.expects(:enqueue).with(@user.account_id, @user.id, url)
            put :update, params: { user: { name: "Hallo Mand", remote_photo_url: url}, id: @user.id }
            assert_response :redirect
          end
        end

        describe "with an invalid URL" do
          it "errors out and not enqueue a job to fetch the remote image" do
            FetchProfileImageJob.expects(:enqueue).never
            put :update, params: { user: { name: "Hallo Mand", remote_photo_url: "bad url"}, id: @user.id, format: :json }
            assert_response :not_acceptable
          end
        end
      end

      it "does not allow an end-user to be updated if the email is a recipient address" do
        @user.identities.email.first.update_column(:value, recipient_addresses(:with_groups_backup).email)
        put :update, params: { user: {name: "something new"}, id: @user.id }
        assert_includes flash[:error].to_s.gsub("&#39;", "'"), "support@withgroups.zendesk-test.com cannot be used; it is in use as a support address"
      end

      it "is able to set a language that is part of the accounts available languages" do
        Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
        Account.any_instance.stubs(:available_languages).returns([@locale])
        put :update, params: { user: { locale_id: @locale.id }, id: @user.id }
        assert_response :redirect
        assert_equal @locale.id, @user.reload.locale_id
      end
    end

    describe "end user" do
      before do
        @user = users(:with_groups_end_user)
        assert @user.photo.nil?
      end

      it "is able to set a photo" do
        login('with_groups_end_user')
        put :update, params: { id: @user.id, user: { name: "Hallo Mand" }, photo: { uploaded_data: fixture_file_upload('small.png', 'image/png') } }
        assert_response :redirect
        assert @user.reload.photo.present?
      end

      it "is not able to set a photo with invalid content type" do
        login('with_groups_end_user')
        put :update, params: { id: @user.id, user: { name: "Hallo Mand" }, photo: { uploaded_data: fixture_file_upload('small.png', 'foo/bar') } }
        assert_match %r{Photo(.+)is invalid}i, flash[:error]
        assert @user.reload.photo.nil?
      end

      it "is not able to set a photo with invalid extension" do
        login('with_groups_end_user')
        put :update, params: { id: @user.id, user: { name: "Hallo Mand" }, photo: { uploaded_data: fixture_file_upload('notepad.exe', 'image/png') } }
        flash[:error].must_include "Failed to update user<ul><li><strong>Photo: </strong> extension exe is not allowed. Please use jpg, jpeg, gif, or png for your photo.</li></ul>"
        assert @user.reload.photo.nil?
      end

      it "is not able to upload a fake photo file" do
        login('with_groups_end_user')
        assert @user.reload.photo.nil?
        put :update, params: { id: @user.id, user: { name: "Hallo Mand" }, photo: { uploaded_data: fixture_file_upload('notepad.exe.png', 'image/png') } }
        assert @user.reload.photo.nil?
        assert_match %r{Photo(.+)is invalid}i, flash[:error]
      end

      it "is not able to set external_id" do
        login('with_groups_end_user')
        assert @user.external_id.blank?
        put :update, params: { id: @user.id, user: { name: "Hallo Mand", external_id: "randombugger" } }
        assert_response :redirect
        assert @user.external_id.blank?
      end

      describe "changing roles" do
        before { login('with_groups_end_user') }

        it "does not allow change to admin" do
          put :update, params: { user: { roles: Role::ADMIN.id, name: "Some new name" }, id: @user.id }
          assert_response :redirect
          assert_equal @user.reload.name, "Some new name"
          refute @user.is_admin?
        end

        it "does not allow change to agent" do
          put :update, params: { user: { roles: Role::AGENT.id, name: "Some other name" }, id: @user.id }
          assert_response :redirect
          assert_equal @user.reload.name, "Some other name"
          refute @user.reload.is_agent?
        end
      end
    end
  end

  describe "#assume" do
    describe "with :publish_assumption_event_to_kafka arturo off" do
      before { Arturo.disable_feature! :publish_assumption_event_to_kafka }

      it "does not emit assumption event kafka message" do
        EscKafkaMessage.expects(:create!).never
        login('with_groups_admin')

        post :assume, params: { id: @with_groups_end_user.id }
        assert_equal(users(:with_groups_admin), @controller.send(:original_user))
        assert_equal(@with_groups_end_user, @controller.send(:current_user))
        assert_redirected_to "/home"
      end
    end

    describe "with :publish_assumption_event_to_kafka arturo on" do
      let(:shared_session_id) { "a8369f6133889f206c21360efb1893fdefca2a842ff689539c4cb2b185087dab" }
      before do
        Arturo.enable_feature! :publish_assumption_event_to_kafka
      end

      it "emits assumption event kafka messagee" do
        Timecop.freeze('30 Apr 2019 00:00:00') do
          EscKafkaMessage.expects(:create!).with(
            account_id: @account.id,
            topic: "auth.assumption_events",
            key: shared_session_id,
            value: AssumptionEventsProtobufEncoder.new(event_type: "assume", event_time: Time.now, account_id: @account.id, original_user_id: @with_groups_admin.id, current_user_id: @with_groups_end_user.id).full.to_proto
          )
          login('with_groups_admin')
          shared_session["id"] = shared_session_id

          post :assume, params: { id: @with_groups_end_user.id }
        end
      end
    end

    it "assumes the logged in user is the specified user" do
      login('with_groups_admin')
      post :assume, params: { id: @with_groups_end_user.id }
      assert_equal(users(:with_groups_admin), @controller.send(:original_user))
      assert_equal(@with_groups_end_user, @controller.send(:current_user))
      assert_redirected_to "/home"
    end

    describe "assuming an anonymous user" do
      it "ifs invalid id is specified" do
        login('with_groups_admin')
        post :assume, params: { id: 1234 }
        assert_equal(users(:with_groups_admin), @controller.send(:original_user))
        assert(@controller.send(:current_user).is_anonymous_user?)
        assert_redirected_to "/home"
      end

      it "ids specified is 'anonymous'" do
        login('with_groups_admin')
        post :assume, params: { id: 'anonymous' }
        assert_equal users(:with_groups_admin), @controller.send(:original_user)
        assert @controller.send(:current_user).is_anonymous_user?
        assert_redirected_to "/home"
      end
    end

    describe "hostmapping and ssl" do
      before { login('with_groups_admin') }

      it "assumes user using challenge token with hostmapping" do
        @account.update_attribute(:host_mapping, 'test.com')
        post :assume, params: { id: @with_groups_end_user.id }
        assert_equal(users(:with_groups_admin), @controller.send(:original_user))
        assert_equal(@with_groups_end_user, @controller.send(:current_user))
        assert_match /http:\/\/test.com\/access\/return_to\?challenge/, @response.redirected_to
      end

      it "redirects to https with ssl enabled" do
        Account.any_instance.stubs(is_ssl_enabled?: true)
        post :assume, params: { id: @with_groups_end_user.id }
        assert_equal(users(:with_groups_admin), @controller.send(:original_user))
        assert_equal(@with_groups_end_user, @controller.send(:current_user))
        assert_redirected_to "https://withgroups.zendesk-test.com/home"
      end
    end

    it "sets the locale if specified and set the flash notice" do
      user = users(:with_groups_admin)
      user.translation_locale = translation_locales(:brazilian_portuguese)
      user.save!

      login(user)
      translation_locale = translation_locales(:japanese)
      post :assume, params: { id: @with_groups_end_user.id, translation_id: translation_locale.id }
      assert_equal translation_locale.id, shared_session[:locale_id].to_i
      assert_equal "You are previewing a translation with user #{@with_groups_end_user.name}.", flash[:notice]
    end

    it "returns deny_user if the current_user is not allowed to assume the desired user" do
      login(:with_groups_agent1)
      admin = users(:with_groups_admin)
      User.any_instance.stubs(:can?).returns(true)
      User.any_instance.expects(:can?).with(:assume, @with_groups_end_user).returns(false)
      People::UsersController.any_instance.stubs(:current_user).returns(admin)
      post :assume, params: { id: @with_groups_end_user.id }
      assert_response 403
    end

    it "does not allow assumption if the user is already being assumed" do
      login('with_groups_admin')
      @original_user = users(:with_groups_admin)
      People::UsersController.any_instance.stubs(is_assuming_user?: true, original_user: @original_user)
      post :assume, params: { id: @with_groups_end_user.id }
      assert_equal 'Cannot assume the user because you are already assuming a user.', flash[:error]
      assert_template :index
    end

    it "does not do anything if trying to assume the current user" do
      login('with_groups_admin')
      People::UsersController.any_instance.stubs(:current_user?).returns(@with_groups_end_user)
      post :assume, params: { id: @with_groups_end_user.id }
      assert_template nil
    end

    describe_with_arturo_enabled :disable_user_assume do
      it "does not allow assumption" do
        login('with_groups_admin')
        post :assume, params: { id: @with_groups_end_user.id }
        assert_nil @controller.send(:original_user)
        assert_equal(users(:with_groups_admin), @controller.send(:current_user))
        assert_response 403
      end
    end
  end

  describe '#revert' do
    before do
      login('with_groups_admin')
    end

    describe "with :publish_assumption_event_to_kafka arturo off" do
      before do
        Arturo.disable_feature! :publish_assumption_event_to_kafka
        # Stubbing other user events that send kafka messages
        UserEventsProtobufEncoder.any_instance.stubs(:to_a).returns([])
      end

      it "does not emit assumption event kafka message" do
        EscKafkaMessage.expects(:create!).once # Publish user entity
        login('with_groups_admin')
        post :assume, params: { id: @with_groups_end_user.id }

        put :revert
        assert_redirected_to "/home"
      end
    end

    describe "with :publish_assumption_event_to_kafka arturo on" do
      let(:shared_session_id) { "a8369f6133889f206c21360efb1893fdefca2a842ff689539c4cb2b185087dab" }
      before do
        Arturo.enable_feature! :publish_assumption_event_to_kafka
        # Stubbing other user events that send kafka messages
        UserEventsProtobufEncoder.any_instance.stubs(:to_a).returns([])
      end

      it "emits assumption event kafka message" do
        Timecop.freeze('30 Apr 2019 00:00:00') do
          shared_session["id"] = shared_session_id
          EscKafkaMessage.expects(:create!).twice # publish a user entity
          post :assume, params: { id: @with_groups_end_user.id }

          EscKafkaMessage.expects(:create!).with(
            account_id: @account.id,
            topic: "auth.assumption_events",
            key: shared_session_id,
            value: AssumptionEventsProtobufEncoder.new(event_type: "revert", event_time: Time.now, account_id: @account.id, original_user_id: nil, current_user_id: @with_groups_admin.id).full.to_proto
          )

          put :revert
          assert_redirected_to "/home"
        end
      end
    end

    it "redirects successful revert to home" do
      post :assume, params: { id: @with_groups_end_user.id }

      put :revert
      assert_redirected_to "/home"
    end

    it "redirects to home if we not assuming a user" do
      put :revert
      assert_redirected_to "/home"
    end
  end

  describe "#successful_twitter_auth" do
    it "adds an identity to the user" do
      login(@with_groups_admin)
      access_token = stub(params: { user_id: '123' })
      Zendesk::Auth::Twitter.any_instance.expects(:create_access_token).with(anything).returns(access_token)
      stub_request(:post, "https://api.twitter.com/oauth2/token").
        to_return(
          status: 200,
          body: {"token_type" => "bearer", "access_token" => "AAAA%2FAAA%3DAAAAAAAA"}.to_json,
          headers: {'Content-Type' => 'application/json'}
        )
      stub_request(:get, "https://api.twitter.com/1.1/users/show.json?user_id=123").
        to_return(
          status: 200,
          body: read_test_file('twitter_users_show_110542029_1.1.json'),
          headers: {'Content-Type' => 'application/json'}
        )

      get :successful_twitter_auth, params: { id: @with_groups_admin.id, state: Zendesk::Auth::Warden::OAuthStrategy.state({}, Zendesk::Configuration.dig!(:google, :key)) }
      assert_response :redirect
      assert_match(/Twitter/, flash[:notice])
    end

    it "denys access with an invalid state" do
      login(@with_groups_admin)
      get :successful_twitter_auth, params: { id: users(:with_groups_agent1).id, state: Zendesk::Auth::Warden::OAuthStrategy.state({session_id: '123'}, Zendesk::Configuration.dig!(:google, :key)) }
      assert_response 403
    end

    it "shows a flash error message if the user clicks 'Deny'" do
      login(@with_groups_admin)

      Zendesk::Auth::Twitter.any_instance.expects(:create_access_token).raises(OAuth::Unauthorized)
      get :successful_twitter_auth, params: { id: @with_groups_admin.id, state: Zendesk::Auth::Warden::OAuthStrategy.state({}, Zendesk::Configuration.dig!(:google, :key)) }
      assert_response :redirect
      assert_match(/the authorization was denied/, flash[:error])
    end

    it "shows a flash error message when something bad happened" do
      login(@with_groups_admin)

      Zendesk::Auth::Twitter.any_instance.expects(:create_access_token).raises(Net::HTTPFatalError.new(500, "Service Unavailable"))
      get :successful_twitter_auth, params: { id: @with_groups_admin.id, state: Zendesk::Auth::Warden::OAuthStrategy.state({}, Zendesk::Configuration.dig!(:google, :key)) }
      assert_response :redirect
      assert_match(/there was a communication error/, flash[:error])
    end
  end

  describe "#successful_facebook_auth" do
    before do
      @env = { 'rack.session' => { callback_url: "http://callbackurl" } }
    end

    it "adds an identity to the user" do
      login(@with_groups_admin)
      Zendesk::Auth::Facebook.any_instance.stubs(:create_access_token).returns('access_token')
      appsecret_proof = Digest::HMAC.hexdigest('access_token', Zendesk::Configuration.fetch(:facebook_integration)['secret'], Digest::SHA256)
      stub_request(:get, "https://graph.facebook.com/v3.2/me?access_token=access_token&" \
        "appsecret_proof=#{appsecret_proof}").
        with { |v| v.headers['User-Agent'].match(/^Faraday/) }.
        to_return(status: 200, body: read_test_file('facebook_user_profile.json'))
      get :successful_facebook_auth, params: { id: @with_groups_admin.id, state: Zendesk::Auth::Warden::OAuthStrategy.state({}, Zendesk::Configuration.dig!(:google, :key)) }
      assert_response :redirect
    end

    it "shows a flash error message if the user clicks 'Deny'" do
      login(@with_groups_admin)

      Zendesk::Auth::Facebook.any_instance.expects(:create_access_token).raises(OAuth2::Error.new(stub_everything))
      get :successful_facebook_auth, params: { id: @with_groups_admin.id, error: "access_denied", state: Zendesk::Auth::Warden::OAuthStrategy.state({}, Zendesk::Configuration.dig!(:google, :key)) }
      assert_response :redirect
      assert_match(/the authorization was denied/, flash[:error])
    end

    it "denys access with an invalid state" do
      login(@with_groups_admin)
      get :successful_facebook_auth, params: { id: users(:with_groups_agent1).id, state: Zendesk::Auth::Warden::OAuthStrategy.state({ session_id: '123' }, Zendesk::Configuration.dig!(:google, :key)) }
      assert_response 403
    end

    it "shows a flash error message if random baddness happened" do
      login(@with_groups_admin)

      Zendesk::Auth::Facebook.any_instance.expects(:create_access_token).raises(OAuth2::Error.new(stub_everything))
      get :successful_facebook_auth, params: { id: @with_groups_admin.id, error: "busted_yo", error_description: "Shit got ill", state: Zendesk::Auth::Warden::OAuthStrategy.state({}, Zendesk::Configuration.dig!(:google, :key)) }
      assert_response :redirect
      assert_match(/Unable to add new Facebook account/, flash[:error])
    end

    it "shows a flash error message when something bad happened" do
      login(@with_groups_admin)

      Zendesk::Auth::Facebook.any_instance.expects(:create_access_token).raises(Net::HTTPFatalError.new(500, "Service Unavailable"))
      get :successful_facebook_auth, params: { id: @with_groups_admin.id, state: Zendesk::Auth::Warden::OAuthStrategy.state({}, Zendesk::Configuration.dig!(:google, :key)) }
      assert_response :redirect
      assert_match(/there was a communication error/, flash[:error])
    end
  end

  describe "#create_twitter_profile" do
    before do
      @request.stubs(:ssl?).returns(true)
      session[:return_to] = "/forums"
      @access_token = stub(params: {user_id: '123'}, token: 'abc', secret: 'def')
      @twitter_auth = stub(create_access_token: @access_token)
      Zendesk::Auth::Twitter.stubs(:new).with('twitter_readonly').returns(@twitter_auth)
      ::Channels::TwitterUserProfile.stubs(:from_external_id)
      session[:create_twitter_profile_user_id] = @with_groups_admin.id
    end

    describe "with successful authentication" do
      before do
        profile = channels_user_profiles(:minimum_twitter_user_profile_1)
        ::Channels::TwitterUserProfile.expects(:from_external_id).
          with(@account, '123', @with_groups_admin, access_token: 'abc', secret: 'def').
          returns(profile)
        get :create_twitter_profile, params: { id: @with_groups_admin, oauth_verifier: '123456' }
      end

      should_not set_flash
      should_redirect_to("the return_to url") { File.join(@account.url, "/forums") }
      should_create :user_twitter_identity
      it("log me in") { refute @controller.send(:current_user).is_anonymous_user? }
    end

    describe "with missing return_to parameter" do
      before do
        session[:return_to] = nil
        get :create_twitter_profile, params: { id: @with_groups_admin, oauth_verifier: '123456' }
      end
      should_not set_flash
      should_redirect_to("the home page") { File.join(@account.url, home_path) }
      should_create :user_twitter_identity
    end

    describe "with OAuth::Unauthorized during authentication" do
      before do
        @twitter_auth.stubs(:create_access_token).raises(OAuth::Unauthorized)
        get :create_twitter_profile, params: { id: @with_groups_admin, oauth_verifier: '123456' }
      end
      should_set_the_flash_to :error
      should_redirect_to("the return_to url") { File.join(@account.url, "/forums") }
      should_not_change("the number of User Identities") { UserTwitterIdentity.count(:all) }
      it("not log me in") { assert @controller.send(:current_user).is_anonymous_user? }
    end

    describe "with OAuth::Unauthorized during authentication and missing return-to parameter" do
      before do
        session[:return_to] = nil
        @twitter_auth.stubs(:create_access_token).raises(OAuth::Unauthorized)
        get :create_twitter_profile, params: { id: @with_groups_admin, oauth_verifier: '123456' }
      end
      should_redirect_to("the home page") { File.join(@account.url, home_path) }
    end

    describe "with HTTPFatalError during authentication" do
      before do
        @twitter_auth.stubs(:create_access_token).raises(Net::HTTPFatalError.new(500, "Internal Server Error"))
        get :create_twitter_profile, params: { id: @with_groups_admin, oauth_verifier: '123456' }
      end
      should_set_the_flash_to :error
      should_redirect_to("the return_to url") { File.join(@account.url, "/forums") }
      should_not_change("the number of User Identities") { UserTwitterIdentity.count(:all) }
    end

    describe "with unrecognized create_twitter_profile_user_id" do
      before do
        session[:create_twitter_profile_user_id] = 'invalid'
        get :create_twitter_profile, params: { id: @with_groups_admin, oauth_verifier: '123456' }
      end

      should_set_the_flash_to :error
      should_redirect_to("the return_to url") { File.join(@account.url, "/forums") }
      should_not_change("the number of User Identities") { UserTwitterIdentity.count(:all) }
      it("not log me in") { assert @controller.send(:current_user).is_anonymous_user? }
    end

    describe "with unmatched create_twitter_profile_user_id" do
      before do
        session[:create_twitter_profile_user_id] = users(:minimum_end_user).id
        get :create_twitter_profile, params: { id: @with_groups_admin, oauth_verifier: '123456' }
      end

      should_set_the_flash_to :error
      should_redirect_to("the return_to url") { File.join(@account.url, "/forums") }
      should_not_change("the number of User Identities") { UserTwitterIdentity.count(:all) }
      it("not log me in") { assert @controller.send(:current_user).is_anonymous_user? }
    end
  end

  describe "#create" do
    describe "admin" do
      before do
        @with_groups_admin = login('with_groups_admin')
      end

      it 'returns 410 gone' do
        post :create, params: { user: { name: 'Mickey Mouse', email: 'mickey@aghassipour.com', roles: Role::END_USER.id } }
        assert_response :gone
      end
    end

    describe "unrestricted agents" do
      it "are not able to change their role" do
        login('with_groups_agent1')

        put :update, params: { id: users(:with_groups_agent1).id, user: { roles: Role::ADMIN.id } }
        assert_response 403
        refute users(:with_groups_agent1).is_admin?

        put :update, params: { id: users(:with_groups_agent1).id, user: { roles: Role::END_USER.id } }
        assert_response 403
        refute users(:with_groups_agent1).is_end_user?
      end
    end
  end

  describe "as an end user" do
    before { login('with_groups_end_user') }

    describe "accessing #show" do
      describe "with own id" do
        before do
          get :show, params: { id: @with_groups_end_user.id }
        end
        should_render_template :show
        it('responds with success') { assert_response :success }
      end
    end

    describe "accessing #new" do
      before { get :new }
      it('responds with gone') { assert_response :gone }
    end

    describe "accessing #update" do
      describe "trying to change role" do
        before { put :update, params: { user: { roles: Role::ADMIN.id }, id: @with_groups_end_user.id } }
        it('responds with redirect') { assert_response :redirect }
        it "does not change the role" do
          assert_equal 0, @with_groups_end_user.reload.roles
        end
      end

      describe "updating profile" do
        before do
          assert_not_equal 'gedebuk', @with_groups_end_user.name
          assert_not_equal '4152852324', @with_groups_end_user.phone
          # BBO -- removing time_zone tests for now, the with_groups user is not 'Plus', which has the per-agent timezone features.
          # assert_not_equal 'Pacific Time (US & Canada)', @with_groups_end_user.time_zone
          # put :update, :user => { :name => 'gedebuk', :phone => '4152852324', :time_zone => 'Pacific Time (US & Canada)' }, :id => @with_groups_end_user.id
          put :update, params: { user: { name: 'gedebuk', phone: '4152852324' }, id: @with_groups_end_user.id }
        end
        it('responds with redirect') { assert_response :redirect }
        it "updates the user" do
          @with_groups_end_user.reload
          assert_equal 'gedebuk',    @with_groups_end_user.name
          assert_equal '4152852324', @with_groups_end_user.phone
          # assert_equal 'Pacific Time (US & Canada)', @with_groups_end_user.time_zone
        end
      end

      describe "updating password" do
        before do
          refute @with_groups_end_user.authenticated?("password")
          put :update, params: { user: {}, password: 'password', id: @with_groups_end_user.id }
          @with_groups_end_user.reload
        end
        it('responds with redirect') { assert_response :redirect }
        it "updates the password" do
          assert @with_groups_end_user.authenticated?("password")
        end
      end
    end

    describe "accessing #destroy" do
      before do
        delete :destroy, params: { id: @with_groups_end_user.id }
      end
      it('responds with forbidden') { assert_response :forbidden }
      it "does not delete the user" do
        assert @with_groups_end_user.reload.is_active?
      end
    end
  end

  describe "as a restricted agent" do
    before { login('with_groups_agent_groups_restricted') }

    it "does not allow updating users" do
      # Restricted agents can't update end users
      # TODO - Does not really make sense..?
      put :update, params: { user: { name: 'new name', roles: Role::END_USER.id }, id: @with_groups_end_user.id }
      assert_response 403
    end
  end

  describe "as an unrestricted agent" do
    before { login('with_groups_agent1') }

    describe "accessing #new" do
      before { get :new }
      it('responds with gone') { assert_response :gone }
    end

    describe "accessing #show" do
      before { get :show, params: { id: @with_groups_end_user.id } }
      it('redirects to lotus') { assert_redirected_to "/agent/users/#{@with_groups_end_user.id}" }
    end

    describe "accessing #edit" do
      before { get :edit, params: { id: @with_groups_end_user.id } }
      it('responds with success') { assert_response :success }
    end

    describe "accessing #update" do
      describe "to update an end user" do
        before { put :update, params: { user: { name: 'new name', roles: Role::END_USER.id }, id: @with_groups_end_user.id } }
        it('responds with redirect') { assert_response :redirect }
      end

      describe "to update an agent" do
        before { put :update, params: { user: { name: 'new name' }, id: users(:with_groups_agent2).id } }
        it('responds with forbidden') { assert_response :forbidden }
      end

      describe "to update him self" do
        before { put :update, params: { user: { name: 'new name' }, id: users(:with_groups_agent1).id } }
        it('responds with redirect') { assert_response :redirect }
      end
    end

    describe "accessing #assume" do
      describe "to assume an agent" do
        before { get :assume, params: { id: users('with_groups_agent2').id } }
        it('responds with forbidden') { assert_response :forbidden }
      end
      describe "to assume an end user" do
        before { get :assume, params: { id: users('with_groups_end_user').id } }
        it('responds with redirect') { assert_response :redirect }
      end
    end

    describe "accessing #verify" do
      before do
        @identity = users('with_groups_end_user').identities.email.first
        @identity.update_attribute(:is_verified, false)
        get :verify, params: { id: users('with_groups_end_user').id, identity_id: @identity.id }
      end
      it('responds with redirect') { assert_response :redirect }
      it "verifys the identity" do
        assert @identity.reload.is_verified?
      end
    end
  end

  describe "as an admin" do
    before do
      login('with_groups_admin')
    end

    describe "accessing #show" do
      before do
        get :show, params: { id: @with_groups_end_user.id }
      end
      should_redirect_to("lotus user profile") { "/agent/users/#{@with_groups_end_user.id}" }
    end

    describe "accessing #edit" do
      before { get :edit, params: { id: @with_groups_end_user.id } }
      it('responds with success') { assert_response :success }
    end

    describe "accessing #destroy for foreign user" do
      before do
        @agent_to_delete = users(:with_groups_agent_assigned_restricted)
        @agent_to_delete.identities << UserForeignIdentity.new(user: @agent_to_delete, value: SecureRandom.hex.to_s, is_verified: true)
        @agent_to_delete.foreign = true
        @agent_to_delete.foreign_agent = true
        @agent_to_delete.save!
      end

      describe "when the user has a ticket" do
        before do
          t = tickets(:with_groups_1)
          t.requester = @agent_to_delete
          t.will_be_saved_by(@with_groups_admin)
          t.save!
        end

        describe "and with a return_to parameter" do
          before do
            delete :destroy, params: { id: @agent_to_delete.id, return_to: home_url }
          end
          should_redirect_to("the return_to url") { home_url }

          it "does not delete the user" do
            assert @agent_to_delete.reload.is_active?
          end
        end

        describe "and with no return_to parameter" do
          before do
            delete :destroy, params: { id: @agent_to_delete.id }
          end
          should_redirect_to("the return_to url") { edit_user_path(@agent_to_delete) }

          it "does not delete the user" do
            assert @agent_to_delete.reload.is_active?
          end
        end
      end

      describe "when the user has no tickets" do
        before do
          delete :destroy, params: { id: @agent_to_delete.id }
        end
        it('responds with redirect') { assert_response :redirect }
        it "deletes the user" do
          refute @agent_to_delete.reload.is_active?
        end
      end
    end

    describe "accessing #assume" do
      describe "to assume an agent" do
        before { get :assume, params: { id: users('with_groups_agent2').id } }
        it('responds with redirect') { assert_response :redirect }
      end
      describe "to assume an end user" do
        before { get :assume, params: { id: users('with_groups_end_user').id } }
        it('responds with redirect') { assert_response :redirect }
      end
    end
  end

  describe 'tags' do
    before do
      @request.account.settings.has_user_tags = true
      @request.account.settings.save!

      @request.account = accounts(:with_groups)
      @user = users(:with_groups_agent1)
    end

    it "Adds a tag to agent as admin" do
      login(users(:with_groups_admin))
      put :update, params: { user: { additional_tags: 'hej' }, id: @user.id }
      assert_equal "hej", @user.reload.current_tags
    end

    it "Nots add a tag to agent as agent" do
      login(users(:with_groups_agent1))
      put :update, params: { user: { additional_tags: 'hello' }, id: @user.id }
      assert_equal "", @user.reload.current_tags.to_s
    end
  end

  describe "#merge_with_email" do
    before do
      @request.account = accounts(:minimum)
      @loser = users(:minimum_end_user)
      @loser.stubs(:can_be_merged_as_loser?).returns(true)
      login(users(:minimum_end_user))
    end

    describe "an email that does not exist on any other user in the account" do
      before do
        User.any_instance.expects(:has_alter_ego?).returns(false)
        xhr :post, :merge_with_email, params: { email: 'yahoo@example.com' }
      end

      it('responds with ok') { assert_response :ok }
      it('responds with content_type js') { assert_equal 'text/javascript', @controller.response.content_type.to_s }
      should_create :user_identity
      should_change("the loser's number of identities") { @loser.identities.count(:all) }

      it "sends verify email" do
        assert @loser.send_verify_email
      end
    end

    describe "an email that does not exist on any other user in the account but has a valid facebook or twitter identity" do
      before do
        User.any_instance.expects(:has_alter_ego?).returns(true)
      end

      it "adds new unverified email address to current user" do
        assert_difference('@loser.reload.unverified_email_addresses.count(:all)') do
          post :merge_with_email, params: { email: 'denmark_is_awesome@forever.com' }
        end
      end
    end

    describe "with winner that is an agent" do
      before do
        xhr :post, :merge_with_email, params: { email: users(:minimum_agent).email }
      end

      should_set_the_flash_to(/cannot be added to your account/)
      it('responds with content_type js') { assert_equal 'text/javascript', @controller.response.content_type.to_s }
    end

    describe "with valid winner, where password is needed" do
      before do
        xhr :post, :merge_with_email, params: { email: users(:minimum_author).email }
      end

      it('responds with content_type js') { assert_equal 'text/javascript', @controller.response.content_type.to_s }
      it "returns form switch javascript" do
        assert_equal '$z("people/users/merge").switch_to_password_form()();', @response.body
      end
    end

    describe "with unverified winner that needs a verification email" do
      before do
        User.any_instance.stubs(:is_verified?).returns(false)
        xhr :post, :merge_with_email, params: { email: users(:minimum_author).email }
      end

      should_set_the_flash_to(/Please verify/)
      it('responds with content_type js') { assert_equal 'text/javascript', @controller.response.content_type.to_s }

      before_should 'sign the user out' do
        warden.expects :logout
      end
    end
  end

  describe "#merge_with_winner" do
    before do
      @request.account = @account = accounts(:minimum)
      @loser = users(:minimum_end_user)
      @loser.stubs(:can_be_merged_as_loser?).returns(true)
      login(users(:minimum_end_user))
    end

    describe "with invalid password" do
      before do
        xhr :post, :merge_with_winner, params: { email: users(:minimum_author).email, password: "invalid_pass" }
      end

      it('responds with content_type js') { assert_equal 'text/javascript', @controller.response.content_type.to_s }
      it "returns form switch javascript" do
        assert_equal '$z(\'people/users/merge\').password_does_not_match("Password does not match. Try again.")();', @response.body
      end
    end

    describe "with correct password" do
      before do
        Zendesk::RedisStore.client.flushall
        @winner = users(:minimum_author)
        UserMergeJob.expects(:enqueue).with(@account.id, @winner.id, @loser.id)
        xhr :post, :merge_with_winner, params: { email: @winner.email, password: "123456" }
      end

      it('responds with content_type js') { assert_equal 'text/javascript', @controller.response.content_type.to_s }
      # should_set_the_flash_to /Succes/
      it "sets current user to winner" do
        assert_equal @winner, @controller.send(:current_user)
      end
    end

    describe "limit password attempts" do
      before do
        @winner = users(:minimum_author)
      end

      it "registers each password attempt" do
        Prop.expects(:throttle!).with(:password_attempt, @winner.id).once
        xhr :post, :merge_with_winner, params: { email: users(:minimum_author).email, password: "invalid_pass" }
      end

      it "clears past password attempts with correct password" do
        Prop.expects(:reset).with(:password_attempt, @winner.id).once
        xhr :post, :merge_with_winner, params: { email: @winner.email, password: "123456" }
      end
    end
  end

  describe "#verify" do
    before do
      @request.account = accounts(:with_groups)
      login('with_groups_agent1')
      @user = users(:with_groups_end_user)
      @user.identities.each do |identity|
        identity.update_attribute(:is_verified, false);
      end
      @identity = @user.identities.first
    end

    it "verifys the identity" do
      assert_equal false, @user.is_verified?
      get :verify, params: { id: @user.id, identity_id: @identity.id }
      @user.reload

      assert(@user.is_verified?)
    end

    it "does not add a random password when one is present" do
      refute @user.crypted_password.blank?

      User.any_instance.expects(:randomize_password).never
      get :verify, params: { id: @user.id, identity_id: @identity.id }
    end
  end

  describe "#find_or_create" do
    before do
      @request.account = accounts(:minimum)
      login(users(:minimum_agent))
      @user_params = { email: 'new_user@example.com', name: 'New User', phone: '555-5555', organization_id: organizations(:minimum_organization2).id }
      @existent_user = users(:minimum_end_user)
    end

    it 'returns 410 gone' do
      post :find_or_create, params: { user: @user_params }
      assert_response :gone
    end
  end

  describe 'An admin' do
    before do
      role_settings = @with_groups_admin.account.role_settings
      role_settings.update_attributes!(end_user_twitter_login: true)

      login(@with_groups_admin)
    end

    it "sees the add a Twitter handle manually link on end user" do
      get :edit, params: { id: @with_groups_end_user.id }
      assert_select 'a', href: /manual_twitter/
    end

    it "does not see the add a Twitter handle manually link for admins" do
      get :edit, params: { id: @with_groups_admin.id }
      assert_select "a#twitter_manually", count: 0
    end
  end

  describe 'An end-user' do
    before do
      login(@with_groups_end_user)
    end

    it "nevers see the add a Twitter handle manually link" do
      get :edit, params: { id: @with_groups_end_user.id }
      assert_select "a#twitter_manually", count: 0
    end
  end

  describe "Phone number identities" do
    before do
      @request.account = accounts(:with_groups)
      login("with_groups_agent1")
    end

    describe "#update_number" do
      before do
        @request.account = accounts(:with_groups)
        login("with_groups_agent1")
        @user = @with_groups_end_user
        @user.update_attribute(:phone, "+15551234567")
      end

      describe "with an existing direct line" do
        before do
          UserPhoneNumberIdentity.create!(user: @user, value: @user.phone)
        end

        it "updates the direct line and creates secondary identity when phone number on profile is updated" do
          post :update_number, params: { id: @user.id, number: "15551234545", direct_line: 1 }
          assert_response :redirect

          @user.reload
          assert_equal ["+15551234567", "+15551234545"], @user.identities.phone.map(&:value)
        end
      end

      describe "without an existing direct line" do
        describe "with account setting is true" do
          before do
            Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
          end

          it "creates a new direct line when :direct_line is checked" do
            post :update_number, params: { id: @with_groups_end_user.id, number: "+15551234545", direct_line: 1 }
            assert_response :redirect
            assert_equal "+15551234545", @with_groups_end_user.identities(:reload).phone.first.value
          end

          it "does not create a new direct line when :direct_line is unchecked" do
            post :update_number, params: { id: @with_groups_end_user.id, number: "+15551234545", direct_line: 0 }
            assert_response :redirect
            assert_nil @with_groups_end_user.identities(:reload).phone.first
          end

          it "creates a new direct line when :direct_line is not present" do
            post :update_number, params: { id: @with_groups_end_user.id, number: "+15551234545" }
            assert_response :redirect
            assert_equal "+15551234545", @with_groups_end_user.identities(:reload).phone.first.value
          end

          it "does not create a new direct line when :direct_line is not present and phone number is not unique" do
            UserPhoneNumberIdentity.create!(user: @with_groups_admin, value: "+15551234777")
            post :update_number, params: { id: @with_groups_end_user.id, number: "+15551234777" }
            assert_response :redirect
            assert_nil @with_groups_end_user.identities(:reload).phone.first
          end
        end

        describe "with account setting is false" do
          before do
            Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(false)
          end

          it "creates a new direct line when :direct_line is checked" do
            post :update_number, params: { id: @with_groups_end_user.id, number: "+155", direct_line: 1 }
            assert_response :redirect
            assert_equal "+155", @with_groups_end_user.identities(:reload).phone.first.value
          end

          it "does not create a new direct line when :direct_line is unchecked" do
            post :update_number, params: { id: @with_groups_end_user.id, number: "+155", direct_line: 0 }
            assert_response :redirect
            assert_nil @with_groups_end_user.identities(:reload).phone.first
          end

          it "creates a new direct line when :direct_line is not present" do
            post :update_number, params: { id: @with_groups_end_user.id, number: "+155" }
            assert_response :redirect
            assert_equal "+155", @with_groups_end_user.identities(:reload).phone.first.value
          end

          it "does not create a new direct line when :direct_line is not present and phone number is not unique" do
            UserPhoneNumberIdentity.create!(user: @with_groups_admin, value: "+177")
            post :update_number, params: { id: @with_groups_end_user.id, number: "+177" }
            assert_response :redirect
            assert_nil @with_groups_end_user.identities(:reload).phone.first
          end
        end
      end
    end
  end

  describe 'an archived ticket' do
    before do
      @archived_ticket = tickets(:minimum_5)
      archive_and_delete(@archived_ticket)
      @request.account = accounts(:minimum)
      @user = @archived_ticket.requester

      login(users(:minimum_admin))
    end

    it "redirects to lotus" do
      @archived_ticket.collaborations.create(user: @user)
      get :show, params: { id: @user.id, select: 'ccd' }
      assert_redirected_to "/agent/users/#{@user.id}"
    end
  end

  [:index, :update, :assume, :select_bulk_action, :edit, :bulk, :choose_locale].each do |action|
    describe ":#{action}" do
      it "is ssl_allowed" do
        assert @controller.class.ssl_allowed_actions.include?(action)
      end
    end
  end

  describe "permissons" do
    describe "#index" do
      before do
        @request.account = accounts(:minimum)
        login('minimum_agent')
      end

      describe "for an agent who has the permission to view a user listing" do
        before do
          Access::Policies::UserPolicy.any_instance.stubs(:list?).returns(true)
        end

        it "is allowed" do
          get :index
          assert_response :ok
        end
      end

      describe "for an agent who doesn't have the permission to view a user listing" do
        before do
          Access::Policies::UserPolicy.any_instance.stubs(:list?).returns(false)
        end

        it "is not allowed" do
          get :index
          assert_response :forbidden
        end
      end
    end

    describe "#autocomplete" do
      before do
        @request.account = accounts(:minimum)
        login('minimum_agent')
      end

      describe "for an agent who has the permission to view a user search listing" do
        before do
          Access::Policies::UserPolicy.any_instance.stubs(:lookup?).returns(true)
        end

        it "is allowed" do
          get :autocomplete
          assert_response :ok
        end
      end

      describe "for an agent who doesn't have the permission to view a user search listing" do
        before do
          Access::Policies::UserPolicy.any_instance.stubs(:lookup?).returns(false)
        end

        it "is not allowed" do
          get :autocomplete
          assert_response :forbidden
        end
      end
    end

    describe "#new" do
      before do
        @request.account = accounts(:minimum)
        login('minimum_agent')
      end

      it 'returns 410 gone' do
        get :new
        assert_response :gone
      end
    end

    describe "#edit" do
      before do
        @request.account = accounts(:minimum)
        login('minimum_agent')
      end

      describe "for an agent who has the permission to edit a user" do
        before do
          user = users('minimum_end_user')
          Access::Policies::UserPolicy.any_instance.stubs(:edit?).with(user).returns(true)
        end

        it "is allowed" do
          get :edit, params: { id: users('minimum_end_user').id }
          assert_response :ok
        end
      end

      describe "for an agent who doesn't have the permission to edit a user" do
        before do
          Access::Policies::UserPolicy.any_instance.stubs(:edit?).returns(false)
        end

        it "is not allowed" do
          get :edit, params: { id: users('minimum_end_user').id }
          assert_response :forbidden
        end
      end
    end

    describe "#show" do
      before do
        @request.account = accounts(:minimum)
        @user = users('minimum_end_user')
        login('minimum_agent')
      end

      describe "for an agent who has the permission to view user profiles" do
        before do
          Access::Policies::UserPolicy.any_instance.stubs(:view?).with(@user).returns(true)
        end

        it "gets redirected to lotus" do
          get :show, params: { id: @user.id }
          assert_redirected_to "/agent/users/#{@user.id}"
        end
      end

      describe "for an agent who doesn't have the permission to view user profiles" do
        before do
          Access::Policies::UserPolicy.any_instance.stubs(:view?).returns(false)
        end

        it "is not allowed" do
          get :show, params: { id: @user.id }
          assert_response :forbidden
        end
      end
    end

    describe "#create with mocked abilities" do
      before do
        @request.account = accounts(:minimum)
        login('minimum_agent')
      end

      it 'returns 410 gone' do
        post :create, params: { user: { name: 'Mickey Mouse', email: 'mickey@aghassipour.com', roles: Role::END_USER.id } }
        assert_response :gone
      end
    end

    describe "#create" do
      before do
        @request.account = accounts(:minimum)
        login('minimum_agent')
      end

      it 'returns 410 gone' do
        post :create, params: { user: { name: 'Mickey Mouse', email: 'mickey@aghassipour.com', roles: Role::END_USER.id } }
        assert_response :gone
      end
    end

    describe "#update with mocked abilities" do
      before do
        @request.account = accounts(:minimum)
        login('minimum_agent')
      end

      describe "for an agent who has the permission to update a user" do
        before do
          Access::Policies::UserPolicy.any_instance.stubs(:edit?).returns(true)
        end

        it "is allowed" do
          post :update, params: { id: users(:minimum_admin_not_owner).id, user: { name: 'Mickey Mouse'} }
          assert_response :redirect
        end
      end

      describe "for an agent who doesn't have the permission to update a user" do
        before do
          Access::Policies::UserPolicy.any_instance.stubs(:edit?).returns(false)
        end

        it "is not allowed" do
          post :update, params: { id: users(:minimum_admin_not_owner).id, user: { name: 'Mickey Mouse'} }
          assert_response :forbidden
        end
      end

      describe "for an agent who has the permission to edit a user's password" do
        before do
          Access::Policies::UserPolicy.any_instance.stubs(:edit?).returns(true)
          Access::Policies::UserPolicy.any_instance.stubs(:edit_password?).returns(true)
        end

        it "is allowed to change the password" do
          User.any_instance.expects(:"password=")
          post :update, params: { id: users(:minimum_admin_not_owner).id, password: "123456", user: { name: 'Mickey Mouse' } }
          assert_response :redirect
        end
      end

      describe "for an agent who doesn't have the permission to edit a user's password" do
        before do
          Access::Policies::UserPolicy.any_instance.stubs(:edit?).returns(true)
          Access::Policies::UserPolicy.any_instance.stubs(:edit_password?).returns(false)
        end

        it "is not allowed to change the password" do
          User.any_instance.expects(:"password=").never
          post :update, params: { id: users(:minimum_admin_not_owner).id, password: "foo", user: { name: 'Mickey Mouse'} }
          assert_response :redirect
        end
      end
    end

    describe "#update" do
      before do
        @request.account = accounts(:minimum)
      end

      describe "for a non-admin agent" do
        before do
          login('minimum_agent')
        end

        it "is allowed to update users" do
          post :update, params: { id: users(:minimum_end_user), user: { name: 'Mickey Mouse' } }
          assert_response :redirect
        end

        it "is not allowed to update admin users" do
          post :update, params: { id: users(:minimum_admin), user: { name: 'Mickey Mouse' } }
          assert_response :forbidden
        end
      end

      describe "for an admin agent" do
        before do
          login('minimum_admin')
        end

        it 'updates phone and identity when phone is not blank' do
          user = users(:minimum_end_user)
          People::UsersController.any_instance.stubs(user_initializer: stub(apply: user))

          user.expects(:update_phone_and_identity).with('+383899760155', true)

          post :update, params: { id: user.id, user: { phone: '+383899760155' } }
        end

        it 'does not try to update phone and identity when phone is blank' do
          user = users(:minimum_end_user)
          People::UsersController.any_instance.stubs(user_initializer: stub(apply: user))

          user.expects(:update_phone_and_identity).never

          post :update, params: { id: user.id, user: { phone: '' } }
        end

        it "is allowed to update users" do
          post :update, params: { id: users(:minimum_end_user).id, user: { name: 'Mickey Mouse' } }
          assert_response :redirect
        end

        it "is allowed to update admin users" do
          post :update, params: { id: users(:minimum_admin_not_owner).id, user: { name: 'Mickey Mouse' } }
          assert_response :redirect
        end

        describe "when not the account owner" do
          before do
            login('minimum_admin_not_owner')
          end

          it "is not allowed to update account owner" do
            post :update, params: { id: accounts(:minimum).owner.id, user: { name: 'Mickey Mouse' } }
            assert_response :forbidden
          end
        end
      end

      describe "for an agent who doesn't have the permission to edit a user" do
        before do
          @request.account = accounts(:minimum)
          login('minimum_agent')
          Access::Policies::UserPolicy.any_instance.stubs(:edit?).returns(false)
        end

        it "is not allowed" do
          post :update, params: { id: users(:minimum_admin_not_owner).id, user: { name: 'Mickey Mouse' } }
          assert_response :forbidden
        end
      end
    end

    describe "#select_bulk_action" do
      let(:account) { accounts(:minimum) }
      let(:admin) { users(:minimum_admin) }
      let(:user1) { users(:minimum_agent) }
      let(:user2) { users(:minimum_end_user) }

      before do
        @request.account = @account = nil
        login(admin)
      end

      it "accepts an array of strings for user_ids" do
        post :select_bulk_action, params: { users: { ids: [user1.id.to_s, user2.id.to_s] } }

        assert_response :success
      end

      it "accepts an array of a single user_id as a string" do
        post :select_bulk_action, params: { users: { ids: [user1.id.to_s] } }

        assert_response :success
      end

      it "accepts an array of integers for user_ids" do
        post :select_bulk_action, params: { users: { ids: [user1.id, user2.id] } }

        assert_response :success
      end

      it "accepts an array of a single user_id as a integer" do
        post :select_bulk_action, params: { users: { ids: [user1.id] } }

        assert_response :success
      end
    end

    describe "#bulk" do
      let(:account) { accounts(:minimum) }
      let(:admin) { users(:minimum_admin) }
      let(:user1) { users(:minimum_agent) }
      let(:user2) { users(:minimum_end_user) }
      let(:notification) { mock('notification') }

      describe "low_seat_radar_notification" do
        before do
          @request.account = @account = nil
          login(admin)
          @permission_set = PermissionSet.create(account: account, name: "admin_permisson")
        end

        describe_with_arturo_disabled :low_agent_seat_radar_notification do
          it "radar notification not trigger" do
            RadarFactory.expects(:create_radar_notification).never
            put :bulk, params: { users: { permission_set: @permission_set.id, ids: [user1.id, user2.id]} }
          end
        end

        describe_with_arturo_enabled :low_agent_seat_radar_notification do
          let(:billable_agent_count) { 0 }

          before do
            Account.any_instance.stubs(:multiproduct_billable_agent_count!).returns(billable_agent_count)
          end

          describe 'when seats remaining less than or equal to 0' do
            let(:billable_agent_count) { 10 } # fixture account has max_agents 10

            it 'triggers notification' do
              ::RadarFactory.expects(:create_radar_notification).returns(notification).once
              notification.expects(:send)
              put :bulk, params: { users: { permission_set: @permission_set.id, ids: [user1.id, user2.id]} }
            end
          end

          describe 'when seats remaining is more than 0' do
            let(:billable_agent_count) { 1 }

            it 'does not trigger notification' do
              ::RadarFactory.expects(:create_radar_notification).returns(notification).never
              put :bulk, params: { users: { permission_set: @permission_set.id, ids: [user1.id, user2.id]} }
            end
          end

          describe 'when seats response count returned nil' do
            let(:billable_agent_count) { nil }

            it 'does not trigger notification' do
              ::RadarFactory.expects(:create_radar_notification).returns(notification).never
              put :bulk, params: { users: { permission_set: @permission_set.id, ids: [user1.id, user2.id]} }
            end
          end
        end
      end

      describe "as admin" do
        before do
          @request.account = @account = nil
          login(admin)
          @permission_set = PermissionSet.create(account: account, name: "ticket_triager")
        end

        it "records the actor as the current user, not the system user in the Audit" do
          put :bulk, params: { users: { permission_set: @permission_set.id, ids: [user1.id, user2.id]} }

          assert_equal admin.id, CIA::Event.last.actor_id
        end
      end

      describe "as end user" do
        before do
          @request.account = @account = nil
          login(user2)
          @permission_set = 'admin'
        end

        it "forbid non-admins from bulk updating" do
          put :bulk, params: { users: { permission_set: @permission_set, ids: [user2.id]} }
          assert_response :forbidden
        end
      end
    end
  end
end
