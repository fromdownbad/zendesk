require_relative "../../support/test_helper"

SingleCov.covered!

describe People::SearchController do
  fixtures :accounts, :users, :subscriptions, :role_settings

  should_include_module Zendesk::LotusEmbeddable

  before do
    @request.account = accounts(:minimum)
  end

  describe "When logged in as an admin" do
    before do
      login(:minimum_admin)
      ZendeskSearch::Client.any_instance.stubs(:search).returns(count: 0, results: [])
    end

    describe "Viewing the search page" do
      before do
        get :index
      end

      before_should "register activity on the current device" do
        @controller.expects(:register_device_activity)
      end
    end
  end

  describe "Searching for people" do
    it "succeeds for admins" do
      admin = login('minimum_admin')
      options = {per_page: 30, page: 1, sort_mode: :relevance,
                 type: :people, hl: nil, incremental: true, without: {type: :article}}

      Zendesk::Search.expects(:search).with(admin, 'hi', options).returns(Zendesk::Search::Results.empty)
      get :index, params: { query: 'hi' }
      assert_response :success
    end

    it "is denied to non-admins" do
      login('minimum_agent')
      get :index, params: { query: 'hi' }

      assert_response :forbidden
    end

    describe "optimizing performance for an account with many users" do
      before do
        login('minimum_admin')
        @request.account.stubs(:users).returns(stub(capped_count: Zendesk::CappedCounts::CappedNum.new(10, 10)))
      end

      it "does not perform the search when it's blank" do
        Zendesk::Search.expects(:search).never
        get :index
        assert_response :success
      end

      it "performs the search when it's not blank" do
        Zendesk::Search.expects(:search).returns(Zendesk::Search::Results.empty)
        get :index, params: { query: 'hi' }
        assert_response :success
      end
    end

    describe "for an account without many users" do
      before do
        login('minimum_admin')
        @request.account.stubs(:capped_user_count).returns(99_000)
      end

      it "performs blank searches" do
        Zendesk::Search.expects(:search).returns(Zendesk::Search::Results.empty)
        get :index
        assert_response :success
      end
    end
  end
end
