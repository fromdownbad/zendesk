require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 10

describe People::RolesController do
  fixtures :accounts, :subscriptions, :users, :addresses, :role_settings

  before do
    Account.any_instance.stubs(:has_permission_sets?).returns(true)
    @request.account = accounts(:minimum)
    login('minimum_admin')
  end

  should_include_module Zendesk::LotusEmbeddable

  describe "authorization" do
    before { logout }

    describe "admins" do
      before do
        login('minimum_admin')
        get :index
      end
      it "is allowed" do
        assert_template :index
        assert_response :success
      end
    end

    describe "agents" do
      before do
        login('minimum_agent')
        get :index
      end
      it "is denied" do
        assert_response :forbidden
      end
    end

    describe "end users" do
      before do
        login('minimum_end_user')
        get :index
      end
      it "is denied" do
        assert_response :forbidden
      end
    end
  end

  describe "#index" do
    before do
      get :index
    end
    it "renders" do
      assert_template :index
      assert_response :success
    end
  end

  describe "#new" do
    before do
      PermissionSet.expects(:build_new_permission_set).with(@request.account).returns(@request.account.permission_sets.new)
    end

    it "renders" do
      get :new
      assert_template :edit
      assert_response :success
    end

    describe "with permissions_roles_redesign enabled" do
      before do
        Arturo.enable_feature! :permissions_roles_redesign
      end

      it "renders new edit view" do
        get :new
        assert_layout "roles"
        assert_template :new_edit
        assert_response :success
      end
    end
  end

  describe "#show" do
    before do
      permission_set = accounts(:minimum).permission_sets.create!(name: "sfsf", description: "sdfdf")
      permission_set.expects(:translated_name).returns('krampus').at_least_once
      permission_set.expects(:translated_description).returns('wampus')
      association_mock = mock
      association_mock.expects(:find_by_id).with('54321').returns(permission_set)
      @request.account.expects(:permission_sets).returns(association_mock)
    end
    it "renders" do
      get :show, params: { id: 54321 }
      assert_template :edit
      assert_response :success
    end

    describe "with permissions_roles_redesign enabled" do
      before do
        Arturo.enable_feature! :permissions_roles_redesign
      end
      it "renders new edit view" do
        get :show, params: { id: 54321 }
        assert_layout "roles"
        assert_template :new_edit
        assert_response :success
      end
    end

    describe "when account has satisfaction prediction enabled" do
      before do
        @request.account.stubs(:has_satisfaction_prediction_enabled?).returns(true)
        get :show, params: { id: 54321 }
      end

      it "displays ticket show satisfaction prediction option" do
        assert_includes @response.body, I18n.t('txt.admin.views.people.roles.role_definition.can_view_ticket_satisfaction_prediction')
      end
    end

    describe "when account does not have satisfaction prediction enabled" do
      before do
        @request.account.stubs(:has_satisfaction_prediction_enabled?).returns(false)
        get :show, params: { id: 54321 }
      end

      it "does not display ticket show satisfaction prediction option" do
        refute_includes @response.body, I18n.t('txt.admin.views.people.roles.role_definition.can_view_ticket_satisfaction_prediction')
      end
    end
  end

  describe "#clone" do
    before do
      @source_permission_set = @request.account.permission_sets.new(name: "Source")
      @source_permission_set.permissions.disable(:ticket_editing)
      @source_permission_set.save!
    end

    it "prepopulates a new permission set with an existing permission set's values" do
      get :clone, params: { id: @source_permission_set.id }
      assert_select "input[name='permission_set[permissions][ticket_editing]'][value='0']", 1
    end

    describe "with permissions_roles_redesign enabled" do
      before do
        Arturo.enable_feature! :permissions_roles_redesign
      end

      it "renders new edit view" do
        get :clone, params: { id: @source_permission_set.id }
        assert_layout "roles"
        assert_template :new_edit
        assert_response :success
      end
    end
  end

  describe "#create" do
    let(:ps_name) { 'New Role' }
    let(:ps_description) { 'New description' }

    it "creates the permission set" do
      post :create, params: { permission_set: {name: ps_name, permissions: {ticket_editing: true}} }
      assert_redirected_to people_roles_path
      assert_response :redirect

      set = PermissionSet.last
      assert_equal ps_name, set.name
      assert_includes set.permissions.map { |p| [p.name, p.value] }, ["ticket_editing", "1"]
    end

    describe "when 'view_user_profile_lists' permission is checked" do
      it "creates the permission set with 'available_user_lists' permission set to 'all'" do
        post :create, params: { permission_set: {name: "View user profile lists all", permissions: {}, view_user_profile_lists: "1"} }
        assert_response :redirect, @response.body
        assert_redirected_to people_roles_path

        set = PermissionSet.last
        assert_equal "View user profile lists all", set.name
        assert_includes set.permissions.map { |p| [p.name, p.value] }, ["available_user_lists", "all"]
      end
    end

    describe "when 'view_user_profile_lists' permission is unchecked" do
      it "creates the permission set with 'available_user_lists' permission set to 'all'" do
        post :create, params: { permission_set: {name: "View user profile lists none", permissions: {}, view_user_profile_lists: "0"} }
        assert_response :redirect, @response.body
        assert_redirected_to people_roles_path

        set = PermissionSet.last
        assert_equal "View user profile lists none", set.name
        assert_includes set.permissions.map { |p| [p.name, p.value] }, ["available_user_lists", "none"]
      end
    end

    describe "when dual save is enabled on the account" do
      let(:subdomain) { 'test' }
      let(:user_id) { 10011 }
      let(:client_options) { { circuit_options: { failure_threshold: 10000, reset_timeout: 5 } } }
      let(:permissions_set_policies_helper) { Zendesk::ExternalPermissions::PermissionSetPoliciesHelper.new(subdomain, user_id, client_options) }
      let(:subdomain_policies_url) { permissions_set_policies_helper.policies_url }

      before do
        Account.any_instance.stubs(:has_permissions_custom_role_policy_dual_save?).returns(true)
        Arturo.enable_feature! :permissions_roles_redesign
      end

      it "creates the permission set and calls permissions service to create policy" do
        stub_request = stub_request(:post, subdomain_policies_url).to_return(status: 200)
        post :create, params: { permission_set: {name: ps_name, description: ps_description, permissions: {ticket_editing: true}} }
        assert_requested stub_request, times: 1
        assert_redirected_to people_roles_path
        assert_response :redirect

        set = PermissionSet.last
        assert_equal ps_name, set.name
        assert_equal ps_description, set.description
        assert_includes set.permissions.map { |p| [p.name, p.value] }, ["ticket_editing", "1"]
      end

      it "undoes permission_set creation and redirects to role edit page if call to permission service throws an error" do
        assert_difference('PermissionSet.count(:all)', 0) do
          stub_request(:post, subdomain_policies_url).to_raise(Kragle::ResponseError)
          post :create, params: { permission_set: {name: ps_name, description: ps_description, permissions: {ticket_editing: true}} }
          assert_layout "roles"
          assert_template :new_edit
          assert_response :success
        end
      end

      it "undoes permission_set creation and redirects to role edit page if call to permission service is unsuccessful" do
        assert_difference('PermissionSet.count(:all)', 0) do
          stub_request(:post, subdomain_policies_url).to_return(status: 300)
          post :create, params: { permission_set: {name: ps_name, description: ps_description, permissions: {ticket_editing: true}} }
          assert_layout "roles"
          assert_template :new_edit
          assert_response :success
        end
      end
    end

    describe "when help_center is enabled on the account" do
      before do
        https!
        Account.any_instance.stubs(:help_center_enabled?).returns(true)
      end

      it "creates the permission set" do
        post :create, params: { permission_set: {name: "Help Center", permissions: {}, manage_help_center: "1"} }
        assert_response :redirect, @response.body
        assert_redirected_to people_roles_path

        set = PermissionSet.last
        assert_equal "Help Center", set.name
        assert_includes set.permissions.map { |p| [p.name, p.value] }, ["forum_access", "full"]
      end
    end

    it 'syncs the new role to Pravda' do
      pravda_role_name = 'role_name'
      PermissionSet.any_instance.expects(:pravda_role_name).returns(pravda_role_name)
      Omnichannel::RoleSyncJob.expects(:enqueue).with(has_entries(
        account_id: @request.account.id,
        role_name: pravda_role_name,
        context: 'lotus_create',
        permissions_changed: []
      ))
      post :create, params: { permission_set: { name: ps_name, description: ps_description, permissions: { ticket_editing: true } } }
    end
  end

  describe "#update" do
    before do
      User.any_instance.stubs(:can?).returns(true)
      @permission_set = PermissionSet.create(account: accounts(:minimum), name: "things")
    end

    describe "an editable role" do
      before do
        User.any_instance.expects(:can?).with(:edit, @permission_set).returns(true)
        PermissionSet.any_instance.expects(:assign_attributes).returns(:true)
        put :update, params: { id: @permission_set.id, permission_set: { foo: 'bar' } }
      end

      should_redirect_to("the people roles page") { people_roles_path }
    end

    describe "a non editable role" do
      before do
        User.any_instance.expects(:can?).with(:edit, @permission_set).returns(false)
        put :update, params: { id: @permission_set.id, permission_set: { foo: 'bar' } }
      end

      it('responds with forbidden') { assert_response :forbidden }
    end

    describe "when dual save is enabled on the account" do
      let(:subdomain) { 'test' }
      let(:user_id) { 10011 }
      let(:client_options) { { circuit_options: { failure_threshold: 10000, reset_timeout: 5 } } }
      let(:permissions_set_policies_helper) { Zendesk::ExternalPermissions::PermissionSetPoliciesHelper.new(subdomain, user_id, client_options) }
      let(:subdomain_policies_url) { permissions_set_policies_helper.policies_url }

      before do
        Account.any_instance.stubs(:has_permissions_custom_role_policy_dual_save?).returns(true)
        Arturo.enable_feature! :permissions_roles_redesign
        User.any_instance.expects(:can?).with(:edit, @permission_set).returns(true)
        PermissionSet.any_instance.expects(:assign_attributes).returns(:true)
      end

      it "calls permissions service to update policy then updates permission set - policy exists" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
            status: 200,
            body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_update_policy_request = stub_request(:put, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_return(status: 200)
        put :update, params: { id: @permission_set.id, permission_set: { name: @permission_set.name, description: @permission_set.description, permissions: { ticket_editing: true } } }
        assert_requested stub_get_policy_request, times: 1
        assert_requested stub_update_policy_request, times: 1
        assert_redirected_to people_roles_path
        assert_response :redirect
      end

      it "calls permissions service to update policy then updates permission set - policy does not exist" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
            status: 200,
            body: { policies: [] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_create_policy_request = stub_request(:post, subdomain_policies_url).to_return(
            status: 200,
            body: { policy: { id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] } }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        put :update, params: { id: @permission_set.id, permission_set: { name: @permission_set.name, description: @permission_set.description, permissions: { ticket_editing: true } } }
        assert_requested stub_get_policy_request, times: 1
        assert_requested stub_create_policy_request, times: 1
        assert_redirected_to people_roles_path
        assert_response :redirect
      end

      it "fails and renders role edit page if call to permission service to get policy by subject throws an error" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_raise(Kragle::ResponseError)
        put :update, params: { id: @permission_set.id, permission_set: { name: @permission_set.name, description: @permission_set.description, permissions: { ticket_editing: true } } }
        assert_requested stub_get_policy_request, times: 1
        assert_layout "roles"
        assert_template :new_edit
        assert_response :success
      end

      it "fails and renders role edit page if call to permission service to create non existent policy throws an error" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
            status: 200,
            body: { policies: [] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_create_policy_request = stub_request(:post, subdomain_policies_url).to_raise(Faraday::ClientError)
        put :update, params: { id: @permission_set.id, permission_set: { name: @permission_set.name, description: @permission_set.description, permissions: { ticket_editing: true } } }
        assert_requested stub_get_policy_request, times: 1
        assert_requested stub_create_policy_request, times: 1
        assert_layout "roles"
        assert_template :new_edit
        assert_response :success
      end

      it "fails and renders role edit page if call to permission service to update policy throws an error" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
            status: 200,
            body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_update_policy_request = stub_request(:put, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_raise(Faraday::ClientError)
        put :update, params: { id: @permission_set.id, permission_set: { name: @permission_set.name, description: @permission_set.description, permissions: { ticket_editing: true } } }
        assert_requested stub_get_policy_request, times: 1
        assert_requested stub_update_policy_request, times: 1
        assert_layout "roles"
        assert_template :new_edit
        assert_response :success
      end

      it "fails and renders role edit page if call to permission service to update policy is unsuccessful" do
        stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
            status: 200,
            body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
            headers: { "Content-Type" => "application/json" }
          )
        stub_update_policy_request = stub_request(:put, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_return(status: 300)
        put :update, params: { id: @permission_set.id, permission_set: { name: @permission_set.name, description: @permission_set.description, permissions: { ticket_editing: true } } }
        assert_requested stub_get_policy_request, times: 1
        assert_requested stub_update_policy_request, times: 1
        assert_layout "roles"
        assert_template :new_edit
        assert_response :success
      end
    end

    describe "when help_center is enabled on the account" do
      before do
        https!
        Account.any_instance.stubs(:help_center_enabled?).returns(true)
        post :create, params: { permission_set: {name: "New Role", permissions: {}, manage_help_center: "0"} }
      end

      it "updates the permission set" do
        set = PermissionSet.last
        assert_includes set.permissions.map { |p| [p.name, p.value] }, ["forum_access", "readonly"]

        put :update, params: { id: set.id, permission_set: { permissions: {}, manage_help_center: "1" } }
        assert_includes set.reload.permissions.map { |p| [p.name, p.value] }, ["forum_access", "full"]
      end
    end

    describe "when 'view_user_profile_lists' permission is checked" do
      before do
        https!
        post :create, params: { permission_set: { name: "New Role", permissions: {}, view_user_profile_lists: "0" } }
      end

      it "updates the permission set with 'available_user_lists' permission set to 'all'" do
        set = PermissionSet.last
        assert_includes set.permissions.map { |p| [p.name, p.value] }, ["available_user_lists", "none"]
        put :update, params: { id: set.id, permission_set: { permissions: {}, view_user_profile_lists: "1" } }
        assert_response :redirect, @response.body
        assert_redirected_to people_roles_path

        set = PermissionSet.last
        assert_includes set.reload.permissions.map { |p| [p.name, p.value] }, ["available_user_lists", "all"]
      end
    end

    describe "when 'view_user_profile_lists' permission is unchecked" do
      before do
        https!
        post :create, params: { permission_set: { name: "New Role", permissions: {}, view_user_profile_lists: "1" } }
      end

      it "updates the permission set with 'available_user_lists' permission set to 'none'" do
        set = PermissionSet.last
        assert_includes set.permissions.map { |p| [p.name, p.value] }, ["available_user_lists", "all"]
        put :update, params: { id: set.id, permission_set: { permissions: {}, view_user_profile_lists: "0" } }
        assert_response :redirect, @response.body
        assert_redirected_to people_roles_path

        assert_includes set.reload.permissions.map { |p| [p.name, p.value] }, ["available_user_lists", "none"]
      end
    end

    describe "when the account has satisfaction prediction enabled" do
      before do
        https!
        @request.account.stubs(:has_satisfaction_prediction_enabled?).returns(true)
      end

      it "updates the permission set to enable viewing ticket satisfaction prediction" do
        post :create, params: { permission_set: {name: "Prediction Agent", permissions: { view_ticket_satisfaction_prediction: "0" }} }
        set = PermissionSet.last
        refute set.permissions.view_ticket_satisfaction_prediction?

        put :update, params: { id: set.id, permission_set: { permissions: {view_ticket_satisfaction_prediction: "1"} } }
        assert set.reload.permissions.view_ticket_satisfaction_prediction?
      end

      it "updates the permission set to disable viewing ticket satisfaction prediction" do
        post :create, params: { permission_set: {name: "Non-Prediction Agent", permissions: { view_ticket_satisfaction_prediction: "1" }} }
        set = PermissionSet.last
        assert set.permissions.view_ticket_satisfaction_prediction?

        put :update, params: { id: set.id, permission_set: { permissions: {view_ticket_satisfaction_prediction: "0"} } }
        refute set.reload.permissions.view_ticket_satisfaction_prediction?
      end
    end

    it 'syncs the new role to Pravda with changed permissions' do
      Omnichannel::RoleSyncJob.expects(:enqueue).with(
        account_id: @request.account.id,
        permission_set_id: @permission_set.id,
        role_name: @permission_set.pravda_role_name,
        context: 'lotus_update',
        permissions_changed: ['view_ticket_satisfaction_prediction']
      )
      put :update, params: { id: @permission_set.id, permission_set: { permissions: {view_ticket_satisfaction_prediction: "0"} } }
    end
  end

  describe "#destroy" do
    before do
      User.any_instance.stubs(:can?).returns(true)
      @permission_set = PermissionSet.create(account: accounts(:minimum), name: "things")
    end

    describe "a deletable role" do
      before do
        User.any_instance.expects(:can?).with(:delete, @permission_set).returns(true)
      end

      describe "with members" do
        before do
          users(:minimum_agent).permission_set = @permission_set
          users(:minimum_agent).save!
          delete :destroy, params: { id: @permission_set.id }
        end

        should_redirect_to("the edit role page") { edit_people_role_path(@permission_set) }
        should_set_the_flash_to :error
        should_not_change("the number of PermissionSets") { PermissionSet.count(:all) }
      end

      describe "without members" do
        before { delete :destroy, params: { id: @permission_set.id } }

        should_destroy :permission_set
        should_redirect_to("the people roles page") { people_roles_path }
      end
    end

    describe "a non deletable role" do
      before do
        User.any_instance.expects(:can?).with(:delete, @permission_set).returns(false)
        delete :destroy, params: { id: @permission_set.id }
      end

      should_not_change("the number of PermissionSets") { PermissionSet.count(:all) }
      it('responds with forbidden') { assert_response :forbidden }
    end

    describe "when dual save is enabled on the account" do
      let(:subdomain) { 'test' }
      let(:user_id) { 10011 }
      let(:client_options) { { circuit_options: { failure_threshold: 10000, reset_timeout: 5 } } }
      let(:permissions_set_policies_helper) { Zendesk::ExternalPermissions::PermissionSetPoliciesHelper.new(subdomain, user_id, client_options) }
      let(:subdomain_policies_url) { permissions_set_policies_helper.policies_url }

      before do
        Account.any_instance.stubs(:has_permissions_custom_role_policy_dual_save?).returns(true)
        Arturo.enable_feature! :permissions_roles_redesign
        User.any_instance.expects(:can?).with(:delete, @permission_set).returns(true)
      end

      it "calls permissions service to delete policy then deletes permission_set" do
        assert_difference('PermissionSet.count(:all)', -1) do
          stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
              status: 200,
              body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
              headers: { "Content-Type" => "application/json" }
            )
          stub_delete_policy_request = stub_request(:delete, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_return(status: 204)
          delete :destroy, params: { id: @permission_set.id }
          assert_requested stub_get_policy_request, times: 1
          assert_requested stub_delete_policy_request, times: 1
          assert_redirected_to people_roles_path
          assert_response :redirect
        end
      end

      it "calls permissions service to delete policy then deletes permission_set if policy resource_not_found on deletion" do
        assert_difference('PermissionSet.count(:all)', -1) do
          stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
              status: 200,
              body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
              headers: { "Content-Type" => "application/json" }
            )
          stub_delete_policy_request = stub_request(:delete, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_raise(Kragle::ResourceNotFound)
          delete :destroy, params: { id: @permission_set.id }
          assert_requested stub_get_policy_request, times: 1
          assert_requested stub_delete_policy_request, times: 1
          assert_redirected_to people_roles_path
          assert_response :redirect
        end
      end

      it "fails and redirects to role edit page if call to permissions service to get policy (for deletion) fails" do
        assert_difference('PermissionSet.count(:all)', 0) do
          stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_raise(Kragle::ServerError)
          delete :destroy, params: { id: @permission_set.id }
          assert_requested stub_get_policy_request, times: 1
          assert_redirected_to edit_people_role_path(@permission_set)
          assert_response :redirect
        end
      end

      it "fails and redirects to role edit page if call to permissions service to delete policy fails" do
        assert_difference('PermissionSet.count(:all)', 0) do
          stub_get_policy_request = stub_request(:get, "#{subdomain_policies_url}?subject_id=#{@permission_set.id}&subject_type=custom_role").to_return(
              status: 200,
              body: { policies: [{ id: '01EHD5H0YHC81MYDR45Y1R1SYF', name: 'test_policy', subject: { id: @permission_set.id, type: 'custom_role' }, statements: [{ effect: 'allow', resource: 'tickets', scopes: ['read'] }] }] }.to_json,
              headers: { "Content-Type" => "application/json" }
            )
          stub_delete_policy_request = stub_request(:delete, "#{subdomain_policies_url}/01EHD5H0YHC81MYDR45Y1R1SYF").to_raise(Kragle::ServerError)
          delete :destroy, params: { id: @permission_set.id }
          assert_requested stub_get_policy_request, times: 1
          assert_requested stub_delete_policy_request, times: 1
          assert_redirected_to edit_people_role_path(@permission_set)
          assert_response :redirect
        end
      end
    end

    it 'syncs the new role to Pravda' do
      Omnichannel::RoleSyncJob.expects(:enqueue).with(
        account_id: @request.account.id,
        permission_set_id: @permission_set.id,
        role_name: @permission_set.pravda_role_name,
        context: 'lotus_destroy',
        permissions_changed: []
      )
      delete :destroy, params: { id: @permission_set.id }
    end
  end
end
