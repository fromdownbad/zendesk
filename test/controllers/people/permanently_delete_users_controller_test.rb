require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 2

describe People::PermanentlyDeleteUsersController do
  fixtures :all

  let(:deleted_user) { users(:minimum_end_user2) }
  let(:active_user) { users(:minimum_end_user) }
  let(:current_account) { active_user.account }
  let(:current_user) { @controller.send(:current_user) }
  let(:zendesk_agent) do
    FactoryBot.create(:agent,
      account: current_account,
      name: "Double Agent")
  end

  before do
    @agent = users(:minimum_agent)
    ComplianceDeletionStatus.any_instance.stubs(:publish_to_world).returns(true)
  end

  as_an_admin do
    before do
      # Setup Account to have at least one deleted user
      deleted_user.current_user = zendesk_agent
      deleted_user.delete!
    end

    describe "#index" do
      it 'should be successful' do
        get :index
        assert_response :success
      end

      it 'should be able to sort by name' do
        get :index, params: { order: 'name' }
        assert_response :success
      end

      it 'should be able to sort by name' do
        get :index, params: { order: 'id' }
        assert_response :success
      end
    end

    describe "#show" do
      let(:ticket) { tickets(:minimum_1) }

      before do
        ticket.assignee_id = deleted_user.id
        ticket.save
      end

      it 'should render json' do
        get :show, params: { id: deleted_user.id }
        assert_response :success
      end

      it 'should not show an active user' do
        get :show, params: { id: active_user.id }
        assert_response :forbidden
      end
    end

    describe "#destroy" do
      it 'should not delete an active user' do
        delete :destroy, params: { id: active_user.id }
        assert_response :forbidden
      end

      it 'should delete an inactive user' do
        delete :destroy, params: { id: deleted_user.id }
        assert_redirected_to people_permanently_delete_users_path
        refute !!current_account.all_users.inactive_and_not_redacted.find_by_id(deleted_user.id)
      end
    end
  end
end
