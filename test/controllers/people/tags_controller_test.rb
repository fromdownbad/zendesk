require_relative "../../support/test_helper"

SingleCov.covered!

describe People::TagsController do
  fixtures :organizations, :users, :taggings, :accounts, :role_settings

  should_include_module Zendesk::LotusEmbeddable

  before do
    @request.account = accounts(:minimum)
    login('minimum_admin')
  end

  describe "#index" do
    before { get :index }
    it('responds with success') { assert_response :success }
  end

  describe "#destroy" do
    describe "with a valid id" do
      before { delete :destroy, params: { id: 'beta.test' } }
      should_change("the Tagging count", by: -3) { Tagging.count(:all) }
      it('responds with redirect') { assert_response :redirect }
    end
  end
end
