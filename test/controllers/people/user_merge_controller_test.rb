require_relative "../../support/test_helper"
# require_relative "../../support/search_controller_test_helper"

SingleCov.covered! uncovered: 12

describe People::UserMergeController do
  include DomainEventsHelper
  let(:domain_event_publisher) { FakeEscKafkaMessage.new }

  fixtures :accounts, :addresses, :subscriptions, :remote_authentications,
    :users, :user_identities, :groups, :translation_locales, :tickets,
    :organizations, :channels_user_profiles, :role_settings

  should_be_unauthorized_when_not_logged_in([:new, :show, :create, {add_to_all: {user_id: 1}}])

  should_be_unauthorized_when_not_logged_in([:new, :show, :create, {add_to_all: {user_id: 1}}], 'minimum_end_user', 403)

  should_route :get, "/users/1/merge/new", controller: 'people/user_merge', action: :new, user_id: "1"
  should_route :get, "/users/1/merge", controller: 'people/user_merge', action: :show, user_id: "1"
  should_route :post, "/users/1/merge", controller: 'people/user_merge', action: :create, user_id: "1"

  with_options(controller: "people/user_merge") do |request|
    request.should_route :get, "/users/1/merge/autocomplete", action: 'autocomplete', user_id: 1
  end

  before do
    User.any_instance.stubs(domain_event_publisher: domain_event_publisher)
    Zendesk::RedisStore.client.flushall
    @request.account = accounts(:minimum)
  end

  describe "#new" do
    before do
      login('minimum_agent')
      @loser = users(:minimum_author)
      @suggested_user = users(:minimum_end_user)
      Zendesk::Search.stubs(:search).returns(WillPaginate::Collection.create(1, 10) { |p| p.replace([@suggested_user]) })
    end

    describe "with loser id" do
      before { get :new, params: { user_id: @loser.to_param } }
      it('responds with success') { assert_response :success }
      should_render_template :new
      should_assign_to(:users) { [@suggested_user] }
    end

    describe "without loser id" do
      before { get :new, params: { user_id: 111 } }
      should_render_template "people/user_merge/_error"
    end

    describe "with remote auth and an external ID" do
      before do
        @loser.stubs(:login_allowed?).with(:remote).returns(true)
        @loser.external_id = 123456
        @loser.save!
        get :new, params: { user_id: @loser.to_param }
      end
      should_render_template :new
    end
  end

  describe "#autocomplete" do
    before do
      login('minimum_agent')
      @loser = users(:minimum_author)
      @user = users(:minimum_end_user)
      @email = "email@example.com"
      @twitter = "@screen_name"
      Zendesk::Search.stubs(:search).returns(WillPaginate::Collection.create(1, 10) { |p| p.replace([@user]) })
    end

    describe "returning users with email address" do
      before do
        User.any_instance.stubs(:main_identity).returns(@email)
        get :autocomplete, params: { user_id: @loser.to_param, name: "hey" }
      end
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
      it "responds with the user name and the email address" do
        assert @response.body.include? "#{@user.name} <#{@email}>"
      end
    end

    describe "returning users with twitter account" do
      before do
        User.any_instance.stubs(:main_identity).returns(@twitter)
        get :autocomplete, params: { user_id: @loser.to_param, name: "hey" }
      end
      it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }
      it "responds with the user name and the email address" do
        assert @response.body.include? "#{@user.name} <#{@twitter}>"
      end
    end
  end

  describe "#show" do
    before do
      login('minimum_agent')
      @loser = users(:minimum_author)
      @winner = users(:minimum_end_user)
    end

    describe "with loser and winner id" do
      before { get :show, params: { user_id: @loser.to_param, winner: @winner.to_param } }
      it('responds with success') { assert_response :success }
      should_render_template :show
    end

    describe "with loser id and winner from autocomplete with email address" do
      before do
        @winner.name = "123 by me"
        @winner.identities.first { |i| i.type == "UserEmailIdentity" && i.priority == 1 }.tap { |i| i.value = "123byme@example.com" }.save
        get :show, params: { user_id: @loser.to_param, winner: "#{@winner.name} <#{@winner.email}>" }
      end
      it('responds with success') { assert_response :success }
      should_render_template :show
    end

    describe "with loser id and winner from email address" do
      before { get :show, params: { user_id: @loser.to_param, winner: @winner.email } }
      it('responds with success') { assert_response :success }
      should_render_template :show
    end

    describe "with loser id and winner from autocomplete with twitter account" do
      before do
        profile = channels_user_profiles(:minimum_twitter_user_profile_1)
        profile.update_column(:external_id, @winner.identities.twitter.first.value)
        get :show, params: { user_id: @loser.to_param, winner: "#{@winner.name} <#{profile.screen_name}>" }
      end
      it('responds with success') { assert_response :success }
      should_render_template :show
    end

    describe "with invalid loser id" do
      before { get :show, params: { user_id: 111, winner: @winner.to_param } }
      should_render_template "people/user_merge/_error"
    end

    describe "without winner id" do
      before { get :show, params: { user_id: @loser.to_param, winner: "" } }
      it "responds with an error message" do
        assert @response.body.include? "User not found"
      end
    end
  end

  describe "#find_winner" do
    before do
      @controller.stubs(current_account: @request.account)
      @controller.params[:winner] = '"><img src="http://cdn.zendesk.com/images/misc/highlight_circle_helpcenter.png" alt="Test"><h1>Test</h1>Only'
    end

    it "calls show_error without displaying user input" do
      @controller.expects(:show_error).with("User not found")
      @controller.send(:find_winner)
    end
  end

  describe "#create" do
    before do
      login('minimum_agent')
      @loser = users(:minimum_author)
      @winner = users(:minimum_end_user)
    end

    describe "using html format" do
      describe "with successfull merge" do
        before do
          post :create, params: { user_id: @loser.to_param, winner: @winner.to_param }
        end
        it('responds with ok') { assert_response :ok }
        it "emits user_merged domain events" do
          user_merged_events = decode_user_events(domain_event_publisher.events, :user_merged)
          assert_equal user_merged_events.size, 1
          assert_equal user_merged_events.first.user_merged.winner.id.value, @winner.id
        end
      end

      describe "with failed merge" do
        before do
          Users::Merge.expects(:validate_and_merge).with(@winner, @loser).returns(:loser_non_enduser)
          post :create, params: { user_id: @loser.to_param, winner: @winner.to_param }
        end
        should_redirect_to("the loser page") { user_path(@loser) }
        should_set_the_flash_to /problem during merge/
        it "does not emit user_merged domain events" do
          assert_equal decode_user_events(domain_event_publisher.events, :user_merged).size, 0
        end
      end
    end

    describe "using js format" do
      describe "with succesfull merge" do
        before do
          Users::Merge.expects(:validate_and_merge).with(@winner, @loser).returns(:merge_complete)
          post :create, params: { user_id: @loser.to_param, winner: @winner.to_param, format: "js" }
        end
        should_render_template 'people/user_merge/create'
      end

      describe "with failed merge" do
        before do
          Users::Merge.expects(:validate_and_merge).with(@winner, @loser).returns(:loser_non_enduser)
          post :create, params: { user_id: @loser.to_param, winner: @winner.to_param, format: "js" }
        end
        should_render_template 'people/user_merge/create'
        should_set_the_flash_to /problem during merge/
      end
    end

    describe "without loser id" do
      before { post :create, params: { user_id: 111, winner: @winner.to_param } }
      should_render_template "people/user_merge/_error"
    end

    describe "without winner id" do
      before { post :create, params: { user_id: @loser.to_param, winner: "" } }
      should_render_template "people/user_merge/_error"
    end

    describe "when the loser has one phone identity and winner both email and phone identity" do
      before do
        @phone_1 = "+353851234567"
        @phone_2 = "+353851234568"

        @winner.update_attribute(:phone, @phone_1)
        @old_winner_identity = UserPhoneNumberIdentity.new(account: @account, user: @winner, value: @phone_1)
        @winner.identities << @old_winner_identity

        @loser.update_attribute(:phone, @phone_2)
        @loser.identities.delete_all
        @old_loser_identity = UserPhoneNumberIdentity.new(account: @account, user: @loser, value: @phone_2)
        @loser.identities << @old_loser_identity

        post :create, params: { user_id: @loser.to_param, winner: @winner.to_param }
      end

      it('responds with ok') { assert_response :ok }

      it "preserves both the winner's and loser's phone identities" do
        assert_equal 2, @winner.identities.phone.count(:all)
        assert_equal [@phone_1, @phone_2], UserPhoneNumberIdentity.where(user_id: @winner).map(&:value)
      end
    end
  end
end
