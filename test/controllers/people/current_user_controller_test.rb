require_relative "../../support/test_helper"

SingleCov.covered!

describe People::CurrentUserController do
  describe "CurrentUserController" do
    fixtures :accounts, :users, :organizations, :role_settings

    before do
      @request.account = accounts(:minimum)
    end

    should_route :get, "users/current",      controller: "people/current_user", action: "show"
    should_route :get, "users/current/edit", controller: "people/current_user", action: "edit"

    should_be_unauthorized_when_not_logged_in([:show, :edit])

    describe "#edit" do
      before do
        @user = users(:minimum_end_user)
        login(@user)
        get :edit
      end

      it "routes the user to their edit profile page" do
        assert_response :redirect
        assert @response.location =~ /\/users\/#{@user.id}\/edit$/
      end
    end

    describe "#show" do
      describe "when accessed by an end user" do
        before do
          @user = users(:minimum_end_user)
          login(@user)
        end

        describe "with format HTML" do
          before do
            accept :html
            get :show
          end

          it('responds with redirect') { assert_response :redirect }
        end
      end
    end
  end
end
