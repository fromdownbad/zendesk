require_relative "../../support/test_helper"

SingleCov.covered!

describe People::BulkDeleteController do
  fixtures :organizations, :users, :taggings, :accounts, :posts, :entries, :role_settings, :forums, :user_settings

  should_include_module Zendesk::LotusEmbeddable

  before do
    @request.account = accounts(:minimum)
  end

  describe "#order" do
    it "defaults to sorting by recent created_at" do
      @controller.params = {}
      assert_equal "created_at DESC", @controller.send(:order)
      expected = {"desc" => '1', "order" => 'created_at'}
      assert_equal expected, @controller.params
    end

    it "does not allow unknown columns" do
      @controller.params = {desc: '', order: "hax"}
      assert_equal "created_at", @controller.send(:order)
    end

    it "nows allow unknown order" do
      @controller.params = {desc: "hax", order: 'suspended'}
      assert_equal "suspended DESC", @controller.send(:order)
    end
  end

  describe_with_arturo_enabled :suspended_users_filter do
    let!(:unsuspended_user) { users(:minimum_author) }
    let!(:suspended_user) { users(:minimum_author_suspend) }

    before do
      suspended_user.settings.where(name: 'suspended').first.update_column :value, 'true'
      login("minimum_admin")
    end

    it "includes all users" do
      get :index
      assert_includes @controller.instance_variable_get(:@users), unsuspended_user, 'includes an unsuspended user'
      assert_includes @controller.instance_variable_get(:@users), suspended_user, 'includes a suspended user'
    end

    it "doesn't order by suspended" do
      @controller.params = { order: 'suspended' }
      assert_equal "created_at DESC", @controller.send(:order)
    end

    it "filters suspended" do
      get :index, params: { filter: 'suspended' }
      assert_not_includes @controller.instance_variable_get(:@users), unsuspended_user, "doesn't include unsuspended users"
      assert_includes @controller.instance_variable_get(:@users), suspended_user, 'includes suspended users'
    end

    describe "#total_entries" do
      let(:total_entries) { @controller.send(:total_entries) }

      it "returns the count of users without 'suspended' filter" do
        ActiveRecord::Relation.any_instance.stubs(:count).returns(10)
        assert_equal 10, total_entries
      end

      it "returns nil with 'suspended' filter" do
        @controller.params = { filter: 'suspended' }
        assert_nil total_entries
      end
    end

    describe "#filter_conditions" do
      let(:filter_conditions) { @controller.send(:filter_conditions) }

      it "returns nil without 'suspended' filter" do
        assert_nil filter_conditions
      end

      it "returns filter by suspended conditions with 'suspended' filter" do
        @controller.params = { filter: 'suspended' }
        assert_equal "us.account_id = #{accounts(:minimum).id} AND (us.value = '1' OR us.value='true')", filter_conditions
      end
    end
  end

  describe "for an agent" do
    before do
      login("minimum_agent")
      get :index
    end

    it('responds with forbidden') { assert_response :forbidden }
  end

  describe "for an admin" do
    before do
      login("minimum_admin")
    end

    describe "#index" do
      before { get :index }
      it('responds with success') { assert_response :success }
      should_render_template 'people/bulk_delete/index'
    end

    describe "#new" do
      describe "basic delete" do
        before { get :new, params: { user_ids: ["12", "34"] } }
        it('responds with success') { assert_response :success }
        should_render_template 'people/bulk_delete/new'
        it "shows basic confirmation message" do
          @response.body.must_include I18n.t('txt.admin.views.people.bulk_delete.basic_delete_confirmation')
        end
      end
    end

    describe "#create" do
      before do
        @user1 = users(:minimum_author)
        @user2 = users(:minimum_author_suspend)
      end

      describe "basic delete" do
        it "deletes users but leave content" do
          @user1.tickets.update_all(status_id: StatusType.CLOSED)
          post :create, params: { user_ids: [@user1.id, @user2.id] }
          assert_response :redirect
          assert_equal I18n.t('txt.admin.views.people.bulk_delete.delete_success_notice'), flash[:notice]
          assert @user1.reload.posts.any?
          assert @user1.reload.entries.any?
          assert_nil @request.account.users.find_by_id(@user1.id)
          assert_nil @request.account.users.find_by_id(@user2.id)
        end
      end
    end
  end
end
