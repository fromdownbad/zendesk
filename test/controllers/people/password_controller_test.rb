require_relative "../../support/test_helper"
require_relative "../../support/brand_test_helper"

SingleCov.covered! uncovered: 7

describe People::PasswordController do
  include BrandTestHelper
  fixtures :all

  before do
    @request.account = accounts(:with_groups)
    @controller.stubs(:current_brand).returns(@request.account.default_brand)
    @controller.stubs(:current_route).returns(@request.account.default_brand.route)
  end

  with_options(controller: "people/password") do |request|
    request.should_route :get, "/password/validate_password", action: 'validate_password'
    request.should_route :put, "/password/update",            action: 'update'
  end

  should_route :get,  "/password",           controller: "people/password", action: :index
  should_route :get,  "/password/reset/foo", controller: "people/password", action: :reset, token: "foo"
  should_route :post, "/password/reset/foo", controller: "people/password", action: :reset_password, token: "foo"
  should_route :post, "/password/reset",     controller: "people/password", action: :reset_password
  should_route :get,  "/password/create/foo", controller: "people/password", action: :create, token: "foo"
  should_route :post, "/password/create/foo", controller: "people/password", action: :create_password, token: "foo"

  describe "#update" do
    describe "A user with an expired password" do
      before do
        login('with_groups_end_user')
        @user = users(:with_groups_end_user)
        shared_session[:auth_password_expired] = true

        @controller.stubs(:current_route).returns(@request.account.default_brand.route)
      end

      it "is able to update their password" do
        expired_password = @user.crypted_password
        put :update, params: { current_password: '123456', new_password: 'newpassword' }
        @user.reload

        assert_not_equal expired_password, @user.crypted_password
      end

      it "removes the expired password restriction when the password is changed" do
        put :update, params: { current_password: '123456', new_password: 'newpassword' }

        assert_nil shared_session[:auth_password_expired]
      end

      it "does not remove the expired password restriction when the password isn't changed" do
        assert(shared_session[:auth_password_expired])
        put :update, params: { current_password: '123456', new_password: '123' }
        assert(shared_session[:auth_password_expired])
      end
    end

    describe "Updating the password as an end user" do
      before do
        login('with_groups_end_user')
      end

      it "redirects correctly" do
        put :update, params: { current_password: '123456', new_password: 'newpassword' }
        assert_redirected_to '/auth/v2/login/signed_in'
      end

      it "succeeds when a new, valid password is provided" do
        user = users(:with_groups_end_user)
        old_pass = user.crypted_password
        put :update, params: { current_password: '123456', new_password: 'newpassword' }

        assert flash[:notice]
        assert_response :redirect
        user.reload

        assert_not_equal old_pass, user.crypted_password, old_pass
      end

      describe "when failing and with return_to_on_failure" do
        before do
          put :update, params: { current_password: 'oops', new_password: 'newpassword', return_to_on_failure: @request.account.url + '/failure' }
        end

        it 'redirects to return_to_on_failure' do
          location = @response.headers['Location']
          assert location.start_with?(@request.account.url + '/failure'),
            "#{location} is not return_to_on_failure"
          assert_equal flash[:error], I18n.t('txt.users.update_password.incorrect')
        end
      end

      it "fails when the new password isn't different form the current password, and the security policy enforces password history" do
        Zendesk::Users::SecuritySettings.any_instance.stubs(:security_policy_id).returns(Zendesk::SecurityPolicy::High.id)
        put :update, params: { current_password: '123456', new_password: '123456', return_to_on_failure: @request.account.url + '/failure' }
        # New password must be different than current password
        assert_equal I18n.t('txt.users.update_password.unchanged'), flash[:error]
        assert_response :redirect
        assert response.location.starts_with?(@request.account.url + '/failure')
      end

      it "does not fail when the new password isn't different form the current password, and the security policy does not enforce password history" do
        put :update, params: { current_password: '123456', new_password: '123456' }
        assert_nil flash[:error]
        assert_response :redirect
      end

      it "fails when the new password is invalid" do
        put :update, params: { current_password: '123456', new_password: 'new' }
        assert_equal "Password entered does not meet the following requirements<ul><li><strong>Password: </strong> must be at least 5 characters</li></ul>", flash[:error]
      end

      it "fails when the new password is too long" do
        put :update, params: { current_password: '123456', new_password: '111111111133311111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111' }
        assert_equal "Password entered does not meet the following requirements<ul><li><strong>Password: </strong> Enter a password that is fewer than 128 characters</li></ul>", flash[:error]
      end

      it "updates password version" do
        authentication = @controller.send(:authentication)
        authentication.expects(:password_changed_for_user!).with(users(:with_groups_end_user), shared_session_record_id: nil)
        put :update, params: { current_password: '123456', new_password: 'newpassword' }
      end

      it "redirects to the login page" do
        put :update, params: { current_password: '123456', new_password: '123456' }
        assert_redirected_to "#{accounts(:with_groups).url(ssl: true)}/auth/v2/login/signed_in"
      end
    end

    describe "auditing password changes as an end user" do
      before do
        @user = users(:with_groups_end_user)
        login('with_groups_end_user')
        @request.stubs(:remote_ip).returns("131.175.6.2")
      end

      it "creates an audit on change" do
        assert_difference 'PasswordChange.count(:all)', 1 do
          put :update, params: { current_password: '123456', new_password: 'newpassword' }
        end
      end

      it "logs ip in audit" do
        put :update, params: { current_password: '123456', new_password: 'newpassword' }
        assert_equal "131.175.6.2", @user.password_changes.last.ip_address
        assert_equal [], @user.cia_events # should not log password changes
      end

      it "logs actor in audit" do
        put :update, params: { current_password: '123456', new_password: 'newpassword' }
        assert_equal @user, @user.password_changes.last.actor
        assert_equal [], @user.cia_events # should not log password changes
      end
    end
  end

  describe "#validate_password" do
    describe "with verification token" do
      let(:json_response) { JSON.parse(@response.body) }
      let(:user_security_policy_id) { '200' }
      let(:password) { '123456' }

      before do
        @user = users(:with_groups_end_user)
        @token = @user.verification_tokens.create
        @user.identities.first.update_attribute(:is_verified, false)
      end

      describe_with_arturo_enabled :disallow_verification_tokens_for_password_resets do
        it "returns 200" do
          put(:validate_password, params: { password: password, user_security_policy_id: user_security_policy_id, sequence: '1', token: @token.value })

          assert_response :ok
        end
      end
    end

    describe "when the user is known" do
      let(:user)          { users(:with_groups_end_user) }
      let(:json_response) { JSON.parse(@response.body) }

      before do
        login('with_groups_end_user')

        user.password = '123456'
        user.save!(validate: false)

        put(:validate_password, params: { password: password, user_security_policy_id: user_security_policy_id, sequence: '1' })
      end

      describe "when the user security policy is known" do
        let(:user_security_policy_id) { '300' }
        # non_custom security defined at security_policy.rb

        describe "and there are validation errors" do
          let(:password) { 'rar' }

          let(:errors) do
            [
              'must be at least 6 characters',
              'must include letters in mixed case and numbers',
              'must include a character that is not a letter or number'
            ]
          end

          it "allows CORS" do
            assert_not_equal nil, @response.headers['Access-Control-Allow-Origin']
          end

          it "returns the errors" do
            assert json_response['errors'].all? { |e| errors.include?(e) }
          end

          it "returns the specified sequence" do
            assert_equal 1, json_response['sequence']
          end
        end

        describe "and the input has special characters" do
          let(:password) { 'tru%40' }
          let(:errors) { 'must be at least 6 characters' }

          it "decodes the password and returns a length error" do
            assert json_response['errors'].include?(errors)
          end
        end

        describe "and the password is too long" do
          let(:password) { '1' * 140 }
          let(:errors) { 'must be fewer than 128 characters' }

          it "returns a length error" do
            assert json_response['errors'].include?(errors)
          end
        end

        describe "and the password has been recently used" do
          let(:password) { 'wombat!A1' }
          let(:errors) { ["must be different than the previous #{Zendesk::SecurityPolicy.find(user_security_policy_id).password_history_length} passwords"] }

          before do
            user.change_password!('wombat!A1')
            put(:validate_password, params: { password: password, user_security_policy_id: user_security_policy_id, sequence: '1' })
          end

          it "returns the UniqueHistory validation error" do
            assert !json_response['errors'].empty?, 'The response should have at least one error'
            assert json_response['errors'].all? { |e| errors.include?(e) }
          end
        end

        describe "and there are no validation errors" do
          let(:password) { 'wombat!A1' }

          it "returns no errors" do
            assert_empty(json_response['errors'])
          end
        end
      end

      describe "when the user security policy is custom" do
        let(:user_security_policy_id) { '400' }

        describe "and there are validation errors" do
          let(:password) { 'rar' }
          let(:errors) { ['must be at least 5 characters'] }

          it "returns the errors" do
            assert json_response['errors'].all? { |e| errors.include?(e) }
          end

          it "returns the specified sequence" do
            assert_equal 1, json_response['sequence']
          end
        end

        describe "and the password is too long" do
          let(:password) { '1' * 142 }
          let(:errors) { 'must be fewer than 128 characters' }

          it "returns a length error" do
            assert json_response['errors'].include?(errors)
          end
        end

        describe "and there are no validation errors" do
          let(:password) { 'wombat!A1' }

          it "returns no errors" do
            assert_empty(json_response['errors'])
          end
        end
      end

      describe "when the user security policy is not known" do
        let(:user_security_policy_id) { nil }
        let(:password)                { 'rar' }

        it "returns a 400" do
          assert_response :bad_request
        end
      end
    end

    describe "when the user is nil" do
      let(:password) { 'testing' }
      let(:user_security_policy_id) { '300' }

      before do
        put(:validate_password, params: { password: password, user_security_policy_id: user_security_policy_id, sequence: '1', token: 'invalid' })
      end

      it "returns a 400" do
        assert_response :bad_request
      end
    end
  end

  describe "#create_password" do
    describe "A user without a password" do
      before do
        @user = users(:with_groups_end_user)
        @token = @user.password_reset_tokens.create
        @user.identities.first.update_attribute(:is_verified, false)
      end

      it "has the identity verified when the new password is set" do
        post :create_password, params: { token: @token.value, new_password: "wookiee" }
        @user.reload
        assert @user.identities.first.is_verified?
      end

      it "redirects to the login page" do
        post :create_password, params: { token: @token.value, new_password: "wookiee" }
        assert_redirected_to '/access'
      end

      it "logs the user in" do
        post :create_password, params: { token: @token.value, new_password: "wookiee" }
        assert_equal @user, warden.user
      end

      it "uses the token before the current_user" do
        login(users(:with_groups_agent1))

        post :create_password, params: { token: @token.value, new_password: "wookiee" }

        assert_equal @user, warden.user
        assert @user.reload.identities.first.is_verified?
        assert_redirected_to '/access'
      end
    end
  end

  describe "#reset_password" do
    before do
      @account = @request.account = accounts(:minimum)
      @user = users(:minimum_agent)
      @token = @user.password_reset_tokens.create
    end

    it "allows SSL" do
      logout

      post :reset_password, params: { token: @token.value, new_password: "wookiee" }

      assert_redirected_to "#{@account.url}/access"
    end

    describe "when not logged in" do
      before do
        logout
        post :reset_password, params: { token: @token.value, new_password: "wookiee" }
      end

      it "redirects to the login page" do
        assert_redirected_to '/access'
      end

      it "logs the user in" do
        assert_equal @user, warden.user
      end

      it "resets the password for a user" do
        @user.reload
        assert @user.authenticated?("wookiee")
      end

      it "destroys the token once it's been used" do
        refute VerificationToken.exists?(@token.id)
      end

      it "leaves an audit entry with the correct actor" do
        event = CIA::Event.for_account(@account).last
        assert event
        assert_equal @user, event.actor
        assert_equal ["crypted_password"], event.attribute_change_hash.keys
      end
    end

    describe "When on a secondary brand" do
      before do
        logout
        ActionMailer::Base.perform_deliveries = true
        ActionMailer::Base.deliveries.clear
        @account.stubs(:has_multibrand?).returns(true)
        @secondary_brand = create_brand(@account, name: 'Brand1', subdomain: 'brand1')
        post :reset_password, params: { token: @token.value, new_password: "wookiee", auth_origin: "#{@secondary_brand.id},false,true" }
      end

      it "sends the passwords changed notification email from the specified brand" do
        assert_equal 1, ActionMailer::Base.deliveries.size
        assert_equal ["support@brand1.zendesk-test.com"], ActionMailer::Base.deliveries[0].from
      end
    end

    describe "when the password is invalid" do
      it "renders the reset template" do
        post :reset_password, params: { token: @token.value, new_password: "" }
        assert_template 'auth/sso_v2_index'
      end

      it "renders the reset template v2 if multibrand auth is enabled" do
        @controller.stubs(:current_brand).returns(@request.account.default_brand)
        @controller.stubs(:current_route).returns(@request.account.default_brand.route)

        post :reset_password, params: { token: @token.value, new_password: "" }
        assert_template :sso_v2_index
      end

      it "renders the reset template on mobile" do
        post :reset_password, params: { token: @token.value, new_password: "", format: "mobile_v2" }
        assert_template :sso_v2_index
      end
    end

    describe "with 2FA enabled" do
      before do
        User.any_instance.stubs(otp_configured?: true)
      end

      it "redirects to the login page" do
        put :reset_password, params: { token: @token.value, new_password: "wookiee" }
        assert_redirected_to '/auth/v2/login'
      end

      it "redirects to the MB login page with a MB account" do
        put :reset_password, params: { token: @token.value, new_password: "wookiee", auth_origin: "#{@account.default_brand.route.id},false,true" }
        assert_redirected_to "/auth/v2/login?auth_origin=#{@account.default_brand.route.id}%2Cfalse%2Ctrue"
      end

      it "does not log the user in" do
        put :reset_password, params: { token: @token.value, new_password: "wookiee" }
        assert_nil warden.user
      end
    end

    describe "with 2FA enforced on the account" do
      before do
        @account.settings.create(name: 'two_factor_enforce', value: true)
        assert @account.settings.two_factor_enforce
      end

      it "redirects to the login form" do
        put :reset_password, params: { token: @token.value, new_password: "wookiee" }
        assert_redirected_to '/auth/v2/login'
        assert_nil warden.user
      end
    end

    describe "when user is unverified" do
      before do
        @user.is_verified = false
        @user.save!
        post :reset_password, params: { token: @token.value, new_password: "wookiee" }
      end

      it "verifies the user" do
        @user.reload
        assert @user.is_verified?
      end
    end
  end

  describe "On GET to :reset" do
    describe "as an end_user" do
      let(:brand_route) { @request.account.default_brand.route }
      before do
        @user = users(:with_groups_end_user)
        @token = @user.password_reset_tokens.create
      end

      it "renders sso_login template" do
        get :reset, params: { token: @token.value }
        assert_template 'auth/sso_v2_index'
      end

      it "renders sso_v2_login template if multibrand_auth?" do
        @controller.stubs(:current_brand).returns(@request.account.default_brand)
        @controller.stubs(:current_route).returns(brand_route)

        get :reset, params: { token: @token.value }
        assert_template :sso_v2_index
      end

      describe "from a mobile device" do
        it "it renders the auth/sso_v2_index form" do
          get :reset, params: { token: @token.value, format: 'mobile_v2' }

          assert_template 'auth/sso_v2_index'
          assert_response :ok
        end
      end

      describe "when expired token" do
        before do
          @account = @request.account = accounts(:with_groups)
          @token.update_attribute(:expires_at, 24.hours.ago.to_s(:db))
        end

        it "does not allow you to use it" do
          get :reset, params: { token: @token.value }
          assert_redirected_to "#{@account.url}/hc/signin"
          assert_equal 'You can generate a new reset link from the sign-in page.', flash[:error]
        end
      end

      describe "when it's a verification token" do
        before do
          @account = @request.account = accounts(:with_groups)
          @token = @user.verification_tokens.create
        end

        describe_with_arturo_disabled :disallow_verification_tokens_for_password_resets do
          it "allows you to use it" do
            get :reset, params: { token: @token.value }
            assert_response :success
            assert_nil flash[:error]
          end
        end

        describe_with_arturo_enabled :disallow_verification_tokens_for_password_resets do
          it "does not allow you to use it" do
            get :reset, params: { token: @token.value }
            assert_redirected_to "#{@account.url}/hc/signin"
            assert_equal 'You can generate a new reset link from the sign-in page.', flash[:error]
          end
        end
      end
    end
  end

  describe "On GET to :create" do
    before do
      @account = @request.account = accounts(:minimum)
      @user = users(:minimum_agent)
    end

    describe "with a valid password reset token" do
      before do
        @token = @user.password_reset_tokens.create
        get :create, params: { token: @token.value }
      end

      it('responds with ok') { assert_response :ok }
    end

    describe "with a verification token" do
      before do
        @token = @user.verification_tokens.create
      end

      describe_with_arturo_disabled :disallow_verification_tokens_for_password_resets do
        it 'responds with ok' do
          get :create, params: { token: @token.value }
          assert_response :ok
        end
      end

      describe_with_arturo_enabled :disallow_verification_tokens_for_password_resets do
        it 'responds with an error' do
          get :create, params: { token: @token.value }
          assert_redirected_to "#{@account.url}/hc/signin"
          assert_not_nil flash[:error]
        end
      end
    end
  end

  describe "#index" do
    before { login('with_groups_end_user') }

    it "renders" do
      get :index
      assert_response :success
    end

    it "renders sso_login" do
      get :index

      assert_template 'auth/sso_v2_index'
    end

    describe "with hostmapping" do
      before do
        @request.account.update_attribute(:host_mapping, "wibble.example.com")
        @controller.stubs(:current_brand).returns(@request.account.default_brand)
        @controller.stubs(:current_route).returns(@request.account.route)
      end

      it "renders the page on the subdomain" do
        get :index

        assert_response :ok
        assert_template 'auth/sso_v2_index'
      end

      it "renders the page on the hostmapped domain" do
        @request.host = 'wibble.example.com'
        get :index

        assert_response :ok
        assert_template 'auth/sso_v2_index'
      end
    end

    describe "when web portal is not enabled" do
      before do
        @request.account.stubs(:web_portal_enabled?).returns(false)
      end

      it "renders the index page with hc theme" do
        get :index
        assert_includes @response.body, "help_center"
      end
    end
  end
end
