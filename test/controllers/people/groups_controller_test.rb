require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 15

describe People::GroupsController do
  fixtures :accounts, :addresses, :users, :user_identities, :groups, :memberships, :tickets, :plans, :ticket_fields, :role_settings

  should_include_module Zendesk::LotusEmbeddable

  describe "Groups controller" do
    should_be_unauthorized_when_not_logged_in([:index, :new, :create, :edit, :update, :destroy, :search])
    should_be_unauthorized_when_not_logged_in([:index, :new, :create, :edit, :update, :destroy, :search], 'with_groups_end_user', 403)

    before do
      @account = accounts(:with_groups)
      @group1  = groups(:with_groups_group1)
      @group2  = groups(:with_groups_group2)

      @request.account = @account
    end

    it "routes correctly" do
      assert_recognizes({ controller: 'people/groups', action: 'index' }, '/groups')
      assert_recognizes({ controller: 'people/groups', action: 'show', id: '1' }, '/groups/1')
      assert_recognizes({ controller: 'people/groups', action: 'edit', id: '1' }, '/groups/1/edit')
      assert_recognizes({ controller: 'people/groups', action: 'search' }, '/groups/search')
    end

    describe "#new" do
      before do
        login(@account.owner)
      end

      it 'renders the correct template' do
        get :new
        assert_response :success
        assert_template :edit
        assert_select 'input[id*="group_agents_"]', 6        # assert user checkboxes render correctly
        assert_select 'input[id="group_default"]', 1         # assert default group checkbox is present
        refute response.body.include?("checked=\"checked\"") # assert all checkboxes default to unchecked
      end
    end

    describe '#create' do
      let(:last_group) { Group.last }

      before do
        @account.stubs(:has_groups?).returns(true)
        login @account.owner
        post :create, params: { group: { name: 'new group', description: 'new group description' } }
      end

      should_redirect_to('the group index page') { groups_path }

      it 'creates a new group according to parameters' do
        assert_equal 'new group', last_group.name
        assert_equal 'new group description', last_group.description
        refute last_group.default?
      end
    end

    describe "#edit" do
      before do
        login(@account.owner)
      end

      it "does not generate N+1 membership queries" do
        @group2.agents = @account.agents.map(&:id)
        @group2.save!
        assert_sql_queries 2, /memberships/ do
          get :edit, params: { id: @group2.id }
        end
      end
    end

    describe "#update" do
      let(:agent) { users(:with_groups_agent1) }
      let(:agent2) { users(:with_groups_agent2) }
      before do
        login(@account.owner)
        agent.set_default_group(@group1)
      end

      it "can add members to a group" do
        refute agent.memberships.map(&:group_id).include?(@group2.id)
        put :update, params: { id: @group2.id, group: { agents: @group2.memberships.map(&:user_id) + [agent.id], name: @group2.name } }
        agent.reload.memberships.count(:all).must_equal 2
      end

      it "can remove members from a group" do
        agent.memberships.create!(group_id: @group2.id)
        agent.reload.memberships.count(:all).must_equal 2
        put :update, params: { id: @group2.id, group: { agents: @group2.memberships.map(&:user_id) - [agent.id], name: @group2.name } }
        agent.reload.memberships.count(:all).must_equal 1
      end

      it "sets the default group" do
        @group1.update_attributes!(default: true)
        assert_equal @group1.id, @account.default_group.id
        put :update, params: { id: @group2.id, group: { agents: @group2.memberships.map(&:user_id), name: @group2.name, default: "true"} }
        assert_equal @group2.id, @account.default_group.id
      end

      it "won't let me remove an agent from all groups" do
        put :update, params: { id: @group1.id, group: { agents: [agent2], name: @group1.name } }
        agent.reload.memberships.count(:all).must_equal 1
      end

      it "scopes to active memberships" do
        agent.current_user = agent.account.owner
        agent.delete
        agent3 = users(:with_groups_admin)
        membership = agent3.memberships.first
        membership.default = true
        membership.save!
        put :update, params: { id: @group1.id, group: { agents: [agent2.id, agent3.id], name: @group1.name } }
        assert_nil flash[:error]
        @group1.memberships.count(:all).must_equal 3
      end

      describe "with an agent that doesn't belong to a default group" do
        before do
          agent.memberships.each { |m| m.update_attribute(:default, false) }
          put :update, params: { id: @group2.id, group: { agents: @group2.memberships.map(&:user_id) + [agent.id], name: @group2.name } }
        end

        it "does not update the agent/group" do
          agent.reload.memberships.count(:all).must_equal 1
        end
        should_redirect_to("the edit group page") { edit_group_path(@group2) }
        should_set_the_flash_to(/agent must be a member of the group that has been selected as the default group/)
      end

      describe "removing all groups" do
        before do
          @group2.memberships.map(&:user).each do |user|
            user.set_default_group(@group2.id)
            user.save!
            user.set_default_group(@group1.id)
            user.save!
          end
          @group2.memberships.count(:all).must_equal 3
        end

        it "can let me remove all members of a group" do
          put :update, params: { id: @group2.id, group: {agents: [""], name: @group2.name } }
          @group2.reload.memberships.count(:all).must_equal 0
        end
      end
    end

    describe "#confirm_delete" do
      it 'calls Voice' do
        login(@account.owner)
        voice_request = stub_request(:get, %r{/api/v2/channels/voice/internal/account/groups/#{@group2.id}/usage}).
          to_return(
            body: {
              group: {
                phone_numbers: [], ivrs: []
              }
            }.to_json,
            headers: { content_type: 'application/json' }
          )

        get :confirm_delete, params: { id: @group2.id }

        assert_requested voice_request
      end
    end

    describe "when deleting a group" do
      describe "that is still used as an agents default group" do
        before do
          Group.any_instance.stubs(:remove_from_phone_group_routing!)
          login('with_groups_admin')
          user = @group2.memberships.map(&:user).first
          user.default_group_id = @group2.id
          user.save!
        end

        it "does not delete the group" do
          @controller.expects(:update_members).never
          delete :destroy, params: { id: @group2.id }
          assert @group2.is_active?
          assert_redirected_to :groups

          default_user_names = @group2.memberships.where(default: true).map(&:user).map(&:name).sort
          user_name_list = "<ul><li>#{default_user_names.join("</li><li>")}</li></ul>".html_safe
          expected_flash_error = I18n.t('txt.admin.controllers.people.groups.group.group_still_agent_default') + user_name_list
          assert_equal expected_flash_error, flash[:error]
        end
      end

      describe "and the group is the default group for the account" do
        before do
          login('with_groups_admin')
          @group1.update_attributes!(default: true)
        end

        describe 'and the group is not the default group for any agents' do
          before do
            Group.any_instance.stubs(:remove_from_phone_group_routing!)
            @group1.memberships.map(&:user).map do |user|
              user.set_default_group(@group2.id)
              user.save!
            end
          end

          it "does not delete the group" do
            delete :destroy, params: { id: @group1.id }
            assert @group1.is_active?
            assert_redirected_to :groups

            expected_flash_error = I18n.t('txt.admin.controllers.people.groups.group.group_is_default', default_group_name: @group1.name)
            assert_equal expected_flash_error, flash[:error]
          end
        end
      end
    end

    describe "when searching for a group" do
      describe "with a privileged agent" do
        before { login('with_groups_admin') }

        it "renders the right template and returns 2 results" do
          get :search, params: { query: "with" }
          assert_template 'people/search/index'
          assert_select '.item', 2
        end

        it "renders the right template and returns 1 result" do
          get :search, params: { query: "with_groups_1" }
          assert_template 'people/search/index'
          assert_select '.item', 1
        end

        it "renders the right template and returns 0 results" do
          get :search, params: { query: "with_groups_group0" }
          assert_template 'people/search/index'
          assert_select '.item', 0
        end

        describe "with specified formats" do
          it "returns a 200 response" do
            get :search, params: { query: "randommmm", format: "json" }
            assert_response 200

            get :search, params: { query: "randommmm", format: "xml" }
            assert_response 200
          end
        end
      end
    end

    it "allows agents to see the index and show only" do
      login('with_groups_agent1')
      get :index

      assert_response :ok
      assert_equal 'text/html', @response.content_type
      assert_template 'index'

      get :show, params: { id: @group1.id }
      assert_equal 'text/html', @response.content_type
      assert_template 'show'

      get :new
      assert_response 403

      post :create
      assert_response 403

      put :update, params: { id: @group1.id }
      assert_response 403

      delete :destroy, params: { id: @group1.id }
      assert_response 403
    end

    describe "agents viewing tabs" do
      before { login('with_groups_agent1') }

      it "renders tickets in the ticket tab" do
        get :show, params: { id: @group1.id, select: 'tickets' }
        assert_response :ok
        assert_select '.linked', 3
      end

      it "renders users in the users tab" do
        get :show, params: { id: @group1.id, select: 'users' }
        assert_response :ok
        assert_select '.item', 2
      end
    end

    describe 'limit count_with_archived' do
      before { login('with_groups_admin') }

      describe_with_arturo_enabled :limit_group_count_with_archived do
        it 'limits count to COUNT_WITH_ARCHIVED_LIMIT' do
          count_with_limit = 100_000
          Ticket.stubs(:count_with_archived).with(limit: count_with_limit).returns(count_with_limit)
          get :show, params: { id: @group1.id }
          assert_match /Tickets \(100000\)/, @response.body
        end

        it 'has the appropriate LIMIT sql queries' do
          regex = /LIMIT (\d+)\) subquery_for_count/
          sql = assert_sql_queries(2, regex) do
            get :show, params: { id: @group1.id }
          end
          # based on the count from unarchived table, the limit is adjusted
          # on the archived stubs table
          limits = sql.map { |s| s.match(regex)[1].to_i }
          assert limits.all? { |l| l > 90000 && l <= 100000 }, "limit values: #{limits.inspect} should be close to 100000"
        end

        describe "Caching count" do
          before(:each) { Rails.cache.clear }

          it "caches ticket count if doesn't exists" do
            Ticket.stubs(:count_with_archived).returns(100_000)
            cache_key = "cached_count_tickets_by_group_with_archived_#{@group1.id}/#{@account.id}"

            get :show, params: { id: @group1.id }

            assert_equal 100000, Rails.cache.read(cache_key)
          end
        end
      end

      describe_with_arturo_disabled :limit_group_count_with_archived do
        it 'does not limit count' do
          count_without_limit = 150_000
          Ticket.stubs(:count_with_archived).returns(count_without_limit)
          get :show, params: { id: @group1.id }
          assert_match /Tickets \(150000\)/, @response.body
        end

        it 'has no LIMIT sql queries' do
          assert_sql_queries(0, /LIMIT 100000\) subquery_for_count/) do
            get :show, params: { id: @group1.id }
          end
        end
      end
    end

    it "allows admins to access all actions" do
      login('with_groups_admin')

      get :index
      assert_response :ok
      @response.content_type.must_equal 'text/html'
      assert_template 'index'

      [@group1, @group2].each do |group|
        get :edit, params: { id: group.id }
        assert_response :ok
        assert_template 'edit'

        get :show, params: { id: group.id }
        assert_response :ok
        assert_template 'show'
      end
    end

    describe 'archived tickets' do
      before do
        @request.account = accounts(:minimum)
        @archived_ticket = tickets(:minimum_4)
        @archived_ticket.will_be_saved_by users(:minimum_agent)
        @archived_ticket.status_id = StatusType.CLOSED
        @archived_ticket.save!
        archive_and_delete(@archived_ticket)
      end

      it "shows archived tickets for the group" do
        @group = @archived_ticket.group
        login(users(:minimum_admin))

        get :show, params: { id: @group.id, select: 'tickets' }

        tickets = assigns(:tickets)
        assert tickets.include?(@archived_ticket)
      end
    end

    describe "flash sanitization" do
      before { login('with_groups_admin') }

      describe "#create" do
        before { post :create, params: { group: { name: '"><img src="x">'} } }
        should_set_the_flash_to(/&quot;&gt;&lt;img src=&quot;x&quot;&gt;/)
      end

      describe "#update" do
        before { put :update, params: { id: @group1.id, group: { name: '"><img src="x">'} } }
        should_set_the_flash_to(/&quot;&gt;&lt;img src=&quot;x&quot;&gt;/)
      end

      describe "#destroy" do
        before do
          Group.any_instance.stubs(:remove_from_phone_group_routing!)
          Group.any_instance.stubs(name: '"><img src="x">')
          delete :destroy, params: { id: @group1.id }
        end
        should_set_the_flash_to(/&quot;&gt;&lt;img src=&quot;x&quot;&gt;/)
      end
    end
  end
end
