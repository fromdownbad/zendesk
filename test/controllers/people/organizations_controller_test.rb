require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 14

describe People::OrganizationsController do
  should_include_module Zendesk::LotusEmbeddable

  fixtures :all

  before do
    @request.account = accounts(:minimum)
  end

  with_options(controller: 'people/organizations') do |request|
    request.should_route :post, '/organizations/autocomplete', action: 'autocomplete' # deprecated can be removed 2012-12-01 + change route from :any to :get
    request.should_route :get, '/organizations/autocomplete', action: 'autocomplete'
  end

  should_be_unauthorized_when_not_logged_in([:index, :new, :create, :edit, :update, :destroy])
  should_be_unauthorized_when_not_logged_in([:index, :new, :create, :edit, :update, :destroy], 'minimum_end_user', 403)

  it "is mapped such that /organizations routes to /people/organizations" do
    assert_recognizes({ controller: 'people/organizations', action: 'index' }, '/organizations')
    assert_recognizes({ controller: 'people/organizations', action: 'show', id: '1' }, '/organizations/1')
    assert_recognizes({ controller: 'people/organizations', action: 'edit', id: '1' }, '/organizations/1/edit')
    assert_recognizes({ controller: 'people/organizations', action: 'search' }, '/organizations/search')
  end

  describe "when searching for an organization" do
    before do
      @request.account.organizations = [organizations(:minimum_organization1),
                                        organizations(:minimum_organization2),
                                        organizations(:minimum_organization3)]
    end

    describe "with a privileged agent" do
      before do
        login('minimum_admin')
      end

      it "renders the right template and return 2 results" do
        get :search, params: { query: "organization" }
        assert_template 'people/search/index'
        assert_select '.item', 2
      end

      it "renders the right template and return 1 result" do
        get :search, params: { query: "minimum" }
        assert_template 'people/search/index'
        assert_select '.item', 1
      end

      it "renders the right template and return 0 results" do
        get :search, params: { query: "with_groups_group0" }
        assert_template 'people/search/index'
        assert_select '.item', 0
      end

      describe "with specified formats" do
        it "returns 200 responses" do
          get :search, params: { query: "randommmm", format: "json" }
          assert_response 200

          get :search, params: { query: "randommmm", format: "xml" }
          assert_response 200
        end
      end
    end

    describe "with an unpriveleged agent" do
      before do
        login('minimum_agent')
      end

      it "returns a 403" do
        get :search, params: { query: "some randommmm" }
        assert_response 403
      end
    end
  end

  describe "organization autocomplete" do
    before { login('minimum_admin') }

    it "supports prototype-style autocomplete endpoints" do
      post :autocomplete, params: { name: 'orga' }
      assert_response 200
      assert_equal 'application/json', @response.content_type, @response.body.inspect
      assert_equal ['organization 2', 'organization 3'], ActiveSupport::JSON.decode(@response.body)
    end

    it "supports jquery style autocomplete" do
      get :jq_autocomplete, params: { term: 'orga' }
      assert_response 200
      assert_equal 'application/json', @response.content_type, @response.body.inspect

      parsed = JSON.parse(@response.body)
      assert_equal ['organization 2', 'organization 3'], parsed.map { |p| p['label'] }
      assert parsed.first['value']
    end

    it "alsos respond with a label / value pair if given an id" do
      org = @request.account.organizations.first
      get :jq_autocomplete, params: { id: org.id }
      assert_response 200
      assert_equal 'application/json', @response.content_type, @response.body.inspect
      assert_equal({'label' => org.name, 'value' => org.id}, JSON.parse(@response.body))
    end
  end

  it "has limited access for agents" do
    login('minimum_agent')

    get :show, params: { id: organizations(:minimum_organization1).id }
    assert_response :ok

    post :autocomplete, params: { name: 'orga' }
    assert_response :ok

    get :new
    assert_response 403

    get :edit, params: { id: 1 }
    assert_response 403

    post :create
    assert_response 403

    get :search
    assert_response 403

    put :update, params: { id: 1 }
    assert_response 403

    put :update_notes, params: { id: organizations(:minimum_organization1).id, organization: { notes: 'hello' }, field: 'look-at-me', format: 'js' }
    assert_response :ok
    assert_match(/look-at-me-show/, @response.body)
  end

  describe "agents viewing tabs" do
    before do
      login('minimum_agent')
    end

    it "renders tickets in the ticket tab" do
      get :show, params: { id: organizations(:minimum_organization1).id, select: 'tickets' }
      assert_response :ok
      assert_select '.linked', 5
    end

    it "renders users in the users tab" do
      get :show, params: { id: organizations(:minimum_organization1).id, select: 'users' }
      assert_response :ok
      assert_select '.item', 2
    end
  end

  it "does not allow restricted agents to edit notes" do
    login(users(:with_groups_agent_groups_restricted))
    put :update_notes, params: { id: organizations(:minimum_organization1).id, organization: { notes: 'hello' }, format: 'js' }
    assert_response 403
  end

  describe "#index" do
    before do
      @agent = users(:minimum_agent)
      login(@agent)
    end

    describe "for an organization restricted agent" do
      before do
        @agent.restriction_id = RoleRestrictionType.ORGANIZATION
        @agent.save!
      end

      it "denys access" do
        get :index
        assert_response :forbidden
      end
    end

    describe "for a non-restricted agent" do
      before do
        @agent.restriction_id = nil
        @agent.save!
      end

      it "renders the organizations" do
        get :index
        assert_response :success
      end

      describe "with a large number of organization memberships" do
        it "counts only up to 10,000 memberships" do
          Zendesk::CappedCounts::CappedNum.any_instance.stubs(:capped?).returns(true)
          regex = /LIMIT 10000\) subquery_for_count/
          # 3 organizations, 1 query for each
          assert_sql_queries(3, regex) do
            get :index
          end
          assert_response :success
          # stubbing capped? as false means assume over limit of 10,000
          assert_match(/minimum organization.*\n.*item_count.*\(10000\+\)/, @response.body)
        end
      end

      describe "with a small number of organization memberships" do
        it "returns the exact count, still limiting the query" do
          regex = /LIMIT 10000\) subquery_for_count/
          # 3 organizations, 1 query for each
          assert_sql_queries(3, regex) do
            get :index
          end
          assert_no_match(/item_count.*\(\d+\+\)/, @response.body)
          # minimum organization has 3 memberships
          assert_match(/minimum organization.*\n.*item_count.*\(3\)/, @response.body)
          assert_response :success
        end
      end
    end
  end

  describe "#show" do
    before do
      @agent = users(:minimum_agent)
      login(@agent)
    end

    describe "for an organization restricted agent" do
      before do
        @agent.restriction_id = RoleRestrictionType.ORGANIZATION
        @agent.organization = organizations(:minimum_organization1)
        @agent.save!
      end

      it "denys access for other organizations" do
        get :show, params: { id: organizations(:minimum_organization2).id }
        assert_response :forbidden
      end

      it "allows access to own organization" do
        get :show, params: { id: organizations(:minimum_organization1).id }
        assert_response :success
      end
    end

    describe "for a non-restricted agent" do
      before do
        @agent.restriction_id = nil
        @agent.save!
      end

      it "renders the organization" do
        get :show, params: { id: organizations(:minimum_organization2).id }
        assert_response :success
      end
    end
  end

  describe "#edit" do
    describe "user tags" do
      before do
        login('minimum_admin')
      end

      it "is visible if the account has them enabled in arturo" do
        @request.account.settings.has_user_tags = true
        @request.account.settings.save!

        get :edit, params: { id: organizations(:minimum_organization1).id }
        assert_response :success
        assert_select 'input#organization-tags-input'
        assert_select 'input#organization_domain_names'
      end

      it "does not be visible when not enabled in arturo" do
        get :edit, params: { id: organizations(:minimum_organization1).id }
        assert_response :success
        assert_select 'input#organization-tags-input', count: 0
        assert_select 'input#organization_domain_names'
      end
    end
  end

  describe "flash sanitization" do
    before do
      login('minimum_admin')
      @organization = organizations(:minimum_organization1)
    end

    describe "#create" do
      before { post :create, params: { organization: { name: '"><img src="x">'} } }
      should_set_the_flash_to(/&quot;&gt;&lt;img src=&quot;x&quot;&gt;/)
    end

    describe "#update" do
      before { put :update, params: { id: @organization.id, organization: { name: '"><img src="x">'} } }
      should_set_the_flash_to(/&quot;&gt;&lt;img src=&quot;x&quot;&gt;/)
    end

    describe "#destroy" do
      before do
        Organization.any_instance.stubs(name: '"><img src="x">')
        delete :destroy, params: { id: @organization.id }
      end
      should_set_the_flash_to(/&quot;&gt;&lt;img src=&quot;x&quot;&gt;/)
    end
  end

  it "Adds a tag" do
    @request.account.settings.has_user_tags = true
    @request.account.settings.save!

    @organization = organizations(:minimum_organization1)
    login(users(:minimum_admin))

    put :update, params: { organization: { additional_tags: 'hej' }, id: @organization.id }
    assert_equal "beta.test hej premium", @organization.reload.current_tags
  end

  it "shows archived tickets" do
    @archived_ticket = tickets(:minimum_5)
    archive_and_delete(@archived_ticket)
    @organization = @archived_ticket.organization
    login(users(:minimum_admin))

    get :show, params: { id: @organization.id, select: 'tickets' }
    tickets = assigns(:tickets)
    assert_includes tickets, @archived_ticket
  end

  describe "#create" do
    describe "admin" do
      before do
        login(users(:minimum_admin))
      end

      describe "when passed a :default param" do
        it 'replaces the :default param with a :domain_names param in order to use the Organization#domain_names setter' do
          Organization.any_instance.expects(:default=).never
          Organization.any_instance.expects(:domain_names=).with('haha.com').once

          post :create, params: { organization: { name: 'some-org', default: 'haha.com' } }
        end
      end
    end
  end

  describe "#update" do
    describe "admin" do
      before do
        login(users(:minimum_admin))
        @organization = organizations(:minimum_organization1)
      end

      describe "when passed a :default param" do
        it 'replaces the :default param with a :domain_names param in order to use the Organization#domain_names setter' do
          Organization.any_instance.expects(:domain_names=).with('haha.com').once

          put :update, params: { id: @organization.id, organization: { default: 'haha.com' } }
        end
      end
    end
  end
end
