require_relative "../support/test_helper"

SingleCov.covered! uncovered: 8

describe TicketFieldsController do
  fixtures :accounts, :users, :ticket_fields, :role_settings, :custom_field_options

  should_include_module Zendesk::LotusEmbeddable

  with_options(controller: "ticket_fields") do |request|
    request.should_route :post, "/ticket_fields/sort", action: 'sort'
  end

  should_be_unauthorized_when_not_logged_in([:index, :new, :create, :update, :destroy])
  should_be_unauthorized_when_not_logged_in([:index, :new, :create, :update, :destroy], 'minimum_end_user', 403)

  before do
    @request.account = accounts(:minimum)
  end

  it "routes" do
    do_routing('ticket_fields')
  end

  describe "#index" do
    it "renders" do
      @controller.expects(:register_device_activity)
      stub_request(:get, %r{/api/v2/apps/requirements.json\?includes=installation})
      login(:minimum_agent)
      get :index
    end
  end

  describe "#new" do
    before { login(:minimum_admin) }

    it "renders" do
      get :new, params: { ticket_field: {type: 'FieldTagger'} }
      assert_response :ok
      assert_template 'edit'
    end

    it "does not crash when using the field type as the :id parameter" do
      get :new, params: { id: 'field_textarea' }
      assert_response :ok
      assert_select "input[value='FieldTextarea']", 1
    end

    it "returns bad_request error when passed an invalid type" do
      get :new, params: { ticket_field: {type: 'unknown field'} }
      assert_response :bad_request
    end
  end

  describe "#create" do
    it "creates a tagger" do
      login('minimum_admin')

      post :create, params: { ticket_field: {title: 'hej', custom_field_options: valid_tags, type: 'FieldTagger'} }
      assert_equal 2, accounts(:minimum).field_taggers.count
      assert_redirected_to action: :index, controller: :ticket_fields
    end

    it "sanitizes user input" do
      login('minimum_admin')
      post :create, params: { ticket_field: {title: '<script>alert("boo")</script>New name', custom_field_options: valid_tags, type: 'FieldTagger'} }
      assert_equal 2, accounts(:minimum).field_taggers.count
      assert_equal "New name", accounts(:minimum).field_taggers.reload.last.title
    end
  end

  describe "#edit" do
    it "renders" do
      login('minimum_admin')

      get :edit, params: { id: ticket_fields(:field_tagger_custom).id }
      assert_response :ok
      assert_template 'edit'
    end
  end

  describe "#update" do
    it "updates" do
      login('minimum_admin')
      field = ticket_fields(:field_tagger_custom)
      put :update, params: { id: field.id, ticket_field: {title: 'hej', custom_field_options: valid_tags} }
      assert_redirected_to action: :index, controller: :ticket_fields
      assert_equal 2, field.reload.custom_field_options(true).length
    end

    it 'sanitizes user input on custom field update' do
      login('minimum_admin')
      field = ticket_fields(:field_tagger_custom)
      put :update, params: { id: field.id, ticket_field: {title: 'xss "><img src=x onerror=prompt(/xss/) x=', custom_field_options: valid_tags} }
      assert_equal "xss \">&lt;img src=x onerror=prompt(/xss/) x=", field.reload.title
    end

    it 'skips non provided params' do
      login('minimum_admin')
      field = @request.account.field_status
      put :update, params: { id: field.id, ticket_field: { title_in_portal: 'Status', description: 'request status', use_status_hold: 1, type: 'FieldStatus' } }
      assert_equal 'Status', field.reload.title
    end

    it "does not update with invalid params" do
      login('minimum_admin')
      put :update, params: { id: ticket_fields(:field_tagger_custom).id, ticket_field: {title: ""} }
      assert flash[:error]
    end

    it "does not update with empty tagger field options" do
      login('minimum_admin')
      field = ticket_fields(:field_tagger_custom)
      field.custom_field_options = [custom_field_options(:field_tagger_custom_option_1), custom_field_options(:field_tagger_custom_option_2)]
      field.save!
      put :update, params: { id: ticket_fields(:field_tagger_custom).id, ticket_field: {title: 'hej', custom_field_options: nil} }, as: :json
      assert flash[:error]
    end
  end

  describe "#destroy" do
    it "destroys" do
      login('minimum_admin')
      delete :destroy, params: { id: ticket_fields(:field_tagger_custom).id }
      assert_redirected_to action: :index, controller: :ticket_fields
    end
  end

  describe "#activate" do
    let(:ticket_field) { ticket_fields(:field_tagger_custom) }
    let(:credit_card_field) { FieldPartialCreditCard.create!(account: accounts(:minimum), title: 'credit card field', is_active: false) }

    before { login('minimum_admin') }

    it "does not change ticket field activation when the field is an app requirement" do
      FactoryBot.create(:resource_collection_resource,
        account_id: ticket_field.account.id,
        resource: ticket_field)
      assert ticket_field.is_active?
      put :activate, params: { id: ticket_field.id }
      assert ticket_field.reload.is_active?
    end

    it "deactivates" do
      put :activate, params: { id: ticket_field.id }
      assert_redirected_to action: :index, controller: :ticket_fields
      refute ticket_field.reload.is_active?
    end

    it "activates" do
      ticket_field.toggle!
      put :activate, params: { id: ticket_field.id }
      assert_redirected_to action: :index, controller: :ticket_fields
      assert ticket_field.reload.is_active?
    end

    it "shows a flash error when the field cannot be activated" do
      ticket_field = ticket_fields(:minimum_field_status)
      put :activate, params: { id: ticket_field.id }
      assert_redirected_to action: :index, controller: :ticket_fields
      flash[:error].must_include "Ticket field 'Status' cannot be deactivated"
    end

    it "does not allow a partial credit card field to be activated without the pci_credit_card_custom_field feature" do
      put :activate, params: { id: credit_card_field.id }

      assert_redirected_to action: :index, controller: :ticket_fields
      assert_includes flash[:error], "Advanced Security Add-on required to activate the 'credit card field' field"
      refute credit_card_field.reload.is_active?
    end

    it "allows a partial credit card field to be activated with the pci_credit_card_custom_field feature" do
      Account.any_instance.stubs(has_pci_credit_card_custom_field?: true)
      put :activate, params: { id: credit_card_field.id }

      assert_redirected_to action: :index, controller: :ticket_fields
      assert_nil flash[:error]
      assert credit_card_field.reload.is_active?
    end

    it "does not allow a field to be activated if it has the same tags as an already active field" do
      field1 = FieldCheckbox.create!(title: "Wombat Checkbox", account: accounts(:minimum), tag: "wombats")
      field1.update_attributes(is_active: false)
      field2 = ticket_fields(:field_tagger_custom)
      field2.custom_field_options.build(name: "Wombats", value: "wombats")
      field2.save

      Timecop.travel(Time.now + 1.seconds)
      put :activate, params: { id: field1.id }
      assert_redirected_to action: :index, controller: :ticket_fields
      flash[:error].must_include "The tag <strong>wombats</strong> is already used in a custom field drop-down, multi-select or checkbox."
    end
  end

  private

  def valid_tags
    [{'name' => 'hej title', 'value' => 'hej'}, {'name' => 'hello title', 'value' => 'hello'}]
  end
end
