require_relative "../support/test_helper"

SingleCov.covered! uncovered: 12

describe TagsController do
  fixtures :accounts, :users, :tickets, :ticket_fields, :role_settings

  with_options(controller: "tags") do |request|
    request.should_route :get, "/tags/autotag", action: 'autotag'
    request.should_route :get, "/tags/show/autotag", action: 'show', id: 'autotag'
    request.should_route :get, "/tags/show/this.has.dots", action: 'show', id: 'this.has.dots'
    request.should_route :get, "/tags/this.has.dots/entries", action: 'show', for: 'entry', id: 'this.has.dots'
  end

  should_include_module Zendesk::LotusEmbeddable

  before do
    @account = accounts(:minimum)
    @request.account = @account
    login('minimum_admin')
    @account.taggings.each(&:destroy)
  end

  describe "#index" do
    describe "for agents" do
      before do
        @agent = users(:minimum_agent)
        login(@agent)
      end

      it "renders a forbidden page" do
        @account.tag_scores << TagScore.new(tag_name: "apples", score: 10)
        get :index
        assert_response :forbidden
      end
    end

    it "renders a tag cloud" do
      @account.tag_scores << TagScore.new(tag_name: "apples", score: 10)
      @account.tag_scores << TagScore.new(tag_name: "oranges", score: 10)
      get :index
      assert_response :success
    end

    it "renders with funny tags" do
      @account.tag_scores << TagScore.new(tag_name: "fu/bar", score: 10)
      get :index
      assert_response :success
    end

    it "registers activity on the current device" do
      @controller.expects(:register_device_activity)
      get :index
    end
  end

  describe "#show" do
    it "renders with forward slash as triple pipe" do
      ZendeskSearch::Client.any_instance.expects(:search).
        with("order_by:updated_at sort:desc tags:this/that", is_a(Hash)).
        returns(count: 0, results: [])
      get :show, params: { id: "this|||that", for: 'ticket' }
      assert_response :success
      assert_includes @response.body, "https://#{@request.host}/tags/show/this%7C%7C%7Cthat?for=ticket"
      assert_includes @response.body, "No tickets found matching this/that"
    end

    it "does not render tags with invalid characters" do
      ZendeskSearch::Client.any_instance.stubs(:search).returns(count: 0, results: [])
      get :show, params: { id: "Go%20to%20evil.com", for: 'ticket' }

      assert_response :success
      assert_includes @response.body, "https://#{@request.host}/tags/show/Go%2520to%2520evil.com"
      assert_includes @response.body, "Tag cannot contain special characters. Use only underscore and regular characters."
    end

    it "renders rss" do
      ZendeskSearch::Client.any_instance.stubs(:search).returns(count: 0, results: [])
      get :show, params: { id: "blah bleh", for: 'ticket', format: "rss" }
      assert_response :success
      assert_includes @response.body, "<link>https://#{@request.host}/tags/show/blah%20bleh</link>"
    end

    it "renders rss with correct link for tag that shadows an action" do
      ZendeskSearch::Client.any_instance.stubs(:search).returns(count: 0, results: [])
      get :show, params: { id: "autotag", for: 'ticket', format: "rss" }
      assert_response :success
      assert_includes @response.body, "<link>https://#{@request.host}/tags/show/autotag</link>"
    end

    it "generates correct search query" do
      Zendesk::Search.expects(:search).
        with(anything, "tags:foo", has_entries(with: {type: :ticket})).
        returns(Zendesk::Search::Results.empty)
      get :show, params: { id: "foo", for: 'ticket' }
      assert_response :success
    end

    it "generates correct search query with hyphen" do
      Zendesk::Search.expects(:search).
        with(anything, "tags:-", has_entries(with: {type: :ticket})).
        returns(Zendesk::Search::Results.empty)
      get :show, params: { id: "-", for: 'ticket' }
      assert_response :success
    end
  end

  describe "#destroy" do
    it "deletes taggings and tag scores" do
      tag_name = "pferdchen"
      ticket   = tickets(:minimum_1)
      ticket.additional_tags = tag_name
      ticket.will_be_saved_by(users(:minimum_admin))
      ticket.save

      assert_equal 1, Tagging.where(tag_name: tag_name, taggable_type: 'Ticket').count(:all)
      TagScore.expects(:update).with(@account, tag_name)
      delete :destroy, params: { id: tag_name }
      assert_response :redirect
      assert_equal 0, Tagging.where(tag_name: tag_name, taggable_type: 'Ticket').count(:all)
    end
  end

  describe "#bulk tag removal" do
    before do
      login('minimum_admin')

      @tag_name = "pferdchen"
      ticket = tickets(:minimum_1)
      ticket.additional_tags = @tag_name
      ticket.will_be_saved_by(users(:minimum_admin))
      ticket.save

      @tickets = [ticket]
    end

    describe "for HTML" do
      before do
        assert_equal 1, Tagging.where(tag_name: @tag_name, taggable_type: 'Ticket').count(:all)
        TagScore.expects(:update).with(@account, @tag_name)
        put :bulk, params: { id: @tag_name }
      end

      it "removes the tag from the tickets properly" do
        assert_equal "Tag \"pferdchen\" successfully removed from:<ul><li>1 open ticket</li></ul>", flash[:notice]
        assert_response :redirect
        assert_equal 0, Tagging.where(tag_name: @tag_name, taggable_type: 'Ticket').count(:all)
      end

      should_set_the_flash_to /1 open ticket/
    end

    describe "for GET" do
      before { get :bulk, params: { id: @tag_name } }
      it('responds with bad_request') { assert_response :bad_request }
    end

    describe "for agent" do
      before do
        login('minimum_agent')
        get :bulk, params: { id: @tag_name }
      end
      it('responds with bad_request') { assert_response :bad_request }
    end
  end

  describe "#autotag" do
    it "renders" do
      Thesaurus.expects(:autotag).with("xxx", anything).returns ["zzz"]
      xhr :get, :autotag, params: { text: "xxx", target: "yyy" }
      assert_response :success
      assert_equal "yyy.addEntry(\"zzz\");", @response.body
    end

    it "does not re-render arbitrary target input" do
      Thesaurus.expects(:autotag).with("xxx", anything).returns ["zzz"]
      xhr :get, :autotag, params: { text: "xxx", target: "yYy.a12 asdsfdds \nevil foo" }
      assert_response :success
      assert_equal "yYy.a12.addEntry(\"zzz\");", @response.body
    end
  end
end
