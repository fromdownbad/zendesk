require_relative "../support/test_helper"
require_relative "../support/brand_test_helper"

SingleCov.covered! uncovered: 3

describe CertificateSigningRequestController do
  include BrandTestHelper
  fixtures :accounts, :brands, :certificates, :role_settings

  before do
    @minimum_account = accounts(:minimum)
    @minimum_brand   = brands(:minimum)
    @minimum_account.update_attribute(:is_ssl_enabled, true)
    @request.account = @minimum_account
  end

  as_an_agent do
    should_be_forbidden :generate, :cancel
  end

  describe "as an admin" do
    before do
      @minimum_admin = users(:minimum_admin)
      login(@minimum_admin)
    end

    describe "#generate" do
      it "does not generate a certificate request for accounts without host mapping" do
        @minimum_account.update_attribute(:host_mapping, nil)

        get :generate

        assert_redirected_to settings_security_path(anchor: "ssl")
        assert_equal I18n.t('txt.admin.controllers.settings.security_controller.you_must_have_host_mapping_defined'), flash[:error]
      end

      it "generates a certificate request for an account with no host mapping but with brand with host mapping" do
        @minimum_account.stubs(:has_multibrand?).returns(true)
        @minimum_account.update_attribute(:host_mapping, nil)
        create_brand(@minimum_account, name: 'donald', subdomain: 'donald', host_mapping: 'donald.com')

        get :generate

        assert_response :success
        assert_equal "/C=/ST=City/L=City/O=Foo/OU=Foo/CN=donald.com",
          @minimum_account.certificates.temporary.last.csr_object.subject.to_s
      end

      it "generates a certificate request for an account with host mapping" do
        @minimum_account.update_attribute(:host_mapping, "carlos")

        get :generate

        assert_response :success
        assert_equal "/C=/ST=City/L=City/O=Foo/OU=Foo/CN=carlos",
          @minimum_account.certificates.temporary.last.csr_object.subject.to_s
      end

      describe "#generate for changed CSR subject" do
        before do
          @minimum_account.update_attribute(:host_mapping, "random.example.com")

          certificate = @minimum_account.certificates.create
          certificate.generate_temporary
          certificate.save
        end

        it "regenerates a CSR if CSR subject has changed" do
          @minimum_account.address.update_attribute(:name, "something new")

          get :generate

          assert_response :success
          # side effect: temporary cert deleted and recreated
          assert_equal "/C=/ST=City/L=City/O=something new/OU=something new/CN=random.example.com",
            @minimum_account.reload.certificates.temporary.last.csr_object.subject.to_s
        end
      end

      describe "#generate for changed SANs" do
        before do
          create_brand(@minimum_account, name: 'donald', subdomain: 'donald', host_mapping: 'donald.com')
          create_brand(@minimum_account, name: 'daffy', subdomain: 'daffy', host_mapping: 'daffy.com')

          certificate = @minimum_account.certificates.create
          certificate.generate_temporary
          certificate.save
        end

        it "regenerates a CSR if host mapped brands have been added" do
          @minimum_account.stubs(:has_multibrand?).returns(true)
          create_brand(@minimum_account, name: 'count', subdomain: 'count', host_mapping: 'count.com')

          get :generate

          assert_response :success
          # side effect: temporary cert deleted and recreated
          assert @minimum_account.reload.certificates.temporary.last.csr_object.to_text.include?('count.com'), 'new brand missing from SAN'
        end
      end
    end

    describe "#cancel" do
      before do
        @minimum_account.update_attribute(:host_mapping, "random.example.com")
        certificate = @minimum_account.certificates.create
        certificate.state = "pending"
        certificate.save!
      end

      it "cancels a pending certificate" do
        get :cancel

        assert_redirected_to settings_security_path(anchor: "ssl")
        assert_equal I18n.t('txt.admin.controllers.settings.security_controller.pending_certificate_cancelled'), flash[:notice]
      end
    end
  end
end
