require_relative "../support/test_helper"
require_relative "../support/collaboration_settings_test_helper"

SingleCov.covered! uncovered: 39

describe TicketsController do
  fixtures :all

  should_be_unauthorized_when_not_logged_in([:index, :new, :edit])

  before do
    CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
    @current_account = accounts(:minimum)
    @current_account.settings.prefer_lotus = false
    @current_account.save!
    @request.account = @current_account
    ActionMailer::Base.deliveries = []
  end

  it "recognizes /rules.xml" do
    assert_recognizes({controller: 'tickets', action: 'index', format: 'xml'}, '/rules.xml')
  end

  it "routes restful actions" do
    do_routing('tickets')
  end

  describe "basic actions" do
    describe "when logged in as an end user" do
      before do
        login('minimum_end_user')
      end

      describe "#show" do
        describe "when hc disabled" do
          before do
            get :show, params: { id: tickets(:minimum_1).nice_id }
          end

          it "redirects to subdomain" do
            assert_redirected_to "/"
          end
        end

        describe "when hc enabled" do
          before do
            https!
            Account.any_instance.stubs(help_center_enabled?: true)
            get :show, params: { id: tickets(:minimum_1).nice_id }
          end

          it "redirects to the requests controller" do
            assert_redirected_to "/hc/requests/1"
          end
        end

        describe "when the ticket is not found" do
          before { get :show, params: { id: (Ticket.maximum("id") + 1) } }

          it('responds with not_found') { assert_response :not_found }
        end
      end

      describe "#index" do
        before { get :index, params: {} }
        it "redirects to the requests controller" do
          assert_redirected_to({action: :index, controller: 'requests/portal'}.merge({}))
        end
      end

      describe "#new" do
        before { get :new, params: {} }
        it "redirects to the requests controller" do
          assert_redirected_to({action: :new, controller: 'requests/portal'}.merge({}))
        end
      end
    end

    describe "when logged in as an agent" do
      before { login("minimum_agent") }

      describe "a request to #index" do
        it "redirects to the first available active View" do
          @current_account.all_views.update_all(is_active: false)
          available_view = @current_account.all_views.first
          available_view.update_attribute(:is_active, true)
          get :index

          assert_equal 'text/html', @response.content_type
          assert_redirected_to rule_path(available_view)
        end

        it "redirects to a new View when no views are available" do
          @current_account.all_views.destroy_all
          assert_equal 0, @current_account.all_views.count(:all)

          get :index
          assert_redirected_to new_rule_path(filter: 'view')
        end
      end

      describe "a request to show a ticket" do
        before { get :show, params: { id: tickets(:minimum_2).nice_id } }
        it('responds with ok') { assert_response :ok }
        it('responds with content_type html') { assert_equal 'text/html', @controller.response.content_type.to_s }
      end

      describe "a request to #new" do
        before { get :new }
        it "responds with redirect" do
          assert_response :redirect
          assert_redirected_to "/requests/new"
        end
      end
    end

    describe "when logged in as an admin" do
      before { login("minimum_admin") }

      describe "a request to show a ticket" do
        before { get :show, params: { id: tickets(:minimum_2).nice_id } }
        it('responds with ok') { assert_response :ok }
        it('responds with content_type html') { assert_equal 'text/html', @controller.response.content_type.to_s }
      end

      describe "with deprecate_tickets_controller_show" do
        before { Arturo.enable_feature! :deprecate_tickets_controller_show }
        before { get :show, params: { id: tickets(:minimum_2).nice_id } }
        it('responds with gone') { assert_response :gone }
      end

      describe "a RSS request to show a ticket" do
        # NOTE this does not work on customers without help_center_enabled
        before do
          Account.any_instance.stubs(:help_center_enabled?).returns(true)
          get :show, params: { id: tickets(:minimum_2).nice_id, format: 'rss' }
        end
        it "responds with RSS" do
          assert_response :ok
          assert_select 'rss'
        end
      end

      describe "a request to #new" do
        before { get :new }
        it "responds with redirect" do
          assert_response :redirect
          assert_redirected_to "/requests/new"
        end
      end
    end
  end

  describe "destroying tickets" do
    before do
      Ticket.any_instance.expects(:destroy).never
    end

    describe "as an agent" do
      before { login(users(:minimum_agent)) }
      describe "with ticket_delete_for_agents feature" do
        before do
          account = accounts(:minimum)
          account.settings.enable(:ticket_delete_for_agents)
          account.save!

          agent = users(:minimum_agent)
          agent.account.settings.reload
        end

        describe "using #bulk" do
          before { post :bulk, params: { tickets_to_bulk_update: "#{tickets(:minimum_2).nice_id}, #{tickets(:minimum_1).nice_id}", submit_type: 'delete', ticket: {}, source_page: tickets_url, quiet: true } }
          it('responds with found') { assert_response :found }
          should_change("the number of tickets", by: -2) { Ticket.count(:all) }
          should_redirect_to("the source_page") { tickets_url }
        end
      end

      describe "without ticket_delete_for_agents feature" do
        describe "using #bulk" do
          before { post :bulk, params: { tickets_to_bulk_update: tickets(:minimum_2).nice_id.to_s, submit_type: 'delete', ticket: {}, quiet: true } }
          it('responds with found') { assert_response :found }
          should_not_change("the number of tickets") { Ticket.count(:all) }
        end
      end
    end

    describe "as an admin" do
      before { login(users(:minimum_admin)) }

      describe "using #bulk" do
        before { post :bulk, params: { tickets_to_bulk_update: "#{tickets(:minimum_1).nice_id},#{tickets(:minimum_2).nice_id}", submit_type: 'delete', ticket: {}, quiet: true } }
        it('responds with found') { assert_response :found }
        should_change("the number of tickets", by: -2) { Ticket.count(:all) }
      end
    end
  end

  describe "adding task to calendar" do
    before { login(users(:minimum_admin)) }

    it "sends data with the correct mime type" do
      get :show, params: { id: tickets(:minimum_2).nice_id, format: 'ics' }

      assert_equal 'text/calendar', response.header['Content-Type']
    end

    describe "from a windows machine" do
      before do
        @request.user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; rv:1.9.1.4) Gecko/20091016 Firefox/3.5.4 (.NET CLR 3.5.30729)'
        get :show, params: { id: tickets(:minimum_2).nice_id, format: 'ics' }
      end

      it "generates ics file with version 1.0" do
        assert @response.body.include?('VERSION:1.0')
      end
    end

    describe "from a linux machine" do
      before do
        @request.user_agent = 'Mozilla/5.0 (X11; U; Linux; en-US) AppleWebKit/527+ (KHTML, like Gecko, Safari/419.3) konqueror/4.3.2'
        get :show, params: { id: tickets(:minimum_2).nice_id, format: 'ics' }
      end

      it "generates ics file with version 2.0" do
        assert @response.body.include?('VERSION:2.0')
      end
    end

    describe "from a mac machine" do
      before do
        @request.user_agent = 'Opera/9.80 (Macintosh; Intel Mac OS X; U; en) Presto/2.2.15 Version/10.10'
        get :show, params: { id: tickets(:minimum_2).nice_id, format: 'ics' }
      end

      it "generates ics file with version 2.0" do
        assert @response.body.include?('VERSION:2.0')
      end
    end

    it "only have a date with no time in the ics file" do
      get :show, params: { id: tickets(:minimum_2).nice_id, format: 'ics' }
      assert @response.body.include?('VALUE=DATE')
    end

    it "removes carraige returns" do
      Ticket.any_instance.stubs(:description).returns("1st line\r\n2nd line\r\n3rd line")
      get :show, params: { id: tickets(:minimum_2).nice_id, format: 'ics' }
      assert_no_match /\r/, @response.body
      assert_includes @response.body, "1st line\\n2nd line\\n3rd line"
    end
  end

  describe "On GET to :new" do
    describe "when logged in as an end-user" do
      before { login('minimum_end_user') }

      it "responds with redirect" do
        get :new
        assert_response :redirect
        assert_redirected_to "/requests/new"
      end
    end
  end

  describe "On GET to :show" do
    before do
      login('minimum_agent')
      @ticket = tickets(:minimum_1)
    end

    describe "for a Ticket created from an Entry" do
      before do
        @entry = entries(:ponies)
        @ticket.will_be_saved_by(@ticket.submitter)
        @ticket.update_attribute(:entry_id, @entry.id)
        get :show, params: { id: @ticket.nice_id.to_s }
      end
      it('responds with ok') { assert_response :ok }
      should_render_template :show

      it "defaults the comment to a private one" do
        refute assigns(:comment).is_public?
      end
    end

    describe "for a non existent Ticket" do
      before do
        get :show, params: { id: "9999" }
      end
      it('responds with not_found') { assert_response :not_found }
    end

    describe "for a non existent Ticket from IE" do
      before do
        # Send retarded accept headers used by IE: http://tinyurl.com/2fl4tbe
        @request.accept = 'image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, application/x-ms-application, application/vnd.ms-xpsdocument, application/xaml+xml, application/x-ms-xbap, */*'
        @request.user_agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)'
        get :show, params: { id: "9999" }
      end
      it('responds with not_found') { assert_response :not_found }
      it('responds with content_type html') { assert_equal 'text/html', @controller.response.content_type.to_s }
    end

    describe "for a ticket you don't have access to" do
      before do
        User.any_instance.stubs(:can?).returns(false)
        get :show, params: { id: tickets(:minimum_1).nice_id }
      end
      it('responds with forbidden') { assert_response :forbidden }
    end
  end

  describe "bulk ticket update" do
    before do
      login('minimum_agent')
      @tickets = [tickets(:minimum_1), tickets(:minimum_2)]
      @update_params = {
        tickets_to_bulk_update: @tickets.map(&:nice_id).join(','),
        ticket: {
          priority_id: PriorityType.High,
          set_tags: 'hilarious',
          fields: {
            ticket_fields(:field_textarea_custom).id.to_s => NO_CHANGE.dup,
            ticket_fields(:field_text_custom).id.to_s => "ORLY?",
            ticket_fields(:field_tagger_custom).id.to_s => "hilarious"
          }
        },
        comment: {value: 'hello {{ticket.requester.name}}', is_public: "1"},
        macro_applied: ""
      }
    end

    describe "with macro usage statistics enabled" do
      before do
        @update_params[:macro_applied] = accounts(:minimum).macros.first.id
        Account.any_instance.stubs(:has_rule_usage_stats?).returns(true)
      end
      it "creates rule_ticket_joins for each (ticket, macro) pair" do
        assert_difference("accounts(:minimum).macros.first.rule_ticket_joins.tickets.count(:all)", @tickets.count) do
          post :bulk, params: @update_params
        end
        assert_equal @tickets.sort_by(&:id), accounts(:minimum).macros.first.rule_ticket_joins.tickets.sort_by(&:id)
      end
    end

    describe "for HTML" do
      before do
        post :bulk, params: @update_params
        assert_response :redirect
      end

      it "updates the tickets properly" do
        @tickets.each do |ticket|
          ticket.reload
          assert_equal PriorityType.High, ticket.priority_id
          assert_equal 'hello ' + ticket.requester.name, ticket.comments.last.to_s
          assert_equal ticket.comments.last.is_public, true
          assert_equal 'hilarious', ticket.current_tags
          assert_equal ticket.current_tags, ticket.tag_array.sort.join(' ')
          assert_equal ticket_fields(:field_textarea_custom).value(ticket), ""
          assert_equal ticket_fields(:field_text_custom).value(ticket), "ORLY?"
          assert_equal ticket_fields(:field_tagger_custom).value(ticket), "hilarious"
        end
      end

      should_set_the_flash_to /2 ticket\(s\) processed/
    end

    describe "with different locale than English" do
      before do
        @portuguese = translation_locales(:brazilian_portuguese)
        @agent = users(:minimum_agent)

        login(@agent)
        @tickets = [tickets(:minimum_1), tickets(:minimum_2)]
        @update_params = {
          tickets_to_bulk_update: @tickets.map(&:nice_id).join(','),
          ticket: {
            priority_id: PriorityType.High,
            set_tags: 'hilarious',
            fields: {
              ticket_fields(:field_textarea_custom).id.to_s => I18n.t(NO_CHANGE, locale: @portuguese),
              ticket_fields(:field_text_custom).id.to_s => "ORLY?",
              ticket_fields(:field_tagger_custom).id.to_s => "hilarious"
            }
          },
          comment: {value: 'hello {{ticket.requester.name}}', is_public: "1"}
        }
      end

      it "updates the tickets properly and not change no change field" do
        @agent.translation_locale = @portuguese
        @agent.save!

        post :bulk, params: @update_params

        assert_response :redirect

        assert_match /2 ticket\(s\) processed/, flash[:notice]

        @tickets.each do |ticket|
          ticket.reload
          assert_equal PriorityType.High, ticket.priority_id
          assert_equal 'hello ' + ticket.requester.name, ticket.comments.last.to_s
          assert(ticket.comments.last.is_public)
          assert_equal 'hilarious', ticket.current_tags
          assert_equal ticket.current_tags, ticket.tag_array.sort.join(' ')
          assert_equal "", ticket_fields(:field_textarea_custom).value(ticket)
          assert_equal "ORLY?", ticket_fields(:field_text_custom).value(ticket)
          assert_equal "hilarious", ticket_fields(:field_tagger_custom).value(ticket)
        end
      end

      it "updates the tickets properly and not change no change field when agent has different locale from account" do
        @agent.locale_id = nil
        @agent.save!
        @agent.account.locale_id = @portuguese.id
        @agent.account.save!
        @update_params[:ticket][:fields][ticket_fields(:field_textarea_custom).id.to_s] = I18n.t(NO_CHANGE)

        post :bulk, params: @update_params

        assert_response :redirect

        assert_match /2 ticket\(s\) processed/, flash[:notice]

        @tickets.each do |ticket|
          ticket.reload
          assert_equal PriorityType.High, ticket.priority_id
          assert_equal 'hello ' + ticket.requester.name, ticket.comments.last.to_s
          assert(ticket.comments.last.is_public)
          assert_equal 'hilarious', ticket.current_tags
          assert_equal ticket.current_tags, ticket.tag_array.sort.join(' ')
          assert_equal "", ticket_fields(:field_textarea_custom).value(ticket)
          assert_equal "ORLY?", ticket_fields(:field_text_custom).value(ticket)
          assert_equal "hilarious", ticket_fields(:field_tagger_custom).value(ticket)
        end
      end
    end

    describe "when there are no tickets_to_bulk_update in the parameters" do
      before do
        post :bulk
      end
      should_set_the_flash_to /0 ticket\(s\) processed/
    end
  end

  describe "incidents" do
    before do
      Account.any_instance.stubs(:has_extended_ticket_types?).returns(true)
      tickets(:minimum_2).will_be_saved_by(tickets(:minimum_2).submitter)
      tickets(:minimum_2).update_attributes! ticket_type_id: TicketType.PROBLEM
      tickets(:minimum_1).will_be_saved_by(tickets(:minimum_1).submitter)
      tickets(:minimum_1).update_attributes! linked_id: tickets(:minimum_2).id
      login('minimum_agent')
    end

    describe "problem for incident" do
      before do
        get :show, params: { id: tickets(:minimum_1).nice_id }
      end

      it "renders problem box" do
        assert_select "#linked_problem"
      end
    end

    describe "incidents for problem" do
      before do
        get :show, params: { id: tickets(:minimum_2).nice_id }
      end

      it "renders incidents box" do
        assert_select "#associated_incidents"
      end
    end
  end

  describe "follow-ups" do
    before do
      login('minimum_agent')
    end

    it "redirects to requests portal on a new follow-up for a valid source" do
      get :new, params: { ticket: {via_followup_source_id: tickets(:minimum_5).nice_id} }
      assert_response :redirect
      assert_redirected_to "/requests/new"
    end

    it "does not blow up on closed tickets when the followup is missing a subject" do
      source = tickets(:minimum_5)
      assert(source.update_columns(status_id: StatusType.CLOSED))

      source.account.tickets.new do |t|
        t.via_followup_source_id = source.nice_id
        t.description = 'hellows'
        t.requester = source.requester
        t.via_id = ViaType.CLOSED_TICKET
        t.set_followup_source(source.requester)
        t.will_be_saved_by(source.requester)
      end.save

      get :show, params: { id: source.nice_id }
      assert_response :success
    end
  end

  describe "#show_landing_page_for_mobile" do
    before do
      login('minimum_agent')
      @ticket = tickets(:minimum_1)
    end

    describe "if request.is_mobile? is true" do
      let (:ipad_ua) { "Mozilla/5.0 (iPad; CPU 0S 5_0_1 like Mac OS X) AppleWebkit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3" }
      let (:iphone_ua) { "Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25	" }

      describe "and mobile_v2 is enabled" do
        before do
          @current_account.stubs(:has_mobile_switch_enabled?).returns(true)
        end

        describe "and referer is nil" do
          describe "accessing :new" do
            it "redirects to landing page" do
              return_to_url = "https://somehelpdesk.zendesk.com/tickets/new"
              unless RAILS4 # Rails 4 drops the additional query params on routes
                return_to_url += "?id=1"
              end
              @request.env['HTTP_HOST'] = 'somehelpdesk.zendesk.com'
              @request.env['PATH_INFO'] = '/tickets/new'

              get :new, params: { id: @ticket.nice_id }, session: { is_mobile: true }

              assert_redirected_to mobile_landing_page_path(params: {ticket_id: @ticket.nice_id, return_to: return_to_url})
            end
          end
          describe "accessing :show" do
            it "redirects to landing page" do
              return_to_url = "https://somehelpdesk.zendesk.com/tickets/show"
              @request.env['HTTP_HOST'] = 'somehelpdesk.zendesk.com'
              @request.env['PATH_INFO'] = '/tickets/show'

              get :show, params: { id: @ticket.nice_id }, session: { is_mobile: true }

              assert_redirected_to mobile_landing_page_path(params: {ticket_id: @ticket.nice_id, return_to: return_to_url})
            end
          end
        end

        describe "and current_user is not an agent" do
          before do
            login('minimum_end_user')
          end

          it "redirects to new request page if action was #new" do
            get :new
            assert_redirected_to new_request_path
          end

          it "redirects to requests/{id} page if action was #show" do
            get :show, params: { id: @ticket.nice_id }, session: { is_mobile: true }
            assert_redirected_to request_path(@ticket.nice_id)
          end

          it "redirects to requests page if action was #index" do
            get :index, session: { is_mobile: true }
            assert_redirected_to requests_path
          end
        end

        it "redirects to the landing page if request.referer is access_controller" do
          get :show, params: { id: @ticket.nice_id }, session: { is_mobile: true }
          assert_redirected_to mobile_landing_page_path(params: {return_to: ticket_url(@ticket.nice_id), ticket_id: @ticket.nice_id})
        end

        it "does not redirect to landing page if referer is different from nil" do
          request.env['HTTP_REFERER'] = 'something'
          get :show, params: { id: @ticket.nice_id }, session: { is_mobile: true }
          assert_redirected_to request_path(@ticket.nice_id)
        end
      end
    end

    describe "if request.is_mobile? is false" do
      before do
        @current_account.stubs(:has_mobile_switch_enabled?).returns(false)
      end

      it "goes to the new action" do
        get :new, params: { id: @ticket.nice_id }
        assert_response :redirect
        assert_redirected_to "/requests/new"
      end

      it "goes to the show action" do
        get :show, params: { id: @ticket.nice_id }
        assert_response :ok
      end
    end
  end

  describe "archive (v2) tickets" do
    before do
      login("minimum_agent")
      @ticket = tickets(:minimum_1)
      archive_and_delete @ticket
    end

    it "returns :ok" do
      get :show, params: { id: @ticket.nice_id }
      assert_response :ok
    end
  end

  describe "print ticket" do
    before do
      login("minimum_agent")
      Arturo.enable_feature! :print
      ticket = tickets(:minimum_2)
      get :print, params: { id: ticket.nice_id }
    end

    it "renders print partial" do
      assert_response :ok
    end
    should_render_template('tickets/_print')
  end

  describe "#bulk_result" do
    before do
      login("minimum_agent")
    end

    it "redirects and show summary" do
      Resque::Plugins::Status::Hash.expects(:get).with("xxx").returns "results" => [{"status" => "updated", "success" => true, "id" => 123}], "options" => {}
      get :bulk_result, params: { background: "xxx" }
      assert_response :redirect
      assert_includes flash[:notice], "123"
    end

    it "404 with unknown token" do
      get :bulk_result, params: { background: "xxx" }
      assert_response :not_found
      refute flash[:notice]
    end
  end

  describe "mobile deeplinking" do
    describe "arturo flags" do
      describe "user agents" do
        let (:ipad_ua) { "Mozilla/5.0 (iPad; CPU 0S 5_0_1 like Mac OS X) AppleWebkit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A405 Safari/7534.48.3" }
        let (:iphone_ua) { "iPhone 5 (iOS6)	Mozilla/5.0 (iPhone; CPU iPhone OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5376e Safari/8536.25" }
        let (:android_ua) { "Mozilla/5.0 (Linux; U; Android 4.0.4; en-gb; GT-I9300 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30" }
        let (:windows_ua) { "Mozilla/5.0 (compatible; MSIE 10.0; Windows Phone 8.0; Trident/6.0; IEMobile/10.0; ARM; Touch; NOKIA; Lumia 920)" }
        let (:zendesk_mobile_deeplink) { Zendesk::MobileDeeplink::Engine.routes.url_helpers }
        let (:ticket) { tickets(:minimum_1) }
        attr_reader :request
        let (:return_to_url) { "https://somehelpdesk.zendesk.com/tickets/#{ticket.nice_id}" }
        before do
          request.env['HTTP_HOST'] = 'somehelpdesk.zendesk.com'
          request.env['PATH_INFO'] = "/tickets/#{ticket.nice_id}"
        end
        describe "windows" do
          it "redirects windows to mobile redirect" do
            Account.any_instance.stubs(:has_windows_mobile_deeplinking?).returns(true)
            request.user_agent = windows_ua
            get :show, params: { id: ticket.nice_id }
            assert_redirected_to zendesk_mobile_deeplink.mobile_redirect_path(params: {return_to: return_to_url, ticket_id: ticket.nice_id})
          end
          describe "when arturo is off" do
            it "does not redirect to the mobile redirect page" do
              login("minimum_admin")
              Account.any_instance.stubs(:has_windows_mobile_deeplinking?).returns(false)
              request.user_agent = windows_ua
              get :show, params: { id: ticket.nice_id }
              assert_redirected_to mobile_landing_page_path(params: {ticket_id: ticket.nice_id, return_to: return_to_url})
            end
          end
        end
        describe "android" do
          describe "when arturo is on" do
            it "redirects android to mobile redirect" do
              Account.any_instance.stubs(:has_android_mobile_deeplinking?).returns(true)
              request.user_agent = android_ua
              get :show, params: { id: ticket.nice_id }
              assert_redirected_to zendesk_mobile_deeplink.mobile_redirect_path(params: {return_to: return_to_url, ticket_id: ticket.nice_id})
            end
          end
          describe "when arturo is off" do
            it "does not redirect to the mobile redirect page" do
              login("minimum_admin")
              Account.any_instance.stubs(:has_android_mobile_deeplinking?).returns(false)
              request.user_agent = android_ua
              get :show, params: { id: ticket.nice_id }
              assert_redirected_to mobile_landing_page_path(params: {ticket_id: ticket.nice_id, return_to: return_to_url})
            end
          end
        end
        describe "ios" do
          describe "when arturo is on" do
            it "redirects iphone to mobile redirect" do
              Account.any_instance.stubs(:has_ios_mobile_deeplinking?).returns(true)
              request.user_agent = iphone_ua
              get :show, params: { id: ticket.nice_id }
              assert_redirected_to zendesk_mobile_deeplink.mobile_redirect_path(params: {return_to: return_to_url, ticket_id: ticket.nice_id})
            end
            it "redirects ipad to mobile redirect" do
              Account.any_instance.stubs(:has_ios_mobile_deeplinking?).returns(true)
              request.user_agent = ipad_ua
              get :show, params: { id: ticket.nice_id }
              assert_redirected_to zendesk_mobile_deeplink.mobile_redirect_path(params: {return_to: return_to_url, ticket_id: ticket.nice_id})
            end
          end
          describe "when arturo is off" do
            it "does not redirect iphone to mobile redirect" do
              login("minimum_admin")
              Account.any_instance.stubs(:has_ios_mobile_deeplinking?).returns(false)
              request.user_agent = iphone_ua
              get :show, params: { id: ticket.nice_id }
              assert_redirected_to mobile_landing_page_path(params: {ticket_id: ticket.nice_id, return_to: return_to_url})
            end
            it "does not redirects ipad to mobile redirect" do
              login("minimum_admin")
              Account.any_instance.stubs(:has_ios_mobile_deeplinking?).returns(false)
              request.user_agent = ipad_ua
              get :show, params: { id: ticket.nice_id }
              assert_response :ok
            end
          end
        end
      end
    end
  end

  private

  def create_light_agent
    Account.any_instance.stubs(has_permission_sets?: true, has_light_agents?: true)

    @light_agent = User.create! do |user|
      user.account = accounts(:minimum)
      user.name = "Prince Light"
      user.email = "prince@example.org"
      user.roles = Role::AGENT.id
    end

    @light_agent.permission_set = PermissionSet.create_light_agent!(accounts(:minimum))
    @light_agent.save!
  end
end
