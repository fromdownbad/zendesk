require_relative "../../support/test_helper"

SingleCov.covered!

describe Twitter::ReviewedTweetsController do
  fixtures :users, :accounts

  before do
    login(:minimum_agent)
    @account = accounts(:minimum)
    @request.env['zendesk.account'] = @account
  end

  describe 'listing reviewed tweets' do
    before do
      ReviewedTweet.mark_as_reviewed(@account, '2')
      get :index, params: { format: 'json', tweet_ids: '1,2,3' }
    end

    it('responds with success') { assert_response :success }
    it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }

    it 'responds with the reviewed tweet ids' do
      assert_match(/\[2\]/, @response.body)
    end
  end

  describe 'marking tweets as reviewed' do
    before do
      post :create, params: { format: 'json', tweet_ids: '2,3' }
    end

    it('responds with created') { assert_response :created }
    it('responds with content_type json') { assert_equal 'application/json', @controller.response.content_type.to_s }

    it 'records the tweet ids' do
      assert_equal([2, 3], ReviewedTweet.reviewed(@account, '1,2,3'))
    end

    it 'returns the list of tweet ids that had just been reviewed' do
      assert_equal('[2,3]', @response.body)
    end
  end
end
