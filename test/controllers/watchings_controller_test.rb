require_relative "../support/test_helper"

SingleCov.covered! uncovered: 8

describe WatchingsController do
  fixtures :accounts, :users, :forums, :entries, :subscriptions, :addresses

  before do
    @request.account = accounts(:minimum)
  end

  should_route :post, "/watchings/create/12345", controller: "watchings", action: :create, id: 12345

  describe "as an anonymous user" do
    describe "#index" do
      before { get :index }
      should_show_login_page
    end

    describe "#create" do
      before { get :create }
      should_show_login_page
    end

    describe "#destroy" do
      before { get :destroy, params: { id: 1 } }
      should_show_login_page
    end
  end

  describe "#show" do
    before do
      login('minimum_agent')
      @user = users(:minimum_agent)
      @forum = forums(:announcements)
    end

    describe "when allow to subscribe to the forum" do
      before { get :show, params: { type: :forum, id: @forum.id } }
      should_render_template 'watchings/_subscribe'
    end

    describe "when making a request to a bogus watching" do
      before { get :show, params: { type: :forum, id: 54321 } }

      it "does not blow up" do
        assert @response.body.blank?
        assert_response :success
      end
    end

    describe "when not allowed to subscribe to the forum" do
      before do
        User.any_instance.expects(:can?).with(:watch, @forum).returns(false)
        get :show, params: { type: :forum, id: @forum.id }
      end

      it "does not render a template" do
        assert_template nil
      end
    end
  end

  describe "#index" do
    before do
      login('minimum_agent')
      @user = users(:minimum_agent)
      @forum = forums(:announcements)
      @entry = entries(:sticky)
      @forum.subscribe(@user)
      @entry.subscribe(@user)
    end

    describe "for forum subscriptions" do
      before { get :index, params: { type: 'forum' } }
      should_assign_to(:subscriptions) { [@forum] }
    end

    describe "for entry subscriptions" do
      before { get :index, params: { type: 'entry' } }
      should_assign_to(:subscriptions) { [@entry] }
    end

    describe "for all subscriptions" do
      before { get :index }
      should_assign_to(:subscriptions) { [@entry, @forum] }
    end

    describe "as curl" do
      before do
        @request.accept = "*/*"
        get :index
      end
      # Only xml format available for the moment
      it('responds with content_type xml') { assert_equal 'application/xml', @controller.response.content_type.to_s }
    end

    describe "as browser" do
      before do
        @request.accept = "text/html,*/*"
        get :index
      end
      # Only xml format available for the moment
      it('responds with content_type xml') { assert_equal 'application/xml', @controller.response.content_type.to_s }
    end

    describe "with xml format" do
      before { get :index, format: 'xml' }
      it('responds with content_type xml') { assert_equal 'application/xml', @controller.response.content_type.to_s }
      it "returns the requested subscriptions as xml" do
        expected_response = Time.use_zone(@user.time_zone) { [@entry, @forum].to_xml }
        assert_equal expected_response, @response.body
      end
    end
  end

  describe "#create" do
    before do
      @forum = forums(:announcements)
      @agent = users(:minimum_agent)
      @end_user = users(:minimum_end_user)
    end

    describe "as a logged in agent" do
      before do
        login('minimum_agent')
      end

      it "subscribes self to forum" do
        post :create, params: { id: @forum.id, type: 'forum', format: 'js' }

        assert @forum.subscribed?(@agent)
      end

      it "subscribes an end user to forum" do
        post :create, params: { id: @forum.id, type: 'forum', user_id: @end_user.id, format: 'json' }

        assert @forum.subscribed?(@end_user)
      end
    end

    describe "as a logged in end user" do
      before do
        login('minimum_end_user')
      end

      it "subscribes self to forum" do
        post :create, params: { id: @forum.id, type: 'forum', format: 'js' }

        assert @forum.subscribed?(@end_user)
      end

      it "does not be able to subscribe another user to forum" do
        post :create, params: { id: @forum.id, type: 'forum', user_id: @agent.id, format: 'json' }

        refute @forum.subscribed?(@agent)
      end
    end

    describe "as a logged in user with no email address" do
      before do
        User.any_instance.stubs(:has_email?).returns(false)
        login('minimum_agent')
        post :create, params: { id: @forum.id, type: 'forum', format: 'js' }
      end

      should render_template "create"
      it('responds with content_type js') { assert_equal 'text/javascript', @controller.response.content_type.to_s }

      it "does not subscribe user to forum" do
        refute @forum.subscribed?(@agent)
      end
    end
  end

  describe "#destroy as a logged in user" do
    before do
      login('minimum_agent')
      @user = users(:minimum_agent)
      @forum = forums(:announcements)
      @forum.subscribe(@user)
      post :destroy, params: { id: @forum.id, type: 'forum', format: 'js' }
    end

    should render_template "destroy"
    it('responds with content_type js') { assert_equal 'text/javascript', @controller.response.content_type.to_s }

    it "unsubscribes 'minimum_agent' from the forum" do
      refute @forum.subscribed?(@user)
    end
  end

  describe "#unsubscribe as an anonymous user" do
    before do
      @user = users(:minimum_agent)
      @forum = forums(:announcements)
      @forum.subscribe(@user)
      @watching = Watching.last
      get :unsubscribe, params: { token: @watching.token }
    end

    should_render_template :unsubscribe

    it "unsubscribes 'minimum_agent' from the forum" do
      refute @forum.subscribed?(@user)
    end
  end

  describe "#unsubscribe_comments as an anonymous user" do
    before do
      @user = users(:minimum_agent)
      @forum = forums(:announcements)
      @forum.subscribe(@user)
      @watching = Watching.last
      get :unsubscribe_comments, params: { token: @watching.token }
    end

    should_route :get, '/watchings/unsubscribe_comments/myponytoken', action: :unsubscribe_comments, token: "myponytoken"

    should_render_template :unsubscribe

    it "unsubscribes 'minimum_agent' from the forum's comments" do
      refute @forum.subscribed_with_posts?(@user)
    end
  end

  describe "Feedback messages." do
    before do
      @user = users(:minimum_agent)
      @forum = forums(:announcements)
      @entry = entries(:sticky)
    end

    describe "When unsubscribing from forum successfully" do
      before do
        @forum.subscribe(@user)
        @watching = Watching.last
        get :unsubscribe, params: { token: @watching.token }
      end

      it "displays proper success message" do
        assert_equal "You will no longer receive notifications when new topics added or comments are made in \"#{@forum.name}\"",
          assigns(:feedback_message)
      end
    end

    describe "When unsubscribing goes wrong" do
      before do
        @forum.subscribe(@user)
        @watching = Watching.last
        get :unsubscribe, params: { token: "a-wrong-token" }
      end

      it "displays error message" do
        assert_equal "No subscription found, it may have been removed previously",
          assigns(:feedback_message)
      end
    end

    describe "When unsubscribing from topics (Entry)" do
      before do
        @entry.subscribe(@user)
        @watching = Watching.last
        get :unsubscribe, params: { token: @watching.token }
      end

      it "displays proper success message" do
        assert_equal "You will no longer receive notifications when new comments are added to \"#{@entry.name}\"",
          assigns(:feedback_message)
      end
    end

    describe "When unsubscribing from comments (Post)" do
      before do
        @forum.subscribe(@user)
        @watching = Watching.last
        get :unsubscribe_comments, params: { token: @watching.token }
      end

      it "displays proper success message" do
        assert_equal "You have now been unsubscribed from receiving notification about new comments in \"#{@forum.name}\"",
          assigns(:feedback_message)
      end
    end
  end
end
