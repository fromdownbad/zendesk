require_relative "../support/test_helper"

SingleCov.covered! uncovered: 9

describe ExternalEmailCredentialsController do
  fixtures :all
  let(:existing_refresh_token) { "xxxxx" }

  def create_credential(username = "user@gmail.com")
    @account.external_email_credentials.new(
      username: username,
      current_user: users(:minimum_agent)
    ).tap do |external_email_credential|
      external_email_credential.encrypted_value = existing_refresh_token
      external_email_credential.save!
    end
  end

  before do
    @request.account = @account = accounts(:minimum)
    @minimum_admin   = users(:minimum_admin)
    shared_session[:csrf_token] = 'csrf'
  end

  should_be_unauthorized_when_not_logged_in([:destroy, :oauth_start])

  describe "#oauth_start" do
    it "redirects to google with API scopes" do
      login(@minimum_admin)
      get :oauth_start, params: { foo: 'bar' }
      assert_response :redirect
      assert_includes response.location, "https://accounts.google.com/o/oauth2/auth?access_type=offline"
      assert_includes response.location, "%3Ffoo%3Dbar"
      uri = URI.parse(response.location)
      hashed_csrf_token = Rack::Utils.parse_query(uri.query)["state"].split(':').last
      assert BCrypt::Password.new(hashed_csrf_token) == 'csrf'
      assert Rack::Utils.parse_query(uri.query)["scope"] =~ /auth\/gmail\.modify/
      assert_nil Rack::Utils.parse_query(uri.query)["scope"] =~ /mail\.google\.com/
    end

    describe 'when reconnecting an existing credential' do
      before do
        create_credential('support@z3nmail.com')
        @credential = @account.external_email_credentials.last
      end

      it 'suggests the email address to connect' do
        login(@minimum_admin)
        get :oauth_start, params: { id: @credential.id }
        assert_response :redirect
        uri = URI.parse(response.location)
        assert Rack::Utils.parse_query(uri.query)["login_hint"] =~ /support@z3nmail\.com/
      end

      it 'attempts to revoke the credential' do
        RevokeExternalEmailCredentialJob.expects(:work).once
        login(@minimum_admin)
        get :oauth_start, params: { id: @credential.id }
      end
    end
  end

  describe "#oauth_callback" do
    def create_user_with_same_email
      UserEmailIdentity.create! value: "user@gmail.com", account: @account, user: @minimum_admin
    end

    before do
      login(@minimum_admin)
      stub_request(:post, "https://accounts.google.com/o/oauth2/token").
        with(body: hash_including("code" => "CODE")).to_return(status: 200, body: '{"access_token":"ACCESS", "refresh_token":"REFRESH"}')
      stub_request(:get, "https://www.googleapis.com/oauth2/v1/userinfo?access_token=ACCESS").
        to_return(status: 200, body: '{"email":"user@gmail.com"}')
    end

    it "creates a corresponding recipient address record" do
      assert_empty @account.recipient_addresses.by_email("user@gmail.com")
      get :oauth_callback, params: { code: "CODE", csrf_token: BCrypt::Password.create('csrf') }
      assert_equal 1, @account.recipient_addresses.by_email("user@gmail.com").count(:all)
    end

    it "trades tokens, request email and store email" do
      get :oauth_callback, params: { code: "CODE", csrf_token: BCrypt::Password.create('csrf') }

      assert_redirected_to "/agent/admin/email"
      credential = @account.external_email_credentials.first
      assert_equal "user@gmail.com", credential.username
      assert_equal false, credential.initial_import
      assert_equal "REFRESH", credential.decrypted_refresh_token
    end

    it "does not log encryption attributes" do
      Rails.logger.stubs(:info)
      Rails.logger.
        expects(:info).
        with(regexp_matches(/encrypted_value|encryption_key_name|encryption_cipher_name/)).
        never

      get :oauth_callback, params: { code: "CODE", csrf_token: BCrypt::Password.create('csrf') }
    end

    describe 'when credential fails validation' do
      before { ExternalEmailCredential.any_instance.stubs(save: false) }

      it 'does not log' do
        Rails.logger.stubs(:info)
        Rails.logger.expects(:info).with(/External Email Credential saved: /).never
        get :oauth_callback, params: { code: "CODE", csrf_token: BCrypt::Password.create('csrf') }
      end
    end

    it "requires a valid csrf token" do
      get :oauth_callback, params: { code: "CODE" }

      assert_response :forbidden
    end

    it "rejects an invalid csrf token" do
      get :oauth_callback, params: { code: "CODE", csrf_token: 'foo' }

      assert_response :forbidden
    end

    it "sets initial import to true when it's true" do
      get :oauth_callback, params: { code: "CODE", initial_import: "true", csrf_token: BCrypt::Password.create('csrf') }
      assert(@account.external_email_credentials.first.initial_import)
    end

    it "redirects to lotus when lotus is preferred" do
      @account.settings.prefer_lotus = true
      @account.settings.save!
      assert @account.settings.prefer_lotus?
      get :oauth_callback, params: { code: "CODE", csrf_token: BCrypt::Password.create('csrf') }
      assert_redirected_to "/agent/admin/email"
    end

    it "fails when canceled" do
      get :oauth_callback, params: { error: "access_denied", csrf_token: BCrypt::Password.create('csrf') }
      assert_redirected_to "/agent/admin/email"
    end

    describe "updating existing credential by id" do
      let(:credential_username) { "user@gmail.com" }

      before do
        @credential = credential_username ? create_credential(credential_username) : create_credential
      end

      describe "when created without username" do
        let(:credential_username) { nil }

        it "updates" do
          get :oauth_callback, params: { code: "CODE", id: @credential.id, csrf_token: BCrypt::Password.create('csrf') }
          assert_equal [@credential.id], @account.external_email_credentials.map(&:id)
        end
      end

      it "updates when created with username" do
        get :oauth_callback, params: { code: "CODE", csrf_token: BCrypt::Password.create('csrf') }
        assert_equal [@credential.id], @account.external_email_credentials.map(&:id)
      end

      describe "when email returned is different from the existing credential" do
        let(:credential_username) { "otheruser@gmail.com" }

        it "does not update when the email returned does not match the one in the credential or the recipient address" do
          get :oauth_callback, params: { code: "CODE", id: @credential.id, csrf_token: BCrypt::Password.create('csrf') }
          assert_redirected_to "/agent/admin/email"
          assert flash[:error].is_a?(String)
        end

        it "updates when the email returned matches the one in the credential but not the one in the recipient address" do
          user_credential = create_credential("user@gmail.com")
          RecipientAddress.any_instance.stubs(:email).returns("otheruser@gmail.com")

          get :oauth_callback, params: { code: "CODE", id: user_credential.id, csrf_token: BCrypt::Password.create('csrf') }
          user_credential.reload
          user_credential.username.must_equal "user@gmail.com"
          refute flash[:error]
        end

        it "updates to the new email when it matches the one in the recipient address but not the one in the credential" do
          RecipientAddress.any_instance.stubs(:email).returns("user@gmail.com")

          get :oauth_callback, params: { code: "CODE", id: @credential.id, csrf_token: BCrypt::Password.create('csrf') }
          @credential.reload
          @credential.username.must_equal "user@gmail.com"
          refute flash[:error]
        end
      end
    end

    describe "redirect when access denied" do
      describe "when return_to_access_denied is given" do
        let(:access_denied_path) { "/denied" }

        def error_query
          error = {
            key: 'txt.admin.views.settings.email._settings.external_email_credentials.access_denied'
          }
          base64_error = Base64.urlsafe_encode64(error.to_json)
          { error: base64_error}.to_query
        end

        before { get :oauth_callback, params: { error: "access_denied", return_to: "/success", return_to_access_denied: access_denied_path, csrf_token: BCrypt::Password.create('csrf') } }

        describe "when access denied" do
          let(:access_denied_path) { '/agent/admin/email' }

          it { assert_redirected_to "/agent/admin/email?#{error_query}" }
        end

        describe "when given URL points to a valid page" do
          let(:access_denied_path) { '/agent/discovery/feature/email' }

          it { assert_redirected_to "/agent/discovery/feature/email?#{error_query}" }
        end

        describe "when given URL is absolute" do
          let(:access_denied_path) { "http://example.com" }

          it { assert_redirected_to "/agent/admin/email?#{error_query}" }
        end

        describe "when given URL appears scheme-relative but contains an absolute URL" do
          let(:access_denied_path) { "//http://example.com" }

          it { assert_redirected_to "/agent/admin/email?#{error_query}" }
        end

        describe "when given URL appears scheme-relative but contains a host" do
          let(:access_denied_path) { "//example.com" }

          it { assert_redirected_to "/agent/admin/email?#{error_query}" }
        end

        describe "when given URL appears relative but contains additional slashes and a host" do
          let(:access_denied_path) { "////example.com" }

          it { assert_redirected_to "/agent/admin/email?#{error_query}" }
        end

        describe "when given URL appears relative but contains additional slashes and does not point to a valid page" do
          let(:access_denied_path) { "////example" }

          it { assert_redirected_to "/agent/admin/email?#{error_query}" }
        end

        describe "when given URL is a byte-parsing attempt" do
          let(:access_denied_path) { "@16843009" }

          it { assert_redirected_to "/agent/admin/email?#{error_query}" }
        end
      end

      it "redirects to return_to when given and return_to_access_denied is not given" do
        get :oauth_callback, params: { error: "access_denied", return_to: "/done", csrf_token: BCrypt::Password.create('csrf') }
        assert_redirected_to "/done"
      end
    end

    it "does not create a recipient address if the address is already in use" do
      Account.any_instance.expects(:find_user_by_email).with("user@gmail.com").returns(users(:minimum_end_user))
      get :oauth_callback, params: { code: "CODE", csrf_token: BCrypt::Password.create('csrf') }
      assert_equal 0, @account.recipient_addresses.by_email("user@gmail.com").count(:all)
    end

    it "fails when user with this email already exists" do
      create_user_with_same_email
      get :oauth_callback, params: { code: "CODE", csrf_token: BCrypt::Password.create('csrf') }
      assert_redirected_to "/agent/admin/email"
      assert flash[:error].is_a?(String)
    end
  end

  describe "#destroy" do
    before { login(@minimum_admin) }
    let(:credential) { create_credential }

    it "destroys the external_email_credential" do
      delete :destroy, params: { id: credential.id }

      assert_response :redirect
      refute @account.reload.external_email_credentials.exists?
    end

    it "destroys the associated recipient_address" do
      assert recipient_address_id = credential.recipient_address.id
      delete :destroy, params: { id: credential.id }

      assert_raise(ActiveRecord::RecordNotFound) do
        @account.recipient_addresses.find(recipient_address_id)
      end
    end
  end
end
