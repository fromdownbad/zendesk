require_relative "../../support/test_helper"

SingleCov.covered!

# There are multiple "agreements controllers":
#
#   app/controllers/sharing/agreements_controller.rb
#   app/controllers/sharing_agreements_controller.rb
#   app/controllers/api/**/sharing_agreements_controller.rb
#
# app/controllers/sharing_agreements.controller.rb is user facing
# app/controllers/sharing/agreements_controller.rb) is for backend work
# app/controllers/api/** directories are for api work

describe Sharing::AgreementsController do
  fixtures :accounts

  before do
    @request.account = @account = accounts(:minimum)
  end

  describe "#create" do
    let(:data) do
      {
      'sender_url' => 'http://example.com/sharing',
      'receiver_url' => @account.sharing_url
    }
    end

    def assert_created(status = :created)
      @request.env['RAW_POST_DATA'] = data.to_json
      post :create, params: { uuid: '1234', format: 'json' }
      assert_response status, @response.body
    end

    it "creates" do
      assert_difference "Sharing::Agreement.count", +1 do
        assert_created
      end
    end

    it "creates even with mismatched protocol" do
      assert data['receiver_url'].sub!('http', 'https')
      assert_difference "Sharing::Agreement.count", +1 do
        assert_created
      end
    end

    it "does not create with bad data" do
      assert data.delete('sender_url')
      assert_created :unprocessable_entity
    end

    it "does not create a duplicate with the same uuid" do
      assert_created
      refute_difference "Sharing::Agreement.count" do
        assert_created :success
      end
    end
  end

  describe "#update" do
    def assert_updated(status = :success)
      put :update, params: { uuid: agreement.uuid, format: 'json' }
      assert_response status, @response.body
    end

    let(:direction) { :out }
    let(:agreement) { FactoryBot.create(:agreement, account: @account, direction: direction) }

    before do
      @ts_agreement = agreement.for_partner
      @ts_agreement.status = 'accepted'
      @request.env['RAW_POST_DATA'] = @ts_agreement.to_json
      set_header('X-Ticket-Sharing-Token', @ts_agreement.authentication_token)
    end

    it "updates an agreement" do
      refute_difference "Sharing::Agreement.count(:all)" do
        assert_updated
      end
      assert_equal :accepted, agreement.reload.status
    end

    it "fails on update to agreement without authentication" do
      @request.env.delete 'HTTP_X_TICKET_SHARING_TOKEN'
      assert_updated :unauthorized
    end

    describe 'inbound' do
      let(:direction) { :in }

      it "does not allow change from pending to accepted" do
        refute_difference "Sharing::Agreement.count" do
          assert_updated :unprocessable_entity
        end
        assert_equal :pending, agreement.reload.status
      end

      it "still changes to disabled" do
        @ts_agreement.status = 'disabled'
        refute_difference "Sharing::Agreement.count(:all)" do
          assert_updated :unprocessable_entity
        end
        assert_equal :pending, agreement.reload.status
      end
    end
  end
end
