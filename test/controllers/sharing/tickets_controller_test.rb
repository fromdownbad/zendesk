require_relative "../../support/test_helper"
require_relative "../../support/sharing_test_helper"

SingleCov.covered! uncovered: 7

describe Sharing::TicketsController do
  include SharingTestHelper

  fixtures :accounts, :tickets, :events, :subscriptions, :payments,
    :remote_authentications, :sequences, :ticket_fields, :users,
    :groups, :memberships, :account_property_sets, :rules

  resque_inline false

  def with_lock_timeout(value)
    old_value = Sharing::TicketsController::CREATION_LOCKING_TIMEOUT
    silence_warnings { Sharing::TicketsController.send(:const_set, :CREATION_LOCKING_TIMEOUT, value) }
    yield
  ensure
    silence_warnings { Sharing::TicketsController.send(:const_set, :CREATION_LOCKING_TIMEOUT, old_value) }
  end

  before do
    @request.account = @account = accounts(:minimum)
  end

  describe '#create' do
    let(:ts_ticket) { new_ticket_sharing_ticket }

    before do
      @agreement = create_valid_agreement(status: :accepted)
    end

    def assert_create(status: :created, agreement: @agreement)
      set_header('X-Ticket-Sharing-Token', agreement.authentication_token)
      set_body(ts_ticket.to_json)
      post :create, params: { uuid: ts_ticket.uuid }, format: 'json'
      assert_response status, @response.body
    end

    it 'creates a new shared ticket with a valid api_key' do
      assert_difference 'SharedTicket.count', +1 do
        assert_difference 'Ticket.count', +1 do
          assert_create
        end
      end
    end

    it "fails without api key" do
      @agreement.stubs(authentication_token: nil)
      assert_create status: :unauthorized
    end

    it "ignores invalid characters" do
      ts_ticket.subject = "Bad\xEF\xEF\xEF\xEF charså∆ƒ".force_encoding("UTF-8")
      assert_create
      SharedTicket.last.ticket.subject.must_equal "Bad charså∆ƒ"
    end

    it "saves the original id" do
      assert_create
      assert SharedTicket.last.original_id.present?
    end

    describe "when preserving idempotency" do
      it "does not create a duplicate in the same agreement" do
        assert_create status: :created
        refute_difference 'SharedTicket.count', +1 do
          assert_create status: :no_content
        end
      end

      describe "when in a new agreement" do
        it "does recreate a ticket" do
          assert_create status: :created

          @agreement.update_column(:status_id, 4)
          new_agreement = create_valid_agreement(status: :accepted)
          assert_create status: :created, agreement: new_agreement
        end
      end
    end

    it "blows up when locked" do
      Rails.cache.write("ticket_sharing/#{@agreement.account_id}/#{ts_ticket.uuid}", true)
      with_lock_timeout 0.1 do
        assert_raise Timeout::Error do
          assert_create
        end
      end
    end
  end

  describe "#update" do
    describe 'Given an existing shared ticket' do
      before do
        @agreement = create_valid_agreement(status: :accepted)
        @ticket = tickets(:minimum_1)
        @shared_ticket = @account.shared_tickets.create!(ticket: @ticket, agreement: @agreement)
      end

      describe 'and a valid api key' do
        before do
          set_header('X-Ticket-Sharing-Token', @agreement.authentication_token)
        end

        describe 'and the agreement is updateable via the api' do
          before do
            assert @agreement.allows_partner_updates?
          end

          describe "and it's a problem incident with linked incidents" do
            before do
              Account.any_instance.stubs(:has_extended_ticket_types?).returns(true)

              @ticket.ticket_type_id = 3
              @ticket.will_be_saved_by(@ticket.assignee)
              @ticket.save!

              @incident = tickets(:minimum_2)
              @incident.linked_id = @ticket.id
              @incident.will_be_saved_by(@ticket.assignee)
              @incident.save!

              @actor = new_ticket_sharing_actor('role' => 'agent')

              ts_ticket = new_ticket_sharing_ticket(
                'uuid' => @shared_ticket.uuid,
                'status' => 'solved',
                'current_actor' => @actor,
                'requester' => @actor
              )

              set_body(ts_ticket.to_json)
              put :update, params: { uuid: @shared_ticket.uuid }
            end

            it "updates the problem ticket's status and solve linked incidents" do
              @ticket.reload
              @incident.reload
              @shared_ticket.reload
              assert_equal 1, @ticket.incidents.count(:all)
              assert_equal @incident, @ticket.incidents.last
              assert_equal('Solved', @shared_ticket.ticket.status)
              assert_equal('Solved', @incident.status)
            end
          end

          describe 'updating that ticket' do
            before do
              ts_ticket = new_ticket_sharing_ticket(
                'uuid' => @shared_ticket.uuid,
                'status' => 'solved',
                'subject' => 'boogaloo',
                'current_actor' => new_ticket_sharing_actor
              )

              set_body(ts_ticket.to_json)
              put :update, params: { uuid: @shared_ticket.uuid }
            end

            it 'responds' do
              assert_response :ok, @response.body
            end

            it "updates the ticket's subject" do
              @shared_ticket.reload
              assert_equal 'boogaloo', @shared_ticket.ticket.subject
            end

            it "updates that ticket's status" do
              @shared_ticket.reload
              assert_equal('Solved', @shared_ticket.ticket.status)
            end
          end

          describe 'updating that ticket with a hold status' do
            before do
              Account.any_instance.stubs(use_status_hold?: false)
              Account.any_instance.stubs(:fetch_active_triggers).returns([])

              @shared_ticket.ticket.current_tags = "findme"
              @shared_ticket.ticket.will_be_saved_by(users(:minimum_agent))
              @shared_ticket.ticket.save!

              Account.any_instance.stubs(:fetch_active_triggers).returns([rules(:trigger_cascade1)])

              ts_ticket = new_ticket_sharing_ticket(
                'uuid' => @shared_ticket.uuid,
                'status' => 'hold',
                'current_actor' => new_ticket_sharing_actor
              )

              set_body(ts_ticket.to_json)
              put :update, params: { uuid: @shared_ticket.uuid }
            end

            it 'responds' do
              assert_response :ok, @response.body
            end

            it "updates that ticket's status" do
              @shared_ticket.reload
              assert_equal('Open', @shared_ticket.ticket.status)
            end
          end

          describe 'updating that ticket with a closed status' do
            before do
              ts_ticket = new_ticket_sharing_ticket(
                'uuid' => @shared_ticket.uuid,
                'status' => 'closed',
                'subject' => 'some updated subject',
                'current_actor' => new_ticket_sharing_actor
              )

              set_body(ts_ticket.to_json)
            end

            describe 'and disable_closing_tickets_via_sharing is turned off' do
              before do
                put :update, params: { uuid: @shared_ticket.uuid }
              end

              it 'responds' do
                assert_response :ok, @response.body
              end

              it "updates that ticket's status" do
                @shared_ticket.reload
                assert_equal('Closed', @shared_ticket.ticket.status)
              end
            end

            describe 'and disable_closing_tickets_via_sharing is turned on' do
              before do
                Account.any_instance.stubs(:has_disable_closing_tickets_via_sharing?).returns(true)
                put :update, params: { uuid: @shared_ticket.uuid }
              end

              it "updates that ticket's subject but does not change the status to closed" do
                @shared_ticket.reload
                assert_equal('some updated subject', @shared_ticket.ticket.subject)
                refute_equal('Closed', @shared_ticket.ticket.status)
              end
            end
          end
        end

        describe 'And the agreement is not updateable via the api' do
          before do
            @agreement.status = :declined
            @agreement.save!
            refute @agreement.deleted?
            refute @agreement.allows_partner_updates?
          end

          describe 'When an update comes in for the ticket' do
            before do
              set_header('X-Ticket-Sharing-Token', @agreement.authentication_token)
              ts_ticket = new_ticket_sharing_ticket(
                'uuid' => @shared_ticket.uuid,
                'status' => 'solved',
                'current_actor' => new_ticket_sharing_actor
              )

              set_body(ts_ticket.to_json)
              put :update, params: { uuid: @shared_ticket.uuid }
            end

            it 'responds with "forbidden"' do
              assert_response :forbidden, @response.body
            end
          end
        end
      end
    end

    describe "Given an existing shared ticket that is closed" do
      before do
        @agreement = create_valid_agreement(status: :accepted)
        @ticket = tickets(:minimum_1)
        @ticket.force_status_change = true
        @ticket.status_id = StatusType.closed
        @ticket.will_be_saved_by(@ticket.assignee)
        @ticket.save!
        @ticket.reload
        assert_equal(StatusType.closed, @ticket.status_id)

        @shared_ticket = @account.shared_tickets.create!(ticket: @ticket, agreement: @agreement)
      end

      describe "Trying to update that ticket with a new status" do
        before do
          set_header('X-Ticket-Sharing-Token', @agreement.authentication_token)
          ts_ticket = new_ticket_sharing_ticket(
            'uuid' => @shared_ticket.uuid,
            'status' => 'open',
            'current_actor' => new_ticket_sharing_actor
          )

          set_body(ts_ticket.to_json)
          put :update, params: { uuid: @shared_ticket.uuid }
        end

        it "responds with 'method not allowed'" do
          assert_response :method_not_allowed, @response.body
        end

        it "does not change the ticket's status" do
          @ticket.reload
          assert_equal(StatusType.closed, @ticket.status_id)
        end
      end
    end

    describe 'Given a valid API key' do
      before do
        @agreement = create_valid_agreement(status: :accepted)
        set_header('X-Ticket-Sharing-Token', @agreement.authentication_token)
      end

      describe 'When trying to update a shared ticket that does not exist' do
        before do
          set_body(new_ticket_sharing_ticket.to_json)
        end

        it 'justs suck it up and create the ticket anyway' do
          assert_difference 'SharedTicket.count(:all)', 1 do
            put :update, params: { uuid: 'doesntexist' }
          end

          assert_response :ok, @response.body
        end

        describe "within 10 seconds after deleting a shared ticket with the same uuid" do
          before do
            Timecop.freeze

            @first_ticket = SharedTicket.create!(account: accounts(:minimum), agreement: @agreement, ticket: tickets(:minimum_1))
            @first_ticket.save!
            @first_ticket.soft_delete!
            @second_ticket = SharedTicket.create!(account: accounts(:minimum), agreement: @agreement, ticket: tickets(:minimum_1))
            @second_ticket.save!
            @second_ticket.update_attribute(:uuid, @first_ticket.uuid)

            set_body(@second_ticket.to_json)
          end

          it "returns not create a new ticket" do
            assert_difference 'SharedTicket.count(:all)', 0 do
              put :update, params: { uuid: @first_ticket.uuid }
            end

            assert_response 499, @response.body
          end
        end
      end
    end
  end

  describe "#destroy" do
    before do
      @agreement = create_valid_agreement(status: :accepted)
      ticket = tickets(:minimum_1)
      ticket.shared_tickets.create!(uuid: 't1', agreement: @agreement)
      set_header('X-Ticket-Sharing-Token', @agreement.authentication_token)
    end

    it 'deletes the shared ticket' do
      assert_difference 'SharedTicket.count', -1 do
        delete :destroy, params: { uuid: 't1' }
        assert_response :success
      end
    end

    it 'does not delete the shared ticket' do
      set_header('X-Ticket-Sharing-Token', nil)
      refute_difference 'SharedTicket.count' do
        delete :destroy, params: { uuid: 't1' }
        assert_response :unauthorized
      end
    end
  end
end
