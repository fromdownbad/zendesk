require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe ActivateTrialController do
  fixtures :accounts, :users, :role_settings

  let(:account) { accounts(:shell_account_without_support) }
  let(:user) { account.owner }
  let(:support_product) { nil }
  let(:cache_key) { "some-cache-key" }
  let(:job_id) { "123" }
  let(:user_entitlements) { { chat: 'admin' } }

  before do
    @request.account = account
    @controller.stubs(current_account: account)

    login(user)

    chat_product = Zendesk::Accounts::Product.new('name' => 'chat', 'state' => Zendesk::Accounts::Product::TRIAL, 'plan_settings' => { 'phase' => 4 })
    response_body = { products: [support_product, chat_product].compact }.to_json
    metropolis_response = { entitlements: user_entitlements }.to_json

    stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
      to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })

    stub_request(:get, "http://metropolis:8888/api/services/staff/#{user.id}/entitlements").
      to_return(body: metropolis_response, headers: { content_type: 'application/json; charset=utf-8' })
  end

  describe '#index' do
    describe 'multiproduct account' do
      describe 'un-started support state' do
        let(:support_product) { FactoryBot.build(:support_not_started_product) }

        it 'renders the trial activation page' do
          get :index
          assert_response :success
          assert_template :index
        end
      end

      describe 'with account suspended for fraud' do
        before { account.update_column(:is_serviceable, false) }

        it 'redirects to billing subscription page' do
          get :index
          assert_redirected_to '/admin/billing/subscription'
        end
      end

      describe 'active trial support state' do
        let(:support_product) { FactoryBot.build(:support_trial_product) }
        it 'renders the main agent page' do
          get :index
          assert_redirected_to '/agent'
        end

        describe 'and SupportCreationJob is already enqueued' do
          before do
            @controller.stubs(job_status: true)
            @controller.stubs(job_failed?: false)
          end

          it 'renders the main agent page' do
            get :index
            assert_redirected_to '/agent'
          end
        end
      end

      describe 'free support state' do
        let(:support_product) { FactoryBot.build(:support_free_product) }
        it 'renders the main page' do
          get :index
          assert_redirected_to '/agent'
        end
      end

      describe 'subscribed support state' do
        let(:support_product) { FactoryBot.build(:support_subscribed_product) }
        it 'renders the main page' do
          get :index
          assert_redirected_to '/agent'
        end
      end

      describe 'expired trial support state' do
        let(:support_product) { FactoryBot.build(:support_expired_trial_product) }
        it 'renders the billing page' do
          get :index
          assert_redirected_to '/admin/billing/subscription'
        end
      end

      describe 'inactive trial support state' do
        let(:support_product) { FactoryBot.build(:support_inactive_product) }
        it 'renders the billing page' do
          get :index
          assert_redirected_to '/admin/billing/subscription'
        end
      end

      describe 'cancelled support state' do
        let(:support_product) { FactoryBot.build(:support_cancelled_product) }
        it 'renders the billing page' do
          get :index
          assert_redirected_to '/admin/billing/subscription'
        end
      end

      describe 'no existing support state in DB' do
        let(:support_product) { nil }
        it 'renders the trial activation page' do
          get :index
          assert_response :success
          assert_template :index
        end
      end
    end

    describe 'non multiproduct account' do
      let(:account) { accounts(:minimum) }
      let(:support_product) { FactoryBot.build(:support_subscribed_product) }

      it 'returns 403 forbidden' do
        get :index
        assert_response :forbidden
      end
    end

    describe 'no-support agent' do
      let(:support_product) { FactoryBot.build(:support_not_started_product) }
      let(:account) { accounts(:multiproduct) }
      let(:user) { users(:multiproduct_contributor) }

      it 'renders page' do
        get :index
        assert_response :success
      end
    end

    describe 'non owner support agent' do
      let(:support_product) { FactoryBot.build(:support_not_started_product) }
      let(:account) { accounts(:multiproduct) }
      let(:user) { users(:multiproduct_support_agent) }

      it 'renders page' do
        get :index
        assert_response :success
      end
    end
  end

  describe '#wait' do
    let(:support_product) { nil }

    describe 'when SupportCreationJob is already enqueued' do
      before do
        @controller.stubs(job_status: true)
        @controller.stubs(job_failed?: false)
      end

      it 'renders the wait state page' do
        get :wait
        assert_response :success
        assert_template :wait
      end
    end

    describe 'when SupportCreationJob is NOT already enqueued' do
      before do
        @controller.stubs(support_creation_job_enqueued?: false)
      end

      it 'redirects to index page' do
        get :wait
        assert_redirected_to '/agent/start'
      end
    end

    describe_with_arturo_enabled :accsrv_product_created_notification do
      describe 'when support is not present in the account' do
        let(:support_product) { nil }

        it 'redirects to the index page' do
          get :wait
          assert_redirected_to '/agent/start'
        end
      end

      describe 'when support is inactive but less than 5 minutes have passed' do
        let(:support_product) { FactoryBot.build(:support_not_started_product, created_at: 4.minutes.ago) }

        before do
          Timecop.freeze(Time.now)
        end

        it 'renders the wait state page' do
          get :wait
          assert_response :success
          assert_template :wait
        end
      end

      describe 'when support remains inactive for more than 5 minutes' do
        let(:support_product) { FactoryBot.build(:support_not_started_product, created_at: 10.minutes.ago) }

        before do
          Timecop.freeze(Time.now)
        end

        it 'redirects to the failure page' do
          get :wait
          assert_redirected_to '/agent/activate_trial/failed'
        end
      end
    end
  end

  describe '#activate' do
    before do
      @controller.stubs(internal_request?: false)
      @controller.expects(:set_system_user_actor).never
    end

    it 'returns 406 response for non-json formats' do
      post :activate
      assert_equal 406, @response.status
    end

    describe 'non multiproduct account' do
      let(:account) { accounts(:minimum) }
      let(:support_product) { FactoryBot.build(:support_subscribed_product) }

      it 'returns 403 forbidden' do
        post :activate, format: :json
        assert_response :forbidden
      end
    end

    describe 'with owner' do
      let(:support_product) { FactoryBot.build(:support_not_started_product) }

      it 'should allow calling action' do
        SupportCreationJob.expects(:enqueue).with(account_id: account.id, user_id: user.id)
        post :activate, format: :json
        assert_response :accepted
      end
    end

    describe 'with non-owner chat admin' do
      let(:account) { accounts(:multiproduct) }
      let(:user) { users(:multiproduct_contributor) }
      let(:user_entitlements) { { chat: 'admin' } }
      let(:support_product) { nil }

      it 'should allow calling action' do
        SupportCreationJob.expects(:enqueue).with(account_id: account.id, user_id: user.id)
        post :activate, format: :json
        assert_response :accepted
      end
    end

    describe 'with no-support agent' do
      let(:account) { accounts(:multiproduct) }
      let(:user) { users(:multiproduct_contributor) }
      let(:support_product) { nil }
      let(:user_entitlements) { { chat: 'agent' } }

      it 'should not activate throwing 403' do
        post :activate, format: :json
        assert_response :forbidden
      end
    end

    describe 'with non-owner' do
      let(:account) { accounts(:multiproduct) }
      let(:user) { users(:multiproduct_support_agent) }
      let(:support_product) { nil }
      let(:user_entitlements) { { support: 'agent' } }

      it 'should not activate throwing 403' do
        post :activate, format: :json
        assert_response :forbidden
      end
    end

    describe 'when signed internal request' do
      before do
        @controller.stubs(internal_request?: true)
        @controller.expects(:authenticate_user).never
        @controller.expects(:authorize_owner).never
        @controller.expects(:verify_authenticity_token).never
        @controller.expects(:set_system_user_actor).once
      end

      describe 'when Support product is not active' do
        let(:support_product) { nil }

        it 'succeeds' do
          account.expects(:save_job_id).once
          SupportCreationJob.expects(:enqueue).with(account_id: account.id, user_id: User.system.id)
          post :activate, format: :json
          assert_response :accepted
        end
      end

      describe 'with account suspended for fraud' do
        before { account.update_column(:is_serviceable, false) }

        it 'fails' do
          SupportCreationJob.expects(:enqueue).never
          post :activate, format: :json
          assert_response :forbidden
        end
      end

      describe 'when Support product is active' do
        let(:support_product) { FactoryBot.build(:support_subscribed_product) }

        it 'fails' do
          SupportCreationJob.expects(:enqueue).never
          post :activate, format: :json
          assert_response :conflict
        end
      end

      describe 'when job is already enqueued' do
        let(:support_product) { nil }

        before do
          @controller.stubs(support_creation_job_enqueued?: true)
        end

        it 'returns accepted' do
          SupportCreationJob.expects(:enqueue).never
          post :activate, format: :json
          assert_response :accepted
        end
      end
    end

    describe_with_arturo_enabled :accsrv_product_created_notification do
      before do
        @controller.stubs(create_not_started_support_product: nil)
      end

      it 'creates not started support product and does not enqueue a SupportCreationJob' do
        SupportCreationJob.expects(:enqueue).never
        @controller.expects(:create_not_started_support_product)
        post :activate, format: :json
        assert_response :accepted
      end
    end
  end

  describe '#state' do
    it 'returns 406 response for non-json formats' do
      get :state
      assert_equal 406, @response.status
    end

    describe 'when Support is not active' do
      let(:support_product) { FactoryBot.build(:support_inactive_product) }

      describe 'when job is enqueued' do
        before do
          @controller.stubs(support_creation_job_enqueued?: true)
        end

        it 'returns pending state' do
          get :state, format: :json
          assert_response :success
          assert_equal "{\"state\":\"pending\"}", @response.body
        end
      end

      describe 'when job has failed' do
        before do
          @controller.stubs(support_creation_job_enqueued?: false)
        end

        it 'returns failure state' do
          get :state, format: :json
          assert_response :ok
          assert_equal "{\"state\":\"failed\"}", @response.body
        end
      end
    end

    describe 'when Support is active and job completed' do
      let(:support_product) { FactoryBot.build(:support_trial_product) }

      before do
        @controller.stubs(job_status: Resque::Plugins::Status::STATUS_COMPLETED)
      end

      it 'returns complete state' do
        get :state, format: :json
        assert_response :success
        assert_equal "{\"state\":\"complete\"}", @response.body
      end
    end

    describe 'when signed internal request' do
      let(:user) { users(:multiproduct_support_agent) }

      before do
        @controller.stubs(internal_request?: true)
        @controller.stubs(support_creation_job_enqueued?: true)
        @controller.expects(:authenticate_user).never
        @controller.expects(:authorize_owner).never
        @controller.expects(:verify_authenticity_token).never
        @controller.expects(:set_system_user_actor).once
      end

      it 'succeeds' do
        get :state, format: :json
        assert_response :success
        assert_equal "{\"state\":\"pending\"}", @response.body
      end
    end

    describe_with_arturo_enabled :accsrv_product_created_notification do
      describe 'when support is active' do
        let(:support_product) { FactoryBot.build(:support_trial_product) }

        it 'returns complete state' do
          get :state, format: :json
          assert_response :success
          assert_equal "{\"state\":\"complete\"}", @response.body
        end
      end

      describe 'when support is not present in the account' do
        let(:support_product) { nil }

        it 'returns state pending' do
          get :state, format: :json
          assert_response :success
          assert_equal "{\"state\":\"pending\"}", @response.body
        end
      end

      describe 'when support is inactive but less than 5 minutes have passed' do
        let(:support_product) { FactoryBot.build(:support_not_started_product, created_at: 4.minutes.ago) }

        before do
          Timecop.freeze(Time.now)
        end

        it 'returns state pending' do
          get :state, format: :json
          assert_response :success
          assert_equal "{\"state\":\"pending\"}", @response.body
        end
      end

      describe 'when support remains inactive for more than 5 minutes' do
        let(:support_product) { FactoryBot.build(:support_not_started_product, created_at: 10.minutes.ago) }

        before do
          Timecop.freeze(Time.now)
        end

        it 'returns state failed' do
          get :state, format: :json
          assert_response :success
          assert_equal "{\"state\":\"failed\"}", @response.body
        end
      end
    end
  end

  describe '#internal_request?' do
    describe 'when signed internal request' do
      before do
        @request.stubs(signed_internal_request?: true)
      end

      it 'returns true' do
        assert(@controller.send(:internal_request?))
      end
    end

    describe 'when NOT a signed internal request' do
      before do
        @request.stubs(signed_internal_request?: false)
      end

      it 'returns false' do
        assert_equal false, @controller.send(:internal_request?)
      end
    end
  end

  describe '#support_creation_job_enqueued?' do
    it 'returns false when no job is enqueued' do
      @controller.stubs(job_status: nil)
      assert_equal false, @controller.send(:support_creation_job_enqueued?)
    end

    it 'returns false when job failed' do
      @controller.stubs(job_status: Resque::Plugins::Status::STATUS_FAILED)
      assert_equal false, @controller.send(:support_creation_job_enqueued?)
    end

    it 'returns false when job was killed' do
      @controller.stubs(job_status: Resque::Plugins::Status::STATUS_KILLED)
      assert_equal false, @controller.send(:support_creation_job_enqueued?)
    end

    it 'returns true when job was enqueued' do
      @controller.stubs(job_status: Resque::Plugins::Status::STATUS_QUEUED)
      assert(@controller.send(:support_creation_job_enqueued?))
    end

    it 'returns true when job is running' do
      @controller.stubs(job_status: Resque::Plugins::Status::STATUS_WORKING)
      assert(@controller.send(:support_creation_job_enqueued?))
    end

    it 'returns true when job was completed' do
      @controller.stubs(job_status: Resque::Plugins::Status::STATUS_COMPLETED)
      assert(@controller.send(:support_creation_job_enqueued?))
    end
  end

  describe '#job_status' do
    before do
      account.stubs(support_creation_cache_key: cache_key)
    end

    it 'looks up job_id from cache' do
      Rails.cache.expects(:read).with(cache_key)
      @controller.send(:job_status)
    end

    describe 'when job_id exists' do
      before do
        account.send(:save_job_id, job_id)
      end

      it 'retrieves job_status' do
        Resque::Plugins::Status::Hash.expects(:get).with(job_id)
        @controller.send(:job_status)
      end
    end

    describe 'when job_id does NOT exist' do
      before do
        Rails.cache.clear
      end

      it 'retrieves job_status' do
        Resque::Plugins::Status::Hash.expects(:get).never
        @controller.send(:job_status)
      end
    end
  end

  describe '#fail' do
    before do
      @controller.stubs(job_failed?: true)
    end

    it 'redirects from wait to fail if job failed' do
      get :wait
      assert_redirected_to '/agent/activate_trial/failed'
    end
  end

  describe '#job_failed' do
    it 'returns true if job failed' do
      @controller.stubs(job_status: Resque::Plugins::Status::STATUS_FAILED)
      assert(@controller.send(:job_failed?))
    end

    it 'returns true if job was killed' do
      @controller.stubs(job_status: Resque::Plugins::Status::STATUS_KILLED)
      assert(@controller.send(:job_failed?))
    end
  end

  describe '#activation_succeeded?' do
    describe 'when support product is active' do
      let(:support_product) { FactoryBot.build(:support_trial_product) }

      it 'returns truthy when job completed' do
        @controller.stubs(job_status: Resque::Plugins::Status::STATUS_COMPLETED)
        assert @controller.send(:activation_succeeded?)
      end

      it 'returns falsy when job did not complete' do
        @controller.stubs(job_status: Resque::Plugins::Status::STATUS_FAILED)
        refute @controller.send(:activation_succeeded?)
      end
    end

    describe 'when support product is NOT active' do
      let(:support_product) { FactoryBot.build(:support_inactive_product) }

      it 'returns falsy when job completed' do
        @controller.stubs(job_status: Resque::Plugins::Status::STATUS_COMPLETED)
        refute @controller.send(:activation_succeeded?)
      end
    end
  end
end
