require_relative "../support/test_helper"

SingleCov.covered! uncovered: 5

describe CsvExportsController do
  fixtures :accounts, :users, :role_settings, :brands, :routes, :account_settings, '../files/stat_rollup/hc_search_string_stats'

  let(:account) { accounts(:minimum) }

  describe "#hc_search_stats" do
    before do
      login(:minimum_admin)

      @account = accounts(:minimum)
      @default_help_center = HelpCenter.new(id: 42, state: 'enabled')
      @brand = FactoryBot.create(:brand, name: 'Secondary Brand', active: true, account_id: @account.id)
      @help_center = HelpCenter.new(id: 43, state: 'enabled')
      @request.env['zendesk.brand'] = @account.default_brand

      HelpCenter.stubs(:find_by_brand_id).with(@account.default_brand.id).returns(@default_help_center)
      HelpCenter.stubs(:find_by_brand_id).with(@brand.id).returns(@help_center)
    end

    it 'scopes to individual brands' do
      create_stat(account: @account, help_center_id: @help_center.id, string: 'A Search in Secondary Brand')

      get :hc_search_stats, params: { multibrand: true, brand_id: @brand.id }

      assert_equal 2, @response.body.lines.count
      assert @response.body.lines[1].include?('A Search in Secondary Brand')
    end

    it 'scopes to default brand without multibrand parameter' do
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'A Search in Default Brand')

      get :hc_search_stats

      assert_equal 2, @response.body.lines.count
      assert @response.body.lines[1].include?('A Search in Default Brand')
    end

    it 'returns status 200 on success' do
      get :hc_search_stats
      assert_response :success, @response.body
    end

    it 'includes column headers' do
      get :hc_search_stats

      expected_headers = "\"Search string\",\"Total searches\",\"Avg number of results\",\"Click-through rate\",\"Tickets created\",\"Top clicked result\"\n"
      column_headers = @response.body.lines.first
      assert_equal expected_headers, column_headers
    end

    it 'conforms to expected filename format' do
      Timecop.freeze(Time.local(2008, 9, 1, 12, 0, 0))

      get :hc_search_stats

      assert @response['Content-Disposition'].include?('filename="search-strings-2008-09-01.csv"')
    end

    it 'orders by number of tickets and filters zero ticket searches when tickets statistic is specified' do
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'First', ctr: 0, tickets: 5)
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'Secod', ctr: 1, tickets: 0)
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'Third', ctr: 0, tickets: 1)

      get :hc_search_stats, params: { statistic: :tickets }

      assert_equal 3, @response.body.lines.count
      assert @response.body.lines[1].include?('First')
    end

    it 'orders by number of searches and filters searches that resulted in a click when no_clicks statistics is specified' do
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'First', ctr: 0, searches: 1)
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'Secod', ctr: 1, searches: 1)
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'Third', ctr: 0, searches: 4)

      get :hc_search_stats, params: { statistic: :no_clicks }

      assert_equal 3, @response.body.lines.count
      assert @response.body.lines[1].include?('Third')
    end

    it 'orders by number of searches and filters searches that returned results when no_results statistics is specified' do
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'First', searches: 1, avg_results: 0)
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'Secod', searches: 1, avg_results: 1)
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'Third', searches: 4, avg_results: 0)

      get :hc_search_stats, params: { statistic: :no_results }

      assert_equal 3, @response.body.lines.count
      assert @response.body.lines[1].include?('Third')
    end

    it 'orders by number of searches and does not filter when searches statistic is specified' do
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'First', searches: 1, avg_results: 1)
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'Secod', searches: 1, avg_results: 0)
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'Third', searches: 4, avg_results: 1)

      get :hc_search_stats, params: { statistic: :searches }

      assert_equal 4, @response.body.lines.count
      assert @response.body.lines[1].include?('Third')
    end

    it 'should not include a link if there is no top_child_type' do
      create_stat(account: @account, help_center_id: @default_help_center.id)

      get :hc_search_stats

      # The last column in the CSV file must be empty string (no link to top result)
      assert_equal 2, @response.body.lines.count
      @response.body.lines.last.split(',').last.chomp.must_equal '""'
    end

    it 'includes generated link for top clicked articles' do
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'A Search in Default Brand', top_child_id: 2, top_child_type: 'Article')

      get :hc_search_stats

      @response.body.lines[1].include?('/hc/articles/2')
    end

    it 'includes generated link for top clicked questions' do
      create_stat(account: @account, help_center_id: @default_help_center.id)

      get :hc_search_stats

      @response.body.lines[1].include?('communities/public/posts/2')
    end

    it 'ensures that a single quote character is inserted before "=" when it is at the beginning of a cell to prevent CSV injection vulnerability' do
      create_stat(account: @account, help_center_id: @default_help_center.id, string: '=AND(2>1)', searches: 2)
      create_stat(account: @account, help_center_id: @default_help_center.id, string: 'zendesk = awesome', searches: 1)

      get :hc_search_stats, params: { statistic: :searches }

      assert_equal %{"'=AND(2>1)"}, @response.body.lines[1].split(',').first
      assert_equal %("zendesk = awesome"), @response.body.lines[2].split(',').first
    end

    private

    def create_stat(options)
      [30.days.to_i, 1.day.to_i].each { |duration| FactoryBot.create(:hc_search_string_stat, options.merge!(duration: duration)) }
    end
  end
end
