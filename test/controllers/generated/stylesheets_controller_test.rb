require_relative "../../support/test_helper"
require_relative "../../support/multiproduct_test_helper"

SingleCov.covered!

describe Generated::StylesheetsController do
  include MultiproductTestHelper
  fixtures :accounts, :users

  describe "Generated stylesheets" do
    let(:params) do
      {
        account_id: accounts(:minimum).id,
        top_directory: "xxx",
        format: "css",
        account_updated_at: 123
      }
    end
    let(:folder) { "public/generated/stylesheets/branding" }
    let(:shell_account) { setup_shell_account }

    before { @request.account = accounts(:minimum) }

    describe "#branding" do
      it "provides branding for mobile devices" do
        @request.user_agent = 'Mozilla/5.0 (iPhone; U; CPU like Mac OS X; en)'
        get :branding, params: params
        assert_response :success
      end

      it "provides branding for regular browsers" do
        login('minimum_admin')
        get :branding, params: params
        assert_response :success
      end

      it "serves a 404 for invalid format" do
        get :branding, params: params.merge(format: "horse")
        assert_response 404
      end

      it "serves a 404 when the account_id parameter does not match the account's id" do
        account2 = accounts(:multiproduct)
        get :branding, params: params.merge(account_id: account2.id)
        assert_response 404
      end

      it "serves a 404 for account on different pod" do
        account2 = accounts(:multiproduct)
        account2.update_column(:shard_id, 222222)
        get :branding, params: params.merge(account_id: account2.id)
        assert_response 404
      end

      it "provides branding for a multiproduct account" do
        params.merge(account_id: shell_account.id)
        login(shell_account.owner)
        get :branding, params: params
        assert_response :success
      end

      describe_with_arturo_disabled :assets_strip_response_cookies_generated_style_branding do
        it 'does not strip Set-Cookie headers' do
          get :branding, params: params

          assert_response :success
          refute request.env['rack.session.options'][:skip]
        end
      end

      describe_with_arturo_enabled :assets_strip_response_cookies_generated_style_branding do
        it 'does not set the shared session cookie' do
          get :branding, params: params

          assert_response :success
          assert request.env['rack.session.options'][:skip]
        end
      end
    end

    describe "caches_page" do
      it "does not generate a cache if the top_directory param is not in the integer range" do
        Dir["#{folder}/**/*"].each { |file| FileUtils.rm_rf(file) } # delete directory first
        get :branding, params: params.merge(top_directory: (Account.maximum(:id) / 100) + 1)
        assert_equal [], Dir["#{folder}/**/*"]
      end

      it "returns a 404 if the top_directory param is not in the integer range" do
        get :branding, params: params.merge(top_directory: (Account.minimum(:id) / 100) - 1)
        assert_response :not_found
      end
    end
  end
end
