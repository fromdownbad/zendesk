require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Generated::JavascriptsController do
  fixtures :accounts, :users, :organizations, :translation_locales

  with_options(controller: "generated/javascripts") do |request|
    request.should_route :get, "/generated/javascripts/mobile_user.js", action: 'mobile_user', format: 'js'
    request.should_route :get, "/generated/javascripts/locale.js",      action: 'locale',      format: 'js'
  end

  before do
    @request.account = accounts(:minimum)
    @controller.stubs(:current_brand).returns(@request.account.brands.first)
    Timecop.freeze
  end

  describe "#locale" do
    let(:params) do
      {
        top_directory: "0",
        id: ENGLISH_BY_ZENDESK.id.to_s,
        format: "js"
      }
    end

    it "renders" do
      get :locale, params: params
      assert_response :success
      assert_equal "text/javascript", @response.content_type.to_s
    end

    it "generates different etags when locale changes" do
      get :locale, params: params
      old = @response.headers["ETag"]

      TranslationLocale.any_instance.stubs(updated_at: Time.now)
      get :locale, params: params
      new = @response.headers["ETag"]

      assert_not_equal old, new
    end

    it "generates different etags after trial upgrade" do
      Subscription.any_instance.stubs(is_trial?: true)
      get :user, format: "json"

      old = @response.headers["ETag"]

      Subscription.any_instance.stubs(is_trial?: false)
      get :user, format: "json"
      new = @response.headers["ETag"]

      assert_not_equal old, new
    end

    # Should_eventually because it was breaking master build local branch ci are green, maybe a timing issue?

    should_eventually "generate same etags during trial" do
      Subscription.any_instance.stubs(:is_trial?).returns(true)
      get :user, format: "json"

      old = @response.headers["ETag"]

      get :user, format: "json"
      new = @response.headers["ETag"]

      assert_equal old, new
    end

    it "is 404 when locale is not found" do
      get :locale, params: params.merge(id: 12345)
      assert_response :not_found
    end

    describe_with_arturo_disabled :assets_strip_response_cookies_generated_javascript_locale do
      it 'sets the shared session cookie' do
        get :locale, params: params

        assert_response :success
        assert_equal "text/javascript", @response.content_type.to_s
        refute request.env['rack.session.options'][:skip]
      end
    end

    describe_with_arturo_enabled :assets_strip_response_cookies_generated_javascript_locale do
      it 'does not set the shared session cookie' do
        get :locale, params: params

        assert_response :success
        assert_equal "text/javascript", @response.content_type.to_s
        assert request.env['rack.session.options'][:skip]
      end

      describe 'with no current account' do
        before { Arturo.enable_feature!(:assets_strip_response_cookies_generated_javascript_locale) }
        before { @request.account = nil }

        it 'does not set the shared session cookie' do
          get :locale, params: params

          assert_response :success
          assert_equal "text/javascript", @response.content_type.to_s
          assert request.env['rack.session.options'][:skip]
        end
      end
    end

    it "caches publicly if cache_key matches the locale's cache_key" do
      TranslationLocale.any_instance.stubs(:cache_key).returns("1399931857-1-1399931857")
      get :locale, params: params.merge(cache_key: ENGLISH_BY_ZENDESK.cache_key)
      refute_includes @response.headers["Cache-Control"], "private"
    end

    it "caches privately if cache_key is not provided" do
      get :locale, params: params
      assert_includes @response.headers["Cache-Control"], "private"
    end

    it "caches privately if the cache_key does not match" do
      get :locale, params: params.merge(cache_key: "abc")
      assert_includes @response.headers["Cache-Control"], "private"
    end

    describe "page caching" do
      let(:folder) { "public/generated/javascripts" }
      let(:cached) { Dir["#{folder}/**/*.js"] }

      before do
        TranslationLocale.any_instance.stubs(:cache_key).returns("1399931857-1-1399931857") # cache keys are Time.now in tests
        FileUtils.rm_rf(folder)
      end

      it "caches page if the cache_key matches" do
        get :locale, params: params.merge(cache_key: ENGLISH_BY_ZENDESK.cache_key)
        cached.must_equal ["#{folder}/locale/0/1/#{ENGLISH_BY_ZENDESK.cache_key}.js"]
      end

      it "does not cache page if the cache_key does not match" do
        get :locale, params: params.merge(cache_key: "abc")
        cached.must_equal []
      end

      it "does not generate a cache if the top_directory param is not in the integer range" do
        get :locale, params: params.merge(top_directory: (TranslationLocale.maximum(:id) / 100) + 1)
        cached.must_equal []
        assert_response :not_found
      end

      it "does not validate the top directory if action is different than locale" do
        expects(:validate_top_directory).never
        get :user, format: "json"
      end

      it "does not fail when previous before_action already rendered and then we raise a NotFound" do
        Account.any_instance.stubs(is_active?: false)
        not_found_id = 171717
        get :locale, params: params.merge(id: not_found_id)
        assert_response :not_found
      end
    end

    it "caches privately if only the locale updated_at portion of the cache_key matches" do
      locale = translation_locales(:brazilian_portuguese)
      get :locale, params: params.merge(id: locale.id, cache_key: "#{1.day.ago.to_i}-#{locale.updated_at.to_i}")
      assert_includes @response.headers["Cache-Control"], "private"
    end

    it "renders in requested locale" do
      portuguese = translation_locales(:brazilian_portuguese)
      get :locale, params: params.merge(id: portuguese.id)
      assert_includes @response.body, "THIS IS A BRAZIAN PORTUGUESE EMAIL DELIMITER"
    end
  end

  describe '#user' do
    before do
      account = accounts(:minimum)
      account.settings.voice = 1
      account.settings.save
    end
    it "renders normally" do
      get :user, format: "json"
      assert_response :success
    end
  end

  describe "#mobile_user" do
    it "renders" do
      get :mobile_user, params: { id: ENGLISH_BY_ZENDESK.id.to_s, format: "js" }
      assert_equal "text/javascript", @response.content_type.to_s
    end
  end

  describe "#cache_key_for_user" do
    before do
      Timecop.freeze
      get :user, format: "json"
      assert_response :success
    end

    it "includes account cache_key" do
      assert_match(@request.account.cache_key, cache_key_for_user)
    end

    it "includes js model version" do
      GIT_TIMESTAMPS['app/helpers/js_models_helper.rb'] = Time.at(12345678)
      assert_match(/12345678/, cache_key_for_user)
    end

    it "includes 'business' if business hours active" do
      @request.account.stubs(:business_hours_active?).returns(true)
      assert_match(/business/, cache_key_for_user)
    end

    it "does not include 'business' if business hours not active" do
      @request.account.stubs(:business_hours_active?).returns(false)
      assert_no_match(/business/, cache_key_for_user)
    end

    it "includes 'anonymous' if is anonymous user" do
      current_user.stubs(:is_anonymous_user?).returns(true)
      assert_match(/anonymous/, cache_key_for_user)
    end

    it "includes user version if isn't anonymous user" do
      current_user.stubs(:is_anonymous_user?).returns(false)
      assert(cache_key_for_user.include?(current_user.cache_key))
    end

    it "includes organization version if present" do
      current_user.stubs(:organization).returns(Organization.first)
      assert(cache_key_for_user.include?(Organization.first.cache_key))
    end

    it "includes password_expires_at" do
      current_user.stubs(:password_expires_at).returns('now')
      assert_match(/now/, cache_key_for_user)
    end

    it "includes timestamp" do
      assert(cache_key_for_user.include?((Time.now.to_i / 1.hour).to_s))
    end

    it "includes form_authenticity_token" do
      form_authenticity_token = @controller.send(:form_authenticity_token)
      @controller.stubs(:form_authenticity_token).returns(form_authenticity_token)
      assert(cache_key_for_user.include?(form_authenticity_token))
    end

    it "includes the locale" do
      assert(cache_key_for_user.include?('en-US-x-1'))
    end

    describe "if the locale changes" do
      before do
        I18n.stubs(:locale).returns('new_locale_for_this_guy')
        get :user, format: "json"
      end

      it "includes the portuguese locale" do
        assert(cache_key_for_user.include?('new_locale_for_this_guy'))
      end
    end

    it "includes accessible_forums_count" do
      current_user.stubs(:accessible_forums_count).returns(0)
      old = cache_key_for_user
      current_user.stubs(:accessible_forums_count).returns(1)
      new = cache_key_for_user
      assert_not_equal old, new
    end

    it "includes the MonitoredTwitterHandles" do
      mth = MonitoredTwitterHandle.create!(
        access_token: "<token>",
        secret: "<secret>",
        account: @request.account,
        twitter_user_id: 1
      )
      assert(cache_key_for_user.include?(mth.cache_key))
    end

    describe "if the account does not have a support subscription" do
      before do
        Account.any_instance.stubs(:multiproduct?).returns(true)
      end

      it "still returns the cache key" do
        assert cache_key_for_user
      end
    end
  end

  def cache_key_for_user
    @controller.send(:cache_key_for_user)
  end

  def current_user
    @controller.send(:current_user)
  end
end
