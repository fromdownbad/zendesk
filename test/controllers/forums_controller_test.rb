require_relative "../support/test_helper"

SingleCov.covered!

describe ForumsController do
  fixtures :forums

  before do
    @minimum_account = accounts(:minimum)
    @request.account = @minimum_account
    @forum = forums(:english_forum)
  end

  describe "with web_portal_state disabled" do
    it "renders 404" do
      get :show, params: { id: @forum.id }
      assert_response :redirect
    end
  end

  describe "with web_portal_state restricted" do
    before do
      @request.account.settings.web_portal_state = 'restricted'
      @request.account.settings.save!
    end

    describe "with help center enabled" do
      before do
        @request.account.stubs(:has_public_forums?).returns(true)
        activate_hc!
      end

      it "redirects to HelpCenter" do
        ContentMapping.create! do |cm|
          cm.account_id = @forum.account_id
          cm.classic_object_type = "Legacy::Forum"
          cm.classic_object_id = @forum.id
          cm.hc_object_type = "Foo"
          cm.hc_object_id = 42
          cm.url = "/foo"
        end
        get :show, params: { id: @forum.id }
        assert_redirected_to "https://minimum.zendesk-test.com/foo"
      end
    end

    describe "with help center disabled" do
      it "redirects to help center closed url" do
        get :show, params: { id: @forum.id }
        assert_match %r{app/help-center-closed/}, @response.location
        assert_equal 301, @response.status.to_i
      end
    end
  end
end
