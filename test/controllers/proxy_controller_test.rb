require_relative "../support/test_helper"
require_relative "../support/proxy_controller_test_helper"

SingleCov.covered! uncovered: 5

describe ProxyController do
  let(:arturo_status) { false }

  include ProxyControllerTestHelper

  fixtures :accounts, :users, :user_identities, :tickets

  def unparenthesize(string)
    raise "Invalid string, no starting ( or ending )" if string.index('(') != 0 || string.rindex(')') != string.length - 1
    string.slice(1, string.length - 2)
  end

  before do
    Resolv::DNS.any_instance.stubs(:getaddress).returns(Resolv::IPv4.create("1.2.3.4"))
    @request.account = accounts(:minimum)
    Account.any_instance.stubs(has_sse_remove_proxy_direct_endpoint?: arturo_status)
  end

  it "responds with Invalid URL parameter" do
    login('minimum_agent')
    @controller.stubs(:verify_csrf_token)

    Faraday::Connection.any_instance.expects(:post).never
    ZendeskExceptions::Logger.expects(:record).never
    post :direct, params: { format: 'xml', url: "\\http://invalid;uri.com" }
    assert_equal "Invalid URL parameter", @response.body
  end

  describe "when a request times out" do
    it "responds with the Timeout exception message" do
      login('minimum_agent')
      @controller.stubs(:verify_csrf_token)

      Faraday::Connection.any_instance.expects(:post).raises(Faraday::Error::TimeoutError, "Timeout error")
      ZendeskExceptions::Logger.expects(:record).never

      post :direct, params: { format: 'xml', url: "talladega.foo.com" }
      assert_equal "Timeout error", @response.body
    end
  end

  describe "when there's a connectivity error" do
    it "responds with the connectivity error message" do
      login('minimum_agent')
      @controller.stubs(:verify_csrf_token)

      [SocketError, Errno::ECONNREFUSED, Faraday::ConnectionFailed, Faraday::SSLError].each do |error|
        Faraday::Connection.any_instance.expects(:post).raises(error, "Connectivity error")
        ZendeskExceptions::Logger.expects(:record).never
        post :direct, params: { format: 'xml', url: "talladega.foo.com" }
        @response.body.must_include "Connectivity error"
      end
    end
  end

  it "follows redirects" do
    login('minimum_agent')
    @controller.stubs(:verify_csrf_token)

    stub_request(:post, "http://example.com/redirect").to_return(status: 302, headers: { "Location" => "http://example.com/other/path" })
    stub_request(:get, "http://example.com/other/path").to_return(status: 200, body: "Hello.")

    post :direct, params: { url: "http://example.com/redirect", format: "xml" }
    assert_equal "Hello.", @response.body
  end

  describe "follows a maximum of 4 redirects" do
    before do
      login('minimum_agent')
      @controller.stubs(:verify_csrf_token)
    end

    it "responds with an error message" do
      stub_request(:post, "http://example.com/redirect").to_return(status: 302, headers: { "Location" => "http://example.com/other/path/1" })
      stub_request(:get, "http://example.com/other/path/1").to_return(status: 302, headers: { "Location" => "http://example.com/other/path/2" })
      stub_request(:get, "http://example.com/other/path/2").to_return(status: 302, headers: { "Location" => "http://example.com/other/path/3" })
      stub_request(:get, "http://example.com/other/path/3").to_return(status: 302, headers: { "Location" => "http://example.com/other/path/4" })
      stub_request(:get, "http://example.com/other/path/4").to_return(status: 302, headers: { "Location" => "http://example.com/other/path/5" })

      post :direct, params: { url: "http://example.com/redirect", format: "xml" }
      assert_equal "Error connecting to http://example.com/redirect", @response.body
    end

    it "does not follow bad redirects" do
      stub_request(:post, "http://example.com/redirect").to_return(status: 302, headers: { "Location" => "http://0.0.0.0" })
      stub_request(:get, "http://0.0.0.0").to_return(status: 200, body: "you're owned")

      post :direct, params: { url: "http://example.com/redirect", format: "xml" }
      assert_equal "Error connecting to http://example.com/redirect", @response.body
    end
  end

  describe "#direct" do
    before do
      login('minimum_agent')
      stub_request(:post, "http://example.com/enterprise")
      @controller.stubs(:verify_csrf_token)
    end

    describe 'with sse_remove_proxy_direct_endpoint Arturo enabled' do
      let(:arturo_status) { true }

      it 'renders 404 page' do
        post :direct, params: { format: "xml", url: "http://example.com/enterprise", SOAPAction: 'soapy param' }
        assert_equal 404, @response.status
      end
    end

    it "pass along the SOAPAction parameter as a header" do
      post :direct, params: { format: "xml", url: "http://example.com/enterprise", SOAPAction: 'soapy param' }
      assert_requested(:post, "http://example.com/enterprise", headers: {"SOAPAction" => "soapy param"})
    end

    it "pass along the SOAPAction header as a header" do
      set_header('SOAPAction', 'soapy header')
      post :direct, params: { format: "xml", url: "http://example.com/enterprise" }

      assert_requested(:post, "http://example.com/enterprise", headers: {"SOAPAction" => "soapy header"})
    end

    describe "GET when the response body contains data" do
      it "returns the response" do
        set_header('Authorization', 'credentials')
        stub_request(:get, "http://thisworks.com/now").
          with(headers: {'Authorization' => 'credentials', 'Content-Type' => 'text/xml', 'User-Agent' => 'Zendesk'}).
          to_return(status: 200, body: "response from server", headers: {})
        get :direct, params: { format: "xml", url: "http://thisworks.com/now" }

        assert_equal "response from server", response.body
      end
    end

    describe "POST when the response body contains data" do
      it "returns the response" do
        set_header('Authorization', 'credentials')
        stub_request(:post, "http://thisworks.com/now").
          with(headers: {'Authorization' => 'credentials', 'Content-Type' => 'text/xml', 'User-Agent' => 'Zendesk'}).
          to_return(status: 200, body: "response from server", headers: {})
        post :direct, params: { format: "xml", url: "http://thisworks.com/now" }

        assert_equal "response from server", response.body
      end
    end

    describe "POST when the response body is empty" do
      it "returns the response" do
        set_header('Authorization', 'credentials')
        stub_request(:post, "http://thisworks.com/now").
          with(headers: {'Authorization' => 'credentials', 'Content-Type' => 'text/xml', 'User-Agent' => 'Zendesk'}).
          to_return(status: 200, body: "", headers: {})
        post :direct, format: "xml", url: "http://thisworks.com/now", body: { something: 'nothing' }

        expected_response = { error: "Empty response" }.to_xml(root: "result")
        assert_equal expected_response, response.body
      end
    end

    it "pass along the authorization header as a header" do
      set_header('Authorization', 'credentials')
      post :direct, params: { format: "xml", url: "http://example.com/enterprise" }

      assert_requested(:post, "http://example.com/enterprise", headers: {"Authorization" => "credentials"})
    end

    it "sets a request-specific timeout if the timeout parameter is passed to #direct" do
      Faraday::Connection.any_instance.expects(:get).with("http://example.com/enterprise")
      # Not sure how to do this differently
      ProxyController.any_instance.expects(:connection).with(timeout: 10, headers: {'Content-Type' => 'text/xml', 'User-Agent' => 'Zendesk'}).returns(Faraday.new)
      get :direct, params: { format: "xml", url: "http://example.com/enterprise", timeout: 10 }
    end

    describe "when attempting to bypass the filter" do
      before do
        login('minimum_agent')
        stub_request(:post, "http://172.15.5.6/")
        stub_request(:post, "http://172.32.5.6/")
        # need a regex because it wants us to stub a request with the form http:///
        # but `stub_request` will parse that to http:// so it is impossible to stub correctly
        stub_request(:post, %r{example.com})
        accept :xml
      end

      should_not_filter :direct, "example.com"
      should_filter :direct, "example.com", scheme: :ftp

      should_filter :direct, "localhost"
      should_filter :direct, "localhost", scheme: :http

      should_filter :direct, "zendesk.com"
      should_filter :direct, "zendesk.com", scheme: :http
      should_filter :direct, "www.zendesk.com"
      should_filter :direct, "www.zendesk.com", scheme: :http

      should_filter :direct, "zd-staging.com"
      should_filter :direct, "zd-staging.com", scheme: :http
      should_filter :direct, "www.zd-staging.com"
      should_filter :direct, "www.zd-staging.com", scheme: :http

      should_filter :direct, "10.1.0.1"
      should_filter :direct, "10.1.0.1", scheme: :http

      should_filter :direct, "127.1.0.1"
      should_filter :direct, "127.1.0.1", scheme: :http

      should_filter :direct, "192.168.5.6"
      should_filter :direct, "192.168.5.6", scheme: :http

      describe "filter out the 172.(16 - 31).5.6 range of IPs" do
        should_not_filter :direct, "172.15.5.6", scheme: :http
        (16..31).each do |i|
          should_filter :direct, "172.#{i}.5.6"
          should_filter :direct, "172.#{i}.5.6", scheme: :http
        end
        should_not_filter :direct, "172.32.5.6", scheme: :http
      end

      describe "with no url parameter" do
        it "does not be allowed" do
          post :direct
          assert_equal "Missing mandatory URL parameter", @response.body
        end
      end

      describe "with blank url" do
        it "does not be allowed" do
          post :direct, params: { url: "" }
          assert_equal "Missing mandatory URL parameter", @response.body
        end
      end

      describe "with bad url format" do
        describe "without scheme" do
          it "does not be allowed" do
            post :direct, params: { url: "badform:example.com" }
            assert_equal "Invalid URL parameter", @response.body
          end
        end

        describe "with scheme" do
          it "does not be allowed" do
            post :direct, params: { url: "https://badform:example.com" }
            assert_equal "Invalid URL parameter", @response.body
          end
        end
      end
    end
  end

  describe "#direct with csrf check" do
    before do
      login('minimum_agent')
      stub_request(:get, "http://example.com/enterprise")
      stub_request(:post, "http://example.com/enterprise")
    end

    describe "without csrf header in the request" do
      it "does not be allowed with no XHR header" do
        get :direct, params: { format: "xml", url: "http://example.com/enterprise", timeout: 10 }
        assert_equal "Proxy Not Allowed", @response.body
        assert_equal 403, @response.status
      end

      it "is allowed when XHR header is XMLHttpRequest" do
        @request.env['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
        get :direct, params: { format: "xml", url: "http://example.com/enterprise", timeout: 10 }
        assert_equal 200, @response.status
      end
    end

    describe "with csrf header in the request not matching authenticity_token" do
      before do
        @request.env['HTTP_X_CSRF_TOKEN'] = '1234'
        @controller.stubs(:form_authenticity_token).returns('5678')
      end

      it "does not be allowed with no XHR header" do
        get :direct, params: { format: "xml", url: "http://example.com/enterprise", timeout: 10 }
        assert_equal "Proxy Not Allowed", @response.body
        assert_equal 403, @response.status
      end

      it "is allowed when XHR header is XMLHttpRequest" do
        @request.env['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
        get :direct, params: { format: "xml", url: "http://example.com/enterprise", timeout: 10 }
        assert_equal 200, @response.status
      end
    end

    describe "with csrf header in the request matching authenticity_token" do
      before do
        token = '1234'
        @request.env['HTTP_X_CSRF_TOKEN'] = token
        @controller.stubs(:form_authenticity_token).returns(token)
      end

      it "is allowed for GET" do
        get :direct, params: { format: "xml", url: "http://example.com/enterprise", timeout: 10 }
        assert_equal 200, @response.status
      end

      it "is allowed for POST" do
        post :direct, params: { format: "xml", url: "http://example.com/enterprise", timeout: 10 }
        assert_equal 200, @response.status
      end
    end

    describe 'with feature proxy_v1_csrf_verification enabled' do
      before { Arturo.enable_feature!(:proxy_v1_csrf_verification) }

      describe "without csrf header in the request" do
        it "does not be allowed" do
          @request.env['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
          get :direct, params: { format: "xml", url: "http://example.com/enterprise", timeout: 10 }
          assert_equal "Proxy Not Allowed", @response.body
          assert_equal 403, @response.status
        end
      end
    end
  end
end
