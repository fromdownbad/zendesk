require_relative "../../support/test_helper"
require_relative '../../support/collaboration_settings_test_helper'

SingleCov.covered! uncovered: 1

describe Tickets::MergeController do
  tests Tickets::MergeController

  fixtures :tickets, :ticket_fields, :accounts, :users, :groups

  should_be_unauthorized_when_not_logged_in([:new, :create])

  before do
    @request.account = accounts(:minimum)
    @request.env['HTTP_REFERER'] = 'http://dev.localhost/'
  end

  should_route :get, "/merge/new", controller: 'tickets/merge', action: :new
  should_route :get, "/merge", controller: 'tickets/merge', action: :show
  should_route :post, "/merge", controller: 'tickets/merge', action: :create

  describe "#new" do
    before do
      login('minimum_admin')

      @source = tickets(:minimum_1)
      @target = tickets(:minimum_3)

      @controller.stubs(:recent_tickets).returns([@source, @target])
    end

    describe "when the account has ticket collaboration enabled and email CCs disabled" do
      before do
        accounts(:minimum).update_attribute(:is_collaboration_enabled, true)
        Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)

        get :new, params: { source_ids: @source.nice_id, target_id: @target.nice_id }
      end

      it "sets @recent to the user's recent tickets" do
        assert_equal [@target], assigns(:recent)
      end
    end

    describe "when the account has ticket collaboration disabled and email CCs enabled" do
      before do
        accounts(:minimum).update_attribute(:is_collaboration_enabled, false)
        Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)

        get :new, params: { source_ids: @source.nice_id, target_id: @target.nice_id }
      end

      it "sets @recent to the user's recent tickets" do
        assert_equal [@target], assigns(:recent)
      end
    end

    describe "when the account has ticket collaboration disabled and email CCs disabled" do
      before do
        accounts(:minimum).update_attribute(:is_collaboration_enabled, false)
        Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)

        get :new, params: { source_ids: @source.nice_id, target_id: @target.nice_id }
      end

      it "does not set @recent to the user's recent tickets" do
        assert_empty(assigns(:recent))
      end
    end
  end

  it "tests comments" do
    login('minimum_agent')
    source = tickets(:minimum_1)
    target = tickets(:minimum_2)
    post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "This ticket was closed and merged into Ticket ##{target.nice_id}", target_comment: "Ticket ##{source.nice_id} has been closed and merged into this ticket. Please direct all future updates to this ticket." }
    source.reload
    target.reload
    assert source.comments.last.body.include?("##{target.nice_id}")
    assert source.status?(:closed)
    assert target.comments.last.body.include?("##{source.nice_id}")
    assert_equal ViaType.MERGE, source.audits.last.via_id
    assert_equal target.id, source.audits.last.via_reference_id
    assert_equal ViaType.MERGE, target.audits.last.via_id
  end

  it "tests bulk comments" do
    login('minimum_agent')
    source1 = tickets(:minimum_1)
    source2 = tickets(:minimum_2)
    target = tickets(:minimum_3)
    source_ids = "#{source1.nice_id}, #{source2.nice_id}"
    post :create, params: { source_ids: source_ids, target_id: target.nice_id, source_comment: "This ticket was closed and merged into Ticket ##{target.nice_id}", target_comment: "Tickets ##{source_ids} have been closed and merged into this ticket." }
    source1.reload
    source2.reload
    target.reload
    assert source1.comments.last.body.include?("##{target.nice_id}")
    assert source1.status?(:closed)
    assert source1.comments.last.body.include?("##{target.nice_id}")
    assert source1.status?(:closed)
    assert target.comments.last.body.include?(source1.nice_id.to_s)
    assert target.comments.last.body.include?(source2.nice_id.to_s)
    assert_equal ViaType.MERGE, source1.audits.last.via_id
    assert_equal ViaType.MERGE, source2.audits.last.via_id
    assert_equal ViaType.MERGE, target.audits.last.via_id
  end

  describe_with_arturo_disabled :email_ccs do
    it "tests collaboration when enabled" do
      login('minimum_agent')
      accounts(:minimum).update_attribute(:is_collaboration_enabled, true)
      source = tickets(:minimum_1)
      target = tickets(:minimum_2)
      post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
      source.reload
      target.reload
      assert_equal [source.requester], target.collaborators
    end

    it "tests move ccs from source to target when different requesters" do
      login('minimum_agent')
      accounts(:minimum).update_attribute(:is_collaboration_enabled, true)
      source = tickets(:minimum_1)
      source.set_collaborators = [users(:minimum_agent).id]
      source.will_be_saved_by(source.submitter)
      source.save
      target = tickets(:minimum_2)
      target.set_collaborators = [users(:minimum_admin).id]
      target.will_be_saved_by(target.submitter)
      target.save
      post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
      source.reload
      target.reload
      assert_equal [users(:minimum_admin), users(:minimum_agent), source.requester], target.collaborators
    end
  end

  it "tests move ccs from source to target when same requester" do
    login('minimum_agent')
    accounts(:minimum).update_attribute(:is_collaboration_enabled, true)
    source = tickets(:minimum_2)
    source.set_collaborators = [users(:minimum_agent).id]
    source.will_be_saved_by(source.submitter)
    source.save
    target = tickets(:minimum_3)
    target.set_collaborators = [users(:minimum_admin).id]
    target.will_be_saved_by(target.submitter)
    target.save
    post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
    source.reload
    target.reload
    assert_equal [users(:minimum_admin), users(:minimum_agent)], target.collaborators
  end

  describe_with_arturo_disabled :email_ccs do
    it "tests move ccs from sources to target and check duplicated" do
      login('minimum_agent')
      accounts(:minimum).update_attribute(:is_collaboration_enabled, true)
      Account.any_instance.stubs(:has_email_ccs?).returns(false)
      source1 = tickets(:minimum_1)
      source1.set_collaborators = [users(:minimum_author).id, users(:minimum_agent).id]
      source1.will_be_saved_by(source1.submitter)
      source1.save
      source2 = tickets(:minimum_2)
      source2.set_collaborators = [users(:minimum_agent).id, users(:minimum_admin).id]
      source2.will_be_saved_by(source2.submitter)
      source2.save
      target = tickets(:minimum_3)
      target.set_collaborators = [users(:minimum_admin).id]
      target.will_be_saved_by(target.submitter)
      target.save
      post :create, params: { source_ids: "#{source1.nice_id}, #{source2.nice_id}", target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
      target.reload
      assert_equal [users(:minimum_admin), users(:minimum_agent), users(:minimum_author)], target.collaborators
    end

    it "tests bulk collaboration" do
      login('minimum_agent')
      accounts(:minimum).update_attribute(:is_collaboration_enabled, true)
      source1 = tickets(:minimum_1)
      source2 = tickets(:minimum_2)
      source2.will_be_saved_by(accounts(:minimum).owner)
      source2.update_attribute(:requester, users(:minimum_admin))
      target = tickets(:minimum_3)
      source_ids = "#{source1.nice_id}, #{source2.nice_id}"
      post :create, params: { source_ids: source_ids, target_id: target.nice_id, source_comment: "This ticket was closed and merged into Ticket ##{target.nice_id}", target_comment: "Tickets ##{source_ids} have been closed and merged into this ticket." }
      source1.reload
      source2.reload
      target.reload
      assert_equal [source2.requester, source1.requester], target.collaborators
    end
  end

  it "tests collaboration when disabled" do
    login('minimum_agent')
    accounts(:minimum).update_attribute(:is_collaboration_enabled, false)
    source = tickets(:minimum_1)
    target = tickets(:minimum_2)
    post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
    source.reload
    target.reload
    assert_empty target.collaborators
  end

  it "tests with assignee" do
    login('minimum_admin')
    source = tickets(:minimum_1)
    target = tickets(:minimum_2)
    assert_equal users(:minimum_agent), source.assignee
    post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
    source.reload
    target.reload
    # The assignee must be the same as before
    assert_equal users(:minimum_agent), source.assignee
  end

  it "tests without assignee" do
    login('minimum_admin')
    source = tickets(:minimum_3)
    target = tickets(:minimum_2)
    assert_nil source.assignee
    post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
    source.reload
    target.reload
    # The assignee must change to the current user
    assert_equal users(:minimum_admin), source.assignee
    assert_equal groups(:minimum_group), source.group
  end

  it "tests private comments" do
    login('minimum_admin')
    source = tickets(:minimum_3)
    target = tickets(:minimum_2)
    assert_nil source.assignee
    post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
    source.reload
    target.reload
    refute source.comments.last.is_public?
    refute target.comments.last.is_public?
  end

  it "tests public comments" do
    login('minimum_admin')
    source = tickets(:minimum_3)
    target = tickets(:minimum_2)
    assert_nil source.assignee
    post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment", source_is_public: '1', target_is_public: '1' }
    source.reload
    target.reload
    assert source.comments.last.is_public?
    assert target.comments.last.is_public?
  end

  it "tests private and public comments" do
    login('minimum_admin')
    source = tickets(:minimum_3)
    target = tickets(:minimum_2)
    assert_nil source.assignee
    post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment", target_is_public: '1' }
    source.reload
    target.reload
    refute source.comments.last.is_public?
    assert target.comments.last.is_public?
  end

  it "tests new collaboration enabled" do
    login('minimum_admin')
    accounts(:minimum).update_attribute(:is_collaboration_enabled, true)
    source = tickets(:minimum_3)
    target = tickets(:minimum_2)
    get :new, params: { source_ids: source.nice_id, target_id: target.nice_id }
    assert_response :ok
    refute assigns(:requested).empty?
    assert_empty(assigns(:current_view))
  end

  it "tests new collaboration disabled" do
    login('minimum_admin')
    accounts(:minimum).update_attribute(:is_collaboration_enabled, false)
    source = tickets(:minimum_3)
    target = tickets(:minimum_2)
    get :new, params: { source_ids: source.nice_id, target_id: target.nice_id }
    assert_response :ok
    refute assigns(:requested).empty?
    assert_empty(assigns(:current_view))
  end

  it "tests new bulk" do
    login('minimum_admin')
    accounts(:minimum).update_attribute(:is_collaboration_enabled, false)
    source1 = tickets(:minimum_1)
    source2 = tickets(:minimum_3)
    unchecked = tickets(:minimum_4)
    unchecked.will_be_saved_by(unchecked.submitter)
    unchecked.update_attribute(:status_id, 0)
    target = tickets(:minimum_2)
    source_ids = "#{source1.nice_id}, #{source2.nice_id}"
    get :new, params: { source_ids: source_ids, target_id: target.nice_id, unchecked: unchecked.nice_id }
    assert_response :ok
    assert_empty(assigns(:requested))
    refute assigns(:current_view).empty?
  end

  it "tests new bulk with multiple unchecked" do
    login('minimum_admin')
    accounts(:minimum).update_attribute(:is_collaboration_enabled, false)
    source1 = tickets(:minimum_1)
    source2 = tickets(:minimum_3)
    unchecked = tickets(:minimum_4)
    unchecked.will_be_saved_by(unchecked.submitter)
    unchecked.update_attribute(:status_id, 0)
    unchecked2 = tickets(:minimum_5)
    unchecked2.will_be_saved_by(unchecked.submitter)
    unchecked2.update_attribute(:status_id, 0)

    target = tickets(:minimum_2)
    source_ids = [source1.nice_id, source2.nice_id].join(',')
    unchecked_ids = [unchecked.nice_id, unchecked2.nice_id].join(',')
    get :new, params: { source_ids: source_ids, target_id: target.nice_id, unchecked: unchecked_ids }
    assert_response :ok
    assert_empty(assigns(:requested))

    assert_equal [unchecked2, unchecked].map(&:nice_id), assigns(:current_view).map(&:nice_id)
  end

  it "tests new without sources" do
    login('minimum_admin')
    get :new
    assert_template "tickets/merge/_error"
  end

  describe '#show' do
    describe "ticket is not from a social channel (Facebook, Twitter, AnyChannel, etc.)" do
      before do
        login 'minimum_admin'
        @source = tickets(:minimum_3)
        @target = tickets(:minimum_2)
      end

      it 'responds with OK' do
        get :show, params: { source_ids: @source.nice_id, target_id: @target.nice_id }
        assert_response :ok
      end

      it 'does not sanitize XML out of the proposed comment' do
        Ticket.any_instance.stubs(:latest_public_comment).returns("Some <xml>markup</xml>")
        get :show, params: { source_ids: @source.nice_id, target_id: @target.nice_id }
        assert_match %r{Some &lt;xml&gt;markup&lt;/xml&gt;}, @response.body
      end

      it "matchs account's default public comment settings" do
        Account.any_instance.stubs(:is_comment_public_by_default?).returns(true)
        get :show, params: { source_ids: @source.nice_id, target_id: @target.nice_id }
        assert_select "input[type='checkbox'][name='source_is_public'][value='1'][checked='checked']"
        assert_select "input[type='checkbox'][name='target_is_public'][value='1'][checked='checked']"

        Account.any_instance.stubs(:is_comment_public_by_default?).returns(false)
        get :show, params: { source_ids: @source.nice_id, target_id: @target.nice_id }
        assert_select("input[type='checkbox'][name='source_is_public'][value='1'][checked='checked']", false)
        assert_select("input[type='checkbox'][name='target_is_public'][value='1'][checked='checked']", false)
      end
    end

    describe "ticket is from a social channel (Facebook, Twitter, AnyChannel, etc.)" do
      before do
        login 'minimum_admin'
        @source = tickets(:minimum_3)
        @target = tickets(:minimum_2)
        @target.update_attribute(:via_id, ViaType.FACEBOOK_POST)
      end

      it 'allows for only private merge comments' do
        get :show, params: { source_ids: @source.nice_id, target_id: @target.nice_id }
        refute assigns(:comment_can_be_public)
      end
    end

    describe "#private tickets" do
      before do
        login 'minimum_admin'
        @private_source = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: {
            assignee_id: users(:minimum_agent).id,
            comment: { value: "Hi wombat, private source comment", public: false },
          }).ticket
        @private_source.will_be_saved_by(users(:minimum_admin))
        @private_source.save!
        @public_source = tickets(:minimum_1)
        @source_ids = "#{@public_source.nice_id}, #{@private_source.nice_id}"
        @target = tickets(:minimum_2)
      end

      it 'allows only private merge comments if any of the sources is a private ticket' do
        get :show, params: { source_ids: @source_ids, target_id: @target.nice_id }
        refute assigns(:comment_can_be_public)
      end

      it 'allows only private merge comments if the target is a private ticket' do
        get :show, params: { source_ids: @public_source, target_id: @private_source.nice_id }
        refute assigns(:comment_can_be_public)
      end

      it 'allows public comments if all source tickets and target ticket are public' do
        get :show, params: { source_ids: @public_source.nice_id, target_id: @target.nice_id }
        assert assigns(:comment_can_be_public)
      end
    end
  end

  it "tests show with same source and target" do
    login('minimum_admin')
    source = tickets(:minimum_2)
    target = tickets(:minimum_2)
    get :show, params: { source_ids: source.nice_id, target_id: target.nice_id }
    assert_template "tickets/merge/_error"
  end

  it "tests show with target included in sources" do
    login('minimum_admin')
    source1 = tickets(:minimum_1)
    source2 = tickets(:minimum_2)
    target = tickets(:minimum_2)
    source_ids = "#{source1.nice_id}, #{source2.nice_id}"
    get :show, params: { source_ids: source_ids, target_id: target.nice_id }
    assert_template "tickets/merge/_error"
  end

  it "tests into solved ticket" do
    login('minimum_admin')
    source = tickets(:minimum_1)
    target = tickets(:minimum_4)
    post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment", target_is_public: '1' }
    assert_redirected_to ticket_path(target)
  end

  it "tests into closed ticket" do
    login('minimum_admin')
    source = tickets(:minimum_1)
    target = tickets(:minimum_5)
    post :create, params: { source_ids: source.nice_id, target_id: target.nice_id, source_comment: "Source comment", target_comment: "Target comment", target_is_public: '1' }
    assert_template "tickets/merge/_error"
  end

  Tickets::MergeController.class_eval do
    attr_writer :sources

    attr_writer :target
  end

  describe "#new" do
    before do
      login('minimum_agent')
      @source1 = tickets(:minimum_1)
      @source2 = tickets(:minimum_2)
      @target = tickets(:minimum_3)
      accounts(:minimum).update_attribute(:is_collaboration_enabled, true)
    end

    describe "with one source_id and user can merge the ticket" do
      before do
        User.any_instance.expects(:can?).with(:merge, @source1).returns(true)
        get :new, params: { source_ids: @source1.nice_id.to_s }
      end
      it('responds with success') { assert_response :success }
      should_assign_to :requested
      should_assign_to :current_view
      should_assign_to :recent
    end

    describe "when user can't merge that ticket" do
      before do
        User.any_instance.expects(:can?).with(:merge, @source1).returns(false)
        get :new, params: { source_ids: @source1.nice_id.to_s }
      end
      should_render_template "tickets/merge/_error"
    end

    describe "with no source_id" do
      before { get :new, params: { source_ids: "" } }
      should_render_template "tickets/merge/_error"
    end

    describe "with many source_ids and user can merge those tickets" do
      before do
        User.any_instance.expects(:can?).with(:merge, @source1).returns(true)
        User.any_instance.expects(:can?).with(:merge, @source2).returns(true)
        get :new, params: { source_ids: "#{@source1.nice_id}, #{@source2.nice_id}" }
      end
      it('responds with success') { assert_response :success }
    end

    describe "with collaboration disabled" do
      before do
        accounts(:minimum).update_attribute(:is_collaboration_enabled, false)
      end

      describe "and CCs/Followers settings disabled" do
        before do
          CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
          get :new, params: { source_ids: @source1.nice_id.to_s }
        end

        it('responds with success') { assert_response :success }
      end

      describe "and CCs/Followers settings enabled" do
        before do
          CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
          get :new, params: { source_ids: @source1.nice_id.to_s }
        end

        it('responds with success') { assert_response :success }
      end
    end

    describe "with collaboration enabled and CCs/Followers settings disabled" do
      before do
        accounts(:minimum).update_attribute(:is_collaboration_enabled, true)
        CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
        get :new, params: { source_ids: @source1.nice_id.to_s }
      end
      it('responds with success') { assert_response :success }
    end
  end

  describe "#validate_target" do
    describe "with non-numeric user input" do
      before do
        @controller.params[:target_id] = '123 Zendesk Content Spoofing'
      end
      it "calls show_error with non-numeric chars stripped" do
        @controller.expects(:show_error).with("You are unable to merge into #123. Tickets that are Closed, tickets that are shared with other accounts, and tickets you don't have access to cannot be merged into.")
        @controller.send(:validate_target)
      end
    end

    describe "the source ticket and target ticket have different requesters" do
      before do
        login('minimum_agent')
        @source1 = tickets(:minimum_1)
        @target = tickets(:minimum_3)
      end

      describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
        it "shows an error when is_collaboration is disabled" do
          Account.any_instance.stubs(:is_collaboration_enabled?).returns(false)
          @controller.expects(:show_error).with("You can only merge tickets with the same requester. The source ticket's requester, Author Minimum, is not the same as the target ticket's requester, minimum_end_user.")
          post :create, params: { source_ids: @source1.nice_id.to_s, target_id: @target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
        end
      end

      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          describe_with_arturo_setting_enabled :ticket_followers_allowed do
            it "shows an error when source requesters are not ALL agents" do
              @controller.expects(:show_error).with("You can only merge tickets with the same requester. The source ticket's requester, Author Minimum, is not the same as the target ticket's requester, minimum_end_user.")
              post :create, params: { source_ids: @source1.nice_id.to_s, target_id: @target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
            end

            it "allows the merge when source requesters are ALL agents" do
              @source1.requester = users(:minimum_agent)
              @source1.will_be_saved_by(users(:minimum_admin))
              @source1.save!
              @target.requester = users(:minimum_admin)
              @target.will_be_saved_by(users(:minimum_admin))
              @target.save!

              post :create, params: { source_ids: @source1.nice_id.to_s, target_id: @target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
              assert_match(/has been merged into ticket/, flash[:notice])
            end
          end

          describe_with_arturo_setting_disabled :ticket_followers_allowed do
            let(:error_message) do
              I18n.t(
                'txt.error_message.tickets.merge_controller.cannot_merge_tickets_with_different_requesters',
                target_name: @target.requester.name,
                source_name: @source1.requester.name
              )
            end

            def merge_tickets
              post :create, params: { source_ids: @source1.nice_id.to_s, target_id: @target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
            end

            describe "and the user is an agent" do
              before do
                login('minimum_agent')

                merge_tickets
              end

              before_should "show an error" do
                @controller.expects(:show_error).with(error_message)
              end
            end

            describe "and the user is an admin" do
              before do
                login('minimum_admin')

                merge_tickets
              end

              before_should "add additional output to the error message" do
                @controller.expects(:show_error).with(
                  error_message +
                  "\s" +
                  I18n.t(
                    'txt.error_message.tickets.merge_controller.cannot_merge_tickets_with_different_requesters_admin_addition',
                    learn_more_link: I18n.t('txt.admin.views.settings.tickets._settings.ccs_and_followers.learn_more_link')
                  )
                )
              end
            end
          end
        end

        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          it "allows the merge" do
            post :create, params: { source_ids: @source1.nice_id.to_s, target_id: @target.nice_id, source_comment: "Source comment", target_comment: "Target comment" }
            assert_match(/has been merged into ticket/, flash[:notice])
          end
        end
      end
    end
  end

  describe "#create" do
    before do
      login('minimum_agent')
      @user = users(:minimum_agent)
      @source = tickets(:minimum_1)
      @target = tickets(:minimum_2)
      @controller.target = @target
      @controller.stubs(:find_target)
      @controller.stubs(:validate_target)
      @controller.sources = [@source]
      @controller.stubs(:find_sources)
      @controller.stubs(:validate_sources)

      @ticket_merger = stub("ticket_merger")
      Zendesk::Tickets::Merger.stubs(:new).returns(@ticket_merger)
    end

    describe "with successful merge" do
      before do
        @ticket_merger.expects(:merge)
        post :create
      end
      it "sets flash[:notice]" do
        assert_match(/has been merged into ticket/, flash[:notice])
      end
    end

    describe "with invalid source" do
      before do
        @ticket_merger.expects(:merge).raises(Zendesk::Tickets::Merger::SourceInvalid.new(@source))
        @source.errors.add(:base, "Some error")
        post :create
      end
      it "sets flash[:error] with the error from source" do
        assert flash[:error].include?('Some error')
      end
      it "redirect_tos :back" do
        assert_redirected_to :back
      end
    end

    describe "with invalid target" do
      before do
        @ticket_merger.expects(:merge).raises(Zendesk::Tickets::Merger::TargetInvalid.new(@target, @user))
        @target.errors.add(:base, "Some error")
        post :create
      end
      it "sets flash[:error] with the error from target" do
        assert flash[:error].include?('Some error')
      end
      it "redirect_tos :back" do
        assert_redirected_to :back
      end
    end

    describe "with unexpected error" do
      before do
        @ticket_merger.expects(:merge).raises(Zendesk::Tickets::Merger::MergeFailed)
        post :create
      end
      it "sets flash[:error] with general message" do
        assert_equal flash[:error], I18n.t('txt.controllers.tickets.merge_controller.problem_while_merging')
      end
      it "redirects to :back" do
        assert_redirected_to :back
      end
    end

    describe "with merge in background" do
      describe "enqueue" do
        before do
          TicketMergeJob.expects(:enqueue).with(
            account_id: accounts(:minimum).id,
            user_id: users(:minimum_agent).id,
            target: @target.nice_id,
            sources: [@source.nice_id],
            target_comment: nil,
            source_comment: nil,
            target_is_public: false,
            source_is_public: false,
            target_href: "/tickets/#{@target.nice_id}",
            source_href: "/tickets/#{@source.nice_id}"
          ).returns('job-id')
          post :create, params: { background: 'true' }
        end
        it "responds with job id and status url" do
          body = JSON.parse(@response.body)
          assert_equal('job-id', body['id'])
          assert_equal(api_v2_job_status_url('job-id', format: :json), body['status_url'])
        end
      end

      describe "enqueue and work" do
        before do
          @ticket_merger.expects(:merge)
          post :create, params: { background: 'true' }
        end

        it "merges successfully" do
          body = JSON.parse(@response.body)
          job_id = body['id']
          status = Resque::Plugins::Status::Hash.get(job_id)
          assert(status['results'].first['success'])
          assert_match(/has been merged into ticket/, status['results'].first['message'])
        end
      end
    end
  end

  describe "#result" do
    before do
      login('minimum_agent')
      @target = tickets(:minimum_2)

      @job_status_stub = stub
      @job_status_stub.stubs(:[]).with('options').returns(target_href: '/ticket/123')
      @message = 'The Results.'
      Resque::Plugins::Status::Hash.expects(:get).with('job-id').returns(@job_status_stub)
    end

    describe "with successful merge" do
      before do
        @job_status_stub.stubs(:[]).with('results').returns([success: true, message: @message])

        get :result, params: { background: 'job-id' }
      end
      it "sets flash[:notice]" do
        assert_equal('The Results.', flash[:notice])
      end
      it "redirects to target href" do
        assert_redirected_to '/ticket/123'
      end
    end

    describe "with failed merge" do
      before do
        @job_status_stub.stubs(:[]).with('results').returns([success: false, message: @message])

        get :result, params: { background: 'job-id' }
      end
      it "sets flash[:error]" do
        assert_equal('The Results.', flash[:error])
      end
      it "redirects to :back" do
        assert_redirected_to :back
      end
    end
  end
end
