require_relative "../../support/test_helper"

SingleCov.covered!

describe Voice::RecordingsController do
  fixtures :accounts, :tickets, :addresses

  describe "#show v1" do
    before do
      @request.account = accounts(:minimum)
      @comment = VoiceComment.new
      comments = mock
      comments.expects(:find_by_id).with(as_id(1)).returns(@comment)
      Ticket.any_instance.expects(:comments).returns(comments)
    end

    describe "#recording_url" do
      describe "when sid matches the comment" do
        before do
          @comment.expects(:internal_recording_url).returns("http://foo.com/blah.mp3")
          recording = mock
          recording.expects(:matches_sid?).returns(true)
          @comment.expects(:recording).returns(recording).twice
        end

        it "redirects to recording_url" do
          get :show, params: { ticket_id: tickets(:minimum_1).id, comment_id: 1, sid: "foo" }
          assert_response :redirect
          assert_equal "http://foo.com/blah.mp3", @response.location
        end
      end

      describe "when sid doesn't match the comment" do
        before do
          recording = mock
          recording.expects(:matches_sid?).returns(false)
          @comment.expects(:recording).returns(recording).twice
        end

        it "returns a 404" do
          get :show, params: { ticket_id: tickets(:minimum_1).id, comment_id: 1, sid: "foo" }
          assert_response :not_found
        end
      end
    end
  end

  describe "#show v2" do
    before do
      @request.account = accounts(:minimum)
    end

    describe "#recording_url" do
      describe "when found call" do
        before do
          @call = Voice::Call.new
          calls = mock
          calls.expects(:find_by_id).with(as_id(1)).returns(@call)
          Account.any_instance.expects(:calls).returns(calls)
          @call.expects(:recording_url).returns("http://foo.com/blah")
        end

        it "redirects to recording_url" do
          get :show, params: { call_id: 1, sid: "foo" }
          assert_response :redirect
          assert_equal "http://foo.com/blah.#{VoiceComment::FORMAT}", @response.location
        end
      end

      describe "when call not found" do
        before do
          calls = mock
          calls.expects(:find_by_id).with(as_id(1)).returns(nil)
          Account.any_instance.expects(:calls).returns(calls)
        end

        it "returns a 404" do
          get :show, params: { call_id: 1, sid: "foo" }
          assert_response :not_found
        end
      end
    end
  end
end
