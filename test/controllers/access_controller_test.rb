require_relative "../support/test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"
require_relative "../support/brand_test_helper"

SingleCov.covered! uncovered: 36

describe AccessController do
  extend MobileSdk::RateLimitHelper
  include BrandTestHelper

  fixtures :all

  let(:warden_stub) { ActionController::TestCase::WardenStub }
  let(:spanish) { translation_locales(:spanish) }
  let(:hebrew) { translation_locales(:hebrew) }

  let(:error_params) { { error: "broken", error_description: "Not working" } }

  before do
    @request.account       = @minimum_account = @account = accounts(:minimum)
    @minimum_agent         = users(:minimum_agent)
    @remote_authentication = remote_authentications(:minimum_remote_authentication)
    @remote_authentication.update_attributes!(remote_logout_url: 'https://foobar.com')

    @saml_remote_logout_url = 'https://saml-exists-logout.com'
    @jwt_remote_logout_url = 'https://jwt-exists-logout.com'

    User.any_instance.stubs(:login_allowed?).returns(true)
    warden_stub.any_instance.stubs(:winning_strategy).returns(stub(redirect_at_end?: true, mobile_auth?: false))
  end

  with_options(controller: "access") do |request|
    request.should_route :get,  "/access/login",           action: "login"
    request.should_route :get,  "/access",                 action: "index"
    request.should_route :post, "/access/login",           action: 'login'
    request.should_route :put,  "/access/login",           action: 'login'
    request.should_route :post, "/access/normal_login",    action: 'normal_login'

    request.should_route :get,  "/access/logout",          action: "logout"
    request.should_route :get,  "/access/logout.json",     action: "logout", format: 'json'
    request.should_route :get,  "/access/normal",          action: "normal"
    request.should_route :get,  "/access/return_to",       action: "return_to"
    request.should_route :post, "/access/return_to",       action: "return_to"
    request.should_route :get,  "/access/unauthenticated", action: "unauthenticated"
    request.should_route :post, "/access/unauthenticated", action: 'unauthenticated'
    request.should_route :get, "/access/saml/login",       action: "saml", sso_redirection_strategy: "saml"
    request.should_route :post, "/access/saml",            action: "saml"
    request.should_route :get,  "/access/jwt",             action: "jwt"
    request.should_route :post, "/access/jwt",             action: "jwt"
    request.should_route :get,  "/access/oauth",           action: "oauth"
    request.should_route :post, "/access/oauth",           action: "oauth"
    request.should_route :get,  "/access/sso",             action: "sso"
    request.should_route :post, "/access/sso",             action: "sso"
    request.should_route :post, "/access/oauth_mobile",    action: "oauth_mobile_login"
    request.should_route :get, "/access/google",           action: "google"
    request.should_route :get, "/access/gam",              action: "gam"
    request.should_route :get, "/access/office_365",       action: "office_365"
    request.should_route :post, "/access/sdk/jwt",         action: "sdk_jwt"
    request.should_route :post, "/access/sdk/anonymous",   action: "sdk_anonymous"
    request.should_route :post, "/access/google_play",     action: "google_play"
    request.should_route :post, "/access/google_play_jwt", action: "google_play_jwt"
    request.should_route :get, "/access/sso_bypass",       action: "sso_bypass"
    request.should_route :get, "/access/request_two_factor", action: "request_two_factor"
    request.should_route :get, "/access/configure_two_factor", action: "configure_two_factor"
    request.should_route :get, "/access/access_token/123", action: "access_token", token: 123
    request.should_route :post, "/access/token",           action: "token"
  end

  it "recognizes GET /access/login" do
    assert_recognizes({controller: 'access', action: 'login'}, path: '/access/login', method: :get)
  end

  it "recognizes GET /login" do
    assert_recognizes({controller: 'access', action: 'index'}, path: '/login', method: :get)
  end

  it "recognizes master token urls" do
    assert_recognizes({controller: 'access', action: 'master', token: "jwt"}, path: '/access/master/jwt', method: :get)
  end

  describe "#request_oauth" do
    before do
      login(@minimum_agent)
    end

    it "shows a flash error message if there's a communication error with twitter" do
      exception = Net::HTTPFatalError.new(500, "Service unavailable")
      Zendesk::Auth::OauthDelegator.expects(:request_oauth!).raises(exception)
      get :request_oauth, params: { profile: 'twitter' }
      assert_response :redirect
      flash[:error].must_include I18n.t("txt.access.login.twitter_error")
    end

    it "shows a flash error message if there's a communication error with facebook" do
      exception = Net::HTTPFatalError.new(500, "Service unavailable")
      Zendesk::Auth::OauthDelegator.expects(:request_oauth!).raises(exception)
      get :request_oauth, params: { profile: 'facebook' }
      assert_response :redirect
      flash[:error].must_include I18n.t("txt.access.login.facebook_error")
    end

    it "shows a flash error message if there's a timeout error with facebook" do
      exception = Faraday::Error::TimeoutError.new("Timeout")
      Zendesk::Auth::OauthDelegator.expects(:request_oauth!).raises(exception)
      get :request_oauth, params: { profile: 'facebook' }
      assert_response :redirect
      flash[:error].must_include I18n.t("txt.access.login.facebook_error")
    end

    it "shows a flash error message if the user clicks 'Deny'" do
      access_token = mock('access_token')
      Zendesk::Auth::OauthDelegator.stubs(:request_oauth!).returns(access_token)
      exception = OAuth::Unauthorized.new(stub(code: 401, message: 'Unauthorized'))
      Zendesk::Auth::OauthDelegator.expects(:request_oauth!).raises(exception)
      get :request_oauth, params: { profile: 'twitter' }
      assert_response :redirect
      flash[:error].must_include I18n.t("txt.access.login.twitter_error")
    end

    it "shows a flash error message if the user clicks 'Deny' in facebook" do
      access_token = mock('access_token')
      Zendesk::Auth::OauthDelegator.stubs(:request_oauth!).returns(access_token)
      exception = OAuth2::Error.new(stub_everything(code: 400, error: 'access_denied'))
      Zendesk::Auth::OauthDelegator.expects(:request_oauth!).raises(exception)
      get :request_oauth, params: { profile: 'facebook' }
      assert_response :redirect
      flash[:error].must_include I18n.t("txt.access.login.facebook_error")
    end

    it "shows a flash error message if the auth profile is missing" do
      exception = Zendesk::Auth::InvalidProfileException.new("tweeter")
      Zendesk::Auth::OauthDelegator.expects(:request_oauth!).raises(exception)
      get :request_oauth, params: { profile: 'tweeter' }
      assert_response :redirect
      flash[:error].must_include I18n.t("txt.access.login.twitter_error")
    end
  end

  describe "#login" do
    before do
      logout
      @request.stubs(:ssl?).returns(true)
      @valid_login = {user: {email: "agent@zendesk.com", password: "123456"}}
      warden_stub.any_instance.stubs(:params).returns({})
    end

    it "logins" do
      post :login, params: @valid_login

      assert_redirected_to "/access"
    end

    it "normal_logins" do
      post :normal_login, params: @valid_login

      assert_redirected_to "/access"
    end

    it "expired_passwords" do
      shared_session[:auth_password_expired] = true
      post :login, params: @valid_login

      assert_redirected_to "/access"
    end

    describe "return_to" do
      it "returns to /access when return_to was blank" do
        post :login, params: @valid_login.merge(return_to: "")

        assert_redirected_to "/access"
      end

      it "returns to path ZD#243573" do
        post :login, params: @valid_login.merge(return_to: "/login")

        assert_redirected_to "/access"
      end

      describe "when Lotus is preferred" do
        before do
          # Right now, @valid_login won't actually log in because there are no
          # warden auth strategies setup so we stub the user model
          @controller.stubs(:current_user).returns users(:minimum_agent)
          @account.settings.prefer_lotus = true
          @account.settings.save!
        end

        it "redirects to agent ui" do
          post :login, params: @valid_login
          assert_redirected_to "https://minimum.zendesk-test.com/agent"
        end

        it "discards flash after redirecting to Lotus" do
          post :login, params: @valid_login
          assert_nil flash.to_session_value
        end

        it "redirects with challenge token when lotus is on a different host" do
          @account.host_mapping = "hostmapped.com"
          @request.stubs(host: @account.host_mapping)
          post :login, params: @valid_login
          expected = "https://minimum.zendesk-test.com/access/return_to?challenge=TOKEN&locale=en-US-x-1&return_to=https://minimum.zendesk-test.com/agent"
          assert_equal expected, @response.location.sub(/challenge=\w+/, "challenge=TOKEN")
        end
      end
    end

    describe "request from HelpCenter with return_to /hc" do
      before do
        @controller.stubs(:current_user).returns(@minimum_agent)
      end

      it "ensure_return_tos HelpCenter when login in as an Agent who prefer_lotus" do
        @account.settings.prefer_lotus = true
        @account.settings.save!

        post :login, params: @valid_login.merge(return_to: "/hc/en-us")
        assert_equal "https://minimum.zendesk-test.com/hc/en-us", response.location
      end

      it "redirects to hostmapped helpcenter with challenge token" do
        @account.update_attribute(:host_mapping, "mapped.com")
        assert_equal "minimum.zendesk-test.com", request.host
        assert_equal "http://mapped.com", @account.url

        post :login, params: @valid_login.merge(return_to: "/hc/en-us")
        assert_equal "http://mapped.com/access/return_to?challenge=XXX&locale=en-US-x-1&return_to=http://mapped.com/hc/en-us", response.location.gsub(/(challenge)=\w+/, "\\1=XXX")
      end
    end

    describe "with bad login" do
      before do
        warden_stub.any_instance.expects(:authenticate!).raises(Warden::NotAuthenticated)
        ZendeskExceptions::Logger.expects(:record)
      end

      let(:login_params) do
        {
          user: {
            email: "agent@zendesk.com",
            password: "123456"
          }
        }
      end

      it "shows flash error message for unknown error" do
        post :login, params: login_params
        assert_response :redirect
        assert_match /Unknown error during sign-in/, flash[:error]
      end

      it "shows a flash error message if bad things were specified" do
        post :login, params: login_params.merge(error_params)
        assert_response :redirect
        assert_match(/Authentication Error: broken: Not working/, flash[:error])
      end
    end

    describe "when rate limited" do
      before do
        warden_stub.any_instance.expects(:authenticate!).raises(
          Prop::RateLimited.new(
            handle: "Hello",
            cache_key: "123",
            threshold: 10,
            interval: 10,
            strategy: Prop::IntervalStrategy,
            first_throttled: false
          )
        )
      end

      it "renders the fail message" do
        post :login, params: @valid_login
        assert_response :forbidden
        @response.body.must_include "Forbidden by rate limit"
      end
    end

    describe "with arturo feature mobile_v2 enabled" do
      before do
        @request.account = @minimum_account
        Account.any_instance.stubs(:has_mobile_switch_enabled?).returns(true)
      end

      it "does not show flash notice on enduser's login" do
        login(:minimum_end_user)
        post :login, format: 'mobile_v2'
        assert_nil flash[:notice]
      end

      it "does not show flash notice on agent's login" do
        login(@minimum_agent)
        post :login, format: 'mobile_v2'
        assert_nil flash[:notice]
      end

      it "no flash on agent login, default to lotus now" do
        login(@minimum_agent)
        post :login
        assert_nil flash[:notice]
      end
    end
  end

  describe "#access_token" do
    it "performs authentication" do
      warden_stub.any_instance.expects(:authenticate!)
      get :access_token, params: { token: 123 }
      assert_response :redirect
    end

    it "does not perform authentication if head request" do
      warden_stub.any_instance.expects(:authenticate!).never
      head :access_token, params: { token: 123 }
      assert_response :ok
    end
  end

  describe "#return_to" do
    it "redirects back" do
      get :return_to, params: { return_to: "/ping/host" }
      assert_redirected_to "/ping/host"
    end

    it "does not flash" do
      get :return_to, params: { return_to: "/ping/host" }.merge(error_params)
      refute flash[:notice]
    end

    it "does not redirect to proper protocol to avoid redirect loops" do
      @account.update_attribute(:host_mapping, "foobar.com")
      Account.any_instance.stubs(ssl_should_be_used?: true)
      get :return_to, params: { return_to: "http://foobar.com/ping/host" }
      assert_redirected_to "http://foobar.com/ping/host"
    end
  end

  describe "#oauth" do
    describe "with twitter" do
      describe "when user clicks 'Deny'" do
        before do
          exception = OAuth::Unauthorized.new(stub(code: 401, message: 'Unauthorized'))
          warden_stub.any_instance.expects(:authenticate!).raises(exception)
          post :oauth, params: { profile: 'twitter' }
        end

        it "shows a flash error message if the user clicks 'Deny'" do
          assert_response :redirect
          flash[:error].must_include I18n.t("txt.access.login.twitter_error_v2")
        end
      end
    end

    describe "with facebook" do
      it "shows a flash error message if the user clicks 'Deny'" do
        exception = OAuth2::Error.new(stub_everything(code: 400, error: 'access_denied'))
        warden_stub.any_instance.expects(:authenticate!).raises(exception)
        post :oauth, params: { profile: 'facebook' }
        assert_response :redirect
        flash[:error].must_include I18n.t("txt.access.login.facebook_error_v2")
      end

      it "shows a flash error message with error from facebook if we get a bad request" do
        warden_stub.any_instance.expects(:authenticate!).raises(Warden::NotAuthenticated)
        ZendeskExceptions::Logger.expects(:record)
        post :oauth, params: { profile: 'facebook', error: 'access_denied', error_description: 'The user denied your request' }
        assert_response :redirect
        flash[:error].must_include "The user denied your request"
      end

      it "shows a flash error message with error from facebook if there is a timeout" do
        warden_stub.any_instance.expects(:authenticate!).raises(Faraday::Error::TimeoutError.new("Timeout"))
        post :oauth, params: { profile: 'facebook' }
        assert_response :redirect
        flash[:error].must_include I18n.t("txt.access.login.facebook_error_v2")
      end

      it "shows a flash error message with error from facebook if network is unreachable" do
        warden_stub.any_instance.expects(:authenticate!).raises(Faraday::Error::ConnectionFailed.new("Network is unreachable"))
        post :oauth, params: { profile: 'facebook' }
        assert_response :redirect
        flash[:error].must_include I18n.t("txt.access.login.facebook_error_v2")
      end

      it "shows a flash error message if received retryable channels error" do
        warden_stub.any_instance.expects(:authenticate!).raises(Channels::Errors::RetryError.new("Channels error"))
        post :oauth, params: { profile: 'facebook' }
        assert_response :redirect
        flash[:error].must_include I18n.t("txt.access.login.facebook_error_v2")
      end

      it "has a callback_url in the request parameters" do
        warden_stub.any_instance.expects(:authenticate!)
        post :oauth, params: { profile: 'facebook' }
        assert_not_nil @request.params[:callback_url]
      end
    end
  end

  describe "#logout" do
    before do
      @controller.stubs(:current_account).returns(@minimum_account)
      @controller.stubs(:current_brand).returns(@minimum_account.default_brand)
      @controller.stubs(:clear_session_and_logout)
      @controller.stubs(:set_locale)
      @controller.stubs(:log_session_info)
      @controller.stubs(:use_remote_authentication?).returns(false)
    end

    describe "for a single domain" do
      before do
        @controller.stubs(:initial_redirect_to_other_domain)
      end

      it "shows a logged out flash message" do
        @controller.stubs(:initial_redirect_to_other_domain)
        get :logout
        assert_equal 'You have been signed out.', flash[:notice]
      end

      it "protects against scripty return_to" do
        @return_to = 'data:x,<script>alert(1)</script><about:blank>'
        get :logout, params: { return_to: @return_to }
        assert_redirected_to '/access'
      end

      it "protects against another kind of scripty return_to" do
        @return_to = URI.unescape('javascript://%0Ahttp://e%22%3E%3Cs%3Evil.com')
        get :logout, params: { return_to: @return_to }
        assert_redirected_to '/access'
      end

      it "respects force_unaltered_return_to" do
        @minimum_account.anonymous_user.stubs(:email).returns("foo")
        @controller.stubs(:current_user).returns(@minimum_account.anonymous_user)
        @controller.stubs(:current_user_exists?).returns(true)

        return_to = CGI.escape("/hc")
        get :logout, params: { return_to: return_to, force_unaltered_return_to: true }
        assert_redirected_to '/hc'
      end

      describe "when the user cannot use remote authentication" do
        before do
          @minimum_account.stubs(:remote_authentications).returns(stub(active: [@remote_authentication]))
          @minimum_agent.stubs(:login_allowed?).with(:remote).returns(false)
          @controller.stubs(:current_user).returns(@minimum_agent)
          get :logout
        end

        it "redirects to /access" do
          assert_redirected_to '/access'
        end
      end

      describe "if remote authentication should be used and we're sending a return_to param" do
        before do
          @minimum_account.stubs(:remote_authentications).returns(stub(active: [@remote_authentication]))
          @minimum_agent.stubs(:login_allowed?).with(:remote).returns(true)
          @controller.stubs(:current_user).returns(@minimum_agent)
          get :logout, params: { return_to: "/home" }
        end

        it "redirects to the remote logout url instead of return_to param" do
          assert response.location.start_with?(@remote_authentication.remote_logout_url)
        end
      end

      describe "setup sso with saml and jwt on the test account" do
        before do
          Account.any_instance.stubs(has_saml?: true)

          @auth = @account.remote_authentications.create!(
            fingerprint: '55:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C',
            remote_logout_url: @saml_remote_logout_url
          )
          @auth.auth_mode = RemoteAuthentication::SAML
          @auth.is_active = true
          @auth.save!

          @auth = @account.remote_authentications.create!(
            next_token: SecureRandom.hex(20), # same as the shared_secret parameter that is passed, but needs next_token to pass validations
            remote_logout_url: @jwt_remote_logout_url
          )
          @auth.auth_mode = RemoteAuthentication::JWT
          @auth.is_active = true
          @auth.save!

          @request.account = @account
          @controller.stubs(:current_account).returns(@account)

          @minimum_agent.stubs(:login_allowed?).with(:remote).returns(true)
          @controller.stubs(:current_user).returns(@minimum_agent)
        end

        describe "with sso_logout_redirect_url_change flag enabled" do
          before { Arturo.enable_feature!(:sso_logout_redirect_url_change) }

          describe "if saml/jwt remote authentication should be used and we're sending a return_to param" do
            it "redirects to the saml remote logout url" do
              @request.shared_session[:auth_via] = 'saml'
              get :logout, params: { return_to: "/home" }
              assert response.location.start_with?(@saml_remote_logout_url)
            end

            it "redirects to the jwt remote logout url" do
              @request.shared_session[:auth_via] = 'jwt'
              get :logout, params: { return_to: "/home" }
              assert response.location.start_with?(@jwt_remote_logout_url)
            end

            it "redirects to the first remote authentication(based on priority) logout url when auth_via is nil" do
              get :logout, params: { return_to: "/home" }
              assert response.location.start_with?(@saml_remote_logout_url)
            end

            it "shows a flash error message if bad things were specified" do
              warden_stub.any_instance.expects(:authenticate!).raises(Warden::NotAuthenticated)
              post :login, params: { return_to: "/home" }.merge(error_params)
              assert_match(/Authentication Error: broken: Not working/, flash[:error])
            end
          end
        end

        describe "with sso_logout_redirect_url_change flag disabled" do
          before { Arturo.disable_feature!(:sso_logout_redirect_url_change) }

          describe "if saml/jwt remote authentication should be used and we're sending a return_to param based on priority" do
            it "redirects to the saml remote logout url" do
              @request.shared_session[:auth_via] = 'saml'
              get :logout, params: { return_to: "/home" }
              assert response.location.start_with?(@saml_remote_logout_url)
            end

            it "redirects to the saml remote logout url" do
              @request.shared_session[:auth_via] = 'jwt'
              get :logout, params: { return_to: "/home" }
              assert response.location.start_with?(@saml_remote_logout_url)
            end
          end
        end
      end
    end

    describe "with multiple domains" do
      before { @account.update_attribute(:host_mapping, "foo.com") }

      describe "from zendesk domain" do
        it "redirects to the return_to" do
          get :logout, params: { return_to: "/ping/host" }
          assert_redirected_to "http://foo.com/ping/host"
        end
      end

      describe "from hostmapped domain" do
        before { @request.stubs(host: @account.host_mapping) }

        it "redirects to the return_to" do
          get :logout, params: { return_to: "/ping/host" }
          assert_redirected_to "http://foo.com/ping/host"
        end
      end
    end
  end

  describe "#index" do
    describe "anonymous user" do
      before do
        @controller.stubs(:current_account).returns(@minimum_account)
        @controller.stubs(:current_user).returns(@minimum_account.anonymous_user)
        @controller.stubs(:current_brand).returns(@request.account.default_brand)
        @controller.stubs(:current_route).returns(@request.account.default_brand.route)
      end

      it "renders the sso v2 template" do
        get :index

        assert_template 'sso_v2_index'
      end

      it "escapes return_to" do
        xss = "</script><script>alert('XSS')</script>"
        get :index, params: { return_to: xss }
        refute_includes response.body, xss
      end

      describe "when the mobile format is requested" do
        before do
          @controller.stubs(:current_brand).returns(@request.account.default_brand)
          @controller.stubs(:current_route).returns(@request.account.default_brand.route)
          get :index, format: 'mobile_v2'
        end

        it "renders normal" do
          assert_template 'sso_v2_index'
        end

        it('responds with success') { assert_response :success }
      end
    end

    describe "agent" do
      before do
        @controller.stubs(:current_user).returns(@minimum_agent)
        @account.settings.prefer_lotus = true
        @account.settings.save!
      end

      it "redirects to lotus" do
        get :index
        assert_redirected_to "https://minimum.zendesk-test.com/agent"
      end

      it "redirects to access" do
        session[:is_mobile] = true
        get :index
        assert_redirected_to "https://minimum.zendesk-test.com/access"
      end
    end
  end

  describe "#normal" do
    before do
      @request.account = @minimum_account
      @account.update_attribute(:host_mapping, "mapped.com")
      @account.update_attribute(:is_ssl_enabled, true)
      @account.certificates.first.update_attribute(:state, "active")
      @account.certificates.first.update_attribute(:valid_until, 10.days.from_now.to_date)
      @account.stubs(:has_access_token_sso_bypass?).returns(false)

      @controller.stubs(:current_brand).returns(@account.default_brand)
      @controller.stubs(:current_route).returns(@account.default_brand.route)

      assert @account.certificates.active.any?
      assert @account.is_ssl_enabled?
    end

    describe 'when accessing from iframe on hostmapped domain' do
      describe 'access_controller_csp_frame_ancestors_enabled turnd on' do
        it 'should contain hostmapping header in csp' do
          Arturo.enable_feature!(:access_controller_csp_frame_ancestors_enabled)
          get :normal
          assert_nil response.headers['X-Frame-Options']
          assert_equal response.headers['Content-Security-Policy'], "frame-ancestors 'self' mapped.com;"
        end
      end

      describe 'access_controller_csp_frame_ancestors_enabled turnd off' do
        it 'should contain hostmapping header in csp' do
          Arturo.disable_feature!(:access_controller_csp_frame_ancestors_enabled)
          get :normal
          assert_equal response.headers['X-Frame-Options'], "SAMEORIGIN"
          assert_nil response.headers['Content-Security-Policy']
        end
      end

      describe 'access_controller_csp_frame_ancestors_enabled turnd on on multibrand account' do
        before do
          create_brand(@account, name: 'brand2', subdomain: 'brand2', host_mapping: 'brand2.com')
        end
        it 'should contain hostmapping header in csp' do
          Arturo.enable_feature!(:access_controller_csp_frame_ancestors_enabled)
          get :normal
          assert_nil response.headers['X-Frame-Options']
          assert_equal response.headers['Content-Security-Policy'], "frame-ancestors 'self' mapped.com brand2.com;"
        end
      end
    end

    describe 'when accessed over HTTP' do
      before { https!(false) }

      describe 'on non-hostmapped url' do
        before do
          request.stubs(host: 'minimum.zendesk-test.com')
          @account.update_attribute(:host_mapping, nil)
          get :normal
        end

        it 'redirects to https' do
          assert_redirected_to "https://minimum.zendesk-test.com/access/normal"
        end
      end

      describe 'on hostmapped url' do
        before do
          request.stubs(host: @account.host_mapping)
          get :normal
        end

        it 'redirects to https' do
          assert_redirected_to "https://mapped.com/access/normal"
        end
      end
    end

    it 'renders the new auth if we are on the hostmapped url' do
      request.stubs(host: @account.host_mapping)
      get :normal

      assert_template 'sso_v2_index'
    end

    it 'renders the new auth if we are not on the hostmapped url' do
      request.stubs(host: "minimum.zendesk-test.com")
      get :normal

      assert_template 'sso_v2_index'
    end

    describe 'with access_token_sso_bypass enabled' do
      before do
        @account.stubs(:has_access_token_sso_bypass?).returns(true)
      end

      describe 'with sso_bypass account setting disabled' do
        before do
          @account.settings.sso_bypass = false
          @account.settings.save!
          get :normal
        end

        it 'renders a 404' do
          assert_response 404
        end
      end

      describe 'with sso_bypass account setting enabled' do
        before do
          @account.settings.sso_bypass = true
          @account.settings.save!
          get :normal
        end

        should_redirect_to('sso_bypass') { access_sso_bypass_path }
      end
    end

    describe 'when agents can have a password' do
      before do
        @account.role_settings.update_attributes!(agent_zendesk_login: true, end_user_zendesk_login: false, agent_remote_bypass: 0, end_user_password_allowed: false)
      end

      it "renders access/normal" do
        get :normal
        assert_response :ok
      end
    end

    describe 'when end users can have a password' do
      before do
        @account.role_settings.update_attributes!(agent_zendesk_login: false, end_user_zendesk_login: true, agent_remote_bypass: 0, agent_password_allowed: true)
      end

      it "renders access/normal" do
        get :normal
        assert_response :ok
      end
    end

    describe 'when no roles can use a password' do
      before do
        @account.role_settings.update_attributes!(agent_zendesk_login: false, end_user_zendesk_login: false, agent_password_allowed: false, end_user_password_allowed: false)
      end

      it 'renders a 404 if remote bypass is disabled' do
        @account.role_settings.update_attributes!(agent_remote_bypass: 0)
        get :normal

        assert_response 404
      end

      it 'redirects to sso_bypass if enabled' do
        @account.role_settings.update_attributes!(agent_remote_bypass: 1)
        get :normal

        assert_redirected_to access_sso_bypass_path
      end
    end
  end

  describe "#request_two_factor" do
    before do
      @controller.stubs(:current_brand).returns(@request.account.default_brand)
      @controller.stubs(:current_route).returns(@request.account.default_brand.route)
    end

    it "redirects to account home when 2FA not in progress" do
      get :request_two_factor
      assert_redirected_to "https://minimum.zendesk-test.com/access"
    end

    it "redirects to /access when logged in" do
      @controller.stubs(:current_registered_user).returns(@minimum_agent)
      get :request_two_factor
      assert_redirected_to "https://minimum.zendesk-test.com/access"
    end

    it "renders the sso v2 template" do
      @request.session[Zendesk::Auth::Otp::SESSION_USER_KEY] = 1
      get :request_two_factor
      assert_template 'sso_v2_index'
      assert_includes response.body, 'request_two_factor'
    end
  end

  describe "#configure_two_factor" do
    before do
      @controller.stubs(:current_brand).returns(@request.account.default_brand)
      @controller.stubs(:current_route).returns(@request.account.default_brand.route)
    end

    it "redirects to /access when 2FA not in progress" do
      get :configure_two_factor
      assert_redirected_to "https://minimum.zendesk-test.com/access"
    end

    it "redirects to /access when logged in" do
      @controller.stubs(:current_registered_user).returns(@minimum_agent)
      get :configure_two_factor
      assert_redirected_to "https://minimum.zendesk-test.com/access"
    end

    it "renders the sso v2 template" do
      @request.session[Zendesk::Auth::Otp::SESSION_USER_KEY] = 1
      get :configure_two_factor
      assert_template 'sso_v2_index'
      assert_includes response.body, 'configure_two_factor'
    end
  end

  describe "#sso_bypass" do
    before do
      @controller.stubs(:current_brand).returns(@request.account.default_brand)
      @controller.stubs(:current_route).returns(@request.account.default_brand.route)
    end

    describe 'with sso_bypass account setting enabled' do
      before do
        Arturo.enable_feature! :access_token_sso_bypass
        @account.settings.sso_bypass = true
        @account.settings.save!
      end

      it "renders the sso v2 template" do
        get :sso_bypass

        assert_template 'sso_v2_index'
      end
    end

    describe 'with sso_bypass account setting disabled' do
      before do
        Arturo.enable_feature! :access_token_sso_bypass
        @account.settings.sso_bypass = false
        @account.settings.save!
      end

      it 'renders a 404' do
        get :sso_bypass

        assert_response 404
      end
    end

    it "renders the sso_bypass page when agent bypass is allowed" do
      @account.role_settings.update_attributes!(agent_zendesk_login: 0, agent_remote_bypass: 1)
      get :sso_bypass

      assert_template 'sso_v2_index'
    end

    it 'renders a 404 when agent bypass is blocked' do
      @account.role_settings.update_attributes!(agent_remote_bypass: 0)
      get :sso_bypass

      assert_response 404
    end
  end

  describe "#token" do
    before do
      @request.account = @minimum_account
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []
      @account.role_settings.update_attributes!(agent_zendesk_login: false, agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ADMINS)
      assert_equal 0, @account.otp_tokens.count(:all)
    end

    describe 'when locale of user is LTR' do
      before do
        @account.owner.update_attributes!(locale_id: spanish.id)
      end

      describe 'with an invalid email' do
        it 'redirects back to success_sso_bypass with an error message to avoid user enumeration' do
          post :token, params: { email: 'dog@cats.com' }

          assert_redirected_to "https://minimum.zendesk-test.com/auth/v2/login/success_access_token"
        end
      end

      describe 'with a email from an admin' do
        let(:email) { @account.owner.email }

        it 'creates a otp token' do
          post :token, params: { email: email }

          assert_equal 1, @account.otp_tokens.count(:all)
        end

        it 'sends an access email with the token, proper content and formatting' do
          post :token, params: { email: email }

          email = ActionMailer::Base.deliveries.last
          token = @account.otp_tokens.last.value

          expected_link = "https://minimum.zendesk-test.com/access/access_token/#{token}"
          expected_subject = "Vínculo de inicio de sesión de un solo uso para su cuenta de Zendesk (Minimum account)"

          assert_equal expected_subject, email.subject
          assert_match /Puede usar el siguiente vínculo dentro de los próximos cinco minutos/, email.parts.last.body.to_s
          assert_match expected_link, email.parts.last.body.to_s
          assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
        end

        it 'redirects to the success page' do
          post :token, params: { email: email, auth_origin: 'auth_origin' }

          expected = "https://minimum.zendesk-test.com/auth/v2/login/success_access_token?auth_origin=auth_origin"
          assert_redirected_to expected
        end

        it 'does nothing if disabled' do
          @account.role_settings.update_attributes!(agent_remote_bypass: RoleSettings::REMOTE_BYPASS_DISABLED)
          post :token, params: { email: email }

          assert_equal 0, @account.otp_tokens.count(:all)
        end

        it 'does nothing when an admin attempts and sso bypass is owner only' do
          @account.role_settings.update_attributes!(agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ACCOUNT_OWNER)
          post :token, params: { email: users(:minimum_admin_not_owner).email }

          assert_equal 0, @account.otp_tokens.count(:all)
        end
      end

      describe 'with an agent email' do
        let(:email) { @minimum_agent.email }

        before do
          stub_request(:get, "http://metropolis:8889/api/services/staff/#{@minimum_agent.id}/entitlements").to_return(
            status: 200,
            body: { entitlements: { support: 'agent' } }.to_json,
            headers: {'Content-Type' => 'application/json'}
          )
        end

        it 'does nothing' do
          post :token, params: { email: email }

          assert_equal 0, @account.otp_tokens.count(:all)
        end

        it 'redirect to the success_access_token page with an error to avoid user enumeration' do
          post :token, params: { email: email }

          assert_redirected_to "https://minimum.zendesk-test.com/auth/v2/login/success_access_token"
        end
      end
    end

    describe 'when locale of user is RTL' do
      before do
        @account.owner.update_attributes!(locale_id: hebrew.id)
      end

      let(:email) { @account.owner.email }

      it 'adds RTL styling to email' do
        post :token, params: { email: email }

        email = ActionMailer::Base.deliveries.last
        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end

  describe "#one_time_password" do
    before do
      @request.account = @minimum_account
    end

    it "renders the otp template" do
      get :one_time_password, params: { theme: 'hc' }

      assert_template 'one_time_password'
    end

    describe "when the mobile format is requested" do
      before { get :one_time_password, params: { format: 'mobile_v2' } }

      it "renders otp" do
        assert_template 'one_time_password'
      end

      it('responds with success') { assert_response :success }
    end
  end

  describe '#ensure_authentication_domain' do
    before do
      @request.account = @minimum_account
      https!(false)
    end

    it 'redirects on #one_time_password' do
      get :one_time_password, params: { return_to: 'hi' }
      assert_redirected_to @minimum_account.authentication_domain + '/access/one_time_password?return_to=hi'
    end
  end

  describe "#dispatch_to_home" do
    it "responds with ok and do internal redirect if internal_help_center_root? is true" do
      @controller.stubs(internal_help_center_root?: true)
      get :dispatch_to_home

      assert_response :ok
      assert_equal "/hc?internal_root=true", @controller.response.headers["X-Accel-Redirect"]
    end

    it "redirects to access/ for other requests" do
      get :dispatch_to_home

      assert_redirected_to "/access"
      assert_response :moved_permanently
    end
  end

  describe 'sends Zendesk-EP header when rate_limit_hc_redirects: true' do
    before do
      Arturo.enable_feature! :rate_limit_hc_redirects
    end

    it 'returns zendesk-ep header with a value of -10' do
      @controller.stubs(help_center_home?: true)
      get :dispatch_to_home
      assert_equal -10, @controller.response.headers["ZENDESK-EP"]
    end
  end

  describe 'sends Zendesk-EP header when rate_limit_hc_redirects: true' do
    before do
      Arturo.enable_feature! :rate_limit_hc_redirects
      Arturo.enable_feature! :rate_limit_hc_redirects_challenge
    end

    it 'returns zendesk-ep header with a value of 10' do
      @controller.stubs(help_center_home?: true)
      get :dispatch_to_home
      assert_equal 10, @controller.response.headers["ZENDESK-EP"]
    end
  end

  describe 'does not send Zendesk-EP header when rate_limit_hc_redirects: false' do
    before do
      Arturo.disable_feature!(:rate_limit_hc_redirects)
      Arturo.disable_feature!(:rate_limit_hc_redirects_challenge)
    end
    it 'returns zendesk-ep header with a value of nil' do
      @controller.stubs(help_center_home?: true)
      get :dispatch_to_home
      assert_nil @controller.response.headers["ZENDESK-EP"]
    end
  end

  describe "#request_oauth callback urls" do
    before do
      @request.account = @minimum_account
      @url = @account.url
      @state = Zendesk::Auth::Warden::OAuthStrategy.state({}, Zendesk::Configuration.dig!(:google, :key))
    end

    describe "for facebook" do
      it "uses secure sub-domain url" do
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).with(
          anything,
          "https://support.zendesk-test.com/ping/redirect_to_account",
          true,
          @request.env,
          state: "minimum:/access/oauth?profile=facebook&real_domain=monkeys&return_to=monkeys&state=#{@state}"
        ).returns(@url)
        get :request_oauth, params: { profile: 'facebook', return_to: 'monkeys' }
      end
    end

    describe "for facebook with iframe=true" do
      it "displays as popup" do
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).with(
          anything,
          "https://support.zendesk-test.com/ping/redirect_to_account",
          true,
          @request.env,
          state: "minimum:/access/oauth?profile=facebook&real_domain=monkeys&return_to=monkeys&state=#{@state}",
          display: 'popup'
        ).returns(@url)
        get :request_oauth, params: { profile: 'facebook', return_to: 'monkeys', iframe: 'true' }
      end
    end

    describe "for facebook with iframe=false" do
      it "does not display as popup" do
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).with(
          anything,
          "https://support.zendesk-test.com/ping/redirect_to_account",
          true,
          @request.env,
          state: "minimum:/access/oauth?profile=facebook&real_domain=monkeys&return_to=monkeys&state=#{@state}"
        ).returns(@url)
        get :request_oauth, params: { profile: 'facebook', return_to: 'monkeys', iframe: 'false' }
      end
    end

    describe "for twitter" do
      it "uses secure sub-domain url" do
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).with(
          anything,
          "https://support.zendesk-test.com/ping/redirect_to_account?state=minimum%3A%2Faccess%2Foauth%3Fprofile%3Dtwitter%26real_domain%3Dmonkeys%26return_to%3Dmonkeys%26state%3D#{@state}",
          true,
          @request.env,
          {}
        ).returns(@url)
        get :request_oauth, params: { profile: 'twitter', return_to: 'monkeys' }
      end

      it "does the same thing for twitter_readonly" do
        Zendesk::Auth::OauthDelegator.expects(:request_oauth!).with(
          anything,
          "https://support.zendesk-test.com/ping/redirect_to_account?state=minimum%3A%2Faccess%2Foauth%3Fprofile%3Dtwitter_readonly%26real_domain%3Dmonkeys%26return_to%3Dmonkeys%26state%3D#{@state}",
          false,
          @request.env,
          {}
        ).returns(@url)
        get :request_oauth, params: { profile: 'twitter_readonly', return_to: 'monkeys' }
      end
    end
  end

  describe "#master" do
    before do
      logout
      @request.stubs(:ssl?).returns(true)
      warden_stub.any_instance.stubs(:params).returns({})
    end

    it "only authenticate with master_token strategy" do
      warden.expects(:authenticate!).with(:master_token)
      get :master, params: { token: 'token' }
    end
  end

  specialized_auth_endpoints = {
    return_to: [:challenge_token],
    saml: [:saml],
    jwt: [:jwt],
    oauth: [:twitter, :facebook],
    sso: [:saml, :jwt]
  }

  specialized_auth_endpoints.each do |action, strategies|
    describe "##{action}" do
      before do
        logout
        @request.stubs(:ssl?).returns(true)
        warden_stub.any_instance.stubs(:params).returns({})
      end

      it "only authenticate with strategies #{strategies.join(', ')}" do
        warden.expects(:authenticate!).with(*strategies)
        post action
      end

      describe "return_to" do
        it "returns to /access when return_to was blank" do
          post action
          assert_match %r{/access$}, @response.redirected_to
        end

        it "returns to the redirect_to" do
          post action, params: { return_to: "/foo" }
          assert_match %r{/foo$}, @response.redirected_to
        end

        describe 'login success stats logging' do
          before do
            Zendesk::StatsD::Client.any_instance.
              stubs(:increment).
              with(
                Not(equals('access.login.success')),
                any_parameters
              )
          end

          describe 'with agent return_to' do
            it 'sends expected tags' do
              Zendesk::StatsD::Client.any_instance.
                expects(:increment).
                with('access.login.success',
                  tags: [
                    'user_role:end_user',
                    'form_origin:',
                    'auth_flow_by_role:enduser_agent',
                    'return_to_path:agent',
                    "controller_action:#{action}"
                  ]).once

              post action, params: { return_to: "/agent" }
            end
          end

          describe 'with hc return_to' do
            it 'sends expected tags' do
              Zendesk::StatsD::Client.any_instance.
                expects(:increment).
                with('access.login.success',
                  tags: [
                    'user_role:end_user',
                    'form_origin:',
                    'auth_flow_by_role:enduser_hc',
                    'return_to_path:hc',
                    "controller_action:#{action}"
                  ]).once

              post action, params: { return_to: "/hc/en-us" }
            end
          end

          describe 'with no return_to' do
            it 'sends expected tags' do
              Zendesk::StatsD::Client.any_instance.
                expects(:increment).
                with('access.login.success',
                  tags: [
                    'user_role:end_user',
                    'form_origin:',
                    'auth_flow_by_role:enduser_unknown',
                    'return_to_path:other',
                    "controller_action:#{action}"
                  ]).once

              post action
            end
          end
        end
      end
    end
  end

  describe "#sdk_jwt" do
    before do
      logout
      accept :json
      @controller.stubs(:allow_only_mobile_sdk_clients_access).returns(nil)
      @request.stubs(:ssl?).returns(true)
      warden_stub.any_instance.stubs(:params).returns({})
      @sdk_access_token = "test_access_token_for_sdk"
      sdk_jwt_presenter = {
        "authentication" => {
          "access_token" => @sdk_access_token
        },
        "access_token" => @sdk_access_token
      }
      warden_stub.any_instance.stubs(:winning_strategy).returns(stub(redirect_at_end?: false, present: sdk_jwt_presenter))
    end

    it "shoulds return json with access_token" do
      warden.expects(:authenticate!).with(:sdk_jwt)
      post :sdk_jwt
      assert_response 200
      assert_equal @sdk_access_token, JSON(response.body)["authentication"]["access_token"]
      # Testing backwards compatibility
      assert_equal @sdk_access_token, JSON(response.body)["access_token"]
    end

    params = [
      :mobile_sdk_limit_medium,
      "mobile_sdk_access_sdk_jwt",
      :enable_throttle_for_mobile_sdk_access_sdk_jwt,
      :mobile_sdk_access_sdk_jwt_emergency_limit
    ]

    it_throttles_mobile_sdk_requests *params do
      post :sdk_jwt
    end
  end

  describe "#sdk_anonymous" do
    before do
      logout
      accept :json
      @controller.stubs(:allow_only_mobile_sdk_clients_access).returns(nil)
      @request.stubs(:ssl?).returns(true)
      warden_stub.any_instance.stubs(:params).returns({})
      @sdk_access_token = "test_access_token_for_sdk"
      sdk_anonymous_presenter = {
        "authentication" => {
          "access_token" => @sdk_access_token
        }
      }
      warden_stub.any_instance.stubs(:winning_strategy).returns(stub(redirect_at_end?: false, present: sdk_anonymous_presenter))
    end

    it "returns a 409 when dealing with a dupe request" do
      # KeyConstraintViolation now requires an argument
      warden.expects(:authenticate!).
        with(:sdk_anonymous).
        raises(ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, 'message')
      post :sdk_anonymous
      assert_response 409
    end

    it "returns a X when dealing with an invalid request" do
      bad_identity = @account.owner.identities.facebook.new
      refute_predicate bad_identity, :valid?
      warden.expects(:authenticate!).with(:sdk_anonymous).raises(ActiveRecord::RecordInvalid.new(bad_identity))
      post :sdk_anonymous
      assert_response 422
      assert_equal "Validation failed: Value:  cannot be blank", response.body
    end

    it "returns json with access_token" do
      warden.expects(:authenticate!).with(:sdk_anonymous)
      post :sdk_anonymous
      assert_response 200
      assert_equal @sdk_access_token, JSON(response.body)["authentication"]["access_token"]
    end

    params = [
      :mobile_sdk_limit_medium,
      "mobile_sdk_access_sdk_anonymous",
      :enable_throttle_for_mobile_sdk_access_sdk_anonymous,
      :mobile_sdk_access_sdk_anonymous_emergency_limit
    ]

    it_throttles_mobile_sdk_requests *params do
      post :sdk_anonymous
    end
  end

  describe "#oauth_mobile_login" do
    before do
      logout
      Zendesk::Accounts::Source.stubs(:zendesk_app_user_agent?).returns(true)
      @request.stubs(:ssl?).returns(true)
      @user = users(:minimum_end_user)
      @client = FactoryBot.create(:client, account: @account,
                                  user: @user)
    end

    it "authenticates user with native mobile strategy" do
      redirect_uri = @client.redirect_uri.first
      user_params = { email: @user.email, password: '123456' }
      warden.expects(:authenticate!).with(:native_mobile)
      post :oauth_mobile_login, params: { client_id: @client.id, redirect_uri: redirect_uri, user: user_params, native_mobile: true }
    end

    it "authenticates user with native mobile strategy via JSON" do
      redirect_uri = @client.redirect_uri.first
      user_params = { email: @user.email, password: '123456' }
      warden.expects(:authenticate!).with(:native_mobile)
      expected_data = { "authentication" => {
        "username" => "user@example.com", "access_token" => "98765resdfghj8765"
      }}
      warden.winning_strategy.stubs(:present).returns(expected_data)
      response = post :oauth_mobile_login, params: { client_id: @client.id.to_s, redirect_uri: redirect_uri, user: user_params, native_mobile: true, format: 'json' }
      assert_response 200
      assert_equal expected_data, JSON(response.body)
    end
  end

  describe "#google_play" do
    before do
      logout
      Zendesk::Accounts::Source.stubs(:zendesk_app_user_agent?).returns(true)
      @request.stubs(:ssl?).returns(true)
      @user = users(:minimum_agent)
      @client = FactoryBot.create(:client, account: @account, user: @user)
    end

    it "authenticates user" do
      warden.expects(:authenticate!).with(:google_play)
      expected_data = { "authentication" => {
        "username" => "user@example.com", "access_token" => "98765resdfghj8765"
      }}
      warden.winning_strategy.stubs(:present).returns(expected_data)
      response = post :google_play, params: { email: @user.email, google_auth_token: 'lmkjnhgfderr5t67y8ui', format: 'json' }
      assert_response 200
      assert_equal expected_data, JSON(response.body)
    end
  end

  describe "#google_play_jwt" do
    before do
      logout
      Zendesk::Accounts::Source.stubs(:zendesk_app_user_agent?).returns(true)
      @request.stubs(:ssl?).returns(true)
      @user = users(:minimum_agent)
      @client = FactoryBot.create(:client, account: @account, user: @user)
    end

    it "authenticates user" do
      warden.expects(:authenticate!).with(:google_play_jwt)
      expected_data = { "authentication" => {
        "username" => "user@example.com", "access_token" => "98765resdfghj8765"
      }}
      warden.winning_strategy.stubs(:present).returns(expected_data)
      response = post :google_play_jwt, params: { email: @user.email, auth_token: 'lmkjnhgfderr5t67y8ui', format: 'json' }
      assert_response 200
      assert_equal expected_data, JSON(response.body)
    end
  end

  describe '#office_365' do
    before do
      logout
      @request.stubs(:ssl?).returns(true)
      warden.expects(:authenticate!).with(:office)
    end

    it "authenticates the user and redirects them to access" do
      # This interfaces with zendesk_auth. We just want to make sure we
      # call perform_auth with the right strategy
      get :office_365
      assert_response :redirect
      assert_redirected_to 'https://minimum.zendesk-test.com/access'
    end
  end

  describe '#google' do
    before do
      logout
      @request.stubs(:ssl?).returns(true)
      warden.expects(:authenticate!).with(:google_o_auth2)
    end

    describe 'current_account is a G Suite Marketplace account' do
      before do
        @gam_domain = 'foo.com'
        @account.route.update_attribute(:gam_domain, @gam_domain)
        @account.stubs(:role_settings).returns(role_settings(:minimum))
        @account.role_settings.update_attribute(:agent_google_login, true)
        warden.winning_strategy.stubs(google_token: 'access_token')
      end

      it "stores google token in session" do
        get :google
        assert_response :redirect
        assert_equal 'access_token', session['google_token']
      end

      describe 'current_user is not an end-user and authenticated via google' do
        before do
          @controller.stubs(:current_user).returns(@account.owner)
          Zendesk::AuthenticatedSession.any_instance.stubs(via: 'google_o_auth2')
        end

        describe 'with an email address not containing G Suite domain' do
          before do
            @account.owner.stubs(email: "foo@gmail.com")
          end

          it "lets gmail users continue without requesting more scopes" do
            get :google
            assert_response :redirect

            location = @response.headers["Location"]
            refute_match 'gam_universal_navigation_link', location
          end
        end

        describe 'with an email address not containing G Suite domain' do
          before do
            @user_email = "bar@#{@gam_domain}"
            @account.owner.stubs(email: @user_email)
          end

          it 'redirects to universal navigation link with login hint to request more scopes' do
            get :google
            assert_response :redirect

            location = @response.headers["Location"]
            params = Rack::Utils.parse_nested_query(location.split('?', 2).last)
            location.must_include 'gam_universal_navigation_link'
            assert_equal @user_email, params['login_hint']
          end
        end
      end
    end

    describe 'current_account is not a G Suite Marketplace account' do
      it "authenticates user with google" do
        get :google
        assert_response :redirect
      end
    end
  end

  describe '#gam' do
    let(:access_token) { 'access_token' }

    before do
      logout
      @request.stubs(:ssl?).returns(true)
    end

    describe 'G Suite enabled accounts' do
      before do
        @account.enable_google_apps!('googledomain.com')
        @controller.stubs(:current_user).returns users(:minimum_admin)
        session['enabled_google_apps'] = true
        @return_to = '/return/to'
        get :gam, params: { denied: true, return_to: @return_to }
      end

      it "will disable G Suite and redirect to return_to" do
        @account.reload
        assert @account.role_settings.agent_zendesk_login
        refute @account.role_settings.agent_google_login
        assert_nil @account.route.gam_domain
        assert_nil @account.settings.google_apps_domain
        refute @account.settings.has_google_apps
        refute @account.settings.has_google_apps_admin
        refute @account.settings.switched_to_google_apps
        assert_response :redirect
      end
    end

    describe 'authenticates successfully' do
      before do
        warden.expects(:authenticate!).with(:gam_o_auth2)
      end

      describe 'nil warden strategy' do
        before do
          warden_stub.any_instance.unstub(:winning_strategy)
          gam_token = Zendesk::Auth::GAMToken.new(
            account: @account, id_token: 'id_token', access_token: access_token,
            information: 'information'
          )
          get :gam, params: { token: gam_token.encrypt }
        end

        it "decodes the GAM token and redirects" do
          assert_response :redirect
        end

        it "stores access token in session" do
          assert_equal 'access_token', session['google_token']
        end
      end

      describe 'warden with winning strategy' do
        before do
          gam_token_stub = stub(claims: {access_token: access_token})
          warden.winning_strategy.stubs(gam_token: gam_token_stub)
          get :gam
        end

        it "authenticates user and redirects" do
          assert_response :redirect
        end

        it "stores access token in session" do
          assert_equal 'access_token', session['google_token']
        end
      end
    end
  end
end
