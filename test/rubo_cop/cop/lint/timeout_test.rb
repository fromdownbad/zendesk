require_relative '../../../support/test_helper'
require_relative '../../../support/rubocop/timeout'
require_relative './lint_test_helper'

SingleCov.covered! file: 'test/support/rubocop/timeout.rb'

describe RuboCop::Cop::Lint::Timeout do
  include RuboCop::Cop::Lint::LintTestHelper
  let(:cop) { RuboCop::Cop::Lint::Timeout.new }

  def assert_timeout(source)
    inspect_source source

    assert_equal 1, cop.offenses.size
    offense = cop.offenses.first
    assert_equal 1, offense.line
    assert_equal Range.new(0, source.length, true), offense.column_range
    assert_equal 'Calls to \'timeout\' are strongly discouraged.  They are' \
        ' very dangerous.', offense.message
  end

  describe 'calls to bare `timeout`' do
    describe 'passing in integer' do
      it 'finds the exception' do
        assert_timeout('timeout(1)')
      end
    end

    describe 'passing in float' do
      it 'finds the exception' do
        assert_timeout('timeout(1.1)')
      end
    end

    describe 'passing in a string' do
      it 'finds the exception' do
        assert_timeout('timeout("tomorrow")')
      end
    end
  end

  describe 'calls to `Timeout.timeout`' do
    describe 'passing in integer' do
      it 'finds the exception' do
        assert_timeout('Timeout.timeout(1)')
      end
    end

    describe 'passing in float' do
      it 'finds the exception' do
        assert_timeout('Timeout.timeout(1.1)')
      end
    end

    describe 'passing in string' do
      it 'finds the exception' do
        assert_timeout('Timeout.timeout("tomorrow")')
      end
    end
  end
end
