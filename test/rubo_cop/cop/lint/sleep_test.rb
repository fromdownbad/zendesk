require_relative '../../../support/test_helper'
require_relative '../../../support/rubocop/sleep'
require_relative './lint_test_helper'

SingleCov.covered! file: 'test/support/rubocop/sleep.rb'

describe RuboCop::Cop::Lint::Sleep do
  include RuboCop::Cop::Lint::LintTestHelper
  let(:cop) { RuboCop::Cop::Lint::Sleep.new }

  def assert_sleep(source, expected_corrected)
    inspect_source source

    assert_equal 1, cop.offenses.size
    offense = cop.offenses.first
    assert_equal 1, offense.line
    assert_equal Range.new(0, source.length, true), offense.column_range
    assert_equal 'Calls to \'sleep\' are discouraged in tests, use Timecop instead', offense.message

    assert_equal expected_corrected, autocorrect_source(source)
  end

  describe 'calls to bare `sleep`' do
    describe 'passing in integer' do
      it 'finds the exception' do
        assert_sleep('sleep(1)', 'Timecop.travel(Time.now + 1)')
      end
    end

    describe 'passing in float' do
      it 'finds the exception' do
        assert_sleep('sleep(1.1)', 'Timecop.travel(Time.now + 1.1)')
      end
    end

    describe 'passing in a string' do
      it 'finds the exception' do
        assert_sleep('sleep("tomorrow")', 'Timecop.travel(Time.now + "tomorrow")')
      end
    end
  end

  describe 'calls to `Kernel.sleep`' do
    describe 'passing in integer' do
      it 'finds the exception' do
        assert_sleep('Kernel.sleep(1)', 'Timecop.travel(Time.now + 1)')
      end
    end

    describe 'passing in float' do
      it 'finds the exception' do
        assert_sleep('Kernel.sleep(1.1)', 'Timecop.travel(Time.now + 1.1)')
      end
    end

    describe 'passing in string' do
      it 'finds the exception' do
        assert_sleep('Kernel.sleep("tomorrow")', 'Timecop.travel(Time.now + "tomorrow")')
      end
    end
  end
end
