require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Sharing::AgreementsHelper do
  it 'humanize_status should map statuses it knows about' do
    assert_equal('Rejected', humanize_status(:declined))
  end

  it 'permissions_string' do
    agreement = Sharing::Agreement.new

    agreement.sync_tags = true
    agreement.sync_custom_fields = true
    agreement.allows_public_comments = true
    actual = permissions_string(agreement)
    assert_equal('Public comments allowed. Sync status and share tags', actual)

    agreement.sync_tags = false
    agreement.sync_custom_fields = false
    agreement.allows_public_comments = true
    actual = permissions_string(agreement)
    assert_equal("Public comments allowed. Sync status", actual)

    agreement.sync_tags = false
    agreement.sync_custom_fields = false
    agreement.allows_public_comments = false
    actual = permissions_string(agreement)
    assert_equal("Private comments only. Don't sync status", actual)

    agreement.sync_tags = true
    agreement.sync_custom_fields = true
    agreement.allows_public_comments = false
    actual = permissions_string(agreement)
    assert_equal("Private comments only. Don't sync status, do share tags", actual)
  end
end
