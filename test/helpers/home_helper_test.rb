require_relative "../support/test_helper"

SingleCov.covered! uncovered: 3

describe HomeHelper do
  fixtures :all

  attr_accessor :current_account, :current_user

  before do
    @current_account = accounts(:minimum)
  end

  describe 'Given: Anybody can submit tickets' do
    before { @current_account.is_open = true }

    describe 'and an anonymous user' do
      before { @current_user = User.new }

      describe '#new_request_link' do
        it 'links to new anonymous request' do
          assert_match(/anonymous/, new_request_link)
        end
      end
    end

    describe 'and a logged in user' do
      before { @current_user = users(:minimum_end_user) }

      describe '#new_request_link' do
        it 'links to new request' do
          assert_no_match(/anonymous/, new_request_link)
        end
      end
    end
  end

  describe 'Given: Anybody can NOT submit tickets' do
    before { @current_account.is_open = false }

    describe 'and an anonymous user' do
      before { @current_user = User.new }

      describe '#new_request_link' do
        it 'links to new request' do
          assert_no_match(/anonymous/, new_request_link)
        end
      end
    end

    describe 'and a logged in user' do
      before { @current_user = users(:minimum_end_user) }

      describe '#new_request_link' do
        it 'links to new request' do
          assert_no_match(/anonymous/, new_request_link)
        end
      end
    end
  end

  describe 'introductory text' do
    before do
      @current_user = users(:minimum_end_user)
      @current_account.introductory_text = "{{dc.welcome_wombat}}"
    end

    it 'renders dynamic content correctly when viewing page' do
      Zendesk::Liquid::DcContext.any_instance.expects(:render).returns("Welcome to Wombat Customer Service!")
      account_introductory_text.must_include "Welcome to Wombat Customer Service"
    end

    it 'shows unrendered liquid content when editing page' do
      Zendesk::Liquid::DcContext.any_instance.expects(:render).never
      account_introductory_text(true).must_include "{{dc.welcome_wombat}}"
    end

    it 'renders preset introductory text if not set with edit_mode true' do
      @current_account.introductory_text = nil
      I18n.expects(:t).with("txt.default.introductory_text.paragraph1", anything).returns("paragraph1")
      I18n.expects(:t).with("txt.default.introductory_text.paragraph2", anything).returns("paragraph2")
      assert_equal "paragraph1<br />paragraph2", account_introductory_text(true)
    end

    it 'renders preset introductory text if not set with edit_mode false' do
      @current_account.introductory_text = nil
      I18n.expects(:t).with("txt.default.introductory_text.paragraph1", anything).returns("paragraph1")
      I18n.expects(:t).with("txt.default.introductory_text.paragraph2", anything).returns("paragraph2")
      assert_equal "paragraph1<br />paragraph2", account_introductory_text(false)
    end
  end

  describe 'introductory title' do
    before do
      @current_user = users(:minimum_end_user)
      @current_account.introductory_title = "{{dc.wombat_title}}"
    end

    it 'renders dynamic content correctly when viewing page' do
      Zendesk::Liquid::DcContext.any_instance.expects(:render).returns("Yo Dawg!")
      account_introductory_title.must_include "Yo Dawg"
    end

    it 'shows unrendered liquid content when editing page' do
      Zendesk::Liquid::DcContext.any_instance.expects(:render).never
      account_introductory_title(true).must_include "{{dc.wombat_title}}"
    end

    it 'renders preset introductory title if not set' do
      @current_account.introductory_title = nil
      I18n.expects(:t).with("txt.default.introductory_text.title", anything).returns("Title")
      assert_equal "Title", account_introductory_title
    end
  end
end
