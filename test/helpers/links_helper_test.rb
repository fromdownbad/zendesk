require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe LinksHelper do
  include LinksHelper, RulesHelper

  fixtures :users, :accounts

  def current_user;
    users(:minimum_admin);
  end

  def current_account;
    current_user.account;
  end

  describe "#render_personal_views" do
    it "is HTML safe" do
      assert render_personal_views.html_safe?
    end

    describe "when no views" do
      before do
        current_user.personal_views.delete_all
        views = current_user.personal_views.select(&:is_active?)
        assert_equal 0, views.size

        render plain: render_personal_views
      end

      it "does not render anything" do
        assert_equal '', @rendered
      end
    end

    describe "when less than maximum" do
      before do
        current_user.views.delete_all
        current_user.views.create!(view_attrs)
        current_user.views.create!(view_attrs)

        views = current_user.personal_views.select(&:is_active?)
        assert_equal 2, views.size

        render plain: render_personal_views
      end

      it "renders one link per view" do
        assert_select 'li' do
          assert_select 'a', 2
        end
      end
    end

    describe "when more than maximum" do
      before do
        @max = current_account.max_private_views_for_display
        (@max + 2).times { current_user.views.create!(view_attrs) }

        views = current_user.personal_views.select(&:is_active?)
        assert views.size > @max

        render plain: render_personal_views
      end

      it "renders max links and a more link" do
        assert_select 'li' do
          assert_select 'a', @max + 1
        end
      end
    end

    describe "system dynamic content in the view title" do
      before do
        current_user.views.delete_all
        current_user.views.create!(view_attrs)
        view = current_user.personal_views.last
        view.update_attribute(:title, '{{zd.your_unsolved_tickets}}')

        render plain: render_personal_views
      end

      it "renders the dynamic content" do
        @rendered.must_match(/#{I18n.t('txt.default.views.your_unsolved.title')}/)
      end
    end
  end

  describe "#render_shared_views" do
    it "is HTML safe" do
      assert render_shared_views.html_safe?
    end

    describe "when less than maximum" do
      before do
        current_account.views.delete_all
        current_account.suspended_tickets.delete_all

        current_account.views.create!(view_attrs)
        current_account.views.create!(view_attrs)

        views = current_account.shared_views.select(&:is_active?)
        assert_equal 2, views.size

        render plain: render_shared_views
      end

      it "renders one link per view" do
        assert_select 'li' do
          assert_select 'a', 2
        end
      end
    end

    describe "when more than maximum" do
      before do
        current_account.suspended_tickets.delete_all

        @max = current_account.max_views_for_display
        (@max + 2).times { current_account.views.create!(view_attrs) }

        views = current_user.shared_views.select(&:is_active?)
        assert views.size > @max

        render plain: render_shared_views
      end

      it "renders max links and a more link" do
        assert_select 'li' do
          assert_select 'a', @max + 1
        end
      end
    end

    describe "when there are suspended tickets" do
      before do
        render plain: render_shared_views
      end

      it "renders link to suspended tickets once" do
        assert_select 'li' do
          assert_select "a[href=\"#{main_app.suspended_tickets_path}\"]", 1
        end
      end
    end

    describe "system dynamic content in the view title" do
      before do
        current_account.views.delete_all
        current_account.views.create!(view_attrs)
        view = current_user.shared_views.last
        view.update_attribute(:title, '{{zd.your_unsolved_tickets}}')

        render plain: render_shared_views
      end

      it "renders the dynamic content" do
        @rendered.must_match(/#{I18n.t('txt.default.views.your_unsolved.title')}/)
      end
    end
  end

  private

  def view_attrs
    definition = Definition.new
    definition.conditions_all.push DefinitionItem.new('priority_id', "is", PriorityType.HIGH.to_s)
    definition.conditions_all.push DefinitionItem.new('status_id', 'less_than', StatusType.Pending.to_s)

    {
      is_active: true,
      position: 999,
      definition: definition,
      title: "Test view #{rand(2 << 32)}",
      output: Output.create(columns: View::COLUMNS)
    }
  end
end
