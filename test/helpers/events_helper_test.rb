require_relative "../support/test_helper"

SingleCov.covered! uncovered: 68

describe EventsHelper do
  include UsersHelper
  include EntriesHelper

  fixtures :events, :tickets, :users, :addresses, :accounts

  def current_user;
    users(:minimum_admin);
  end

  def current_account;
    accounts(:minimum);
  end

  def can?(*args);
    current_user.can?(*args);
  end

  def request
    stub(format: stub(mobile_v2?: false))
  end

  describe "#should_show_comment_privacy_link?" do
    before { current_user.stubs(:is_admin?).returns(false) }

    describe "for non-Comments" do
      before { @event = Change.new }
      it "returns false" do
        refute should_show_comment_privacy_link?(@event)
      end
    end

    describe "for Comments" do
      before { @event = Comment.new(is_public: true) }

      describe "when the current_user is the author of the comment and an agent" do
        before do
          current_user.stubs(:is_admin?).returns(false)
          current_user.stubs(:is_agent?).returns(true)
        end

        it "returns true" do
          @event.expects(:author_id).returns(current_user.id)
          assert should_show_comment_privacy_link?(@event)
        end

        describe "and the comment is already private" do
          before do
            @event.expects(:is_public?).returns(false)
            @event.expects(:author_id).never
          end
          it("return false") { refute should_show_comment_privacy_link?(@event) }
        end

        describe "when the comment is the first on the ticket" do
          before do
            @event.expects(:author_id).never
          end
          it("return false") { refute should_show_comment_privacy_link?(@event, true) }
        end
      end

      describe "when the current_user is the author of the comment and not an agent" do
        before do
          current_user.stubs(:is_agent?).returns(false)
          @event.stubs(:author).returns(current_user) # Not actually called.
        end
        it("return false") { refute should_show_comment_privacy_link?(@event) }
      end

      describe "when the current_user is not the author of the comment and an agent" do
        before do
          current_user.stubs(:is_agent?).returns(true)
          current_user.stubs(:is_admin?).returns(false)
          @event.expects(:author_id).returns(users(:minimum_agent).id)
        end
        it("return false") { refute should_show_comment_privacy_link?(@event) }
      end
    end
  end

  describe "#describe_if_authorized" do
    let(:audit) { events(:create_audit_for_minimum_ticket_1) }

    describe "public comment" do
      it "contains the 'say public' class" do
        comment = events(:create_comment_for_minimum_ticket_1)
        expects(:comment_class).with(comment).returns('say public')
        result = describe_if_authorized(comment)
        result.must_include "say public"
      end
    end

    describe "events caused by ticket creation" do
      it "is properly html escaped" do
        event = events(:create_create_for_miniumum_ticket_1)
        result = describe_if_authorized(event)
        assert result.html_safe?
        event = events(:create_change_for_minimum_ticket_1)
        result = describe_if_authorized(event)
        assert result.html_safe?
      end
    end

    it "renders AttachmentRedactionEvent" do
      stubs(can?: true)
      result = describe_if_authorized AttachmentRedactionEvent.new(audit: audit, attachment_id: 123)
      assert_includes result, "Attachment id: 123 redacted"
    end

    it "renders CommentRedactionEvent" do
      stubs(can?: true)
      result = describe_if_authorized CommentRedactionEvent.new(audit: audit, comment_id: 123)
      assert_includes result, "Comment id: 123 redacted"
    end

    describe "render ChannelBackEvent" do
      before do
        stubs(can?: true)
        ChannelBackEvent.any_instance.expects(:initiate_channels_conversion)
        @event = ChannelBackEvent.create!(
          ticket: audit.ticket,
          audit: audit,
          value: {
            requester: { screen_name: "abc" },
            source: { screen_name: "def" }
          }
        )
      end

      [ViaType.TWITTER, ViaType.TWITTER_FAVORITE].each do |via_id|
        it "renders correct I18n for Tweets with via_id #{via_id}" do
          audit.ticket.update_column(:via_id, ViaType.TWITTER)
          result = describe_if_authorized @event
          assert result.html_safe?
          assert_equal "<li class=\"tweet\">Tweet back to @abc from @def (##{@event.id})</li>", result
        end
      end

      it "renders correct I18n for Twitter DM" do
        audit.ticket.update_column(:via_id, ViaType.TWITTER_DM)
        result = describe_if_authorized @event
        assert result.html_safe?
        assert_equal "<li class=\"tweet\">DM via twitter to @abc (##{@event.id})</li>", result
      end

      it "renders just the link for Facebook event" do
        audit.ticket.update_column(:via_id, ViaType.FACEBOOK_POST)
        result = describe_if_authorized @event
        assert result.html_safe?
        assert_equal "<li class=\"tweet\">##{@event.id}</li>", result
      end
    end
  end

  describe "#comment_class" do
    before { @comment = events(:create_comment_for_minimum_ticket_1) }

    describe "with public comment by agent" do
      it "returns the 'say public agent' class" do
        assert_equal 'say agent public', comment_class(@comment)
      end

      it "returns the 'say public agent email' class for a comment via email" do
        @comment.via_id = 4;
        assert_equal 'say agent public email', comment_class(@comment)
      end

      it "returns the 'say public agent email' class for a comment via voicemail" do
        @comment.via_id = 33;
        assert_equal 'say agent public voicemail', comment_class(@comment)
      end
    end

    describe "with public comment by end user" do
      it "contains the 'say public' class" do
        @comment.stubs(:author).returns(users(:minimum_author))
        assert_equal 'say public', comment_class(@comment)
      end
    end

    describe "with private comment" do
      it "returns the 'say private' class" do
        @comment.stubs(:is_public?).returns(false)
        assert_equal 'say agent private', comment_class(@comment)
      end
    end
  end

  describe "#comment_value_as_html" do
    before do
      @comment = Comment.new(account: accounts(:minimum))
      Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(false)
    end

    it "escapes the Comment's string representation and call present on it with rel='nofollow'." do
      @comment.body = "A search engine: <www.google.com> #123"
      assert_equal '<div class="zd-comment" dir="auto"><p>A search engine: &lt;<a href="http://www.google.com" target="_blank" rel="nofollow noreferrer">www.google.com</a>&gt; <a href="/agent/tickets/123" target="_blank" rel="ticket">#123</a></p></div>', comment_value_as_html(@comment)
    end
  end

  describe "#comment_value_as_html for voice comment" do
    before do
      @comment = VoiceComment.new
      Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(false)
    end

    it "escapes the Comment's string representation and call present on it." do
      expects(:render_to_string).with(partial: "/events/voice_comment", locals: {event: @comment}).returns('foo')
      assert_equal 'foo', comment_value_as_html(@comment)
    end
  end

  describe "#render_comment_value" do
    before do
      @comment = Comment.new(account: accounts(:minimum), body: 'foo')
      @comment.account.stubs(subscription: stub(has_individual_language_selection?: true))
      Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(false)
    end

    it "returns the html body with options." do
      @comment.expects(:html_body).with(length: 800, default_value: 'Empty comment', for_agent: true).returns('foo')
      assert_equal 'foo', render_comment_value(@comment, length: 800)
    end
  end

  describe "#render_comment_value for voice comment" do
    before { @ticket = tickets(:minimum_1) }

    it "returns the ticket subject if via_id doesn't belong to Voice" do
      comment = VoiceComment.new ticket: @ticket, account: @ticket.account

      assert_equal "<p>#{@ticket.subject}</p>", render_comment_value(comment)
    end

    it 'returns a translated value based on the via_id if it belongs to Voice' do
      VoiceComment::VOICE_VIA_TYPES.each do |via_type|
        comment = VoiceComment.new ticket: @ticket, via_id: via_type, data: {}

        assert_equal "<p>#{comment.translated_subject}</p>", render_comment_value(comment)
      end
    end
  end

  describe "#render_comment_value for empty comment" do
    before { @comment = Comment.new(body: 'foo') }

    it "returns empty comment." do
      @comment.expects(:html_body).with(length: 800, default_value: 'Empty comment', for_agent: true).returns(nil)
      assert_equal 'Empty comment', render_comment_value(@comment, length: 800)
    end
  end

  describe "#user_badge_for_ticket" do
    describe "for facebook wall post tickets" do
      before { @ticket = tickets(:minimum_1) }

      describe "type is comment" do
        before do
          value_reference = { type: "comment", requester: { name: "Um Teste", id: "456" }, content: "with pic from um", via_zendesk: false }
          @comment = FacebookComment.new(account: @ticket.account, is_public: true, value_reference: value_reference, via_id: ViaType.FACEBOOK_POST)
        end

        it "returns requester user badge" do
          assert user_badge_for_ticket(@ticket, @comment).index("http://facebook.com/456")
          assert user_badge_for_ticket(@ticket, @comment).index("Um Teste")
        end
      end

      describe "type is photo" do
        before do
          requester       = Hashie::Mash.new(id: "456", name: "Um Teste")
          content         = "Um Teste posted a photo: https://www.facebook.com/photo.php?fbid=123&set=p.123&type=1"
          value_reference = { type: "photo", link: "https://www.facebook.com/photo.php?fbid=123&set=p.123&type=1", requester: requester, content: content, via_zendesk: false }
          @comment = FacebookComment.new(account: @ticket.account, is_public: true, value_reference: value_reference, via_id: ViaType.FACEBOOK_POST)
        end

        it "returns requester user badge" do
          assert user_badge_for_ticket(@ticket, @comment).index("http://facebook.com/456")
          assert user_badge_for_ticket(@ticket, @comment).index("Um Teste")
        end
      end
    end

    describe "for twickets" do
      before do
        twitter_user_id = '1'
        mth = MonitoredTwitterHandle.new(twitter_user_id: twitter_user_id, account: current_account)

        url = "https://api.twitter.com/1/users/show.json?user_id=#{twitter_user_id}"
        body = read_test_file('twitter_users_show_153594297_1.1.json')
        stub_request(:get, url).to_return(body: body)

        @ticket = tickets(:minimum_1)
        @ticket.stubs(:twitter_ticket_source).returns(mth)
      end

      it "returns a MTH user badge for comments" do
        comment = @ticket.comments.first
        comment.via_id = ViaType.TWITTER
        comment.author_id = User.system_user_id
        comment.value_reference = 10000
        assert user_badge_for_ticket(@ticket, comment).index("/images/zd_monitored_handle_user.png")
      end

      it "returns a MTH user badge for audits" do
        audit = @ticket.audits.first
        audit.via_id = ViaType.TWITTER
        audit.author_id = User.system_user_id
        audit.comment.value_reference = 10000
        assert user_badge_for_ticket(@ticket, audit).index("/images/zd_monitored_handle_user.png")
      end
    end
  end

  describe "#describe_via" do
    before do
      @archived_ticket = tickets(:minimum_5)
      archive_and_delete(@archived_ticket)
      @ticket = tickets(:minimum_1)
      @ticket2 = @archived_ticket
      Ticket.any_instance.stubs(:status_id).returns(StatusType.ARCHIVED)
      @merge_event = @ticket.events.last
      @merge_event.stubs(:via_id).returns(19)                       # merge
      @merge_event.stubs(:via_reference_id).returns(@ticket2.id)    # archived ticket
    end

    it "finds the archived ticket" do
      assert describe_via(@merge_event)
    end
  end

  describe "#describe_comment" do
    before do
      @comment = Comment.new(author: current_user)
    end

    it "is html_safe?" do
      result = describe_comment(@comment)
      assert result.present?
      assert result.html_safe?
    end

    # this seems broken, just documenting, comment.to_s returns 'Empty comment' not NO_CONTENT
    it "renders nothing for no-content" do
      @comment.body = NO_CONTENT.dup
      assert describe_comment(@comment).blank?
    end

    it "does not render non-existent attachments" do
      expects(:render).returns "\n\n".html_safe
      result = describe_comment(@comment)
      refute_includes result, 'class="attachments"'
      assert result.html_safe?
    end

    it "renders attachments" do
      expects(:render).returns "<b>RENDER</b>".html_safe
      @comment.attachments.build
      result = describe_comment(@comment)
      assert_includes result, 'class="attachments"'
      assert_includes result, "<b>RENDER</b>"
      assert result.html_safe?
    end
  end
end
