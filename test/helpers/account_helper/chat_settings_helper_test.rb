require_relative "../../support/test_helper"

SingleCov.covered!

describe AccountHelper::ChatSettingsHelper do
  describe '#maximum_chat_requests_options' do
    before do
      @opts = maximum_chat_requests_options
    end

    it 'renturns nine number values and an "unlimited" value' do
      (1..9).each do |i|
        assert_equal ["#{i} per agent", i], @opts[i - 1]
      end
      assert_equal ['Unlimited', 1000], @opts.last
    end
  end
end
