require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 7

describe AccountHelper::SubscriptionHelper do
  include BillingHelper

  describe "#boostable?" do
    before do
      @account = Account.new
      @account.subscription = Subscription.new(plan_type: SubscriptionPlanType.Small)
      @account.subscription.stubs(:registered_with_gateway?).returns(true)

      Timecop.freeze(BOOSTING_ON_UNTIL.to_time - 5.days)

      assert boostable?(@account)
    end

    it "returns false if the account is on a plus plan" do
      @account.subscription = Subscription.new(plan_type: SubscriptionPlanType.Large)
      refute boostable?(@account)
    end

    it "returns false if todays date is after BOOSTING_ON_UNTIL" do
      Timecop.freeze(BOOSTING_ON_UNTIL.to_time + 5.days)
      refute boostable?(@account)
    end

    it "returns false if the account is not registered with a gateway" do
      @account.subscription.stubs(:registered_with_gateway?).returns(false)
      refute boostable?(@account)
    end

    it "returns false if the account already has feature boost" do
      @account.build_feature_boost
      refute boostable?(@account)
    end
  end
end
