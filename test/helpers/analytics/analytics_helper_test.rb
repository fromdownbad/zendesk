require_relative "../../support/test_helper"

SingleCov.covered!

describe Analytics::AnalyticsHelper do
  describe "#analytics_columns" do
    it "contains columns" do
      assert_operator analytics_columns.scan("<col").size, :>=, 6
    end

    it "is html_safe" do
      assert analytics_columns.html_safe?
    end
  end

  describe "#analytics_view_table_header" do
    it "contains headers" do
      assert_includes analytics_view_table_header, "<th class"
    end

    it "is html_safe" do
      assert analytics_view_table_header.html_safe?
    end
  end
end
