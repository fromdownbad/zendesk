require_relative "../support/test_helper"

SingleCov.covered!

describe TriggersHelper do
  fixtures :accounts

  attr_accessor :current_account

  before do
    @current_account = accounts(:minimum)
  end

  it 'sharing_agreements_for_trigger_actions' do
    agreement = FactoryBot.create(:agreement, account: current_account,
                                              direction: :out, status: :accepted, name: 'Partner name')

    expected = [{ text: 'Partner name', value: agreement.id }]
    assert_equal(expected, sharing_agreements_for_trigger_actions)
  end
end
