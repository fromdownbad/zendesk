require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe GeneratedAssetsHelper do
  include GeneratedAssetsHelper

  describe "#include_generated_stylesheets" do
    it "returns nil if there's no current_account" do
      stubs(:current_account).returns(nil)
      assert_nil include_generated_stylesheets
    end

    it "returns a relative path if there's a current_account" do
      updated_at = 7.days.ago
      account    = stub(id: 123, updated_at: updated_at)
      stubs(:current_account).returns(account)
      assert_equal include_generated_stylesheets, "<link id='generated_styles' rel='stylesheet' type='text/css' href='/generated/stylesheets/branding/#{account.id / 100}/#{account.id}/#{account.updated_at.to_i}.css' media='screen' />"
    end
  end

  describe "#include_generated_mobile_stylesheets" do
    it "returns nil if there's no current_account" do
      stubs(:current_account).returns(nil)
      assert_nil include_generated_mobile_stylesheets
    end

    it "returns a relative path if there's a current_account" do
      updated_at = 7.days.ago
      account    = stub(id: 123, updated_at: updated_at)
      stubs(:current_account).returns(account)
      assert_equal include_generated_mobile_stylesheets, "<link id='generated_styles' rel='stylesheet' type='text/css' href='/generated/stylesheets/branding/mobile/#{account.id / 100}/#{account.id}/#{account.updated_at.to_i}.css' media='screen' />"
    end
  end

  describe "#include_generated_javascripts" do
    it "returns nil if there's no i18n.locale" do
      I18n.stubs(:translation_locale).returns(nil)
      assert_nil include_generated_javascripts
    end

    it "returns a relative path if there's a current_account" do
      updated_at = 7.days.ago
      locale     = stub('TranslationLocale', id: 123, cache_key: updated_at.to_s)
      I18n.stubs(:translation_locale).returns(locale)
      assert_equal include_generated_javascripts, "<script id='generated_javascript' type='text/javascript' src='/generated/javascripts/locale/#{locale.id / 100}/#{locale.id}/#{locale.cache_key}.js'></script>"
    end
  end
end
