require_relative '../support/test_helper'

SingleCov.covered! uncovered: 16

describe LotusBootstrapHelper do
  include JsModelsHelper

  fixtures :accounts, :users, :translation_locales

  attr_accessor :current_account, :current_user, :request

  before do
    @current_account = accounts(:minimum)
    @current_user = users(:minimum_agent)
    @manager = Lotus::ManifestManager.new(@current_account)
    @request = ActionDispatch::Request.new(Rack::MockRequest.env_for('/agent'))
    @data = {
      'createdAt' => '',
      'css' => [],
      'js' => [
        {
          'name' => 'app.js',
          'path' => '/agent/assets/lr/app.d07c795669440a45.js'
        }
      ],
      'sha' => '',
      'version' => 1234
    }
    @asset_manifest = Lotus::AssetManifest.new(@current_account, @data.to_json)
    @manager.stubs(:find).returns(@asset_manifest)

    stubs(:current_account).returns(@current_account)
    stubs(:manifest_manager).returns(@manager)
    stubs(:asset_manifest).returns(@manager.find(:current))
  end

  describe '#insert_json' do
    let(:presenter) do
      Class.new do
        def initialize(current_user, options)
        end

        def present(data)
          data
        end
      end
    end

    it 'returns JSON' do
      result = insert_json(Api::V2::Users::SettingsPresenter, current_user)
      assert_match(/^\{"/, result)
    end

    it 'sanitizes script tags' do
      settings = {settings: {lotus: {text: "<script>console.log('xss problem:macro name')</script>"}}}
      Api::V2::Users::SettingsPresenter.any_instance.stubs(:present).returns(settings)
      result = insert_json(Api::V2::Users::SettingsPresenter, current_user)
      result.must_include "<\\/script>"
    end

    it 'escapes HTML comments' do
      data = { omg: 'Hello <!-- comment --> World' }
      result = insert_json(presenter, data)
      assert_no_match(/<!--/, result)
      assert_no_match(/-->/, result)
      result.must_include "&lt;!--"
      result.must_include "--&gt;"
    end

    it 'accepts overwriting data' do
      api_url = 'https://support.zendesk-test.com/api/v2/tickets/1/comments.json?page=2'
      bootstrap_url = 'https://support.zendesk-test.com/agent/tickets/1/comments.json'
      data = { next_page: bootstrap_url }
      result = insert_json(presenter, data, overwrite: { next_page: api_url })
      assert_match(/api\/v2/, result)
    end
  end

  describe '#static_asset_host' do
    before do
      ENV['ZENDESK_STATIC_ASSETS_DOMAIN'] = 'https://static.zdassets.com'
      ENV['ZENDESK_FALLBACK_STATIC_ASSETS_DOMAIN'] = 'https://static-fallback.zdassets.com'
    end

    it 'returns ZENDESK_FALLBACK_STATIC_ASSETS_DOMAIN if fallback_cdn is not set' do
      assert_equal static_asset_host, ENV['ZENDESK_STATIC_ASSETS_DOMAIN']
    end

    it 'returns ZENDESK_FALLBACK_STATIC_ASSETS_DOMAIN if fallback_cdn is set' do
      request.stubs(:params).returns(fallback_cdn: '')
      assert_equal static_asset_host, ENV['ZENDESK_FALLBACK_STATIC_ASSETS_DOMAIN']
    end
  end

  describe '#static_cdn_provider' do
    it 'returns default cdn provider if fallback_cdn is not set' do
      assert_equal static_cdn_provider, current_account.settings.cdn_provider
    end

    it 'returns static-cloudfront if fallback_cdn is set' do
      request.stubs(:params).returns(fallback_cdn: '')
      assert_equal static_cdn_provider, 'static-cloudfront'
    end
  end

  describe '#requested_manifest_version_information' do
    before do
      @current_account = accounts(:minimum)
    end

    it 'is json parseable' do
      assert JSON.parse(requested_manifest_version_information)
    end

    it 'is html_safe' do
      assert requested_manifest_version_information.html_safe?
    end
  end

  describe '#javascript_asset_tag' do
    it 'appends a necessary extension' do
      assert_match /application\.js/, javascript_asset_tag('application')
    end

    it 'is html_safe' do
      assert javascript_asset_tag('application.js').html_safe?
    end

    it 'adds fatal error call if an asset is falling back' do
      request.stubs(:params).returns(fallback_cdn: 'cloudfront')
      assert_match /Zd.onFatalError/, javascript_asset_tag('application')
    end
  end

  describe '#pci_credit_card_javascript_include_tag' do
    it 'returns nothing without the PCI feature' do
      assert_nil pci_credit_card_javascript_include_tag
    end

    it 'returns a relative path without an asset_host' do
      Account.any_instance.stubs(has_pci_credit_card_custom_field?: true)
      assert_equal '<script type="text/javascript" src="/assets/zendesk_pci_lotus.v2.js" defer="defer"></script>', pci_credit_card_javascript_include_tag
    end

    describe 'with an asset host' do
      def asset_host
        'https://p1.zdassets.com'
      end

      it 'returns an absolute path with an asset_host' do
        Account.any_instance.stubs(has_pci_credit_card_custom_field?: true)
        assert_equal '<script type="text/javascript" src="https://p1.zdassets.com/assets/zendesk_pci_lotus.v2.js" defer="defer"></script>', pci_credit_card_javascript_include_tag
      end
    end
  end

  describe '#fallback_cdn_name' do
    it 'returns cloudfront if account set to default CDN' do
      assert_equal 'cloudfront', fallback_cdn_name
    end

    it 'returns cloudfront if account set to edgecast' do
      assert_equal 'cloudfront', fallback_cdn_name
    end

    it 'returns edgecast if account set to cloudfront' do
      current_account.settings.cdn_provider = 'cloudfront'
      assert_equal 'edgecast', fallback_cdn_name
    end
  end

  describe '#display_fallback_form?' do
    it 'returns true if fallback_cdn is empty' do
      request.stubs(:params).
        returns(fallback_cdn: '')

      assert display_fallback_form?
    end

    it 'returns true when fallback_cdn is set and asset is translation' do
      request.stubs(:params).
        returns(fallback_cdn: 'cloudfront').then.
        returns(fallback_cdn: 'cloudfront').then.
        returns(fallback_asset_name: 'translations')

      assert display_fallback_form?
    end
  end

  describe '#translations_asset_tag' do
    def is_ie? # rubocop:disable Naming/PredicateName
      false
    end

    describe_with_arturo_enabled :translations_from_cdn do
      it 'generates a script tag pointing to rosetta' do
        tag = translations_asset_tag

        tag.wont_include '/>'
        tag.must_include '</script>'
        tag.must_match(/src=".*?\/api\/v2\/locales/)
      end

      it 'sets crossorigin to anonymous on the script tag' do
        translations_asset_tag.must_include 'crossorigin="anonymous"'
      end

      describe 'with an asset host' do
        before do
          Arturo.enable_feature!(:cdn_fastly)
          config.stubs(asset_host: Zendesk::AccountCdn.asset_host)
        end

        it 'generates a script tag pointing to the CDN' do
          translations_asset_tag.must_match(/src="#{asset_host}\/api\/v2\/locales/)
        end
      end

      it 'adds fatal error call if an asset is falling back' do
        request.stubs(:params).returns(translations_asset_fallback_to_api: true)
        assert_match /Zd.onFatalError/, translations_asset_tag
      end

      it 'adds script load error call if asset is falling back' do
        request.stubs(:params).returns(translations_asset_fallback_to_api: nil)
        assert_match /Zendesk.onScriptLoadError/, javascript_asset_tag('application')
      end

      describe 'rosetta api fallback' do
        it 'generates a script tag pointing to rosetta' do
          request.stubs(:params).returns(force_api: 'translation')
          translations_asset_tag.must_match(/src=".*?\/api\/v2\/locales/)
        end
      end
    end
  end

  describe '#translations_api_uri' do
    describe 'when the current user has no locale set' do
      before do
        @current_user.locale_id = nil
        @current_user.save!
      end

      it 'uses the accounts locale' do
        account_locale = @current_user.account.translation_locale.locale
        translations_api_uri.must_include "api/v2/locales/#{account_locale}.json"
      end
    end

    describe 'when the current user has a locale set' do
      let(:spanish) { translation_locales(:spanish) }

      before do
        @current_user.locale_id = spanish.id
        @current_user.save!
      end

      it 'wont use /current.json for locales' do
        translations_api_uri.wont_include 'current.json'
        translations_api_uri.must_include "api/v2/locales/#{spanish.locale}.json"
      end
    end
  end

  describe '#rollbar_access_token' do
    config = {
      ROLLBAR_US_LOTUS_ACCESS_TOKEN: 'usa12345',
      ROLLBAR_EU_LOTUS_ACCESS_TOKEN: 'eu123456',
      ROLLBAR_LOTUS_ACCESS_TOKEN: 'global12'
    }

    with_env config.merge(ROLLBAR_REGION: 'US') do
      it 'returns the us access token' do
        assert_equal ENV['ROLLBAR_US_LOTUS_ACCESS_TOKEN'], rollbar_access_token
      end
    end

    with_env config.merge(ROLLBAR_REGION: 'EU') do
      it 'returns the eu access token' do
        assert_equal ENV['ROLLBAR_EU_LOTUS_ACCESS_TOKEN'], rollbar_access_token
      end
    end

    with_env config.merge(ROLLBAR_REGION: 'UNKNOWN') do
      it 'returns a generic access token' do
        assert_equal ENV['ROLLBAR_LOTUS_ACCESS_TOKEN'], rollbar_access_token
      end

      with_env config.merge(ROLLBAR_LOTUS_ACCESS_TOKEN: nil) do
        it 'still provides a string' do
          assert_equal '', rollbar_access_token
        end
      end
    end
  end

  describe '#auto_translation?' do
    describe 'non-Agent Workspace account' do
      before do
        Account.any_instance.stubs(:has_polaris?).returns(false)
      end

      describe_with_arturo_enabled :lotus_feature_auto_translation do
        it 'returns false' do
          assert_equal false, auto_translation?
        end
      end

      describe_with_arturo_disabled :lotus_feature_auto_translation do
        it 'returns false' do
          assert_equal false, auto_translation?
        end
      end
    end

    describe 'Agent Workspace account' do
      before do
        Account.any_instance.stubs(:has_polaris?).returns(true)
      end

      describe_with_arturo_enabled :lotus_feature_auto_translation do
        it 'returns true' do
          assert auto_translation?
        end
      end

      describe_with_arturo_disabled :lotus_feature_auto_translation do
        it 'returns false' do
          assert_equal false, auto_translation?
        end
      end
    end
  end

  describe '#auto_translation_languages' do
    let(:japanese) { translation_locales(:japanese) }

    before do
      @current_user.locale_id = japanese.id
      @current_user.save!
    end

    it 'does not return an empty list' do
      languages = auto_translation_languages

      assert_operator languages.length, :>, 0
    end

    it 'returns the list in locale of the current user' do
      languages = auto_translation_languages

      assert_equal({ code: 'is', name: 'アイスランド語' }, languages.first)
    end
  end
end
