require_relative "../support/test_helper"

SingleCov.covered! uncovered: 8

describe TargetsHelper do
  fixtures :accounts
  attr_reader :request

  let(:account) { accounts(:minimum) }

  before { stubs(current_account: account) }

  describe "#authorize_url" do
    before do
      @request = ActionController::TestRequest.create
      @target = TwitterTarget.new
      @target.callback_url = "http://example.com"
      request_token = mock(authorize_url: @target.send(:consumer).authorize_url + "?oauth_token=<token>")
      OAuth::Consumer.any_instance.expects(:get_request_token).returns(request_token)
    end

    it "provides authorize link" do
      assert_equal "https://api.twitter.com/oauth/authorize?oauth_token=<token>", authorize_url
    end
  end

  describe "#target_name_translated" do
    it "returns the translated target name" do
      name = "url_target"
      I18n.expects(:t).with("txt.admin.views.targets.select_target_to_add.name_of_target_#{name}")
      target_name_translated(name)
    end
  end

  describe "#show_target_failures?" do
    describe "when the account has url_target_v2_failures enabled" do
      before { current_account.stubs(has_targets_v2_failures?: true) }

      describe "when the target is a v2 target" do
        before { @target = UrlTargetV2.new }

        describe "when the target has not been saved" do
          describe "when the target has a failure" do
            before { @target.stubs(url_target_failures: stub(exists?: true)) }

            it "returns false" do
              refute show_target_failures?
            end
          end

          describe "when the target does not have a failure" do
            before { @target.stubs(url_target_failures: stub(exists?: false)) }

            it "returns false" do
              refute show_target_failures?
            end
          end
        end

        describe "when the target has been saved" do
          before { @target.stubs(new_record?: false) }

          describe "when the target has a failure" do
            before { @target.stubs(url_target_failures: stub(exists?: true)) }

            it "returns true" do
              assert show_target_failures?
            end
          end

          describe "when the target does not have a failure" do
            before { @target.stubs(url_target_failures: stub(exists?: false)) }

            it "returns false" do
              refute show_target_failures?
            end
          end
        end
      end

      describe "when the target is not a v2 target" do
        before { @target = UrlTarget.new }

        describe "when the target has not been saved" do
          describe "when the target has a failure" do
            before { @target.stubs(url_target_failures: stub(exists?: true)) }

            it "returns false" do
              refute show_target_failures?
            end
          end

          describe "when the target does not have a failure" do
            before { @target.stubs(url_target_failures: stub(exists?: false)) }

            it "returns false" do
              refute show_target_failures?
            end
          end
        end

        describe "when the target has been saved" do
          before { @target.stubs(new_record?: false) }

          describe "when the target has a failure" do
            before { @target.stubs(url_target_failures: stub(exists?: true)) }

            it "returns false" do
              refute show_target_failures?
            end
          end

          describe "when the target does not have a failure" do
            before { @target.stubs(url_target_failures: stub(exists?: false)) }

            it "returns false" do
              refute show_target_failures?
            end
          end
        end
      end
    end

    describe "when the  does not have url_target_v2_failure enabled" do
      before { current_account.stubs(has_targets_v2_failures?: false) }

      describe "when the target is a v2 target" do
        before { @target = UrlTargetV2.new }

        describe "when the target has not been saved" do
          it "returns false" do
            refute show_target_failures?
          end
        end

        describe "when the target has been saved" do
          before { @target.stubs(new_record?: false) }

          describe "when the target has a failure" do
            before { @target.stubs(url_target_failures: stub(exists?: true)) }

            it "returns false" do
              refute show_target_failures?
            end
          end

          describe "when the target does not have a failure" do
            before { @target.stubs(url_target_failures: stub(exists?: false)) }

            it "returns false" do
              refute show_target_failures?
            end
          end
        end
      end

      describe "when the target is not a v2 target" do
        before { @target = UrlTarget.new }

        describe "when the target has not been saved" do
          it "returns false" do
            refute show_target_failures?
          end
        end

        describe "when the target has been saved" do
          before { @target.stubs(new_record?: false) }

          it "returns false" do
            refute show_target_failures?
          end
        end
      end
    end
  end

  describe "#password_placeholder" do
    before { @target = UrlTarget.new }

    describe "when the target has a password" do
      before do
        @target.encrypted_password = 'test'
        @target.encrypt!
      end

      it "returns a palceholder the size of the password" do
        assert_equal '••••', password_placeholder
      end
    end

    describe "when the target password is nil" do
      before do
        @target.encrypted_password = nil
        @target.encrypt!
      end

      it "returns an empty string" do
        assert_equal '', password_placeholder
      end
    end

    describe "when the target password is empty" do
      before do
        @target.encrypted_password = ''
        @target.encrypt!
      end

      it "returns an empty string" do
        assert_equal '', password_placeholder
      end
    end
  end
end
