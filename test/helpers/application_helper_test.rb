require_relative "../support/test_helper"

SingleCov.covered! uncovered: 38

describe ApplicationHelper do
  include ApplicationHelper

  attr_reader :request, :current_user, :current_account

  fixtures :accounts, :users, :entries, :forums, :categories

  before do
    @current_user = users(:minimum_agent)
    @current_account = @current_user.account
    stubs(:user_portal_state).returns(Zendesk::UserPortalState.new(@current_account))
  end

  describe "#render_canonical_link" do
    before do
      @current_user = users(:minimum_end_user)
    end

    describe "when action is :show" do
      before do
        stubs(:action_name).returns("show")
      end

      describe "and controller is EntriesController" do
        before do
          stubs(:controller_name).returns("entries")
          @entry = entries(:ponies)
        end

        it "returns the <link rel='canonical' ... html" do
          stubs(:params).returns(id: "#{@entry.id}-boelle-bob")
          assert_equal %(<link href="https://minimum.zendesk-test.com/entries/#{@entry.id}-An-entry-title-for-ponies" rel="canonical"></link>), render_canonical_link
        end
      end

      describe "and controller is ForumsController" do
        before do
          stubs(:controller_name).returns("forums")
          @forum = forums(:announcements)
        end

        it "returns the <link rel='canonical' ... html" do
          assert_equal %(<link href="https://minimum.zendesk-test.com/forums/#{@forum.id}-Announcements" rel="canonical"></link>), render_canonical_link
        end
      end

      describe "and controller is CategoriesController" do
        before do
          stubs(:controller_name).returns("categories")
          @category = categories(:minimum_category)
        end

        it "returns the <link rel='canonical' ... html" do
          assert_equal %(<link href="https://minimum.zendesk-test.com/categories/#{@category.id}-Hello-Category" rel="canonical"></link>), render_canonical_link
        end
      end
    end

    describe "when action is unauthenticated and controller is AccessController" do
      before do
        stubs(:controller_name).returns("access")
        stubs(:action_name).returns("unauthenticated")
      end

      it "returns the <link rel='canonical' ... html" do
        assert_equal %(<link href="https://minimum.zendesk-test.com/access/unauthenticated" rel="canonical"></link>), render_canonical_link
      end

      describe "when account is nil" do
        before { @current_account = nil }

        it "does not blow up!" do
          render_canonical_link
        end
      end
    end
  end

  describe "#render_to_string" do
    it 'returns rendered partial as string' do
      render_to_string(partial: 'tags/autocomplete').must_equal "<ul></ul>\n"
    end
  end

  describe "#available_languages" do
    before do
      Language            = Struct.new(:id, :name)
      user_language       = Language.new(1, 'Danish')
      @available_language = Language.new(2, 'French')

      @current_user = users(:minimum_end_user)
      @current_user.stubs(:available_languages).returns([user_language, @available_language])
      @current_user.stubs(:translation_locale).returns(user_language)
    end

    it "returns a list of available languages minus the current one" do
      assert_equal [@available_language], available_languages
    end
  end

  describe "#encode_meta_tag_content" do
    it "encodes single and double quotes" do
      assert_equal "0;url=http://attacker.com&quot; HTTP-EQUIV=&quot;refresh&quot; foo=&quot;",
        encode_meta_tag_content("0;url=http://attacker.com\" HTTP-EQUIV=\"refresh\" foo=\"")
    end

    it "handles bad content" do
      assert_equal "Bad���� charså∆ƒ", encode_meta_tag_content("Bad\xEF\xEF\xEF\xEF charså∆ƒ".force_encoding("UTF-8"))
    end
  end

  describe "#css_truncate" do
    it "creates div with truncate class and set title attr and inner html to content" do
      assert_equal "<div class=\"truncate\" title=\"Hello World\">Hello World</div>",
        css_truncate("Hello World")
    end
  end

  describe "#form_for" do
    it "produces put methods since patch is not supported by 1-off routes we added" do
      form_for(users(:minimum_agent)) {}.must_include '<input type="hidden" name="_method" value="put" />'
    end
  end

  describe "#render_data_rtl_attribute" do
    it "returns a data attribute if the appropriate arturo is enabled" do
      Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(true)
      assert_includes render_data_rtl_attribute, "data-rtl-language"
    end

    it "returns a data attribute set to true if the translation locale is RTL" do
      TranslationLocale.any_instance.stubs(:rtl?).returns(true)
      assert_equal render_data_rtl_attribute, "data-rtl-language=true"
    end

    it "returns a data attribute set to false if the translation locale is not RTL" do
      TranslationLocale.any_instance.stubs(:rtl?).returns(false)
      assert_equal render_data_rtl_attribute, "data-rtl-language=false"
    end

    it "returns an empty string if the appropriate arturo is not enabled" do
      Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(false)
      assert_equal render_data_rtl_attribute, ""
    end
  end

  describe "#seat_types" do
    let(:actual) { seat_types(types) }

    describe "when types are absent" do
      let(:types) { nil }

      it "returns an empty string" do
        assert_equal "", actual
      end
    end

    describe "when types are present" do
      let(:types) { [:chat, :support] }

      it "returns the correct string" do
        assert_equal "Chat, Support", actual
      end
    end
  end
end
