require_relative "../support/test_helper"

SingleCov.covered! uncovered: 16

describe FormHelper do
  fixtures :ticket_fields, :custom_field_options

  before do
    @form = ActionView::Base.default_form_builder.new(:test, nil, self, {})
    @template = @form.instance_variable_get(:@template)
  end

  describe "field tagger selects" do
    subject { ticket_fields(:field_tagger_custom) }

    describe "static_nested_select" do
      it "adds span to template" do
        @template.expects(:content_tag).with('span', "[no selection]")
        @form.static_nested_select(subject)
      end
    end

    describe "reverse_truncate" do
      it "reverses truncate long strings" do
        assert_equal "...ting", @form.reverse_truncate("abctesting", 7)
      end

      it "does not truncate short strings" do
        assert_equal "test", @form.reverse_truncate("test", 7)
      end

      it "does not truncate exact strings" do
        assert_equal "testing", @form.reverse_truncate("testing", 7)
      end
    end

    describe "build_nested_select" do
      it "creates unordered list" do
        Zendesk::Utils::NestedCategoryBuilder.any_instance.expects(:render).returns("STUB")

        @template.expects(:content_tag).with('ul', 'STUB', instance_of(Hash))
        @form.build_nested_select(subject)
      end

      let(:current_account) { ticket_fields(:field_tagger_custom).account }
      let(:current_user) { ticket_fields(:field_tagger_custom).account }

      before do
        fallback_attributes = { is_fallback: true, nested: true, value: "Welcome Wombat", translation_locale_id: 1 }
        @text = Cms::Text.create!(name: "welcome_wombat", fallback_attributes: fallback_attributes, account: current_account)
      end

      describe "when passing in a dc_cache" do
        it "does not query the db for cms_texts or cms_variants" do
          dc_cache = {@text.identifier => @text.fallback.value}

          queries = sql_queries do
            form = @form.build_nested_select(
              subject,
              current_account: current_account,
              current_user: current_user,
              dc_cache: dc_cache
            )
            assert_match /<li class=\"link\" data-value=\"dc_content\" data-full-title=\"Welcome Wombat\">Welcome Wombat<\/li>/, form
          end

          queries.join("\n").wont_include "cms_texts"
          queries.join("\n").wont_include "cms_variants"
        end
      end

      describe "when passing in an empty dc_cache" do
        it "querys the db for cms_texts or cms_variants" do
          dc_cache = {}

          queries = sql_queries do
            form = @form.build_nested_select(
              subject,
              current_account: current_account,
              current_user: current_user,
              dc_cache: dc_cache
            )
            assert_match /<li class=\"link\" data-value=\"dc_content\" data-full-title=\"Welcome Wombat\">Welcome Wombat<\/li>/, form
          end

          queries.join("\n").must_include "cms_texts"
          queries.join("\n").must_include "cms_variants"
        end
      end
    end

    describe "tagger_nested_select" do
      before do
        Zendesk::Utils::NestedCategoryBuilder.any_instance.expects(:render).returns("STUB")
        @output = @form.tagger_nested_select(subject)
      end

      it "includeds nested select" do
        assert @output.include?("STUB")
      end
    end

    describe "nested_select_value" do
      it "returns value if none is selected" do
        assert_equal "[no selection]", @form.nested_select_value(subject, {})
      end

      it "returns value if cannot find custom field option" do
        assert_equal "-", @form.nested_select_value(subject, selected: "nosuchvalue")
      end
    end

    describe "with custom field options" do
      before do
        @option = custom_field_options(:field_tagger_custom_option_1)
      end

      describe "static_nested_select" do
        it "adds span to template" do
          @template.expects(:content_tag).with('span', @option.name)
          @form.static_nested_select(subject, selected: @option.value)
        end
      end

      describe "build_nested_select" do
        before do
          Zendesk::Utils::NestedCategoryBuilder.any_instance.expects(:render).returns("STUB")
        end

        it "creates unordered list" do
          @template.expects(:content_tag).with('ul', 'STUB', instance_of(Hash))
          @form.build_nested_select(subject)
        end
      end

      describe "nested_select_value" do
        it "returns value if none is selected" do
          assert_equal "[no selection]", @form.nested_select_value(subject, {})
        end

        it "returns value if cannot find custom field option" do
          assert_equal "-", @form.nested_select_value(subject, selected: "nosuchvalue")
        end

        it "returns option name if selected" do
          assert_equal @option.name, @form.nested_select_value(subject, selected: @option.value)
        end
      end
    end

    describe "with mapped custom field options" do
      before do
        subject.custom_field_options.loaded
        subject.custom_field_options.target.replace([
                                                      { "name" => "testing#{CATEGORY_DELIMITER}abc", "value" => "test" }
                                                    ])
      end

      describe "static_nested_select" do
        it "adds span to template" do
          @template.expects(:content_tag).with('span', "testing#{CATEGORY_DELIMITER}abc")
          @form.static_nested_select(subject, selected: "test")
        end
      end

      describe "build_nested_select" do
        before do
          Zendesk::Utils::NestedCategoryBuilder.any_instance.expects(:render).returns("STUB")
        end

        it "creates unordered list" do
          @template.expects(:content_tag).with('ul', 'STUB', instance_of(Hash))
          @form.build_nested_select(subject)
        end
      end

      describe "nested_select_value" do
        it "returns value if none is selected" do
          assert_equal "[no selection]", @form.nested_select_value(subject, {})
        end

        it "returns value if cannot find custom field option" do
          assert_equal "-", @form.nested_select_value(subject, selected: "nosuchvalue")
        end

        it "returns option name if selected" do
          assert_equal "testing#{CATEGORY_DELIMITER}abc", @form.nested_select_value(subject, selected: "test")
        end
      end
    end
  end
end
