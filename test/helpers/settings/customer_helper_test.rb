require_relative "../../support/test_helper"

SingleCov.covered!

describe Settings::CustomerHelper do
  fixtures :accounts
  let(:current_account) { accounts(:minimum) }

  before do
    Satisfaction::Reason.create_system_reasons_for(current_account)
  end

  describe "#render_reason_codes" do
    it "renders a list of reason codes" do
      assert_includes render_reason_codes, "<li>No reason provided</li><li>The issue took too long to resolve</li><li>The issue was not resolved</li>"
    end
  end

  describe "#satisfaction_reasons" do
    it "returns reasons" do
      first_reason = satisfaction_reasons(current_account).first

      refute_nil first_reason[:id]
      refute_nil first_reason[:translated_value]
      refute_nil first_reason[:active]
    end
  end

  describe "#satisfaction_reasons_vendor_css" do
    before do
      mock_asset_manifest = mock
      mock_asset_manifest.stubs(:initial_css_assets).returns([{ "name" => "vendor", "path" => "vendor"}])
      Lotus::ManifestManager.any_instance.stubs(:find).with(:current).returns(mock_asset_manifest)
    end

    it "returns the vendor.css file" do
      assert_equal "vendor", satisfaction_reasons_vendor_css(current_account)
    end
  end

  describe "#satisfaction_prediction_setting_content" do
    let (:content) { Nokogiri.HTML(satisfaction_prediction_setting_content(text: "hello", class: "warning")) }

    it "should link to help center article" do
      assert_equal I18n.t('txt.admin.views.settings.customers._satisfaction.prediction_learn_more_article_link_1'), content.css('a').first["href"]
    end

    it "should have a link text learn more" do
      assert_equal "hello Learn more", content.text
    end

    it "should have a target parent attribute(so the link can be opened in a new tab)" do
      assert_equal '_blank', content.css('a').attribute('target').value
    end
  end

  describe "#satisfaction_prediction_feature_available?" do
    it "should be true for enterprise customers" do
      Arturo.stubs(:feature_enabled_for?).returns(true)
      assert satisfaction_prediction_feature_available?(current_account)
    end
  end

  describe "#satisfaction_prediction_model_available?" do
    it 'should be true if model for the account is available' do
      TicketPrediction.stubs(:satisfaction_prediction_model_available?).returns(true)
      assert satisfaction_prediction_model_available?(current_account)
    end
  end

  describe "#satisfaction_prediction_allowed_for_subscription?" do
    it "should be true for enterprise customers" do
      current_account.subscription.stubs(:has_satisfaction_prediction?).returns(true)
      assert satisfaction_prediction_allowed_for_subscription?(current_account)
    end
  end

  describe '#satisfaction_prediction_checkbox_options' do
    describe 'when satisfaction prediction is not available' do
      before do
        TicketPrediction.stubs(:satisfaction_prediction_model_available?).returns(true)
        current_account.subscription.stubs(:has_satisfaction_prediction?).returns(false)
      end

      it 'should not have the checkbox checked' do
        refute satisfaction_prediction_checkbox_options(current_account)[:checked]
      end

      it 'should set the checkbox to be disabled' do
        assert satisfaction_prediction_checkbox_options(current_account)[:disabled]
      end
    end

    describe 'when satisfaction prediction is available' do
      before do
        TicketPrediction.stubs(:satisfaction_prediction_model_available?).returns(true)
        current_account.subscription.stubs(:has_satisfaction_prediction?).returns(true)
      end

      it 'should not have the checkbox checked' do
        assert satisfaction_prediction_checkbox_options(current_account)[:checked]
      end

      it 'should not set the checkbox to be disabled' do
        refute satisfaction_prediction_checkbox_options(current_account)[:disabled]
      end
    end
  end

  describe "#satisfaction_prediction_checkbox_enabled?" do
    it "should be true when satisfaction prediction is allowed for subscription and model is available" do
      TicketPrediction.stubs(:satisfaction_prediction_model_available?).returns(true)
      current_account.subscription.stubs(:has_satisfaction_prediction?).returns(true)
      assert satisfaction_prediction_checkbox_enabled?(current_account)
    end

    it "should be false when satisfaction prediction is not allowed for subscription, or model is unavailable" do
      TicketPrediction.stubs(:satisfaction_prediction_model_available?).returns(true)
      current_account.subscription.stubs(:has_satisfaction_prediction?).returns(false)
      refute satisfaction_prediction_checkbox_enabled?(current_account)

      TicketPrediction.stubs(:satisfaction_prediction_model_available?).returns(false)
      current_account.subscription.stubs(:has_satisfaction_prediction?).returns(true)
      refute satisfaction_prediction_checkbox_enabled?(current_account)
    end
  end
end
