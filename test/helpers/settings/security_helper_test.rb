require_relative "../../support/test_helper"
require_relative "../../support/certificate_test_helper"

SingleCov.covered! uncovered: 4

describe Settings::SecurityHelper do
  include Settings::SecurityHelper
  include CertificateTestHelper

  fixtures :accounts

  let(:current_account) { accounts(:minimum) }

  describe "#lets_encrypt_status?" do
    it "is nil without a status" do
      assert_nil lets_encrypt_status
    end

    it "fails without a certificate" do
      current_account.acme_certificate_job_statuses.create!(status: 'completed')
      assert_equal :failed, lets_encrypt_status
    end

    it "is pending when the job is working" do
      current_account.acme_certificate_job_statuses.create!(status: 'working')
      assert_equal :pending, lets_encrypt_status
    end

    describe "with a certificate" do
      before do
        create_certificate!(state: 'active', create_ip: false, account: current_account)
        @certificate = current_account.certificates.active.first
        @certificate.autoprovisioned = true
        @certificate.save!
      end

      it "fails when the job failed" do
        current_account.acme_certificate_job_statuses.create!(status: 'failed')
        assert_equal :failed, lets_encrypt_status
      end

      it "succeeds when lets encrypt is properly setup" do
        current_account.acme_certificate_job_statuses.create!(status: 'completed')
        assert_equal :success, lets_encrypt_status
      end
    end
  end

  describe "#active_cert_is_activating?" do
    let(:active_cert) { current_account.certificates.active.first }

    before do
      create_certificate!(state: 'active', create_ip: false, account: current_account)
    end

    describe "without a certificate" do
      let(:active_cert) { nil }

      it "is false" do
        assert_equal false, active_cert_is_activating?
      end
    end

    it "is true when the certificate is 14 minutes old" do
      active_cert.created_at = Time.now - 14.minutes
      assert(active_cert_is_activating?)
    end

    it "is false when the certificate is 16 minutes old" do
      active_cert.created_at = Time.now - 16.minutes
      assert_equal false, active_cert_is_activating?
    end
  end

  describe "#sso_bypass_options" do
    describe 'current account has sso disabled' do
      before do
        current_account.settings.sso_bypass_can_be_disabled = false
      end

      it 'does not include the disabled option' do
        result = sso_bypass_options
        assert_equal 2, result.size
        assert_equal RoleSettings::REMOTE_BYPASS_ACCOUNT_OWNER, result[0][1]
      end
    end

    describe 'current account has sso enabled' do
      before do
        current_account.settings.sso_bypass_can_be_disabled = true
      end

      it 'includes the disabled option' do
        result = sso_bypass_options
        assert_equal 3, result.size
        assert_equal RoleSettings::REMOTE_BYPASS_DISABLED, result[0][1]
      end
    end
  end

  describe '#sso_bypass_link' do
    it "returns the correct address" do
      assert_equal "<a target=\"_blank\" href=\"https://minimum.zendesk-test.com/access/sso_bypass\">https://minimum.zendesk-test.com/access/sso_bypass</a>", sso_bypass_link
    end
  end
end
