require_relative "../../support/test_helper"

SingleCov.covered!

describe Settings::SpamPreventionHelper do
  describe "#spam_prevention_options" do
    let(:subscription) { stub('Subscription') }
    let(:account) { stub('Account', subscription: subscription) }

    it "renders options with correct data-toggles" do
      subscription.stubs(:with_compulsory_spam_prevention?).returns(false)

      expected = {"data-toggles" => "#sub_setting_spam_prevention_settings"}
      assert_equal expected, spam_prevention_options(account)
    end

    it "renders options with disabled when account has compulsory spam prevention" do
      subscription.stubs(:with_compulsory_spam_prevention?).returns(true)

      expected = {
        "data-toggles" => "#sub_setting_spam_prevention_settings",
        "disabled" => "disabled"
      }
      assert_equal expected, spam_prevention_options(account)
    end
  end
end
