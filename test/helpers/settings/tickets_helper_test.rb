require_relative "../../support/test_helper"

SingleCov.covered!

describe Settings::TicketsHelper do
  fixtures :accounts
  let(:current_account) { accounts(:minimum) }

  describe "#display_first_comment_private_setting?" do
    describe "when FCP setting is disabled" do
      before { current_account.stubs(:is_first_comment_private_enabled?).returns(false) }

      it "returns true" do
        assert display_fcp_setting?
      end
    end

    describe "when FCP setting is enabled" do
      let(:date_before_rollout) { Account::FIRST_COMMENT_PRIVATE_ROLLOUT_DATE - 2.days }
      let(:date_after_rollout) { Account::FIRST_COMMENT_PRIVATE_ROLLOUT_DATE + 2.days }

      before { current_account.stubs(:is_first_comment_private_enabled?).returns(true) }

      describe "and current_account was created before FCP rollout date" do
        before { current_account.stubs(:created_at).returns(date_before_rollout) }

        it "returns true" do
          assert display_fcp_setting?
        end
      end

      describe "and current_account was created after FCP rollout date" do
        before { current_account.stubs(:created_at).returns(date_after_rollout) }

        it "returns false if the account is not a sandbox" do
          current_account.stubs(:is_sandbox?).returns(false)
          refute display_fcp_setting?
        end

        describe "and current_account is a sandbox" do
          before { current_account.stubs(:is_sandbox?).returns(true) }

          describe "and master account was created before FCP rollout date" do
            before { current_account.stubs(:sandbox_master).returns(stub(created_at: date_before_rollout)) }

            it "returns true" do
              assert display_fcp_setting?
            end
          end

          describe "and master account was created after FCP rollout date" do
            before { current_account.stubs(:sandbox_master).returns(stub(created_at: date_after_rollout)) }

            it "returns false" do
              refute display_fcp_setting?
            end
          end
        end

        describe "and current_account is a spoke" do
          before { current_account.stubs(:spoke?).returns(true) }

          describe "and hub account was created before FCP rollout date" do
            before { current_account.subscription.stubs(:hub_account).returns(stub(created_at: date_before_rollout)) }

            it "returns true" do
              assert display_fcp_setting?
            end
          end

          describe "and master account was created after FCP rollout date" do
            before { current_account.subscription.stubs(:hub_account).returns(stub(created_at: date_after_rollout)) }

            it "returns false" do
              refute display_fcp_setting?
            end
          end
        end
      end
    end
  end
end
