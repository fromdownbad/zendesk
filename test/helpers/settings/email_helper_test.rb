require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 11

describe Settings::EmailHelper do
  fixtures :accounts, :brands, :recipient_addresses

  let(:account) { accounts(:minimum) }
  let(:current_account) { account }
  let(:recipient_address) { RecipientAddress.new { |r| r.account = account; r.id = 123 } }
  let(:credential) { ExternalEmailCredential.new }

  describe "#gmail_error_detail" do
    it "does not render ok" do
      assert_nil gmail_error_detail(credential)
    end

    describe "with errors" do
      before { credential.error_count = 1 }

      it "renders know even if it just happened once for fast feedback" do
        credential.last_error_message = "invalid_grant"
        assert_equal "We can't connect to your Gmail account because your OAuth credentials are invalid. Please reconnect.", gmail_error_detail(credential)
      end

      it "renders folder conflict" do
        credential.last_error_message = "Folder name conflicts with existing folder name"
        assert_includes gmail_error_detail(credential), "delete the label"
      end

      it "renders imap disabled" do
        credential.last_error_message = "IMAP access is disabled for your domain"
        assert_includes gmail_error_detail(credential), "IMAP has been disabled"
      end

      it "renders too many emails" do
        credential.last_error_message = "Too many emails"
        assert_includes gmail_error_detail(credential), "emails in a short time"
      end

      it "renders over limit" do
        credential.last_error_message = "Account exceeded command or bandwidth limits"
        assert_includes gmail_error_detail(credential), "too many connection"
      end

      it "renders rate limited" do
        credential.last_error_message = 'Zendesk::MailFetcherAPIResponse::RateLimitedError: Too many requests'
        assert_includes gmail_error_detail(credential), "too many connection"
      end

      it "renders bad request" do
        credential.last_error_message = 'Zendesk::MailFetcherAPIResponse::BadRequestError: Token has been expired or revoked'
        assert_equal "We can't connect to your Gmail account because your OAuth credentials are invalid. Please reconnect.", gmail_error_detail(credential)
      end

      it "renders unauthorized request" do
        credential.last_error_message = 'Zendesk::MailFetcherAPIResponse::UnauthorizedError: Invalid access token'
        assert_equal "We can't connect to your Gmail account because your OAuth credentials are invalid. Please reconnect.", gmail_error_detail(credential)
      end

      describe "with unknown error" do
        before { credential.last_error_message = "Fooo" }

        it "does not render unknown if it could still be randomness" do
          assert_nil gmail_error_detail(credential)
        end

        it "renders unknown if it is persistent" do
          credential.error_count = 7
          assert_equal "We can't connect to your Gmail account because of an unknown error.", gmail_error_detail(credential)
        end
      end
    end
  end

  describe "#recipient_address_verifications" do
    # Rendered HTML includes 8 checks and accompanying icons:
    # - Forwarding Check
    # - SPF Check
    # - Overall DNS status
    #   - 4 CNAME records [Optional for success. Failure means warning for DNS status]
    #   - Domain verification record [Required for success of DNS status]

    let(:html) { recipient_address_verifications(recipient_address) }
    let(:text) { html&.strip_tags&.strip }
    let(:status) { html&.scan(/status-icon-header.+?zd-svg-icon-[0-9]+-([a-z0-9-]+)/)&.flatten }

    before do
      recipient_address.spf_status_id = RecipientAddress::SPF_STATUS.fetch(:verified)
      recipient_address.cname_status_id = RecipientAddress::SPF_STATUS.fetch(:verified)
      recipient_address.metadata = {"cname": {"zendesk1": {"lookup_result": "mail1.zendesk-test.com", "status": "verified"}, "zendesk2": {"lookup_result": "mail2.zendesk-test.com", "status": "verified"}, "zendesk3": {"lookup_result": "mail3.zendesk-test.com", "status": "verified"}, "zendesk4": {"lookup_result": "mail4.zendesk-test.com", "status": "verified"}}, "domain_verification": {"status": "verified"}}
      recipient_address.forwarding_verified_at = Time.now
    end

    it "renders verified" do
      assert_equal ["checkmark-circle"] * 3, status, html
    end

    describe_with_arturo_disabled :email_deprecate_cname_checks do
      it "renders warning if an optional field is invalid" do
        recipient_address.metadata["cname"]["zendesk1"]["status"] = "failed"
        assert_equal ["checkmark-circle", "checkmark-circle", "warning"], status, html
      end
    end

    it "renders failed if a required field is failed" do
      recipient_address.forwarding_verified_at = nil
      recipient_address.spf_status_id = RecipientAddress::SPF_STATUS.fetch(:deprecated)
      recipient_address.metadata["domain_verification"]["status"] = "failed"
      assert_equal ["error", "warning", "error"], status, html
    end

    it "renders plain for zendesk address" do
      recipient_address.email = "foo@#{account.default_host}"
      assert_equal "Zendesk Support address", text
    end

    it "renders simple warning when unable to send due to inactive brand" do
      recipient_address.brand.active = false
      assert_equal "Invalid Zendesk Support address", text
    end

    it "renders unknown as error" do
      recipient_address.forwarding_verified_at = nil
      recipient_address.spf_status_id = RecipientAddress::SPF_STATUS.fetch(:unknown)
      recipient_address.metadata["domain_verification"]["status"] = ""
      assert_equal ["error", "warning", "error"], status, html
    end

    it "renders abridged results for free email domains" do
      recipient_address.email = "xxx@gmail.com"
      assert_equal ["checkmark-circle"], status, html
    end
  end

  describe "#brands_with_recipient_addresses" do
    let(:default_brand) { account.brands.first }
    let(:active_brand) { FactoryBot.create(:brand, name: "yay brand", active: true) }
    let(:inactive1) { FactoryBot.create(:brand, name: "foo brand", active: false) }
    let(:inactive2) do
      brand = FactoryBot.create(:brand, name: "bar brand", active: false)
      account.recipient_addresses.create(email: "support2@#{brand.route.default_host}", brand: brand)
      brand
    end

    before do
      account.settings.default_brand_id = default_brand.id
      assert inactive1.recipient_addresses.one?
      assert inactive2.recipient_addresses.size == 2
      assert active_brand.recipient_addresses.one?
    end

    describe "in account with multibrand" do
      before { account.stubs(:has_multibrand?).returns(true) }

      describe_with_and_without_arturo_enabled(:email_settings_paginated) do
        it "returns all brands sorted by active state and name" do
          assert_equal 4, brands_with_recipient_addresses.count
          assert_equal [default_brand, active_brand, inactive2, inactive1], brands_with_recipient_addresses
        end
      end
    end

    describe "in account without multibrand" do
      before { account.stubs(:has_multibrand?).returns(false) }

      describe_with_and_without_arturo_enabled(:email_settings_paginated) do
        it "doesn't return inactive brands with only one address" do
          assert_equal 3, brands_with_recipient_addresses.count
          assert_equal [default_brand, active_brand, inactive2], brands_with_recipient_addresses
        end
      end
    end
  end

  describe "#recipient_forwarding_status" do
    let(:html) { recipient_forwarding_status(recipient_address)&.join(" ") }
    before { Timecop.freeze }

    it "renders unknown as failed since this should never happen" do
      assert_includes html, "Forwarding check failed less than a minute ago"
    end

    it "renders waiting" do
      recipient_address.forwarding_sent_at = Time.now
      assert_includes html, "waiting"
      assert_includes html, "refresh_status\" data-id=\"123\" data-type=\"forwarding\" href=\"#\">"
    end

    it "renders verified" do
      recipient_address.forwarding_verified_at = Time.now
      assert_includes html, "verified"
    end

    it "renders verified for default verified" do
      recipient_address.forwarding_verified_at = Time.at(0)
      assert_includes html, "Verified <"
    end

    it "renders failed" do
      recipient_address.forwarding_sent_at = 1.hour.ago
      assert_includes html, "failed"
    end
  end

  describe "#recipient_spf_status" do
    let(:html) { recipient_spf_status(recipient_address)&.join(" ") }
    let(:text) { html.strip_tags.strip }

    it "shows failed for unknown since unknown should no longer happen" do
      assert_includes text, "SPF does not include Zendesk Support"
    end

    it "shows failed" do
      recipient_address.spf_status_id = RecipientAddresses::SpfStatus::SPF_STATUS.fetch(:failed)
      assert_includes text, "SPF does not include Zendesk Support"
    end

    it "shows deprecated" do
      recipient_address.spf_status_id = RecipientAddresses::SpfStatus::SPF_STATUS.fetch(:deprecated)
      assert_includes text, "SPF record needs an update"
    end

    it "shows verified" do
      recipient_address.spf_status_id = RecipientAddresses::SpfStatus::SPF_STATUS.fetch(:verified)
      assert_includes html, "refresh_status\" data-id=\"123\" data-type=\"spf\" href=\"#\">Verify SPF record</a>"
      assert_includes text, "SPF record is valid"
    end

    it "does not show for common email provider since customer cannot add spf" do
      recipient_address.email = "xxx@gmail.com"
      assert_nil html
    end

    describe 'with Gmail address' do
      before { recipient_address.external_email_credential = credential }

      it "shows SPF status when sending via Gmail" do
        assert_includes text, "SPF does not include Zendesk Support"
      end

      it "shows SPF status when not sending via Gmail" do
        account.settings.send_gmail_messages_via_gmail = false
        assert_includes text, "SPF does not include Zendesk Support"
      end
    end
  end

  describe "#recipient_cname_status" do
    let(:html) { recipient_cname_status(recipient_address).try(:last) }
    let(:text) { html.strip_tags.strip }

    it "shows failed for unknown since unknown should no longer happen" do
      assert_equal "CNAME does not include Zendesk Support Retry | Learn more", text
    end

    it "shows failed" do
      recipient_address.cname_status_id = RecipientAddresses::SpfStatus::SPF_STATUS.fetch(:failed)
      assert_equal "CNAME does not include Zendesk Support Retry | Learn more", text
    end

    it "shows verified" do
      recipient_address.cname_status_id = RecipientAddresses::SpfStatus::SPF_STATUS.fetch(:verified)
      assert_includes html, "<a class=\"refresh_status\" data-id=\"123\" data-type=\"dns\" href=\"#\">Retry</a>"
      assert_equal "CNAME record is valid Retry", text
    end

    it "does not show for common email provider since customer cannot add CNAME records" do
      recipient_address.email = "xxx@gmail.com"
      assert_nil html
    end

    it "opens learn more in a new window to also work when iframed in lotus with same-origin policy" do
      recipient_address.cname_status_id = RecipientAddresses::SpfStatus::SPF_STATUS.fetch(:failed)
      assert_match /target="_blank"[^>]*>Learn more/, html
    end

    describe 'with Gmail address' do
      before { recipient_address.external_email_credential = credential }

      it "shows CNAME status when not sending via Gmail" do
        account.settings.send_gmail_messages_via_gmail = false
        assert_equal "CNAME does not include Zendesk Support Retry | Learn more", text
      end

      it "shows CNAME status when sending via Gmail" do
        assert_equal "CNAME does not include Zendesk Support Retry | Learn more", text
      end
    end
  end

  describe "#recipient_dns_status" do
    let(:html) { recipient_dns_status(recipient_address)&.join(" ") }
    let(:text) { html.strip_tags.strip }

    it "shows failed for unknown since unknown should no longer happen" do
      assert_includes text, "DNS records are not set up correctly"
    end

    it "shows failed" do
      recipient_address.metadata = recipient_address.metadata.to_h.merge("domain_verification": {"status" => "failed"})
      assert_includes text, "TXT record is missing for zendeskverification"
    end

    it "shows verified" do
      recipient_address.metadata = recipient_address.metadata.to_h.merge("cname": {"zendesk1": {"lookup_result": "mail1.zendesk-test.com", "status": "verified"}}, "domain_verification": {"status": "verified"})
      assert_match /refresh_status\" data-id=\"123\" data-type=\"dns\" href=\"#\">Verify DNS records<\/a>/, html
      assert_includes html, "CNAME record is valid for <code>zendesk1"
      assert_includes text, "CNAME record is valid for zendesk1"

      assert_includes html, "TXT record is valid for <code>zendeskverification"
      assert_includes text, "TXT record is valid for zendeskverification"
    end

    it "does not show for common email provider since customer cannot add DNS records" do
      recipient_address.email = "xxx@gmail.com"
      assert_nil html
    end

    describe 'with Gmail address' do
      before { recipient_address.external_email_credential = credential }

      it "shows DNS verification when not sending via Gmail" do
        account.settings.send_gmail_messages_via_gmail = false
        assert_includes text, "DNS records are not set up correctly"
      end

      it "shows DNS verification when sending via Gmail" do
        assert_includes text, "DNS records are not set up correctly"
      end
    end
  end

  describe "#recipient_address_actions" do
    let(:account) { accounts(:minimum) }
    let(:recipient_address) { RecipientAddress.new { |r| r.account = account; r.id = 123 } }

    it "renders delete" do
      assert_includes recipient_address_actions(recipient_address), ">delete<"
    end

    it "does not render delete for default" do
      recipient_address.default = true
      refute_includes recipient_address_actions(recipient_address), ">delete<"
    end

    it "does not render delete for backup" do
      recipient_address.email = account.backup_email_address
      refute_includes recipient_address_actions(recipient_address), ">delete<"
    end

    it "renders default exaplanation for default + backup" do
      recipient_address.email = account.backup_email_address
      recipient_address.default = true
      assert_includes recipient_address_actions(recipient_address), ">default<"
    end

    it "renders backup exaplanation for backup" do
      recipient_address.email = account.backup_email_address
      assert_includes recipient_address_actions(recipient_address), ">system<"
    end
  end

  describe "#dns_worst_status" do
    let(:status) { dns_worst_status(recipient_address) }

    describe "when domain is not verified" do
      before { recipient_address.stubs(:domain_verified?).returns(false) }

      it "fails" do
        assert_equal :failed, status
      end
    end

    describe "when domain is verified" do
      before { recipient_address.stubs(:domain_verified?).returns(true) }

      describe "when CNAMEs are valid" do
        before { recipient_address.stubs(:all_cnames_valid?).returns(true) }

        it "is verified" do
          assert_equal :verified, status
        end
      end

      describe "when CNAMEs are not valid" do
        before { recipient_address.stubs(:all_cnames_valid?).returns(false) }

        it "shows a warning" do
          assert_equal :warning, status
        end
      end
    end
  end
end
