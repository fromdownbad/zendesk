require_relative "../../support/test_helper"

SingleCov.covered!

describe Twitter::LinkHelper do
  include Twitter::LinkHelper, AccountsHelper

  fixtures :users, :accounts

  def current_user;
    users(:minimum_admin);
  end

  def current_account;
    current_user.account;
  end

  before do
    @mta = MonitoredTwitterHandle.create!(access_token: "<token>", secret: "<secret>", account: current_account, twitter_user_id: 1)
  end

  describe "#shared_twitter_searches_links" do
    describe "Account#max_twitter_searches_for_display" do
      before do
        20.times do |_i|
          Channels::Twitter::Search.create!(twitter_search_attrs)
        end
      end

      it "limits the number of items rendered" do
        list_maximum = current_account.max_twitter_searches_for_display
        assert twitter_searches_for_account.length > list_maximum
        assert_equal list_maximum, shared_twitter_searches_links.length
      end
    end
  end

  describe "#render_personal_twitter_searches" do
    it "is HTML safe" do
      assert render_personal_twitter_searches.html_safe?
    end
  end

  describe "#render_shared_twitter_searches" do
    it "is HTML safe" do
      assert render_shared_twitter_searches.html_safe?
    end
  end

  describe "#personal_twitter_searches_links" do
    describe "Account#max_private_twitter_searches_for_display" do
      before do
        10.times do |_i|
          Channels::Twitter::Search.create!(twitter_search_attrs.merge(user_id: current_user.id))
        end
      end

      it "limits the number of items rendered" do
        list_maximum = current_account.max_private_twitter_searches_for_display
        assert twitter_searches_for_current_user.length > list_maximum
        assert_equal list_maximum, personal_twitter_searches_links.length
      end
    end
  end

  describe "#link_to_more_twitter_searches" do
    it "returns a link to the twitter saved search index page" do
      assert_match zendesk_channels.twitter_settings_path, link_to_more_twitter_searches
    end
  end

  private

  def twitter_search_attrs
    {
      monitored_twitter_handle: @mta,
      position: 999,
      owner_id: nil,
      name: "Test Twitter search #{rand(2 << 32)}",
      query: "Test query #{rand(2 << 32)}",
      account: current_account
    }
  end
end
