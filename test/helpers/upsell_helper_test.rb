require_relative "../support/test_helper"

SingleCov.covered! uncovered: 6

describe UpsellHelper do
  describe "#should_show_upsell?" do
    it "reflects the feature setting on the subscription" do
      subscription     = stub(has_horses?: true, has_crack?: false)
      @current_account = stub(subscription: subscription)
      refute should_show_upsell?(:horses)
      assert should_show_upsell?(:crack)
    end
  end

  describe '#has_feature?' do
    it 'reflects the feature setting on the subscription' do
      subscription     = stub(has_horses?: true, has_crack?: false)
      @current_account = stub(subscription: subscription)
      assert has_feature?(:horses)
      refute has_feature?(:crack)
    end
  end

  attr_reader :current_account
end
