require_relative "../support/test_helper"
require 'mocha/test_unit'

SingleCov.covered! uncovered: 3

describe SatisfactionRatingsHelper do
  include SatisfactionRatingsHelper

  describe "#right_to_left?" do
    before do
      TranslationLocale.create!(locale: locale, right_to_left: rtl)
      I18n.stubs(:locale).returns(locale)
    end

    describe "when locale is right to left" do
      let (:locale) { 'ar' }
      let (:rtl) { true }

      it "returns true" do
        assert_equal right_to_left?, true
      end
    end

    describe "when locale is left to right" do
      let (:locale) { 'ru' }
      let (:rtl) { false }

      it "returns false" do
        assert_equal right_to_left?, false
      end
    end
  end

  describe "#reason_disabled?" do
    let(:account) { accounts(:minimum) }
    let(:brand) { brands(:minimum) }

    let(:reason) do
      Satisfaction::Reason.create(
        account: account, value: 'Poor quality'
      )
    end

    describe "when the reason is deleted" do
      before do
        reason.soft_delete!
      end

      it "returns true" do
        assert_equal reason_disabled?(reason, brand), true
      end
    end

    describe "when the reason is deactivated" do
      before do
        reason.stubs(:deactivated?).returns(true)
      end

      it "returns true" do
        assert_equal reason_disabled?(reason, brand), true
      end
    end

    describe "when the reason is not disabled" do
      before do
        reason.stubs(:deleted?).returns(false)
        reason.stubs(:deactivated?).returns(false)
      end

      it "returns false" do
        assert_equal reason_disabled?(reason, brand), false
      end
    end
  end

  describe "#disabled_reason_options" do
    let(:account) { accounts(:minimum) }

    let(:reason) do
      Satisfaction::Reason.create(
        account: account, value: 'Poor quality'
      )
    end

    let(:ticket) { Ticket.first }

    before do
      ticket.stubs(:satisfaction_reason).returns(reason)
    end

    describe "when the satisfaction_reason is disabled" do
      before do
        stubs(:reason_disabled?).returns(true)
      end

      it "returns an array with the reason code inside" do
        assert_includes disabled_reason_options(ticket), ticket.satisfaction_reason.reason_code
      end
    end

    describe "when the satisfaction_reason is disabled" do
      before do
        stubs(:reason_disabled?).returns(false)
      end

      it "returns an empty array" do
        refute_includes disabled_reason_options(ticket), ticket.satisfaction_reason.reason_code
      end
    end
  end
end
