require_relative '../support/test_helper'

SingleCov.covered!

describe AdminHelper do
  describe '#rollbar_config' do
    let(:rollbar_web_base) { 'api.zendesk.com' }
    let(:environment)      { 'production' }

    before do
      ENV['ROLLBAR_WEB_BASE'] = rollbar_web_base

      Rails.stubs(env: environment)
    end

    after { ENV.delete('ROLLBAR_WEB_BASE') }

    it 'returns the JSON-encoded rollbar configuration' do
      assert_equal(
        {
          endpoint: "#{rollbar_web_base}/api/1/item/",
          payload:  {environment: environment}
        }.to_json,
        rollbar_config
      )
    end
  end
end
