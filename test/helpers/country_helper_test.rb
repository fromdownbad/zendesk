require_relative '../support/test_helper'

SingleCov.covered!

describe CountryHelper do
  include CountryHelper

  describe '#sort_countries' do
    let(:locale) { 'fr' } # French
    let(:country_list) do
      [
        { name: 'Belgie', code: 'BE' },
        { name: 'Afghanistan', code: 'AF' },
        { name: 'Cookovy ostrovy', code: 'CK' }
      ]
    end

    let(:expected_list) do
      [
        { name: 'Afghanistan', code: 'AF' },
        { name: 'Belgie', code: 'BE' },
        { name: 'Cookovy ostrovy', code: 'CK' }
      ]
    end

    it 'returns sorted list' do
      list = sort_countries(country_list, locale)
      assert_equal expected_list, list
    end
  end

  describe '#localized_name' do
    let(:locale) { 'ja' } # Japanese
    let(:country) do
      { name: 'China', code: 'CN' }
    end

    it 'returns the German name for China' do
      name = localized_name(country, locale)
      assert_equal '中国', name # China
    end
  end
end
