require_relative "../support/test_helper"

SingleCov.covered!

describe WatchingsHelper do
  include PrototypeLegacyHelper

  describe '#subscribe_link' do
    it 'returns the expected link for a create' do
      account = Account.new
      expected_link = "<a class=\"subscribe\" title=\"Receive updates by mail\" href=\"#\" onclick=\"new Ajax.Request(&#39;/watchings/create/?type=account&#39;, {asynchronous:true, evalScripts:true}); return false;\">subscribe</a>"
      assert_equal expected_link, subscribe_link(account, 'create')
    end

    it 'returns the expected link for a destroy' do
      account = Account.new
      expected_link = "<a class=\"subscribe\" title=\"Stop receiving updates by mail\" href=\"#\" onclick=\"new Ajax.Request(&#39;/watchings/destroy/?type=account&#39;, {asynchronous:true, evalScripts:true}); return false;\">unsubscribe</a>"
      assert_equal expected_link, subscribe_link(account, 'destroy')
    end
  end
end
