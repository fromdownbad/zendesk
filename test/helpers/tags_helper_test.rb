require_relative "../support/test_helper"

SingleCov.covered! uncovered: 35

describe TagsHelper do
  fixtures :all

  attr_reader :current_user
  attr_reader :current_account

  describe 'Given an agent' do
    before do
      @user = users(:minimum_end_user)
      @current_user = users(:minimum_admin)
      @current_account = @current_user.account
      accounts(:minimum).settings.has_user_tags = '1'
      accounts(:minimum).settings.save!
    end

    describe '#render_static_tags_for' do
      it 'renders all' do
        result = render_static_tags_for(@user)
        assert_match(/beta/, result)
        assert_no_match(/\+/, result)
      end

      it 'renders limited set' do
        result = render_static_tags_for(@user, 1)
        assert_match(/\+2/, result)
      end
    end
  end

  describe 'Given an end-user' do
    before do
      @user = users(:minimum_end_user)
      @current_user = users(:minimum_author)
      @current_account = @current_user.account
      @current_account.settings.has_user_tags = '1'
      @current_account.settings.save!
    end

    describe '#render_static_tags_for' do
      before { @result = render_static_tags_for(@user) }
      it 'does not render' do
        assert @result.blank?
      end
    end
  end
end
