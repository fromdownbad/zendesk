require_relative "../support/test_helper"

SingleCov.covered! uncovered: 42

describe MobileHelper do
  fixtures :accounts, :logos, :users

  attr_accessor :current_account, :current_user

  def current_account
    @current_account || accounts(:minimum)
  end

  describe "#mobile_retina_background_size_string" do
    before do
      @mobile_logo_size = 55
      @retina_dimensions = 114
    end

    it "returns '55px 55px' if width or height isn't set" do
      current_account.mobile_logo.stubs(:width).returns(nil)
      current_account.mobile_logo.stubs(:height).returns(nil)
      assert_equal "55px 55px", mobile_retina_background_size_string
    end

    it "returns '55px auto' if logo dimensions are 114x50" do
      current_account.mobile_logo.stubs(:width).returns(114)
      current_account.mobile_logo.stubs(:height).returns(50)
      assert_equal "55px auto", mobile_retina_background_size_string
    end

    it "returns 'auto auto' if logo dimensions are 40x40" do
      current_account.mobile_logo.stubs(:width).returns(40)
      current_account.mobile_logo.stubs(:height).returns(40)
      assert_equal "auto auto", mobile_retina_background_size_string
    end

    it "returns 'auto auto' if logo dimensions are 55x55" do
      current_account.mobile_logo.stubs(:width).returns(55)
      current_account.mobile_logo.stubs(:height).returns(55)
      assert_equal "auto auto", mobile_retina_background_size_string
    end

    it "returns '55px 55px' if logo dimensions are 114x114" do
      current_account.mobile_logo.stubs(:width).returns(114)
      current_account.mobile_logo.stubs(:height).returns(114)
      assert_equal "55px 55px", mobile_retina_background_size_string
    end

    it "returns 'auto 55px' if logo dimensions are 55x114" do
      current_account.mobile_logo.stubs(:width).returns(55)
      current_account.mobile_logo.stubs(:height).returns(114)
      assert_equal "auto 55px", mobile_retina_background_size_string
    end

    it "returns '28px 55px' if logo dimensions are 56x114" do
      current_account.mobile_logo.stubs(:width).returns(56)
      current_account.mobile_logo.stubs(:height).returns(114)
      assert_equal "28px 55px", mobile_retina_background_size_string
    end

    it "returns '49px 49px' if logo dimensions are 100x100" do
      current_account.mobile_logo.stubs(:width).returns(100)
      current_account.mobile_logo.stubs(:height).returns(100)

      # relative dimensions
      assert_equal "49px 49px", mobile_retina_background_size_string
    end
  end

  describe "#mobile_retina_relative_height" do
    it "returns nil if mobile_logo doesn't have a height set" do
      current_account.mobile_logo.stubs(:height).returns(nil)

      assert_nil mobile_retina_relative_height
    end

    it "returns nil if mobile_logo.height is smaller than 55px" do
      current_account.mobile_logo.stubs(:height).returns(40)

      assert_nil mobile_retina_relative_height
    end

    it "returns the relative size if mobile_logo.height is bigger than 55px" do
      current_account.mobile_logo.stubs(:height).returns(150)

      assert_equal 73, mobile_retina_relative_height
    end
  end

  describe "#mobile_retina_relative_width" do
    it "returns nil if mobile_logo doesn't have a width set" do
      current_account.mobile_logo.stubs(:width).returns(nil)

      assert_nil mobile_retina_relative_width
    end

    it "returns nil if mobile_logo.width is smaller than 55px" do
      current_account.mobile_logo.stubs(:width).returns(34)

      assert_nil mobile_retina_relative_width
    end

    it "returns the relative size if mobile_logo.width is bigger than 55px" do
      current_account.mobile_logo.stubs(:width).returns(130)

      assert_equal 63, mobile_retina_relative_width
    end
  end

  describe "#mobile_ajax_button" do
    it "responds to regular link_to calling form" do
      actual = mobile_ajax_button "foo", "bar"
      assert_match /href=\"bar\"/, actual
      assert_match /<span class=\"text\">foo<\/span>/, actual
    end

    it "is able to incorporate arbitrary html options the same way as link_to" do
      actual = mobile_ajax_button "name", "url", foo: "bar"
      assert_match /<a [^>]*foo=\"bar\"/, actual
    end

    it "escapes non HTML safe names" do
      html_unsafe_string = "<b>foo</b>"
      actual = mobile_ajax_button(html_unsafe_string, "url")
      assert_match /<span class=\"text\">&lt;b&gt;foo&lt;\/b&gt;<\/span>/, actual
    end

    it "does not escape HTML safe names" do
      html_safe_string = "<b>foo</b>".html_safe
      actual = mobile_ajax_button(html_safe_string, "url")
      assert_match /<span class=\"text\"><b>foo<\/b><\/span>/, actual
    end
  end

  describe "#show_login_logout_link" do
    before do
      stubs(return_to_parser: stub(return_to_url: '/'))
    end

    it "shows the sign in link if the user is not logged in" do
      expects(:current_user_exists?).returns(false)
      expects(:secure_url_for).with(anything).returns(controller: '/access', action: 'logout')
      assert_match(/Sign in/, show_login_logout_link)
    end

    it "shows the sign out link if the user is logged in" do
      expects(:current_user_exists?).returns(true)
      expects(:secure_url_for).with(anything).returns(controller: '/access', action: 'login')
      assert_match(/Sign out/, show_login_logout_link)
    end
  end

  describe "#go_to_full_site_link" do
    it "is shown if the controller isn't mobile/landing_page" do
      assert show_go_to_full_site_link_for('forums', return_to: 'http://some-return-url.com').present?
    end

    it "does not be shown if the controller is mobile/landing_page" do
      assert_nil show_go_to_full_site_link_for('mobile/landing_page')
    end
  end

  describe "#mobile_site_title" do
    before do
      @current_user    = users(:minimum_end_user)
      @current_account = accounts(:minimum)
    end

    it "renders account name if mobile_title is not set" do
      assert_equal @current_account.name, mobile_site_title
    end

    it "renders mobile_title if it is set" do
      @current_account.settings.mobile_title = "Mobile site title"
      assert_equal "Mobile site title", mobile_site_title
    end

    it "renders dynamic content correctly" do
      @current_account.settings.mobile_title = "{{dc.mobile_title}}"
      Zendesk::Liquid::DcContext.any_instance.expects(:render).returns("Welcome")
      mobile_site_title.must_include "Welcome"
    end

    it "does not render dynamic content if it doesn't exist" do
      @current_account.settings.mobile_title = "{{dc.mobile_title}}"
      assert_equal "", mobile_site_title
    end

    it "renders the account name when Help Center is enabled" do
      @current_account.stubs(:help_center_enabled?).returns(true)
      assert_equal @current_account.name, mobile_site_title
    end
  end

  describe "#include_apple_touch_icon_for" do
    let(:account) { accounts(:minimum) }

    it "returns a link tag with the favicon if no mobile_logo is uploaded" do
      account.stubs(mobile_logo: nil)
      account.stubs(favicon: 'something')
      account.favicon.stubs(path_for_url: 'favicon.png')

      result = include_apple_touch_icon_for(account)
      assert_match(/link/, result)
      assert_match(/favicon.png/, result)
    end

    it "returns a link tag with the mobile_logo if there is any" do
      account.stubs(mobile_logo: 'something')
      account.mobile_logo.stubs(path_for_url: 'mobile_logo.png')

      result = include_apple_touch_icon_for(account)
      assert_match(/link/, result)
      assert_match(/mobile_logo.png/, result)
    end

    it "returns a link tag with the mobile_logo also if there's uploaded a favicon" do
      account.stubs(favicon: 'something')
      account.favicon.stubs(path_for_url: 'favicon.png')
      account.stubs(mobile_logo: 'something')
      account.mobile_logo.stubs(path_for_url: 'mobile_logo.png')

      result = include_apple_touch_icon_for(account)
      assert_match(/link/, result)
      assert_match(/mobile_logo.png/, result)
    end

    it "does not return anything if neither mobile_logo or favicon is set" do
      account.stubs(favicon: nil)
      account.stubs(mobile_logo: nil)

      assert_nil include_apple_touch_icon_for(account)
    end
  end
end
