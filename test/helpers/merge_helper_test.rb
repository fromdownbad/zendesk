require_relative '../support/test_helper'

SingleCov.covered!

describe MergeHelper do
  fixtures :tickets, :accounts

  let(:current_account)    { accounts(:minimum) }
  let(:translation_locale) { current_account.translation_locale }
  let(:first_requester)    { users(:minimum_agent) }
  let(:second_requester)   { users(:minimum_admin) }

  before do
    # Merge Ticket #1 and Ticket #2 into Ticket #3.
    @target = tickets(:minimum_3)
    @sources = [tickets(:minimum_1), tickets(:minimum_2)]
    @source_ids = @sources.map(&:id)
  end

  describe '#source_ids_string' do
    subject { source_ids_string }
    before { stubs(params: {source_ids: [1, 2, 3]}) }
    it { assert_equal [1, 2, 3], subject }
  end

  describe '#new_collaborator_names' do
    subject { new_collaborator_names }

    describe_with_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
      it { assert_blank new_collaborator_names }
    end

    describe_with_arturo_setting_disabled(:follower_and_email_cc_collaborations) do
      before do
        @sources.first.collaborations << Collaboration.new(collaborator_type: CollaboratorType.EMAIL_CC, user: users(:minimum_end_user2))
      end

      it 'returns only source requesters of source tickets' do
        assert_equal 'Author Minimum', new_collaborator_names
      end
    end
  end

  describe '#new_follower_names' do
    describe_with_arturo_setting_disabled(:ticket_followers_allowed) do
      it { assert_blank new_follower_names }
    end

    describe_with_arturo_setting_enabled(:ticket_followers_allowed) do
      describe 'and adding agents as requesters' do
        before do
          @sources.first.requester  = first_requester
          @sources.second.requester = second_requester
          @sources.first.will_be_saved_by(users(:minimum_admin))
          @sources.first.save!
        end

        it 'returns the agents as email_ccs' do
          assert_equal 'Agent Minimum and Admin Man', new_follower_names
        end

        describe 'and the new requester is already a requester on the ticket' do
          before { @target.requester = users(:minimum_admin) }

          it 'does not include the new requester' do
            assert_equal 'Agent Minimum', new_follower_names
          end
        end

        describe_with_arturo_setting_enabled(:comment_email_ccs_allowed) do
          describe 'and the new requester is an email_cc, but not a follower' do
            before do
              Collaboration.create!(
                ticket: @target,
                user: users(:minimum_admin_not_owner),
                collaborator_type: CollaboratorType.EMAIL_CC
              )
            end

            it 'includes the new requester' do
              assert_equal 'Agent Minimum and Admin Man', new_follower_names
            end
          end
        end

        describe_with_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
          describe 'and the new requester is already a follower' do
            before do
              Collaboration.create!(
                ticket: @target,
                user: second_requester,
                collaborator_type: CollaboratorType.FOLLOWER
              )
            end

            it 'does not include the new requester' do
              assert_equal 'Agent Minimum', new_follower_names
            end
          end
        end
      end

      describe 'and adding end users as requesters' do
        before do
          @sources.first.requester  = users(:minimum_end_user)
          @sources.second.requester = users(:minimum_end_user2)
          @sources.first.will_be_saved_by(users(:minimum_admin))
          @sources.first.save!
        end

        it 'does not return the end users as followers' do
          assert_empty(new_follower_names)
        end
      end
    end
  end

  describe '#new_email_cc_names' do
    describe_with_arturo_setting_disabled(:comment_email_ccs_allowed) do
      it { assert_blank new_email_cc_names }
    end

    describe_with_arturo_setting_enabled(:comment_email_ccs_allowed) do
      describe 'and adding new email_ccs' do
        before do
          @sources.first.requester  = first_requester
          @sources.second.requester = second_requester
          @sources.first.will_be_saved_by(users(:minimum_admin))
          @sources.first.save!
        end

        it 'returns the new requesters as email_ccs' do
          assert_equal 'Agent Minimum and Admin Man', new_email_cc_names
        end

        describe 'and the new requester is already a requester on the ticket' do
          before { @target.requester = users(:minimum_admin) }

          it 'does not include the new requester' do
            assert_equal 'Agent Minimum', new_email_cc_names
          end
        end

        describe_with_arturo_setting_enabled(:comment_email_ccs_allowed) do
          describe 'and the new requester is already an email_cc' do
            before do
              Collaboration.create!(
                ticket: @target,
                user: second_requester,
                collaborator_type: CollaboratorType.EMAIL_CC
              )
            end

            it 'does not include the new requester' do
              assert_equal 'Agent Minimum', new_email_cc_names
            end
          end
        end

        describe_with_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
          describe 'and the new requester is a follower, but not an email_cc' do
            before do
              Collaboration.create!(
                ticket: @target,
                user: second_requester,
                collaborator_type: CollaboratorType.FOLLOWER
              )
            end

            it 'includes the new requester' do
              assert_equal 'Agent Minimum and Admin Man', new_email_cc_names
            end
          end
        end
      end
    end
  end

  describe '#locale' do
    subject { locale }
    it { assert_equal translation_locale, subject }
  end

  describe '#default_source_comment' do
    subject { default_source_comment }

    let(:target_nice_id) { @target.nice_id }
    let(:target_title) { @target.title(35) }
    let(:locale) { translation_locale }

    it { assert_equal "This request was closed and merged into request ##{target_nice_id} \"#{target_title}\".", subject }
  end

  describe '#default_target_comment' do
    subject { default_target_comment }

    describe 'for 1-1 merges' do
      before { stubs(is_bulk_merge?: false) }

      let(:source_nice_id) { @sources.first.nice_id }
      let(:source_title) { @sources.first.title(35) }
      let(:source_comment) { @sources.first.latest_public_comment }

      it { assert_equal "Request ##{source_nice_id} \"#{source_title}\" was closed and merged into this request. Last comment in request ##{source_nice_id}:\n\n#{source_comment}", subject }
    end

    describe 'for bulk merges' do
      before { stubs(is_bulk_merge?: true) }

      let(:source_ids) { @source_ids.join(', #') }
      it { assert_equal "Requests ##{source_ids} were closed and merged into this request.", subject }
    end
  end

  describe '#this_or_these_tickets_will_be_closed' do
    subject { this_or_these_tickets_will_be_closed }

    describe 'for 1-1 merges' do
      before { stubs(is_bulk_merge?: false) }
      it { assert_equal "This ticket will be closed with the following comment:", subject }
    end

    describe 'for bulk merges' do
      before { stubs(is_bulk_merge?: true) }
      it { assert_equal "These tickets will be closed with the following comment:", subject }
    end
  end

  describe '#you_are_about_to_merge_or_merge_in_bulk' do
    subject { you_are_about_to_merge_or_merge_in_bulk }

    describe 'for 1-1 merges' do
      before { stubs(is_bulk_merge?: false) }

      let(:ticket_number_source) { @sources.first.nice_id }
      let(:ticket_into_be_merge) { @target.nice_id }

      it { assert_equal "You are about to merge ticket ##{ticket_number_source} into ticket ##{ticket_into_be_merge}", subject }
    end

    describe 'for bulk merges' do
      before { stubs(is_bulk_merge?: true) }

      let(:list_of_tickets) { @source_ids.join(', #') }
      let(:the_number_of_the_ticket) { @target.nice_id }

      it { assert_equal "You are about to merge tickets ##{list_of_tickets} into ticket ##{the_number_of_the_ticket}", subject }
    end
  end

  describe '#ccs_and_or_requester_can_see_this_comment' do
    subject { ccs_and_or_requester_can_see_this_comment }

    describe_with_arturo_disabled(:email_ccs) do
      it { assert_equal 'Requester can see this comment', subject }
    end

    describe_with_arturo_enabled(:email_ccs) do
      describe_with_arturo_setting_disabled(:comment_email_ccs_allowed) do
        it { assert_equal 'Requester can see this comment', subject }
      end

      describe_with_arturo_setting_enabled(:comment_email_ccs_allowed) do
        it { assert_equal 'Requester and CCs can see this comment', subject }
      end
    end
  end
end
