require_relative "../support/test_helper"

SingleCov.covered! uncovered: 36

describe SettingsHelper do
  include SettingsHelper
  extend ArturoTestHelper

  fixtures :accounts, :translation_locales, :users, :subscriptions

  attr_accessor :current_account, :current_user

  let(:account) { accounts(:minimum) }

  describe "#show_upgrade_to_hc?" do
    before { self.current_account = account }
    it "is true only when account is plus or enterprise" do
      allowed_plans = [SubscriptionPlanType.LARGE, SubscriptionPlanType.EXTRALARGE]
      SubscriptionPlanType.fields.each do |plan_size|
        current_account.subscription.stubs(:plan_type).returns(plan_size)
        assert_equal allowed_plans.include?(plan_size), show_upgrade_to_hc?, "We should only display the upgrade to HC message for Plus and Enterprise"
      end
    end
  end

  describe "#api_used_string" do
    let(:api_token) { account.api_tokens.create! }

    it "returns never if it hasn't been used" do
      I18n.expects(:t).with('txt.admin.views.settings.api._settings.never_used')

      api_used_string(api_token)
    end

    it "returns less than 60 min ago if it was used 60 min ago or less" do
      api_token.update_attribute(:last_used, 30.minutes.ago)

      I18n.expects(:t).with('txt.admin.views.settings.api._settings.api_token_used_60_min_ago')
      api_used_string(api_token)
    end

    it "returns more that 60 min ago if it wasn't used in 60 min" do
      api_token.update_attribute(:last_used, 70.minutes.ago)

      I18n.expects(:t).with('txt.admin.views.settings.api._settings.api_token_used_more_60_min_ago')
      api_used_string(api_token)
    end
  end

  describe "#api_token_value" do
    let(:api_token) { account.api_tokens.create! }

    it "returns the truncated value" do
      truncated_token = api_token_value(api_token)
      assert_equal truncated_token.size, 8
    end
  end

  describe "#format_countries" do
    before do
      @current_user = users(:minimum_agent)
      stubs(:current_user).returns(@current_user)
    end

    it "creates a hash with the countries and locale codes with Norwegian" do
      norwegian = translation_locales(:norwegian)
      I18n.stubs(:translation_locale).returns(norwegian)
      countries = format_countries
      assert countries.include?(["De amerikanske jomfruøyene", 170])
    end
  end

  describe "#personalized_address_example" do
    let(:recipient_address) { RecipientAddress.new(email: "foo@bar.com", name: "Foo-Bar") { |ra| ra.account = accounts(:minimum) } }

    before do
      self.current_account = account
      self.current_user = users(:minimum_agent)
    end

    it "shows from and reply_to" do
      expected = [
        "\"Agent Minimum (Foo-Bar)\" <foo@bar.com>",
        "\"Foo-Bar\" <foo@bar.com>"
      ]
      assert_equal expected, personalized_address_example(recipient_address)
    end

    it "shows from and reply_to for un-personalized" do
      current_account.stubs(is_personalized_reply_enabled?: false)
      expected = [
        "\"Foo-Bar\" <foo@bar.com>",
        "\"Foo-Bar\" <foo@bar.com>"
      ]
      assert_equal expected, personalized_address_example(recipient_address)
    end

    it "works with empty" do
      recipient_address.email = nil
      recipient_address.name = nil
      expected = ["\"Agent Minimum\" <>", "<>"]
      assert_equal expected, personalized_address_example(recipient_address)
    end

    it "does not show id-masking" do
      recipient_address.email = "foo@#{current_account.subdomain}.#{Zendesk::Configuration.fetch(:host)}"
      expected = [
        "\"Agent Minimum (Foo-Bar)\" <foo@minimum.zendesk-test.com>",
        "\"Foo-Bar\" <foo+id@minimum.zendesk-test.com>"
      ]
      assert_equal expected, personalized_address_example(recipient_address)
    end

    it "shows non-asci" do
      recipient_address.name = "M∂ƒß∂M"
      expected = [
        "\"Agent Minimum (#{recipient_address.name})\" <foo@bar.com>",
        "\"#{recipient_address.name}\" <foo@bar.com>"
      ]
      assert_equal expected, personalized_address_example(recipient_address)
    end

    it "shows super-long non-asci" do
      recipient_address.name = "M∂ƒß∂M" * 10 # > 80 asci characters to trigger mail newline
      slightly_broken_from = "M∂ƒß∂MM∂ƒß∂MM∂ƒß∂MM∂ƒß∂MM∂ƒß∂MM∂ƒß∂MM∂ƒß∂MM∂ß∂MM∂ƒß∂MM∂ƒß∂M" # 1 char missing at line break
      slightly_broken_reply = "M∂ƒß∂MM∂ƒß∂MM∂ƒß∂MM∂ƒßMM∂ƒß∂MM∂ƒß∂MM∂ƒß∂MM∂ƒß∂MM∂ƒß∂MM∂ƒß∂M" # 1 char missing at line break
      expected = [
        "\"Agent Minimum (#{slightly_broken_from})\" <foo@bar.com>",
        "\"#{slightly_broken_reply}\" <foo@bar.com>"
      ]
      assert_equal expected, personalized_address_example(recipient_address)
    end
  end

  describe "#link_to_authorize_gmail_for_external_email_credentials" do
    before { self.current_account = account }

    it "sets initial_import to false when credential already exists" do
      link = link_to_authorize_gmail_for_external_email_credentials("foo", 1)
      link.must_include "initial_import=false"
    end

    it "sets initial_import to true when credential does not exist" do
      link = link_to_authorize_gmail_for_external_email_credentials("foo", nil)
      link.must_include "initial_import=true"
    end
  end
end
