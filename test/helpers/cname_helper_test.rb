require_relative "../support/test_helper"

SingleCov.covered! uncovered: 8

describe CnameHelper do
  fixtures :all
  include CnameHelper

  def flash
    ActionDispatch::Flash::FlashHash.new
  end

  def params
    @request.params
  end

  attr_reader :request

  def current_user
    @user
  end

  def current_user_exists?
    current_user && !current_user.is_anonymous_user?
  end

  def current_account
    @account
  end

  def current_brand
    @brand
  end

  attr_reader :current_url_provider

  def controller
    stub(ssl_environment?: true)
  end

  def normalize_challenge(url)
    url.sub(/challenge=\w+/, "challenge=XXX")
  end

  def default_url_options(_options = nil)
    {}
  end

  before do
    @account = accounts(:minimum)
    @user = users(:minimum_admin)
    @request = ActionDispatch::Request.new(Rack::MockRequest.env_for('/wibble'))
    @current_url_provider = @account
  end

  describe "#return_to_for_input" do
    let(:defaults) { {controller: "home"} }

    # This URL needs to be escaped because it causes UTF-8 errors
    # when trying to build the template. There doesn't appear to be a
    # way to full integration test it, and it's only reproducable by
    # curl https://trial.zendesk.dev/access/unauthenticated?return_to=http://something.de/entries/48663546-Welche-Möglichkeiten-bestehen-wenn-ich-mein-Paket-nicht-exakt-wiegen-kann-
    it "pass on params[:return_to]" do
      @request.stubs(:params).returns(return_to: "/foo")
      assert_equal "%2Ffoo", return_to_for_input
    end

    it "does not refuse any params[:return_to] since this will be done on the server" do
      @request.stubs(:params).returns(return_to: "http://google.com/foo")
      assert_equal "http%3A%2F%2Fgoogle.com%2Ffoo", return_to_for_input(defaults)
    end

    it "does not be safe" do
      @request.stubs(:params).returns(return_to: "<hello this is broken>")
      refute return_to_for_input(defaults).html_safe?
    end

    # not sure if this is good, just documenting ...
    it "returns nothing without return_to" do
      assert_nil return_to_for_input(defaults)
    end

    it "returns return_to" do
      @request.stubs(:params).returns(return_to: "/foo")
      assert_equal "%2Ffoo", return_to_for_input(defaults)
    end
  end

  describe "#secure_url_for" do
    before do
      request.stubs(:host_with_port).returns("somewhere.else.com")
    end

    it "does not generate tokens for anonymous users" do
      @user = @account.anonymous_user
      url = secure_url_for(controller: "home", action: "index")
      assert_equal "https://minimum.zendesk-test.com/home", url
    end

    it "generates tokens for normal users" do
      url = secure_url_for(controller: "home", action: "index")
      assert_equal "https://minimum.zendesk-test.com/home?challenge=XXX", normalize_challenge(url)
    end

    describe "with hostmapping" do
      before do
        @account.stubs(:is_ssl_enabled?).returns(true)
        @account.update_attribute(:host_mapping, "foo.com")
      end

      describe "and ssl" do
        before do
          @account.certificates.first.activate!
          @account.certificates.first.update_attribute(:valid_until, 10.days.from_now.to_date)
          assert_equal "https://minimum.zendesk-test.com", current_account.authentication_domain
        end

        it "generates mapped urls with ssl" do
          url = secure_url_for(controller: "home", action: "index")
          assert_equal "https://foo.com/home?challenge=XXX", normalize_challenge(url)
        end

        it "generates unmapped urls with ssl with :unmapped" do
          url = secure_url_for(controller: "home", action: "index", unmapped: true)
          assert_equal "https://minimum.zendesk-test.com/home?challenge=XXX", normalize_challenge(url)
        end
      end

      it "generates unmapped urls without ssl" do
        url = secure_url_for(controller: "home", action: "index")
        assert_equal "https://minimum.zendesk-test.com/home?challenge=XXX", normalize_challenge(url)
      end
    end

    describe "in a multi brand env with a branded url" do
      let(:brand) { FactoryBot.create(:brand) }

      before { @current_url_provider = brand.route }

      it "generates secure brand url" do
        url = secure_url_for(controller: "home", action: "index")
        assert_equal "https://#{brand.route.subdomain}.zendesk-test.com/home?challenge=XXX", normalize_challenge(url)
      end
    end
  end

  describe "#secure_form_for" do
    let(:options) { {url: {controller: :home}, return_to: {controller: :home}} }

    it "does not have a challenge token" do
      refute_includes (secure_form_for(User.new, options) { "HELLO" }), "challenge="
    end

    it "has a secure url" do
      assert_includes (secure_form_for(User.new, options) { "HELLO" }), "https://"
    end
  end
end
