require_relative "../support/test_helper"

SingleCov.covered! uncovered: 31

describe UserInterfaceHelper do
  fixtures :tickets, :suspended_tickets, :entries

  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::FormHelper
  include Rails.application.routes.url_helpers
  include TimeDateHelper

  let(:ticket) { tickets(:minimum_1) }

  describe "#tip_attribute_text" do
    it "munges multiple newline characters surrounded by whitespace characters into a single newline and strip out invalid utf-8" do
      result = tip_attribute_text("hello\t \251\252\253  \n \n there", 300)
      result.must_equal "<p>hello\n<br />there</p>"
    end
  end

  describe "#tip_attributes_for" do
    it "returns correct values for a ticket" do
      ticket.description << "\\nThis is a path: c:\\foo\\bar\\n"
      assert_equal "minimum 1 ticket\\nThis is a path: c:\\foo\\bar\\n", ticket.description
      assert_equal ["Incident #1 &nbsp;<span style=\"font-weight:normal\">(new/normal/assigned)</span>", "<p>minimum 1 ticket\\nThis is a path: c:\\foo\\bar\\n</p>"], tip_attributes_for(ticket).map { |x| JSON.parse(x) }
    end

    it "returns status open for ticket in on-hold status when requester is user" do
      ticket.status_id = 6
      assert_equal "Incident #1 &nbsp;<span style=\"font-weight:normal\">(on-hold/normal/assigned)</span>", tip_attributes_for(ticket, false).map { |x| JSON.parse(x) }.first
    end

    it "does not return priority for a ticket to end user when priority isn't visible to end users" do
      Account.any_instance.expects(:field_priority).returns(stub(is_visible_in_portal: false))
      assert_equal "Incident #1 &nbsp;<span style=\"font-weight:normal\">(new/assigned)</span>", JSON.parse(tip_attributes_for(ticket, true).first)
    end

    # should "return correct values for an entry" do
    #  assert_equal ["An entry title for ponies", "<p>ponies</p>"], tip_attributes_for(entries(:ponies))
    # end

    it "returns correct values for a suspended ticket" do
      assert_equal ["minimum spam suspended ticket", "<p>minimum spam suspended ticket</p>"], tip_attributes_for(suspended_tickets(:minimum_spam)).map { |x| JSON.parse(x) }
    end

    describe "when description is private and current user is end user" do
      it "returns the no description default text" do
        Comment.any_instance.stubs(is_public?: false)
        ticket.filter_description
        assert_equal ["Incident #1 &nbsp;<span style=\"font-weight:normal\">(new/normal/assigned)</span>", "<p>Description does not exist</p>"], tip_attributes_for(ticket, true).map { |x| JSON.parse(x) }
      end
    end
  end

  describe '#render_menu_items' do
    it 'is html safe, if all items are already html_safe' do
      menu_items << "a safe string".html_safe
      assert menu_items.all?(&:html_safe?)
      rendered_items = render_menu_items
      assert rendered_items.html_safe?
      rendered_items.must_include "a safe string"
    end

    it 'does not be html safe if any items are questionable' do
      menu_items << "a safe string".html_safe
      menu_items << "an unsafe string"
      refute render_menu_items.html_safe?
    end
  end
end
