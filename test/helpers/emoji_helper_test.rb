require_relative "../support/test_helper"

SingleCov.covered!

describe EmojiHelper do
  let_all(:emojifier) { Class.new { include EmojiHelper } }

  before do
    emojifier::WhiteList.entries = ['ambulance', '+1', '-1']
  end

  describe ".emojify!" do
    it "emojifys :ambulance:" do
      content = "this is an :ambulance:"
      emojify!(content)
      assert_equal "this is an <img src='https://static.zdassets.com/classic/images/emojis/ambulance.png' height='20px' width='20px' alt='ambulance' title='ambulance' />", content
    end

    it "does not emojify or modify :notanemoji:" do
      content = "this is :notanemoji:"
      emojify!(content)
      assert_equal 'this is :notanemoji:', content
    end

    it "emojis strings with only an emoji" do
      content = ":ambulance:"
      emojify!(content)
      assert_equal "<img src='https://static.zdassets.com/classic/images/emojis/ambulance.png' height='20px' width='20px' alt='ambulance' title='ambulance' />", content
    end

    it "emojifys emojies that are in double colons" do
      content = "::ambulance::"
      emojify!(content)
      assert_equal ":<img src='https://static.zdassets.com/classic/images/emojis/ambulance.png' height='20px' width='20px' alt='ambulance' title='ambulance' />:", content
    end

    it "preserves text that doesn't match the emoji regex" do
      content = "nothing to see here"
      emojify!(content)
      assert_equal "nothing to see here", content
    end

    it "does not match text inside code or pre blocks" do
      content = "stuff:+1:blah <code>\n:-1:\r\n</code> \n:-1:\n <pre>:+1:</pre> asdf"
      expected = "stuff<img src='https://static.zdassets.com/classic/images/emojis/%2B1.png' height='20px' width='20px' alt='%2B1' title='%2B1' />blah <code>\n:-1:\r\n</code> \n<img src='https://static.zdassets.com/classic/images/emojis/-1.png' height='20px' width='20px' alt='-1' title='-1' />\n <pre>:+1:</pre> asdf"

      emojify!(content)
      assert_equal expected, content
    end

    it "does not error on an empty string" do
      content = ""
      emojify!(content)
      assert_equal "", content
    end

    it "does not error on nil input" do
      content = nil
      emojify!(content)
      assert_nil content
    end

    it "works for html_safe strings" do
      content = ":ambulance:".html_safe
      emojify!(content)
      assert_equal "<img src='https://static.zdassets.com/classic/images/emojis/ambulance.png' height='20px' width='20px' alt='ambulance' title='ambulance' />", content
    end
  end
end
