require_relative "../support/test_helper"

SingleCov.covered!

describe SanitizedAndAutoLinkedBodyHelper do
  describe "#sanitized_and_auto_linked_body" do
    before { @entry = Entry.new }

    it "returns the sanitized body of the entry when the entry's sanitize_on_display is true" do
      @entry.sanitize_on_display = true
      @entry.body = "<p>Safe</p><object>but-this-is-dangerous()</object>"

      assert_equal @entry.body.sanitize, sanitized_and_auto_linked_body(@entry)
    end

    it "returns the body of the entry as-is when the entry's sanitize_on_display is false" do
      @entry.sanitize_on_display = false
      @entry.body = "<p>Safe</p><object>this-is-dangerous-but-i-know-better()</object>"

      assert_equal @entry.body, sanitized_and_auto_linked_body(@entry)
    end

    it "auto_links the entry's body with rel='nofollow'" do
      @entry.body = "http://www.zendesk.com/api"

      assert_equal @entry.body.auto_link(rel: "nofollow"), sanitized_and_auto_linked_body(@entry)
    end

    it "truncates link text to 70 characters" do
      @entry.body = "www.#{'a' * 63}.com"

      expected_body = %(<a href="http://#{@entry.body}" rel="nofollow noreferrer">#{@entry.body.first(67)}...</a>)

      assert_equal expected_body, sanitized_and_auto_linked_body(@entry)
    end

    it "does not double-link a elements" do
      @entry.body = %(Hello <a href="http://www.externalsite.com" rel="nofollow">External Site</a>)

      assert_equal @entry.body, sanitized_and_auto_linked_body(@entry)
    end

    it "correctly autolink URLs with ampersands" do
      @entry.body = 'foo http://foo.bar.com/a?b=c&amp;d=e'
      assert_equal 'foo <a href="http://foo.bar.com/a?b=c&amp;d=e" rel="nofollow noreferrer">http://foo.bar.com/a?b=c&amp;d=e</a>', sanitized_and_auto_linked_body(@entry)
      assert sanitized_and_auto_linked_body(@entry).html_safe?
    end
  end

  describe "#formatted_sanitized_linked_text" do
    it "sanitizes content" do
      assert_equal "<p>hello</p>", formatted_sanitized_linked_text("hello<script>alert('boo')</alert>")
    end

    it "formats newlines to <br />" do
      assert_equal "<p>hello\n<br />there</p>", formatted_sanitized_linked_text("hello\nthere")
    end

    it "handles nil input" do
      assert_equal '', formatted_sanitized_linked_text(nil)
      assert_equal '', formatted_sanitized_linked_text('    ')
    end

    it "allows basic HTML" do
      assert_equal "<p><b>hello</b></p>", formatted_sanitized_linked_text('<b>hello</b>')
      assert_equal "<p><table><td><td>hello</td></tr></table></p>", formatted_sanitized_linked_text('<table><td><td>hello</td></tr></table>')
    end

    it "autos link" do
      assert_equal "<p><a href=\"mailto:hello@foo.com\">hello@foo.com</a></p>", formatted_sanitized_linked_text('hello@foo.com')
      assert_equal '<p><a href="http://moo.com" rel="noreferrer">http://moo.com</a></p>', formatted_sanitized_linked_text('http://moo.com')
    end

    it "does not fail to handle funky situations" do
      # ActionView::Helpers::SanitizeHelper cannot handle this situation
      # it does not escape '<' in a smart way
      # https://github.com/rgrove/sanitize is a supposedly new/improved .sanitize, it uses Nokogiri
      # for parsing underneath instead of regular expressions but also does not handle this case
      # there is a web UI for testing it: http://sanitize.pieisgood.org/
      # assert_equal "<p>Gorillas <--- primates</p>", formatted_sanitized_linked_text("Gorillas <--- primates")
    end
  end
end
