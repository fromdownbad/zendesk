require_relative "../support/test_helper"

SingleCov.covered! uncovered: 26

describe UsersHelper do
  fixtures :accounts, :users, :translation_locales

  include UsersHelper

  describe "#permission_sets_list" do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_agent)
      3.times { |i| @account.permission_sets.create!(name: i) }
    end

    describe "role name translations" do
      before do
        Arturo.enable_feature!(:dc_in_role_names)
        @account.permission_sets.create!(name: "{{zd.staff_role}}")
      end

      it "translates system content if it is there" do
        list = permission_sets_list(@account)
        names = list.map { |p| p[:name] }

        assert_includes names, "Staff"
        refute_includes names, "{{zd.staff_role}}"
      end
    end

    describe "when the user has been assigned a permission set" do
      before do
        @user.permission_set = @account.permission_sets.first
        @user.save!
        @list = permission_sets_list(@account)
      end

      it "includes the account's permission sets" do
        assert_equal "0", @list[0][:name]
        assert_equal "1", @list[1][:name]
        assert_equal "2", @list[2][:name]
      end

      it "includes the adminstrator role in the last position" do
        assert_equal "Administrator", @list[-1][:name]
      end
    end

    describe "when the user has not been assigned a permission set" do
      before do
        include_legacy_agent = true
        @user.permission_set = nil
        @user.save!
        @list = permission_sets_list(@account, include_legacy_agent)
      end

      it "includes the Legacy role at position 0" do
        assert_equal "Legacy agent", @list[0][:name]
      end

      it "includes the account's permission sets" do
        assert_equal "0", @list[1][:name]
        assert_equal "1", @list[2][:name]
        assert_equal "2", @list[3][:name]
      end

      it "includes the adminstrator role in the last position" do
        assert_equal "Administrator", @list[-1][:name]
      end
    end

    describe "#role_description" do
      before do
        @ps = @account.permission_sets.first
        @ps_no_desc = @ps
        @ps_no_default_content = @ps
      end

      it "returns the description on the permission set if it exists" do
        @ps.expects(:description).twice.returns("foo")
        assert_equal role_description(@ps), "foo"
      end

      it "returns the default translation when no description is present" do
        @ps_no_desc.expects(:description).once.returns("")
        @ps_no_desc.expects(:name).once.returns("Staff")
        I18n.expects(:t).with("txt.default.roles.staff.description").returns("food is good")
        assert_equal role_description(@ps_no_desc), "food is good"
      end

      it "returns no description if there is no default content and no description" do
        @ps_no_default_content.expects(:description).once.returns("")
        @ps_no_default_content.expects(:name).once.returns("food")
        I18n.expects(:t).with('txt.admin.helpers.user_helper.no_description_label').returns("No description")
        assert_equal role_description(@ps_no_default_content), "No description"
      end
    end
  end

  describe 'social sharing helpers' do
    before do
      @user1 = stub('african', translation_locale: stub('locale', language_code: 'af-za'))
      @user2 = stub('japanese', translation_locale: stub('locale', language_code: 'ja-JP'))
    end

    describe "#user_locale_for_facebook" do
      describe "when the user has a locale" do
        it "fetches the correct fb_code" do
          assert_equal('af_ZA', user_locale_for_facebook(@user1))
        end
      end

      describe "when the user's locale doesn't have an fb code" do
        it "defaults to en_US" do
          assert_equal('en_US', user_locale_for_facebook(@user2))
        end
      end
    end
  end

  describe "#get_default_language_agent" do
    before do
      @account = accounts(:minimum)
      def current_account
        @account
      end
    end

    describe "when the account locale is localized for agents" do
      before { TranslationLocale.any_instance.expects(:localized_agent?).returns(true) }

      it "returns the account locale" do
        assert_equal @account.translation_locale, get_default_language_agent
      end
    end

    describe "when the account locale is not localized for agents" do
      before { TranslationLocale.any_instance.expects(:localized_agent?).returns(false) }

      it "returns ENGLISH_BY_ZENDESK" do
        assert_equal ENGLISH_BY_ZENDESK, get_default_language_agent
      end
    end
  end

  describe "On user page, when generating create and reset passwd options for #edit_options" do
    before do
      def is_assuming_user? # rubocop:disable Naming/PredicateName
        false
      end
      @current_user = users(:minimum_admin)
      @other_user = users(:minimum_end_user)
      def current_user # rubocop:disable Style/TrivialAccessors
        @current_user
      end
      current_user.stubs(:can?).returns(false)
    end
    describe "when request_passwd_reset permission is available" do
      before do
        current_user.stubs(:can?).with(:request_password_reset, @other_user).returns(true)
        current_user.stubs(:can?).with(:create_password, @other_user).returns(false)
      end
      it "has request passwd option" do
        assert edit_options(@other_user) =~ /password\/admin_reset_request/
        assert_nil edit_options(@other_user) =~ /password\/admin_change_request/
      end
    end
    describe "when create_passwd permission is available" do
      before do
        current_user.stubs(:can?).with(:request_password_reset, @other_user).returns(false)
        current_user.stubs(:can?).with(:create_password, @other_user).returns(true)
      end
      it "has request passwd option" do
        assert_nil edit_options(@other_user) =~ /password\/admin_reset_request/
        assert edit_options(@other_user) =~ /password\/admin_change_request/
      end
    end
  end

  describe "#can_start_merge?" do
    let(:current_user) { users(:minimum_agent) }
    let(:admin) { users(:minimum_admin) }
    let(:enduser) { users(:minimum_end_user) }

    it "can_start_merge? on an invalid loser fails" do
      refute can_start_merge?(admin)
    end
    it "can_start_merge? on a valid loser succeeds" do
      assert can_start_merge?(enduser)
    end
  end
end
