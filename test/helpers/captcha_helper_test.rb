require_relative '../support/test_helper'

SingleCov.covered!

describe CaptchaHelper do
  fixtures :accounts

  include Recaptcha::ClientHelper
  include CaptchaHelper

  def current_account
    accounts(:minimum)
  end

  describe "#captcha" do
    before do
      Recaptcha.configuration.skip_verify_env.delete("test")
    end

    after do
      Recaptcha.configuration.skip_verify_env << "test"
    end

    it "defaults to the 'light' theme" do
      zendesk_captcha.must_include 'data-theme="light"'
    end

    it "uses ssl" do
      zendesk_captcha.must_include 'https://www.recaptcha.net/recaptcha'
    end

    it "includes the translation locale" do
      zendesk_captcha.must_include 'hl=en-US'
    end

    describe_with_and_without_arturo_enabled :orca_classic_recaptcha_enterprise do
      it "uses the correct site key" do
        if @arturo_enabled
          zendesk_captcha.must_include ENV.fetch('ZENDESK_RECAPTCHA_ENTERPRISE_SITE_KEY')
        else
          zendesk_captcha.must_include ENV.fetch('ZENDESK_RECAPTCHA_V2_SITE_KEY')
        end
      end

      it "uses correct javascript file" do
        if @arturo_enabled
          zendesk_captcha.must_include 'https://www.recaptcha.net/recaptcha/enterprise.js'
        else
          zendesk_captcha.must_include 'https://www.recaptcha.net/recaptcha/api.js'
        end
      end
    end
  end
end
