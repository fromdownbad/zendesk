require_relative "../support/test_helper"
require_relative "../support/attachment_test_helper"
require_relative "../../app/helpers/application_helper"
require_relative "../../app/helpers/cname_helper"

SingleCov.covered! uncovered: 28

describe ImagePathHelper do
  include ApplicationHelper
  include CnameHelper

  fixtures :users, :accounts, :photos
  attr_reader :request

  describe "#link_to_attachment" do
    should_eventually "display a link to the attachment that's displayable in the native translation" do
      # SJT - TODO our CI box doesn't handle cyrillic characters at the filesystem level
      attachment = create_attachment("#{Rails.root}/test/files/урожай.xml")
      assert_equal "<a href=\"/attachments/token/#{attachment.token}/?name=______.xml\" target=\"_blank\">урожай.xml</a>", link_to_attachment(attachment)
    end
  end

  describe "#photo_path" do
    before do
      @user_photo = users(:photo_user)
      @user_no_photo = users(:minimum_end_user)
      @request = OpenStruct.new(request_method: :post, ssl?: false)
      ImagePathHelper.stubs(:request).returns
    end

    it "returns an image url for a user with a photo" do
      assert_includes photo_path(@user_photo), "minimum_photo.jpg"
    end

    it "returns a gravatar path for a user without a photo" do
      Zendesk::Gravatar.stubs(url: 'example_image')
      assert_equal 'example_image', photo_path(@user_no_photo)
    end

    it "returns a gravatar path for a user without a photo unless external services are off in Arturo" do
      Account.any_instance.stubs(has_disable_external_services?: true)
      assert_includes photo_path(@user_no_photo), "frame_user.jpg"
    end

    it "returns a gravatar path for a user without a photo unless have_gravatars_enabled setting is off" do
      @user_no_photo.account.settings.have_gravatars_enabled = false
      @user_no_photo.account.settings.save!
      @user_no_photo.account.settings.reload
      assert_includes photo_path(@user_no_photo), "frame_user.jpg"
    end

    it "returns a HTTPS gravatar path" do
      assert photo_thumb_path_for_email(@user_no_photo).starts_with?("https:")
    end

    it "builds a thumb" do
      assert_includes photo_path(@user_photo, version: :thumb), "minimum_photo_thumb.jpg"
    end
  end

  describe "#help_center_logo" do
    before do
      @account = accounts(:minimum)
      stubs(:user_portal_state).returns(Zendesk::UserPortalState.new(@account))
    end

    it "returns the logo set on hc if hc is enabled" do
      Zendesk::UserPortalState.any_instance.stubs(:show_help_center_logo?).returns(true)
      assert_includes help_center_logo(@account), "/hc/logo"
      # We don't want a width/height to be set for the custom HC logo
      refute_includes help_center_logo(@account), "height"
      refute_includes help_center_logo(@account), "width"
    end

    it "returns the zendesk logo if hc is disabled" do
      Zendesk::UserPortalState.any_instance.stubs(:show_help_center_logo?).returns(false)
      refute_includes help_center_logo(@account), "/hc/logo"
      assert_includes help_center_logo(@account), "width=\"109\""
      assert_includes help_center_logo(@account), "height=\"30\""
    end

    describe "with an asset host" do
      before do
        @controller.config[:asset_host] = Zendesk::AccountCdn.asset_host
      end

      it "does not return an asset path url" do
        Zendesk::UserPortalState.any_instance.stubs(:show_help_center_logo?).returns(true)
        assert_includes help_center_logo(@account), "src=\"/hc/logo"
      end
    end
  end

  describe "#help_center_favicon" do
    before do
      @account = accounts(:minimum)
      stubs(:user_portal_state).returns(Zendesk::UserPortalState.new(@account))
      stubs(:cname_wrap).returns("favicon")
    end

    it "returns the favicon set on hc if hc is enabled" do
      Zendesk::UserPortalState.any_instance.stubs(:show_help_center_favicon?).returns(true)
      assert_includes help_center_favicon(@account), "image/png"
    end

    it "returns the zendesk favicon if hc is disabled" do
      favicon = mock
      favicon.stubs(:path_for_url).returns('test')
      favicon.stubs(:content_type).returns('image/something')
      @account.stubs(:favicon).returns(favicon)
      Zendesk::UserPortalState.any_instance.stubs(:show_help_center_favicon?).returns(false)
      assert_includes help_center_favicon(@account), "image/something"
    end

    it "returns default favicon if hc is disabled and no favicon is set on account" do
      @account.stubs(:favicon).returns(nil)
      Zendesk::UserPortalState.any_instance.stubs(:show_help_center_favicon?).returns(false)
      assert_includes help_center_favicon(@account), "image/x-icon"
    end
  end

  describe "#mobile_logo_path" do
    before do
      @account = accounts(:minimum)
      stubs(:cname_wrap).returns("https://example.zendesk.com/images/header-logo.png")
    end

    it "returns the header logo if custom logo is not set" do
      @account.stubs(:header_logo).returns(nil)
      assert_includes mobile_logo_path(@account), "https://example.zendesk.com/images/header-logo.png"
    end

    it "returns the path to custom logo if it is set" do
      header_logo = mock
      header_logo.stubs(:path_for_url).returns('test')
      @account.stubs(:header_logo).returns(header_logo)
      assert_includes mobile_logo_path(@account), "https://example.zendesk.com/images/header-logo.png"
    end
  end

  describe "#favicon_path" do
    it "returns the default favicon if no account is passed, using the asset host" do
      assert_equal 'http://test.host/images/favicon_2.ico', favicon_path(nil)
    end

    it "returns icon_path if no account is passed and icon_path is passed" do
      assert_equal '/images/some_favicon.ico', favicon_path(nil, '/images/some_favicon.ico')
    end

    it "returns icon_path if both account and icon_path are passed" do
      account = OpenStruct.new
      assert_equal '/images/some_favicon.ico', favicon_path(account, '/images/some_favicon.ico')
    end

    it "returns the account's favicon if an account with a favicon is passed" do
      favicon = OpenStruct.new(path_for_url: '/images/custom_favicon.ico')
      account = OpenStruct.new(favicon: favicon)
      assert_equal '/images/custom_favicon.ico', favicon_path(account)
    end

    it "returns the account's favicon if an account with a favicon and icon_path are passed" do
      favicon = OpenStruct.new(path_for_url: '/images/custom_favicon.ico')
      account = OpenStruct.new(favicon: favicon)
      assert_equal '/images/custom_favicon.ico', favicon_path(account, '/image/some_favicon.ico')
    end
  end
end
