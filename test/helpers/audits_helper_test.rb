require_relative "../support/test_helper"

SingleCov.covered!

describe AuditsHelper do
  describe "#cia_event_presenters_for" do
    it "returns a collection of CIA::EventPresenters" do
      events = [stub(:event), stub(:event)]

      assert cia_event_presenters_for(events).all? { |item| item.is_a?(CIA::EventPresenter) }
    end
  end

  describe "#order_direction_link" do
    before do
      stubs(:params).returns(controller: "home", action: "index")
    end

    it "displays humanized field" do
      assert_includes order_direction_link("xxx_id yy_foo"), "Xxx id yy foo"
    end

    it "displays :text" do
      assert_includes order_direction_link("xxx", text: "xxx_id yy_foo"), "xxx_id yy_foo"
    end

    it "resets the page" do
      stubs(:params).returns page: "666", controller: "home", action: "index"
      assert_includes order_direction_link("xxx"), "page=1"
    end

    [["xxx", "asc"], nil, "unrelated"].each do |column, direction|
      it "displays desc and down with #{column} #{direction}" do
        stubs(:params).returns sort_by: column, sort_order: direction, controller: "home", action: "index"
        header = order_direction_link("xxx")
        assert_includes header, "sort_by=xxx&amp;sort_order=desc"
        assert_includes header, "table-arrow.png"
      end
    end

    it "displays asc with default column and desc" do
      stubs(:params).returns sort_order: "desc", controller: "home", action: "index"
      header = order_direction_link("xxx", default_sort_by: "xxx")
      assert_includes header, "sort_by=xxx&amp;sort_order=asc"
    end

    it "displays asc and up with desc" do
      stubs(:params).returns sort_by: "xxx", sort_order: "desc", controller: "home", action: "index"
      header = order_direction_link("xxx")
      assert_includes header, "sort_by=xxx&amp;sort_order=asc"
      assert_includes header, "table-arrow1.png"
    end
  end
end
