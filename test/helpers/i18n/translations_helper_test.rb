require_relative "../../support/test_helper"

SingleCov.covered!

describe I18n::TranslationsHelper do
  describe "#translations" do
    before do
      @i18n_translations = TRANSLATION_FILES.instance.js_keys
    end

    describe "when options are not passed in" do
      before { @translations = translations.keys }

      it "only returns i18n translations" do
        assert (@i18n_translations - @translations).empty? && (@translations - @i18n_translations).empty?
      end
    end

    describe "when cldr_includes option is passed in" do
      before do
        @cldr_translations = { date_time: I18n::TranslationsHelper::Cldr.translations_for(:date_time).keys,
                               unit: I18n::TranslationsHelper::Cldr.translations_for(:unit).keys }
      end

      describe "and option contains a single type" do
        before do
          @translations = translations(cldr_includes: :date_time).keys
        end

        it "returns cldr translations for that type in addition to i18n translations" do
          all_translations = @i18n_translations + @cldr_translations[:date_time]

          assert (all_translations - @translations).empty? && (@translations - all_translations).empty?
        end
      end

      describe "and option contains an array of types" do
        before do
          @translations = translations(cldr_includes: [:date_time, :unit]).keys
        end

        it "returns cldr translations for those types in addition to i18n translations" do
          all_translations = @i18n_translations + @cldr_translations[:date_time] + @cldr_translations[:unit]

          assert (all_translations - @translations).empty? && (@translations - all_translations).empty?
        end
      end
    end
  end

  describe "#rosetta_url" do
    it "returns rosetta url if no id or not action is present" do
      stubs(:params).returns({})
      assert_equal "http://support.localhost:5100/rosetta/translations_gui", rosetta_url
    end

    it "returns rosetta_url plus the id" do
      stubs(:params).returns(id: 3)
      assert_equal "http://support.localhost:5100/rosetta/translations_gui/3", rosetta_url
    end

    describe "when params has action" do
      it "returns the action if it is not show or index" do
        stubs(:params).returns(action: "clone")
        assert_equal "http://support.localhost:5100/rosetta/translations_gui/clone", rosetta_url
      end

      it "does not return the action if it is show" do
        stubs(:params).returns(action: "show")
        assert_equal "http://support.localhost:5100/rosetta/translations_gui", rosetta_url
      end

      it "does not return the action if it is index" do
        stubs(:params).returns(action: "show")
        assert_equal "http://support.localhost:5100/rosetta/translations_gui", rosetta_url
      end
    end
  end
end
