require_relative "../support/test_helper"

SingleCov.covered! uncovered: 31

describe AccountsHelper do
  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::FormHelper
  include Rails.application.routes.url_helpers
  include TimeDateHelper

  fixtures :accounts, :translation_locales, :users, :groups, :subscriptions, :monitored_twitter_handles, :memberships
  attr_accessor :current_user, :current_account

  before do
    @locale = translation_locales(:japanese)
    @minimum_admin = users(:minimum_admin)
    @minimum_account = accounts(:minimum)
  end

  describe "#campaign_message" do
    let (:site_url) do
      I18n.t('txt.admin.helpers.accounts_helper.marketing_website')
    end

    describe "when account id is 22844" do
      before { @minimum_account.id = 22844 }
      it "returns empty" do
        assert_empty(campaign_message(@minimum_account))
      end
    end

    describe "when :image_self_branding is off" do
      before { Arturo.disable_feature!(:image_self_branding) }

      it "returns text link markup" do
        url = add_tracking_params(site_url, utm_medium: "poweredbyzendesk", utm_source: "webportal",
                                            utm_campaign: "text", utm_content: @minimum_account.name)

        assert_equal(' <a target="_blank" href="' + url + '">Support Software</a> by Zendesk ', campaign_message(@minimum_account))
      end
    end

    describe "when :image_self_branding is on" do
      before { Arturo.enable_feature!(:image_self_branding) }

      it "returns non-text link markup" do
        url = add_tracking_params(site_url, utm_medium: "poweredbyzendesk", utm_source: "webportal",
                                            utm_campaign: "image", utm_content: @minimum_account.name)
        message = " <a href='#{url}' target='_blank' class='zd_branding_logo'><img src='/images/branding/powered_by_default.png' /></a> "
        assert_equal(message, campaign_message(@minimum_account))
      end
    end
  end

  describe "#translation_select_box_format" do
    it "formats the list of languages so that a select box can add them as options" do
      assert_equal [["Japanese - 日本語", @locale.id]], translation_select_box_format([@locale])
    end

    it "returns a sorted array of locales" do
      @japanese = translation_locales(:japanese)
      @spanish = translation_locales(:spanish)
      @custom_japanese = translation_locales(:japanese_x)
      assert_equal [["English (United States)", 1], ["Japanese - 日本語", 4], ["Japanese (Private-Use=test) - 日本語 (私用=test)", 59], ["Spanish - español", 6]], translation_select_box_format([@japanese, @spanish, @custom_japanese, ENGLISH_BY_ZENDESK])
    end
  end

  describe "#twitter_searches_for_account" do
    before do
      @searches = [create_twitter_search(name: "New Search 1"), create_twitter_search(name: "New Search 2"),
                   create_twitter_search(name: "New Search 3", group_id: groups(:with_groups_group1).id)]

      @current_account = accounts(:with_groups)
      @current_user = users(:with_groups_agent1)
      CIA.stubs(:current_actor).returns(@current_user)
    end

    it "includes searches for everyone and group searches" do
      @searches.each { |s| assert twitter_searches_for_account.include?(s) }
    end

    it "does not include all group searches if current user is not admin" do
      @searches << create_twitter_search(name: "New Search 4", group_id: groups(:with_groups_group2).id)
      @searches[0..-2].each { |s| assert twitter_searches_for_account.include?(s), "Should include #{s.name}" }
      refute twitter_searches_for_account.include?(@searches[-1]), "Shouldn't include #{@searches[-1].name}"
    end

    it "includes all group searches if current user is admin" do
      @searches << create_twitter_search(name: "New Search 4", group_id: groups(:with_groups_group2).id)
      @current_user.update_attribute(:roles, Role::ADMIN.id)
      @searches.each { |s| assert twitter_searches_for_account.include?(s), "Should include #{s.name}" }
    end
  end

  describe '#help_desk_meta_description_tag' do
    before { @current_account = @minimum_account }

    it 'returns nothing when the setting is blank' do
      current_account.texts.expects(:help_desk_description).returns(nil)
      assert_nil help_desk_meta_description_tag
    end

    it 'returns a <meta> tag when the setting is present' do
      current_account.texts.expects(:help_desk_description).returns("A help desk")
      assert_select_in help_desk_meta_description_tag, 'meta[name="description"][content="A help desk"]'
    end
  end

  private

  def new_twitter_search(attributes = {})
    default_attributes = {
      account: accounts(:with_groups),
      monitored_twitter_handle: MonitoredTwitterHandle.where(account_id: accounts(:minimum).id).first,
      name: 'New search',
      query: 'foo bar'
    }

    Channels::Twitter::Search.new(default_attributes.merge(attributes))
  end

  def create_twitter_search(attributes = {})
    new_twitter_search(attributes).tap(&:save!)
  end
end
