require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 17

describe People::UsersHelper do
  fixtures :users, :groups, :forums

  attr_reader :current_user

  describe 'Given one group' do
    before do
      @current_user = users(:minimum_agent)
      @groups = [groups(:with_groups_group1)]
    end

    describe '#group_label' do
      before { @result = group_label(@groups) }
      it 'displays a singular label' do
        assert_no_match(/groups/, @result)
      end
    end

    describe '#display_groups' do
      before { @result = display_groups(@groups) }
      it 'displays the group name' do
        assert_match(/#{@groups.first.name}/, @result)
      end

      it 'is html_safe?' do
        assert @result.html_safe?
      end

      describe 'as admin' do
        before { @current_user.stubs(:is_admin?).returns(true) }

        it 'includes a link' do
          result = display_groups(@groups)
          assert_match(/<\/a>/, result)
          assert result.html_safe?
        end
      end
    end
  end

  describe 'Given multiple groups' do
    before do
      @current_user = users(:minimum_agent)
      @groups = [groups(:with_groups_group1), groups(:with_groups_group2)]
    end

    describe '#group_label' do
      before { @result = group_label(@groups) }
      it 'displays a plural label' do
        assert_match(/groups/, @result)
      end
    end

    describe '#display_groups' do
      before { @result = display_groups(@groups) }
      it 'displays the group names' do
        assert_match(/#{@groups.first.name}/, @result)
        assert_match(/#{@groups.last.name}/, @result)
      end
    end
  end

  describe 'Given an administrator' do
    before do
      @current_user = users(:minimum_admin)
      @groups = [groups(:with_groups_group1), groups(:with_groups_group2)]
    end

    describe '#display_groups' do
      before { @result = display_groups(@groups) }
      it 'displays as links' do
        assert_match(/<a href/, @result)
      end
    end
  end

  describe 'Given a non-administrator' do
    before do
      @current_user = users(:minimum_agent)
      @groups = [groups(:with_groups_group1), groups(:with_groups_group2)]
    end

    describe '#display_groups' do
      before { @result = display_groups(@groups) }
      it 'does not display as links' do
        assert_no_match(/<a href/, @result)
      end
    end
  end

  describe 'Given a user and some watchings' do
    before do
      @user = users(:minimum_end_user)
      @forum = forums(:announcements)
      Watching.create!(user: @user, source: @forum, account: @user.account)

      assert_equal(@forum, @user.watchings.first.source)
    end

    describe '#watchings_for' do
      it 'returns the watchings' do
        watchings = @user.watchings
        assert_equal(@user.watchings, watchings)
      end
    end
  end
end
