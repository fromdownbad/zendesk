require_relative '../../support/test_helper'

SingleCov.covered! uncovered: 35

describe People::RolesHelper do
  describe '#rtl?' do
    before do
      TranslationLocale.create!(locale: locale, right_to_left: rtl)
      I18n.stubs(:locale).returns(locale)
    end

    describe 'when locale is right to left' do
      let (:locale) { 'ar' }
      let (:rtl) { true }

      it 'returns true' do
        assert rtl?
      end
    end

    describe 'when locale is left to right' do
      let (:locale) { 'ru' }
      let (:rtl) { false }

      it 'returns false' do
        refute rtl?
      end
    end
  end

  describe '#string_prefix' do
    it 'returns role definition translation string prefix' do
      assert_equal string_prefix, 'txt.admin.views.people.roles.role_definition'
    end
  end

  describe '#rtl_class' do
    describe 'when locale is right to left' do
      before do
        stubs(:rtl?).returns(true)
      end

      it 'returns the is_rtl class' do
        assert_equal 'is-rtl', rtl_class
      end
    end

    describe 'when locale is left to right' do
      before do
        stubs(:rtl?).returns(false)
      end

      it 'returns an empty string' do
        assert_equal '', rtl_class
      end
    end
  end

  describe '#section_separator' do
    it 'renders a div with a border' do
      assert_equal '<div class="u-border-b u-bc-grey-300"></div>', section_separator
    end
  end

  describe '#section' do
    it 'renders a section element with a separator and renders block content' do
      expected = '<section class="u-mv-lg"><div class="u-mb"><h2 class="u-fs-lg u-mb-xs">title</h2><p>description</p></div><div>Content</div></section><div class="u-border-b u-bc-grey-300"></div>'
      assert_equal expected, section(title: 'title', description: 'description') { 'Content' }
    end

    it 'allows for a section with no title or description' do
      expected = '<section class="u-mv-lg"><div class="u-mb"><h2 class="u-fs-lg u-mb-xs"></h2><p></p></div><div>Content</div></section><div class="u-border-b u-bc-grey-300"></div>'
      assert_equal expected, section { 'Content' }
    end
  end

  describe '#hidden_style' do
    it 'returns a style propery "display: none" when hidden is true' do
      assert_equal 'display: none;', hidden_style(true)
    end

    it 'returns an empty string when hidden is false' do
      assert_equal '', hidden_style(false)
    end
  end
end
