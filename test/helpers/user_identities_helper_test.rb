require_relative "../support/test_helper"

SingleCov.covered! uncovered: 18

describe UserIdentitiesHelper do
  include ActionView::Helpers::UrlHelper
  include ActionView::Helpers::FormHelper
  include Rails.application.routes.url_helpers
  include TimeDateHelper

  fixtures :accounts, :users, :user_identities

  attr_reader :current_user

  describe '#can_add_twitter_handle_manually?' do
    it 'is false if the current user is not an admin' do
      @current_user = users(:minimum_end_user)
      @user = users(:minimum_end_user)
      refute can_add_twitter_handle_manually?
    end

    it 'is false if the current user is an admin, but the account being edited is also an admin' do
      @current_user = users(:minimum_admin)
      @user = users(:minimum_admin)
      refute can_add_twitter_handle_manually?
    end

    it 'is true if the current user is an admin and the account being edited is not an admin' do
      @current_user = users(:minimum_admin)
      @user = users(:minimum_end_user)
      assert can_add_twitter_handle_manually?
    end
  end
end
