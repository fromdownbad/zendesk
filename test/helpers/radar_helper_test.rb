require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'RadarHelper' do
  include RadarHelper

  def assert_hash_equal(left, right)
    left.each do |key, value|
      right[key].must_equal value, "'#{right[key]}' is not equal to '#{value}' in '#{key}'"
    end
  end

  class ChangingValue
    def ==(other)
      !!other
    end
  end

  attr_reader :request, :current_user, :current_account

  fixtures :accounts, :users, :entries

  before do
    ENV.delete('RADAR_URL') # this can be set in development and messes up the test
    @current_user = users(:minimum_agent)
    @current_account = @current_user.account
  end

  describe "#radar_configuration" do
    let(:config) do
      {
        'engineio_079' => true,
        'path' => '/engine.io-0.7.9',
        'secure' => true,
        'upgrade' => false,
        'userId' => 57888,
        'userType' => 4,
        'accountName' => 'minimum',
        'auth' => ChangingValue.new,
        'expires_at' => ChangingValue.new,
        'transports' => ['polling', 'websocket'],
        'upgrades' => ['websocket'],
        'query' => {
          'section' => 1,
          'user' => 57888,
          'subdomain' => 'minimum'
        },
        'userData' => {
          'name' => 'Agent Minimum',
          'id' => 57888,
          'accountId' => @current_account.id
        },
        'enabled' => true
      }
    end

    it "includes the default data" do
      assert_hash_equal(config, radar_configuration)
    end

    it "includes URL data for a sharded configuration" do
      sharded = {
        'servers' => [{ 'host' => 'support.localhost', 'port' => 843 }]
      }
      assert_hash_equal(sharded, radar_configuration)
    end

    describe "radar_sockets" do
      describe "with it" do
        before do
          current_account.stubs(:has_radar_sockets?).returns(true)
        end

        it "upgrade is true" do
          assert_equal(['polling', 'websocket'], radar_configuration['transports'])
          assert(radar_configuration['upgrade'])
          assert(radar_configuration['rememberUpgrade'])
        end
      end

      describe "without it" do
        before do
          current_account.stubs(:has_radar_sockets?).returns(false)
        end

        it "upgrade is false" do
          assert_equal(['polling', 'websocket'], radar_configuration['transports'])
          assert_equal(false, radar_configuration['upgrade'])
          assert(radar_configuration['rememberUpgrade'])
        end
      end
    end

    describe "with multiple urls" do
      before do
        current_account.shard_id = 2
        current_account.stubs(:pod_id).returns(1)
      end

      after do
        current_account.shard_id = 1
      end

      it "includes URL data for a sharded configuration" do
        sharded = {
          'servers' => [{ 'host' => 'support.localhost', 'port' => 843 }, { 'host' => 'support.localhost', 'port' => 844 }]
        }
        assert_hash_equal(sharded, radar_configuration)
      end
    end

    describe "when radar pod level kill switch arturo is enabled" do
      before do
        Arturo.enable_feature!(:pod_or_cluster_radar_kill_switch)
        f = Arturo::Feature.find_feature(:pod_or_cluster_radar_kill_switch)
        f.update_attributes(phase: 'external_beta')
        f.external_beta_subdomains = ['+pod1']
        f.save!
      end

      it "will be disabled" do
        radar_configuration['enabled'].must_equal false
      end
    end

    describe "with radar_polling_timeout enabled" do
      before do
        Arturo.enable_feature!(:radar_polling_timeout)
      end

      it "sets requestTimeout" do
        radar_configuration['requestTimeout'].must_equal RadarHelper::SOCKET_TIMEOUT
      end
    end

    describe "with radar_polling_timeout disabled" do
      before do
        Arturo.disable_feature!(:radar_polling_timeout)
      end

      it "sets requestTimeout" do
        radar_configuration['requestTimeout'].must_be_nil
      end
    end
  end

  describe "#radar_auth_token" do
    it "returns a valid auth token" do
      time = Time.parse('2013-06-21 00:00:00UTC')
      token = radar_auth_token(time).to_s
      assert_equal "4fgGHLhnu9R2dWRRVwr/tdXe1wCNhHvvXmKXbZDPwHJ8VqCz+vQJhDf2hO3OA4HkEyzg+c3vW0gCFpHEcdD2NQ==", token

      # half an hour past
      time = Time.parse('2013-06-21 00:30:00UTC')
      token = radar_auth_token(time).to_s
      assert_equal "gfLiBptrDNMBGlvSHQwaYzX6TlUAPI3VVnoX4+T09H0zW/+nKk+71RVd/n9jkvgd0G4n2J7uIcNT7tABPSMtJg==", token
    end

    it "returns expiry at timestep boundary" do
      time = Time.parse('2013-06-21 00:00:00UTC')
      assert_equal expires_at(time), Time.parse('2013-06-21 00:30:00UTC').to_i

      # within half hour
      time = Time.parse('2013-06-21 00:12:13UTC')
      assert_equal expires_at(time), Time.parse('2013-06-21 00:30:00UTC').to_i
    end

    it "returns a unique chat token each time" do
      generate_chat_manager_token.wont_equal generate_chat_manager_token
    end

    it "returns api host correctly for non-ssl hostmapped accounts" do
      @current_account.stubs(:host_mapping).returns("support.minimum.com")
      @current_account.is_ssl_enabled = false
      @current_account.save!

      assert_equal radar_api_host, "minimum.zendesk-test.com"
    end

    it "returns no api host for others " do
      @current_account.is_ssl_enabled = true
      @current_account.save!

      assert_nil radar_api_host
    end
  end
end
