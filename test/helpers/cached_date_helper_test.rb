require_relative "../support/test_helper"

SingleCov.covered!

describe CachedDateHelper do
  include UserInterfaceHelper

  describe "#cacheable_time_ago_in_words" do
    before do
      @time = 'Apr 1, 2009'.to_time
      Kernel.stubs(:rand).returns('0.123456')
    end

    it "provides cachable i18n time ago in words" do
      I18n.stubs(:t).with('txt.views.ticket_list.comment_by', name: 'pdeuter', time_ago: "XXX").returns "Comment by pdeuter, XXX ago"
      cachable_js = "<span id='cached-date-0.123456'></span><script>\n//<![CDATA[\n$('cached-date-0.123456').replace(\"Comment by pdeuter, XXX ago\".gsub('XXX', i18n_time_ago_in_words(1238544000000)));\n//]]>\n</script>"

      assert_equal cachable_js, i18n_cacheable_time_ago_in_words(@time, I18n.t("txt.views.ticket_list.comment_by", name: "pdeuter", time_ago: "XXX"))
    end
  end
end
