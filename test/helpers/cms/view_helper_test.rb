require_relative '../../support/test_helper'
require_relative '../../support/rule'

SingleCov.covered! uncovered: 30

describe Cms::ViewHelper do
  include TestSupport::Rule::Helper
  include Cms::ViewHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }

  describe '#rule_reference_link' do
    %i[automation trigger macro].each do |rule_type|
      describe "when the rule's type is `{rule_type}`" do
        let(:rule)  { send("create_#{rule_type}", title: title) }
        let(:title) { 'New rule' }
        let(:href)  { "\"#{rule.lotus_relative_link}\"" }

        it "returns a link to the #{rule_type}" do
          assert_equal(
            "<a href=#{href}>#{rule.title}</a>",
            rule_reference_link(rule)
          )
        end

        describe 'and the title exceeds the length limit' do
          let(:title) do
            'Rule title that exceeds the currently set length limits'
          end

          it "returns a truncated link to the #{rule_type}" do
            assert_equal(
              "<a href=#{href}>#{rule.title[0...47]}...</a>",
              rule_reference_link(rule)
            )
          end
        end
      end
    end
  end

  describe '#rule_reference_owner' do
    describe 'when the rule is a macro' do
      let(:rule) { create_macro }

      describe 'and it is a personal macro' do
        before do
          rule.owner      = users(:minimum_admin)
          rule.owner_type = 'User'
        end

        it 'displays the owner information with a link' do
          assert_equal(
            "<div class=\"dc-owner-information\">"\
              "<span>#{"\u2022".force_encoding('utf-8')}</span>
              Owned by <a href=\"/users/86133\">Admin Man</a></div>".gsub(/\n\s+/, ""),
            rule_reference_owner(rule)
          )
        end

        describe 'when the user is inactive' do
          before { rule.owner.is_active = false }

          it 'does not contain a link' do
            assert_equal(
              "<div class=\"dc-owner-information\">"\
                "<span>#{"\u2022".force_encoding('utf-8')}</span>
                #{I18n.t('txt.admin.views.cms.texts.references.macro_owned_by',
                  owner: rule.owner)}</div>".gsub(/\n\s+/, ""),
              rule_reference_owner(rule)
            )
          end
        end
      end

      it 'does not display owner information' do
        assert_nil rule_reference_owner(rule)
      end
    end

    %i[automation trigger].each do |rule_type|
      let(:rule) { send("create_#{rule_type}") }

      describe "when the rule is a #{rule_type}" do
        it 'does not display owner information' do
          assert_nil rule_reference_owner(rule)
        end
      end
    end
  end
end
