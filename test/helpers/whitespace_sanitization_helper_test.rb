require_relative "../support/test_helper"

SingleCov.covered!

describe WhitespaceSanitizationHelper do
  describe "#to_single_spaces" do
    it "substitutes whitespace for a single space" do
      expected = "Software art thou?"
      string = "\f Software   \n\r art thou?     \t "

      assert_equal expected, to_single_spaces(string)
    end
  end

  describe "#strip_between_tags" do
    describe "when html is nil" do
      it "handles gracefully" do
        expected = ""
        html = nil

        assert_equal expected, strip_between_tags(html)
      end
    end

    it "strips whitespace between html tags" do
      expected = "<p><span>Software art thou?</span></p>"
      html = <<-HTML
        <p>\n\t<span>Software art thou?</span>  \r\n</p>
      HTML

      assert_equal expected, strip_between_tags(html)
    end
  end

  describe "#strip_and_compress_lines" do
    it "compresses lines and strips whitespace" do
      expected = 'hello<p> welcome aboard! </p>you are awesome'
      html = <<-HTML
        hello
            <p> welcome aboard! </p>
        you are awesome
      HTML

      assert_equal expected, strip_and_compress_lines(html)
    end
  end
end
