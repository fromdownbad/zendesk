require_relative '../support/test_helper'

SingleCov.covered!

describe LedgerRateLimitHelper do
  include LedgerRateLimitHelper

  def subdomain
    'subdomainsRus'
  end

  let(:account) { accounts(:minimum) }
  let(:arturo_enabled) { true }

  before do
    account.expects(:has_ocp_log_crl_events_to_ledger_enabled?).returns(arturo_enabled)

    stub_request(:post, "#{Zendesk::Configuration.dig!(:ledger, :base_url)}/api/v1/events").
      to_return(status: 200, body: "", headers: {})
  end

  describe "with :ocp_log_crl_events_to_ledger arturo enabled" do
    describe 'Ledger' do
      it ':create event is logged to Ledger' do
        account.send(:ledger_log_rate_limit_create, 'api/v2/test', 789)
      end

      it ':delete event is logged to Ledger' do
        account.send(:ledger_log_rate_limit_destroy, 'test')
      end
    end

    describe 'when the config is wrongly configured' do
      before do
        Rails.logger.expects(:error).once
      end

      it 'a :create error is sent to Rails.logger' do
        account.send(:ledger_log_rate_limit_create, 'api/v2/test', 789)
      end

      it 'a :delete error is sent to Rails.logger' do
        account.send(:ledger_log_rate_limit_destroy, 'api/v2/test')
      end
    end
  end

  describe "with :ocp_log_crl_events_to_ledger arturo disabled" do
    let(:arturo_enabled) { false }

    describe 'Ledger' do
      it ':create event is NOT logged to Ledger' do
        account.send(:ledger_log_rate_limit_create, 'api/v2/test', 789)
      end

      it ':delete event is NOT logged to Ledger' do
        account.send(:ledger_log_rate_limit_destroy, 'test')
      end
    end
  end
end
