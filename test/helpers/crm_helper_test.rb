require_relative "../support/test_helper"

SingleCov.covered!

describe CrmHelper do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
  end

  def current_account
    @account
  end

  describe "#crm_selected_option" do
    it "defaults to Salesforce Integration" do
      crm_selected_option.must_equal SalesforceIntegration.name
    end

    it "returns correct crm integration" do
      Account.any_instance.stubs(:crm_integration).returns(SugarCrmIntegration.new)
      assert_equal SugarCrmIntegration.name, crm_selected_option
    end
  end

  describe "#salesforce_env_selected_option" do
    before do
      Account.any_instance.stubs(:crm_integration).returns(SalesforceIntegration.new)
    end

    it "defaults to production" do
      assert_equal "production", salesforce_env_selected_option
    end

    it "returns sandbox" do
      SalesforceIntegration.any_instance.stubs(:is_sandbox?).returns(true)
      assert_equal "sandbox", salesforce_env_selected_option
    end
  end

  describe "#salesforce_app_latency_select_options" do
    let(:configured_delay) { 45 }

    before { Account.any_instance.stubs(:salesforce_integration).returns(SalesforceIntegration.new) }

    before do
      Account.any_instance.stubs(:salesforce_integration).returns(SalesforceIntegration.new)
    end

    it "defaults to 60 minutes" do
      assert_select_in salesforce_app_latency_select_options, "option[selected='selected'][value='60']"
    end

    it "selects configured latency" do
      SalesforceIntegration.any_instance.stubs(:app_latency).returns(configured_delay)
      assert_select_in salesforce_app_latency_select_options, "option[selected='selected'][value='#{configured_delay}']"
    end
  end

  describe "#salesforce_lookup_types_selected_option" do
    let(:lookup_type) { "RequesterEmail" }

    before { Account.any_instance.stubs(:crm_integration).returns(SalesforceIntegration.new) }

    it "selects lookup type latency" do
      SalesforceIntegration.any_instance.stubs(:lookup_type).returns(lookup_type)
      assert_equal lookup_type, salesforce_lookup_types_selected_option
    end
  end

  describe "#class_for_crm_div" do
    it "returns 'hide' if selected option and div class are not equal" do
      selected_option = "selected option"
      assert_equal "hide", class_for_crm_div(selected_option)
    end

    it "returns 'show' if selected option and div class are equal" do
      selected_option = "SalesforceIntegration"
      assert_equal "show", class_for_crm_div(selected_option)
    end
  end

  describe "#mapped_zendesk_field" do
    it "returns field label" do
      configure_zendesk_field = "ticket.requester.name"
      assert_equal I18n.t("placeholder.#{configure_zendesk_field}"), mapped_zendesk_field(configure_zendesk_field)
    end
  end

  describe "#salesforce_filter_options" do
    let(:selected_key) { "selected_option_key" }
    let(:selected_type) { "selected_option_type" }

    it "creates options" do
      options = [
        { key: "key_1", type: "type_1" },
        { key: selected_key, type: selected_type },
        { key: "key_2", type: "type_2" }
      ]
      assert_select_in salesforce_filter_options(options, selected_key, ''),
        "option[selected='selected'][data-type='#{selected_type}'][value='#{selected_key}']"
    end
  end
end
