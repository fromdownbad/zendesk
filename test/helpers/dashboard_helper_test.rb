require_relative "../support/test_helper"

SingleCov.covered! uncovered: 17

describe DashboardHelper do
  include TimeDateHelper

  fixtures :accounts, :users, :tickets

  let(:current_user) { users(:with_groups_agent_groups_restricted) }
  let(:other_agent) { users(:minimum_agent) }
  let(:user_without_groups) { users(:support_agent) }

  describe "#new_and_open_tickets_in_group_with_restrictions" do
    before do
      @tickets_count = accounts(:with_groups).tickets.for_user(users(:with_groups_agent_groups_restricted)).where(group_id: current_user.groups, status_id: [StatusType.NEW, StatusType.OPEN]).count(:all)
    end

    it "returns the exact amount of new and open tickets with group restrictions for the current user" do
      assert_equal @tickets_count, new_and_open_tickets_in_group_with_restrictions
    end

    it "returns 0 if the user has no groups" do
      assert_equal 0, new_and_open_tickets_in_group_with_restrictions(user_without_groups)
    end
  end

  describe "#positively_rated_tickets_for_user_link" do
    before do
      stubs(:agent_tickets_received_good_rating).with(current_user).returns(1)
      stubs(:agent_tickets_received_good_rating).with(other_agent).returns(2)
    end

    describe "for other agent" do
      it "renders the correct link" do
        assert_equal "<a href=\"#{good_this_week_satisfaction_path(agent_id: other_agent.id)}\">      <span class=\"dashboard-stats small\">\n        <span class=\"count\">2</span>\n        <span></span>\n        <span class=\"you\">Good</span>\n      </span>\n</a>", positively_rated_tickets_for_user_link(other_agent)
      end
    end

    describe "for yourself" do
      it "renders the correct link" do
        assert_equal "<a href=\"#{good_this_week_satisfaction_path(agent_id: current_user.id)}\">      <span class=\"dashboard-stats small\">\n        <span class=\"count\">1</span>\n        <span></span>\n        <span class=\"you\">Good</span>\n      </span>\n</a>", positively_rated_tickets_for_user_link
      end
    end

    describe "with no ratings" do
      before { stubs(:agent_tickets_received_good_rating).with(other_agent).returns(0) }

      it "does not render the any link (anchor)" do
        assert_equal "      <span class=\"dashboard-stats small\">\n        <span class=\"count\">0</span>\n        <span></span>\n        <span class=\"you\">Good</span>\n      </span>\n", positively_rated_tickets_for_user_link(other_agent)
      end
    end
  end

  describe "#negatively_rated_tickets_for_user_link" do
    before do
      stubs(:agent_tickets_received_bad_rating).with(current_user).returns(1)
      stubs(:agent_tickets_received_bad_rating).with(other_agent).returns(2)
    end

    describe "for other agent" do
      it "renders the correct link" do
        assert_equal "<a href=\"#{bad_this_week_satisfaction_path(agent_id: other_agent.id)}\">      <span class=\"dashboard-stats small\">\n        <span class=\"count\">2</span>\n        <span></span>\n        <span class=\"you\">Bad</span>\n      </span>\n</a>", negatively_rated_tickets_for_user_link(other_agent)
      end
    end

    describe "for yourself" do
      it "renders the correct link" do
        assert_equal "<a href=\"#{bad_this_week_satisfaction_path(agent_id: current_user.id)}\">      <span class=\"dashboard-stats small\">\n        <span class=\"count\">1</span>\n        <span></span>\n        <span class=\"you\">Bad</span>\n      </span>\n</a>", negatively_rated_tickets_for_user_link
      end
    end

    describe "with no ratings" do
      before { stubs(:agent_tickets_received_bad_rating).with(other_agent).returns(0) }

      it "does not render the any link (anchor)" do
        assert_equal "      <span class=\"dashboard-stats small\">\n        <span class=\"count\">0</span>\n        <span></span>\n        <span class=\"you\">Bad</span>\n      </span>\n", negatively_rated_tickets_for_user_link(other_agent)
      end
    end
  end

  describe "#agent_tickets_received_good_rating" do
    before do
      @account = accounts(:minimum)
      @agent = users(:minimum_agent)

      t1 = tickets(:minimum_1)
      t2 = tickets(:minimum_2)

      assert(t1.update_columns(assignee_id: @agent.id, satisfaction_score: SatisfactionType.GOOD, status_id: 3, solved_at: Time.now))
      assert(t2.update_columns(assignee_id: @agent.id, satisfaction_score: SatisfactionType.GOODWITHCOMMENT, status_id: 3, solved_at: Time.now))
      t1.reload
      t2.reload

      @account.reload
    end

    it "returns 2" do
      count = agent_tickets_received_good_rating(@agent)
      assert_equal(2, count)
    end
  end

  describe "#agent_tickets_received_bad_rating" do
    before do
      @account = accounts(:minimum)
      @agent = users(:minimum_agent)

      t1 = tickets(:minimum_1)
      t2 = tickets(:minimum_2)

      t1.update_columns(assignee_id: @agent.id, satisfaction_score: SatisfactionType.BAD, status_id: 3, solved_at: Time.now)
      t2.update_columns(assignee_id: @agent.id, satisfaction_score: SatisfactionType.BADWITHCOMMENT, status_id: 3, solved_at: Time.now)
      t1.reload
      t2.reload

      @account.reload
    end

    it "returns 2" do
      count = agent_tickets_received_bad_rating(@agent)
      assert_equal(2, count)
    end
  end
end
