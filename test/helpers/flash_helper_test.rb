require_relative "../support/test_helper"

SingleCov.covered! uncovered: 4

describe FlashHelper do
  include FlashHelper
  fixtures :users

  describe "#flash_error" do
    before do
      @header = "<header></header>"
    end

    describe "with a header" do
      describe "and no models" do
        it "returns only the header" do
          assert_equal "<header></header>", flash_error(@header)
        end
      end

      describe "and a nil model" do
        before do
          @model = nil
        end

        it "returns the header" do
          assert_equal "<header></header>", flash_error(@header, @model)
        end
      end

      describe "and a model that is a String" do
        before do
          @model = "String"
        end

        it "returns the header with an unordered list and the string in a list item" do
          assert_equal "<header></header><ul><li>String</li></ul>", flash_error(@header, @model)
        end
      end

      describe "and a model that is a subclass of ActiveRecord::Base" do
        describe "and one error with an :events attribute" do
          before do
            @model = FlashHelperErrorTestClass.new(:events, "Events")
          end

          it "returns the header and an empty unordered list" do
            assert_equal "<header></header><ul></ul>", flash_error(@header, @model)
          end
        end

        describe "and one error with none :base attribute" do
          before do
            @model = FlashHelperErrorTestClass.new(:not_base, "Not Base")
          end

          it "returns the header and unordered list with one list item with a strong element in it" do
            assert_equal "<header></header><ul><li><strong>Not base</strong> Not Base</li></ul>", flash_error(@header, @model)
          end
        end

        describe "that is a Rule model" do
          before do
            @definition = Definition.new
            @definition.actions.push(DefinitionItem.new('assignee_id', nil, users(:minimum_agent).id))
            @model = Rule.new(definition: @definition)
          end

          describe "with a random base error message" do
            before do
              @model.errors.add(:base, "bop")
            end

            it "returns the default error for the rule" do
              assert_equal "<header></header><ul><li>bop</li></ul>", flash_error(@header, @model)
            end
          end

          describe "with a specific base group deleted error message" do
            [View, UserView, Trigger, Automation, Macro].each do |rule_type|
              describe "with #{rule_type} subtype" do
                before do
                  @model = rule_type.new(definition: @definition)
                  @model.errors.add(:base, "Invalid value '20426187' in 'Group / Is / 20426187'")
                end

                it "returns the specific group deleted error" do
                  assert_match(/\<header\>\<\/header\>\<ul\>\<li\>A group associated with this .* has been deleted\<\/li\>\<\/ul\>/, flash_error(@header, @model))
                end
              end
            end
          end
        end
      end
    end
  end

  class FlashHelperErrorTestClass < ActiveRecord::Base
    def initialize(attribute, message)
      errors.add(attribute, message)
    end

    def errors
      @errors ||= ActiveModel::Errors.new(self)
    end
  end

  class FlashHelperUnknownTestClass
  end
end
