require_relative "../support/test_helper"

SingleCov.covered! uncovered: 95

describe TicketFieldsHelper do
  include TicketFieldsHelper

  fixtures :accounts, :users, :groups, :memberships, :translation_locales, :tickets, :ticket_fields, :ticket_field_entries, :custom_field_options, :ticket_forms

  before do
    @ticket  = Ticket.new
    @account = accounts(:minimum)
    @user    = users(:minimum_admin)
  end

  def current_account
    @account
  end

  def current_user
    @user
  end

  let(:request) do
    stub(format: stub(mobile?: false), is_mobile?: false)
  end

  describe "#assignable_agents_and_groups" do
    it "is html safe" do
      assert assignable_agents_and_groups.html_safe?
    end
  end

  describe "#field_show_for_agent" do
    let(:field) { ticket_fields(:field_tagger_custom) }
    let(:humanized_value) { "Thing & Other Thing < Other other thing" }

    it 'renders special characters correctly' do
      field.stubs(:humanize_value).returns(humanized_value)
      assert_equal "<li><h4>Fun factor </h4>Thing &amp; Other Thing &lt; Other other thing</li>",
        field_show_for_agent(field, @ticket)
    end
  end

  describe "#field_edit_element_for_end_user (and _end_user_mobile)" do
    before do
      @subject_field = @account.field_subject
      @description_field = @account.field_description
      @form = mock('FormBuilder')
      @form.stubs(:text_field)
    end

    it "attempts to translate title_in_portal and description into the user's locale" do
      @subject_field.expects(:multilingual_field).with(user_locale: current_user.translation_locale, account_locale: current_account.translation_locale, field_type: :title_in_portal)
      @subject_field.expects(:multilingual_field).with(user_locale: current_user.translation_locale, account_locale: current_account.translation_locale, field_type: :description)
      field_edit_element_for_end_user(@subject_field, @ticket, @form)

      @subject_field.expects(:multilingual_field).with(user_locale: current_user.translation_locale, account_locale: current_account.translation_locale, field_type: :title_in_portal)
      @subject_field.expects(:multilingual_field).with(user_locale: current_user.translation_locale, account_locale: current_account.translation_locale, field_type: :description)
      field_edit_element_for_end_user_mobile(@subject_field, @ticket, @form)
    end

    describe "when the stored value matches the default for the account locale" do
      before do
        @japanese = translation_locales(:japanese)
        current_user.stubs(:translation_locale).returns(@japanese)
        @subject_field.title = "Subject"
        @subject_field.save!
      end

      it "translates values into the user's locale" do
        field_edit_element_for_end_user(@subject_field, @ticket, @form).must_include "Japanese subject field"
      end
    end

    describe "when either title_in_portal or description has been changed from default" do
      before do
        @japanese = translation_locales(:japanese)
        current_user.stubs(:translation_locale).returns(@japanese)
        @subject_field.title = "Subject"
        @subject_field.description = "Not the Default Description"
        @subject_field.save!
      end

      it "does not translate it" do
        field_edit_element_for_end_user(@subject_field, @ticket, @form).must_include "Japanese subject field"
        field_edit_element_for_end_user(@subject_field, @ticket, @form).must_include "Not the Default Description"
      end
    end
  end

  describe "#get_status" do
    describe "when enabled" do
      before { @account.expects(:use_status_hold?).returns(true) }

      it "includes status hold" do
        assert get_status(@ticket).include?(["On-hold", 6])
      end
    end

    describe "when disabled" do
      before { @account.expects(:use_status_hold?).returns(false) }

      it "does not include status hold" do
        assert_equal false, get_status(@ticket).include?(["On-hold", 6])
      end

      describe "and the ticket is on-hold" do
        before { @ticket.status_id = StatusType.HOLD }

        it "includes status hold" do
          assert get_status(@ticket).include?(["On-hold", 6])
        end
      end
    end
  end
  describe "rendering js_groups_array" do
    before do
      @account = accounts(:with_groups)
      @user = users(:with_groups_agent1)
    end

    it "gives json of groups and users" do
      js_groups = js_groups_array(@account.groups.assignable(@user)).sort_by { |e| e[:id] }
      js_groups.each { |e| e[:users].sort! } # easier to compare
      groups_and_users = [{ id: groups(:with_groups_group1).id, users: [users(:with_groups_agent1).id, users(:with_groups_agent2).id].sort },
                          { id: groups(:with_groups_group2).id, users: [users(:with_groups_agent2).id, users(:with_groups_agent_groups_restricted).id, users(:with_groups_admin).id].sort }].sort_by { |e| e[:id] }
      assert_equal groups_and_users, js_groups
    end

    it "handles an account with no assignable groups" do
      js_groups = js_groups_array(mock(pluck: []))
      assert_empty(js_groups)
    end

    describe "with an empty group" do
      before do
        @empty_group = @account.groups.create(name: 'Empty group')
      end

      it "gives json of groups and users" do
        js_groups = js_groups_array(@account.groups.assignable(@user)).sort_by { |e| e[:id] }
        js_groups.each { |e| e[:users].sort! } # easier to compare
        groups_and_users = [{ id: groups(:with_groups_group1).id, users: [users(:with_groups_agent1).id, users(:with_groups_agent2).id].sort },
                            { id: groups(:with_groups_group2).id, users: [users(:with_groups_agent2).id, users(:with_groups_agent_groups_restricted).id, users(:with_groups_admin).id].sort },
                            { id: @empty_group.id,                users: [] }].sort_by { |e| e[:id] }
        assert_equal groups_and_users, js_groups
      end
    end
  end

  describe "rendering the ticket_type_options" do
    let(:ticket) { tickets(:minimum_1) }
    let(:ticket2) { tickets(:minimum_2) }

    before do
      problems = mock("AccountProblems")
      current_account.expects(:problems).returns(problems)
      problems.expects(:working).returns(problems)

      ticket2.is_public = false
      ticket2.subject = ''
      ticket2.will_be_saved_by(@user)
      ticket2.save!

      # Make sure only these attributes in the select statement are ever called
      Ticket.new.attributes.keys.each do |attribute|
        ticket2.expects(attribute).never unless %i[id nice_id account_id subject description].include?(attribute.to_sym)
      end

      problems.expects(:select).returns(problems)
      problems.expects(:from).returns(problems)
      problems.expects(:order).returns(problems)
      problems.expects(:limit).returns([ticket2])
    end

    # This test is largely here to test that the markup below doesn't end up escaped.
    it "meets expectations" do
      form = mock("FormBuilder")

      form.expects(:select).with(
        :linked_id, [["#2 minimum ticket 2", ticket2.id]], { include_blank: true }, style: 'width:auto'
      ).returns('linked_problem')

      assert ticket_type_options(@ticket, form)
    end

    it "works when ticket.nil?" do
      form = mock("FormBuilder")

      form.expects(:select).with(
        :linked_id, [[I18n.t(NO_CHANGE), -1], ['#2 minimum ticket 2', ticket2.id]], { include_blank: false }, style: 'width:auto'
      ).returns('linked_problem')

      assert ticket_type_options(nil, form)
    end

    describe "requires the 'is_public' attribute to be used" do
      it "does not blow up on fetching description" do
        form = mock("FormBuilder")
        form.expects(:select).with(
          :linked_id, [["#2 minimum 2 ticket", ticket2.id]], { include_blank: true }, style: 'width:auto'
        ).returns('linked_problem')

        Ticket.any_instance.stubs(:via?).with(:closed_ticket).returns(false)
        Ticket.any_instance.stubs(:subject).returns(nil)
        ticket.expects(:is_public?).never
        ticket2.expects(:is_public?).returns(false).once

        assert ticket_type_options(ticket, form)
      end
    end
  end

  describe "#ticket_type_options" do
    it "does not blow up on arturo check" do
      form = mock("FormBuilder")
      Ticket.any_instance.stubs(:subject).returns(nil)

      assert ticket_type_options(@ticket, form)
    end
  end

  describe '#form_element_for_field' do
    before do
      @form = mock('FormBuilder')
      @form.stubs(:select)
    end

    describe 'for a subject field' do
      before do
        @field = { type: 'FieldSubject' }
        @field.stubs(:is_system_field?).returns(true)
      end

      it 'renders a text field' do
        @form.expects(:text_field).with(:subject, class: 'title', maxlength: 150)
        form_element_for_field(@field, @ticket, @form)
      end
    end

    describe 'for a description field' do
      before do
        @field = { type: 'FieldDescription' }
        @field.stubs(:is_system_field?).returns(true)
      end

      it 'renders a text area' do
        expects(:text_area).with(:comment, :value, rows: 6, cols: 68, style: 'width:100%')
        form_element_for_field(@field, @ticket, @form)
      end
    end

    describe 'for a status field' do
      before do
        @field = { type: 'FieldStatus' }
        @field.stubs(:is_system_field?).returns(true)
      end

      it 'renders a select' do
        @form.expects(:select).with(:status_id, get_status(@ticket), {}, onchange: 'toggleRequiredFields();')
        form_element_for_field(@field, @ticket, @form)
      end

      describe 'in twitter search' do
        before do
          stubs(:in_twitter_search?).returns(true)
        end

        it 'renders a select for v2' do
          @form.expects(:select).with(:status, get_status_for_v2(@account, @ticket), {}, onchange: 'toggleRequiredFields();')
          form_element_for_field(@field, @ticket, @form)
        end
      end
    end

    describe 'for a ticket type field' do
      before do
        @field = { type: 'FieldTicketType' }
        @field.stubs(:is_system_field?).returns(true)
      end

      it 'renders a select' do
        @form.expects(:select).with(:ticket_type_id, get_ticket_types(@ticket), {}, onchange: 'renderPerType();')
        form_element_for_field(@field, @ticket, @form)
      end

      describe 'for mobile' do
        let(:request) do
          stub(format: :mobile_v2)
        end

        it 'renders a select' do
          @form.expects(:select).with(:ticket_type_id, get_ticket_types(@ticket), {}, onchange: 'window.mobile.toggleDueDate();')
          form_element_for_field(@field, @ticket, @form)
        end
      end

      describe 'in twitter search' do
        before do
          stubs(:in_twitter_search?).returns(true)
        end

        it 'renders a select' do
          expects(:select_tag).with('ticket[type]', options_for_select(get_ticket_types_for_v2(@account, @ticket)), onchange: 'renderPerType();')
          form_element_for_field(@field, @ticket, @form)
        end

        describe 'for mobile' do
          let(:request) do
            stub(format: :mobile_v2)
          end

          it 'renders a select' do
            expects(:select_tag).with('ticket[type]', options_for_select(get_ticket_types_for_v2(@account, @ticket)), onchange: 'window.mobile.toggleDueDate();')
            form_element_for_field(@field, @ticket, @form)
          end
        end
      end
    end

    describe 'for a priority field' do
      before do
        @field = { type: 'FieldPriority' }
        @field.stubs(:is_system_field?).returns(true)
      end

      it 'renders a select' do
        @form.expects(:select).with(:priority_id, get_priorities(@ticket), {}, onchange: '')
        form_element_for_field(@field, @ticket, @form)
      end

      describe 'in twitter search' do
        before do
          stubs(:in_twitter_search?).returns(true)
        end

        it 'renders a select for v2' do
          @form.expects(:select).with(:priority, get_priorities_for_v2(@account, @ticket), {}, onchange: '')
          form_element_for_field(@field, @ticket, @form)
        end
      end
    end

    describe 'for any field type' do
      before do
        @form.stubs(:text_field)
        @form.stubs(:fields_for).yields(@form)
        @field = FieldText.create!(
          account: @account,
          title: 'Basic Text Field',
          title_in_portal: 'Basic Text Field',
          is_active: true,
          is_editable_in_portal: true,
          is_visible_in_portal: true,
          is_required_in_portal: false
        )
      end

      describe 'for a group or assignee field type' do
        it 'not set the field value to the value entered previously' do
          form_element_for_field(@field, @ticket, @form, false, "")

          assert_received @form, :text_field do |expect|
            expect.with do |_field_name, options|
              options[:value] == ""
            end
          end
        end
      end

      describe 'for a field type other than group or assignee' do
        describe 'when a value has been entered' do
          it 'set the field value to the value entered previously' do
            form_element_for_field(@field, @ticket, @form, false, "How now brown cow")

            assert_received @form, :text_field do |expect|
              expect.with do |_field_name, options|
                options[:value] == "How now brown cow"
              end
            end
          end
        end

        describe 'when a value has not been entered previously' do
          it 'not set the field value to the value entered previously' do
            form_element_for_field(@field, @ticket, @form, false, "")

            assert_received @form, :text_field do |expect|
              expect.with do |_field_name, options|
                options[:value] == ""
              end
            end
          end
        end
      end
    end

    describe 'for a group field' do
      before do
        @ticket = Ticket.new
        @field = { type: 'FieldGroup' }
        @field.stubs(:is_system_field?).returns(true)
      end

      describe 'on an account with only one group' do
        before do
          assert_equal 1, @account.groups.length
          @ticket.assignee = @user
          @group = @account.groups.first
          @form.stubs(:hidden_field).returns('<input type="hidden" name="ticket_group_id" />')
          form_element_for_field(@field, @ticket, @form)
        end

        it 'creates a hidden group field' do
          assert_received @form, :hidden_field do |expect|
            expect.with do |field_name, options|
              field_name == :group_id && options[:value] == @group.id
            end
          end
        end
      end

      describe 'on an account with multiple groups' do
        before do
          @group = @account.groups.create! name: 'Anything'
          @ticket.assignee = @user
          @form.stubs(:select).returns('<select />')
          @result = form_element_for_field(@field, @ticket, @form)
        end

        it 'creates a group select' do
          assert_received @form, :select do |expect|
            expect.with do |field_name, groups|
              field_name == :group_id && groups.include?([@group.name, @group.id])
            end
          end
        end
      end

      describe 'on an account where requester has group restriction' do
        before do
          @account = accounts(:with_groups)
          @user = users(:with_groups_agent1)
          @user.restriction_id = RoleRestrictionType.GROUPS
          assert_equal false, @user.groups.include?([@account.groups.last])

          @ticket.account = accounts(:with_groups)
          @ticket.requester_id = @user.id
          @ticket.group_id = @account.groups.last.id
          @ticket.description = 'test'
          @ticket.will_be_saved_by(@user)
          @ticket.save!

          @form.stubs(:select).returns('<select />')
          @result = form_element_for_field(@field, @ticket, @form)
        end

        it 'includes the ticket group in the group select' do
          assert_received @form, :select do |expect|
            expect.with do |field_name, groups|
              field_name == :group_id && groups.include?([@account.groups.last.name, @account.groups.last.id])
            end
          end
        end
      end
    end

    describe 'with an assignee field and a nil ticket (for bulk ticket forms)' do
      before do
        stubs(:assignable_agents).returns([@user, @user])
        @field = { type: 'FieldAssignee' }
        @field.stubs(:is_system_field?).returns(true)
        form_element_for_field(@field, nil, @form, true)
      end

      it 'knows it is a bulk update' do
        assert_received(@form, :select) do |expect|
          expect.with do |_field_name, _agents, _field_options, html_options|
            html_options['data-bulk-update'] == true
          end
        end
      end

      it 'renders all assignable agents, independent of a ticket context' do
        assert_received(@form, :select) do |expect|
          expect.with do |_field_name, agents, _field_options, _html_options|
            agents.include?([@user.name, @user.id])
          end
        end
      end
    end

    describe 'with an assignee field and a ticket with an assignee' do
      before do
        ticket = Ticket.new
        ticket.assignee = @user
        @field = { type: 'FieldAssignee' }
        @field.stubs(:is_system_field?).returns(true)
        form_element_for_field(@field, ticket, @form)
      end

      it 'knows it is not a bulk update' do
        assert_received(@form, :select) do |expect|
          expect.with do |_field_name, _agents, _field_options, html_options|
            html_options['data-bulk-update'] == false
          end
        end
      end

      it 'pass the current assignee ID as a data attribute' do
        assert_received(@form, :select) do |expect|
          expect.with do |_field_name, _agents, _field_options, html_options|
            html_options['data-value'] == @user.id
          end
        end
      end
    end

    describe "with a field tagger" do
      before do
        @form.stubs(:fields_for).yields(@form)
        @field = FieldTagger.new
        @field.custom_field_options.loaded
        @field.custom_field_options.target.replace(subject)
      end

      describe "with a ticket and a user that can't edit it" do
        subject { [{"name" => "test", "value" => "test"}] }

        before do
          @ticket = tickets(:minimum_1)
          current_user.stubs(:can?).with(:edit_properties, @ticket).returns(false)
        end

        it "renders a static nested select" do
          @form.expects(:static_nested_select)
          form_element_for_field(@field, nil, @form)
        end
      end

      describe "with nested tags" do
        subject { [{"name" => "test#{CATEGORY_DELIMITER}test", "value" => "testing"}] }

        before do
          @ticket = nil
        end

        it "renders a static nested select" do
          @form.expects(:tagger_nested_select)
          form_element_for_field(@field, nil, @form)
        end

        it "returns the tagger_nested_select" do
          @form.expects(:tagger_nested_select)
          form_element_for_field(@field, nil, @form)
        end
      end

      describe "with no ticket and no nested tags" do
        subject { [{"name" => "test", "value" => "testing"}] }

        before do
          @ticket = nil
          @form.stubs(:select).returns("")
          form_element_for_field(@field, nil, @form)
        end

        it "renders a select" do
          assert_received(@form, :select) do |expect|
            expect.with do |_field_name, tags, _field_options|
              (tags & subject.map { |c| [c["name"], c["value"]] }).size == subject.size
            end
          end
        end
      end

      describe "with no nested tags and Arturo tag" do
        subject { [{"name" => "test", "value" => "testing"}] }

        before do
          @ticket = Ticket.new(ticket_type_id: 4)
          @form.stubs(:select).returns("")
        end

        it "renders a select" do
          form_element_for_field(@field, @ticket, @form)
          assert_received(@form, :select) do |expect|
            expect.with do |_field_name, tags, _field_options|
              (tags & subject.map { |c| [c["name"], c["value"]] }).size == subject.size
            end
          end
        end
      end

      describe "with a bulk update" do
        subject { [{"name" => "test", "value" => "testing"}] }

        before do
          @ticket = nil
          @form.stubs(:select).returns("")
          form_element_for_field(@field, @ticket, @form, true)
        end

        it "has No Change selected" do
          assert_received(@form, :select) do |expect|
            expect.with do |_field_name, _tags, field_options|
              field_options[:selected] == '- No Change -'
            end
          end
        end
      end
    end
  end

  describe 'render plain field that uses datepicker' do
    before do
      @form = mock('FormBuilder')
      @form.stubs(:select)
      @ticket = Ticket.new(ticket_type_id: 4)
    end

    it 'renders field with no value' do
      ticket_options = ticket_type_options(@ticket, @form)
      assert_match("<input type='text' id='ticket_due_date' class='new_date_picker' data-field='hidden_date' readonly='readonly'>", ticket_options)
    end

    describe 'render field with the due date' do
      before do
        @ticket.due_date = "Fri, 08 Jun 2012 00:28:24 UTC +00:00"
      end

      it 'renders value in input field' do
        ticket_options = ticket_type_options(@ticket, @form)
        assert_match("<input type='text' id='ticket_due_date' class='new_date_picker' data-field='hidden_date' value='12-06-08' readonly='readonly'>", ticket_options)
      end

      describe 'render due date in the proper locale' do
        before do
          @portuguese = translation_locales(:brazilian_portuguese)
          @user = users(:minimum_admin)
          @user.locale_id = @portuguese.id
        end

        it 'renders value in input field' do
          ticket_options = ticket_type_options(@ticket, @form)
          assert_match("<input type='text' id='ticket_due_date' class='new_date_picker' data-field='hidden_date' value='12-06-08' readonly='readonly'>", ticket_options)
        end
      end
    end
  end

  describe 'Dynamic Content' do
    before do
      @japanese = translation_locales(:japanese)
      @spanish = translation_locales(:spanish)

      @account = accounts(:minimum)
      @tagger_field = ticket_fields(:field_tagger_custom)
      @ticket = @account.tickets.first
      @ticket.additional_tags = "dc_content"
      @ticket.will_be_saved_by(users(:minimum_admin))
      @ticket.save!

      @user = @ticket.requester
      @user.account.locale_id = @japanese.id

      stubs(:current_user).returns(@user)
      stubs(:current_account).returns(@account)

      fallback_locale = translation_locales(:english_by_zendesk)
      @fbattrs = {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: fallback_locale.id }
      @cms_text = Cms::Text.create!(name: "Welcome Wombat", fallback_attributes: @fbattrs, account: @ticket.account)
      @cms_japanese_variant = @cms_text.variants.create!(active: true, translation_locale_id: @japanese.id, value: "Japanese Value!")
      @cms_spanish_variant = @cms_text.variants.create!(active: true, translation_locale_id: @spanish.id, value: "ello! wotcher! wombats are awesome")
    end

    it "renders DC in end user portal" do
      @form = mock('FormBuilder')
      @form.expects(:fields_for)

      field = field_edit_element_for_end_user(@tagger_field, @ticket, @form)
      assert_equal "<div data-field-id=\"#{@tagger_field.id}\"><h3>Fun factor (portal) Japanese Value! </h3>\n<p>test</p><p></p></div>", field
      assert field.html_safe?
    end

    it "renders DC in end user portal for mobile" do
      @form = mock('FormBuilder')
      @form.expects(:fields_for)

      field = field_edit_element_for_end_user_mobile(@tagger_field, @ticket, @form)
      assert_equal "<div class=\"field-wrapper   field-dropdown\"><h3 data-role=\"title\" id=\"fun-factor-{{dc.welcome_wombat}}\">Fun factor (portal) Japanese Value! </h3><div data-role=\"description\"><p>test</p></div></div>", field
      assert field.html_safe?
    end

    describe "#ticket_field_value_for_print" do
      before do
        @date_field = ticket_fields(:field_date_custom)
        @textarea_field = ticket_fields(:field_textarea_custom)
        @ticket.ticket_field_entries << ticket_field_entries(:field_date_custom_entry)
        @ticket.ticket_field_entries << ticket_field_entries(:field_textarea_custom_entry)
        @ticket.save!
      end

      it "renders textarea field values" do
        assert_equal "<p>here is some textarea text...</p>", ticket_field_value_for_print(@textarea_field, @ticket)
      end

      it "renders human readable dates" do
        assert_equal "January 2, 2012", ticket_field_value_for_print(@date_field, @ticket)
      end

      it "renders human readable dates for different timezones" do
        Time.zone = "Pacific/Auckland"
        assert_equal "January 2, 2012", ticket_field_value_for_print(@date_field, @ticket)
      end

      it "renders human readable dates for current year" do
        Timecop.freeze("2012-01-01")
        assert_equal "Jan 2", ticket_field_value_for_print(@date_field, @ticket)
      end

      it "renders DC for ticket print view" do
        assert_equal "Japanese Value!", ticket_field_value_for_print(@tagger_field, @ticket)
      end

      describe "deleted_tickets" do
        let(:deleted_ticket) { @ticket.becomes(FakeScrubbedTicket) }

        describe 'with prevent_deletion_if_churned' do
          before do
            Arturo.enable_feature!(:prevent_deletion_if_churned)
          end

          it "renders textarea field values for scrubbed tickets" do
            assert_equal "X", ticket_field_value_for_print(@textarea_field, deleted_ticket)
          end

          it "renders X for dates" do
            assert_equal "X", ticket_field_value_for_print(@date_field, deleted_ticket)
          end

          it "renders X for dates in different timezones" do
            Time.zone = "Pacific/Auckland"
            assert_equal "X", ticket_field_value_for_print(@date_field, deleted_ticket)
          end

          it "renders X for dates without the current year" do
            Timecop.freeze("2012-01-01")
            assert_equal "X", ticket_field_value_for_print(@date_field, deleted_ticket)
          end

          it "renders DC for ticket print view" do
            assert_equal "X", ticket_field_value_for_print(@tagger_field, deleted_ticket)
          end
        end

        describe 'without prevent_deletion_if_churned' do
          before do
            Arturo.disable_feature!(:prevent_deletion_if_churned)
          end

          it "renders textarea field values for scrubbed tickets" do
            assert_equal "<p>here is some textarea text...</p>", ticket_field_value_for_print(@textarea_field, deleted_ticket)
          end

          it "renders dates" do
            assert_equal "January 2, 2012", ticket_field_value_for_print(@date_field, deleted_ticket)
          end

          it "renders DC for ticket print view" do
            assert_equal "Japanese Value!", ticket_field_value_for_print(@tagger_field, deleted_ticket)
          end
        end
      end
    end

    describe "#field_show_for_print" do
      it "renders field title and DC elements for ticket print view" do
        assert_equal "<h3>Fun factor Japanese Value!</h3><p>Japanese Value!</p>",
          field_show_for_print(@tagger_field, @ticket)
      end
    end
  end

  describe "in_twitter_search?" do
    it "is false" do
      refute in_twitter_search?
    end

    describe "when ::Zendesk::Channels::Twitter::SearchController" do
      before do
        controller.class.stubs(:name).returns('Zendesk::Channels::Twitter::SearchController')
      end

      it "is true" do
        assert in_twitter_search?
      end
    end
  end
end
