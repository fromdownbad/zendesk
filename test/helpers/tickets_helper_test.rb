require_relative "../support/test_helper"

SingleCov.covered! uncovered: 82

describe TicketsHelper do
  include TimeDateHelper
  include RulesHelper
  include ApplicationHelper
  include TicketFieldsHelper
  include ActionView::Helpers::FormHelper

  fixtures :tickets, :accounts, :users, :groups, :organizations, :events, :subscriptions, :translation_locales, :ticket_fields, :facebook_pages

  before do
    @ticket = Ticket.new
  end

  def current_account
    accounts(:minimum)
  end

  def current_user
    users(:minimum_admin)
  end

  describe "#custom_fields_render" do
    before do
      @ticket = tickets(:minimum_1)
      @link   = 'example.org'
    end

    it "does not have N+1s" do
      assert_sql_queries 14 do
        form_for(@ticket) do |f|
          custom_fields_render(f, @link, false)
        end
      end
    end

    describe "with a ticket that has a custom_field_tagger and custom_field_options" do
      before do
        @ticket = tickets(:minimum_5)
        fallback_attributes = { is_fallback: true, nested: true, value: "Welcome Wombat", translation_locale_id: current_user.translation_locale.id }
        Cms::Text.create!(name: "welcome_wombat", fallback_attributes: fallback_attributes, account: current_account)
      end

      it "does not have N+1s" do
        assert_sql_queries 5 do
          form_for(@ticket) do |f|
            custom_fields_render(f, 'example.com', false)
          end
        end
      end
    end
  end

  describe "#output_due_date_or_linked_problem" do
    before do
      @ticket = tickets(:minimum_1)
      @field1 = ticket_fields(:support_field_ticket_type)
      @field2 = @field1.clone
    end

    it "outputs the linked problem_id" do
      @field1.expects(:value).with(@ticket).returns(TicketType.INCIDENT)
      @ticket.expects(:linked_id).with.returns(1)
      @ticket.expects(:problem_id).with.twice.returns(1)

      expected_result = "<div class='name_value'><h3> #{I18n.t('txt.admin.views.tickets.print.incident_of')} </h3><p> #{@ticket.problem_id} </p></div>"
      result = output_due_date_or_linked_problem(@field1)
      assert_equal expected_result, result
    end

    it "outputs the task due date" do
      @field2.expects(:value).with(@ticket).twice.returns(TicketType.TASK)
      @ticket.expects(:due_date).with.at_least(3).returns(Time.now.utc)

      expected_result = "<div class='name_value'><h3> #{I18n.t('txt.admin.views.tickets.print_ticket_field.due_date')} </h3><p> #{format_date(@ticket.due_date) if @ticket.due_date} </p></div>"
      result = output_due_date_or_linked_problem(@field2)
      assert_equal expected_result, result
    end
  end

  describe "#render_followers" do
    it "does not fail if ticket has no collaborators" do
      Ticket.any_instance.expects(:collaborators).returns(nil)
      assert_nil render_followers
    end

    it "renders correctly if only one collaborator" do
      user = users(:minimum_admin)
      Ticket.any_instance.expects(:collaborators).times(2).returns([user])
      assert_equal "<div id='ccs'><h3>CCs</h3><p>#{h user.email_address_with_name_without_quotes}</p></div>", render_followers
    end

    it "renders correctly if multiple collaborators" do
      user1 = users(:minimum_admin)
      user2 = users(:minimum_agent)
      Ticket.any_instance.expects(:collaborators).times(2).returns([user1, user2])
      assert_equal "<div id='ccs'><h3>CCs</h3><p>#{h user1.email_address_with_name_without_quotes}, #{h user2.email_address_with_name_without_quotes}</p></div>", render_followers
    end
  end

  describe "#ticket_requester_name_for_display" do
    before do
      @ticket = tickets(:minimum_1)
    end

    it "returns properly formatted name and email" do
      assert_equal "<p>#{@ticket.requester.name} &lt;#{@ticket.requester.email}&gt;</p>", ticket_requester_name_for_display
    end

    it "returns just the requester name when no email is available" do
      @ticket.requester.stubs(:email).returns(nil)
      assert_equal "<p>#{@ticket.requester.name}</p>", ticket_requester_name_for_display
    end

    it "adds a span tag in RTL for proper formatting" do
      hebrew = translation_locales(:hebrew)
      current_user.translation_locale = hebrew
      current_user.save!

      assert_equal "<p>#{@ticket.requester.name} <span style=\"display: inline-block;\">&lt;#{@ticket.requester.email}&gt;</span></p>", ticket_requester_name_for_display
    end

    it "returns just the requester name in RTL when no email is available" do
      @ticket.requester.stubs(:email).returns(nil)
      hebrew = translation_locales(:hebrew)
      current_user.translation_locale = hebrew
      current_user.save!

      assert_equal "<p>#{@ticket.requester.name}</p>", ticket_requester_name_for_display
    end
  end

  describe "#ticket_field_print_title" do
    it "renders DC" do
      Zendesk::Liquid::DcContext.expects(:render).returns("WITH DC")
      assert_equal "WITH DC", ticket_field_print_title("field name!")
    end
  end

  describe "#new_user_link" do
    it "includes an inline value in the query string" do
      new_user_link('email').must_include "inline=true"
    end
  end

  describe "#set_page_title" do
    it "sets the page title based on the ticket's nice id" do
      @ticket.stubs(:title).returns("Some Title")
      @ticket.stubs(:nice_id).returns("1234")

      assert_equal %(#1234 "Some Title"), page_title
    end
  end

  describe "#show_ticket_tags?" do
    describe "when the ticket tags setting is enabled" do
      before do
        current_account.settings.enable(:ticket_tagging)
      end

      it "returns false for users with no edit_tags permission and ticket has no tags" do
        current_user.expects(:can?).with(:edit_tags, Ticket).returns(false)
        @ticket.stubs(:current_tags).returns("")
        refute show_ticket_tags?(@ticket)
      end

      it "returns true for users with no edit_tags permission but ticket has tags" do
        current_user.stubs(:can?).with(:edit_tags, Ticket).returns(false)
        @ticket.stubs(:current_tags).returns("foo bar")
        assert show_ticket_tags?(@ticket)
      end

      it "returns false for users with edit_tags permission but limited access to that ticket" do
        current_user.stubs(:can?).with(:edit_tags, Ticket).returns(true)
        current_user.expects(:can?).with(:only_create_ticket_comments, @ticket).returns(true)
        refute show_ticket_tags?(@ticket)
      end

      it "returns true for users with edit_tags permission and full access to that ticket" do
        current_user.stubs(:can?).with(:edit_tags, Ticket).returns(true)
        current_user.expects(:can?).with(:only_create_ticket_comments, @ticket).returns(false)
        assert show_ticket_tags?(@ticket)
      end

      it "returns true for users with edit_tags permission and no ticket is given" do
        current_user.stubs(:can?).with(:edit_tags, Ticket).returns(true)
        assert show_ticket_tags?(nil)
      end

      it "returns false for users with edit_tags permission and no ticket is given" do
        current_user.stubs(:can?).with(:edit_tags, Ticket).returns(false)
        refute show_ticket_tags?(nil)
      end
    end

    describe "when the ticket tags setting is disabled" do
      before do
        current_account.settings.disable(:ticket_tagging)
      end

      it "returns false even if the user has edit_tags permission and full access to that ticket" do
        current_user.stubs(:can?).with(:edit_tags, Ticket).returns(true)
        current_user.stubs(:can?).with(:only_create_ticket_comments, @ticket).returns(false)
        refute show_ticket_tags?(@ticket)
      end
    end
  end

  describe "#can_edit_ticket_tags?" do
    it "returns false for users with no edit_tags permission" do
      current_user.expects(:can?).with(:edit_tags, Ticket).returns(false)
      refute can_edit_ticket_tags?(@ticket)
    end

    it "returns false for users with edit_tags permission but limited access to that ticket" do
      current_user.stubs(:can?).with(:edit_tags, Ticket).returns(true)
      current_user.expects(:can?).with(:only_create_ticket_comments, @ticket).returns(true)
      refute can_edit_ticket_tags?(@ticket)
    end

    it "returns true for users with edit_tags permission and full access to that ticket" do
      current_user.stubs(:can?).with(:edit_tags, Ticket).returns(true)
      current_user.expects(:can?).with(:only_create_ticket_comments, @ticket).returns(false)
      assert can_edit_ticket_tags?(@ticket)
    end
  end

  describe "#public_comment_label" do
    describe "with a Ticket that belongs to an Entry as the argument" do
      before { @ticket.build_entry }

      it "returns a message about the Entry" do
        assert_equal "Also post this as a comment in the linked topic.", public_comment_label(@ticket)
      end
    end

    describe "with a nil argument" do
      describe_with_arturo_setting_enabled :collaboration_enabled do
        it "returns a message that the requester can see the comment" do
          assert_equal "Requester can see this comment (public comment).", public_comment_label(nil)
        end
      end

      describe_with_arturo_setting_disabled :collaboration_enabled do
        it "returns a message that the requester can see the comment" do
          assert_equal "Requester can see this comment (public comment).", public_comment_label(nil)
        end
      end
    end
  end

  describe "#private_comment_label" do
    describe "with a nil argument" do
      it "returns a message about the Requester" do
        assert_equal "This comment will not be seen by the requester.", private_comment_label(nil)
      end
    end

    describe "for a Ticket that does not belong to an Entry" do
      it "returns a message about the Requester" do
        assert_equal "This comment will not be seen by the requester.", private_comment_label(@ticket)
      end
    end

    describe "for a Ticket that belongs to an Entry" do
      before { @ticket.build_entry }

      it "returns a message about the Entry" do
        assert_equal "This comment will not be posted as a comment in the linked topic.", private_comment_label(@ticket)
      end
    end
  end

  describe "#author_name_for" do
    before do
      @comment = tickets(:minimum_1).comments.last
    end

    it "returns a author name for a non-system comment" do
      assert_equal @comment.author.name, author_name_for(@comment)
    end

    it "returns a author name for a system comment for non-twickets" do
      @comment.stubs(:author).returns(User.system)
      @comment.ticket.stubs(:twitter_ticket_source).returns(nil)
      assert_equal @comment.author.name, author_name_for(@comment)
    end
  end

  describe "#requester_name" do
    before do
      @ticket = tickets(:minimum_1)
    end
    it "returns requester name if possible" do
      assert_equal @ticket.requester.name, requester_name(@ticket)
    end
    it "returns empty string if no requester" do
      @ticket.requester = nil
      assert_equal "", requester_name(@ticket)
    end
    it "returns empty string if no ticket!" do
      assert_equal "", requester_name(nil)
    end
  end

  describe "#requester_header" do
    before do
      @ticket = tickets(:minimum_1)
    end

    it "returns header with requester name and requester date" do
      assert requester_header(@ticket).index(format_date(@ticket.created_at, with_time: true))
      assert requester_header(@ticket).index(@ticket.requester.name)
    end
  end

  describe "#next_ticket_in_collection" do
    before do
      stubs(:params).returns(col: 0)
      stubs(:get_page).returns(1)
      @key = tickets_in_collection_cache_key(0)
      Rails.cache.write(@key, [1, 2, 3], expires_in: 3.hours)
    end

    it "returns next ticket nice id in collection when available" do
      assert_equal 3, next_ticket_in_collection(2)
    end

    it "retrieves and return next ticket nice id when collection not available" do
      Rails.cache.write(@key, nil, expires_in: 3.hours)
      expects(:tickets_in_page).with(1).returns([1, 2, 3])
      assert_equal 3, next_ticket_in_collection(2)
    end

    it "retrieves and cache result" do
      Rails.cache.write(@key, nil, expires_in: 3.hours)
      expects(:tickets_in_page).with(1).returns([1, 2, 3]).once
      assert_equal 3, next_ticket_in_collection(2)
      assert_equal 3, next_ticket_in_collection(2)
    end

    it "retrieves and return ticket nice id from next collection if not available and last ticket" do
      expects(:tickets_in_page).with(2).returns([4, 5, 6])
      assert_equal 4, next_ticket_in_collection(3)
      assert_equal 2, params[:next_page]
    end

    it "returns nil if not available and not last ticket" do
      assert_nil next_ticket_in_collection(12)
    end
  end

  describe "#tickets_in_page" do
    it "finds ticket nice ids for page" do
      @rule = Rule.new
      @rule.id = 123
      stubs(:params).returns(col: 123, order: 'requester', desc: 1)
      current_account.rules.expects(:find_by_id).with(123).returns(@rule)
      tickets = [mock('ticket', nice_id: 1), mock('ticket', nice_id: 2)]
      @rule.expects(:find_tickets).with(
        current_user,
        paginate: true,
        order: params[:order],
        desc: params[:desc],
        output_type: params[:output_type],
        page: 4,
        backend: params[:backend],
        caller: 'backend'
      ).returns(tickets)

      expects(:set_tickets_in_collection_cache_and_map_comments).with(tickets, 123)
      tickets_in_page(4)
    end

    it "finds nothing if can't find rule" do
      refute(Rule.find_by_id(123456))
      stubs(:params).returns(col: 123456)
      assert_equal [], tickets_in_page(1)
    end
  end

  describe "#next_ticket_page" do
    it "returns next page if present" do
      stubs(:params).returns(next_page: 2)
      assert_equal 2, next_ticket_page
    end

    it "returns current page if no next page" do
      stubs(:params).returns({})
      stubs(:get_page).returns(1)
      assert_equal 1, next_ticket_page
    end
  end

  describe "#ticket_stats(record)" do
    it "returns correct tickets stats for a requester" do
      data = ticket_stats(users('minimum_end_user'))
      assert_equal 3, data[:working][:count]
      assert_equal 2, data[:recently_solved][:count]
      assert_nil data[:archived]
      assert_equal 0, data[:assigned][:count]
    end

    it "returns correct tickets stats for an agent" do
      data = ticket_stats(users('minimum_agent'))
      assert_equal 1, data[:working][:count]
      assert_equal 2, data[:recently_solved][:count]
      assert_nil data[:archived]
      assert_nil data[:assigned]
    end

    it "returns correct tickets stats for an organization" do
      data = ticket_stats(organizations('minimum_organization1'))
      assert_equal 3, data[:working][:count]
      assert_equal 2, data[:recently_solved][:count]
      assert_nil data[:archived]
      assert_equal 0, data[:assigned][:count]
    end

    it "returns correct tickets stats for group" do
      data = ticket_stats(groups('minimum_group'))
      assert_equal 3, data[:working][:count]
      assert_equal 0, data[:recently_solved][:count]
      assert_nil data[:archived]
      assert_equal 0, data[:assigned][:count]
    end

    describe "archived tickets" do
      before do
        @account = accounts('minimum')
      end

      it "returns correct tickets stats for a requester" do
        data = ticket_stats(users('minimum_end_user'))
        assert_equal 3, data[:working][:count]
        assert_equal 2, data[:recently_solved][:count]
        assert_equal 5, data[:all][:count]
        assert_nil data[:archived]
        assert_equal 0, data[:assigned][:count]
      end

      it "returns correct tickets stats for an agent" do
        data = ticket_stats(users('minimum_agent'))
        assert_equal 1, data[:working][:count]
        assert_equal 2, data[:recently_solved][:count]
        assert_equal 3, data[:all][:count]
        assert_nil data[:archived]
        assert_nil data[:assigned]
      end

      it "returns correct tickets stats for an organization" do
        data = ticket_stats(organizations('minimum_organization1'))
        assert_equal 3, data[:working][:count]
        assert_equal 2, data[:recently_solved][:count]
        assert_equal 5, data[:all][:count]
        assert_nil data[:archived]
        assert_equal 0, data[:assigned][:count]
      end

      it "returns correct tickets stats for group" do
        data = ticket_stats(groups('minimum_group'))
        assert_equal 3, data[:working][:count]
        assert_equal 0, data[:recently_solved][:count]
        assert_equal 3, data[:all][:count]
        assert_nil data[:archived]
        assert_equal 0, data[:assigned][:count]
      end
    end
  end

  describe "#ticket_stats_render(record)" do
    it "returns render tickets stats for a requester" do
      assert ticket_stats_render(users('minimum_end_user')).is_a?(String)
    end
  end

  describe "#ticket_pagination_col" do
    it "returns impromptu rule id" do
      stubs(:params).returns(col: 0)
      assert_equal 0, ticket_pagination_col
    end

    it "returns given rule id" do
      @rule = Rule.new
      @rule.id = 123
      assert_equal 123, ticket_pagination_col
    end
  end

  describe "#ticket_pagination_options" do
    it "has col, page, order, desc" do
      stubs(:ticket_pagination_col).returns(20)
      stubs(:params).returns(order: 'requester', desc: 1)
      stubs(:get_page).returns(1)
      assert_equal({ order: "requester", col: 20, page: 1, desc: 1 }, ticket_pagination_options)
    end
  end

  describe '#ticket_path_for_collection(nice_id)' do
    before do
      @ticket = Ticket.first
      stubs(:controller_name).returns("tickets")
      stubs(:get_page).returns(1)
    end

    it 'returns request path for end-user' do
      stubs(:current_user).returns(users(:minimum_end_user))
      assert_equal "/requests/#{@ticket.nice_id}", ticket_path_for_collection(@ticket.nice_id)
    end

    it 'returns ticket path for agent with collection for impromptu rule id' do
      stubs(:params).returns(col: 0)
      assert_equal "/tickets/#{@ticket.nice_id}?col=0&page=1", ticket_path_for_collection(@ticket.nice_id)
    end

    it 'returns ticket path for agent with collection for given rule id' do
      stubs(:params).returns({})
      @rule = Rule.new
      @rule.id = 123
      assert_equal "/tickets/#{@ticket.nice_id}?col=123&page=1", ticket_path_for_collection(@ticket.nice_id)
    end

    it 'returns ticket path with page for next ticket' do
      stubs(:ticket_pagination_options).returns(col: 20)
      stubs(:next_ticket_page).returns(30)
      assert_equal "/tickets/#{@ticket.nice_id}?col=20&page=30", ticket_path_for_collection(@ticket.nice_id)
    end
  end

  describe '#ticket_table_path_for_record' do
    it 'returns user profile path to record' do
      assert_equal "/users/#{users('minimum_end_user').id}?order=status", ticket_table_path_for_record(users('minimum_end_user'))
    end

    it 'returns organization profile path with tickets selected' do
      assert_equal "/organizations/#{organizations('minimum_organization1').id}?order=status&select=tickets", ticket_table_path_for_record(organizations('minimum_organization1'))
    end

    it 'returns group profile path with tickets table selected' do
      assert_equal "/groups/#{groups('minimum_group').id}?order=status&select=tickets", ticket_table_path_for_record(groups('minimum_group'))
    end
  end

  describe '#should_show_channel_back_options?' do
    before do
      @ticket = Ticket.new
    end

    it 'displays if there is an MTH and the requester has a twitter account' do
      refute should_show_channel_back_options?

      requester = OpenStruct.new(has_twitter_identity?: true)
      @ticket.stubs(:requester).returns(requester)

      refute should_show_channel_back_options?

      @ticket.via_id = ViaType.TWITTER

      assert should_show_channel_back_options?
    end
  end

  describe '#should_show_facebook_back_options?' do
    before do
      @ticket = Ticket.new
    end

    describe "when the ticket is via facebook" do
      before do
        @ticket.stubs(:via_facebook?).returns(true)
      end

      it "returns true" do
        assert should_show_facebook_back_options?
      end
    end

    describe "when the ticket is not via facebook" do
      before do
        @ticket.stubs(:via_facebook?).returns(false)
      end

      it "returns true" do
        refute should_show_facebook_back_options?
      end
    end
  end

  describe 'Given an inverse agreement relationship' do
    before do
      attributes = { status: :accepted, account: current_account }

      @agreement1 = FactoryBot.create(:agreement, attributes.merge(direction: :in))
      @agreement2 = FactoryBot.create(:agreement, attributes.merge(direction: :out))

      assert !eligible_agreements_for_sharing.empty?
    end

    describe 'Given a ticket that has not been shared' do
      before do
        @ticket = tickets(:minimum_1)
      end

      describe '#eligible_agreements_for_dropdown' do
        it 'includes the outbound agreement' do
          agreements = eligible_agreements_for_dropdown(@ticket)
          assert agreements.include?(@agreement2), agreements.inspect
        end
      end
    end

    describe 'Given a ticket that has been shared in that relationship' do
      before do
        @ticket = tickets(:minimum_1)
        SharedTicket.create!(ticket: @ticket, agreement: @agreement1)
      end

      describe '#eligible_agreements_for_dropdown' do
        it 'does not include the outbound agreement' do
          agreements = eligible_agreements_for_dropdown(@ticket)
          refute agreements.include?(@agreement2), agreements.inspect
        end
      end
    end
  end

  describe "#is_twicket_and_twitter_profiles_are_present?" do
    it "returns true if has tweet back handle twitter screen name and requester twitter screen name" do
      @ticket.stubs(:twitter_ticket_source).returns(stub(twitter_screen_name: 'name'))
      @ticket.stubs(:requester).returns(stub(twitter_profile: stub(screen_name: 'name')))
      assert is_twicket_and_twitter_profiles_are_present?
    end

    it "returns false if has tweet back handle twitter screen name but not requester twitter screen name" do
      @ticket.stubs(:twitter_ticket_source).returns(stub(twitter_screen_name: 'name'))
      @ticket.stubs(:requester).returns(stub(twitter_profile: stub(screen_name: nil)))
      refute is_twicket_and_twitter_profiles_are_present?
    end

    it "returns false if does not have tweet back handle twitter screen name and has requester twitter screen name" do
      @ticket.stubs(:twitter_ticket_source).returns(stub(twitter_screen_name: nil))
      @ticket.stubs(:requester).returns(stub(twitter_profile: stub(screen_name: 'name')))
      refute is_twicket_and_twitter_profiles_are_present?
    end

    it "returns false if does not have tweet back handle twitter screen name and does hot have requester twitter screen name" do
      @ticket.stubs(:twitter_ticket_source).returns(stub(twitter_screen_name: nil))
      @ticket.stubs(:requester).returns(stub(twitter_profile: stub(screen_name: nil)))
      refute is_twicket_and_twitter_profiles_are_present?
    end

    it "returns false if no ticket" do
      @ticket = nil
      refute is_twicket_and_twitter_profiles_are_present?
    end
  end

  describe "#recent_ticket_link_to" do
    it "links to ticket path" do
      ticket = tickets(:minimum_1)
      recent_ticket_link_to(ticket).must_include "href=\"#{main_app.ticket_path(ticket)}\""
    end
  end
end
