require_relative '../support/test_helper'
require_relative '../support/rule'

SingleCov.covered! uncovered: 144

describe RulesHelper do
  include CustomFieldsTestHelper
  include TestSupport::Rule::Helper

  fixtures :accounts,
    :users,
    :tickets,
    :groups,
    :translation_locales,
    :ticket_fields,
    :custom_field_options

  def current_user;
    users(:minimum_agent);
  end

  def current_account;
    current_user.account;
  end

  # Gives a rounded approximation of what this number is
  def approximate(number)
    factor = if number < 1000
      10
    else
      (number < 10000 ? 50 : 100)
    end
    approx = factor * (number / factor.to_f).round
    approx
  end

  it "tests rule count approximations" do
    assert_equal 150, approximate(149)
    assert_equal 150, approximate(150)
    assert_equal 220, approximate(220)
    assert_equal 920, approximate(920)
    assert_equal 1200, approximate(1220)
    assert_equal 1400, approximate(1420)
    assert_equal 1700, approximate(1720)
    assert_equal 1750, approximate(1770)
    assert_equal 3750, approximate(3770)
    assert_equal 9700, approximate(9720)
    assert_equal 11700, approximate(11720)
    assert_equal 98600, approximate(98620)
  end

  def ticket_path_for_collection(*_args)
    ""
  end

  describe '#can_create_rule?' do
    %w[triggers automations].each do |rule_type|
      describe rule_type do
        let(:klass) { rule_type.capitalize.singularize.constantize }

        it 'returns true when current user can edit rules' do
          current_user.expects(:can?).with(:edit, klass).returns(true)
          assert can_create_rule?(rule_type)
        end

        it 'returns false when current user cannot edit rules' do
          current_user.expects(:can?).with(:edit, klass).returns(false)
          refute can_create_rule?(rule_type)
        end
      end
    end

    %w[macros views].each do |rule_type|
      describe rule_type do
        let(:klass) { rule_type.capitalize.singularize.constantize }

        describe 'when personal rules are selected' do
          before do
            params[:select] = 'personal'
          end

          it 'should return true when the user can manage personal views' do
            current_user.expects(:can?).with(:manage_personal, klass).returns(true)
            assert can_create_rule?(rule_type)
          end

          it 'should return false when the user cannot manage personal views' do
            current_user.expects(:can?).with(:manage_personal, klass).returns(false)
            refute can_create_rule?(rule_type)
          end
        end

        describe 'when group rules are selected' do
          it 'should return true if can_manage_group_or_shared' do
            expects(:can_manage_group_or_shared?).with(klass).returns(true)
            assert can_create_rule?(rule_type)
          end

          it 'should return false if can_manage_group_or_shared is false' do
            expects(:can_manage_group_or_shared?).with(klass).returns(false)
            refute can_create_rule?(rule_type)
          end
        end
      end
    end
  end

  describe '#filtered?' do
    let(:account) { current_account }
    let(:rule) { create_view }

    before do
      @rule = rule
    end

    describe 'when `skill_based_filtered_views` setting is available' do
      describe 'and view is a skill based filtered view' do
        before do
          account.settings.stubs(skill_based_filtered_views: [rule.id])
        end

        it 'returns true' do
          assert filtered?
        end
      end

      describe 'and view is a not skill based filtered view' do
        it 'returns false' do
          refute filtered?
        end
      end
    end

    describe 'when `skill_based_filtered_views` setting is not available' do
      before do
        account.settings.stubs(skill_based_filtered_views: nil)
      end

      it 'returns false' do
        refute filtered?
      end
    end
  end

  describe '#can_edit_rule?' do
    let(:account) { current_account }
    let(:rule)    { create_view }

    let(:installations) { {} }

    before do
      Rule.stubs(apps_installations: installations)

      @rule = rule
    end

    describe 'when the rule is new' do
      before { rule.stubs(new_record?: true) }

      it 'returns true' do
        assert can_edit_rule?
      end
    end

    describe 'when the user can edit the rule' do
      before { current_user.stubs(:can?).with(:edit, rule).returns(true) }

      it 'returns true' do
        assert can_edit_rule?
      end

      describe 'and the rule is an app requirement' do
        let(:installations) do
          {rule.id => {'settings' => {'title' => 'Test app'}}}
        end

        it 'returns false' do
          refute can_edit_rule?
        end
      end
    end

    describe 'when the user cannot edit the rule' do
      before { current_user.stubs(:can?).with(:edit, rule).returns(false) }

      it 'returns false' do
        refute can_edit_rule?
      end
    end
  end

  describe '#app_installation_title' do
    let(:account)   { current_account }
    let(:rule)      { create_view }
    let(:app_title) { 'Test app installation' }

    before do
      Rule.stubs(
        apps_installations: {rule.id => {'settings' => {'title' => app_title}}}
      )

      @rule = rule
    end

    it 'returns the app installation title' do
      assert_equal(
        I18n.t('txt.admin.view.app_requirement.title', title: app_title),
        app_installation_title
      )
    end
  end

  describe "#build_set" do
    it "creates a set with the given conditions and index" do
      user = users(:minimum_agent)
      set1 = {
        "1" => {
          conditions: {
            "0" => { operator: "is", source: "assignee_id", value: { "0" => user.id }},
            "1" => { operator: "is", source: "status_id", value: { "0" => 3 }},
            "2" => { operator: "is", source: "status_id", value: { "0" => 4 }}
          }
        }
      }
      assert_equal set1, build_set([assignee_condition(user), solved_condition])

      set2 = {
        "2" => {
          conditions: {
            "0" => { value: { "0" => user.id }, source: "requester_id", operator: "is"},
            "1" => {value: {"0" => 0}, source: "status_id", operator: "is"},
            "2" => {value: {"0" => 1}, source: "status_id", operator: "is"},
            "3" => {value: {"0" => 2}, source: "status_id", operator: "is"}
          }
        }
      }
      assert_equal set2, build_set([requester_condition(user), working_condition], "2")
    end

    it "has composable results" do
      # Kind of silly to test this, as it's basically adding coverage to Hash#merge. But
      # we end up relying on this behavior, so it's more than likely to be a good thing
      # to describe it here.

      conditions_all = build_condition('is', users(:minimum_agent).id, 'assignee_id')
      conditions_any = working_condition
      conditions = {}.merge(build_set(conditions_all)).merge(build_set(conditions_any, "2"))

      expected = {
        "2" => {
          conditions: {
            "1" => { value: { "0" => 1 }, source: "status_id", operator: "is" },
            "2" => { value: { "0" => 2 }, source: "status_id", operator: "is" },
            "0" => { value: { "0" => 0 }, source: "status_id", operator: "is" }
          }
        },
        "1" => {
          conditions: {
            "0" => {
              value: { "0" => users(:minimum_agent).id}, source: "assignee_id", operator: "is"
            }
          }
        }
      }
      assert_equal expected, conditions
    end
  end

  describe "#week_time_label" do
    before do
      Timecop.travel(Time.parse("2011-04-09"))
    end

    it "renders (current week) when the argument time is in the current week" do
      assert_equal "Week 14 (current week)", week_time_label(Time.now)
    end

    it "does not render (current week) when the argument time is not in the current week" do
      date = Date.parse("Jun 08 2011")
      assert_equal "Week #{date.cweek}", week_time_label(date.to_time)
    end

    it "renders the year if the argument time is not in the current year" do
      assert_equal "Week #{1.year.ago.to_date.cweek} #{1.year.ago.year}", week_time_label(1.year.ago)
    end

    it "works well when week is from a past year is returned by strftime" do
      date = Date.parse("Jan 01 2011")
      assert_equal "Week #{date.cweek}", week_time_label(date.to_time)
      date = Date.parse("Jan 01 2010")
      assert_equal "Week #{date.cweek} #{date.year}", week_time_label(date.to_time)
    end
  end

  describe '#view_header' do
    before do
      stubs(:current_user).returns(User.first)
      @field_class = ZendeskRules::RuleField.lookup(:requester_updated_at)
      @result = view_header(@field_class)
    end

    it 'uses the truncated label for the value' do
      assert_select_in @result, 'th', 'Requester ...'
    end

    it 'uses the full label for the title' do
      assert_select_in @result, 'th[title="Requester updated"]'
    end

    it 'does not have a width' do
      style = @result.gsub(/^.* style=['"](.+?)['"].*$/, '\1')
      assert_no_match(/\bwidth\b/, style)
    end

    it "has a CSS class that matches the field's identifier" do
      assert_select_in @result, "th.requester_updated_at"
    end

    describe 'with sorting_allowed = true and sorting on the current column' do
      before do
        stubs(:params).returns(order: @field_class.identifier)
        stubs(:url_for).returns('/foo/bar')
        @result = view_header(@field_class, true)
      end

      it 'has a link tag with the short title and an img tag' do
        assert_select_in @result, 'th a', 'Requester ...'
        assert_select_in @result, 'th a img'
      end
    end

    describe 'with sorting_allowed = true and no sorting on the current column' do
      before do
        stubs(:current_user).returns(users(:minimum_end_user))
        @field_class = ZendeskRules::RuleField.lookup(:solved_at)
        stubs(:params).returns(order: @field_class.identifier)
        stubs(:url_for).returns('/foo/bar')
        @result = view_header(@field_class, true)
      end

      it 'shorts title should not include a link or an img tag' do
        assert_select_in @result, 'th a', false, 'Requester ...'
        assert_select_in @result, 'th a img', false
      end
    end
  end

  describe '#ticket_field_value' do
    before do
      @ticket = tickets(:minimum_1)
      @group_field = ZendeskRules::RuleField.lookup(:group) # Zendesk::Rules::GroupField
    end

    it 'truncates per request' do
      assert_equal 'minimum_group', ticket_field_value(@ticket, @group_field)
      assert_equal 'mi...', ticket_field_value(@ticket, @group_field, 5)
    end

    describe "field indentifier :type" do
      before do
        @type_group_field = ZendeskRules::RuleField.lookup(:type)
      end

      it "returns dash if no value is present" do
        @type_group_field.stubs(:value).returns(nil)
        assert_equal '-', ticket_field_value(nil, @type_group_field)
      end

      it "returns the localize name if value is present" do
        @type_group_field.stubs(:value).returns(3)
        assert_equal "Problem", ticket_field_value(@tickets, @type_group_field)
      end
    end

    describe "field indentifier :brand" do
      before do
        @brand_field = ZendeskRules::RuleField.lookup(:brand)
      end

      it "returns the brand name" do
        @ticket.stubs(brand: mock(name: 'brand_name'))
        assert_equal 'brand_name', ticket_field_value(@ticket, @brand_field)
      end

      it "does not explode if the ticket has no brand" do
        @ticket.stubs(brand: nil)
        assert_equal '-', ticket_field_value(@ticket, @brand_field)
      end
    end

    describe "field identifier :assignee" do
      it "html encodes the value" do
        @assignee_field = ZendeskRules::RuleField.lookup(:assignee)
        user = stub
        user.stubs(:safe_name).returns('<script>')
        @ticket.stubs(assignee: user)
        assert_equal "&lt;script&gt;", ticket_field_value(@ticket, @assignee_field)
      end
    end

    describe "field identifier :group" do
      it "html encodes the value" do
        group = stub
        group.stubs(:name).returns('<script>')
        @ticket.stubs(group: group)
        @group_field = ZendeskRules::RuleField.lookup(:group) # Zendesk::Rules::GroupField
        assert_equal "&lt;script&gt;", ticket_field_value(@ticket, @group_field)
      end
    end

    describe "field identifier :ticket_form" do
      it "html encodes the value" do
        ticket_form_field = ZendeskRules::RuleField.lookup(:ticket_form)
        ticket_form_field.stubs(:humanized_value).returns('<script>')
        assert_equal "&lt;script&gt;", ticket_field_value(@ticket, ticket_form_field)
      end
    end
  end

  describe '#render_view_tickets_table_group_row' do
    fixtures :ticket_fields

    before do
      @ticket = tickets(:minimum_1)
      @grouping_field = ZendeskRules::RuleField.lookup(ticket_fields(:field_checkbox_custom).id)
    end

    it 'shows correct grouping field title' do
      expected = "#{@grouping_field.title(current_user.is_agent?)} -"
      assert_select_in render_view_tickets_table_group_row(@ticket, [], @grouping_field), 'tr.group_by td', expected
    end
  end

  describe '#render_view_tickets_table_ticket_rows_with_grouping' do
    before do
      stubs(:params).returns({})
      ActionView::Base.any_instance.stubs(:current_user).returns(users(:minimum_end_user)) # short-circuit ticket_path_for_collection
      ActionView::Base.any_instance.stubs(:ticket_path_for_collection).returns("/foo")
      @tickets = [tickets(:minimum_1), tickets(:minimum_2), tickets(:minimum_3), tickets(:minimum_4), tickets(:minimum_5)]
      @fields = []
      @output = stub(group: :created_at, order: :subject, order_asc: true)
      @rendered_view = render_view_tickets_table_ticket_rows_with_grouping(@tickets, @fields, @output, {})
    end

    it 'sorts the grouped tickets by view defined order field' do
      assert_not_nil pos = @rendered_view.index('group_by', 0)
      assert_not_nil pos = @rendered_view.index('nice-id="1"', pos)
      assert_not_nil pos = @rendered_view.index('group_by', pos)
      assert_not_nil pos = @rendered_view.index('nice-id="2"', pos)
      assert_not_nil pos = @rendered_view.index('nice-id="5"', pos)
      assert_not_nil pos = @rendered_view.index('nice-id="4"', pos)
      assert_not_nil       @rendered_view.index('nice-id="3"', pos)
    end
  end

  describe '#js_array' do
    it 'returns HTML-safe value' do
      assert js_array(["a & b"]).html_safe?
    end

    it 'returns html escaped json string' do
      assert_equal "[\"a &amp; &lt; &gt; &#039; &quot; b\"]", js_array(["a & < > ' \" b"])
    end
  end

  describe "#system_feature_url_for" do
    it "returns url for the salesforce feature page" do
      rule = stub(feature_identifier: 'salesforce_integration')
      result = system_feature_url_for(rule)
      assert_equal settings_extensions_path(anchor: "crm"), result
    end

    it "returns # if feature_type can't be matched" do
      target = stub(feature_identifier: 'DoesntExist')
      result = system_feature_url_for(target)
      assert_equal '#', result
    end
  end

  describe "#description_translations" do
    it "returns the correct description translation for each rule type" do
      assert_equal rule_description_translation("triggers"), I18n.t('txt.admin.views.rules.analysis.show.triggers_description1')
      assert_equal rule_description_translation("Triggers"), I18n.t('txt.admin.views.rules.analysis.show.triggers_description1')
      assert_equal rule_description_translation("automations"), I18n.t('txt.admin.views.rules.analysis.show.automations_description1')
      assert_equal rule_description_translation("Automations"), I18n.t('txt.admin.views.rules.analysis.show.automations_description1')
      assert_equal rule_description_translation("views"), I18n.t('txt.admin.views.rules.analysis.show.views_description1')
      assert_equal rule_description_translation("Views"), I18n.t('txt.admin.views.rules.analysis.show.views_description1')
      assert_equal rule_description_translation("macros"), I18n.t('txt.admin.views.rules.analysis.show.macros_description')
      assert_equal rule_description_translation("Macros"), I18n.t('txt.admin.views.rules.analysis.show.macros_description')
      assert_equal rule_description_translation("somethingelse"), ''
      assert_equal rule_description_translation(nil), ''
    end
  end

  describe "rendering of DC for ticket fields" do
    before do
      @japanese = translation_locales(:japanese)
      @spanish = translation_locales(:spanish)

      @account = accounts(:minimum)
      @tagger_field = ticket_fields(:field_tagger_custom)
      @ticket = @account.tickets.first
      @ticket.additional_tags = "dc_content"
      @ticket.will_be_saved_by(users(:minimum_admin))
      @ticket.save!

      @user = @ticket.requester
      @user.account.locale_id = @japanese.id

      stubs(:current_user).returns(@user)
      stubs(:current_account).returns(@account)

      fallback_locale = translation_locales(:english_by_zendesk)
      @fbattrs = {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: fallback_locale.id }
      @cms_text = Cms::Text.create!(name: "Welcome Wombat", fallback_attributes: @fbattrs, account: @ticket.account)
      @cms_japanese_variant = @cms_text.variants.create!(active: true, translation_locale_id: @japanese.id, value: "Japanese Value!")
      @cms_spanish_variant = @cms_text.variants.create!(active: true, translation_locale_id: @spanish.id, value: "ello! wotcher! wombats are awesome")
    end

    it "renders the DC value in rules for macros" do
      taggers = render_dc_in_custom_field(@account.active_taggers.all, current_account, current_user, {})
      custom_field_with_dc = taggers.first.custom_field_options.last
      assert_equal "Fun factor Japanese Value!", taggers.first.title
      assert_equal "Japanese Value!", custom_field_with_dc.name
    end

    it "renders the DC values in ticket fields in view" do
      value = ticket_field_value(@ticket, ZendeskRules::RuleField.lookup(@tagger_field.id))
      assert_equal "Japanese Value!", value
    end

    it "renders the DC in the header of a view" do
      content = view_header(ZendeskRules::RuleField.lookup(@tagger_field.id))
      assert_equal '<th class="10045" title="Fun factor (portal) Japanese Value!">Fun factor...</th>', content
    end
  end

  describe "#include_available_for_me_radio_option?" do
    before { @rule = View.new }

    describe "when current_user cannot manage_personal View" do
      before { current_user.stubs(:can?).with(:manage_personal, @rule.class).returns(false) }

      it "returns false" do
        refute include_available_for_me_radio_option?
      end
    end

    describe "when current_user can manage_personal View" do
      before { current_user.stubs(:can?).with(:manage_personal, @rule.class).returns(true) }

      describe "and the rule owner is not a user" do
        before { @rule.stubs(owner: current_account) }

        it "returns true" do
          assert include_available_for_me_radio_option?
        end
      end

      describe "and the rule owner is a user" do
        describe "and the current user is the rule owner" do
          before { @rule.stubs(owner: current_user) }

          it "returns true" do
            assert include_available_for_me_radio_option?
          end
        end

        describe "and the current user is not the rule owner" do
          before { @rule.stubs(owner: users(:minimum_admin)) }

          it "returns false" do
            refute include_available_for_me_radio_option?
          end
        end
      end
    end
  end

  describe "#sanitize_definitions" do
    it "builds simple conditions" do
      context = Zendesk::Rules::Context.new(current_account, current_user, rule_type: :macro)
      result = sanitize_definitions(ZendeskRules::Definitions::Conditions, context, :condition, :all)
      assert_includes result, "[{\"title\":\"-- Click to select"
    end

    it "replaces line separators from ticket, user, and org custom field titles" do
      FieldText.create!(account: current_account, title: "z\u2028\u2029ZD2501348TicketField\u2028\u2029z", is_active: true)
      @account = current_account # used by custom field helpers below
      create_user_custom_field!("SanitizationUserField", "z\u2028\u2029ZD2501348UserField\u2028\u2029z", "Text", is_active: true)
      create_organization_custom_field!("SanitizationOrganizationField", "z\u2028\u2029ZD2501348OrganizationField\u2028\u2029z", "Text", is_active: true)
      context = Zendesk::Rules::Context.new(@account, current_user, rule_type: :trigger)
      result = sanitize_definitions(ZendeskRules::Definitions::Conditions, context, :condition, :all)
      assert_includes result, "\"title\":\"z\\u2028\\u2029ZD2501348TicketField\\u2028\\u2029z\""
      assert_includes result, "\"title\":\"z\\u2028\\u2029ZD2501348UserField\\u2028\\u2029z\""
      assert_includes result, "\"title\":\"z\\u2028\\u2029ZD2501348OrganizationField\\u2028\\u2029z\""
    end

    it "does not HTML escape characters like apostrophes (')" do
      FieldText.create!(account: current_account, title: "Zendesk's test ticket field", is_active: true)
      @account = current_account # used by custom field helpers below
      create_user_custom_field!("SanitizationUserField", "Zendesk's test user field", "Text", is_active: true)
      create_organization_custom_field!("SanitizationOrganizationField", "Zendesk's test organization field", "Text", is_active: true)
      context = Zendesk::Rules::Context.new(@account, current_user, rule_type: :trigger)
      result = sanitize_definitions(ZendeskRules::Definitions::Conditions, context, :condition, :all)
      assert_includes result, "\"title\":\"Zendesk's test ticket field\""
      assert_includes result, "\"title\":\"Zendesk's test user field\""
      assert_includes result, "\"title\":\"Zendesk's test organization field\""
    end
  end

  describe "Dynamic Content in selectables" do
    before do
      @japanese = translation_locales(:japanese)
      @spanish = translation_locales(:spanish)
      @ticket = tickets(:minimum_1)

      fallback_locale = translation_locales(:english_by_zendesk)
      @fbattrs = {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: fallback_locale.id }
      @cms_text = Cms::Text.create!(name: "Welcome Wombat", fallback_attributes: @fbattrs, account: @ticket.account)
      @cms_japanese_variant = @cms_text.variants.create!(account: @ticket.account, active: true, translation_locale_id: @japanese.id, value: "Japanese Value!")
      @cms_spanish_variant = @cms_text.variants.create!(account: @ticket.account, active: true, translation_locale_id: @spanish.id, value: "ello! wotcher! wombats are awesome")
      @output = stub(group: :created_at, order: :subject, order_asc: true)
      @output.stubs(:column_identifiers).returns([])
    end

    it "renders the DC" do
      user = users(:minimum_agent)
      user.translation_locale = @japanese

      selected = selectables(included: false)
      assert selected.include? ["Fun factor Japanese Value!", "10045"]
    end
  end

  describe "#recipient_emails" do
    let(:definition) { Definition.new }
    let(:rule)       { Trigger.new(definition: definition) }

    it "returns recipient email addresses list from definition items" do
      definition.conditions_all << DefinitionItem.new("recipient", "is", "foo@bar.com")
      assert_equal ["foo@bar.com"], recipient_emails(rule)
    end

    it "returns an empty array if there are no 'recipient' definition items" do
      assert_equal [], recipient_emails(rule)
    end

    it "does not raise an error if the rule does not have a definition" do
      assert_equal [], recipient_emails(Trigger.new)
    end
  end

  describe "#selectables" do
    before do
      @output = stub(group: :created_at, order: :subject, order_asc: true)
      @output.stubs(:column_identifiers).returns([])
    end

    it "does not return a list containing column elements with a nil identifier" do
      selected = selectables(included: false)
      assert_equal false, (selected.map { |t, _| t }.member? nil)
    end
  end
end
