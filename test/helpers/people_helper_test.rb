require_relative "../support/test_helper"

SingleCov.covered! uncovered: 7

describe PeopleHelper do
  fixtures :users, :accounts
  include PeopleHelper

  attr_reader :current_user
  def current_user
    users(:minimum_admin)
  end

  def current_account
    current_user.account
  end

  describe "Linking to agents with legacy permissions" do
    before do
      @user = users(:minimum_agent)
      @user.stubs(:has_permission_set?).returns(false)
      @user.expects(:is_legacy_agent?).returns(true)
    end

    it "provides a valid link" do
      assert_equal "<a href=\"/users?legacyagents=1\">Legacy Agents</a> (unassigned)", permission_set_name_or_link(@user)
    end
  end

  describe "Linking to agents with the light-agent role" do
    before do
      @permissionset = PermissionSet.create_light_agent!(current_account)
      @light_agent = users(:minimum_agent)
      @light_agent.update_attributes! name: "Light agent", permission_set: @permissionset
      @light_agent.stubs(:has_permission_set?).returns(true)
    end

    it "returns the 'Light agents' string" do
      assert_equal "<a href=\"/users?permission_set=" + @permissionset.id.to_s + "\">Light agent</a>", permission_set_name_or_link(@light_agent)
    end
  end

  describe "Group links" do
    describe "when the account has the groups feature" do
      before { current_account.stubs(:has_groups?).returns(true) }
      it "shows the group links" do
        people_browser.must_include "groups"
        people_creation_links.must_include "group"
      end
    end
    describe "when the account does not have the groups feature" do
      before { current_account.stubs(:has_groups?).returns(false) }
      it "doesn't show the group links" do
        refute_match /groups/, people_browser
        refute_match /group/, people_creation_links
      end
    end
  end

  describe "Organization links" do
    describe "when the account has the organizations feature" do
      before { current_account.stubs(:has_organizations?).returns(true) }
      it "shows the organization links" do
        people_browser.must_include "organizations"
        people_creation_links.must_include "organization"
      end
    end
    describe "when the account does not have the organizations feature" do
      before { current_account.stubs(:has_organizations?).returns(false) }
      it "doesn't show the organization links" do
        refute_match /organizations/, people_browser
        refute_match /organization/, people_creation_links
      end
    end
  end

  describe "when current account has user_and_organization_fields feature" do
    before { current_account.stubs(:has_user_and_organization_fields?).returns(true) }

    describe "and user tags setting is enabled" do
      before { current_account.settings.stubs(:has_user_tags?).returns(true) }

      it "shows the tags link in people browse links" do
        people_browser.must_include "tags"
      end
    end

    describe "and user tags setting is disabled" do
      before { current_account.settings.stubs(:has_user_tags?).returns(false) }

      it "does not show the tags link in people browse links" do
        refute_match /tags/, people_browser
      end
    end
  end

  describe "when current account does not have user_and_organization_fields feature" do
    before { current_account.stubs(:has_user_and_organization_fields?).returns(false) }

    describe "and user tags setting is enabled" do
      before { current_account.settings.stubs(:has_user_tags?).returns(true) }

      it "does not show the tags link in people browse links" do
        refute_match /tags/, people_browser
      end
    end

    describe "and user tags setting is disabled" do
      before { current_account.settings.stubs(:has_user_tags?).returns(false) }

      it "does not show the tags link in people browse links" do
        refute_match /tags/, people_browser
      end
    end
  end

  describe "#people_filter_title" do
    let(:controller_name) { 'users' }
    let(:params) { {} }

    it "defaults to a translated 'Users' string" do
      assert_equal "Users", people_filter_title
    end

    describe "with a search query" do
      let(:controller_name) { 'search' }

      before do
        instance_variable_set(:@query, 'foo')
      end

      it "returns the 'People' string" do
        assert_equal "People", people_filter_title
      end
    end

    describe "legacy agents" do
      let(:params) { { legacyagents: 1 } }

      it "returns the 'Legacy Agents' string" do
        assert_equal "Legacy Agents", people_filter_title
      end
    end

    describe "the hase non-user case" do
      let(:controller_name) { 'groups' }

      it "calls #get_localization_string_controller" do
        expects(:get_localization_string_controller)

        people_filter_title
      end
    end

    describe "a specific permission set" do
      let(:staff) { current_account.permission_sets.create!(name: "{{zd.staff_role}}") }
      let(:params) { { permission_set: staff.id } }

      before do
        Arturo.enable_feature!(:dc_in_role_names)
      end

      it "returns the translated name for the permission set" do
        assert_equal "Staff", people_filter_title
      end
    end

    describe "a base role" do
      let(:params) { { role: Role::ADMIN.id} }

      it "calls #get_translated_users_title" do
        expects(:get_translated_users_title)

        people_filter_title
      end
    end
  end

  describe "#get_translated_users_title" do
    [
      ["txt.helpers.people_helper.end_users_label", "0"],
      ["txt.helpers.people_helper.agents_label", ["4", "2"]],
      ["txt.admin.helpers.user_helper.administrators_label_plural", "2"],

       # now lets add some defensive checks

      ["txt.helpers.people_helper.end_users_label", [["0"]]],
      ["txt.helpers.people_helper.end_users_label", Role.find(0)],
      ["txt.admin.helpers.people.people_helper.people_label", ["4"]]
    ].each do |set|
      it "returns #{set.first} when passed #{set.last.inspect}" do
        assert_equal I18n.t(set.first), get_translated_users_title(set.last)
      end
    end

    it "returns #{Role.find(4).name.pluralize} when passed a role object" do
      assert_equal Role::AGENT.name.pluralize, get_translated_users_title(Role.find(4))
    end
  end

  describe "#get_localization_string_controller" do
    [
      %w[groups Groups],
      %w[organizations Organizations],
      %w[roles Roles],
      %w[users Users]
    ].each do |cname, string|
      describe "the #{cname} controller" do
        let(:controller_name) { cname }

        it "calls I18n.t" do
          I18n.expects(:t)
          get_localization_string_controller
        end

        it "returns the nice name for the controller" do
          assert_equal string, get_localization_string_controller
        end
      end
    end

    describe "an unknown controller" do
      let(:controller_name) { 'team' }

      it "returns the human readable version of the controller name" do
        assert_equal 'Teams', get_localization_string_controller
      end
    end
  end

  describe "#default_group?" do
    describe "when account has a default group" do
      let!(:current_account) do
        a = accounts(:minimum)
        a.groups.first.update_attributes!(default: true)
        a
      end

      describe "when `id` represents the default group for an account" do
        it "returns true" do
          assert default_group?(current_account.default_group.id)
        end
      end

      describe "when `id` represents a non-default group of an account" do
        it "returns false" do
          refute default_group?(-1)
        end
      end
    end

    describe "when account has no default group" do
      before { current_account.stubs(:default_group).returns(false) }

      it "returns false when passed any group id" do
        refute default_group?(123)
      end
    end
  end
end
