require_relative "../support/test_helper"

SingleCov.covered! uncovered: 30

describe RequestsHelper do
  fixtures :accounts, :tickets, :ticket_fields, :translation_locales, :custom_field_options, :organizations, :events, :organization_memberships

  include RequestsHelper

  before do
    @account = accounts(:minimum)
    stubs(:current_user).returns(@account.owner)
  end

  describe "all_comments_but_the_first" do
    before do
      @ticket = tickets(:minimum_1)
      @all_but_first = nil
    end

    it "Satisfies Mister Kenny Buckler that the new efficient method is the same as the old, braindead one" do
      first_comment = @ticket.comments.first
      inefficient_results = @ticket.comments.reject { |c| c == first_comment }
      assert_equal inefficient_results, all_comments_but_the_first(@ticket)
    end
  end

  describe "#ticket_form_visible_custom_fields" do
    before { @account = accounts(:minimum) }

    it "returns find ticket forms if not empty" do
      expects(:find_ticket_form).returns(TicketForm.new).twice
      TicketForm.any_instance.expects(ticket_fields: @account.ticket_fields.active.visible_in_portal.for_visible).once
      send(:ticket_form_visible_custom_fields)
    end

    it "returns current visible fields if find ticket form is empty" do
      stubs(:current_account).returns(@account)
      expects(:find_ticket_form).returns(nil).once
      @account.expects(ticket_fields: @account.ticket_fields.active.visible_in_portal.for_visible).once
      send(:ticket_form_visible_custom_fields)
    end
  end

  describe "when account has ticket_forms enabled" do
    before do
      @account = accounts(:minimum)
      stubs(:current_account).returns(@account)
      stubs(:current_user).returns(users(:minimum_end_user))
      @ticket = @account.tickets.first
      @account.stubs(:has_ticket_forms?).returns(true)
      @custom_ticket_field = ticket_fields(:field_text_custom)
      @custom_ticket_field2 = ticket_fields(:field_checkbox_custom)

      params = {
        ticket_form: {
          name: "Wombat", display_name: "Wombats are everywhere", position: 2,
          default: false, active: true, end_user_visible: false,
          ticket_field_ids: [@custom_ticket_field.id]
        }
      }
      @ticket_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
      @ticket_form.save!
      default_params = {
        ticket_form: {
          name: "Wombat Default", display_name: "Default", position: 1,
          default: true, active: true, end_user_visible: true,
          ticket_field_ids: [@custom_ticket_field2.id]
        }
      }
      @default_form = Zendesk::TicketForms::Initializer.new(@account, default_params).ticket_form
      @default_form.save!
    end

    it "renders all editable fields no matter what form is used, JS will handle hiding unnecessary ones" do
      fields = editable_custom_fields
      assert_equal @account.ticket_fields.active.visible_in_portal.editable_in_portal, fields
    end

    it "renders the organization field if requester has multiple organizations" do
      current_user.organizations << organizations(:minimum_organization2)

      fields = editable_custom_fields
      assert fields.any? { |field| field.is_a?(FieldOrganization) }

      form_fields = ticket_field_data(@default_form)
      assert form_fields.any? { |field| field[:type] == 'FieldOrganization' }
    end

    it "does not render the organization field if requester has no multiple organizations" do
      fields = editable_custom_fields
      refute fields.any? { |field| field.is_a?(FieldOrganization) }

      form_fields = ticket_field_data(@default_form)
      refute form_fields.any? { |field| field[:type] == 'FieldOrganization' }
    end

    it "renders visible fields only for the form specified" do
      @ticket.expects(:ticket_form_id).returns(@ticket_form.id).twice
      fields = visible_custom_fields
      assert fields.include?(@custom_ticket_field)
      refute fields.include?(@custom_ticket_field2)
    end

    it "renders end user ticket form names with dynamic content" do
      @ticket_form.update_attributes(display_name: "{{dc.wombat}}", end_user_visible: true)

      Zendesk::Liquid::DcContext.expects(:render).returns("Hello Wombat Form")
      Zendesk::Liquid::DcContext.expects(:render).returns("Default")

      options_for_enduser_ticket_forms.must_include "Hello Wombat Form"
    end

    describe "#options_for_enduser_ticket_forms" do
      it "selects - by default if no ticket form has been previously selected" do
        Ticket.any_instance.expects(:ticket_form_id).returns(nil)
        assert_equal "<option selected=\"selected\" value=\"-1\">-</option>\n<option value=\"#{@default_form.id}\">Default</option>", options_for_enduser_ticket_forms
      end

      it "selects the previously selected ticket form by default if the user has previously selected a form" do
        @ticket_form.update_attributes(end_user_visible: true)
        Ticket.any_instance.expects(:ticket_form_id).twice.returns(@ticket_form.id)
        assert_equal "<option value=\"-1\">-</option>\n<option value=\"#{@default_form.id}\">Default</option>\n<option selected=\"selected\" value=\"#{@ticket_form.id}\">Wombats are everywhere</option>", options_for_enduser_ticket_forms
      end
    end
  end

  describe "#render_visible_custom_fields" do
    before do
      @account = accounts(:minimum)
      @ticket = @account.tickets.first
      @subject_field = ticket_fields(:field_textarea_custom)
      @subject_field.stubs(:value).returns('<script>alert("test")</script>')
      stubs(:visible_custom_fields).returns([@subject_field])
      stubs(:current_account).returns(@account)
      @account.stubs(:has_ticket_forms?).returns(false)
    end

    it "escapes JavaScript from value" do
      @account.stubs(:ticket_fields).returns([@subject_field])
      value = render_visible_custom_fields(@ticket)

      refute value.include?('<script>alert("test")</script>')
      assert value.include?('&lt;script&gt;alert(&quot;test&quot;)&lt;/script&gt;')
    end

    it "escapes JavaScript" do
      @account.stubs(:ticket_fields).returns([@subject_field])
      value = render_visible_custom_fields(@ticket)

      refute value.include?('<script>alert("test")</script>')
      assert value.include?('&lt;script&gt;alert(&quot;test&quot;)&lt;/script&gt;')
    end

    describe "with DC" do
      before do
        @japanese = translation_locales(:japanese)
        @spanish = translation_locales(:spanish)

        @text_field_with_dc = ticket_fields(:field_with_dc)
        @field_tagger_with_dc = ticket_fields(:field_tagger_custom)
        @field_tagger_with_dc.stubs(:value).returns('dc_content')

        @account = accounts(:minimum)
        @ticket = @account.tickets.first
        @user = @ticket.requester
        @user.account.locale_id = @japanese.id
        stubs(:current_user).returns(@user)
        @account.stubs(:ticket_fields).returns([@subject_field])

        fallback_locale = translation_locales(:english_by_zendesk)
        @fbattrs = {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: fallback_locale.id }
        @cms_text = Cms::Text.create!(name: "Welcome Wombat", fallback_attributes: @fbattrs, account: @ticket.account)
        @cms_japanese_variant = @cms_text.variants.create!(active: true, translation_locale_id: @japanese.id, value: "Japanese Value!")
        @cms_spanish_variant = @cms_text.variants.create!(active: true, translation_locale_id: @spanish.id, value: "ello! wotcher! wombats are awesome")

        stubs(:visible_custom_fields).returns([@text_field_with_dc, @field_tagger_with_dc])
      end

      it "renders DC strings" do
        value = render_visible_custom_fields(@ticket)
        assert value.include?('<h4>Japanese Value!</h4>')
        assert value.include?('<h4>Fun factor (portal) Japanese Value!</h4><p style="margin-top:0px">Japanese Value!</p>')
      end
    end

    describe "#can_add_attachments?" do
      before do
        @account = accounts(:minimum)
        stubs(:current_account).returns(@account)
      end

      it "returns false if current_account doesn't have attaching enable" do
        @account.stubs(:is_attaching_enabled?).returns(false)
        refute can_add_attachments?
      end

      it "returns false if account has private attachment and it is an anonymous request" do
        @account.stubs(:is_attaching_enabled?).returns(true)
        @account.settings.stubs(:private_attachments?).returns(true)
        stubs(:is_anonymous_request?).returns(true)
        refute can_add_attachments?
      end

      it "returns true if account has private attachment and it is a login user" do
        @account.stubs(:is_attaching_enabled?).returns(true)
        @account.settings.stubs(:private_attachments?).returns(true)
        stubs(:is_anonymous_request?).returns(false)
        assert can_add_attachments?
      end

      it "returns true if account if account doesn't have private attachment and it is an anonymous user" do
        @account.stubs(:is_attaching_enabled).returns(true)
        @account.settings.stubs(:private_attachments?).returns(false)
        expects(:is_anonymous_request?).never
        assert can_add_attachments?
      end
    end
  end
end
