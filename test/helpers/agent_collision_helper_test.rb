require_relative "../support/test_helper"

SingleCov.covered!

describe 'AgentCollisionHelper' do
  fixtures :accounts, :subscriptions, :users

  class MockView
    include AgentCollisionHelper
    include ApiHelper

    attr_accessor :request, :current_account, :current_user

    def params
      request.params
    end

    def session
      request.session
    end

    def shared_session
      request.shared_session
    end
  end

  describe "AgentCollision Settings" do
    before do
      @view = MockView.new
      @view.request = ActionController::TestRequest.create
      @view.current_account = accounts(:minimum).tap(&:subscription) # pre-load the Subscription
    end

    describe '#show_agent_collision?' do
      describe 'when an end-user is signed in' do
        before do
          @view.current_user = users(:minimum_end_user)
        end
        it 'returns false even if the account has agent collision enabled' do
          @view.current_account.stubs(:has_agent_collision?).returns(true)
          refute @view.show_agent_collision?
        end
      end
    end
  end
end
