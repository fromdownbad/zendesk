require_relative "../../support/test_helper"

SingleCov.covered!

describe AutomaticAnswers::AuthenticationHelper do
  fixtures :tickets
  let(:auth_token) { AutomaticAnswersJwtToken.encrypted_token(1, 25, 5, 55, [30]) }
  let(:params) { {article_id: '20', auth_token: auth_token} }
  let(:ticket) { tickets(:minimum_1) }
  let(:token) { stub('token', ticket: ticket, value: 'CRC32VALUE') }

  describe "#valid_request?" do
    describe 'when auth_token is present' do
      describe 'when auth_token is valid' do
        it "returns true" do
          assert valid_request?(params)
        end
      end

      describe 'when auth_token is invalid' do
        let(:params) { {article_id: '20', auth_token: "1nval1d.@uth.t0ken", ticket_id: 'wrong_id'} }

        it "returns false" do
          refute valid_request?(params)
        end
      end
    end

    describe 'when auth token is not present' do
      let(:params) { {article_id: '20', token: token.value, ticket_id: ticket.nice_id} }

      it "returns false" do
        refute valid_request?(params)
      end
    end
  end

  describe '#jwt_token_params' do
    describe 'when auth_token is valid' do
      it "returns decrypted params" do
        assert jwt_token_params(params)
      end
    end

    describe 'when auth_token is invalid' do
      let(:auth_token) { "Some.malicious.string" }
      it "returns nil" do
        refute jwt_token_params(params)
      end
    end
  end

  describe '#valid_auth_token' do
    describe 'when auth_token is valid' do
      it 'validates request' do
        assert valid_auth_token?(params)
      end
    end

    describe 'when auth_token is invalid' do
      let(:auth_token) { "Some.malicious.string" }
      it 'invalidates the request' do
        refute valid_auth_token?(params)
      end
    end
  end
end
