require_relative '../support/test_helper'

SingleCov.covered! uncovered: 53

describe StatsHelper do
  fixtures :all

  describe '#validate_stats_options' do
    describe 'start and end times given' do
      before do
        stubs(:params).returns(start: Time.now.to_i, end: Time.now.to_i - 10)
      end

      it 'replies with a bad request if start > end' do
        expects(:head).with(:bad_request).once

        validate_stats_options
      end
    end

    describe 'page number given' do
      before do
        stubs(:params).returns(page: "bad'page")
      end

      it 'replies with a bad request if the page is invalid' do
        expects(:head).with(:bad_request).once

        validate_stats_options
      end
    end
  end

  describe '#find_subject' do
    let(:account) { accounts(:minimum) }

    before do
      stubs(:current_account).returns(account)
    end

    it 'returns a deleted group' do
      group = account.groups.create! name: 'delete me'
      group.update_attribute :is_active, false

      params[:object_type] = 'group'
      params[:object_id] = group.id
      find_subject
      assert_equal group.id, @stats_subject.id
    end

    it 'returns a deleted organization' do
      organization = organizations(:minimum_organization1)
      organization.soft_delete

      params[:object_type] = 'organization'
      params[:object_id] = organization.id
      find_subject

      assert_equal organization.id, @stats_subject.id
    end

    it 'returns a deleted agent' do
      agent = users(:minimum_agent)
      User.connection.execute("update users set is_active = false where id = #{agent.id}")

      params[:object_type] = 'agent'
      params[:object_id] = agent.id
      find_subject

      assert_equal agent.id, @stats_subject.id
    end

    it 'returns a deleted forum' do
      forum = forums(:questionable)
      forum.soft_delete

      params[:object_type] = 'forum'
      params[:object_id] = forum.id
      find_subject

      assert_equal forum.id, @stats_subject.id
    end

    it 'returns a deleted entry' do
      entry = entries(:ponies)
      entry.soft_delete

      params[:object_type] = 'entry'
      params[:object_id] = entry.id
      find_subject

      assert_equal entry.id, @stats_subject.id
    end

    it 'returns a deleted category' do
      category = categories(:minimum_category)
      category.soft_delete

      params[:object_type] = 'category'
      params[:object_id] = category.id
      find_subject

      assert_equal category.id, @stats_subject.id
    end
  end
end
