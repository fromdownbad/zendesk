require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Api::V2::Internal::SecuritySettingsErrorsFormatterHelper do
  describe '#hashify_errors' do
    let(:errors_array) do
      [
        'this error has no matching key',
        I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: '67.67.67.67.67'),
        I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: '222.222.222'),
        'Cannot customize password without selecting agent custom policy.',
        I18n.t('txt.admin.views.settings.security_policy.update.more_than_one_auth_type_selected'),
        'Cannot disable all remote auths with agent/end-user remote login enabled',
        'JWT and SAML remote authentications can\'t have the same priority',
        [
          I18n.t('txt.admin.models.remote_authentication.profile_does_not_support_saml'),
          I18n.t('txt.admin.models.remote_authentication.remote_login_url_is_not_url_v3'),
          I18n.t('txt.admin.models.remote_authentication.remote_logout_url_is_not_zendesk_v3'),
          I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: '3.3.3'),
          'Fingerprint:  is invalid',
          :saml
        ],
        [
          I18n.t('txt.admin.models.remote_authentication.profile_does_not_support_jwt'),
          'Secret is invalid',
          I18n.t('txt.admin.models.remote_authentication.remote_login_url_is_http_v3'),
          I18n.t('txt.admin.models.remote_authentication.remote_logout_url_is_too_long_v4',
            char_limit: RemoteAuthentication::MAXIMUM_URL_LENGTH),
          I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: '9.9.9'),
          I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: '1.1'),
          :jwt
        ]
      ]
    end

    let(:expected_hash) do
      {
        errors_without_keys: [
          'this error has no matching key',
          I18n.t('txt.admin.models.remote_authentication.profile_does_not_support_saml'),
          I18n.t('txt.admin.models.remote_authentication.profile_does_not_support_jwt')
        ],
        ip: {
          ip_ranges: [
            I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: '67.67.67.67.67'),
            I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: '222.222.222')
          ]
        },
        authentication: {
          agent: {
            password: {
              password_history_length: "Cannot customize password without selecting agent custom policy.",
              password_in_mixed_case: "Cannot customize password without selecting agent custom policy.",
              password_length: "Cannot customize password without selecting agent custom policy."
            },
            security_policy_id: "Cannot customize password without selecting agent custom policy.",
            google_login: I18n.t('txt.admin.views.settings.security_policy.update.more_than_one_auth_type_selected'),
            office_365_login: I18n.t('txt.admin.views.settings.security_policy.update.more_than_one_auth_type_selected')
          }
        },
        remote_authentications: {
          saml: {
            is_active: "Cannot disable all remote auths with agent/end-user remote login enabled",
            priority: "JWT and SAML remote authentications can't have the same priority",
            remote_login_url: I18n.t('txt.admin.models.remote_authentication.remote_login_url_is_not_url_v3'),
            remote_logout_url: I18n.t('txt.admin.models.remote_authentication.remote_logout_url_is_not_zendesk_v3'),
            ip_ranges: [
              I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: '3.3.3')
            ],
            fingerprint: I18n.t("txt.admin.models.remote_authentication.invalid_fingerprint"),
          },
          jwt: {
            is_active: "Cannot disable all remote auths with agent/end-user remote login enabled",
            priority: "JWT and SAML remote authentications can't have the same priority",
            remote_login_url: I18n.t('txt.admin.models.remote_authentication.remote_login_url_is_http_v3'),
            remote_logout_url: I18n.t('txt.admin.models.remote_authentication.remote_logout_url_is_too_long_v4',
              char_limit: RemoteAuthentication::MAXIMUM_URL_LENGTH),
            ip_ranges: [
              I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: '9.9.9'),
              I18n.t('txt.admin.models.remote_authentication.ip_range_invalid_v2', range: '1.1')
            ],
            shared_secret: I18n.t("txt.admin.models.remote_authentication.invalid_shared_secret"),
          }
        }
      }
    end

    let(:params) do
      {
        ip: {
          ip_ranges: '67.67.67.67.67 123.123.123.123 222.222.222',
        },
        authentication: {
          agent: {
            google_login: true,
            office_365_login: true,
            password: {
              password_history_length: '7',
              password_length: '10',
              password_in_mixed_case: false
            }
          }
        },
        remote_authentications: {
          saml: {
            is_active: true,
            remote_login_url: 'blah',
            fingerprint: '88:BD:4D',
            ip_ranges: '123.123.123.123 3.3.3',
            remote_logout_url: 'https://zendesk.com'
          },
          jwt: {
            is_active: true,
            remote_login_url: 'http://www.jwt-login-url.co.in',
            ip_ranges: '9.9.9 123.123.123.123 1.1',
            remote_logout_url: "https://#{"long" * 2048}.com",
            shared_secret: 'uVyPVc'
          }
        }
      }
    end

    it 'returns the correct errors hash' do
      assert_equal expected_hash, send(:hashify_errors, errors_array)
    end
  end
end
