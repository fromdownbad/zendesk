require_relative "../support/test_helper"

SingleCov.covered! uncovered: 2

describe AssetsHelper do
  attr_reader :template

  # also try test/functional/home_controller_test.rb
  describe "js_init" do
    before do
      @virtual_path = "foo/bar"
    end

    it "renders without arguments" do
      result = js_init
      assert_equal "<script>\n//<![CDATA[\nzd.jsInitializers.push([\"foo/bar\",[]]);\n//]]>\n</script>", result
      assert result.html_safe?
    end

    it "renders with arguments" do
      result = js_init_with_escape("baz", "zoing/blah")
      # escape of / will result in zoing\/blah
      assert_equal "<script>\n//<![CDATA[\nzd.jsInitializers.push([\"foo/bar\",[\"baz\",\"zoing\\/blah\"]]);\n//]]>\n</script>", result
      assert result.html_safe?
    end

    it "removes starting slash" do
      @virtual_path = "/foo/bar"
      result = js_init
      assert_equal "<script>\n//<![CDATA[\nzd.jsInitializers.push([\"foo/bar\",[]]);\n//]]>\n</script>", result
      assert result.html_safe?
    end

    it "sanitizes sneaky whitespace" do
      result = js_init("some\u2028sneaky\u2029stuff\r\nhere")
      assert_equal "<script>\n//<![CDATA[\nzd.jsInitializers.push([\"foo/bar\",[\"some\\nsneaky\\nstuff\\r\\nhere\"]]);\n//]]>\n</script>", result
      assert result.html_safe?
    end
  end
end
