require_relative "../support/test_helper"
require "action_view/test_case"
require "action_view/helpers"

SingleCov.covered! uncovered: 44

describe TabsHelper do
  def normalize_html(html)
    html.split("\n").map(&:strip).join("")
  end

  describe "#zd_enable_checkbox" do
    let(:account) { Account.new }

    it "builds a simple checkbox" do
      html = nil
      form_for(account) { |f| html = zd_enable_checkbox(f, :is_serviceable, "LABEL_TEXT") }

      checkbox_input = "<input type=\"checkbox\" value=\"1\" checked=\"checked\" name=\"account[is_serviceable]\" id=\"account_is_serviceable\" />"

      expected = <<-HTML
        <div class=\"enable_checkbox\">
          <input name=\"account[is_serviceable]\" type=\"hidden\" value=\"0\" />
          #{checkbox_input}
          <label class=\"checkbox\" for=\"account_is_serviceable\">LABEL_TEXT</label>
        </div>
      HTML
      assert_equal normalize_html(expected), normalize_html(html)
    end

    it "builds a nested checkbox" do
      account.settings.set(accept_wildcard_emails: true)
      html = nil
      form_for(account) { |f| html = zd_enable_checkbox(f.property_set(:settings), :accept_wildcard_emails, "LABEL_TEXT") }

      checkbox_input = "<input id=\"account_settings_accept_wildcard_emails\" name=\"account[settings][accept_wildcard_emails]\" type=\"checkbox\" value=\"1\" checked=\"checked\" />"

      expected = <<-HTML
        <div class=\"enable_checkbox\">
          <input name=\"account[settings][accept_wildcard_emails]\" type=\"hidden\" value=\"0\" />
          #{checkbox_input}
          <label class=\"checkbox\" for=\"account_settings_accept_wildcard_emails\">LABEL_TEXT</label>
        </div>
      HTML
      assert_equal normalize_html(expected), normalize_html(html)
    end
  end
end
