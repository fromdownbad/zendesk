require_relative "../support/test_helper"

SingleCov.covered! uncovered: 33

describe RulesAnalysisHelper do
  include ApplicationHelper

  fixtures :accounts, :subscriptions, :users, :tickets, :rules
  attr_accessor :current_account

  before do
    @current_account = accounts(:minimum)
    @rule_stats = RuleStats.new(@current_account)
    stubs(:form_authenticity_token).returns("12345")
    stubs(:params).returns({})
  end

  describe_with_arturo_enabled :use_get_for_rules_analysis_show do
    describe "#render_item" do
      it "renders a linked header for an item, with a count" do
        assert_equal '<h3><a href="/rules/analysis/assignee_id">Agent assignment <span class="sub-counter">(14)</span> <span class="follow_link">»</span></a></h3>',
          render_item('assignee_id')
      end
    end

    describe "#render_key" do
      it "renders a linked span for an ID key, with a count" do
        assert_equal '<span class="tag-wrapper"><a href="/rules/analysis/assignee_id?key=Agent+Minimum">gob</a>&nbsp;<span class="count" dir="ltr">(2)</span>&nbsp;</span><wbr />',
          render_key('assignee_id', users(:minimum_agent).to_s, 'gob', 2)
      end

      it "renders a linked span for a text key, with a count" do
        assert_equal '<span class="tag-wrapper"><a href="/rules/analysis/assignee_id?key=current_user">gob</a>&nbsp;<span class="count" dir="ltr">(2)</span>&nbsp;</span><wbr />',
          render_key('assignee_id', 'current_user', 'gob', 2)
      end
    end
  end

  describe_with_arturo_disabled :use_get_for_rules_analysis_show do
    describe "#render_item" do
      it "renders a linked header for an item, with a count" do
        assert_equal '<h3><a class="nube1" href="/rules/analysis/assignee_id?authenticity_token=12345">Agent assignment <span class="sub-counter">(14)</span> <span class="follow_link">»</span></a></h3>',
          render_item('assignee_id')
      end
    end

    describe "#render_key" do
      it "renders a linked span for an ID key, with a count" do
        assert_equal '<span class="tag-wrapper"><a class="nube1" data-key="Agent Minimum" href="/rules/analysis/assignee_id?authenticity_token=12345">gob</a>&nbsp;<span class="count" dir="ltr">(2)</span>&nbsp;</span><wbr />',
          render_key('assignee_id', users(:minimum_agent).to_s, 'gob', 2)
      end

      it "renders a linked span for a text key, with a count" do
        assert_equal '<span class="tag-wrapper"><a class="nube1" data-key="current_user" href="/rules/analysis/assignee_id?authenticity_token=12345">gob</a>&nbsp;<span class="count" dir="ltr">(2)</span>&nbsp;</span><wbr />',
          render_key('assignee_id', 'current_user', 'gob', 2)
      end
    end
  end

  describe "#item_title" do
    it "lookups item title" do
      assert_equal 'Agent assignment', item_title('assignee_id')
    end
  end

  describe "#key_title" do
    it "lookups a title for an ID key" do
      assert_equal ['Agent Minimum'], key_title('assignee_id', users(:minimum_agent).to_s)
    end

    it "Lookups a title for a text key" do
      assert_equal '(current user)', key_title('assignee_id', 'current_user')
    end

    it "Lookups a title for a blank key" do
      assert_equal '(blank)', key_title('assignee_id', '')
    end
  end

  describe "#render_matches" do
    it "lists all matches in a rule definition for a given source and value" do
      assert_equal 'Requester is not <strong>current user</strong>?',
        render_matches('requester_id', 'current_user', rules(:trigger_notify_requester_of_comment_update).definition, 'current user')
    end

    it "lists all matches in a rule definition for an empty assignee" do
      assert_equal "Assignee is <strong>blank</strong>?<br/>Assignee <span class=\'action-attenuate\'>=</span> (blank)",
        render_matches('assignee_id', '', rules(:trigger_notify_requester_of_comment_update_with_empty_assignee).definition, '')
    end
  end
end
