require_relative "../support/test_helper"

SingleCov.covered! uncovered: 18

describe EntriesHelper do
  describe "html content" do
    before do
      @text = "<script>alert('foo')</script><!-- bar --><h1>Hi!</h1> <p>This is a very simple text, dude.</p>"
    end

    describe "#clean" do
      it "removes multi-line comment" do
        string = "<!--\nCAN YOU\nEAT ME?\n-->yes"
        assert_equal("yes", clean(string))
      end
    end
  end
end
