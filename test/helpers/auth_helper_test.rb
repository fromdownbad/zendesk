require_relative "../support/test_helper"

SingleCov.covered!

describe AuthHelper do
  describe "#javascript_include_auth" do
    let(:account) { accounts(:minimum) }

    before do
      stubs(:current_account).returns(account)
      stubs(:current_brand).returns(account.default_brand)
      stubs(:current_route).returns(current_brand.route)
      stubs(:request).returns(stub(ssl?: true))
    end

    it "renders script with data" do
      current_account.enable_help_center!

      javascript_include_auth.must_include %(data-brand-id="#{current_brand.id}" data-auth-origin="#{current_brand.id},false,true" data-auth-domain="https://minimum.zendesk-test.com" data-return-to="http://test.host/" data-theme="hc" data-locale="1")
    end

    it "renders script with source" do
      assert_includes javascript_include_auth, %(src="/javascripts/zendesk/auth/v2/host.js)
    end

    describe "with passport arturo" do
      before do
        Arturo.enable_feature! :passport
      end

      it 'renders script with passport source' do
        assert_includes javascript_include_auth, %(src="/javascripts/zendesk/auth/v2/passport_host.js)
      end
    end
  end

  describe "#return_to_url" do
    let(:account) { accounts(:minimum) }

    it "returns the root_url if return_to is not present" do
      assert_equal "http://test.host/", return_to_url
    end

    describe "returns return_to if present with the current_route" do
      before { stubs(:current_route).returns(account.route) }

      it "works with just the path" do
        stubs(:params).returns(return_to: '/return/to/here')

        assert_equal "https://minimum.zendesk-test.com/return/to/here", return_to_url
      end

      it "adds a '/' to the path if needed" do
        stubs(:params).returns(return_to: 'attacker.com')

        assert_equal "https://minimum.zendesk-test.com/attacker.com", return_to_url
      end

      it "works with the full url" do
        stubs(:params).returns(return_to: 'http://minimum.zendesk-test.com/full/url')

        assert_equal "https://minimum.zendesk-test.com/full/url", return_to_url
      end

      it "passes along oauth parameters" do
        params = "client_id=zendesk_developer_portal&redirect_uri=https%3A%2F%2Fdeveloper.zd-dev.com%2Fauth%2Fzendesk%2Fcallback&response_type=code&scope=read&state=2a68963e7eb048405c5c9bf87e37d9be4f562e96b23799ca"
        stubs(:params).returns(return_to: "http://minimum.zendesk-test.com/full/url?#{params}")

        assert_equal "https://minimum.zendesk-test.com/full/url?#{params}", return_to_url
      end

      it "fallbacks to the root_url with invalid hosts" do
        stubs(:params).returns(return_to: "https://{}/agent/#/dashboard")

        assert_equal "http://test.host/", return_to_url
      end

      it "fallbacks to the root_url when it's not a string" do
        stubs(:params).returns(return_to: {'foo' => 'bar'})

        assert_equal "http://test.host/", return_to_url
      end

      it "doesn't allow return_to to the HTTP zendesk domain" do
        account.settings.ssl_enabled = false
        account.save!

        stubs(:params).returns(return_to: 'https://minimum.zendesk-test.com')

        assert_equal "https://minimum.zendesk-test.com/", return_to_url
      end

      it "does allow return_to to a HTTP hostmapped domain" do
        account.settings.ssl_enabled = false
        account.save!
        account.update_attribute(:host_mapping, 'minimum.example.com')

        stubs(:params).returns(return_to: 'http://minimum.example.com')

        assert_equal "http://minimum.example.com/", return_to_url
      end
    end
  end
end
