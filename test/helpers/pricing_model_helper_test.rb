require_relative "../support/test_helper"

SingleCov.covered!

describe PricingModelHelper do
  fixtures :accounts

  class TestController
    include PricingModelHelper

    def initialize(account)
      @account = account
    end

    def current_account
      @account
    end
  end

  describe 'plan_name helpers' do
    let(:account) { accounts(:minimum) }
    let(:subject) { TestController.new(account) }

    describe 'for legacy pricing model revision ZBC::Zendesk::PricingModelRevision::NEW_FORUMS' do
      it 'returns Starter for #small_plan_name' do
        assert_equal 'Starter', subject.small_plan_name
      end

      it 'returns Starter for #medium_plan_name' do
        assert_equal 'Regular', subject.medium_plan_name
      end

      it 'returns Starter for #large_plan_name' do
        assert_equal 'Plus', subject.large_plan_name
      end

      it 'returns Starter for #extra_large_plan_name' do
        assert_equal 'Enterprise', subject.extra_large_plan_name
      end
    end

    describe 'for patagonia pricing model revision' do
      before do
        Subscription.any_instance.expects(:pricing_model_revision).returns(7)
      end

      it 'returns Essential for #small_plan_name' do
        assert_equal 'Essential', subject.small_plan_name
      end

      it 'returns Starter for #medium_plan_name' do
        assert_equal 'Team', subject.medium_plan_name
      end

      it 'returns Starter for #large_plan_name' do
        assert_equal 'Professional', subject.large_plan_name
      end
      it 'returns Starter for #extra_large_plan_name' do
        assert_equal 'Enterprise', subject.extra_large_plan_name
      end
    end
  end
end
