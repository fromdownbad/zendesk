require_relative "../support/test_helper"

SingleCov.covered! uncovered: 9

describe SuspendedTicketsHelper do
  describe "#render_cause" do
    it "returns a translated cause" do
      SuspensionType.list.each do |translated_cause, value|
        assert_equal translated_cause, render_cause(value)
      end

      assert_equal I18n.t("txt.admin.helpers.suspended_tickets_helper.unknown_label"), render_cause(10000)
      assert_equal I18n.t("txt.admin.helpers.suspended_tickets_helper.unknown_label"), render_cause(nil)
    end
  end
end
