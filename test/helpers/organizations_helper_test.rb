require_relative "../support/test_helper"

SingleCov.covered!

describe OrganizationsHelper do
  fixtures :organizations, :accounts

  let(:organization) { organizations(:minimum_organization1) }
  let(:account) { accounts(:minimum) }

  before { stubs(:current_account).returns(account) }

  describe "with a large number of organization memberships" do
    before { Zendesk::CappedCounts::CappedNum.any_instance.stubs(:capped?).returns(true) }

    it "counts only up to 10,000 memberships" do
      regex = /LIMIT 10000\) subquery_for_count/
      assert_sql_queries(1, regex) do
        user_count_as_string(organization)
      end
    end

    it "returns '(10000+)'" do
      # stubbing capped? as false means assume over limit of 10,000
      assert_equal("(10000+)", user_count_as_string(organization))
    end
  end

  describe "with a small number of organization memberships" do
    it "still counts only up to 10,000 memberships" do
      regex = /LIMIT 10000\) subquery_for_count/
      assert_sql_queries(1, regex) do
        user_count_as_string(organization)
      end
    end

    it "returns the exact user count" do
      assert_equal("(2)", user_count_as_string(organization))
    end
  end
end
