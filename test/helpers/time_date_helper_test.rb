require_relative "../support/test_helper"

SingleCov.covered! uncovered: 6

describe TimeDateHelper do
  def current_user
    users(:minimum_agent)
  end

  describe "#format_absolute_date" do
    it "returns date" do
      assert_equal "February 13, 2009, 23:31", format_absolute_date(Time.at(1234567890))
    end

    it "returns date in timezone" do
      assert_equal "February 14, 2009, 00:31", format_absolute_date(Time.at(1234567890), time_zone: current_user.time_zone)
    end

    it "returns N/A on bad date" do
      assert_equal "N/A", format_absolute_date(nil)
    end
  end

  describe "#format_absolute_date_with_utc_alt" do
    it "returns date" do
      assert_equal "<span title=\"2009-02-13T23:31:30Z\">February 13, 2009, 23:31</span>", format_absolute_date_with_utc_alt(Time.at(1234567890))
    end

    it "returns - on bad date" do
      assert_equal "N/A", format_absolute_date_with_utc_alt(nil)
    end
  end

  describe "Time intervals" do
    it "splits time intervals in hours, minutes and seconds" do
      assert_equal [1, 0, 0], split_time_interval(3600)
      assert_equal [0, 1, 0], split_time_interval(60)
      assert_equal [0, 0, 1], split_time_interval(1)

      assert_equal [0, 1, 10], split_time_interval(70)

      assert_equal [0, 1, 59], split_time_interval(119)
    end

    it "formats time intervals into human readable hour/min/sec strings" do
      assert_equal("1 minute", format_time_interval(60))
      assert_equal("2 minutes, 10 seconds", format_time_interval(130))
      assert_equal("1 hour, 2 minutes, 10 seconds", format_time_interval(3600 + 130))
    end

    it "handles strings" do
      assert_equal [1, 0, 0], split_time_interval("3600")
    end
  end

  describe "Time intervals when included in other class" do
    before do
      @subject = Class.new { include TimeDateHelper }.new
    end

    it "splits time intervals in hours, minutes and seconds" do
      assert_equal [1, 0, 0], @subject.split_time_interval(3600)
      assert_equal [0, 1, 0], @subject.split_time_interval(60)
      assert_equal [0, 0, 1], @subject.split_time_interval(1)

      assert_equal [0, 1, 10], @subject.split_time_interval(70)

      assert_equal [0, 1, 59], @subject.split_time_interval(119)
    end

    it "formats time intervals into human readable hour/min/sec strings" do
      assert_equal("1 minute", @subject.format_time_interval(60))
      assert_equal("2 minutes, 10 seconds", @subject.format_time_interval(130))
      assert_equal("1 hour, 2 minutes, 10 seconds", @subject.format_time_interval(3600 + 130))
      assert_equal("Unknown", @subject.format_time_interval(-1))
    end

    it "handles strings" do
      assert_equal [1, 0, 0], @subject.split_time_interval("3600")
    end
  end
end
