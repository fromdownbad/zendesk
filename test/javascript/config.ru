#!/usr/bin/env ruby

root = File.expand_path('../../', File.dirname(__FILE__))

use Rack::Lint
use Rack::ShowExceptions
run Rack::Directory.new(root)

# logger.info "Javascript test server running. Please open http://localhost:9292/test/javascript/all.html"
