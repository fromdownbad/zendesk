$j = jQuery.noConflict();

// Who changes the order of parameters?
// Stupid QUnit.
function assert_equal(expected, actual, message) {
  message = message || ('Expected ' + actual + ' to equal ' + expected + '.');
  equals(actual, expected, message);
}

function assert_match(regex, text, message) {
  message = message || ("Expected " + text + ' to match ' + regex + '.');
  ok(regex.test(text), message);
}

function assert_include(includer, included, message) {
  message = message || ('Expected ' + includer + ' to include ' + included + '.');
  ok(includer.indexOf && includer.indexOf(included) >= 0, message);
}



// jQuery.cookie mock for the test environment
//
// jQuery.cookie('foo', 'bar'); // sets a cookie
// jQuery.cookie('foo'); // returns 'bar'
(function($) {

  var cookies = {};

  $.cookie = function(key, value) {
    if (value != null) {
      cookies[key] = value;
    } else {
      return cookies[key];
    }
  };

})(jQuery);
