require_relative "../support/test_helper"

SingleCov.not_covered!

describe 'SyntaxCheck' do
  # Uses the Google JS compiler to detect hard errors in the Zendesk JS
  # http://code.google.com/closure/compiler/
  describe "JavaScript" do
    before do
      java = `which java`.chomp
      fail "No Java installed" if java.blank?

      vers = `java -version 2>&1`.split("\n").find do |output|
        output =~ /(\d+)\.(\d+)\..+/
      end

      if vers
        major = $1
        minor = $2
      end

      unless major.to_i == 1 && minor.to_i >= 6
        fail "JDK 1.6+ required, found #{vers}"
      end

      @compiler = "#{File.dirname(__FILE__)}/lib/compiler.jar"

      # our main files, not every library <-> lots of bugs there...
      @files = Dir["#{Rails.root}/app/assets/javascripts/*.js"]

      fail "No input files found" if @files.empty?
      fail "Missing compiler" unless File.exist?(@compiler)
    end

    after do
      FileUtils.rm_rf("#{Rails.root}/public/assets/")
    end

    it "compiles with closure without errors" do
      output = ""
      call   = "java -jar #{@compiler} #{@files.map { |f| "--js #{f}" }.join(' ')} --js_output_file #{Dir.tmpdir}/tmp/js_syntax_test.js 2>&1"

      IO.popen(call) do |f|
        output = f.readlines.to_s
      end

      fail "Error in JavaScript compilation of :\n#{output}" if output.index("ERROR")
    end
  end
end
