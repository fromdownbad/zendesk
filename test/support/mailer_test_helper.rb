module MailerTestHelper
  def body_by_content_type(mail, content_type)
    mail.parts.detect { |p| p.mime_type == content_type }.body.to_s
  end

  def force_utf_8(text)
    text.force_encoding("utf-8")
  end
end
