# https://github.com/metaskills/minitest-spec-rails/blob/master/lib/minitest-spec-rails/init/mini_shoulda.rb
module MinitestShould
  module ImplicitSubject
    def describe(*args, &block)
      klass = super
      if args.first.is_a?(Class) && !klass.instance_methods(false).include?(:subject)
        klass.let(:subject) { args.first.new }
      end
      klass
    end
  end

  module Init
    module MiniShouldaBehavior
      extend ActiveSupport::Concern

      included do
        class << self
          include ImplicitSubject # TODO: move all classes to the top-level and this can go away ...
          prepend ClassMethods
          undef :test # using test xxx do causes tests to run twice and other foobar
        end
      end

      module ClassMethods
        def let!(name, &block)
          let(name) { instance_eval(&block) }
          before { send(name) }
        end

        # copied from https://github.com/grosser/maxitest/blob/master/lib/maxitest/let_all.rb
        def let_all(name, &block)
          cache = []
          define_method(name) do
            if cache.empty?
              cache[0] = instance_eval(&block)
            else
              cache[0]
            end
          end
        end

        # support multiple before/after blocks
        def before(_type = nil, &block)
          prepend Module.new { define_method(:setup) { super(); instance_exec(&block) } }
        end

        def after(_type = nil, &block)
          prepend Module.new { define_method(:teardown) { instance_exec(&block); super() } }
        end

        def should(*args, &block)
          if args.size == 2 && args[1][:before] # TODO: blow up with anything but :before
            let(:before_block) { nil }
            include(Module.new do
              define_method(:setup) do
                super()
                if before_block && !@before_block_called
                  @before_block_called = true
                  instance_exec &before_block
                end
              end
            end)

            describe "with before" do
              let(:before_block) { args[1].fetch(:before) }
              it(args[0], &block)
            end
          elsif block
            raise "use it directly"
          else
            matcher = args.first
            it(matcher.description) do
              matcher.in_context(self) if matcher.respond_to?(:in_context)
              unless matcher.matches?(@controller || subject)
                flunk matcher.failure_message
              end
            end
          end
        end

        def should_not(matcher)
          it(matcher.description) do
            matcher.in_context(self) if matcher.respond_to?(:in_context)
            if matcher.matches?(@controller || subject)
              flunk matcher.failure_message_when_negated
            end
          end
        end

        def before_should(*args, &block)
          should(*args, before: block) {}
        end

        def should_eventually(desc)
          describe "skip" do
            def setup
            end

            def teardown
            end
            it(desc)
          end
        end
      end
    end
  end
end

# tests sometimes hang locally or on ci and with this we can actually debug the cause instead of just hanging forever
module TimeoutEveryTestCase
  # we use our own timeout so nobody that rescues Timeout::Error catches ours
  class TestTimeout < StandardError
  end

  def timeout_for_test
    !ENV['NO_CI_TIMEOUT'] && !!ENV['CI'] && 45
  end

  def run(*args)
    if timeout_for_test == false
      super
    else
      Timeout.timeout(timeout_for_test, TimeoutEveryTestCase::TestTimeout) { super }
    end
  end
end
ActiveSupport::TestCase.prepend TimeoutEveryTestCase
ActiveSupport::TestCase.include MinitestShould::Init::MiniShouldaBehavior
Object.include MinitestShould::ImplicitSubject
