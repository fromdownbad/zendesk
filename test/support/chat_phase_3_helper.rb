module ChatPhase3Helper
  DEFAULT_MAX_AGENTS = 100

  def stub_account_service_sync
    stub_request(:patch, /[^.]+\.zendesk-test\.com\/api\/services\/accounts\/\d+/)
    Omnichannel::RoleSyncJob.stubs(:enqueue)
  end

  def create_phase_3_zopim_subscription(account, options = {})
    set_up_stubs
    clean_up(account)

    options = {
      status:                        ZBC::Zopim::SubscriptionStatus::Active,
      plan_type:                     ZBC::Zopim::PlanType::Lite.name,
      max_agents:                    DEFAULT_MAX_AGENTS,
      skip_chat_only_permission_set: false
    }.merge(options)

    skip_chat_only_permission_set = options.delete(:skip_chat_only_permission_set)

    z = account.build_zopim_subscription(
      status: options[:status],
      plan_type: options[:plan_type],
      max_agents: options[:max_agents]
    )
    z.phase = 3
    z.skip_chat_only_permission_set = skip_chat_only_permission_set
    z.save!
    z
  end

  def create_zopim_integration(account, phase, options = {})
    set_up_stubs
    account.zopim_integration.try(:delete)
    options = {
      account: account,
      external_zopim_id: "1234#{phase}",
      zopim_key: "1234#{phase}",
      phase: phase
    }.merge(options)
    ZopimIntegration.create!(options)
  end

  def clean_up(account)
    account.zopim_subscription.try(:delete)
    account.zopim_integration.try(:delete)
    account.owner.zopim_identity.try(:delete)
    account.reload
  end

  def set_up_stubs
    stub_request(:patch, %r{/api/services/staff/\d+/entitlements}).
      to_return(status: 200, body: { entitlements: { chat: "admin" } }.to_json, headers: { "Content-Type" => "application/json" })
    stub_request(:post, %r{embeddable/api/internal/config_sets.json})
    Account.any_instance.stubs(has_chat_permission_set?: true)
    ZBC::Zopim::Subscription.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      ensure_not_phase_four: nil
    )
    ZopimIntegration.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      ensure_not_phase_four: nil
    )
  end
end
