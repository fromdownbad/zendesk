module SignedAuthHelper
  def request_system_user_signed(method, system_user, url, parameters = nil, headers = {})
    uri = URI.parse(url)

    key = Zendesk::SystemUser.subsystem_key_for(system_user.to_s, "signed")
    time = Time.now.rfc2822

    sig = Zendesk::SystemUser::Authentication.signature(
      key, method.to_s.upcase, uri.host,
      fullpath(method, uri.path, parameters), time
    )

    send(method, url, params: parameters, headers: headers.merge("HTTP_X_ZENDESK_API" => "#{system_user}:#{sig}", "HTTP_DATE" => time))
  end

  def request_system_user_oauth(system_user, url, parameters = nil, headers: {}, method: :get, use_deprecated_key: false, use_deprecated_mac_key: false)
    key = if use_deprecated_key
      Zendesk::Configuration.dig!(:system_user_auth, system_user, :oauth_deprecated)
    else
      Zendesk::Configuration.dig!(:system_user_auth, system_user, :oauth)
    end

    client = global_oauth_clients(:system_users)
    client.update_columns(identifier: Zendesk::OAuth::GlobalClient::SYSTEM_USERS_IDENTIFIER)
    client.tokens.where(token: key).first_or_create! do |t|
      t.account_id = client.account.id
      t.user_id = -1
      t.scopes = system_user
    end

    uri = URI.parse(url)

    oauth_token = build_token(key, fullpath(method, url, parameters), use_deprecated_mac_key)
    signature = oauth_token.header(method, fullpath(method, url, parameters))

    send(method, url, params: parameters, headers: headers.merge('HTTP_AUTHORIZATION' => signature, 'SERVER_PORT' => uri.port))
  end

  def assume_via_new_monitor(assumed)
    @account.time_zone = "Pacific Time (US & Canada)"
    @account.save!
    request_system_user_signed(
      :post,
      :zendesk,
      "#{@account.url(protocol: 'https')}/api/v2/internal/monitor/token",
      {user_id: assumed.id},
      "X-Zendesk-Via-Reference-ID" => "1234",
      "X-Zendesk-Via" => "Monitor event"
    )
    assert_response :success
    assert_equal 1234, MasterToken.last.assuming_monitor_user_id
    token = JSON.parse(response.body)["verification_token"]["value"]

    # login via this token
    jwt_claim = JWT.encode({token: token, permission: {read_only: false}}, Zendesk::Configuration["monitor"]["key"])
    get "/access/master/jwt?jwt=#{jwt_claim}"
    assert_match %r{/agent$}, response.redirected_to
  end

  private

  def build_token(key, url, use_deprecated_mac_key)
    mac_key = if use_deprecated_mac_key
      Zendesk::Configuration.dig!(:system_user_auth, :mac_key_deprecated)
    else
      Zendesk::Configuration.dig!(:system_user_auth, :mac_key)
    end

    oauth2_client = OAuth2::Client.new('system_users', nil, site: url)
    OAuth2::MACToken.new(oauth2_client, key, mac_key)
  end

  def fullpath(method, url, parameters)
    fullpath = url

    # system_users does not sign non-get params
    if parameters && method.to_sym == :get
      fullpath += "?" + parameters.to_query
    end

    fullpath
  end
end
