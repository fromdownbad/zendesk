module ProxyControllerTestHelper
  def self.included(base)
    base.extend(Macros)
  end

  module Macros
    def should_not_filter(action, domain, options = { })
      scheme = options[:scheme]

      describe "with domain: #{domain.inspect}, scheme: #{scheme.inspect}" do
        describe "as is" do
          it "is allowed" do
            url = scheme ? "#{scheme}://#{domain}" : domain
            post action, params: { url: url }
            assert_response :success
          end
        end
      end
    end

    def should_filter(action, domain, options = { })
      scheme  = options[:scheme]
      message = options[:message] || "Invalid URL parameter"

      describe "with domain: #{domain.inspect}, scheme: #{scheme.inspect}" do
        describe "as is" do
          it "does not be allowed" do
            url = scheme ? "#{scheme}://#{domain}" : domain
            post action, params: { url: url }
            assert_equal message, @response.body
          end
        end

        describe "with credentials" do
          it "does not be allowed" do
            url = "username:password@#{domain}"
            url = scheme ? "#{scheme}://#{url}" : url
            post action, params: { url: url }
            assert_equal message, @response.body
          end
        end

        describe "with partial credential" do
          it "does not be allowed" do
            url = "password@#{domain}"
            url = scheme ? "#{scheme}://#{url}" : url
            post action, params: { url: url }
            assert_equal message, @response.body
          end
        end

        describe "with port" do
          it "does not be allowed" do
            url = "#{domain}:1234"
            url = scheme ? "#{scheme}://#{url}" : url
            post action, params: { url: url }
            assert_equal message, @response.body
          end
        end

        describe "with credentials and port" do
          it "does not be allowed" do
            url = "username:password@#{domain}:1234"
            url = scheme ? "#{scheme}://#{domain}" : url
            post action, params: { url: url }
            assert_equal message, @response.body
          end
        end
      end
    end
  end
end
