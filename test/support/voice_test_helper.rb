def create_phone_number(account, options = {})
  account.voice_sub_account = FactoryBot.create(:voice_sub_account, account: account) unless account.voice_sub_account

  options = {
    account: account,
    sid: nil
  }.merge!(options)
  phone_number = FactoryBot.build(:incoming_phone_number, options)

  phone_number.save
  phone_number
end
