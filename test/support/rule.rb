module TestSupport
  module Rule
  end
end

require_relative './rule/admin_request'
require_relative './rule/diff_helper'
require_relative './rule/helper'
require_relative './rule/usage_helper'
