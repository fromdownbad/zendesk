class ActionController::TestCase
  def self.behaves_like_rate_limited(limit_name, threshold:, variant: true, &block)
    as_an_agent do
      instance_exec(&block) if block_given?

      before do
        Timecop.freeze
        Datadog.tracer.stubs(:active_span).returns(span)
        span.stubs(:set_tag)
        Zendesk::Rules::ViewsRateLimiter.any_instance.stubs(:statsd_client).returns(statsd_client)
        @controller.expects(:request_origin_details).returns(request_origin_details).at_least_once
      end

      after do
        Timecop.return
      end

      describe "when #{limit_name} is enabled" do
        before { Arturo.enable_feature!(limit_name) }

        describe 'and views_enforce_rate_limits is enabled' do
          before { Arturo.enable_feature!(:views_enforce_rate_limits) }

          describe "with more than #{threshold} requests" do
            before { threshold.times { Prop.throttle!(limit_name, throttle_key) } }

            describe 'when request_origin_details is lotus' do
              let(:request_origin_details) { stub_everything(lotus?: true, request_source: 'lotus') }
              let(:tags) do
                [
                  "handle:#{limit_name}",
                  'first_throttled:true',
                  'request_source:lotus',
                  'status:200'
                ]
              end

              it 'reports breach and returns without any error' do
                statsd_client.expects(:increment).with('rate_limited', tags: tags)
                Rails.logger.expects(:warn).with(regexp_matches(/rate-limit/))
                span.expects(:set_tag).with('zendesk.rate_limited.handle', limit_name)
                span.expects(:set_tag).with('zendesk.rate_limited.count', threshold + 1)
                span.expects(:set_tag).with('zendesk.rate_limited.first_throttled', true)
                @controller.stubs(:view_tickets).returns([])
                send(request_type, action, params: params)

                assert response.body.present?
                assert_response :ok
              end
            end

            describe 'when request_origin_details is zendesk mobile client' do
              let(:request_origin_details) do
                stub_everything(
                  lotus?: false,
                  zendesk_mobile_client?: true,
                  request_source: 'api'
                )
              end
              let(:tags) do
                [
                  "handle:#{limit_name}",
                  'first_throttled:true',
                  'request_source:api',
                  'status:200'
                ]
              end

              it 'reports breach and returns without any error' do
                statsd_client.expects(:increment).with('rate_limited', tags: tags)
                Rails.logger.expects(:warn).with(regexp_matches(/rate-limit/))
                span.expects(:set_tag).with('zendesk.rate_limited.handle', limit_name)
                span.expects(:set_tag).with('zendesk.rate_limited.count', threshold + 1)
                span.expects(:set_tag).with('zendesk.rate_limited.first_throttled', true)
                @controller.stubs(:view_tickets).returns([])
                send(request_type, action, params: params)

                assert response.body.present?
                assert_response :ok
              end
            end

            describe 'when request_origin_details is not lotus' do
              let(:request_origin_details) do
                stub_everything(lotus?: false, request_source: 'api')
              end
              let(:tags) do
                [
                  "handle:#{limit_name}",
                  'first_throttled:true',
                  'request_source:api',
                  'status:429'
                ]
              end
              describe 'for the same view' do
                it 'reports and rate limits the requests' do
                  statsd_client.expects(:increment).with('rate_limited', tags: tags)
                  Rails.logger.expects(:warn).with(regexp_matches(/rate-limit/))
                  span.expects(:set_tag).with('zendesk.rate_limited.handle', limit_name)
                  span.expects(:set_tag).with('zendesk.rate_limited.count', threshold + 1)
                  span.expects(:set_tag).with('zendesk.rate_limited.first_throttled', true)

                  send(request_type, action, params: params)

                  assert response.headers['Retry-After']
                  assert_equal response.body, Prop.configurations[limit_name][:description]
                  assert_response :too_many_requests
                end

                describe_with_arturo_enabled :views_cache_429 do
                  it 'caches the result with a set of headers' do
                    send(request_type, action, params: params)
                    assert response.headers['Retry-After'].present?
                    assert_equal response.headers['Cache-Control'], 'max-age=10, private'
                    assert_equal response.headers['Vary'], 'HTTP_X_ZENDESK_APP_ID'
                    assert_response :too_many_requests
                  end
                end

                describe_with_arturo_disabled :views_cache_429 do
                  it 'does not cache the result' do
                    send(request_type, action, params: params)
                    assert response.headers['Retry-After']
                    assert_nil response.headers['Cache-Control']
                    assert_response :too_many_requests
                  end
                end

                describe "with more than #{threshold} requests from another agent" do
                  let(:throttle_key) { another_throttle_key }
                  it 'returns without any error' do
                    statsd_client.expects(:increment).with('rate_limited', anything).never
                    Rails.logger.expects(:warn).with(regexp_matches(/rate-limit/)).never
                    span.expects(:set_tag).with('zendesk.rate_limited.handle', anything).never
                    send(request_type, action, params: params)

                    assert response.body.present?
                    assert_response :ok
                  end
                end
              end
              if variant
                describe 'for different params' do
                  it 'returns without any error' do
                    statsd_client.expects(:increment).with('rate_limited', anything).never
                    Rails.logger.expects(:warn).with(regexp_matches(/rate-limit/)).never
                    span.expects(:set_tag).with('zendesk.rate_limited.handle', anything).never

                    send(request_type, action, params: other_params)

                    assert response.body.present?
                    assert_response :ok
                  end
                end
              end
            end
          end

          describe "with more than #{threshold + 1} requests" do
            before { (threshold + 1).times { Prop.throttle(limit_name, throttle_key) } }

            describe 'when request_origin_details is lotus' do
              let(:request_origin_details) { stub_everything(lotus?: true, request_source: 'lotus') }
              let(:tags) do
                [
                  "handle:#{limit_name}",
                  'first_throttled:false',
                  'request_source:lotus',
                  'status:200'
                ]
              end

              it 'reports breach and returns without any error' do
                statsd_client.expects(:increment).with('rate_limited', tags: tags)
                Rails.logger.expects(:warn).with(regexp_matches(/rate-limit/))
                span.expects(:set_tag).with('zendesk.rate_limited.handle', limit_name)
                span.expects(:set_tag).with('zendesk.rate_limited.count', threshold + 2)
                span.expects(:set_tag).with('zendesk.rate_limited.first_throttled', false)
                @controller.stubs(:view_tickets).returns([])
                send(request_type, action, params: params)

                assert response.body.present?
                assert_response :ok
              end
            end
          end

          describe "with less than #{threshold - 1} requests" do
            before { (threshold - 1).times { Prop.throttle!(limit_name, throttle_key) } }

            describe('when request_origin_details is not lotus') do
              let(:request_origin_details) { stub_everything(lotus?: false) }

              it 'returns without any error' do
                statsd_client.expects(:increment).with('rate_limited', anything).never
                Rails.logger.expects(:warn).with(regexp_matches(/rate-limit/)).never
                span.expects(:set_tag).with('zendesk.rate_limited.handle', anything).never
                send(request_type, action, params: params)

                assert response.body.present?
                assert_response :ok
              end
            end
          end
        end

        describe 'and views_enforce_rate_limits is disabled' do
          before { Arturo.disable_feature!(:views_enforce_rate_limits) }
          before { threshold.times { Prop.throttle!(limit_name, throttle_key) } }

          describe 'when request_origin_details is not lotus' do
            let(:request_origin_details) { stub_everything(request_source: 'app') }
            let(:tags) do
              [
                "handle:#{limit_name}",
                'first_throttled:true',
                'request_source:app',
                'status:200'
              ]
            end
            it 'logs and responds without errors' do
              statsd_client.expects(:increment).with('rate_limited', tags: tags)
              Rails.logger.expects(:warn).with(regexp_matches(/rate-limit/))
              span.expects(:set_tag).with('zendesk.rate_limited.handle', limit_name)
              span.expects(:set_tag).with('zendesk.rate_limited.count', threshold + 1)
              span.expects(:set_tag).with('zendesk.rate_limited.first_throttled', true)

              send(request_type, action, params: params)

              assert response.body.present?
              assert_response :ok
            end
          end
        end
      end

      describe "when #{limit_name} arturo is disabled" do
        describe "with more than #{threshold} requests" do
          before { threshold.times { Prop.throttle!(limit_name, throttle_key) } }

          describe('when request_origin_details is not lotus') do
            let(:request_origin_details) { stub_everything(request_source: 'app') }

            it 'does not log and return without any error' do
              statsd_client.expects(:increment).with('rate_limited', anything).never
              Rails.logger.expects(:warn).with(regexp_matches(/rate-limit/)).never
              span.expects(:set_tag).with('zendesk.rate_limited.handle', anything).never

              send(request_type, action, params: params)

              assert response.body.present?
              assert_response :ok
            end
          end
        end
      end
    end
  end
end
