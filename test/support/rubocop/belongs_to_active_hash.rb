require 'rubocop'

module RuboCop
  module Cop
    module Lint
      # Do not use the `belongs_to` association with a subclass of
      # `ActiveHash::Base`
      #
      # @example usage
      #
      #   # bad
      #   belongs_to :foo, class: "EntryFlagType"
      #
      #   # ok
      #   belongs_to_active_hash :foo, class: "EntryFlagType".
      #
      class BelongsToActiveHash < Cop
        MSG = 'Use `belongs_to_active_hash` and ' \
              '`extend ActiveHash::Associations::ActiveRecordExtensions` in ' \
              'the model.'.freeze

        def_node_matcher :belongs_to_active_hash_subclass, <<-PATTERN
          (send nil? :belongs_to
            (sym ...)
            (hash
              (pair (sym :class_name) $_)
              ...
            )
          )
        PATTERN

        def on_send(node)
          belongs_to_active_hash_subclass(node) do |class_name_arg|
            active_hash_subclasses.each do |subclass|
              # Resort to substring match on the source, so we match all of
              #   Foo
              #   Foo.to_s
              #   Foo.name
              #   "Foo"
              add_offense(node) if class_name_arg.source.include?(subclass)
            end
          end
        end

        private

        def active_hash_subclasses
          config.for_cop('Lint/ActiveHashDescendants').fetch('AllowedSubclasses')
        end
      end
    end
  end
end
