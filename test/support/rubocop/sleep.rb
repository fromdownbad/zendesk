require 'rubocop'

module RuboCop
  module Cop
    module Lint
      # Checks that sleep isn't called in tests.
      #
      # This cop will look for calls to sleep or Kernel.sleep in test files.
      # It will convert them to calls to Timecop.travel.
      #
      # @example usage
      #
      #   # bad
      #   sleep(1)
      #
      #   # bad
      #   Kernel.sleep(2.3)
      #
      #   # good
      #   Timecop.travel(Time.now + 2.3)
      #
      class Sleep < Cop
        MSG = 'Calls to \'sleep\' are discouraged ' \
              'in tests, use Timecop instead'.freeze

        def_node_matcher :sleep?, <<-PATTERN
          (send {nil? (:const nil? :Kernel)} :sleep _)
        PATTERN

        def on_send(node)
          filename = processed_source.buffer.name
          return unless filename.end_with?('test.rb', 'spec.rb') || filename == '(string)'
          sleep?(node) do
            add_offense(node)
          end
        end

        def autocorrect(node)
          lambda do |corrector|
            sleep_amt = node.children[2].source
            corrector.replace(node.loc.expression, "Timecop.travel(Time.now + #{sleep_amt})")
          end
        end
      end
    end
  end
end
