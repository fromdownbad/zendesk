module RuboCop
  module Cop
    module Lint
      # Checks that Rails.cache.fetch doesn't get called without a block.
      #
      # This cop will convert calls to Rails.cache.fetch with no block to a call
      # to Rails.cache.read. This change allows all cache clients to support caching of
      # nil values.
      #
      # @example usage
      #
      #   # bad
      #   Rails.cache.fetch("my_key")
      #
      #   # good
      #   Rails.cache.read("my_key")
      #
      #   # good
      #   Rails.cache.fetch("my_key") { "value if missing" }
      #
      class CacheFetchNoBlock < Cop
        MSG = %(Don't call cache#fetch without a block, use cache#read instead).freeze

        def_node_matcher :cache_fetch?, <<-PATTERN
          (send (send (const nil? :Rails) :cache) :fetch $...)
        PATTERN

        def on_send(node)
          detect_fetch_with_no_block(node)
        end

        def autocorrect(node)
          correct_fetch_with_no_block(node)
        end

        private

        def detect_fetch_with_no_block(node)
          cache_fetch?(node) do |args|
            if !args.last.block_pass_type? && !node.parent.block_type?
              add_offense(node)
            end
          end
        end

        def correct_fetch_with_no_block(node)
          lambda do |corrector|
            corrector.replace(node.location.selector, 'read')
          end
        end
      end
    end
  end
end
