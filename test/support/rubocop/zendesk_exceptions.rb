module RuboCop
  module Cop
    module Lint
      # Checks that Zendesk::Configuration doesn't get called with unexpected methods.
      #
      # This cop is configurable using the `AllowConfigShorthand` and
      # `AllowedMethods` options.
      #
      # The default allowed methods are `fetch`, `dig`, and `dig!`
      #
      # @example usage
      #
      #   # bad
      #   Zendesk::Configuration.pod_id
      #
      #   # good
      #   Zendesk::Configuration.fetch(:pod_id)
      #
      # @example configuration
      #
      #   # .rubocop.yml
      #   Lint/ZendeskConfiguration:
      #     AllowedMethods:
      #       - dig
      #
      #   # bad
      #   Zendesk::Configuration.dig(:foo, :bar)
      #
      #   # good
      #   Zendesk::Configuration.dig(:foo, :bar)
      #
      # @example configuration
      #
      #   # .rubocop.yml
      #   Lint/ZendeskConfiguration
      #     AllowConfigShorthand: false
      #
      #   # bad
      #   Zendesk.config.fetch(:foo)
      #
      #   # good
      #   Zendesk::Configuration.fetch(:foo)
      #
      class ZendeskExceptions < Cop
        def_node_matcher :exceptionlogger_call, <<-PATTERN
          (send
            (const (const nil? :Zendesk) :ExceptionLogger)
            :record
            $...
          )
        PATTERN

        def_node_matcher :record_or_raise_call, <<-PATTERN
          (send
            (const (const nil? :ZendeskExceptions) :Logger)
            :record_or_raise
            $...
          )
        PATTERN

        def_node_matcher :record_call, <<-PATTERN
          (send
            (const (const nil? :ZendeskExceptions) :Logger)
            :record
            $...
          )
        PATTERN

        def_node_matcher :fingerprint_param, <<-PATTERN
            (pair
              (sym :fingerprint)
              $_
            )
        PATTERN

        def_node_search :fingerprint_param?, <<-PATTERN
            (pair
              (sym :fingerprint)
              _
            )
        PATTERN

        def on_send(node)
          detect_exceptionlogger_call(node)
          detect_record_or_raise_call(node)
          detect_record_call(node)
        end

        def autocorrect(node)
          if record_or_raise?(node)
            correct_record_or_raise(node)
          elsif fingerprint_param?(node)
            correct_fingerprint_length(node)
          else
            correct_record_syntax(node)
          end
        end

        private

        def detect_exceptionlogger_call(node)
          exceptionlogger_call(node) do
            add_offense(node, message: 'Zendesk::ExceptionLogger has been deprecated.  Please use ZendeskExceptions::Logger')
          end
        end

        def detect_record_or_raise_call(node)
          record_or_raise_call(node) do
            add_offense(node, message: 'ZendeskExceptions::Logger.record_or_raise is no longer supported')
          end
        end

        def detect_record_call(node)
          record_call(node) do |args|
            # Valid record calls contain two args
            if args.length == 2
              parameters = args[1]
              # Require all new calls to record to fingerprint themselves
              if fingerprint_param?(parameters)
                # Fingerprints should be 40 characters or less
                parameters.each_child_node do |param|
                  fingerprint_param(param) do |fingerprint|
                    # Chop off the parens and check the length
                    if fingerprint.source[1..-2].length > 40
                      add_offense(node, message: 'ZendeskExceptions::Logger. fingerprints must be 40 characters or less')
                    end
                  end
                end
              else
                add_offense(node, message: 'Calls to ZendeskExceptions::Logger.record require a fingerprint parameter')
              end
            else
              add_offense(node, message: 'Calls to ZendeskExceptions::Logger.record now require named parameters')
            end
          end
        end

        def record_or_raise?(node)
          node.location.selector.source == 'record_or_raise'
        end

        def correct_record_syntax(node)
          lambda do |corrector|
            location    = node.children[2].source
            message     = node.children[3].source
            exception   = node.children[4]
            fingerprint = Digest::SHA256.hexdigest(node.source_range.to_s)[0, 40]

            corrector.replace(node.location.expression, "ZendeskExceptions::Logger.record(#{exception ? exception.source : 'Exception.new'}, location: #{location}, message: #{message}, fingerprint: '#{fingerprint}')")
          end
        end

        def correct_fingerprint_length(node)
          lambda do |corrector|
            record_call(node) do |args|
              fingerprint_node = args[1].children.find { |c| fingerprint_param?(c) }
              new_fingerprint = fingerprint_node.children[1].source[1, 40]
              corrector.replace(fingerprint_node.location.expression, "fingerprint: '#{new_fingerprint}'")
            end
          end
        end

        def correct_record_or_raise(node)
          lambda do |corrector|
            reraise     = node.children[2].source
            location    = node.children[3].source
            message     = node.children[4].source
            exception   = node.children[5]
            fingerprint = Digest::SHA256.hexdigest(node.source_range.to_s)[0, 40]

            corrector.replace(node.location.expression, "ZendeskExceptions::Logger.record(#{exception ? exception.source : 'Exception.new'}, location: #{location}, message: #{message}, reraise: !(#{reraise}), fingerprint: '#{fingerprint}')")
          end
        end
      end
    end
  end
end
