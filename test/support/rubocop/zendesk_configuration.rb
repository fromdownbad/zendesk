module RuboCop
  module Cop
    module Lint
      # Checks that Zendesk::Configuration doesn't get called with unexpected methods.
      #
      # This cop is configurable using the `AllowConfigShorthand` and
      # `AllowedMethods` options.
      #
      # The default allowed methods are `fetch`, `dig`, and `dig!`
      #
      # @example usage
      #
      #   # bad
      #   Zendesk::Configuration.pod_id
      #
      #   # good
      #   Zendesk::Configuration.fetch(:pod_id)
      #
      # @example configuration
      #
      #   # .rubocop.yml
      #   Lint/ZendeskConfiguration:
      #     AllowedMethods:
      #       - dig
      #
      #   # bad
      #   Zendesk::Configuration.dig(:foo, :bar)
      #
      #   # good
      #   Zendesk::Configuration.dig(:foo, :bar)
      #
      # @example configuration
      #
      #   # .rubocop.yml
      #   Lint/ZendeskConfiguration
      #     AllowConfigShorthand: false
      #
      #   # bad
      #   Zendesk.config.fetch(:foo)
      #
      #   # good
      #   Zendesk::Configuration.fetch(:foo)
      #
      class ZendeskConfiguration < Cop
        ALWAYS_ALLOWED_METHODS = %w[
          application_version
          config_from_env?
          evaluate_yaml_file
          load_dotenv
          refresh_servers
          servers
          settings
          settings
          setup
        ].freeze
        DEFAULT_ALLOWED_METHODS = %w[fetch dig dig!].freeze
        DEFAULT_IGNORED_METHODS = %w[nested nested!].freeze

        MSG_SHORTHAND = "Configuration should be referenced as `Zendesk::Configuration`, not `Zendesk.config`.".freeze
        BASE_CALL_MSG = "Zendesk::Configuration should only be called with ".freeze
        MSG_DIG       = "Arguments to `dig` or `nested` may not be strings.".freeze
        MSG_FETCH     = "First argument to Zendesk::Configuration.fetch may not be a string.".freeze

        def_node_matcher :invalid_config_shorthand, <<-PATTERN
          (send
            (const nil? :Zendesk)
            :config
            ...
          )
        PATTERN

        def_node_matcher :invalid_zc_call, <<-PATTERN
          (send
            (const (const nil? :Zendesk) :Configuration)
            $...
          )
        PATTERN

        def_node_matcher :zc_digger, <<-PATTERN
          (send
            (const (const nil? :Zendesk) :Configuration)
            {:dig :dig! :nested :nested!}
            $...
          )
        PATTERN

        def_node_matcher :zc_fetcher, <<-PATTERN
          (send
            (const (const nil? :Zendesk) :Configuration)
            :fetch
            $...
          )
        PATTERN

        def on_send(node)
          detect_shorthand_usage(node)
          detect_invalid_call_usage(node)
          detect_invalid_dig_usage(node)
          detect_invalid_fetch_usage(node)
        end

        def autocorrect(node)
          if shorthand_usage?(node)
            correct_shorthand_usage(node)
          else
            correct_method_call(node)
          end
        end

        private

        def detect_shorthand_usage(node)
          return if shorthand_allowed?

          invalid_config_shorthand(node) do
            add_offense(node, message: MSG_SHORTHAND)
          end
        end

        def detect_invalid_call_usage(node)
          invalid_zc_call(node) do |method_name, _args|
            unless allowed_method?(method_name)
              add_offense(node, message: invalid_call_msg)
            end
          end
        end

        def detect_invalid_dig_usage(node)
          zc_digger(node) do |args|
            if args.any? { |arg| arg.type == :str }
              add_offense(node, message: MSG_DIG)
            end
          end
        end

        def detect_invalid_fetch_usage(node)
          zc_fetcher(node) do |args|
            if args.first.type == :str
              add_offense(node, message: MSG_FETCH)
            end
          end
        end

        def shorthand_usage?(node)
          node.location.expression.source == 'Zendesk.config'
        end

        def correct_shorthand_usage(node)
          return if shorthand_allowed?

          lambda do |corrector|
            corrector.replace(node.location.dot, '::')
            corrector.replace(node.location.selector, 'Configuration')
          end
        end

        def correct_method_call(node)
          return if autocorrect_ignored_methods.include?(node.method_name.to_s)

          lambda do |corrector|
            corrector.replace(node.location.selector, "fetch(:#{node.method_name})")
          end
        end

        def shorthand_allowed?
          cop_config.fetch('AllowConfigShorthand', false)
        end

        def allowed_method?(method_name)
          ALWAYS_ALLOWED_METHODS.include?(method_name.to_s) ||
            allowed_methods.include?(method_name.to_s)
        end

        def allowed_methods
          cop_config.fetch('AllowedMethods', DEFAULT_ALLOWED_METHODS)
        end

        def autocorrect_ignored_methods
          (
            DEFAULT_IGNORED_METHODS +
            ALWAYS_ALLOWED_METHODS +
            allowed_methods
          ).uniq
        end

        def invalid_call_msg
          BASE_CALL_MSG + allowed_methods.map { |m| "`##{m}`" }.join(', ') + '.'
        end
      end
    end
  end
end
