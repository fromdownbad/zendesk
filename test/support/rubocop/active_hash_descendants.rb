require 'rubocop'

module RuboCop
  module Cop
    module Lint
      # Do not inherit from `ActiveHash::Base`.
      #
      # @example usage
      #
      #   # bad
      #   class Foo < ActiveHash::Base
      #
      #   # ok
      #   class Foo < Bar
      #
      class ActiveHashDescendants < Cop
        MSG = 'Do not inherit from `ActiveHash::Base`. Only allowed ' \
              'subclasses are: `%<allowed_subclasses>s`.'.freeze

        def_node_matcher :active_hash_base?, <<-PATTERN
          (const
            (const nil? :ActiveHash) :Base)
        PATTERN

        def on_class(node)
          name, superclass, _body = *node

          if active_hash_base?(superclass) && !allowed_subclasses.include?(name.source)
            add_offense(node, message: format(MSG, allowed_subclasses: allowed_subclasses))
          end
        end

        private

        def allowed_subclasses
          cop_config.fetch('AllowedSubclasses')
        end
      end
    end
  end
end
