require 'rubocop'

module RuboCop
  module Cop
    module Lint
      # Do not use `require File.expand_path`, it will lead to double require errors.
      #
      # @example usage
      #
      #   # bad
      #   require File.expand_path('../foo', File.dirname(__FILE__))
      #
      #   # ok
      #   require_relative '../foo'
      #
      class RequireFileExpandPath < Cop
        MSG = 'Use `require_relative` instead of `require File.expand_path`'.freeze

        def_node_matcher :require_file_expand_path?, <<-PATTERN
          (send nil? :require
            (send (const nil? :File) :expand_path ...)
          )
        PATTERN

        def on_send(node)
          add_offense(node) if require_file_expand_path?(node)
        end
      end
    end
  end
end
