require 'rubocop'

module RuboCop
  module Cop
    module Lint
      # Checks that timeout isn't called
      #
      # This cop will look for calls to timeout or Timeout.timeout.
      # There is no autocorrect for timeout, since it is evil. See articles like:
      #  https://www.mikeperham.com/2015/05/08/timeout-rubys-most-dangerous-api/
      #  https://vaneyckt.io/posts/the_disaster_that_is_rubys_timeout_method/
      #  https://medium.com/@adamhooper/in-ruby-dont-use-timeout-77d9d4e5a001
      #  https://jvns.ca/blog/2015/11/27/why-rubys-timeout-is-dangerous-and-thread-dot-raise-is-terrifying/
      #
      # @example usage
      #
      #   # bad
      #   timeout(1)
      #
      #   # bad
      #   Timeout.timeout(2.3)
      #
      class Timeout < Cop
        MSG = 'Calls to \'timeout\' are strongly discouraged.  They are' \
          ' very dangerous.'.freeze

        def_node_matcher :timeout?, <<-PATTERN
          (send { nil? (const nil? :Timeout) } :timeout _)
        PATTERN

        def on_send(node)
          timeout?(node) do
            add_offense(node)
          end
        end
      end
    end
  end
end
