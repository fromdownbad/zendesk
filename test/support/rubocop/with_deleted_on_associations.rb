require 'rubocop'

module RuboCop
  module Cop
    module Lint
      # Do not use `with_deleted` on associations, it does not scope correctly
      #
      # @example usage
      #
      #   # bad
      #   foo.bar.with_deleted
      #   foo.bar.baz.with_deleted
      #
      #   # ok
      #   Foo.with_deleted
      #
      class WithDeletedOnAssociations < Cop
        MSG = 'Do not use `with_deleted` on associations'.freeze

        def_node_matcher :with_deleted_on_association?, <<-PATTERN
          (send (send (send _ _) _) :with_deleted)
        PATTERN

        def on_send(node)
          add_offense(node) if with_deleted_on_association?(node)
        end
      end
    end
  end
end
