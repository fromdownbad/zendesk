# frozen_string_literal: true

# tested via test/integration/query_counter_support_test.rb
class ActiveSupport::TestCase
  INSERT_REGEX = /INSERT(?: IGNORE)? INTO `?(\w+)`?/.freeze
  UPDATE_REGEX = /UPDATE `?(\w+)`?/.freeze

  def tables_inserted(queries)
    queries.map { |query| INSERT_REGEX.match(query)[1] if INSERT_REGEX.match(query) }.compact
  end

  def tables_updated(queries)
    queries.map { |query| UPDATE_REGEX.match(query)[1] if UPDATE_REGEX.match(query) }.compact
  end

  def self.should_do_sql_queries(count, &block)
    it "does #{count} sql queries" do
      assert_sql_queries(count) do
        instance_eval(&block)
      end
    end
  end

  def sql_queries(regex = nil, &block)
    queries = []
    counter = ->(*, payload) do
      unless ["CACHE", "SCHEMA"].include?(payload.fetch(:name))
        query = payload.fetch(:sql)
        next if query.include?("arturo_features")
        next if query == "select 1"
        next if query.match?(/^RELEASE SAVEPOINT |^SAVEPOINT/)
        next if query.match?(/^REPLACE INTO \S+_ids /)
        query = query.sub "  WHERE", " WHERE"
        query = query.sub "  ORDER", " ORDER"
        queries << query
      end
    end

    ActiveSupport::Notifications.subscribed(counter, "sql.active_record", &block)

    queries = queries.grep(regex) if regex
    queries
  end

  def assert_sql_queries(count, option = //, &block)
    if option.is_a? Regexp
      match = option
      filter = nil
    else
      # assume a hash
      match = option[:match] || //
      filter = option[:filter] || nil
    end

    queries =
      if block
        sql_queries(&block)
      else
        # support legacy usages patterns, prefer to remove
        @sql_queries || raise("Need @sql_queries captured when not using a block")
      end
    queries = queries.grep(match)
    queries = queries.reject { |q| filter.match(q) } if filter

    if count.is_a?(Range)
      assert_includes count, queries.size, queries.join("\n")
    else
      assert_equal count, queries.size, queries.join("\n")
    end
    queries
  end

  def n_plus_one_in_logs(only_tables: nil, ignore_duplicates: false, &block)
    request_hash = {}

    clean_request = proc do |r|
      r.
        # and converts integers to ?
        gsub(/=\s*(\d)+/, '= ?').
        # and converts IN query to ?
        gsub(/\s*IN \([\d\s,]+\)/, '= ?').
        # Removes ``
        delete('`').
        # Case insensitive
        downcase
    end

    requests = sql_queries(&block).reject do |l|
      # Uninteresting stuff
      l =~ /^SELECT `(accounts|account_settings|routes|translation_locales)`/ || \
      # Not a select
      l !~ /SELECT/
    end

    if ignore_duplicates
      requests.uniq!
    end

    only_tables&.each do |tbl_name|
      requests.select! { |l| l =~ /FROM `#{tbl_name}`/ }
    end

    requests.each do |r|
      key = clean_request.call(r)
      request_hash[key] ||= []
      request_hash[key] << r
    end

    request_hash.map { |pattern, ary| ary.size > 1 ? [pattern, ary] : nil }.compact
  end

  # Example of usage:
  #
  # assert_no_n_plus_one do
  #   get :index
  # end
  def assert_no_n_plus_one(*args, &block)
    assert_n_plus_ones 0, *args, &block
  end

  def assert_n_plus_ones(count, *args, &block)
    raise ArgumentError, 'block must be given' unless block_given?

    issues = n_plus_one_in_logs(*args, &block)

    issues_str = issues.map do |pattern, requests|
      "-> #{pattern}\n" + requests.map { |r| "    #{r}" }.join("\n")
    end.join("\n")

    assert(
      count.is_a?(Range) ? count.include?(issues.size) : issues.size == count,
      "Expected #{count} N+1s, found #{issues.size}\nThese requests are called multiple times\n#{issues_str}"
    )
  end
end
