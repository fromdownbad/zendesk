class ActiveSupport::TestCase
  def self.should_be_able_to(verb, message = nil, &block)
    it ["is able to #{verb}", message].join(' ') do
      object = instance_eval(&block)
      assert subject.can?(verb, object), "Expected #{subject} to be able to #{verb} #{object}"
    end
  end

  def self.should_not_be_able_to(verb, message = nil, &block)
    it ["is not able to #{verb}", message].join(' ') do
      object = instance_eval(&block)
      refute subject.can?(verb, object), "Expect #{subject} not to be able to #{verb} #{object}"
    end
  end
end
