module LoginsTestHelper
  include Zendesk::Auth::V1::LoginsHelper

  def brand; end

  def token;
    @token;
  end

  def form_for(record, options, &block)
    options[:url] += "?return_to=#{params[:return_to]}" if params[:return_to].present?
    super(record, options, &block)
  end
end
