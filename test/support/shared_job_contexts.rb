module SharedJobContexts
  def self.included(klass)
    klass.class_eval do
      describe "#args_to_log" do
        let(:args_to_log) { self.class.described_type.method(:args_to_log) }
        let(:arguments) { Array.new([1, args_to_log.arity.abs].max) { {} } }

        it "returns a hash" do
          assert_kind_of Hash, args_to_log.call(*arguments)
        end
      end
    end
  end
end
