module DataDeletionJobSupport
  def self.included(base)
    base.class_eval do
      before do
        # This would be ideal to integrate this library, but transactional_fixtures
        # does not support threads and causes MySQL deadlock
        Zendesk::DataDeletion::JobHeartbeater.any_instance.stubs(:with_poll).yields
      end
    end
  end

  def create_job(account, job_class, options = {})
    ActiveRecord::Base.connection.execute <<-MYSQL
    UPDATE `subscriptions`
    SET
      `manual_discount` = 0,
      `is_trial` = 0,
      `churned_on` = '#{130.days.ago.to_s(:db)}',
      `updated_at` = '#{Time.now.to_s(:db)}'
    WHERE
      `subscriptions`.`id` = #{account.subscription.id}
    MYSQL
    account.update_column(:deleted_at, 40.days.ago)

    audit = account.data_deletion_audits.build_for_cancellation
    audit.created_at = 40.days.ago
    audit.save!

    job_audit = audit.build_job_audit_for_klass(job_class).tap(&:save!)
    job = job_class.new(job_audit.id, options)
    job_class.any_instance.stubs(:audit).returns(job_audit)

    job
  end
end
