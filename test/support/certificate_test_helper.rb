module CertificateTestHelper
  def cert_from_csr(csr)
    @ca = {}
    @ca[:key_pair] = OpenSSL::PKey::RSA.new(FixtureHelper::Certificate.read('ca.key'))
    @ca[:cert] = OpenSSL::X509::Certificate.new(FixtureHelper::Certificate.read('ca.crt'))

    cert = OpenSSL::X509::Certificate.new
    from = Time.now
    cert.subject = csr.subject
    cert.issuer = @ca[:cert].subject
    cert.not_before = from
    cert.not_after = from + 1 * 24 * 60 * 60
    cert.public_key = csr.public_key
    cert.serial = 0x1
    cert.version = 2 # X509v3

    key_usage = []
    ext_key_usage = []

    basic_constraint = "CA:FALSE"
    key_usage << "digitalSignature" << "keyEncipherment"
    ext_key_usage << "serverAuth"

    ef = OpenSSL::X509::ExtensionFactory.new
    ef.subject_certificate = cert
    ef.issuer_certificate = @ca[:cert]
    ex = []
    ex << ef.create_extension("basicConstraints", basic_constraint, true)
    ex << ef.create_extension("nsComment",
      "Ruby/OpenSSL Generated Certificate")
    ex << ef.create_extension("subjectKeyIdentifier", "hash")
    ex << ef.create_extension("keyUsage", key_usage.join(","))
    ex << ef.create_extension("extendedKeyUsage", ext_key_usage.join(","))

    cert.extensions = ex
    cert.sign(@ca[:key_pair], OpenSSL::Digest::SHA256.new)

    cert
  end

  def create_certificate!(state:, create_ip: true, account: @account)
    cert = account.certificates.create
    cert.subject = "#{state} certificate #{SecureRandom.uuid}"
    cert.state = state
    cert.save!

    cip = nil
    if create_ip
      used_ports = CertificateIp.uniq.pluck(:port)
      next_port = (1...9999).find { |port| !used_ports.include?(port) }
      raise 'no free ports in create_certificate!' unless next_port
      cip = cert.certificate_ips.create
      cip.ip = [rand(255), rand(255), rand(255), rand(255)].join('.')
      cip.port = next_port
    else
      cip = cert.certificate_ips.create
      cip.sni = true
    end

    cip.pod_id = account.pod_id
    cip.save!

    cert
  end

  def create_autoprovisioned_certificate!(options = {})
    cert = account.certificates.build
    cert.state = options.fetch(:state)
    cert.autoprovisioned = true
    cert.sni_enabled = true
    cert.save

    self_sign_cert(cert, new_csr(account.host_mapping, account.brand_host_mappings), options)
    cert.save!
    cert
  end

  def new_csr(domain, san = nil)
    R509::CSR.new(
      subject: [["CN", domain]],
      bit_length: 1024,
      san_names: san
    )
  end

  def self_sign(csr, options = {})
    if csr.san.present?
      ext = [R509::Cert::Extensions::SubjectAlternativeName.new(value: csr.san.to_h)]
    end

    R509::CertificateAuthority::Signer.selfsign(
      csr: csr,
      extensions: ext,
      not_before: Time.now,
      not_after: options.fetch(:not_after, 1.year.from_now)
    )
  end

  def self_sign_cert(cert, csr, options = {})
    cert.set_uploaded_data(self_sign(csr, options).to_pem, csr.key.to_pem)
  end

  def disable_lets_encrypt_observers
    AcmeCertificateJob.stubs(:enqueue)
    yield
  ensure
    AcmeCertificateJob.unstub(:enqueue)
  end
end
