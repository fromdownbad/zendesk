module SalesforceTestHelper
  def self.included(base)
    base.extend(ClassMethods)
  end

  def with_bubbles(bubbles, raises, &block)
    if bubbles
      assert_raises(raises, &block)
    else
      yield
    end
  end

  module ClassMethods
    # Expects `@user` to be set in the parent context
    def it_syncs_errored(options = {})
      raises = options[:raises]
      error = options[:error]
      bubbles = options[:bubbles] || false

      describe "when integration raises #{raises}" do
        before do
          salesforce_integration = mock
          salesforce_integration.expects(:fetch_info_for).raises(raises)
          Account.any_instance.stubs(:salesforce_integration).returns(salesforce_integration)
        end

        it "syncs errored with error '#{error}'" do
          with_bubbles(bubbles, raises) do
            SalesforceSyncJob.perform(@user.account_id, @user.id)
          end

          @user.salesforce_data.reload
          assert_equal @user.salesforce_data.data, records: [], error: error
          assert_equal ExternalUserData::SyncStatus::SYNC_ERRORED, @user.salesforce_data.sync_status
        end
      end
    end

    def socket_dns_error
      SocketError.new('getaddrinfo: Name or service not known (sammich.example.org)')
    end

    def fault_error
      fault = SOAP::SOAPFault.new
      fault.faultstring = SOAP::SOAPElement.new('error', 'an error')
      fault.faultcode = SOAP::SOAPElement.new('error')
      SOAP::FaultError.new(fault)
    end
  end
end
