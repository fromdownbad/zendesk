module FeaturesHelpers
  def assert_feature(expectation, feature)
    assert_equal expectation,
      @subscription.send("has_#{feature}?"),
      "expected feature #{feature.inspect} to be #{expectation.inspect}"
  end

  def get_features(catalog, plan, feature_type)
    case feature_type
    when :feature
      get_catalog(catalog).features_for_plan(plan).map(&:name).sort
    when :addon
      get_catalog(catalog).addons_for_plan(plan).map(&:name).sort
    when :bundle
      get_catalog(catalog).bundles.map(&:name).sort
    end
  end

  def get_catalog(catalog)
    if catalog == :legacy
      Zendesk::Features::Catalogs::LegacyCatalog
    elsif catalog == :catalog
      Zendesk::Features::Catalogs::Catalog
    end
  end
end
