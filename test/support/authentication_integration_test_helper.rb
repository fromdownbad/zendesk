module AuthenticationIntegrationTestHelper
  def self.included(klass)
    klass.extend ClassMethods
  end

  module ClassMethods
    def should_redirect_to_remote_auth(url, path_only = false)
      it "redirects to the remote auth URL" do
        assert_redirects_to_remote_auth(url, path_only)
      end
    end

    def should_not_redirect_to_remote_auth(url)
      it "does not redirect to the remote auth URL" do
        visit(url)
        if redirect?
          assert_no_match(/^#{@auth.remote_login_url}?/, headers["Location"])
        end
      end
    end

    def should_give_an_error(message = nil, options = {})
      it "gives an error" do
        visit(headers["Location"]) while redirect?

        assert_response :success
        assert_template 'auth/sso_v2_index'
        assert_select (options[:selector] || '#error') do |elements|
          assert_match(/#{message}/, elements.text) if message.present?
        end
      end
    end

    def should_redirect_to_hc
      it "redirects to HC" do
        assert_not_nil headers["X-Zendesk-User-Id"]
        assert_equal "/hc", request.path
      end
    end

    def should_authenticate(message = nil)
      it "authenticates" do
        visit(headers["Location"]) while redirect?

        assert_response :success
        assert_select   '#notice' do |elements|
          assert_match(/#{message}/, elements.text) if message.present?
        end
      end
    end
  end

  def get_query_params_from(url)
    if url.index('?')
      Rack::Utils.parse_nested_query(url.split('?')[1]).with_indifferent_access
    else
      {}
    end
  end

  def assert_redirects_to_remote_auth(url, path_only = false)
    visit(url)
    assert(redirect?)
    assert_includes headers["Location"], @auth.remote_login_url
    assert_includes response.location, "brand_id=#{@account.default_brand.id}"

    if path_only
      assert_equal URI.parse(url).path, return_to
    else
      assert_equal url, return_to
    end
  end

  def return_to
    location_params['return_to'] || session[:return_to]
  end

  def location_params
    get_query_params_from(headers["Location"])
  end
end
