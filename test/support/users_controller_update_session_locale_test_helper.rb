module UsersControllerUpdateSessionLocaleTestHelper
  def self.included(base)
    base.instance_eval do
      fixtures :translation_locales

      as_an_admin do
        describe "changing the language" do
          let(:english_by_zendesk) { translation_locales(:english_by_zendesk) }
          let(:portuguese) { translation_locales(:brazilian_portuguese) }
          let(:spanish) { translation_locales(:spanish) }

          before do
            shared_session[:locale_id] = spanish.id
          end

          it "does not set the session if it wasn't set already" do
            shared_session.delete(:locale_id)
            put :update, params: { id: @user.id, user: { locale_id: portuguese.id } }
            [:ok, :redirect].include?(response.status)
            assert_nil shared_session[:locale_id]
          end

          it "does not update the session if the user is updating a profile different than their own" do
            other_user = users(:minimum_end_user2)
            put :update, params: { id: other_user.id, user: {locale_id: english_by_zendesk.id} }
            [:ok, :redirect].include?(response.status)
            assert_equal(spanish.id, shared_session[:locale_id])
          end

          it "does not update the session if the language it is not changed" do
            put :update, params: { id: @user.id, user: {name: "abcdef"} }
            [:ok, :redirect].include?(response.status)
            assert_equal(spanish.id, shared_session[:locale_id])
          end

          it "updates the session locale if the user updates their profile language" do
            portuguese.localized_agent = true
            portuguese.save!
            @account.translation_locale = portuguese
            @account.save!

            put :update, params: { id: @user.id, user: {locale_id: portuguese.id} }
            [:ok, :redirect].include?(response.status)
            assert_equal(portuguese.id, shared_session[:locale_id])
            assert_equal(portuguese, @user.translation_locale)
          end
        end
      end
    end
  end
end
