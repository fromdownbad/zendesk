module TicketDeflectionTestHelper
  def create_ticket_with_account(account:, options: {})
    user = account.agents.first

    attributes = options.merge(
      subject: "Thing!",
      description: "Boom.",
      requester: user
    )

    ticket = account.tickets.build
    ticket.attributes = attributes
    ticket.brand = account.brands.first
    ticket.will_be_saved_by(user)
    # Ensure the account fixture used includes a sequence mapping,
    # otherwise ticket.set_nice_id will fail
    # See: test/fixtures/sequences.yml
    ticket.save!

    ticket
  end

  def create_ticket_deflection(ticket:, deflection_channel_id:, via_id: nil)
    deflection = ticket.build_ticket_deflection
    deflection.ticket = ticket
    deflection.enquiry = 'Unicorns really, really like toast.'
    deflection.user = ticket.requester
    deflection.via_id = via_id
    deflection.brand = ticket.brand
    deflection.deflection_channel_id = deflection_channel_id
    deflection.save!

    deflection
  end
end
