module DeflectionHelper
  def build_deflection_action
    Definition.new.tap do |definition|
      definition.conditions_all.push(
        DefinitionItem.new('status_id', 'is', 1)
      )
      definition.actions.push(
        DefinitionItem.new('deflection', 'is', ['requester_id', 'subject', 'body'])
      )
    end
  end

  def build_notification_action_conditions_all(update_type = 'Create', with_cc: false)
    Definition.new.tap do |definition|
      definition.conditions_all.push(item_update_type(update_type))
      definition.actions.push(with_cc ? item_notification_user_with_cc : item_notification_user)
    end
  end

  def build_notification_action_conditions_any(update_type = 'Create')
    Definition.new.tap do |definition|
      definition.conditions_any.push(item_update_type(update_type))
      definition.actions.push(item_notification_user)
    end
  end

  def item_update_type(type)
    DefinitionItem.new('update_type', 'is', type)
  end

  def item_notification_user
    DefinitionItem.new('notification_user', 'is', ['requester_id', 'subject', 'body'])
  end

  def item_notification_user_with_cc
    DefinitionItem.new('notification_user', 'is', ['requester_and_ccs', 'subject', 'body'])
  end
end
