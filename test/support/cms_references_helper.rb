module CmsReferencesHelper
  def creates_cms_references(type:, actions:)
    describe '#create_cms_references' do
      let(:account) { accounts(:minimum) }

      let!(:cms_text) do
        account.cms_texts.create(
          name:                'Text',
          fallback_attributes: {
            is_fallback:           true,
            nested:                true,
            account_id:            1,
            value:                 'The fallback value',
            translation_locale_id: 1
          }
        )
      end

      describe "when the #{type} is updated" do
        let(:rule)            { public_send("create_#{type}") }
        let(:base_definition) { public_send("build_#{type}_definition") }

        before do
          rule.definition = base_definition.tap do |definition|
            definition.actions.push(
              DefinitionItem.new(action_subject, nil, [action_value])
            )
          end
        end

        actions.each do |action|
          describe "when the subject is `#{action}`" do
            let(:action_subject) { action }

            describe 'and it does not contain a CMS reference' do
              let(:action_value) { 'test' }

              before { rule.save! }

              before_should 'call `create_cms_references`' do
                rule.expects(:create_cms_references).at_least_once
              end

              it 'does not create a CMS rule reference' do
                refute cms_text.references.find_by_rule_id(rule.id).present?
              end

              should_not_change 'the number of CMS references' do
                cms_text.references.count
              end

              should_not_change 'the number of rule references' do
                rule.rule_references.count
              end
            end

            describe 'and it contains a CMS reference' do
              let(:action_value) { '{{dc.text}}' }

              before { rule.save! }

              it 'creates a CMS rule reference' do
                assert cms_text.references.find_by_rule_id(rule.id).present?
              end

              should_change('the number of CMS references', by: 1) do
                cms_text.references.count
              end

              should_change('the number of rule references', by: 1) do
                rule.rule_references.count
              end
            end
          end
        end
      end
    end
  end
end
