require 'role'
require 'minitest/mock'

module EntitlementTestHelper
  private

  def make_new_staff
    new_user = User.new
    new_user.account_id = account.id
    new_user.roles = Role::AGENT.id
    new_user.name = "New Staff"
    new_user.email = "newbie@new.com"
    new_user
  end

  def make_new_end_user
    new_user = User.new
    new_user.account_id = account.id
    new_user.roles = Role::END_USER.id
    new_user.name = "New End-User"
    new_user
  end

  def make_light_agent(user)
    user.user_seats.destroy_all
    user.roles = Role::AGENT.id
    user.permission_set_id = user.account.light_agent_permission_set.id
    user
  end

  def make_chat_only_agent(user)
    user.user_seats.destroy_all
    add_chat(user)
    user.roles = Role::AGENT.id
    user.permission_set_id = user.account.chat_permission_set.id
    user
  end

  def make_support_agent(user)
    user.roles = Role::AGENT.id
    user.permission_set_id = nil
    user
  end

  def make_admin(user)
    user.roles = Role::ADMIN.id
    user.permission_set_id = nil
    user
  end

  def make_custom(user)
    user.roles = Role::AGENT.id
    user.permission_set_id = account.permission_sets.find_by_name('Custom Agent').id
    user
  end

  def add_chat(user)
    zopim_identity = stub(
      zopim_agent_id: 1234,
      update_attribute: true,
      is_enabled?: true,
      is_owner: false,
      is_zopim_owner: false,
      disable!: true
    )

    user.stubs(zopim_identity: zopim_identity)
    user
  end

  def delete_zopim_integration(account)
    account.zopim_subscription.try(:delete)
    account.zopim_integration.try(:delete)
  end

  def add_explore(user)
    user.user_seats.create(seat_type: 'explore', account_id: user.account_id)
    user
  end

  def add_connect(user)
    user.user_seats.create(seat_type: 'connect', account_id: user.account_id)
    user
  end

  def make_entitlements_response(
    support: nil,
    chat: nil,
    explore: nil,
    connect: nil
  )
    {
      'entitlements' => {
        Zendesk::Entitlement::TYPES[:support] => support,
        Zendesk::Entitlement::TYPES[:chat] => chat,
        Zendesk::Entitlement::TYPES[:explore] => explore,
        Zendesk::Entitlement::TYPES[:connect] => connect
      }.delete_if { |_, v| v.nil? }
    }.to_json
  end

  def staff_service_entitlements_seats_remaining_url
    /#{staff_service_url_prefix}\/api\/services\/entitlements\/seats_remaining/
  end

  def staff_service_seats_remaining_url
    /#{staff_service_url_prefix}\/api\/services\/seats\/remaining/
  end

  def staff_service_seats_occupied_url
    /#{staff_service_url_prefix}\/api\/services\/seats\/occupied/
  end

  def staff_service_entitlements_url(_subdomain, is_end_user = false)
    /#{staff_service_url_prefix}\/api\/services\/staff\/\d+\/entitlements#{Regexp.escape '?is_end_user=true' if is_end_user}/
  end

  def staff_service_get_full_entitlements_url(_subdomain)
    /#{staff_service_url_prefix}\/api\/services\/staff\/\d+\/full_entitlements/
  end

  def staff_service_patch_full_entitlements_url(_subdomain, skip_validation: false, is_end_user: false)
    /#{staff_service_url_prefix}\/api\/services\/staff\/\d+\/full_entitlements\?is_end_user=#{is_end_user}&skip_validation=#{skip_validation}/
  end

  def staff_service_url_prefix
    "http://metropolis:8888"
  end

  def stub_staff_service_patch_request(subdomain, patch_status, is_end_user = false)
    stub_request(:patch, staff_service_entitlements_url(subdomain, is_end_user)).to_return do |request|
      { status: patch_status, body: request.body, headers: {"Content-Type" => "application/json"} }
    end
  end

  def stub_staff_service_full_entitlements_patch_request(subdomain, patch_status, skip_validation: false, is_end_user: false)
    stub_request(:patch, staff_service_patch_full_entitlements_url(subdomain, skip_validation: skip_validation, is_end_user: is_end_user)).to_return do |request|
      { status: patch_status, body: request.body, headers: {"Content-Type" => "application/json"} }
    end
  end

  def stub_staff_service_entitlements_get_request(entitlements_get_response, is_end_user = false)
    stub_request(:get, %r{/api/services/staff/[0-9]+/entitlements#{Regexp.escape '?is_end_user=true' if is_end_user}}).to_return(
      status: 200,
      body: entitlements_get_response,
      headers: {"Content-Type" => "application/json"}
    )
  end
end
