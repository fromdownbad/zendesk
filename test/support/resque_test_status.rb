require 'resque/status'
require 'logger'

module Resque
  class TestStatus
    include ::Resque::Plugins::Status

    def self.override_status!
      silence_warnings do
        ::Resque.const_set('Status', self)
      end
    end

    KILL_LIST = Set.new

    def self.kill(uuid)
      KILL_LIST.add(uuid)
    end

    def self.should_kill?(uuid)
      KILL_LIST.include?(uuid)
    end

    def self.killed(uuid)
      KILL_LIST.delete(uuid)
    end

    def self.logger(_uuid, _opttions = {})
      @logger ||= Logger.new(STDOUT)
    end

    def self.redis
      @redis ||= FakeRedis::Redis.new
    end
  end
end
