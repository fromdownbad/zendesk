module ZopimTestHelper
  private

  def setup_zopim_subscription(account)
    stub_request(:post, %r{embeddable/api/internal/config_sets.json})
    ZopimIntegration.create!(
      account: account,
      external_zopim_id: 1,
      zopim_key: 'abc'
    )

    account.create_zopim_subscription!.tap do |record|
      record.zopim_account_id = account.id
      record.save!
      record.update_column(:syncstate, ZendeskBillingCore::Zopim::SyncState::Ready)
    end
  end

  def setup_zopim_identity(user, is_owner: false, is_administrator: false, is_enabled: true)
    user.create_zopim_identity!.tap do |record|
      record.is_owner         = is_owner
      record.is_administrator = is_administrator
      record.zopim_agent_id   = record.id
      record.is_enabled       = is_enabled
      record.save!
      record.update_column(:syncstate, ZendeskBillingCore::Zopim::SyncState::Ready)
    end
  end

  def setup_zopim_stubs
    Zopim::Reseller.client.stubs(find_accounts!: [])
    Account.any_instance.stubs(has_chat_permission_set?: true)
    Zopim::AgentSyncJob.stubs(:work)
    User.any_instance.stubs(is_verified?: true)
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(is_serviceable?: true)
    stub_request(:put, %r{/api/accounts/})

    ZopimIntegration.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      ensure_not_phase_four: nil
    )
    User.any_instance.stubs(:zopim_email).returns("test")
    Zopim::Agent.any_instance.stubs(:link_to_remote_account_record)
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:sync)
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:link_account_record)
    ZendeskBillingCore::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)
    ZendeskBillingCore::Zopim::SubscriptionDeactivator.any_instance.stubs(:deactivate!)
  end
end
