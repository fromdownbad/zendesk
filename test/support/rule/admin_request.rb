module TestSupport::Rule
  module AdminRequest
    def records_admin_request(action, &block)
      describe 'when recording' do
        let(:admin_version) { '4.2.0' }
        let(:status_code)   { 200 }

        before do
          klass = if RAILS4
            ActionController::TestResponse
          else
            ActionDispatch::TestResponse
          end

          klass.
            any_instance.
            stubs(:status).
            returns(status_code)

          # Rails 4 triggers a bunch of radar expiry, so ignore those stats
          Zendesk::StatsD::Client.any_instance.stubs(:increment)
        end

        describe 'when the `X-Zendesk-Rule-Admin-Version` header is missing' do
          before(&block)

          before_should 'not record the request' do
            Zendesk::StatsD::Client.
              any_instance.
              expects(:increment).
              with('rule.admin.ui.api.requests').
              never
          end
        end

        describe 'when the `X-Zendesk-Rule-Admin-Version` header is present' do
          before do
            set_header('X-Zendesk-Rule-Admin-Version', admin_version)
          end

          describe 'and the request succeeds' do
            before(&block)

            before_should 'record the request' do
              Zendesk::StatsD::Client.
                any_instance.
                expects(:increment).
                with(
                  'rule.admin.ui.api.requests',
                  tags: %W[
                    controller:#{@controller.controller_name}
                    action:#{action}
                    version:#{admin_version}
                    success
                    status:#{status_code}
                  ]
                ).once
            end
          end

          describe 'and the request errors' do
            let(:status_code) { 500 }

            before(&block)

            before_should 'record the request' do
              Zendesk::StatsD::Client.any_instance.expects(:increment).with(
                'rule.admin.ui.api.requests',
                tags: %W[
                  controller:#{@controller.controller_name}
                  action:#{action}
                  version:#{admin_version}
                  error
                  status:#{status_code}
                ]
              ).once
            end
          end
        end
      end
    end
  end
end
