module TestSupport::Rule
  module DiffHelper
    def create_trigger_revision_diff(
      revision_one = create_trigger.revisions.first,
      revision_two = create_trigger.revisions.first
    )
      Zendesk::Rules::Trigger::RevisionDiff.new(
        source: revision_one,
        target: revision_two
      )
    end

    def change_object(
      change  = Zendesk::Rules::ChangeObject::CHANGES.sample,
      content = nil
    )
      Zendesk::Rules::ChangeObject.new(change, content)
    end
  end
end
