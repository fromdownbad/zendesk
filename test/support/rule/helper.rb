module TestSupport::Rule
  module Helper
    def create_macro(params = {})
      _create_rule(
        params.merge(
          owner:      Array(params.fetch(:owner) { account }).first,
          definition: params.fetch(:definition) { build_macro_definition },
          rule_type:  :macros
        )
      ) do |macro|
        if params[:owner].is_a?(Array)
          macro.group_owner_ids = params[:owner].map(&:id)
        end
      end
    end

    def create_view(params = {}, &on_create)
      _create_rule(
        params.merge(
          definition: params.fetch(:definition) { build_view_definition },
          output:     params.fetch(:output)     { build_view_output },
          rule_type:  :views
        ),
        &on_create
      )
    end

    def create_automation(params = {})
      _create_rule(
        params.merge(
          definition: params.fetch(:definition) { build_automation_definition },
          owner:      params.fetch(:owner)      { account },
          rule_type:  :automations
        )
      )
    end

    def create_trigger(params = {})
      _create_rule(
        params.merge(
          definition:        params.fetch(:definition) { build_trigger_definition },
          owner:             params.fetch(:account)    { account },
          rule_type:         :triggers,
          rules_category_id: params.fetch(:rules_category_id) { nil },
          category: params.fetch(:category) { nil }
        )
      )
    end

    def create_revision(trigger = create_trigger, params = {})
      trigger.revisions.create! do |revision|
        revision.account_id = params.fetch(:account_id) { trigger.account_id }
        revision.author_id  = params.fetch(:author_id)  { trigger.owner_id }

        revision.nice_id = params.fetch(:nice_id) {
          trigger.revisions.maximum(:nice_id).next
        }

        if params[:after].present?
          revision.created_at = trigger.created_at + params[:after]
        end
      end
    end

    def create_category(params = {})
      RuleCategory.create!(
        account:     params.fetch(:account)     { account },
        name:        params.fetch(:name)        { 'Create Category' },
        rule_type:   params.fetch(:rule_type)   { 'Trigger' },
        position:    params.fetch(:position)    { 1 }
      )
    end

    def new_trigger(params = {})
      _new_rule(
        params.merge(
          definition: params.fetch(:definition) { build_trigger_definition },
          owner:      params.fetch(:account)    { account },
          rule_type:  :triggers,
          rules_category_id: params.fetch(:rules_category_id) { nil },
          category: params.fetch(:category) { nil }
        )
      )
    end

    def assert_rules_equal(expected, rules)
      assert_equal(
        expected,
        rules.map do |rule|
          {
            owner_type: rule.owner_type,
            active:     rule.is_active,
            title:      rule.title,
            position:   rule.position
          }
        end
      )
    end

    def build_macro_definition
      Definition.new.tap do |definition|
        definition.actions.push(
          DefinitionItem.new('set_tags', nil, ['test'])
        )
      end
    end

    def build_view_definition
      Definition.new.tap do |definition|
        definition.conditions_all.push(
          DefinitionItem.new('priority_id', 'is', PriorityType.HIGH.to_s)
        )
        definition.conditions_all.push(
          DefinitionItem.new(
            'status_id',
            'less_than',
            StatusType.Pending.to_s
          )
        )
      end
    end

    def build_view_output
      Output.create(
        group:     :status,
        group_asc: true,
        order:     :id,
        order_asc: true,
        columns:   %i[description requester created],
        type:      'table'
      )
    end

    def build_automation_definition(params = {})
      Definition.new.tap do |definition|
        (
          params[:conditions_all] ||
            [
              build_definition_item(
                field:    'status_id',
                operator: 'less_than',
                value:    StatusType.CLOSED
              ),
              build_definition_item(
                field:    'SOLVED',
                operator: 'is',
                value:    ['24']
              )
            ]
        ).each { |item| definition.conditions_all.push(item) }

        (params[:conditions_any] || []).each do |item|
          definition.conditions_any.push(item)
        end

        (
          params[:actions] ||
            [
              build_definition_item(
                field:    'status_id',
                operator: 'is',
                value:    StatusType.CLOSED
              )
            ]
        ).each { |item| definition.actions.push(item) }
      end
    end

    def build_trigger_definition(params = {})
      Definition.new.tap do |definition|
        (params[:conditions_all] || [build_definition_item]).each do |item|
          definition.conditions_all.push(item)
        end

        (params[:conditions_any] || []).each do |item|
          definition.conditions_any.push(item)
        end

        (
          params[:actions] ||
            [
              build_definition_item(
                field: 'current_tags',
                operator: nil,
                value: 'first_trigger'
              )
            ]
        ).each { |item| definition.actions.push(item) }
      end
    end

    def build_definition_item(field: 'status_id', operator: 'is', value: 1)
      DefinitionItem.new(field, operator, value)
    end

    private

    def _create_rule(params = {}, &on_create)
      params.fetch(:owner) { account }.send(params[:rule_type]).create!(
        **_rule_params(params),
        &on_create
      )
    end

    def _new_rule(params = {})
      params.fetch(:owner) { account }.send(params[:rule_type]).new(
        **_rule_params(params)
      )
    end

    def _rule_params(params = {})
      rule_params = {
        account: params.fetch(:account) { account },
        title: params.fetch(:title) { 'Rule' },
        description: params.fetch(:description) { 'description' },
        definition: params.fetch(:definition),
        is_active: params.fetch(:active) { true },
        position: params.fetch(:position) { nil },
        output: params.fetch(:output) { nil },
        rules_category_id: params.fetch(:rules_category_id) { nil }
      }

      rule_params[:category] = params[:category] if params.key?(:category)

      rule_params
    end
  end
end
