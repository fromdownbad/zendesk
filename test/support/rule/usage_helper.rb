module TestSupport::Rule
  module UsageHelper
    def record_usage(type:, id:, count: 1)
      Zendesk::RuleSelection::RuleUsage.new(account).tap do |rule_usage|
        count.times { rule_usage.record(rule_id: id, type: type) }
      end
    end

    def base_key(account, timeframe, type)
      "rule:#{type}:usage:for-account:#{account.id}:#{timeframe}"
    end

    def monthly_key(account, type, time)
      "#{base_key(account, :monthly, type)}#{format(':%s', time.to_date)}"
    end

    def weekly_key(account, type, time)
      "#{base_key(account, :weekly, type)}#{format(':%s', time.to_date)}"
    end

    def daily_key(account, type, time)
      "#{base_key(account, :daily, type)}" \
        "#{format(':%s:%0.2d', time.to_date, time.hour)}"
    end

    def hourly_key(account, type, time)
      "#{base_key(account, :hourly, type)}" \
        "#{format(':%s:%0.2d:%0.2d', time.to_date, time.hour, time.min)}"
    end
  end
end
