require_relative "features_helpers"
require_relative "billing_test_helper"

module BoostingPricingModelFeatureTests
  include FeaturesHelpers
  include BillingTestHelper

  def should_have_boostable_features
    let(:plus_boost_features) do
      [
        :agent_collision,
        :agent_display_names,
        :business_hours,
        :categorized_forums,
        :chat,
        :community,
        :community_forums,
        :credit_card_sanitization,
        :custom_security_policy,
        :customer_satisfaction,
        :customizable_help_center_themes,
        :dc_in_templates,
        :extended_ticket_metrics,
        :forum_statistics,
        :gooddata_advanced_analytics,
        :group_rules,
        :help_center_analytics,
        :help_center_article_labels,
        :help_center_google_analytics,
        :host_mapping,
        :incremental_export,
        :individual_language_selection,
        :individual_time_zone_selection,
        :internal_help_center,
        :ip_restrictions,
        :macro_search,
        :multiple_organizations,
        :nps_surveys,
        :personal_rules,
        :report_feed,
        :rule_usage_stats,
        :saml,
        :sandbox,
        :screencasts_for_tickets,
        :search_statistics,
        :service_level_agreements,
        :user_views, # Display customer lists when account is boosted
        :user_xml_export,
        :view_csv_export,
        :voice_business_hours,
        :xml_export
      ]
    end

    let(:enterprise_boost_features) do
      [
        :agent_collision,
        :agent_display_names,
        :audit_log,
        :bcc_archiving,
        :business_hours,
        :categorized_forums,
        :chat,
        :collision_chat,
        :community,
        :community_forums,
        :conditional_fields_app,
        :credit_card_sanitization,
        :custom_security_policy,
        :customer_satisfaction,
        :customizable_help_center_themes,
        :dc_in_templates,
        :extended_ticket_metrics,
        :forum_statistics,
        :gooddata_advanced_analytics,
        :gooddata_hourly_synchronization,
        :group_rules,
        :help_center_analytics,
        :help_center_article_labels,
        :help_center_google_analytics,
        :host_mapping,
        :hourly_incremental_export,
        :incremental_export,
        :individual_language_selection,
        :individual_time_zone_selection,
        :internal_help_center,
        :ip_restrictions,
        :light_agents,
        :macro_search,
        :multibrand,
        :multiple_organizations,
        :multiple_schedules,
        :nps_surveys,
        :permission_sets,
        :personal_rules,
        :report_feed,
        :rule_analysis,
        :rule_usage_stats,
        :saml,
        :sandbox,
        :satisfaction_prediction,
        :screencasts_for_tickets,
        :search_statistics,
        :search_statistics,
        :service_level_agreements,
        :tag_phrases_for_forum_entries,
        :ticket_forms,
        :ticket_sharing_triggers,
        :unlimited_multibrand,
        :user_views, # Display customer lists when account is boosted
        :user_xml_export,
        :view_csv_export,
        :voice_business_hours,
        :xml_export
      ]
    end

    let(:enterprise_specific_features) do
      [
        :audit_log,
        :bcc_archiving,
        :collision_chat,
        :conditional_fields_app,
        :gooddata_hourly_synchronization,
        :hourly_incremental_export,
        :light_agents,
        :multibrand,
        :multiple_schedules,
        :permission_sets,
        :rule_analysis,
        :satisfaction_prediction,
        :tag_phrases_for_forum_entries,
        :ticket_forms,
        :ticket_sharing_triggers,
        :unlimited_multibrand
      ]
    end

    let(:nonboostable_features) { [:hosted_ssl] }

    describe "for regular plans" do
      before do
        @subscription.update_attributes! plan_type: SubscriptionPlanType.Medium
      end

      describe "when boosted to Large (Plus)" do
        before do
          @subscription.account.create_feature_boost(boost_level: SubscriptionPlanType.Large, valid_until: 20.days.from_now)
          assert @subscription.account.boosted?
        end

        it "has all plus features except unboostable ones" do
          plus_boost_features.each do |feature|
            assert_feature true, feature
          end
        end

        it "does not have enterprise features enabled" do
          enterprise_specific_features.each do |feature|
            assert_feature false, feature
          end
        end

        it "does not have nonboostable_features features enabled" do
          nonboostable_features.each do |feature|
            assert_feature false, feature
          end
        end
      end

      describe "when boosted to ExtraLarge (Enterprise)" do
        before do
          @subscription.account.create_feature_boost(boost_level: SubscriptionPlanType.ExtraLarge, valid_until: 20.days.from_now)
          assert @subscription.account.boosted?
        end

        it "has all enterprise features except unboostable ones" do
          enterprise_boost_features.each do |feature|
            assert_feature true, feature
          end
        end

        it "does not have nonboostable_features features enabled" do
          nonboostable_features.each do |feature|
            assert_feature false, feature
          end
        end
      end
    end
  end
end

class ActiveSupport::TestCase
  extend BoostingPricingModelFeatureTests
end
