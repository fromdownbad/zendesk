module SuiteTestHelper
  def self.included(base)
    base.class_eval do
      before do
        Account.any_instance.stubs(:has_active_suite?).returns(false)
      end
    end
  end
end
