module InboundMailRateLimitTestHelper
  private

  def create_inbound_mail_rate_limit(params = {})
    inbound_mail_rate_limit = InboundMailRateLimit.new(
      is_sender: params.fetch(:is_sender) { true },
      email: params.fetch(:email) { account.admins.first.email },
      rate_limit: params.fetch(:rate_limit) { 9001 }
    ).tap do |new_inbound_mail_rate_limit|
      new_inbound_mail_rate_limit.account_id = params.fetch(:account) { account }.id
    end
    inbound_mail_rate_limit.save!
    inbound_mail_rate_limit
  end
end
