module MultiproductTestHelper
  def it_allows_access_for_all_multiproduct_product_types(action = :index)
    describe 'multiproduct' do
      let(:account) { accounts(:multiproduct) }

      before do
        Zendesk::Accounts::Client.any_instance.stubs(:products).raises(StandardError.new('should not have been called'))
      end

      it 'always allows access to multiproduct accounts' do
        get action
        assert_response :success
      end
    end
  end

  def stub_account_service_account(account, response_body: { account: { billing_id: nil } })
    stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}").
      to_return(body: response_body.to_json, headers: { content_type: 'application/json; charset=utf-8' })
  end

  def stub_account_service_product(product, data)
    stub_request(:get, %r{/api/services/accounts/[0-9]+/products/#{product}}).
      to_return(body: { product: data }.to_json, headers: { content_type: 'application/json; charset=utf-8' })
  end

  def stub_account_service_support_product(account, response_body: { product: FactoryBot.build(:support_trial_product) })
    stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products/support").
      to_return(body: response_body.to_json, headers: { content_type: 'application/json; charset=utf-8' })
  end

  def stub_account_service_missing_product(product)
    stub_request(:get, %r{/api/services/accounts/[0-9]+/products/#{product}}).
      to_return(status: 404)
  end

  def stub_account_service_guide_product
    stub_account_service_missing_product 'guide'
  end

  def stub_account_service_products(products: [FactoryBot.build(:support_trial_product)])
    stub_request(:get, %r{/api/services/accounts/[0-9]+/products$}).to_return(body: { products: products }.to_json, headers: { content_type: 'application/json; charset=utf-8' })
  end

  def stub_account_service_post_products
    stub_request(
      :post,
      /https\:\/\/accounts\.zendesk-test.com\/api\/services\/accounts\/\d+\/products\?include_deleted_account=false/
    )
  end

  def setup_shell_account_with_support
    stub_account_service_post_products
    account = setup_shell_account
    stub_account_service_account(account)
    stub_staff_service_entitlement_sync
    Zendesk::TrialActivation.activate_support_trial!(account)

    Account.find(account.id)
  end

  def setup_shell_account(subdomain: 'test-shell-account', account_name: 'shell account')
    stub_staff_service_entitlement_sync

    shell_account = ShellAccount.create(
      name: account_name,
      subdomain: subdomain,
      time_zone: 'America/New_York',
      is_active: true,
      is_serviceable: true,
      multiproduct: true
    )

    account = ::Accounts::ShellAccountActivation.find(shell_account.id)
    route = ShellRoute.create(account_id: account.id, subdomain: subdomain)

    ShellRoleSettings.create(account_id: account.id)
    ShellAddress.create(account_id: account.id, name: account_name, phone: '18888888888')

    ActiveRecord::Base.on_shard(account.shard_id) do
      ShellUser.create(
        account_id: account.id,
        name: 'Shell Acct Owner',
        roles: 2,
        is_active: true
      )
    end

    owner = ActiveRecord::Base.on_shard(account.shard_id) { User.find_by_account_id(account.id) }

    ActiveRecord::Base.on_shard(account.shard_id) do
      UserEmailIdentity.create(
        account: account,
        user: owner,
        value: 'testowner@example.com',
        is_verified: true
      )
    end

    account.update_column(:route_id, route.id)
    account.update_column(:owner_id, owner.id)

    account
  end
end

class ShellAccount < ActiveRecord::Base
  self.table_name = Account.table_name
end

class ShellRoute < ActiveRecord::Base
  self.table_name = Route.table_name
end

class ShellRoleSettings < ActiveRecord::Base
  self.table_name = RoleSettings.table_name
end

class ShellAddress < ActiveRecord::Base
  self.table_name = Address.table_name
end

class ShellUser < ActiveRecord::Base
  self.table_name = User.table_name
end

class ShellUserIdentity < ActiveRecord::Base
  self.table_name = UserIdentity.table_name
end
