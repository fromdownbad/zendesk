class CollaborationSettingsTestHelper
  def initialize(feature_arturo: false, feature_setting: false, followers_setting: false, email_ccs_setting: false)
    @feature_arturo = feature_arturo
    @feature_setting = feature_setting
    @followers_setting = followers_setting
    @email_ccs_setting = email_ccs_setting
  end

  def to_s
    [].tap do |summary|
      summary << "email_ccs arturo #{on_off?(@feature_arturo)}"
      summary << "follower_and_email_cc_collaborations setting #{on_off?(@feature_setting)}"
      summary << "ticket_followers_allowed #{on_off?(@followers_setting)}"
      summary << "comment_email_ccs_allowed #{on_off?(@email_ccs_setting)}"
    end.join("::")
  end

  def apply
    Account.any_instance.stubs(:has_email_ccs?).returns(@feature_arturo)
    Account.any_instance.stubs(:has_follower_and_email_cc_collaborations_enabled?).returns(@feature_setting)
    Account.any_instance.stubs(:has_ticket_followers_allowed_enabled?).returns(@followers_setting)
    Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(@email_ccs_setting)
  end

  def self.enable_all_new_collaboration_settings
    new(
      feature_arturo: true,
      feature_setting: true,
      followers_setting: true,
      email_ccs_setting: true
    ).apply
  end

  def self.disable_all_new_collaboration_settings
    new(
      feature_arturo: false,
      feature_setting: false,
      followers_setting: false,
      email_ccs_setting: false
    ).apply
  end

  private

  def on_off?(bool)
    bool ? "on" : "off"
  end
end
