class ActiveSupport::TestCase
  def load_fixtures_with_account_ids_table(*args)
    load_fixtures_without_account_ids_table(*args)
  ensure
    ActiveRecord::Base.connection.execute('INSERT IGNORE INTO account_ids SELECT id FROM accounts')
  end
  alias_method :load_fixtures_without_account_ids_table, :load_fixtures
  alias_method :load_fixtures, :load_fixtures_with_account_ids_table
end
