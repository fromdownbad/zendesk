module ClassicTestHelper
  def create_account_from_hash(values, set_owner = true, owner_is_verified = false)
    new_account = values.fetch(:class, Accounts::Classic).new do |a|
      a.name            = values[:name]
      a.subdomain       = values[:subdomain]
      a.time_zone       = values[:time_zone]
      a.help_desk_size  = values[:help_desk_size]
      a.trial_coupon_code = values[:trial_coupon_code]
    end

    new_account.translation_locale = TranslationLocale.last

    if set_owner
      owner_attributes = {name: 'Random Person', email: 'random.person@example.com', is_verified: owner_is_verified}
      new_account.set_owner(owner_attributes)
    end

    new_account.set_address(country_code: 'SI', phone: '123456')

    new_account.build_pre_account_creation(
      locale_id: 1,
      account_class: Accounts::Classic.to_s,
      region: 'us',
      source: 'Marketing website',
      pod_id: 1,
      status: PreAccountCreation::ProvisionStatus::ONGOING
    )

    new_account
  end

  def default_account_hash
    {
      name: 'NewTestAccount',
      subdomain: Token.generate(10),
      time_zone: 'Copenhagen',
      help_desk_size: "Small team"
    }
  end
end
