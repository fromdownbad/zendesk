# The default Rails cookies store will accept values in two ways:
#
#     cookies[:foo] = "bar"
#     cookies[:foo] = { :path => "/", :value => "bar", ... }
#
# Once the cookie is set, in either of these two ways, it just acts
# like a normal reader, e.g. either strategy would respond correctly with:
#
#     cookies[:foo] # => "bar"
#
class FakeCookieStore < Hash
  def method_missing(_name, *_args)
    self
  end

  def []=(key, value)
    if value.is_a?(Hash) && (v = value[:value])
      super(key, v)
    else
      super
    end
  end
end

class FakeRequest
  attr_reader :env

  def initialize
    @env = {'action_dispatch.cookies' => ActionDispatch::Cookies::CookieJar.new(nil)}
  end
end
