module CustomFieldsTestHelper
  private

  def create_custom_field!(key, title, owner, type, options = {})
    cf = @account.custom_fields.build(key: key, title: title, owner: owner)
    cf.regexp_for_validation = options[:regexp_for_validation]
    cf.type = type
    cf.is_system = options[:is_system].presence || cf.is_system
    cf.save!
    # get it to be the proper STI type
    cf = @account.custom_fields.find(cf.id)
    if options[:dropdown_choices]
      options[:dropdown_choices].each { |choice| cf.dropdown_choices.build(name: choice[:name], value: choice[:value]) }
      cf.save!
    end
    cf
  end

  def create_user_custom_field!(key, title, type, options = {})
    create_custom_field!(key, title, "User", type, options)
  end

  def create_organization_custom_field!(key, title, type, options = {})
    create_custom_field!(key, title, "Organization", type, options)
  end
end
