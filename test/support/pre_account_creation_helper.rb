module PreAccountCreationHelper
  def self.included(base)
    base.class_eval do
      def get_precreated_account(name:,
        account_id:,
        target_locale_id:,
        account_class: PreAccountCreation::DEFAULT_ACCOUNT_CLASS)
        # fixtures are soft deleted on initialization
        PreAccountCreation.unscoped do
          pre_account_creations(name.to_sym).tap do |acc|
            acc.update_attributes(
              deleted_at: nil,
              account_class: account_class,
              locale_id:  target_locale_id,
              account_id: account_id
            )
          end
        end
      end

      def pre_account_creation_callbacks
        [
          :update_property_set,
          :precreate_new_account,
          :update_account_timestamps,
          :create_support_product,
          :generate_owner_welcome_email,
          :enqueue_timestamps_update_job,
          :enqueue_fraud_score_job,
          :generate_sample_ticket_by_locale,
          :refresh_account_trial_limits,
          :add_google_domain,
          :save_trial_extras,
          :setup_chat_to_support_xsell,
          :sync_entitlements,
          :set_support_suite_trial_refresh_capability,
        ]
      end

      def initialize_account(extra_params = {})
        @account_name = "Wombat Snowboards"
        @account_subdomain = "wombatsnowboards"
        @params = {
          account: {
            name: @account_name,
            subdomain: @account_subdomain,
            help_desk_size: "Small team"
          },
          owner: {
            name: "Sir Wombat",
            email: "wbsnowboards@tz.com",
            is_verified: "1"
          },
          address: {
            phone: "+1-123-456-7890",
            country_code: "US"
          },
          trial_extras: {
            "expected_num_seats" => "1-14",
            "Convertro_SID__c" => "EWN99GMXGZ6W",
            "Session_Landing__c" => "https://www.zendesk.com/apps/surveymonkey-create/",
            "Sub_Industry" => "",
            "Behavioral_1__c" => "",
            "Behavioral_2__c" => "",
            "Behavioral_3__c" => "",
            "DB_City__c" => "",
            "DB_State__c" => "",
            "DB_CName__c" => "",
            "DB_CCode__c" => "",
            "Country__c" => "",
            "DB_Zip__c" => "",
            "Behavioral_4__c" => "",
            "Session_Count__c" => "",
            "Session_First__c" => "",
            "Session_Last__c" => "",
            "Session_Referrer__c" => "",
            "features" => ""
          }
        }.deep_merge(extra_params)
        @request_ip = "98.210.110.15"
        Zendesk::GeoLocation.stubs(:locate).returns(city: "San Francisco", country_code: "US")
        @account_initializer = Zendesk::Accounts::Initializer.new(@params, @request_ip, :us)
        @new_account = @account_initializer.account
      end
    end
  end
end
