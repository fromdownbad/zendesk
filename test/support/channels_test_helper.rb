module ChannelsTestHelper
  def read_json(path)
    JSON.parse(read_test_file(path))
  end

  def ris_presenter_dummy_response(ris_set, include_isi = false)
    JSON.parse({
      registered_integration_services: ris_set.map do |ris|
        ris_attributes = ris.attributes
        if include_isi == true
          isi_data = { integration_service_instances: ris.integration_service_instances.map(&:attributes) }
          ris_attributes.merge!(isi_data)
        end
        ris_attributes
      end,
      next_page: nil,
      previous_page: nil,
      count: ris_set.count
    }.to_json)
  end

  def redefine_constant(klass, const, value)
    old_value = klass.const_get(const)
    klass.send(:remove_const, const)
    klass.const_set(const, value)
    old_value
  end

  def assert_attributes_from_hash(hash, attribute_infos)
    attribute_infos.each { |ai| assert_attribute_from_hash(hash, ai) }
  end

  def assert_attribute_from_hash(hash, attribute_info)
    path = attribute_info[0]
    expected = extract_path_from_hash(hash, path)
    actual = attribute_info[1]
    label = attribute_info[2]

    assert_equal expected, actual, label
  end

  def extract_path_from_hash(hash, path)
    return hash[path] if path.is_a?(String)

    return hash[path[0]] if path.length == 1
    extract_path_from_hash(hash[path.shift], path)
  end

  def facebook_appsecret_proof(access_token)
    Digest::HMAC.hexdigest(
      access_token,
      Zendesk::Configuration.dig!(:facebook_integration, :secret),
      Digest::SHA256
    )
  end
end
