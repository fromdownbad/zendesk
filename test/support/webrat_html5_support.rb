require 'webrat/core/elements/field'
require 'webrat/core/scope'

module Webrat
  class SearchField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'search']"]
    end
  end

  class EmailField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'email']"]
    end
  end

  class URLField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'url']"]
    end
  end

  class TelephoneField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'tel']"]
    end
  end

  class ColorField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'color']"]
    end
  end

  class NumberField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'number']"]
    end
  end

  class RangeField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'range']"]
    end
  end

  class DateField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'date']"]
    end
  end

  class MonthField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'month']"]
    end
  end

  class WeekField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'week']"]
    end
  end

  class TimeField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'time']"]
    end
  end

  class DateTimeField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'datetime']"]
    end
  end

  class DateTimeLocalField < Webrat::Field #:nodoc:
    def self.xpath_search
      [".//input[@type = 'datetime-local']"]
    end
  end

  TEXTUAL_FIELDS = [
    TextField, TextareaField, PasswordField,
    SearchField, EmailField, URLField, TelephoneField,
    ColorField, NumberField, RangeField,
    DateField, MonthField, WeekField, TimeField,
    DateTimeField, DateTimeLocalField
  ].freeze

  def Field.field_class(element)
    case element.name
    when "button" then ButtonField
    when "select"
      if element.attributes["multiple"].nil?
        SelectField
      else
        MultipleSelectField
      end
    when "textarea" then TextareaField
    else
      case element["type"]
      when "checkbox" then CheckboxField
      when "hidden"   then HiddenField
      when "radio"    then RadioField
      when "password" then PasswordField
      when 'search'   then SearchField
      when 'email'    then EmailField
      when 'url'      then URLField
      when 'tel'      then TelephoneField
      when 'color'    then ColorField
      when 'number'   then NumberField
      when 'range'    then RangeField
      when 'date'     then DateField
      when 'month'    then MonthField
      when 'week'     then WeekField
      when 'time'     then TimeField
      when 'datetime' then DateTimeField
      when 'datetime-local' then DateTimeLocalField
      when "file"     then FileField
      when "reset"    then ResetField
      when "submit"   then ButtonField
      when "button"   then ButtonField
      when "image"    then ButtonField
      else TextField
      end
    end
  end
end

Webrat::Scope.class_eval do
  def fill_in(field_locator, options = {})
    field = locate_field(field_locator, *Webrat::TEXTUAL_FIELDS)
    field.raise_error_if_disabled
    field.set(options[:with])
  end
end
