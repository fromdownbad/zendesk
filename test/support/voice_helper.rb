ActiveSupport::TestCase.class_eval do
  set_fixture_class voice_subscriptions: Voice::Subscription

  def mock_voice_call
    @sub_account_stub = stub_request(:get, %r{/api/v2/channels/voice/sub_account}).
      to_return(
        body: {
          sub_account: {
            id: 1,
            opted_in: true,
            out_of_credits: false,
            balance: 0,
            features: {
              multiple_numbers: true,
              call_hold: true,
              group_routing: true,
              call_activity: true,
              legacy_features: true,
              ivr: false,
              transfer: false,
              failover: false,
              dashboard: false
            }
          }
        }.to_json,
        headers: {
          content_type: "application/json"
        }
      )
  end

  setup :mock_voice_call

  def stub_voice
    stub_request(:put, %r{/api/v2/channels/voice/internal/sub_accounts/suspend})
    stub_request(:post, %r{api/v2/channels/voice/internal/availabilities/agent_forwarding_number_removed.json})
  end

  setup :stub_voice
end
