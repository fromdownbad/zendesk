require 'set'

module AccountProductsTestHelper
  INACTIVE_STATES = Set.new(['inactive', 'cancelled', 'expired']).freeze

  def make_generic_products_response(account, product_names)
    products = product_names.map.with_index do |n, i|
      make_product(account_id: account.id, product_id: i, product_name: n)
    end

    { "products" => products }.to_json
  end

  def make_product(
    product_id: 1,
    account_id: 1,
    product_name: 'acme_app',
    state: 'trial',
    state_updated_at: Time.now + 20.days,
    trial_expires_at: Time.now,
    plan_settings: default_plan_settings,
    created_at: Time.now,
    updated_at: Time.now
  )
    {
      "id" => product_id,
      "account_id" => account_id,
      "name" => product_name,
      "active" => calculate_active(state),
      "state" => state,
      "state_updated_at" => state_updated_at,
      "trial_expires_at" => trial_expires_at,
      "plan_settings" => plan_settings,
      "created_at" => created_at,
      "updated_at" => updated_at
    }.
      # Stronger parameters doesn't like superfluous nils unless the type
      # explicitly says they're expected. Remove them all and if you really
      # need them, splice them in deliberately afterward.
      compact
  end

  def default_plan_settings
    { "plan_type" => 1, "max_agents" => 100 }
  end

  def calculate_active(state)
    !INACTIVE_STATES.include?(state)
  end
end
