module DomainEventsHelper
  def decode_user_events(events, type = nil)
    decoded_events = events.map do |event|
      proto = event.fetch(:value)
      ::Zendesk::Protobuf::Support::Users::V2::UserEvent.decode(proto)
    end

    filter_domain_events(decoded_events, type)
  end

  def decoded_organization_events(events, type = nil)
    decoded_events = events.map do |event|
      proto = event.fetch(:value)
      ::Zendesk::Protobuf::Support::Users::V2::OrganizationEvent.decode(proto)
    end

    filter_domain_events(decoded_events, type)
  end

  def filter_domain_events(events, type)
    return events if type.nil?

    events.select { |event| event.event == type }
  end
end
