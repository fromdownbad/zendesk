module TestSupport::Routing
  module Helper
    def new_attribute_ticket_map(params = {})
      params.fetch(:account) { account }.attribute_ticket_maps.create! do |atm|
        atm.attribute_id       = params.fetch(:attribute_id) { '1' }
        atm.attribute_value_id = params.fetch(:attribute_value_id) { '2' }

        atm.definition = begin
          Definition.build(
            conditions_all: params.fetch(:conditions_all) do
              [
                {
                  source:   'comment_includes_word',
                  operator: 'includes',
                  value:    params.fetch(:comment) { 'test' }
                }
              ]
            end
          )
        end
      end
    end
  end
end
