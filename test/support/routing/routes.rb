# all descendants of rules_controller have controller_path overwritten to use the views of rules -> would also change the test controller
# so we allow these normally non-existent routes to work
%i[index show edit new create update destroy_inactive activate sort destroy preview].each do |action|
  match "/rules/#{action}", controller: :rules, action: action, via: :all
  match "/rules/#{action}/:id", controller: :rules, action: action, via: :all
end

# HACK: to prevent settings/base_controller_test to blow up, works in development
match '/settings/access', controller: 'settings/access', action: :logout, via: :all
match '/settings/search', controller: 'settings/search', action: :index, via: :all
