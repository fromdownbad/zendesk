module SatisfactionTestHelper
  def build_ticket(options = {})
    ticket = accounts(:minimum).tickets.new
    ticket.assignee = options.delete(:assignee) || users(:minimum_agent)

    attributes = options.merge(
      subject: "Foo!",
      description: "BAR!",
      requester: users(:minimum_end_user)
    )
    ticket.attributes = attributes
    ticket.will_be_saved_by(users(:minimum_end_user))
    ticket.save!

    ticket.reload
  end

  def build_satisfaction_rating(ticket, options = {})
    ticket.attributes = {
      satisfaction_comment: 'I laughed. I cried. It was better than Cats.',
      satisfaction_score: SatisfactionType.GOOD
    }.merge(options)

    ticket.will_be_saved_by(users(:minimum_end_user))
    ticket.save!
    ticket
  end

  def build_ticket_with_satisfaction_rating(options = {})
    build_satisfaction_rating(build_ticket, options)
  end

  def add_bad_satisfaction_rating_to_ticket(ticket, options = {})
    ticket.attributes = {
      satisfaction_comment: 'I laughed. I cried. It was better than Cats.',
      satisfaction_score: SatisfactionType.BAD
    }.merge(options)

    ticket.will_be_saved_by(users(:minimum_end_user))
    ticket.save!
  end

  def set_ticket_satisfaction_rating(ticket_id, rating, assignee_id = :minimum_agent)
    ticket = tickets(ticket_id)
    ticket.attributes = {
      satisfaction_comment: 'I laughed. I cried. It was better than Cats.',
      satisfaction_score: rating
    }
    ticket.assignee = users(assignee_id)
    ticket.will_be_saved_by(ticket.requester)
    ticket.solved_at = nil
    ticket.save!
    ticket
  end
end
