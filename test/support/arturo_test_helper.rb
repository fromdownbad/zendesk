module ArturoTestHelper
  def describe_with_and_without_arturo_enabled(arturo, stub = Account.any_instance, &block)
    [true, false].each { |enabled| describe_with_arturo(arturo: arturo, stub: stub, enabled: enabled, &block) }
  end

  def describe_with_arturo_enabled(arturo, stub = Account.any_instance, &block)
    describe_with_arturo(arturo: arturo, stub: stub, enabled: true, &block)
  end

  def describe_with_arturo_disabled(arturo, stub = Account.any_instance, &block)
    describe_with_arturo(arturo: arturo, stub: stub, enabled: false, &block)
  end

  def describe_with_and_without_arturo_setting_enabled(arturo, stub = Account.any_instance, &block)
    [true, false].each { |enabled| describe_with_arturo(arturo: arturo, stub: stub, setting: true, enabled: enabled, &block) }
  end

  def describe_with_arturo_setting_enabled(arturo, stub = Account.any_instance, &block)
    describe_with_arturo(arturo: arturo, stub: stub, setting: true, enabled: true, &block)
  end

  def describe_with_arturo_setting_disabled(arturo, stub = Account.any_instance, &block)
    describe_with_arturo(arturo: arturo, stub: stub, setting: true, enabled: false, &block)
  end

  def describe_with_arturo(arturo:, stub: Account.any_instance, setting: false, enabled:, &block)
    arturo = arturo.is_a?(Symbol) ? arturo.to_s : arturo
    describe "with '#{arturo}' #{setting ? 'setting' : 'feature'} #{enabled ? 'enabled' : 'disabled'}" do
      before do
        setting_suffix = setting ? "_enabled" : ""
        stub.stubs("has_#{arturo}#{setting_suffix}?".to_sym).returns(enabled)
        @arturo_enabled = enabled
      end

      class_eval(&block)
    end
  end
end
