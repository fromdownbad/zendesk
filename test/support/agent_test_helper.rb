module AgentTestHelper
  def create_light_agent
    Account.any_instance.stubs(has_permission_sets?: true, has_light_agents?: true)

    light_agent = User.create! do |user|
      user.account = accounts(:minimum)
      user.name = "Prince Light"
      user.email = "prince@example.org"
      user.roles = Role::AGENT.id
    end

    # We do not want to enqueue this job in test
    PermissionSet.stubs(:pravda_role_sync)

    light_agent.tap do |user|
      user.permission_set = PermissionSet.create_light_agent!(accounts(:minimum))
      user.permission_set.permissions.set(PermissionSet::LightAgent.configuration)
      user.save!
    end
  end

  def create_custom_role_agent(configuration = {})
    Account.any_instance.stubs(has_permission_sets?: true)

    custom_agent = User.create! do |user|
      user.account = accounts(:minimum)
      user.name = "Custom Agent"
      user.email = "custom@example.org"
      user.roles = Role::AGENT.id
    end

    custom_agent.tap do |user|
      user.permission_set ||=
        user.account.permission_sets.create!(name: 'Custom Agent Role', role_type: PermissionSet::Type::CUSTOM)
      user.permission_set.permissions.set(default_configuration.merge(configuration))
      user.permission_set.save!
      user.save!
    end
  end

  def update_comment_permission(user, comment_type:)
    Account.any_instance.stubs(:has_permission_sets?).returns(true)

    user.permission_set ||= user.account.permission_sets.create!(name: 'Test')
    user.permission_set.permissions.comment_access = comment_type

    user.permission_set.save!
    user.save!

    user
  end

  private

  def default_configuration
    {
      ticket_access:                     'all',      # 'all', 'within-groups', 'within-organization', 'assigned-only'
      comment_access:                    'public',   # 'public', 'private'
      ticket_editing:                    true,
      ticket_deletion:                   true,
      ticket_merge:                      true,
      edit_ticket_tags:                  true,
      assign_tickets_to_any_group:       true,
      view_deleted_tickets:              true,

      end_user_profile:                  'readonly', # 'readonly', 'edit-within-org', 'full'
      available_user_lists:              'none',     # 'all', 'none'
      edit_organizations:                false,

      explore_access:                    'none',     # 'edit', 'none'
      report_access:                     'readonly',
      view_access:                       'readonly',
      macro_access:                      'readonly',
      user_view_access:                  'none',

      forum_access:                      'readonly',
      forum_access_restricted_content:   false,

      voice_availability_access:         false,
      voice_dashboard_access:            false,
      chat_availability_access:          false,

      business_rule_management:          false,
      extensions_and_channel_management: false,

      view_twitter_searches:             true,

      manage_dynamic_content:            false,
      manage_facebook:                   false
    }
  end
end
