module UserViewsTestHelper
  private

  def build_condition(field, operator, value)
    {}.tap do |d|
      d[:field]    = field.is_a?(String) ? field : "custom_fields.#{field.key}"
      d[:operator] = operator
      d[:value]    = value
    end
  end

  def update_timestamps
    @users[0..1].each do |u|
      u.created_at = Time.parse("2012-10-09")
      u.save!
    end
    @users[2].created_at = Time.parse("2012-05-05")
    @users[2].save!
  end

  def create_custom_fields
    @text_field     = create_user_custom_field!("field_1", "Field 1", "Text")
    @decimal_field  = create_user_custom_field!("field_2", "Field 2", "Decimal")
    @checkbox_field = create_user_custom_field!("field_3", "Field 3", "Checkbox")
    @date_field     = create_user_custom_field!("field_4", "Field 4", "Date")
  end

  def create_custom_field_values
    @users[0..1].each do |u|
      u.custom_field_values.build(field: @text_field,     value: "Moal").save!
      u.custom_field_values.build(field: @checkbox_field, value: false).save!
      u.custom_field_values.build(field: @date_field,     value: "2012-10-10").save!
    end

    @users[2].custom_field_values.build(field: @text_field,     value: "Paz").save!
    @users[2].custom_field_values.build(field: @decimal_field,  value: 3).save!
    @users[2].custom_field_values.build(field: @checkbox_field, value: true).save!
    @users[2].custom_field_values.build(field: @date_field,     value: "2012-12-20").save!
  end

  def create_dropdown_field(key = "field_dropdown", title = "Field Dropdown", choices = [])
    @dropdown_field = create_user_custom_field!(key, title, "Dropdown")
    choices = [{name: "Option one", value: "1"}, {name: "Option two", value: "2"}] unless choices.present?
    choices.each do |choice|
      @dropdown_field.dropdown_choices.build(name: choice[:name], value: choice[:value])
    end
    @dropdown_field.save!
  end

  def create_dropdown_field_values
    @users[0..1].each do |u|
      u.custom_field_values.update_from_hash(field_dropdown: "1")
      u.save!
      u.reload
    end
    @users[2].custom_field_values.update_from_hash(field_dropdown: "2")
    @users[2].save!
    @users[2].reload
  end

  def create_user_tags
    AccountSetting.any_instance.stubs(:value).returns(1) # to enable tags
    @users[0..1].each do |uu|
      uu.tags = %w[the_owner super_user]
      uu.save!
      uu.reload
    end
    @users[2].tags = %w[other_tag]
    @users[2].save!
    @users[2].reload
  end

  def view_params(all, any, output)
    {}.tap do |vp|
      vp[:user_view] = {}.tap do |p|
        p[:all]    = all if all
        p[:any]    = any if any
        p[:output] = output if output
        p[:title]  = "A View to a Kill"
      end
    end
  end

  def build_view(conditions_all, conditions_any = nil, output = nil)
    params = view_params(conditions_all, conditions_any, output)
    create_view(params[:user_view])
  end

  def create_view(params)
    view = Zendesk::Rules::UserViewInitializer.new(@account, {user_view: params}, :user_view).user_view
    view.save!
    view
  end

  def sort(field, order)
    OpenStruct.new.tap do |o|
      o.sort = {}.tap do |sort|
        sort[:id]    = field
        sort[:order] = order
      end
    end
  end

  def create_and_execute_view(params)
    view = create_view setup_params(params)
    get :execute, params: { id: view.id }
    @result = JSON.parse(@response.body)
    @user_ids = @result["rows"].map { |user| user["id"] }
  end

  def preview_view(params)
    post :preview, params: { user_view: setup_params(params).slice('all', 'any', 'execution') }
    @result = JSON.parse(@response.body)
    @user_ids = @result["rows"].map { |user| user["id"] }
  end

  def assert_record_invalidation(error, message)
    assert_equal "422", @response.code
    assert_equal({
      "error" => "RecordInvalid",
      "description" => "Record validation errors",
      "details" => { "base" => [{ "error" => error, "description" => message }] }
    }, JSON.parse(@response.body))
  end

  def view_with_removed_locale(locale)
    Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
    @account.allowed_translation_locales << locale
    @account.save
    view = create_view setup_params("all" => [{"field" => "locale_id", "operator" => "is", "value" => locale.id }])
    @account.allowed_translation_locales.delete(locale)
    @account.save
    view
  end

  def setup_params(options = {})
    @params = HashWithIndifferentAccess.new(
      "title" => "Title",
      "all" => [],
      "any" => [],
      "active" => true,
      "execution" => {}
    ).merge(options)
  end

  def all_conditions
    {
      "title" => "Gold Users",
      "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold" }]
    }
  end

  def any_conditions
    {
      "title" => "Top tier users",
      "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold" }],
      "any" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "silver" }]
    }
  end

  def restricted_to_agent(agent_id)
    {
      "title" => "Gold Users",
      "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold" }],
      "restriction" => { "type" => "User", "id" => agent_id }
    }
  end

  def restricted_to_group(group_id)
    {
      "title" => "Gold Users",
      "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold" }],
      "restriction" => { "type" => "Group", "id" => group_id }
    }
  end

  def restricted_to_account(_account_id)
    {
      "title" => "Gold Users",
      "all" => [{ "field" => "custom_fields.plan_type", "operator" => "is", "value" => "gold" }],
      "restriction" => nil
    }
  end

  def grouping
    {
      "title" => "Grouped By Name",
      "all" => [{ "field" => "active", "operator" => "is", "value" => true }],
      "execution" => { "group" => { "id" => "created_at", "order" => "desc" } }
    }
  end

  def sorting
    {
      "title" => "Sorted By Name",
      "all" => [{ "field" => "active", "operator" => "is", "value" => true }],
      "execution" => { "sort" => { "id" => "name", "order" => "desc" } }
    }
  end

  def column_selection
    {
      "title" => "With name and active columns",
      "all" => [{ "field" => "active", "operator" => "is", "value" => true }],
      "execution" => { "columns" => ["name", "current_tags"] }
    }
  end
end
