module SharingTestHelper
  def new_ticket_sharing_agreement(options = {})
    @current_agreement_uuid_index ||= 0
    @current_agreement_uuid_index += 1

    TicketSharing::Agreement.new({
      'uuid' => "agreement#{@current_agreement_uuid_index}",
      'sender_url' => 'http://sender.example.com/sharing',
      'receiver_url' => 'http://receiver.example.com/sharing',
      'status' => 'pending',
      'access_key' => '<access_key>'
    }.merge(options))
  end

  def new_ticket_sharing_ticket(options = {})
    @current_ticket_uuid_index ||= 0
    @current_ticket_uuid_index += 1

    ts_ticket = TicketSharing::Ticket.new({
      'uuid' => "Ticket#{@current_ticket_uuid_index}",
      'subject' => "The Subject for Ticket #{@current_ticket_uuid_index}",
      'status' => 'new',
      'requested_at' => 5.days.ago,
      'original_id' => 123
    }.merge(options))

    ts_ticket.requester ||= new_ticket_sharing_actor
    if ts_ticket.comments.empty?
      ts_ticket.comments << new_ticket_sharing_comment('author' => ts_ticket.requester)
    end
    ts_ticket
  end

  def new_ticket_sharing_actor(options = {})
    @current_actor_uuid_index ||= 0
    @current_actor_uuid_index += 1

    TicketSharing::Actor.new({
      'uuid' => "Actor#{@current_actor_uuid_index}",
      'name' => "Actor #{@current_actor_uuid_index}",
      'role' => 'user'
    }.merge(options))
  end

  def new_ticket_sharing_comment(options = {})
    @current_comment_uuid_index ||= 0
    @current_comment_uuid_index += 1

    ts_comment = TicketSharing::Comment.new({
      'uuid' => "Comment#{@current_comment_uuid_index}",
      'body' => "this is comment #{@current_comment_uuid_index}",
      'public' => true,
      'authored_at' => 2.days.ago
    }.merge(options))

    ts_comment.author ||= new_ticket_sharing_actor
    ts_comment
  end

  def new_ticket_sharing_attachment(_options = {})
    TicketSharing::Attachment.new(
      'url' => 'http://example.com/foo.jpg',
      'filename' => 'foo.jpg',
      'content_type' => 'image/jpg'
    )
  end

  def valid_agreement(params = {})
    account = accounts(:minimum)
    admin   = users(:minimum_admin)
    remote  = users(:minimum_agent)
    params = {
      created_type: :remote,
      access_key: '<access_key>',
      remote_url: 'http://example.com/sharing',
      account: account,
      local_admin: admin,
      remote_admin: remote,
      direction: 'out'
    }.merge(params)

    if params[:created_type].to_sym == :remote
      params[:uuid] ||= Digest::SHA1.hexdigest(rand.to_s + Time.now.to_s)
    end

    agreement = Sharing::Agreement.new(params)
    assert agreement.valid?

    agreement
  end

  def create_valid_agreement(params = {})
    agreement = valid_agreement(params)
    agreement.save!
    agreement
  end
end
