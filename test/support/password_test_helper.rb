require 'zendesk/users/user_manager'

module PasswordTestHelper
  def change_password!(user, to)
    auth = Zendesk::AuthenticatedSession.new({}).tap do |a|
      a.current_user = user
      a.current_account = user.account
    end
    manager = Zendesk::UserManager.new(auth)
    manager.reset_password_for(user, to: to)
  end
end
