module SandboxTestHelper
  def create_sandbox
    click_button("Create my sandbox")
    assert_response :redirect
    follow_redirect!
    @account.reload.sandbox
  end
end
