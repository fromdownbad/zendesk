module ReplyParserTestHelper
  def describe_with_reply_parser(context_name, &block)
    describe("with reply parser") do
      before do
        content ||= Zendesk::MtcMpqMigration::Content.any_instance
        content.stubs(use_reply_parser: true)
        Account.any_instance.stubs(has_email_no_delimiter?: true)
        Account.any_instance.stubs(has_no_mail_delimiter_enabled?: true)
      end

      describe(context_name, &block)
    end
  end

  def describe_with_and_without_reply_parser(context_name, &block)
    [:with, :without].each do |w|
      describe("#{w} reply parser") do
        let(:use_reply_parser) { w == :with }

        before do
          content ||= Zendesk::MtcMpqMigration::Content.any_instance
          content.stubs(use_reply_parser: use_reply_parser)
          Account.any_instance.stubs(has_email_no_delimiter?: use_reply_parser)
          Account.any_instance.stubs(has_no_mail_delimiter_enabled?: use_reply_parser)
        end

        describe(context_name, &block)
      end
    end
  end
end
