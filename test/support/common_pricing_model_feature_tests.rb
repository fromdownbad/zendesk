require_relative "features_helpers"
require_relative "billing_test_helper"

module CommonPricingModelFeatureTests
  include FeaturesHelpers
  include BillingTestHelper

  def should_have_properly_configured_plans(enterprise_features, plus_features, regular_features, solo_features, global_features, unavailable_features)
    describe "enterprise plans" do
      before { @subscription.update_attributes! plan_type: SubscriptionPlanType.ExtraLarge }
      it "has enterprise features enabled" do
        (enterprise_features + plus_features + regular_features + solo_features + global_features).each do |feature|
          assert @subscription.send("has_#{feature}?"), "Feature: #{feature.inspect}"
        end
      end

      it "does not have unavailable_features" do
        unavailable_features.each do |feature|
          assert_feature false, feature
        end
      end
    end

    describe "for plus plans" do
      before { @subscription.update_attributes! plan_type: SubscriptionPlanType.Large }
      it "has plus features enabled" do
        (plus_features + regular_features + solo_features + global_features).each do |feature|
          assert_feature true, feature
        end
      end

      it "has enterprise features disabled" do
        enterprise_features.each do |feature|
          assert_feature false, feature
        end
      end

      it "does not have unavailable_features" do
        unavailable_features.each do |feature|
          assert_feature false, feature
        end
      end
    end

    describe "for regular plans" do
      before do
        @subscription.update_attributes! plan_type: SubscriptionPlanType.Medium
      end

      it "has regular features enabled" do
        (regular_features + solo_features + global_features).each do |feature|
          assert_feature true, feature
        end
      end

      it "has plus features disabled" do
        (enterprise_features + plus_features).each do |feature|
          assert_feature false, feature
        end
      end

      it "does not have unavailable_features" do
        unavailable_features.each do |feature|
          assert_feature false, feature
        end
      end
    end

    describe "for starter plans" do
      before do
        @subscription.update_attributes! plan_type: SubscriptionPlanType.Small
      end

      it "has starter features enabled" do
        (solo_features + global_features).each do |feature|
          assert_feature true, feature
        end
      end

      it "has non-starter features disabled" do
        (enterprise_features + plus_features + regular_features).each do |feature|
          assert_feature false, feature
        end
      end

      it "does not have unavailable_features" do
        unavailable_features.each do |feature|
          assert_feature false, feature
        end
      end
    end
  end

  def legacy_api_check
    it "has 700 rpm API limit (legacy)" do
      assert @subscription.has_api_limit_700_rpm_legacy?
    end

    it "has API" do
      assert @subscription.has_api?
    end
  end
end

class ActiveSupport::TestCase
  extend CommonPricingModelFeatureTests
end
