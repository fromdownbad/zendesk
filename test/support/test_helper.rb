test_directory_parent = File.expand_path('../..', __dir__)
raise "Tests must be run from the '#{test_directory_parent}' directory" if test_directory_parent != Dir.pwd

ENV["NO_SSL"] = "false"
require 'bundler/setup'

# we eager load on travis + preload the helper -> coverage is unreliable
require 'single_cov'
SingleCov.setup :minitest unless ENV["CI"]
SingleCov::APP_FOLDERS.concat Dir['app/*'].map { |d| d.split('/').last }

if ENV["CI"] && ENV["TEST_ENV_NUMBER"] == "2"
  ENV["RAILS_MEMCACHED_HOSTS"] = "127.0.0.1:11212"
end

require_relative "base_helper"
require_relative "authentication_test_helper"
require_relative "sharding_test_helper"
require 'api/presentation/test_helper'
require 'api/presentation/lint'
require_relative "api_v2_test_helper"
require_relative "shoulda_2_backports"
require_relative "webrat_setup"
require_relative "authentication_integration_test_helper"
require_relative "custom_fields_test_helper"
require_relative "arturo_test_helper"
require_relative "mail_test_helper"
require_relative "archive_helper"
require_relative "fake_esc_kafka_message"
require_relative "domain_events_helper"
require_relative "logins_test_helper"
require_relative "minitest_should"
require_relative "rails4_helper"
require_relative "voice_helper"
require "parallel_tests/test/runtime_logger" if ENV["RECORD_RUNTIME"]
require "subscription" # needed for Fixture load order for SubscriptionFeature
require_relative "mobile_sdk_rate_limit_helper"

Dir[File.expand_path('../support/*_support.rb', __dir__)].sort.each { |file| require file }

require 'sass'
require 'webmock/minitest'
require 'vcr'
require 'timecop'
require 'arturo/test_support'
require_relative "arturo_feature_helper"
require 'test_benchmark' if ENV['BENCHMARK'].to_s.match?(/^true|full$/i)
require 'schmobile'
require 'shoulda/matchers'
require 'shoulda/change_matchers'
if RAILS4
  require 'test_after_commit'
end
require 'nori'
require 'bourne'
require 'pry'
require 'byebug'
require 'awesome_print'
require 'minitest/around/spec'
require 'zendesk/configuration/test_support'
require 'will_paginate/array'

# RAILS5UPGRADE: remove this and the gem before going to rails 5
if RAILS4
  require 'rails/forward_compatible_controller_tests'
end

require 'factory_bot'
FactoryBot.find_definitions

require_relative 'fake_redis'
require_relative 'resque_test_status'
Resque::TestStatus.override_status!

# make the test output readable on travis for now
if ENV['CI']
  Zendesk::Configuration.class_eval do
    def self.deprecation_message
    end
  end
end

Shoulda::Matchers.configure do |config|
  config.integrate do |with|
    with.test_framework :minitest

    with.library :rails
  end
end

# when an exception is raised, show everything above the minitest runner
# since the rest is just framework noise
class << MiniTest
  def filter_backtrace(backtrace)
    if runner = backtrace.index { |line| line =~ /minitest\/.*\.rb.*in \`?run/ }
      backtrace = backtrace[0...runner]
    else
      warn "Unable to find minitest backtrace start"
    end

    backtrace.map { |l| l.sub("#{Rails.root}/", "") }
  end
end

module Abilities
end

# Use ActiveSupport::TestCase for everything that was not matched before
Minitest::Spec::DSL::TYPES[-1][-1] = ActiveSupport::TestCase

class ActionController::TestCase
  # RAILS5UPGRADE: remove for rails 5.1?
  register_spec_type self do |desc, *additional|
    name = desc.is_a?(Module) && desc.name || desc
    additional.include?(:controller) || name =~ /controller$/i
  end

  if RAILS5
    def scrub_env_with_rack_reload!(env)
      scrub_env_without_rack_reload!(env)
      env.values.each do |var|
        var.reload if var.is_a?(ActiveRecord::Base)
      end
      env
    end

    alias_method :scrub_env_without_rack_reload!, :scrub_env!
    alias_method :scrub_env!, :scrub_env_with_rack_reload!
  end
end

class ActionDispatch::IntegrationTest
  register_spec_type self do |desc, *additional|
    name = desc.is_a?(Module) && desc.name || desc
    additional.include?(:integration) || name =~ /integration$/i
  end

  # stolen from ActiveSupport::TestCase below
  def fixture_file_upload(path, mime_type = nil, binary = false)
    Rack::Test::UploadedFile.new("test/files/#{path}", mime_type, binary)
  end
end

class ActionView::TestCase
  register_spec_type self do |desc, *additional|
    name = desc.is_a?(Module) && desc.name || desc
    additional.include?(:helper) || name =~ /helper$/i
  end
end

if RAILS5
  raise "we probably don't want this for 5.1+" if RAILS51
  require 'rails-controller-testing'
  Rails::Controller::Testing.install
end

if RAILS5
  # For minitest/spec w/ the minitest-rails gem,
  # ActionController::TestCase is the default for rails 4, but in
  # rails 5 it is ActionDispatch::IntegrationTest... switching to this
  # is a burden we'd rather not deal with for now so we're patching in
  # some aliases directly.

  class ActionController::TestCase # :nodoc:
    include Minitest::Rails::Expectations::ActionDispatch
  end
end

ActiveSupport::TestCase.class_eval do
  extend ArturoTestHelper

  if RAILS4
    self.use_transactional_fixtures = true
  else
    self.use_transactional_tests = true
  end

  self.use_instantiated_fixtures = false

  set_fixture_class facebook_pages: Facebook::Page
  set_fixture_class channels_user_profiles: ::Channels::UserProfile
  set_fixture_class global_oauth_clients: Zendesk::OAuth::GlobalClient
  set_fixture_class oauth_clients: Zendesk::OAuth::Client
  set_fixture_class '../files/stat_rollup/hc_search_string_stats': StatRollup::HCSearchStringStat
  set_fixture_class '../files/stat_rollup/ticket_stats': StatRollup::Ticket

  set_fixture_class zuora_coupons: ZendeskBillingCore::Zuora::Coupon
  set_fixture_class zuora_coupon_redemptions: ZendeskBillingCore::Zuora::CouponRedemption

  set_fixture_class cf_fields: CustomField::Field
  set_fixture_class cf_values: CustomField::Value
  set_fixture_class cf_dropdown_choices: CustomField::DropdownChoice
  set_fixture_class role_settings: RoleSettings
  set_fixture_class channels_resources: Channels::Resource
  set_fixture_class incoming_channels_conversions: Channels::IncomingConversion
  set_fixture_class zopim_subscriptions: ZendeskBillingCore::Zopim::Subscription
  set_fixture_class zopim_agents: Zopim::Agent

  set_fixture_class experiment_participations: Experiment
  set_fixture_class device_identifiers: PushNotifications::DeviceIdentifier

  if ENV['FIXTURES_ALL'] || ENV['CI']
    fixtures :all
  else
    # The following fixtures are needed every time you check the validity of
    # an Account, which means they're needed on nearly every test.
    fixtures :accounts, :brands, :routes, :subscriptions, :account_property_sets, :users,
      :user_identities, :groups, :memberships, :addresses
  end

  def self.with_env(env, &block)
    # use a describe block here so the before/after below don't hit the whole suite
    describe 'with a custom environment' do
      before do
        @original_env = ENV.dup
        env.each { |k, v| ENV[k.to_s] = (v.nil? ? v : v.to_s) } # allowed to set nil or string, no numbers/booleans
      end

      class_eval(&block)

      after do
        Object.send(:remove_const, :ENV)
        Object.const_set(:ENV, @original_env)
      end
    end
  end

  def self.conditional_describe(string, switch, &block)
    if switch
      describe(string, &block)
    else
      warn("warning: not running tests in '#{string}' describe")
    end
  end

  def self.resque_inline(val = true)
    before do
      @old_resque_job_setting = Resque.inline
      Resque.inline = val
    end

    after do
      Resque.inline = @old_resque_job_setting
    end
  end

  # Revert global state overridden in a block, even when encountering an exception.
  # Example:
  #
  # ActiveRecord::Base.logger.level
  # => 0
  #
  # revert('ActiveRecord::Base.logger.level') do
  #   ActiveRecord::Base.logger.level = 4
  # end
  #
  # ActiveRecord::Base.logger.level
  # => 0
  #
  def revert(source)
    original_value = instance_eval(source) # rubocop:disable Lint/UselessAssignment
    yield
  ensure
    instance_eval("#{source} = original_value")
  end

  # Switches the request object's Accept header to a new type, one of :html, :js or :xml
  def accept(type)
    @request.accept = response_type_to_mime_type(type)
  end

  def _difference_between_hashes(expected, actual)
    begin
      diff = HashDiff.best_diff(expected, actual)
      diff.map! do |change, position, expected_value, actual_value = nil|
        expected_value = "#{expected_value.inspect} -> #{actual_value.inspect}" if actual_value
        " #{change} :#{position} => #{expected_value}"
      end
    rescue StandardError => e
      Rails.logger.error(e.message)
      diff = ['>>> NOT AVAILABLE <<<']
    end

    [
      'Expected hash was:', "  #{expected.inspect}",
      'Actual hash is:', "  #{actual.inspect}",
      'Difference:', *diff
    ].join("\n")
  end

  def assert_equal(expected, actual, message = nil)
    raise "Suspicious use of assert_equal(nil, *) detected.\nIf nil was intended, use assert_nil/must_be_nil/assert_equal_with_nil" if expected.nil?
    super
  rescue MiniTest::Assertion => e
    if expected.is_a?(Hash) && actual.is_a?(Hash) && message.nil?
      message = _difference_between_hashes(expected, actual)
      raise Minitest::Assertion, message, e.backtrace
    else
      raise e
    end
  end

  def assert_equal_with_nil(expected, actual, *args)
    if expected.nil?
      assert_nil actual, *args
    else
      assert_equal expected, actual, *args
    end
  end

  def assert_number_fails_validation(subject, attr, number)
    assert_raise ActiveRecord::RecordInvalid do
      subject.update_attributes! attr => number
    end
  end

  # Mocha does type-sensitive comparisons, which can be inconvenient
  # for IDs when AR doesn't care
  def as_id(id)
    any_of(id.to_s, id.to_i)
  end

  # Convert a response type symbol to a MIME type string using the Mime module.
  def response_type_to_mime_type(s)
    Mime[s]
  end

  def assert_responded_with(s)
    assert_equal response_type_to_mime_type(s).to_s, @response.content_type
  end

  def self.should_be_unauthorized_when_not_logged_in(actions, *args)
    unrestful_should_be_unauthorized_when_not_logged_in(make_actions_restful(actions), *args)
  end

  def self.make_actions_restful(actions)
    options = actions.extract_options!
    actions = actions.map do |a|
      case a
      when :create then [:post, a, {}]
      when :edit then [:get, a, {id: 1}]
      when :update then [:put, a, {id: 1}]
      when :destroy then [:delete, a, {id: 1}]
      when :show then [:get, a, {id: 1}]
      when Array then a
      else
        [:get, a, {}]
      end
    end
    actions.map! { |a, b, c| [a, b, c.merge(options[:add_to_all])] } if options[:add_to_all]
    actions
  end

  def self.unrestful_should_be_unauthorized_when_not_logged_in(actions, user_key = nil, return_value = 401)
    describe "unauthorized_user" do
      actions.each do |action|
        method, action, params = if action.is_a?(Array)
          action
        else
          [:get, action, {}]
        end

        user_str = user_key ? "if logged in with #{user_key}" : "if not logged in"
        it "responds with a #{return_value}  #{user_str} for action #{action}" do
          login(user_key) if user_key
          send method, action, params: params
          assert_response return_value, "#{action} did not respond with a #{return_value}"
        end
      end
    end
  end

  def do_routing(path_root, options = {})
    with_options controller: options.fetch(:controller, path_root) do |test|
      test.assert_routing path_root, action: 'index'
      test.assert_routing "#{path_root}/1", action: 'show', id: '1'
      test.assert_routing "#{path_root}/1/edit", action: 'edit', id: '1'
    end
  end

  def xml_document
    @xml_document ||= Nokogiri::XML::Document.parse(@response.body)
  end

  def assert_xml_select(*args, &block)
    @html_document = xml_document
    assert_select(*args, &block)
  end

  def assert_emails_sent(count = 1)
    ActionMailer::Base.perform_deliveries = true
    num_deliveries = ActionMailer::Base.deliveries.size
    yield
    assert_equal num_deliveries + count, ActionMailer::Base.deliveries.size,
      "Expected #{count} email(s) to be sent but only #{ActionMailer::Base.deliveries.size} was."
  end

  # returns attachment, token if you want easy access to them in your test.
  def add_attachment(token = nil, file_path = '../../Capfile')
    stub_request(:put, %r{\.amazonaws.com/data/attachments/\d+/Capfile})
    filedata = fixture_file_upload(file_path)

    account = @request.account
    token =
      case token
      when Token then token
      when nil then account.upload_tokens.create(source: account)
      else
        account.upload_tokens.find_by_value(token)
      end

    attachment = token.add_attachment(filedata)
    attachment.save!
    [attachment, token]
  end

  def destroy_attachment(id, token)
    controller_bak = @controller
    @controller = UploadsController.new
    delete :destroy, params: { format: 'js', token: token, id: id }
    @controller = controller_bak
  end

  def delete_and_reload_ticket(ticket)
    CIA.audit(actor: users(:minimum_admin)) do
      ticket.will_be_saved_by(users(:minimum_admin))
      ticket.soft_delete!
    end
    ticket.reload
    ticket
  end

  def build_valid_permission_set(account = nil, name = 'Super agent', permissions = {})
    permission_set = PermissionSet.new(name: name, account: account)
    permission_set.permissions.set(
      macro_access: permissions[:macro_access] || 'full',
      ticket_access: permissions[:ticket_access] || 'all',
      comment_access: permissions[:comment_access] || 'public',
      view_access: permissions[:view_access] || 'full',
      user_view_access: permissions[:user_view_access] || 'full',
      report_access: permissions[:report_access] || 'full',
      forum_access: permissions[:forum_access] || 'full',
      forum_access_restricted_content: permissions[:forum_access_restricted_content] || true
    )
    permission_set
  end

  def valid_signup_parameters
    {
      owner: {name: 'Ricky Gervais', email: 'ricky@aghassipour.com'},
      account: {
        name: 'My Company name', time_zone: 'Copenhagen', uses_12_hour_clock: '1',
        subdomain: 'jabadadoo', help_desk_size: "Small team"
      },
      address: {name: 'My Company name', phone: '123', country: 'Denmark'}
    }
  end

  def valid_user_signup_parameters
    {
      user: {
        name: 'Ricky Gervais',
        email: 'ricky@aghassipour.com',
        password: 'juletrae88'
      }
    }
  end

  def valid_sla_parameters
    {
      sla: {
        assigned_when: "1",
        priority_operator: "is",
        priority_id: "1",
        ticket_type_id: "0",
        percentage: "17",
        solved_when: "2",
        organization_id: ''
      }
    }
  end

  def use_ssl
    @request.env["HTTPS"] = "on"
  end

  def assert_select_in(html, selector, equality = nil, _message = nil, &block)
    stubs(:document_root_element).returns(Nokogiri::HTML::Document.parse(html))
    assert_select(selector, equality, &block)
  rescue ArgumentError => e
    if /assertion message must be String or Proc/.match?(e.message)
      raise Test::Unit::AssertionFailedError, "Expected #{selector.inspect} to match, but it didn't"
    else
      raise
    end
  end

  def assert_same_elements(a, b)
    assert_equal a.to_set, b.to_set
  end

  def assert_document_equality(expected, actual)
    assert_array_equality(expected.root.children.map(&:name).sort, actual.root.children.map(&:name).sort)

    expected.root.children.each do |expected_child|
      actual_child = actual.xpath("//#{expected.root.name}/#{expected_child.name}").first
      assert actual_child, "No match for node //#{expected.root.name}/#{expected_child.name}"
      assert_equal expected_child.name, actual_child.name
    end

    actual.root.children.each do |actual_child|
      expected_child = expected.xpath("//#{actual.root.name}/#{actual_child.name}").first
      assert_equal expected_child.name, actual_child.name
    end
  end

  def assert_array_equality(a, b)
    a.each_index do |i|
      assert_equal a[i], b[i], "Failed at position #{i} comparing #{a[i]} and #{b[i]}"
    end

    b.each_index do |i|
      assert_equal b[i], a[i], "Failed at position #{i} reverse comparing #{b[i]} and #{a[i]}"
    end
  end

  def read_test_file(filename)
    self.class.read_test_file(filename)
  end

  def read_test_file_no_new_lines(filename)
    read_test_file(filename).gsub(/(\r\n)+|(\n)+/m, " ")
  end

  def self.read_test_file(filename)
    File.read("#{Rails.root}/test/files/#{filename}")
  end

  # this code is similarily used Fixtures below, maybe there's a
  # place to put it to be reused?
  def without_foreign_key_checks
    ActiveRecord::Base.connection.execute "SET FOREIGN_KEY_CHECKS = 0"
    yield
  ensure
    ActiveRecord::Base.connection.execute "SET FOREIGN_KEY_CHECKS = 1"
  end

  def with_rollback(record)
    class << record.class
      def transaction_with_rollback(*args)
        transaction_without_rollback(*args) do
          yield
          raise ActiveRecord::Rollback
        end
      end
      alias_method :transaction_without_rollback, :transaction
      alias_method :transaction, :transaction_with_rollback
    end

    yield

    record.class.instance_eval { alias :transaction :transaction_without_rollback }
  end

  def set_header(name, value)
    header_name = name
    header_name = "HTTP_#{header_name.upcase.tr('-', '_')}" unless header_name.match?(/^HTTP_/)
    @request.env[header_name] = value
  end

  def set_body(body) # rubocop:disable Naming/AccessorMethodName
    @request.env['RAW_POST_DATA'] = body.to_s
  end

  # Useful for parsing multiple response types in a single block
  # and returning a unified object (Hash)
  #
  #   %w[xml json].each do |format|
  #     describe "#{format} request" do
  #       before do
  #         get :index, :format => format
  #       end
  #       it "has some content" do
  #         data = parse_response(@response.body, format)
  #         assert_equal "value", data["attribute"]
  #       end
  #     end
  #   end
  #
  # returns a plain old Ruby hash for either XML or JSON
  def parse_response(body, format = :json)
    case format.to_sym
    when :xml
      parser = Nori.new(parser: :nokogiri)
      hash = parser.parse(body)
      hash[hash.keys[0]] # API collections have a top-level node named after the collection
    when :json
      JSON.parse(body)
    end
  end

  def create_audit_for(object, options = {})
    if options[:old] # TODO: maybe removable...
      object.audits.create!(account: object.account, name: "foo")
    else
      CIA::Event.create!(account: object.account, source: object, action: "update", actor: users(:systemuser))
    end
  end

  def expects_chain(object, *method_names)
    chain_sequence = sequence("expects_chain_#{object_id}")
    my_mock = mock("#{object} chain sequence #{object_id}")
    result = nil
    method_names.each_with_index do |method_name, index|
      chainer =
        case index
        when 0
          object
        else
          my_mock
        end

      result =
        case method_name
        when Symbol
          chainer.expects(method_name).in_sequence(chain_sequence)
        when Hash
          temp_method_name = method_name.keys.first
          extras = method_name[temp_method_name]
          chain = chainer.expects(temp_method_name)
          extras.each do |key, value|
            chain = chain.send(key, *value)
          end
          chain.in_sequence(chain_sequence)
        end

      if index != method_names.length - 1
        result = result.returns(my_mock)
      end
    end
    result
  end

  def fixture_file_upload(path, mime_type = nil, binary = false)
    Rack::Test::UploadedFile.new("test/files/#{path}", mime_type, binary)
  end

  def assert_valid(record)
    assert record.valid?, "Expected to be valid but had errors: #{record.errors.full_messages.join(", ")}"
  end

  def refute_valid(record)
    refute record.valid?, "Expected to be invalid, but was not."
  end

  def with_fake_translation(key, value)
    backend = I18n.backend.backends.last
    old_translations = backend.instance_variable_get(:@translations)
    new_translations = old_translations.deep_merge(i18n_key_to_hash(key, value))
    backend.instance_variable_set(:@translations, new_translations)
    yield
  ensure
    backend.instance_variable_set(:@translations, old_translations)
  end

  # a.b.c, V -> {a: {b: {c: V}}}
  def i18n_key_to_hash(key, value)
    key.split('.').map!(&:to_sym).reverse!.reduce(value) { |h, k| {k => h} }
  end

  undef :assert_nothing_raised

  def write_file(path, data)
    FileUtils.mkdir_p(File.dirname(path))
    File.write(path, data)
  end

  def delete_file(path)
    File.delete(path) if File.exist?(path)
  end

  def verify_account_setting(account, setting, value)
    actual = account.settings.send(setting)
    assert actual == value, "Expected account setting #{setting} to be #{value}, but was #{actual}"
  end

  def assert_account_setting(account, setting)
    verify_account_setting(account, setting, true)
  end

  def refute_account_setting(account, setting)
    verify_account_setting(account, setting, false)
  end

  def self.remove_object_instance_variable(klass, var)
    remove = -> { klass.remove_instance_variable(var) if klass.instance_variable_defined?(var) }
    before &remove
    after &remove
  end

  def stub_for_statsd
    stub(
      'Fake StatsD client',
      increment: true,
      histogram: true,
      time: true,
      count: true,
      gauge: true,
      timing: true,
      sli_histogram_decremental: true
    ).tap do |statsd|
      statsd.stubs(:batch).yields(statsd)
    end
  end

  def set_field_value(object, field)
    field_type = object.class.columns_hash[field.to_s].type

    val = case field_type
          when :string
            "a_cute_cat"
          when :text
            "an_adorable_dog"
          when :integer
            22
          when :datetime
            Time.now
          when :boolean
            true
          else
            raise "set_field_value does not recognize #{field_type}"
    end

    object.update_column(field, val)
  end
end

# This overrides `inspect` to speed up instances where it is called on an
# ActiveRecord::Fixture. An example of this occurring is a `NameError` in the
# context of a test block. To construct the message for the error `inspect` is
# called which recursively inspects all instance variables that are set. An
# instance variable `loaded_fixtures` contains all the loaded fixtures and takes
# magnitudes of seconds to complete.
#
# This can be avoided by not using `fixtures :all` (or a large subset)
# however it is always used in CI so this seemed like a relatively targeted way
# to address the issue.
class ActiveRecord::FixtureSet
  def inspect
    to_s
  end
end

# Make all tests https by default
ActionController::TestCase.class_eval do
  before { https! }
end

ActionDispatch::IntegrationTest.class_eval do
  before { https! }
end

# copy https setting to new session
ActionDispatch::IntegrationTest.prepend(Module.new do
  def open_session
    super do |sess|
      sess.https! if https?
      yield sess
    end
  end
end)

ActiveSupport::TestCase.class_eval do
  setup :cleanup_environment

  # cleanup before fixtures are loaded since previous tests could have polluted the environment
  # and for example timezones are messed up (set to Copenhagen during fixture load)
  # testrbl test/functional/api/v1/settings_controller_test.rb test/functional/api/v1/stats_controller_test.rb
  #
  # this is not ideal since it runs before every describe block
  # but better than running before every fixture load (60x times)
  def load_fixtures_with_cleanup_environment(*args)
    cleanup_environment
    load_fixtures_without_cleanup_environment(*args)
  end
  alias_method :load_fixtures_without_cleanup_environment, :load_fixtures
  alias_method :load_fixtures, :load_fixtures_with_cleanup_environment

  def cleanup_environment
    self.class.cleanup_environment
  end

  def self.cleanup_environment
    Resque.redis = FakeRedis::Redis.new
    Zendesk::RedisStore.redis_client = FakeRedis::Redis.new

    Resque.jobs.clear
    FakeRedis::Redis.new.flushall # uses a shared memory database
    Rails.cache.clear
    I18n.locale = ENGLISH_BY_ZENDESK
    Time.zone = Rails.configuration.time_zone
    RemoteFiles::MemoryStore.clear!
    Timecop.return
    # Tests should still pass if time is frozen.  This simulates running
    # the tests on a very fast machine (where instructions run much faster than
    # the clock.)
    # Timecop.freeze
    ActionMailer::Base.perform_deliveries = false
    ActionMailer::Base.deliveries = []

    # This ensures that no tests accidently leave VCR on
    VCR.turn_off!
  end

  def mock_zopim_api_notification_request
    stub_request(:post, %r{/api/v2/internal/change_notifications/staff}).to_return(status: 201, body: '', headers: {})
  end

  setup :mock_zopim_api_notification_request

  def stub_apps_feature_changes
    stub_request(:post, %r{/api/v2/apps/feature_changes}).to_return(status: 201, body: "", headers: {})
  end

  setup :stub_apps_feature_changes

  def stub_guide_sandbox_notifications
    stub_request(:post, %r{/hc/api/internal/guide_sandbox}).to_return(status: 201, body: "", headers: {})
  end

  setup :stub_guide_sandbox_notifications

  def stub_user_avatar_request
    stub_request(:get, %r{/images/types/sample_user.jpg})
    stub_request(:get, %r{/profile_images/\d+/angry_unicorn.png})
  end

  setup :stub_user_avatar_request

  def stub_pravda_products_fetch
    stub_request(:get, %r{/api/services/accounts/[0-9]+/products}).
      to_return(
        status:  200,
        body:    { products: [] }.to_json,
        headers: { content_type: 'application/json; charset=utf-8' }
      )
  end

  setup :stub_pravda_products_fetch

  def stub_guide_trial_creation_request
    stub_request(:get, %r{/api/services/accounts/[0-9]+/products/support}).to_return(status: 404)
    stub_request(:get, %r{/api/services/accounts/[0-9]+/products/guide}).to_return(status: 404)
    stub_request(:post, %r{/api/services/accounts/[0-9]+/products}).to_return(status: 200)
  end

  setup :stub_guide_trial_creation_request

  def stub_pravda_explore_product_fetch_missing
    stub_request(:get, %r{/api/services/accounts/[0-9]+/products/explore}).to_return(status: 404)
  end

  setup :stub_pravda_explore_product_fetch_missing

  def stub_pravda_support_product_update
    stub_request(:patch, %r{/api/services/accounts/[0-9]+/products/support\?.+}).to_return(status: 200)
  end

  setup :stub_pravda_support_product_update

  def stub_pravda_support_product_sync
    Account.any_instance.stubs(:instrument_product_sync_validation)
    Subscription.any_instance.stubs(:instrument_product_sync_validation)
  end

  setup :stub_pravda_support_product_sync

  require 'zendesk/staff_client'
  def stub_staff_service_entitlement_sync
    Zendesk::StaffClient.any_instance.stubs(:update_entitlements!)
    Zendesk::StaffClient.any_instance.stubs(:update_full_entitlements!)
  end

  setup :stub_staff_service_entitlement_sync

  def stub_pravda_guide_product_update
    stub_request(:patch, %r{/api/services/accounts/[0-9]+/products/guide\?.+}).to_return(status: 200)
  end

  setup :stub_pravda_guide_product_update

  def stub_create_ses_account_configuration
    stub_request(:post, %r{hermes-configuration-service-web.zendesk-staging.com/api/v1/account_configurations}).to_return(status: 201, body: {message: 'OK'}.to_json, headers: {'Content-Type' => 'application/json'})
  end

  setup :stub_create_ses_account_configuration

  def stub_update_ses_account_configuration
    stub_request(:patch, %r{hermes-configuration-service-web.zendesk-staging.com/api/v1/account_configurations}).to_return(status: 200, body: {message: 'OK'}.to_json, headers: {'Content-Type' => 'application/json'})
    stub_request(:patch, %r{hermes-configuration-service-web.zendesk-staging.com/api/v1/scheduled_account_configurations}).to_return(status: 202, body: {message: 'OK'}.to_json, headers: {'Content-Type' => 'application/json'})
  end

  setup :stub_update_ses_account_configuration

  def stub_delete_ses_account_configuration
    stub_request(:delete, %r{hermes-configuration-service-web.zendesk-staging.com/api/v1/account_configurations}).to_return(status: 200, body: {message: 'OK'}.to_json, headers: {'Content-Type' => 'application/json'})
    stub_request(:delete, %r{hermes-configuration-service-web.zendesk-staging.com/api/v1/scheduled_account_configurations}).to_return(status: 202, body: {message: 'OK'}.to_json, headers: {'Content-Type' => 'application/json'})
  end

  setup :stub_delete_ses_account_configuration

  def stub_show_ses_account_configuration
    stub_request(:get, %r{hermes-configuration-service-web.zendesk-staging.com/api/v1/account_configurations}).to_return(status: 200, body: {message: 'OK'}.to_json, headers: {'Content-Type' => 'application/json'})
  end

  setup :stub_show_ses_account_configuration

  def stub_create_ses_identity_configuration
    stub_request(:post, %r{hermes-configuration-service-web.zendesk-staging.com/api/v1/identity_configurations}).to_return(status: 201, body: {message: 'OK'}.to_json, headers: {'Content-Type' => 'application/json'})
  end

  setup :stub_create_ses_identity_configuration

  def stub_show_ses_identity_configuration
    identities = [{"us-east-1" => {"verification_status" => "Success"}}, {"us-west-2" => {"verification_status" => "Success"}}]
    stub_request(:get, %r{hermes-configuration-service-web.zendesk-staging.com/api/v1/identity_configurations}).to_return(status: 200, body: {message: 'OK', identity: identities}.to_json, headers: {'Content-Type' => 'application/json'})
  end

  setup :stub_show_ses_identity_configuration

  def stub_staff_service_request
    response_body = { entitlements: { 'support' => nil, 'chat' => nil } }.to_json
    stub_request(:get, %r{http://metropolis:[0-9]+/api/services/staff/[0-9]+/entitlements}).
      to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
  end

  setup :stub_staff_service_request

  def stub_revoke_google_oauth_token_request
    stub_request(:get, %r{https://accounts.google.com/o/oauth2/revoke}).
      to_return(status: 200, body: {message: 'OK'}.to_json, headers: {'Content-Type' => 'application/json'})
  end

  setup :stub_revoke_google_oauth_token_request

  def stub_role_sync_job
    Omnichannel::RoleSyncJob.stubs(:enqueue)
  end

  setup :stub_role_sync_job

  def stub_challenge_token_creation
    stub_request(:post, %r{/api/v2/internal/challenge_token.json}).to_return(
      status: 200,
      body: { challenge: nil }.to_json,
      headers: {'Content-Type' => 'application/json'}
    )
  end

  setup :stub_challenge_token_creation

  def stub_pravda_get
    stub_request(:get, "http://pravda:8888/api/services/accounts/90538").to_return(status: 200, body: "", headers: {})
  end

  setup :stub_pravda_get

  def stub_ledger
    config = Zendesk::Configuration.fetch(:ledger)
    external_url = "#{config.dig('endpoints', 'base_url')}/api/v1/events"
    stub_request(:post, external_url).to_return(status: 200, body: "", headers: {})
  end

  setup :stub_ledger

  ## JSON Logging per-test
  def subscribe_collator
    unless Rails.logger.respond_to?(:collator) && Rails.logger.subscribers.include?(Rails.logger.collator)
      Rails.logger.subscribe(Rails.logger.collator)
    end
  end

  setup :subscribe_collator

  # If you want to configure everything from the environment and support consul lookups,
  # uncomment this block
  #
  # require_relative "consul_lookup_support"
  # include ConsulLookupSupport
  # setup :stub_sharddb_consul_lookup
  # setup :stub_datacenters_consul_lookup

  def flush_collator
    return unless Rails.logger.respond_to?(:collator)
    Rails.logger.collator.flush
  end

  teardown :flush_collator

  def self.should_include_module(some_module)
    it "includes #{some_module.name}" do
      @controller.class.included_modules.must_include some_module
    end
  end

  def follow_all_redirects!(options = {})
    while redirect? && (!options[:except] || headers["Location"].to_s !~ options[:except])
      visit(headers["Location"])
    end
  end

  # Test if an expression is blank. Passes if object.blank? is true.
  #
  #   assert_blank [] # => true
  def assert_blank(object, message = nil)
    message ||= "#{object.inspect} is not blank"
    assert object.blank?, message
  end
  alias_method :refute_present, :assert_blank

  # Test if an expression is not blank. Passes if object.present? is true.
  #
  #   assert_present {:data => 'x' } # => true
  def assert_present(object, message = nil)
    message ||= "#{object.inspect} is blank"
    assert object.present?, message
  end
  alias_method :refute_blank, :assert_present
end

module HackHttpKeywordArgs
  raise "remove me!" if RAILS51

  def process_with_kwargs(http_method, action, *args)
    if kwarg_request?(args)
      args.first[:method] = http_method if args.first.is_a?(Hash)
    elsif args.empty?
      args << { method: http_method }
    else
      non_kwarg_request_warning if args.any?
      args = args.unshift(http_method)
    end
    process(action, *args)
  end
end

ActionController::TestCase.class_eval do
  include HackHttpKeywordArgs

  # our files are in test/files since everything in test/fixtures is loaded since rails 3
  def fixture_file_upload(path, mime_type = nil, binary = false)
    Rack::Test::UploadedFile.new("test/files/#{path}", mime_type, binary)
  end

  def process_with_memoized_clear(*args)
    process_without_memoized_clear(*args).tap do
      @controller.instance_variable_set(:@return_to_parser, nil)
    end
  end
  unless method_defined?(:process_without_memoized_clear)
    alias_method :process_without_memoized_clear, :process
    alias_method :process, :process_with_memoized_clear
  end

  def self.should_not_have_extra_actions(controller)
    describe "actions" do
      it "does not have any actions" do
        assert_equal [], controller.action_methods.to_a.reject { |a| a =~ /^(_conditional_callback_around_|_callback_before_)/ } - ["flash"]
      end
    end
  end

  def refute_layout
    assert_layout nil
  end

  def assert_layout(expected_layout)
    layouts = @_layouts
    assert(
      (expected_layout.nil? && layouts.keys.empty?) ||
      layouts.key?(expected_layout)
    )
  end

  def assert_etagged(action)
    get action unless @controller.response

    etag = @response.headers['ETag']
    assert etag

    # same response without changes ?
    get action
    assert_equal etag, @response.headers['ETag']

    # different response with changes ?
    yield
    get action
    refute_equal etag, @response.headers['ETag']
  end

  def https!(enabled = true)
    if enabled
      request.env['HTTPS'] = 'on'
    else
      request.env.delete 'HTTPS'
    end
  end
end

module ControllerDefaultParams
  def self.prepended(base)
    def base.use_test_route!
      before do
        @default_parameters ||= {}
        @default_parameters[:test_route] = :route
      end
    end

    # Specify test relevant routes by name
    def base.use_test_routes
      use_test_route!
      controller_name = controller_class.controller_name
      controller_path = controller_class.controller_path
      names = controller_class.action_methods.to_a - %w[warden_scope?]
      Rails.application.routes.prepend do
        names.each do |name|
          match "test/:test_route/#{controller_name}/#{name}",
            action: name,
            controller: controller_path,
            via: [:get, :post],
            as: "#{name}_#{controller_path.underscore}"
        end
      end
      Rails.application.reload_routes!
    end

    # For setting up integration tests
    def base.integrate_test_routes(klass)
      cattr_accessor :controller_class do
        klass
      end
      use_test_routes
    end
  end

  if RAILS4
    def xhr(method, action, *args)
      params = args[0] && args[0].is_a?(Hash) && args[0][:params]
      args[0] = params if params

      super
    end

    def process(*args, &block)
      has_bad_args = args[2] && !args[2].is_a?(Hash)
      raise ArgumentError, "bad HTTP verb syntax: %p" % [args] if has_bad_args
      args[2] = (args[2] || {}).merge(@default_parameters) if @default_parameters
      super
    end
  else
    def process(action, *args)
      # this has already passed through HackHttpKeywordArgs
      raise ArgumentError, "bad HTTP verb syntax 1: %p" % [args] unless kwarg_request?(args)

      if @default_parameters
        args[0][:params] ||= {}
        args[0][:params].merge! @default_parameters
      end

      super
    end

  end
end
ActionController::TestCase.prepend ControllerDefaultParams

ActiveRecord::FixtureSet.class_eval do
  class << self
    def create_fixtures_with_translations(*args)
      result = Arsi.disable { create_fixtures_without_translations(*args) }

      # refresh cached nil's when fixtures where not loaded -- rake db:test:prepare && ruby test/unit/translation_locale/translation_locale_test.rb
      ENGLISH_BY_ZENDESK.parent ||= TranslationLocale.find_by_id(ENGLISH_BY_ZENDESK.parent_id)

      result
    end
    alias_method :create_fixtures_without_translations, :create_fixtures
    alias_method :create_fixtures, :create_fixtures_with_translations
  end

  # Sparse ids generated by foxy fixtures cause unrealistic scaling problems
  # http://dev.rubyonrails.org/ticket/11339
  def self.identify(label, column_type = :integer)
    if column_type == :uuid
      super
    else
      Zlib.crc32(label.to_s) % 100000 + 1
    end
  end

  def self.account_fixtures
    [
      "system", "support", "minimum", "with_trial_zopim_subscription", "with_paid_zopim_subscription",
      "with_paid_voice_and_zopim_subscription", "with_unprovisioned_zopim_subscription", "with_groups", "with_photo",
      "payment", "reports", "billing", "serialization", "browser", "trial", "inactive", "inbox",
      "mixpanel", "minimum_sdk", "extra_large", "multiproduct", "with_ticket_form", "minimum_jwt_saml",
      "shell_account_without_support", "minimum_answer_bot", "reply_to"
    ]
  end

  def self.identify_account(label)
    raise unless account_fixtures.include?(label.to_s)
    {system: -1, support: 1}[label.to_sym] || identify(label)
  end

  # even though we explicitly pass the class things get messed up with ::File:: namespace -> fix it
  # test/functional/api/v1/stats_controller_test.rb
  # --> DELETE FROM ``.``.`_files_stat_rollup_forum_stats`
  def initialize_with_folder_fixtures(connection, table_name, class_name, options)
    if table_name.to_s.starts_with?(".._files_")
      class_name = ActiveSupport::TestCase.fixture_class_names[table_name.sub(".._files_stat_rollup_", "../files/stat_rollup/").to_sym]
      table_name = class_name.table_name
    end
    initialize_without_folder_fixtures(connection, table_name, class_name, options)
  end
  alias_method :initialize_without_folder_fixtures, :initialize
  alias_method :initialize, :initialize_with_folder_fixtures
end

# This is necessary in order to ensure that the fixtures in test/fixtures/cf_fields.yml
# are inserted with a valid "type" attribute. Otherwise they are inserted with the
# CustomField:: namespace and the CustomField::Field#validate_type fails on those
# records. This causes test failures anytime at test attempts to update those records
# For example: test/unit/custom_fields/custom_field_manager_test.rb#reorder
ActiveRecord::ConnectionAdapters::DatabaseStatements.class_eval do
  def insert_fixture_with_custom_field_type(fixture, table)
    fixture["type"].gsub!("CustomField::", "") if table == "cf_fields"
    insert_fixture_without_custom_field_type(fixture, table)
  end
  alias_method :insert_fixture_without_custom_field_type, :insert_fixture
  alias_method :insert_fixture, :insert_fixture_with_custom_field_type
end

# do not load every class when using fixtures :all -> speedup
ActiveRecord::TestFixtures::ClassMethods.class_eval do
  def require_fixture_classes(*args)
  end
end

ActionController::TestRequest.class_eval do
  if RAILS5
    def recycle!
      # TODO: remove fix this hack to avoid MonitorMixin double-initialize error:
      @mon_mutex_owner_object_id = nil
      @mon_mutex = nil
      initialize
    end
  end

  if RAILS4
    def self.create
      new
    end
  end

  include Schmobile::Request

  def account=(account)
    self.host = account.try(:host_name)
    @env['zendesk.account'] = account
  end

  def account
    @env['zendesk.account']
  end

  # RAILS5UPGRADE: Rails 5 stops using recycle! so that can no longer
  # be used as the hook to reload the entities stored in env.
  # This whole thing is pretty reliant on internal implementation so
  # may need to be revisited in future versions of Rails.
  if RAILS4
    def recycle_with_rack_reload!
      recycle_without_rack_reload!
      @env.values.each do |var|
        var.reload if var.is_a?(ActiveRecord::Base)
      end
    end

    unless method_defined?(:recycle_without_rack_reload!)
      alias_method :recycle_without_rack_reload!, :recycle!
      alias_method :recycle!, :recycle_with_rack_reload!
    end
  end

  if RAILS4
    include Zendesk::Extensions::RackRequestInternalClient
    alias_method :assign_parameters_without_override, :assign_parameters
    def assign_parameters(routes, controller_path, action, parameters = {})
      controller_path = parameters.delete(:override_controller) || controller_path
      assign_parameters_without_override(routes, controller_path, action, parameters)
    end
  end
end

require 'mail'
Mail::Message.class_eval do
  def joined_bodies
    if parts.empty?
      body.to_s
    else
      parts.map(&:body).join("\n")
    end
  end
end

ActionDispatch::Integration::Session.class_eval do
  if RAILS4
    # Stolen from https://github.com/jfirebaugh/openstreetmap-website/blob/master/test/integration/cors_test.rb
    def options(path, parameters = nil, headers = nil)
      process :options, path, parameters, headers
    end
  else
    def options(path, *args)
      process_with_kwargs :options, path, *args
    end
  end
end

# Avoid logging when running tests. Set the ZENDESK_LOG_LEVEL envvar if you need to change
# the value.
log_level = ENV['ZENDESK_LOG_LEVEL']
Rails.logger.level = Integer(log_level) if log_level.present?

if RAILS4
  ActionController::TestResponse.class_eval do
    alias_method :redirected_to, :location
  end
end
ActionDispatch::TestResponse.class_eval do
  alias_method :redirected_to, :location
end

ActionView::TestCase.include(ERB::Util)
ActionView::TestCase.send(:include, Rails.application.routes.url_helpers)

# methods added by url_helpers start with test -> make them private or they add 10 extra empty test-cases
ActionView::TestCase.class_eval do
  test_like_methods = Rails.application.routes.url_helpers.public_methods.select { |x| x.to_s.starts_with?("test") }
  private *test_like_methods
end

# show backtraces for liquid errors or they are untraceable
Liquid::Context.class_eval do
  def handle_error(e, *_args)
    raise e
  end
end

module AWS
  class << self
    def unstub!
      config(stub_requests: false)
    end
  end
end

module I18n
  mattr_accessor :previous_request_locale
end

Time.class_eval do
  cattr_accessor :previous_request_zone
end

# Rails.logger uses method_missing which mocha cannot stub/expect on
Logcast::Broadcaster.class_eval do
  def expects(*args)
    subscribers.first.expects(*args)
  end

  def stubs(*args)
    subscribers.first.stubs(*args)
  end
end

VCR.configure do |config|
  config.cassette_library_dir = "test/files/vcr_cassettes"
  config.hook_into :webmock
end
VCR.turn_off! # Turning VCR off by default allows it to play nice with webmock

# Get value of a particular custom field for a user
def custom_field_values_for(key, user)
  CustomField::Value.joins(:field).where(
    cf_fields: { owner: "User", key: key, account_id: user.account.id },
    cf_values: { owner_id: user.id, account_id: user.account.id }
  )
end

def stub_occam_find(ticket_data)
  ticket_ids = Array(ticket_data).map do |value|
    case value
    when Integer then value
    when Ticket then value.id
    end
  end.compact

  Zendesk::Rules::OccamClient.
    any_instance.
    stubs(:find).
    returns('ids' => ticket_ids)
end

def stub_occam_count(value)
  Zendesk::Rules::OccamClient.
    any_instance.
    stubs(:count).
    returns('count' => value)
end

ActiveRecordStats.unsubscribe
ActionViewStats.unsubscribe

def stub_const(klass, const, value)
  old = klass.const_get(const)
  klass.send(:remove_const, const)
  klass.const_set(const, value)
  yield
ensure
  klass.send(:remove_const, const)
  klass.const_set(const, old)
end
