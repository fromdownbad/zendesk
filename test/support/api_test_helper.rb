module ApiTestHelper
  def api_basic_auth_headers(login, password, options = {})
    type = (options[:type] || Mime[:json]).to_s

    {
      'HTTP_ACCEPT' => type,
      'HTTP_AUTHORIZATION' => basic_auth_code(login, password),
      'CONTENT_TYPE' => type,
      # Basic auth is turned off except for specific exceptions (eg mobile)
      'HTTP_USER_AGENT' => 'Zendesk for mobile'
    }
  end

  def basic_auth_code(login, password)
    "Basic " + Base64.encode64("#{login}:#{password}")
  end

  def assert_api_raise(exception, error_message)
    e = assert_raise exception do
      yield
    end
    assert_equal error_message, Api::V2::ErrorsPresenter.new.present(e.record)
  end

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def should_set_cache_headers
      it 'sets ETag and last-modified headers' do
        assert @response.headers['ETag'].present?, 'Expected an ETag header, but none was found.'
        assert @response.headers['Last-Modified'].present?, 'Expected a Last-Modified header, but none was found.'
      end
    end

    def should_set_version_header(version)
      it "sets X-Zendesk-API-Version to #{version}" do
        assert_equal @response.headers["X-Zendesk-API-Version"], version
      end
    end
  end
end
