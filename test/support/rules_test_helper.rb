module RulesTestHelper
  def get_valid_rule_parameters(type = :trigger)
    type = type.to_sym
    res =
      { sets:           { '1' => { }},
        rule: {title: 'Trigger test', owner_type: "Account", type: type.to_s.capitalize}}

    res[:sets]['1'][:conditions] = {"0" => {"operator" => "is", "value" => {"0" => "0"}, "source" => "status_id"}} unless type == :macro
    res[:sets]['1'][:actions]    = {"0" => {"operator" => "is", "value" => {"0" => "1"}, "source" => "status_id"}} unless type == :view
    res
  end

  def build_rule(attributes)
    account = attributes.delete(:account) || Account.find_by_subdomain("minimum")
    type = attributes.delete(:type) || "View"
    assoc = type.downcase.pluralize
    title = attributes.delete(:title) || "test rule #{rand}"
    description = attributes.delete(:description) || "test rule #{rand}"
    owner = attributes.delete(:owner) || account

    rule = account.send(assoc).build(
      owner:       owner,
      title:       title,
      description: description
    )

    d = Definition.new

    attributes.each do |key, value|
      item =
        case key
        when :status
          DefinitionItem.new('status_id', 'is', [StatusType.find!(value)])
        when :ticket_type
          DefinitionItem.new('ticket_type_id', 'is', [TicketType.find!(value)])
        when :assignee
          DefinitionItem.new('assignee', 'is', [value])
        when :comment_is_public
          DefinitionItem.new('comment_is_public', 'is', [value])
        when :conditions_all, :conditions_any, :actions
          value = [value] unless value.first.is_a?(Array)
          value.each do |arr|
            d.send(key).push(DefinitionItem.new(*arr))
          end
          nil
        end
      d.conditions_all.push(item) if item
    end

    rule.definition = d
    if rule.is_a?(View)
      rule.include_archived_tickets = rule.is_a?(View)
      rule.output = Output.create(columns: View::COLUMNS)
    end
    rule
  end

  def new_rule(condition, options = {})
    definition = Definition.new
    definition.send(options[:type] || :actions).push(condition)
    Rule.new(definition: definition)
  end
end
