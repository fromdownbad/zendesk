module TestSupport::SimplifiedEmailThreading
  module Helper
    private

    def setup_base_i18n_assertions
      I18n.stubs(:t).with(regexp_matches(/txt.admin.views.rules/))
      I18n.stubs(:t).with(regexp_matches(/type.status/))
      I18n.stubs(:t).with(regexp_matches(/type.priority/))
      I18n.stubs(:t).with(regexp_matches(/activemodel.errors.messages.invalid/), anything)
      I18n.stubs(:t).with(regexp_matches(/txt.admin.models.rules/), anything)
      I18n.stubs(:t).with(regexp_matches(/txt.admin.helpers/), anything)
    end

    def set_admin_locales
      @account_admin_locales = []

      account.admins.take(locales.length).each_with_index do |admin, index|
        @account_admin_locales.push(admin.locale_id)
        admin.locale_id = locales[index].id
        admin.save!
      end
    end

    def reset_admin_locales
      account.admins.take(locales.length).each_with_index do |admin, index|
        admin.locale_id = @account_admin_locales[index]
        admin.save!
      end
    end

    def string_suffix_from_translation_locale(translation_locale)
      case translation_locale.id
      when 1 then 'english'
      when 4 then 'japanese'
      when 6 then 'spanish'
      else ''
      end
    end
  end
end
