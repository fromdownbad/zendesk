module VoiceCommentTestHelper
  def it_includes_answered_by
    it "includes Answered by" do
      assert_match(/Answered by: #{users(:minimum_agent).clean_name}/, @voice_comment.body)
    end
  end

  def it_does_not_include_answered_by
    it "does not include Answered by" do
      assert_no_match(/Answered by: #{users(:minimum_agent).clean_name}/, @voice_comment.body)
    end
  end

  def it_includes_called_by
    it "includes Called by" do
      assert_match(/Called by: #{users(:minimum_agent).clean_name}/, @voice_comment.body)
    end
  end

  def it_does_not_include_called_by
    it "does not include Called by" do
      assert_no_match(/Called by: #{users(:minimum_agent).clean_name}/, @voice_comment.body)
    end
  end

  def it_does_not_include_answered_by_when_name_is_nil
    describe "when answered by name is nil" do
      before { @voice_comment.data[:answered_by_name] = nil }

      it_does_not_include_answered_by
    end
  end

  def it_does_not_include_called_by_when_name_is_nil
    describe "when answered by name is nil" do
      before { @voice_comment.data[:answered_by_name] = nil }

      it_does_not_include_called_by
    end
  end
end
