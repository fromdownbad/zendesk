# The absolute minimum all tests need
ENV["RAILS_ENV"] = "test"

require_relative "../../config/environment"

# Disable tracer in tests by default
Datadog.tracer.configure(enabled: false)
Datadog.configure { |c| c.tracer.enabled = false }

require "minitest/autorun"
require "minitest/rg"
require "minitest/rerun"
require "rails/test_help"
require "minitest/rails"
require "mocha/setup"

ActiveRecord::Base.default_shard = 1
