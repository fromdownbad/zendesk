# An alternative to EscKafkaMessage to allow events to be buffered in memory
# instead of written to the (unqueryable) MySQL BLACKHOLE Escape table.
#
# Note these are instance methods for ease of resetting, so you should
# dependency inject `FakeEscKafkaMessage.new` to replace `EscKafkaMessage`
class FakeEscKafkaMessage
  attr_accessor :events

  def initialize
    @events = []
  end

  def create!(event)
    @events << event
  end

  # Bulk INSERT method provided by https://github.com/zdennis/activerecord-import
  # In Rails 6 this can be migrated to the new native insert_all method
  def bulk_import!(events, _opts = {})
    @events += events
  end

  def clear
    @events = []
  end
end
