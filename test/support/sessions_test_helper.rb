module SessionsTestHelper
  def session_decryptor
    @session_decryptor ||= Zendesk::SharedSession::CookieValue.new(Zendesk::SharedSession::SecretManager.new('minimum.zendesk-test.com'))
  end

  def decode_cookie(cookie)
    session_decryptor.load(cookie)
  end
end
