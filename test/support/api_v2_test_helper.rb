module Api::V2::TestHelper
  def self.extended(base)
    # make available to test namespace collisions with ::Account
    ::Api::V2::Account # rubocop:disable Lint/Void

    base.extend(Api::Presentation::TestHelper)
    base.send(:include, InstanceMethods)
  end

  def should_paginate(collection)
    before_should "paginate #{collection}" do
      @controller.expects(:pagination).returns(per_page: 77, page: 5)
      collection = @controller.send(collection)

      assert_equal 77, collection.per_page,     "Ignored per page setting"
      assert_equal 5,  collection.current_page, "Ignored page"
    end
  end

  def should_support_cursor_pagination(action, method: :get, with_custom_index: false, extra_params: {})
    it "should support cursor pagination with cursor" do
      send method, action, params: extra_params.merge(cursor: Base64.urlsafe_encode64('1||1|'))
      assert_response :ok
      json_keys = JSON.parse(response.body).keys
      err = "Cursor pagination does not seem to be enabled on that endpoint"
      assert_equal 2, (json_keys & ['after_url', 'before_url']).size, err
    end

    it "should support cursor pagination with count" do
      send method, action, params: extra_params.merge(limit: 1)
      assert_response :ok
      json_keys = JSON.parse(response.body).keys
      err = "Cursor pagination does not seem to be enabled on that endpoint"
      assert_equal 2, (json_keys & ['after_url', 'before_url']).size, err
    end

    if with_custom_index
      it "should support use_index as param" do
        e = assert_raises ActiveRecord::StatementInvalid do
          send method, action, params: extra_params.merge(limit: 1, use_index: 'foo')
        end
        assert !!(e.message =~ /Mysql2::Error: Key 'foo' doesn't exist in table 'satisfaction_ratings'/)
      end
    end
  end

  def should_support_cursor_pagination_conversion(action, response_object, cursor_arturo, offset_removal_arturo, method: :get, extra_params: {}, cursor_pagination_version: :v2, table_name: nil, sortable_fields: nil)
    it 'should use cursor pagination when provided the right param' do
      if cursor_pagination_version == :v2
        send method, action, params: extra_params.merge(page: { size: 100 })
      else
        send method, action, params: extra_params.merge(limit: 1000)
      end

      assert_response :ok
      json_response = JSON.parse(response.body)
      assert json_response[response_object].size >= 1

      if cursor_pagination_version == :v2
        assert_not_nil json_response['links']
        assert_not_nil json_response['links']['prev']
        assert_not_nil json_response['links']['next']
      else
        assert json_response['after_url'].include? "limit=100"
        assert json_response['after_url'].include? "order=asc"
      end
    end

    if cursor_pagination_version == :v2
      describe 'default sort parameter of id' do
        it 'generates the correct order clause' do
          if sortable_fields
            sortable_fields.keys.each do |sort_column|
              assert_sql_queries(1, /ORDER BY `#{table_name || response_object}`.`#{sort_column}` DESC/) do
                send method, action, params: extra_params.merge(page: { size: 100 }, sort: "-#{sort_column}")
              end

              assert_sql_queries(1, /ORDER BY `#{table_name || response_object}`.`#{sort_column}` ASC/) do
                send method, action, params: extra_params.merge(page: { size: 100 }, sort: sort_column)
              end
            end
          else
            assert_sql_queries(1, /ORDER BY `#{table_name || response_object}`.`id` DESC/) do
              send method, action, params: extra_params.merge(page: { size: 100 }, sort: '-id')
            end

            assert_sql_queries(1, /ORDER BY `#{table_name || response_object}`.`id` ASC/) do
              send method, action, params: extra_params.merge(page: { size: 100 }, sort: 'id')
            end
          end
        end

        it 'shows an error message for invalid sort options' do
          send method, action, params: extra_params.merge(page: { size: 100 }, sort: '-invalid')
          assert_response :bad_request
          assert @response.body =~ /sort is not valid/
        end
      end
    end

    describe_with_arturo_enabled cursor_arturo do
      it 'should default to cursor pagination when no params are present' do
        send method, action, params: extra_params

        assert_response :ok
        json_response = JSON.parse(response.body)
        assert json_response[response_object].size >= 1

        if cursor_pagination_version == :v2
          assert_not_nil json_response['links']['prev']
          assert_not_nil json_response['links']['next']
        else
          assert json_response['after_url'].include? "limit=100"
          assert json_response['after_url'].include? "order=asc"
        end
      end

      it 'should use offset pagination when offset pagination params are present' do
        send method, action, params: extra_params.merge(per_page: 100)
        assert_response :ok
        json_response = JSON.parse(response.body)
        assert json_response[response_object].size >= 1
        assert_equal 2, (json_response.keys & ['next_page', 'previous_page']).size
      end

      if cursor_pagination_version == :v2
        it 'should use cursor pagination v2 when the page parameter is present and is a Hash' do
          send method, action, params: extra_params.merge(page: { size: 100 })
          assert_response :ok
          json_response = JSON.parse(response.body)
          assert json_response[response_object].size >= 1
          assert_not_nil json_response['links']['prev']
          assert_not_nil json_response['links']['next']
        end
      end
    end

    describe_with_arturo_enabled offset_removal_arturo do
      it 'should use cursor pagination' do
        if cursor_pagination_version == :v2
          send method, action, params: extra_params.merge(page: { size: 100 })
        else
          send method, action, params: extra_params
        end

        assert_response :ok
        json_response = JSON.parse(response.body)
        assert json_response[response_object].size >= 1

        if cursor_pagination_version == :v2
          assert_not_nil json_response['links']['prev']
          assert_not_nil json_response['links']['next']
        else
          assert json_response['after_url'].include? "limit=100"
          assert json_response['after_url'].include? "order=asc"
        end
      end

      it 'should use cursor pagination when offset pagination params are present' do
        send method, action, params: extra_params.merge(per_page: 100)

        assert_response :ok
        json_response = JSON.parse(response.body)
        assert json_response[response_object].size >= 1

        if cursor_pagination_version == :v2
          assert_not_nil json_response['links']['prev']
          assert_not_nil json_response['links']['next']
        else
          assert json_response['after_url'].include? "cursor"
        end
      end
    end
  end

  module InstanceMethods
    def normalize_volatile_data(data)
      fake_id = 12345
      fake_date = "11-11-11T11:11:11Z"

      case data
      when Hash
        Hash[data.map do |k, v|
          value =
            case k
            when "url"
              v.sub(/\d+\.json/, "#{fake_id}.json").sub(":80/", "/")
            when "external_id"
              normalize_volatile_data(v)
            when /(\A|_)id\Z/
              v ? fake_id : nil
            else
              normalize_volatile_data(v)
            end

          [k, value]
        end]
      when /\A\d+-\d+-\d+T\d+:\d+:\d+Z\Z/ then fake_date
      when Array then data.map { |d| normalize_volatile_data(d) }
      else data
      end
    end

    def assert_no_api_change(file)
      file = Rails.root.join(file)
      generated = normalize_volatile_data(JSON.load(@response.body))
      File.write(file, generated.to_yaml.gsub(/ +$/, "").strip + "\n") unless File.exist?(file)
      assert_equal YAML.load_file(file), generated
    end

    def json
      JSON.parse(response.body)
    end

    def stub_account_service_sync
      stub_request(:patch, /[^.]+\.zendesk-test\.com\/api\/services\/accounts\/\d+/)
    end

    def stub_account_service_get
      stub_request(:get, /[^.]+\.zendesk-test\.com\/api\/services\/accounts\/\d+/)
    end
  end
end
