class ActiveSupport::TestCase
  def create_attachment(filename, user = nil)
    stub_request(:put, %r{\.amazonaws.com/data/.*})
    attachment = Attachment.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new(filename))
    attachment.author   = user || Account.last.users.last
    attachment.account  = attachment.author.account
    attachment.save!
    attachment
  end

  def create_expirable_attachment(filename, account, options = {})
    stub_request(:put, %r{\.amazonaws.com/data/expirable_attachments/.*})
    klass = options[:klass] || ExpirableAttachment
    attachment = klass.new(uploaded_data: Zendesk::Attachments::CgiFileWrapper.new(filename))
    attachment.account     = account
    attachment.author      = account.users.last
    attachment.created_via = options[:created_via] || "XmlExportJob"
    attachment.save!
    attachment
  end

  def assert_valid_attachment_domain_url(url)
    url = URI.parse(response.location)
    assert_equal 'p1.zdusercontent.com', url.host

    assert jwe = Rack::Utils.parse_query(url.query)["token"]
    token = Zendesk::Attachments::Token.new(token: jwe)
    assert_not_nil token.attachment_id
  end
end
