require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

module GooddataTestHelper
  def self.included(klass)
    klass.before do
      @account = accounts(:minimum)
      @admin   = users(:minimum_admin)

      gooddata_client = FactoryBot.create(:global_client, user: @admin, account: @account)
      gooddata_client.update_columns(identifier: 'gooddata')
      @gooddata_token = FactoryBot.create(:token, global_client: gooddata_client, user: nil, scopes: 'gooddata')
    end
  end

  def get_as_gooddata(path, options = {})
    Account.any_instance.stubs(has_gooddata_oauth?: true)

    ip = options[:ip]
    date = Time.now.rfc822
    host = @account.host_name(ssl: true, mapped: false)

    get "https://#{host}#{path}", params: {}, headers: {
      'REMOTE_ADDR' => ip,
      'HTTP_DATE' => date,
      'HTTP_AUTHORIZATION' => "Bearer #{@gooddata_token.token(:full)}"
    }

    tested_body = response.body

    key = Zendesk::SystemUser.subsystem_key_for("gooddata", "signed")
    signature = Zendesk::SystemUser::Authentication.signature(key, "GET", host, path, date)

    header = "gooddata:#{signature}"
    header_key = 'HTTP_' + Zendesk::SystemUser::Authentication::CLIENT_SYSTEM_API_HEADER.tr('-', '_').upcase
    ip = options[:ip]

    get "https://#{host}#{path}", params: {},
      headers: { 'REMOTE_ADDR' => ip, header_key => header, 'HTTP_DATE' => date }

    assert_equal response.body, tested_body
  end
end
