ActionDispatch::IntegrationTest.class_eval do
  def warden
    @request.env['warden']
  end

  def current_user
    warden.user || warden.user(scope: :api)
  end

  def shared_session
    @controller.shared_session
  end

  def logged_in?
    current_user.present?
  end

  def login(user, options = {})
    # if we've just requested something that required authentication, then follow that flow
    if @controller && redirect? && headers["Location"] =~ /\/access\/unauthenticated/
      visit(headers["Location"]) while redirect?
    end

    # if we are still not on the login page, then take us there

    login_url = options[:login_url] || user.account.url(mapped: false, protocol: 'https')
    login_path = options[:login_path] || "/auth/v2/login/signin"
    full_url = "#{login_url}#{login_path}"

    visit full_url

    fill_in('user[email]', with: user.email)
    fill_in('user[password]', with: options[:password] || '123456')
    check('remember_me') if options[:remember_me]
    submit_form(session[Schmobile::IS_MOBILE] ? 'password-form' : 'login-form')
    visit(headers["Location"]) while redirect?

    user == warden.user
  end

  def login_to_host_mapped(user, url, options = {})
    login(user, options)
    challenge = user.find_or_create_challenge_token(request.remote_ip, request.shared_session).value

    visit "#{url}/auth/v2/login/signed_in?challenge=#{challenge}"
  end

  def logout
    visit "/access/logout"
    follow_all_redirects!
  end
end

class ActionController::TestCase
  setup :init_warden

  def shared_session
    @controller.shared_session
  end

  def login(user)
    user = users(user) unless user.is_a?(User)

    @request.account = user.account unless @request.account
    @controller.send(:current_user=, user)

    return user if user.is_system_user?

    # mimic warden
    if @controller.respond_to?(:authentication, true)
      Zendesk::AuthenticatedSession.create(
        session: shared_session,
        account: user.account,
        user: user,
        store: true,
        via: 'password'
      )
    end

    user
  end

  def logout
    @controller.send(:current_user=, nil)
    if @controller.respond_to?(:authentication, true)
      @controller.send(:authentication).destroy
      @controller.instance_variable_set(:@authentication, nil)
    end
  end

  def self.as_an_anonymous_user(&blk)
    describe("when not logged in") do
      before do
        @account = accounts(:minimum)
        @request.account = @account
      end

      describe(', ', &blk)
    end
  end

  def self.when_logged_in_as(user_identifier, &blk)
    describe("when logged in as #{user_identifier}") do
      before do
        @user    = users(user_identifier)
        @account = @user.account
        @request.account = @account
        login(@user)
      end

      describe(', ', &blk)
    end
  end

  def self.when_logged_in_as_chat_agent(user_identifier, &blk)
    describe("when logged in as chat_agent") do
      before do
        user    = users(user_identifier)
        account = user.account
        @request.account = account
        ZopimIntegration.create!(
          account: account,
          external_zopim_id: 1,
          zopim_key: 'abc'
        )

        account.create_zopim_subscription!.tap do |record|
          record.zopim_account_id = account.id
          record.save!
          record.update_column(:syncstate, ZendeskBillingCore::Zopim::SyncState::Ready)
        end
        user.make_chat_agent!
        user.zopim_identity.update_column(:zopim_agent_id, 123)
        user.zopim_identity.update_column(:syncstate, ZendeskBillingCore::Zopim::SyncState::Ready)
        login(user)
      end

      describe(', ', &blk)
    end
  end

  # Internal API requests uses sub system users
  def self.as_a_subsystem_user(options, &blk)
    describe("when logged in as a sub system user") do
      before do
        @account         = accounts(options.fetch(:account))
        @request.account = @account
        system_user      = Zendesk::SystemUser.find_subsystem_user_by_name(options.fetch(:user, "zendesk"))
        assert system_user, "No subsystem user found, bad zendesk.yml?"

        # The WardenStrategy in the zendesk_system_users gem sets the current account to the user account for subsystems
        system_user.account = @account

        @controller.stubs(:current_registered_user).returns(system_user)
      end

      describe(", ", &blk)
    end
  end

  def self.as_a_system_user(&blk)
    describe "as a system user" do
      before do
        @user      = User.find(-1)
        @account ||= accounts(:systemaccount)
        @request.account = @account

        def warden.authenticate(*)
          set_user(User.find(-1))
        end
      end

      describe(", ", &blk)
    end
  end

  def self.as_an_owner(&blk)
    when_logged_in_as(:minimum_admin, &blk)
  end

  def self.as_an_admin(&blk)
    when_logged_in_as(:minimum_admin_not_owner, &blk)
  end

  def self.as_an_agent(&blk)
    when_logged_in_as(:minimum_agent, &blk)
  end

  def self.as_an_agent_with_permissions(permissions, &blk)
    describe "as an agent with permissions like #{permissions}" do
      before do
        @user    = users(:minimum_agent)
        @account = @user.account

        @request.account = @account

        login(@user)

        @account.stubs(has_permission_sets?: true)

        @user.permission_set = @account.permission_sets.create!(name: 'test')

        permissions.each do |permission, value|
          @user.permission_set.permissions.send("#{permission}=", value)
        end
      end

      describe(', ', &blk)
    end
  end

  def self.as_a_chat_agent(&blk)
    when_logged_in_as_chat_agent(:minimum_end_user, &blk)
  end

  def self.as_an_end_user(&blk)
    when_logged_in_as(:minimum_end_user, &blk)
  end

  # Authenticates the user using the fixture in the +user_type+ parameter using
  # the SDK anonymous strategy
  def self.as_an_sdk_anonymous_end_user(user_fixture = :minimum_sdk_end_user, &blk)
    fixtures :mobile_sdk_auths, :oauth_clients
    describe("when logged in via sdk ") do
      before do
        # Common SDK authentication preamble
        set_required_sdk_initial_state!(user_fixture, "sdk")

        # When we log in as a anonnymous SDK user we create an UserSdkIdentity
        # we are replicating the process here.
        @user_sdk_identity = user_identities(:mobile_sdk_identity)
        @user_sdk_identity.save!
        @user.identities << @user_sdk_identity
        @user.save!

        set_header "HTTP_MOBILE_SDK_IDENTITY", @user_sdk_identity.value

        login(@user)
      end

      describe("using Anonymous authentication as '#{user_fixture}' user ", &blk)
    end
  end

  # Authenticates the user using the fixture in the +user_type+ parameter using
  # the SDK JWT strategy
  def self.as_an_sdk_jwt_end_user(user_fixture = :minimum_sdk_end_user, &blk)
    fixtures :mobile_sdk_auths, :oauth_clients
    describe("when logged in via sdk ") do
      before do
        # Common SDK authentication preamble
        set_required_sdk_initial_state!(user_fixture, "read write")

        login(@user)
      end

      describe("using JWT authentication as '#{user_fixture}' user ", &blk)
    end
  end

  def self.should_respond_with_status_on_actions(status, *actions, &block)
    check_authorized_actions(*actions, &block)

    make_actions_restful(actions).each do |action|
      describe "a request to #{action}" do
        before do
          action = [:delete, :destroy, {}] if action == :destroy # support old usage

          method, action, params = if action.is_a?(Array)
            action
          else
            [:get, action, {}]
          end

          send(method, action, params: params || {})
        end

        it("responds with #{status}") { assert_response status }
      end
    end
  end

  def self.check_authorized_actions(*actions)
    if actions.length == 1 && actions.first.is_a?(Hash) || block_given?
      raise(
        ArgumentError,
        "\nInvalid action argument(s). Expected one or more of the following:\n " \
        "\t- a symbol representing a rails resource. eg :create\n" \
        "\t- a custom action name. eg :custom_action. This will default to a GET with no params\n" \
        "\t- an Array that has a http verb, custom action name, and params. eg [:post, :custom_action, {}]\n" \
        "An optional final argument that represents params that are shared amongst all actions"
      )
    end
  end

  def self.should_be_forbidden(*actions, &block)
    should_respond_with_status_on_actions(:forbidden, *actions, &block)
  end

  def self.should_be_unauthorized(*actions, &block)
    should_respond_with_status_on_actions(:unauthorized, *actions, &block)
  end

  def warden
    @warden ||= WardenStub.new
  end

  private

  def init_warden
    @request.env['warden'] = warden
  end

  class WardenStub
    Config = Struct.new(:default_scope)
    attr_accessor :message, :session, :winning_strategy

    def initialize
      @session = {}
    end

    def authenticate(*)
    end

    def authenticate!(*)
    end

    def authenticated?(*)
      @user.present?
    end

    def custom_failure!
    end

    def logout(*)
      @user = nil
    end

    def user(*)
      @user
    end

    def set_user(user, *)
      @user = user
    end

    def flash
      @flash ||= {}
    end

    def config
      @config ||= Config.new(:default)
    end
  end

  def set_required_sdk_initial_state!(user_identifier, scopes)
    require "zendesk/testing/factories/global_uid"
    require "zendesk/o_auth/testing/client_factory"
    require "zendesk/o_auth/testing/token_factory"

    @user = users(user_identifier.to_sym)
    @account = @user.account
    @account.default_brand = brands(:minimum_sdk)
    @request.account = @account
    @request.user_agent = "Zendesk SDK for iOS"
    @controller.stubs(:allow_only_mobile_sdk_clients_access).returns(nil)
    @controller.stubs(:allow_only_mobile_sdk_tokens_access).returns(nil)
    @mobile_sdk_auth = mobile_sdk_auths(:minimum_sdk)
    @mobile_sdk_app = @mobile_sdk_auth.mobile_sdk_app
    token = FactoryBot.create(:token, client: @mobile_sdk_auth.client, user: @user, scopes: scopes.to_s)
    @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = token
  end
end
