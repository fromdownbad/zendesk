module ApiActivityTestHelper
  def hour_cache_key(account_id, client_id, hour)
    "api-activity/v5/#{account_id}/#{client_id}/count/#{hour}"
  end

  def peak_count_key(account_id, hour)
    "api-activity/v5/#{account_id}/peak_count/#{hour}"
  end

  def populate_memcached(account, current_time)
    account_key = "api-activity/v5/#{account.id}/keys"
    # account key => [md5_key, current_time.to_i, display_name, client_type, client_id]
    client_keys = {'user-1' => ['12345', current_time.to_i - 1.hour, 'user-1@zendesk.com', 'basic', 'user-1@zendesk.com'],
                   'user-2' => ['67890', current_time.to_i - 2.hour, 'user-2@zendesk.com', 'basic', 'user-2@zendesk.com'],
                   'user-3' => ['abcde', current_time.to_i - 3.hour, 'user-3@zendesk.com', 'basic', 'user-3@zendesk.com']}
    current_hour = current_time.to_i / 1.hour

    # write account client key
    Rails.cache.write(account_key, client_keys, expires_in: 7.days)

    # write peak count key for current and previous two hours to ensure we only write previous hour only
    Rails.cache.write(peak_count_key(account.id, current_hour), 404, expires_in: 24.hours)
    Rails.cache.write(peak_count_key(account.id, current_hour - 1), 303, expires_in: 24.hours)
    Rails.cache.write(peak_count_key(account.id, current_hour - 2), 202, expires_in: 24.hours)

    # write hour keys with each client key starting at 100, 200, 300 respectively
    # each will have a peak count of 303
    start_count = 100
    Rails.cache.write(account_key, client_keys, expires_in: 7.days)
    client_keys.each_pair do |_key, client_profile|
      count = start_count
      start_count += 100
      3.times do |i|
        hour_key = hour_cache_key(account.id, client_profile.first, current_hour - i)
        Rails.cache.write(hour_key, count, expires_in: 24.hours)
        count += 100
      end
    end
    ['12345', '67890', 'abcde']
  end

  def create_client_data(current_time, number_of_hours = 10 * 24, api_count = 1000, peak_count = 400)
    {}.tap do |blob|
      current_hour = current_time.to_i / 1.hour - 1
      number_of_hours.times do |i|
        current_day = Time.at((current_hour - i) * 1.hour).day
        # the count for each hour of the day is equal to 1000+day_of_month
        # this ensures the code counts over the entire day without going into the previous or next day
        blob[current_hour - i] = {count: api_count + current_day, peak_count: peak_count}
      end
    end.to_json
  end

  def populate_client_data(account, current_time, request_time)
    account.api_activity_clients.each do |client|
      client.data = create_client_data(current_time)
      client.last_request_time = request_time
      client.save
    end
  end

  def add_client_data(account, current_time, last_request_time)
    client = ApiActivityClient.new do |new_client|
      new_client.account_id = account.id
      new_client.client_key = "123456"
      new_client.display_name = "foobar"
      new_client.client_type = "basic"
      new_client.last_request_time = last_request_time
      new_client.data = create_client_data(current_time)
    end
    client.save
  end
end
