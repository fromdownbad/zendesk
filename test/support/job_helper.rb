require_relative 'test_helper'
require_relative 'shared_job_contexts'

# Perform jobs _and their resque hooks_ (copied from Resque's own test_helper.rb)
module PerformJob
  def perform_job(klass, *args)
    resque_job = Resque::Job.new(:testqueue, 'class' => klass, 'args' => args)
    resque_job.perform
  end
end

# NB: this will affect all direct subclasses of ActiveSupport::TestCase
# as well as subclasses of those classes
class << ActiveSupport::TestCase
  def inherited_with_shared_job_contexts(klass)
    inherited_without_shared_job_contexts(klass)

    begin
      if klass.described_type.singleton_class.include?(ZendeskJob::Resque::BaseJob)
        klass.send(:include, SharedJobContexts)
      end
    rescue NameError, LoadError
      # NameError: klass.described_type raises when it can't find the class
      # LoadError: triggered in #{__FILE__}: it could be that `described_type` for #{klass.name} is wrong.
      nil
    end
  end
  alias_method :inherited_without_shared_job_contexts, :inherited
  alias_method :inherited, :inherited_with_shared_job_contexts
end
