module CollaborationTestHelper
  def build_collaborations_with_collaborator_type(ticket, users, collaborator_type)
    users.each do |user|
      Collaboration.create(ticket: ticket, user: user, collaborator_type: collaborator_type)
    end
  end

  def setup_collaborations(ticket, agent, include_min_agent_follower = true)
    followers = [users(:minimum_admin)]
    followers << agent if include_min_agent_follower
    build_collaborations_with_collaborator_type(ticket, followers, CollaboratorType.FOLLOWER)
    build_collaborations_with_collaborator_type(ticket, [users(:minimum_admin), users(:minimum_end_user)], CollaboratorType.EMAIL_CC)
    build_collaborations_with_collaborator_type(ticket, [users(:minimum_admin_not_owner), users(:minimum_search_user)], CollaboratorType.LEGACY_CC)
    ticket.will_be_saved_by(users(:minimum_agent))
  end
end
