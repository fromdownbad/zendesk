module CollectionPresenterHelper
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    def should_present_collection_keys(*keys)
      describe 'presenter payload' do
        let(:payload) { @presenter.present(@collection)[@model_key].first }

        before do
          unless @presenter && @collection && @model_key
            fail(
              'You need to set up a @presenter, @collection, & @model_key in' \
                'your test to use `should_present_collection_keys`'
            )
          end
        end

        it "presents #{keys.join(', ')}" do
          assert_equal (payload.keys & keys).sort_by(&:to_s), keys.sort_by(&:to_s)
        end
      end
    end
  end
end
