require 'zendesk_archive'
require 'zendesk_archive/storage/in_memory'

# Setup a global instance for tests that don't opt into a clean room
ZendeskArchive.router = ZendeskArchive::Storage::Router.new(
  [
    ZendeskArchive::Storage::InMemory.new(key_version: 7, primary: true)
  ]
)

# Default-on usage of ZendeskArchive.router
require 'arturo/test_support'

def with_in_memory_archive_router(test)
  old_router = ZendeskArchive.router
  ZendeskArchive.router = ZendeskArchive::Storage::Router.new(
    [
      ZendeskArchive::Storage::InMemory.new(key_version: 7, primary: true)
    ]
  )
  test.call
ensure
  ZendeskArchive.router = old_router
end

def archive_and_delete(*tickets)
  tickets.each(&:archive!)
end
