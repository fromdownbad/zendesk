# list features here that should always be on in the test suite.  ie most features should
# live here!

ON_FEATURES = %i[
  dc_in_macro_titles
  macro_render_dc_markdown
  occam_count_many
  only_set_shared_session_when_changed
  persist_sessions_with_renew_header
  simple_format_dc_in_rich_comments
  skill_based_attribute_ticket_mapping
  skill_based_ticket_routing
  skip_locale_bulk_update_job
  sunset_macro_actions_endpoint
  user_identity_limit
  auto_suspend_via_afs
  enable_captcha_via_afs
  free_mail_via_afs
  mark_risk_assessment_as_safe_age_via_afs
  mark_risk_assessment_as_safe_paid_via_afs
  outbound_email_risky_via_afs
  outbound_email_safe_via_afs
  restrict_unauthenticated_user
  mark_abusive_via_afs
  mark_not_abusive_via_afs
  support_risky_via_afs
  takeover_via_afs
  talk_risky_via_afs
  unsuspend_via_afs
  whitelist_via_afs
  fix_group_filter_for_agent_search
  account_fraud_service_enabled
  downgrade_support_trial_specific_support_features
  use_trigger_req_received_body_v4
  multiple_group_views_reads
  multiple_group_views_writes
  doorman_header_for_account_logging
  update_account_associations_for_security_settings_api
  polaris
  sell_only_user_redirect
  bulk_insert_or_update_custom_field_options
  new_multiproduct_redirect
  no_lotus_redirection_on_multiproduct
  reset_passwords_before_20161101
  restrict_identity_verification_email
  no_session_renewal_in_all_lotus_requests
  deflection_mailer_header
  publish_assumption_event_to_kafka
  downcased_recipients_in_rule_conditions
  sla_side_conversation_reply_time
  generate_password_reset_token_subclass
  last_login_readonly_check
].freeze

# Feature that have :arturo => true can get turned off here
OFF_FEATURES = %i[
  agent_as_end_user
  rule_matching_extraction
  throttle_user_create_or_update
].freeze

ON_FEATURES.each do |f|
  Arturo.enable_feature!(f)
end

OFF_FEATURES.each do |f|
  Arturo.disable_feature!(f)
end
