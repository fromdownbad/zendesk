require "nokogumbo"
require "htmlentities"
require "zendesk/inbound_mail"

module MailTestHelper
  def create_content_id_map(account, attachments)
    attachments.each_with_object({}) do |attachment, id_map|
      content_type = attachment.content_type
      attachment_model = account.attachments.build(
        account: account,
        author: account.admins.first,
        content_type: content_type,
        filename: attachment.file_name
      )
      attachment_model.inline = !!attachment.headers['inline'].try(:first)
      content_id = attachment.headers['content-id'].try(:first)
      id_map[content_id] = attachment_model
    end
  end

  def create_attachment(options = {})
    Zendesk::Mail::Part.new.tap do |attachment|
      attachment.file_name = options['file_name'] || "test.txt"
      attachment.body = options['body'] || "body"
      attachment.content_type = options['content_type'] || "text/plain"
      attachment.remote_url = options['remote_url'] if options['remote_url']
      attachment.headers['content-id'] = options['content_id'] if options['content_id']
      attachment.headers['inline'] = options['inline'] if options['inline']
    end
  end

  def nest_html_content(html_content, nesting)
    "#{'<div>' * nesting}#{html_content}#{'</div>' * nesting}"
  end

  def address(email)
    Zendesk::Mail::Address.new(address: email)
  end

  def process_mail(mail, options = {})
    account = options[:account]
    mail.account = account
    recipient = options[:recipient] || account.try(:reply_address)
    eml_fixture = options[:eml_fixture]

    store_mail_in_remote_files(mail, eml_fixture: eml_fixture)

    Zendesk::InboundMail::TicketProcessingJob.work(
      mail: mail,
      account_id: account ? account.id : nil,
      recipient: recipient,
      route_id: options[:route_id]
    )
  end

  def store_mail_in_remote_files(mail, options = {})
    return unless mail.eml_url

    eml_content = options[:eml_fixture] || "raw eml source of #{mail.message_id}"

    remote_eml_file = Zendesk::Mail.raw_remote_files.file_from_url(mail.eml_url,
      content: eml_content,
      content_type: "msg/rfc822")

    if remote_eml_file
      remote_eml_file.stored_in.clear
      remote_eml_file.store_once!

      mail.json_url ||= mail.eml_url.gsub(/\.eml$/, ".json")
      remote_json_file = Zendesk::Mail.raw_remote_files.file_from_url(mail.json_url,
        content: Yajl::Encoder.encode(mail.as_json, pretty: true),
        content_type: "application/json",
        configuration: Zendesk::Mail.raw_remote_files.name)
      remote_json_file.stored_in.clear
      remote_json_file.store_once!
    end
  end

  def stub_mail_from(address)
    mail.from = address
    Zendesk::InboundMail::MailParsingQueue::ProcessingState.any_instance.stubs(from: mail.from)
  end
end

module Minitest::Assertions
  def assert_html_equal(expected, actual)
    HTMLEntities.new.decode(actual).gsub(/[\n\s\t]*/, '').must_equal(HTMLEntities.new.decode(expected).gsub(/[\n\s\t]*/, ''))
  end

  def assert_html_includes(expected, actual)
    assert_includes HTMLEntities.new.decode(Nokogiri::HTML5.parse(actual).to_html), HTMLEntities.new.decode(expected)
  end

  def refute_html_includes(expected, actual)
    refute_includes HTMLEntities.new.decode(Nokogiri::HTML5.parse(actual).to_html), HTMLEntities.new.decode(expected)
  end
end

module MiniTest::Expectations
  infect_an_assertion :assert_html_equal, :html_must_equal
  infect_an_assertion :assert_html_includes, :html_must_include
  infect_an_assertion :refute_html_includes, :html_wont_include
end
