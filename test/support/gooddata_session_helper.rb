module GooddataSessionHelper
  def stub_gooddata_session
    stub_request(:post, %r{v1\.gooddata\.com/gdc/account/login}).to_return(
      headers: {'set-cookie' => 'GDCAuthSST=abc;'},
      body: {
        userLogin: {
          profile: "/gdc/account/profile/profile_id",
          state: "/gdc/account/login/abcloginid"
        }
      }.to_json
    )

    stub_request(:get, %r{v1\.gooddata\.com/gdc/account/token}).to_return(
      headers: {'set-cookie' => 'GDCAuthTT=def;'}
    )

    stub_request(:delete, "https://v1.gooddata.com/gdc/account/login/abcloginid").
      with(headers: {'Cookie' => 'GDCAuthTT=def; GDCAuthSST=abc'})

    stub_request(:post, %r{v2\.zendesk\.com/gdc/account/login}).to_return(
      headers: {'set-cookie' => 'GDCAuthSST=abc;'},
      body: {
        userLogin: {
          profile: "/gdc/account/profile/profile_id",
          state: "/gdc/account/login/abcloginid"
        }
      }.to_json
    )
    stub_request(:get, %r{v2\.zendesk\.com/gdc/account/token}).to_return(
      headers: {'set-cookie' => 'GDCAuthTT=def;'}
    )

    stub_request(:delete, "https://v2.zendesk.com/gdc/account/login/abcloginid").
      with(headers: {'Cookie' => 'GDCAuthTT=def; GDCAuthSST=abc'})
  end
end
