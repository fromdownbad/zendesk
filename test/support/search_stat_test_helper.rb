module SearchStatTestHelper
  def build_search_stat(options = {})
    search_stat = accounts(:minimum).hc_search_stats.new

    attributes = {
      ts: options.fetch(:ts, Time.now),
      string: options.fetch(:string, 'default search'),
      duration: options.fetch(:duration, 86400),
      origin: options.fetch(:origin, 'all')
    }

    search_stat.attributes = attributes
    search_stat.save!
    search_stat
  end
end
