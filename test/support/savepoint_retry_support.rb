module SavepointRetrySupport
  def run(runner)
    Zendesk::Retrier.retry_on_error([ActiveRecord::StatementInvalid], 3, retry_if: ->(e) { e.message.downcase.include?('savepoint') }) do |e|
      if e
        warn('Retrying test due to savepoint error')
        sleep 3 # give it some time for other things to happen
      end
      super(runner)
    end
  rescue ActiveRecord::StatementInvalid => e
    # this is copied from the minitest file, so I don't really care what rubocop has to say
    runner.record(self.class, self.__name__, self._assertions, 0, e) # rubocop:disable Style/RedundantSelf
    runner.puke(self.class, self.__name__, e) # rubocop:disable Style/RedundantSelf
  end
end

# Super shitty hack to help with false negatives from savepoint errors
MiniTest::Unit::TestCase::PASSTHROUGH_EXCEPTIONS << ActiveRecord::StatementInvalid
MiniTest::Unit::TestCase.prepend(SavepointRetrySupport)
