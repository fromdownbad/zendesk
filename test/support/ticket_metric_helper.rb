require 'zendesk/sla'

module TicketMetricHelper
  def after_creation(minutes)
    Timecop.freeze(ticket.created_at + minutes) { yield }
  end

  def status_updated(params = {})
    field_updated(params.merge(field: 'status_id'))
  end

  def priority_updated(params = {})
    field_updated(params.merge(field: 'priority_id'))
  end

  def tags_updated(params = {})
    field_updated(params.merge(field: 'current_tags'))
  end

  def assignee_updated(params = {})
    field_updated(params.merge(field: 'assignee_id'))
  end

  def field_updated(params = {})
    after_creation(params.fetch(:after)) do
      ticket.tap do |t|
        t.send("#{params.fetch(:field)}=", params.fetch(:to))

        t.will_be_saved_by(users(:minimum_agent))
        t.save!
      end.reload
    end
  end

  def comments_added(comments)
    comments.each do |comment|
      after_creation(comment.fetch(:after)) do
        ticket.tap do |t|
          t.add_comment(
            body:      'test comment',
            author:    comment.fetch(:by),
            is_public: comment.fetch(:public, true),
            via_id:    ViaType.WEB_SERVICE
          )

          t.will_be_saved_by(users(:minimum_agent))
          t.save!
        end.reload
      end
    end
  end

  def new_sla_policy(params = {})
    params.fetch(:account) { account }.sla_policies.create!(
      title:    params.fetch(:title) { 'test title' },
      position: params.fetch(:position) { 1 },
      filter: Definition.build(
        conditions_all: params.fetch(:conditions_all) do
          [
            {
              source:   'current_tags',
              operator: 'includes',
              value:    params.fetch(:tags) { 'match_sla' }
            }
          ]
        end
      )
    )
  end

  def new_sla_policy_metric(params = {})
    params.fetch(:policy).policy_metrics.create! do |pm|
      pm.target         = params.fetch(:target)
      pm.business_hours = params.fetch(:business_hours) { false }
      pm.priority_id    = params.fetch(:priority_id) { ticket.priority_id }
      pm.metric_id      = params.fetch(:metric_id)
      pm.account        = params.fetch(:account) { account }
      pm.policy         = params.fetch(:policy)
    end
  end

  def assign_policy(params = {})
    target_ticket = params.fetch(:ticket) { ticket }

    if target_ticket.sla_ticket_policy.present?
      target_ticket.sla_ticket_policy.destroy
      target_ticket.sla_ticket_policy = nil
    end

    new_policy = params.fetch(:policy) { policy }

    if new_policy
      target_ticket.create_sla_ticket_policy(
        account: ticket.account,
        policy:  new_policy
      ).tap(&:cache_statuses).tap(&:save!)
    end
  end

  def measure_metrics(*metrics)
    metrics.each do |metric|
      TicketMetric::Measure.create(
        account: ticket.account,
        ticket:  ticket,
        metric:  metric,
        time:    ticket.created_at
      )
    end

    ticket.reload
  end

  def assert_events_equal(expected, metric_events)
    actual = metric_events.map do |event|
      {
        after:  event.time - event.ticket.created_at,
        metric: event.metric,
        type:   event.type
      }
    end

    assert(
      _collection_equal(expected, actual),
      "Expected: #{expected}\nActual: #{actual}"
    )
  end

  def assert_statuses_equal(expected, statuses)
    actual = statuses.map do |status|
      {
        metric_id:       status.policy_metric.metric_id,
        paused:          status.paused?,
        breached_after:  status.breach_at.try do |breach_at|
          breach_at - status.ticket.created_at
        end,
        fulfilled_after: status.fulfilled_at.try do |fulfilled_at|
          fulfilled_at - status.ticket.created_at
        end
      }
    end

    assert(
      _collection_equal(expected, actual),
      "Expected: #{expected}\nActual: #{actual}"
    )
  end

  def _collection_equal(expected, actual)
    expected.count == actual.count &&
      actual.all? { |item| expected.include?(item) }
  end
end
