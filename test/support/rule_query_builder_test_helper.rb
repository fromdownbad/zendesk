module RuleQueryBuilderTestHelper
  def get_query(defitems)
    account = @current_user ? @current_user.account : @account

    definition = Definition.new.tap do |defn|
      defn.conditions_all.concat(defitems)
    end
    rule = View.new(definition: definition)
    rule.output = Output.create(columns: View::COLUMNS)
    rule.account = account

    @builder ||= Zendesk::Rules::RuleQueryBuilder.new(account, @current_user, rule, skip_default_restrictions: true)
    @builder.query
  end

  def get_query_for_conditions(*conditions)
    @query = get_query(conditions.each_slice(3).map { |args| DefinitionItem.new(*args) }).compile(:sql)
  end

  def generate_sql
    get_query([@definition_item]).compile(:sql).to_sql
  end
end
