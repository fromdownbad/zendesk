module TicketTestHelper
  def ticket_new(user, options = {})
    ticket = Ticket.new
    ticket.account = user.account
    ticket.requester = user
    ticket.client = 'IP address: 127.0.0.1'
    is_public = options.delete(:is_public) unless options[:is_public].nil?
    attributes = {
      via_id: ViaType.WEB_FORM,
      fields: {ticket_fields(:field_text_custom).id.to_s => 'Some text here'}
    }

    if is_public == false
      ticket.add_comment(body: "private ticket first comment", is_public: false)
      ticket.description = "private ticket first comment"
    else
      attributes[:description] = 'first comment'
    end
    attributes[:set_tags] = 'a3 a1' if user.can?(:edit_tags, Ticket)
    ticket.attributes = attributes.merge(options)
    assert ticket.new_record?
    ticket
  end

  def ticket_create(user, ticket_options: {})
    ticket = ticket_new(user, ticket_options)
    ticket.will_be_saved_by(user)
    ticket.save!
    ticket.reload
    assert_equal ticket.submitter, ticket.requester
    if ticket.is_public
      assert_equal 'first comment', ticket.description
      assert_equal ticket.description, ticket.comments.last.to_s
      assert_equal ticket.description, ticket.comments.last.body
      assert_equal ticket.description, ticket.comments.is_public.last.body
      assert ticket.comments.last.is_public?
    else
      assert_equal [], ticket.comments.is_public
    end
    assert_equal 1, ticket.comments.length
    assert_equal ticket.requester, ticket.comments.last.author
    if user.can?(:edit_tags, Ticket)
      assert_equal ticket.current_tags, ticket.tag_array.sort.join(' ')
      assert_equal 'a1 a3', ticket.current_tags
    end
    assert_equal ViaType.WEB_FORM, ticket.via_id
    assert ticket.updated_at

    # Root audit
    assert_equal "Audit via Web form", ticket.audits.first.to_s
    assert_equal "IP address: 127.0.0.1", ticket.audits.first.client
    assert ticket.audits.first.events[0].is_a?(Comment)
    assert ticket.audits.first.events[1].is_a?(Create)

    ticket
  end

  def create_light_agent
    Account.any_instance.stubs(:has_permission_sets?).returns(true)

    @light_agent = User.create! do |user|
      user.account = accounts(:minimum)
      user.name = "Prince Light"
      user.email = "prince@example.org"
      user.roles = Role::AGENT.id
    end

    @light_agent.permission_set = PermissionSet.create_light_agent!(accounts(:minimum))
    @light_agent.save!
  end
end
