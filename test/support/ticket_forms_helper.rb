module TicketFormsHelper
  # setup_ticket_forms creates two ticket_forms
  #
  # 1 - The default TicketForm (validation requires at least one default form)
  #     hence this TicketForm is required to setup before creating any other
  #
  # 2 - An non-active TicketForm  is required to test the presenter with "deleted_ticket_form"
  #
  def setup_ticket_forms
    params = {
      ticket_form: { name: "default wombat", display_name: "default wombat", position: 1, default: true, active: true, end_user_visible: true }
    }
    @default_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
    @default_form.save!
    params = {
      ticket_form: {
        name: "Wombat Inactive", display_name: "Inactive wombat", position: 2, default: false, active: false, end_user_visible: false
      }
    }
    @non_active_form = Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
    @non_active_form.soft_delete!
  end
end
