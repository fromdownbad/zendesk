require 'webrat'

Webrat.configure do |c|
  c.mode = :rack
  c.open_error_files = false
end
require_relative "webrat_html5_support"

ActionDispatch::IntegrationTest.class_eval do
  require 'webrat/core/matchers'
  include Webrat::Methods
  include Webrat::Matchers
end

Webrat::RackAdapter.class_eval do
  def response
    @session.instance_variable_get(:@response)
  end

  def response_code
    response.status.to_i # convert "200 OK" to 200
  end
end

Webrat::Logging.class_eval do
  def logger
    Rails.logger
  end
end
