module ConsulLookupSupport
  SHARDDB = '[
    {
      "Node": { "Node": "s1", "Address": "1.0.0.4" },
      "Service": { "Service": "sharddb", "Tags": [ "master", "pod-1", "shard-1", "shard-1-db-dbxshard1" ], "Port": 3 }
    },
    {
      "Node": { "Node": "s2", "Address": "1.0.0.5" },
      "Service": { "Service": "sharddb", "Tags": [ "master", "pod-2", "shard-3", "shard-3-db-dbxshard1" ], "Port": 3 }
    },
    {
      "Node": { "Node": "s3", "Address": "1.0.0.6" },
      "Service": { "Service": "sharddb", "Tags": [ "master", "pod-3", "shard-4", "shard-4-db-dbxshard1" ], "Port": 3 }
    }
  ]'.freeze

  DATACENTERS = '["dc1"]'.freeze
  REDIS_SENTINEL = '[]'.freeze

  REDIS = '[
    {
      "Node": { "Node": "radar1", "Address": "1.0.0.1" },
      "Service": { "Service": "redis", "Tags": [ "cluster-misc", "pod-1", "role-master" ], "Port": 6379 }
    }
  ]'.freeze

  ACCOUNT_SERVICE = '[
    {
      "Node": { "Node": "account1", "Address": "1.0.0.21" },
      "Service": { "Service": "account-service", "Tags": [ "pod-1" ], "Port": 12080 }
    },
    {
      "Node": { "Node": "account2", "Address": "1.0.0.22" },
      "Service": { "Service": "account-service", "Tags": [ "pod-1" ], "Port": 12080 }
    },
    {
      "Node": { "Node": "account3", "Address": "1.0.0.23" },
      "Service": { "Service": "account-service", "Tags": [ "pod-1" ], "Port": 12080 }
    }
  ]'.freeze

  AUTH_SERVICE = '[
    {
      "Node": { "Node": "auth1", "Address": "1.0.0.11" },
      "Service": { "Service": "auth-service", "Tags": [ "pod-1" ], "Port": 12080 }
    },
    {
      "Node": { "Node": "auth2", "Address": "1.0.0.12" },
      "Service": { "Service": "auth-service", "Tags": [ "pod-1" ], "Port": 12080 }
    },
    {
      "Node": { "Node": "auth3", "Address": "1.0.0.13" },
      "Service": { "Service": "auth-service", "Tags": [ "pod-1" ], "Port": 12080 }
    }
  ]'.freeze

  def stub_sharddb_consul_lookup
    stub_request(:get, %r{http://localhost:8500/v1/health/service/sharddb?cached&stale}).
      to_return(body: SHARDDB)
  end

  def stub_datacenters_consul_lookup
    stub_request(:get, %r{http://localhost:8500/v1/catalog/datacenters?cached&stale}).
      to_return(body: DATACENTERS)
  end

  def stub_redis_sentinel_consul_lookup
    stub_request(:get, %r{http://localhost:8500/v1/health/service/redis_sentinel?cached&stale}).
      to_return(body: REDIS_SENTINEL)
  end

  def stub_redis_consul_lookup
    stub_request(:get, %r{http://localhost:8500/v1/health/service/redis?cached&stale}).
      to_return(body: REDIS)
  end

  def stub_account_service_lookup
    stub_request(:get, %r{http://(consul\.service\.consul|localhost):8500/v1/health/service/account-service\?cached&passing=1&stale&tag=pod-1}).
      to_return(body: ACCOUNT_SERVICE)
  end

  def stub_auth_service_lookup
    stub_request(:get, %r{http://(consul\.service\.consul|localhost):8500/v1/health/service/auth-service\?cached&passing=1&stale&tag=pod-1}).
      to_return(body: AUTH_SERVICE)
  end
end
