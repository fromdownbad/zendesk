module CustomFieldOptionsTestHelper
  def assert_matches_custom_field_options(parent_field)
    fields = JSON.parse(@response.body)['custom_field_options']

    known_ids = parent_field.custom_field_options.map(&:id)
    returned_ids = fields.map { |i| i['id'] }

    assert_equal known_ids.sort, returned_ids.sort
  end

  def assert_matches_custom_field_option(option)
    field = JSON.parse(@response.body)['custom_field_option']

    refute_nil field
    assert_equal option.id, field['id']
    assert_equal option.name, field['name']
  end

  def assert_creates_custom_field_option
    field = JSON.parse(@response.body)['custom_field_option']

    refute_nil field
    assert_kind_of Numeric, field['id']
    assert_equal 'my_value', field['value']
  end

  def assert_updates_existing_custom_field_option(option, updates)
    assert_response 200
    field = JSON.parse(@response.body)['custom_field_option']
    refute_nil field

    option.reload

    updates.each do |key, value|
      assert_equal value, option.send(key)
    end
  end

  def assert_handles_invalid_custom_field_option_update(option)
    assert_response 404
    option.reload
    refute_equal 'my_other_new_value', option.value
  end

  def assert_deletes_custom_field_option(option, klass)
    assert_response :no_content
    assert_raises ActiveRecord::RecordNotFound do
      klass.find(option.id)
    end
  end
end
