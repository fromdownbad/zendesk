module SearchControllerTestHelper
  def self.included(base)
    base.extend(Macros)
  end

  module Macros
    def it_behaves_like_a_search_controller
      describe "authorization" do
        it "allows access to anonymous users when forums are public" do
          Account.any_instance.stubs(:has_public_forums?).returns(false)
          get :index
          assert_response :unauthorized

          Account.any_instance.stubs(:has_public_forums?).returns(true)
          get :index
          assert_response :success
        end

        it "allows access to logged in users when forums are private" do
          login('minimum_end_user')
          Account.any_instance.stubs(:has_public_forums?).returns(false)
          get :index

          assert_response :success
        end
      end

      describe "#index" do
        it "records search statistics" do
          @controller.expects(:store_search_stat).with('hi', anything)
          expect_search('hi')
          get :index, params: { query: 'hi' }
        end

        describe "when searching for a ticket ID" do
          describe "as an agent" do
            before do
              login('minimum_agent')
              @ticket = tickets(:minimum_1)
            end

            describe "with no results" do
              before do
                @controller.stubs(:agent_found_ticket_match?).returns(false)
                ZendeskSearch::Client.any_instance.expects(:search).
                  with("1", is_a(Hash)).
                  returns(count: 0, results: [])
              end

              it "dos nothing" do
                @controller.expects(:ticket_match_shortcut).never
                get :index, params: { query: @ticket.nice_id }
              end
            end

            describe "with one nice_id match" do
              it "redirects to ticket URL with correct format" do
                get :index, params: { query: @ticket.nice_id }
                default_format = (@request.accept || "application/xml").sub("application/", "")
                assert_match /#{ticket_path(@ticket.nice_id)}(\.#{default_format})?$/, @response.redirected_to

                get :index, params: { query: @ticket.nice_id, format: "json" }
                assert_redirected_to ticket_path(@ticket.nice_id, format: "json")

                get :index, params: { query: @ticket.nice_id, format: "xml" }
                assert_redirected_to ticket_path(@ticket.nice_id, format: "xml")
              end
            end

            describe "with matches through shared_ticket.original_id" do
              before do
                @ticket_2 = tickets(:minimum_2)
                @agreement = FactoryBot.create(:agreement, account: @request.account)
              end

              describe "with one original_id match" do
                before do
                  @agreement.shared_tickets.create!(ticket: @ticket_2, original_id: 123123123)
                  assert_nil @request.account.tickets.find_by_nice_id(123123123)
                end

                it "redirects to ticket URL" do
                  get :index, params: { query: 123123123 }
                  default_format = (@request.accept || "application/xml").sub("application/", "")
                  assert_match /#{ticket_path(@ticket_2.nice_id)}(\.#{default_format})?$/, @response.redirected_to
                end
              end

              describe "with multiple results" do
                before do
                  @ticket_3 = tickets(:minimum_3)
                  @ticket.update_attribute(:nice_id, 123123123)
                  @agreement.shared_tickets.create!(ticket: @ticket_2, original_id: 123123123)
                  @agreement.shared_tickets.create!(ticket: @ticket_3, original_id: 123123123)
                end

                it "finds all matching tickets" do
                  get :index, params: { query: 123123123 }
                  query   = assigns(:query)
                  matches = query.result

                  assert_equal 3, matches.count
                  assert matches.include?(@ticket)
                  assert matches.include?(@ticket_2)
                  assert matches.include?(@ticket_3)
                  assert_response :success
                end
              end
            end
          end

          describe "as an end-user" do
            before do
              login('minimum_end_user')
            end

            it "does not redirect to the ticket URL" do
              ticket = tickets(:minimum_1)
              expect_search(ticket.nice_id.to_s)
              get :index, params: { query: ticket.nice_id }

              assert_response :success
            end
          end
        end
      end

      describe "searching forums" do
        describe "as an end user" do
          before do
            login('minimum_end_user')
          end

          it "allows searching on a single forum" do
            expect_search('hi', forum_id: '1')
            get :index, params: { query: 'hi', forum_id: '1' }
          end
        end

        describe "as an agent" do
          before do
            login('minimum_agent')
          end

          it "allows searching on all forums" do
            expect_search('hi')
            get :index, params: { query: 'hi' }
          end

          it "allows a single forum" do
            expect_search('hi', forum_id: '1')
            get :index, params: { query: 'hi', forum_id: '1' }
          end
        end
      end
    end
  end

  def expect_search(query, options = {})
    Zendesk::Search.expects(:search).
      with(anything, query, has_entries(default_options.merge(options))).
      returns(Zendesk::Search::Results.empty)
  end

  def default_options
    { sort_mode: :relevance, per_page: 15, page: 1 }
  end

  def expert_search_options(options)
    expect_search(anything, options)
  end
end
