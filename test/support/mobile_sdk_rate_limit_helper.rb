module MobileSdk
  module RateLimitHelper
    def it_throttles_mobile_sdk_requests(prop, endpoint, throttle_arturo, emergency_arturo, &block)
      describe_with_arturo_disabled(throttle_arturo) do
        it "never imposes a rate limit" do
          @request.stubs(:user_agent).returns("Zendesk SDK for Android")
          Prop.stubs(:throttle).with(prop, [@account.id, endpoint]).returns(true)

          Prop.expects(:throttle!).with(prop, [@account.id, endpoint]).never
          instance_eval(&block)
        end
      end

      describe_with_arturo_enabled(throttle_arturo) do
        it "imposes a rate limit if the request has an SDK User Agent" do
          @request.stubs(:user_agent).returns("Zendesk SDK for Android")
          Prop.stubs(:throttle).with(prop, [@account.id, endpoint]).returns(true)

          Prop.expects(:throttle!).with(prop, [@account.id, endpoint])
          instance_eval(&block)
        end

        describe_with_arturo_enabled(emergency_arturo) do
          it "uses the emergency limit" do
            @request.stubs(:user_agent).returns("Zendesk SDK for Android")
            Prop.stubs(:throttle).with(prop, [@account.id, endpoint]).returns(true)

            Prop.expects(:throttle!).with(:mobile_sdk_emergency_limit, [@account.id, endpoint])
            instance_eval(&block)
          end
        end
      end
    end
  end
end
