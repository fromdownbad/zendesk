require 'shoulda/change_matchers'

module Shoulda2Backports
  # TODO: convert everything to the new syntax via search & replace
  def should_set_the_flash_to(val)
    if val.is_a?(Symbol) # no real support in shoulda, swallowed in 2 and raises in 3
      it "sets the #{val} flash" do
        assert flash[val]
      end
    else
      it { set_flash.to(val) }
    end
  end

  # https://github.com/thoughtbot/shoulda-matchers/issues/252
  def should_assign_to(*names, &block)
    it "assigns to #{names.inspect}" do
      names.each do |name|
        assigned = assigns(name)
        if block_given?
          assert_equal instance_exec(&block), assigned
        else
          assert assigned
        end
      end
    end
  end

  def should_not_assign_to(*names, &block)
    it "assigns not to #{names.inspect}" do
      names.each do |name|
        assigned = assigns(name)
        if block_given?
          assert_not_equal instance_exec(&block), assigned
        else
          refute assigned
        end
      end
    end
  end

  def should_render_template(template)
    should render_template(template)
  end

  def should_redirect_to(description, &block)
    should redirect_to(description, &block)
  end

  def should_route(method, path, options)
    should route(method, path).to(options)
  end

  def should_validate_presence_of(*attributes)
    message = shoulda_get_options!(attributes, :message)

    attributes.each do |attribute|
      it { validate_presence_of(attribute).with_message(message) }
    end
  end

  def should_not_allow_values_for(attribute, *bad_values)
    message = shoulda_get_options!(bad_values, :message)
    bad_values.each do |value|
      should_not allow_value(value).for(attribute).with_message(message)
    end
  end

  def should_allow_values_for(attribute, *good_values)
    shoulda_get_options!(good_values)
    good_values.each do |value|
      it { allow_value(value).for(attribute) }
    end
  end
end

ActiveSupport::TestCase.extend(Shoulda2Backports)
# TODO: for rails5? ActionDispatch::IntegrationTest.extend(Shoulda2Backports)
ActiveSupport::TestCase.extend(Shoulda::ChangeMatchers)

require "shoulda/matchers/version.rb"
raise "verify me by commenting me out and running tos_controller_test.rb" if RAILS51
# https://github.com/thoughtbot/shoulda-matchers/issues/809#issuecomment-165383383
require "shoulda/matchers/action_controller/route_params.rb"
Shoulda::Matchers::ActionController::RouteParams::PARAMS_TO_SYMBOLIZE.clear
