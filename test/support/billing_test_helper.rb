require_relative "../support/voice_test_helper"

module BillingTestHelper
  include ZendeskBillingCore::ZuoraTestHelper

  def check_pricing(subscription, expected = {})
    check_expectations(subscription, populate_related_expectations(expected))
  end

  def populate_related_expectations(expected)
    expected = expected.clone

    2.times do
      expected[:cycle_undiscounted_price] ||= expected[:amount_no_rounding]       if expected.key?(:amount_no_rounding)
      expected[:amount_no_rounding]       ||= expected[:cycle_undiscounted_price] if expected.key?(:cycle_undiscounted_price)

      if expected.key? :cycle_duration
        expected[:monthly_undiscounted_price] ||= expected[:cycle_undiscounted_price] * 30 / expected[:cycle_duration] if expected.key?(:cycle_undiscounted_price)
        expected[:cycle_undiscounted_price]   ||= expected[:cycle_duration] * expected[:monthly_undiscounted_price]    if expected.key?(:monthly_undiscounted_price)
      end
    end

    if (expected.keys & [:amount, :net, :discount]).count == 2
      missing = [:amount, :net, :discount] - expected.keys
      case missing.first
      when :amount
        expected[:amount] = expected[:net] + expected[:discount]
      when :net
        expected[:net] = expected[:amount] - expected[:discount]
      when :discount
        expected[:discount] = expected[:amount] - expected[:net]
      end
    end

    component_discounts = [:discount_from_manual, :discount_from_cycle, :discount_from_coupon]
    if expected.key?(:discount) && expected[:discount] == 0
      component_discounts.each { |d| expected[d] = 0 }
    end

    component_discounts_given = expected.keys & component_discounts
    missing = component_discounts - component_discounts_given
    if expected.key?(:discount) && missing.count == 1
      missing = missing.first
      expected[missing] = expected[:discount] - expected.values_at(*component_discounts_given).reduce(:+)
    end

    expected
  end

  def force_into_dunning(subscription)
    payment = subscription.payments.last
    payment.update_attributes! failures: 30

    subscription.dunning_notifications.create! do |notification|
      notification.payment = payment
      notification.delivered_at = 2.days.ago
      notification.dunning_level = ZendeskBillingCore::States::Dunning::State::WARNING.level
    end

    subscription.dunning_notifications.create! do |notification|
      notification.payment = payment
      notification.delivered_at = Time.now
      notification.dunning_level = ZendeskBillingCore::States::Dunning::State::LASTCHANCE.level
    end
  end

  # create a paying account with records consistent with the following history:
  # 1) Was a trial
  # 2) Converted to paying via credit card with the provided subscription details two days before trial expiry
  # 3) First payment was paid the same day as the conversion
  DEFAULT_SUBSCRIPTION_ATTRIBUTES = {
    billing_cycle_type: BillingCycleType.Monthly,
    base_agents: 5,
    plan_type: SubscriptionPlanType.Medium,
    pricing_model_revision: ZBC::Zendesk::PricingModelRevision::PATAGONIA
  }.freeze

  def paying_subscription(attributes = DEFAULT_SUBSCRIPTION_ATTRIBUTES)
    account = paying_account({}, attributes)

    payment = account.subscription.payments.pending.first
    subscription = account.subscription
    [account.reload, subscription.reload, payment.reload, subscription.payments.pending.first]
  end

  # Build a "paying" account, one paid payment, fake CC, 10 agents.
  def paying_account(account_attr = {}, subscription_attr = {})
    accounts(:trial).tap do |account|
      options = { account: account }.merge(subscription_attr)
      create_zuora_subscription(options)
      account.attributes = account_attr
      account.subscription.attributes = subscription_attr
      account.save!
      account.subscription.zuora_subscription.push!
    end.reload
  end

  # currently returns a fixture account/subscription pair, both in trial
  # may be refactored away from fixtures at some point, but will always return a created subscription in trial
  # the linked account might disappear however
  def trial_subscription
    subscriptions(:trial)
  end

  def enterprise_trial_account
    account = accounts(:trial)
    account.subscription.update_attributes!(plan_type: 4)

    light_agent = account.agents.first
    light_agent.permission_set = light_agent.account.permission_sets.create!(
      role_type: PermissionSet::Type::LIGHT_AGENT,
      account: account,
      name: "Test Light Agent Role"
    )
    light_agent.save!

    billable_agents = (1..5).map { |_| account.agents.first }
    unbillable_agents = (1..5).map { |_| light_agent }
    agents = billable_agents + unbillable_agents

    Account.any_instance.stubs(:agents).returns(agents)
    Account.any_instance.stubs(:has_permission_sets?).returns(true)
    Account.any_instance.stubs(:billable_agents).returns(billable_agents)
    Account.any_instance.stubs(:unbillable_agents).returns(unbillable_agents)

    account
  end

  def manually_set_credit_card(subscription, options = {})
    if options[:number].present?
      CreditCard.delete_all(subscription_id: subscription.id)
      subscription.build_credit_card(options.merge(payment_gateway_type: 'quickpay')) # only quickpay cards can be created this way.
    end
  end

  def converted_trial_with_voice
    account = accounts(:trial)
    subscription = account.subscription
    manually_set_credit_card(subscription, FactoryBot.attributes_for(:credit_card))
    subscription.update_attributes! base_agents: 10
    voice_sub_account_setup(account)
    account.reload
  end

  def invoice_paying_account
    account = accounts(:trial)
    account.subscription.update_attributes!(payment_method_type: PaymentMethodType.MANUAL)
    account.payments.pending.last.update_attributes!(status: PaymentType.INVOICED)
    account.payments.invoiced.last.update_attributes!(billing_cycle_type: nil)
    account.payments.invoiced.last.update_attributes!(status: PaymentType.PAID)
    account.reload
  end

  def voice_sub_account_setup(account)
    account.create_voice_sub_account
    account.phone_numbers.create(monthly_charge: 1)
    account.voice_sub_account.update_attributes!(opted_in: true)
  end

  def voice_paying_account_by_credit_card
    account = paying_account
    voice_sub_account_setup(account)
    account.reload
  end

  def voice_paying_account_by_invoice
    account = paying_account
    CreditCard.any_instance.expects(:destroy)
    account.subscription.update_attributes!(payment_method_type: PaymentMethodType.MANUAL)
    voice_sub_account_setup(account)
    account.reload
  end
end
