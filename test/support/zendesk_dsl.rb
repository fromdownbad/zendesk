module ZendeskDSL
  def goes_to_login(path = '')
    get path
    get headers["Location"] while redirect?
    assert_response :success
    assert_template 'auth/sso_v2_index'
  end

  def logs_in_as(user, password, options = {})
    auth_url = "#{user.account.url(mapped: false, ssl: true)}/access/login"
    options[:return_to] ||= "#{user.account.url}/ping/host"
    post auth_url, params: options.except(:assert_return_to).merge(user: { email: user.email, password: password })
    follow_all_redirects!
    assert_equal (options[:assert_return_to] || options[:return_to]), current_url.sub(/\?flash_digest=\w+/, "")
  end

  def goes_to_verify(user)
    get verify_url(user)
    assert_response :ok
    assert_template 'details'
  end

  def verify_url(user)
    "/verification/email/#{user.identities.email.first.verification_tokens.first.value}"
  end

  def goes_to_new_password(password, user)
    post '/verification/details', params: { user: {password: password }, token: user.identities.email.first.verification_tokens.first.value }
    is_redirected_to('home/index')
  end

  def goes_to_logout
    get '/access/logout'
    is_redirected_to 'auth/sso_v2_index'
  end

  def redirected_to_hc?
    get(headers["Location"]) while redirect?
    assert_equal "/hc", request.path
  end

  def redirected_to_home?
    get(headers["Location"]) while redirect?
    assert_equal "/home/index", request.path
  end

  def is_redirected_to(template) # rubocop:disable Naming/PredicateName
    get(headers["Location"]) while redirect?
    assert_response :success
    assert_template template
  end

  # Form a postdata request to submit to the Braintree Transparent Redirect API. Normally, this is POSTed
  # directly by the user, in the browser. However, the integration test runner can't
  # talk to the outside world, so we simulate it here and pass the response redirect
  # back to Zendesk.
  def register_credit_card(raw_card_data, user)
    gateway_response = Zendesk::Billing::Braintree.test_mode_transparent_redirect(host, raw_card_data, user.account)

    # The gateway is expected to process the request, and redirect back
    # to the callback we specified as part of our original request to api/transact.php.

    assert_equal 302, gateway_response.status
    host_with_protocol     = "https://#{host}"
    redirection_callback   = Zendesk::Billing::Braintree.redirection_callback(host)
    redirect_location      = gateway_response.header['Location'].first
    redirect_location_path = redirect_location.gsub(host_with_protocol, '')
    assert_equal redirection_callback, redirect_location.slice(0, redirection_callback.length)
    assert_match /^http/, redirect_location

    # Simulate the browser's redirection by directly GET'ing the value of
    # the Location header provided.

    get redirect_location_path
    follow_redirect! # SSL
    gateway_response
  end

  def visit!(url, *args)
    visit(url, *args)
    assert_equal url, current_url
  end

  def new_session_as(host, user, password)
    open_session do |sess|
      sess.extend(ZendeskDSL)
      sess.host = host
      sess.goes_to_login
      sess.logs_in_as(user, password)
      yield sess if block_given?
    end
  end

  def signup_from_fixture
    account = accounts(:trial)
    [account.host_name, account.users.first, account]
  end

  def signup
    host = user = account = nil
    open_session do |sess|
      sess.extend(ZendeskDSL)
      sess.post 'https://www.example.com/accounts/', params: valid_signup_parameters
      sess.assert_response :success
      host = valid_signup_parameters[:account][:subdomain] + ".#{Zendesk::Configuration.fetch(:host)}"
      user = User.find_by_name('Ricky Gervais')
      account = user.account
      sess.host! host
      assert_equal 'My Company name', account.name
      assert account.is_active?
      assert account.is_open?
      assert_equal '', account.domain_whitelist
      refute user.is_verified?
      assert account.subscription.payments.pending.first
      assert_equal PaymentType.PENDING, account.subscription.payments.pending.first.status
      sess.goes_to_logout
    end
    [host, user, account]
  end

  def verify_and_set_password(host, user, account, password = 'mypassword')
    # Verify new help desk, and choose password for initial @user (owner)
    open_session do |sess|
      sess.extend(ZendeskDSL)
      sess.host! host
      sess.goes_to_verify(user)
      account.reload
      sess.goes_to_new_password(password, user)
      user.reload
      assert user.is_verified?
      assert user.last_login
      assert_equal user.id, account.owner.id
      assert account.subscription.credit_card.nil?
      sess.host! host
      sess.goes_to_logout
    end
  end
end
