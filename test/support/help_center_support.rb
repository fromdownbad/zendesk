class ActionController::TestCase
  def activate_hc!
    https!
    @request.account.settings.help_center_state = :enabled
    @request.account.default_brand.stubs(:help_center_in_use?).returns(true)
    @request.env['zendesk.brand'] = @request.account.default_brand
    @request.account.settings.save!
  end

  def self.where_web_portal_is_disabled
    describe "where web portal is disabled" do
      before do
        @request.account.disable_web_portal!
        @controller.stubs(:current_brand).returns(@request.account.default_brand)
      end

      yield
    end
  end

  def self.where_web_portal_is_restricted
    describe "where web portal is restricted" do
      before do
        @request.account.restrict_web_portal!
        @controller.stubs(:current_brand).returns(@request.account.default_brand)
      end

      yield
    end
  end

  def self.where_web_portal_is_enabled(&block)
    describe "where web portal is enabled" do
      before do
        @request.account.enable_web_portal!
      end

      instance_exec(&block)
    end
  end

  def self.index_redirects_to_help_center_root
    describe "a GET to :index" do
      it "redirects to help center root" do
        https!
        get :index

        assert_response :redirect
        assert_equal 'https://minimum.zendesk-test.com/hc', response.location.sub(":80", "") # test/functional/categories_controller_test.rb fails without this
      end
    end
  end

  def self.new_redirects_to_help_center_new_content(type)
    describe "a GET to :new" do
      it "redirects to help center root" do
        https!
        get :new

        assert_redirected_to "https://minimum.zendesk-test.com/hc/admin/#{type}/new"
      end
    end
  end

  def self.index_redirects_to_help_center_requests
    describe "a GET to :index" do
      it "redirects to help center requests" do
        get :index

        assert_redirected_to 'https://minimum.zendesk-test.com/hc/requests'
      end
    end
  end

  # Expects @id to be assigned beforehand
  def self.show_redirects_to_help_center_requests
    describe "a GET to :show" do
      it "redirects to the request in help center" do
        get :show, params: { id: @id }

        assert_redirected_to "https://minimum.zendesk-test.com/hc/requests/#{@id}"
      end
    end
  end

  def self.new_redirects_to_help_center_new_request
    describe "a GET to :new" do
      it "redirects to the new request page in help center" do
        get :new

        assert_redirected_to "https://minimum.zendesk-test.com/hc/requests/new"
      end
    end
  end

  def self.index_renders_a_301
    describe "a GET to :index" do
      it "renders a 301" do
        get :index

        assert_equal 301, @response.status.to_i
      end
    end
  end

  def self.index_renders_a_404
    describe "a GET to :index" do
      it "renders a 404" do
        get :index

        assert_equal 404, @response.status.to_i
      end
    end
  end

  def self.show_renders_a_404
    describe "a GET to :show" do
      it "renders a 404" do
        get :show, params: { id: 42 }

        assert_equal 404, @response.status.to_i
      end
    end
  end
end
