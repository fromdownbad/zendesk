class ActiveSupport::TestCase
  def self.should_serialize(*attributes)
    attributes.each do |attribute|
      it "serializes #{attribute}" do
        assert subject.type_for_attribute(attribute.to_s).is_a?(ActiveRecord::Type::Serialized)
      end
    end
  end
end
