module BrandTestHelper
  def create_brand(account, attributes)
    brand = Zendesk::BrandCreator.new(account).brand
    brand.assign_attributes(attributes)
    brand.save!
    brand
  end

  def associate_brand_with(page)
    return unless ChannelsBrand.where(brandable: page).empty?

    channel_brand = ChannelsBrand.last.dup
    channel_brand.brandable = page
    channel_brand.save!
  end
end
