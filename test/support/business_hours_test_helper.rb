module BusinessHoursTestHelper
  def add_schedule_intervals(schedule, *intervals)
    if intervals.empty?
      schedule.workweek.configure_as_default
    else
      schedule.workweek.update(_raw_intervals(intervals))
    end
  end

  def set_time_zone(schedule, time_zone = 'Copenhagen')
    schedule.tap { |s| s.time_zone = time_zone }.tap(&:save).reload
  end

  private

  def _raw_intervals(intervals)
    intervals.map do |day_of_week, start_timestamp, end_timestamp|
      {
        start_time: _minutes_from_week_start(day_of_week, start_timestamp),
        end_time:   _minutes_from_week_start(day_of_week, end_timestamp)
      }
    end
  end

  def _minutes_from_week_start(day_of_week, timestamp)
    Biz::DayOfWeek.from_symbol(day_of_week).week_minute(
      _timestamp_minutes(timestamp)
    )
  end

  def _timestamp_minutes(timestamp)
    hours, minutes = timestamp.split(':').map(&:to_i)

    hours * 60 + minutes
  end
end
