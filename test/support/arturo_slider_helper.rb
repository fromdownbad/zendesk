module ArturoSliderHelper
  def enable_arturo(name, attributes = {})
    Arturo.enable_feature!(name)
    return if attributes.blank?

    Arturo::Feature.find_feature(name).update_attributes(attributes)
  end

  def rollout_arturo(name, attributes = {})
    enable_arturo(name, attributes.merge(phase: 'rollout'))
  end

  def disable_arturo(name)
    Arturo.disable_feature!(name)
  end
end
