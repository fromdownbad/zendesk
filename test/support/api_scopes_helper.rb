require 'zendesk/testing/factories/global_uid'
require 'zendesk/o_auth/testing/client_factory'
require 'zendesk/o_auth/testing/token_factory'

module ApiScopesHelper
  def self.extended(base)
    base.fixtures :accounts, :role_settings
  end

  def with_scopes(*scopes, &block)
    describe "with scopes" do
      let(:account) { accounts(:minimum) }
      let(:client) { FactoryBot.create(:client, account: account, user: account.owner) }

      before do
        @token = FactoryBot.create(:token, client: client, user: account.owner)
        @request.account = account
        @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = @token

        @controller.send(:current_user=, @token.user)
      end

      scopes.each do |scope|
        describe "defined as #{scope}" do
          before do
            @token.scopes = [scope]
            @token.save!
          end

          instance_eval(&block)
        end
      end
    end
  end

  def should_be_authorized(&block)
    it "should be authorized" do
      instance_eval(&block)
      assert response.success?, "Expected response to be successful for #{request.method} #{request.url}"
    end
  end

  def should_not_be_authorized(&block)
    it "should not be authorized" do
      instance_eval(&block)
      assert_equal 403, response.response_code, "For request #{request.method} #{request.url}"
    end
  end
end
