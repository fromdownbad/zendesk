require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered! uncovered: 4

describe BackgroundJobMailer do
  fixtures :accounts

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
  end

  it "delivers completion alert mail for Accountant job completion" do
    BackgroundJobMailer.deliver_completion_alert(accounts(:support), "Zendesk::Maintenance::Jobs::AccountantJob", ["foo@bar.com"])
    sent = ActionMailer::Base.deliveries.first
    assert sent.body.include?("Zendesk::Maintenance::Jobs::AccountantJob completed")
  end
end
