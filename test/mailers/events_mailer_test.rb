require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"
require_relative "../support/reply_parser_test_helper"
require_relative "../support/collaboration_settings_test_helper"

SingleCov.covered! uncovered: 17

describe EventsMailer do
  fixtures :all

  extend ReplyParserTestHelper
  extend ArturoTestHelper
  include MailerTestHelper

  def self.it_only_sends_latest_comments_to_machines
    before do
      ticket.comments.first.update_column(:value, "AAAA")
      ticket.comments.last.update_column(:value, "BBBB")
    end

    it "send all comments" do
      assert_includes text_body, "AAAA"
      assert_includes text_body, "BBBB"
    end

    it "just sends the passed comment when sending to machines" do
      recipient.stubs(machine?: true)
      refute_includes text_body, "AAAA"
      assert_includes text_body, "BBBB"
    end
  end

  let(:settings_private_attachments) { false }
  let(:settings_email_attachments) { false }
  let(:attached_images) { [] }
  let(:inline_attachments) { [] }
  let(:path_prefix) { "#{Rails.root}/test/files" }

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []

    @account = accounts(:minimum)
    @ticket = tickets(:minimum_1)
    @author = users(:minimum_agent)

    @mailer = EventsMailer.send(:new)
    @mailer.stubs(:ticket).returns(@ticket)
    @mailer.stubs(:account).returns(@account)
    @mailer.stubs(:author).returns(@author)

    CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
    Account.any_instance.stubs(:uses_12_hour_clock).returns(true)
  end

  describe "#ticket_message_id" do
    it "idempotently generate a ticket message id" do
      @ticket.expects(:generate_message_id).once.returns("hello")
      assert_equal "hello", @mailer.send(:ticket_message_id)
      assert_equal "hello", @mailer.send(:ticket_message_id)
    end
  end

  describe "#ticket_reply_to_id" do
    it "idempotently generate a ticket reply to id" do
      @ticket.expects(:generate_message_id).with(false).once.returns("hello")
      assert_equal "hello", @mailer.send(:ticket_reply_to_id)
      assert_equal "hello", @mailer.send(:ticket_reply_to_id)
    end
  end

  describe "#subject_account_name" do
    describe "when recipient address name is blank" do
      it "is an empty string" do
        assert_equal "", @mailer.send(:subject_account_name)
      end
    end

    describe "when recipient address name is present" do
      it "is a formatted string" do
        recipient_addresses(:default).update_column(:name, "Rec name")
        assert_equal "[Rec name]", @mailer.send(:subject_account_name)
      end
    end
  end

  describe "#event_headers" do
    describe "when the email is for agents" do
      before do
        @mailer.stubs(:for_agent).returns(true)
        @mailer.stubs(:recipients).returns(users(:minimum_agent))
        @headers = @mailer.send(:event_headers, '123', '123')
      end

      it "sets default headers including priority" do
        assert_match(/^<8XNLPWRKDO_([a-z0-9_]+)_sprut@zendesk-test\.com>$/, @headers["Message-Id"])
        assert_equal "event-id-123", @headers["X-Delivery-Context"]
        assert_equal 3, @headers["X-Priority"]
        assert_equal ["<8XNLPWRKDO@zendesk-test.com>"], @headers["In-Reply-To"]
        assert_equal 4, @headers.keys.size
      end
    end

    describe "when the email is for end users" do
      before do
        @mailer.stubs(:for_agent).returns(false)
        @mailer.stubs(:recipients).returns(users(:minimum_end_user))
        @headers = @mailer.send(:event_headers, '123', '123')
      end

      it "sets default headers, but no priority" do
        assert_match(/^<8XNLPWRKDO_([a-z0-9_]+)_sprut@zendesk-test\.com>$/, @headers["Message-Id"])
        assert_equal "event-id-123", @headers["X-Delivery-Context"]
        assert_equal ["<8XNLPWRKDO@zendesk-test.com>"], @headers["In-Reply-To"]
        assert_equal 3, @headers.keys.size
        refute @headers.key?("X-Priority")
      end
    end

    describe "#ticket_url" do
      it "uses a ticket builder to construct the URL" do
        url_builder = mock('url_builder')
        Zendesk::Tickets::UrlBuilder.expects(:new).returns(url_builder)
        url_builder.expects(:url_for)

        @mailer.send(:ticket_url)
      end
    end

    describe "#event_part_heading" do
      describe "when requested type is Mime[:html]" do
        before do
          @type = Mime[:html]
          I18n.expects(:t).returns("argle")
        end

        describe "and account has SSL enabled" do
          before do
            @ticket.account.is_ssl_enabled = true
          end

          it "wraps the title in an https tag" do
            assert_equal "<a href=\"#{@ticket.url}\" class=\"zd_link\">argle</a>", @mailer.send(:event_part_heading, @type)
          end
        end

        describe "and account doesn't have SSL enabled" do
          before do
            @ticket.account.is_ssl_enabled = false
          end

          it "wraps the title in an http tag" do
            assert_equal "<a href=\"#{@ticket.url}\" class=\"zd_link\">argle</a>", @mailer.send(:event_part_heading, @type)
          end
        end
      end

      describe "when requested type is not Mime[:html]" do
        before do
          @type = "beaver"
          I18n.expects(:t).returns("argle")
        end

        it "returns the translated text" do
          assert_equal "argle", @mailer.send(:event_part_heading, @type)
        end
      end
    end

    describe "#event_part_footer" do
      let(:brand) { stub(name: "Some brand") }

      before do
        @mailer.stubs(:to).returns(:to)
        @mailer.stubs(:brand).returns(brand)
      end

      subject { @mailer.send(:event_part_footer, type) }

      describe "when requested type is Mime[:html]" do
        let(:type) { Mime[:html] }

        describe "and use_agent_footer? is true" do
          before do
            @mailer.stubs(:for_agent).returns(true)
            @mailer.stubs(:use_agent_footer?).returns(true)
            @mailer.stubs(:locale).returns(:locale)
            @mailer.stubs(:ticket_properties_for_agent).returns("ticket_properties_for_agent_v1")
            @mailer.stubs(:ticket_properties_for_agent_v2).returns("ticket_properties_for_agent_v2")
          end

          describe_with_arturo_enabled :email_simplified_threading do
            it 'returns the HTML footer excluding the ticket notificaiton title to agents' do
              refute_includes subject, "ticket_properties_for_agent_v1"
              assert_includes subject, "ticket_properties_for_agent_v2"
              refute_includes subject, I18n.t("txt.email.rule_notification.agent_footer.title", ticket_url: @ticket.url(for_agent: true))
              assert_includes subject, I18n.t("txt.email.footer", account_name: brand.name)
              refute_includes subject, 'Include <strong>"#note"</strong> in the text of your reply to make the reply a team note.'
            end
          end

          it "returns the HTML footer" do
            assert_includes subject, "ticket_properties_for_agent_v1"
            refute_includes subject, "ticket_properties_for_agent_v2"
            assert_includes subject, I18n.t("txt.email.rule_notification.agent_footer.title", ticket_url: @ticket.url(for_agent: true))
            assert_includes subject, I18n.t("txt.email.footer", account_name: brand.name)
            refute_includes subject, 'Include <strong>"#note"</strong> in the text of your reply to make the reply a team note.'
          end
        end

        describe "and use_agent_footer? is false" do
          before do
            @mailer.expects(:use_agent_footer?).returns(false)
          end

          it "returns the HTML footer" do
            assert_equal "This email is a service from Some brand.", subject
          end
        end
      end

      describe "when requested type is not Mime[:html]" do
        let(:type) { "horse" }

        it "returns the plain footer" do
          assert_equal "--------------------------------\nThis email is a service from Some brand.", subject
        end
      end
    end

    describe "#rule_notification_title" do
      before do
        @mailer.expects(:locale).returns(:locale)
        @mailer.stubs(:to).returns([])
      end

      describe "when mail subject ids are disabled" do
        before do
          @mailer.stubs(:for_agent).returns(false)
          Zendesk::Liquid::TicketContext.expects(:render).with("title", @ticket, @author, false, ENGLISH_BY_ZENDESK, Mime[:text], updater: @author, include_tz: true, skip_attachments: nil).returns("rendered")
          @mailer.stubs(:from).returns("'Zendesk' <abc@foo.com>")
        end

        it "renders the subject without the ticket id" do
          assert_equal "rendered", @mailer.send(:rule_notification_title, "title")
        end

        it "renders the subject with the ticket id when delivering to docomo" do
          @mailer.stubs(:to).returns(["docomo08025940776@docomo.ne.jp"])
          @mailer.stubs(:locale_for_liquid).returns(ENGLISH_BY_ZENDESK)
          I18n.expects(:t).with("txt.email.rule_notification.title.for_end_user", ticket_title: "rendered", locale: :locale, ticket_id: @ticket.nice_id).returns("translated")

          assert_equal "translated", @mailer.send(:rule_notification_title, "title")
        end
      end

      describe "for an agent" do
        before do
          @mailer.stubs(:for_agent).returns(true)
          @mailer.stubs(:from).returns("'Zendesk' <abc@foo.zendesk-test.com>")
          Zendesk::Liquid::TicketContext.expects(:render).with("title", @ticket, @author, true, ENGLISH_BY_ZENDESK, Mime[:text], updater: @author, include_tz: true, skip_attachments: nil).returns("rendered")
        end

        describe "when reply address is a Docomo domain and to is only one email" do
          before do
            @mailer.stubs(:to).returns(["docomo08025940776@docomo.ne.jp"])
            @mailer.stubs(:locale_for_liquid).returns(ENGLISH_BY_ZENDESK)
          end

          it "renders the internationalized agent title" do
            I18n.expects(:t).with("txt.email.rule_notification.title.for_agent", ticket_title: "rendered", locale: :locale, ticket_id: @ticket.encoded_id).returns("translated")
            assert_equal "translated", @mailer.send(:rule_notification_title, "title")
          end
        end

        describe "when reply address is a Docomo domain and to is an array of emails" do
          before do
            @mailer.stubs(:to).returns(["test@test.com", "docomo08025940776@docomo.ne.jp", "test2@test.com"])
            @mailer.stubs(:locale_for_liquid).returns(ENGLISH_BY_ZENDESK)
          end

          it "renders the internationalized agent title" do
            I18n.expects(:t).with("txt.email.rule_notification.title.for_agent", ticket_title: "rendered", locale: :locale, ticket_id: @ticket.encoded_id).returns("translated")
            assert_equal "translated", @mailer.send(:rule_notification_title, "title")
          end
        end
      end

      describe "for an end user" do
        before do
          @mailer.stubs(:for_agent).returns(false)
          Zendesk::Liquid::TicketContext.expects(:render).with("title", @ticket, @author, false, ENGLISH_BY_ZENDESK, Mime[:text], updater: @author, include_tz: true, skip_attachments: nil).returns("rendered")
          @mailer.stubs(:from).returns("'Zendesk' <abc@foo.com>")
        end

        it "renders the raw title" do
          assert_equal "rendered", @mailer.send(:rule_notification_title, "title")
        end

        describe "when there is a Docomo domain" do
          before { with_docomo_destination }

          it "renders the internationalized agent title" do
            I18n.expects(:t).with("txt.email.rule_notification.title.for_end_user", ticket_title: "rendered", locale: :locale, ticket_id: @ticket.nice_id).returns("translated")
            assert_equal "translated", @mailer.send(:rule_notification_title, "title")
          end
        end
      end
    end

    describe "#follower_notification_title" do
      before { @mailer.stubs(:to).returns([]) }

      describe "when using a regular address" do
        before do
          @mailer.stubs(:for_agent).returns(true)
          @mailer.expects(:locale).returns(:locale)
        end

        it "renders the title without ticket id" do
          assert_equal "title", @mailer.send(:follower_notification_title, "title")
        end
      end

      describe "when there is a Docomo domain" do
        before { with_docomo_destination }

        it "renders the title with ticket id for user" do
          @mailer.stubs(:for_agent).returns(false)
          assert_match(/^title.*1/, @mailer.send(:follower_notification_title, "title"))
        end

        it "renders the title with encoded ticket id for agent" do
          @mailer.stubs(:for_agent).returns(true)
          assert_match(/^title.*8XNLPW-RKDO/, @mailer.send(:follower_notification_title, "title"))
        end
      end
    end

    describe "#organization_activity_notification_title" do
      before do
        @mailer.expects(:use_personalized_address?).returns(false)
        @mailer.stubs(:for_agent).returns(false)
        @mailer.expects(:locale).returns(:locale)
        @ticket.expects(:title_for_role).with(false, EventsMailer::TITLE_TRUNCATION_LENGTH).returns("hello")
      end

      it "returns the translated title" do
        I18n.expects(:t).with("txt.email.organization_activity_notification.title.with_no_account_for_end_user", locale: :locale, ticket_title: "hello")
        @mailer.send(:organization_activity_notification_title)
      end
    end

    describe "#delimiter" do
      describe "when account.mail_delimiter includes {{txt.email.delimiter}}" do
        before do
          @account.mail_delimiter = "abc {{txt.email.delimiter}} def"
          @mailer.expects(:locale).returns(:schmocale)
          I18n.expects(:t).with("txt.email.delimiter", locale: :schmocale).returns("123")
        end

        it "returns a translated delimiter" do
          assert_equal "##- 123 -##", @mailer.send(:delimiter)
        end
      end

      describe "when account.mail_delimiter does not include {{txt.email.delimiter}}" do
        before { @account.mail_delimiter = "kogle" }

        it "returns account.delimiter" do
          assert_equal "kogle", @mailer.send(:delimiter)
        end
      end
    end

    describe "#text_part_render" do
      before do
        @mailer.expects(:event_part_heading).with(Mime[:text]).returns("title")
        @mailer.expects(:event_part_footer).with(Mime[:text]).returns("footer")
        @mailer.expects(:for_agent).returns(true)
        @mailer.expects(:ticket_url).returns("https://foo.com/tickets/123")
        @account.expects(:text_mail_template).returns("text mail template")
        @mailer.stubs(:placeholder_suppression).returns(false)

        Zendesk::Liquid::TicketContext.expects(:render).with(
          "body", @ticket, @author, true, ENGLISH_BY_ZENDESK, Mime[:text].to_s, ticket_url: "https://foo.com/tickets/123", current_user: nil, updater: @author, include_tz: true, skip_attachments: nil, suppress_placeholder: false, show_last_three_comments: false
        ).returns("ticket content")
      end

      describe "rendering" do
        before do
          @mailer.expects(:delimiter).returns("delimiter")

          Zendesk::Liquid::MailContext.expects(:render).with(
            template: "text mail template", header: "title",
            content: "delimiter\n\nticket content", footer: "footer",
            account: @account, translation_locale: @mailer.send(:locale_for_liquid),
            suppress_placeholder: false
          ).returns("email content")
        end

        it "has the encoded id" do
          assert_equal "email content\n\n\n\n\n\n\n\n\n\n[#{@ticket.encoded_id}]", @mailer.send(:text_part, "body")
        end
      end

      describe_with_reply_parser "rendering without delimiter" do
        it "does not render the delimiter in the email content" do
          Zendesk::Liquid::MailContext.expects(:render).with(
            template: "text mail template", header: "title",
            content: "ticket content", footer: "footer",
            account: @account, translation_locale: @mailer.send(:locale_for_liquid),
            suppress_placeholder: false
          ).returns("email content")
          assert_equal "email content\n\n\n\n\n\n\n\n\n\n[#{@ticket.encoded_id}]", @mailer.send(:text_part, "body")
        end
      end
    end

    describe "#text_part" do
      it "is timed using Context.time_and_record" do
        Zendesk::Liquid::Context.expects(:time_and_record).
          with('events_mailer', ['part:text_part']).
          once

        @mailer.send(:text_part, "body")
      end
    end

    describe "#html_part_render" do
      let(:delimiter) { 'delimiter-delimiter-delimiter' }

      describe "without autolinking" do
        it "assembles the email content using the respective methods" do
          expected = "#{delimiter} <a href=\"\" class=\"zd_link\">"\
            "Ticket #1: minimum 1 ticket</a>  This email is a service from Minimum account.<span style=\"color:#FFFFFF\" aria-hidden=\"true\">"\
            "[#{@ticket.encoded_id}]</span>"
          content = @mailer.send(:html_part, "")
          content.must_include(expected)
        end

        it "uses the encoded ID" do
          @mailer.unstub(:ticket_message_id)
          @mailer.expects(:ticket_message_id).never
          content = @mailer.send(:html_part, "")
          content.must_include "[#{@ticket.encoded_id}]"
        end
      end

      describe "autolinking" do
        describe "when hc is enabled" do
          before do
            Zendesk::Liquid::TicketContext.stubs(render: " #123 ")
            Account.any_instance.stubs(help_center_enabled?: true)
          end

          describe "when for agent" do
            before do
              Zendesk::Liquid::TicketContext.stubs(render: " #123 ")
              @mailer.instance_variable_set(:@for_agent, true)
            end

            it "autolinks correctly" do
              assert_match(/<a target="_blank" href="\/agent\/tickets\/123">#123<\/a>/, @mailer.send(:html_part, "body"))
            end

            it "successfully links with lazy matching" do
              Zendesk::Liquid::TicketContext.stubs(render: "<a>First link</a> #123 <a>Second link</a>")
              assert_match(/<a>First link<\/a> <a target="_blank" href="\/agent\/tickets\/123">#123<\/a> <a>Second link<\/a>/, @mailer.send(:html_part, "body"))
            end
          end

          describe "when not for agent" do
            it "autolinks correctly" do
              assert_match(/<a target="_blank" href="\/hc\/requests\/123">#123<\/a>/, @mailer.send(:html_part, "body"))
            end
          end

          describe "for preformatted text" do
            it "does not autolink pre tags" do
              Zendesk::Liquid::TicketContext.stubs(render: "<pre>#123</pre>")
              assert_match(/<pre>#123<\/pre>/, @mailer.send(:html_part, "body"))
            end

            it "does not autolink code tags" do
              Zendesk::Liquid::TicketContext.stubs(render: "<code>#123</code>")
              assert_match(/<code>#123<\/code>/, @mailer.send(:html_part, "body"))
            end

            it "does not autolink style tags" do
              Zendesk::Liquid::TicketContext.stubs(render: "<style type='text/css'>p { color: #111; }</style>")
              assert_match(/<style type="text\/css">p { color: #111; }<\/style>/, @mailer.send(:html_part, "body"))
            end

            it "does not autolink when there are nested html elements inside preformatted_text" do
              Zendesk::Liquid::TicketContext.stubs(render: "<code>#123 <br /></code>")
              assert_match(/<code>#123 <br \/><\/code>/, @mailer.send(:html_part, "body"))
            end
          end
        end

        describe "when hc is not enabled" do
          before { Zendesk::Liquid::TicketContext.stubs(render: " #123 ") }

          describe "when for agent" do
            before { @mailer.instance_variable_set(:@for_agent, true) }

            it "autolinks correctly" do
              assert_match(/<a target="_blank" href="\/agent\/tickets\/123">#123<\/a>/, @mailer.send(:html_part, "bottles & cans"))
            end
          end
        end

        describe_with_arturo_setting_enabled :no_mail_delimiter do
          it "does not include a delimiter" do
            refute_includes @mailer.send(:html_part, ''), delimiter
          end
        end
      end
    end

    describe "#html_part" do
      it "is timed using class's #time_and_record" do
        Zendesk::Liquid::Context.expects(:time_and_record).
          with('events_mailer', ['part:html_part']).
          once

        @mailer.send(:html_part, "")
      end
    end

    describe "#inject_message" do
      let(:slug) do
        "<span style='color:#FFFFFF' aria-hidden='true'>[#{@ticket.encoded_id}]</span>"
      end
      let(:agent_slug) do
        "<span style='color:#FFFFFF' aria-hidden='true'>[#{@ticket.encoded_id}]</span><span style='color:#FFFFFF' aria-hidden='true'>Ticket-Id:#{@ticket.nice_id}</span><span style='color:#FFFFFF' aria-hidden='true'>Account-Subdomain:#{@account.subdomain}</span>"
      end

      before do
        @account.expects(:is_sandbox?).returns(false)
        @mailer.instance_variable_set(:@for_agent, false)
      end

      describe "when the input text has a </body> tag" do
        before do
          @text = "and hello </body>"
          @notification = Notification.new(subject: 'bar', body: 'foo', author: @agent)
          @notification.stubs(message_id: message_id)
          @mailer.stubs(event: @notification)
        end

        let(:message_id) { '<foo_12345_sprut@zendesk.com>' }

        it "injects a message just before the </body> tag" do
          assert_equal "and hello #{slug}</body>", @mailer.send(:inject_message, @text)
        end

        describe_with_arturo_enabled :email_invisible_metadata_span_tags do
          let(:slug) do
            "<span style='color:#FFFFFF;visibility:hidden' aria-hidden='true'>[#{@ticket.encoded_id}]</span>"
          end

          it "injects a message just before the </body> tag" do
            assert_equal "and hello #{slug}</body>", @mailer.send(:inject_message, @text)
          end
        end

        describe_with_arturo_enabled :email_invisible_message_id do
          describe 'when message-id is not nil' do
            let(:slug_with_message_id) do
              "<span style='color:#FFFFFF' aria-hidden='true'>[#{@ticket.encoded_id}]</span>" \
            "<span style='color:#FFFFFF' aria-hidden='true'>Message-Id:#{message_id.gsub(/[<>@]/, '')}</span>"
            end

            it "injects a message just before the </body> tag that includes the Message-ID" do
              assert_equal "and hello #{slug_with_message_id}</body>", @mailer.send(:inject_message, @text)
            end
          end

          describe 'when message-id is nil' do
            before do
              @notification.stubs(message_id: nil)
            end

            it 'does not emit the text for message-id' do
              assert_equal "and hello #{slug}</body>", @mailer.send(:inject_message, @text)
            end
          end
        end
      end

      describe "when the input text has no </body> tag" do
        before { @text = "and hello " }

        it "appends a message" do
          assert_equal "and hello #{slug}", @mailer.send(:inject_message, @text)
        end

        describe_with_arturo_enabled :email_invisible_message_id do
          let(:message_id) { '<foo_12345_sprut@zendesk.com>' }
          let(:slug_with_message_id) do
            "<span style='color:#FFFFFF' aria-hidden='true'>[#{@ticket.encoded_id}]</span>" \
            "<span style='color:#FFFFFF' aria-hidden='true'>Message-Id:#{message_id.gsub(/[<>@]/, '')}</span>"
          end

          before do
            notification = Notification.new(subject: 'bar', body: 'foo', author: @agent)
            notification.stubs(message_id: message_id)
            @mailer.stubs(event: notification)
          end

          it "injects a message just before the </body> tag that includes the Message-ID" do
            assert_equal "and hello #{slug_with_message_id}", @mailer.send(:inject_message, @text)
          end
        end
      end

      describe "when the user is an agent" do
        before do
          @text = "and hello "
          @mailer.instance_variable_set(:@for_agent, true)
        end
        it "includes the ticket id and account subdomain" do
          assert_equal "and hello #{agent_slug}", @mailer.send(:inject_message, @text)
        end
      end
    end

    describe "statsd counters" do
      it "increments events_header counter" do
        Rails.application.config.statsd.client.expects(:increment).at_least_once
        @mailer.send(:event_headers, '123', '123')
      end

      it "increments events_headers_for_satisfaction counter if the body contains the satisfaction placeholder" do
        Rails.application.config.statsd.client.expects(:increment).times(2)
        @mailer.send(:event_headers, '123', '{{satisfaction.rating_section}}')
      end
    end
  end

  describe '#organization_activity_notification' do
    let(:author)    { @minimum_agent }
    let(:ticket)    { tickets(:minimum_1) }
    let(:recipient) { ticket.requester }

    let(:organization_activity) do
      OrganizationActivity.create(
        recipients: [recipient.id],
        author: author,
        audit: events(:create_audit_for_minimum_ticket_1)
      )
    end

    let(:email) do
      EventsMailer.create_organization_activity_notification(
        ticket,
        recipient,
        organization_activity,
        organization_activity.id
      )
    end

    it 'sets the `to` header with the recipient' do
      assert_equal(
        [recipient.email_address_with_name],
        email.header[:to].value
      )
    end

    it 'does not set the `cc` header' do
      assert_empty email.header[:cc].value
    end

    describe 'when an email has valid attachments' do
      let(:inline_attachments) { %w[google_default.jpg] }

      describe 'when the account can send real attachments' do
        before do
          Account.any_instance.stubs(:send_real_attachments?).returns(true)
          Account.any_instance.stubs(:organization_activity_email_template).returns "Organization activity email template {{ ticket.comments_formatted }}"
          StoresSynchronizationJob.stubs(:enqueue)

          ticket.add_comment body: "I've attached that file you asked for", is_public: true
          ticket.will_be_saved_by(author)

          @attachments = []
          inline_attachments.each do |filename|
            @attachments << ticket.comment.attachments.build(
              uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{filename}"), filename),
              account: ticket.account,
              author: author,
              is_public: true
            )
          end

          comment_body = "I've attached that file you asked for"

          inline_attachments = %w[normal_1.jpg]

          inline_attachments.each do |filename|
            @attachments << ticket.comment.attachments.build(
              uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{filename}"), filename),
              account: ticket.account,
              author: author,
              is_public: true
            ) do |a|
              a.inline = true
            end

            comment_body = [comment_body, "![](#{@attachments.last.url})"].join(' ')
          end

          ticket.comment.body = comment_body
          ticket.save!

          ticket.reload

          @email = ActionMailer::Base.deliveries.last
        end

        it "includes inline attachments in the email" do
          @email.attachments.count { |a| a.content_disposition =~ /^inline;/ }.must_equal 1
          assert_match /cid:/, @email.html_part.body.to_s
        end

        it "includes appended attachments in the email" do
          appended_attachments = @email.attachments.select { |a| a.content_disposition =~ /^attachment;/ }
          appended_attachments.size.must_equal 1
        end
      end
    end
  end

  describe "#follower_notification" do
    let(:email) { EventsMailer.create_follower_notification(ticket, recipient, recipient.is_agent?, ticket.comments.first, ticket.comments.first.id) }
    let(:text_body) { email.parts.first.body.to_s }
    let(:html_body) { email.parts.last.body.to_s }

    describe "for a normal account" do
      let(:account) { accounts(:minimum) }
      let(:ticket) { tickets(:minimum_1) }
      let(:recipient) { ticket.requester }

      it "builds" do
        assert_includes html_body, "Ticket #1: minimum 1 ticket"
      end

      it "does not add reference when there are no inbound emails" do
        assert_nil email.references
      end

      it 'sets the `cc` header with the recipient' do
        assert_equal(
          [recipient.email_address_with_name],
          email.header[:cc].value
        )
      end

      it 'does not set the `to` header' do
        refute email.header[:to].present?
      end

      describe "with comments in cc body" do
        before do
          account.cc_email_template = "XXX {{ticket.comments_formatted}} XXX"
          account.save!
        end

        it_only_sends_latest_comments_to_machines
      end

      describe "with inbound emails" do
        let(:static_id) { "8XNLPWRKDO@zendesk-test.com" }
        let!(:inbound) { ticket.inbound_emails.create! { |m| m.from = 'from@example.com'; m.message_id = "<inbound@other.com>"; m.in_reply_to = "<reply@other.com>"; m.references = ["<ref1@other.com>", "<ref2@other.com>"] } }

        it "includes references/reply/message-id/ticket-id" do
          assert_equal [
            static_id,
            "reply@other.com",
            "ref2@other.com",
            "ref1@other.com",
            "inbound@other.com"
          ], email.references
        end

        it "only sends 10" do
          inbound.update_attribute(:references, ["<aaa1@bbb>", "<aaa2@bbb>", "<aaa3@bbb>", "<aaa4@bbb>", "<aaa5@bbb>", "<aaa6@bbb>", "<aaa7@bbb>", "<aaa8@bbb>"])
          assert_equal [
            static_id,
            "aaa8@bbb",
            "aaa7@bbb",
            "aaa6@bbb",
            "aaa5@bbb",
            "aaa4@bbb",
            "aaa3@bbb",
            "aaa2@bbb",
            "aaa1@bbb",
            "inbound@other.com"
          ], email.references
        end

        it "prioritizes message-ids and sorts by created at" do
          Timecop.freeze(1.minute.from_now)
          ticket.inbound_emails.create! { |m| m.from = 'from@example.com'; m.message_id = "<inbound2@other.com>"; m.in_reply_to = "<reply2@other.com>"; m.references = ["<ref1@2other.com>", "<ref2@2other.com>"] }
          assert_equal [
            static_id,
            "reply@other.com",
            "ref2@other.com",
            "ref1@other.com",
            "reply2@other.com",
            "ref2@2other.com",
            "ref1@2other.com",
            "inbound@other.com",
            "inbound2@other.com"
          ], email.references
        end

        it "does not remove static ticket id when it was already in a reference / unique from behind" do
          inbound.update_attribute(:references, ["<#{static_id}>", "<aaa2@bbb>", "<aaa3@bbb>", "<aaa4@bbb>", "<aaa5@bbb>", "<aaa6@bbb>", "<aaa7@bbb>", "<aaa8@bbb>"])
          assert_equal [
            static_id,
            "reply@other.com",
            "aaa8@bbb",
            "aaa7@bbb",
            "aaa6@bbb",
            "aaa5@bbb",
            "aaa4@bbb",
            "aaa3@bbb",
            "aaa2@bbb",
            "inbound@other.com"
          ], email.references
        end

        it "sends references only from 10 newest incoming emails" do
          10.times do |i|
            Timecop.freeze(1.minute.from_now)
            ticket.inbound_emails.create! { |m| m.from = "from@example.com"; m.message_id = "<inbound#{i}@other.com>" }
          end
          assert_equal [
            static_id,
            "inbound1@other.com",
            "inbound2@other.com",
            "inbound3@other.com",
            "inbound4@other.com",
            "inbound5@other.com",
            "inbound6@other.com",
            "inbound7@other.com",
            "inbound8@other.com",
            "inbound9@other.com"
          ], email.references
        end
      end
    end
  end

  describe "#rule" do
    let(:account) { accounts(:minimum) }

    let(:notification) do
      Notification.new(
        subject: '{{current_user.name}} has updated {{ticket.title}}',
        body: '{{ticket.url}}',
        author: @minimum_agent,
        recipients: ['fox.trot@example.com']
      )
    end

    before do
      @minimum_1_ticket = tickets(:minimum_1)
      @minimum_end_user = users(:minimum_end_user)
      @minimum_agent = users(:minimum_agent)
      @minimum_account ||= account
      @translation = translation_locales(:japanese)

      audit = events(:create_audit_for_minimum_ticket_1)
      audit.events << notification

      @minimum_account.settings.email_attachments = settings_email_attachments
      @minimum_account.settings.private_attachments = settings_private_attachments

      @minimum_account.text_mail_template += 'My TEXT PART is BLEEDING'
      @minimum_account.html_mail_template += 'My HTML PART is BLEEDING'
      @minimum_account.subscription.plan_type = 3
      @minimum_account.save!
    end

    describe 'when delivered' do
      let(:cc_user)      { users(:minimum_author) }
      let(:email_header) { ActionMailer::Base.deliveries.last.header }

      before do
        EventsMailer.deliver_rule(
          notification,
          @minimum_1_ticket,
          @minimum_end_user,
          false,
          [cc_user]
        )
      end

      it 'sets the correct `from` header' do
        assert_includes email_header['from'].to_s, 'Agent Minimum'
      end

      it 'sets the correct `reply_to` header' do
        assert_equal(
          'support@zdtestlocalhost.com',
          email_header['reply_to'].to_s
        )
      end

      it 'sets the correct `subject` header' do
        assert_equal(
          'Agent Minimum has updated minimum 1 ticket',
          email_header['subject'].to_s
        )
      end

      it 'sets the correct `to` header' do
        assert_includes email_header['to'].to_s, 'minimum_end_user'
      end

      it 'sets the correct `cc` header' do
        assert_equal [cc_user.email_address_with_name], email_header['cc'].value
      end
    end

    describe "with attachments" do
      let(:attached_data) { %w[hello world] }
      let(:attached_images) { %w[normal_1.jpg] }
      let(:inline_attachments) { %w[google_default.jpg] }
      let(:comment_is_public) { true }
      let(:is_handle_inline_attachments_with_same_filename_feature_enabled) { true }

      before do
        StoresSynchronizationJob.stubs(:enqueue)
        Account.any_instance.stubs(:has_email_handle_inline_attachments_with_same_filename?).returns(is_handle_inline_attachments_with_same_filename_feature_enabled)
        @minimum_1_ticket.disable_triggers = true
        @minimum_1_ticket.add_comment body: "I've attached that file you asked for", is_public: comment_is_public
        @minimum_1_ticket.will_be_saved_by(@minimum_agent)
        audit = @minimum_1_ticket.audit

        @attachments = []
        attached_data.each_with_index do |data, i|
          @attachments << @minimum_1_ticket.comment.attachments.build(
            uploaded_data: RestUpload.new(data, "#{i}.txt", 'text/plain'),
            account: @account,
            author: @minimum_agent,
            is_public: true
          )
        end

        attached_images.each do |filename|
          @attachments << @minimum_1_ticket.comment.attachments.build(
            uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{filename}"), filename),
            account: @account,
            author: @minimum_agent,
            is_public: true
          )
        end

        comment_body = "I've attached that file you asked for"

        inline_attachments.each do |filename|
          @attachments << @minimum_1_ticket.comment.attachments.build(
            uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{filename}"), filename),
            account: @account,
            author: @minimum_agent,
            is_public: true
          ) do |a|
            a.inline = true
          end

          comment_body = [comment_body, "![](#{@attachments.last.url})"].join(' ')
        end

        @minimum_1_ticket.comment.body = comment_body

        @minimum_1_ticket.save!

        @notification = Notification.new(
          subject: "Here's that file", author: @minimum_1_agent, recipients: ["pickles@example.com"],
          body: "My formatted and not-HTML-escaped comments are: {{ticket.comments_formatted}}"
        )

        audit.events << @notification

        @args = [@notification, @minimum_1_ticket, @minimum_agent, true]

        EventsMailer.deliver_rule(
          @notification,
          @minimum_1_ticket,
          @minimum_agent,
          true
        )

        @email = ActionMailer::Base.deliveries.last
      end

      describe 'with private attachments setting enabled' do
        let(:settings_private_attachments) { true }

        it 'does not attach the files' do
          @email.attachments.size.must_equal 0

          @attachments.each do |attachment|
            @email.text_part.body.to_s.must_include attachment.url
          end

          assert_select_in(@email.html_part.body.to_s, ':root') do
            assert_select 'a[href=?]', @attachments[0].url, count: 1
            assert_select 'a[href=?]', @attachments[1].url, count: 1
            assert_select 'a[href=?]', @attachments[2].url, count: 1
            assert_select 'img[src=?]', @attachments[3].url, count: 1
          end
        end
      end

      describe 'with private attachments setting disabled' do
        let(:settings_private_attachments) { false }

        describe 'with include attachments in emails setting disabled' do
          let(:settings_email_attachments) { false }

          it 'does not attach the files' do
            @email.attachments.size.must_equal 0

            @attachments.each do |attachment|
              @email.text_part.body.to_s.must_include attachment.url
            end

            assert_select_in(@email.html_part.body.to_s, ':root') do
              assert_select 'a[href=?]', @attachments[0].url, count: 1
              assert_select 'a[href=?]', @attachments[1].url, count: 1
              assert_select 'a[href=?]', @attachments[2].url, count: 1
              assert_select 'a[href=?]', @attachments[3].url, count: 0
              assert_select 'img[src=?]', @attachments[3].url, count: 1
            end
          end
        end

        describe 'with include attachments in emails setting enabled' do
          let(:settings_email_attachments) { true }

          describe 'with files smaller than the individual and overall filesize limits' do
            let(:attached_data) { %w[hello world] }
            let(:attached_images) { %w[normal_1.jpg] }
            let(:inline_attachments) { %w[google_default.jpg] }

            it 'attaches the files to the email' do
              @email.attachments.size.must_equal 4

              @email.attachments[0].content_type.must_match /^text\/plain/
              @email.attachments[0].filename.must_equal '0.txt'
              @email.attachments[0].body.to_s.must_equal 'hello'
              @email.attachments[0].content_disposition.must_equal 'attachment; filename=0.txt'

              @email.attachments[1].content_type.must_match /^text\/plain/
              @email.attachments[1].filename.must_equal '1.txt'
              @email.attachments[1].body.to_s.must_equal 'world'
              @email.attachments[1].content_disposition.must_equal 'attachment; filename=1.txt'

              @email.attachments[2].content_type.must_match /^image\/jpeg/
              @email.attachments[2].filename.must_equal 'google_default.jpg'
              @email.attachments[2].content_disposition.must_equal 'inline; filename=google_default.jpg'

              @email.attachments[3].content_type.must_match /^image\/jpeg/
              @email.attachments[3].filename.must_equal 'normal_1.jpg'
              @email.attachments[3].content_disposition.must_equal 'attachment; filename=normal_1.jpg'

              @email.text_part.body.to_s.wont_include @attachments[0].url
              @email.text_part.body.to_s.wont_include @attachments[1].url
              @email.text_part.body.to_s.wont_include @attachments[2].url
              @email.text_part.body.to_s.must_include @attachments[3].url

              assert_select_in(@email.html_part.body.to_s, ':root') do
                assert_select 'a[href=?]', @attachments[0].url, count: 0
                assert_select 'a[href=?]', @attachments[1].url, count: 0
                assert_select 'a[href=?]', @attachments[2].url, count: 0
                assert_select 'a[href=?]', @attachments[3].url, count: 0
              end
            end

            it 'replaces image sources with cids' do
              assert_select_in(@email.html_part.body.to_s, ':root') do
                assert_select 'img[src=?]', @attachments[2].url, count: 0
                assert_select 'img[src=?]', @email.attachments[2].url, count: 1
              end
            end

            describe "when an audit triggers a notification trigger" do
              let(:recipients) { @minimum_end_user }
              let(:notification_body) { '{{ticket.comments_formatted}}' }
              let(:notification_subject) { '{{ticket.title}}' }
              let(:setup_trigger) do
                create_trigger!("A",
                  conditions_all: [
                    ["update_type", "is", "Change"]
                  ], actions: [
                    ["notification_user", ['requester_id', notification_subject, notification_body]]
                  ])
              end

              before do
                @minimum_1_ticket.disable_triggers = false
                @minimum_1_ticket.save!
                audit = events(:create_audit_for_minimum_ticket_1)
                notification = Notification.new(
                  subject: '{{current_user.name}} has updated {{ticket.title}}',
                  body: '{{ticket.url}}',
                  author: @minimum_agent,
                  recipients: [@minimum_end_user.email]
                )
                audit.events << notification

                @account.triggers.delete_all
                setup_trigger
                ActionMailer::Base.deliveries = []

                @minimum_1_ticket.subject = "Updated subject"
                @minimum_1_ticket.will_be_saved_by(@minimum_agent)
                @minimum_1_ticket.save
                @email = ActionMailer::Base.deliveries.last
              end

              it 'attaches files to the email' do
                @email.attachments.size.must_equal 4
              end
            end

            describe "when an internal note contains attachments" do
              let(:comment_is_public) { false }

              describe "and the recipient is an end user" do
                before { EventsMailer.deliver_rule(@notification, @minimum_1_ticket, @minimum_end_user, false) }

                it { ActionMailer::Base.deliveries.last.attachments.size.must_equal 0 }
              end

              describe "and the recipient is an agent" do
                before { EventsMailer.deliver_rule(@notification, @minimum_1_ticket, @minimum_agent, true) }

                it { ActionMailer::Base.deliveries.last.attachments.size.must_equal 4 }
              end
            end
          end

          describe 'with some appended attachments larger than the individual file limit' do
            let(:attached_data) do
              [
                'A' * (EventsMailer::MAX_ATTACHMENTS_SIZE_OUTBOUND + 1),
                'hello'
              ]
            end
            let(:attached_images) { [] }
            let(:inline_attachments) { ['google_default.jpg'] }

            it 'send all appended attachments as links' do
              @email.attachments.size.must_equal 1

              @email.text_part.body.to_s.must_include @attachments[0].url
              @email.text_part.body.to_s.must_include @attachments[1].url

              assert_select_in(@email.html_part.body.to_s, ':root') do
                assert_select 'a[href=?]', @attachments[0].url, count: 1
                assert_select 'a[href=?]', @attachments[1].url, count: 1
              end
            end

            it 'attaches valid inline attachments' do
              @email.attachments.size.must_equal 1

              @email.text_part.body.to_s.must_include @attachments[2].url

              assert_select_in(@email.html_part.body.to_s, ':root') do
                assert_select 'a[href=?]', @attachments[2].url, count: 0
                assert_select 'img[src=?]', @email.attachments[0].url, count: 1
              end
            end
          end

          describe 'with some inline attachments larger than the individual file limit' do
            let(:attached_data) { [] }
            let(:attached_images) { [] }
            let(:inline_attachments) { %w[google_default.jpg larger_than_limit_image_attachment.png] }

            it 'attaches smallest inline files first up to the message limit' do
              @email.attachments.size.must_equal 1

              @email.attachments[0].content_type.must_match /^image\/jpeg/
              @email.attachments[0].filename.must_equal 'google_default.jpg'
              @email.attachments[0].content_disposition.must_equal 'inline; filename=google_default.jpg'

              @email.text_part.body.to_s.must_include @attachments[0].url
              @email.text_part.body.to_s.must_include @attachments[1].url

              assert_select_in(@email.html_part.body.to_s, ':root') do
                assert_select 'img[src=?]', @email.attachments[0].url, count: 1
                assert_select 'img[src=?]', @attachments[0].url, count: 0
                assert_select 'img[src=?]', @attachments[1].url, count: 1
              end
            end
          end

          describe 'with files of a combined filesize larger than the combined filesize limit' do
            describe 'with only appended attachments' do
              let(:attached_data) do
                individual_filesize = [EventsMailer::MAX_ATTACHMENTS_SIZE_OUTBOUND, account.max_attachment_size].min
                combined_limit = EventsMailer::MAX_ATTACHMENTS_TOTAL_SIZE_OUTBOUND
                number_of_files = (combined_limit.to_f / individual_filesize).ceil
                attachments = []

                number_of_files.times do |i|
                  attachments.push(i.to_s * individual_filesize)
                end

                attachments
              end
              let(:attached_images) { [] }
              let(:inline_attachments) { [] }

              it 'sends attachments as links' do
                @email.attachments.size.must_equal 0

                @email.text_part.body.to_s.must_include @attachments[0].url
                @email.text_part.body.to_s.must_include @attachments[1].url

                assert_select_in(@email.html_part.body.to_s, ':root') do
                  assert_select 'a[href=?]', @attachments[0].url, count: 1
                  assert_select 'a[href=?]', @attachments[1].url, count: 1
                end
              end
            end

            describe 'with only inline attachments' do
              let(:attached_data) { [] }
              let(:attached_images) { [] }
              let(:inline_attachments) { %w[lottaframes.gif large_image_attachment.jpg google_default.jpg large_image_attachment_4mb.jpg] }

              it 'attaches smallest inline attachments up to the limit' do
                @email.attachments.size.must_equal 3

                @email.attachments[0].filename.must_equal 'google_default.jpg'
                @email.attachments[1].filename.must_equal 'lottaframes.gif'
                @email.attachments[2].filename.must_equal 'large_image_attachment_4mb.jpg'

                assert_select_in(@email.html_part.body.to_s, ':root') do
                  assert_select 'img[src=?]', @attachments[0].url, count: 0
                  assert_select 'img[src=?]', @attachments[1].url, count: 1
                  assert_select 'img[src=?]', @attachments[2].url, count: 0
                  assert_select 'img[src=?]', @attachments[3].url, count: 0

                  assert_select 'img[src=?]', @email.attachments[0].url, count: 1
                  assert_select 'img[src=?]', @email.attachments[1].url, count: 1
                  assert_select 'img[src=?]', @email.attachments[2].url, count: 1
                end
              end

              it 'sends the remaining attachments as links' do
                @attachments[1].filename.must_equal 'large_image_attachment.jpg'

                assert_select_in(@email.html_part.body.to_s, ':root') do
                  assert_select 'img[src=?]', @attachments[1].url, count: 1
                end
              end
            end

            describe 'with a mixture of appended and inline attachments' do
              let(:attached_data) do
                [
                  'A' * 1.megabytes,
                  'B' * 2.megabytes
                ]
              end
              let(:attached_images) { [] }
              let(:inline_attachments) { %w[lottaframes.gif large_image_attachment.jpg google_default.jpg large_image_attachment_4mb.jpg] }

              it 'attaches smallest inline attachments up to the limit' do
                @email.attachments.size.must_equal 3

                @email.attachments[0].filename.must_equal 'google_default.jpg'
                @email.attachments[1].filename.must_equal 'lottaframes.gif'
                @email.attachments[2].filename.must_equal 'large_image_attachment_4mb.jpg'

                assert_select_in(@email.html_part.body.to_s, ':root') do
                  assert_select 'img[src=?]', @attachments[2].url, count: 0
                  assert_select 'img[src=?]', @attachments[3].url, count: 1
                  assert_select 'img[src=?]', @attachments[4].url, count: 0
                  assert_select 'img[src=?]', @attachments[5].url, count: 0

                  assert_select 'img[src=?]', @email.attachments[0].url, count: 1
                  assert_select 'img[src=?]', @email.attachments[1].url, count: 1
                  assert_select 'img[src=?]', @email.attachments[2].url, count: 1
                end
              end

              it 'sends the remaining attachments as links' do
                @attachments[0].filename.must_equal '0.txt'
                @attachments[1].filename.must_equal '1.txt'
                @attachments[3].filename.must_equal 'large_image_attachment.jpg'

                assert_select_in(@email.html_part.body.to_s, ':root') do
                  assert_select 'a[href=?]', @attachments[0].url, count: 1
                  assert_select 'a[href=?]', @attachments[1].url, count: 1
                  assert_select 'img[src=?]', @attachments[3].url, count: 1
                end
              end
            end

            describe 'with inline attachments having same filename' do
              describe 'when `email_handle_inline_attachments_with_same_filename` arturo is disabled' do
                let(:attached_data) { [] }
                let(:attached_images) { [] }
                let(:is_handle_inline_attachments_with_same_filename_feature_enabled) { false }
                let(:inline_attachments) { %w[lottaframes.gif google_default.jpg lottaframes.gif] }

                it 'replaces image sources with incorrect or duplicate cids' do
                  @email.attachments.size.must_equal 3
                  @email.attachments[0].filename.must_equal 'google_default.jpg'
                  @email.attachments[1].filename.must_equal 'lottaframes.gif'
                  @email.attachments[2].filename.must_equal 'lottaframes.gif'

                  assert_select_in(@email.html_part.body.to_s, ':root') do
                    assert_select 'img[src=?]', @email.attachments[0].url, count: 1
                    assert_select 'img[src=?]', @email.attachments[1].url, count: 2
                    assert_select 'img[src=?]', @email.attachments[2].url, count: 0
                  end
                end
              end

              describe 'when `email_handle_inline_attachments_with_same_filename` arturo is enabled' do
                let(:attached_data) { [] }
                let(:attached_images) { [] }
                let(:is_handle_inline_attachments_with_same_filename_feature_enabled) { true }
                let(:inline_attachments) { %w[lottaframes.gif google_default.jpg lottaframes.gif] }

                it 'replaces image sources with unique cids' do
                  @email.attachments.size.must_equal 3
                  @email.attachments[0].filename.must_equal 'google_default.jpg'
                  @email.attachments[1].filename.must_equal 'lottaframes.gif'
                  @email.attachments[2].filename.must_equal 'lottaframes.gif'

                  assert_select_in(@email.html_part.body.to_s, ':root') do
                    assert_select 'img[src=?]', @email.attachments[0].url, count: 1
                    assert_select 'img[src=?]', @email.attachments[1].url, count: 1
                    assert_select 'img[src=?]', @email.attachments[2].url, count: 1
                  end
                end
              end
            end
          end
        end
      end
    end

    describe "placeholder suppression" do
      let(:placeholder) do
        FactoryBot.build(
          :ticket,
          account: @account,
          requester: @minimum_end_user,
          submitter: @minimum_end_user,
          via_id: ViaType.MAIL
        )
      end
      let(:attachment) { 'google_default.jpg' }

      before do
        Account.any_instance.stubs(:send_real_attachments?).returns(true)
        StoresSynchronizationJob.stubs(:enqueue)
        @mailer.stubs(:ticket).returns(placeholder)

        placeholder.add_comment(body: "I'm spammy spam.", is_public: true, author: @minimum_end_user)
        placeholder.will_be_saved_by(@minimum_end_user)

        # Add an appended and inline attachment to the comment
        @attachments = []
        @attachments << placeholder.comment.attachments.build(
          uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{attachment}"), attachment),
          account: placeholder.account,
          author: @minimum_end_user,
          is_public: true
        )
        @attachments << placeholder.comment.attachments.build(
          uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{attachment}"), attachment),
          account: @account,
          author: @minimum_end_user,
          is_public: true
        ) do |a|
          a.inline = true
        end

        placeholder.comment.body = ["I'm spammy spam.", "![](#{@attachments.last.url})"].join(' ')
        placeholder.save!

        audit = placeholder.audits[0]
        audit.comment = placeholder.comments[0]

        @notification = Notification.new(
           subject:    'Re: {{ticket.title}}',
           body:       '{{ticket.comments_formatted}} PLACEHOLDER',
           author:     @minimum_end_user,
           recipients: ['fox.trot@example.com', @minimum_end_user.id]
         )
        audit.events << @notification
      end

      describe "When an agent creates a ticket with submitter and requester as end user" do
        let(:placeholder) do
          FactoryBot.build(
            :ticket,
            account: @account,
            requester: @minimum_end_user,
            submitter: @minimum_end_user,
            via_id: ViaType.WEB_SERVICE
          )
        end

        before do
          audit = placeholder.audits[0]
          audit.author_id = @minimum_agent.id
          audit.save!
        end

        describe_with_arturo_enabled :email_placeholder_check_ticket_author do
          it 'sets suppress_placeholder option for the MailContext to false (text and html)' do
            Zendesk::Liquid::MailContext.expects(:render).with(
              has_entries(suppress_placeholder: false)
            ).returns("foo content").twice

            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
          end
        end

        describe_with_arturo_disabled :email_placeholder_check_ticket_author do
          it 'sets suppress_placeholder option for the MailContext to true (text and html)' do
            Zendesk::Liquid::MailContext.expects(:render).with(
              has_entries(suppress_placeholder: true)
            ).returns("foo content").twice

            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, true, [])
          end
        end
      end

      describe "When anybody can submit tickets setting is disabled" do
        before do
          Account.any_instance.stubs(:is_open?).returns(false)
        end

        describe_with_and_without_arturo_enabled :email_placeholder_suppress_with_liquid do
          it "should not remove placeholder from the body" do
            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
            email = ActionMailer::Base.deliveries.last

            email.text_part.body.to_s.must_include "PLACEHOLDER"
            email.text_part.body.to_s.must_include "I'm spammy spam."
            email.html_part.body.to_s.must_include "PLACEHOLDER"
            email.html_part.body.to_s.must_include "I'm spammy spam."

            # should attach the files to the email
            email.attachments.size.must_equal 2
            email.attachments[0].content_disposition.must_equal 'attachment; filename=google_default.jpg'
            email.attachments[1].content_disposition.must_equal 'inline; filename=google_default.jpg'
          end
        end

        it 'sets suppress_placeholder option for the MailContext to false (text and html)' do
          Zendesk::Liquid::MailContext.expects(:render).with(
            has_entries(suppress_placeholder: false)
          ).returns("foo content").twice

          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
        end

        it 'does not set suppress_placeholder option for the TicketContext to true' do
          Zendesk::Liquid::TicketContext.expects(:render).at_least_once
          Zendesk::Liquid::TicketContext.expects(:render).with(
            anything, anything, anything, anything, anything, anything,
            has_entries(suppress_placeholder: true)
          ).returns("bar content").never

          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
        end
      end

      describe "When require users to register is enabled" do
        before do
          Account.any_instance.stubs(:is_signup_required?).returns(true)
        end

        describe_with_and_without_arturo_enabled :email_placeholder_suppress_with_liquid do
          it "should not remove placeholder from the body" do
            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
            email = ActionMailer::Base.deliveries.last

            email.text_part.body.to_s.must_include "PLACEHOLDER"
            email.text_part.body.to_s.must_include "I'm spammy spam."
            email.html_part.body.to_s.must_include "PLACEHOLDER"
            email.html_part.body.to_s.must_include "I'm spammy spam."

            # should attach the files to the email
            email.attachments.size.must_equal 2
            email.attachments[0].content_disposition.must_equal 'attachment; filename=google_default.jpg'
            email.attachments[1].content_disposition.must_equal 'inline; filename=google_default.jpg'
          end
        end

        it 'sets suppress_placeholder option for the MailContext to false (text and html)' do
          Zendesk::Liquid::MailContext.expects(:render).with(
            has_entries(suppress_placeholder: false)
          ).returns("foo content").twice

          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
        end

        it 'does not set suppress_placeholder option for the TicketContext to true' do
          Zendesk::Liquid::TicketContext.expects(:render).at_least_once
          Zendesk::Liquid::TicketContext.expects(:render).with(
            anything, anything, anything, anything, anything, anything,
            has_entries(suppress_placeholder: true)
          ).returns("bar content").never

          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
        end
      end

      describe "for accounts where anyone can submit tickets and no sign up is required" do
        before do
          # 'Anybody can submit tickets' setting is enabled
          Account.any_instance.stubs(:is_open?).returns(true)
          # 'Require users to register is enabled' setting is disabled
          Account.any_instance.stubs(:is_signup_required?).returns(false)
        end

        describe_with_arturo_enabled :email_placeholder_suppress_with_liquid do
          it "removes placeholder from the body" do
            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
            email = ActionMailer::Base.deliveries.last

            email.text_part.body.to_s.must_include "PLACEHOLDER"
            email.text_part.body.to_s.wont_include "I'm spammy spam."
            email.html_part.body.to_s.must_include "PLACEHOLDER"
            email.html_part.body.to_s.wont_include "I'm spammy spam."
          end
        end

        it 'removes attachments' do
          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
          email = ActionMailer::Base.deliveries.last
          email.attachments.size.must_equal 0
        end

        it 'sets suppress_placeholder option for the MailContext to true (text and html)' do
          Zendesk::Liquid::MailContext.expects(:render).with(
            has_entries(suppress_placeholder: true)
          ).returns("foo content").twice

          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
        end

        it 'sets suppress_placeholder option for the TicketContext to true (text and html)' do
          Zendesk::Liquid::TicketContext.expects(:render).at_least_once
          Zendesk::Liquid::TicketContext.expects(:render).with(
            anything, anything, anything, anything, anything, anything,
            has_entries(suppress_placeholder: true)
          ).returns("bar content").twice

          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
        end
      end

      describe "When anybody can submit tickets setting is disabled" do
        before do
          Account.any_instance.stubs(:is_open?).returns(false)
        end

        it "does not remove placeholder from the subject" do
          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
          email = ActionMailer::Base.deliveries.last

          assert_includes email.subject, "Re:"
          assert_includes email.subject, "A subject"
        end
      end

      describe "When require users to register is enabled" do
        before do
          Account.any_instance.stubs(:is_signup_required?).returns(true)
        end

        it "does not remove placeholder from the subject" do
          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
          email = ActionMailer::Base.deliveries.last

          assert_includes email.subject, "Re:"
          assert_includes email.subject, "A subject"
        end
      end

      describe "when email_placeholder_suppress_subject feature is enabled with right account settings" do
        before do
          # 'Anybody can submit tickets' setting is enabled
          Account.any_instance.stubs(:is_open?).returns(true)
          # 'Require users to register is enabled' setting is disabled
          Account.any_instance.stubs(:is_signup_required?).returns(false)
        end

        it "removes placeholder from the subject" do
          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
          email = ActionMailer::Base.deliveries.last

          assert_includes email.subject, "Re:"
          refute_includes email.subject, "A subject"
        end
      end

      describe "when a organization_activity_notification is sent" do
        describe_with_arturo_enabled :email_placeholder_suppress_with_liquid do
          before do
            Account.any_instance.stubs(:organization_activity_email_template).returns("{{ticket.comments_formatted}}")
          end

          it "does not remove the placeholders" do
            EventsMailer.deliver_organization_activity_notification(placeholder, @minimum_end_user, @notification, @notification.id)
            email = ActionMailer::Base.deliveries.last

            email.text_part.body.to_s.must_include "I'm spammy spam."
            email.html_part.body.to_s.must_include "I'm spammy spam."
          end
        end
      end

      describe_with_arturo_disabled :email_placeholder_suppress_subject do
        it "does not remove placeholder from the subject" do
          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
          email = ActionMailer::Base.deliveries.last

          assert_includes email.subject, "Re:"
          assert_includes email.subject, "A subject"
        end
      end

      describe_with_arturo_disabled :email_placeholder_suppress_with_liquid do
        it "should not remove attachment from the body" do
          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
          email = ActionMailer::Base.deliveries.last

          # should attach the files to the email
          email.attachments.size.must_equal 2
          email.attachments[0].content_disposition.must_equal 'attachment; filename=google_default.jpg'
          email.attachments[1].content_disposition.must_equal 'inline; filename=google_default.jpg'
        end
      end
    end

    describe 'redact attachments' do
      before do
        Account.any_instance.stubs(:send_real_attachments?).returns(true)
        Account.any_instance.stubs(:is_signup_required?).returns(true)
        StoresSynchronizationJob.stubs(:enqueue)
        @mailer.stubs(:ticket).returns(placeholder)

        placeholder.add_comment(body: "I'm spammy spam.", is_public: true, author: @minimum_end_user)
        placeholder.will_be_saved_by(@minimum_end_user)

        attachments = build_inline_and_appended_attachments(placeholder, 'google_default.jpg')
        placeholder.comment.body = ["I'm spammy spam.", "![](#{attachments.last.url})"].join(' ')
        placeholder.save!

        @audit = placeholder.audits[0]
        @audit.author_id = @minimum_end_user.id
        @audit.save!
      end

      describe 'when public comment placeholders do not exist in the notification body' do
        let(:placeholder) { FactoryBot.build(:ticket, account: @account, requester: @minimum_end_user, submitter: @minimum_agent, via_id: ViaType.WEB_SERVICE) }

        before do
          @notification = Notification.new(
            subject:    'Re: {{ticket.title}}',
            body:       'PLACEHOLDER',
            author:     @minimum_end_user,
            recipients: [@minimum_agent.id, @minimum_end_user.id]
          )
          @audit.events << @notification
        end

        it 'should not send attachments in the outgoing email' do
          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
          email = ActionMailer::Base.deliveries.last
          email.attachments.size.must_equal 0
        end

        describe "when there are more than one public comment on the ticket" do
          before do
            placeholder.add_comment(body: "I'm spammy spam.", is_public: true, author: @minimum_end_user)
            placeholder.will_be_saved_by(@minimum_end_user)
            placeholder.save!
          end

          describe "#follower_notification" do
            before do
              CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
              placeholder.account.follower_email_template = "Custom follower placeholder"
              placeholder.account.cc_email_template = "Custom email CC placeholder"
              placeholder.account.save!
            end

            let(:recipient) { placeholder.requester }

            it "should not send attachments in the outgoing email" do
              EventsMailer.deliver_follower_notification(placeholder, recipient, recipient.is_agent?, placeholder.comments.first, placeholder.comments.first.id)
              email = ActionMailer::Base.deliveries.last
              email.attachments.size.must_equal 0
            end

            describe_with_arturo_disabled :email_remove_attachments_if_no_public_comment_placeholder do
              it "should continue to send attachments in the outgoing email" do
                EventsMailer.deliver_follower_notification(placeholder, recipient, recipient.is_agent?, placeholder.comments.first, placeholder.comments.first.id)
                email = ActionMailer::Base.deliveries.last
                email.attachments.size.must_equal 2
              end
            end
          end

          it "should not send attachments in the outgoing email" do
            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
            email = ActionMailer::Base.deliveries.last
            email.attachments.size.must_equal 0
          end

          describe_with_arturo_disabled :email_remove_attachments_if_no_public_comment_placeholder do
            it "should continue to send attachments in the outgoing email" do
              EventsMailer.deliver_rule(@notification, placeholder, @minimum_agent, false, [])
              email = ActionMailer::Base.deliveries.last
              email.attachments.size.must_equal 2
            end
          end
        end

        describe 'when the recipient of the email is not an end-user' do
          it 'should continue to send attachments in the outgoing email' do
            EventsMailer.deliver_rule(@notification, placeholder, @minimum_agent, false, [])
            email = ActionMailer::Base.deliveries.last
            email.attachments.size.must_equal 2
          end
        end
      end

      describe 'when public comment placeholders exists in the notification body' do
        let(:placeholder) { FactoryBot.build(:ticket, account: @account, requester: @minimum_end_user, submitter: @minimum_agent, via_id: ViaType.MAIL) }

        before do
          @notification = Notification.new(
            subject:    'Re: {{ticket.title}}',
            body:       'PLACEHOLDER {{ticket.comments_formatted}}',
            author:     @minimum_end_user,
            recipients: ['fox.trot@example.com', @minimum_end_user.id]
          )
          @audit.events << @notification
        end

        it 'should send attachments in the outgoing email' do
          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
          email = ActionMailer::Base.deliveries.last
          email.attachments.size.must_equal 2
        end
      end

      describe 'when dynamic placeholder exists in the notification body' do
        describe 'when the public comment placeholder is in a lower level of dynamic content' do
          let(:placeholder) { FactoryBot.build(:ticket, account: @account, requester: @minimum_end_user, submitter: @minimum_agent, via_id: ViaType.MAIL) }

          before do
            create_dynamic_content!(@account, "foo", "{{ticket.comments_formatted}}")
            @notification = Notification.new(
              subject:    'Re: {{ticket.title}}',
              body:       'PLACEHOLDER {{dc.foo}}',
              author:     @minimum_end_user,
              recipients: ['fox.trot@example.com', @minimum_end_user.id]
            )

            @audit.events << @notification
          end

          it 'should send attachments in the outgoing email' do
            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
            email = ActionMailer::Base.deliveries.last
            email.attachments.size.must_equal 2
          end
        end

        describe 'when the public comment placeholder is not in a lower level of dynamic content' do
          let(:placeholder) { FactoryBot.build(:ticket, account: @account, requester: @minimum_end_user, submitter: @minimum_agent, via_id: ViaType.MAIL) }

          before do
            create_dynamic_content!(@account, "bar", "{{ticket.url}}")
            @notification = Notification.new(
              subject:    'Re: {{ticket.title}}',
              body:       'PLACEHOLDER {{dc.bar}}',
              author:     @minimum_end_user,
              recipients: ['fox.trot@example.com', @minimum_end_user.id]
            )
            @audit.events << @notification
          end

          it 'should not send attachments in the outgoing email' do
            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
            email = ActionMailer::Base.deliveries.last
            email.attachments.size.must_equal 0
          end
        end
      end

      describe 'when the author of the ticket comment is not an end-user' do
        describe 'when public comment placeholders do not exist in the notification body' do
          let(:placeholder) { FactoryBot.build(:ticket, account: @account, requester: @minimum_agent, submitter: @minimum_agent, via_id: ViaType.WEB_SERVICE) }

          before do
            @notification = Notification.new(
              subject:    'Re: {{ticket.title}}',
              body:       'PLACEHOLDER',
              author:     @minimum_end_user,
              recipients: ['fox.trot@example.com', @minimum_end_user.id]
            )

            @audit = placeholder.audits[0]
            @audit.author_id = @minimum_agent.id
            @audit.save!
            @audit.events << @notification
          end

          it 'should send attachments in the outgoing email' do
            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
            email = ActionMailer::Base.deliveries.last
            email.attachments.size.must_equal 2
          end
        end

        describe 'when public comment placeholders exists in the notification body' do
          let(:placeholder) { FactoryBot.build(:ticket, account: @account, requester: @minimum_agent, submitter: @minimum_agent, via_id: ViaType.MAIL) }

          before do
            @notification = Notification.new(
              subject:    'Re: {{ticket.title}}',
              body:       'PLACEHOLDER {{ticket.comments_formatted}}',
              author:     @minimum_end_user,
              recipients: ['fox.trot@example.com', @minimum_end_user.id]
            )

            @audit = placeholder.audits[0]
            @audit.author_id = @minimum_agent.id
            @audit.save!
            @audit.events << @notification
          end

          it 'should send attachments in the outgoing email' do
            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
            email = ActionMailer::Base.deliveries.last
            email.attachments.size.must_equal 2
          end
        end
      end

      describe 'with more than one public comments' do
        let(:placeholder) { FactoryBot.build(:ticket, account: @account, requester: @minimum_end_user, submitter: @minimum_agent, via_id: ViaType.MAIL) }

        before do
          placeholder.add_comment(body: "I'm spammy spam update.", is_public: true, author: @minimum_end_user)
          placeholder.will_be_saved_by(@minimum_end_user)

          attachments = build_inline_and_appended_attachments(placeholder, 'google_default.jpg')
          placeholder.comment.body = ["I'm spammy spam update.", "![](#{attachments.last.url})"].join(' ')
          placeholder.save!

          @audit = placeholder.audits[1]
          @audit.author_id = @minimum_end_user.id
          @audit.save!

          @notification = Notification.new(
            subject:    'Re: {{ticket.title}}',
            body:       'PLACEHOLDER {{ticket.comments_formatted}}',
            author:     @minimum_end_user,
            recipients: ['fox.trot@example.com', @minimum_end_user.id]
          )
          @audit.events << @notification
        end

        it 'should send attachments in the outgoing email' do
          EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
          email = ActionMailer::Base.deliveries.last
          email.attachments.size.must_equal 2
        end
      end

      describe_with_arturo_disabled :email_redact_attachments_when_public_comment_placeholders_not_exists do
        describe 'when public comment placeholders does not exists in the notification body' do
          let(:placeholder) { FactoryBot.build(:ticket, account: @account, requester: @minimum_end_user, submitter: @minimum_agent, via_id: ViaType.MAIL) }

          before do
            @notification = Notification.new(
              subject:    'Re: {{ticket.title}}',
              body:       'PLACEHOLDER',
              author:     @minimum_end_user,
              recipients: ['fox.trot@example.com', @minimum_end_user.id]
            )
            @audit.events << @notification
          end

          it 'should continue to send attachments in the outgoing email' do
            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
            email = ActionMailer::Base.deliveries.last
            email.attachments.size.must_equal 2
          end
        end

        describe 'when public comment placeholders does exists in the notification body' do
          let(:placeholder) { FactoryBot.build(:ticket, account: @account, requester: @minimum_end_user, submitter: @minimum_agent, via_id: ViaType.MAIL) }

          before do
            @notification = Notification.new(
              subject:    'Re: {{ticket.title}}',
              body:       'PLACEHOLDER',
              author:     @minimum_end_user,
              recipients: ['fox.trot@example.com', @minimum_end_user.id]
            )
            @audit.events << @notification
          end

          it 'should continue to send attachments in the outgoing email' do
            EventsMailer.deliver_rule(@notification, placeholder, @minimum_end_user, false, [])
            email = ActionMailer::Base.deliveries.last
            email.attachments.size.must_equal 2
          end
        end
      end
    end

    describe "with multiple comments" do
      before do
        @minimum_1_ticket.add_comment body: "<hr /> woot! another comment!\n\nand some stuff after newlines\nENDCOMMENT"
        @minimum_1_ticket.will_be_saved_by(@minimum_agent)
        @minimum_1_ticket.save!

        @minimum_1_ticket.add_comment body: "this comment contains an ampersanded URL: http://foo.com?foo=1&foo=2"
        @minimum_1_ticket.will_be_saved_by(@minimum_agent)
        @minimum_1_ticket.save!

        @notification = Notification.new(
          subject: "Yeah, this title rocks. ENDTITLE", author: @minimum_1_agent, recipients: ["pickles@example.com"],
          body: "I'm a little teapot. I have unescaped teapot HTML here: <hr /><div>HI!</div>. My formatted and not-HTML-escaped comments are: {{ticket.comments_formatted}} ENDTEMPLATE"
        )

        audit = events(:create_audit_for_minimum_ticket_1)
        audit.events << @notification

        @args = [@notification, @minimum_1_ticket, @minimum_agent, true]
      end

      describe "time_zoned {{ticket.comments_formatted}}" do
        before do
          Notification.any_instance.stubs(author: @minimum_end_user)

          @minimum_end_user.time_zone = "Pacific Time (US & Canada)"
          @agent_time_zoned_created_at = ICU::TimeFormatting.create(zone: ActiveSupport::TimeZone::MAPPING[@minimum_agent.time_zone], date: :medium, time: :long).tap do |f|
            f.set_date_format(true, f.date_format.gsub(/:ss/, ""))
            f.set_date_format(true, f.date_format.tr('h', "H").gsub(/\s*a/, ''))
          end.format(@minimum_1_ticket.comments.last.created_at)
          @author_time_zoned_created_at = ICU::TimeFormatting.create(zone: ActiveSupport::TimeZone::MAPPING[@minimum_end_user.time_zone], date: :medium, time: :long).tap do |f|
            f.set_date_format(true, f.date_format.gsub(/:ss/, ""))
            f.set_date_format(true, f.date_format.tr('h', "H").gsub(/\s*a/, ''))
          end.format(@minimum_1_ticket.comments.last.created_at)

          assert_not_equal @agent_time_zoned_created_at, @author_time_zoned_created_at
        end

        it "uses the to time_zone if to is a user" do
          EventsMailer.deliver_rule(*@args)

          email = ActionMailer::Base.deliveries.first
          html_body = body_by_content_type(email, Mime[:html])

          assert_match @agent_time_zoned_created_at, html_body
        end

        it "uses the author time_zone if to is an email address" do
          @args[2] = @minimum_agent.email_address_with_name
          EventsMailer.deliver_rule(*@args)

          email = ActionMailer::Base.deliveries.first
          html_body = body_by_content_type(email, Mime[:html])

          assert_match @author_time_zoned_created_at, html_body
        end

        it "uses the to time_zone if to is a one user array" do
          @args[2] = [@minimum_agent]
          EventsMailer.deliver_rule(*@args)

          email = ActionMailer::Base.deliveries.first
          html_body = body_by_content_type(email, Mime[:html])

          assert_match @agent_time_zoned_created_at, html_body
        end

        it "uses the to time_zone if to is a two user array" do
          @args[2] = [@minimum_agent, @minimum_end_user]
          EventsMailer.deliver_rule(*@args)

          email = ActionMailer::Base.deliveries.first
          html_body = body_by_content_type(email, Mime[:html])

          assert_match @author_time_zoned_created_at, html_body
        end
      end

      it "BCCs the account's email archive address when enabled" do
        Account.any_instance.stubs(:has_bcc_archiving_enabled?).returns(true)
        @minimum_account.settings.class.any_instance.stubs(:bcc_archive_address).returns('bcc@example.com')
        @minimum_account.settings.bcc_archive_address = 'bcc@example.com'
        @minimum_account.save!
        EventsMailer.deliver_rule(*@args)
        email = ActionMailer::Base.deliveries.first

        assert_equal ['bcc@example.com'], email.bcc
      end

      it "does not BCC the account's email archive address when disabled" do
        Account.any_instance.stubs(:has_bcc_archiving_enabled?).returns(false)
        EventsMailer.deliver_rule(*@args)
        email = ActionMailer::Base.deliveries.first

        assert_nil email.bcc
      end

      it "includes the encoded id as a token in the HTML body" do
        EventsMailer.deliver_rule(*@args)
        email = ActionMailer::Base.deliveries.first
        html = body_by_content_type(email, Mime[:html])
        assert html.index("<span style=\"color:#FFFFFF\" aria-hidden=\"true\">[#{@ticket.encoded_id}]</span>")
      end

      it "renders correctly in Mime[:html] and Mime[:text]" do
        EventsMailer.deliver_rule(*@args)
        email = ActionMailer::Base.deliveries.first

        html_body = body_by_content_type(email, Mime[:html])

        # The agent-supplied liquid template should have unescaped HTML elements
        assert_select_in html_body, 'hr + div', 'HI!'
        # The Zendesk-supplied HTML formatting should be rendered directly.
        assert_select_in html_body, 'hr[style="border: none 0; border-top: 1px dashed #c2c2c2; height: 1px; margin: 15px 0 0 0;"]'
        # The comment contents should be escaped, and wrapped in <div class="zd-comment"> tags
        assert_select_in html_body, 'div.zd-comment', @minimum_1_ticket.comments.size
        assert_select_in html_body, 'div.zd-comment' do |divs|
          assert divs.one? { |d| d.text =~ %r{<hr /> woot! another comment!} }
        end

        html_body.must_include %(<a href="http://foo.com?foo=1&amp;foo=2" rel="noreferrer">http://foo.com?foo=1&amp;foo=2</a>)
        html_body.must_include "My HTML PART is BLEEDING"

        text_body = body_by_content_type(email, Mime[:text])

        text_body.to_s.must_include "woot! another comment!"
        text_body.to_s.must_include "My TEXT PART is BLEEDING"
      end

      describe "preventing leaks of private comments" do
        let(:private_comment_1) { "this comment contains an ampersanded" }
        let(:private_comment_2) { "woot! another comment" }
        let(:body) do
          @args = [
            @notification,
            @minimum_1_ticket,
            @minimum_end_user,
            false
          ]

          EventsMailer.deliver_rule(*@args)

          email = ActionMailer::Base.deliveries.first
          email.joined_bodies
        end

        it "delivers public comments to enduser" do
          @minimum_1_ticket.comments.last.update_column(:is_public, true)
          body.must_include private_comment_1
          body.wont_include private_comment_2
        end

        it "does not deliver private comments to enduser" do
          body.wont_include private_comment_1
          body.wont_include private_comment_2
        end
      end

      describe 'with a template containing a URL with Liquid placeholders that have filters' do
        before do
          @minimum_1_ticket.group.update_attribute(:name, 'Awesome Agents')

          @notification = Notification.new(
            subject: "Yeah, this title rocks. ENDTITLE",
            author: @minimum_1_agent,
            recipients: ["pickles@example.com"],
            body: "A URL: http://www.really-long-domain.com/some/path/to/somewhere.ext?name={{ticket.assignee.first_name}}+{{ticket.assignee.last_name}}&id={{ticket.id}}&group={{ticket.group.name | replace:' ', '+'}} ENDTEMPLATE"
          )

          audit = events(:create_audit_for_minimum_ticket_1)
          audit.events << @notification

          EventsMailer.deliver_rule(
            @notification,
            @minimum_1_ticket,
            @minimum_agent,
            true
          )

          email = ActionMailer::Base.deliveries.first
          @html_body = body_by_content_type(email, Mime[:html])
        end

        it 'renders the URLs properly as links in HTML' do
          assert_select_in @html_body, "a[href='http://www.really-long-domain.com/some/path/to/somewhere.ext?name=Agent+Minimum&id=#{@minimum_1_ticket.nice_id}&group=Awesome+Agents']"
        end
      end

      describe "with comments in body" do
        let(:ticket) { @minimum_1_ticket }
        let(:recipient) { @minimum_agent }
        let(:email) { EventsMailer.create_rule(*@args) }
        let(:text_body) { email.parts.first.body.to_s }
        let(:html_body) { email.parts.last.body.to_s }

        it_only_sends_latest_comments_to_machines
      end
    end

    describe "without comments" do
      it "delivers rules with sane settings" do
        Account.any_instance.stubs(:has_agent_display_names?).returns(true)
        # It adds a request link only if HC is enabled
        @account.enable_help_center!
        @minimum_agent.agent_display_name = 'Agent" Minimum'
        @minimum_agent.save!

        EventsMailer.deliver_rule(
          notification,
          @minimum_1_ticket,
          @minimum_end_user,
          false
        )

        refute ActionMailer::Base.deliveries.empty?
        sent = ActionMailer::Base.deliveries.first
        assert_equal @minimum_end_user.email, sent.to.first
        assert_equal 'support@zdtestlocalhost.com', sent.from.first
        assert_includes ActionMailer::Base.deliveries.first.header['from'].to_s, 'Agent Minimum'
        assert_equal 'support@zdtestlocalhost.com', sent.from.first

        assert_equal 'support@zdtestlocalhost.com', sent.reply_to.first
        assert_equal 'Agent" Minimum has updated minimum 1 ticket', sent.subject
        assert_match(/hc\/requests\/1/, sent.joined_bodies)
      end

      describe "when there is 'Subject:' in the subject" do
        before do
          Notification.any_instance.stubs(subject: "Example Subject: Example")
        end

        it "does not remove 'Subject:' " do
          EventsMailer.deliver_rule(
            notification,
            @minimum_1_ticket,
            @minimum_end_user,
            false
          )

          sent = ActionMailer::Base.deliveries.first

          assert_equal "Example Subject: Example", sent.subject
        end
      end

      it "evens deliver rules with no body or subject" do
        @notification = Notification.new(subject: nil, body: nil, author: @minimum_1_agent, recipients: ["pickles@example.com"])
        audit = events(:create_audit_for_minimum_ticket_1)
        audit.events << @notification

        EventsMailer.deliver_rule(
          @notification,
          @minimum_1_ticket,
          @minimum_agent,
          true
        )

        email = ActionMailer::Base.deliveries.first
        @html_body = body_by_content_type(email, Mime[:html])
        assert_nil email.subject
        assert_includes @html_body.to_s, "Ticket #1: minimum 1 ticket</a>  This email is a service" # nothing but the footers
      end

      it "uses masked ids in plus addressing for agents" do
        recipient_addresses(:backup).update_attributes!(default: true)

        EventsMailer.deliver_rule(
          notification,
          @minimum_1_ticket,
          @minimum_agent,
          true
        )

        sent = ActionMailer::Base.deliveries.first

        assert_equal "support+id#{@minimum_1_ticket.encoded_id}@#{@minimum_account.default_host}", sent.reply_to.first
      end

      describe "subjects for agents" do
        it "does not use masked ids" do
          EventsMailer.deliver_rule(
            notification,
            @minimum_1_ticket,
            @minimum_agent,
            true
          )

          sent = ActionMailer::Base.deliveries.first

          assert_equal "Agent Minimum has updated minimum 1 ticket", sent.subject
        end
      end

      it "includes the account mail delimiter in the mail" do
        mail_delimiter = '## This is a new mail delimiter ##'
        @minimum_account.update_attributes! mail_delimiter: mail_delimiter

        EventsMailer.deliver_rule(
          notification,
          @minimum_1_ticket,
          @minimum_agent,
          false
        )

        sent = ActionMailer::Base.deliveries.last
        assert sent.joined_bodies.include?(mail_delimiter)
      end

      it "includes the mail delimiter translated to the end user's language if the account uses an i18n'ed delimiter and the recipient is a string" do
        locale = translation_locales(:brazilian_portuguese)
        User.any_instance.stubs(:translation_locale).returns(locale)
        Account.any_instance.stubs(:mail_delimiter).returns("{{txt.email.delimiter}}")

        EventsMailer.deliver_rule(
          notification,
          @minimum_1_ticket,
          @minimum_agent,
          false
        )

        sent = ActionMailer::Base.deliveries.last
        expected = I18n.t('txt.email.delimiter', locale: locale)
        assert sent.joined_bodies.include?(expected)
      end

      it "includes the mail delimiter translated to the first end user's language if the account uses an i18n'ed delimiter and the recipient is an array" do
        locale = translation_locales(:brazilian_portuguese)
        User.any_instance.stubs(:translation_locale).returns(locale)
        Account.any_instance.stubs(:mail_delimiter).returns("{{txt.email.delimiter}}")

        EventsMailer.deliver_rule(
          notification,
          @minimum_1_ticket,
          [@minimum_agent],
          false
        )

        sent = ActionMailer::Base.deliveries.last
        expected = I18n.t('txt.email.delimiter', locale: locale)
        assert sent.joined_bodies.include?(expected)
      end

      it "includes the mail footer translated to the end user's language" do
        locale = translation_locales(:brazilian_portuguese)
        User.any_instance.stubs(:translation_locale).returns(locale)

        EventsMailer.deliver_rule(
          notification,
          @minimum_1_ticket,
          @minimum_end_user,
          false
        )

        email = ActionMailer::Base.deliveries.last
        text_body = body_by_content_type(email, Mime[:text])
        html_body = body_by_content_type(email, Mime[:html])
        expected = I18n.t('txt.email.footer', locale: locale)
        assert_includes text_body.to_s, expected
        assert_includes html_body.to_s, expected
      end

      it "uses the account reply address" do
        recipient_addresses(:backup).update_attributes!(default: true)

        EventsMailer.deliver_rule(
          notification,
          @minimum_1_ticket,
          @minimum_end_user,
          false
        )

        sent = ActionMailer::Base.deliveries.first
        assert_equal @minimum_end_user.email, sent.to.first
        assert_includes ActionMailer::Base.deliveries.first.header['from'].to_s, 'Agent Minimum'
        assert_equal "support@#{@minimum_account.default_host}", sent.from.first
        assert_equal "support+id#{@minimum_1_ticket.nice_id}@#{@minimum_account.default_host}", sent.reply_to.first
        assert_equal 'Agent Minimum has updated minimum 1 ticket', sent.subject
      end

      it "sets the Message-ID to a the ticket's generated message id in the headers" do
        @message_id = @minimum_1_ticket.generate_message_id
        Ticket.any_instance.stubs(:generate_message_id).returns(@message_id)

        EventsMailer.deliver_rule(
          notification,
          @minimum_1_ticket,
          @minimum_end_user,
          false
        )

        sent = ActionMailer::Base.deliveries.first.encoded

        assert_includes sent, "Message-ID: #{@message_id}"
        assert_match %r{<.*@.*>}, @message_id
      end

      describe "#ticket_properties_for_agent for modern notifications" do
        before do
          @account.settings.stubs(:modern_email_template?).returns(true)
          EventsMailer.any_instance.stubs(:account).returns(@account)
        end

        it "includes a table with ticket properties in rule notifications sent to agents" do
          EventsMailer.deliver_rule(
            notification,
            @minimum_1_ticket,
            @minimum_end_user,
            true
          )

          sent = ActionMailer::Base.deliveries.first

          assert sent.joined_bodies.include?('agent footer')
        end

        it "includes the correct channel name for ticket created from the web form in rule notifications sent to agents" do
          EventsMailer.deliver_rule(
            notification,
            @minimum_1_ticket,
            @minimum_end_user,
            true
          )

          sent = ActionMailer::Base.deliveries.first

          assert_includes sent.joined_bodies, I18n.t("txt.via_types.web_form")
        end

        it "includes the correct channel name for tickets created via the HelpCenter channel" do
          Ticket.any_instance.stubs(:via_id).returns(ViaType.HelpCenter)

          EventsMailer.deliver_rule(
            notification,
            @minimum_1_ticket,
            @minimum_end_user,
            true
          )

          sent = ActionMailer::Base.deliveries.first

          assert_includes sent.joined_bodies, I18n.t("txt.via_types.help_center")
        end

        it "does not include a table with ticket properties in rule notifications sent to non-agents" do
          EventsMailer.deliver_rule(
            notification,
            @minimum_1_ticket,
            @minimum_end_user,
            false
          )

          sent = ActionMailer::Base.deliveries.first

          refute sent.joined_bodies.include?('agent footer')
        end

        describe "ticket followers row" do
          let(:mail) do
            EventsMailer.deliver_rule(notification, @minimum_1_ticket, @minimum_agent, true)
            ActionMailer::Base.deliveries.first
          end

          describe "when ticket_followers_allowed setting is enabled" do
            before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

            it "includes follower placeholder row when no followers exist" do
              html = Nokogiri::HTML(mail.html_part.body.to_s)
              followers = html.css("table tr").detect { |tr| tr.css("td").first.text == "Followers" }.css("td").last.text
              assert followers == "-"
            end

            it "includes follower details row when followers exist" do
              FactoryBot.create(:agent_follower, user: @minimum_agent, account: @account, ticket: @minimum_1_ticket)
              html = Nokogiri::HTML(mail.html_part.body.to_s)
              followers = html.css("table tr").detect { |tr| tr.css("td").first.text == "Followers" }.css("td").last.text
              assert followers == @minimum_agent.name
            end
          end

          describe "when ticket_followers_allowed setting is disabled" do
            before { CollaborationSettingsTestHelper.new(feature_arturo: true, feature_setting: true, followers_setting: false, email_ccs_setting: true).apply }

            it "includes no follower details row" do
              html = Nokogiri::HTML(mail.html_part.body.to_s)
              followers = html.css("table tr").detect { |tr| tr.css("td").first.text == "Followers" }
              assert_nil followers
            end
          end
        end

        describe "ticket ccs row" do
          let(:mail) do
            EventsMailer.deliver_rule(notification, @minimum_1_ticket, @minimum_agent, true)
            ActionMailer::Base.deliveries.first
          end

          describe "when follower_and_email_cc_collaborations setting is enabled" do
            describe "when comment_email_ccs_allowed setting is enabled" do
              before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

              it "includes CC details row when CCs exist" do
                FactoryBot.create(:agent_email_cc, user: @minimum_agent, account: @account, ticket: @minimum_1_ticket)
                html = Nokogiri::HTML(mail.html_part.body.to_s)
                ccs = html.css("table tr").detect { |tr| tr.css("td").first.text == "CCs" }.css("td").last.text
                assert ccs == @minimum_agent.name
              end

              it "includes CC placeholder row when no CCs exist" do
                html = Nokogiri::HTML(mail.html_part.body.to_s)
                ccs = html.css("table tr").detect { |tr| tr.css("td").first.text == "CCs" }.css("td").last.text
                assert ccs == "-"
              end
            end

            describe "when comment_email_ccs_allowed setting is disabled" do
              before { CollaborationSettingsTestHelper.new(feature_arturo: true, feature_setting: true, followers_setting: true, email_ccs_setting: false).apply }

              it "includes no CC details row" do
                FactoryBot.create(:agent_email_cc, user: @minimum_agent, account: @account, ticket: @minimum_1_ticket)
                html = Nokogiri::HTML(mail.html_part.body.to_s)
                ccs = html.css("table tr").detect { |tr| tr.css("td").first.text == "CCs" }
                assert_nil ccs
              end
            end
          end

          describe "when follower_and_email_cc_collaborations is disabled" do
            before { CollaborationSettingsTestHelper.new(feature_arturo: true, feature_setting: false, followers_setting: false, email_ccs_setting: false).apply }

            describe "when legacy collaboration is enabled" do
              before { Account.any_instance.stubs(is_collaboration_enabled?: true) }

              it "includes CC details row when CCs exist" do
                FactoryBot.create(:agent_email_cc, user: @minimum_agent, account: @account, ticket: @minimum_1_ticket)
                html = Nokogiri::HTML(mail.html_part.body.to_s)
                ccs = html.css("table tr").detect { |tr| tr.css("td").first.text == "CCs" }.css("td").last.text
                assert ccs == @minimum_agent.name
              end

              it "includes CC placeholder row when no CCs exist" do
                html = Nokogiri::HTML(mail.html_part.body.to_s)
                ccs = html.css("table tr").detect { |tr| tr.css("td").first.text == "CCs" }.css("td").last.text
                assert ccs == "-"
              end
            end

            describe "when legacy collaboration is disabled" do
              before { Account.any_instance.stubs(is_collaboration_enabled?: false) }

              it "includes CC placeholder row even when CCs exist" do
                FactoryBot.create(:agent_email_cc, user: @minimum_agent, account: @account, ticket: @minimum_1_ticket)
                html = Nokogiri::HTML(mail.html_part.body.to_s)
                ccs = html.css("table tr").detect { |tr| tr.css("td").first.text == "CCs" }.css("td").last.text
                assert ccs == "-"
              end

              it "includes CC placeholder row when no CCs exist" do
                html = Nokogiri::HTML(mail.html_part.body.to_s)
                ccs = html.css("table tr").detect { |tr| tr.css("td").first.text == "CCs" }.css("td").last.text
                assert ccs == "-"
              end
            end
          end
        end

        describe "groups row" do
          before do
            @minimum_account.subscription.stubs(:plan_type).returns(plan_type)
            StoresSynchronizationJob.stubs(:enqueue)
            @minimum_1_ticket.disable_triggers = true
            Ticket.any_instance.stubs(:group).returns(group) if defined?(group)
            EventsMailer.deliver_rule(notification, @minimum_1_ticket, @minimum_end_user, true)
            sent = ActionMailer::Base.deliveries.first
            html = Nokogiri::HTML(sent.joined_bodies)
            @group_value = html.css('td[class="group_name"] + td').text
          end

          describe "when account subscription plan is essential" do
            let(:plan_type) { ZBC::Zendesk::PlanType::Essential.plan_type }

            it "does not include the groups row" do
              assert_equal "", @group_value
            end
          end

          [
            ZBC::Zendesk::PlanType::Team,
            ZBC::Zendesk::PlanType::Professional,
            ZBC::Zendesk::PlanType::Enterprise
          ].each do |plan|
            let(:plan_type) { plan.plan_type }

            describe "when account subscription plan is #{plan.name}" do
              describe "and the ticket group is present" do
                it "includes group name" do
                  assert_equal @minimum_1_ticket.group.name, @group_value
                end
              end

              describe "and the ticket group is absent" do
                let(:group) { nil }

                it "includes the group placeholder" do
                  assert_equal "-", @group_value
                end
              end
            end
          end
        end
      end

      describe "gmail markup" do
        let(:mail) do
          EventsMailer.create_rule(
            notification,
            @minimum_1_ticket,
            @minimum_end_user,
            @for_agent
          )
        end

        let(:html) { mail.parts.last.body.to_s }

        it "adds gmail markup for end users" do
          assert_includes html, %(href="\")
        end

        it "does not add CDATA since gmail cannot read it" do
          # check in https://www.google.com/webmasters/markup-tester/u/0/ to remove this requirement
          refute_includes html, "CDATA"
        end

        it "adds gmail markup for agents" do
          @for_agent = true
          assert_includes html, %(href="https://minimum.zendesk-test.com/agent/tickets/1")
          script = html[%r{<div itemscope.*?</div></div>}]
          refute_includes script, "<br"
        end
      end
    end
  end

  describe "rendering" do
    before do
      Timecop.freeze(Time.at(1336610850))

      @ticket = tickets(:minimum_1)
      @user = users(:minimum_end_user)
      @agent = users(:minimum_agent)
      @account = accounts(:minimum)

      @ticket.add_comment body: "Here is comment A\n\nIn two paragraphs."
      @ticket.will_be_saved_by(@agent)
      @ticket.save!

      @ticket.add_comment body: "Here is comment B"
      @ticket.will_be_saved_by(@agent)
      @ticket.save!

      @ticket.add_comment body: "> Here is comment C"
      @ticket.will_be_saved_by(@agent)
      @ticket.save!

      @ticket.add_comment body: "```\nHere is comment D\n```"
      @ticket.will_be_saved_by(@agent)
      @ticket.save!

      data = {
        recording_url: "http://example.com/2010-04-01/Accounts/AC987414b864/Recordings/mjallo",
        recorded: true,
        transcription_text: "transcript",
        transcription_status: "completed",
        answered_by_id: @agent.id,
        call_id: 456,
        from: '+14155556666',
        to: '+14155557777',
        started_at: 'June 04, 2014 05:10:27 AM',
        call_duration: 50
      }
      @ticket.add_voice_api_comment(data)
      @ticket.will_be_saved_by(@agent)
      @ticket.save!

      @notification = Notification.new(
        subject: "{{current_user.name}} has updated {{ticket.title}}",
        body: "Check http://{{ticket.url}} and comments {{ticket.comments_formatted}}",
        author: @agent,
        recipients: [@agent.email]
      )

      audit = events(:create_audit_for_minimum_ticket_1)
      audit.events << @notification
    end

    describe "Text email delivery" do
      before do
        EventsMailer.deliver_rule(@notification, @ticket, [@agent], true)
        @email = ActionMailer::Base.deliveries.first
      end

      it "renders voice comment stats" do
        assert_includes body_by_content_type(@email, Mime[:text]), "Call from: +1 (415) 555-6666"
      end
    end

    describe "HTML email delivery" do
      let :html_body do
        EventsMailer.deliver_rule(@notification, @ticket, [@agent], true)
        email = ActionMailer::Base.deliveries.first
        body_by_content_type(email, Mime[:html]).to_s
      end

      it "does not add extra breaks to comment content" do
        assert_equal 15, html_body.split(/<br\s*\/>/).size
      end

      it "does not add extra http:// to URLs" do
        html_body.wont_include "http://http://"
      end

      it "adds a link in the header part" do
        html_body.must_include "<a href=\"https://minimum.zendesk-test.com/agent/tickets/1\" class=\"zd_link\">Ticket #1: minimum 1 ticket</a>"
      end

      it "does not have the subject in the body" do
        html_body.wont_include "Agent Minimum has updated minimum 1 ticket"
      end

      it "has a meaningful subject" do
        html_body.wont_include "Agent Minimum has updated minimum 1 ticket"
      end

      describe "with rich comments coming from mail" do
        let(:body) { '<p>TEST</p>' }

        before do
          Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(false)
          @ticket.comment = Comment.new(ticket: @ticket, account: @account, via_id: ViaType.MAIL, html_body: body)
          @ticket.will_be_saved_by(@minimum_agent)
          @ticket.save!

          @ticket.comments.last.html_body.must_equal "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\"><p>TEST</p></div>"
          @ticket.comments.last.rich?.must_equal true
          @ticket.comments.last.via.must_equal "Mail"
        end

        describe_with_arturo_enabled(:email_notification_body_with_dir_auto_tags) do
          let(:wrapped_body) { "<p dir=\"ltr\">TEST</p>" }

          it "does not add styles to completely styled" do
            html_body.must_include wrapped_body
          end
        end

        describe_with_arturo_disabled(:email_notification_body_with_dir_auto_tags) do
          let(:wrapped_body) { "<p>TEST</p>" }

          it "does not add styles to completely styled" do
            html_body.must_include wrapped_body
          end
        end
      end
    end

    describe "for markdown enabled accounts" do
      describe "in LTR languages" do
        before do
          Comment.any_instance.stubs(:process_as_markdown?).returns(true)
          EventsMailer.deliver_rule(@notification, @ticket, [@agent], true)

          @email = ActionMailer::Base.deliveries.first
          @html_body = body_by_content_type(@email, Mime[:html])
        end

        it "does not auto link markdown CSS" do
          assert @html_body !~ /color: \<a target/
        end

        it "includes the proper directionality for blockquotes" do
          assert_includes @html_body, '<blockquote dir="ltr"'
        end

        it "includes the proper directionality for code blocks" do
          assert_match(/<pre dir="ltr".*<code/, @html_body)
        end
      end

      describe "in RTL languages" do
        let(:hebrew) { translation_locales(:hebrew) }

        before do
          @agent.update_attributes!(locale_id: hebrew.id)

          Comment.any_instance.stubs(:process_as_markdown?).returns(true)
          EventsMailer.deliver_rule(@notification, @ticket, [@agent], true)

          @email = ActionMailer::Base.deliveries.last
          @html_body = body_by_content_type(@email, Mime[:html])
        end

        it "does not auto link markdown CSS" do
          assert @html_body !~ /color: \<a target/
        end

        it "includes the proper directionality for blockquotes" do
          assert_includes @html_body, '<blockquote dir="rtl"'
        end

        it "ensures that code blocks remain LTR" do
          assert_match(/<pre dir="ltr".*<code/, @html_body)
        end
      end
    end
  end

  describe "notifications" do
    before do
      @ticket = tickets(:minimum_1)
      @user = users(:minimum_end_user)
      @agent = users(:minimum_agent)
      @account = accounts(:minimum)
    end

    describe "containing dynamic content" do
      before do
        create_dynamic_content!(@account, "foo", "p2\n\np3")

        @notification = Notification.new(
          subject: "Title",
          body: "p1\n\n{{dc.foo}}\n\np4",
          author: @agent,
          recipients: [@agent.email]
        )

        @ticket.will_be_saved_by(@agent)
        @ticket.audit.events << @notification
        @ticket.save!

        EventsMailer.deliver_rule(@notification, @ticket, [@agent], true)
        @mail = ActionMailer::Base.deliveries.last
      end

      it "does not simple format the dynamic content in text" do
        body_by_content_type(@mail, Mime[:text]).must_include "p1\n\np2\n\np3\n\np4"
      end

      describe_with_arturo_enabled(:email_notification_body_with_dir_auto_tags) do
        before do
          EventsMailer.deliver_rule(@notification, @ticket, [@agent], true)
          @mail = ActionMailer::Base.deliveries.last
        end

        it "simples format the dynamic content in html" do
          body_by_content_type(@mail, Mime[:html]).sub(/<script.*script>/, "").must_include "<p dir=\"ltr\">p1</p><p dir=\"ltr\">p2</p><p dir=\"ltr\">p3</p><p dir=\"ltr\">p4</p>"
        end
      end

      describe_with_arturo_disabled(:email_notification_body_with_dir_auto_tags) do
        before do
          EventsMailer.deliver_rule(@notification, @ticket, [@agent], true)
          @mail = ActionMailer::Base.deliveries.last
        end

        it "simples format the dynamic content in html" do
          body_by_content_type(@mail, Mime[:html]).sub(/<script.*script>/, "").must_include "<p>p1</p><p>p2</p><p>p3</p><p>p4</p>"
        end
      end
    end

    describe "defined with placeholders that also contain placeholders/DC" do
      before do
        create_dynamic_content!(@account, "foo", "bar")
        @ticket.subject = "{{ticket.status}} foo{{dc.foo}}"
        @notification = Notification.new(
          subject: "{{ticket.title}}",
          body: "this ticket subject has placeholders/ DC",
          author: @agent,
          recipients: [@agent.email]
        )

        @ticket.will_be_saved_by(@agent)
        @ticket.audit.events << @notification
        @ticket.save!

        EventsMailer.deliver_rule(@notification, @ticket, [@agent], true)

        @mail = ActionMailer::Base.deliveries.last
      end

      it "resolves both the placeholder and dymanic content in the subject" do
        @mail.subject.must_include "Open foobar"
      end
    end

    describe "defined with DC that needs to be truncated" do
      before do
        create_dynamic_content!(
          @account,
          "foo",
          "this is quite a long bit of dynamic content and shall      need to be truncated to an\s " \
          "appropriate number of characters to avoid becoming a bother especially since it would not " \
          "show the full subject in email clients as i'm sure they too would truncate it too"
        )
        @ticket.subject = "{{ticket.status}} foo{{dc.foo}}"
        @notification = Notification.new(
          subject: "{{ticket.title}}",
          body: "this ticket subject has placeholders/ DC",
          author: @agent,
          recipients: [@agent.email]
        )
        @ticket.will_be_saved_by(@agent)
        @ticket.audit.events << @notification
        @ticket.save!

        EventsMailer.deliver_rule(@notification, @ticket, [@agent], true)

        @mail = ActionMailer::Base.deliveries.last
      end

      it "truncates the DC to an appropriate length" do
        @mail.subject.must_include "i'm sure they too would tr..."
      end
    end

    describe "defined with placeholders that contains recursive DC" do
      before do
        create_dynamic_content!(@account, "foo", "{{dc.foo}}")
        @ticket.subject = "{{ticket.status}} foo{{dc.foo}}"
        @notification = Notification.new(
          subject: "{{ticket.title}}",
          body: "this ticket subject has recursive DC",
          author: @agent,
          recipients: [@agent.email]
        )
        @ticket.will_be_saved_by(@agent)
        @ticket.audit.events << @notification
        @ticket.save!

        EventsMailer.deliver_rule(@notification, @ticket, [@agent], true)

        @mail = ActionMailer::Base.deliveries.last
      end

      it "resolves the placeholder but not the DC in the subject" do
        @mail.subject.must_include "Open foo"
      end
    end

    describe "defined with a recursive placeholder" do
      before do
        Ticket.any_instance.stubs(:subject).returns("{{ticket.title}}")
        @notification = Notification.new(
          subject: "{{ticket.title}}",
          body: "this ticket's subject is silly",
          author: @agent,
          recipients: [@agent.email]
        )
        @ticket.will_be_saved_by(@agent)
        @ticket.audit.events << @notification
        @ticket.save!

        EventsMailer.deliver_rule(@notification, @ticket, [@agent], true)

        @mail = ActionMailer::Base.deliveries.last
      end

      it "retains the placeholder in the subject" do
        @mail.subject.must_include "{{ticket.title}}"
      end
    end

    describe "outbound email message IDs" do
      def deliver_notification(message_id, subject, body, other_time: nil, recipients: [@agent], for_agent: true)
        Timecop.freeze(other_time) if other_time
        EventsMailer.any_instance.stubs(:ticket_message_id).returns(message_id)

        notification = Notification.new(
          subject: subject,
          body: body,
          author: @agent,
          recipients: recipients.map(&:id)
        )
        @ticket.will_be_saved_by(@agent)
        @ticket.audit.events << notification
        @ticket.save!

        EventsMailer.deliver_rule(
          notification,
          @ticket,
          recipients,
          for_agent
        )

        Timecop.return if other_time

        notification
      end

      before do
        @in_reply_to_id = "in-reply-to id"
        EventsMailer.any_instance.stubs(:ticket_reply_to_id).returns(@in_reply_to_id)
      end

      it "saves the current message ID for the notification" do
        message_id = "sample message id"
        notification = deliver_notification(message_id, "Subject", "Body")
        notification.message_id.must_equal(message_id)
      end

      it "appends the recipient's most recent outbound message ID to In-Reply-To when there's one recipient" do
        previous_message_id = "previous message id"
        deliver_notification(previous_message_id, "Previous Subject", "Previous Body", other_time: 1.minute.ago)
        deliver_notification("message id", "Subject", "Body")

        @mail = ActionMailer::Base.deliveries.last
        @mail.in_reply_to.must_equal [@in_reply_to_id, previous_message_id].join(', ')
      end

      it "appends all recipients' most recent outbound message IDs to In-Reply-To in descending order when there are multiple recipients" do
        other_agent = users(:minimum_admin)
        previous_message_ids = (0..5).map do |num|
          message_id = "previous id #{num}"
          recipients = (num < 3) ? [@agent] : [other_agent]
          deliver_notification(
            message_id,
            "Previous Subject #{num}",
            "Previous Body #{num}",
            other_time: (6 - num).minutes.ago,
            recipients: recipients
          )
          message_id
        end

        deliver_notification("message id", "Subject", "Body", recipients: [@agent, other_agent])

        @mail = ActionMailer::Base.deliveries.last
        @mail.in_reply_to.must_equal [@in_reply_to_id, previous_message_ids[5], previous_message_ids[2]].join(', ')
      end

      it "does not append anything to the In-Reply-To header when there is no previous message for the current recipient" do
        other_agent = users(:minimum_admin)
        previous_message_id = "previous message id"
        deliver_notification(previous_message_id, "Previous Subject", "Previous Body", other_time: 1.minute.ago, recipients: [other_agent])

        deliver_notification("message id", "Subject", "Body")

        @mail = ActionMailer::Base.deliveries.last
        @mail.in_reply_to.must_equal @in_reply_to_id.to_s
      end

      it "does not append more than 9 previous outbound message IDs to the In-Reply-To header" do
        users = (0..9).map do |num|
          @account.users.create! do |user|
            user.name = "Minimum End User #{num}"
            user.email = "enduser#{num}@example.com"
            user.roles = Role::END_USER.id
          end
        end

        previous_message_ids = (0..9).map do |num|
          message_id = "previous id #{num}"
          deliver_notification(
            message_id,
            "Previous Subject #{num}",
            "Previous Body #{num}",
            other_time: (10 - num).minutes.ago,
            recipients: [users[num]],
            for_agent: false
          )
          message_id
        end

        deliver_notification("message id", "Subject", "Body", recipients: users, for_agent: false)

        @mail = ActionMailer::Base.deliveries.last
        @mail.in_reply_to.must_equal(([@in_reply_to_id] + previous_message_ids[1..9].reverse).join(', '))
      end
    end
  end

  private

  def create_dynamic_content!(account, name, value)
    Cms::Text.create!(
      name: name,
      account: account,
      fallback_attributes: {
        is_fallback: true,
        nested: true,
        value: value,
        translation_locale_id: ENGLISH_BY_ZENDESK.id
      }
    )
  end

  def with_docomo_destination
    @mailer.stubs(:to).returns(["test@test.com", "docomo08025940776@docomo.ne.jp", "test2@test.com"])
  end

  def create_trigger!(name, options)
    options[:conditions_all] ||= []
    options[:conditions_any] ||= []
    options[:actions] ||= options[:action] if options[:action]

    definition = Definition.new

    options[:conditions_all].each do |condition|
      definition.conditions_all << DefinitionItem.new(*condition)
    end

    options[:conditions_any].each do |condition|
      definition.conditions_any << DefinitionItem.new(*condition)
    end

    options[:actions].each do |action|
      definition.actions << DefinitionItem.new(action[0], nil, action[1])
    end

    trigger = @account.triggers.create!(title: name, definition: definition)
    Timecop.travel(Time.now + 1.seconds)
    trigger
  end

  def build_inline_and_appended_attachments(ticket, attachment)
    attachments = []
    attachments << ticket.comment.attachments.build(
      uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{attachment}"), attachment),
      account: ticket.account,
      author: @minimum_end_user,
      is_public: true
    )
    attachments << ticket.comment.attachments.build(
      uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{attachment}"), attachment),
      account: @account,
      author: @minimum_end_user,
      is_public: true
    ) do |a|
      a.inline = true
    end
  end
end
