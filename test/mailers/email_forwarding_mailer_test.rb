require_relative '../support/test_helper'
require_relative '../support/mailer_test_helper'

SingleCov.covered!

describe EmailForwardingMailer do
  include MailerTestHelper

  fixtures :accounts, :users, :brands, :translation_locales

  let(:spanish) { translation_locales(:spanish) }
  let(:hebrew) { translation_locales(:hebrew) }
  let(:account) { accounts(:minimum) }
  let(:admin) { users(:minimum_admin_not_owner) }

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries         = []

    brand = brands(:minimum)
    account.default_brand = brand
    account.save!
  end

  describe '#trial_limit_exceeded_notice' do
    describe 'when locale of user is LTR' do
      before do
        admin.translation_locale = spanish
        admin.save!
      end

      it 'sends email to user with proper content and formatting' do
        ActionMailer::Base.deliveries.clear
        EmailForwardingMailer.deliver_forwarding_complete_notice(admin, 'test@example.com')
        email = ActionMailer::Base.deliveries.last

        assert_match /Ahora está activado el reenvío de correo electrónico/, email.subject
        assert_match /se convertirá automáticamente en ticket en Zendesk Support./, email.parts.last.body.to_s
        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end
    end

    describe 'when locale of user is RTL' do
      before do
        admin.translation_locale = hebrew
        admin.save!
      end

      it 'adds RTL styling to email' do
        ActionMailer::Base.deliveries.clear
        EmailForwardingMailer.deliver_forwarding_complete_notice(admin, 'test@example.com')
        email = ActionMailer::Base.deliveries.last

        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end
end
