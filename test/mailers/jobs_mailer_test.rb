require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered!

describe JobsMailer do
  fixtures :accounts, :account_property_sets, :subscriptions, :users, :user_identities, :translation_locales

  let(:minimum_agent) { users(:minimum_agent) }
  let(:minimum_account) { accounts(:minimum) }
  let(:identifier) { "JobsMailerTest" }
  let(:subject) { "JobsMailerTest subject" }
  let(:content) { "JobsMailerTest content" }

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
  end

  describe "#job_complete_to_multiple_recipients" do
    let(:recipients) { [minimum_agent, users(:minimum_admin), users(:minimum_admin_not_owner)] }

    it "sends mail in batches" do
      JobsMailer.deliver_job_complete_to_multiple_recipients(recipients, identifier, subject, content)
      sent = ActionMailer::Base.deliveries.first
      assert_equal sent.to.sort, recipients.map(&:email).sort
      assert_equal "job-complete-#{identifier}", sent.header[Zendesk::OutgoingMail::Mailer::X_DELIVERY_CONTEXT].to_s
      assert_equal subject, sent.subject
      assert_includes sent.joined_bodies, content
    end

    describe "when translation_locale is provided" do
      let(:options) { { translation_locale: translation_locales(:hebrew) } }

      it "forwards the translation_locale when rendering the email" do
        Zendesk::Liquid::MailContext.expects(:render).
          with(has_key(:translation_locale)).
          with(has_value(translation_locales(:hebrew))).
          twice

        JobsMailer.deliver_job_complete_to_multiple_recipients(recipients, identifier, subject, content, options)
      end
    end
  end

  describe "#job_complete" do
    it "sends a job complete mail" do
      JobsMailer.deliver_job_complete(minimum_agent, identifier, subject, content)
      sent = ActionMailer::Base.deliveries.first
      assert sent.to.include?(minimum_agent.email)
      assert_equal "job-complete-#{identifier}", sent.header[Zendesk::OutgoingMail::Mailer::X_DELIVERY_CONTEXT].to_s
      assert_equal subject, sent.subject
      assert_includes sent.joined_bodies, content
    end

    # This test previously checked that a method wasn't called that should actually never be called because it was in a different
    # module. For the case where a user without an email is passed into this mailer, downstream code will fail because a mail object won't be created
    # due to the early return. This is bad, and should be dealt with in this JIRA: EM-2840
    it "does not send mail to users without emails" do
      user = minimum_account.users.new(name: "user noemail")
      user.save!
      assert_raises do
        JobsMailer.deliver_job_complete(user, identifier, subject, content)
      end
    end
  end

  describe "#job_notification" do
    it "sends a job notification mail" do
      JobsMailer.deliver_job_notification(minimum_agent.email, minimum_account, identifier, subject, content)
      sent = ActionMailer::Base.deliveries.first
      assert sent.to.include?(minimum_agent.email)
      assert_equal "job-notification-#{identifier}", sent.header[Zendesk::OutgoingMail::Mailer::X_DELIVERY_CONTEXT].to_s
      assert_equal subject, sent.subject
      assert_includes sent.joined_bodies, content
    end
  end
end
