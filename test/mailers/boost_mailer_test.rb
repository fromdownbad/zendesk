require_relative "../support/test_helper"

SingleCov.covered!

describe BoostMailer do
  fixtures :all

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    @account = accounts(:minimum)
  end

  describe "#deliver_boost_expiry" do
    let(:sent) { ActionMailer::Base.deliveries.last }

    before do
      BoostMailer.any_instance.expects(:read_only).times(5)
      @account.create_feature_boost(valid_until: 30.days.from_now, boost_level: SubscriptionPlanType.ExtraLarge)
    end

    it "delivers 5 day notice" do
      BoostMailer.deliver_boost_expiry(@account, 5)
      sent.subject.must_include "Your Zendesk Support free upgrade expires in 5 days"
    end

    it "delivers 1 day notice" do
      BoostMailer.deliver_boost_expiry(@account, 1)
      sent.subject.must_include "Your Zendesk Support free upgrade expires in 24 hours"
    end
  end
end
