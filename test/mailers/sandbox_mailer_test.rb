require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered! uncovered: 0

describe SandboxMailer do
  fixtures :all

  include MailerTestHelper

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:subdomain) { 'mysandboxaccount' }
  let(:to) { user.email }
  let(:agent_name) { 'Adam In' }
  let(:sent) do
    assert_equal 1, ActionMailer::Base.deliveries.size
    ActionMailer::Base.deliveries.first
  end

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries.clear
  end

  describe "#success_notification" do
    it "delivers" do
      SandboxMailer.deliver_success_notification(account, user, subdomain)

      assert_includes sent.subject, 'Your sandbox has finished copying'
      assert_includes sent.from, 'support@support.zendesk-test.com'
      assert_includes sent.to, to
      assert_equal('multipart/alternative', sent.mime_type)
    end
  end

  describe "#failure_notification" do
    it "delivers" do
      SandboxMailer.deliver_failure_notification(account, user)

      assert_includes sent.subject, "Your sandbox didn't complete"
      assert_includes sent.from, 'support@support.zendesk-test.com'
      assert_includes sent.to, to
      assert_equal('multipart/alternative', sent.mime_type)
    end
  end

  describe "#ready_notification" do
    it "delivers" do
      SandboxMailer.deliver_ready_notification(account, user, subdomain)

      assert_includes sent.subject, 'You can use your sandbox'
      assert_includes sent.from, 'support@support.zendesk-test.com'
      assert_includes sent.to, to
      assert_equal('multipart/alternative', sent.mime_type)
    end
  end
end
