require_relative "../support/test_helper"

SingleCov.covered!

describe AgreementMailer do
  fixtures :accounts, :users, :user_identities, :account_property_sets

  before { ActionMailer::Base.perform_deliveries = true }

  describe '#new_from_partner' do
    before do
      @account = accounts(:minimum)
      @agreement = FactoryBot.create(
        :agreement,
        account: @account,
        remote_admin: FactoryBot.create(:end_user, account: @account),
        name: 'other company'
      )
    end

    it 'delivers' do
      email = AgreementMailer.create_new_from_partner(@agreement)

      assert_equal('multipart/alternative', email.mime_type)
      assert_match(/https.*#{@account.host_name}.*agent\/admin\/tickets\/ticket_sharing\/#{@agreement.id}/, email.joined_bodies)
      assert_equal([@account.reply_address], email.from)
      assert_equal(@account.admins.map(&:email), email.to)
    end
  end

  describe '#status_change' do
    before do
      @account = accounts(:minimum)
    end

    describe 'Accepted email' do
      before do
        @agreement = FactoryBot.create(
          :agreement,
          account: @account,
          remote_admin: FactoryBot.create(:end_user, account: @account),
          name: 'other company',
          status: :accepted
        )
      end

      it 'delivers' do
        email = AgreementMailer.create_status_change(@agreement, "some dude")

        assert_equal('multipart/alternative', email.mime_type)
        assert_equal([@account.reply_address], email.from)
        assert_equal(@account.admins.map(&:email), email.to)
        assert_match(/https.*#{@account.host_name}.*agent\/admin\/tickets\/ticket_sharing\//, email.joined_bodies)
        assert_match(/accepted by some dude/, email.joined_bodies)
      end
    end

    it 'delivers declined email' do
      agreement = FactoryBot.create(
        :agreement,
        account: @account,
        remote_admin: FactoryBot.create(:end_user, account: @account),
        name: 'other company',
        status: :declined
      )

      email = AgreementMailer.create_status_change(agreement, "some dude")

      assert_equal('multipart/alternative', email.mime_type)
      assert_equal([@account.reply_address], email.from)
      assert_equal(@account.admins.map(&:email), email.to)
      assert_match(/https.*#{@account.host_name}.*agent\/admin\/tickets\/ticket_sharing\//, email.joined_bodies)
      assert_match(/declined by some dude/, email.joined_bodies)
    end
  end

  describe '#invite_revoked' do
    it 'delivers' do
      account = accounts(:minimum)
      agreement = FactoryBot.create(
        :agreement,
        account: account,
        remote_admin: FactoryBot.create(:end_user, account: account),
        name: 'other company'
      )

      email = AgreementMailer.create_invite_revoked(agreement)

      assert_equal('multipart/alternative', email.mime_type)
      assert_equal([account.reply_address], email.from)
      assert_equal(account.admins.map(&:email), email.to)
    end
  end

  describe '#activation_change' do
    it 'delivers' do
      account = accounts(:minimum)
      agreement = FactoryBot.create(
        :agreement,
        account: account,
        remote_admin: FactoryBot.create(:end_user, account: account),
        name: 'other company'
      )

      email = AgreementMailer.create_activation_change(agreement)

      assert_equal('multipart/alternative', email.mime_type)
      assert_equal([account.reply_address], email.from)
      assert_equal(account.admins.map(&:email), email.to)
    end
  end

  describe '#sharing_agreement_deleted' do
    it 'delivers' do
      account = accounts(:minimum)
      agreement = FactoryBot.create(
        :agreement,
        account: account,
        remote_admin: FactoryBot.create(:end_user, account: account),
        name: 'other company'
      )

      email = AgreementMailer.create_sharing_agreement_deleted(agreement)

      assert_equal('multipart/alternative', email.mime_type)
      assert_equal([account.reply_address], email.from)
      assert_equal(account.admins.map(&:email), email.to)
    end

    it 'has text and html content' do
      account = accounts(:minimum)
      agreement = FactoryBot.create(
        :agreement,
        account: account,
        remote_admin: FactoryBot.create(:end_user, account: account),
        name: 'other company'
      )

      email = AgreementMailer.create_sharing_agreement_deleted(agreement)
      assert_equal ["text/plain", "text/html"], email.parts.map(&:mime_type)
    end
  end
end
