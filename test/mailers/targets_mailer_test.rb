require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered!

describe TargetsMailer do
  fixtures :all

  include MailerTestHelper

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []

    @target = targets(:twitter_invalid)
    @unverified_admin = users(:minimum_admin_not_verified)
    @agent = users(:minimum_agent)
    @target.account.admins.first.identities << UserEmailIdentity.new(user: @agent, value: 'agent123@zendesk.com', is_verified: true)
    @target.account.admins.first.save!

    account = @target.account
    account.text_mail_template += "TEXT me"
    account.html_mail_template += "HTML you"
    account.save!
  end

  it "sends target disabled notice to all verified admins, with the right template and stripping used for text vs html parts" do
    TargetsMailer.deliver_target_disabled(@target)
    refute ActionMailer::Base.deliveries.empty?

    sent = ActionMailer::Base.deliveries.first
    path = "temporarily disabled"

    refute_equal @target.account.admins.verified.count, sent.bcc.count
    assert_equal @target.account.admins.verified.uniq.count, sent.bcc.count
    refute sent.bcc.include? @unverified_admin.email
    refute sent.bcc.include? @agent.email

    html_body = body_by_content_type(sent, Mime[:html])
    assert html_body.index(path)
    assert_match(/HTML you/, html_body)
    assert_match(/<p>.{100,}<\/p>/m, html_body)

    text_body = body_by_content_type(sent, Mime[:text])
    assert text_body.index(path)
    assert_match(/TEXT me/, text_body)
    assert_no_match(/<p>/, text_body)
  end

  describe "#target_message" do
    before do
      @event   = events(:create_external_for_minimum_ticket_1_and_minimum_author)
      @account = @event.ticket.account
      @ticket  = @event.ticket
      @ticket.add_comment body: "<hr /> woot! another comment!\n\nand some stuff after newlines\nENDCOMMENT"
      @ticket.will_be_saved_by(users(:minimum_author))
      @ticket.save!
      @message = "{{ticket.url}}"
    end

    it "has for_agent set to true for notifications to agents" do
      Zendesk::Liquid::TicketContext.expects(:render).
        with(@message, @ticket, @event.author, true, @account.translation_locale, Mime[:text].to_s).
        returns('foo')
      Zendesk::Liquid::TicketContext.expects(:render).
        with('{{ticket.url}}', @ticket, @event.author, true, @account.translation_locale, Mime[:html].to_s).
        returns('foo')
      Zendesk::Liquid::TicketContext.expects(:render).
        with('A subject', @event.ticket, @event.author, true, @account.translation_locale).
        returns('foo')
      TargetsMailer.deliver_target_message(@account, users(:minimum_admin).email, 'A subject', @message, @event)
    end

    it "contains branded footer and from address" do
      Brand.any_instance.stubs(:name).returns("Some brand")
      Brand.any_instance.stubs(:reply_address).returns("support@somebrand.com")
      TargetsMailer.deliver_target_message(@account, users(:minimum_end_user).email, 'A subject', @message, @event)
      email     = ActionMailer::Base.deliveries.last
      html_part = body_by_content_type(email, Mime[:html])
      text_part = body_by_content_type(email, Mime[:text])
      html_part.must_include "This email is a service from Some brand"
      text_part.must_include "This email is a service from Some brand"
      email.from.must_include "support@somebrand.com"
    end

    it "has for_agent set to false for notifications to end-users" do
      Zendesk::Liquid::TicketContext.expects(:render).
        with(@message, @ticket, @event.author, false, @account.translation_locale, Mime[:text].to_s).
        returns('foo')
      Zendesk::Liquid::TicketContext.expects(:render).
        with('{{ticket.url}}', @ticket, @event.author, false, @account.translation_locale, Mime[:html].to_s).
        returns('foo')
      Zendesk::Liquid::TicketContext.expects(:render).
        with('A subject', @event.ticket, @event.author, false, @account.translation_locale).
        returns('foo')
      TargetsMailer.deliver_target_message(@account, users(:minimum_end_user).email, 'A subject', @message, @event)
    end
  end

  it "properly escape target messages, using the right templates for text vs html parts" do
    event   = events(:create_external_for_minimum_ticket_1_and_minimum_author)
    ticket  = event.ticket
    ticket.add_comment body: "<hr /> woot! another comment!\n\nand some stuff after newlines\nENDCOMMENT"
    ticket.will_be_saved_by(users(:minimum_author))
    ticket.save!
    message = "I'm a little teapot. I have escaped teapot HTML here: <hr /><div>HI!</div>. " \
      "My formatted and not-HTML-escaped comments are: {{ticket.comments_formatted}}"
    TargetsMailer.deliver_target_message(event.ticket.account, users(:minimum_admin).email, 'A subject', message, event)

    email     = ActionMailer::Base.deliveries.last
    html_part = body_by_content_type(email, Mime[:html])
    text_part = body_by_content_type(email, Mime[:text])

    # The agent-supplied liquid template should have unescaped HTML elements
    html_part.must_include "<hr /><div>HI!</div>."
    # The comment contents should be escaped
    assert_match /&lt;hr \/&gt; woot! another comment!/, html_part

    # The agent-supplied liquid template should have plain text
    text_part.must_include "HI!"
    # The comment contents should be present and in plain text
    text_part.must_include "woot! another comment!"
  end

  it "properly resolve liquid in the account language" do
    account = accounts(:minimum)
    spanish = translation_locales(:spanish)
    account.translation_locale = spanish

    subject = 'Asunto de ticket con prioridad {{ticket.priority}}'
    message = 'Mensaje de ticket con prioridad {{ticket.priority}}'
    event   = events(:create_external_for_minimum_ticket_1_and_minimum_author)

    TargetsMailer.deliver_target_message(account, users(:minimum_admin).email, subject, message, event)

    email     = ActionMailer::Base.deliveries.last
    html_part = body_by_content_type(email, Mime[:html])
    text_part = body_by_content_type(email, Mime[:text])

    html_part.must_include "Asunto de ticket con prioridad Normal"
    html_part.must_include "Mensaje de ticket con prioridad Normal"
    text_part.must_include "Mensaje de ticket con prioridad Normal"
  end

  it "properly escape target messages, using the right templates for text vs html parts (when message is nil)" do
    event   = events(:create_external_for_minimum_ticket_1_and_minimum_author)
    ticket  = event.ticket
    ticket.add_comment body: "<hr /> woot! another comment!\n\nand some stuff after newlines\nENDCOMMENT"
    ticket.will_be_saved_by(users(:minimum_author))
    ticket.save!
    message = nil
    TargetsMailer.deliver_target_message(event.ticket.account, users(:minimum_admin).email, 'A subject', message, event)

    email     = ActionMailer::Base.deliveries.last
    html_part = body_by_content_type(email, Mime[:html])
    text_part = body_by_content_type(email, Mime[:text])

    # The agent-supplied liquid template should have unescaped HTML elements
    html_part.must_include "<hr /><div>HI!</div>."
    # The comment contents should be escaped
    html_part.must_include "&lt;hr /&gt; woot! another comment!"

    # The agent-supplied liquid template should have plain text
    text_part.must_include "HI!"
    # The comment contents should be present and in plain text
    text_part.must_include " woot! another comment!"
  end
end
