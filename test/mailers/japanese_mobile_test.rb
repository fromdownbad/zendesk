require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.not_covered! # covers integration with code in outgoing_mail

describe 'japanese mobile encoding' do
  fixtures :accounts, :account_property_sets, :users

  include MailerTestHelper

  class JapaneseMockMailer < ApplicationMailer
    def simple_message
      set_headers Account.last
      mail(
        from: 'support@support.zendesk.com',
        to: ['recipient@example.com', 'recipient2@example.com'],
        cc: ['cc@example.com', 'cc2@example.com'],
        bcc: ['bcc@example.com', 'bcc2@example.com'],
        body: 'Test'
      )
    end

    # ActionMailer overrides the new method to return nil.
    def self.method_missing(method_id, *args)
      method_id == :new ? new : super
    end
  end

  describe ApplicationMailer::JapaneseMobile do
    before do
      @account = accounts(:minimum)
      Arturo.enable_feature!(:japanese_mobile_no_html_in_email)
      @mail = ::Mail.new
      @mailer = ApplicationMailer::JapaneseMobile.new(@mail, @account)
    end

    it "does not recognize mail to destinations that aren't japanese mobile devices" do
      ['not_mobile@example.jp', '<not_mobile@example.jp>'].each do |not_mobile|
        @mail.to = not_mobile
        refute @mailer.send(:matches?), "Should not have matched on #{not_mobile}"
      end
    end

    it "recognizes mail being sent to a japanese mobile device" do
      mobile_addresses = [
        'docomo@docomo.ne.jp', 'au@ezweb.ne.jp', 'vodafone@t.vodafone.ne.jp',
        'willcom@willcom.com', 'pdx@dk.pdx.ne.jp', 'emobile@emnet.ne.jp'
      ]

      mobile_addresses.each do |mobile_address|
        @mail.to = mobile_address
        assert @mailer.send(:matches?), "Failed to match on #{mobile_address}"
      end
    end

    it "supports valid address styles" do
      mobile_addresses = ['Angle Brackets <docomo@docomo.ne.jp>', '"Quoted Angle Brackets" <docomo@docomo.ne.jp>']
      mobile_addresses.each do |mobile_address|
        @mail.to = mobile_address
        assert @mailer.send(:matches?), "Failed to match on #{mobile_address}"
      end

      @mail.to = users(:minimum_agent).to_s
      refute @mailer.send(:matches?)
    end
  end

  describe "Messages to Japanese mobile devices" do
    before do
      ActionMailer::Base.perform_deliveries = true
      @account = accounts(:minimum)
      Arturo.enable_feature!(:japanese_mobile_no_html_in_email)
      sender = 'sender@example.com'
      recipient = 'mobile@docomo.ne.jp'
      title     = 'バックアップテープの件'
      body      = 'hello .... バックアップテープの件 バックアップテープの件'
      ApplicationMailer.deliver_generic(@account, sender, recipient, title, body)
      @mail = ApplicationMailer.deliveries.last
    end

    describe "destinations" do
      before do
        @mail = JapaneseMockMailer.simple_message.create
      end

      it "includes all recipients, CCs, and BCCs" do
        all_destinations = [
          'recipient@example.com', 'recipient2@example.com', 'cc@example.com',
          'cc2@example.com', 'bcc@example.com', 'bcc2@example.com'
        ]
        assert_equal all_destinations, @mail.send(:destinations)
      end
    end

    it "only include plain text parts" do
      assert_equal 0, @mail.parts.size
      assert_equal "text/plain; charset=iso-2022-jp", @mail.content_type
      assert_includes @mail.body.to_s, "hello .... "
      refute_includes @mail.encoded, "mimepart" # no parts, just plain text
    end

    it "declares 7bit transfer encoding" do
      assert_equal '7bit', @mail.content_transfer_encoding
    end

    it "declares its charset as iso-2022-jp" do
      assert_equal 'iso-2022-jp', @mail.charset
    end

    it "encodes the body in iso-2022-jp" do
      encoded_body = "hello .... \e$B%P%C%/%\"%C%W%F!<%W$N7o\e(B \e$B%P%C%/%\"%C%W%F!<%W$N7o\e(B\r\n\r\n" \
        "--------------------------------\r\nThis email is a service from Minimum account"
      assert_includes @mail.encoded, encoded_body
    end

    it "encodes the subject in base64 iso-2022-jp" do
      encoded_subject = "Subject: =?ISO-2022-JP?B?GyRCJVAlQyUvJSIlQyVXJUYhPCVXJE43bxsoQg==?="
      assert_includes @mail.encoded, encoded_subject
    end

    if defined?(Encoding)
      it "does not blow up on characters that do not map to iso-2022-jp" do
        class UndefinedConversionMailer < ApplicationMailer
          def undef(account)
            set_headers(account, X_DELIVERY_CONTEXT => "generic")

            mail(
              from: 'sender@example.com',
              to: 'mobile@docomo.ne.jp',
              subject: 'title'
            ) do |format|
              format.text { "\xE3".force_encoding(Encoding::ASCII_8BIT) }
              format.html { "\xE3".force_encoding(Encoding::ASCII_8BIT) }
            end
          end
        end

        # does not raise an excaption
        UndefinedConversionMailer.deliver_undef(@account)
      end
    end

    # should_eventually "encode all addresses in base64 encoded iso-2022-jp" do
    #  puts @mail.encoded
    # end
  end

  describe "Messages not for Japanese mobile phones" do
    before do
      ActionMailer::Base.perform_deliveries = true
      account = accounts(:minimum)
      sender = 'sender@example.com'
      recipient = 'not_mobile@example.jp'
      title     = 'バックアップテープの件'
      body      = 'hello .... バックアップテープの件 バックアップテープの件'

      ApplicationMailer.deliver_generic(account, sender, recipient, title, body)
      @mail = ApplicationMailer.deliveries.last
    end

    it "is UTF-8 encoded" do
      assert_equal 'utf-8', @mail.parts.first.charset.downcase
    end

    it "includes non-plain text parts" do
      assert @mail.parts.any? { |part| part.mime_type == 'text/html' }
    end
  end
end
