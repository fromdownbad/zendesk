require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered!

describe SuspendedTicketsMailer do
  include MailerTestHelper

  fixtures :accounts, :users, :suspended_tickets

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    @account = accounts(:minimum)
    @account.create_suspended_ticket_notification(email_list: "elefant@example.org giraf@example.com")
  end

  it "generates suspended ticket notifications" do
    SuspendedTicketsMailer.deliver_suspended_notification(@account, [suspended_tickets(:minimum_spam), suspended_tickets(:minimum_unverified)], 2)
    refute ActionMailer::Base.deliveries.empty?
    email = ActionMailer::Base.deliveries.last

    assert_equal ["elefant@example.org", "giraf@example.com"], email.to
    assert_equal "Your Zendesk \"Minimum account\" has 2 new suspended ticket(s)", email.subject
    assert_match /Since last check, about 2 suspended tickets have been created/, body_by_content_type(email, Mime::TEXT)
    assert_match /ince last check, about 2 suspended tickets have been created/, body_by_content_type(email, Mime::HTML)
  end

  describe "Suspended ticket URL" do
    it "points to lotus" do
      SuspendedTicketsMailer.deliver_suspended_notification(@account, [suspended_tickets(:minimum_spam), suspended_tickets(:minimum_unverified)], 2)
      email = ActionMailer::Base.deliveries.last
      email.joined_bodies.must_include "<p><a href=\"https://minimum.zendesk-test.com/agent/filters/suspended\"> Show me our suspended tickets</a></p>"
    end
  end
end
