require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 6

describe SecurityNotifications::UserProfileChanged do
  fixtures :users, :accounts, :translation_locales, :subscriptions, :subscription_features

  let(:spanish) { translation_locales(:spanish) }
  let(:hebrew) { translation_locales(:hebrew) }
  let(:minimum_agent) { users(:minimum_agent) }

  let(:minimum_admin) do
    user = users(:minimum_admin)
    user.update_attributes!(locale_id: locale.id)
    assert_equal locale.locale, user.locale
    user
  end

  let(:end_user) do
    user = users(:minimum_end_user)
    user.update_attributes!(locale_id: locale.id)
    assert_equal locale.locale, user.locale
    user
  end

  before do
    account = accounts(:minimum)
    account.subscription.update_attributes!(plan_type: SubscriptionPlanType.Large)
    Zendesk::Features::SubscriptionFeatureService.new(account).execute!
    account.reload

    account.update_attributes!(allowed_translation_locale_ids: [spanish.id, hebrew.id])
    assert account.available_languages.include?(spanish)
    assert account.available_languages.include?(hebrew)
  end

  describe '#profile_crypted_password_changed' do
    describe 'when locale of user is LTR' do
      let(:locale) { spanish }

      it 'adds LTR styling to email' do
        email = SecurityMailer.create_profile_crypted_password_changed(end_user, minimum_admin)
        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end
    end

    describe 'when locale of user is RTL' do
      let(:locale) { hebrew }

      it 'adds RTL styling to email' do
        email = SecurityMailer.create_profile_crypted_password_changed(end_user, minimum_admin)
        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end

  describe '#profile_admin_user_added_v2' do
    describe 'when locale of user is LTR' do
      let(:locale) { spanish }

      it 'adds LTR styling to email' do
        email = SecurityMailer.create_profile_admin_user_added_v2(end_user, 'test@test.com', locale, minimum_agent)
        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end
    end

    describe 'when locale of user is RTL' do
      let(:locale) { hebrew }

      it 'adds RTL styling to email' do
        email = SecurityMailer.create_profile_admin_user_added_v2(end_user, 'test@test.com', locale, minimum_agent)
        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end

  describe '#profile_admin_user_added' do
    describe 'when locale of user is LTR' do
      let(:locale) { spanish }

      it 'adds LTR styling to email' do
        email = SecurityMailer.create_profile_admin_user_added(minimum_admin, end_user, 'test@test.com', minimum_agent)
        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end
    end

    describe 'when locale of user is RTL' do
      let(:locale) { hebrew }

      it 'adds RTL styling to email' do
        email = SecurityMailer.create_profile_admin_user_added(minimum_admin, end_user, 'test@test.com', minimum_agent)
        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end

  describe '#profile_two_factor_auth_enabled' do
    describe 'when locale of user is LTR' do
      let(:locale) { spanish }

      it 'adds LTR styling to email' do
        email = SecurityMailer.create_profile_two_factor_auth_enabled(minimum_admin)
        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end
    end

    describe 'when locale of user is RTL' do
      let(:locale) { hebrew }

      it 'adds RTL styling to email' do
        email = SecurityMailer.create_profile_two_factor_auth_enabled(minimum_admin)
        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end

  describe '#profile_two_factor_auth_disabled' do
    describe 'when locale of user is LTR' do
      let(:locale) { spanish }

      it 'adds LTR styling to email' do
        email = SecurityMailer.create_profile_two_factor_auth_disabled(minimum_admin)
        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end

      it 'adds a link to the settings page' do
        email = SecurityMailer.create_profile_two_factor_auth_disabled(minimum_admin)
        assert_match /\/agent\/users\/\d{1,}\/security_settings/, email.parts.last.body.to_s
      end
    end

    describe 'when locale of user is RTL' do
      let(:locale) { hebrew }

      it 'adds RTL styling to email' do
        email = SecurityMailer.create_profile_two_factor_auth_disabled(minimum_admin)
        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end
end
