require_relative "../../support/test_helper"

SingleCov.covered!

describe SecurityNotifications::UserProfileChanged do
  fixtures :users, :accounts, :translation_locales

  let(:spanish) { translation_locales(:spanish) }
  let(:hebrew) { translation_locales(:hebrew) }

  let(:minimum_admin) do
    user = users(:minimum_admin)
    user.update_attributes!(locale_id: locale.id)
    assert_equal locale.locale, user.locale
    user
  end

  let(:end_user) do
    user = users(:minimum_end_user)
    user.update_attributes!(locale_id: locale.id)
    assert_equal locale.locale, user.locale
    user
  end

  before do
    account = accounts(:minimum)
    account.subscription.update_attributes!(plan_type: SubscriptionPlanType.Large)
    Zendesk::Features::SubscriptionFeatureService.new(account).execute!
    account.reload

    account.update_attributes!(allowed_translation_locale_ids: [spanish.id, hebrew.id])
    assert account.available_languages.include?(spanish)
    assert account.available_languages.include?(hebrew)
  end

  describe '#user_identity_added' do
    describe 'when locale of user is LTR' do
      let(:locale) { spanish }

      it 'translates the subject, content, and footer properly' do
        email = SecurityMailer.create_user_identity_added(end_user, minimum_admin, identity_type: 'facebook', identity_value: '1')
        assert_match /La cuenta de Facebook fue agregada a su cuenta de Zendesk/, email.parts.last.body.to_s
        assert_match /Se agregó la cuenta de Facebook/, email.parts.last.body.to_s

        # TODO: Refactor the #footer method in application_mailer.rb to use I18n.translation_locale,
        # which should be set at the individual mailer method using I18n.translation_locale.
        # For more info, see: https://zendesk.atlassian.net/browse/LOCAL-1721
        assert_match /This email is a service from Minimum account./, email.parts.last.body.to_s
      end

      it 'adds LTR styling to email' do
        email = SecurityMailer.create_user_identity_added(end_user, minimum_admin, identity_type: 'facebook', identity_value: '1')
        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end
    end

    describe 'when locale of user is RTL' do
      let(:locale) { hebrew }

      it 'adds RTL styling to email' do
        email = SecurityMailer.create_user_identity_added(end_user, minimum_admin, identity_type: 'facebook', identity_value: '1')
        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end

  describe '#user_identity_removed' do
    describe 'when locale of user is LTR' do
      let(:locale) { spanish }

      it 'translates the subject and content properly' do
        email = SecurityMailer.create_user_identity_removed(end_user, minimum_admin, identity_type: 'facebook', identity_value: '1')
        assert_match /La cuenta de Facebook fue eliminada de su cuenta de Zendesk/, email.parts.last.body.to_s
        assert_match /Se eliminó la cuenta de Facebook/, email.parts.last.body.to_s
      end

      it 'adds LTR styling to email' do
        email = SecurityMailer.create_user_identity_removed(end_user, minimum_admin, identity_type: 'facebook', identity_value: '1')
        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end
    end

    describe 'when locale of user is RTL' do
      let(:locale) { hebrew }

      it 'adds RTL styling to email' do
        email = SecurityMailer.create_user_identity_removed(end_user, minimum_admin, identity_type: 'facebook', identity_value: '1')
        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end

  describe '#user_primary_email_changed' do
    describe 'when locale of user is LTR' do
      let(:locale) { spanish }

      it 'translates the subject and content properly' do
        email = SecurityMailer.create_user_primary_email_changed(end_user, minimum_admin, identity_type: 'facebook', identity_value: '1')
        assert_match /La dirección de correo electrónico principal de su cuenta de Zendesk cambió/, email.parts.last.body.to_s
        assert_match /Tenemos el agrado de comunicarle que su perfil de usuario fue actualizado/, email.parts.last.body.to_s
      end

      it 'adds LTR styling to email' do
        email = SecurityMailer.create_user_primary_email_changed(end_user, minimum_admin, identity_type: 'facebook', identity_value: '1')
        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end

      describe 'when the account is multiproduct' do
        before do
          end_user.account.stubs(:multiproduct?).returns(true)
        end

        it 'sets the profile URL as a Central Admin one' do
          email = SecurityMailer.create_user_primary_email_changed(end_user, minimum_admin, identity_type: 'facebook', identity_value: '1')
          assert_match /#{end_user.account.url}\/admin\/staff\/#{end_user.id}/, email.parts.last.body.to_s
        end
      end
    end

    describe 'when locale of user is RTL' do
      let(:locale) { hebrew }

      it 'adds RTL styling to email' do
        email = SecurityMailer.create_user_primary_email_changed(end_user, minimum_admin, identity_type: 'facebook', identity_value: '1')
        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end
end
