require_relative "../../support/test_helper"
require_relative "../../support/mailer_test_helper"

SingleCov.covered!

describe SecurityNotifications::AccountAssumption do
  include MailerTestHelper

  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:admin_recipients) do
    [
      "minimum_admin@aghassipour.com",
      "minimum_admin_not_owner@aghassipour.com"
    ]
  end

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries         = []
    SecurityMailer.deliver_assumption_bypass(account)

    @email = ActionMailer::Base.deliveries.last
  end

  it "has delivered the notification" do
    refute_empty ActionMailer::Base.deliveries
  end

  it "has been sent to the owner" do
    assert_equal admin_recipients, @email.bcc
  end

  it "has the bypass assumption subject line" do
    @email.subject.must_include I18n.t('txt.email.account_assumption.assumption_bypass.subject')
  end

  it "mentions the nature of the notification in the body text" do
    @email.text_part.body.decoded.must_include I18n.t('txt.email.account_assumption.assumption_bypass.content')
  end
end
