require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered!

describe SecurityPolicyMailer do
  fixtures :accounts, :users, :account_property_sets, :user_identities, :groups, :translation_locales, :organizations

  include MailerTestHelper

  let(:spanish) { translation_locales(:spanish) }

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    @account = accounts(:minimum)
    @user    = users(:minimum_agent)

    @account.text_mail_template += 'I am TEXT hear me roar'
    @account.html_mail_template += 'I am HTML hear me render ugly flashing text'
    @account.save!

    @user.update_attributes!(locale_id: spanish.id)
    User.any_instance.stubs(:account_is_serviceable).returns(true)
  end

  describe 'with :new_password_expired_email Arturo feature off' do
    before { Arturo.disable_feature! :new_password_expired_email }

    it "delivers expiring password warnings to users" do
      User.any_instance.stubs(:password_expires_at).returns(5.days.from_now)
      SecurityPolicyMailer.deliver_expiring_password_warning(@user)
      mail = ActionMailer::Base.deliveries.last
      assert mail

      # English: "password expiring"
      mail.subject.must_include "La contraseña está por vencer"

      assert_equal 'support@zdtestlocalhost.com',         mail.from.join
      assert_equal 'minimum_agent@aghassipour.com',       mail.to.join
      mail.joined_bodies.must_include "https://minimum.zendesk-test.com"

      # English: "password expires in 5 days."
      mail.joined_bodies.must_include "Su contraseña actual vencerá en 5 días."

      User.any_instance.stubs(:password_expires_at).returns(23.hours.from_now)
      SecurityPolicyMailer.deliver_expiring_password_warning(@user)

      mail = ActionMailer::Base.deliveries.last

      text_body = body_by_content_type(mail, Mime[:text])
      text_body.must_include "I am TEXT hear me roar"
      assert_no_match /HTML/, text_body

      # English: "password expires in 5 days."
      text_body.must_include "contraseña actual vence hoy"

      assert_no_match /<a href=\"/, text_body

      html_body = body_by_content_type(mail, Mime[:html])
      html_body.must_include "I am HTML hear me render ugly flashing text"
      assert_no_match /TEXT/, html_body

      # English: "password expires in 5 days."
      html_body.must_include "contraseña actual vence hoy"

      assert_match /<a href=\"/, html_body
    end
  end

  describe 'with :new_password_expired_email Arturo feature on' do
    before { Arturo.enable_feature! :new_password_expired_email }

    it "delivers expiring password warnings to users" do
      User.any_instance.stubs(:password_expires_at).returns(5.days.from_now)
      SecurityPolicyMailer.deliver_expiring_password_warning(@user)
      mail = ActionMailer::Base.deliveries.last
      assert mail

      # English: "password expiring"
      mail.subject.must_include "La contraseña está por vencer"

      assert_equal 'support@zdtestlocalhost.com',         mail.from.join
      assert_equal 'minimum_agent@aghassipour.com',       mail.to.join
      mail.joined_bodies.must_include "https://minimum.zendesk-test.com"

      # English: "password expires in 5 days."
      mail.joined_bodies.must_include "contraseña vencerá en 5 días."

      # English: "Please sign in to our support site as soon as possible to change your password."
      mail.joined_bodies.must_include "Inicie sesión en nuestro sitio de soporte lo antes posible para cambiar su contraseña."

      # English: "Once you sign in, you can change your password"
      mail.joined_bodies.must_include "Una vez que haya iniciado sesión, puede cambiar su contraseña"
      mail.joined_bodies.must_include "https://minimum.zendesk-test.com/agent/users/#{@user.id}/security_settings"

      User.any_instance.stubs(:password_expires_at).returns(23.hours.from_now)
      SecurityPolicyMailer.deliver_expiring_password_warning(@user)

      mail = ActionMailer::Base.deliveries.last

      text_body = body_by_content_type(mail, Mime[:text])
      text_body.must_include "I am TEXT hear me roar"
      assert_no_match /HTML/, text_body

      # English: "Your current password expires today."
      text_body.must_include "Su contraseña actual vence hoy."

      assert_no_match /<a href=\"/, text_body

      html_body = body_by_content_type(mail, Mime[:html])
      html_body.must_include "I am HTML hear me render ugly flashing text"
      assert_no_match /TEXT/, html_body

      # English: "Your current password expires today."
      html_body.must_include "Su contraseña actual vence hoy."

      assert_match /<a href=\"/, html_body
    end
  end

  it "delivers expire password warnings to users in their set locale(subject and footer)" do
    User.any_instance.stubs(:password_expires_at).returns(23.hours.from_now)
    Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
    Account.any_instance.stubs(:available_languages).returns([translation_locales(:japanese), translation_locales(:english_by_zendesk)])
    @user = users(:minimum_end_user)
    @user.organization_id = organizations(:minimum_organization1).id
    @user.locale_id = translation_locales(:japanese).id
    @user.save!

    SecurityPolicyMailer.deliver_expiring_password_warning(@user)

    mail = ActionMailer::Base.deliveries.last
    assert_match /パースワド エクスパイア/, force_utf_8(mail.subject)
    text_body = body_by_content_type(mail, Mime[:text])
    assert_match /日本語フター/, force_utf_8(text_body)
  end

  it "delivers high security policy warning to account owner" do
    @account.owner.update_attributes!(locale_id: spanish.id)
    SecurityPolicyMailer.deliver_high_security_policy_warning(@account)
    mail = ActionMailer::Base.deliveries.last

    assert mail

    # English: "Your account's password security level is currently set to High"
    mail.subject.must_include "Advertencia de política de seguridad alta"

    assert_equal 'support@support.zendesk-test.com', mail.from.join
    assert_equal @account.owner.email, mail.to.join
    mail.joined_bodies.must_include "https://minimum.zendesk-test.com/settings/security#password_policy"

    # English: "Your account's password security level is currently set to High"
    mail.joined_bodies.must_include "El nivel de seguridad de la contraseña de su cuenta es actualmente Alto"
  end
end
