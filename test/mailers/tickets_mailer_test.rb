require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered!

describe TicketsMailer do
  include MailerTestHelper

  fixtures :accounts, :users, :user_identities, :tickets, :translation_locales, :account_property_sets, :groups

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    @minimum_1_ticket = tickets(:minimum_1)
    @minimum_end_user = users(:minimum_end_user)
    @minimum_agent = users(:minimum_agent)
    @minimum_account = accounts(:minimum)
    @translation = translation_locales(:japanese)
    @new_notification = Notification.new(
      subject: '{{current_user.name}} has updated {{ticket.title}}',
      body: '{{ticket.url}}',
      author: @minimum_agent
    )
    @minimum_account.text_mail_template += 'With great text comes great responsibility'
    @minimum_account.html_mail_template += 'By the power of HTML'
    @minimum_account.save!
  end

  it "generates closed_bounce email" do
    TicketsMailer.deliver_closed_bounce(@minimum_account, @minimum_agent.email, @minimum_1_ticket.subject, @minimum_1_ticket.id)
    refute ActionMailer::Base.deliveries.empty?

    email = ActionMailer::Base.deliveries.last
    email.subject.must_include "Update on closed request failed:"
    body_by_content_type(email, Mime[:text]).must_include "You tried to update a request that has been closed. Please submit a new request"
    body_by_content_type(email, Mime[:html]).must_include "You tried to update a request that has been closed. Please submit a new request"
  end

  it "generates support_bounce email" do
    TicketsMailer.deliver_support_bounce(@minimum_account, @minimum_agent.email)
    refute ActionMailer::Base.deliveries.empty?

    email = ActionMailer::Base.deliveries.last
    email.subject.must_include "Please find help at the Twitter support site"
    warning = "Twitter is no longer accepting email at this address. If you need help using Twitter, visit"
    body_by_content_type(email, Mime[:text]).must_include warning
    body_by_content_type(email, Mime[:html]).must_include warning
  end
end
