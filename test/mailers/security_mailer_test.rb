require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"
require "timecop"

SingleCov.covered!

describe SecurityMailer do
  extend ArturoTestHelper

  include MailerTestHelper

  fixtures :all

  let(:user) { users(:minimum_agent) }
  let(:actor) { users(:minimum_agent) }
  let(:account) { accounts(:minimum) }
  let(:email) { ActionMailer::Base.deliveries.last }
  let(:spanish) { translation_locales(:spanish) }
  let(:hebrew) { translation_locales(:hebrew) }

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries         = []

    CIA.stubs(:current_actor).returns(actor)

    @owner_recipients = [
      "minimum_admin@aghassipour.com"
    ]
    @admin_recipients = [
      "minimum_admin@aghassipour.com",
      "minimum_admin_not_owner@aghassipour.com"
    ]
    Timecop.freeze('2016-01-01')

    Account.any_instance.stubs(has_email_suppress_email_identity_update_notifications?: false)
  end

  describe 'for a shell account without support' do
    let(:account) { accounts(:shell_account_without_support) }
    let(:user) { FactoryBot.create(:user, account: account, skip_verification: false) }

    describe SecurityNotifications::UserProfileChanged do
      describe '#profile_crypted_password_changed' do
        describe 'for a staff' do
          before do
            CIA.stubs(:current_actor).returns(user)

            user.change_password!('secret')
          end

          it 'has delivered the email notification' do
            refute_empty ActionMailer::Base.deliveries
          end

          it 'generates password changed email notification with the appropriate subject' do
            assert_equal 'User profile updated: password changed', email.subject
          end

          it 'should come from the default auth email' do
            assert_equal 'noreply@shellaccountwithoutsupport.zendesk-test.com', email.from.first
          end

          it 'has been sent to the user' do
            assert_equal user.email, email.to.first
          end

          it 'generates password changed email notification with the appropriate body' do
            email.text_part.body.decoded.must_include 'Your password was changed'
          end

          it 'indicates who made the change' do
            email.text_part.body.decoded.must_include 'updated by you'
          end

          it 'shows the sign-in to the auth domain' do
            expected = 'You can sign in at: https://shellaccountwithoutsupport.zendesk-test.com/login'
            email.text_part.body.decoded.must_match(expected)
          end
        end
      end
    end
  end

  describe SecurityNotifications::AccountSettingsChanged do
    let(:user) { users(:minimum_admin) }
    let(:account) do
      a = accounts(:minimum)
      a.texts.ip_restriction = nil
      a.api_tokens.destroy_all
      a.settings.api_token_access    = nil
      a.settings.api_password_access = nil
      a.save!
      a
    end

    before do
      Arturo.enable_feature!(:security_email_notifications_for_account_settings)
      ActionMailer::Base.deliveries.clear
    end

    describe "#ip_restriction_changed" do
      before do
        account.texts.ip_restriction = 'localhost'
        account.translation_locale = spanish
        account.save!
      end

      it "has delivered the notification" do
        refute_empty ActionMailer::Base.deliveries
      end

      it "has been sent to the appropriate security notifications recipients" do
        assert_equal @admin_recipients, email.bcc
      end

      it "has the API settings change subject line" do
        # English: "IP Restrictions changed"
        email.subject.must_include "Cambiaron las restricciones de IP"
      end

      it "indicates the nature of change in the body text" do
        # English: "Change: Updates to your IP restriction settings"
        email.text_part.body.decoded.must_include "Cambio: Se actualizó la configuración de restricción de IP"
      end

      it "indicates who made the change in the body text" do
        # English: "Performed by: Agent Minimum"
        email.text_part.body.decoded.must_include "Realizado por: Agent Minimum"
      end

      it 'uses the proper formatting for LTR content' do
        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end

      describe "when the ip restriction field is not blank" do
        before do
          account.texts.ip_restriction = 'www.xxx.yyy.zzz'
          account.save!
        end

        describe "and a new value was defined" do
          before do
            account.texts.ip_restriction = 'zzz.yyy.xxx.www'
            account.save!
          end

          it "indicates the previous value" do
            # English: "Previous accepted IP range: www.xxx.yyy.zzz"
            email.text_part.body.decoded.must_include "Intervalo IP aceptado anterior: www.xxx.yyy.zzz"
          end

          it "indicates the new value" do
            # English: "New accepted IP range: zzz.yyy.xxx.www"
            email.text_part.body.decoded.must_include "Nuevo intervalo IP aceptado: zzz.yyy.xxx.www"
          end
        end
      end

      describe "when the ip restriction field is blank" do
        before do
          account.texts.ip_restriction = [nil, ''].sample
          account.save!
        end

        describe "and a new value was defined" do
          before do
            account.texts.ip_restriction = 'localhost'
            account.save!
          end

          it "indicates the previous value was blank" do
            # English: "Previous accepted IP range: (blank)"
            email.text_part.body.decoded.must_include "Intervalo IP aceptado anterior: (blank)"
          end

          it "indicates the new value" do
            # English: "New accepted IP range: localhost"
            email.text_part.body.decoded.must_include "Nuevo intervalo IP aceptado: localhost"
          end
        end
      end

      describe 'when the locale of user is RTL' do
        it 'adds RTL styling to email' do
          account.texts.ip_restriction = 'zzz.yyy.xxx.www'
          account.translation_locale = hebrew
          account.save!

          assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
        end
      end
    end

    describe "#api_token_changed" do
      before do
        account.api_tokens.create!
        account.save!
      end

      # See discussion in https://zendesk.atlassian.net/browse/APPSEC-371
      # on why we are not delivering notification for this change.
      it "does not have delivered the notification" do
        assert_empty(ActionMailer::Base.deliveries)
      end
    end

    describe "#api_token_access_enabled" do
      describe "assuming api token access is currently disabled" do
        before do
          account.settings.api_token_access = '0'
          account.save!
        end

        describe "when it is enabled" do
          before do
            account.settings.api_token_access = '1'
            account.save!
          end

          # See discussion in https://zendesk.atlassian.net/browse/APPSEC-371
          # on why we are not delivering notification for this change.
          it "does not have delivered the notification" do
            assert_empty(ActionMailer::Base.deliveries)
          end
        end
      end
    end

    describe "#api_token_access_disabled" do
      describe "assuming api token access is currently enabled" do
        before do
          account.settings.api_token_access = '1'
          account.save!
        end

        describe "when it is disabled" do
          before do
            account.settings.api_token_access = '0'
            account.save!
          end

          # See discussion in https://zendesk.atlassian.net/browse/APPSEC-371
          # on why we are not delivering notification for this change.
          it "does not have delivered the notification" do
            assert_empty(ActionMailer::Base.deliveries)
          end
        end
      end
    end

    describe "#api_password_access_enabled" do
      describe "assuming api password access is currently disabled" do
        before do
          account.settings.api_password_access = '0'
          account.translation_locale = spanish
          account.save!
        end

        describe "when it is enabled" do
          before do
            account.settings.api_password_access = '1'
            account.save!
          end

          it "has delivered the notification" do
            refute_empty ActionMailer::Base.deliveries
          end

          it "has been sent to the appropriate security notifications recipients" do
            assert_equal @admin_recipients, email.bcc
          end

          it "has the API settings change subject line" do
            # English: "API Settings changed"
            email.subject.must_include "Cambió configuración de API"
          end

          it "indicates the nature of change in the body text" do
            # English: "Change: API password access enabled"
            email.text_part.body.decoded.must_include "Cambio: Se ha activado el acceso con contraseña a API"
          end

          it "indicates who made the change in the body text" do
            # English: "Performed by: Agent Minimum"
            email.text_part.body.decoded.must_include "Realizado por: Agent Minimum"
          end

          it 'uses the proper formatting for LTR content' do
            assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
          end
        end

        describe 'when the locale of user is RTL' do
          it 'adds RTL styling to email' do
            account.settings.api_password_access = '1'
            account.translation_locale = hebrew
            account.save!

            assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
          end
        end
      end
    end

    describe "#api_password_access_disabled" do
      describe "assuming api password access is currently enabled" do
        before do
          account.settings.api_password_access = '1'
          account.translation_locale = spanish
          account.save!
        end

        describe "when it is disabled" do
          before do
            account.settings.api_password_access = '0'
            account.save!
          end

          it "has delivered the notification" do
            refute_empty ActionMailer::Base.deliveries
          end

          it "has been sent to the appropriate security notifications recipients" do
            assert_equal @admin_recipients, email.bcc
          end

          it "has the API settings change subject line" do
            # English: "API Settings changed"
            email.subject.must_include "Cambió configuración de API"
          end

          it "indicates the nature of change in the body text" do
            # English: "Change: API password access disabled"
            email.text_part.body.decoded.must_include "Cambio: Se desactivó el acceso con contraseña a API"
          end

          it "indicates who made the change in the body text" do
            # English: "Performed by: Agent Minimum"
            email.text_part.body.decoded.must_include "Realizado por: Agent Minimum"
          end

          it 'uses the proper formatting for LTR content' do
            assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
          end
        end

        describe 'when the locale of user is RTL' do
          it 'adds RTL styling to email' do
            account.settings.api_password_access = '0'
            account.translation_locale = hebrew
            account.save!

            assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
          end
        end
      end
    end

    describe "#assumption_expiration" do
      before do
        Timecop.freeze

        account.translation_locale = spanish
        account.save!
      end

      after do
        Timecop.return
      end

      describe "when enabled" do
        before do
          account.settings.assumption_expiration = 1.year.from_now
          account.save!
        end

        it "delivers the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "sends to the appropriate recipients" do
          assert_equal @admin_recipients, email.bcc
        end

        it "has the correct account assumption subject line" do
          # English: "Zendesk has been granted temporary access to your account"
          # v1: "The Zendesk team has been given temporary access to your account"
          email.subject.must_include "El equipo Zendesk ha recibido acceso temporal a su cuenta"
        end

        it "indicates the nature of change in the body text" do
          # English: "The Allow Account Assumption feature has been enabled"
          # v1: "The <b>Allow account assumption</b> date setting has been updated."
          email.text_part.body.decoded.must_include "La función <b>Permitir adopción de identidad de cuenta</b> está activada."
        end

        it "includes the expiration date" do
          email.text_part.body.decoded.must_include "domingo, 1 de enero de 2017, 0:00:00 (+00:00)"
        end

        it "includes the link to the support article" do
          email.text_part.body.decoded.must_include "https://support.zendesk.com/hc/en-us/articles/115001753608"
        end

        it 'uses the proper formatting for LTR content' do
          assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
        end
      end

      describe "when disabled" do
        before do
          account.settings.assumption_expiration = 1.year.from_now
          account.save!
          account.settings.assumption_expiration = nil
          account.save!
        end

        it "delivers the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "sends to the appropriate recipients" do
          assert_equal @admin_recipients, email.bcc
        end

        it "has the correct account assumption subject line" do
          # English: "Zendesk's temporary access to your account feature has been disabled"
          # v1: "The Zendesk team no longer has access to your account"
          email.subject.must_include "El equipo de Zendesk ya no tiene acceso a su cuenta"
        end

        it "indicates the nature of change in the body text" do
          # English: "The Allow Account Assumption feature duration has been disabled"
          # v1: "La función <b>Permitir adopción de identidad de cuenta</b> ha sido desactivada."
          email.text_part.body.decoded.must_include "La función <b>Permitir adopción de identidad de cuenta</b> ha sido desactivada."
        end

        it "includes the link to the support article" do
          email.text_part.body.decoded.must_include "https://support.zendesk.com/hc/en-us/articles/115001753608"
        end
      end

      describe "when changing" do
        before do
          account.settings.assumption_expiration = 1.year.from_now
          account.save!
          account.settings.assumption_expiration = 1.month.from_now
          account.save!
        end

        it "delivers the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "sends to the appropriate recipients" do
          assert_equal @admin_recipients, email.bcc
        end

        it "has the correct account assumption subject line" do
          # English: "Zendesk's temporary access to your account has been updated"
          # v1: "The subject line used when account assumption expiration changes"
          email.subject.must_include "La configuración del acceso a la cuenta ha sido actualizada"
        end

        it "indicates the nature of change in the body text" do
          # English: "The Allow Account Assumption feature duration has been changed"
          # v1: "The <b>Allow account assumption</b> date setting has been updated."
          email.text_part.body.decoded.must_include "La configuración de la fecha para <b>Permitir adopción de identidad de cuenta</b> ha sido actualizada."
        end

        it "includes the expiration date" do
          email.text_part.body.decoded.must_include "lunes, 1 de febrero de 2016, 0:00:00 (+00:00)"
        end

        it "includes the link to the support article" do
          email.text_part.body.decoded.must_include "https://support.zendesk.com/hc/en-us/articles/115001753608"
        end
      end

      describe "when permanent" do
        before do
          account.settings.assumption_expiration = Account::AssumptionSupport::ALWAYS_ASSUMABLE
          account.save!
        end

        it "delivers the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "sends to the appropriate recipients" do
          assert_equal @admin_recipients, email.bcc
        end

        it "has the correct account assumption subject line" do
          # English: "Zendesk has been granted indefinite access to your account"
          # v1: "The Zendesk team has been given access to your account"
          email.subject.must_include "El equipo de Zendesk ha recibido acceso a su cuenta"
        end

        it "indicates the nature of change in the body text" do
          # English: "The Allow Account Assumption feature is enabled indefinitely"
          # v1: "The <b>Allow account assumption</b> date setting has been changed."
          email.text_part.body.decoded.must_include "La configuración de la fecha para <b>Permitir adopción de identidad de cuenta</b> ha sido cambiada."
        end

        it "includes the link to the support article" do
          email.text_part.body.decoded.must_include "https://support.zendesk.com/hc/en-us/articles/115001753608"
        end
      end

      describe 'when the locale of user is RTL' do
        it 'adds RTL styling to email' do
          account.settings.assumption_expiration = 1.month.from_now
          account.translation_locale = hebrew
          account.save!

          assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
        end
      end
    end
  end

  describe SecurityNotifications::UserIdentityChanged do
    let(:user) { users(:minimum_agent) }

    def google_profile_for(user, is_verified = true)
      UserEmailIdentity.create(user: user, account: user.account, value: "example@gmail.com", is_verified: is_verified).tap do |identity|
        identity.google_profile = GoogleProfile.create(user_identity: identity)
        user.reload
      end
    end

    describe "UserVoiceForwardingIdentity" do
      before do
        UserVoiceForwardingIdentity.create(user: user, account: user.account, value: "+1 (415) 327-6120")
      end

      describe "#user_voice_forwarding_number_added" do
        it "delivers the email notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "has been sent to the appropriate security notifications recipients" do
          assert_equal ['minimum_agent@aghassipour.com'], email.to
        end

        it "generates user voice forwarding number added subject notification" do
          email.subject.must_include "Agent forwarding number added to your Zendesk account"
        end

        it "generates user voice forwarding number added body notification" do
          # The HTML entities ensure that the + sign remains
          # to the left of the number, even in RTL languages.
          email.text_part.body.decoded.must_include "The agent forwarding number '\u202D+14153276120\u202C' was added"
        end

        it "indicates who made the change" do
          email.text_part.body.decoded.must_include "updated by\nAgent Minimum"
        end
      end

      describe "#user_voice_forwarding_number_removed" do
        before do
          stub_request(:put, "https://minimum.zendesk-test.com/api/v2/channels/voice/availabilities/57888.json").to_return(status: 200)

          user.identities.voice_number.first.destroy
        end

        it "has delivered the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "has been sent to the appropriate security notifications recipients" do
          assert_equal ['minimum_agent@aghassipour.com'], email.to
        end

        it "generates user voice forwarding number added subject notification" do
          email.subject.must_include I18n.t('txt.email.user_identity_agent_forwarding_removed.subject')
        end

        it "generates user voice forwarding number added body notification" do
          # The HTML entities ensure that the + sign remains
          # to the left of the number, even in RTL languages.
          email.text_part.body.decoded.must_include "The agent forwarding number '\u202D+14153276120\u202C' associated"
        end

        it "indicates who made the change" do
          email.text_part.body.decoded.must_include "updated by\nAgent Minimum"
        end
      end
    end

    describe "#user_external_account_added" do
      describe "when external account is google" do
        before do
          google_profile_for(user, false).update_attributes!(is_verified: true)
        end

        it "has delivered the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "has been sent to the appropriate security notifications recipients" do
          assert_equal ['minimum_agent@aghassipour.com'], email.to
        end

        it "generates google email address added subject notification" do
          email.subject.must_include I18n.t('txt.email.user_identity_google_added.subject')
        end

        it "generates google email address added body notification" do
          email.text_part.body.decoded.must_include "Google identity 'example@gmail.com' was added"
        end

        it "indicates who made the change" do
          email.text_part.body.decoded.must_include "updated by\nAgent Minimum"
        end
      end
    end

    describe "#user_external_account_removed" do
      describe "when external account is google" do
        before do
          google_profile_for(user).destroy
        end

        it "has delivered the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "has been sent to the appropriate security notifications recipients" do
          assert_equal ['minimum_agent@aghassipour.com'], email.to
        end

        it "generates google email address removed subject notification" do
          email.subject.must_include I18n.t('txt.email.user_identity_google_removed.subject')
        end

        it "generates google email address removed body notification" do
          email.text_part.body.decoded.must_include "Google identity 'example@gmail.com' associated with your Zendesk account\nhas been removed."
        end

        it "indicates who made the change" do
          email.text_part.body.decoded.must_include "updated by\nAgent Minimum"
        end
      end
    end

    describe "UserEmailIdentity" do
      describe_with_arturo_disabled :email_suppress_email_identity_update_notifications do
        before do
          UserEmailIdentity.create(user: user, account: user.account, value: "auxilliary@example.com", is_verified: true)
        end

        describe "#user_auxillary_email_added" do
          it "has delivered the notification" do
            refute_empty ActionMailer::Base.deliveries
          end

          it "has been sent to the appropriate security notifications recipients" do
            assert_equal ['minimum_agent@aghassipour.com'], email.to
          end

          it "generates auxillary email added subject notification" do
            email.subject.must_include I18n.t('txt.email.user_identity_email_added.subject')
          end

          it "generates auxillary email added body notification" do
            email.text_part.body.decoded.must_include "The email address 'auxilliary@example.com' was added"
          end

          it "indicates who made the change" do
            email.text_part.body.decoded.must_include "updated by\nAgent Minimum"
          end
        end

        describe "#user_auxillary_email_removed" do
          before { user.identities.email.find_by_value("auxilliary@example.com").destroy }

          it "has delivered the notification" do
            refute_empty ActionMailer::Base.deliveries
          end

          it "has been sent to the appropriate security notifications recipients" do
            assert_equal ['minimum_agent@aghassipour.com'], email.to
          end

          it "generates auxillary email removed subject notification" do
            email.subject.must_include I18n.t('txt.email.user_identity_email_removed.subject')
          end

          it "generates auxillary email removed body notification" do
            email.text_part.body.decoded.must_include "The email address 'auxilliary@example.com' associated"
          end

          it "indicates who made the change" do
            email.text_part.body.decoded.must_include "updated by\nAgent Minimum"
          end
        end

        describe "#user_primary_email_changed" do
          before { user.identities.email.find_by_value("auxilliary@example.com").make_primary! }

          it "has delivered the notification" do
            refute_empty ActionMailer::Base.deliveries
          end

          it "uses the previous primary email address as the recipient" do
            assert_equal "minimum_agent@aghassipour.com", email.bcc.first
          end

          it "generates primary email changed subject notification" do
            email.subject.must_include "Zendesk account primary email address changed"
          end

          it "has been sent to the user" do
            assert_equal user.reload.email, email.to.first
          end

          it "generates primary email changed body notification" do
            email.text_part.body.decoded.must_include(
              "The primary email address for your Zendesk account was changed from\n" \
            "'minimum_agent@aghassipour.com' to 'auxilliary@example.com'"
            )
          end

          it "indicates who made the change" do
            email.text_part.body.decoded.must_include "updated by\nAgent Minimum"
          end
        end
      end

      describe_with_arturo_enabled :email_suppress_email_identity_update_notifications do
        describe "#user_auxillary_email_added" do
          it "does not send notifications" do
            SecurityMailer.expects(:send).never
            UserEmailIdentity.create(user: user, account: user.account, value: "auxilliary@example.com", is_verified: true)
          end

          describe "#user_auxillary_email_removed" do
            before do
              UserEmailIdentity.create(user: user, account: user.account, value: "auxilliary@example.com", is_verified: true)
            end

            it "does not send notifications" do
              SecurityMailer.expects(:send).never
              user.identities.email.find_by_value("auxilliary@example.com").destroy
            end
          end

          describe "#user_primary_email_changed" do
            let(:identity) do
              UserEmailIdentity.create(user: user, account: user.account, value: "auxilliary@example.com", is_verified: true)
            end

            it "does not send notifications" do
              SecurityMailer.expects(:deliver_user_primary_email_changed).never
              identity.make_primary!
            end
          end
        end
      end
    end
  end

  describe SecurityNotifications::PasswordStrategy do
    describe "#maximum_login_attempts_exceeded" do
      def strategy_for(account)
        env = Rack::MockRequest.env_for("/access/login")
        env["zendesk.account"] = account
        Zendesk::Auth::Warden::PasswordStrategy.new(env)
      end

      def exceed_maximum_login_attempts!(user)
        Prop.stubs(:throttled?).returns(true)
        Prop.stubs(:throttle!).raises(
          Prop::RateLimited.new(
            handle: :password_attempt,
            cache_key: [],
            interval: 1,
            threshold: 10,
            strategy: Prop::IntervalStrategy,
            first_throttled: false
          )
        )

        account  = user.account
        strategy = strategy_for(account)

        strategy.stubs(:email_parameter).returns(user.email)
        begin
          strategy.send(:ensure_rate_limiting!, [], {})
        rescue
          :ignore
        end
      end

      before do
        Arturo.enable_feature!(:security_email_notifications_for_password_strategy)
      end

      describe "when maximum login attempts is exceeded" do
        before do
          exceed_maximum_login_attempts! users(:minimum_agent)
        end

        it "has delivered the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "has been sent to the user" do
          assert_equal user.email, email.to.first
        end

        it "has been sent to the appropriate security notifications recipients" do
          assert_equal @admin_recipients, email.bcc
        end

        it "has the name of the agent in the subject line" do
          email.subject.must_include "Agent Agent Minimum"
        end

        it "indicates the nature of the change" do
          email.text_part.body.decoded.must_include "Change: One of your agents has been suspended due to multiple failed login attempts"
        end

        it "indicates the name of the agent that was suspended" do
          email.text_part.body.decoded.must_include "Name: Agent Minimum"
        end

        it "indicates the email address of the agent that was suspended" do
          email.text_part.body.decoded.must_include "Email: minimum_agent@aghassipour.com"
        end

        it "indicates the profile url of the agent that was suspended" do
          email.text_part.body.decoded.must_include "To view this user, follow this link: https://minimum.zendesk-test.com/users/#{user.id}"
        end
      end

      describe "when the authenticating user is the owner" do
        before do
          @owner = account.owner
          exceed_maximum_login_attempts! @owner
        end

        it "has delivered the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "has notified the admins" do
          assert @admin_recipients.all? { |admin| admin.in? email.bcc + email.to }
        end

        it "has notified the authenticating user" do
          assert email.to.include? @owner.email
        end

        it "indicates the name of the agent that was suspended" do
          email.text_part.body.decoded.must_include "Name: Admin Man"
        end
      end

      describe "when the authenticating user is an admin but not the owner" do
        before do
          @admin = users(:minimum_admin_not_owner)
          exceed_maximum_login_attempts! @admin
        end

        it "has delivered the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "has notified the admins" do
          assert_equal @admin_recipients - [@admin.email], email.bcc
        end

        it "does not have notified the authenticating user" do
          refute email.bcc.include? @admin.email
        end

        it "indicates the name of the agent that was suspended" do
          email.text_part.body.decoded.must_include "Name: Not owner man"
        end
      end

      describe "when the authenticating user is an agent" do
        before do
          @agent = users(:minimum_agent)
          exceed_maximum_login_attempts! @agent
        end

        it "has delivered the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "has notified the admins" do
          assert_equal @admin_recipients, email.bcc
        end

        it "does not have notified the authenticating user" do
          refute email.bcc.include? @agent.email
        end

        it "indicates the name of the agent that was suspended" do
          email.text_part.body.decoded.must_include "Name: Agent Minimum"
        end
      end

      describe "when the authenticating user is an end-user" do
        before do
          exceed_maximum_login_attempts! users(:minimum_end_user)
        end

        it "has not delivered the notification" do
          assert_empty(ActionMailer::Base.deliveries)
        end
      end
    end
  end

  describe SecurityNotifications::UserProfileChanged do
    let(:user) { users(:minimum_agent) }

    describe "#profile_crypted_password_changed" do
      let(:account) { accounts(:minimum) }

      before do
        brand0 = account.default_brand
        HelpCenter.
          stubs(:find_by_brand_id).
          with(brand0.id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))

        brand1 = FactoryBot.create(:brand, account_id: account.id, name: "Wombats", subdomain: "wombats", active: true)
        HelpCenter.
          stubs(:find_by_brand_id).
          with(brand1.id).
          returns(HelpCenter.new(id: 43, state: 'enabled'))

        brand2 = FactoryBot.create(:brand, account_id: account.id, name: "Nope", subdomain: "nope", active: true)
        HelpCenter.
          stubs(:find_by_brand_id).
          with(brand2.id).
          returns(HelpCenter.new(id: 44, state: 'restricted'))

        account.default_brand = brand1
        account.save!
      end

      describe "for an end user" do
        let(:user) { users(:minimum_end_user) }
        let(:actor) { user }
        let(:active_brand_id) { user.account.brands.first.id }

        describe "who is doing the change himself" do
          before do
            CIA.stubs(:current_actor).returns(user)

            User.any_instance.expects(:active_brand_id).returns(active_brand_id)

            user.change_password!("secret")
          end

          it "has delivered the email notification" do
            refute_empty ActionMailer::Base.deliveries
          end

          it "generates password changed email notification with the appropriate subject" do
            assert_equal "User profile updated: password changed", email.subject
          end

          it "should come from the active brand email" do
            assert_equal "support@zdtestlocalhost.com", email.from.first
          end

          it "has been sent to the user" do
            assert_equal "minimum_end_user@aghassipour.com", email.to.first
          end

          it "generates password changed email notification with the appropriate body" do
            email.text_part.body.decoded.must_include "Your password was changed"
          end

          it "indicates who made the change" do
            email.text_part.body.decoded.must_include "updated by you"
          end

          it "show the sign-in to the current brand" do
            expected = "You can sign in at: https://minimum.zendesk-test.com/login"
            email.text_part.body.decoded.must_match(expected)
          end

          it "lists all the active help centers" do
            expected = "https://minimum.zendesk-test.com\n· https://wombats.zendesk-test.com"
            email.text_part.body.decoded.must_match(expected)
          end

          it "shouldn't include inactive help centers" do
            not_expected = "https://nope.zendesk-test.com"
            email.text_part.body.decoded.wont_match(not_expected)
          end
        end

        describe "whose password is changed by an admin" do
          before do
            CIA.stubs(:current_actor).returns(@actor)

            user.change_password!("secret")
          end

          it "has delivered the email notification" do
            refute_empty ActionMailer::Base.deliveries
          end

          it "generates password changed email notification with the appropriate subject" do
            assert_equal "User profile updated: password changed", email.subject
          end

          it "should come from the default brand email" do
            email.from.first.must_include "support@wombats.zendesk-test.com"
          end

          it "has been sent to the user" do
            assert_equal "minimum_end_user@aghassipour.com", email.to.first
          end

          it "generates password changed email notification with the appropriate body" do
            email.text_part.body.decoded.must_include "Your password was changed"
          end

          it "indicates who made the change" do
            email.text_part.body.decoded.must_include "updated by the administrator"
          end

          it "show the sign-in to default brand" do
            expected = "You can sign in at: https://wombats.zendesk-test.com/login"
            email.text_part.body.decoded.must_include expected
          end

          it "lists all the active brands" do
            expected = "https://minimum.zendesk-test.com\n· https://wombats.zendesk-test.com"
            email.text_part.body.decoded.must_include expected
          end

          it "shouldn't include inactive brands" do
            not_expected = "https://nope.zendesk-test.com"
            email.text_part.body.decoded.wont_include(not_expected)
          end
        end
      end

      describe "for an agent" do
        let(:user) { users(:minimum_agent) }
        let(:actor) { user }

        before do
          CIA.stubs(:current_actor).returns(actor)

          user.change_password!("secret")
        end

        it "has delivered the email notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "generates password changed email notification with the appropriate subject" do
          assert_equal "User profile updated: password changed", email.subject
        end

        it "should come from the default auth email" do
          assert_equal "support@wombats.zendesk-test.com", email.from.first
        end

        it "has been sent to the user" do
          assert_equal user.email, email.to.first
        end

        it "generates password changed email notification with the appropriate body" do
          email.text_part.body.decoded.must_include "Your password was changed"
        end

        it "indicates who made the change" do
          email.text_part.body.decoded.must_include "updated by you"
        end

        it "show the sign-in to the auth domain" do
          expected = "You can sign in at: https://minimum.zendesk-test.com/login"
          email.text_part.body.decoded.must_match(expected)
        end

        it "lists all the active brands" do
          expected = "https://minimum.zendesk-test.com\n· https://wombats.zendesk-test.com"
          email.text_part.body.decoded.must_match(expected)
        end

        it "shouldn't include inactive brands" do
          not_expected = "https://nope.zendesk-test.com"
          email.text_part.body.decoded.wont_match(not_expected)
        end
      end
    end

    # Remove the following block when cleaning up arturo email_notification_profile_admin_added_duplicates_fix
    describe "#profile_admin_user_added_v2" do
      before do
        new_user            = User.new
        new_user.account_id = user.account.id
        new_user.roles      = Role::ADMIN.id
        new_user.name       = "Rinko Kikuchi"
        new_user.email      = "rinko.kikuchi@example.com"
        new_user.save!
      end

      it "has delivered the notification" do
        refute_empty ActionMailer::Base.deliveries
      end

      it "has been sent to the appropriate security notifications recipients" do
        assert_equal @admin_recipients, email.bcc
      end

      it "indicates the nature of change in the subject line" do
        email.subject.must_include "Additional Admin created"
      end

      it "indicates the nature of change in the body text" do
        email.text_part.body.decoded.must_include "A new admin user has been added to your account"
      end

      it "indicates who made the change in the body text" do
        email.text_part.body.decoded.must_include "added to your account by Agent Minimum"
      end

      it "indicates the name of the new admin in the body text" do
        email.text_part.body.decoded.must_include "Name: Rinko Kikuchi"
      end

      it "indicates the email of the new admin in the body text" do
        email.text_part.body.decoded.must_include "Email: rinko.kikuchi@example.com"
      end

      describe "with rtl admins" do
        let(:japanese) { translation_locales(:japanese) }

        before do
          japanese.update_column(:right_to_left, true)

          new_user            = User.new
          new_user.account_id = user.account.id
          new_user.roles      = Role::ADMIN.id
          new_user.name       = "Rinko Kikuchi 2"
          new_user.email      = "rinko.kikuchi.2@example.com"
          new_user.account.update_attributes locale_id: japanese.id
          new_user.save!
        end

        it "wraps email in right-to-left embedding characters when locale is right-to-left" do
          email.text_part.body.decoded.must_match /\u202B(.+?)\u202C/
        end
      end
    end

    describe "#profile_admin_user_added" do
      before do
        Account.any_instance.stubs(:has_email_notification_profile_admin_added_duplicates_fix?).returns(true)
        new_user            = User.new
        new_user.account_id = user.account.id
        new_user.roles      = Role::ADMIN.id
        new_user.name       = "Rinko Kikuchi"
        new_user.email      = "rinko.kikuchi@example.com"
        new_user.save!
      end

      it "has delivered the notification" do
        refute_empty ActionMailer::Base.deliveries
      end

      it "has been sent to the appropriate security notifications recipients" do
        assert_equal @admin_recipients, email.bcc
      end

      it "indicates the nature of change in the subject line" do
        email.subject.must_include "Additional Admin created"
      end

      it "indicates the nature of change in the body text" do
        email.text_part.body.decoded.must_include "A new admin user has been added to your account"
      end

      it "indicates who made the change in the body text" do
        email.text_part.body.decoded.must_include "added to your account by Agent Minimum"
      end

      it "indicates the name of the new admin in the body text" do
        email.text_part.body.decoded.must_include "Name: Rinko Kikuchi"
      end

      it "indicates the email of the new admin in the body text" do
        email.text_part.body.decoded.must_include "Email: rinko.kikuchi@example.com"
      end

      describe "with rtl admins" do
        let(:japanese) { translation_locales(:japanese) }

        before do
          japanese.update_column(:right_to_left, true)

          new_user            = User.new
          new_user.account_id = user.account.id
          new_user.roles      = Role::ADMIN.id
          new_user.name       = "Rinko Kikuchi 2"
          new_user.email      = "rinko.kikuchi.2@example.com"
          new_user.account.update_attributes locale_id: japanese.id
          new_user.save
        end

        it "wraps email in right-to-left embedding characters when locale is right-to-left" do
          email.text_part.body.decoded.must_match /\u202B(.+?)\u202C/
        end
      end
    end
  end

  describe SecurityNotifications::CertificateStatusChanged do
    before do
      account.update_attribute(:host_mapping, 'www.google.com')
      @certificate = account.certificates.create!
    end

    describe "#certificate_approval" do
      before do
        @certificate.state       = "active"
        @certificate.valid_until = 1.year.from_now
        @certificate.save!
        @certificate.notify_customer_of_activation!
      end

      it "has delivered the notification" do
        refute_empty ActionMailer::Base.deliveries
      end

      it "has been sent to the owner" do
        assert_equal @owner_recipients, email.bcc
      end

      it "has the certificate approval subject line" do
        email.subject.must_include "Zendesk SSL certificate approved for #{@certificate.account.host_name}"
      end

      it "mentions the domain name of the account in the body text" do
        email.text_part.body.decoded.must_include "Name of domain: www.google.com"
      end

      it "mentions the expiration date of the certificate of the account in the body text" do
        email.text_part.body.decoded.must_include "Expiring on: #{@certificate.valid_until.to_date}"
      end

      it "mentions the nature of the notification in the body text" do
        email.text_part.body.decoded.must_include "Your SSL certificate has been approved"
      end

      it "mentions that the DNS setting needs to be update to point to the appropriate address" do
        email.text_part.body.decoded.must_include "You will also need to update your DNS settings for the www.google.com"
        email.text_part.body.decoded.must_include "domain to be a CNAME pointing to the following address: minimum.ssl.zendesk-test.com"
      end

      describe "with a non-english account locale" do
        before do
          account.translation_locale = translation_locales(:spanish)
          account.save!
          ActionMailer::Base.deliveries.clear
          @certificate.notify_customer_of_activation!
        end

        it "sends the email in the account locale" do
          email.subject.must_include "This is a spanish string"
        end
      end
    end

    describe "#certificate_approval of a sni certificate" do
      before do
        @certificate.state       = "active"
        @certificate.valid_until = 1.year.from_now
        @certificate.sni_enabled = true
        @certificate.save!
        @certificate.notify_customer_of_activation!
      end

      it "has delivered the notification" do
        refute_empty ActionMailer::Base.deliveries
      end

      it "has been sent to the owner" do
        assert_equal @owner_recipients, email.bcc
      end

      it "has the certificate approval subject line" do
        email.subject.must_include "Zendesk SSL certificate approved for #{@certificate.account.host_name}"
      end

      it "mentions the domain name of the account in the body text" do
        email.text_part.body.decoded.must_include "Name of domain: www.google.com"
      end

      it "mentions the expiration date of the certificate of the account in the body text" do
        email.text_part.body.decoded.must_include "Expiring on: #{@certificate.valid_until.to_date}"
      end

      it "mentions the nature of the notification in the body text" do
        email.text_part.body.decoded.must_include "Your SSL certificate has been approved"
      end
    end

    describe "#lets_encrypt_enabled" do
      before do
        @certificate.state       = "active"
        @certificate.valid_until = 1.year.from_now
        @certificate.sni_enabled = true
        @certificate.autoprovisioned = true
        @certificate.save!
        @certificate.notify_customer_of_activation!

        SecurityMailer.deliver_lets_encrypt_enabled(@certificate)
      end

      it "has delivered the notification" do
        refute_empty ActionMailer::Base.deliveries
      end

      it "has been sent to the owner" do
        assert_equal @owner_recipients, email.bcc
      end

      it "has the certificate approval subject line" do
        email.subject.must_include "Zendesk-provisioned SSL is enabled"
      end

      it "mentions the nature of the notification in the body text" do
        email.text_part.body.decoded.must_include "Zendesk-provisioned SSL is now enabled on your account and Zendesk obtained a SSL certificate for you."
      end
    end

    describe "#certificate_expiration_warning" do
      before do
        @certificate.state       = "active"
        @certificate.valid_until = 1.months.from_now
        @certificate.save!
      end

      describe "with one brand" do
        before do
          Zendesk::Maintenance::Jobs::CertificateMaintenanceJob.execute
          # SecurityMailer.deliver_certificate_about_to_expire(@certificate) # SEE: CertificateExpirationJob#L7
        end

        it "has delivered the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "has been sent to the owner" do
          assert_equal @owner_recipients, email.bcc
        end

        it "has the certificate expiring subject line" do
          email.subject.must_match /Your Zendesk SSL certificate is expiring in (28|29|30|31) days/
        end

        it "shows the domain name in the body text" do
          email.text_part.body.decoded.must_include "\n\nDomain: www.google.com"
        end

        it "mentions the expiration date of the certificate of the account in the body text" do
          email.text_part.body.decoded.must_include "Your SSL certificate will expire on #{@certificate.valid_until.to_date} for:"
        end

        it "mentions the .ssl CNAME warning" do
          email.text_part.body.decoded.must_include "Zendesk-provisioned SSL certificates will not work with CNAME values that have '.ssl' in them. To solve this problem, remove '.ssl' from your CNAME."
        end
      end

      describe "with multiple brands" do
        before do
          route = account.routes.create!(host_mapping: 'support.test.com', subdomain: 'test')
          account.brands.create! { |b| b.name = 'test'; b.route = route }

          Zendesk::Maintenance::Jobs::CertificateMaintenanceJob.execute
          # SecurityMailer.deliver_certificate_about_to_expire(@certificate) # SEE: CertificateExpirationJob#L7
        end

        it "shows a list of domain names in the body text" do
          email.text_part.body.decoded.must_include "\n\nDomains:\nwww.google.com\nsupport.test.com"
        end
      end
    end

    describe "#certificate_replacement_warning" do
      before do
        @certificate.state       = "active"
        @certificate.valid_until = 4.days.from_now
        @certificate.save!
      end

      describe "with one brand" do
        before do
          Zendesk::Maintenance::Jobs::CertificateMaintenanceJob.execute
        end

        it "has delivered the notification" do
          refute_empty ActionMailer::Base.deliveries
        end

        it "has been sent to the owner" do
          assert_equal @owner_recipients, email.bcc
        end

        it "has the certificate replacement subject line" do
          email.subject.must_match /Your SSL certificate will soon be replaced with a Zendesk-provisioned SSL certificate/
        end

        it "shows the domain name in the body text" do
          email.text_part.body.decoded.must_include "\n\nDomain: www.google.com"
        end

        it "mentions the expiration date of the certificate of the account in the body text" do
          email.text_part.body.decoded.must_include "Your SSL certificate will expire on #{@certificate.valid_until.to_date} for:"
        end
      end

      describe "with multiple brands" do
        before do
          route = account.routes.create!(host_mapping: 'support.test.com', subdomain: 'test')
          account.brands.create! { |b| b.name = 'test'; b.route = route }

          Zendesk::Maintenance::Jobs::CertificateMaintenanceJob.execute
          # SecurityMailer.deliver_certificate_about_to_expire(@certificate) # SEE: CertificateExpirationJob#L7
        end

        it "shows the domain name in the body text" do
          email.text_part.body.decoded.must_include "\n\nDomains:\nwww.google.com\nsupport.test.com"
        end
      end
    end
  end
end
