require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered! uncovered: 1

describe DeflectionMailer do
  fixtures :all

  include MailerTestHelper
  let(:ticket) { tickets(:minimum_1) }
  let(:user) { users(:minimum_agent) }
  let(:system_user) { users(:systemuser) }
  let(:mailer) { DeflectionMailer.send(:new) }
  let(:brand) { brands(:minimum) }
  let(:template) { "Ticket id: \#{{ticket.id}} {{automatic_answers.article_list}} {{ticket.comments_formatted}}" }
  let(:notification) do
    audit = ticket.audits[0]

    AnswerBotNotification.new(
      id: 1,
      subject:    'foo title',
      body:       'foo',
      recipients: [user.email],
      audit: audit
    ) do |event|
      event.via_id = ViaType.RULE
      event.via_reference_id = rule_id

      # Don't deliver notifications during test setup
      AnswerBotNotification.skip_callback(:commit, :after, :deliver_notifications)
      event.save!
      AnswerBotNotification.set_callback(:commit, :after, :deliver_notifications)
    end
  end
  let(:subject) { "Thank you for contacting K&R! You request is received for: {{ticket.title}}" }
  let(:ticket_deflection) { TicketDeflection.new(ticket: ticket, user: user, account: ticket.account) }
  let(:ticket_deflection_article) { TicketDeflectionArticle.new(ticket_deflection: ticket_deflection, account: ticket.account, score: 0.89, brand: brand) }
  let(:deflection_config) do
    {recipient: 'requester_id', subject: subject, body: template}
  end
  let(:deflection_trigger) do
    d = Definition.new
    d.actions.push(DefinitionItem.new("deflection", "is", deflection_config))
    d.conditions_all.push(DefinitionItem.new("status_id", "is", [StatusType.NEW]))
    Trigger.create!(account: ticket.account, title: 'deflection trigger', definition: d, owner: ticket.account)
  end
  let(:rule_id) { deflection_trigger.id }

  before do
    Account.any_instance.stubs(:send_real_attachments?).returns(true)
    Account.any_instance.expects(:has_automatic_answers_enabled?).returns(true).at_least_once
    Ticket.any_instance.expects(:ticket_deflection).returns(ticket_deflection).at_least_once
    TicketDeflection.any_instance.expects(:ticket_deflection_articles).returns([ticket_deflection_article]).at_least_once
    AnswerBot::Trigger.stubs(:config).with(ticket.account, rule_id).returns(subject_template: subject, body_template: template, labels_for_filter: nil)
    Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:answer_bot_setting_enabled?).returns(false)
    Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_articles_for_ids_and_locales).returns([{
      id: ticket_deflection_article,
      title: 'How to fix dE sign on display',
      html_url: 'zendesk.com/hc/en-us/fix-de-sign-display',
      body: 'Call us and we will fix it for you'
    }])
  end

  describe 'email presentation' do
    before do
      account = ticket.account
      account.text_mail_template += "TEXT me"
      account.html_mail_template += "HTML you"
      account.save!

      ticket.will_be_saved_by system_user
      ticket.save!

      ticket.stubs(:generate_message_id).with(true).returns("random id")
      ticket.stubs(:generate_message_id).with(false).returns("static id")
    end

    let(:ccs) { [] }
    let(:mail) { mailer.answer_bot_email(ticket, notification, rule_id, ccs) }

    it 'should use the locale of the recipient' do
      locale = translation_locales(:japanese)
      ticket.requester.stubs(:translation_locale).returns(locale)
      mail # force mail setup to happen

      assert_equal locale.name, mailer.send(:locale).name
    end

    it 'should have reply_to the ticket address' do
      assert_equal "support@zdtestlocalhost.com", mail.reply_to.first
    end

    describe_with_arturo_enabled :email_ccs do
      describe 'with CCs on the ticket' do
        let(:end_user) { users(:minimum_end_user) }
        let(:ccs) { [end_user] }

        before do
          ticket.will_be_saved_by system_user
          ticket.collaborations.build(user: end_user, ticket: ticket, collaborator_type: CollaboratorType.EMAIL_CC)
          ticket.save
        end

        describe 'with email CCs enabled' do
          before { ticket.account.stubs(has_comment_email_ccs_allowed_enabled?: true) }

          it 'sends email to requester and ccs' do
            mail # force mail setup to happen

            assert_equal 'minimum_author@aghassipour.com', mail.to.first
            assert_equal 'minimum_end_user@aghassipour.com', mail.cc.first
          end
        end

        describe 'with email CCs not enabled' do
          before { ticket.account.stubs(has_comment_email_ccs_allowed_enabled?: false) }

          it 'sends email only to requester' do
            mail # force mail setup to happen

            assert_equal 'minimum_author@aghassipour.com', mail.to.first
            assert_empty mail.cc
          end
        end
      end
    end

    it 'should have mail `to` field to the ticket requester' do
      assert_equal 'minimum_author@aghassipour.com', mail.to.first
    end

    it 'should use delimiter specified for account' do
      assert_match /delimiter-delimiter-delimiter/, mail.html_part.to_s
    end

    it "should use the footer from the account settings" do
      assert_match /This email is a service from minimum/, mail.html_part.to_s
    end

    it 'should set for_agent so that ticket title will get shown for light user' do
      light_agent = users(:multiproduct_light_agent)
      ticket.stubs(:requester).returns(light_agent)
      mail # force mail setup

      assert mailer.send(:for_agent)
    end

    it 'contains a html part' do
      assert mail.html_part
    end

    it 'is a multipart email' do
      assert mail.multipart?
    end

    it 'contains a plain text part with rendered text' do
      assert mail.text_part.body.to_s
    end

    describe 'when the Subject field is NOT visible in HC and Email for end-users' do
      before { FieldSubject.any_instance.stubs(is_visible_in_portal?: false) }

      it "should display the first public comment's body" do
        assert_equal "Thank you for contacting K&R! You request is received for: minimum ticket 1", mail.subject.to_s
      end
    end

    describe 'when the Subject field is visible in HC and Email for end-users' do
      before { FieldSubject.any_instance.stubs(is_visible_in_portal?: true) }

      it "should have the subject with ticket_id" do
        assert_equal "Thank you for contacting K&R! You request is received for: minimum 1 ticket", mail.subject.to_s
      end
    end

    it "should have an automatic-answers header" do
      assert mail.header.to_s.include?("X-Delivery-Context: automatic-answer-#{ticket.id}")
    end

    it "should have the correct in-reply-to header" do
      assert_equal('static id', mail.header['In-Reply-To'].to_s)
    end

    it "should have the correct mail-id header" do
      assert_equal('random id', mail.header['Message-ID'].to_s)
    end

    describe 'when the comment has attachments' do
      before do
        StoresSynchronizationJob.stubs(:enqueue)

        ticket.comments.delete_all

        ticket.add_comment body: "I've attached that file you asked for", is_public: true
        ticket.will_be_saved_by(user)

        @attachments = []
        attached_files = %w[google_default.jpg]

        attached_files.each do |filename|
          @attachments << ticket.comment.attachments.build(
            uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{filename}"), filename),
            account: ticket.account,
            author: user,
            is_public: true
          )
        end

        comment_body = "I've attached that file you asked for"

        inline_attachments = %w[normal_1.jpg]

        inline_attachments.each do |filename|
          @attachments << ticket.comment.attachments.build(
            uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{filename}"), filename),
            account: ticket.account,
            author: user,
            is_public: true
          ) do |a|
            a.inline = true
          end

          comment_body = [comment_body, "![](#{@attachments.last.url})"].join(' ')
        end

        ticket.comment.body = comment_body

        ticket.save!

        ticket.reload

        ActionMailer::Base.perform_deliveries = true
        ActionMailer::Base.deliveries = []
      end

      describe "and the account can send Real Attachments" do
        before do
          Account.any_instance.stubs(:send_real_attachments?).returns(true)
        end

        it "includes attachments in the email" do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id)

          appended_attachments = mail.attachments.select { |a| a.content_disposition =~ /^attachment;/ }
          appended_attachments.size.must_equal 1
        end

        it "includes inline attachments in the email" do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id)

          inline_attachments = mail.attachments.select { |a| a.content_disposition =~ /^inline;/ }
          inline_attachments.size.must_equal 1
          assert_match /cid:/, mail.html_part.body.to_s
        end
      end
    end
  end

  describe "placeholder suppression" do
    let(:ticket) { tickets(:minimum_5) } # requester is end user
    let(:attachment) { 'google_default.jpg' }
    let(:user) { users(:minimum_end_user) }

    before do
      Account.any_instance.stubs(:send_real_attachments?).returns(true)
      StoresSynchronizationJob.stubs(:enqueue)

      # reset ticket state
      ticket.comments.delete_all
      ticket.audits.delete_all
      ticket.update_columns(status_id: 0)

      comment = ticket.add_comment(
        body: 'minimum ticket 5',
        is_public: true,
        author: ticket.requester,
        via_id: 0,
        via_reference_id: 0
      )

      ticket.will_be_saved_by(user)
      ticket.save!

      # Add some attachments
      comment.attachments.create(
        uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{attachment}"), attachment),
        account: ticket.account,
        author: user,
        is_public: true
      )
      inline_attachment = comment.attachments.create(
        uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{attachment}"), attachment),
        account: ticket.account,
        author: user,
        is_public: true
      ) do |a|
        a.inline = true
      end

      # Add the attachment inline
      comment.update_columns(
        value: "#{comment.body} ![](#{inline_attachment.url})"
      )

      Ticket.any_instance.unstub(:ticket_deflection)
      TicketDeflection.any_instance.unstub(:ticket_deflection_articles)

      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []
    end

    describe "When anybody can submit tickets setting is disabled" do
      before do
        Account.any_instance.stubs(:is_open?).returns(false)
      end

      it "should not remove placeholder from the body" do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last

        email.text_part.body.to_s.must_include "minimum ticket 5"
        email.html_part.body.to_s.must_include "minimum ticket 5"
      end

      it 'attaches files to the email' do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last

        email.attachments.size.must_equal 2
        email.attachments[0].content_disposition.must_equal 'attachment; filename=google_default.jpg'
        email.attachments[1].content_disposition.must_equal 'inline; filename=google_default.jpg'
      end

      it "does not remove placeholder from the subject" do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last

        assert_includes email.subject, "Thank you for contacting K&R! You request is received for:"
        assert_includes email.subject, "minimum five ticket"
      end

      it 'sets suppress_placeholder option for the MailContext to false (text and html)' do
        Zendesk::Liquid::MailContext.expects(:render).with(
          has_entries(suppress_placeholder: false)
        ).returns("foo content").twice

        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
      end

      it 'does not set suppress_placeholder option for the TicketContext to true' do
        Zendesk::Liquid::TicketContext.expects(:render).at_least_once
        Zendesk::Liquid::TicketContext.expects(:render).with(
          anything, anything, anything, anything, anything, anything,
          has_entries(suppress_placeholder: true)
        ).returns("bar content").never

        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
      end
    end

    describe "When require users to register is enabled" do
      before do
        Account.any_instance.stubs(:is_signup_required?).returns(true)
      end

      it "should not remove placeholder from the body" do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last

        email.text_part.body.to_s.must_include "minimum ticket 5"
        email.html_part.body.to_s.must_include "minimum ticket 5"
      end

      it 'attaches files to the email' do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last

        email.attachments.size.must_equal 2
        email.attachments[0].content_disposition.must_equal 'attachment; filename=google_default.jpg'
        email.attachments[1].content_disposition.must_equal 'inline; filename=google_default.jpg'
      end

      it "does not remove placeholder from the subject" do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last

        assert_includes email.subject, "Thank you for contacting K&R! You request is received for:"
        assert_includes email.subject, "minimum five ticket"
      end

      it 'sets suppress_placeholder option for the MailContext to false (text and html)' do
        Zendesk::Liquid::MailContext.expects(:render).with(
          has_entries(suppress_placeholder: false)
        ).returns("foo content").twice

        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
      end

      it 'does not set suppress_placeholder option for the TicketContext to true' do
        Zendesk::Liquid::TicketContext.expects(:render).at_least_once
        Zendesk::Liquid::TicketContext.expects(:render).with(
          anything, anything, anything, anything, anything, anything,
          has_entries(suppress_placeholder: true)
        ).returns("bar content").never

        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
      end
    end

    describe "for accounts where anyone can submit tickets and no sign up is required" do
      before do
        # 'Anybody can submit tickets' setting is enabled
        Account.any_instance.stubs(:is_open?).returns(true)
        # 'Require users to register is enabled' setting is disabled
        Account.any_instance.stubs(:is_signup_required?).returns(false)
      end

      it "removes placeholder from the body" do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last

        email.text_part.body.to_s.wont_include "minimum ticket 5"
        email.html_part.body.to_s.wont_include "minimum ticket 5"
      end

      it 'removes attachments' do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last
        email.attachments.size.must_equal 0
      end

      it 'sets suppress_placeholder option for the MailContext to true (text and html)' do
        Zendesk::Liquid::MailContext.expects(:render).with(
          has_entries(suppress_placeholder: true)
        ).returns("foo content").twice

        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
      end

      it 'sets suppress_placeholder option for the TicketContext to true (text and html)' do
        Zendesk::Liquid::TicketContext.expects(:render).at_least_once
        Zendesk::Liquid::TicketContext.expects(:render).with(
          anything, anything, anything, anything, anything, anything,
          has_entries(suppress_placeholder: true)
        ).returns("bar content").twice

        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
      end

      it "removes placeholder from the subject" do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last

        assert_includes email.subject, "Thank you for contacting K&R! You request is received for:"
        refute_includes email.subject, "minimum 5 ticket"
      end

      describe_with_arturo_disabled :email_placeholder_subject_deflection_suppression do
        it "does not remove placeholder from the subject" do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
          email = ActionMailer::Base.deliveries.last

          assert_includes email.subject, "Thank you for contacting K&R! You request is received for:"
          assert_includes email.subject, "minimum five ticket"
        end
      end

      describe_with_arturo_disabled :email_placeholder_body_deflection_suppression do
        it "does not remove placeholder from the body" do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
          email = ActionMailer::Base.deliveries.last

          email.text_part.body.to_s.must_include "minimum ticket 5"
          email.html_part.body.to_s.must_include "minimum ticket 5"
        end

        it 'attaches files to the email' do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
          email = ActionMailer::Base.deliveries.last

          email.attachments.size.must_equal 2
          email.attachments[0].content_disposition.must_equal 'attachment; filename=google_default.jpg'
          email.attachments[1].content_disposition.must_equal 'inline; filename=google_default.jpg'
        end
      end
    end

    describe "auto-linked tickets" do
      describe "when help center is enabled for account" do
        before do
          Account.any_instance.stubs(:help_center_enabled?).returns(true)
        end

        it "should present a link in html mode" do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
          email = ActionMailer::Base.deliveries.last

          email.html_part.body.to_s.must_include 'Ticket id: <a target="_blank" href="https://minimum.zendesk-test.com/hc/requests/5">#5</a>'
        end

        it "should present ticket id as text in plain text mode" do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
          email = ActionMailer::Base.deliveries.last

          email.text_part.body.to_s.must_include "Ticket id: #5"
        end
      end
    end
  end

  describe 'redact attachments' do
    let(:ticket) { tickets(:minimum_5) }
    let(:attachment) { 'google_default.jpg' }
    let(:user) { users(:minimum_end_user) }

    before do
      Account.any_instance.stubs(:send_real_attachments?).returns(true)
      Account.any_instance.stubs(:is_signup_required?).returns(true)
      StoresSynchronizationJob.stubs(:enqueue)

      # reset ticket state
      ticket.comments.delete_all
      ticket.audits.delete_all
      ticket.update_columns(status_id: 0)

      comment = ticket.add_comment(
        body: 'minimum ticket 5',
        is_public: true,
        author: ticket.requester,
        via_id: 0,
        via_reference_id: 0
      )

      ticket.will_be_saved_by(user)
      ticket.save!

      # Add some attachments
      comment.attachments.create(
        uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{attachment}"), attachment),
        account: ticket.account,
        author: user,
        is_public: true
      )
      inline_attachment = comment.attachments.create(
        uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{attachment}"), attachment),
        account: ticket.account,
        author: user,
        is_public: true
      ) do |a|
        a.inline = true
      end

      # Add the attachment inline
      comment.update_columns(
        value: "#{comment.body} ![](#{inline_attachment.url})"
      )

      Ticket.any_instance.unstub(:ticket_deflection)
      TicketDeflection.any_instance.unstub(:ticket_deflection_articles)

      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []
    end

    describe 'when public comment placeholders does not exists in the notification body' do
      let(:template) { 'Ticket id: \#{{ticket.id}} {{automatic_answers.article_list}}' }

      it 'should not send attachments in the outgoing email' do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last

        email.attachments.size.must_equal 0
      end

      describe 'when the recipient of the email is not an end-user' do
        let(:user) { users(:minimum_agent) }

        it 'should continue to send attachments in the outgoing email' do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])

          email = ActionMailer::Base.deliveries.last
          email.attachments.size.must_equal 2
          email.attachments[0].content_disposition.must_equal 'attachment; filename=google_default.jpg'
          email.attachments[1].content_disposition.must_equal 'inline; filename=google_default.jpg'
        end
      end
    end

    describe 'when dynamic placeholder exists in the notification body' do
      describe 'when the public comment placeholder is in a lower level of dynamic content' do
        let(:template) { 'Ticket id: {{ticket.id}} {{dc.foo}} {{automatic_answers.article_list}}' }

        before do
          create_dynamic_content!(ticket.account, 'foo', '{{ticket.comments_formatted}}')
        end

        it 'should send attachments in the outgoing email' do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])

          email = ActionMailer::Base.deliveries.last
          email.attachments.size.must_equal 2
          email.attachments[0].content_disposition.must_equal 'attachment; filename=google_default.jpg'
          email.attachments[1].content_disposition.must_equal 'inline; filename=google_default.jpg'
        end
      end

      describe 'when the public comment placeholder is not in a lower level of dynamic content' do
        let(:template) { 'Ticket id: {{ticket.id}} {{dc.foo}} {{automatic_answers.article_list}}' }

        before do
          create_dynamic_content!(ticket.account, 'foo', 'bar')
        end

        it 'should not send attachments in the outgoing email' do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])

          email = ActionMailer::Base.deliveries.last
          email.attachments.size.must_equal 0
        end
      end
    end

    describe 'with more than one public comments' do
      let(:template) { 'Ticket id: \#{{ticket.id}} {{automatic_answers.article_list}}' }

      before do
        comment = ticket.add_comment(body: 'first comment', is_public: true, author: ticket.requester)
        ticket.will_be_saved_by(user)
        ticket.save!

        # Add inline and appended attachments
        comment.attachments.create(
          uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{attachment}"), attachment),
          account: ticket.account,
          author: user,
          is_public: true
        )
        inline_attachment = comment.attachments.create(
          uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{attachment}"), attachment),
          account: ticket.account,
          author: user,
          is_public: true
        ) do |a|
          a.inline = true
        end
        comment.update_columns(
          value: "#{comment.body} ![](#{inline_attachment.url})"
        )
      end

      it "should not send attachments in the outgoing email" do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last
        email.attachments.size.must_equal 0
      end

      describe_with_arturo_disabled :email_remove_attachments_if_no_public_comment_placeholder do
        it 'should send attachments in the outgoing email' do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
          email = ActionMailer::Base.deliveries.last
          email.attachments.size.must_equal 2
          email.attachments[0].content_disposition.must_equal 'attachment; filename=google_default.jpg'
          email.attachments[1].content_disposition.must_equal 'inline; filename=google_default.jpg'
        end
      end
    end

    describe 'when public comment placeholders does exists in the notification body' do
      let(:template) { 'Ticket id: \#{{ticket.id}} {{automatic_answers.article_list}} {{ticket.comments_formatted}}' }

      it 'should send attachments in the outgoing email' do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
        email = ActionMailer::Base.deliveries.last

        email.attachments.size.must_equal 2
        email.attachments[0].content_disposition.must_equal 'attachment; filename=google_default.jpg'
        email.attachments[1].content_disposition.must_equal 'inline; filename=google_default.jpg'
      end
    end

    describe_with_arturo_disabled :email_deflection_mailer_redact_attachments do
      describe 'when public comment placeholders does not exists in the notification body' do
        let(:template) { 'Ticket id: \#{{ticket.id}} {{automatic_answers.article_list}}' }

        it 'should send attachments in the outgoing email' do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id, [])
          email = ActionMailer::Base.deliveries.last

          email.attachments.size.must_equal 2
          email.attachments[0].content_disposition.must_equal 'attachment; filename=google_default.jpg'
          email.attachments[1].content_disposition.must_equal 'inline; filename=google_default.jpg'
        end
      end
    end
  end

  describe 'email sending behaviour' do
    before do
      ticket.will_be_saved_by system_user
      ticket.save!
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []
    end

    it "sends the deflection email if the requester is human" do
      refute ticket.requester.machine?
      DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id)
      assert_equal 1, ActionMailer::Base.deliveries.size
    end

    it "does not send the deflection email if the requester is a machine" do
      ticket.requester.identities.first.mark_as_machine
      assert ticket.requester.machine?
      DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id)
      assert_equal 0, ActionMailer::Base.deliveries.size
    end

    describe "when a comment contains newline characters" do
      it "renders content with line breaks" do
        DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id)
        email = ActionMailer::Base.deliveries.last
        email.html_part.body.to_s.must_include "LINE 1\n\n\nLINE 2"
      end
    end
  end

  describe 'logging' do
    let(:statsd_client_stub) { stub_for_statsd }

    it 'logs when a deflection email has sent' do
      DeflectionMailer.any_instance.stubs(:statsd_client).returns(statsd_client_stub)
      statsd_client_stub.expects(:increment).with('email_sent', tags: ["subdomain:#{ticket.account.subdomain}"])

      DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id)
    end
  end

  private

  def create_dynamic_content!(account, name, value)
    Cms::Text.create!(
      name: name,
      account: account,
      fallback_attributes: {
        is_fallback: true,
        nested: true,
        value: value,
        translation_locale_id: ENGLISH_BY_ZENDESK.id
      }
    )
  end
end
