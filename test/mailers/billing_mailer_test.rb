require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered!

describe BillingMailer do
  include MailerTestHelper

  fixtures :all

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries         = []

    @minimum_1_ticket = tickets(:minimum_1)
    @minimum_end_user = users(:minimum_end_user)
    @minimum_agent    = users(:minimum_agent)
    @minimum_account  = accounts(:minimum)
    @translation      = translation_locales(:japanese)
    @credit_card      = credit_cards(:minimum)
    @new_notification = Notification.new(
      subject: '{{current_user.name}} has updated {{ticket.title}}',
      body: '{{ticket.url}}',
      author: @minimum_agent
    )
    FileUtils.stubs(:rm).with(anything)
  end

  describe "#voice_account_suspension_notification" do
    before do
      voice_sub_account = stub(id: anything, suspended?: anything, activate!: anything)
      Account.any_instance.stubs(:voice_sub_account).returns(voice_sub_account)
    end

    it "generates and send a voice account suspension email to the account owner" do
      assert_difference "ActionMailer::Base.deliveries.length", 1 do
        BillingMailer.deliver_voice_account_suspension_notification(@minimum_account)
      end

      message = ActionMailer::Base.deliveries.last
      assert_equal "Zendesk Voice account suspended", message.subject
      assert message.joined_bodies.index("Your account is currently still active, but Zendesk Voice has temporarily been disabled in your account.")
      assert message.joined_bodies.index("Your <strong>#{@minimum_account.name}</strong> account owner: <strong>#{@minimum_account.owner.name}</strong>")
    end
  end
end
