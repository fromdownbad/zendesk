require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"
require_relative "../support/multiproduct_test_helper"

SingleCov.covered!

describe UsersMailer do
  include MailerTestHelper
  include MultiproductTestHelper

  fixtures :accounts, :brands, :account_settings, :users, :user_identities, :tickets,
    :translation_locales, :account_property_sets, :groups, :translation_locales, :recipient_addresses, :role_settings

  let(:spanish) { translation_locales(:spanish) }
  let(:hebrew) { translation_locales(:hebrew) }

  before do
    User.any_instance.stubs(account_is_serviceable: true)

    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []

    @minimum_1_ticket = tickets(:minimum_1)
    @minimum_end_user = users(:minimum_end_user)
    @minimum_agent = users(:minimum_agent)
    @minimum_account = accounts(:minimum)
    @translation = translation_locales(:japanese)
    @new_notification = Notification.new(
      subject: '{{current_user.name}} has updated {{ticket.title}}',
      body: '{{ticket.url}}', author: @minimum_agent
    )
    @minimum_account.text_mail_template += 'With great text comes great responsibility'
    @minimum_account.html_mail_template += 'By the power of HTML'
    @minimum_account.save!
  end

  describe "#verify" do
    describe "with 1 email identity" do
      let(:user) { users(:minimum_author) }
      let(:identity) { user.identities.first }
      before { user.verification_tokens.create! }

      it "delivers welcome for users" do
        UsersMailer.deliver_verify(identity)
        assert_includes ActionMailer::Base.deliveries.last.subject, "Welcome to"
      end

      it "generates a token as necessary when none is passed in" do
        UserEmailIdentity.any_instance.expects(:verification_token).returns('footokenbar')
        UsersMailer.deliver_verify(identity, nil, nil, nil)
        ActionMailer::Base.deliveries.last.joined_bodies.must_include "/verification/email/footokenbar"
      end

      describe "for agents" do
        let(:identity) { @minimum_agent.identities.first }

        it "delivers with verification link to join Support" do
          UsersMailer.deliver_verify(identity)
          assert_includes ActionMailer::Base.deliveries.last.joined_bodies, "To accept the invitation and create your account"
          assert_includes ActionMailer::Base.deliveries.last.joined_bodies, "Zendesk Support"
        end

        describe "and agent is a contributor" do
          before do
            contributor_permission_set = PermissionSet.enable_contributor!(@minimum_account)
            @minimum_agent.permission_set = contributor_permission_set
            @minimum_agent.save!
          end

          it "delivers with verification link for multiproduct agent" do
            UsersMailer.deliver_verify(identity)
            assert_includes ActionMailer::Base.deliveries.last.joined_bodies, "To accept the invitation and create your account"
            assert_not_includes ActionMailer::Base.deliveries.last.joined_bodies, "Zendesk Support"
          end
        end

        describe "with google login enabled" do
          before do
            @minimum_account.role_settings.agent_zendesk_login = false
            @minimum_account.role_settings.agent_google_login = true
            @minimum_account.role_settings.save!
          end

          it "delivers without verification link" do
            UsersMailer.deliver_verify(identity)
            assert_not_includes ActionMailer::Base.deliveries.last.joined_bodies, "To accept the invitation and create your account"
          end
        end
      end

      describe "with multiple active help centers on the account" do
        before do
          @brand1 = FactoryBot.create(:brand, account_id: @minimum_account.id, subdomain: "wombats", active: true)
          @brand2 = FactoryBot.create(:brand, account_id: @minimum_account.id, subdomain: "penguins", active: true)
          Account.any_instance.stubs(:help_center_enabled?).returns(true)
          HelpCenter.
            stubs(:find_by_brand_id).
            returns(HelpCenter.new(id: 42, state: 'enabled'))
        end

        it "includes a list of active help centers in the welcome message for an end user" do
          UsersMailer.deliver_verify(identity)
          assert_match %r{https://wombats.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
          assert_match %r{https://penguins.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
        end

        it "does not include a list of active help centers in the welcome message for agents" do
          agent = users(:minimum_agent)
          UsersMailer.deliver_verify(agent.identities.first)
          refute_match %r{has multiple portals}, ActionMailer::Base.deliveries.last.joined_bodies
        end

        it "shows branded token link specific to the hc the end user signed in on" do
          User.any_instance.stubs(:active_brand_id).returns(@brand1.id)
          UsersMailer.deliver_verify(identity, nil, nil, nil, @brand1.id)
          ActionMailer::Base.deliveries.last.joined_bodies.must_include "https://wombats.zendesk-test.com/verification/email/"
        end

        describe "with account setting list_help_centers_in_account_emails = false" do
          before do
            @minimum_account.settings.list_help_centers_in_account_emails = false
            @minimum_account.settings.save!
          end

          it "does not include a list of active help centers in the welcome message for an end user" do
            UsersMailer.deliver_verify(identity)
            refute_match %r{has multiple portals}, ActionMailer::Base.deliveries.last.joined_bodies
          end
        end
      end

      it "does not include a list of active help centers if there is only one help center on the account" do
        Account.any_instance.stubs(:help_center_enabled?).returns(true)
        HelpCenter.
          stubs(:find_by_brand_id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))

        UsersMailer.deliver_verify(identity)
        refute_match %r{has multiple portals}, ActionMailer::Base.deliveries.last.joined_bodies
      end

      describe "with brand_id passed into the verify" do
        before do
          @brand = FactoryBot.create(
            :brand,
            account_id: @minimum_account.id,
            subdomain: 'wombatsandlemurs',
            name: 'Wombat Support Brand',
            active: true
          )
        end

        it "sends an email using the passed in brand for all links and text" do
          user = users(:minimum_author)
          identity = user.identities.first
          User.any_instance.stubs(:active_brand_id).returns(@brand.id)
          UsersMailer.deliver_verify(identity, nil, nil, nil, @brand.id)
          ActionMailer::Base.deliveries.last.joined_bodies.must_include "//wombatsandlemurs.zendesk-test.com/verification/email/"
          assert_equal ["support@wombatsandlemurs.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
          assert_equal "Welcome to Wombat Support Brand", ActionMailer::Base.deliveries.last.subject
          ActionMailer::Base.deliveries.last.joined_bodies.must_include "This email is a service from Wombat Support Brand"
        end
      end

      it "does not deliver welcome for machines" do
        User.any_instance.stubs(machine?: true)
        UsersMailer.deliver_verify(identity)
        ActionMailer::Base.deliveries.size.must_equal 0
      end

      it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
        UsersMailer.deliver_verify(identity)
        email = ActionMailer::Base.deliveries.last
        email[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal "Verification Email"
      end
    end

    it "provides a link to verify email in help_center if it's enabled" do
      Account.any_instance.stubs(:help_center_enabled?).returns(true)
      user = users(:minimum_author)
      identity = user.identities.first
      UsersMailer.deliver_verify(identity, nil, nil, 'token')
      ActionMailer::Base.deliveries.last.joined_bodies.must_include "/verification/email/token"
    end

    describe "with multiple active help centers on the account" do
      before do
        @brand1 = FactoryBot.create(:brand, account_id: @minimum_account.id, subdomain: "wombats", active: true)
        @brand2 = FactoryBot.create(:brand, account_id: @minimum_account.id, subdomain: "penguins", active: true)
        Account.any_instance.stubs(:help_center_enabled?).returns(true)
        HelpCenter.
          stubs(:find_by_brand_id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))
      end

      it "provides a list of active help centers associated with the account" do
        user = users(:minimum_author)
        identity = user.identities.first

        UsersMailer.deliver_verify(identity, nil, nil, 'token')
        assert_match %r{https://wombats.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
        assert_match %r{https://penguins.zendesk-test.com}, ActionMailer::Base.deliveries.last.joined_bodies
      end

      it "does not include a list of active help centers for agents" do
        user = users(:minimum_agent)
        identity = user.identities.first
        UsersMailer.deliver_verify(identity, nil, nil, 'token')
        refute_match %r{has multiple portals}, ActionMailer::Base.deliveries.last.joined_bodies
      end

      it "shows branded token link specific to the hc the end user signed in on" do
        user = users(:minimum_author)
        identity = user.identities.first

        UsersMailer.deliver_verify(identity, nil, nil, 'token', @brand1.id)
        ActionMailer::Base.deliveries.last.joined_bodies.must_include "https://wombats.zendesk-test.com/verification/email/"
        assert_equal ["support@wombats.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
      end
    end

    it "does not provide a list of help centers if there is only one" do
      Account.any_instance.stubs(:help_center_enabled?).returns(true)
      HelpCenter.
        stubs(:find_by_brand_id).
        returns(HelpCenter.new(id: 42, state: 'enabled'))
      user = users(:minimum_author)
      identity = user.identities.first

      UsersMailer.deliver_verify(identity, nil, nil, 'token')
      refute_match %r{has multiple portals}, ActionMailer::Base.deliveries.last.joined_bodies
    end

    it "generates headers and footers in right language" do
      @author = users(:minimum_author)
      Subscription.any_instance.stubs(:has_individual_language_selection?).returns(true)
      Account.any_instance.stubs(:available_languages).returns([translation_locales(:japanese), translation_locales(:english_by_zendesk)])
      @author.locale_id = 4
      @author.save!
      @author.reload
      @author.verification_tokens.create!
      UsersMailer.deliver_verify(@author.identities.first)
      assert_match /いらっしゃませ/, force_utf_8(ActionMailer::Base.deliveries.last.subject)
      assert_match /日本語フター/, force_utf_8(ActionMailer::Base.deliveries.last.joined_bodies)
    end

    it "generates for user identities" do
      identity = users(:minimum_author).identities.email.first
      identity.verification_tokens.create!
      UsersMailer.deliver_verify(identity)
      refute ActionMailer::Base.deliveries.empty?
      ActionMailer::Base.deliveries.last.subject.must_include "Welcome"
    end

    it "generates for users and resolve dynamic content in the signup message" do
      Account.any_instance.stubs(:signup_email_text).returns("{{dc.signup_wombat}}")
      Zendesk::Liquid::DcContext.any_instance.expects(:render).twice.returns("Thank you for signing up wombat!")
      users(:minimum_author).verification_tokens.create!
      UsersMailer.deliver_verify(users(:minimum_author).identities.first)
      refute ActionMailer::Base.deliveries.empty?
      ActionMailer::Base.deliveries.last.joined_bodies.must_include "Thank you for signing up wombat!"
    end

    it "generates for second email addresses" do
      identity = UserEmailIdentity.create!(account: accounts(:minimum), user: users(:minimum_author), value: 'new@identity.com')
      users(:minimum_author).reload
      identity.verification_tokens.create!
      UsersMailer.deliver_verify(identity)
      refute ActionMailer::Base.deliveries.empty?
      ActionMailer::Base.deliveries.last.subject.must_include "Please verify your email address"
    end

    describe 'with multiple identities' do
      before do
        @user = users(:minimum_author)
        @identity = UserEmailIdentity.create!(account: accounts(:minimum), user: @user, value: 'new@identity.com')
        @user.reload
        @identity.verification_tokens.create!
      end

      describe 'with arturo on' do
        before { Arturo.enable_feature! :restrict_identity_verification_email }

        describe 'primary identity is verified' do
          before { @user.identities.first.is_verified? }

          it 'sends verification email' do
            UsersMailer.deliver_verify(@identity)
            refute ActionMailer::Base.deliveries.empty?
            assert_includes ActionMailer::Base.deliveries.last.subject, "Please verify your email address"
          end
        end

        describe 'primary identity is unverified' do
          before do
            @user.identities.first.update_attribute(:is_verified, false)
            refute @user.identities.first.is_verified?
          end

          it 'sends welcome email' do
            UsersMailer.deliver_verify(@identity)
            refute ActionMailer::Base.deliveries.empty?
            assert_includes ActionMailer::Base.deliveries.last.subject, "Welcome to"
          end
        end
      end

      describe 'with arturo off' do
        before { Arturo.disable_feature! :restrict_identity_verification_email }

        describe 'primary identity is verified' do
          before { @user.identities.first.is_verified? }

          it 'sends verification email' do
            UsersMailer.deliver_verify(@identity)
            refute ActionMailer::Base.deliveries.empty?
            assert_includes ActionMailer::Base.deliveries.last.subject, "Please verify your email address"
          end
        end

        describe 'primary identity is unverified' do
          before do
            @user.identities.first.update_attribute(:is_verified, false)
            refute @user.identities.first.is_verified?
          end

          it 'sends verification email' do
            UsersMailer.deliver_verify(@identity)
            refute ActionMailer::Base.deliveries.empty?
            assert_includes ActionMailer::Base.deliveries.last.subject, "Please verify your email address"
          end
        end
      end
    end

    it "generates for second email addresses and resolve dynamic content in the account verification message" do
      Account.any_instance.stubs(:verify_email_text).returns("{{dc.verify_wombat}}")
      Zendesk::Liquid::DcContext.any_instance.expects(:render).twice.returns("verify your email wombat!")
      identity = UserEmailIdentity.create!(account: accounts(:minimum), user: users(:minimum_author), value: 'new@identity.com')
      users(:minimum_author).reload
      identity.verification_tokens.create!
      UsersMailer.deliver_verify(identity)
      refute ActionMailer::Base.deliveries.empty?
      ActionMailer::Base.deliveries.last.joined_bodies.must_include "verify your email wombat"
    end

    it "generates with custom text" do
      user = users(:minimum_author)
      identity = user.identities.first
      message  = "Custom message"
      subject  = 'hi'
      user.verification_tokens.create!
      UsersMailer.deliver_verify(identity, message, subject)
      refute ActionMailer::Base.deliveries.empty?
      assert_equal 'hi', ActionMailer::Base.deliveries.last.subject
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(message)
    end

    describe "with dynamic content" do
      it "generates" do
        Zendesk::Liquid::DcContext.any_instance.expects(:render).twice.returns("Welcome Wombat!")
        user = users(:minimum_author)
        identity = user.identities.first
        subject  = 'hi'
        message  = '{{dc.welcome_wombat}}'
        user.verification_tokens.create!
        UsersMailer.deliver_verify(identity, message, subject)

        refute ActionMailer::Base.deliveries.empty?
        assert_equal 'hi', ActionMailer::Base.deliveries.last.subject
        assert ActionMailer::Base.deliveries.last.joined_bodies.include?("Welcome Wombat!")
      end

      describe "simply formatted" do
        before do
          user = users(:minimum_author)
          user.verification_tokens.create!
          Cms::Text.create!(
            name: "wombat",
            account: user.account,
            fallback_attributes: {
              is_fallback: true,
              nested: true,
              value: "foo\n\nbar",
              translation_locale_id: ENGLISH_BY_ZENDESK.id
            }
          )
          identity = user.identities.first
          @expected_link = "https://minimum.zendesk-test.com/verification/email/#{identity.verification_token}"
          UsersMailer.deliver_verify(identity, "{{dc.wombat}}", "hello")
          refute ActionMailer::Base.deliveries.empty?
          @email = ActionMailer::Base.deliveries.last
        end

        it "rendereds formatted dynamic content correctly" do
          assert_includes body_by_content_type(@email, Mime[:html]), "hello <p>foo</p><p>bar</p>"
        end

        it "contains a correctly formatted verification link" do
          assert_includes body_by_content_type(@email, Mime[:html]), "<p><a href=\"#{@expected_link}\">#{@expected_link}</a></p>"
        end
      end
    end

    it "generates with custom text and subject" do
      user = users(:minimum_author)
      identity = user.identities.first
      subject = "Custom subject"
      message = "Custom message"
      user.verification_tokens.create!
      UsersMailer.deliver_verify(identity, message, subject)
      refute ActionMailer::Base.deliveries.empty?
      assert_equal subject, ActionMailer::Base.deliveries.last.subject
      assert ActionMailer::Base.deliveries.last.joined_bodies.include?(message)
    end

    describe 'for a shell account without support' do
      let(:end_user_signup_email_text) { 'Welcome end user' }
      let(:signup_email_subject) { 'Welcome to' }
      let(:account) { setup_shell_account }
      let(:user) { account.owner }
      let(:identity) { user.identities.first }

      before do
        Account.any_instance.stubs(:multiproduct?).returns(true)
        Account.any_instance.stubs(:signup_email_text).returns(end_user_signup_email_text)
      end

      it 'delivers the agent welcome email' do
        UsersMailer.deliver_verify(identity)
        email = ActionMailer::Base.deliveries.last
        assert_includes email.subject, signup_email_subject
        assert_not_includes body_by_content_type(email, Mime[:text]), end_user_signup_email_text
      end

      describe 'when user is an end-user' do
        let(:user) do
          account.users.create!(name: 'some end user', email: 'end_user_doge@example.com')
        end

        it 'raises an exception' do
          assert_raises NoMethodError do
            UsersMailer.deliver_verify(identity)
          end
        end
      end
    end
  end

  describe "#verify_multiproduct_staff" do
    let (:account) { setup_shell_account }
    let (:user) { account.owner }
    let (:identity) { user.identities.first }
    let (:second_identity) { UserEmailIdentity.create!(user: user, value: 'email@example.com') }

    it "delivers multiproduct verification email with verification link" do
      UsersMailer.deliver_verify_multiproduct_staff(identity)
      assert_includes ActionMailer::Base.deliveries.last.joined_bodies, "You have been invited to join Zendesk for"
      assert_includes ActionMailer::Base.deliveries.last.joined_bodies, "To accept the invitation and create your account"
    end

    it "delivers multiproduct verification email for non-primary identities with verification link" do
      identity.update_attribute(:is_verified, true)
      UsersMailer.deliver_verify_multiproduct_staff(second_identity)
      assert_includes ActionMailer::Base.deliveries.last.joined_bodies, "We need to verify that you are the owner of this email address."
      assert_includes ActionMailer::Base.deliveries.last.joined_bodies, "Please follow the link below to verify."
    end

    describe "with google login enabled" do
      before do
        account.role_settings.agent_zendesk_login = false
        account.role_settings.agent_google_login = true
        account.role_settings.save!
      end

      it "delivers multiproduct verification email without verification link" do
        UsersMailer.deliver_verify_multiproduct_staff(identity)
        assert_includes ActionMailer::Base.deliveries.last.joined_bodies, "You have been invited to join Zendesk for"
        assert_not_includes ActionMailer::Base.deliveries.last.joined_bodies, "To accept the invitation and create your account"
      end
    end
  end

  describe '#verify_multiproduct_email_address' do
    let (:account) { setup_shell_account }
    let (:user) { account.owner }
    let (:identity) { user.identities.first }

    it "generates for unverified email addresses" do
      unverified_address = UnverifiedEmailAddress.create!(user: user, email: 'email@example.com')
      assert_difference "ActionMailer::Base.deliveries.size", 1 do
        UsersMailer.deliver_verify_multiproduct_email_address(unverified_address)
      end

      email = ActionMailer::Base.deliveries.last

      email.subject.must_include "Please verify your email address"
      assert_equal(["noreply@#{user.account.subdomain}.zendesk-test.com"], email.from)
    end

    it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
      unverified_address = UnverifiedEmailAddress.create!(user: user, email: 'email@example.com')

      UsersMailer.deliver_verify_multiproduct_email_address(unverified_address)

      email = ActionMailer::Base.deliveries.last
      email[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal "Verify Email Address"
    end
  end

  describe "#verify_email_address" do
    it "generates for unverified email addresses" do
      user = FactoryBot.create(:end_user)
      unverified_address = UnverifiedEmailAddress.create!(user: user, email: 'email@example.com')
      assert_difference "ActionMailer::Base.deliveries.size", 1 do
        UsersMailer.deliver_verify_email_address(unverified_address, nil)
      end

      email = ActionMailer::Base.deliveries.last

      email.subject.must_include "Please verify your email address"
      assert_equal(["support@#{user.account.subdomain}.zendesk-test.com"], email.from)
    end

    it "generates branded verification links for end users for unverified email addresses" do
      user = users(:minimum_author)
      brand = FactoryBot.create(:brand, account_id: user.account_id, subdomain: 'wombats', active: true, name: "Wombat Brand")

      unverified_address = UnverifiedEmailAddress.create!(user: user, email: 'email@example.com')
      assert_difference "ActionMailer::Base.deliveries.size", 1 do
        UsersMailer.deliver_verify_email_address(unverified_address, brand.id)
      end

      email = ActionMailer::Base.deliveries.last

      email.subject.must_include "Please verify your email address"
      email.joined_bodies.must_include "wombats.zendesk-test.com/verify/email/"
      assert_equal ["support@wombats.zendesk-test.com"], email.from
      ActionMailer::Base.deliveries.last.joined_bodies.must_include "This email is a service from Wombat Brand"
    end

    it "appends the make_primary parameter to the link if it is present" do
      user = users(:minimum_author)
      @identity = user.identities.first

      UsersMailer.deliver_verify(@identity, nil, nil, 'token', nil, true)

      assert_match %r{/verification/email/token\?make_primary=true}, ActionMailer::Base.deliveries.last.joined_bodies
    end

    it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
      unverified_address = UnverifiedEmailAddress.create!(user: FactoryBot.create(:end_user), email: 'email@example.com')

      UsersMailer.deliver_verify_email_address(unverified_address, nil)

      email = ActionMailer::Base.deliveries.last
      email[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal "Verify Email Address"
    end
  end

  describe "#suspend_verify" do
    before do
      # We only send this mail if the account HC or WP
      @minimum_account.enable_help_center!
    end

    let(:suspended_ticket) do
      SuspendedTicket.new do |s|
        s.account    = @minimum_account
        s.subject    = 'Wiffle..'
        s.from_name  = 'Svend Svendsen'
        s.from_mail  = 'aa@aghassipour.com'
        s.content    = 'Stuff..'
        s.cause      = SuspensionType.SIGNUP_REQUIRED
        s.properties = {}
      end
    end

    it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
      suspended_ticket.save!
      email = ActionMailer::Base.deliveries.last
      email[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal "Suspend Verify"
    end

    describe "verification link and from address" do
      it "uses the values of the account's default brand if the suspended ticket has no brand" do
        suspended_ticket.properties.delete(:brand_id)
        suspended_ticket.save!
        email = ActionMailer::Base.deliveries.last
        assert_match %r{#{@minimum_account.default_brand.url}}, email.joined_bodies
        assert_match %r{#{@minimum_account.default_brand.default_recipient_address.email}}, email.from.first
      end

      it "uses the values of the brand that is in the suspended ticket" do
        brand = FactoryBot.create(:brand)
        suspended_ticket.properties[:brand_id] = brand.id
        suspended_ticket.save!
        email = ActionMailer::Base.deliveries.last
        assert_match %r{#{brand.url}}, email.joined_bodies
        assert_match %r{#{brand.default_recipient_address.email}}, email.from.first
      end
    end

    describe "locale" do
      before do
        @japanese = translation_locales(:japanese)
        @portuguese = translation_locales(:brazilian_portuguese)
        @minimum_account.translation_locale = @portuguese
        @minimum_account.signup_email_text = "{{dc.signup_wombat}}"
        @minimum_account.save!
        Zendesk::Liquid::DcContext.any_instance.expects(:render).twice.returns("いらっしゃませ！日本語ロギン")
      end

      it "when the suspended ticket has a locale" do
        suspended_ticket.properties[:locale_id] = @japanese.id
        suspended_ticket.save!

        refute ActionMailer::Base.deliveries.empty?
        email = ActionMailer::Base.deliveries.last
        assert_match /いらっしゃませ/, email.subject
        assert_match /日本語ロギン/, force_utf_8(body_by_content_type(email, Mime[:text]))
      end

      it "when the suspended ticket has no locale" do
        suspended_ticket.save!

        refute ActionMailer::Base.deliveries.empty?
        email = ActionMailer::Base.deliveries.last
        assert_match /Bem-vindo/, email.subject
      end
    end
  end

  describe '#multiproduct_password_reset' do
    let(:account) { accounts(:shell_account_without_support) }
    let(:user) { User.find(account.owner_id) }

    it 'generates password reset emails' do
      UsersMailer.any_instance.stubs(:recipient_locale).returns(translation_locales(:spanish))
      UsersMailer.deliver_multiproduct_password_reset(user, 'token')
      refute ActionMailer::Base.deliveries.empty?

      email = ActionMailer::Base.deliveries.last

      assert_match /#{user.account.name} password reset/, email.subject

      assert_match /token\?locale=6/, body_by_content_type(email, Mime[:text])
      assert_match /token\?locale=6/, body_by_content_type(email, Mime[:html])
      body_by_content_type(email, Mime[:text]).must_include "This email was sent to you because someone requested a password reset on your account."
      body_by_content_type(email, Mime[:html]).must_include "This email was sent to you because someone requested a password reset on your account."
    end

    it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
      UsersMailer.deliver_password_reset(@minimum_end_user, 'token')
      email = ActionMailer::Base.deliveries.last
      email[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal 'Password Reset'
    end
  end

  describe "#password_reset" do
    it "generates password reset emails" do
      UsersMailer.any_instance.stubs(:recipient_locale).returns(translation_locales(:spanish))
      UsersMailer.deliver_password_reset(@minimum_end_user, 'token')
      refute ActionMailer::Base.deliveries.empty?

      email = ActionMailer::Base.deliveries.last

      assert_match /#{@minimum_end_user.account.route.brand.name} password reset/, email.subject

      assert_match /token\?locale=6/, body_by_content_type(email, Mime[:text])
      assert_match /token\?locale=6/, body_by_content_type(email, Mime[:html])
      body_by_content_type(email, Mime[:text]).must_include "This email was sent to you because someone requested a password reset on your account."
      body_by_content_type(email, Mime[:html]).must_include "This email was sent to you because someone requested a password reset on your account."
    end

    it "provides a link on the account subdomain help_center when enabled" do
      Account.any_instance.stubs(:help_center_enabled?).returns(true)
      UsersMailer.any_instance.stubs(:recipient_locale).returns(translation_locales(:spanish))
      UsersMailer.deliver_password_reset(@minimum_end_user, 'token')

      refute ActionMailer::Base.deliveries.empty?
      email = ActionMailer::Base.deliveries.last

      email.joined_bodies.must_include "/password/reset/token"
    end

    describe "with multiple active help centers on the account" do
      before do
        @brand1 = FactoryBot.create(:brand, account_id: @minimum_account.id, subdomain: "wombats", active: true, name: "Wombat Brand")
        @brand2 = FactoryBot.create(:brand, account_id: @minimum_account.id, subdomain: "penguins", active: true)
        Account.any_instance.stubs(:help_center_enabled?).returns(true)
        HelpCenter.
          stubs(:find_by_brand_id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))
      end

      it "provides a list of active help centers associated with the account" do
        UsersMailer.deliver_password_reset(@minimum_end_user, 'token')

        refute ActionMailer::Base.deliveries.empty?
        email = ActionMailer::Base.deliveries.last

        assert_match %r{https://wombats.zendesk-test.com}, email.joined_bodies
        assert_match %r{https://penguins.zendesk-test.com}, email.joined_bodies
      end

      it "does not include a list of active help centers for agents" do
        UsersMailer.deliver_password_reset(@minimum_agent, 'token')
        refute ActionMailer::Base.deliveries.empty?
        email = ActionMailer::Base.deliveries.last
        refute_match %r{has multiple portals}, email.joined_bodies
      end

      it "shows branded token and login links specific to the hc the end user signed in on" do
        UsersMailer.deliver_password_reset(@minimum_end_user, 'token', @brand1.id)
        ActionMailer::Base.deliveries.last.joined_bodies.must_include "https://wombats.zendesk-test.com/password/reset/token"
        ActionMailer::Base.deliveries.last.joined_bodies.must_include "You can do a regular login at: https://wombats.zendesk-test.com"
        assert_equal ["support@wombats.zendesk-test.com"], ActionMailer::Base.deliveries.last.from
        ActionMailer::Base.deliveries.last.joined_bodies.must_include "This email is a service from Wombat Brand"
        assert_equal "Wombat Brand password reset", ActionMailer::Base.deliveries.last.subject
      end
    end

    it "does not provide a list of help centers if there is only one" do
      Account.any_instance.stubs(:help_center_enabled?).returns(true)
      HelpCenter.
        stubs(:find_by_brand_id).
        returns(HelpCenter.new(id: 42, state: 'enabled'))

      UsersMailer.deliver_password_reset(@minimum_end_user, 'token')

      refute ActionMailer::Base.deliveries.empty?
      email = ActionMailer::Base.deliveries.last

      refute_match %r{has multiple portals}, email.joined_bodies
    end

    it "generates branded password change emails" do
      brand = FactoryBot.create(:brand, account_id: @minimum_end_user.account.id, subdomain: 'wombats', name: "Wombat Brand")
      UsersMailer.any_instance.stubs(:recipient_locale).returns(translation_locales(:spanish))

      UsersMailer.deliver_password_reset(@minimum_end_user, 'token', brand.id)
      refute ActionMailer::Base.deliveries.empty?

      email = ActionMailer::Base.deliveries.last

      assert_equal "minimum", @minimum_end_user.account.route.subdomain

      assert_match "Wombat Brand password reset", email.subject
      body_by_content_type(email, Mime[:text]).must_include "This email was sent to you because someone requested a password reset on your account"
      ActionMailer::Base.deliveries.last.joined_bodies.must_include "wombats.zendesk-test.com/password/reset/token"
      assert_equal ["support@wombats.zendesk-test.com"], email.from
      ActionMailer::Base.deliveries.last.joined_bodies.must_include "Este correo electrónico es un servicio de Wombat Brand"
    end

    it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
      UsersMailer.deliver_password_reset(@minimum_end_user, 'token')
      email = ActionMailer::Base.deliveries.last
      email[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal "Password Reset"
    end

    describe 'with a shell account without support' do
      let(:account) { accounts(:shell_account_without_support) }
      let(:user) { User.find(account.owner_id) }

      it 'generates password reset emails' do
        UsersMailer.any_instance.stubs(:recipient_locale).returns(translation_locales(:spanish))
        UsersMailer.deliver_multiproduct_password_reset(user, 'token')
        refute ActionMailer::Base.deliveries.empty?

        email = ActionMailer::Base.deliveries.last

        assert_match /#{user.account.name} password reset/, email.subject

        assert_match /token\?locale=6/, body_by_content_type(email, Mime[:text])
        assert_match /token\?locale=6/, body_by_content_type(email, Mime[:html])
        body_by_content_type(email, Mime[:text]).must_include "This email was sent to you because someone requested a password reset on your account."
        body_by_content_type(email, Mime[:html]).must_include "This email was sent to you because someone requested a password reset on your account."
      end
    end
  end

  describe '#reminder' do
    it 'delivers' do
      UsersMailer.deliver_reminder(@minimum_agent.email, @minimum_account, [@minimum_account, nil])
      email = ActionMailer::Base.deliveries.last

      assert_match /Forgot your Zendesk URL?/, email.subject
      body_by_content_type(email, Mime[:text]).must_include 'We received a request to help you recover your Zendesk URL.'
      body_by_content_type(email, Mime[:html]).must_include 'We received a request to help you recover your Zendesk URL.'
    end

    it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
      UsersMailer.deliver_reminder(@minimum_agent.email, @minimum_account, [@minimum_account, nil])
      email = ActionMailer::Base.deliveries.last
      email[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal 'Password Reminder'
    end
  end

  describe "#reminder_no_accounts" do
    it "delivers" do
      locale = translation_locales(:japanese)
      I18n.stubs(:locale=)
      I18n.expects(:locale=).with(locale)

      # NOTE: #support_account is used (systemaccount will be ignored)
      UsersMailer.deliver_reminder_no_accounts(@minimum_agent.email, locale, accounts(:systemaccount))

      refute ActionMailer::Base.deliveries.empty?
      email = ActionMailer::Base.deliveries.last

      assert_match /Forgot your Zendesk URL?/, email.subject
      body_by_content_type(email, Mime[:text]).must_include "We were not able to find any accounts associated with this email address."
      body_by_content_type(email, Mime[:html]).must_include "We were not able to find any accounts associated with this email address."
    end
  end

  describe "#support_account" do
    it "does not trigger any additional queries when used for set_headers to avoid unsharded queries" do
      mailer = UsersMailer.send(:new)
      account = mailer.send(:support_account)
      assert_sql_queries 0 do
        mailer.send(:set_headers, account)
      end
    end
  end

  describe "#brand" do
    before do
      @brand = FactoryBot.create(:brand, account_id: @minimum_account.id, name: 'wombats', active: true, created_at: Time.now - 5.days)
      @brand2 = FactoryBot.create(:brand, account_id: @minimum_account.id, name: 'lemurs', active: true, created_at: Time.now - 1.days)
      @brand3 = FactoryBot.create(:brand, account_id: @minimum_account.id, name: 'giraffes', active: true, created_at: Time.now)
    end

    describe "with brand_id passed in" do
      it "returns the account's brand corresponding to that id" do
        mailer = UsersMailer.send(:new)
        brand = mailer.send(:brand, @brand.id, @minimum_account)
        assert_equal @brand.id, brand.id
      end
    end

    describe "without brand_id passed in" do
      it "returns agent_route's brand if it has a valid hc" do
        brand = @minimum_account.route.brand
        HelpCenter.
          stubs(:find_by_brand_id).
          with(brand.id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))

        mailer = UsersMailer.send(:new)
        brand = mailer.send(:brand, nil, @minimum_account)
        assert_equal @minimum_account.route.brand.id, brand.id
      end

      it "returns the account's default brand if it has a valid hc and the agent_route didn't have a hc" do
        HelpCenter.stubs(:find_by_brand_id).returns(nil)
        HelpCenter.
          stubs(:find_by_brand_id).
          with(@brand.id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))
        @minimum_account.stubs(:default_brand).returns(@brand)

        refute @minimum_account.route.brand.has_help_center?

        mailer = UsersMailer.send(:new)
        brand = mailer.send(:brand, nil, @minimum_account)
        assert_equal @brand.id, brand.id
      end

      it "returns the oldest brand that has a hc, if neither the agent brand nor the default brand had a hc" do
        HelpCenter.stubs(:find_by_brand_id).returns(nil)
        HelpCenter.
          stubs(:find_by_brand_id).
          with(@brand2.id).
          returns(HelpCenter.new(id: 42, state: 'enabled'))
        HelpCenter.
          stubs(:find_by_brand_id).
          with(@brand3.id).
          returns(HelpCenter.new(id: 43, state: 'enabled'))

        refute @brand.has_help_center?
        refute @minimum_account.route.brand.has_help_center?
        refute @minimum_account.default_brand.has_help_center?

        mailer = UsersMailer.send(:new)
        brand = mailer.send(:brand, nil, @minimum_account)
        assert_equal @brand2.id, brand.id
      end

      it "returns the agent_route's brand as a last resort if no brand_id was sent and there are no other brand hcs enabled" do
        HelpCenter.stubs(:find_by_brand_id).returns(nil)
        HelpCenter.
          stubs(:find_by_brand_id).
          with(@brand2.id).
          returns(HelpCenter.new(id: 42, state: 'restricted'))
        HelpCenter.
          stubs(:find_by_brand_id).
          with(@brand3.id).
          returns(HelpCenter.new(id: 43, state: 'restricted'))

        refute @brand.has_help_center?
        refute @minimum_account.route.brand.has_help_center?
        refute @minimum_account.default_brand.has_help_center?

        mailer = UsersMailer.send(:new)
        brand = mailer.send(:brand, nil, @minimum_account)
        assert_equal @minimum_account.route.brand.id, brand.id
      end
    end
  end

  describe "#admin_password_reset" do
    let(:user) do
      user = User.find_by_name('Agent Minimum')
      user.update_attributes!(locale_id: locale.id)
      assert_equal locale.locale, user.locale
      user
    end

    before do
      Account.any_instance.stubs(:help_center_enabled?).returns(true)
      HelpCenter.
        stubs(:find_by_brand_id).
        returns(HelpCenter.new(id: 42, state: 'enabled'))
    end

    describe 'when locale of user is LTR' do
      let(:locale) { spanish }

      it "generates password reset emails" do
        UsersMailer.any_instance.stubs(:recipient_locale).returns(translation_locales(:spanish))
        UsersMailer.deliver_admin_password_reset(user, 'token')
        refute ActionMailer::Base.deliveries.empty?

        email = ActionMailer::Base.deliveries.last

        assert_equal "minimum", user.account.route.subdomain

        assert_match /Contraseña de #{@minimum_account.name} restablecida/, email.subject

        body_by_content_type(email, Mime[:text]).must_include "Está recibiendo este mensaje de correo electrónico porque alguien ha restablecido la contraseña de su cuenta de Zendesk."
        ActionMailer::Base.deliveries.last.joined_bodies.must_include "https://minimum.zendesk-test.com/password/reset/token"
      end

      it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
        UsersMailer.deliver_admin_password_reset(user, 'token')
        email = ActionMailer::Base.deliveries.last
        email[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal "Admin Password Reset"
      end

      it 'adds LTR styling to email' do
        UsersMailer.deliver_admin_password_reset(user, 'token')
        email = ActionMailer::Base.deliveries.last

        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end
    end

    describe 'when locale of user is RTL' do
      let(:locale) { hebrew }

      it "adds RTL styling to email" do
        UsersMailer.deliver_admin_password_reset(user, 'token')
        email = ActionMailer::Base.deliveries.last

        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end

  describe "#admin_password_change" do
    let(:user) do
      user = User.find_by_name('Agent Minimum')
      user.update_attributes!(locale_id: locale.id)
      assert_equal locale.locale, user.locale
      user
    end

    before do
      Account.any_instance.stubs(:help_center_enabled?).returns(true)
      HelpCenter.
        stubs(:find_by_brand_id).
        returns(HelpCenter.new(id: 42, state: 'enabled'))
    end

    describe 'when locale of user is LTR' do
      let(:locale) { spanish }

      it "generates password change emails" do
        UsersMailer.any_instance.stubs(:recipient_locale).returns(translation_locales(:spanish))
        UsersMailer.deliver_admin_password_change(user, 'token')
        refute ActionMailer::Base.deliveries.empty?

        email = ActionMailer::Base.deliveries.last

        assert_equal "minimum", user.account.route.subdomain

        assert_match /Cree una contraseña para #{user.account.route.brand.name}/, email.subject

        body_by_content_type(email, Mime[:text]).must_include "Haga clic en el vínculo a continuación para verificar su identidad y elegir una contraseña"
        ActionMailer::Base.deliveries.last.joined_bodies.must_include "https://minimum.zendesk-test.com/password/create/token"
      end

      it "generates branded password change emails" do
        brand = FactoryBot.create(:brand, account_id: user.account.id, subdomain: 'wombats', name: "Wombat Brand", active: true)
        UsersMailer.any_instance.stubs(:recipient_locale).returns(translation_locales(:spanish))

        UsersMailer.deliver_admin_password_change(user, 'token', brand.id)
        refute ActionMailer::Base.deliveries.empty?

        email = ActionMailer::Base.deliveries.last

        assert_equal "minimum", user.account.route.subdomain

        assert_match /Cree una contraseña para Wombat Brand/, email.subject
        body_by_content_type(email, Mime[:text]).must_include "Haga clic en el vínculo a continuación para verificar su identidad y elegir una contraseña"
        ActionMailer::Base.deliveries.last.joined_bodies.must_include "https://wombats.zendesk-test.com/password/create/token"
        assert_equal ["support@wombats.zendesk-test.com"], email.from
        ActionMailer::Base.deliveries.last.joined_bodies.must_include "Este correo electrónico es un servicio de Wombat Brand"
      end

      it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
        UsersMailer.deliver_admin_password_change(user, 'token')
        email = ActionMailer::Base.deliveries.last
        email[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal "Admin Password Change"
      end

      it 'adds LTR styling to email' do
        UsersMailer.deliver_admin_password_change(user, 'token')
        email = ActionMailer::Base.deliveries.last

        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end
    end

    describe 'when locale of user is RTL' do
      let(:locale) { hebrew }

      it 'adds RTL styling to email' do
        UsersMailer.deliver_admin_password_change(user, 'token')
        email = ActionMailer::Base.deliveries.last

        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end

  describe '#multiproduct_admin_password_change' do
    let(:user) do
      user = User.find(accounts(:shell_account_without_support).owner_id)
      user.update_attributes!(locale_id: locale.id)
      assert_equal locale.locale, user.locale
      user
    end

    describe 'when locale of user is LTR' do
      let(:locale) { spanish }

      it 'generates password change emails' do
        UsersMailer.any_instance.stubs(:recipient_locale).returns(translation_locales(:spanish))
        UsersMailer.deliver_multiproduct_admin_password_change(user, 'token')
        refute ActionMailer::Base.deliveries.empty?

        email = ActionMailer::Base.deliveries.last

        assert_equal 'shellaccountwithoutsupport', user.account.subdomain

        assert_match /Cree una contraseña para #{user.account.name}/, email.subject

        body_by_content_type(email, Mime[:text]).must_include 'Haga clic en el vínculo a continuación para verificar su identidad y elegir una contraseña'
        ActionMailer::Base.deliveries.last.joined_bodies.must_include 'https://shellaccountwithoutsupport.zendesk-test.com/password/create/token'
      end

      it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
        UsersMailer.deliver_admin_password_change(@minimum_end_user, 'token')
        email = ActionMailer::Base.deliveries.last
        email[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal 'Admin Password Change'
      end

      it 'adds LTR styling to email' do
        UsersMailer.deliver_multiproduct_admin_password_change(user, 'token')
        email = ActionMailer::Base.deliveries.last

        assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
      end
    end

    describe 'when locale of user is RTL' do
      let(:locale) { hebrew }

      it 'adds RTL styling to email' do
        UsersMailer.deliver_multiproduct_admin_password_change(user, 'token')
        email = ActionMailer::Base.deliveries.last

        assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
      end
    end
  end
end
