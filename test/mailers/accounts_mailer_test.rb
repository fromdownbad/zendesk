require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered! uncovered: 7

describe AccountsMailer do
  fixtures :accounts, :account_property_sets, :subscriptions, :users,
    :user_identities, :translation_locales, :remote_authentications, :recipient_addresses

  include MailerTestHelper

  let(:account) { accounts(:minimum) }
  let(:sent) do
    assert_equal 1, ActionMailer::Base.deliveries.size
    ActionMailer::Base.deliveries.first
  end
  let(:html_body) { sent.parts.last.body.to_s }
  let(:locale) do
    locale_stub = stub
    locale_stub.expects(:id).returns('bleep-blorp')
    locale_stub
  end
  let(:account_url) { "https://#{account.subdomain}.#{Zendesk::Configuration.fetch(:host)}" }

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries.clear
    @minimum_end_user = users(:minimum_end_user)
    @minimum_agent = users(:minimum_agent)
    @translation = translation_locales(:japanese)

    account.text_mail_template += "My TEXT template is TOO BIG"
    account.html_mail_template += "My HTML template is TOO BIG"
    account.save!

    @minimum_authentication = remote_authentications(:minimum_remote_authentication)
    @minimum_authentication.stubs(auth_mode: RemoteAuthentication::JWT)
  end

  describe "#welcome_account_owner" do
    def self.it_delivers_the_normal_trial_welcome_email
      it "delivers the normal trial welcome email" do
        path = "#{account_url}/verification/email"
        AccountsMailer.deliver_welcome_account_owner(@minimum_end_user)

        assert_includes html_body, path
        assert_includes html_body, account.subdomain
        refute_includes html_body, @minimum_end_user.name
        assert_includes html_body, 'Your Zendesk trial awaits.'
        assert_includes html_body, 'May your trial not be trivial'
        assert sent.from.include?("support@support.zendesk-test.com")
        assert_equal('multipart/alternative', sent.mime_type)
      end
    end

    describe 'welcome email' do
      it_delivers_the_normal_trial_welcome_email
    end

    describe "with priority_mail" do
      it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
        path = "https://#{account.subdomain}.#{Zendesk::Configuration.fetch(:host)}/verification/email"
        AccountsMailer.deliver_welcome_account_owner(@minimum_end_user, priority_mail: "Shell Account Owner Welcome Email")
        assert_includes html_body, path
        assert sent.from.include?("support@support.zendesk-test.com")
        assert_equal('multipart/alternative', sent.mime_type)
        sent[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal "Shell Account Owner Welcome Email"
      end
    end

    describe "welcome email for buy now customers" do
      describe "when Zendesk Support 'Buy Now' option was selected by customer" do
        before do
          account.trial_extras.create!(key: "buy_now", value: "true")
          account.trial_extras.create!(key: "product_sign_up", value: "zendesk_support")
        end

        it "delivers buy now email for buy now customers" do
          path = "#{account_url}/verification/email"
          AccountsMailer.deliver_welcome_account_owner(@minimum_end_user)

          assert_includes html_body, path
          assert_includes html_body, account.subdomain
          refute_includes html_body, @minimum_end_user.name
          assert_includes html_body, 'Welcome to the start of a beautiful relationship with your customers'
          assert_includes html_body, "Just verify your email and you'll be on your way to a better help desk solution for routing, prioritizing, and solving customer support tickets."
          assert sent.from.include?("support@support.zendesk-test.com")
          assert_equal('multipart/alternative', sent.mime_type)
        end
      end

      describe "when 'Buy Now' option was not selected by customer" do
        it_delivers_the_normal_trial_welcome_email
      end
    end

    describe "when Zendesk Suite 'Buy Now' option was selected by customer" do
      before do
        account.trial_extras.create!(key: "buy_now", value: "true")
        account.trial_extras.create!(key: "product_sign_up", value: "zendesk_suite")
      end

      describe_with_arturo_enabled :billing_buy_now_zendesk_suite do
        it "delivers buy now email for buy now customers" do
          path = "#{account_url}/verification/email"
          AccountsMailer.deliver_welcome_account_owner(@minimum_end_user)

          assert_includes html_body, path
          assert_includes html_body, account.subdomain
          refute_includes html_body, @minimum_end_user.name
          assert_includes html_body, 'Welcome to the start of a beautiful relationship with your customers'
          assert_includes html_body, "Just verify your email and you'll be on your way to a better help desk solution for routing, prioritizing, and solving customer support tickets."
          assert sent.from.include?("support@support.zendesk-test.com")
          assert_equal('multipart/alternative', sent.mime_type)
        end
      end

      describe_with_arturo_disabled :billing_buy_now_zendesk_suite do
        it_delivers_the_normal_trial_welcome_email
      end
    end
  end

  describe "#owner_email_change" do
    it "delivers" do
      recipient_address_email = recipient_addresses(:default).email
      AccountsMailer.deliver_owner_email_change(@minimum_agent, recipient_address_email)

      assert_includes sent.subject, 'Changes to your Zendesk account'
      assert_includes sent.from, 'support@support.zendesk-test.com'
      assert_includes sent.to, recipient_address_email

      assert_includes sent.body, account.subdomain
      assert_includes sent.body, 'transfer ownership'

      sent.content_type.must_match /^text\/html;/
    end
  end

  describe "#remote_authentication_activated" do
    it "delivers" do
      AccountsMailer.deliver_remote_authentication_activated(account, @minimum_authentication)

      assert_includes sent.joined_bodies, "remote authentication has been activated"

      body_by_content_type(sent, Mime[:text]).must_include "My TEXT template is TOO BIG"
      body_by_content_type(sent, Mime[:html]).must_include "My HTML template is TOO BIG"
      sent.content_type.must_match /^multipart\/alternative;/
    end
  end

  describe "#external_email_credential_test" do
    it "delivers" do
      credential = account.external_email_credentials.new(
        username: "foo@bar.com",
        initial_import: true,
        current_user: users(:minimum_agent)
      ).tap do |external_email_credential|
        external_email_credential.encrypted_value = "unencrypted_token"
        external_email_credential.save!
      end
      ActionMailer::Base.deliveries.clear
      AccountsMailer.deliver_external_email_credential_test(account, @minimum_agent, credential)
      assert_equal 1, ActionMailer::Base.deliveries.size

      text = body_by_content_type(sent, Mime[:text])
      html = body_by_content_type(sent, Mime[:html])

      assert_includes html, ".com/agent/admin/email"
      assert_includes text, "email settings page"

      refute_includes text, "<"
      assert_includes html, "<a"
      assert_includes html, "<br>"
    end
  end

  describe "#forwarding_status_verification" do
    it "delivers" do
      recipient_address = recipient_addresses(:not_default)
      AccountsMailer.deliver_forwarding_status_verification(account, recipient_address)

      assert_equal ["support@support.zendesk.com"], sent.reply_to
      assert sent.from.include?("support@support.zendesk-test.com")
      assert sent.header["X-Mailer"] # keep normal headers
      assert_equal "ZFVTest", sent.header["X-Zendesk-Verification"].value # marked so it passes mail_parsing_queue blocker
      assert_includes sent.body.encoded, recipient_address.brand.default_recipient_address.email
    end
  end

  describe "#gmail_forwarding_verification_notification" do
    it "delivers" do
      token = '123456789'
      AccountsMailer.deliver_gmail_forwarding_verification_notification(
        account,
        @minimum_agent,
        token
      )

      assert_includes sent.body, token
      assert_equal ["support@support.zendesk-test.com"], sent.from
    end
  end

  describe "#gmail_sending_rate_limit_reached" do
    it "delivers" do
      AccountsMailer.deliver_gmail_sending_rate_limit_reached(
        account,
        'foo@bar.com'
      )

      assert_equal ["support@support.zendesk-test.com"], sent.from
    end
  end

  describe "#update_subscription_request" do
    let(:html) { body_by_content_type(sent, Mime[:html]) }

    describe "locale handling" do
      before do
        User.any_instance.expects(:translation_locale).returns(locale)
      end

      it "passes the owner locale to render_email_parts" do
        ApplicationMailer.any_instance.expects(:render_email_parts).with(
          account,
          is_a(String),
          is_a(Hash),
          anything,
          nil,
          locale
        ).once

        AccountsMailer.deliver_low_seat_count_notification(account)
      end
    end

    it "delivers" do
      AccountsMailer.deliver_update_subscription_request(
        account,
        'email@example.com',
        'name',
        'role name',
        @minimum_agent.name
      )

      assert_equal ["support@support.zendesk-test.com"], sent.from
      assert_includes html, "zendesk.com/admin/billing/subscription"
    end

    describe 'for staging' do
      let(:environment) { stub }

      it 'delivers with staging subscription link' do
        Rails.env.stubs(:staging?).returns(true)
        AccountsMailer.deliver_update_subscription_request(
          account,
          'email@example.com',
          'name',
          'role name',
          @minimum_agent.name
        )
        assert_includes html, "zendesk-staging.com/admin/billing/subscription"
      end
    end
  end

  describe "#low_seat_count_notification" do
    let(:html) { body_by_content_type(sent, Mime[:html]) }

    describe "locale handling" do
      before do
        User.any_instance.expects(:translation_locale).returns(locale)
      end

      it "passes the owner locale to render_email_parts" do
        ApplicationMailer.any_instance.expects(:render_email_parts).with(
          account,
          is_a(String),
          is_a(Hash),
          anything,
          nil,
          locale
        ).once

        AccountsMailer.deliver_low_seat_count_notification(account)
      end
    end

    describe "self service account" do
      it "delivers with expected content" do
        AccountsMailer.deliver_low_seat_count_notification(account)

        assert_equal ["support@support.zendesk-test.com"], sent.from
        assert_includes html, "zendesk.com/admin/billing/subscription"
      end

      describe "for staging" do
        it "delivers with staging subscription link" do
          Rails.env.stubs(:staging?).returns(true)
          AccountsMailer.deliver_low_seat_count_notification(account)

          assert_includes html, "zendesk-staging.com/admin/billing/subscription"
        end
      end

      describe_with_arturo_enabled :allow_seats_remaining_suite do
        it "has subdomain in params zsub of url" do
          AccountsMailer.deliver_low_seat_count_notification(account)

          assert_includes html, "zsub=minimum"
        end

        it "has segment tracking cpgid " do
          AccountsMailer.deliver_low_seat_count_notification(account)

          assert_includes html, "cpgid=lowseatcount_email"
        end

        describe "environment" do
          before do
            Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(1)
            Zendesk::Configuration.stubs(:fetch).with(:host).returns(host)
            AccountsMailer.deliver_low_seat_count_notification(account)
          end

          describe "when environment is staging" do
            let(:host) { "zendesk-staging.com" }

            it "delivers with correct subscription link" do
              assert_includes html, "billing.#{host}/billing/manage"
            end
          end

          describe "when environment is production" do
            let(:host) { "zendesk.com" }

            it "renders the correct subscription link" do
              assert_includes html, "billing.#{host}/billing/manage"
            end
          end
        end
      end
    end

    describe "sales assisted account" do
      before do
        Subscription.any_instance.stubs(sales_model_value: ZBC::States::SalesModel::ASSISTED)
      end

      it "delivers with expected content" do
        AccountsMailer.deliver_low_seat_count_notification(account)

        assert_equal ["support@support.zendesk-test.com"], sent.from
        assert_includes html, "https://support.zendesk.com/hc/en-us/articles/360026614173"
      end

      describe "for staging" do
        let(:environment) { stub }

        it "delivers with staging subscription link" do
          Rails.env.stubs(:staging?).returns(true)
          AccountsMailer.deliver_low_seat_count_notification(account)

          assert_includes html, "https://support.zendesk.com/hc/en-us/articles/360026614173"
        end
      end
    end
  end
end
