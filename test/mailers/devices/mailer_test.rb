require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Devices::Mailer do
  fixtures :accounts, :users, :user_identities

  before { ActionMailer::Base.perform_deliveries = true }

  describe '#new_device_notification' do
    describe "in a legacy account" do
      it 'generates a valid email with a link to support' do
        user = users(:minimum_agent)
        email, device = generate_new_device_mail(accounts(:minimum), user)

        check_new_device_mail(email, device, user)
        assert_match /\/agent\/users\/#{user.id}\/devices_and_apps/, email.parts.last.body.to_s
      end
    end

    describe "in a multiproduct account" do
      it 'generates a valid email with no link to support' do
        Arturo.enable_feature!(:new_device_notification_for_multiproduct_accounts)
        user = users(:multiproduct_support_agent)
        email, device = generate_new_device_mail(accounts(:multiproduct), user)

        check_new_device_mail(email, device, user)
        refute_match /\/agent\/users\/#{user.id}\/devices_and_apps/, email.parts.last.body.to_s
      end
    end
  end

  private

  def generate_new_device_mail(account, user)
    params  = { ip: '1.2.3.4', user_agent: 'Mozilla/5.0' }
    device  = user.devices.create!(params.merge(account: account))

    [Devices::Mailer.create_new_device_notification(device), device]
  end

  def check_new_device_mail(email, device, user)
    plain = email.parts.find { |part| part.to_s.include? "Content-Type: text/plain;" }

    refute plain.to_s.include?('&#39;')
    assert_equal('multipart/alternative', email.mime_type)
    assert_equal("device-#{device.id}", email.header[Zendesk::OutgoingMail::Mailer::X_DELIVERY_CONTEXT].to_s)
    assert email.from.include?('support@support.zendesk-test.com')
    assert email.to.include?(user.email)
    assert_match /Zendesk sign-in detected from a new device/, email.parts.last.body.to_s
  end
end
