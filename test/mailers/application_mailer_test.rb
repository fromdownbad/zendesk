require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered! uncovered: 26

describe ApplicationMailer do
  fixtures :tickets, :translation_locales, :users, :accounts

  include MailerTestHelper

  class TestMailer < ApplicationMailer
    def with_return_path(account)
      set_headers(account)
      mail(
        :subject => 'SUB',
        :body => 'BODY',
        :from => 'from@bbb.com',
        'Return-Path' => 'return@bbb.com'
      )
    end

    def with_perform(account, setting)
      set_headers(account)
      mail(
        subject: "xxx",
        from: "xxx@xxx.com",
        to: "yyy@yyy.com",
        body: "Body"
      )

      message.perform_deliveries = setting
    end

    def with_ticket(account, ticket, extra)
      set_headers(account)
      @ticket = ticket
      mail(extra.merge(
        subject: 'SUB',
        body: 'BODY',
        from: 'from@bbb.com'
      ))
    end
  end

  let(:title) { 'title' }
  let(:content) { 'content' }
  let(:locale) { ENGLISH_BY_ZENDESK }
  let(:account) { accounts(:minimum) }

  before { ActionMailer::Base.perform_deliveries = true }

  describe "an email recipient with a locale other than the current user's" do
    before do
      @locale = translation_locales(:brazilian_portuguese)
      account.stubs(:translation_locale).returns(@locale)

      user = users(:minimum_end_user)
      User.any_instance.stubs(:translation_locale).returns(@locale)
      user.identities[1..-1].map(&:destroy)
      identity = user.identities(:reload).first

      @outbound_welcome = UsersMailer.create_verify(identity)
    end

    it "translates subject to recipient's locale" do
      expected = I18n.t('txt.email.welcome.subject', locale: @locale)
      assert_match expected, @outbound_welcome.subject
    end

    it "translates footer to recipient's locale" do
      expected = I18n.t('txt.email.footer', locale: @locale)
      assert_match expected, @outbound_welcome.joined_bodies
    end
  end

  describe "an email recipient receiving an email using a brand name instead of the account name" do
    before do
      user = users(:minimum_end_user)
      brand = FactoryBot.create(:brand, account_id: user.account.id, subdomain: 'wombats', name: "Wombat Brand", active: true)
      @pass_change = UsersMailer.create_admin_password_change(user, 'token', brand.id)
    end

    it "uses the  brand name in the footer instead of the account name" do
      assert_match "Wombat Brand", @pass_change.joined_bodies
    end
  end

  describe "an email recipient receiving an email with no brand" do
    before do
      @mail = ApplicationMailer.create_generic(account, 'Sir Wombat <wombat@taz.com>', 'Lemur <lemur@tree.com>', 'subject stuff', 'Hey hey snowboard')
    end

    it "users the account name in the footer if brand is nil" do
      assert_match "This email is a service from Minimum account", body_by_content_type(@mail, Mime[:text])
    end
  end

  it "uses folded encoding for long quoted subjects" do
    long_subject = "시금치를  사랑햬 " * 6
    @mail = ApplicationMailer.create_generic(account, 'The Sailor Man <popeye@example.com>', 'Ahhhhh <olive@example.com>', long_subject, 'Mmmm...spanich')
    quoted_subject = "Subject: =?UTF-8?Q?=EC=8B=9C=EA=B8=88=EC=B9=98=EB=A5=BC?=\r\n =?UTF-8?Q?_=EC=82=AC=EB=9E=91=ED=96=AC_=EC=8B=9C=EA=B8=88=EC=B9=98=EB=A5=BC?=\r\n =?UTF-8?Q?_=EC=82=AC=EB=9E=91=ED=96=AC_=EC=8B=9C=EA=B8=88=EC=B9=98=EB=A5=BC?=\r\n =?UTF-8?Q?_=EC=82=AC=EB=9E=91=ED=96=AC_=EC=8B=9C=EA=B8=88=EC=B9=98=EB=A5=BC?=\r\n =?UTF-8?Q?_=EC=82=AC=EB=9E=91=ED=96=AC_=EC=8B=9C=EA=B8=88=EC=B9=98=EB=A5=BC?=\r\n =?UTF-8?Q?_=EC=82=AC=EB=9E=91=ED=96=AC_=EC=8B=9C=EA=B8=88=EC=B9=98=EB=A5=BC?=\r\n =?UTF-8?Q?_=EC=82=AC=EB=9E=91=ED=96=AC?="

    assert_includes @mail.encoded, quoted_subject

    assert_equal long_subject.squeeze.strip, Mail::Message.new(@mail.encoded).subject

    short_subject = '시금치를 사랑햬...'
    @mail = ApplicationMailer.create_generic(account, 'The Sailor Man <popeye@example.com>', 'Ahhhhh <olive@example.com>', short_subject, 'Mmmm...spanich')

    assert_equal short_subject, Mail::Message.new(@mail.encoded).subject
  end

  it "is able to parse encoded email addresses" do
    address = '"«●ŔŽ๏̯͡oй Al-Ghamdi" <popeye@example.com>' # mixed utf8 and ascii -> most edge cases covered (actual username)
    @mail = ApplicationMailer.create_generic(account, address, '"Jérôme Petazzoni" <olive@example.com>', 'A subject', 'Mmmm...spanich')

    encoded = Mail::Message.new(@mail.encoded)
    assert_equal ["popeye@example.com"], encoded.from
    assert_equal ["olive@example.com"], encoded.to

    expected = "To: =?UTF-8?B?SsOpcsO0bWUgUGV0YXp6b25p?= <olive@example.com>"

    assert_includes @mail.encoded.gsub("=?utf-8?", "=?UTF-8?"), expected
  end

  it "keeps markdown/newlines in body" do
    body = "<p>bar</p>\n<pre><code>baz\n</code></pre>" # markdown output
    @mail = ApplicationMailer.create_generic(account, 'olive@example.com', 'olive@example.com', 'A subject', body)

    encoded = Mail::Message.new(@mail.encoded)
    assert_includes encoded.html_part.body.to_s.gsub("\r\n", "\n"), body
  end

  # make sure rails 3.0 issue in old mailer api is fixed <-> https://github.com/rails/rails/pull/3090
  it "includes a boundary for multipart emails" do
    @mail = ApplicationMailer.create_generic(account, 'olive@example.com', 'olive@example.com', 'A subject', 'XXX')
    assert_includes @mail.encoded.gsub("\r\n", ""), "Content-Type: multipart/alternative; boundary="
    encoded = Mail::Message.new(@mail.encoded)
    assert encoded.text_part.body.to_s.present?
  end

  describe "new GoLion template" do
    let(:go_lion_color) { "#03363d" }
    let(:zendesk_green) { "#98c332" }
    let(:rtl_locale) { translation_locales(:hebrew) }

    let(:application_mailer) do
      application_mailer = ApplicationMailer.send(:new)
      application_mailer.account = account
      application_mailer
    end

    it 'uses the the new GoLion template' do
      html_body = application_mailer.send(:zendesk_templated_body, title, content, locale)
      html_body.wont_include zendesk_green
      html_body.must_include go_lion_color
    end

    it 'includes RTL styling when locale is RTL' do
      html_body = application_mailer.send(:zendesk_templated_body, title, content, rtl_locale)
      html_body.must_include 'text-align: right'
      html_body.wont_include 'text-align: left'
    end

    it 'includes default styling when locale is LTR' do
      html_body = application_mailer.send(:zendesk_templated_body, title, content, locale)
      html_body.must_include 'text-align: left'
      html_body.wont_include 'text-align: right'
    end
  end

  it 'renders html and text parts with appropriate templates and stripping' do
    account.text_mail_template += 'Popeye loves TEXT'
    account.html_mail_template += 'Bluto loves HTML'
    account.save!

    mail = ApplicationMailer.create_generic(account, 'The Sailor Man <popeye@example.com>', 'Ahhhhh <olive@example.com>', 'Subject', 'Mmmm...<blink>spanich</blink>')

    text_body = body_by_content_type(mail, Mime[:text])
    text_body.must_include "Popeye loves TEXT"
    assert_no_match /blink/, text_body

    html_body = body_by_content_type(mail, Mime[:html])
    html_body.must_include "Bluto loves HTML"
    html_body.must_include "<blink>"
  end

  describe "an email recipient with a rtl locale" do
    before do
      I18n.translation_locale.stubs(:rtl?).returns(true)
    end

    it 'renders styles and attributes in html body' do
      account.html_mail_template = <<~HTML
        <html>
        <head>
        <style>{{styles}}</style>
        </head>
        <body>
          <div {{attributes}}>{{delimiter}}</div>
          content:{{content}}
          <div>{{footer}}</div>
          <div>{{footer_link}}</div>
        </body>
        </html>
      HTML
      account.save!

      mail = ApplicationMailer.create_generic(account, 'The Sailor Man <popeye@example.com>', 'Ahhhhh <olive@example.com>', 'Subject', 'Mmmm...<blink>spanich</blink>')

      html_body = body_by_content_type(mail, Mime[:html])
      expected_styles = "body[dir=rtl] .directional_text_wrapper { direction: rtl; unicode-bidi: embed; }"
      html_body.must_include "content:Mmmm"
      html_body.must_include "<blink>"
      assert_match /dir=rtl/, html_body
      assert_match /<style>.*?<\/style>/m, html_body
      assert_includes html_body, expected_styles
    end
  end

  it "renders text and html parts as quoted-printable" do
    mail = ApplicationMailer.create_generic(account, 'popeye@example.com', 'olive@example.com', 'Subject', 'Mmmm...<blink>spanich</blink>')
    expected = ["quoted-printable", "quoted-printable"]
    actual = mail.encoded.scan(/Content-Transfer-Encoding: (.*)/).map(&:first).map(&:strip).map(&:downcase)
    assert_equal expected, actual - ["7bit"]
  end

  it "keeps full content of text and html" do
    body = 'XX<a b="x">a=∂</a>XX'
    mail = ApplicationMailer.create_generic(account, 'popeye@example.com', 'olive@example.com', 'Subject', body)

    encoded = mail.encoded
    assert_includes encoded, "XXa=3D=E2=88=82XX"
    assert_includes encoded, 'XX<a b=3D"x">a=3D=E2=88=82</a>XX'
  end

  it "does not blow up with nil addresses, because they happen once in a while in production" do
    ApplicationMailer.create_generic(account, "from@support.zendesk.com", [nil], "aaa", "aaa")
  end

  describe "use from as sender" do
    def mail(from = "from@example.com")
      ApplicationMailer.create_generic(account, from, "to@exmaple.com", "SUB", 'BOD')
    end

    it "does not overwrite Return-Path" do
      mail = TestMailer.create_with_return_path(account)
      assert_equal "return@bbb.com", mail.smtp_envelope_from
    end

    it "sends from mailer2 for @zendesk.com" do
      account.subdomain = 'press' # whitelisted to use @zendesk.com
      account.settings.owned_by_zendesk = true
      mail = mail("foo@zendesk-test.com")
      assert_equal "support@minimum.zendesk-test.com", mail.smtp_envelope_from
    end

    it "uses backup address for unverified" do
      mail = mail()
      assert_equal account.backup_email_address, mail.smtp_envelope_from
    end

    it "uses zendesk.com addresses because we have valid spf" do
      mail = mail("foo@foo.zendesk-test.com")
      assert_equal "foo@foo.zendesk-test.com", mail.smtp_envelope_from
    end

    it "uses spf-checked addresses" do
      recipient_address = account.recipient_addresses.last
      recipient_address.update_column(:spf_status_id, RecipientAddress::SPF_STATUS[:verified])
      recipient_address.update_column(:email, "from@example.com")
      mail = mail()
      assert_equal "from@example.com", mail.smtp_envelope_from
    end
  end

  describe "#encode_address_name" do
    before do
      @from = ApplicationMailer.encode_address_name(subject, account)
    end

    describe "with regular name" do
      subject { "The Method Man" }

      it "quotes it" do
        assert_equal "\"The Method Man\"", @from
      end
    end

    describe "with new lines" do
      subject { "The Method\nMan\n\n" }

      it "strips them" do
        assert_equal "\"The MethodMan\"", @from
      end
    end

    describe "with carriage returns" do
      subject { "The Method\r\nMan\n" }

      it "strips them" do
        assert_equal "\"The MethodMan\"", @from
      end
    end

    describe "with a single trailing period" do
      subject { "The Method Man." }

      it "strips it" do
        assert_equal "\"The Method Man\"", @from
      end
    end

    describe "with multiple trailing periods" do
      subject { "The Method Man.." }

      it "strips it" do
        assert_equal "\"The Method Man\"", @from
      end
    end

    describe "with multiple sets of trailing periods" do
      subject { "The Method Man. .. ... .. ." }

      it "strips it" do
        assert_equal "\"The Method Man\"", @from
      end
    end

    describe "with multiple sets of trailing periods and a trailing space" do
      subject { "The Method Man. .. ... .. . " }

      it "strips it" do
        assert_equal "\"The Method Man\"", @from
      end
    end

    describe "with multiple sets of trailing periods and trailing spaces" do
      subject { "The Method Man. .. ... .. .            " }

      it "strips it" do
        assert_equal "\"The Method Man\"", @from
      end
    end

    describe "escaping" do
      subject { "The Me\\thod (\",)" }

      it "escapes quotes" do
        assert_equal "\"The Me\\\\thod (,)\"", @from
      end
    end

    describe "when there is an @" do
      subject { "The Method@Man" }

      it "does not remove the @" do
        assert_equal "\"The Method@Man\"", @from
      end
    end

    describe_with_arturo_disabled :email_encode_address_name_fix do
      before { @from = ApplicationMailer.encode_address_name(subject, account) }

      describe "with multiple sets of trailing periods" do
        subject { "The Method Man. .. ... .. ." }

        it "does not strip trailing periods" do
          assert_equal "\"The Method Man. .. ... .. \"", @from
        end
      end

      describe "with multiple sets of trailing periods and a trailing space" do
        subject { "The Method Man. .. ... .. . " }

        it "does not strip trailing periods" do
          assert_equal "\"The Method Man. .. ... .. . \"", @from
        end
      end

      describe "with multiple sets of trailing periods and trailing spaces" do
        subject { "The Method Man. .. ... .. .            " }

        it "does not strip trailing periods" do
          assert_equal "\"The Method Man. .. ... .. .            \"", @from
        end
      end
    end
  end

  describe "#user_locale" do
    it "does not fail if user is blank" do
      ApplicationMailer.send(:new).send(:user_locale, nil, account)
    end
  end

  describe "#recipient_locale" do
    before do
      @spanish = translation_locales(:spanish)
      @application_mailer = ApplicationMailer.send(:new)
    end

    it "returns the locale of the user" do
      User.any_instance.expects(:translation_locale).returns(@spanish).once
      assert_equal @spanish, @application_mailer.send(:recipient_locale, User.new)
    end

    it "returns the locale of the account" do
      Account.any_instance.expects(:translation_locale).returns(@spanish).once
      assert_equal @spanish, @application_mailer.send(:recipient_locale, Account.new)
    end

    it "returns the locale of the suspeded ticket" do
      SuspendedTicket.any_instance.expects(:translation_locale).returns(@spanish).once
      assert_equal @spanish, @application_mailer.send(:recipient_locale, SuspendedTicket.new)
    end

    it "returns the locale of the user associated with the user identity" do
      UserIdentity.any_instance.stubs(:user).returns(User.new)
      User.any_instance.expects(:translation_locale).returns(@spanish).once
      assert_equal @spanish, @application_mailer.send(:recipient_locale, UserIdentity.new)
    end

    it "returns the locale of the user associated with the email" do
      minimum_account = accounts(:support)
      @application_mailer.expects(:user_from_recipient).with("test@zendesk.com", minimum_account).returns(User.new)
      User.any_instance.expects(:translation_locale).returns(@spanish).once
      assert_equal @spanish, @application_mailer.send(:recipient_locale, "test@zendesk.com", minimum_account)
    end

    it "calls best_locale_for_group if item is an array" do
      minimum_account = accounts(:support)
      @application_mailer.expects(:best_locale_for_group).with(["a", "b", "c"], minimum_account).returns(@spanish)
      assert_equal @spanish, @application_mailer.send(:recipient_locale, ["a", "b", "c"], minimum_account)
    end

    it "returns the account locale if no match is found and the account is present" do
      minimum_account = accounts(:support)
      minimum_account.expects(:translation_locale).returns(@spanish)
      assert_equal @spanish, @application_mailer.send(:recipient_locale, nil, minimum_account)
    end

    it "returns the ENGLISH_BY_ZENDESK if no match is found" do
      assert_equal ENGLISH_BY_ZENDESK, @application_mailer.send(:recipient_locale, nil)
    end
  end

  describe "#best_locale_for_group" do
    let(:portuguese) { translation_locales(:brazilian_portuguese) }
    let(:spanish) { translation_locales(:spanish) }
    let(:spanish_user) { users(:minimum_agent) }

    def locale_for_group(recipients)
      ApplicationMailer.send(:new).send(:best_locale_for_group, recipients, account)
    end

    it "finds locale for recipients as emails" do
      ApplicationMailer.any_instance.expects(:user_locale).with(spanish_user.email, account).returns(spanish)
      assert_equal spanish, locale_for_group([spanish_user.email])
    end

    it "finds locale for recipients as users" do
      ApplicationMailer.any_instance.expects(:user_locale).with(spanish_user, account).returns(spanish)
      assert_equal spanish, locale_for_group([spanish_user])
    end

    it "returns the account locale if the second most popular locale is the account locale" do
      portuguese_user = spanish_user.dup
      portuguese_user.stubs(:translation_locale).returns(portuguese)
      spanish_user.stubs(:translation_locale).returns(spanish)
      Account.any_instance.stubs(:translation_locale).returns(portuguese)

      assert_equal portuguese, locale_for_group([portuguese_user, spanish_user])
    end

    it "returns the most popular locale from multiple locales" do
      spanish_user_2 = spanish_user.dup
      spanish_user.stubs(:translation_locale).returns(spanish)
      spanish_user_2.stubs(:translation_locale).returns(spanish)

      portuguese_user = users(:minimum_end_user)
      portuguese_user.stubs(:translation_locale).returns(portuguese)

      assert_equal spanish, locale_for_group([spanish_user, spanish_user_2, portuguese_user])
    end
  end

  describe "prevent mail delivery to invalid domains" do
    before do
      @delivery_method = mock
      Mail::Message.any_instance.stubs(delivery_method: @delivery_method)
      Zendesk::PreventEmailsToInvalidDomains.stubs(:test_env?).returns(false)
    end

    it "delivers to valid domains" do
      @delivery_method.expects(:deliver!)
      deliver_to('olive@bar.com')
    end

    it "delivers to valid domains (that may look invalid)" do
      @delivery_method.expects(:deliver!)
      deliver_to('mario@superexample.com')
    end

    it 'delivers to @zendesk.com domains' do
      @delivery_method.expects(:deliver!)
      deliver_to('support@zendesk.com')
    end

    describe "having @example.com recipients" do
      it "bails out when the last recipient is invalid" do
        @delivery_method.expects(:deliver!).never
        deliver_to('customer@valid.com; nobody@example.com')
      end

      it "bails out when all recipients are invalid" do
        @delivery_method.expects(:deliver!).never

        deliver_to('olive@example.com')
        deliver_to('olive@other.example.com')
        deliver_to('org@example.org; com@example.com')
        deliver_to('org@example.org; com@example.com; net@example.net')
      end

      it "is dropping the entire email when the last recipient is invalid" do
        @delivery_method.expects(:deliver!).never
        deliver_to('customer@valid.com; nobody@example.com')
      end

      it "delivers to valid domains when the first recipient is invalid" do
        @delivery_method.expects(:deliver!).once
        deliver_to('nobody@example.com; customer@valid.com')
      end
    end

    it "bails out when attempting to deliver to mailer-daemon@anywhere.com" do
      @delivery_method.expects(:deliver!).never
      deliver_to('mailer-daemon@anywhere.com')
    end
  end

  describe "prevent_no_from" do
    it "stops emails without from" do
      assert_raises RuntimeError do
        ApplicationMailer.deliver_generic(account, nil, 'recipient@zendesk.com', title, content)
      end.message.must_include "No from specified"
    end
  end

  describe "perform_deliveries vs deprecated mailer" do
    it "is on by default" do
      TestMailer.deliver_with_perform(account, true)
      assert_equal 1, ActionMailer::Base.deliveries.size
    end

    it "does not deliver when off" do
      TestMailer.deliver_with_perform(account, false)
      assert_equal 0, ActionMailer::Base.deliveries.size
    end
  end

  describe "#locale_is_rtl?" do
    let(:mailer) { ApplicationMailer.send(:new) }
    let(:user) { users(:minimum_end_user) }

    it "returns false if user is nil" do
      refute mailer.send(:locale_is_rtl?, nil)
    end

    it "returns false if the rtl Arturo feature is not enabled" do
      refute mailer.send(:locale_is_rtl?, user)
    end

    it "returns false when the user's translation locale is ltr" do
      refute mailer.send(:locale_is_rtl?, user)
    end

    describe "when the user's translation locale is rtl" do
      before do
        Arturo.enable_feature!(:rtl_lang_html_attr)
        user.translation_locale.stubs(:rtl?).returns(true)
      end

      it "returns true when the rtl Arturo feature is enabled" do
        assert mailer.send(:locale_is_rtl?, user)
      end
    end
  end

  describe "#render_email_parts" do
    let(:mailer) { ApplicationMailer.send(:new) }
    let(:format) { stub }
    let(:locale) { stub }

    before do
      format.stubs(:text)
      format.stubs(:html)
    end

    it "accepts a hash for email content" do
      content = {text: "hello!", html: "<h1>hello!!</h1>"}
      mailer.send(:render_email_parts, account, title, content, format)
    end

    it "accepts a string for email content" do
      content = "<p>only one content version</p>"
      mailer.send(:render_email_parts, account, title, content, format)
    end

    it "passes expected arguments (like locale) to footer method" do
      ApplicationMailer.any_instance.expects(:footer).with(
        account,
        any_of(true, false),
        nil,
        locale,
        nil
      ).times(2)

      mailer.send(:render_email_parts, account, title, content, format, nil, locale)
    end

    describe "with a shell account" do
      let(:account) { accounts(:shell_account_without_support) }

      it 'uses the default plain text and html template' do
        mailer.send(:render_email_parts, account, title, content, format)
      end
    end
  end

  describe "#use_email_outbound_blocker" do
    let(:mailer) do
      mailer = ApplicationMailer.send(:new)
      mailer.account = account
      mailer
    end

    before do
      Account.any_instance.stubs(has_email_outbound_blocker?: true)
    end

    describe "when account is nil" do
      let(:account) { nil }

      it "returns false" do
        refute mailer.use_email_outbound_blocker?
      end
    end

    describe "when the account has a non-trial subscription" do
      let(:account) { accounts(:minimum) }

      it "returns false" do
        refute mailer.use_email_outbound_blocker?
      end
    end

    describe "when the account has a trial subscription" do
      let(:account) { accounts(:trial) }

      it "returns true" do
        assert mailer.use_email_outbound_blocker?
      end
    end

    describe "when the account is a shell account" do
      let(:account) { accounts(:shell_account_without_support) }

      before do
        assert account.multiproduct?
      end

      describe "when the account is serviceable" do
        before do
          Account.any_instance.stubs(is_serviceable?: true)
        end

        it "returns false" do
          refute mailer.use_email_outbound_blocker?
        end
      end

      describe "when the account is not serviceable" do
        before do
          Account.any_instance.stubs(is_serviceable?: false)
        end

        it "returns true" do
          assert mailer.use_email_outbound_blocker?
        end
      end
    end
  end

  describe "#invoke_outbound_account_circuit_breaker?" do
    let(:account) { accounts(:minimum) }
    let(:mailer) do
      mailer = ApplicationMailer.send(:new)
      mailer.account = account
      mailer
    end

    describe_with_arturo_enabled :email_outbound_account_circuit_breaker do
      it { assert mailer.invoke_outbound_account_circuit_breaker? }
    end

    describe_with_arturo_disabled :email_outbound_account_circuit_breaker do
      it { refute mailer.invoke_outbound_account_circuit_breaker? }
    end
  end

  describe "#allow_logged_with_email_id_smtp_delivery_method?" do
    let(:account) { accounts(:minimum) }
    let(:mailer) do
      mailer = ApplicationMailer.send(:new)
      mailer.account = account
      mailer
    end

    describe 'when email_allow_logged_with_email_id_smtp_delivery_method arturo is enabled' do
      it { assert mailer.allow_logged_with_email_id_smtp_delivery_method? }
    end

    describe_with_arturo_disabled :email_allow_logged_with_email_id_smtp_delivery_method do
      it { refute mailer.allow_logged_with_email_id_smtp_delivery_method? }
    end
  end

  describe "#fallback_to_logged_smtp_delivery_method?" do
    let(:account) { accounts(:minimum) }
    let(:mailer) do
      mailer = ApplicationMailer.send(:new)
      mailer.account = account
      mailer
    end

    describe 'when email_fallback_to_logged_smtp_delivery_method arturo is enabled' do
      it { assert mailer.fallback_to_logged_smtp_delivery_method? }
    end

    describe_with_arturo_disabled :email_fallback_to_logged_smtp_delivery_method do
      it { refute mailer.fallback_to_logged_smtp_delivery_method? }
    end
  end

  describe "when attachments are added to the email" do
    let(:mailer) { ApplicationMailer.send(:new) }
    let(:attachment) { %w[normal.jpg] }

    describe "when a NameError exception is raised" do
      before do
        attachment.stubs(:inline?).returns(true)
        attachment.stubs(:current_data).returns(12345)
        attachment.stubs(:display_filename).returns('normal.jpg')
        mailer.stubs(:attachments).raises(NameError.new)
      end
      it "increments statsd" do
        Rails.application.config.statsd.client.expects(:increment).with('attachment_download.attachments')
        Rails.application.config.statsd.client.expects(:increment).with('attachment_errors', tags: %w[attachment:normal.jpg])
        assert_raises NameError do
          mailer.send(:add_attachment_to_email, attachment)
        end
      end
    end
  end

  private

  def deliver_to(to)
    # ApplicationMailer.deliver_generic(<account>, <from>, <to>, <subject>, <body>)
    ApplicationMailer.deliver_generic(account, 'popeye@example.com', to, 'Subject', 'BODY')
  end
end
