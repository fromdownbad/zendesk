require_relative "../support/test_helper"

SingleCov.covered!

describe WelcomeAgentMailer do
  fixtures :accounts, :users, :user_identities, :account_property_sets

  before do
    ActionMailer::Base.perform_deliveries = true
    @account = accounts(:minimum)
    @user = users(:minimum_agent)
    @user_identity = @user.identities.first
  end

  describe "#google_verify" do
    it "for agent contains correct verification link" do
      email = WelcomeAgentMailer.create_google_verify(@user_identity)
      email.joined_bodies.must_include "#{@account.authentication_domain}/access/google"
    end

    it "for agent contains appropriate subject" do
      email = WelcomeAgentMailer.create_google_verify(@user_identity)
      email.subject.must_equal "Admin Man has invited you to try Zendesk"
    end
  end
end
