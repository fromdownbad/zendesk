require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.not_covered!

describe 'DKIM mailer support' do
  include MailerTestHelper

  fixtures :accounts, :recipient_addresses, :account_settings

  it 'uses selector and private key from zendesk.yml' do
    selector = Zendesk::Configuration.dig(:dkim, :selector)
    private_key = OpenSSL::PKey::RSA.new File.read(Zendesk::Configuration.dig(:dkim, :private_key))

    assert ['zendesk1', 'zendesk2'].include?(selector)

    assert_equal selector, Dkim.selector
    assert_equal private_key.to_der, Dkim.private_key.to_der
  end

  it 'has default domain' do
    assert_equal Zendesk::Configuration.fetch(:host), Dkim.domain
  end

  it 'signs consistently' do
    Timecop.freeze("2015-01-01") do
      ApplicationMailer.any_instance.expects(:unique_message_id).returns('1234@mentor-test.com')
      Mail.expects(:random_tag).returns "random-tag"
      mail = ApplicationMailer.create_generic(accounts(:minimum), "foo@bar.com", "foo@bar.com", "foo@bar.com", "foo@bar.com")
      mail['Dkim-Signature'].to_s.must_equal " v=1; a=rsa-sha256; c=relaxed/relaxed; d=zendesk-test.com; q=dns/txt; s=zendesk1; t=1420070400; bh=R2XPApxksAF/HfNRnTp5xCI+durPjD+mdWQv2UiBXfA=; h=date:from:to:message-id:subject:mime-version:content-type:content-transfer-encoding; b=RxKUZaGcSe4vnNzwpUYzVimGRGpdeK5HZS0/Dst7y2WJCszMZCSkMU+sf1mnHp2cY1op0vNj/H6giKU0aWQzyRE8p3g5dGdObW4TEb58R2lbXGlMiSkh50T+WEJXW5/cLjVQ1Gk1rYG2lBNgX257qfYnwcdLyCoTSgXDEhyThnE="
    end
  end
end
