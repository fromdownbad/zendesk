require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered!

describe MonitorMailer do
  fixtures :accounts, :tickets

  include MailerTestHelper

  before do
    @account = accounts(:minimum)
    ActionMailer::Base.perform_deliveries = true
  end

  it "sends an uptime notification" do
    message = 'Last conversion was 1 hour ago.'
    MonitorMailer.deliver_uptime_notification(
      'account_id' => Account.system_account_id,
      'shard_id' => @account.shard_id,
      'facility' => 'Facebook',
      'message' => message
    )
    refute ActionMailer::Base.deliveries.empty?
    sent = ActionMailer::Base.deliveries.first
    assert_equal sent.subject, "[Uptime][#{Rails.env}][Pod1] No Facebook conversions in the past 30 minutes"
    assert sent.from.include?("ocean@support.zendesk.com")
    assert sent.to.include?("ocean@zendesk.com")
    assert_includes sent.body, message
  end

  it "sends an uptime notification with no message" do
    MonitorMailer.deliver_uptime_notification('account_id' => Account.system_account_id, 'facility' => 'Facebook')
    sent = ActionMailer::Base.deliveries.first
    assert sent.from.include?("ocean@support.zendesk.com")
    assert sent.to.include?("ocean@zendesk.com")
    assert_equal '', sent.body.to_s
  end

  it "sends report for user view query test" do
    MonitorMailer.deliver_user_view_query_test_report(@account, "Account,Duration1,Duration2\n")
    refute ActionMailer::Base.deliveries.empty?
    sent = ActionMailer::Base.deliveries.first
    assert_equal sent.subject, "[Report][#{Rails.env}][Pod1] User view query tester results"
    assert sent.from.include?("ocean@support.zendesk.com")
    assert sent.to.include?("ocean@zendesk.com")
    assert_equal '', sent.body.to_s

    assert_equal 1, sent.attachments.length
    attachment = sent.attachments.first
    assert_equal "Account,Duration1,Duration2\n", attachment.decode_body
  end

  it "sends a notification email" do
    options = {
      'subdomain' => 'minimum',
      'prop_key' => 'limit_1',
      'expires_on' => '2020-01-21',
      'owner_email' => 'abc@zendesk.com'
    }
    message = "This conditional rate limit has been removed.\n" + "key: limit_1\nsubdomain: minimum\nexpired: 2020-01-21"
    MonitorMailer.deliver_rate_limit_notification(@account, options)
    refute ActionMailer::Base.deliveries.empty?
    sent = ActionMailer::Base.deliveries.first
    assert_equal "conditional rate limit <limit_1> has been removed", sent.subject
    assert sent.to.include?("abc@zendesk.com")
    assert_includes sent.body, message
  end
end
