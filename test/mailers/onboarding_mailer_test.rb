require_relative '../support/test_helper'
require_relative '../support/mailer_test_helper'

SingleCov.covered!

describe OnboardingMailer do
  include MailerTestHelper

  fixtures :all
  let(:account) { accounts(:minimum) }
  let(:user)    { users(:minimum_admin_not_owner) }
  let(:emails)  { 'email_1@example.com,email_2@example.com' }
  let(:snippet) { 'web_widget_snippet' }
  let(:domain)  { 'example.com' }

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries         = []
    stub_request(:get, %r{/snippets/web_widget/minimum.example.com}).
      to_return(status: 200, body: '', headers: {})
  end

  {
    deliver_web_widget_setup_info: 'txt.email.onboarding_mailer.web_widget.setup_info.subject',
    deliver_connect_setup_info: 'txt.email.onboarding_mailer.connect.setup_info.subject'
  }.each do |method, subject_key|
    describe "##{method}" do
      let(:subject) { I18n.t(subject_key, locale: account.translation_locale) }

      before do
        ActionMailer::Base.deliveries.clear
      end

      it 'sends email to provided emails' do
        OnboardingMailer.send(method, user, domain, emails, false)
        to_emails = ActionMailer::Base.deliveries.select { |email| email.subject == subject }.first.to
        assert_equal 2, to_emails.count
        assert to_emails.all? { |email| emails.include?(email) }
      end

      it 'does NOT cc self' do
        emails = 'dev_email1@example.com,dev_email2@example.com'
        OnboardingMailer.send(method, user, domain, emails, false)
        assert_empty(ActionMailer::Base.deliveries.select { |email| email.subject == subject }.first.cc)
      end

      it 'does cc self' do
        emails = 'dev_email1@example.com,dev_email2@example.com'
        OnboardingMailer.send(method, user, domain, emails, true)
        assert_equal user.email, ActionMailer::Base.deliveries.select { |email| email.subject == subject }.first.cc.first
      end
    end
  end
end
