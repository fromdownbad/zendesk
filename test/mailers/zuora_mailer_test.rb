require_relative "../support/test_helper"

SingleCov.covered!

describe ZuoraMailer do
  fixtures :accounts, :account_property_sets, :subscriptions, :users, :user_identities, :tickets, :translation_locales

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    @account = accounts(:minimum)
    @subscription = @account.subscription
  end

  it "sends mail with link to zuora pending changes" do
    ZuoraMailer.deliver_approval_mail(@account)
    sent = ActionMailer::Base.deliveries.first
    sent.body.to_s.must_include "https://monitor.zendesk-test.com/zuora_subscriptions"
  end

  it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
    ZuoraMailer.deliver_approval_mail(@account)
    sent = ActionMailer::Base.deliveries.last
    sent[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal "Zuora Subscription Approval Mail"
  end
end
