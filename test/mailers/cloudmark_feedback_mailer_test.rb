require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered!

describe CloudmarkFeedbackMailer do
  fixtures :accounts, :account_property_sets, :subscriptions, :users, :user_identities, :tickets, :translation_locales

  include MailerTestHelper

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
    @ticket = tickets(:minimum_1)
    @user = users(:minimum_agent)
    @account = accounts(:minimum)
    @analysis_header = "v=2.0 cv=XrtNzy59 c=0 sm=1 p=zSz-4ogZAAAA:8
\ta=Rg/P9Hy3qYqQfpQqF2dcgg==:17 a=cwja1q-NuFgA:10 a=w84Ey-L832wA:10
\ta=zszh1DEjAAAA:8 a=_Ju5N1Zns28A:10 a=Rg/P9Hy3qYqQfpQqF2dcgg==:117"
  end

  describe "delivering header feedback" do
    before do
      @params = {
        account_id: @account.id,
        user_id: @user.id,
        identifier: 'email_key',
        classification: 'spam',
        analysis_header: @analysis_header
      }
      @mail = CloudmarkFeedbackMailer.create_header_feedback(@params)
    end

    it "sends spam reports to our spambox" do
      assert_equal ['zendesk-spam-headers@feedback.cloudmark.com'], @mail.to
    end

    it "sends ham reports to our hambox" do
      @params[:classification] = 'legit'
      mail = CloudmarkFeedbackMailer.create_header_feedback(@params)

      assert_equal ['zendesk-legit-headers@feedback.cloudmark.com'], mail.to
    end

    it "is From a unique user to utilize Cloudmark's reporter trust system" do
      assert_equal ["cloudmark.#{@account.id}.#{@user.id}@system.zendesk.com"], @mail.from
    end

    it "does not use the account's address in the return-path" do
      assert_equal 'noreply@system.zendesk.com', @mail.smtp_envelope_from
    end

    it "is sure to not introduce headers that expose account contact information" do
      expected_headers = [
        "Return-Path", "From", "To", "Message-ID", "Subject", "Mime-Version", "DKIM-Signature", "Content-Transfer-Encoding",
        "Auto-Submitted", "X-Mailer", "X-Delivery-Context", "Content-Type", "X-Auto-Response-Suppress", "Date", "X-Zendesk-From-Account-Id"
      ]

      assert_equal expected_headers.sort, @mail.header.fields.map(&:name).sort
    end

    it "does not have any content besides the attached feedback" do
      assert_equal '', @mail.body.to_s
      assert_equal 1,  @mail.parts.size
    end

    it "has proper attachment separators" do
      assert_includes @mail.content_type, "multipart/alternative; boundary=\"--==_mim"
      assert_includes @mail.encoded, "----==_mimepart_"
    end

    it "has properties required by Cloudmark" do
      attachment = @mail.parts.first

      assert_equal "message/rfc822", attachment[:content_type].value
      assert_equal "X-CNFS-Analysis: #{@analysis_header}", attachment.body.to_s.strip
    end
  end
end
