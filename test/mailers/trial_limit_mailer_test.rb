require_relative '../support/test_helper'
require_relative '../support/mailer_test_helper'

SingleCov.covered!

describe TrialLimitMailer do
  include MailerTestHelper

  fixtures :all

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries         = []

    @admin = users(:minimum_admin_not_owner)
  end

  describe '#trial_limit_exceeded_notice' do
    let(:subject) { I18n.t('txt.email.trial_limit_mailer.user_identity_limit_exceeded_notice_v2.subject') }

    before do
      ActionMailer::Base.deliveries.clear
      TrialLimitMailer.deliver_trial_limit_exceeded_notice(@admin, :user_identity, 0)
    end

    it 'sends email to user' do
      refute ActionMailer::Base.deliveries.empty?
      assert ActionMailer::Base.deliveries.any? { |email| email.subject == subject }
    end

    it "appends the #{Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL} header" do
      email = ActionMailer::Base.deliveries.last
      email[Zendesk::OutgoingMail::Mailer::X_ZENDESK_PRIORITY_MAIL].value.must_equal "Trial Limit Exceeded Notice"
    end

    it 'has reply_to email suspensions@zendesk.com' do
      email = ActionMailer::Base.deliveries.last
      assert email.reply_to.first == 'suspensions@zendesk.com'
    end

    it 'sends email from suspensions@support.zendesk.com' do
      email = ActionMailer::Base.deliveries.last
      assert email.from.first == 'suspensions@support.zendesk.com'
    end

    it 'translates the subject, content, and footer properly' do
      @admin.translation_locale = translation_locales(:spanish)
      @admin.save!

      ActionMailer::Base.deliveries.clear
      TrialLimitMailer.deliver_trial_limit_exceeded_notice(@admin, :user_identity, 0)
      email = ActionMailer::Base.deliveries.last

      assert_match /Hay un problema con su cuenta de Zendesk - contáctenos/, email.parts.last.body.to_s
      assert_match /Hola, Su cuenta de Zendesk tiene bastante actividad./, email.parts.last.body.to_s

      # TODO: Refactor the #footer method in application_mailer.rb to use I18n.translation_locale,
      # which should be set at the individual mailer method using I18n.translation_locale.
      # For more info, see: https://zendesk.atlassian.net/browse/LOCAL-1721
      assert_match /This email is a service from Minimum account./, email.parts.last.body.to_s
    end

    it 'adds LTR styling to email when locale of user is LTR' do
      email = ActionMailer::Base.deliveries.last
      assert_match /<div style='text-align:left;'>/, email.parts.last.body.to_s
    end

    it 'adds RTL styling to email when locale of user is RTL' do
      @admin.translation_locale = translation_locales(:hebrew)
      @admin.save!

      ActionMailer::Base.deliveries.clear
      TrialLimitMailer.deliver_trial_limit_exceeded_notice(@admin, :user_identity, 0)
      email = ActionMailer::Base.deliveries.last

      assert_match /<div style='text-align:right;direction:rtl;'>/, email.parts.last.body.to_s
    end

    it 'adds subdomain name in the email' do
      ActionMailer::Base.deliveries.clear
      TrialLimitMailer.deliver_trial_limit_exceeded_notice(@admin, :user_identity, 0)
      email = ActionMailer::Base.deliveries.last

      assert_match /Your Zendesk account minimum.zendesk.com is seeing quite a bit of activity./, email.parts.last.body.to_s
    end
  end
end
