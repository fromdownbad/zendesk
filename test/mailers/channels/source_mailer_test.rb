require_relative "../../support/test_helper"
require_relative "../../support/mailer_test_helper"

SingleCov.covered!

describe Channels::SourceMailer do
  include MailerTestHelper

  fixtures :accounts, :tickets, :facebook_pages, :monitored_twitter_handles,
    :translation_locales, :channels_brands

  before do
    @account = accounts(:minimum)
  end

  describe 'Facebook' do
    before do
      @facebook_page = facebook_pages(:minimum_facebook_page_1)
      @learn_more_link = I18n.t('txt.email.facebook_page_unauthorized.learn_more_link')
      ActionMailer::Base.perform_deliveries = true
      @page_link = "https://minimum.zendesk-test.com/agent/admin/facebook"
    end

    it "sends an email for a Facebook Page becoming unauthorized" do
      Channels::SourceMailer.deliver_facebook_page_unauthorized_notification(@account, @facebook_page.id, @facebook_page.name)
      email = ActionMailer::Base.deliveries.last

      assert_equal I18n.t('txt.email.facebook_page_unauthorized.subject', name: @facebook_page.name), email.subject
      body_by_content_type(email, Mime[:text]).must_include @facebook_page.name
      body_by_content_type(email, Mime[:text]).must_include @page_link
      body_by_content_type(email, Mime[:text]).must_include @learn_more_link
      body_by_content_type(email, Mime[:html]).must_include @facebook_page.name
      body_by_content_type(email, Mime[:html]).must_include @page_link
      body_by_content_type(email, Mime[:html]).must_include @learn_more_link

      assert_equal(["support@support.zendesk-test.com"], email.from)
      assert_equal(@account.admins.includes(:identities).map(&:email), email.to)
    end

    it "sends email in best locale for recipients" do
      @account.translation_locale = translation_locales(:japanese)
      @account.save!

      I18n.expects(:t).with do |key, options|
        [
          'txt.email.facebook_page_unauthorized.subject',
          'txt.email.facebook_page_unauthorized.learn_more_link',
          'txt.email.facebook_page_unauthorized.html_body',
          'txt.email.email_landing_page_url',
          'txt.email.branded_delivered_by_url',
          'txt.email.footer'
        ].include?(key) && options.is_a?(Hash) && options[:locale] == translation_locales(:japanese)
      end.at_least_once.returns('in japanese')

      Channels::SourceMailer.deliver_facebook_page_unauthorized_notification(@account, @facebook_page.id, @facebook_page.name)
      email = ActionMailer::Base.deliveries.last

      assert_equal 'in japanese', email.subject
    end

    it "sends HTML as the preferred format" do
      Channels::SourceMailer.deliver_facebook_page_unauthorized_notification(@account, @facebook_page.id, @facebook_page.name)
      email = ActionMailer::Base.deliveries.last
      actual_parts = email.parts.map(&:mime_type)
      # See section 7.2.3 in http://www.w3.org/Protocols/rfc1341/7_2_Multipart.html
      expected_parts = ['text/plain', 'text/html']
      assert_equal expected_parts, actual_parts
    end
  end

  describe 'Twitter' do
    before do
      @twitter_handle = monitored_twitter_handles(:minimum_monitored_twitter_handle_1)
      @learn_more_link = I18n.t('txt.email.twitter_handle_unauthorized.learn_more_link')
      @page_link = "https://minimum.zendesk-test.com/agent/admin/twitter"
      ActionMailer::Base.perform_deliveries = true
    end

    it "sends an email for a Twitter Page becoming unauthorized" do
      Channels::SourceMailer.deliver_twitter_handle_unauthorized_notification(@account, @twitter_handle.id, @twitter_handle.twitter_screen_name)
      email = ActionMailer::Base.deliveries.last

      assert_equal I18n.t('txt.email.twitter_handle_unauthorized.subject', name: @twitter_handle.twitter_screen_name), email.subject
      body_by_content_type(email, Mime[:text]).must_include @twitter_handle.twitter_screen_name
      body_by_content_type(email, Mime[:text]).must_include @page_link
      body_by_content_type(email, Mime[:text]).must_include @learn_more_link
      body_by_content_type(email, Mime[:html]).must_include @twitter_handle.twitter_screen_name
      body_by_content_type(email, Mime[:html]).must_include @page_link
      body_by_content_type(email, Mime[:html]).must_include @learn_more_link

      assert_equal(["support@support.zendesk-test.com"], email.from)
      assert_equal(@account.admins.includes(:identities).map(&:email), email.to)
    end

    it "sends an email for a Twitter Page when it get deactivated by twitter" do
      Channels::SourceMailer.deliver_twitter_handle_deactivated_by_twitter_notification(@account, @twitter_handle.id, @twitter_handle.twitter_screen_name)
      email = ActionMailer::Base.deliveries.last

      assert_equal I18n.t('txt.email.twitter_handle_deactivated_by_twitter.subject', name: @twitter_handle.twitter_screen_name), email.subject
      body_by_content_type(email, Mime[:text]).must_include @twitter_handle.twitter_screen_name
      body_by_content_type(email, Mime[:text]).must_include @page_link
      body_by_content_type(email, Mime[:html]).must_include @twitter_handle.twitter_screen_name
      body_by_content_type(email, Mime[:html]).must_include @page_link

      assert_equal(["support@support.zendesk-test.com"], email.from)
      assert_equal(@account.admins.includes(:identities).map(&:email), email.to)
    end

    it "sends email in best locale for recipients" do
      @account.translation_locale = translation_locales(:japanese)
      @account.save!

      I18n.expects(:t).with do |key, options|
        [
          'txt.email.twitter_handle_unauthorized.subject',
          'txt.email.twitter_handle_unauthorized.learn_more_link',
          'txt.email.twitter_handle_unauthorized.html_body',
          'txt.email.email_landing_page_url',
          'txt.email.branded_delivered_by_url',
          'txt.email.footer'
        ].include?(key) && options.is_a?(Hash) && options[:locale] == translation_locales(:japanese)
      end.at_least_once.returns('in japanese')

      Channels::SourceMailer.deliver_twitter_handle_unauthorized_notification(@account, @twitter_handle.id, @twitter_handle.twitter_screen_name)
      email = ActionMailer::Base.deliveries.last

      assert_equal 'in japanese', email.subject
    end

    it "sends email in best locale for recipients" do
      @account.translation_locale = translation_locales(:japanese)
      @account.save!

      I18n.expects(:t).with do |key, options|
        [
          'txt.email.twitter_handle_unauthorized.subject',
          'txt.email.twitter_handle_unauthorized.learn_more_link',
          'txt.email.twitter_handle_unauthorized.html_body',
          'txt.email.email_landing_page_url',
          'txt.email.branded_delivered_by_url',
          'txt.email.footer'
        ].include?(key) && options.is_a?(Hash) && options[:locale] == translation_locales(:japanese)
      end.at_least_once.returns('in japanese')

      Channels::SourceMailer.deliver_twitter_handle_unauthorized_notification(@account, @twitter_handle.id, @twitter_handle.twitter_screen_name)
      email = ActionMailer::Base.deliveries.last

      assert_equal 'in japanese', email.subject
    end

    it "sends HTML as the preferred format" do
      Channels::SourceMailer.deliver_twitter_handle_unauthorized_notification(@account, @twitter_handle.id, @twitter_handle.twitter_screen_name)
      email = ActionMailer::Base.deliveries.last
      actual_parts = email.parts.map(&:mime_type)
      # See section 7.2.3 in http://www.w3.org/Protocols/rfc1341/7_2_Multipart.html
      expected_parts = ['text/plain', 'text/html']
      assert_equal expected_parts, actual_parts
    end
  end

  describe 'Any Channel' do
    before do
      @account = accounts(:minimum)

      @ris = ::Channels::AnyChannel::RegisteredIntegrationService.new(
        name: 'ris',
        search_name: 'ris',
        external_id: 1,
        manifest_url: 'manifest_url',
        admin_ui_url: 'admin_ui_url'
      ).tap do |ris|
        ris.account = @account
        ris.save!
      end

      @isi = ::Channels::AnyChannel::IntegrationServiceInstance.new(
        registered_integration_service: @ris,
        name: 'isi',
        metadata: '{}',
        state: ::Channels::AnyChannel::IntegrationServiceInstance::State::ACTIVE
      ).tap do |isi|
        isi.account = @account
        isi.save!
      end

      ActionMailer::Base.perform_deliveries = true
    end

    describe '#any_channel_instance_deactivate_notification' do
      it "sends an email for an integrated service instance becoming inactive" do
        Channels::SourceMailer.
          deliver_any_channel_instance_deactivate_notification(
            @account,
            @isi.id,
            @isi.name,
            @ris.id,
            @ris.name
          )
        email = ActionMailer::Base.deliveries.last

        assert_equal(
          I18n.t(
            'txt.email.any_channel_instance_deactivate_notification.subject',
            registered_integration_service_name: @ris.name,
            integration_service_instance_name: @isi.name
          ),
          email.subject
        )

        body_by_content_type(email, Mime[:text]).must_include(@isi.name)
        body_by_content_type(email, Mime[:text]).must_include(@ris.name)
        body_by_content_type(email, Mime[:text]).must_include(
          "#{@account.url(mapped: false)}/agent/admin"
        )
        body_by_content_type(email, Mime[:html]).must_include(@isi.name)
        body_by_content_type(email, Mime[:html]).must_include(@ris.name)
        body_by_content_type(email, Mime[:html]).must_include(
          "#{@account.url(mapped: false)}/agent/admin/registered_integration_services/#{@ris.id}?tab=accounts&amp;filter=inactive"
        )
        assert_equal(["support@support.zendesk-test.com"], email.from)
        assert_equal(@account.admins.includes(:identities).map(&:email), email.to)
      end

      it "sends email in best locale for recipients" do
        @account.translation_locale = translation_locales(:japanese)
        @account.save!

        I18n.expects(:t).with do |key, options|
          [
            'txt.email.any_channel_instance_deactivate_notification.subject',
            'txt.email.any_channel_instance_deactivate_notification.html_body',
            'txt.email.email_landing_page_url',
            'txt.email.branded_delivered_by_url',
            'txt.email.footer'
          ].include?(key) && options.is_a?(Hash) && options[:locale] == translation_locales(:japanese)
        end.at_least_once.returns('in japanese')

        Channels::SourceMailer.
          deliver_any_channel_instance_deactivate_notification(
            @account,
            @isi.id,
            @isi.name,
            @ris.id,
            @ris.name
          )
        email = ActionMailer::Base.deliveries.last

        assert_equal 'in japanese', email.subject
      end

      it "sends HTML as the preferred format" do
        Channels::SourceMailer.
          deliver_any_channel_instance_deactivate_notification(
            @account,
            @isi.id,
            @isi.name,
            @ris.id,
            @ris.name
          )
        email = ActionMailer::Base.deliveries.last

        actual_parts = email.parts.map(&:mime_type)
        # section 7.2.3 in http://www.w3.org/Protocols/rfc1341/7_2_Multipart.html
        expected_parts = ['text/plain', 'text/html']
        assert_equal expected_parts, actual_parts
      end
    end

    describe '#any_channel_instance_needs_reinitialization_notification' do
      it "sends an email when an integrated service instance needs reinitialization" do
        Channels::SourceMailer.
          deliver_any_channel_instance_needs_reinitialization_notification(
            @account,
            @isi.id,
            @isi.name,
            @ris.id,
            @ris.name
          )
        email = ActionMailer::Base.deliveries.last

        assert_equal(
          I18n.t(
            'txt.email.any_channel_instance_needs_reinitialization_notification.subject',
            registered_integration_service_name: @ris.name,
            integration_service_instance_name: @isi.name
          ),
          email.subject
        )

        body_by_content_type(email, Mime[:text]).must_include(@isi.name)
        body_by_content_type(email, Mime[:text]).must_include(@ris.name)
        body_by_content_type(email, Mime[:text]).must_include(
          "#{@account.url(mapped: false)}/agent/admin"
        )
        body_by_content_type(email, Mime[:html]).must_include(@isi.name)
        body_by_content_type(email, Mime[:html]).must_include(@ris.name)
        body_by_content_type(email, Mime[:html]).must_include(
          "#{@account.url(mapped: false)}/agent/admin/registered_integration_services/#{@ris.id}?tab=accounts"
        )
        assert_equal(["support@support.zendesk-test.com"], email.from)
        assert_equal(@account.admins.includes(:identities).map(&:email), email.to)
      end

      it "sends email in best locale for recipients" do
        @account.translation_locale = translation_locales(:japanese)
        @account.save!

        I18n.expects(:t).with do |key, options|
          [
            'txt.email.any_channel_instance_needs_reinitialization_notification.subject',
            'txt.email.any_channel_instance_needs_reinitialization_notification.html_body',
            'txt.email.email_landing_page_url',
            'txt.email.branded_delivered_by_url',
            'txt.email.footer'
          ].include?(key) && options.is_a?(Hash) && options[:locale] == translation_locales(:japanese)
        end.at_least_once.returns('in japanese')

        Channels::SourceMailer.
          deliver_any_channel_instance_needs_reinitialization_notification(
            @account,
            @isi.id,
            @isi.name,
            @ris.id,
            @ris.name
          )
        email = ActionMailer::Base.deliveries.last

        assert_equal 'in japanese', email.subject
      end

      it "sends HTML as the preferred format" do
        Channels::SourceMailer.
          deliver_any_channel_instance_needs_reinitialization_notification(
            @account,
            @isi.id,
            @isi.name,
            @ris.id,
            @ris.name
          )
        email = ActionMailer::Base.deliveries.last

        actual_parts = email.parts.map(&:mime_type)
        # section 7.2.3 in http://www.w3.org/Protocols/rfc1341/7_2_Multipart.html
        expected_parts = ['text/plain', 'text/html']
        assert_equal expected_parts, actual_parts
      end
    end
  end
end
