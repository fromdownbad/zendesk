require_relative "../support/test_helper"
require_relative "../support/mailer_test_helper"

SingleCov.covered! uncovered: 1

describe OldDeflectionMailer do
  fixtures :all

  include MailerTestHelper
  let(:ticket) { tickets(:minimum_1) }
  let(:user) { users(:minimum_agent) }
  let(:system_user) { users(:systemuser) }
  let(:mailer) { DeflectionMailer.send(:new) }
  let(:brand) { brands(:minimum) }
  let(:template) { "{{automatic_answers.article_list}} {{ticket.comments_formatted}}" }
  let(:subject) { "Thank you for contacting K&R! You request is received for: {{ticket.title}}" }
  let(:ticket_deflection) { TicketDeflection.new(ticket: ticket, user: user, account: ticket.account) }
  let(:ticket_deflection_article) { TicketDeflectionArticle.new(ticket_deflection: ticket_deflection, account: ticket.account, score: 0.89, brand: brand) }
  let(:deflection_config) do
    {recipient: 'requester_id', subject: subject, body: template}
  end
  let(:deflection_trigger) do
    d = Definition.new
    d.actions.push(DefinitionItem.new("deflection", "is", deflection_config))
    d.conditions_all.push(DefinitionItem.new("status_id", "is", [StatusType.NEW]))
    Trigger.create!(account: ticket.account, title: 'deflection trigger', definition: d, owner: ticket.account)
  end
  let(:rule_id) { deflection_trigger.id }
  let(:notification) do
    audit = ticket.audits[0]

    AnswerBotNotification.new(
      id: 1,
      subject:    'foo title',
      body:       'foo',
      recipients: [user.email],
      audit: audit
    ) do |event|
      event.via_id = ViaType.RULE
      event.via_reference_id = rule_id

      # Don't deliver notifications during test setup
      AnswerBotNotification.skip_callback(:commit, :after, :deliver_notifications)
      event.save!
      AnswerBotNotification.set_callback(:commit, :after, :deliver_notifications)
    end
  end

  before do
    Account.any_instance.stubs(:send_real_attachments?).returns(true)
    Account.any_instance.expects(:has_automatic_answers_enabled?).returns(true).at_least_once
    Ticket.any_instance.expects(:ticket_deflection).returns(ticket_deflection).at_least_once
    TicketDeflection.any_instance.expects(:ticket_deflection_articles).returns([ticket_deflection_article]).at_least_once
    AnswerBot::Trigger.stubs(:config).with(ticket.account, rule_id).returns(subject_template: subject, body_template: template, labels_for_filter: nil)
    Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:answer_bot_setting_enabled?).returns(false)
    Zendesk::HelpCenter::InternalApiClient.any_instance.stubs(:fetch_articles_for_ids_and_locales).returns([{
      id: ticket_deflection_article,
      title: 'How to fix dE sign on display',
      html_url: 'zendesk.com/hc/en-us/fix-de-sign-display',
      body: 'Call us and we will fix it for you'
    }])
  end

  describe 'email presentation' do
    before do
      account = ticket.account
      account.text_mail_template += "TEXT me"
      account.html_mail_template += "HTML you"
      account.save!

      ticket.will_be_saved_by system_user
      ticket.save!

      ticket.stubs(:generate_message_id).with(true).returns("random id")
      ticket.stubs(:generate_message_id).with(false).returns("static id")
    end

    let(:ccs) { [] }
    let(:mail) { mailer.answer_bot_email(ticket, notification, rule_id, ccs) }

    it 'should use the locale of the recipient' do
      locale = translation_locales(:japanese)
      ticket.requester.stubs(:translation_locale).returns(locale)
      mail # force mail setup to happen

      assert_equal locale.name, mailer.send(:locale).name
    end

    it 'should have reply_to the ticket address' do
      assert_equal "support@zdtestlocalhost.com", mail.reply_to.first
    end

    describe_with_arturo_enabled :email_ccs do
      describe 'with CCs on the ticket' do
        let(:end_user) { users(:minimum_end_user) }
        let(:ccs) { [end_user] }

        before do
          ticket.will_be_saved_by system_user
          ticket.collaborations.build(user: end_user, ticket: ticket, collaborator_type: CollaboratorType.EMAIL_CC)
          ticket.save
        end

        describe 'with email CCs enabled' do
          before { ticket.account.stubs(has_comment_email_ccs_allowed_enabled?: true) }

          it 'sends email to requester and ccs' do
            mail # force mail setup to happen

            assert_equal 'minimum_author@aghassipour.com', mail.to.first
            assert_equal 'minimum_end_user@aghassipour.com', mail.cc.first
          end
        end

        describe 'with email CCs not enabled' do
          before { ticket.account.stubs(has_comment_email_ccs_allowed_enabled?: false) }

          it 'sends email only to requester' do
            mail # force mail setup to happen

            assert_equal 'minimum_author@aghassipour.com', mail.to.first
            assert_empty mail.cc
          end
        end
      end
    end

    it 'should have mail `to` field to the ticket requester' do
      assert_equal 'minimum_author@aghassipour.com', mail.to.first
    end

    it 'should use delimiter specified for account' do
      assert_match /delimiter-delimiter-delimiter/, mail.html_part.to_s
    end

    it "should use the footer from the account settings" do
      assert_match /This email is a service from minimum/, mail.html_part.to_s
    end

    it 'should set for_agent so that ticket title will get shown for light user' do
      light_agent = users(:multiproduct_light_agent)
      ticket.stubs(:requester).returns(light_agent)
      mail # force mail setup

      assert mailer.send(:for_agent)
    end

    it 'contains a html part' do
      assert mail.html_part
    end

    it 'is a multipart email' do
      assert mail.multipart?
    end

    it 'contains a plain text part with rendered text' do
      assert mail.text_part.body.to_s
    end

    describe 'when the Subject field is NOT visible in HC and Email for end-users' do
      before { FieldSubject.any_instance.stubs(is_visible_in_portal?: false) }

      it "should display the first public comment's body" do
        assert_equal "Thank you for contacting K&R! You request is received for: minimum ticket 1", mail.subject.to_s
      end
    end

    describe 'when the Subject field is visible in HC and Email for end-users' do
      before { FieldSubject.any_instance.stubs(is_visible_in_portal?: true) }

      it "should have the subject with ticket_id" do
        assert_equal "Thank you for contacting K&R! You request is received for: minimum 1 ticket", mail.subject.to_s
      end
    end

    it "should have an automatic-answers header" do
      assert mail.header.to_s.include?("X-Delivery-Context: automatic-answer-#{ticket.id}")
    end

    it "should have the correct in-reply-to header" do
      assert mail.header.to_s.include?("In-Reply-To: static id")
    end

    it "should have the correct mail-id header" do
      assert mail.header.to_s.include?("Message-ID: random id")
    end

    describe 'when the comment has attachments' do
      before do
        StoresSynchronizationJob.stubs(:enqueue)

        ticket.comments.delete_all

        ticket.add_comment body: "I've attached that file you asked for", is_public: true
        ticket.will_be_saved_by(user)

        @attachments = []
        attached_files = %w[google_default.jpg]

        attached_files.each do |filename|
          @attachments << ticket.comment.attachments.build(
            uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{filename}"), filename),
            account: ticket.account,
            author: user,
            is_public: true
          )
        end

        comment_body = "I've attached that file you asked for"

        inline_attachments = %w[normal_1.jpg]

        inline_attachments.each do |filename|
          @attachments << ticket.comment.attachments.build(
            uploaded_data: RestUpload.new(File.read("#{Rails.root}/test/files/#{filename}"), filename),
            account: ticket.account,
            author: user,
            is_public: true
          ) do |a|
            a.inline = true
          end

          comment_body = [comment_body, "![](#{@attachments.last.url})"].join(' ')
        end

        ticket.comment.body = comment_body

        ticket.save!

        ticket.reload

        ActionMailer::Base.perform_deliveries = true
        ActionMailer::Base.deliveries = []
      end

      describe "and the account can send Real Attachments" do
        before do
          Account.any_instance.stubs(:send_real_attachments?).returns(true)
        end

        it "includes attachments in the email" do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id)

          appended_attachments = mail.attachments.select { |a| a.content_disposition =~ /^attachment;/ }
          appended_attachments.size.must_equal 1
        end

        it "includes inline attachments in the email" do
          DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id)

          inline_attachments = mail.attachments.select { |a| a.content_disposition =~ /^inline;/ }
          inline_attachments.size.must_equal 1
          assert_match /cid:/, mail.html_part.body.to_s
        end
      end
    end
  end

  describe 'email sending behaviour' do
    before do
      ticket.will_be_saved_by system_user
      ticket.save!
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []
    end

    it "sends the deflection email if the requester is human" do
      refute ticket.requester.machine?
      DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id)
      assert_equal 1, ActionMailer::Base.deliveries.size
    end

    it "does not send the deflection email if the requester is a machine" do
      ticket.requester.identities.first.mark_as_machine
      assert ticket.requester.machine?
      DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id)
      assert_equal 0, ActionMailer::Base.deliveries.size
    end
  end

  describe 'logging' do
    let(:statsd_client_stub) { stub_for_statsd }

    it 'logs when a deflection email has sent' do
      DeflectionMailer.any_instance.stubs(:statsd_client).returns(statsd_client_stub)
      statsd_client_stub.expects(:increment).with('email_sent', tags: ["subdomain:#{ticket.account.subdomain}"])

      DeflectionMailer.deliver_answer_bot_email(ticket, notification, rule_id)
    end
  end
end
