require_relative '../support/test_helper'
require 'zendesk_protobuf_clients/zendesk/protobuf/account_audits/audit_event_pb'

SingleCov.covered!

describe AccountAuditsConsumer do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:source_id) { 7 }
  let(:kafka_message) do
    event = Zendesk::Protobuf::AccountAudits::AuditEvent.new(
      account_id:          account.id,
      actor_id:            -1,
      action:              ::Zendesk::Protobuf::AccountAudits::AuditEvent::Action::CREATE,
      source_id:           source_id.to_s,
      source_type:         'AutomaticRuleBlocking',
      raw_message:         ::Zendesk::Protobuf::AccountAudits::AuditEvent::RawMessage.new(text: 'Blocked RuleID: 1 for 30 days'),
      visible:             false,
      source_display_name: 'Views'
    )
    OpenStruct.new(
      headers: { 'ZENDESK_ACCOUNT_ID' => 1 },
      value: event.to_proto
    )
  end
  let(:kafka_message_json) do
    OpenStruct.new(
      headers: { 'ZENDESK_ACCOUNT_ID' => 1 },
      value: 'json message'
    )
  end

  describe '#process' do
    describe 'audit message with an invalid schema' do
      it 'never creates an entry in db' do
        CIA::Event.expects(:save!).never

        AccountAuditsConsumer.new.process(kafka_message_json)
      end

      it 'records the exception to rollbar' do
        ZendeskExceptions::Logger.expects(:record).once

        AccountAuditsConsumer.new.process(kafka_message_json)
      end

      it 'swallows the raised exception' do
        AccountAuditsConsumer.new.process(kafka_message_json)
      end
    end

    describe 'audit message without kafka headers' do
      it 'raises an ArgumentError exception' do
        ZendeskExceptions::Logger.expects(:record).with(instance_of(ArgumentError), anything).once
        AccountAuditsConsumer.new.process(OpenStruct.new(headers: {}))
      end
    end

    describe 'audit message with a valid schema' do
      it 'creates an entry in db' do
        AccountAuditsConsumer.new.process(kafka_message)

        assert_equal(source_id, CIA::Event.find_by_source_id(source_id).source_id)
      end

      it 'does not record any exception to rollbar' do
        ZendeskExceptions::Logger.expects(:record).never

        AccountAuditsConsumer.new.process(kafka_message)
      end
    end
  end
end
