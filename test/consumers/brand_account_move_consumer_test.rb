require_relative '../support/test_helper'
require 'zendesk_protobuf_clients/zendesk/protobuf/exodus/event_pb'

SingleCov.covered!

describe BrandAccountMoveConsumer do
  let(:account)  { FactoryBot.create(:account, name: 'A very happy account', subdomain: 'groovy-mermaids') }
  let(:pod_id)   { 163 }

  def build_message(source_pod_id:, target_pod_id:, state:)
    event = Zendesk::Protobuf::Exodus::Event.new(
      account_move: Zendesk::Protobuf::Exodus::AccountMove.new(
        source_pod_id: Google::Protobuf::Int32Value.new(value: source_pod_id),
        target_pod_id: Google::Protobuf::Int32Value.new(value: target_pod_id)
      ),
      state_changed: Zendesk::Protobuf::Exodus::StateChanged.new(
        current_state: state
      ),
      header: Zendesk::Protobuf::Common::ProtobufHeader.new(
        account_id: Google::Protobuf::Int32Value.new(value: account.id)
      )
    )
    OpenStruct.new(value: event.to_proto)
  end

  describe "#process" do
    it "does not publish tombstones for other pods" do
      BrandPublisher.expects(:publish_tombstone).never

      BrandAccountMoveConsumer.new.process(
        build_message(source_pod_id: 666, target_pod_id: 555, state: Zendesk::Protobuf::Exodus::State::DONE)
      )
    end

    it "does not publish tombstones while the account is still being moved" do
      BrandPublisher.expects(:publish_tombstone).never

      BrandAccountMoveConsumer.new.process(
        build_message(source_pod_id: 1, target_pod_id: 1, state: Zendesk::Protobuf::Exodus::State::STARTED)
      )
    end

    it "ignores exceptions" do
      BrandPublisher.any_instance.expects(:publish_tombstone).never
      BrandPublisher.any_instance.expects(:publish_for_account).never
      Zendesk::Protobuf::Exodus::Event.expects(:decode).raises(NilClass)

      BrandAccountMoveConsumer.new.process(
        build_message(source_pod_id: 1, target_pod_id: 1, state: Zendesk::Protobuf::Exodus::State::DONE)
      )
    end

    it "publishes the updates when the account has finished moving from current the pod" do
      FactoryBot.create(:brand, name: 'Wombats from Oz', active: true, account_id: account.id)
      FactoryBot.create(:brand, name: 'Lemurs from space', active: true, account_id: account.id)

      assert_equal Brand.where(account_id: account.id).count, 3
      BrandPublisher.any_instance.expects(:publish_tombstone).times(3)
      BrandPublisher.any_instance.expects(:publish_for_account).once

      BrandAccountMoveConsumer.new.process(
        build_message(source_pod_id: 1, target_pod_id: 1, state: Zendesk::Protobuf::Exodus::State::DONE)
      )
    end
  end
end
