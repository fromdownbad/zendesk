require_relative "../support/test_helper"

SingleCov.covered!

class CIAControllerSupportTest < ActionController::TestCase
  fixtures :accounts, :users

  class CIAControllerSupportTestController < ApplicationController
    allow_anonymous_users

    def record
      CIA.record(:destroy, current_account)
      head :ok
    end
  end

  tests CIAControllerSupportTestController
  use_test_routes

  describe "CIAControllerSupport" do
    before do
      @request.account = @account = accounts(:minimum)
      @user = users(:minimum_agent)
      @request.stubs(:remote_ip).returns("1.2.3.4")
    end

    it "audits with the current user as actor" do
      login @user
      post :record

      assert_response :success
      event = CIA::Event.last
      assert_equal @user, event.actor
      assert_equal @account, event.account
      assert_equal "1.2.3.4", event.ip_address
    end

    it "does not audit if no user is logged in" do
      assert_raises RuntimeError do
        post :record
      end.message.must_include "Actor missing"
      assert_response :success
      refute CIA::Event.last
    end

    describe "assuming" do
      before do
        login @user
        @admin = users(:minimum_admin)
        ApplicationController.any_instance.stubs(:block_monitor_changes)
      end

      it "audits as assuming user" do
        @controller.send(:authentication).original_user = @admin
        post :record

        assert_response :success
        event = CIA::Event.last
        assert_equal @admin, event.actor
      end

      it "audits as assuming monitor user" do
        @controller.shared_session[:auth_assuming_monitor_user_id] = 123
        post :record

        assert_response :success
        event = CIA::Event.last
        assert_equal users(:systemuser), event.actor
        assert_equal 123, event.via_reference_id
        assert_equal ViaType.MONITOR_EVENT, event.via_id
      end
    end

    describe "with via header" do
      before do
        login users(:systemuser)
        set_header("X-Zendesk-Via", "Monitor event")
        set_header("X-Zendesk-Via-Reference-ID", "10")
      end

      it "resolves via headers" do
        post :record
        assert_response 200
        event = CIA::Event.last
        assert_equal ViaType.MONITOR_EVENT, event.via_id
        assert_equal 10, event.via_reference_id
      end

      it "returns bad request when I am not a system user" do
        login @user
        post :record
        assert_response 400
        assert_nil CIA::Event.last
      end

      it "returns bad request with broken via headers" do
        set_header("X-Zendesk-Via", "MonitorEvent")
        post :record
        assert_response 400
        assert_nil CIA::Event.last
      end

      it "returns bad request with an invalid reference id" do
        set_header("X-Zendesk-Via-Reference-ID", "0")
        post :record
        assert_response 400
      end
    end

    describe "with requester_id parameter" do
      before do
        login users(:systemuser)
        Arturo.enable_feature! :new_actor_via_requester_actions
      end

      it "resolves using requester_id if the controller and action allow it" do
        controller_name = CiaControllerSupport::NEW_ACTOR_VIA_REQUESTER_ACTIONS.first.first
        action_name = CiaControllerSupport::NEW_ACTOR_VIA_REQUESTER_ACTIONS.first.last.last
        @controller.stubs(:controller_path).returns(controller_name)
        @controller.stubs(:action_name).returns(action_name)

        post :record, params: { requester_id: @user.id }
        assert_response 200
        event = CIA::Event.last
        assert_equal @user, event.actor
      end
    end
  end
end
