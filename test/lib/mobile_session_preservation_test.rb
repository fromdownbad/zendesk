require_relative "../support/test_helper"

SingleCov.covered!

class MobileSessionPreservationTestController < ActionController::Base
  include ::AllowedParameters
  include MobileSessionPreservation

  around_action :preserve_mobile_preference

  allow_parameters :show, {}
  def show
    session[Schmobile::IS_MOBILE] = "bar"
    head :ok
  end
end

class MobileSessionPreservationTest < ActionController::TestCase
  tests MobileSessionPreservationTestController
  use_test_routes

  it "preserves whether the session is mobile" do
    key = Schmobile::IS_MOBILE
    session[key] = "foo"

    get :show

    assert_equal "foo", session[key]
  end
end
