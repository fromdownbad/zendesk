require_relative "../support/test_helper"
require 'numeric_to_base60'

SingleCov.covered!

describe 'NumericToBase60' do
  describe '.encode60' do
    it 'encodes from base 10 to base 60' do
      NumericToBase60.encode60(1).must_equal "1"
      NumericToBase60.encode60(10).must_equal "a"
      NumericToBase60.encode60(60).must_equal "10"
      NumericToBase60.encode60(70).must_equal "1a"
      NumericToBase60.encode60(3600).must_equal "100"
      NumericToBase60.encode60(3610).must_equal "10a"
      NumericToBase60.encode60(216000).must_equal "1000"
      NumericToBase60.encode60(1273097910).must_equal "1CdWiu"
    end

    it 'handles negative numbers' do
      NumericToBase60.encode60(-3610).must_equal "-10a"
    end

    it 'uses default precision of 0 when not specified in arguments' do
      NumericToBase60.encode60(151.12508012).must_equal "2v"
    end

    it 'encodes fractions using specified precision argument' do
      NumericToBase60.encode60(151.12508012, 1).must_equal "2v.7"
      NumericToBase60.encode60(151.12508012, 4).must_equal "2v.7uhi"
      NumericToBase60.encode60(151.1250801222, 10).must_equal "2v.7uhin1lJHx"
    end

    it 'handles floating point rounding errors' do
      # 0.283333333 * 60 => 16.999999980000002
      # 16.999999980000002 + 0.000001 => 17.000000980000003
      # BASE60[17] => 'h'
      NumericToBase60.encode60(0.283333333, 1).must_equal ".h"
    end
  end
end
