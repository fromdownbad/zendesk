require_relative "../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'RestUpload' do
  describe "a REST upload" do
    before do
      @body = File.read("#{Rails.root}/test/files/large_3.png")
      @filename = "large_3.png"
    end

    describe "with no content-type" do
      it "determines the content-type" do
        r = RestUpload.new(@body, @filename)
        assert_equal("image/png", r.content_type)
      end
    end

    describe "given a content-type" do
      it "allows a good content-type" do
        r = RestUpload.new(@body, @filename, "image/png")
        assert_equal("image/png", r.content_type)
      end

      it "clobbers an unknown content-type" do
        r = RestUpload.new(@body, @filename, "application/unknown")
        assert_equal("image/png", r.content_type)
      end

      it "overrides a known-bogus content-type" do
        r = RestUpload.new(@body, @filename, "image/x-png")
        assert_equal("image/png", r.content_type)
      end
    end
  end
end
