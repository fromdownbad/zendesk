require_relative "../support/test_helper"

SingleCov.covered!

class Test404Controller < ActionController::Base
  include PageNotFoundRenderingSupport

  def no_route_found
    render_404_page
  end
end

class PageNotFoundRenderingSupportTest < ActionController::TestCase
  tests Test404Controller
  use_test_routes

  describe "when current_brand has help center in use" do
    let(:account) { accounts(:minimum) }

    before do
      brand = stub(help_center_in_use?: true)
      @controller.stubs(:current_brand).returns(brand)
      @controller.stubs(:current_account).returns(account)
    end

    describe "with the classic_help_center_redirect_on_non_body_only Arturo enabled" do
      before do
        Arturo.stubs(:feature_enabled_for?).with(:classic_help_center_redirect_on_non_body_only, account).returns(true)
      end

      it "internally redirects a GET request" do
        get :no_route_found

        assert_response :not_found
        assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
      end

      it "internally redirects a DELETE request" do
        delete :no_route_found

        assert_response :not_found
        assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
      end

      it "internally redirects a HEAD request" do
        head :no_route_found

        assert_response :not_found
        assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
      end

      it "redirects a POST request" do
        post :no_route_found

        assert_response :not_found
        assert_nil @response.headers["X-Accel-Redirect"]
        assert_template(file: "#{Rails.root}/public/404.html")
      end

      it "redirects a PUT request" do
        put :no_route_found

        assert_response :not_found
        assert_nil @response.headers["X-Accel-Redirect"]
        assert_template(file: "#{Rails.root}/public/404.html")
      end

      it "redirects a PATCH request" do
        patch :no_route_found

        assert_response :not_found
        assert_nil @response.headers["X-Accel-Redirect"]
        assert_template(file: "#{Rails.root}/public/404.html")
      end
    end

    it "internally redirects a GET request" do
      get :no_route_found

      assert_response :not_found
      assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
    end

    it "internally redirects a DELETE request" do
      delete :no_route_found

      assert_response :not_found
      assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
    end

    it "internally redirects a HEAD request" do
      head :no_route_found

      assert_response :not_found
      assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
    end

    it "internally redirects a POST request" do
      post :no_route_found

      assert_response :not_found
      assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
    end

    it "internally redirects a PUT request" do
      put :no_route_found

      assert_response :not_found
      assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
    end

    it "internally redirects a PATCH request" do
      patch :no_route_found

      assert_response :not_found
      assert_equal "/hc/404", @response.headers["X-Accel-Redirect"]
    end
  end

  describe "when current_brand doesn't have an help center in use" do
    before do
      brand = stub(help_center_in_use?: false)
      @controller.stubs(:current_brand).returns(brand)
    end

    it "renders a static file and return 404" do
      get :no_route_found

      assert_response :not_found
      assert_template(file: "#{Rails.root}/public/404.html")
    end
  end

  describe "when current_brand is nil" do
    before do
      @controller.http_accept_language.stubs(user_preferred_languages: ['en'])
    end

    describe "when request is for an asset" do
      it "responds with 404" do
        @request.path = 'assetserver.zendesk.com/test.js'
        @request.format = :js

        get :no_route_found, format: :js

        assert_response :not_found
      end
    end

    describe "when request host is a blocked domain" do
      it "returns not found for asset servers" do
        @request.host = 'p6assets.zendesk.com'
        get :no_route_found

        assert_response :not_found
      end

      it "returns not found for specific domains" do
        @request.host = 'activehourshelp.zendesk.com'
        get :no_route_found

        assert_response :not_found
      end
    end

    describe "when request host is a valid domain" do
      it "301 redirects to a marketing page with the request host" do
        get :no_route_found

        assert_response :redirect
        assert_equal 301, @response.status
        assert_includes @response.redirect_url, "utm_content=#{@request.host}"
      end
    end
  end
end
