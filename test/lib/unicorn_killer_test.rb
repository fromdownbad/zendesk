require_relative '../support/test_helper'
require 'unicorn_killer'

SingleCov.covered! uncovered: 1

class TestHttpServer
  prepend Unicorn::Killer

  def process_client(client)
    client.whatever
  end
end

describe Unicorn::Killer do
  setup do
    Unicorn::Killer.instance_variable_set(:@max_memory, nil)
  end

  describe '.process_client' do
    setup do
      @fake_client = stub
      @fake_server = TestHttpServer.new
    end

    it 'has a MAX_GC_REQUEST_TIME of 8000 milliseconds' do
      Unicorn::Killer::MAX_GC_REQUEST_TIME.must_equal 8000
    end

    describe 'after an arbitrary number of requests' do
      setup do
        @fake_server.instance_variable_set(:@requests, 51)
        @fake_server.instance_variable_set(:@request_time, Unicorn::Killer::MAX_GC_REQUEST_TIME - 500)
      end

      it 'selectively enables GC after amount of request time exceeds MAX_GC_REQUEST_TIME' do
        def @fake_client.whatever
          sleep(1) # rubocop:disable Lint/Sleep Timecop doesn't seem to affect Process.clock_gettime
        end
        GC.expects(:disable)
        GC.expects(:enable)
        @fake_server.process_client(@fake_client)
      end
    end

    it 'logs, records stats, and dies after a large number of requests' do
      @fake_client.expects(:whatever)
      @fake_server.instance_variable_set(:@requests, Unicorn::Killer::MAX_REQUESTS_PER_CHILD + 1)
      @fake_server.stats.expects(:increment).with("kill.requests")
      @fake_server.expects(:logger).returns(Rails.logger)
      @fake_server.expects(:kill_worker)
      @fake_server.process_client(@fake_client)
    end

    it 'logs, records stats, and dies after using too much memory' do
      @fake_client.expects(:whatever)
      @fake_server.expects(:rss).times(3).returns(Unicorn::Killer.max_memory + 1)
      @fake_server.stats.expects(:increment).with("kill.memory")
      @fake_server.expects(:logger).returns(Rails.logger)
      @fake_server.expects(:kill_worker)
      @fake_server.process_client(@fake_client)
    end
  end

  describe '.max_memory' do
    it 'checks the max if hostname is aws1' do
      Unicorn::Killer.stubs(total_memory_in_mb: 64_000) # 64GB
      Unicorn::Killer.stubs(hostname: 'app1.pod100.aws1.zdsystest.com')

      Unicorn::Killer.max_memory.must_equal 2_000
    end

    it 'checks the max if hostname is production' do
      Unicorn::Killer.stubs(total_memory_in_mb: 64_000) # 64GB

      Unicorn::Killer.max_memory.must_equal 1_120
    end

    it 'falls back to MIN_MAX_MEMORY if total memory less than min' do
      Unicorn::Killer.stubs(total_memory_in_mb: 20_000) # 20G

      Unicorn::Killer.max_memory.must_equal 850
    end

    it 'falls back to MIN_MAX_MEMORY if total memory is 0' do
      Unicorn::Killer.stubs(total_memory_in_mb: 0)

      Unicorn::Killer.max_memory.must_equal 850
    end
  end
end
