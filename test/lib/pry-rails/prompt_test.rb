require_relative '../../support/test_helper'
require 'pry-rails/prompt'

SingleCov.covered! uncovered: 4

describe PryRails::Prompt do
  describe 'formatted_env' do
    it 'is test' do
      PryRails::Prompt.formatted_env.must_equal 'test'
    end
  end

  describe 'project_name' do
    it 'is zendesk' do
      PryRails::Prompt.project_name.must_equal 'zendesk'
    end
  end
end
