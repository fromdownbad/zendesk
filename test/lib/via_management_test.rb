require_relative '../support/test_helper'

SingleCov.covered! uncovered: 29

describe ViaManagement do
  let(:via_manageable) do
    Class.new do
      include ViaManagement
    end
  end
  let(:subject) { via_manageable.new }

  {
    via_tweet?: {
      twitter: true,
      twitter_dm: false,
      twitter_favorite: true,
      facebook_post: false,
      facebook_message: false,
      any_channel: false
    },
    via_twitter?: {
      twitter: true,
      twitter_dm: true,
      twitter_favorite: true,
      facebook_post: false,
      facebook_message: false,
      any_channel: false
    },
    via_facebook?: {
      twitter: false,
      twitter_dm: false,
      twitter_favorite: false,
      facebook_post: true,
      facebook_message: true,
      any_channel: false
    },
    via_any_channel?: {
      twitter: false,
      twitter_dm: false,
      twitter_favorite: false,
      facebook_post: false,
      facebook_message: false,
      any_channel: true
    },
    via_channel?: {
      twitter: true,
      twitter_dm: true,
      twitter_favorite: true,
      facebook_post: true,
      facebook_message: true,
      any_channel: true,
      email: false
    }

  }.each do |method, expected_results|
    describe "##{method}" do
      expected_results.each do |value, expected_result|
        describe "when via #{value}" do
          before do
            subject.stubs(:via?).returns(false)
            subject.stubs(:via?).with(value).returns(expected_result)
          end

          it "should be #{expected_result}" do
            assert_equal expected_result, subject.send(method)
          end
        end
      end
    end
  end
end
