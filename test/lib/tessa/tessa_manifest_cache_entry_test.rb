require_relative '../../support/test_helper'
require_relative '../../../lib/tessa/tessa_manifest_cache_entry'

SingleCov.covered!

describe TessaManifestCacheEntry::V1 do
  let(:manifest) { {} }
  let(:last_modified) { 'Wed, 21 Oct 2015 07:28:00 GMT' }

  it 'allows for the passed manifest to be read' do
    entry = TessaManifestCacheEntry::V1.new(manifest, last_modified)
    assert_equal entry.manifest, manifest
  end

  it 'allows for the passed last_modified to be read' do
    entry = TessaManifestCacheEntry::V1.new(manifest, last_modified)
    assert_equal entry.last_modified, last_modified
  end

  it 'properly marks entries as fresh' do
    initial_time = Time.now
    Time.stubs(:now).returns(initial_time, initial_time + 19)
    entry = TessaManifestCacheEntry::V1.new(manifest, last_modified)
    entry.mark_as_being_updated!
    assert_equal entry.update_in_progress?, true
  end

  it 'properly marks update requests as stale' do
    initial_time = Time.now
    Time.stubs(:now).returns(initial_time, initial_time + 20)
    entry = TessaManifestCacheEntry::V1.new(manifest, last_modified)
    entry.mark_as_being_updated!
    assert_equal entry.update_in_progress?, false
  end

  it 'properly unmarks entries' do
    initial_time = Time.now
    Time.stubs(:now).returns(initial_time)
    entry = TessaManifestCacheEntry::V1.new(manifest, last_modified)
    entry.mark_as_being_updated!
    entry.unmark_as_being_updated!
    assert_equal entry.update_in_progress?, false
  end

  describe 'fetched_recently?' do
    it 'returns true when the entry was updated within the last updater period' do
      update_time = Time.now
      observation_time = update_time + TessaCacheUpdater::V1::PERIOD_IN_SECONDS
      Time.stubs(:now).returns(update_time, update_time, observation_time)
      entry = TessaManifestCacheEntry::V1.new(manifest, last_modified)
      entry.mark_as_being_updated!
      entry.unmark_as_being_updated!
      assert_equal entry.fetched_recently?, true
    end

    it 'returns false when the entry was not updated within the last updater period' do
      update_time = Time.now
      observation_time = update_time + (TessaCacheUpdater::V1::PERIOD_IN_SECONDS + 1)
      Time.stubs(:now).returns(update_time, update_time, observation_time)
      entry = TessaManifestCacheEntry::V1.new(manifest, last_modified)
      entry.mark_as_being_updated!
      entry.unmark_as_being_updated!
      assert_equal entry.fetched_recently?, false
    end
  end
end
