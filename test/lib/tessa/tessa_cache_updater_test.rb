require_relative '../../support/test_helper'
require_relative '../../../lib/tessa/tessa_cache_updater'

SingleCov.covered!

describe TessaCacheUpdater::V1::Runner do
  describe '#run' do
    describe 'when passed a single project' do
      it 'continually updates the project\'s current entry using TessaClient at the correct interval' do
        uri = '/v1/projects/lotus/pods/1/current'
        TessaClient::V1.expects(:generate_current_uri).with(:lotus).returns(uri)
        key = "tessa:v1:#{uri}"
        TessaClient::V1.expects(:generate_uri_cache_key).with(uri).returns(key)
        TessaClient::V1.expects(:fetch_and_cache_manifest).with(key, uri, :lotus, is_for_current: true, is_cache_update: true)

        Thread.expects(:new).yields
        Kernel.expects(:loop).yields
        Kernel.expects(:sleep).with(20)

        runner = TessaCacheUpdater::V1::Runner.new([:lotus])
        runner.run
      end
    end

    describe 'when passed multiple projects' do
      it 'continually updates each project\'s current entry using TessaClient at the correct interval' do
        lotus_uri = '/v1/projects/lotus/pods/1/current'
        other_uri = '/v1/projects/other/pods/1/current'
        TessaClient::V1.expects(:generate_current_uri).with(:lotus).returns(lotus_uri)
        TessaClient::V1.expects(:generate_current_uri).with(:other).returns(other_uri)
        lotus_key = "tessa:v1:#{lotus_uri}"
        other_key = "tessa:v1:#{other_uri}"
        TessaClient::V1.expects(:generate_uri_cache_key).with(lotus_uri).returns(lotus_key)
        TessaClient::V1.expects(:generate_uri_cache_key).with(other_uri).returns(other_key)
        TessaClient::V1.expects(:fetch_and_cache_manifest).with(lotus_key, lotus_uri, :lotus, is_for_current: true, is_cache_update: true)
        TessaClient::V1.expects(:fetch_and_cache_manifest).with(other_key, other_uri, :other, is_for_current: true, is_cache_update: true)

        Thread.expects(:new).yields.times(2)
        Kernel.expects(:loop).yields.times(2)
        Kernel.expects(:sleep).with(20).times(2)

        runner = TessaCacheUpdater::V1::Runner.new([:lotus, :other])
        runner.run
      end
    end

    describe 'error handling' do
      it 'rescues Faraday errors and properly records them with statsd' do
        mock_statsd_client = mock
        Zendesk::StatsD::Client.stubs(:new).with(namespace: ['tessa', 'cache_updater']).returns(mock_statsd_client)
        uri = '/v1/projects/lotus/pods/1/current'
        TessaClient::V1.expects(:generate_current_uri).with(:lotus).returns(uri)
        key = "tessa:v1:#{uri}"
        TessaClient::V1.expects(:generate_uri_cache_key).with(uri).returns(key)
        TessaClient::V1.expects(:fetch_and_cache_manifest).with(key, uri, :lotus, is_for_current: true, is_cache_update: true).raises(Faraday::Error::ClientError, 'Faraday Error')

        Thread.expects(:new).yields
        Kernel.expects(:loop).yields
        Kernel.expects(:sleep).with(20)

        Zendesk::StatsD::Client.expects(:new).with(namespace: ['tessa', 'cache_updater']).returns(mock_statsd_client)
        mock_statsd_client.expects(:increment).with('faraday_error', tags: ['project:lotus', 'error:ClientError'])

        runner = TessaCacheUpdater::V1::Runner.new([:lotus])
        runner.run
      end
    end
  end
end
