require_relative '../../support/test_helper'
require_relative '../../../lib/tessa/tessa_client'

SingleCov.covered!

describe TessaClient::V1 do
  let(:pod_id) { 1 }
  let(:mock_sha) { 'c29057cec9b5115fc3464899c3069468020a2055'.to_sym }
  let(:mock_version) { 2000 }
  let(:mock_manifest) { { 'createdAt' => '2015-08-21T07:28:00Z', 'version' => 1 } }
  let(:mock_newer_manifest) { { 'createdAt' => '2015-08-23T07:28:00Z', 'version' => 2 } }
  let(:last_modified) { 'Wed, 21 Oct 2015 07:28:00 GMT' }
  let(:newer_last_modified) { 'Fri, 23 Oct 2015 07:28:00 GMT' }
  let(:host_url) { 'http://pod-1.tessa.service.consul:1337' }
  let(:connection) { Faraday::Connection.new }

  before do
    Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(pod_id)
    TessaManifestCacheEntry::V1.any_instance.stubs(:fetched_recently?).returns(false)
  end

  describe '::generate_uri_cache_key' do
    it 'returns the correct value' do
      uri = "/v1/projects/lotus/pods/#{pod_id}"
      expected_key = "tessa:v1:#{uri}"
      assert_equal TessaClient::V1.generate_uri_cache_key(uri), expected_key
    end
  end

  describe '::generate_uri_prefix' do
    it 'returns the correct value' do
      expected_uri = "/v1/projects/lotus/pods/#{pod_id}"
      assert_equal TessaClient::V1.generate_uri_prefix(:lotus), expected_uri
    end
  end

  describe '::generate_current_uri' do
    it 'returns the correct value' do
      expected_uri = "/v1/projects/lotus/pods/#{pod_id}/current"
      assert_equal TessaClient::V1.generate_current_uri(:lotus), expected_uri
    end
  end

  describe '::fetch_and_cache_manifest' do
    let(:uri) { '/v1/projects/lotus/pods/1/current' }
    let(:cache_key) { 'tessa:v1:/v1/projects/lotus/pods/1/current' }

    it 'makes use of a properly configured Faraday::Connection' do
      mock_configuration_settings = mock
      mock_configuration_settings.expects(:request).with(:retry, max: 5, interval: 0.05, interval_randomness: 0.5, backoff_factor: 2)
      mock_configuration_settings.expects(:use).with(Faraday::Response::RaiseError)
      mock_configuration_settings.expects(:adapter).with(Faraday.default_adapter)
      Faraday.expects(:new).with(url: host_url).returns(connection).yields(mock_configuration_settings)
      response = Faraday::Response.new(body: mock_manifest.to_json)
      response.stubs(:headers).returns({})
      connection.stubs(:get).with(uri).returns(response)

      TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true)
    end

    describe 'when not performed as a cache update' do
      it 'returns the correct value' do
        Faraday.expects(:new).with(url: host_url).returns(connection)
        response = Faraday::Response.new(body: mock_manifest.to_json)
        response.stubs(:headers).returns({})
        connection.expects(:get).with(uri).returns(response)

        request_response = TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true)
        assert_equal request_response, mock_manifest
      end

      it 'writes the appropriate TessaManifestCacheEntry::V1 to the cache' do
        Faraday.expects(:new).with(url: host_url).returns(connection)
        response = Faraday::Response.new(body: mock_manifest.to_json)
        response.stubs(:headers).returns('Last-Modified' => last_modified)
        connection.expects(:get).with(uri).returns(response)
        mock_manifest_entry = mock
        TessaManifestCacheEntry::V1.expects(:new).with(mock_manifest, last_modified).returns(mock_manifest_entry)
        Rails.cache.expects(:write).with(cache_key, mock_manifest_entry).returns(true)

        TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true)
      end

      it 'sends an appropriate statsd metric' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('fetched_current_manifest', tags: ['project:lotus', "manifest_version:#{mock_manifest['version']}"])
        Faraday.expects(:new).with(url: host_url).returns(connection)
        response = Faraday::Response.new(body: mock_manifest.to_json)
        response.stubs(:headers).returns({})
        connection.expects(:get).with(uri).returns(response)

        request_response = TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true)
        assert_equal request_response, mock_manifest
      end
    end

    describe 'when performed as a cache update' do
      it 'properly fetches and caches the manifest and sends the appropriate statsd metric when there is no cache entry' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('fetched_current_manifest', tags: ['project:lotus', "manifest_version:#{mock_manifest['version']}"])
        Rails.cache.expects(:read).with(cache_key).returns(nil)
        Faraday.expects(:new).returns(connection)
        response = Faraday::Response.new(body: mock_manifest.to_json)
        response.stubs(:headers).returns('Last-Modified' => last_modified)
        connection.expects(:get).with(uri).returns(response)
        final_cache_entry = mock
        TessaManifestCacheEntry::V1.expects(:new).with(mock_manifest, last_modified).returns(final_cache_entry)
        Rails.cache.expects(:write).with(cache_key, final_cache_entry).returns(true)

        request_response = TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true, is_cache_update: true)
        assert_equal request_response, mock_manifest
      end

      it 'immediately returns the cached manifest when there is already a fresh open update request' do
        cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
        cache_entry.expects(:update_in_progress?).returns(true)
        Rails.cache.expects(:read).with(cache_key).returns(cache_entry)

        request_response = TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true, is_cache_update: true)
        assert_equal request_response, mock_manifest
      end

      describe 'when there is not already a fresh open update request' do
        it 'immediately returns the cached manifest when the current cached result was recently added' do
          cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
          cache_entry.expects(:update_in_progress?).returns(false)
          cache_entry.expects(:fetched_recently?).returns(true)
          Rails.cache.expects(:read).with(cache_key).returns(cache_entry)

          request_response = TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true, is_cache_update: true)
          assert_equal request_response, mock_manifest
        end

        describe 'when Tessa responds with a 200' do
          it 'returns the correct value and sends the appropriate statsd metric' do
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('fetched_current_manifest', tags: ['project:lotus', "manifest_version:#{mock_manifest['version']}"])
            Faraday.expects(:new).returns(connection)
            response = Faraday::Response.new(body: mock_manifest.to_json)
            response.stubs(:headers).returns('Last-Modified' => newer_last_modified)
            cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            cache_entry.expects(:update_in_progress?).returns(false)
            updating_cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            cache_entry.expects(:mark_as_being_updated!).returns(updating_cache_entry)
            Rails.cache.expects(:read).with(cache_key).returns(cache_entry)
            Rails.cache.expects(:write).with(cache_key, updating_cache_entry).returns(true)
            final_cache_entry = mock
            TessaManifestCacheEntry::V1.expects(:new).returns(final_cache_entry)
            Rails.cache.stubs(:write).with(cache_key, final_cache_entry).returns(final_cache_entry)
            mock_request = mock
            mock_request.expects(:url).with(uri)
            mock_request.stubs(:headers).returns('Last-Modified' => last_modified)
            connection.expects(:get).returns(response).yields(mock_request)

            request_response = TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true, is_cache_update: true)
            assert_equal request_response, mock_manifest
          end

          it 'writes the appropriate TessaManifestCacheEntry::V1 to the cache' do
            Faraday.expects(:new).returns(connection)
            response = Faraday::Response.new(body: mock_manifest.to_json)
            response.stubs(:headers).returns('Last-Modified' => newer_last_modified)
            cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            cache_entry.expects(:update_in_progress?).returns(false)
            updating_cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            cache_entry.expects(:mark_as_being_updated!).returns(updating_cache_entry)
            final_cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            TessaManifestCacheEntry::V1.stubs(:new).returns(final_cache_entry)
            mock_request = mock
            mock_request.expects(:url).with(uri)
            mock_request.stubs(:headers).returns('Last-Modified' => newer_last_modified)
            connection.expects(:get).returns(response).yields(mock_request)
            Rails.cache.expects(:read).with(cache_key).returns(cache_entry)
            Rails.cache.expects(:write).with(cache_key, updating_cache_entry).returns(true)
            Rails.cache.expects(:write).with(cache_key, final_cache_entry).returns(true)

            TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true, is_cache_update: true)
          end

          it 'updates the fallback manifest' do
            Faraday.expects(:new).returns(connection)
            response = Faraday::Response.new(body: mock_manifest.to_json)
            response.stubs(:headers).returns('Last-Modified' => newer_last_modified)
            cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            cache_entry.stubs(:update_in_progress?).returns(false)
            updating_cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            cache_entry.stubs(:mark_as_being_updated!).returns(updating_cache_entry)
            final_cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            TessaManifestCacheEntry::V1.stubs(:new).returns(final_cache_entry)
            mock_request = mock
            mock_request.stubs(:url).with(uri)
            mock_request.stubs(:headers).returns('Last-Modified' => newer_last_modified)
            connection.stubs(:get).returns(response).yields(mock_request)
            Rails.cache.stubs(:read).with(cache_key).returns(cache_entry)
            Rails.cache.stubs(:write).with(cache_key, updating_cache_entry).returns(true)
            Rails.cache.stubs(:write).with(cache_key, final_cache_entry).returns(true)

            TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true, is_cache_update: true)
            assert_equal TessaClient::V1.project_fallbacks[:lotus], mock_manifest

            # Running a second time with a different manifest guarantees that the fallbacks are
            # properly updated.

            Faraday.expects(:new).returns(connection)
            response = Faraday::Response.new(body: mock_newer_manifest.to_json)
            response.stubs(:headers).returns('Last-Modified' => newer_last_modified)
            cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            cache_entry.stubs(:update_in_progress?).returns(false)
            updating_cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            cache_entry.stubs(:mark_as_being_updated!).returns(updating_cache_entry)
            final_cache_entry = TessaManifestCacheEntry::V1.new(mock_newer_manifest, last_modified)
            TessaManifestCacheEntry::V1.stubs(:new).returns(final_cache_entry)
            mock_request = mock
            mock_request.stubs(:url).with(uri)
            mock_request.stubs(:headers).returns('Last-Modified' => newer_last_modified)
            connection.stubs(:get).returns(response).yields(mock_request)
            Rails.cache.stubs(:read).with(cache_key).returns(cache_entry)
            Rails.cache.stubs(:write).with(cache_key, updating_cache_entry).returns(true)
            Rails.cache.stubs(:write).with(cache_key, final_cache_entry).returns(true)

            TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true, is_cache_update: true)
            assert_equal TessaClient::V1.project_fallbacks[:lotus], mock_newer_manifest
          end

          it 'does not set the If-Modified-Since header if for some reason Tessa previously never sent a Last-Modified header' do
            Faraday.expects(:new).returns(connection)
            response = Faraday::Response.new(body: mock_manifest.to_json)
            response.stubs(:headers).returns({})
            cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, nil)
            updating_cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, nil)
            cache_entry.expects(:mark_as_being_updated!).returns(updating_cache_entry)
            final_cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, nil)
            TessaManifestCacheEntry::V1.stubs(:new).returns(final_cache_entry)
            Rails.cache.expects(:read).with(cache_key).returns(cache_entry)
            Rails.cache.expects(:write).with(cache_key, updating_cache_entry).returns(true)
            Rails.cache.expects(:write).with(cache_key, final_cache_entry).returns(true)
            mock_request = mock
            mock_request.expects(:url).with(uri)
            mock_headers = mock
            mock_headers.stubs(:[]=).with('If-Modified-Since', nil).raises(Exception, 'Unexpected call to set Last-Modified!')
            mock_request.stubs(:headers).returns(mock_headers)
            connection.expects(:get).returns(response).yields(mock_request)

            TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true, is_cache_update: true)
          end

          it 'returns the cached manifest if it happens to be fresher than the response manifest' do
            Faraday.expects(:new).returns(connection)
            response = Faraday::Response.new(body: mock_manifest.to_json)
            response.stubs(:headers).returns('Last-Modified' => last_modified)
            cache_entry = TessaManifestCacheEntry::V1.new(mock_newer_manifest, newer_last_modified)
            cache_entry.expects(:update_in_progress?).returns(false)
            updating_cache_entry = TessaManifestCacheEntry::V1.new(mock_newer_manifest, newer_last_modified)
            cache_entry.expects(:mark_as_being_updated!).returns(updating_cache_entry)
            final_cache_entry = TessaManifestCacheEntry::V1.new(mock_newer_manifest, newer_last_modified)
            updating_cache_entry.expects(:unmark_as_being_updated!).returns(final_cache_entry)
            Rails.cache.expects(:read).with(cache_key).returns(cache_entry)
            Rails.cache.expects(:write).with(cache_key, updating_cache_entry).returns(true)
            Rails.cache.expects(:write).with(cache_key, final_cache_entry).returns(true)
            mock_request = mock
            mock_request.expects(:url).with(uri)
            mock_request.stubs(:headers).returns('Last-Modified' => last_modified)
            connection.expects(:get).returns(response).yields(mock_request)

            request_response = TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true, is_cache_update: true)
            assert_equal request_response, mock_newer_manifest
          end
        end

        describe 'when Tessa responsds with a 304' do
          it 'returns the correct value from the cache and sends the correct statsd metric' do
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('fetched_current_manifest', tags: ['project:lotus', "manifest_version:#{mock_manifest['version']}"])
            Faraday.expects(:new).with(url: host_url).returns(connection)
            response = Faraday::Response.new(body: mock_manifest.to_json)
            response.stubs(:headers).returns('Last-Modified' => last_modified)
            response.expects(:status).returns(304)
            mock_request = mock
            mock_request.expects(:url).with(uri)
            mock_headers = mock
            mock_headers.expects(:[]=).with('If-Modified-Since', last_modified)
            mock_request.expects(:headers).returns(mock_headers)
            connection.expects(:get).returns(response).yields(mock_request)
            cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            cache_entry.expects(:update_in_progress?).returns(false)
            updating_cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            cache_entry.expects(:mark_as_being_updated!).returns(updating_cache_entry)
            final_cache_entry = TessaManifestCacheEntry::V1.new(mock_manifest, last_modified)
            updating_cache_entry.expects(:unmark_as_being_updated!).returns(final_cache_entry)
            Rails.cache.expects(:read).with(cache_key).returns(cache_entry)
            Rails.cache.expects(:write).with(cache_key, updating_cache_entry).returns(true)
            Rails.cache.expects(:write).with(cache_key, final_cache_entry).returns(true)

            request_response = TessaClient::V1.fetch_and_cache_manifest(cache_key, uri, :lotus, is_for_current: true, is_cache_update: true)
            assert_equal request_response, mock_manifest
          end
        end
      end
    end
  end

  describe '::fetch_health' do
    let(:uri) { '/z/ping' }

    it 'returns the correct value' do
      Faraday.expects(:new).with(url: host_url).returns(connection)
      mock_response_body = '{"message": "OK"}'
      response = Faraday::Response.new(body: mock_response_body.to_json)
      connection.expects(:get).with(uri).returns(response)

      assert_equal TessaClient::V1.fetch_health, response
    end

    it 'uses a correctly configured Faraday connection' do
      TessaClient::V1.stubs(:tessa_host_url).returns(host_url)
      mock_configuration_settings = mock
      mock_configuration_settings.expects(:request).with(:retry, max: 5, interval: 0.05, interval_randomness: 0.5, backoff_factor: 2)
      mock_configuration_settings.expects(:use).with(Faraday::Response::RaiseError)
      mock_configuration_settings.expects(:adapter).with(Faraday.default_adapter)
      Faraday.expects(:new).with(url: host_url).returns(connection).yields(mock_configuration_settings)
      connection.stubs(:get).with(uri)

      TessaClient::V1.fetch_health
    end
  end

  describe '#fetch_current_manifest' do
    describe 'when there is no previously cached value' do
      let(:uri) { "/v1/projects/lotus/pods/#{pod_id}/current" }

      before do
        Rails.cache.stubs(:read).returns(nil)
      end

      describe 'when the network request successfully returns a manifest' do
        it 'returns the correct value' do
          Faraday.expects(:new).with(url: host_url).returns(connection)
          response = Faraday::Response.new(body: mock_manifest.to_json)
          response.stubs(:headers).returns({})
          connection.expects(:get).with(uri).returns(response)

          client = TessaClient::V1.new(:lotus)
          request_response = client.fetch_current_manifest
          assert_equal request_response, mock_manifest
        end
      end

      describe 'when there is a Faraday error' do
        it 'sends the appropriate statsd metric for the fetch error' do
          Faraday.expects(:new).with(url: host_url).returns(connection)
          connection.expects(:get).with(uri).raises(Faraday::Error::ClientError, 'Faraday Error')

          Zendesk::StatsD::Client.any_instance.stubs(:increment)
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('faraday_error', tags: ['project:lotus', 'error:ClientError'])

          client = TessaClient::V1.new(:lotus)
          client.fetch_current_manifest
        end

        describe 'when there is a project fallback' do
          it 'returns the fallback manifest' do
            TessaClient::V1.expects(:project_fallbacks).returns(lotus: mock_manifest)
            Faraday.expects(:new).with(url: host_url).returns(connection)
            connection.expects(:get).with(uri).raises(Faraday::Error::ClientError, 'Faraday Error')

            client = TessaClient::V1.new(:lotus)
            request_response = client.fetch_current_manifest
            assert_equal request_response, mock_manifest
          end

          it 'sends the appropriate statsd metric' do
            TessaClient::V1.expects(:project_fallbacks).returns(lotus: mock_manifest)
            Faraday.expects(:new).with(url: host_url).returns(connection)
            connection.expects(:get).with(uri).raises(Faraday::Error::ClientError, 'Faraday Error')

            Zendesk::StatsD::Client.any_instance.stubs(:increment)
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('current_manifest_fallback', tags: ['project:lotus', 'fallback:success'])

            client = TessaClient::V1.new(:lotus)
            request_response = client.fetch_current_manifest
            assert_equal request_response, mock_manifest
          end
        end

        describe 'when there is no project fallback' do
          it 'returns nil' do
            TessaClient::V1.expects(:project_fallbacks).returns({})
            Faraday.expects(:new).with(url: host_url).returns(connection)
            connection.expects(:get).with(uri).raises(Faraday::Error::ClientError, 'Faraday Error')

            client = TessaClient::V1.new(:lotus)
            request_response = client.fetch_current_manifest
            assert_nil request_response
          end

          it 'sends the appropriate statsd metric' do
            TessaClient::V1.expects(:project_fallbacks).returns({})
            Faraday.expects(:new).with(url: host_url).returns(connection)
            connection.expects(:get).with(uri).raises(Faraday::Error::ClientError, 'Faraday Error')

            Zendesk::StatsD::Client.any_instance.stubs(:increment)
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('current_manifest_fallback', tags: ['project:lotus', 'fallback:failure'])

            client = TessaClient::V1.new(:lotus)
            request_response = client.fetch_current_manifest
            assert_nil request_response
          end
        end
      end
    end

    describe 'when there is a previously cached value' do
      before do
        Rails.cache.stubs(:read).returns(TessaManifestCacheEntry::V1.new(mock_manifest, last_modified))
      end

      it 'immediately returns that manifest' do
        client = TessaClient::V1.new(:lotus)
        request_response = client.fetch_current_manifest
        assert_equal request_response, mock_manifest
      end

      it 'does not make a network request' do
        Faraday.stubs(:new).raises(Exception, 'Unexpected network request')

        client = TessaClient::V1.new(:lotus)
        client.fetch_current_manifest
      end
    end
  end

  describe '#fetch_manifest_by_sha' do
    describe 'when there is no previously cached value' do
      let(:uri) { "/v1/projects/lotus/pods/#{pod_id}/sha/#{mock_sha}" }

      before do
        Rails.cache.stubs(:read).returns(nil)
      end

      it 'throws the correct ArgumentError if the sha is not 40 characters in length' do
        client = TessaClient::V1.new(:lotus)
        exception = assert_raises ArgumentError do
          client.fetch_manifest_by_sha('1234567')
        end
        assert_equal 'Expected sha reference to be a full 40 character string, but it was not.', exception.message
      end

      describe 'when the network request successfully returns a manifest' do
        it 'returns the correct value' do
          Faraday.expects(:new).with(url: host_url).returns(connection)
          response = Faraday::Response.new(body: mock_manifest.to_json)
          response.stubs(:headers).returns({})
          connection.expects(:get).with(uri).returns(response)

          client = TessaClient::V1.new(:lotus)
          request_response = client.fetch_manifest_by_sha(mock_sha)
          assert_equal request_response, mock_manifest
        end
      end
    end

    describe 'when there is a previously cached value' do
      before do
        Rails.cache.stubs(:read).returns(TessaManifestCacheEntry::V1.new(mock_manifest, last_modified))
      end

      it 'immediately returns that manifest' do
        client = TessaClient::V1.new(:lotus)
        request_response = client.fetch_manifest_by_sha(mock_sha)
        assert_equal request_response, mock_manifest
      end

      it 'does not make a network request' do
        Faraday.stubs(:new).raises(Exception, 'Unexpected network request')
        client = TessaClient::V1.new(:lotus)
        client.fetch_manifest_by_sha(mock_sha)
      end
    end
  end

  describe '#fetch_manifest_by_version' do
    describe 'when there is no previously cached value' do
      let(:uri) { "/v1/projects/lotus/pods/#{pod_id}/versions/#{mock_version}" }

      before do
        Rails.cache.stubs(:read).returns(nil)
      end

      describe 'when the network request successfully returns a manifest' do
        it 'returns the correct value' do
          Faraday.expects(:new).with(url: host_url).returns(connection)
          response = Faraday::Response.new(body: mock_manifest.to_json)
          response.stubs(:headers).returns({})
          connection.expects(:get).with(uri).returns(response)
          client = TessaClient::V1.new(:lotus)
          request_response = client.fetch_manifest_by_version(mock_version)
          assert_equal request_response, mock_manifest
        end
      end
    end

    describe 'when there is a previously cached value' do
      before do
        Rails.cache.stubs(:read).returns(TessaManifestCacheEntry::V1.new(mock_manifest, last_modified))
      end

      it 'immediately returns that manifest' do
        client = TessaClient::V1.new(:lotus)
        request_response = client.fetch_manifest_by_version(mock_version)
        assert_equal request_response, mock_manifest
      end

      it 'does not make a network request' do
        Faraday.stubs(:new).raises(Exception, 'Unexpected network request')
        client = TessaClient::V1.new(:lotus)
        client.fetch_manifest_by_version(mock_version)
      end
    end
  end

  describe '#fetch_latest_deployed_manifests' do
    let(:mock_manifests) { [mock_newer_manifest, mock_manifest].to_json }
    let(:uri) { "/v1/projects/lotus/pods/#{pod_id}" }

    it 'properly caches and returns the expected Tessa response value' do
      Rails.cache.expects(:fetch).with(uri, expires_in: 20.seconds).yields.returns(mock_manifests)
      Faraday.expects(:new).with(url: host_url).returns(connection)
      response = Faraday::Response.new(body: mock_manifests)
      connection.expects(:get).with(uri).returns(response)
      client = TessaClient::V1.new(:lotus)
      request_response = client.fetch_latest_deployed_manifests
      assert_equal request_response, mock_manifests
    end
  end
end
