require_relative "../../support/test_helper"

SingleCov.covered!

describe Billing::ServiceConnection do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:options) do
    {
      foo:    'bar',
      params: { bar: :foo }
    }
  end

  let(:build_connection) do
    Billing::ServiceConnection.build(account, user, options)
  end

  describe '.build' do
    let(:builder) { stub('builder', build: anything) }

    describe 'when account has billing_primary_sync enabled' do
      before do
        Arturo.enable_feature! :billing_primary_sync
      end

      it 'uses the correct builder (v2)' do
        Billing::ServiceConnection::V2.expects(:new).
          with(account, user, options).returns(builder)
        Billing::ServiceConnection::V1.expects(:new).never
        build_connection
      end
    end

    describe 'when account is multi-product participant and user is verified' do
      before do
        account.stubs(:billing_multi_product_participant?).returns(true)
        user.stubs(:is_verified?).returns(true)
      end

      it 'uses the correct builder (v2)' do
        Billing::ServiceConnection::V2.expects(:new).
          with(account, user, options).returns(builder)
        Billing::ServiceConnection::V1.expects(:new).never
        build_connection
      end
    end

    describe 'when account is multi-product participant and user is not verified' do
      before do
        account.stubs(:billing_multi_product_participant?).returns(true)
        user.stubs(:is_verified?).returns(false)
      end

      it 'uses the correct builder (v1)' do
        Billing::ServiceConnection::V2.expects(:new).never
        Billing::ServiceConnection::V1.expects(:new).
          with(account, user, options[:params]).returns(builder)
        build_connection
      end
    end

    describe 'when account is not a multi-product participant' do
      before do
        account.stubs(:billing_multi_product_participant?).returns(false)
      end

      describe 'when subscription is Sales-Assisted' do
        before do
          account.subscription.stubs(:assisted?).returns(true)
        end
        it 'uses the correct builder (v2)' do
          Billing::ServiceConnection::V2.expects(:new).
            with(account, user, options).returns(builder)
          Billing::ServiceConnection::V1.expects(:new).never
          build_connection
        end
      end

      describe 'when subscription is Self-Service' do
        it 'uses the correct builder (v1)' do
          Billing::ServiceConnection::V1.expects(:new).
            with(account, user, options[:params]).returns(builder)
          Billing::ServiceConnection::V2.expects(:new).never
          build_connection
        end
      end
    end
  end
end
