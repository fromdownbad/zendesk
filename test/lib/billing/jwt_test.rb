require_relative "../../support/test_helper"

SingleCov.covered!

describe Billing::JWT do
  let(:sender)   { 'sender-rsa-key' }
  let(:receiver) { 'receiver-rsa-key' }

  before { Timecop.freeze }

  describe '.encrypt' do
    let(:claims) do
      { iss: issuer, iat: Time.now, content: payload }
    end

    let(:issuer)  { anything }
    let(:payload) { anything }

    let(:encryptor) { stub }

    let(:jwt) { stub(to_s: 'jwt-token') }

    before do
      ENV.expects(:fetch).
        with('CLASSIC_RSA_PRIVATE_KEY').returns(sender)
      ENV.expects(:fetch).
        with('BILLING_RSA_PUBLIC_KEY').returns(receiver)
      Billing::JWT::Encryptor.expects(:new).
        with(sender, receiver).returns(encryptor)
      encryptor.expects(:encrypt).
        with(claims).returns(jwt)
    end

    let(:expected) { 'jwt-token' }
    let(:actual)   { Billing::JWT.encrypt(issuer, payload) }

    it { assert_equal expected, actual }
  end

  describe '.decrypt' do
    let(:decryptor) { stub }

    let(:token) { 'jwt-token' }

    let(:claims) do
      { iss: anything, iat: anything, content: anything }
    end

    before do
      ENV.expects(:fetch).
        with('BILLING_RSA_PUBLIC_KEY').returns(sender)
      ENV.expects(:fetch).
        with('CLASSIC_RSA_PRIVATE_KEY').returns(receiver)
      Billing::JWT::Decryptor.expects(:new).
        with(sender, receiver).returns(decryptor)
      decryptor.expects(:decrypt).
        with(token).returns(claims)
    end

    let(:expected) { claims }
    let(:actual)   { Billing::JWT.decrypt(token) }

    it { assert_equal expected, actual }
  end
end
