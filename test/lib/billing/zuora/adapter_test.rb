require_relative "../../../support/test_helper"

SingleCov.covered!

describe Billing::Zuora::Adapter do
  describe '#update_cc_payment_method' do
    fixtures :accounts
    let(:zuora_account_id)            { '123' }
    let(:zuora_client)                { stub(zuora_account_id: 'zuora_account_id') }
    let(:payment_method_reference_id) { '456' }
    let(:current_account)             { accounts(:support) }
    let(:params) do
      {
        current_account:  current_account,
        zuora_client:     zuora_client,
        zuora_account_id: zuora_account_id
      }
    end

    subject { Billing::Zuora::Adapter.new(params) }

    before do
      Billing::Zuora::IronBank::CCPaymentMethodUpdate.expects(:call).with(
        zuora_account_id:            zuora_account_id,
        payment_method_reference_id: payment_method_reference_id,
        currency:                    1
      )
    end

    it 'uses IronBank' do
      subject.update_cc_payment_method(payment_method_reference_id)
    end
  end
end
