require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Billing::Zuora::IronBank::DefaultPaymentMethodUpdate do
  let(:described_class) { Billing::Zuora::IronBank::DefaultPaymentMethodUpdate }

  describe '.call' do
    let(:options)          { stub }
    let(:request)          { stub('described_class::Request') }
    let(:iron_bank_object) { stub('IronBank::Object') }

    before do
      described_class::Request.stubs(:build).
        with(options).
        returns(request)
      ::IronBank::Update.stubs(:call).
        with(request).
        returns(iron_bank_object)
      described_class::Response.stubs(:build).
        with(iron_bank_object).
        returns(true)
    end

    subject { described_class.call(options) }

    it { assert subject }
  end

  describe Billing::Zuora::IronBank::DefaultPaymentMethodUpdate::Request do
    let(:described_class) do
      Billing::Zuora::IronBank::DefaultPaymentMethodUpdate::Request
    end

    describe '.build' do
      let(:zuora_account_id)  { 'zuora-account-id' }
      let(:payment_method_id) { 'payment-method-id' }
      let(:currency)          { 1 }

      let(:options) do
        {
          zuora_account_id:  zuora_account_id,
          payment_method_id: payment_method_id,
          currency:          currency
        }
      end

      let(:env)                 { Rails.env }
      let(:invoice_template)    { stub('ZBC::Zuora::InvoiceSelector') }
      let(:invoice_template_id) { 'invoice-template-id' }

      before do
        ZBC::Zuora::InvoiceSelector.stubs(:new).
          with(env: env, currency: currency).
          returns(invoice_template)
        invoice_template.stubs(:invoice_template_id).
          returns(invoice_template_id)
      end

      subject { described_class.build(options) }

      it 'builds the desired hash' do
        expected_hash = {
          type:    :account,
          objects: [
            {
              id:                           zuora_account_id,
              auto_pay:                     true,
              default_payment_method_id:    payment_method_id,
              batch:                        'Batch20',
              invoice_template_id:          invoice_template_id,
              invoice_delivery_prefs_email: true,
              dunning_state__c:             'OK',
              communication_profile_id:     '4028e69632bcaec40132cb2c444e0eb7'
            }
          ]
        }

        assert_equal subject, expected_hash
      end
    end
  end

  describe Billing::Zuora::IronBank::DefaultPaymentMethodUpdate::Response do
    let(:described_class) do
      Billing::Zuora::IronBank::DefaultPaymentMethodUpdate::Response
    end

    describe '.build' do
      subject { described_class.build(iron_bank_response) }

      describe 'when the IronBank response is successful' do
        let(:iron_bank_response) { [{ success: true }] }

        it { assert subject }
      end

      describe 'when the IronBank response is not successful' do
        let(:iron_bank_response) { { message: 'Authentication error' } }

        it { refute subject }
      end
    end
  end
end
