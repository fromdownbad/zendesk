require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Billing::Zuora::IronBank::CCPaymentMethodUpdate do
  describe '.call' do
    let(:described_class)             { Billing::Zuora::IronBank::CCPaymentMethodUpdate }
    let(:zuora_account_id)            { '123' }
    let(:payment_method_reference_id) { '987' }
    let(:currency)                    { 1 }
    let(:opts) do
      {
        zuora_account_id:            zuora_account_id,
        payment_method_reference_id: payment_method_reference_id,
        currency:                    currency
      }
    end

    before do
      ZBC::Zuora::ContactsService.stubs(:update_bill_to_from_payment!).
        with(zuora_account_id, payment_method_reference_id)
      Rails.logger.stubs(:info)
      Billing::Zuora::IronBank::DefaultPaymentMethodUpdate.stubs(:call).
        with(zuora_account_id: zuora_account_id, payment_method_id: payment_method_reference_id, currency: currency)
      ZBC::Zuora::Synchronizer.stubs(:synchronize!).with(zuora_account_id)
    end

    it 'updates defualt payment method' do
      described_class.call(opts)
    end
  end
end
