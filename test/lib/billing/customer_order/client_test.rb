require_relative '../../../support/test_helper'

SingleCov.covered!

describe Billing::CustomerOrder::Client do
  fixtures :accounts

  let(:account)                 { accounts(:minimum) }
  let(:user)                    { account.owner }
  let(:zendesk_max_agents)      { 3 }
  let(:described_class)         { Billing::CustomerOrder::Client }
  let(:master_account_id)       { 1234 }
  let(:retry_options)           { { retry_options: { max: 2 } } }
  let(:kragle_options)          { stub('kragle_options', :timeout= => 60) }
  let(:host)                    { 'https://billing.zd-test.com' }
  let(:path)                    { '/billing/api/v1/orders/classic' }
  let(:response)                { Faraday::Response.new }
  let(:headers)                 { { 'Host': '' } }
  let(:current_subscription_id) { 'subscription_id' }

  let(:connection) do
    stub(
      'connection',
      'url_prefix=': host,
      post:          anything,
      headers:       headers,
      options:       kragle_options
    )
  end

  let(:post_customer_order) do
    described_class.post(account, user, zendesk_max_agents)
  end

  describe '.post' do
    let(:issuer)          { 'classic.1234' }
    let(:encrypted_token) { 'token' }

    let(:options) do
      {
        issuer: issuer,
        token: encrypted_token,
        data: {
          type: "customer_order",
          attributes: {
            items: [
              {
                target: {
                  type: "product",
                  id: "zendesk"
                },
                operation: {
                  type: "update",
                  value: {
                    quantity: 3
                  }
                }
              }
            ]
          }
        },
        meta: {
          zuora_subscription_id: current_subscription_id
        }
      }
    end

    before do
      Kragle.expects(:new).with('billing', retry_options).
        returns(connection)

      Billing::CustomerOrder::Payload.expects(:build).
        with(account, zendesk_max_agents).
        returns(options)
    end

    it 'sends request with expected params' do
      connection.expects(:post).
        with(path, options).
        returns(response)

      response.expects(:success?).once

      post_customer_order
    end
  end
end
