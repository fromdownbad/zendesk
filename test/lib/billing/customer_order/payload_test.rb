require_relative '../../../support/test_helper'

SingleCov.covered!

describe Billing::CustomerOrder::Payload do
  fixtures :accounts

  let(:described_class)    { Billing::CustomerOrder::Payload }
  let(:account)            { accounts(:minimum) }
  let(:zendesk_max_agents) { 3 }
  let(:subscription_id)    { 'subscription_id' }
  let(:zuora_client)       { stub(get_current_subscription!: subscription) }
  let(:subscription)       { stub(id: subscription_id) }

  subject { described_class.build(account, zendesk_max_agents) }

  describe '.build' do
    let(:expected) do
      {
        data: {
          type: "customer_order",
          attributes: {
            items: [
              {
                target: {
                  type: "product",
                  id: "zendesk"
                },
                operation: {
                  type: "update",
                  value: {
                    quantity: 3
                  }
                }
              }
            ]
          }
        },
        meta: {
          zuora_subscription_id: subscription_id
        }
      }
    end

    before do
      ZBC::Zuora::Client.expects(:new).
        with(account.billing_id).
        returns(zuora_client)
    end

    it 'builds the correct payload' do
      expected.must_equal subject
    end
  end
end
