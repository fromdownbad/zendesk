require_relative "../../../support/test_helper"

SingleCov.covered!

describe Billing::JWT::Encryptor do
  let(:sender)   { 'sender-rsa-key' }
  let(:receiver) { 'receiver-rsa-key' }

  let(:encryptor) do
    Billing::JWT::Encryptor.new(sender, receiver)
  end

  let(:sender_key)   { anything }
  let(:receiver_key) { anything }

  describe '#encrypt' do
    let(:claims) do
      { issuer: anything, iat: anything, payload: anything }
    end

    let(:jwt)        { stub('jwt') }
    let(:signed_jwt) { stub('signed_jwt') }

    let(:token) { anything }

    before do
      OpenSSL::PKey::RSA.expects(:new).with(sender).
        returns(sender_key)
      OpenSSL::PKey::RSA.expects(:new).with(receiver).
        returns(receiver_key)

      JSON::JWT.expects(:new).with(claims).
        returns(jwt)
      jwt.expects(:sign).with(sender_key, :RS256).
        returns(signed_jwt)

      signed_jwt.expects(:encrypt).
        with(receiver_key, :RSA1_5, :'A128CBC-HS256').
        returns(token)
    end

    let(:expected) { token }
    let(:actual)   { encryptor.encrypt(claims) }

    it { assert_equal expected, actual }
  end
end
