require_relative "../../../support/test_helper"

SingleCov.covered!

describe Billing::JWT::Decryptor do
  let(:claims) do
    { iss: 'ajax', iat: Time.now, content: 'hello world' }
  end

  let(:encryptor) do
    Billing::JWT::Encryptor.new(
      ENV.fetch('BILLING_RSA_PRIVATE_KEY'),
      ENV.fetch('CLASSIC_RSA_PUBLIC_KEY')
    )
  end

  let!(:token) do
    encryptor.encrypt(claims).to_s
  end

  describe 'amount of time before token is stale' do
    it { assert_equal 15.seconds, Billing::JWT::Decryptor::EXPIRY_LEEWAY }
  end

  describe '#decrypt' do
    let(:sender)   { ENV.fetch('BILLING_RSA_PUBLIC_KEY')  }
    let(:receiver) { ENV.fetch('CLASSIC_RSA_PRIVATE_KEY') }

    let(:decryptor) do
      Billing::JWT::Decryptor.new(sender, receiver)
    end

    describe 'when token is valid and not stale' do
      let(:expected) { claims }
      let(:actual)   { decryptor.decrypt(token).symbolize_keys }

      it { assert_equal expected, actual }
    end

    describe 'when token is created with a different algorithm' do
      before do
        JSON::JWE.any_instance.stubs(:alg).returns('XXX')
        JSON::JWS.any_instance.stubs(:alg).returns('XXX')
      end

      it 'raises an error' do
        assert_raises Billing::JWT::Decryptor::Error do
          decryptor.decrypt(token)
        end
      end
    end

    describe 'when token is stale' do
      before { Timecop.travel(16.seconds.from_now) }

      it 'raises an error' do
        assert_raises Billing::JWT::Decryptor::Error do
          decryptor.decrypt(token)
        end
      end
    end
  end
end
