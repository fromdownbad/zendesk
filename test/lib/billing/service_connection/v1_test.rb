require_relative '../../../support/test_helper'

SingleCov.covered!

describe Billing::ServiceConnection::V1 do
  fixtures :accounts

  let(:described_class)   { Billing::ServiceConnection::V1 }
  let(:account)           { accounts(:minimum) }
  let(:user)              { account.owner }
  let(:account_id_string) { account.id.to_s }

  subject { described_class.build(account, user, query_params) }
  before  { Subscription.any_instance.stubs(:id).returns(5678) }

  describe 'composing a billing-service connection spec' do
    before do
      Billing::JWT.stubs(:encrypt).returns('token')
    end

    describe 'without query-parameters' do
      let(:query_params) { nil }

      let(:actual) { subject }
      let(:expected) do
        {
          url:     "https://#{account.subdomain}.zd-test.com/billing?issuer=zendesk&token=token",
          token:   'token',
          payload: {
            product_type:      2,
            product_id:        account_id_string,
            user_id:           user.id,
            user_name:         'Admin Man',
            user_email:        'minimum_admin@aghassipour.com',
            organization_name: 'minimum'
          }
        }
      end

      it 'configures a billing-service connection spec' do
        expected.must_equal actual
      end
    end

    describe 'with query-parameters' do
      let(:query_params) { { foo: 'bar' } }
      let(:query_values) { Addressable::URI.parse(subject[:url]).query_values }

      it 'includes the extra parameters as query-parameters' do
        query_values.keys.must_include 'foo'
        query_values['foo'].must_equal 'bar'
      end
    end
  end

  describe 'when building the token for the billing-service connection' do
    let(:query_params) { nil }
    let(:compose_billing_service_connection) { subject }

    it 'uses the account specific issuer and payload' do
      Billing::JWT.expects(:encrypt).at_least_once.with(
        'zendesk',
        product_type:      2,
        product_id:        account_id_string,
        user_id:           user.id,
        user_name:         'Admin Man',
        user_email:        'minimum_admin@aghassipour.com',
        organization_name: 'minimum'
      )
      compose_billing_service_connection
    end
  end
end
