require_relative '../../../support/test_helper'

SingleCov.covered!

describe Billing::ServiceConnection::V2 do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:user)    { account.owner }

  let(:options) do
    {
      base_url: 'https://foo.example.com',
      params:   { greetings: 'hello world' }
    }
  end

  describe '.build' do
    subject do
      Billing::ServiceConnection::V2.build(account, user, options)
    end

    describe 'url section' do
      let(:url) { URI(subject[:url]) }

      let(:query_params) do
        Rack::Utils.parse_nested_query(url.query)
      end

      it { url.scheme.must_equal 'https' }
      it { url.host.must_equal 'billing.zd-test.com' }
      it { url.path.must_equal '/billing/api/purchase/product' }
      it { assert query_params.key?('token') }
      it { assert query_params.key?('greetings') }
      it { assert query_params.key?('origin') }
      it { assert query_params.key?('issuer') }
    end

    describe 'token section' do
      let(:token)          { subject[:token] }
      let(:expected_token) { 'foobar' }

      before { Billing::JWT.stubs(:encrypt).returns('foobar') }

      it { token.must_equal expected_token }
    end

    describe 'payload section' do
      def self.it_generates_expected_payload
        let(:payload) { subject[:payload] }

        let(:expected_payload) do
          {
            master_account_id: account.id,
            user_id:           user.id,
            currency_id:       1,
            account_url:       expected_account_url,
            notifications_url: 'https://foo.example.com/api/v2/billing/notification'
          }
        end

        it { payload.must_equal expected_payload }
      end

      describe 'when multiproduct account' do
        let(:account)              { accounts(:multiproduct) }
        let(:expected_account_url) { 'https://foo.example.com/admin/billing/overview' }

        it_generates_expected_payload
      end

      describe 'when non-multiproduct account' do
        let(:expected_account_url) { 'https://foo.example.com/agent/admin/subscription' }

        it_generates_expected_payload
      end
    end
  end
end
