require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 10

describe ProductLimits::ScalingStrategies::TicketQuantities do
  fixtures :accounts

  describe "#calculate" do
    let(:definition) { {} }
    let(:account) { accounts(:minimum) }

    it "should return the `throttle_base_value` on the limiter times the multiplier" do
      limiter = mock
      limiter.stubs(:throttle_base_value).returns(3)
      ProductLimits::ScalingStrategies::TicketQuantities.stubs(:multiplier).returns(7)
      assert_equal 21, ProductLimits::ScalingStrategies::TicketQuantities.calculate(limiter, definition, account)
    end

    describe 'the multiplier for different ticket quantities' do
      [
        {ticket_count: 100, multiplier: 1.0},
        {ticket_count: 250, multiplier: 1.25},
        {ticket_count: 500, multiplier: 2.0},
        {ticket_count: 1000, multiplier: 3.0},
        {ticket_count: 5000, multiplier: 5.0},
        {ticket_count: 10000, multiplier: 8.0},
        {ticket_count: 20000, multiplier: 13.0},
        {ticket_count: 50000, multiplier: 21.0},
        {ticket_count: 100000, multiplier: 34.0}
      ].each do |values|
        it "should be #{values[:multiplier]} for a ticket count of #{values[:ticket_count]}" do
          limiter = mock
          limiter.stubs(:throttle_base_value).returns(3)
          ProductLimits::ScalingStrategies::TicketQuantities.stubs(:averaged_ticket_count).returns(values[:ticket_count] - 10)
          assert_equal (values[:multiplier] * 3.0).to_i, ProductLimits::ScalingStrategies::TicketQuantities.calculate(limiter, definition, account)
        end
      end
    end

    describe "just stubbing the SQL count" do
      it "should return 1.25 * throttle_base_value" do
        limiter = mock
        limiter.stubs(:throttle_base_value).returns(50)

        ProductLimits::ScalingStrategies::TicketQuantities.stubs(:tickets_in_last_day).returns(80)
        ProductLimits::ScalingStrategies::TicketQuantities.stubs(:tickets_in_last_week).returns(20 * 5)
        ProductLimits::ScalingStrategies::TicketQuantities.stubs(:tickets_in_last_4weeks).returns(245 * 20)
        assert_equal 62, ProductLimits::ScalingStrategies::TicketQuantities.calculate(limiter, definition, account)
      end
    end
  end
end
