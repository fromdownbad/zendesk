require_relative '../../../support/test_helper'

SingleCov.covered!

describe ProductLimits::ScalingStrategies::Stationary do
  describe "#calculate" do
    describe "#calculate" do
      it "should return the `throttle_base_value` on the limiter object passed in" do
        limiter = mock
        limiter.stubs(:throttle_base_value).returns(200)
        assert_equal 200, ProductLimits::ScalingStrategies::Stationary.calculate(limiter, nil, nil)
      end
    end
  end
end
