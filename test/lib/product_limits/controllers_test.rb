require_relative '../../support/test_helper'

SingleCov.covered!

describe ProductLimits::Controllers do
  describe "DEFAULT_LIMITS" do
    it "has the correct format" do
      allowed_keys = [:identifier, :account_setting_threshold, :account_setting_deep_threshold, :account_setting_shallow_threshold, :arturo, :arturo_master_switch, :grandfathered_date, :limits, :interval]
      ProductLimits::Controllers::DEFAULT_LIMITS.each_pair do |_controller_identifier, actions_hash|
        actions_hash.each_pair do |_action, keys|
          assert keys.all? { |key, _v| allowed_keys.include?(key) }
        end
      end
    end

    it "has the required keys" do
      required_keys = [:identifier, :account_setting_threshold, :arturo, :limits, :interval]
      ProductLimits::Controllers::DEFAULT_LIMITS.each_pair do |_controller_identifier, actions_hash|
        actions_hash.each_pair do |_action, action_hash|
          required_keys.each do |required_key|
            assert action_hash.key?(required_key)
          end
        end
      end
    end

    it "has the required limits default" do
      required_outer_keys = [:definitions, :description]
      required_inner_keys = [:threshold]
      ProductLimits::Controllers::DEFAULT_LIMITS.each_pair do |_controller_identifier, actions_hash|
        actions_hash.each_pair do |_action, limits_hash|
          assert required_outer_keys.all? { |k| limits_hash[:limits][:default].key?(k) }
          assert required_inner_keys.all? { |k| limits_hash[:limits][:default][:definitions][0].key?(k) }
        end
      end
    end

    it "has the required values" do
      required_keys = [:identifier, :account_setting_threshold, :arturo, :limits, :interval]
      ProductLimits::Controllers::DEFAULT_LIMITS.each_pair do |_controller_identifier, actions_hash|
        actions_hash.each_pair do |_action, action_hash|
          required_keys.each do |required_key|
            assert action_hash[required_key].present?
          end
        end
      end
    end

    it "has the required limits hash elements" do
      required_outer_values = [:definitions, :description]
      ProductLimits::Controllers::DEFAULT_LIMITS.each_pair do |controller_identifier, actions_hash|
        actions_hash.each_pair do |action, limits_hash|
          limits_hash[:limits].each do |key, required_hash|
            required_outer_values.each do |outer_value_key|
              assert required_hash[outer_value_key].present?, "#{controller_identifier}::#{action}::#{key} should have value present for #{outer_value_key}"
            end
          end
        end
      end
    end

    it "has the required limits values" do
      required_values = [:threshold]
      ProductLimits::Controllers::DEFAULT_LIMITS.each_pair do |controller_identifier, actions_hash|
        actions_hash.each_pair do |action, limits_hash|
          limits_hash[:limits].each do |limit, outer_hash|
            outer_hash[:definitions].each do |definition|
              required_values.each do |value_key|
                assert definition[value_key].present?, "#{controller_identifier}::#{action}::#{limit} should have value present for #{value_key}"
              end
            end
          end
        end
      end
    end

    it "has pagination and deep pagination limits" do
      ProductLimits::Controllers::DEFAULT_LIMITS.each_pair do |controller_identifier, actions_hash|
        actions_hash.each_pair do |action, action_hash|
          if action_hash[:limits][:paginated].present?
            assert_equal action_hash[:limits][:paginated][:definitions].length, 2, "Pagination and deep pagination limits should exist for #{controller_identifier}::#{action}"
          end
        end
      end
    end
  end
end
