require_relative "../support/test_helper"

SingleCov.covered! uncovered: 5

describe Thesaurus do
  fixtures :all

  def create_10_bananas
    10.times do |i|
      TagScore.create!(account: @account, tag_name: "banana#{i}", score: (19 + i))
    end
  end

  before do
    @account = accounts(:minimum)
    @user = users(:minimum_admin)
    TagScore.create!(account: @account,               tag_name: "über",    score: 20)
    TagScore.create!(account: @account,               tag_name: "straße",  score: 21)
    TagScore.create!(account: @account,               tag_name: "生活信号", score: 20) # kanji
    TagScore.create!(account: @account,               tag_name: "día",     score: 21)
    TagScore.create!(account: @account,               tag_name: "soñar",   score: 19)
    TagScore.create!(account: @account,               tag_name: "beetle",  score: 21)
    TagScore.create!(account: @account,               tag_name: "not-on", score: 18)
    TagScore.create!(account: @account,               tag_name: "road!",   score: 18)
    TagScore.create!(account: @account,               tag_name: "on_the",  score: 18)
    TagScore.create!(account: @account,               tag_name: "beeble",  score: 21)
    TagScore.create!(account: accounts(:with_groups), tag_name: "beeple",  score: 18)
    TagScore.create!(account: @account,               tag_name: "banana",  score: 20)
    Tagging.create!(account: @account,                tag_name: "banaras", taggable: @account.owner)
    Tagging.create!(account: @account,                tag_name: "bandana", taggable: @account.owner)
    Tagging.create!(account: @account,                tag_name: "banarus", taggable: @account.owner)
  end

  describe "#complete" do
    it "does not return records when none match" do
      assert_equal [], Thesaurus.complete("xx", @account, @user)
    end

    it "only return records for the correct account" do
      assert_equal ["beeble", "beetle"], Thesaurus.complete("bee", @account, @user).sort
    end

    it "alsos return taggings" do
      assert_equal ["banana", "banaras", "banarus"], Thesaurus.complete("bana", @account, @user).sort
    end

    it "returns taggings with german characters" do
      assert_equal ["über"], Thesaurus.complete("üb", @account, @user)
    end

    it "returns taggings with spanish characters" do
      assert_equal ["soñar"], Thesaurus.complete("so", @account, @user)
    end

    it "returns taggings with Japanese characters" do
      assert_equal ["生活信号"], Thesaurus.complete("生活", @account, @user)
    end

    it "returns taggings with underscores hyphens and punctuation marks" do
      assert_equal ["on_the"], Thesaurus.complete("on", @account, @user)
    end

    it "returns taggings with punctuation marks" do
      assert_equal ["road!"], Thesaurus.complete("ro", @account, @user)
    end

    it "returns taggings with hyphens" do
      assert_equal ["not-on"], Thesaurus.complete("no", @account, @user)
    end

    it "does not return tagging when there are enough tag scores" do
      create_10_bananas
      Thesaurus.stubs(:autocomplete_limit).returns 10
      Thesaurus.complete("bana", @account, @user).sort.must_equal [
        "banana", "banana1", "banana2", "banana3", "banana4", "banana5", "banana6", "banana7", "banana8", "banana9"
      ]
    end
  end

  describe "#autotag" do
    before do
      @text    = "little pretty beetle on the road"
      @under   = "little pretty beetle! but not-on? on_the floor"
      @marks   = "let's try marks like not-on and road!"
      @text_de = "über der anderen Straße, danke"
      @text_es = "podemos soñar de día pero no"
      @text_jp = "生活信号"
    end

    it "returns nothing if tagging is disabled" do
      @account.expects(:settings).returns(stub(ticket_auto_tagging?: false))
      assert_equal [], Thesaurus.autotag(@text, @account)
    end

    it "returns matching tags" do
      assert_equal ["beetle"], Thesaurus.autotag(@text, @account)
    end

    it "returns matching tags without punctuation marks" do
      assert_equal ["beetle", "on_the"], Thesaurus.autotag(@under, @account)
    end

    it "returns nothing when given nothing" do
      assert_equal [], Thesaurus.autotag(nil, @account)
      assert_equal [], Thesaurus.autotag('', @account)
    end

    it "returns nothing when given hyphens and punctuation marks" do
      assert_equal [], Thesaurus.autotag(@marks, @account)
    end

    it "autotags tags with special characters umlaut and sharp S" do
      assert_equal ['straße', 'über'], Thesaurus.autotag(@text_de, @account)
    end

    it "autotags with spanish characters" do
      assert_equal ['día', 'soñar'], Thesaurus.autotag(@text_es, @account)
    end

    it "autotags tags with Japanese (kanji) characters" do
      assert_equal ['生活信号'], Thesaurus.autotag(@text_jp, @account)
    end
  end
end
