require_relative "../support/test_helper"

SingleCov.covered!

describe CiaEventCreator do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:source_id) { 7 }
  let(:via_id_changes) do
    Zendesk::Protobuf::AccountAudits::AuditEvent::AttributeChange.new(
      attribute_name: 'via_id',
      old_value: '1',
      new_value: '2'
    )
  end
  let(:message_changes) do
    Zendesk::Protobuf::AccountAudits::AuditEvent::AttributeChange.new(
      attribute_name: 'message',
      old_value: 'Blocked RuleID: 1 for 30 days',
      new_value: 'Blocked RuleID: 1 for 45 days'
    )
  end
  let(:attribute_change_list) { [via_id_changes, message_changes] }
  let(:kafka_message) do
    event = Zendesk::Protobuf::AccountAudits::AuditEvent.new(
      account_id: account.id,
      actor_id: -1,
      action: ::Zendesk::Protobuf::AccountAudits::AuditEvent::Action::CREATE,
      ip_address: '172.58.4.6',
      source_id: source_id.to_s,
      source_type: 'Occam::Query_Performance_Tracker',
      raw_message: ::Zendesk::Protobuf::AccountAudits::AuditEvent::RawMessage.new(text: 'Blocked RuleID: 1 for 30 days'),
      visible: false,
      source_display_name: 'Views',
      changes: attribute_change_list
    )
    OpenStruct.new(value: event.to_proto)
  end
  let(:kafka_message_json) do
    OpenStruct.new(value: 'json message')
  end
  let(:audit_event) do
    {
      account_id: 1,
      actor_id: -1,
      actor_type: 'System',
      action: 'create',
      ip_address: '172.58.4.6',
      source_id: 7,
      source_type: 'Occam::Query_Performance_Tracker',
      message: 'Blocked RuleID: 1 for 30 days',
      visible: false,
      source_display_name: 'Views',
      changes: { 'via_id' => [1, 2], 'visible' => [false, true] }
    }
  end

  describe '#create!' do
    describe '#ProtoDecoder when decoding audit kafka message' do
      describe 'with an invalid schema' do
        it 'raises a ParseError exception' do
          assert_raises Google::Protobuf::ParseError do
            CiaEventCreator.create!(
              kafka_message_json&.value,
              decoder: CiaEventCreator::ProtoDecoder
            )
          end
        end
      end
      describe 'with a valid schema' do
        it 'does not throw a ParseError exception' do
          CiaEventCreator.create!(
            kafka_message&.value,
            decoder: CiaEventCreator::ProtoDecoder
          )
        end
        it 'creates an entry in cia_events table' do
          CiaEventCreator.create!(
            kafka_message&.value,
            decoder: CiaEventCreator::ProtoDecoder
          )

          assert_equal(source_id, CIA::Event.find_by_source_id(source_id).source_id)
        end
        it 'creates an entry in cia_attribute_changes table' do
          CiaEventCreator.create!(
            kafka_message&.value,
            decoder: CiaEventCreator::ProtoDecoder
          )

          assert_equal(source_id, CIA::AttributeChange.find_by_source_id(source_id).source_id)
        end
        it 're-raises any other exceptions without swallowing' do
          CIA::Event.any_instance.expects(:save!).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))

          assert_raises ActiveRecord::RecordNotSaved do
            CiaEventCreator.create!(
              kafka_message&.value,
              decoder: CiaEventCreator::ProtoDecoder
            )
          end
        end
      end
    end
    describe 'With no decoder' do
      it 'does not invoke Protobuf decoder a ParseError exception' do
        CiaEventCreator::ProtoDecoder.expects(:decode).never

        CiaEventCreator.create!(audit_event)
      end
      it 'creates an entry in cia_events table' do
        CiaEventCreator.create!(audit_event)

        assert_equal(source_id, CIA::Event.find_by_source_id(source_id).source_id)
      end
      it 'creates an entry in cia_attribute_changes table' do
        CiaEventCreator.create!(audit_event)

        assert_equal(source_id, CIA::AttributeChange.find_by_source_id(source_id).source_id)
      end
      it 're-raises any other exceptions without swallowing' do
        CIA::Event.any_instance.expects(:save!).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))

        assert_raises ActiveRecord::RecordNotSaved do
          CiaEventCreator.create!(audit_event)
        end
      end
    end
  end
end
