require_relative '../support/test_helper'

SingleCov.covered!

describe JobWithStatusTracking do
  # This shitty name is because the cleanliness test gets mad if you reuse class names
  class MediumPriorityJobWithStatusTestJob < JobWithStatus
    priority :medium
    def work; end
  end

  class InFlightLimitJobWithStatus < JobWithStatus
    priority :medium
    enforce_in_flight_limit
    def work; end
  end

  let(:account) { accounts(:minimum) }
  let(:key) { "Zendesk-JobWithStatusQueue-#{account.id}" }

  describe 'when there are more than 100 jobs queued' do
    before do
      Zendesk::RedisStore.client.del(key)
      JobWithStatus.any_instance.stubs(:remove_expired_job_statuses)
      Resque.redis.expects(:del).once

      (1..101).each do |id|
        job = MediumPriorityJobWithStatusTestJob.new(id.to_s, account_id: account.id)
        job.perform
      end
    end

    it 'preserves only 100 ids in the queue' do
      assert_equal 100, Zendesk::RedisStore.client.llen(key)
    end
  end

  describe 'setting of inflight limit span tags' do
    describe 'without Datadog active_span' do
      it 'should work' do
        MediumPriorityJobWithStatusTestJob.enqueue(account_id: account.id)
      end
    end

    describe 'with Datadog active_span' do
      let(:parent_stub) { ActiveSpanStub.new }
      let(:active_span) { ActiveSpanStub.new }
      let(:active_span_stub) { OpenStruct.new(active_span: active_span) }

      before do
        active_span.stubs(:parent).returns(parent_stub)
        Datadog.stubs(:tracer).returns(active_span_stub)
      end

      describe 'with no job running' do
        it 'adds facets' do
          InFlightLimitJobWithStatus.enqueue(account_id: account.id)

          expected_facets = {
            'http.rate_limit.inflight_job.limit' => 30,
            'http.rate_limit.inflight_job.used' => 0,
            'http.rate_limit.inflight_job.remaining' => 30,
            'http.rate_limit.inflight_job.used_percentage' => 0
          }

          expected_facets.each do |facet, expected_value|
            value = parent_stub.tags[facet]
            assert_equal expected_value, value,
              "Expected #{facet} to be #{expected_value.inspect}, but got #{value.inspect}"
          end
        end
      end

      describe 'with jobs running' do
        before do
          10.times { InFlightLimitJobWithStatus.enqueue(account_id: account.id) }
        end

        it 'adds facets' do
          expected_facets = {
            'http.rate_limit.inflight_job.limit' => 30,
            'http.rate_limit.inflight_job.used' => 9,
            'http.rate_limit.inflight_job.remaining' => 21,
            'http.rate_limit.inflight_job.used_percentage' => 30
          }

          expected_facets.each do |facet, expected_value|
            value = parent_stub.tags[facet]
            assert_equal expected_value, value,
              "Expected #{facet} to be #{expected_value.inspect}, but got #{value.inspect}"
          end
        end
      end

      describe 'with account limit of 0' do
        before do
          account.settings.total_in_flight_jobs_limit = 0
          account.settings.save!
        end

        it 'works' do
          InFlightLimitJobWithStatus.enqueue(account_id: account.id)
        end

        it 'does not set tags' do
          InFlightLimitJobWithStatus.enqueue(account_id: account.id)

          assert_nil parent_stub.tags['http.rate_limit.inflight_job.limit']
          assert_nil parent_stub.tags['http.rate_limit.inflight_job.used']
          assert_nil parent_stub.tags['http.rate_limit.inflight_job.remaining']
          assert_nil parent_stub.tags['http.rate_limit.inflight_job.used_percentage']
        end
      end

      describe 'with account limit of nil' do
        before do
          account.settings.total_in_flight_jobs_limit = nil
          account.settings.save!
        end

        it 'works' do
          InFlightLimitJobWithStatus.enqueue(account_id: account.id)
        end

        it 'does not set tags' do
          InFlightLimitJobWithStatus.enqueue(account_id: account.id)

          assert_nil parent_stub.tags['http.rate_limit.inflight_job.limit']
          assert_nil parent_stub.tags['http.rate_limit.inflight_job.used']
          assert_nil parent_stub.tags['http.rate_limit.inflight_job.remaining']
          assert_nil parent_stub.tags['http.rate_limit.inflight_job.used_percentage']
        end
      end
    end
  end

  describe '.enforce_in_flight_job_with_status_limit' do
    let(:redis_key) { InFlightLimitJobWithStatus.in_flight_tracking_redis_key(account) }

    before do
      Zendesk::RedisStore.client.flushall
    end

    describe 'a job without `enforce_in_flight_limit`' do
      it 'just enqueues the job without tracking it in redis' do
        Zendesk::RedisStore.client.expects(:scard).never
        MediumPriorityJobWithStatusTestJob.enqueue(account_id: account.id)
      end
    end

    describe 'a job with `enforce_in_flight_limit`' do
      before do
        InFlightLimitJobWithStatus.stubs(:perform)
      end

      it 'allows you to enqueue up to `in_flight_limit_for_account` jobs without logging or raising an error' do
        InFlightLimitJobWithStatus.expects(:in_flight_limit_for_account).times(10).returns(10)
        Rails.logger.expects(:error).never

        10.times do
          InFlightLimitJobWithStatus.enqueue(account_id: account.id)
        end
      end

      describe 'without passing the `raise_on_too_many?` option in' do
        it 'just logs the error' do
          InFlightLimitJobWithStatus.expects(:in_flight_limit_for_account).times(11).returns(10)
          Rails.logger.expects(:error)

          11.times do
            InFlightLimitJobWithStatus.enqueue(account_id: account.id)
          end
        end
      end

      describe 'setting the `raise_on_too_many?` option to true' do
        it 'raises a `TooManyJobs` exception' do
          InFlightLimitJobWithStatus.expects(:in_flight_limit_for_account).times(11).returns(10)
          Rails.logger.expects(:error).never

          10.times do
            InFlightLimitJobWithStatus.enqueue(account_id: account.id, raise_on_too_many?: true)
          end

          assert_raise JobWithStatusTracking::TooManyJobs do
            InFlightLimitJobWithStatus.enqueue(account_id: account.id, raise_on_too_many?: true)
          end
        end

        it 'includes useful information in the `TooManyJobs` exception' do
          InFlightLimitJobWithStatus.expects(:in_flight_limit_for_account).times(11).returns(10)

          uuids = Array.new(10).map do
            InFlightLimitJobWithStatus.enqueue(account_id: account.id, raise_on_too_many?: true)
          end

          begin
            InFlightLimitJobWithStatus.enqueue(account_id: account.id, raise_on_too_many?: true)
          rescue StandardError => e
            e.job_class.must_equal 'InFlightLimitJobWithStatus'
            e.job_ids.sort.must_equal uuids.sort
            e.message.must_include 'InFlightLimitJobWithStatus'
          end
        end
      end
    end
  end

  describe '#in_flight_tracking_redis_key' do
    it 'does not include the job class by default' do
      InFlightLimitJobWithStatus.in_flight_tracking_redis_key(account).wont_include 'InFlightLimitJobWithStatus'
    end
  end

  describe '#in_flight_limit_for_account' do
    it 'defaults to positive infinity' do
      account.settings.total_in_flight_jobs_limit = nil
      account.settings.save!

      InFlightLimitJobWithStatus.in_flight_limit_for_account(account).must_equal Float::INFINITY
    end

    it 'uses the `total_in_flight_jobs_limit` account setting' do
      account.settings.total_in_flight_jobs_limit = 50
      account.settings.save!

      InFlightLimitJobWithStatus.in_flight_limit_for_account(account).must_equal 50
    end
  end

  describe '#remove_expired_job_statuses' do
    before do
      Zendesk::RedisStore.client.del(key)

      (1..3).each do |id|
        job = MediumPriorityJobWithStatusTestJob.new(id.to_s, account_id: account.id)
        job.perform
      end
    end

    it 'deletes ids from queue since they are not found as keys in Redis' do
      assert_equal 1, Zendesk::RedisStore.client.llen(key)
    end
  end

  # I'm testing this like a public method because I want it to be one, but when
  # the JobWithStatusTracking module gets included in a controller tests complain
  # about not calling `allow_parameters` for all public methods.
  describe '#remove_job_from_in_flight_list' do
    let(:redis_key) { InFlightLimitJobWithStatus.in_flight_tracking_redis_key(account) }

    before do
      Zendesk::RedisStore.client.flushall
    end

    it 'removes the uuid from the set' do
      InFlightLimitJobWithStatus.any_instance.stubs(:perform)

      uuids = Array.new(3).map do
        InFlightLimitJobWithStatus.enqueue(account_id: account.id)
      end

      uuid = uuids.first
      job  = InFlightLimitJobWithStatus.new(uuid, account_id: account.id)
      job.send(:remove_job_from_in_flight_list)

      refute_includes Zendesk::RedisStore.client.smembers(redis_key), uuid
    end

    it 'updates the TTL on the set' do
      InFlightLimitJobWithStatus.any_instance.stubs(:perform)
      # updates expire on the enqueue call and on the cleanup call
      Zendesk::RedisStore.client.expects(:expire).with(redis_key, JobWithStatus::IN_FLIGHT_TTL).twice

      uuid = InFlightLimitJobWithStatus.enqueue(account_id: account.id)
      job  = InFlightLimitJobWithStatus.new(uuid, account_id: account.id)
      job.send(:remove_job_from_in_flight_list)
    end
  end
end

class ActiveSpanStub
  attr_reader :tags, :trace_id, :span_id

  def initialize
    @tags = {}
  end

  def set_tag(name, value)
    @tags[name] = value
  end
end
