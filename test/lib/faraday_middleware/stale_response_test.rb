require_relative "../../support/test_helper"
require 'faraday_middleware/stale_response'

SingleCov.covered!

describe FaradayMiddleware::StaleResponse do
  let(:cache) { ActiveSupport::Cache::MemoryStore.new }

  let(:successful_connection) do
    Faraday.new(url: 'http://zendesk.com') do |builder|
      builder.use FaradayMiddleware::StaleResponse, cache
      builder.use ZendeskAPI::Middleware::Response::RaiseError

      builder.adapter :test do |stub|
        stub.get('/hello') { |_| [200, {}, 'hello world'] }
      end
    end
  end

  let(:timeout_connection) do
    Faraday.new(url: 'http://zendesk.com') do |builder|
      builder.use FaradayMiddleware::StaleResponse, cache
      builder.use ZendeskAPI::Middleware::Response::RaiseError

      builder.adapter :test do |stub|
        stub.get('/hello') { |_| [500, {}, 'Internal Server Error'] }
      end
    end
  end

  it 'returns the cached response from the previous request' do
    res = successful_connection.get '/hello'
    res.body.must_equal 'hello world'

    res = timeout_connection.get '/hello'
    res.body.must_equal 'hello world'
  end

  it 'raises an exception when cache is empty' do
    -> { timeout_connection.get '/hello' }.must_raise ZendeskAPI::Error::NetworkError
  end
end
