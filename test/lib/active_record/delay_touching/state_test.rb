require_relative "../../../support/test_helper"

puts "DELETE THIS FILE: #{__FILE__} and the associated code." if RAILS5

if RAILS4
  SingleCov.covered!

  describe ActiveRecord::DelayTouching::State do
    fixtures :all

    class BadComment < Comment
      after_save do
        touch
        raise 'a problem happened'
      end
    end

    describe 'rolled back objects' do
      it 'does not loop forever' do
        ActiveRecord::Base.delay_touching do
          comment = BadComment.new(account: accounts(:minimum), body: 'something', ticket: tickets(:minimum_1), audit: events(:create_audit_for_minimum_ticket_1))
          begin
            comment.save!
          rescue RuntimeError
          end
        end
      end
    end
  end
end
