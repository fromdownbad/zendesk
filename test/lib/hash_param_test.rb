require_relative "../support/test_helper"
require 'hash_param'

SingleCov.covered!

describe HashParam, '.ish?' do
  it "returns true if the argument responds to dig and is not an Array" do
    assert HashParam.ish?({})
    refute HashParam.ish?([])
    assert HashParam.ish?(::ActionController::Parameters.new({}))
    refute HashParam.ish?(Set.new)
    refute HashParam.ish?("")
  end
end
