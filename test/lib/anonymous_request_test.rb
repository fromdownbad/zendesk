require_relative "../support/test_helper"

SingleCov.covered!

describe 'AnonymousRequest' do
  extend ArturoTestHelper

  fixtures :accounts, :ticket_fields, :users, :account_settings

  describe 'An AnonymousRequest' do
    before do
      @account = accounts(:minimum)
      @account.stubs(nice_id_sequence: stub(next: 123))
      @user = users(:minimum_end_user)
      @custom_field = FieldText.create!(
        account: @account,
        title: 'Favorite Cheese',
        title_in_portal: 'Favorite Cheese',
        is_active: true,
        is_editable_in_portal: true,
        is_visible_in_portal: true
      )
      @request_parameters = {
        account: @account,
        email: @user.email,
        subject: 'Stuff',
        description: 'Some stuff',
        set_tags: 'dropbox',
        via_id: '17',
        locale_id: '2',
        fields: { @custom_field.id.to_s => 'Provolone' },
      }
    end

    describe 'with a custom field that is visible and editable by end users' do
      before do
        @request = AnonymousRequest.new(@request_parameters)
      end

      it 'includes the field in #fields' do
        assert_equal({ @custom_field.id.to_s => 'Provolone' }, @request.custom_fields)
      end
    end

    describe 'with a custom field that is not editable by end users' do
      before do
        @custom_field.update_attributes!(is_editable_in_portal: false)
        @request = AnonymousRequest.new(@request_parameters)
      end

      it 'does not include the field in #fields' do
        assert_equal({ }, @request.custom_fields)
      end
    end

    describe '#to_ticket' do
      before do
        @request = AnonymousRequest.new(@request_parameters)
        @ticket = @request.to_ticket
      end

      it 'returns a new Ticket' do
        assert @ticket.is_a?(Ticket)
        assert @ticket.new_record?
      end

      it 'copies fields properly' do
        assert_equal @account, @ticket.account
        assert_equal @request.via_id.to_s, @ticket.via_id.to_s
        assert_equal @request.subject, @ticket.subject
        assert_equal @request.description, @ticket.description
        assert_equal @request.set_tags, @ticket.set_tags
        assert_equal_with_nil @request.client, @ticket.client # TODO: make them not nil
        assert_equal_with_nil @request.priority_id, @ticket.priority_id # TODO: make them not nil
        assert_equal_with_nil @request.ticket_type_id, @ticket.ticket_type_id # TODO: make them not nil
        assert_equal @request.instance_variable_get(:@locale_id), @ticket.locale_id
        custom_field_value = @request_parameters[:fields][@custom_field.id.to_s]
        assert_equal custom_field_value, @custom_field.value(@ticket)
      end

      it "constructs a valid ticket" do
        @ticket.will_be_saved_by(@user)
        assert @ticket.save, "Errors: #{@ticket.errors.full_messages}"
      end

      it 'is flagged' do
        @ticket.will_be_saved_by(@user)
        assert @ticket.audit.flagged?
      end

      describe 'with brand_id param' do
        it 'sets brand_id param to ticket' do
          @request = AnonymousRequest.new(@request_parameters.merge(brand_id: @account.default_brand_id))
          assert_equal @account.default_brand_id, @request.to_ticket.brand_id
        end
      end

      describe 'without brand_id param' do
        it 'sets default brand_id to ticket' do
          @request = AnonymousRequest.new(@request_parameters)
          ticket = @request.to_ticket
          ticket.will_be_saved_by(@user)
          ticket.save!
          assert_not_nil ticket.brand_id
          assert_equal @account.default_brand_id, ticket.brand_id
        end
      end
    end

    describe '#to_suspended_ticket' do
      before do
        @request = AnonymousRequest.new(@request_parameters)
        @ticket = @request.to_suspended_ticket
      end

      it 'returns a new SuspendedTicket' do
        assert @ticket.is_a?(SuspendedTicket)
        assert @ticket.new_record?
      end

      it 'copies fields properly' do
        assert_equal @account, @ticket.account
        assert_equal @request.via_id.to_s, @ticket.via_id.to_s
        assert_equal @request.subject, @ticket.subject
        assert_equal @request.description, @ticket.content
        assert_equal @request.author.name, @ticket.from_name
        assert_equal @request.author.email, @ticket.from_mail
        assert_equal @request.author, @ticket.author
        assert_equal({
          fields: @request.custom_fields,
          priority_id: @request.priority_id,
          ticket_type_id: @request.ticket_type_id,
          set_tags: @request.set_tags,
          locale_id: @request.instance_variable_get(:@locale_id)
        }, @ticket.properties)
      end
    end

    describe '#to_suspended_ticket when the email address is blacklisted' do
      before do
        @request = AnonymousRequest.new(@request_parameters)
        @account.stubs(:blacklists?).returns(true)
        @account.stubs(:whitelists?).returns(false)
        @ticket = @request.to_suspended_ticket
      end
      it 'sets the cause to "blacklisted"' do
        assert_equal SuspensionType.BLOCKLISTED, @ticket.cause
      end
    end

    describe '#to_suspended_ticket when the email address is not blacklisted or whitelisted' do
      before do
        @request = AnonymousRequest.new(@request_parameters)
        @account.stubs(:blacklists?).returns(false)
        @account.stubs(:whitelists?).returns(false)
        @ticket = @request.to_suspended_ticket
      end

      it 'sets the cause to "signup-required"' do
        assert_equal SuspensionType.SIGNUP_REQUIRED, @ticket.cause
      end
    end

    describe '#to_suspended_ticket when the blacklist contains * and the address is whitelisted' do
      before do
        @request = AnonymousRequest.new(@request_parameters)
        @account.stubs(:domain_blacklist).returns("*")
        @account.stubs(:domain_whitelist).returns(@user.email)
        @ticket = @request.to_suspended_ticket
      end

      it 'returns signup required suspension' do
        assert_equal SuspensionType.SIGNUP_REQUIRED, @ticket.cause
      end
    end
  end

  describe '#priority_id' do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
    end

    describe 'when editable by end users' do
      before do
        priority_field = @account.field_priority
        priority_field.update_attributes!(
          is_active: true,
          is_visible_in_portal: true,
          is_editable_in_portal: true
        )
        @request = AnonymousRequest.new(account: @account,
                                        priority_id: '2')
      end
      it 'returns the passed priority' do
        assert_equal '2', @request.priority_id
      end
    end

    describe 'when not editable by end users' do
      before do
        priority_field = @account.field_priority
        priority_field.update_attributes!(is_editable_in_portal: false)
        @request = AnonymousRequest.new(
          account: @account,
          priority_id: '2'
        )
      end
      it 'is blank' do
        assert @request.priority_id.blank?
      end
    end
  end

  describe '#ticket_type_id' do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
    end

    describe 'when editable by end users' do
      before do
        ticket_type_field = @account.field_ticket_type
        ticket_type_field.update_attributes!(
          is_active: true,
          is_visible_in_portal: true,
          is_editable_in_portal: true
        )
        @account.stubs(:field_ticket_type).returns(ticket_type_field)
        @request = AnonymousRequest.new(
          account: @account,
          ticket_type_id: '3'
        )
      end
      it 'returns the passed ticket_type' do
        assert_equal '3', @request.ticket_type_id
      end
    end

    describe 'when not editable by end users' do
      before do
        ticket_type_field = @account.field_ticket_type
        ticket_type_field.update_attributes!(is_editable_in_portal: false)
        @request = AnonymousRequest.new(
          account: @account,
          ticket_type_id: '3'
        )
      end
      it 'is blank' do
        assert @request.ticket_type_id.blank?
      end
    end
  end

  describe '#author' do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
    end

    describe 'with an email address that exists' do
      before do
        @request = AnonymousRequest.new(account: @account, email: @user.email, description: 'Some stuff')
      end
      it 'looks up the account by email' do
        assert_equal @user, @request.author
      end
    end

    describe 'with an email address that does not yet exist and a name' do
      before do
        @request = AnonymousRequest.new(account: @account, email: 'sue.bee@example.org', name: 'Sue Bee', description: 'Some stuff')
      end
      it 'creates a new user' do
        assert_equal 'sue.bee@example.org', @request.author.email
        assert_equal 'Sue Bee', @request.author.name
      end
    end

    describe 'with an email address that does not yet exist and no name' do
      before do
        @request = AnonymousRequest.new(account: @account, email: 'chuck.shaw@example.org', description: 'Some stuff')
      end
      it 'creates a new user, attempting to determine the name' do
        assert_equal 'chuck.shaw@example.org', @request.author.email
        assert_equal 'Chuck Shaw', @request.author.name
      end
    end
  end
end
