require_relative "../support/test_helper"
require 'zendesk/acme_job/error'

SingleCov.covered! uncovered: 3

describe AcmeReporter do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:exception) { Zendesk::AcmeJob::WrappedError.new(Exception.new).tap { |e| e.set_backtrace(['backtrace']) } }
  let(:error) { "registration failed" }

  describe "#body" do
    it "returns the body for the message" do
      msg = <<~MESSAGE.chomp
        registration failed

        Account: minimum
        Account id: 90538
        Hostmapping:
        Shard: 1

        Exception: `Exception: Exception`
        Backtrace:

        ```
        backtrace
        ```

      MESSAGE
      assert_equal msg, AcmeReporter.new('support', ticket_id: nil).send(:body, account, error, exception)
    end
  end

  describe "#report_error" do
    before do
      stub_request(:get, "https://support.zendesk-test.com/api/v2/groups").
        with(headers: {'Accept' => 'application/json', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => /ZendeskAPI Ruby [\d]+(\.[\d]+)+/}).
        to_return(status: 200, body: {groups: [{name: 'Dev (SecDev)', id: 1234}]}.to_json, headers: {'Content-Type' => 'application/json'})
    end

    describe "with no ticket_id" do
      before do
        @reporter = AcmeReporter.new('support', ticket_id: nil)

        stub_request(:post, "https://support.zendesk-test.com/api/v2/tickets").
          with(body: "{\"ticket\":{\"requester\":{\"name\":\"SQID\",\"email\":\"sqid+le@zendesk.com\"},\"subject\":\"Let's Encrypt Error for minimum\",\"comment\":{\"body\":\"registration failed\\n\\nAccount: minimum\\nAccount id: 90538\\nHostmapping:\\nShard: 1\\n\\nException: `Exception: Exception`\\nBacktrace:\\n\\n```\\nbacktrace\\n```\\n\",\"public\":false},\"type\":\"task\",\"priority\":\"normal\",\"status\":\"open\",\"group_id\":true,\"custom_fields\":[{\"id\":32142077,\"value\":\"3_moderate\"},{\"id\":336767,\"value\":\"about_domains_ssl\"},{\"id\":22694523,\"value\":\"minimum\"}],\"tags\":[\"lets_encrypt\"]}}").
          to_return(status: 200, body: {id: 1, ticket: {id: 1, requester_id: 2}}.to_json, headers: {'Content-Type' => 'application/json'})
      end

      it "reports the error to Z1" do
        ticket = @reporter.report_error(account, error, exception: exception)
        assert_instance_of ZendeskAPI::Ticket, ticket
      end

      it "sets the ticket_id" do
        assert @reporter.report_error(account, error, exception: exception)
        assert_equal 1, @reporter.ticket_id
      end
    end

    describe "with an existing open ticket" do
      before do
        stub_request(:get, "https://support.zendesk-test.com/api/v2/tickets/1").
          with(headers: {'Accept' => 'application/json', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => /ZendeskAPI Ruby [\d]+(\.[\d]+)+/}).
          to_return(status: 200, body: {ticket: {requester_id: 2, status: 'open'}}.to_json, headers: {'Content-Type' => 'application/json'})

        stub_request(:put, "https://support.zendesk-test.com/api/v2/tickets/1").
          with(body: "{\"ticket\":{\"comment\":{\"body\":\"registration failed\\n\\nAccount: minimum\\nAccount id: 90538\\nHostmapping:\\nShard: 1\\n\\nException: `Exception: Exception`\\nBacktrace:\\n\\n```\\nbacktrace\\n```\\n\",\"public\":false,\"author_id\":2}}}",
               headers: {'Accept' => 'application/json', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Content-Type' => 'application/json'}).
          to_return(status: 200, body: "", headers: {})

        ZendeskAPI::User.stubs(:find).returns(nil)
      end

      it "adds the error as a comment if there's an open ticket" do
        ticket = AcmeReporter.new('support', ticket_id: 1).report_error(account, error, exception: exception)
        assert_not_nil ticket.comment["body"]
      end
    end

    describe "with an existing closed ticket" do
      before do
        stub_request(:get, "https://support.zendesk-test.com/api/v2/tickets/1").
          with(headers: {'Accept' => 'application/json', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => /ZendeskAPI Ruby [\d]+(\.[\d]+)+/}).
          to_return(status: 200, body: {ticket: {requester_id: 2, status: 'closed'}}.to_json, headers: {'Content-Type' => 'application/json'})

        stub_request(:post, "https://support.zendesk-test.com/api/v2/tickets").
          with(body: "{\"ticket\":{\"requester\":{\"name\":\"SQID\",\"email\":\"sqid+le@zendesk.com\"},\"subject\":\"Let's Encrypt Error for minimum\",\"comment\":{\"body\":\"registration failed\\n\\nAccount: minimum\\nAccount id: 90538\\nHostmapping:\\nShard: 1\\n\\nException: `Exception: Exception`\\nBacktrace:\\n\\n```\\nbacktrace\\n```\\n\",\"public\":false},\"type\":\"task\",\"priority\":\"normal\",\"status\":\"open\",\"group_id\":true,\"custom_fields\":[{\"id\":32142077,\"value\":\"3_moderate\"},{\"id\":336767,\"value\":\"about_domains_ssl\"},{\"id\":22694523,\"value\":\"minimum\"}],\"tags\":[\"lets_encrypt\"],\"via_followup_source_id\":1}}").
          to_return(status: 200, body: {ticket: {id: 2, requester_id: 2}}.to_json, headers: {'Content-Type' => 'application/json'})
      end

      it "opens a new ticket" do
        ticket = AcmeReporter.new('support', ticket_id: 1).report_error(account, error, exception: exception)
        assert_equal ticket.id, 2
      end
    end
  end
end
