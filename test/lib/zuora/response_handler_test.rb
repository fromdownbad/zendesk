require_relative "../../support/test_helper"

SingleCov.not_covered!

describe 'ResponseHandler' do
  describe "Zuora Response Handler" do
    describe "created with failure case from zuora" do
      before do
        params = {
          id: "2c92c0f8385d9f350138775869543830",
          tenantId: "XXXXX",
          timestamp: "1342036904059",
          token: "t3nlakkgjiAMxXTyFY3CRG2W6WWbtIR5",
          responseSignature: "MjE3NDZmMGFmYTRhZjJhMTI0NGExMTczOWM3MDhlOTI=",
          success: "false",
          errorCode: "GatewayTransactionError",
          errorMessage: "Error processing transaction.CVV2 Mismatch",
          signature: "YWRmMWI0ZjBlNzBlNmM1ZDUwOTgwMDU5ZjkzNzM5OWU=",
          field_passthrough2: 'http://trial.zendesk-test.com'
        }
        @credit_card_form_callout_handler = ZendeskBillingCore::Zuora::CreditCardFormCalloutHandler.new(params)
      end

      describe "#valid?" do
        it "returns false" do
          assert_equal false, @credit_card_form_callout_handler.valid?
        end
      end

      describe "#subscription_details" do
        it "is nil" do
          assert_nil @credit_card_form_callout_handler.subscription_details
        end
      end

      describe "#errors" do
        it "logs the errors if present" do
          Rails.logger.expects(:warn).
            with("CARD_SUBMISSION_FAILURE: http://trial.zendesk-test.com, {:errorCode=>\"GatewayTransactionError\", :errorMessage=>\"Error processing transaction.CVV2 Mismatch\"}")
          @credit_card_form_callout_handler.errors
        end
      end

      describe "#error_title" do
        it "returns an empty string for error_title" do
          assert_equal "", @credit_card_form_callout_handler.error_title
        end
      end

      describe "#error_messages" do
        before do
          @error_message = [{
            message: I18n.t("txt.views.subscription_payment_view.credit_card.errors.gateway_transaction_error.cvv_issue_no_contact")
          }]
        end

        it "returns the correct error message" do
          assert_equal @error_message, @credit_card_form_callout_handler.error_messages
        end
      end
    end

    describe "created with success case from zuora and the signature is verified" do
      before do
        Timecop.freeze(Time.new(2015, 07, 15, 7, 0, 0, '+00:00').utc)
        ZendeskBillingCore::Zuora::CreditCardFormCalloutHandler.any_instance.stubs(:verify_timestamp!)
        ZendeskBillingCore::Zuora::CreditCardFormCalloutHandler.any_instance.stubs(:verify_signature!)
        params = {
          id: "2c92c0f8385d9f350138775869543830",
          tenantId: "XXXXX",
          timestamp: '1436943600000',
          token: "070f09bcdbe74ec71a801d5a3d78f41f",
          responseSignature: "MzYxOTZkNWM3MzkxYTNkOTc1MTNiNjIyNzUyOThjNzk=",
          success: "true",
          refId: "2c92c0f939072aca01390881697018e7",
          field_passthrough1: "trial_upgrade",
          field_passthrough2: 'http://trial.zendesk-test.com',
          field_passthrough3: "billing_cycle_type:1,max_agents:5,plan_type:3,promo_code:COUPONCODE",
          signature: "NTNiYmNlOTYwOWFhMDE5MGJhNzJiYjk0ZDIwMDcyNzA="
        }
        @credit_card_form_callout_handler = ZendeskBillingCore::Zuora::CreditCardFormCalloutHandler.new(params)
      end

      describe "#valid?" do
        it "returns true" do
          assert @credit_card_form_callout_handler.valid?
        end
      end

      describe "#subscription_details" do
        it "is correct" do
          data = @credit_card_form_callout_handler.subscription_details
          assert_equal({
            max_agents: "5",
            plan_type: "3",
            billing_cycle_type: "1",
            promo_code: "COUPONCODE",
            payment_method_reference_id: "2c92c0f939072aca01390881697018e7"
          }, data)
        end
      end
    end

    describe "created with success case from zuora and the signature verification fails" do
      before do
        ZendeskBillingCore::Zuora::Page.stubs(:verify_signature!).raises(ZendeskBillingCore::Zuora::Page::Errors::InvalidSignature)
        ZendeskBillingCore::Zuora::Page.stubs(:verify_timestamp!).raises(ZendeskBillingCore::Zuora::Page::Errors::Timeout)

        params = {
          :id                 => "2c92c0f8385d9f350138775869543830",
          :tenantId           => "XXXXX",
          :timestamp          => "1344467462524",
          :token              => "070f09bcdbe74ec71a801d5a3d78f41f",
          :responseSignature  => "OGZlZDgyZmJkYWFkMWU3NWJmZjExOTNkNmRhMGY2MjU=",
          :success            => "true",
          "refId"             => "2c92c0f939072aca01390881697018e7",
          :field_passthrough1 => "trial_upgrade",
          :field_passthrough2 => "4",
          :field_passthrough3 => "billing_cycle_type:1,max_agents:5,plan_type:3,promo_code:COUPONCODE",
          :signature          => "NTNiYmNlOTYwOWFhMDE5MGJhNzJiYjk0ZDIwMDcyNzA=",
        }
        @credit_card_form_callout_handler = ZendeskBillingCore::Zuora::CreditCardFormCalloutHandler.new(params)
      end

      describe "#valid?" do
        it "raises Zendesk::Zuora::Page::Errors::InvalidSignature" do
          assert_raise ZendeskBillingCore::Zuora::Page::Errors::InvalidSignature, 'ZendeskBillingCore::Zuora::Page::Errors::InvalidSignature' do
            @credit_card_form_callout_handler.valid?
          end
        end
      end
    end
  end
end
