require_relative "../support/test_helper"

SingleCov.covered! uncovered: 5

describe 'MemoryStore' do
  before do
    Timecop.freeze
  end

  it "is used" do
    assert_equal TestMemoryStore, Rails.cache.class
  end

  it "stores indefinetly without expire" do
    Rails.cache.write("xxx", 1)
    assert_equal 1, Rails.cache.read("xxx")
    Timecop.freeze(1.year.from_now)
    assert_equal 1, Rails.cache.read("xxx")
  end

  it "removes when expired" do
    Rails.cache.write("xxx", 1, expires_in: 5)
    assert_equal 1, Rails.cache.read("xxx")
    Timecop.freeze(6.seconds.from_now)
    assert_nil Rails.cache.read("xxx")
  end

  it "is inplace modifyable" do
    Rails.cache.write("xxx", "aaa")
    assert_equal "bbb", Rails.cache.read("xxx").tr!("a", "b")
  end

  it "does not change cache when doing inplace modify" do
    Rails.cache.write("xxx", "aaa")
    assert_equal "bbb", Rails.cache.read("xxx").tr!("a", "b")
    assert_equal "aaa", Rails.cache.read("xxx")
  end

  it "returns true on write" do
    assert(Rails.cache.write("xxx", "aaa"))
  end

  it "returns integers on increment" do
    # :raw is needed for increment
    Rails.cache.write("xxx", 0, raw: true)
    assert_equal 1, Rails.cache.increment("xxx")
  end

  it "returns integers on decrement" do
    # :raw is needed for decrement
    Rails.cache.write("xxx", 0, raw: true)
    assert_equal -1, Rails.cache.decrement("xxx")
  end

  it "supports :unless_exist" do
    assert(Rails.cache.write("xxx", "aaa", unless_exist: true))
    assert_equal false, Rails.cache.write("xxx", "aaa", unless_exist: true)
    assert_equal false, Rails.cache.write("xxx", "aaa", unless_exist: true)
  end

  it "supports :unless_exist + :expires_in" do
    assert(Rails.cache.write("xxx", "aaa", expires_in: 0.1, unless_exist: true))
    Timecop.travel(1.second.from_now) do
      assert(Rails.cache.write("xxx", "aaa", unless_exist: true))
    end
  end
end
