require_relative "../support/test_helper"

# build_for_sc_data_service does not have existing test cov
SingleCov.covered! uncovered: 9

describe 'KragleConnection' do
  fixtures :accounts
  let(:account) { accounts(:minimum) }

  describe '.build_for_help_center' do
    it 'returns a help-center connection' do
      connection = KragleConnection.build_for_help_center(account)
      assert_equal 'https://proxy-nlb.pod-1.zdsystest.com/', connection.url_prefix.to_s
    end

    it 'returns a sets the right headers' do
      connection = KragleConnection.build_for_help_center(account)
      assert_equal 'minimum.zendesk-test.com', connection.headers['Host']
    end

    it 'returns a sets the connection timeouts' do
      connection = KragleConnection.build_for_help_center(account)
      assert_equal connection.options.timeout, 3
      assert_equal connection.options.open_timeout, 2
    end
  end

  describe '.build_for_account' do
    it 'returns a connection' do
      connection = KragleConnection.build_for_account(account, 'my_service')
      assert_equal 'https://minimum.zendesk-test.com/', connection.url_prefix.to_s
    end
  end

  describe '.build_for_sell' do
    it 'returns a connection' do
      connection = KragleConnection.build_for_sell(account)
      assert_equal 'https://minimum.zendesk-test.com/api/sell/private/', connection.url_prefix.to_s
    end
  end

  describe '.build_for_answer_bot_service' do
    let(:connection) { KragleConnection.build_for_answer_bot_service(account) }

    it 'returns a connection' do
      assert_equal 'https://minimum.zendesk-test.com/', connection.url_prefix.to_s
    end

    it 'overrides the default read timeout option' do
      assert_equal 10, connection.options.timeout
    end

    it 'overrides the default open timeout option' do
      assert_equal 5, connection.options.open_timeout
    end
  end

  describe '.build_for_sunshine_account_config' do
    let(:connection)  { KragleConnection.build_for_sunshine_account_config(accounts(:minimum)) }

    it 'overrides the default read timeout option' do
      assert_equal 60, connection.options.timeout
    end

    it 'overrides the default connection timeout option' do
      assert_equal 10, connection.options.open_timeout
    end

    it 'sets account ID to X-Zendesk-Account-Id header' do
      assert_equal '90538', connection.headers.fetch('X-Zendesk-Account-Id')
    end

    it 'sets the url prefix to account header' do
      assert_equal 'https://accounts.zendesk-test.com/api/sunshine/account_config/private/', connection.url_prefix.to_s
    end
  end

  describe '.build_for_explore' do
    it 'returns a explore connection' do
      connection = KragleConnection.build_for_explore(account)
      assert_equal 'https://proxy-nlb.pod-1.zdsystest.com/', connection.url_prefix.to_s
    end

    it 'sets correct Host header' do
      connection = KragleConnection.build_for_explore(account)
      assert_equal 'minimum.zendesk-test.com', connection.headers['Host']
    end

    it 'returns a sets the connection timeouts' do
      connection = KragleConnection.build_for_explore(account)
      assert_equal connection.options.timeout, 3
      assert_equal connection.options.open_timeout, 2
    end

    describe 'for accounts with mixed-case subdomains' do
      before do
        account.subdomain = 'Foo'
        account.save!
      end

      it 'downcases the Host header' do
        connection = KragleConnection.build_for_explore(account)
        assert_equal connection.headers['Host'], 'foo.zendesk-test.com'
      end
    end
  end
end
