require_relative "../../support/test_helper"

SingleCov.covered!

describe Datadog::AnswerBot do
  let(:via_id) { Zendesk::Types::ViaType.WEB_FORM }
  let(:subdomain) { 'foo' }
  let(:deflection_channel_id) { Zendesk::Types::ViaType.MAIL }
  let(:resolution_channel_id) { Zendesk::Types::ViaType.WEB_FORM }
  let(:statsd_client) { mock('statsd_client') }
  let(:base_tags) do
    %W[
      via_id:#{via_id}
      subdomain:#{subdomain}
      deflection_channel_id:#{deflection_channel_id}
      resolution_channel_id:#{resolution_channel_id}
    ]
  end
  let(:subject) do
    Datadog::AnswerBot.new(
      via_id: via_id,
      subdomain: subdomain,
      deflection_channel_id: deflection_channel_id,
      resolution_channel_id: resolution_channel_id
    )
  end

  before do
    Zendesk::StatsD::Client.stubs(:new).
      with(namespace: ['answer_bot.deflection']).
      returns(statsd_client)
  end

  it 'sends `solve_initiated` metric to datadog' do
    expected_tags = base_tags + ['mobile:true', 'something:extra']
    statsd_client.expects(:increment).with('solve_initiated', tags: expected_tags)

    subject.solve_initiated(mobile: true, something: 'extra')
  end

  it 'sends `solved` metric to datadog' do
    expected_tags = base_tags + ['mobile:true', 'something:extra']
    statsd_client.expects(:increment).with('solved', tags: expected_tags)

    subject.solved(mobile: true, something: 'extra')
  end

  it 'sends `solved_undone` metric to datadog' do
    expected_tags = base_tags + ['something:extra']
    statsd_client.expects(:increment).with('solved_undone', tags: expected_tags)

    subject.solved_undone(something: 'extra')
  end

  it 'sends `irrelevant` metric to datadog' do
    expected_tags = base_tags + ['something:extra']
    statsd_client.expects(:increment).with('irrelevant', tags: expected_tags)

    subject.irrelevant(something: 'extra')
  end

  it 'sends `solved_too_quick` metric to datadog' do
    expected_tags = base_tags + ['something:extra']
    statsd_client.expects(:increment).with('solved_too_quick', tags: expected_tags)

    subject.solved_too_quick(something: 'extra')
  end

  it 'sends `ticket_creation_failed` metric to datadog' do
    expected_tags = base_tags + ['something:extra']
    statsd_client.expects(:increment).with('ticket_creation_failed', tags: expected_tags)

    subject.ticket_creation_failed(something: 'extra')
  end

  it 'sends `deflection_solve_failed` metric to datadog' do
    expected_tags = base_tags + ['something:extra']
    statsd_client.expects(:increment).with('deflection_solve_failed', tags: expected_tags)

    subject.deflection_solve_failed(something: 'extra')
  end
end
