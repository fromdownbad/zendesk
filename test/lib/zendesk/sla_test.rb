require_relative '../../support/test_helper'

SingleCov.covered!

describe Zendesk::Sla do
  it 'defines the `APM_SERVICE_NAME` constant' do
    assert_equal 'classic-sla', Zendesk::Sla::APM_SERVICE_NAME
  end
end
