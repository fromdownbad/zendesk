require_relative "../../../support/test_helper"

SingleCov.not_covered! # we don't have a folder for script/ tests ...

Zendesk::Archive::Archiver.class_eval do
  attr_reader :options
end

describe 'ArchiverScript' do
  fixtures :all

  def run_with_arguments(*args)
    Kernel.silence_warnings do
      begin
        old_argv = ARGV
        Object.const_set("ARGV", args)
        load "#{Rails.root}/script/archiver"
      ensure
        Object.const_set("ARGV", old_argv)
      end
    end
  end

  # rubocop:disable Style/GlobalVars
  describe "archiver script" do
    it "always deletes after archive" do
      assert(ZendeskArchive.delete_after_archive)
    end

    it "supports --dry-run" do
      Ticket.any_instance.expects(:archive!).never
      run_with_arguments("--dry-run", "--run_once")
      assert($archiver.dry_run)
    end
  end
  # rubocop:enable Style/GlobalVars
end
