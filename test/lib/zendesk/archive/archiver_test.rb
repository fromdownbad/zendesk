require_relative "../../../support/test_helper"
require_relative "../../../support/archive_helper"

SingleCov.covered! uncovered: 18

describe 'Archiver' do
  fixtures :accounts, :tickets, :users, :organizations, :organization_memberships, :sequences

  let(:archiver) { Zendesk::Archive::Archiver.new(run_once: true) }
  let(:current_shard) { 1 }
  let(:account) { accounts(:minimum) }

  describe 'with archivable ticket' do
    before do
      Zendesk::Configuration::DBConfig.stubs(:cluster_for_shard).returns('1.1')

      account.settings.archive_after_days = 1
      account.save!
      @ticket = account.tickets.build(
        requester: users(:minimum_end_user),
        assignee: users(:minimum_agent),
        subject: "Archivable Ticket",
        description: "This is a description",
        organization: organizations(:minimum_organization1),
        external_id: 5,
        group: users(:minimum_agent).groups.first
      )
      @ticket.updated_at = Time.now - 3.days
      @ticket.will_be_saved_by account.users.first
      @ticket.status_id = StatusType.CLOSED
      @ticket.save!
      @statsd_client = archiver.statsd_client

      Arturo.enable_feature!(:database_backoff_maxwell_filters_lag)
      ZendeskDatabaseSupport::Sensors::MaxwellFiltersLagSensor.any_instance.stubs(:measure).returns(1)
    end

    around do |test|
      with_in_memory_archive_router(test)
    end

    describe "metrics" do
      it "should have correct namespace" do
        assert_equal "zendesk.archiver.v2", archiver.statsd_client.namespace
      end

      it "should have mod 1 of 1 tag when no worker-mod" do
        assert_equal ["mod:1_1"], archiver.statsd_client.tags
      end

      describe "for mod-worker" do
        let(:archiver) do
          Zendesk::Archive::Archiver.new(run_once: true, worker_number: 1, worker_count: 2)
        end

        it "should have worker-mod tag" do
          assert_equal ["mod:1_2"], archiver.statsd_client.tags
        end
      end
    end

    describe "with no tickets eligible to archive" do
      before do
        account.settings.find_by_name("archive_after_days").destroy
      end

      it "does not send ticket-specific metrics" do
        @statsd_client.expects(:histogram).never.with(anything, anything)
        @statsd_client.expects(:gauge).never.with(anything, anything)
        @statsd_client.expects(:increment).once.with(Zendesk::Archive::Archiver::DISABLED_ACCOUNTS_METRIC)
        @statsd_client.expects(:increment).once.with(Zendesk::Archive::Archiver::STARTED_METRIC)
        @statsd_client.expects(:increment).once.with(Zendesk::Archive::Archiver::SHARD_ITERATION_METRIC)
        archiver.run
      end
    end

    describe "with default archive_after_days (120)" do
      before do
        account.settings.find_by_name("archive_after_days").destroy

        @old_ticket = account.tickets.build(
          requester: users(:minimum_end_user),
          assignee: users(:minimum_agent),
          subject: "Archivable Ticket",
          description: "This is a description",
          organization: organizations(:minimum_organization1),
          external_id: 5,
          group: users(:minimum_agent).groups.first
        )
        @old_ticket.updated_at = Time.now - 123.days
        @old_ticket.will_be_saved_by account.users.first
        @old_ticket.status_id = StatusType.CLOSED
        @old_ticket.save!
      end

      it "only archives tickets older than 120 days" do
        archiver.run
        refute @ticket.ticket_archive_stub(true)
        assert @old_ticket.ticket_archive_stub(true)
      end
    end

    describe "a ticket newer than the time specified" do
      before do
        @ticket.update_columns(updated_at: Time.now)
      end

      it "does not get archived" do
        archiver.run
        refute @ticket.ticket_archive_stub
      end
    end

    describe "a ticket with an updated at older than the time specified" do
      before do
        @ticket.update_columns(updated_at: Time.now - 3.days)
      end

      it "is archived" do
        archiver.run
        assert ZendeskArchive.router.get(@ticket.ticket_archive_stub)
      end

      it "calls with key_version 2" do
        Ticket.any_instance.expects(:archive!).with(key_version: 2).at_least_once
        archiver.run
      end

      it "results in metrics" do
        @statsd_client.expects(:histogram).at_least_once.with(Zendesk::Archive::Archiver::TIME_METRIC, anything, anything)
        @statsd_client.expects(:gauge).at_least_once.with(Zendesk::Archive::Archiver::AGE_METRIC, anything, anything)
        @statsd_client.expects(:increment).times(3).with(Zendesk::Archive::Archiver::VISITED_TICKETS_METRIC)
        @statsd_client.expects(:increment).at_least_once.with(Zendesk::Archive::Archiver::DISABLED_ACCOUNTS_METRIC)
        @statsd_client.expects(:increment).at_least(1).with(Zendesk::Archive::Archiver::STARTED_METRIC)
        @statsd_client.expects(:increment).at_least(1).with(Zendesk::Archive::Archiver::SHARD_ITERATION_METRIC)
        @statsd_client.expects(:histogram).at_least(1).with(Zendesk::Archive::Archiver::TICKET_FIND_DURATION_METRIC, anything)
        @statsd_client.expects(:gauge).at_least(1).with(Zendesk::Archive::Archiver::RSS_METRIC, anything)
        archiver.run
      end

      it "archives depending on whether account is disabled or not" do
        ["false", "disable_pending"].each do |not_enabled|
          account.settings.write_to_archive_v2_enabled = not_enabled
          account.settings.save!
          archiver.run
          refute @ticket.ticket_archive_stub(true)
        end

        # clear the disabled refresh time interval
        Timecop.travel(Time.now + 60.seconds)
        account.settings.write_to_archive_v2_enabled = "true"
        account.settings.save!
        archiver.run
        assert @ticket.ticket_archive_stub(true)
      end

      it "always archive for an account that's passed in" do
        account.settings.write_to_archive_v2_enabled = "false"
        account.settings.save!

        archiver = Zendesk::Archive::Archiver.new(account_ids: [account.id], run_once: true)

        archiver.run
        assert @ticket.ticket_archive_stub(true)
      end
    end

    it "does not attempt to re-archive already archived tickets" do
      archiver.run
      archiver.expects(:archive_ticket).never
      archiver.run
    end

    it "chooses some active accounts with an arturo flag when none are passed in" do
      archiver.refresh_all_accounts!
      assert archiver.accounts.include?(account)
    end

    it "does not choose accounts who have the archiver turned off by a setting" do
      account.settings.write_to_archive_v2_enabled = "false"
      account.settings.save!
      assert archiver.archiver_disabled_accounts[current_shard].include?(account.id)
    end

    describe "for account marked to be disabled for archiver" do
      before do
        account.settings.write_to_archive_v2_enabled = "disable_pending"
        account.settings.save!
      end

      it "does not choose accounts and updates account setting" do
        assert archiver.archiver_disabled_accounts[current_shard].include?(account.id)
        account.settings.reload
        assert_equal 'false', account.settings.write_to_archive_v2_enabled
      end
    end

    it "prioritizes accounts" do
      Zendesk::Archive::Archiver::PRIORITY_ACCOUNTS << 'nomorerackcom'
      a = Account.where(subdomain: 'mixpanel').first
      Account.where(id: a.id).update_all(subdomain: "nomorerackcom")

      archiver.refresh_all_accounts!
      assert_equal a, archiver.accounts.first
    end

    it "refreshes accounts when asked to" do
      account.settings.write_to_archive_v2_enabled = "true"
      account.settings.save!

      archiver.refresh_all_accounts!
      assert_includes archiver.accounts, account
      refute archiver.archiver_disabled_accounts[current_shard].include?(account.id)

      account.settings.write_to_archive_v2_enabled = "false"
      account.settings.save!

      assert archiver.archiver_disabled_accounts[current_shard].include?(account.id)
    end

    it "chooses accounts that have a damnable '0' in settings.archive" do
      account.settings.archive = "0"
      account.settings.save!
      archiver.refresh_all_accounts!
      assert archiver.accounts.include?(account)
    end

    describe "with a junk-data ticket" do
      before do
        Ticket.any_instance.stubs(:archive!).raises(ArgumentError.new("invalid byte sequence in UTF-8"))
      end

      it "logs the error and move along" do
        Rails.logger.expects(:error).at_least_once
        archiver = Zendesk::Archive::Archiver.new(account_ids: [account.id], update_status: true, run_once: true)
        archiver.run
        refute @ticket.reload.archived?
      end
    end

    describe "a ticket whose archived bytesize is too large" do
      # Remove this stub with the tickets_over_max_events_bytesize metric
      before do
        Ticket.any_instance.stubs(:events).returns(stub(count: 10_000))
      end

      before do
        Ticket.any_instance.stubs(:as_archived).returns(someval: ("A" * 2.megabytes))
      end

      it "does not get archived" do
        archiver = Zendesk::Archive::Archiver.new(account_ids: [account.id], update_status: true, run_once: true)
        archiver.run
        @ticket.ticket_archive_disqualification.wont_be_nil
        @ticket.ticket_archive_disqualification.metric.must_equal 1
        @ticket.ticket_archive_disqualification.value.must_equal 2097166
        refute @ticket.reload.archived?
      end

      describe "with dry_run enabled" do
        it "does not create a ticket_archive_disqualification for the ticket" do
          archiver = Zendesk::Archive::Archiver.new(dry_run: true, account_ids: [account.id], update_status: true, run_once: true)
          archiver.run
          @ticket.ticket_archive_disqualification.must_be_nil
          refute @ticket.reload.archived?
        end
      end
    end

    describe "a ticket with over 10,000 events" do
      before do
        Ticket.any_instance.stubs(:events).returns(stub(count: 10_001))
      end

      it "does not get archived" do
        archiver = Zendesk::Archive::Archiver.new(account_ids: [account.id], update_status: true, run_once: true)
        archiver.run
        @ticket.ticket_archive_disqualification.wont_be_nil
        @ticket.ticket_archive_disqualification.metric.must_equal 2
        @ticket.ticket_archive_disqualification.value.must_equal 10_001
        refute @ticket.reload.archived?
      end
    end

    describe "dry_run option enabled" do
      let(:archiver) { Zendesk::Archive::Archiver.new(dry_run: true, run_once: true, update_status: true) }
      before do
        @old_ticket = account.tickets.build(
          requester: users(:minimum_end_user),
          assignee: users(:minimum_agent),
          subject: "Archivable Ticket",
          description: "This is a description",
          organization: organizations(:minimum_organization1),
          external_id: 5,
          group: users(:minimum_agent).groups.first
        )
        @old_ticket.updated_at = Time.now - 123.days
        @old_ticket.will_be_saved_by account.users.first
        @old_ticket.status_id = StatusType.CLOSED
        @old_ticket.save!
      end

      it "should not cause any ticket side effects" do
        Rails.logger.info("Archiver: dryrun")
        Ticket.any_instance.expects(:archive!).never

        archiver.run
      end

      it "should not disable pending accounts" do
        account.settings.write_to_archive_v2_enabled = "disable_pending"
        account.settings.save!
        AccountSetting.any_instance.expects(:save!).never
        archiver.run
      end
    end
  end

  describe "with arturo disable_ticket_archiving enabled" do
    before do
      Arturo.enable_feature! :disable_ticket_archiving
      account.settings.write_to_archive_v2_enabled = "disable_pending"
      account.settings.save!
    end

    it "should refresh disabled" do
      archiver.run
      assert archiver.archiver_disabled_accounts[current_shard].include?(account.id)
      account.settings.reload
      assert_equal 'false', account.settings.write_to_archive_v2_enabled
    end

    it "should not query tickets" do
      Ticket.expects(:where).never
      archiver.run
    end
  end

  describe '.shards_for_worker_info' do
    before do
      Zendesk::Configuration::DBConfig.stubs(:shards_to_clusters).returns(1 => '1.1',
                                                                          2 => '1.1',
                                                                          3 => '1.2',
                                                                          4 => '1.2',
                                                                          5 => '1.3',
                                                                          6 => '1.3',
                                                                          7 => '1.4',
                                                                          8 => '1.4')
    end

    describe 'when zendesk.yml and consul match' do
      before { Zendesk::Configuration::PodConfig.stubs(:local_shards).returns((1..8).to_a) }
      it "partitions around clusters" do
        assert_equal [1, 2, 7, 8], Zendesk::Archive::Archiver.shards_for_worker_info(1, 3).sort
        assert_equal [3, 4], Zendesk::Archive::Archiver.shards_for_worker_info(2, 3).sort
        assert_equal [5, 6], Zendesk::Archive::Archiver.shards_for_worker_info(3, 3).sort
      end
    end

    describe 'zendesk.yml and consul do not match' do
      it 'raises a Runtime error' do
        proc { Zendesk::Archive::Archiver.shards_for_worker_info(4, 3) }.must_raise RuntimeError, 'Consul and zendesk.yml show different shards'
      end
    end
  end

  describe "process_health_check!" do
    describe "when over RSS" do
      before { ZendeskJob::RssReader.stubs(:rss).returns(3.gigabytes) }
      it 'aborts' do
        archiver.expects(:abort)
        archiver.send :process_health_check!
      end
    end

    describe "when the RSS recovers after a GC" do
      before { ZendeskJob::RssReader.stubs(:rss).returns(3.gigabytes, 1.gigabyte) }
      it 'continues' do
        archiver.expects(:abort).never
        archiver.send :process_health_check!
      end
    end

    describe "when orphaned" do
      before { Process.stubs(:ppid).returns(1) }
      it 'aborts' do
        archiver.expects(:abort)
        archiver.send :process_health_check!
      end
    end
  end
end
