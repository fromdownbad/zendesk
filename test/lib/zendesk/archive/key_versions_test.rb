require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'KEY_VERSIONS' do
  describe '#key_version_for' do
    it 'computes the correct key_version for stores' do
      assert_equal 1, Zendesk::Archive.key_version_for([:riak_no_account_id])
      assert_equal 2, Zendesk::Archive.key_version_for([:riak])
      assert_equal 3, Zendesk::Archive.key_version_for([:riak, :riak_no_account_id])
      assert_equal 4, Zendesk::Archive.key_version_for([:dynamodb])
      assert_equal 6, Zendesk::Archive.key_version_for([:riak, :dynamodb])
    end

    it 'raises and error given invalid stores' do
      assert_raises ArgumentError do
        Zendesk::Archive.key_version_for([:riak, :wombat])
      end
    end
  end
end
