require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 6

describe Zendesk::AppMarketClient do
  fixtures :accounts, :rules

  let(:account) { accounts(:minimum) }
  let(:client) { Zendesk::AppMarketClient.new account.subdomain }
  let(:view) { account.views.first }
  let(:ticket_field) { FactoryBot.create(:field_tagger, is_active: false, account: account) }

  describe "#requirements" do
    let(:url) do
      "https://minimum.zendesk-test.com/api/v2/apps/requirements.json?" \
        "includes=installation"
    end

    let(:response) do
      {
        "requirements" => [
          {
            "requirement_id"   => view.id,
            "requirement_type" => "views",
            "installation"     => {"settings" => {"title" => "TTT"}}
          }
        ]
      }
    end

    let(:ticket_field_response) do
      {
        "requirements" => [
          {
            "requirement_id"   => ticket_field.id,
            "requirement_type" => "ticket_fields",
            "installation"     => {"settings" => {"title" => "TTT"}, "app_id" => 35111}
          }
        ]
      }
    end

    it "is raises for unsupported" do
      assert_raises RuntimeError do
        client.requirements(Array, account)
      end
    end

    it "returns app_id_requirements" do
      stub_request(:get, url).to_return(body: ticket_field_response.to_json, headers: {'Content-Type' => 'application/json'})
      client.app_id_requirements(TicketField, account).must_equal ticket_field.id => 35111
    end

    it "returns requirements" do
      stub_request(:get, url).to_return(body: response.to_json, headers: {'Content-Type' => 'application/json'})
      client.requirements(View, account).must_equal view.id => "TTT"
    end

    it "ignores bad replies" do
      response.delete 'requirements'
      stub_request(:get, url).to_return(body: response.to_json, headers: {'Content-Type' => 'application/json'})
      client.requirements(View, account).must_equal({})
    end

    it "ignores non matching requirement id" do
      response['requirements'][0]['requirement_id'] = 123
      stub_request(:get, url).to_return(body: response.to_json, headers: {'Content-Type' => 'application/json'})
      client.requirements(View, account).must_equal({})
    end

    it "ignores non matching requirement type" do
      response['requirements'][0]['requirement_type'] = 'triggers'
      stub_request(:get, url).to_return(body: response.to_json, headers: {'Content-Type' => 'application/json'})
      client.requirements(View, account).must_equal({})
    end

    it "returns {} if there is an error" do
      stub_request(:get, url).to_raise(StandardError)
      Rails.logger.expects(:warn).with('Error occurred when requesting apps requirements for https://minimum.zendesk-test.com/api/v2 -- Exception from WebMock')
      client.requirements(View, account).must_equal({})
    end

    it "does not raise an error for uninstalled apps" do
      response['requirements'][0]['installation'] = nil
      stub_request(:get, url).to_return(body: response.to_json, headers: {'Content-Type' => 'application/json'})
      client.requirements(View, account).must_equal({})
    end
  end

  describe "#installations" do
    let(:view_1) { rules(:view_unassigned_to_group) }
    let(:view_2) { rules(:view_for_minimum_agent) }

    let(:url) do
      "https://minimum.zendesk-test.com/api/v2/apps/requirements.json?" \
        "ids%5B%5D=#{view_1.id}&ids%5B%5D=#{view_2.id}&includes=installation"
    end

    let(:response) do
      {
        "requirements" => [
          {
            "requirement_id"   => view_1.id,
            "requirement_type" => "views",
            "installation"     => {"settings" => {"title" => "AAA"}}
          },
          {
            "requirement_id"   => view_2.id,
            "requirement_type" => "views",
            "installation"     => {"settings" => {"title" => "CCC"}}
          }
        ]
      }
    end

    before do
      stub_request(:get, url).to_return(
        body:    response.to_json,
        headers: {'Content-Type' => 'application/json'}
      )
    end

    it "parses the fetched requirements" do
      assert_equal(
        {
          view_1.id => {"settings" => {"title" => "AAA"}},
          view_2.id => {"settings" => {"title" => "CCC"}}
        },
        client.installations([view_1, view_2].map(&:id))
      )
    end

    it "returns an empty object if there is an error" do
      stub_request(:get, url).to_raise(StandardError)

      Rails.logger.expects(:warn).with('Error occurred when requesting apps requirements for https://minimum.zendesk-test.com/api/v2 -- Exception from WebMock')

      assert_equal({}, client.installations([view_1, view_2].map(&:id)))
    end
  end

  describe "#update_installation" do
    let(:installation_id) { 123 }
    let(:base_url) { "https://minimum.zendesk-test.com/api/v2/apps/installations/#{installation_id}.json" }

    it "makes the API call with the correct url and params" do
      body = {
        'id' => installation_id,
        'app_id' => 123,
        'product' => "support",
        'enabled' => false
      }

      stub_request(:put, "#{base_url}?enabled=false").to_return(
        status: 200,
        body: body.to_json,
        headers: {'Content-Type' => 'application/json'}
      )
      response = client.update_installation(installation_id, enabled: false)
      assert_equal body, response.body
    end

    it "raises an exception when the API call fails" do
      stub_request(:put, "#{base_url}?enabled=false").to_return(status: 400)
      assert_raises ZendeskAPI::Error::NetworkError do
        client.update_installation(installation_id, enabled: false)
      end
    end
  end
end
