require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Export::CommentsFinder do
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:account_tickets) { account.tickets.to_a }
  let(:comments) { account_tickets.map(&:comments).flatten }
  let(:nice_ids) { account_tickets.map(&:nice_id) }

  it 'finds all comments' do
    found = Zendesk::Export::CommentsFinder.new(account, nice_ids, 0, 0, 20).find_comments
    found.sort.must_equal comments.sort
  end

  it 'limits the result if limit parameter is specified' do
    max_comments = 5
    found = Zendesk::Export::CommentsFinder.new(account, nice_ids, 0, 0, max_comments).find_comments
    found.size.must_equal max_comments
  end

  it 'returns all the comments except the ones which belong to a ticket which id < id_waterline' do
    found = Zendesk::Export::CommentsFinder.new(account, nice_ids, nice_ids[1], 0, 20).find_comments
    found.sort.must_equal account_tickets[1..-1].map(&:comments).flatten.sort
  end

  it 'skips all comments up to the one after the comment with id == comment_id_waterline' do
    id_waterline = comments[1].ticket.nice_id
    comment_id_waterline = comments[1].id
    waterline_index = comments.find_index { |c| c.id == comment_id_waterline }
    found = Zendesk::Export::CommentsFinder.new(account, nice_ids, id_waterline, comment_id_waterline, 20).find_comments
    found.sort.must_equal comments[(waterline_index + 1)..-1].sort
  end

  describe 'with a ticket with many comments' do
    let(:ticket) { tickets(:minimum_2) }
    let(:ticket_id) { ticket.nice_id }
    let(:page_size) { 4 }

    before do
      1.upto(9) do |counter|
        comment = ticket.add_comment(body: 'hola, amigos')
        ticket.will_be_saved_by(ticket.requester)
        ticket.save!

        # This is to simmulate conditions observed in production
        # where the ordering of comment ids is not correlated to the ordering of created_at
        # as in: comments.order('id ASC') != comments.order('created_at ASC')
        if counter % 3 == 0
          comment.stubs(:readonly?).returns(false)
          comment.reload.update_attribute(:created_at, 1.day.ago)
        end
      end
      ticket.reload
    end

    it 'gets all the comments in chunks' do
      comment_id_waterline = 0
      id_waterline = 0
      found_comments = []

      0.upto(2) do
        finder = Zendesk::Export::CommentsFinder.new(account, [ticket_id], id_waterline, comment_id_waterline, page_size)
        found = Array.wrap(finder.find_comments)

        found_comments.concat found
        break if found.size < page_size

        # This is how Api::V2::Internal::CommentCollectionPresenter creates the watermarks
        comment_id_waterline = found.last.id
        id_waterline = found.last.ticket.nice_id
      end

      found_comments.size.must_equal(ticket.reload.comments.size)
    end
  end

  describe 'with archived ticket with comments' do
    let(:archived_ticket) { tickets(:minimum_1) }

    before do
      archived_ticket.add_comment(body: 'hai, friends')
      archived_ticket.will_be_saved_by(archived_ticket.requester)
      archived_ticket.save!
    end

    it 'gets the ticket (parent relation) from the comments of a stubbed ticket' do
      archive_and_delete archived_ticket
      found = Zendesk::Export::CommentsFinder.new(account, [archived_ticket.nice_id], 0, 0, 10).find_comments
      found.first.ticket.must_equal(archived_ticket)
    end

    describe 'with mixed archived and normal tickets' do
      let(:ticket) { tickets(:minimum_2) }

      before do
        ticket.add_comment(body: 'hola, amigos')

        archived_ticket.add_comment(body: 'hallo, freunde')
        archived_ticket.will_be_saved_by(archived_ticket.requester)
        archived_ticket.save!

        ticket.will_be_saved_by(ticket.requester)
        ticket.save!
      end

      it 'paginates correctly when the page marker is the last comment of an archived ticket' do
        nice_id_waterline = archived_ticket.nice_id
        comment_id_waterline = archived_ticket.comments.last.id

        archive_and_delete archived_ticket

        ticket_ids = [archived_ticket.nice_id, ticket.nice_id]
        finder = Zendesk::Export::CommentsFinder.new(account, ticket_ids, nice_id_waterline, comment_id_waterline, 10)
        found = finder.find_comments

        found.count.must_equal ticket.comments.count
        found.last.ticket.must_equal(ticket)
      end

      it 'paginates correctly when the page marker is an archived ticket and there are comments left to fetch' do
        id_watermark = archived_ticket.nice_id
        comment_id_waterline = archived_ticket.comments[-2].id # only one ticket left in archived ticket

        archive_and_delete archived_ticket

        ticket_ids = [archived_ticket.nice_id, ticket.nice_id]
        finder = Zendesk::Export::CommentsFinder.new(account, ticket_ids, id_watermark, comment_id_waterline, 10)
        found = finder.find_comments

        found.count.must_equal ticket.comments.count + 1, "Did not fetch the correct comments for tickets 1 and 2"
      end
    end
  end
end
