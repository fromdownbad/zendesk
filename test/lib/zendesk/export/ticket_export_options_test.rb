require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

class Options
  include Zendesk::Export::TicketExportOptions
end

describe Zendesk::Export::TicketExportOptions do
  describe "options for incremental ticket export" do
    it "returns a timezone in the linux format" do
      time_zone = "Copenhagen"
      assert_equal "Europe/Copenhagen", Options.linux_time_zone(time_zone)
    end

    describe "hour offset" do
      it "returns the hourly string for enterprise accounts" do
        account = mock
        Options.stubs(:has_hourly_report?).returns(true)
        assert_equal "hourly", Options.hour_offset_for_export(account)
      end

      it "returns the gooddata_integration daily offset for non-enterprise accounts" do
        goodddata_integration = mock(scheduled_at: "13PM")
        account = mock
        account.stubs(:gooddata_integration).returns(goodddata_integration)
        Options.stubs(:has_hourly_report?).returns(false)
        assert_equal "13", Options.hour_offset_for_export(account)
      end

      it "returns 00 if there is no goodddata_integration" do
        account = mock
        Options.stubs(:scheduled_hour_offset).returns(nil)
        Options.stubs(:has_hourly_report?).returns(false)
        assert_equal "00", Options.hour_offset_for_export(account)
      end
    end
  end
end
