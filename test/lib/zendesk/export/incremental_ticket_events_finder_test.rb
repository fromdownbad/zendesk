require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 4
describe Zendesk::Export::IncrementalTicketEventsFinder do
  fixtures :tickets, :accounts, :events
  let(:account) { accounts(:minimum) }

  around do |test|
    with_in_memory_archive_router(test)
  end

  describe 'when pulling events from the database' do
    before do
      Timecop.freeze
      Event.connection.execute('update events e set created_at = NOW() - INTERVAL 10 DAY')
      events(:create_audit_for_minimum_ticket_1).update_attributes(created_at: 3.days.ago, type: TicketMergeAudit)
      events(:create_audit_for_minimum_ticket_2).update_column(:created_at, 5.days.ago)
      events(:create_audit_for_minimum_ticket_5).update_column(:created_at, 8.days.ago)
      events(:create_audit_for_minimum_ticket_6).update_column(:created_at, 8.days.ago)
    end

    after do
      Timecop.return
    end

    describe 'with no results to pull' do
      it 'returns an empty result set and nil next_timestamp' do
        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: account,
          start_time: 2.days.ago.to_i,
          limit: 3
        )

        assert_empty finder.results
        assert_nil finder.next_timestamp
      end
    end

    describe 'with less results to return than the limit' do
      it 'returns all of the matching results' do
        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: account,
          start_time: 9.days.ago.to_i,
          limit: 7
        )

        assert_equal 4, finder.results.count
      end

      it 'sets the next_timestamp equal to the last record timestamp' do
        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: account,
          start_time: 7.days.ago.to_i,
          limit: 7
        )

        assert_equal 3.days.ago.to_i, finder.next_timestamp
      end
    end

    describe 'with more results to return than the limit' do
      describe 'with only one timestamp worth of data' do
        it 'returns all results for the timestamp (exceeding limit)' do
          finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
            account: account,
            start_time: 9.days.ago.to_i,
            limit: 1
          )

          assert_equal 2, finder.results.count
          assert finder.results.all? { |el| el[:created_at].to_i == 8.days.ago.to_i }
        end

        it 'bumps the next_timestamp by 1 since we have grabbed all events for the timestamp' do
          finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
            account: account,
            start_time: 9.days.ago.to_i,
            limit: 1
          )

          assert_equal 8.days.ago.to_i + 1, finder.next_timestamp
        end
      end

      describe 'with multiple timestamps worth of data' do
        it 'returns results up to the limit' do
          finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
            account: account,
            start_time: 9.days.ago.to_i,
            limit: 3
          )

          assert_equal 3, finder.results.count
        end

        it 'returns next_timestamp as the last processed event timestamp' do
          finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
            account: account,
            start_time: 9.days.ago.to_i,
            limit: 3
          )

          assert_equal 5.days.ago.to_i, finder.next_timestamp
        end
      end
    end

    describe 'with events from multiple accounts' do
      before do
        events(:serialization_audit).update_column(:created_at, 6.days.ago)
      end

      it 'returns only events from the specified account' do
        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: account,
          start_time: 9.days.ago.to_i,
          limit: 4
        )

        assert_equal 4, finder.results.count
        assert finder.results.all? { |el| el[:account_id] == account.id }
      end
    end

    describe 'with audit events and other event types' do
      before do
        events(:create_comment_for_minimum_ticket_1).update_column(:created_at, 4.days.ago)
        events(:create_comment_for_minimum_ticket_2).update_column(:created_at, 2.days.ago)
      end

      it 'returns only Audit and TicketMergeAudit events' do
        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: account,
          start_time: 6.days.ago.to_i,
          limit: 4
        )

        assert_equal 2, finder.results.count
      end

      it 'returns redaction events' do
        ticket = tickets(:minimum_2)
        comment = ticket.comments.first
        ticket.redact_comment!(account.owner, comment, "minimum")

        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: ticket.account,
          start_time: 6.days.ago.to_i,
          limit: 5
        )
        assert_equal 3, finder.results.count
        event_types = finder.results.each_with_object([]) do |a, types|
          events = a[:events]
          types << events.collect { |e| e[:type] }
        end.flatten
        assert event_types.include?("CommentRedactionEvent")
      end
    end

    describe 'with some events from deleted tickets' do
      before do
        tickets(:minimum_1).update_column(:status_id, StatusType.DELETED)
      end

      it 'still returns the events' do
        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: account,
          start_time: 9.days.ago.to_i,
          limit: 7
        )

        assert_equal 4, finder.results.count
        assert finder.results.all? { |el| el[:account_id] == account.id }
      end

      describe 'with exclude_deleted' do
        it 'does not return events associated with deleted tickets' do
          finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
            account: account,
            start_time: 9.days.ago.to_i,
            limit: 7,
            exclude_deleted: true
          )

          assert_equal 3, finder.results.count
        end
      end
    end
  end

  describe 'from the archive' do
    before do
      Timecop.freeze

      events(:create_audit_for_minimum_ticket_1).update_column(:type, TicketMergeAudit)

      archive_and_delete(*Ticket.all.to_a)
      TicketArchiveStub.connection.execute('update ticket_archive_stubs set generated_timestamp = NOW() - INTERVAL 10 DAY')

      TicketArchiveStub.find(tickets(:minimum_2).id).update_column(:generated_timestamp, 3.days.ago)
      TicketArchiveStub.find(tickets(:minimum_1).id).update_column(:generated_timestamp, 4.days.ago)
      TicketArchiveStub.find(tickets(:minimum_5).id).update_column(:generated_timestamp, 8.days.ago)
      TicketArchiveStub.find(tickets(:minimum_6).id).update_column(:generated_timestamp, 8.days.ago)
    end

    after do
      Timecop.return
    end

    describe 'with no results to pull' do
      it 'returns an empty result set and nil next_timestamp' do
        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: account,
          start_time: 2.days.ago.to_i,
          limit: 3
        )

        assert_empty finder.results
        assert_nil finder.next_timestamp
      end
    end

    describe 'with less results to return than the limit' do
      it 'returns all of the matching results' do
        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: account,
          start_time: 6.days.ago.to_i,
          limit: 7
        )

        assert_equal 3, finder.results.count
      end

      it 'sets the next_timestamp equal to the last record timestamp' do
        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: account,
          start_time: 6.days.ago.to_i,
          limit: 7
        )

        assert_equal 3.days.ago.to_i, finder.next_timestamp
      end
    end

    describe 'with more results to return than the limit' do
      describe 'with only one timestamp worth of data' do
        it 'returns all results for the generated_timestamp (exceeding limit)' do
          finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
            account: account,
            start_time: 9.days.ago.to_i,
            limit: 1
          )

          assert_equal 2, finder.results.count
          assert finder.results.all? { |el| el[:ticket][:generated_timestamp].to_i == 8.days.ago.to_i }
        end

        it 'bumps the next_timestamp by 1 since we have grabbed all events for the timestamp' do
          finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
            account: account,
            start_time: 9.days.ago.to_i,
            limit: 1
          )

          assert_equal 8.days.ago.to_i + 1, finder.next_timestamp
        end
      end

      describe 'with multiple timestamps worth of data' do
        it 'returns results up to the limit' do
          finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
            account: account,
            start_time: 7.days.ago.to_i,
            limit: 3
          )

          assert_equal 3, finder.results.count
        end

        describe 'if the last record is a ticket stub' do
          it 'returns results up to the limit, including all events from the final ticket' do
            finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
              account: account,
              start_time: 9.days.ago.to_i,
              limit: 3
            )

            assert_equal 4, finder.results.count
          end
        end

        it 'returns next_timestamp as the last processed event timestamp' do
          finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
            account: account,
            start_time: 9.days.ago.to_i,
            limit: 3
          )

          assert_equal 4.days.ago.to_i, finder.next_timestamp
        end
      end
    end

    describe 'with some events from deleted tickets' do
      before do
        # Note: the following line is necessary b/c of the code assumption that archived tickets will be closed
        TicketArchiveStub.connection.execute('update ticket_archive_stubs set status_id = 4')
        TicketArchiveStub.find(tickets(:minimum_1).id).update_column(:status_id, StatusType.DELETED)
      end

      it 'still returns the events' do
        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: account,
          start_time: 9.days.ago.to_i,
          limit: 7
        )

        assert_equal 5, finder.results.count
        assert finder.results.all? { |el| el[:account_id] == account.id }
      end

      describe 'with exclude_deleted' do
        it 'does not return events associated with deleted tickets' do
          finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
            account: account,
            start_time: 9.days.ago.to_i,
            limit: 7,
            exclude_deleted: true
          )

          assert_equal 3, finder.results.count
        end
      end
    end

    describe 'with archive_tickets_per_pass limit' do
      it 'performs as usual' do
        finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
          account: account,
          start_time: 9.days.ago.to_i,
          limit: 7,
          archived_tickets_per_pass: 1
        )

        assert_equal 5, finder.results.count
      end
    end
  end

  describe 'when pulling events from both database and archive' do
    before do
      Timecop.freeze

      events(:create_audit_for_minimum_ticket_1).update_column(:type, TicketMergeAudit)

      archive_and_delete(
        tickets(:minimum_1),
        tickets(:minimum_6)
      )

      TicketArchiveStub.connection.execute('update ticket_archive_stubs set generated_timestamp = NOW() - INTERVAL 10 DAY')
      TicketArchiveStub.find(tickets(:minimum_1).id).update_column(:generated_timestamp, 4.days.ago)
      events(:create_audit_for_minimum_ticket_2).update_column(:created_at, 5.days.ago)
      TicketArchiveStub.find(tickets(:minimum_6).id).update_column(:generated_timestamp, 8.days.ago)
      events(:create_audit_for_minimum_ticket_5).update_column(:created_at, 9.days.ago)
    end

    after do
      Timecop.return
    end

    it 'orders results based on ticket_archive_stubs generated_timestamp and events created_at' do
      finder = Zendesk::Export::IncrementalTicketEventsFinder.new(
        account: account,
        start_time: 9.days.ago.to_i,
        limit: 4
      )

      expected_ticket_order = [
        tickets(:minimum_5).id,
        tickets(:minimum_6).id,
        tickets(:minimum_2).id,
        tickets(:minimum_1).id,
        tickets(:minimum_1).id
      ]

      assert_equal 5, finder.results.count
      assert_equal expected_ticket_order, finder.results.map { |el| el[:ticket_id] }
    end
  end
end
