require_relative "../../../../support/test_helper"
require 'zip/zip'
require 'zip/zipfilesystem'

SingleCov.covered!

describe Zendesk::Export::Xml::XmlZipExporter do
  fixtures :accounts, :tickets, :linkings, :entries, :attachments, :posts

  before do
    Timecop.freeze(Time.at(Time.now.to_i))
    @account = accounts(:minimum)
    @today = Time.now.strftime('%Y%m%d')

    @archive_v2_ticket = tickets(:minimum_5)
    archive_and_delete(@archive_v2_ticket)

    @export_tmp_file = Zendesk::Export::Xml::XmlZipExporter.build_for_account(@account)
  end

  describe "zip file" do
    it "outputs temporary export zip file" do
      assert File.exist?(@export_tmp_file.path)
    end

    it "has README in zip file" do
      readme = "#{@account.subdomain}-#{@today}/README"
      Zip::File.open(@export_tmp_file.path) do |zip_file|
        assert zip_file.file.exist?(readme)
      end
    end

    it "has categories xml file" do
      category = "#{@account.subdomain}-#{@today}/categories.xml"
      Zip::File.open(@export_tmp_file.path) do |zip_file|
        assert zip_file.file.exist?(category)
      end
    end

    describe "entries" do
      before do
        @entry = entries("sticky")
      end

      it "includes attachments" do
        Zip::File.open(@export_tmp_file.path) do |zip_file|
          xml_data = zip_file.file.open("#{@account.subdomain}-#{@today}/entries.xml").read
          entries  = Hash.from_xml(xml_data)
          entry    = entries["entries"]["entry"].detect { |e| e["id"] == @entry.id }
          assert_equal 1, entry["attachments"].size
        end
      end
    end

    describe "posts" do
      it "includes attachments" do
        Zip::File.open(@export_tmp_file.path) do |zip_file|
          xml_data = zip_file.file.open("#{@account.subdomain}-#{@today}/posts.xml").read
          posts    = Hash.from_xml(xml_data)
          post     = posts["posts"]["post"].first
          assert_equal 1, post["attachments"].size
        end
      end
    end

    describe "tickets" do
      before do
        @active_ticket = tickets(:minimum_1)

        ticket_export = "#{@account.subdomain}-#{@today}/tickets.xml"

        Zip::File.open(@export_tmp_file.path) do |zip_file|
          xml_data = zip_file.file.open(ticket_export).read
          assert xml_data
          @tickets = Hash.from_xml(xml_data)
        end
      end

      it "is in the export" do
        assert @tickets['tickets']
      end

      it "includes archive v2" do
        assert @tickets['tickets']['ticket'].any? { |ticket| ticket['nice_id'] == @archive_v2_ticket.nice_id }
      end

      it "includes the live ticket and that should have a linking" do
        ticket = @tickets['tickets']['ticket'].find { |t| t['nice_id'] == @active_ticket.nice_id }
        assert ticket
        assert ticket['linkings']
        assert ticket['linkings'].any? { |linking| linking['external_link_id'].to_s   == '1' }
        assert ticket['linkings'].any? { |linking| linking['external_link_type'].to_s == 'JiraIssue' }
      end
    end
  end
end
