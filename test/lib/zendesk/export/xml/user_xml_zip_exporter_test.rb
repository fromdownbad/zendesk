require_relative "../../../../support/test_helper"
require 'zip/zip'
require 'zip/zipfilesystem'

SingleCov.covered!

describe Zendesk::Export::Xml::UserXmlZipExporter do
  fixtures :accounts, :tickets

  before do
    @account = accounts(:with_groups)
    @today = Time.now.strftime('%Y%m%d')
    @export_tmp_file = Zendesk::Export::Xml::UserXmlZipExporter.build_for_account(@account)
  end

  describe "zip file" do
    it "outputs temporary export zip file" do
      assert File.exist?(@export_tmp_file.path)
    end

    it "has README in zip file" do
      readme = "#{@account.subdomain}-#{@today}/README"
      Zip::File.open(@export_tmp_file.path) do |zip_file|
        assert zip_file.file.exist?(readme)
      end
    end

    describe "users" do
      before do
        ticket_export = "#{@account.subdomain}-#{@today}/tickets.xml"
        user_export   = "#{@account.subdomain}-#{@today}/users.xml"

        Zip::File.open(@export_tmp_file.path) do |zip_file|
          xml_data = zip_file.find_entry(ticket_export)
          refute xml_data
          xml_data = zip_file.file.open(user_export).read
          assert xml_data
          @users = Hash.from_xml(xml_data)
        end
      end

      it "is in the export" do
        assert @users['users']
      end
    end
  end
end
