require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Zendesk::Export::IncrementalExportFinder do
  describe "isolated unit tests" do
    let(:relation_mock) do
      stub.tap do |o|
        o.stubs(:order).returns(o)
        o.stubs(:where).returns(o)
        o.stubs(:limit).returns(o)
        o.stubs(:includes).returns(o)
      end
    end

    before do
      @export_association = mock("query scope")

      @finder = Zendesk::Export::IncrementalExportFinder.new(
        includes: :includes,
        update_ts_field: 'updated_at',
        limit: 3,
        start_time: 0,
        export_association: @export_association
      )

      model_updated_zero = stub(attributes: {'updated_at' => 0})
      model_updated_one  = stub(attributes: {'updated_at' => 1})

      @empty_result = []
      @result_smaller_than_limit = [model_updated_zero, model_updated_zero]
      @result_size_of_limit_within_second = [model_updated_one, model_updated_one, model_updated_one]
      @result_size_of_limit_across_seconds = [model_updated_zero, model_updated_one, model_updated_one]
    end

    it "returns a list of updated records less than limit" do
      @export_association.expects(:where).once.returns(relation_mock)
      relation_mock.expects(:to_a).returns(@result_smaller_than_limit)

      assert_equal @result_smaller_than_limit, @finder.results
      assert_equal 0, @finder.next_timestamp
    end

    it "returns regular results when the updated records are across seconds" do
      @export_association.expects(:where).once.returns(relation_mock)
      relation_mock.expects(:to_a).returns(@result_size_of_limit_across_seconds)

      assert_equal @result_size_of_limit_across_seconds, @finder.results
      assert_equal 1, @finder.next_timestamp
    end

    it "knows to query a second time if all the results are in the same second and at limit" do
      stub1 = stub
      stub2 = stub

      second_result = @result_size_of_limit_within_second + [stub(attributes: {'updated_at' => 1})]

      stub1.expects(:order).returns(stub1)
      stub1.expects(:includes).returns(stub1)
      stub1.expects(:limit).with(3).returns(stub1)
      stub1.expects(:to_a).returns(@result_size_of_limit_within_second)

      stub2.expects(:order).returns(stub2)
      stub2.expects(:includes).returns(stub2)
      stub2.expects(:limit).never
      stub2.expects(:to_a).returns(second_result)

      @export_association.expects(:where).with("updated_at >= ?", Time.at(0)).returns(stub1)
      @export_association.expects(:where).with("updated_at = ?", Time.at(1)).returns(stub2)

      assert_equal second_result, @finder.results
      assert_equal 2, @finder.next_timestamp
    end
  end

  describe "database integration tests" do
    before do
      @account = accounts(:minimum)
    end

    it "gets all updated records when limit larger than the number updated" do
      finder = Zendesk::Export::IncrementalExportFinder.new(
        includes: [],
        update_ts_field: 'updated_at',
        limit: 1000,
        start_time: 0,
        export_association: @account.users
      )

      assert_equal @account.users.count(:all), finder.results.length
      assert_equal finder.results.last.updated_at.to_i, finder.next_timestamp
    end

    it "enoughs records that the next timestamp is one greater if the limit is less than number updated" do
      User.connection.execute("update users set updated_at = '1970-01-01 00:00:03'")

      finder = Zendesk::Export::IncrementalExportFinder.new(
        includes: [],
        update_ts_field: 'updated_at',
        limit: 1,
        start_time: 0,
        export_association: @account.users
      )

      assert_equal @account.users.count(:all), finder.results.length
      assert_equal 4, finder.next_timestamp
    end
  end
end
