require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Export::IncrementalTicketMetricEventsFinder do
  fixtures :tickets

  let(:account) { accounts(:minimum) }
  let(:user)    { users(:minimum_agent) }
  let(:ticket)  { tickets(:minimum_1) }

  let(:association) do
    TicketMetric::Event.where(account_id: ticket.account.id)
  end

  let(:start_time) { ticket.created_at + 1.minute }
  let(:limit)      { 4 }

  let(:finder) do
    Zendesk::Export::IncrementalTicketMetricEventsFinder.new(
      account:     account,
      association: association,
      limit:       limit,
      start_time:  start_time
    )
  end

  let(:limited_queries) { @sql_queries.grep(/LIMIT #{limit}/) }

  before { Timecop.freeze(ticket.created_at + 20.minutes) }

  describe 'when there are no events' do
    describe '#results' do
      it 'returns no events' do
        assert_empty finder.results
      end
    end

    describe '#next_timestamp' do
      it 'returns nil' do
        assert_nil finder.next_timestamp
      end
    end
  end

  describe 'when there are events for archived tickets' do
    let(:archived_ticket) { tickets(:minimum_2) }

    before do
      ticket.metric_events.reply_time.activate.create(
        account: ticket.account,
        time:    ticket.created_at + 10.minutes
      )
      archived_ticket.metric_events.reply_time.activate.create(
        account: ticket.account,
        time:    ticket.created_at + 20.minutes
      )

      account.ticket_archive_stubs.create do |stub|
        stub.id      = archived_ticket.id
        stub.nice_id = archived_ticket.nice_id
      end

      archived_ticket.update_column(:status_id, StatusType.ARCHIVED)
    end

    it 'returns events with associated tickets' do
      assert_equal(
        [ticket, archived_ticket].map(&:nice_id),
        finder.results.map { |event| event.ticket.nice_id }
      )
    end
  end

  describe 'when there are events at different times' do
    before do
      [
        {type: :activate, after:  0.minutes},
        {type: :pause,    after:  1.minutes},
        {type: :breach,   after:  2.minutes},
        {type: :activate, after:  3.minutes},
        {type: :pause,    after:  6.minutes},
        {type: :fulfill,  after: 21.minutes},
        {type: :breach,   after: 10.minutes}
      ].each do |raw_event|
        ticket.metric_events.reply_time.send(raw_event[:type]).create(
          account: ticket.account,
          time:    ticket.created_at + raw_event[:after]
        )
      end

      Timecop.freeze(ticket.created_at + 4.minutes) do
        ticket.metric_events.breach.where(
          time: ticket.created_at + 2.minutes
        ).each(&:soft_delete)
      end

      Timecop.freeze(ticket.created_at + 21.minutes) do
        ticket.metric_events.breach.where(
          time: ticket.created_at + 10.minutes
        ).each(&:soft_delete)
      end
    end

    describe '#results' do
      before do
        @sql_queries = sql_queries do
          finder.results.to_a
        end
      end

      it 'does not include future events' do
        assert finder.results.all? { |event| event.time <= Time.now }
      end

      it 'does not include events before the start time' do
        assert finder.results.all? { |event| event.time >= start_time }
      end

      it 'does not include deleted events' do
        assert finder.results.none?(&:deleted?)
      end

      it 'sorts the events' do
        assert_equal %w[pause activate pause], finder.results.map(&:type)
      end

      it 'queries for active events with appropriate limit' do
        assert_equal 1, limited_queries.count
      end
    end

    describe '#next_timestamp' do
      it 'returns the time of the last included event' do
        assert_equal finder.results.last.time.to_i, finder.next_timestamp
      end
    end

    describe 'and there are more events than the limit' do
      before do
        ticket.metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    ticket.created_at + 10.minutes
        )
        ticket.metric_events.reply_time.pause.create(
          account: ticket.account,
          time:    ticket.created_at + 15.minutes
        )
      end

      describe '#results' do
        before do
          @sql_queries = sql_queries do
            finder.results.to_a
          end
        end

        it 'includes events up to the limit' do
          assert_equal(
            %w[pause activate pause activate],
            finder.results.map(&:type)
          )
        end

        it 'queries for active events with appropriate limit' do
          assert_equal 1, limited_queries.count
        end
      end

      describe '#next_timestamp' do
        it 'returns the time of the last included event' do
          assert_equal finder.results.last.time.to_i, finder.next_timestamp
        end
      end
    end
  end

  describe 'when there are more events at the same time than the limit' do
    before do
      5.times do
        ticket.metric_events.reply_time.breach.create(
          account: ticket.account,
          time:    ticket.created_at + 5.minutes
        )

        ticket.metric_events.reply_time.activate.create(
          account: ticket.account,
          time:    ticket.created_at + 10.minutes
        )
      end
    end

    describe '#results' do
      let(:event_time) { ticket.metric_events.first.time }
      let(:unlimited_queries) do
        @sql_queries.
          reject { |sql| sql =~ /LIMIT/ }.
          select { |sql| sql =~ /'#{event_time.to_s(:db)}'/ }
      end

      before do
        @sql_queries = sql_queries do
          finder.results.to_a
        end
      end

      it 'returns events beyond the limit' do
        assert_equal limit + 1, finder.results.count
      end

      it "orders the events by 'id'" do
        assert_equal finder.results.sort_by(&:id), finder.results
      end

      it 'queries for active events with appropriate limit' do
        assert_equal 1, limited_queries.count
      end

      it 'queries for all events at a given time with no limit' do
        assert_equal 1, unlimited_queries.count
      end
    end

    describe '#next_timestamp' do
      it 'returns the time of the last included event + 1' do
        assert_equal finder.results.last.time.to_i.succ, finder.next_timestamp
      end
    end

    describe 'and there are non-deleted events with a different event time' do
      before do
        Timecop.freeze(ticket.created_at + 10.minutes) do
          ticket.metric_events.reply_time.activate.create(
            account: ticket.account,
            time:    ticket.created_at + 11.minutes
          )
        end
      end

      describe '#results' do
        before do
          @sql_queries = sql_queries do
            finder.results.to_a
          end
        end

        it 'does not return those events' do
          assert_equal limit + 1, finder.results.count
        end

        it 'queries for active events with appropriate limit' do
          assert_equal 1, limited_queries.count
        end
      end
    end
  end
end
