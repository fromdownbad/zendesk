require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Export::TicketFieldEntriesRawDataExporter do
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:tickets) { account.tickets.all_with_archived }

  let(:fields_by_ticket_id) do
    tickets.each_with_object({}) do |ticket, cache|
      cache[ticket.id] = ticket.ticket_field_entries.map(&:ticket_field_id)
    end
  end

  let(:exporter) { Zendesk::Export::TicketFieldEntriesRawDataExporter.new(account, tickets) }
  let(:raw_entries) { exporter.raw_ticket_field_entries }

  before do
    archive_and_delete(
      account.tickets.all.find { |ticket| !ticket.ticket_field_entries.empty? }
    )
  end

  describe_with_and_without_arturo_enabled :build_tfe_cache_for_raw_exports do
    it 'gets the raw ticket field entry data for a set of tickets' do
      fields_by_ticket_id.each do |ticket_id, ticket_field_ids|
        assert_equal Set.new(ticket_field_ids), Set.new(raw_entries[ticket_id].keys)
      end
    end
  end

  describe_with_arturo_enabled :build_tfe_cache_for_raw_exports do
    # In this test, we wrap `ticket.ticket_field_entries_by_ticket_field_id_cache` to handle a
    # subtle but important implementation detail. The `raw_entries` has produced by calling
    # the exporter will default nil values to an empty hash so we have something to look into
    # for every ticket. On the other hand, the custom vield value path in Zendesk::Fom::CustomField#value
    # will treat nil differently than an empty hash. Nil means we didn't build a cache, but an
    # empty hash means we built the cache and there was no data so don't look it up again. For the
    # purpose of this test, converting nil to a hash for comparison covers the intent of the assertion.
    it 'copies the ticket field entry cache to each ticket instance' do
      tickets.each do |ticket|
        assert_equal(
          raw_entries[ticket.id],
          Hash(ticket.ticket_field_entries_by_ticket_field_id_cache)
        )
      end
    end
  end

  describe_with_arturo_disabled :build_tfe_cache_for_raw_exports do
    it 'does not copy the ticket field entry cache to the ticket model' do
      tickets.each do |ticket|
        assert_nil ticket.ticket_field_entries_by_ticket_field_id_cache
      end
    end
  end
end
