require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Zendesk::Export::ConfigurationNotifier do
  fixtures :accounts

  let(:account)              { accounts(:minimum) }
  let(:notifier)             { Zendesk::Export::ConfigurationNotifier.new(account) }
  let(:mock_api_tickets)     { stub }
  let(:mock_api_ticket)      { stub }
  let(:mock_internal_client) { stub(tickets: mock_api_tickets) }

  before do
    Zendesk::InternalApi::Client.expects(:new).with("support").returns(mock_internal_client)
  end

  describe "#create_enable_exports_request" do
    it "posts the correct attributes" do
      mock_internal_client.stubs(current_user: stub(id: 9))
      mock_internal_client.stubs(groups: [stub(name: "Support (PS)", id: 10)])
      mock_internal_client.stubs(ticket_fields: [stub(title: "Subdomain", id: 11)])
      mock_api_tickets.expects(:create).with(
        subject: "Please enable access to data exports",
        type: TicketType.TASK.to_s,
        requester: users(:minimum_admin).email,
        description: "Hello!\n\nI want to allow Administrators in this account to export CSV files containing all ticket data. Please enable this feature for me.\n\nThis ticket was generated on behalf of the owner of the account with the subdomain: 'minimum'.",
        submitter_id: 9,
        group_id: 10,
        fields: {11 => "minimum"}
      ).returns(mock_api_ticket)

      notifier.create_enable_exports_request
    end

    it "returns true if exports are currently disabled" do
      notifier.stubs(attributes_for_enable_ticket: {})
      mock_api_tickets.expects(:create).with({}).returns(mock_api_ticket)

      notifier.create_enable_exports_request
    end

    it "returns false if ticket creation fails" do
      notifier.stubs(attributes_for_enable_ticket: {})
      mock_api_tickets.expects(:create).with({}).
        raises(Faraday::Error::ClientError.new("Server too busy"))

      refute notifier.create_enable_exports_request
    end
  end

  describe "#create_exceeded_export_max_notice" do
    it "posts the correct attributes" do
      mock_internal_client.stubs(current_user: stub(id: 9))
      mock_internal_client.stubs(groups: [stub(name: "Services", id: 12)])
      mock_internal_client.stubs(ticket_fields: [stub(title: "Subdomain", id: 11)])
      mock_api_tickets.expects(:create).with(
        subject: "Maximum number of items for data export exceeded",
        type: TicketType.TASK.to_s,
        requester: users(:minimum_admin).email,
        description: "Hello!\n\nI would like to gain access to my forum, ticket, and user data. Please contact me about exporting this data.\n\nThis ticket was generated on behalf of the owner of the account with the subdomain: 'minimum'.",
        submitter_id: 9,
        group_id: 12,
        fields: {11 => "minimum"}
      ).returns(mock_api_ticket)

      notifier.create_exceeded_export_max_notice
    end

    it "returns true if ticket is created successfully" do
      notifier.stubs(attributes_for_exceeded_max_ticket: {})
      mock_api_tickets.expects(:create).with({}).returns(mock_api_ticket)

      assert notifier.create_exceeded_export_max_notice
    end

    it "returns false if the ticket creation fails" do
      notifier.stubs(attributes_for_exceeded_max_ticket: {})
      mock_api_tickets.expects(:create).with({}).
        raises(Faraday::Error::ClientError.new("Server too busy"))

      refute notifier.create_exceeded_export_max_notice
    end
  end
end
