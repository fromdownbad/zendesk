require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::Export::IncrementalTicketExport do
  fixtures :accounts, :tickets, :ticket_fields, :ticket_field_entries, :custom_field_options, :account_property_sets, :subscriptions, :users, :user_identities

  describe 'without archiver' do
    before do
      @account = accounts(:minimum)
      @archived_ticket = tickets(:minimum_2)
      @archived_ticket.will_be_saved_by(@archived_ticket.requester)

      @archived_ticket.update_attribute(:via_id, 17)

      @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000, show_metrics: true)
      @data     = @exporter.tickets
      @headers  = @exporter.field_map

      @first = @data.first
    end

    it "gives back an array of hashes" do
      assert @data.is_a?(Array)
      assert @first.is_a?(Hash)
    end

    it "Names the columns something idempotent" do
      assert_includes @first.keys, "nice_id"
      assert_includes @first.keys, "created_at"
      assert_includes @first.keys, "status"
    end

    it "provides a field map that matches column names" do
      assert_equal @headers.keys.sort, @first.keys.sort
    end

    it "provides a map for internal names to real names" do
      assert_equal 'Id', @headers['nice_id']
      assert_equal 'Created at', @headers['created_at']
      assert_equal 'Status', @headers['status']
    end

    it "outputs nice_id as an integer" do
      assert_equal @data.first['nice_id'], @data.first['nice_id'].to_i
    end

    it "presents non-hostmapped urls" do
      ticket = @data.detect { |m| m["nice_id"] == @archived_ticket.nice_id }
      assert_equal "https://#{@account.subdomain}.#{Zendesk::Configuration.fetch(:host)}/tickets/#{@archived_ticket.nice_id}", ticket["url"]
    end

    describe "V2 style URLs" do
      before do
        @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000, show_metrics: true, v2_url: true)
        @data     = @exporter.tickets
      end

      it "presents V2 style urls" do
        ticket = @data.detect { |m| m["nice_id"] == @archived_ticket.nice_id }
        assert_equal "https://#{@account.subdomain}.#{Zendesk::Configuration.fetch(:host)}/api/v2/tickets/#{@archived_ticket.nice_id}.json", ticket["url"]
      end
    end

    it "Contains all custom ticket fields" do
      @exporter.send(:custom).each do |c|
        assert @first.keys.detect { |k| k == "field_#{c.id}" }, "keys do not contain field #{c.id}"

        @account.tickets.map(&:ticket_field_entries).flatten.each do |field|
          row = @data.find { |r| r['nice_id'] == field.ticket.nice_id }

          assert row, "could not find id #{field.ticket.nice_id} in #{@data.inspect}"
          value = row["field_" + field.ticket_field_id.to_s]

          field_value = field.ticket_field.type == "FieldTagger" ? field.ticket_field.humanize_value(field.value) : field.value
          assert_equal field_value, value
        end
      end
    end

    it "exports checkboxes as Yes/No" do
      ticket = tickets(:minimum_1)

      field = @account.ticket_fields.where(type: "FieldCheckbox").first
      ticket.fields = {field.id => "1"}
      ticket.will_be_saved_by(@account.owner)
      ticket.save!

      assert @exporter.tickets.any? { |t| t["field_#{field.id}"] == "Yes" }
    end

    it "exports field taggers with their values" do
      ticket = tickets(:minimum_1)
      nice_id = ticket.nice_id

      field = @account.ticket_fields.where(type: "FieldTagger").first
      ticket.fields = {field.id => "not_bad"}
      ticket.will_be_saved_by(@account.owner)
      ticket.save!
      ticket = @exporter.tickets.select { |t| t["nice_id"] == nice_id }

      assert_equal false, ticket.empty?
      assert_equal "Not bad", ticket.first["field_#{field.id}"]
    end

    it "exports multiselect fields with their values" do
      ticket = tickets(:minimum_1)
      nice_id = ticket.nice_id

      field = FieldMultiselect.create!(
        account: @account,
        title: "Multiselect",
        custom_field_options: [
          CustomFieldOption.new(name: "Option1", value: "option1"),
          CustomFieldOption.new(name: "Option2", value: "option2")
        ]
      )
      Timecop.travel(Time.now + 1.seconds)
      ticket.fields = {field.id => ['option1', 'option2']}
      ticket.will_be_saved_by(@account.owner)
      ticket.save!

      # Create exporter again because account ticket fields are cached
      @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000, show_metrics: true)
      exported_ticket = @exporter.tickets.select { |t| t["nice_id"] == nice_id }

      assert_equal "Option1, Option2", exported_ticket.first["field_#{field.id}"]
    end

    it "exports archived ticket fields" do
      tickets = @exporter.tickets
      archived_ticket = tickets.find { |t| t["nice_id"] == @archived_ticket.nice_id }
      field = ticket_fields(:field_text_custom)
      assert_not_nil archived_ticket["field_#{field.id}"]
    end

    it "Dumps all the tickets" do
      assert @data.find { |t| t['nice_id'].to_i == @account.tickets.first.nice_id }
      assert @data.find { |t| t['nice_id'].to_i == @archived_ticket.nice_id }
    end

    it "sorts by generated_timestamp" do
      Ticket.connection.execute("update tickets set generated_timestamp = from_unixtime((rand() * 1231209811) + 400000) where account_id = #{@account.id}")

      exported_ids = @exporter.tickets.map { |t| t['nice_id'].to_i }
      raw_ids = Ticket.with_deleted do
        @account.tickets.where('status_id != ?', StatusType.ARCHIVED).
          order(:generated_timestamp).pluck(:nice_id)
      end

      assert_equal raw_ids, exported_ids
    end

    it "honors the limit" do
      @exporter.limit = 2
      assert_equal 2, @exporter.tickets.size
    end

    it "treats CSR good/good-with-comment and bad/bad-with-comment the same" do
      counter = 0
      @account.tickets.each do |ticket|
        next if ticket.status_id >= StatusType.CLOSED

        case counter
        when 0
          ticket.satisfaction_score = SatisfactionType.GOOD
        when 1
          ticket.satisfaction_score = SatisfactionType.GOOD
          ticket.satisfaction_comment = "yeah"
        when 2
          ticket.satisfaction_score = SatisfactionType.BAD
        when 3
          ticket.satisfaction_score = SatisfactionType.BAD
          ticket.satisfaction_comment = "no"
        end
        ticket.will_be_saved_by(ticket.requester)
        ticket.save!
        counter += 1
      end

      # need a new exporter to get satisfaction stats in there.
      @account.stubs(:extended_ticket_metrics_visible?).returns(true)
      exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000, show_metrics: true)
      exporter.show_metrics = true
      good, bad = exporter.tickets.each_with_object([0, 0]) do |t, accum|
        accum[0] += 1 if t['satisfaction_score'] == "Good"
        accum[1] += 1 if t['satisfaction_score'] == "Good"
      end
      assert_equal 2, good
      assert_equal 2, bad
    end

    describe "exporting options" do
      describe "for a non-Enterprise subscription" do
        it "exports options with timezone and time" do
          @account.stubs(:gooddata_integration).returns(mock(scheduled_at: "15PM"))
          @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000, show_metrics: true)
          options = {timezone: "Europe/Copenhagen", hour_offset: "15"}
          assert_not_nil @exporter.options
          assert_equal options, @exporter.options
        end
      end

      describe "for a Enterprise subscription" do
        it "exports options with timezone and hourly" do
          Zendesk::Export::IncrementalTicketExport.stubs(:has_hourly_report?).with(@account).returns(true)
          @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000, show_metrics: true)
          options = {timezone: "Europe/Copenhagen", hour_offset: "hourly"}
          assert_not_nil @exporter.options
          assert_equal options, @exporter.options
        end
      end
    end

    describe "in a multibrand account" do
      before do
        @account.stubs(:has_multiple_brands?).returns(true)
        @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000, show_metrics: true)
        @headers  = @exporter.field_map
        @data     = @exporter.tickets
      end

      it "adds the Brand header" do
        assert_equal "Brand", @headers['brand_name']
      end

      it "adds the column with the correct brand name" do
        assert @data.all? { |d| d.key?("brand_name") }
        ticket = @data.first
        assert ticket["brand_name"], @account.tickets.find_by_nice_id(ticket["nice_id"].to_i).brand.name
      end
    end

    describe "when configured to map nice_id's" do
      before do
        @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000, show_metrics: true, map_nice_id: true)
        @headers  = @exporter.field_map
        @data     = @exporter.tickets
      end

      it "names the columns something idempotent" do
        assert_includes @data.first.keys, "id"
        assert_includes @data.first.keys, "created_at"
        assert_includes @data.first.keys, "status"
      end

      it "properly return url" do
        assert_match %r{tickets/\d+$}, @data.first["url"]
      end

      it "does not contain nice_id refs" do
        refute @data.first.keys.member?("nice_id")
      end

      it "casts ids to Integer" do
        assert_equal @data.first["id"], @data.first["id"].to_i
      end

      it "uses id for the ticket id header" do
        assert @headers.detect { |h| h[0] == "id" }
      end
    end

    describe "when account has redact fields configured" do
      before do
        @account.stubs(:extended_ticket_metrics_visible?).returns(:true)
        @account.stubs(:has_redact_fields_for_incremental_export?).returns(:true)
        @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000, show_metrics: true)
        @headers  = @exporter.field_map
        @data     = @exporter.tickets
      end

      it "redacts fields" do
        emails = %w[a78b3151e949ed0feeaa1b613166bae2 091c80f0f972a7344f1efdc6f5a7ca08]
        assert_same_elements emails, @data.map { |m| m['req_email'] }.uniq
      end
    end

    describe "non exportable fields" do
      before do
        @field = @account.ticket_fields.where(type: "FieldCheckbox").first
        @field.is_exportable = false
        @field.save!
        @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000, show_metrics: true)
        @headers  = @exporter.field_map
        @data     = @exporter.tickets
      end

      it "does not be exported" do
        assert_equal [false], @data.map { |m| m.key?("field_#{@field.id}") }.uniq
      end
    end
  end

  describe "additional_columns" do
    before do
      @exporter = Zendesk::Export::IncrementalTicketExport.for_account(accounts(:minimum), Time.at(0), 1000,
        show_metrics: true, additional_columns: [["Test Header", :group_id]])

      @data     = @exporter.tickets
      @headers  = @exporter.field_map
    end

    it "adds the header" do
      assert_equal "Test Header", @headers['group_id']
    end

    it "adds the column" do
      assert @data.all? { |d| d.key?("group_id") }
    end
  end

  describe "submitter_name presentation" do
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }

    before do
      # needed to not have to stub out s3 requests during User#destroy
      Attachment.any_instance.stubs(:destroy).returns(true)
      ticket.submitter.destroy
    end

    describe "when a submitter on a ticket is deleted" do
      before do
        @exporter = Zendesk::Export::IncrementalTicketExport.for_account(account, Time.at(0), 1000)
        @data = @exporter.tickets
      end

      it "uses '(deleted)' for submitter_name " do
        t = @data.detect { |m| m["nice_id"] == ticket.nice_id }
        assert_equal "(deleted)", t['submitter_name']
      end
    end

    describe "when a submitter on a ticket is deleted" do
      before do
        @exporter = Zendesk::Export::IncrementalTicketExport.for_account(account, Time.at(0), 1000)
        @data = @exporter.tickets
      end

      it "uses '(deleted)' for submitter_name " do
        t = @data.detect { |m| m["nice_id"] == ticket.nice_id }
        assert_equal "(deleted)", t['submitter_name']
      end
    end
  end

  describe "with only one ticket form" do
    fixtures :ticket_forms

    before do
      @account = accounts(:minimum)
      Zendesk::TicketForms::Initializer.new(@account, ticket_form: {
        name: "Form 1",
        position: 1
      }).ticket_form.save!
      @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000)
      @headers  = @exporter.field_map
      @data     = @exporter.tickets
      assert_equal 1, @account.ticket_forms.count
    end

    it "does not add the Ticket Form header" do
      refute_includes @headers.keys, 'ticket_form_name'
    end

    it "does not add the column with a ticket form name" do
      ticket = @account.tickets.first
      ticket.will_be_saved_by(@account.owner)
      ticket.ticket_form = @account.ticket_forms.first
      ticket.save!
      refute @data.any? { |d| d.key?("ticket_form_name") }
      ticket = @data.find { |t| t["nice_id"] == ticket.nice_id }
      refute_includes ticket.keys, "ticket_form_name"
    end
  end

  describe "with multiple ticket forms" do
    fixtures :ticket_forms

    before do
      @account = accounts(:minimum)
      2.times do |i|
        Zendesk::TicketForms::Initializer.new(@account, ticket_form: {
          name: "Form #{i}",
          position: i + 1
        }).ticket_form.save!
      end
      @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000)
      @headers  = @exporter.field_map
      @data     = @exporter.tickets
      assert_equal 2, @account.ticket_forms.count
    end

    it "adds the Ticket Form header" do
      assert_equal "Ticket Form", @headers['ticket_form_name']
    end

    it "adds the column with the correct ticket form name" do
      ticket = @account.tickets.first
      ticket.will_be_saved_by(@account.owner)
      ticket.ticket_form = @account.ticket_forms.first
      ticket.save!
      assert @data.all? { |d| d.key?("ticket_form_name") }
      ticket = @data.find { |t| t["nice_id"] == ticket.nice_id }
      assert ticket["ticket_form_name"], @account.tickets.find_by_nice_id(ticket["nice_id"].to_i).ticket_form.name
    end
  end

  describe "archived tickets" do
    before do
      @account = accounts(:minimum)
      @account.stubs(:extended_ticket_metrics_visible?).returns(true)
      @exporter = Zendesk::Export::IncrementalTicketExport.for_account(@account, Time.at(0), 1000, show_metrics: true, additional_columns: [["Test Header", :assignee_id]])
      @data     = @exporter.tickets
      @headers  = @exporter.field_map
      @archived_ticket = tickets(:minimum_5)
      archive_and_delete(@archived_ticket)
    end

    it "dumps archived and not archived tickets" do
      assert @data.find { |t| t['nice_id'].to_i == @archived_ticket.nice_id }
      assert @data.find { |t| t['nice_id'].to_i == @account.tickets.first.nice_id }
    end

    it "exports all custom ticket fields" do
      archived_ticket = @exporter.tickets.find { |t| t["nice_id"] == @archived_ticket.nice_id }
      @exporter.send(:custom).each do |c|
        assert archived_ticket.keys.detect { |k| k == "field_#{c.id}" }, "keys do not contain field #{c.id}"
      end
    end

    it "exports the correct columns" do
      @data.each do |t|
        @headers.keys.each do |key|
          assert t.key?(key), "#{key} is not present"
        end
      end
    end

    it "exports submitter names" do
      assert_nil @data.detect { |ticket| ticket['req_name'].nil? }, "submitter name must be present"
    end

    it "has a correctly mapped name for the req_id" do
      @data.each do |ticket|
        requester_name  = ticket['req_name']
        requester_id    = ticket['req_id']
        submitter_id    = ticket['submitter_id']
        submitter_name  = ticket['submitter_name']
        assert_equal @account.users.find(requester_id).name, requester_name unless requester_id.nil?
        assert_equal @account.users.find(submitter_id).name, submitter_name unless submitter_id.nil?
      end
    end

    it "adds the additional columns header" do
      assert_equal "Test Header", @headers['assignee_id']
    end

    it "exports checkboxes as Yes/No" do
      ticket = tickets(:minimum_1)
      field = @account.ticket_fields.where(type: "FieldCheckbox").first
      ticket.fields = {field.id => "1"}
      ticket.will_be_saved_by(@account.owner)
      ticket.save!
      archive_and_delete(ticket)

      assert @exporter.tickets.any? { |t| t["field_#{field.id}"] == "Yes" }
    end

    it "exports satisfaction score" do
      ticket = tickets(:minimum_1)
      ticket.satisfaction_score = SatisfactionType.Good
      ticket.will_be_saved_by(@account.owner)
      ticket.save!
      archive_and_delete(ticket)
      assert @exporter.tickets.any? { |t| t["satisfaction_score"] == "Good" }
    end

    describe "export date columns" do
      before do
        @archived = tickets(:minimum_1)
        columns = Zendesk::Export::TicketExport::DATE_COLUMNS
        attributes = Hash[*columns.zip([Date.tomorrow] * 5).flatten] # abracadadra
        @archived.assign_attributes(attributes)
        @archived.will_be_saved_by(@account.owner)
        @archived.save!
        @archived.reload
        archive_and_delete(@archived)
      end

      Zendesk::Export::TicketExport::DATE_COLUMNS.each do |column|
        it "exports archived ticket.#{column} as unix timestamp" do
          exported = @exporter.tickets.detect { |t| t['nice_id'] == @archived.nice_id }
          actual = exported[column.to_s]
          expected = @archived.send(column)
          assert_equal_with_nil expected, actual
        end

        it "exports non-archived ticket.#{column} as unix timestamp" do
          exported = @exporter.tickets.detect { |t| t['nice_id'] == tickets(:minimum_3).nice_id }
          actual = exported[column.to_s]
          expected = tickets(:minimum_3).send(column)
          assert_equal_with_nil expected, actual
        end
      end
    end

    it "exports null dates as nil" do
      exported = @exporter.tickets.detect { |t| t['nice_id'] == @archived_ticket.nice_id }
      assert_nil exported['initially_assigned_at']
    end

    it "exports non-archived ticket null dates as nil" do
      exported = @exporter.tickets.detect { |t| t['nice_id'] == tickets(:minimum_3).nice_id }
      assert_nil exported['initially_assigned_at']
    end

    it "exports non-archived generated_timestamp as unix timestamps" do
      date = @exporter.tickets.first["generated_timestamp"]
      assert_in_delta tickets(:minimum_1).generated_timestamp.to_i, date, 2
    end

    it "exports archived generated_timestamp as unix timestamps" do
      date = @exporter.tickets.last["generated_timestamp"]
      assert_in_delta @archived_ticket.generated_timestamp.to_i, date, 2
    end

    it "exports satisfaction score and present not offered" do
      ticket = tickets(:minimum_1)
      ticket.satisfaction_score = SatisfactionType.Unoffered
      ticket.will_be_saved_by(@account.owner)
      ticket.save!
      archive_and_delete(ticket)
      assert @exporter.tickets.any? { |t| t["satisfaction_score"] == "Not Offered" }
    end

    describe "with archived custom fields" do
      before do
        archive_and_delete(tickets(:minimum_6))
      end

      it "does not generate N+1s" do
        assert_sql_queries 2, %r{SELECT `ticket_fields`.* FROM `ticket_fields`} do
          @exporter.tickets
        end
      end
    end
  end
end
