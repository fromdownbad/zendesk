require_relative '../../../support/test_helper'

SingleCov.covered!

module Zendesk::Export
  describe BillingReport do
    fixtures :accounts, :subscriptions, :payments

    let(:account) { accounts(:minimum) }

    describe '.nfs_filename' do
      it 'returns a proper filename' do
        expected = /\/billing_report.zip$/
        assert_match expected, BillingReport.nfs_filename
      end
    end

    describe '.filename' do
      it 'returns the billing report filename' do
        expected = 'billing_report.zip'
        assert_equal expected, BillingReport.filename
      end
    end

    describe '.execute' do
      it 'generates three reports: accounts, subscriptions and payments' do
        Zendesk::S3ReportingStore.any_instance.expects(:store!)
        BillingReport.expects(:generate_report).times(3)
        assert BillingReport.execute
      end
    end

    describe '.generate_report' do
      let(:subscription) { subscriptions(:minimum) }

      let(:subscription_type) do
        {
          class: ::Subscription,
          order: 'id',
          fields: %w[id
                     account_id]
        }
      end

      it 'generates a report including the associated account' do
        csv_result = BillingReport.send(:generate_report, subscription_type)
        assert !csv_result.empty?
      end

      describe 'with deleted accounts' do
        before { Subscription.any_instance.stubs(account: nil) }

        it 'skips the associated subscription and/or payments' do
          csv_result = BillingReport.send(:generate_report, subscription_type)
          expected = /id\taccount_id\n/
          assert_match expected, csv_result
        end
      end
    end
  end
end
