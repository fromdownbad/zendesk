require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Export::Configuration do
  fixtures :accounts

  let(:export_configuration) { Zendesk::Export::Configuration.new(account) }
  let(:account)              { accounts(:minimum) }

  describe "#all_accessible?" do
    it "is true if all of the EXPORT_JOBS types are accessible" do
      assert_equal ImportExportJobPolicy::DEFAULT_JOBS, account.settings.export_accessible_types
      assert export_configuration.all_accessible?
    end

    it "is false if any of the EXPORT_JOBS types are inaccessible" do
      account.settings.stubs(export_accessible_types: ["view_csv", "user_xml_export"])
      refute export_configuration.all_accessible?
    end
  end

  describe "#any_accessible?" do
    it "is true if any of the EXPORT_JOBS types are accessible" do
      account.settings.stubs(export_accessible_types: ["view_csv", "user_xml_export"])
      assert export_configuration.any_accessible?
    end

    it "is false if all of the EXPORT_JOBS types are inaccessible" do
      account.settings.stubs(export_accessible_types: ["view_csv"])
      refute export_configuration.any_accessible?
    end
  end

  describe "#maximum_exceeded?" do
    let(:max) { account.settings.export_ticket_max }

    it "returns true when parameter is greater" do
      assert export_configuration.maximum_exceeded?(max + 1)
    end

    it "returns false when parameter is less than or equal" do
      refute export_configuration.maximum_exceeded?(max)
      refute export_configuration.maximum_exceeded?(max - 1)
    end
  end

  describe "#type_accessible?" do
    it "returns true if the type is included in the export_accessible_types" do
      account.settings.export_accessible_types << "xml_export"
      assert export_configuration.type_accessible?("xml_export")
    end

    it "returns false if the type is not present in the export_accessible_types" do
      refute export_configuration.type_accessible?("foo")
    end

    it "returns true if the argument is a class that can be mapped to a type string" do
      account.settings.export_accessible_types << "xml_export"
      assert export_configuration.type_accessible?(XmlExportJob)
    end
  end

  describe "#whitelisted?" do
    let(:owner) { account.owner }
    let(:agent) { users(:minimum_agent) }

    describe "account has a export_whitelisted_domain" do
      it "returns true if the user is the account owner" do
        account.settings.export_whitelisted_domain = "example.net"
        assert export_configuration.whitelisted?(owner)
      end

      it "returns true if the user's email domain matches" do
        account.settings.export_whitelisted_domain = agent.email_domain
        assert export_configuration.whitelisted?(agent)
      end

      it "returns false if the user's email domain doesn't match" do
        account.settings.export_whitelisted_domain = "example.net"
        refute export_configuration.whitelisted?(agent)
      end
    end

    describe "account does not have a export_whitelisted_domain" do
      before { assert account.settings.export_whitelisted_domain.blank? }

      it "returns true" do
        assert export_configuration.whitelisted?(agent)
      end
    end
  end
end
