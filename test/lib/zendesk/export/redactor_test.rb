require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'Redactor' do
  fixtures :accounts

  describe "redacting fields" do
    before do
      @redactor = Zendesk::Export::Redactor.new
      @redactor.redact_fields = %w[req_email]

      @ticket = {
        "req_email" => "user@host.com",
        "field_1" => "1",
        "field_2" => "2",
        "field_3" => "3"
      }

      @ticket_after = {
        "field_1" => "1",
        "field_2" => "2",
        "field_3" => "3",
        "req_email" => "10468ad8d146df7dc85e4f8c51ef542e"
      }
    end

    it "redacts configured fields" do
      @redactor.redact(@ticket)
      assert_equal @ticket_after, @ticket
    end
  end

  describe "redactor setup" do
    before do
      @account = accounts(:minimum)
      @account.stubs(:has_redact_fields_for_incremental_export?).returns(true)
    end

    it "configures default redacted fields" do
      redactor = Zendesk::Export::Redactor.for_account(@account)
      assert_same_elements ['req_email'], redactor.redact_fields
    end
  end
end
