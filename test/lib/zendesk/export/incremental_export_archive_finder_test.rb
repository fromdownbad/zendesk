require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Export::IncrementalExportArchiveFinder do
  before do
    @export_association = mock("Ticket")
    @scoped_export_association = mock("Ticket.where")

    @export_association.expects(:where).at_least(0).returns(@scoped_export_association)

    @finder = Zendesk::Export::IncrementalExportArchiveFinder.new(
      includes: :includes,
      update_ts_field: 'updated_at',
      limit: 3,
      start_time: 0,
      export_association: @export_association
    )

    model_updated_zero = stub(attributes: {'updated_at' => 0})
    model_updated_one  = stub(attributes: {'updated_at' => 1})

    @result_smaller_than_limit = [model_updated_zero, model_updated_zero]
    @result_size_of_limit_within_second = [model_updated_one, model_updated_one, model_updated_one]
    @result_size_of_limit_across_seconds = [model_updated_zero, model_updated_one, model_updated_one]
  end

  it "returns a list of updated records less than limit" do
    Rails.application.config.statsd.expects(:increment).never

    @scoped_export_association.expects(:all_with_archived).with(anything).once.returns(@result_smaller_than_limit)

    assert_equal @result_smaller_than_limit, @finder.results
    assert_equal 0, @finder.next_timestamp
  end

  it "returns regular results when the updated records are across seconds" do
    Rails.application.config.statsd.expects(:increment).never

    @scoped_export_association.expects(:all_with_archived).with(anything).once.returns(@result_size_of_limit_across_seconds)

    assert_equal @result_size_of_limit_across_seconds, @finder.results
    assert_equal 1, @finder.next_timestamp
  end

  it "knows to query a second time if all the results are in the same second and at limit" do
    Rails.application.config.statsd.expects(:increment).with(
      'incremental_exporter_finder.unlimited_results', tags: %w[cause:more_results_than_limit]
    )

    @scoped_export_association.expects(:all_with_archived).with(has_entries(
      order: anything,
      limit: 3,
      include: :includes
    )).once.returns(@result_size_of_limit_within_second)

    second_result = @result_size_of_limit_within_second + [stub(attributes: {'updated_at' => 1})]
    @scoped_export_association.expects(:all_with_archived).with(Not(has_entries(limit: 3))).once.returns(second_result)

    assert_equal second_result, @finder.results
    assert_equal 2, @finder.next_timestamp
  end

  it "forces index because MySQL query planner is buggy" do
    # Skipping the "mock" export_association to assert on SQL query building
    finder = Zendesk::Export::IncrementalExportArchiveFinder.new(
      update_ts_field: 'generated_timestamp',
      limit: 3,
      start_time: 0,
      export_association: Ticket
    )
    sql_queries = sql_queries { finder.results }
    sql_queries.grep(/FROM `tickets`/).size.must_equal 2
    sql_queries.grep(/FROM `ticket_archive_stubs`/).size.must_equal 2
    sql_queries.each do |sql|
      sql.must_match(/index_account_id_and_generated_timestamp_and_status_id/)
    end
  end

  describe "#cursor_results" do
    before do
      @finder = Zendesk::Export::IncrementalExportArchiveFinder.new(
        includes: :includes,
        update_ts_field: 'updated_at',
        limit: 3,
        start_time: 0,
        export_association: @export_association,
        navigation: :cursor,
        cursor: cursor
      )

      @export_association.expects(:paginate_with_cursor).at_least(0).returns(stub(includes: @export_association))
    end

    describe "with a cursor present" do
      let(:cursor) { true }

      it "doesn't append timestamp" do
        @export_association.expects(:where).with(instance_of(Array)).never

        @export_association.stubs(:all_with_archived).returns(@result_smaller_than_limit)

        @finder.results
      end
    end

    describe "with no cursor present" do
      let(:cursor) { nil }

      it "appends timestamp" do
        @export_association.expects(:where).once.returns(@export_association)

        @export_association.stubs(:all_with_archived).returns(@result_smaller_than_limit)

        @finder.results
      end

      it "forces index because MySQL query planner is buggy" do
        # Skipping the "mock" export_association to assert on SQL query building
        finder = Zendesk::Export::IncrementalExportArchiveFinder.new(
          update_ts_field: 'generated_timestamp',
          limit: 3,
          start_time: 0,
          export_association: Ticket,
          navigation: :cursor,
          cursor: cursor
        )
        sql_queries = sql_queries { finder.cursor_results }
        sql_queries.grep(/FROM `tickets`/).size.must_equal 1
        sql_queries.grep(/FROM `ticket_archive_stubs`/).size.must_equal 1
        sql_queries.each do |sql|
          sql.must_match(/index_account_id_generated_timestamp_nice_id/)
        end
      end
    end
  end

  describe "applying limits and ordering" do
    let(:cursor) { nil }
    let(:included) { [] }
    let(:finder) do
      # Skipping the "mock" export_association to assert on SQL query building
      Zendesk::Export::IncrementalExportArchiveFinder.new(
        update_ts_field: 'generated_timestamp',
        limit: 3,
        start_time: 0,
        export_association: Ticket,
        navigation: :cursor,
        cursor: cursor,
        includes: included
      )
    end

    describe "with includes" do
      let(:included) { [:requester] }

      it "applies includes without N+1" do
        sql_queries = sql_queries { finder.cursor_results }
        sql_queries.grep(/FROM `tickets`/).size.must_equal 1
        sql_queries.grep(/FROM `ticket_archive_stubs`/).size.must_equal 1
        sql_queries.grep(/SELECT `users`.* FROM `users`/).size.must_equal 1
        sql_queries.size.must_equal 3
      end
    end

    it "applies limits and orders on cursor_results" do
      sql_queries = sql_queries { finder.cursor_results }
      sql_queries.grep(/FROM `tickets`/).size.must_equal 1
      sql_queries.grep(/FROM `ticket_archive_stubs`/).size.must_equal 1
      sql_queries.each do |sql|
        sql.must_match(/LIMIT 3/)
        sql.must_match(/ORDER BY generated_timestamp asc, nice_id asc/)
      end
    end

    it "applies limits and orders on limited_results" do
      sql_queries = sql_queries { finder.limited_results }
      sql_queries.grep(/FROM `tickets`/).size.must_equal 1
      sql_queries.grep(/FROM `ticket_archive_stubs`/).size.must_equal 1
      sql_queries.each do |sql|
        sql.must_match(/LIMIT 3/)
        sql.must_match(/ORDER BY generated_timestamp/)
      end
    end

    it "applies no limits, and orders by id on unlimited_results" do
      sql_queries = sql_queries { finder.unlimited_results_for_time(1582672433) }
      sql_queries.grep(/FROM `tickets`/).size.must_equal 1
      sql_queries.grep(/FROM `ticket_archive_stubs`/).size.must_equal 1
      sql_queries.each do |sql|
        sql.must_match(/ORDER BY id/)
        assert_nil sql.match(/LIMIT/)
      end
    end
  end
end
