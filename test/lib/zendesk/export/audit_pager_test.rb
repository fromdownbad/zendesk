require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Export::AuditPager do
  before do
    @subject = Zendesk::Export::AuditPager.new(3, 1)

    # Database ticket events
    @t0 = { created_at: Time.at(0), ticket_id: 100, ticket: { generated_timestamp: Time.at(0)} }
    @t1 = { created_at: Time.at(1), ticket_id: 240, ticket: { generated_timestamp: Time.at(1)} }
    @t2 = { created_at: Time.at(2), ticket_id: 481, ticket: { generated_timestamp: Time.at(2)} }
    @t3 = { created_at: Time.at(3), ticket_id: 575, ticket: { generated_timestamp: Time.at(3)} }
    @t4 = { created_at: Time.at(4), ticket_id: 819, ticket: { generated_timestamp: Time.at(4)} }
    @t5 = { created_at: Time.at(5), ticket_id: 333, ticket: { generated_timestamp: Time.at(5)} }
    @t7 = { created_at: Time.at(77), ticket_id: 777, ticket: { generated_timestamp: Time.at(7)} }

    # Archive ticket events
    @a0_4 = { created_at: Time.at(0), ticket_id: 4, ticket: { generated_timestamp: Time.at(2)}}
    @a1_4 = { created_at: Time.at(1), ticket_id: 4, ticket: { generated_timestamp: Time.at(2)}}
    @a2_4 = { created_at: Time.at(2), ticket_id: 4, ticket: { generated_timestamp: Time.at(2)}}

    @a0_7 = { created_at: Time.at(0), ticket_id: 7, ticket: { generated_timestamp: Time.at(5)}}
    @a5_7 = { created_at: Time.at(5), ticket_id: 7, ticket: { generated_timestamp: Time.at(5)}}

    @a1_5 = { created_at: Time.at(1), ticket_id: 5, ticket: { generated_timestamp: Time.at(3)}}
    @a3_5 = { created_at: Time.at(3), ticket_id: 5, ticket: { generated_timestamp: Time.at(3)}}
  end

  it 'does not blow up for no results' do
    assert_equal [[], nil], @subject.page([], [])
    assert_equal [[@t1], 1], @subject.page([@t1], [])
    assert_equal [[@t1], 1], @subject.page([], [@t1])
    assert_equal [[@t1, @t1, @t1], 2], @subject.page([], [@t1, @t1, @t1])
    assert_equal [[@t1, @t1, @t1], 2], @subject.page([@t1, @t1, @t1], [])
  end

  it 'pulls all from s1 when s1 is all one ts at limit' do
    assert_equal [[@t1, @t1, @t1], 2], @subject.page([@t1, @t1, @t1], [@t2, @t2, @t2])
    assert_equal [[@a0_4, @a1_4, @t2, @t2, @t2], 3], @subject.page([@t2, @t2, @t2], [@a0_4, @a1_4, @a5_7])
  end

  it 'combines and chop to limit when times vary' do
    assert_equal [[@t1, @t1, @a2_4], 2], @subject.page([@t1, @t1], [@a2_4, @a1_5, @a3_5])
  end

  it 'takes oldest records first' do
    assert_equal [[@t1, @t2, @t3], 3], @subject.page([@t1, @t2, @t3], [@t4, @t5, @t5])
  end

  it 'takes oldest records from *both* sources' do
    assert_equal [[@t1, @t2, @t2], 2], @subject.page([@t1, @t2, @t3], [@t2, @t3, @t4])
  end

  it 'expands limit when required' do
    assert_equal [[@t1, @t1, @t1, @t1, @t1], 2], @subject.page([@t1, @t1, @t1, @t1], [@t1])
    assert_equal [[@t1, @t1, @t1, @t1, @t1], 2], @subject.page([@t1, @t1, @t1, @t1], [@t1, @t2])
    assert_equal [[@t1, @t1, @t1, @t1, @t1], 2], @subject.page([@t1, @t1, @t1, @t1, @t1], [])
  end

  it 'sets the next time after the end time of the ticket when paging from archive' do
    assert_equal [[@a0_4, @a1_4, @a2_4], 3], @subject.page([], [@a0_4, @a1_4, @a2_4, @a3_5])
  end

  it 'handles events coming from before start_time from archiver' do
    assert_equal [[@a0_4, @a1_4, @a1_4, @a1_4, @a1_4], 3], @subject.page([], [@a0_4, @a1_4, @a1_4, @a1_4, @a1_4])
    assert_equal [[@a0_4, @a1_4, @a0_7, @a5_7], 5], @subject.page([], [@a0_4, @a1_4, @a0_7, @a5_7])
    assert_equal [[@t1, @a0_4, @a1_4], 2], @subject.page([@t1], [@a0_4, @a1_4, @a0_7, @a5_7])
  end

  it 'groups archivers ticket events by ticket' do
    assert_equal [[@a0_7, @a0_7, @a0_7, @a5_7], 6], @subject.page([], [@a0_7, @a0_7, @a0_7, @a5_7])
  end

  it 'groups database ticket events by ticket when event#created_at > ticket#generated_timestamp' do
    assert_equal [[@t4, @t5, @t7], 7], @subject.page([@t4, @t5, @t7], [])
  end

  it 'groups database ticket events by event when event#created_at is sensible' do
    assert_equal [[@t3, @t4, @t5], 5], @subject.page([@t3, @t4, @t5], [])
  end

  it "expands limit even when individually sources don't require it" do
    assert_equal [[@t1, @t1, @t1, @t1], 2], @subject.page([@t1, @t1, @t2], [@t1, @t1, @a5_7])
  end

  it 'sets the next time after the start time when all archived ticket events are before start time' do
    assert_equal [[@a0_4, @a0_4, @a0_4], 3], @subject.page([], [@a0_4, @a0_4, @a0_4])
    assert_equal [[@t1, @a0_4, @a0_4, @a0_4], 2], @subject.page([], [@a0_4, @a0_4, @a0_4, @t1])
  end

  it 'drops items more than the limit over time ranges but returns timestamp of last item in result' do
    assert_equal [[@t1, @a1_4, @t2], 2], @subject.page([@t1, @t2, @t2, @t3, @t3], [@a1_4])
  end

  describe 'nil ticket' do
    before do
      @tnil = { created_at: Time.at(0), ticket_id: 21421, ticket: nil }
    end

    it 'skips the event' do
      assert_equal [[@t0], 0], @subject.page([], [@tnil, @t0])
    end
  end
end
