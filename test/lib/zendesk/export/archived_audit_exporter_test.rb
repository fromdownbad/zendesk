require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Export::ArchivedAuditExporter do
  fixtures :accounts, :tickets, :events, :users

  let(:exporter) { Zendesk::Export::ArchivedAuditExporter.new(account, archive_items, limit) }

  let(:account) { accounts(:minimum) }
  let(:archive_items) { [{ id: ticket.id }] }
  let(:limit) { 1000 }

  let(:ticket) { tickets(:minimum_1) }

  around do |test|
    with_in_memory_archive_router(test)
  end

  before do
    ticket.archive!
  end

  describe "#get_archived_audits" do
    let(:get_archived_audits) { exporter.get_archived_audits }

    it 'returns an array of audit hashes' do
      num_audits = 2
      get_archived_audits.size.must_equal num_audits
    end

    it 'embeds specific child events within each audit' do
      get_archived_audits[0][:events].size.must_equal 5
      get_archived_audits[0][:events].map { |event| event[:type] }.must_equal ['Comment', 'Comment', 'Create', 'Create', 'Change']

      get_archived_audits[1][:events].size.must_equal 1
      get_archived_audits[1][:events].map { |event| event[:type] }.must_equal ['Comment']
    end

    describe 'with a TicketMergeAudit' do
      let(:ticket) do
        ticket = tickets(:minimum_1)
        ticket.add_ticket_merge_audit('source', user: users(:minimum_agent), target_id: 123)
        ticket.save
        ticket
      end

      it 'returns an array of audit hashes' do
        num_audits = 3
        get_archived_audits.size.must_equal num_audits
      end
    end

    it 'embeds the ticket attributes in the parent audit' do
      ticket_attrs = get_archived_audits[0][:ticket]
      ticket_attrs[:id].must_equal ticket.id

      expected_keys = [
        :id,
        :nice_id,
        :account_id,
        :requester_id,
        :assignee_id,
        :organization_id,
        :linked_id,
        :external_id,
        :group_id,
        :subject,
        :generated_timestamp,
        :created_at,
        :updated_at,
        :status_id,
        :ticket_type_id,
        :description,
        :brand_id,
        :is_public,
        :key_version
      ]

      ticket_attrs.keys.must_equal expected_keys
    end

    describe 'timestamps' do
      it 'converts event created_at to a ActiveSupport::TimeWithZone object' do
        created_at = get_archived_audits[0][:events][0][:created_at]
        assert_kind_of ActiveSupport::TimeWithZone, created_at
      end

      it "falls back to updated_at when created_at is missing" do
        ticket_data = { 'id' => 10, 'events' => [{'created_at' => nil, 'updated_at' => '2013-05-14 21:41:20'}]}
        ticket_blobs = [ZendeskArchive::Blob.new(ticket_data, mock)]
        events = exporter.send(:events_from_archived_tickets, ticket_blobs)
        updated_at_time = Time.parse '2013-05-14 21:41:20'

        assert_equal updated_at_time, events.first[:created_at]
      end
    end
  end
end
