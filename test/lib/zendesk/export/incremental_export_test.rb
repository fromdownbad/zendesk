require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Export::IncrementalExport do
  fixtures :users

  let(:page_url_method) { :my_model_url_method }
  let(:url_builder) { stub(params: {}) }
  let(:current_user) { users(:minimum_agent) }
  let(:presenter) { stub(present: {previous_page: ''}) }
  let(:finder) { stub(next_timestamp: 5) }
  let(:limit) { 10 }
  let(:enforce_last_minute_limit) { true }

  let(:incremental_api_export) do
    Zendesk::Export::IncrementalExport.new(
      page_url_method: page_url_method,
      custom_finder: finder,
      url_builder: url_builder,
      presenter: presenter,
      limit: limit
    )
  end

  describe "#next_page" do
    it "returns a url when models are present" do
      finder.expects(:results).returns(['a'])

      url_builder.expects(:send).with(page_url_method, anything).returns('stub_url')

      assert_equal 'stub_url', incremental_api_export.as_json[:next_page]
    end

    it "maintains include param" do
      finder.expects(:results).returns(['a'])
      base_params = { start_time: 5 }
      controller_params = { includes: 'side_load' }

      url_builder.expects(:params).returns(controller_params)
      url_builder.expects(:send).with(page_url_method, base_params.merge(controller_params)).returns('stub_url')

      assert_equal 'stub_url', incremental_api_export.as_json[:next_page]
    end

    it "returns nil when the models collection is empty" do
      finder.expects(:results).returns([])

      url_builder.expects(:send).with(page_url_method, anything).never

      assert_nil incremental_api_export.as_json[:next_page]
    end

    it "includes additional parameters passed to the exporter" do
      incremental_api_export = Zendesk::Export::IncrementalExport.new(
        page_url_method: page_url_method,
        url_builder: url_builder,
        presenter: presenter,
        next_page_parameters: { my_param: 'some_value' },
        custom_finder: stub(results: ['a'], next_timestamp: 5),
        limit: limit
      )

      controller_params = { format: :json }
      url_builder.stubs(:params).returns(controller_params)
      params = { start_time: 5, my_param: 'some_value'}.merge(controller_params)
      url_builder.expects(:send).with(page_url_method, params).returns('stub_url')

      assert_equal 'stub_url', incremental_api_export.as_json[:next_page]
    end
  end

  describe "end_of_stream" do
    it "returns false when results are equal to the limit" do
      finder.expects(:results).returns([*1..limit])

      url_builder.expects(:send).with(page_url_method, anything).returns('stub_url')

      assert_equal false, incremental_api_export.as_json[:end_of_stream]
    end

    it "returns true when results are less than the limit" do
      finder.expects(:results).returns([1])

      url_builder.expects(:send).with(page_url_method, anything).returns('stub_url')

      assert(incremental_api_export.as_json[:end_of_stream])
    end
  end
end
