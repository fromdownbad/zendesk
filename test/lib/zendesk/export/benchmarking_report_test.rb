require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 11

describe Zendesk::Export::BenchmarkingReport do
  fixtures :accounts

  let(:response) { Account::SurveyResponse.create(account: accounts(:minimum)) }

  before do
    Zendesk::S3ReportingStore.any_instance.expects(:store!)
  end

  it "stores the report in S3" do
    Zendesk::Export::BenchmarkingReport.execute
  end

  describe "when the account has been soft-deleted" do
    before do
      response.account.cancel!
      response.account.soft_delete!
      response.account.subscription.destroy!
    end

    it "skips the soft-deleted account" do
      Zendesk::Export::BenchmarkingReport.expects(:survey_response_as_array).never
      Zendesk::Export::BenchmarkingReport.execute
    end
  end

  describe "when the account is locked" do
    before do
      Account.any_instance.stubs(:is_locked?).returns(true)
    end

    it "skips the locked account" do
      Zendesk::Export::BenchmarkingReport.expects(:survey_response_as_array).never
      Zendesk::Export::BenchmarkingReport.execute
    end
  end

  describe "when the account has no subscription" do
    it "skips the account" do
      Zendesk::Export::BenchmarkingReport.expects(:survey_response_as_array).never
      Zendesk::Export::BenchmarkingReport.execute
    end
  end
end
