require_relative "../../support/test_helper"

SingleCov.covered!

describe 'ZendeskTicketFieldConditionConvertion' do
  describe Zendesk::TicketFieldConditionConversion do
    describe "#checkbox" do
      let(:field) { FieldCheckbox.new }

      it "properly converts values to db representation" do
        assert_equal '1', field.condition_value_to_db('1')
        assert_equal '1', field.condition_value_to_db(true)
        assert_equal '0', field.condition_value_to_db('No')
        assert_equal '0', field.condition_value_to_db(false)
        assert_equal '0', field.condition_value_to_db('invalid')
      end

      it "properly presents values" do
        assert(field.present_condition_value('1'))
        assert_equal false, field.present_condition_value('0')
      end
    end

    describe "#priority" do
      let(:field) { FieldPriority.new }

      it "properly converts values to db representation" do
        assert_equal '1', field.condition_value_to_db('low')
        assert_equal '3', field.condition_value_to_db('high')
        assert_equal '0', field.condition_value_to_db('-')
        assert_equal 'invalid', field.condition_value_to_db('invalid')
      end

      it "properly presents values" do
        assert_equal 'low', field.present_condition_value('1')
        assert_equal 'high', field.present_condition_value('3')
        assert_nil field.present_condition_value('-')
      end
    end

    describe "#ticket_type" do
      let(:field) { FieldTicketType.new }

      it "properly converts values to db representation" do
        assert_equal '1', field.condition_value_to_db('question')
        assert_equal '3', field.condition_value_to_db('problem')
        assert_equal '0', field.condition_value_to_db('-')
        assert_equal 'invalid', field.condition_value_to_db('invalid')
      end

      it "properly presents values" do
        assert_equal 'question', field.present_condition_value('1')
        assert_equal 'problem', field.present_condition_value('3')
        assert_nil field.present_condition_value('-')
      end
    end
  end
end
