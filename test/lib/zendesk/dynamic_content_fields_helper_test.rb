require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::DynamicContentFieldsHelper do
  fixtures :accounts

  before do
    @object = Object.new
    @object.extend(Zendesk::DynamicContentFieldsHelper)
    @object.instance_variable_set(:@account, account)
  end

  let(:account) { accounts(:minimum) }

  describe "#map_dc_parameters" do
    let(:params) { { ticket_field: {} } }
    let(:expected) { { ticket_field: {} } }

    it "dos nothing and not blow up if there is params is an empty hash" do
      @object.map_dc_parameters(["description"], {})
    end

    it "returns nil if params is nil" do
      @object.map_dc_parameters(["description"], nil)
    end

    it "maps the raw param to the model attribute" do
      params[:ticket_field]   = { raw_description: "Hi {{dc.user}}" }
      expected[:ticket_field] = { description: "Hi {{dc.user}}" }

      @object.map_dc_parameters(["description"], params[:ticket_field])
      assert_equal expected, params
    end

    it "logs if request is writing to non-raw attribute" do
      expected[:ticket_field] = params[:ticket_field] = { description: "Hi {{dc.user}}" }
      Rails.logger.expects(:debug).with("Account: #{account.id} writing to description rather than raw_description")

      @object.map_dc_parameters(["description"], params[:ticket_field])
      assert_equal expected, params
    end

    it "does not call #map_dc_custom_field_options if param is passed" do
      @object.expects(:map_dc_custom_field_options).never
      @object.map_dc_parameters(["description"], {}, true)
    end
  end

  describe "#map_dc_custom_field_options" do
    it "gos through the custom_field_options and call map_dynamic_content_attribute" do
      custom_field_options = [{raw_name: "Name dc {{dc.stuff}}", value: "the_value"}, {name: "Stuff dc {{dc.stuff}}", value: "the_value_2"}]
      params = {custom_field_options: custom_field_options}
      expected_custom_fields = [{name: "Name dc {{dc.stuff}}", value: "the_value"}, {name: "Stuff dc {{dc.stuff}}", value: "the_value_2"}]

      Rails.logger.expects(:debug).with("Account: #{account.id} writing to name rather than raw_name")
      @object.map_dc_custom_field_options(params)

      assert_equal expected_custom_fields, params[:custom_field_options]
    end
  end

  describe "#map_dynamic_content_attribute" do
    it "maps to the non-raw attribute if we try to write to the raw attribute" do
      params   = {raw_description: "Stuff with dc {{dc.wombats}}"}
      expected = {description: "Stuff with dc {{dc.wombats}}"}
      @object.map_dynamic_content_attribute(params, "description")

      assert_equal expected, params
    end

    it "logs if we try to write to the non raw attribute" do
      expected = params = {description: "Stuff with dc {{dc.wombats}}"}
      Rails.logger.expects(:debug).with("Account: #{account.id} writing to description rather than raw_description")
      @object.map_dynamic_content_attribute(params, "description")

      assert_equal expected, params
    end
  end
end
