require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::Client do
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:client)  { Zendesk::Deco::Client.new(account) }

  let(:connection) { stub('connection') }
  let(:payload)    { stub('payload') }

  let(:options) do
    opt = stub('options')
    opt.
      expects(:timeout=).
      with(Zendesk::Deco::Client::DEFAULT_REQUEST_TIMEOUT_SECS)
    opt.
      expects(:open_timeout=).
      with(Zendesk::Deco::Client::DEFAULT_OPEN_TIMEOUT_SECS)
    opt
  end
  let(:headers) do
    stub('headers')
  end
  let(:request) do
    req = stub('request')
    req.stubs(:options).returns(options)
    req.stubs(:headers).returns(headers)
    req
  end

  before do
    retry_options = {
      max: 3
    }
    circuit_options = {
      failure_threshold: 3,
      reset_timeout: 5,
      dry_run: false
    }
    Kragle.stubs(:new).
      with('deco',
        retry_options: retry_options,
        circuit_options: circuit_options).
      returns(connection)
  end

  let(:url_prefix) { 'http://localhost:8888' }

  describe '#get' do
    describe 'neither json_body nor timeout parameters passed in' do
      before do
        request.expects(:url).
          with("#{url_prefix}/#{account.id}/attributes")
        connection.
          stubs(:get).
          yields(request).
          returns(payload)
      end

      it 'returns the payload' do
        assert_equal payload, client.get('attributes')
      end

      it 'does not mark the account as using routing' do
        client.get('attributes')
        refute account.settings.using_skill_based_routing
      end
    end

    describe 'json_body passed in' do
      before do
        request.expects(:url).
          with("#{url_prefix}/#{account.id}/attributes")
        request.expects(:body=).with('{"foo": "bar"}')
        headers.expects(:[]=).with('Content-Type', 'application/json')
        connection.
          stubs(:get).
          yields(request).
          returns(payload)
      end

      it 'returns the payload' do
        assert_equal payload, client.get('attributes', '{"foo": "bar"}')
      end
    end

    describe 'json_body and timeout parameters passed in' do
      let(:options) do
        opt = stub('options')
        opt.expects(:timeout=).with(12)
        opt.expects(:open_timeout=).with(34)
        opt
      end

      before do
        request.expects(:url).
          with("#{url_prefix}/#{account.id}/attributes")
        request.expects(:body=).with('{"foo": "bar"}')
        headers.expects(:[]=).with('Content-Type', 'application/json')
        connection.
          stubs(:get).
          yields(request).
          returns(payload)
      end

      it 'returns the payload' do
        assert_equal payload, client.get('attributes',
          '{"foo": "bar"}',
          12,
          34)
      end
    end

    describe 'timeout parameters passed in but json_body not supplied' do
      let(:options) do
        opt = stub('options')
        opt.expects(:timeout=).with(123)
        opt.expects(:open_timeout=).with(456)
        opt
      end

      before do
        request.expects(:url).
          with("#{url_prefix}/#{account.id}/attributes")
        connection.
          stubs(:get).
          yields(request).
          returns(payload)
      end

      it 'returns the payload' do
        assert_equal payload, client.get('attributes', nil, 123, 456)
      end
    end
  end

  describe '#post' do
    before do
      request.expects(:url).
        with("#{url_prefix}/#{account.id}/attributes")
      request.expects(:body=).with(name: 'Tier')
      headers.expects(:[]=).with('Content-Type', 'application/json')
      connection.
        stubs(:post).
        yields(request).
        returns(payload)
    end

    it 'returns the payload' do
      assert_equal payload, client.post('attributes', name: 'Tier')
    end

    it 'marks the account as using routing' do
      client.post('attributes', name: 'Tier')
      assert account.settings.using_skill_based_routing
    end
  end

  describe '#put' do
    before do
      request.expects(:url).
        with("#{url_prefix}/#{account.id}/attributes/1")
      request.expects(:body=).with(name: 'Level')
      headers.expects(:[]=).with('Content-Type', 'application/json')
      connection.
        stubs(:put).
        yields(request).
        returns(payload)
    end

    it 'returns the payload' do
      assert_equal payload, client.put('attributes/1', name: 'Level')
    end

    it 'marks the account as using routing' do
      client.put('attributes/1', name: 'Level')
      assert account.settings.using_skill_based_routing
    end
  end

  describe '#delete' do
    before do
      request.expects(:url).
        with("#{url_prefix}/#{account.id}/attributes/1")
      connection.
        stubs(:delete).
        yields(request).
        returns(payload)
    end

    it 'returns the payload' do
      assert_equal payload, client.delete('attributes/1')
    end

    it 'does not mark the account as using routing' do
      client.delete('attributes/1')
      refute account.settings.using_skill_based_routing
    end
  end

  describe '#attributes' do
    let(:request) { stub('attributes request') }

    before do
      Zendesk::Deco::Request::Attributes.
        stubs(:new).
        with(client, includes: 'attribute_values,agent_count').
        returns(request)
    end

    it 'returns an attributes request' do
      assert_equal request, client.attributes('attribute_values,agent_count')
    end
  end

  describe '#attribute' do
    let(:request) { stub('attribute request') }

    before do
      Zendesk::Deco::Request::Attribute.
        stubs(:new).
        with(client, id: '6').
        returns(request)
    end

    it 'returns an attribute request' do
      assert_equal request, client.attribute('6')
    end
  end

  describe '#attribute_value' do
    let(:request) { stub('attribute value request') }

    before do
      Zendesk::Deco::Request::AttributeValue.
        stubs(:new).
        with(client, attribute_id: '6', id: '1').
        returns(request)
    end

    it 'returns an attribute value request' do
      assert_equal request, client.attribute_value('6', '1')
    end
  end

  describe '#attribute_values' do
    let(:request) { stub('attribute values request') }

    before do
      Zendesk::Deco::Request::AttributeValues.
        stubs(:new).
        with(client).
        returns(request)
    end

    it 'returns an attribute values request' do
      assert_equal request, client.attribute_values
    end
  end

  describe '#incremental_instance_values' do
    let(:request) { stub('instance values request') }

    before do
      Zendesk::Deco::Request::IncrementalInstanceValues.
        expects(:new).
        with(client).
        returns(request)
    end

    it 'returns an incremental instance values request' do
      assert_equal request, client.incremental_instance_values
    end
  end

  describe '#base_url' do
    describe 'DECO_URL is defined' do
      before do
        @previous_deco_url = ENV['DECO_URL']
        ENV['DECO_URL'] = 'test_url'
      end

      after do
        ENV['DECO_URL'] = @previous_deco_url
      end

      it 'returns env var' do
        assert_equal 'test_url', client.base_url
      end
    end

    describe 'DECO_URL is not defined' do
      before do
        @previous_deco_url = ENV['DECO_URL']
        ENV.delete('DECO_URL')
      end

      after do
        ENV['DECO_URL'] = @previous_deco_url
      end

      it 'uses `kubernetes_service_url`' do
        assert_equal 'http://deco.pod1.svc.cluster.local.', client.base_url
      end
    end
  end
end
