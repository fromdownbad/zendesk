require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::InstanceValue do
  let(:instance_value) { Zendesk::Deco::InstanceValue.new('1', '2') }

  describe '.from_json' do
    it 'returns a new instance value' do
      assert_equal(
        instance_value,
        Zendesk::Deco::InstanceValue.from_json(
          'id'          => '1',
          'instance_id' => '2'
        )
      )
    end
  end

  describe '#id' do
    it 'returns the instance value ID' do
      assert_equal '1', instance_value.id
    end
  end

  describe '#instance_id' do
    it 'returns the instance ID' do
      assert_equal '2', instance_value.instance_id
    end
  end
end
