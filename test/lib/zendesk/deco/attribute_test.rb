require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::Attribute do
  let(:attribute) do
    Zendesk::Deco::Attribute.new(
      '1',
      'Language',
      '2017-08-25T10:43:48Z',
      '2017-08-28T14:51:29Z'
    )
  end

  describe '.from_json' do
    it 'returns a new attribute' do
      assert_equal(
        attribute,
        Zendesk::Deco::Attribute.from_json(
          'id'         => '1',
          'name'       => 'Language',
          'created_at' => '2017-08-25T10:43:48Z',
          'updated_at' => '2017-08-28T14:51:29Z'
        )
      )
    end
  end

  describe '#id' do
    it 'returns the attribute ID' do
      assert_equal '1', attribute.id
    end
  end

  describe '#name' do
    it 'returns the attribute name' do
      assert_equal 'Language', attribute.name
    end
  end

  describe '#created_at' do
    it 'returns the created date' do
      assert_equal '2017-08-25T10:43:48Z', attribute.created_at
    end
  end

  describe '#updated_at' do
    it 'returns the updated date' do
      assert_equal '2017-08-28T14:51:29Z', attribute.updated_at
    end
  end
end
