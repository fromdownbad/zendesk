require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::Request::AttributeValues do
  fixtures :all

  let(:cursor) { '1234-abcd-5678-efgh' }

  let(:client) { Zendesk::Deco::Client.new(accounts(:minimum)) }

  let(:request) do
    Zendesk::Deco::Request::AttributeValues.new(client)
  end

  let(:attribute_value_fixtures) do
    YAML.load_file(
      "#{Rails.root}/test/files/deco/attribute_values.yml"
    )['attribute_values']
  end

  let(:attribute_value_fixture) do
    attribute_value_fixtures.find { |json| json['id'] == '6' }
  end

  describe '#index' do
    before do
      client.
        stubs(:get).
        with('attributes/1/values').
        returns(
          stub(
            'response',
            body: {'attribute_values' => attribute_value_fixtures}
          )
        )

      client.
        stubs(:get).
        with('attributes/1/values?cursor=1&limit=1').
        returns(
          stub(
            'response',
            body: {'attribute_values' => [attribute_value_fixture]}
          )
        )
    end

    describe 'without pagination arguments' do
      it 'returns the values associated with the attribute' do
        assert_equal(
          attribute_value_fixtures.map do |json|
            Zendesk::Deco::AttributeValue.from_json(json)
          end,
          request.index('1')
        )
      end
    end

    describe 'with pagination arguments' do
      it 'returns the subset of values associated with the attribute' do
        assert_equal(
          [Zendesk::Deco::AttributeValue.from_json(attribute_value_fixture)],
          request.index('1', cursor: '1', limit: '1')
        )
      end
    end
  end

  describe '#incremental' do
    before do
      client.
        expects(:get).
        with(request_url).
        returns(
          stub(
            'response',
            body: {'attribute_values' => attribute_value_fixtures}
          )
        )
    end

    describe 'when `start_time` is provided' do
      let(:request_url) { 'attribute_values/incremental?start_time=1' }

      it 'returns attribute values associated with the account' do
        assert_equal(
          attribute_value_fixtures.map do |json|
            Zendesk::Deco::AttributeValue.from_json(json)
          end,
          request.incremental(1, nil)
        )
      end
    end

    describe 'when `cursor` is provided' do
      let(:request_url) { "attribute_values/incremental?cursor=#{cursor}" }

      it 'returns attribute values associated with the account' do
        assert_equal(
          attribute_value_fixtures.map do |json|
            Zendesk::Deco::AttributeValue.from_json(json)
          end,
          request.incremental(nil, cursor)
        )
      end
    end

    describe 'when neither `start_time` nor `cursor` is provided' do
      let(:request_url) { 'attribute_values/incremental?start_time=0' }

      it 'returns attribute values associated with the account' do
        assert_equal(
          attribute_value_fixtures.map do |json|
            Zendesk::Deco::AttributeValue.from_json(json)
          end,
          request.incremental(nil, nil)
        )
      end
    end
  end

  describe '#create' do
    let(:attribute_value_fixture) do
      attribute_value_fixtures.find { |json| json['name'] == 'Japanese' }
    end

    before do
      client.
        stubs(:post).
        with('attributes/1/values', name: 'Japanese').
        returns(
          stub('response', body: {'attribute_value' => attribute_value_fixture})
        )
    end

    it 'creates an attribute value' do
      assert_equal(
        Zendesk::Deco::AttributeValue.from_json(attribute_value_fixture),
        request.create('1', name: 'Japanese')
      )
    end
  end

  describe '#for_instance' do
    let(:attribute_fixture) do
      yml = YAML.load_file("#{Rails.root}/test/files/deco/attributes.yml")
      yml['attributes'].find { |json| json['id'] == '6' }
    end

    let(:attribute_values_for_instance_fixtures) do
      [
        {
          'attribute' => attribute_fixture,
          'values' => attribute_value_fixtures
        }
      ]
    end

    before do
      client.
        stubs(:get).
        with('type/10/instance/1/attribute_values').
        returns(
          stub(
            'response',
            body: {'items' => attribute_values_for_instance_fixtures}
          )
        )
    end

    it 'returns the values associated with the attribute' do
      assert_equal(
        attribute_value_fixtures.map do |json|
          Zendesk::Deco::AttributeValue.from_json(json)
        end,
        request.for_instance(10, 1)
      )
    end
  end
end
