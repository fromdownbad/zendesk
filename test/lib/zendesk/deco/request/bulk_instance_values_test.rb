require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::Request::BulkInstanceValues do
  fixtures :all

  let(:client)        { Zendesk::Deco::Client.new(accounts(:minimum)) }
  let(:agent_type_id) { Zendesk::Deco::Client::TYPE_ID_AGENT }

  let(:request) do
    Zendesk::Deco::Request::BulkInstanceValues.new(client)
  end

  describe '#post' do
    before do
      request.post(
        '1',
        '6',
        create_instance_ids: ['246357'],
        delete_instance_ids: ['12345'],
        type_id:             agent_type_id
      )
    end

    before_should 'request to associate/unassociate the instances' do
      client.
        expects(:post).
        with(
          'attributes/1/values/6/bulk_instance_values',
          create_instance_ids: ['246357'],
          delete_instance_ids: ['12345'],
          type_id:             agent_type_id
        )
    end
  end

  describe '#post_instance_values' do
    let(:att_val_fixtures) do
      YAML.load_file(
        Rails.root.join('test', 'files', 'deco', 'attributes_and_values.yml')
      )['attributes']
    end

    before do
      client.
        expects(:post).
        with(
          'types/10/instances/11/bulk_instance_values',
          attribute_value_ids: %w[246357 12345]
        ).
        returns(stub('response', body: {
                        'items' => att_val_fixtures
                      }))
    end

    it 'returns info about att vals that were set' do
      response = request.post_instance_values(
        '10',
        '11',
        %w[246357 12345]
      )

      expected = att_val_fixtures.map do |att|
        att['values'].map do |val|
          Zendesk::Deco::AttributeValue.from_json(val)
        end
      end.flatten
      assert_equal expected, response
    end
  end
end
