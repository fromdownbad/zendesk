require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::Request::InstanceValues do
  fixtures :all

  let(:client) { Zendesk::Deco::Client.new(accounts(:minimum)) }
  let(:ticket_type_id) do
    Zendesk::Deco::Client::TYPE_ID_TICKET
  end

  let(:request) do
    Zendesk::Deco::Request::InstanceValues.new(
      client,
      attribute_id:       '1',
      attribute_value_id: '6'
    )
  end

  let(:instance_value_fixtures) do
    YAML.load_file(
      "#{Rails.root}/test/files/deco/instance_values.yml"
    )['instance_values']
  end

  describe '#create' do
    let(:instance_value_fixture) do
      instance_value_fixtures.find { |json| json['instance_id'] == '246357' }
    end

    before do
      client.
        expects(:post).
        with(
          'attributes/1/values/6/instance_values',
          instance_id: '246357',
          type_id: ticket_type_id
        ).
        returns(
          stub('response', body: {'instance_value' => instance_value_fixture})
        )
    end

    it 'creates the instance association value' do
      request.create(instance_id: '246357', type_id: ticket_type_id)
    end
  end

  describe '#index' do
    let(:agent_type_id) { Zendesk::Deco::Client::TYPE_ID_AGENT }

    let(:instance_value_fixture) do
      instance_value_fixtures.find { |json| json['instance_id'] == '246357' }
    end

    before do
      client.
        expects(:get).
        with("attributes/1/values/6/instance_values?type_id=#{agent_type_id}").
        returns(
          stub('response', body: {'instance_values' => instance_value_fixtures})
        )
    end

    it 'returns the associated instance values for the given type' do
      assert_equal(
        instance_value_fixtures.map do |instance_value|
          Zendesk::Deco::InstanceValue.from_json(instance_value)
        end,
        request.index(agent_type_id)
      )
    end
  end
end
