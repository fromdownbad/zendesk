require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::Request::TicketRequirements do
  fixtures :all

  let(:client)  { Zendesk::Deco::Client.new(accounts(:minimum)) }
  let(:request) { Zendesk::Deco::Request::TicketRequirements.new(client) }
  let(:agent) { users(:minimum_agent) }

  describe '#fulfilled_ticket_ids' do
    let(:requiring_ids) { [1, 2] }
    let(:fulfilled_ids) { %w[1 2] }
    before do
      client.
        stubs(:get).
        with("requirements/10/fulfilled_by/20/#{agent.id}",
          {requiring_ids: requiring_ids}.to_json).
        returns(stub('response', body: {'fulfilled_ids' => fulfilled_ids}))
    end

    it 'returns the fulfilled IDs' do
      assert_equal(
        fulfilled_ids,
        request.fulfilled_ticket_ids(agent, requiring_ids)
      )
    end
  end
end
