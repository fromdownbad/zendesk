require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::Request::IncrementalInstanceValues do
  fixtures :all

  let(:cursor) { '1234-abcd-5678-efgh' }

  let(:account) { accounts(:minimum) }

  let(:client) { Zendesk::Deco::Client.new(account) }

  let(:request) do
    Zendesk::Deco::Request::IncrementalInstanceValues.new(client)
  end

  let(:instance_value_fixtures) do
    YAML.load_file(
      "#{Rails.root}/test/files/deco/instance_values.yml"
    )['instance_values']
  end

  describe '#incremental' do
    let(:timeout) { 5 }

    before do
      client.
        expects(:get).
        with(request_url, nil, timeout).
        returns(
          stub(
            'response',
            body: {'instance_values' => instance_value_fixtures}
          )
        )
    end

    describe 'when `start_time` is provided' do
      let(:request_url) { 'instance_values/incremental?start_time=1' }

      it 'returns attribute values associated with the account' do
        assert_equal(
          instance_value_fixtures.map do |json|
            Zendesk::Deco::InstanceValue.from_json(json)
          end,
          request.incremental(1, nil)
        )
      end
    end

    describe 'when `cursor` is provided' do
      let(:request_url) { "instance_values/incremental?cursor=#{cursor}" }

      it 'returns attribute values associated with the account' do
        assert_equal(
          instance_value_fixtures.map do |json|
            Zendesk::Deco::InstanceValue.from_json(json)
          end,
          request.incremental(nil, cursor)
        )
      end
    end

    describe 'when neither `start_time` nor `cursor` is provided' do
      let(:request_url) { 'instance_values/incremental?start_time=0' }

      it 'returns attribute values associated with the account' do
        assert_equal(
          instance_value_fixtures.map do |json|
            Zendesk::Deco::InstanceValue.from_json(json)
          end,
          request.incremental(nil, nil)
        )
      end
    end

    describe 'when account has a custom timeout setting' do
      let(:timeout) { 10 }
      let(:request_url) { 'instance_values/incremental?start_time=0' }

      before do
        account.settings.deco_client_incremental_timeout = 10
      end

      it 'makes the request with the custom timeout' do
        request.incremental(nil, nil)
      end
    end
  end
end
