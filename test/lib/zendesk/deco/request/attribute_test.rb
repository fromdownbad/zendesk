require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::Request::Attribute do
  fixtures :all

  let(:client)  { Zendesk::Deco::Client.new(accounts(:minimum)) }
  let(:request) { Zendesk::Deco::Request::Attribute.new(client, id: '6') }

  let(:attribute_fixture) do
    YAML.load_file("#{Rails.root}/test/files/deco/attributes.yml")['attributes'].
      find { |json| json['id'] == '6' }
  end

  describe '#show' do
    before do
      client.
        stubs(:get).
        with('attributes/6').
        returns(stub('response', body: {'attribute' => attribute_fixture}))
    end

    it 'returns the attribute' do
      assert_equal(
        Zendesk::Deco::Attribute.from_json(attribute_fixture),
        request.show
      )
    end
  end

  describe '#update' do
    before do
      client.
        stubs(:put).
        with('attributes/6', name: 'Produce').
        returns(stub('response', body: {'attribute' => attribute_fixture}))
    end

    it 'updates the attribute' do
      assert_equal(
        Zendesk::Deco::Attribute.from_json(attribute_fixture),
        request.update(name: 'Produce')
      )
    end
  end

  describe '#delete' do
    before { client.stubs(:delete).with('attributes/6').returns(response) }

    describe 'when the response is `204 No Content`' do
      let(:response) { stub('response', status: 204) }

      it 'returns true' do
        assert request.delete
      end
    end

    describe 'when the response is not `204 No Content`' do
      let(:response) { stub('response', status: 500) }

      it 'returns false' do
        refute request.delete
      end
    end
  end

  describe '#values' do
    let(:values_request) { stub('attribute values request') }

    before do
      Zendesk::Deco::Request::AttributeValues.
        stubs(:new).
        with(client).
        returns(values_request)
    end

    it 'returns an attribute values request' do
      assert_equal values_request, request.values
    end
  end

  describe '#value' do
    let(:value_request) { stub('attribute value request') }

    before do
      Zendesk::Deco::Request::AttributeValue.
        stubs(:new).
        with(client, attribute_id: '6', id: '1').
        returns(value_request)
    end

    it 'returns an attribute value request' do
      assert_equal value_request, request.value('1')
    end
  end
end
