require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::Request::AttributeValue do
  fixtures :all

  let(:client) { Zendesk::Deco::Client.new(accounts(:minimum)) }

  let(:request) do
    Zendesk::Deco::Request::AttributeValue.new(
      client,
      attribute_id: '1',
      id:           '6'
    )
  end

  let(:agent_type_id) { Zendesk::Deco::Client::TYPE_ID_AGENT }

  let(:attribute_value_fixture) do
    YAML.load_file(
      "#{Rails.root}/test/files/deco/attribute_values.yml"
    )['attribute_values'].find { |json| json['id'] == '6' }
  end

  describe '#show' do
    before do
      client.
        stubs(:get).
        with('attributes/1/values/6').
        returns(
          stub('response', body: {'attribute_value' => attribute_value_fixture})
        )
    end

    it 'returns the attribute value' do
      assert_equal(
        Zendesk::Deco::AttributeValue.from_json(attribute_value_fixture),
        request.show
      )
    end
  end

  describe '#update' do
    before do
      client.
        stubs(:put).
        with('attributes/1/values/6', name: 'Portuguese').
        returns(
          stub('response', body: {'attribute_value' => attribute_value_fixture})
        )
    end

    it 'updates the attribute value' do
      assert_equal(
        Zendesk::Deco::AttributeValue.from_json(attribute_value_fixture),
        request.update(name: 'Portuguese')
      )
    end
  end

  describe '#delete' do
    describe 'without timeout' do
      before do
        client.stubs(:delete).with('attributes/1/values/6', Zendesk::Deco::Client::DEFAULT_REQUEST_TIMEOUT_SECS).returns(response)
      end

      describe 'when the response is `204 No Content`' do
        let(:response) { stub('response', status: 204) }

        it 'returns true' do
          assert request.delete
        end
      end

      describe 'when the response is not `204 No Content`' do
        let(:response) { stub('response', status: 500) }

        it 'returns false' do
          refute request.delete
        end
      end
    end

    describe 'with timeout' do
      let(:timeout) { 10 }

      before do
        client.stubs(:delete).with('attributes/1/values/6', timeout).returns(response)
      end

      describe 'when the response is `204 No Content`' do
        let(:response) { stub('response', status: 204) }

        it 'returns true' do
          assert request.delete(timeout)
        end
      end

      describe 'when the response is not `204 No Content`' do
        let(:response) { stub('response', status: 500) }

        it 'returns false' do
          refute request.delete(timeout)
        end
      end
    end
  end

  describe '#associate' do
    let(:ticket)         { tickets(:minimum_1) }
    let(:ticket_type_id) { Zendesk::Deco::Client::TYPE_ID_TICKET }

    let(:instance_values_request) { stub('instance values request') }

    before { request.associate(ticket, type_id: ticket_type_id) }

    before_should 'associate the ticket with the attribute value' do
      Zendesk::Deco::Request::InstanceValues.
        expects(:new).
        with(client, attribute_id: '1', attribute_value_id: '6').
        returns(instance_values_request)

      instance_values_request.
        expects(:create).
        with(instance_id: ticket.id, type_id: ticket_type_id)
    end
  end

  describe '#bulk_instances' do
    let(:added_agent)   { users(:minimum_agent) }
    let(:removed_agent) { users(:minimum_admin) }

    let(:bulk_instance_values_request) { stub('bulk instance values request') }

    before do
      request.bulk_instances(
        create_instances: [added_agent],
        delete_instances: [removed_agent],
        type_id:          agent_type_id
      )
    end

    before_should 'associate/unassociate instances with the attribute value' do
      Zendesk::Deco::Request::BulkInstanceValues.
        expects(:new).
        returns(bulk_instance_values_request)

      bulk_instance_values_request.
        expects(:post).
        with(
          '1',
          '6',
          create_instance_ids: [added_agent.id],
          delete_instance_ids: [removed_agent.id],
          type_id:             agent_type_id
        )
    end
  end

  describe '#associations' do
    let(:instance_values_request) { stub('instance values request') }
    let(:associations)            { stub('associations') }

    before do
      Zendesk::Deco::Request::InstanceValues.
        stubs(:new).
        with(client, attribute_id: '1', attribute_value_id: '6').
        returns(instance_values_request)

      instance_values_request.
        stubs(:index).
        with(agent_type_id).
        returns(associations)
    end

    it 'returns the associated instance values for the given type' do
      assert_equal associations, request.associations(agent_type_id)
    end
  end

  describe '#instances_with_value' do
    let(:agents) { [users(:minimum_agent), users(:minimum_admin)] }

    let(:instances_with_value_request) { stub('instances with value request') }

    let(:instance_value_fixtures) do
      YAML.load_file(
        "#{Rails.root}/test/files/deco/instance_values.yml"
      )['instance_values']
    end

    before do
      client.
        stubs(:get).
        with(
          'attributes/1/values/6/instances_with_value',
          instance_ids: agents.map(&:id),
          type_id:      agent_type_id
        ).
        returns(
          stub('response', body: {'instance_values' => instance_value_fixtures})
        )
    end

    it 'returns the instances with the value' do
      assert_equal(
        instance_value_fixtures.map do |instance_value|
          Zendesk::Deco::InstanceValue.from_json(instance_value)
        end,
        request.instances_with_value(instances: agents, type_id: agent_type_id)
      )
    end
  end
end
