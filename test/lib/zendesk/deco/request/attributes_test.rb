require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::Request::Attributes do
  fixtures :all

  let(:included_fields) { '' }
  let(:request) do
    Zendesk::Deco::Request::Attributes.new(client, includes: included_fields)
  end
  let(:client) { Zendesk::Deco::Client.new(accounts(:minimum)) }

  let(:attribute_fixtures) do
    YAML.load_file("#{Rails.root}/test/files/deco/attributes.yml")
  end

  describe '#index' do
    describe 'when sideloads are not requested ' do
      before do
        client.
          stubs(:get).
          with('attributes').
          returns(stub('response', body: {
                          'attributes' => attribute_fixtures['attributes']
                        }))
      end

      it 'returns the attributes associated with the account' do
        assert_equal(
          {
            attributes: attribute_fixtures['attributes'].map do |json|
              Zendesk::Deco::Attribute.from_json(json)
            end
          },
          request.index
        )
      end
    end

    describe 'when sideloads are requested' do
      let(:included_fields) { 'attribute_values,agent_count' }

      before do
        client.
          stubs(:get).
          with("attributes?include=#{included_fields}").
          returns(stub('response', body: attribute_fixtures))
      end

      it 'returns the attributes associated with the account' do
        assert_equal(
          {
            attributes: attribute_fixtures['attributes'].map do |json|
              Zendesk::Deco::Attribute.from_json(json)
            end,
            attribute_values: attribute_fixtures['attribute_values'].map do |j|
              Zendesk::Deco::AttributeValue.from_json(j)
            end,
            agent_count: attribute_fixtures['agent_count']
          },
          request.index
        )
      end
    end
  end

  describe '#incremental' do
    let(:cursor) { '1234-abcd-5678-efgh' }

    describe 'when `start_time` is provided' do
      it 'returns attributes associated with the account' do
        client.
          stubs(:get).
          with('attributes/incremental?start_time=1').
          returns(stub('response', body: {
                          'attributes' => attribute_fixtures['attributes']
                        }))

        assert_equal(
          {
            attributes: attribute_fixtures['attributes'].map do |json|
              Zendesk::Deco::Attribute.from_json(json)
            end
          },
          request.incremental(1, nil)
        )
      end
    end

    describe 'when `cursor` is provided' do
      it 'returns attributes associated with the account' do
        client.
          stubs(:get).
          with("attributes/incremental?cursor=#{cursor}").
          returns(stub('response', body: {
                          'attributes' => attribute_fixtures['attributes']
                        }))

        assert_equal(
          {
            attributes: attribute_fixtures['attributes'].map do |json|
              Zendesk::Deco::Attribute.from_json(json)
            end
          },
          request.incremental(nil, cursor)
        )
      end
    end

    describe 'when neither `start_time` nor `cursor` is provided' do
      it 'returns attributes associated with the account' do
        client.
          stubs(:get).
          with('attributes/incremental?start_time=0').
          returns(stub('response', body: {
                          'attributes' => attribute_fixtures['attributes']
                        }))

        assert_equal(
          {
            attributes: attribute_fixtures['attributes'].map do |json|
              Zendesk::Deco::Attribute.from_json(json)
            end
          },
          request.incremental(nil, nil)
        )
      end
    end
  end

  describe '#create' do
    let(:attribute_fixture) do
      attribute_fixtures['attributes'].find do |json|
        json['name'] == 'Language'
      end
    end

    before do
      client.
        stubs(:post).
        with('attributes', name: 'Language').
        returns(stub('response', body: {'attribute' => attribute_fixture}))
    end

    it 'creates an attribute' do
      assert_equal(
        Zendesk::Deco::Attribute.from_json(attribute_fixture),
        request.create(name: 'Language')
      )
    end
  end
end
