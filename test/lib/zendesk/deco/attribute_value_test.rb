require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Deco::AttributeValue do
  let(:attribute_value) do
    Zendesk::Deco::AttributeValue.new(
      '1',
      '2',
      'Japanese',
      '2017-08-25T10:43:48Z',
      '2017-08-28T14:51:29Z'
    )
  end

  describe '.from_json' do
    it 'returns a new attribute value' do
      assert_equal(
        attribute_value,
        Zendesk::Deco::AttributeValue.from_json(
          'attribute_id' => '1',
          'id'           => '2',
          'name'         => 'Japanese',
          'created_at'   => '2017-08-25T10:43:48Z',
          'updated_at'   => '2017-08-28T14:51:29Z'
        )
      )
    end
  end

  describe '#attribute_id' do
    it 'returns the attribute ID' do
      assert_equal '1', attribute_value.attribute_id
    end
  end

  describe '#id' do
    it 'returns the attribute value ID' do
      assert_equal '2', attribute_value.id
    end
  end

  describe '#name' do
    it 'returns the attribute value name' do
      assert_equal 'Japanese', attribute_value.name
    end
  end

  describe '#created_at' do
    it 'returns the created date' do
      assert_equal '2017-08-25T10:43:48Z', attribute_value.created_at
    end
  end

  describe '#updated_at' do
    it 'returns the updated date' do
      assert_equal '2017-08-28T14:51:29Z', attribute_value.updated_at
    end
  end
end
