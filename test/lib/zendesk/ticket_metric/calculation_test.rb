require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::TicketMetric::Calculation do
  let(:periods) do
    [
      Biz::TimeSegment.new(Time.new(2006, 1, 1, 9), Time.new(2006, 1, 1, 17)),
      Biz::TimeSegment.new(Time.new(2006, 1, 2, 8), Time.new(2006, 1, 2, 16))
    ]
  end

  let(:calculation) { Zendesk::TicketMetric::Calculation.new(periods) }

  describe '#elapsed' do
    describe 'when there are no periods' do
      let(:periods) { [] }

      it 'returns zero' do
        assert_equal 0, calculation.elapsed(to: Time.utc(2007))
      end
    end

    describe 'when an end time is not provided' do
      before { Timecop.freeze(Time.utc(2006, 1, 1, 10, 35)) }

      it 'uses the current time' do
        assert_equal 95, calculation.elapsed
      end
    end

    it 'returns the elapsed time in minutes' do
      assert_equal 527, calculation.elapsed(to: Time.utc(2006, 1, 2, 8, 47))
    end
  end

  describe '#time' do
    describe 'when there are no periods' do
      let(:periods) { [] }

      it 'returns nil' do
        assert_nil calculation.time(after: 10)
      end
    end

    describe 'when the duration is negative' do
      let(:duration) { -1 }

      it 'returns the start time of the first period' do
        assert_equal periods.first.start_time, calculation.time(after: duration)
      end
    end

    describe 'when the duration is zero' do
      let(:duration) { 0 }

      it 'returns the start time of the first period' do
        assert_equal periods.first.start_time, calculation.time(after: duration)
      end
    end

    describe 'when the duration matches the length of a period' do
      let(:duration) { periods.first.duration.in_minutes }

      it 'returns the end time of that period' do
        assert_equal periods.first.end_time, calculation.time(after: duration)
      end
    end

    it 'returns the time after a specified number of minutes' do
      assert_equal Time.utc(2006, 1, 2, 8, 47), calculation.time(after: 527)
    end
  end
end
