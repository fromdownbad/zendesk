require_relative '../../../support/test_helper'
require_relative '../../../support/ticket_metric_helper'

SingleCov.covered!

describe Zendesk::TicketMetric::Sla do
  include TicketMetricHelper

  fixtures :accounts,
    :tickets

  let(:ticket)  { tickets(:minimum_1) }
  let(:account) { ticket.account }
  let(:metric)  { :requester_wait_time }

  let(:context) { Zendesk::TicketMetric::Context.new(ticket, metric) }

  let(:state) { Zendesk::TicketMetric::Sla.new(ticket, context) }

  let(:policy_metric_1) do
    new_sla_policy_metric(
      policy:    new_sla_policy,
      metric_id: Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
      target:    10
    )
  end
  let(:policy_metric_2) do
    new_sla_policy_metric(
      policy:    new_sla_policy(position: 2, tags: 'match_sla_2'),
      metric_id: Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
      target:    20
    )
  end

  describe '#changed?' do
    describe 'when an SLA has not been applied to the ticket' do
      describe 'and there is not a policy assigned to the ticket' do
        it 'returns false' do
          refute state.changed?
        end
      end

      describe 'and there is a policy assigned to the ticket' do
        before { assign_policy(policy: policy_metric_1.policy) }

        it 'returns true' do
          assert state.changed?
        end
      end
    end

    describe 'when an SLA has previously been applied to the ticket' do
      before do
        assign_policy(policy: policy_metric_1.policy)

        TicketMetric::ApplySla.create(
          account: ticket.account,
          ticket:  ticket,
          metric:  'requester_wait_time',
          time:    ticket.updated_at
        )
      end

      describe 'and there is not a policy assigned to the ticket' do
        before { assign_policy(policy: nil) }

        it 'returns true' do
          assert state.changed?
        end
      end

      describe 'and there is a policy assigned to the ticket' do
        describe 'and the policy metrics are different' do
          before { assign_policy(policy: policy_metric_2.policy) }

          it 'returns true' do
            assert state.changed?
          end
        end

        describe 'and the policy metrics are the same' do
          before { assign_policy(policy: policy_metric_1.policy) }

          it 'returns false' do
            refute state.changed?
          end
        end
      end
    end
  end

  describe '#policy_metric' do
    describe 'when the ticket is not associated with a policy' do
      it 'is not present' do
        refute state.policy_metric.present?
      end
    end

    describe 'when the ticket is associated with a policy' do
      before { assign_policy(policy: policy_metric_1.policy) }

      describe 'and the metric has a corresponding policy metric' do
        it 'returns the policy metric' do
          assert_equal policy_metric_1, state.policy_metric
        end
      end

      describe 'and the metric does not have a corresponding policy metric' do
        before { ticket.priority_id = PriorityType.URGENT }

        it 'is not present' do
          refute state.policy_metric.present?
        end
      end
    end
  end

  describe '#last_policy_metric' do
    describe 'when no SLA has been applied' do
      before { ticket.metric_events.apply_sla.destroy_all }

      it 'is not present' do
        refute state.last_policy_metric.present?
      end
    end

    describe 'when an SLA has been applied in the past' do
      before do
        ticket.metric_events.requester_wait_time.apply_sla.create(
          account: ticket.account,
          time:    ticket.updated_at
        )
        ticket.metric_events.apply_sla.last.create_metric_event_policy_metric(
          account:       ticket.account,
          policy_metric: policy_metric_1
        )
      end

      it 'returns the policy metric' do
        assert_equal policy_metric_1, state.last_policy_metric
      end
    end

    describe 'when more than one SLA has been applied in the past' do
      before do
        ticket.metric_events.requester_wait_time.apply_sla.create(
          account: ticket.account,
          time:    ticket.updated_at - 1.minute
        )
        ticket.metric_events.apply_sla.last.create_metric_event_policy_metric(
          account:       ticket.account,
          policy_metric: policy_metric_1
        )

        ticket.metric_events.requester_wait_time.apply_sla.create(
          account: ticket.account,
          time:    ticket.updated_at
        )
        ticket.metric_events.apply_sla.last.create_metric_event_policy_metric(
          account:       ticket.account,
          policy_metric: policy_metric_2
        )
      end

      it 'returns the last policy metric' do
        assert_equal policy_metric_2, state.last_policy_metric
      end
    end
  end

  describe '#breachable?' do
    describe 'when there is no policy metric' do
      it 'returns false' do
        refute state.breachable?
      end
    end

    describe 'when there is a policy metric' do
      before { assign_policy(policy: policy_metric_1.policy) }

      describe "and there are no 'apply_sla' events" do
        it 'returns false' do
          refute state.breachable?
        end
      end

      describe "and there are 'apply_sla' events" do
        before do
          ticket.metric_events.requester_wait_time.apply_sla.create(
            account: ticket.account,
            time:    ticket.updated_at
          )
        end

        describe "and there are no 'breach' events" do
          it 'returns true' do
            assert state.breachable?
          end
        end

        describe 'and there is a breach event' do
          before do
            ticket.metric_events.requester_wait_time.breach.create(
              account: ticket.account,
              time:    breach_time
            )
          end

          describe "and the breach time is after the 'apply_sla' event time" do
            let(:breach_time) { ticket.updated_at + 1.minute }

            it 'returns false' do
              refute state.breachable?
            end
          end

          describe "and the breach time is before the 'apply_sla' event time" do
            let(:breach_time) { ticket.updated_at - 1.minute }

            it 'returns true' do
              assert state.breachable?
            end
          end
        end
      end
    end
  end

  describe '#breach_time' do
    before do
      ticket.events.delete_all

      assign_policy(policy: policy_metric_1.policy)

      ticket.metric_events.requester_wait_time.activate.create(
        account: ticket.account,
        time:    ticket.created_at
      )
    end

    describe "when the breach time is after the ticket's last update time" do
      before do
        ticket.stubs(:updated_at).returns(ticket.created_at + 9.minutes)
      end

      it 'returns the breach time' do
        assert_equal ticket.created_at + 10.minutes, state.breach_time
      end
    end

    describe "when the breach time is before the ticket's last update time" do
      before do
        ticket.stubs(:updated_at).returns(ticket.created_at + 11.minutes)
      end

      it "returns the ticket's 'updated_at' time" do
        assert_equal ticket.updated_at, state.breach_time
      end
    end
  end

  describe '#supported?' do
    describe 'when the metric is supported' do
      let(:metric) { :reply_time }

      it 'returns true' do
        assert state.supported?
      end
    end

    describe 'when the metric is not supported' do
      let(:metric) { :resolution_time }

      it 'returns false' do
        refute state.supported?
      end
    end

    describe 'when the metric does not exist' do
      let(:metric) { :invalid_metric }

      it 'returns false' do
        refute state.supported?
      end
    end
  end
end
