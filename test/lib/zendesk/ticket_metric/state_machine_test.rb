require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 28

describe Zendesk::TicketMetric::StateMachine do
  describe '.all' do
    it 'returns the state machines' do
      assert_equal(
        [
          Zendesk::TicketMetric::StateMachine::AgentWorkTime,
          Zendesk::TicketMetric::StateMachine::PausableUpdateTime,
          Zendesk::TicketMetric::StateMachine::PeriodicUpdateTime,
          Zendesk::TicketMetric::StateMachine::ReplyTime,
          Zendesk::TicketMetric::StateMachine::RequesterWaitTime,
          Zendesk::TicketMetric::StateMachine::ResolutionTime
        ],
        Zendesk::TicketMetric::StateMachine.all
      )
    end
  end
end
