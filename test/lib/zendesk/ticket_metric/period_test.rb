require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::TicketMetric::Period do
  fixtures :tickets

  let(:state) { :activated }

  let(:period) do
    Zendesk::TicketMetric::Period.new(Time.utc(2006), Time.utc(2007), state)
  end

  describe '#active?' do
    describe "when the state is 'activated'" do
      let(:state) { :activated }

      it 'returns true' do
        assert period.active?
      end
    end

    describe "when the state is not 'activated'" do
      let(:state) { :fulfilled }

      it 'returns false' do
        refute period.active?
      end
    end
  end

  describe '#to_time_segment' do
    it 'returns a matching time segment' do
      assert_equal(
        [Time.utc(2006), Time.utc(2007)],
        period.to_time_segment.endpoints
      )
    end
  end
end
