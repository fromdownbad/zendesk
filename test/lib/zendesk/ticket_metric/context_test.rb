require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::TicketMetric::Context do
  fixtures :tickets

  let(:ticket) { tickets(:minimum_1) }
  let(:metric) { :reply_time }

  let(:context) { Zendesk::TicketMetric::Context.new(ticket, metric) }

  describe '#ticket' do
    it 'returns the ticket' do
      assert_equal ticket, context.ticket
    end
  end

  describe '#metric' do
    it 'returns the metric' do
      assert_equal metric, context.metric
    end
  end

  describe '#new_ticket?' do
    describe 'when the ticket is new' do
      before { ticket.stubs(id_changed?: true) }

      it 'returns true' do
        assert context.new_ticket?
      end
    end

    describe 'when the ticket is not new' do
      before { ticket.stubs(id_changed?: false) }

      it 'returns false' do
        refute context.new_ticket?
      end
    end
  end

  describe '#status_changed?' do
    describe 'when the ticket is new' do
      before { ticket.stubs(id_changed?: true) }

      it 'returns true' do
        assert context.status_changed?
      end
    end

    describe 'when the ticket is not new' do
      before { ticket.stubs(id_changed?: false) }

      describe 'and the ticket status has changed' do
        before { ticket.stubs(status_id_changed?: true) }

        it 'returns true' do
          assert context.status_changed?
        end
      end

      describe 'and the ticket status has not changed' do
        before { ticket.stubs(status_id_changed?: false) }

        it 'returns false' do
          refute context.status_changed?
        end
      end
    end
  end

  describe '#measured?' do
    describe 'when the metric was present at launch' do
      let(:metric) { :requester_wait_time }

      describe "and there is a 'measure' event" do
        before do
          TicketMetric::Measure.create(
            account: ticket.account,
            ticket:  ticket,
            metric:  'requester_wait_time',
            time:    ticket.created_at
          )
        end

        it 'returns true' do
          assert context.measured?
        end
      end

      describe "and there is not a 'measure' event" do
        before { ticket.metric_events.measure.destroy_all }

        it 'returns true' do
          assert context.measured?
        end
      end
    end

    describe 'when the metric was not present at launch' do
      let(:metric) { :periodic_update_time }

      describe "and there is a 'measure' event" do
        before do
          TicketMetric::Measure.create(
            account: ticket.account,
            ticket:  ticket,
            metric:  'periodic_update_time',
            time:    ticket.created_at
          )
        end

        it 'returns true' do
          assert context.measured?
        end
      end

      describe "and there is not a 'measure' event" do
        before { ticket.metric_events.measure.destroy_all }

        it 'returns false' do
          refute context.measured?
        end
      end
    end
  end

  describe '.latest_instance_id' do
    describe 'when there are no events' do
      it 'returns one' do
        assert_equal 1, context.latest_instance_id
      end
    end

    describe 'when the metric is measured with no events' do
      before do
        TicketMetric::Measure.create(
          account: ticket.account,
          ticket:  ticket,
          metric:  'reply_time',
          time:    ticket.created_at
        )
      end

      it 'returns one' do
        assert_equal 1, context.latest_instance_id
      end
    end

    describe 'when there are events for one instance' do
      before do
        context.events.activate.create(
          account:     ticket.account,
          instance_id: 1,
          time:        Time.new(2015)
        )
      end

      it 'returns one' do
        assert_equal 1, context.latest_instance_id
      end
    end

    describe 'when there are events for more than one instance' do
      before do
        context.events.activate.create(
          account:     ticket.account,
          instance_id: 1,
          time:        Time.new(2015)
        )
        context.events.fulfill.create(
          account:     ticket.account,
          instance_id: 1,
          time:        Time.new(2016)
        )
        context.events.activate.create(
          account:     ticket.account,
          instance_id: 2,
          time:        Time.new(2017)
        )
      end

      it 'returns the latest instance ID' do
        assert_equal 2, context.latest_instance_id
      end
    end
  end

  describe '.next_instance_id' do
    describe 'when the metric is recurring' do
      let(:metric) { :periodic_update_time }

      describe 'and there are no events' do
        before { context.events.destroy_all }

        it 'returns one' do
          assert_equal 1, context.next_instance_id
        end
      end

      describe 'and there are events for one instance' do
        before do
          context.events.activate.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        it 'returns two' do
          assert_equal 2, context.next_instance_id
        end
      end

      describe 'and there are events for more than one instance' do
        before do
          context.events.activate.create(
            account:     ticket.account,
            instance_id: 1,
            time:        Time.new(2015)
          )
          context.events.activate.create(
            account:     ticket.account,
            instance_id: 2,
            time:        Time.new(2015)
          )
          context.events.activate.create(
            account:     ticket.account,
            instance_id: 3,
            time:        Time.new(2015)
          )
        end

        it "returns the next 'instance_id'" do
          assert_equal 4, context.next_instance_id
        end
      end
    end

    describe 'when the metric is not recurring' do
      let(:metric) { :requester_wait_time }

      describe 'and there are no events' do
        before { context.events.destroy_all }

        it 'returns one' do
          assert_equal 1, context.next_instance_id
        end
      end

      describe 'and there are events for one instance' do
        before do
          context.events.activate.create(
            account: ticket.account,
            time:    Time.new(2015)
          )
        end

        it 'returns one' do
          assert_equal 1, context.next_instance_id
        end
      end

      describe 'and there are events for more than one instance' do
        before do
          context.events.activate.create(
            account:     ticket.account,
            instance_id: 1,
            time:        Time.new(2015)
          )
          context.events.activate.create(
            account:     ticket.account,
            instance_id: 2,
            time:        Time.new(2015)
          )
          context.events.activate.create(
            account:     ticket.account,
            instance_id: 3,
            time:        Time.new(2015)
          )
        end

        it 'returns one' do
          assert_equal 1, context.next_instance_id
        end
      end
    end
  end

  describe '#events' do
    before do
      ticket.metric_events.agent_work_time.activate.create(
        account: ticket.account,
        time:    ticket.created_at
      )
      ticket.metric_events.reply_time.activate.create(
        account: ticket.account,
        time:    ticket.created_at
      )
      ticket.metric_events.reply_time.fulfill.create(
        account: ticket.account,
        time:    ticket.created_at + 1.minute
      )
    end

    it 'returns the events for the specified metric' do
      assert(
        context.events.any? &&
          context.events.all? { |event| event.metric?(:reply_time) }
      )
    end
  end

  describe '#latest_events' do
    before do
      ticket.metric_events.reply_time.activate.create(
        account:     ticket.account,
        instance_id: 1,
        time:        ticket.created_at
      )
      ticket.metric_events.reply_time.fulfill.create(
        account:     ticket.account,
        instance_id: 1,
        time:        ticket.created_at
      )
      ticket.metric_events.reply_time.activate.create(
        account:     ticket.account,
        instance_id: 2,
        time:        ticket.created_at
      )
      ticket.metric_events.reply_time.pause.create(
        account:     ticket.account,
        instance_id: 2,
        time:        ticket.created_at
      )
      ticket.metric_events.reply_time.activate.create(
        account:     ticket.account,
        instance_id: 2,
        time:        ticket.created_at
      )
    end

    it 'returns events for the latest instance' do
      assert(
        context.latest_events.count(:all) == 3 &&
          context.latest_events.all? { |event| event.instance_id == 2 }
      )
    end
  end

  describe '#sla' do
    it "returns an 'Sla' object" do
      assert_equal Zendesk::TicketMetric::Sla, context.sla.class
    end
  end
end
