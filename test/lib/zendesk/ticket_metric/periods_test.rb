require_relative '../../../support/test_helper'
require_relative '../../../support/business_hours_test_helper'

SingleCov.covered!

describe Zendesk::TicketMetric::Periods do
  include BusinessHoursTestHelper

  fixtures :tickets

  let(:ticket)         { tickets(:minimum_1) }
  let(:business_hours) { false }

  let(:periods) do
    Zendesk::TicketMetric::Periods.new(
      ticket:         ticket,
      metric_events:  ticket.metric_events,
      business_hours: business_hours
    )
  end

  before do
    ticket.metric_events.requester_wait_time.activate.create(
      account: ticket.account,
      time:    Time.utc(2006, 1, 2, 8, 13)
    )
    ticket.metric_events.requester_wait_time.pause.create(
      account: ticket.account,
      time:    Time.utc(2006, 1, 2, 10, 17)
    )
    ticket.metric_events.requester_wait_time.activate.create(
      account: ticket.account,
      time:    Time.utc(2006, 1, 2, 16, 5)
    )
    ticket.metric_events.requester_wait_time.fulfill.create(
      account: ticket.account,
      time:    Time.utc(2006, 1, 3, 11, 23)
    )
  end

  describe 'when calendar periods are specified' do
    let(:business_hours) { false }

    it 'returns calendar periods' do
      assert_equal(
        [
          Biz::TimeSegment.new(
            Time.utc(2006, 1, 2, 8, 13),
            Time.utc(2006, 1, 2, 10, 17)
          ),
          Biz::TimeSegment.new(
            Time.utc(2006, 1, 2, 16, 5),
            Time.utc(2006, 1, 3, 11, 23)
          )
        ],
        periods.to_a
      )
    end
  end

  describe 'when business periods are specified' do
    let(:business_hours) { true }
    let(:schedule) do
      ticket.account.schedules.create(
        name:      'Schedule',
        time_zone: ticket.account.time_zone
      )
    end

    before do
      add_schedule_intervals(schedule,
        [:mon, '09:00', '17:00'],
        [:tue, '10:00', '18:00'])

      set_time_zone(schedule, 'London')

      ticket.create_ticket_schedule(account: ticket.account, schedule: schedule)
    end

    it 'returns business periods' do
      assert_equal(
        [
          Biz::TimeSegment.new(
            Time.utc(2006, 1, 2, 9),
            Time.utc(2006, 1, 2, 10, 17)
          ),
          Biz::TimeSegment.new(
            Time.utc(2006, 1, 2, 16, 5),
            Time.utc(2006, 1, 2, 17)
          ),
          Biz::TimeSegment.new(
            Time.utc(2006, 1, 3, 10),
            Time.utc(2006, 1, 3, 11, 23)
          )
        ],
        periods.to_a
      )
    end

    describe 'and there are no calendar periods' do
      before { ticket.metric_events.destroy_all }

      it 'returns no business periods' do
        assert_equal [], periods.to_a
      end
    end
  end

  describe '#calculation' do
    it "returns an 'TicketMetric::Calculation' object" do
      assert_equal Zendesk::TicketMetric::Calculation, periods.calculation.class
    end
  end
end
