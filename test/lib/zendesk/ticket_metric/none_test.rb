require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::TicketMetric::None do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  let(:sla_metric) { Zendesk::Sla::TicketMetric::NextReplyTime }

  let(:none_metric) { Zendesk::TicketMetric::None.new(sla_metric) }

  describe '#equivalent?' do
    let(:policy_metric) do
      Sla::PolicyMetric.new do |pm|
        pm.sla_policy_id  = none_metric.sla_policy_id
        pm.metric_id      = none_metric.metric_id
        pm.target         = none_metric.target
        pm.business_hours = none_metric.business_hours
      end
    end

    describe 'when the policy metric is equivalent' do
      it 'returns true' do
        assert none_metric.equivalent?(none_metric)
      end
    end

    describe 'when the policy metric has a different `sla_policy_id`' do
      before { policy_metric.sla_policy_id = 1 }

      it 'returns false' do
        refute none_metric.equivalent?(policy_metric)
      end
    end

    describe 'when the policy metric has a different `metric_id`' do
      before do
        policy_metric.metric_id =
          Zendesk::Sla::TicketMetric::RequesterWaitTime.id
      end

      it 'returns false' do
        refute none_metric.equivalent?(policy_metric)
      end
    end

    describe 'when the policy metric has a different `target`' do
      before { policy_metric.target = 10 }

      it 'returns false' do
        refute none_metric.equivalent?(policy_metric)
      end
    end

    describe 'when the policy metric has a different `business_hours`' do
      before { policy_metric.business_hours = true }

      it 'returns false' do
        refute none_metric.equivalent?(policy_metric)
      end
    end
  end

  describe '#present?' do
    it 'returns false' do
      refute none_metric.present?
    end
  end

  describe '#metric_id' do
    it 'returns the corresponding `metric_id`' do
      assert_equal sla_metric.id, none_metric.metric_id
    end
  end

  describe '#business_hours' do
    it 'returns nil' do
      assert_nil none_metric.business_hours
    end
  end

  describe '#target' do
    it 'returns nil' do
      assert_nil none_metric.target
    end
  end

  describe '#to_audit' do
    it 'returns nil' do
      assert_nil none_metric.to_audit
    end
  end

  describe '#policy' do
    it 'returns nil' do
      assert_nil none_metric.policy
    end
  end

  describe '#sla_policy_id' do
    it 'returns nil' do
      assert_nil none_metric.sla_policy_id
    end
  end
end
