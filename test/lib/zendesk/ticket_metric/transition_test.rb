require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 1

describe Zendesk::TicketMetric::Transition do
  fixtures :accounts,
    :tickets

  let(:ticket) { tickets(:minimum_1) }

  let(:transition) do
    Zendesk::TicketMetric::Transition.new(
      ticket,
      Zendesk::TicketMetric::Context.
        new(ticket, :periodic_update_time).
        latest_events
    )
  end

  before do
    ticket.metric_events.periodic_update_time.activate.create(
      account:     ticket.account,
      instance_id: 1,
      time:        ticket.created_at
    )
    ticket.metric_events.periodic_update_time.pause.create(
      account:     ticket.account,
      instance_id: 1,
      time:        ticket.created_at + 10.minutes
    )
    ticket.metric_events.periodic_update_time.activate.create(
      account:     ticket.account,
      instance_id: 1,
      time:        ticket.created_at + 30.minutes
    )
    ticket.metric_events.periodic_update_time.fulfill.create(
      account:     ticket.account,
      instance_id: 1,
      time:        ticket.created_at + 60.minutes
    )
  end

  describe '#last' do
    it 'returns the last transition event' do
      assert_equal 'fulfill', transition.last.type
    end

    describe 'when there are no previous transition events' do
      before { ticket.metric_events.destroy_all }

      it "returns a transition in the 'deactivated' state" do
        assert_equal :deactivated, transition.last.to_state
      end

      describe "and the ticket does not have a 'created_at' time" do
        let(:current_time) { Time.utc(2006) }

        before { ticket.stubs(:created_at).returns(nil) }

        it 'returns a transition that occurred at the current time' do
          assert_equal(
            current_time,
            Timecop.freeze(current_time) { transition.last.time }
          )
        end
      end
    end

    describe 'when events from different instances share the same time' do
      before do
        ticket.metric_events.periodic_update_time.activate.create(
          account:     ticket.account,
          instance_id: 2,
          time:        ticket.created_at + 60.minutes
        )
      end

      it 'returns the state of the latest instance' do
        assert_equal :activated, transition.last.to_state
      end
    end
  end

  describe '#periods' do
    it 'returns the corresponding transition periods' do
      assert_equal(
        [
          Zendesk::TicketMetric::Period.new(
            ticket.created_at,
            ticket.created_at + 10.minutes,
            :activated
          ),
          Zendesk::TicketMetric::Period.new(
            ticket.created_at + 10.minutes,
            ticket.created_at + 30.minutes,
            :paused
          ),
          Zendesk::TicketMetric::Period.new(
            ticket.created_at + 30.minutes,
            ticket.created_at + 60.minutes,
            :activated
          ),
          Zendesk::TicketMetric::Period.new(
            ticket.created_at + 60.minutes,
            Biz::Time.heat_death,
            :deactivated
          )
        ],
        transition.periods
      )
    end
  end
end
