require_relative '../../../../support/test_helper'
require_relative '../../../../support/business_hours_test_helper'
require_relative '../../../../support/ticket_metric_helper'

SingleCov.covered!

describe Zendesk::TicketMetric::StateMachine::RequesterWaitTime do
  include BusinessHoursTestHelper
  include TicketMetricHelper

  fixtures :accounts,
    :tickets,
    :users

  let(:ticket)  { tickets(:minimum_1) }
  let(:agent)   { users(:minimum_agent) }
  let(:account) { ticket.account }

  let(:metric_events) { ticket.metric_events.requester_wait_time }

  let(:policy) { new_sla_policy(title: 'requester wait time') }

  let(:state_machine) do
    Zendesk::TicketMetric::StateMachine::RequesterWaitTime.build(ticket)
  end

  before do
    ticket.will_be_saved_by(agent)

    new_sla_policy_metric(
      policy:    policy,
      metric_id: Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
      target:    20
    )
  end

  describe '#advance' do
    describe "when in the 'deactivated' state" do
      it "starts in the 'deactivated' state" do
        assert_equal :deactivated, state_machine.current
      end

      describe 'and the ticket is working and not pending' do
        before do
          ticket.status_id = StatusType.OPEN

          metric_events.breach.create(
            account: ticket.account,
            time:    ticket.updated_at - 1.minute
          )
        end

        describe 'and the state machine advances' do
          before { state_machine.advance }

          should_change("'activate' events", by: 1) do
            metric_events.activate.count(:all)
          end

          it "assigns the ticket's 'updated_at' time to the event" do
            assert_equal ticket.updated_at, metric_events.activate.last.time
          end

          it "assigns the proper 'instance_id' to the event" do
            assert_equal 1, metric_events.activate.last.instance_id
          end

          it "enters the 'resumed' state" do
            assert_equal :resumed, state_machine.current
          end
        end

        describe 'and the SLA has changed' do
          let(:change_audit) do
            SlaTargetChange.where(ticket_id: ticket.id).last
          end

          before do
            assign_policy

            state_machine.advance
          end

          should_change("'apply sla' events", by: 1) do
            metric_events.apply_sla.count(:all)
          end

          it 'assigns the proper policy metric' do
            assert_equal(
              Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
              metric_events.apply_sla.last.policy_metric.metric.id
            )
          end

          it "assigns the ticket's 'updated_at' time to the event" do
            assert_equal(
              ticket.updated_at,
              metric_events.apply_sla.last.time
            )
          end

          it "audits 'initial_target' properly" do
            assert_nil change_audit.initial_target
          end

          it "audits 'final_target' properly" do
            assert_equal(
              {minutes: 20, in_business_hours: false},
              change_audit.final_target
            )
          end

          it "audits 'metric_id' properly" do
            assert_equal(
              metric_events.apply_sla.last.policy_metric.metric_id,
              change_audit.metric_id.to_i
            )
          end

          it "audits 'current_sla_policy' properly" do
            assert_equal policy.title, change_audit.current_sla_policy
          end

          should_not_change("past 'breach' events") do
            metric_events.breach.before(ticket.updated_at).count(:all)
          end

          should_change("future 'breach' events", by: 1) do
            metric_events.breach.after(ticket.updated_at).count(:all)
          end

          should_change('change audits', by: 1) do
            SlaTargetChange.where(ticket_id: ticket.id).count
          end
        end

        describe 'and the SLA has not changed' do
          before do
            assign_policy(policy: nil)

            state_machine.advance
          end

          should_not_change("'apply sla' events") do
            metric_events.apply_sla.count(:all)
          end

          should_not_change("'breach' events") do
            metric_events.breach.count(:all)
          end

          should_not_change('change audits') do
            SlaTargetChange.where(ticket_id: ticket.id).count
          end
        end
      end

      describe 'and the ticket has been working and not pending' do
        before { ticket.update_column(:status_id, StatusType.OPEN) }

        describe 'and it is advanced' do
          before { state_machine.advance }

          should_not_change('metric events') { metric_events.count(:all) }

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end
      end

      describe 'and the ticket is on hold' do
        before do
          ticket.status_id = StatusType.HOLD

          state_machine.advance
        end

        should_change("'activate' events", by: 1) do
          metric_events.activate.count(:all)
        end

        it "assigns the ticket's 'updated_at' time to the event" do
          assert_equal ticket.updated_at, metric_events.activate.last.time
        end

        it "assigns the proper 'instance_id' to the event" do
          assert_equal 1, metric_events.activate.last.instance_id
        end

        it "enters the 'resumed' state" do
          assert_equal :resumed, state_machine.current
        end
      end

      describe 'and the ticket is pending' do
        before do
          ticket.status_id = StatusType.PENDING

          state_machine.advance
        end

        should_change("'pause' events", by: 1) do
          metric_events.pause.count(:all)
        end

        it "assigns the ticket's 'updated_at' time to the event" do
          assert_equal ticket.updated_at, metric_events.pause.last.time
        end

        it "assigns the proper 'instance_id' to the event" do
          assert_equal 1, metric_events.pause.last.instance_id
        end

        it "enters the 'paused' state" do
          assert_equal :paused, state_machine.current
        end

        describe 'and the SLA has changed' do
          before do
            assign_policy

            state_machine.advance
          end

          should_change("'apply sla' events", by: 1) do
            metric_events.apply_sla.count(:all)
          end

          it 'assigns the proper policy metric' do
            assert_equal(
              Zendesk::Sla::TicketMetric::RequesterWaitTime.id,
              metric_events.apply_sla.last.policy_metric.metric.id
            )
          end

          it "assigns the ticket's 'updated_at' time to the event" do
            assert_equal(
              ticket.updated_at,
              metric_events.apply_sla.last.time
            )
          end

          should_not_change("'breach' events") do
            metric_events.breach.count(:all)
          end
        end

        describe 'and the SLA has not changed' do
          before do
            assign_policy(policy: nil)

            state_machine.advance
          end

          should_not_change("'apply sla' events") do
            metric_events.apply_sla.count(:all)
          end

          should_not_change("'breach' events") do
            metric_events.breach.count(:all)
          end
        end
      end

      describe 'and the ticket has been pending' do
        before { ticket.update_column(:status_id, StatusType.PENDING) }

        describe 'and it is advanced' do
          before { state_machine.advance }

          should_not_change('metric events') { metric_events.count(:all) }

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end
      end

      describe 'and the ticket is finished' do
        before do
          ticket.status_id = StatusType.SOLVED

          state_machine.advance
        end

        should_not_change("'fulfill' events") do
          metric_events.fulfill.count(:all)
        end

        should_not_change("'update status' events") do
          metric_events.update_status.count(:all)
        end

        it "remains in the 'deactivated' state" do
          assert_equal :deactivated, state_machine.current
        end
      end
    end

    describe "when in the 'activated' state" do
      before do
        metric_events.activate.create(
          account: ticket.account,
          time:    ticket.created_at
        )
      end

      it "starts in the 'activated' state" do
        assert_equal :activated, state_machine.current
      end

      describe 'and the ticket is working and not pending' do
        before do
          ticket.status_id = StatusType.OPEN

          state_machine.advance
        end

        should_not_change("'activate' events") do
          metric_events.activate.count(:all)
        end

        it "remains in the 'activated' state" do
          assert_equal :activated, state_machine.current
        end
      end

      describe 'and the ticket is pending' do
        before do
          ticket.status_id = StatusType.PENDING

          state_machine.advance
        end

        should_change("'pause' events", by: 1) do
          metric_events.pause.count(:all)
        end

        it "enters the 'paused' state" do
          assert_equal :paused, state_machine.current
        end
      end

      describe 'and the ticket is finished' do
        before do
          ticket.status_id = StatusType.SOLVED

          metric_events.breach.create(
            account: ticket.account,
            time:    ticket.updated_at - 1.minute
          )
          metric_events.breach.create(
            account: ticket.account,
            time:    ticket.updated_at + 1.minute
          )
        end

        describe 'and the state machine advances' do
          before { state_machine.advance }

          should_change("'fulfill' events", by: 1) do
            metric_events.fulfill.count(:all)
          end

          should_change("'update_status' events", by: 1) do
            metric_events.update_status.count(:all)
          end

          should_not_change("past 'breach' events") do
            metric_events.breach.before(ticket.updated_at).count(:all)
          end

          should_change("future 'breach' events", from: 1, to: 0) do
            metric_events.breach.after(ticket.updated_at).count(:all)
          end

          it "enters the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end
      end

      describe 'and the ticket has been finished' do
        before { ticket.update_column(:status_id, StatusType.SOLVED) }

        describe 'and it is advanced' do
          before { state_machine.advance }

          should_not_change('metric events') { metric_events.count(:all) }

          it "remains in the 'activated' state" do
            assert_equal :activated, state_machine.current
          end
        end
      end
    end

    describe "when in the 'paused' state" do
      before do
        metric_events.pause.create(
          account: ticket.account,
          time:    ticket.created_at
        )
      end

      it "starts in the 'paused' state" do
        assert_equal :paused, state_machine.current
      end

      describe 'and the ticket is working and not pending' do
        before do
          ticket.status_id = StatusType.OPEN

          state_machine.advance
        end

        should_change("'activate' events", by: 1) do
          metric_events.activate.count(:all)
        end

        it "enters the 'resumed' state" do
          assert_equal :resumed, state_machine.current
        end
      end

      describe 'and the ticket is pending' do
        before do
          ticket.status_id = StatusType.PENDING

          state_machine.advance
        end

        should_not_change("'pause' events") { metric_events.pause.count(:all) }

        it "remains in the 'paused' state" do
          assert_equal :paused, state_machine.current
        end
      end

      describe 'and the ticket is finished' do
        before do
          ticket.status_id = StatusType.SOLVED

          state_machine.advance
        end

        should_change("'fulfill' events", by: 1) do
          metric_events.fulfill.count(:all)
        end

        should_change("'update_status' events", by: 1) do
          metric_events.update_status.count(:all)
        end

        it "enters the 'deactivated' state" do
          assert_equal :deactivated, state_machine.current
        end
      end
    end

    describe 'when the ticket is new' do
      before do
        ticket.stubs(id_changed?: true)

        state_machine.advance
      end

      should_change("'measure' events", by: 1) do
        metric_events.measure.count(:all)
      end
    end

    describe 'when the ticket is not new' do
      before do
        ticket.stubs(id_changed?: false)

        state_machine.advance
      end

      should_not_change("'measure' events") do
        metric_events.measure.count(:all)
      end
    end
  end
end
