require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::TicketMetric::StateMachine::ResolutionTime do
  fixtures :accounts,
    :tickets

  let(:ticket)  { tickets(:minimum_1) }
  let(:account) { ticket.account }

  let(:metric_events) { ticket.metric_events.resolution_time }

  let(:state_machine) do
    Zendesk::TicketMetric::StateMachine::ResolutionTime.build(ticket)
  end

  describe '#advance' do
    before do
      TicketMetric::Measure.create(
        account: ticket.account,
        ticket:  ticket,
        metric:  'resolution_time',
        time:    ticket.created_at
      )
    end

    describe "when in the 'deactivated' state" do
      it "starts in the 'deactivated' state" do
        assert_equal :deactivated, state_machine.current
      end

      describe 'and the ticket is working' do
        before do
          ticket.status_id = StatusType.OPEN

          state_machine.advance
        end

        should_change("'activate' events", by: 1) do
          metric_events.activate.count(:all)
        end

        it "assigns the update time to the 'activate' event" do
          assert_equal ticket.updated_at, metric_events.activate.last.time
        end

        it "assigns the proper 'instance_id' to the 'activate' event" do
          assert_equal 1, metric_events.activate.last.instance_id
        end

        it "enters the 'resumed' state" do
          assert_equal :resumed, state_machine.current
        end
      end

      describe 'and the ticket has been working' do
        before do
          ticket.update_column(:status_id, StatusType.OPEN)

          state_machine.advance
        end

        should_not_change('metric events') { metric_events.count(:all) }

        it "remains in the 'deactivated' state" do
          assert_equal :deactivated, state_machine.current
        end
      end
    end

    describe "when in the 'activated' state" do
      before do
        metric_events.activate.create(
          account: ticket.account,
          time:    ticket.created_at
        )
      end

      it "starts in the 'activated' state" do
        assert_equal :activated, state_machine.current
      end

      describe 'and the ticket is finished' do
        before do
          ticket.status_id = StatusType.SOLVED

          state_machine.advance
        end

        should_change("'fulfill' events", by: 1) do
          metric_events.fulfill.count(:all)
        end

        should_change("'update_status' events", by: 1) do
          metric_events.update_status.count(:all)
        end

        it "enters the 'deactivated' state" do
          assert_equal :deactivated, state_machine.current
        end
      end

      describe 'and the ticket has been finished' do
        before do
          ticket.update_column(:status_id, StatusType.SOLVED)

          state_machine.advance
        end

        should_not_change('metric events') { metric_events.count(:all) }

        it "remains in the 'activated' state" do
          assert_equal :activated, state_machine.current
        end
      end
    end
  end
end
