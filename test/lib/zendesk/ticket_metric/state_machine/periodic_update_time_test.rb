require_relative '../../../../support/test_helper'
require_relative '../../../../support/ticket_metric_helper'

SingleCov.covered!

describe Zendesk::TicketMetric::StateMachine::PeriodicUpdateTime do
  include TicketMetricHelper

  fixtures :accounts,
    :tickets,
    :users

  let(:ticket)  { tickets(:minimum_1) }
  let(:account) { ticket.account }
  let(:policy)  { new_sla_policy }

  let(:agent)    { users(:minimum_agent) }
  let(:end_user) { users(:minimum_end_user) }

  let(:foreign_agent) { end_user.tap { |user| user.foreign_agent = true } }

  let(:metric_events) { ticket.metric_events.periodic_update_time }

  let(:state_machine) do
    Zendesk::TicketMetric::StateMachine::PeriodicUpdateTime.build(ticket)
  end

  before do
    ticket.will_be_saved_by(agent)

    new_sla_policy_metric(
      policy:    policy,
      metric_id: Zendesk::Sla::TicketMetric::PeriodicUpdateTime.id,
      target:    20
    )
  end

  describe '#advance' do
    before do
      TicketMetric::Measure.create(
        account: ticket.account,
        ticket:  ticket,
        metric:  'periodic_update_time',
        time:    ticket.created_at
      )
    end

    describe "when in the 'deactivated' state" do
      it "starts in the 'deactivated' state" do
        assert_equal :deactivated, state_machine.current
      end

      describe 'and the ticket is new' do
        before { ticket.stubs(id_changed?: true) }

        describe 'and the metric is not activated' do
          before { state_machine.advance }

          should_not_change("'fulfill' events") { metric_events.fulfill.count }
        end
      end

      describe 'and the ticket is working' do
        before { ticket.status_id = StatusType.PENDING }

        describe 'and a comment has not been added' do
          before { state_machine.advance }

          should_not_change('metric events') { metric_events.count }

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and a comment has been added by an end user' do
          before do
            ticket.add_comment(author: end_user, body: 'comment')

            state_machine.advance
          end

          should_not_change('metric events') { metric_events.count }

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and a non-public comment has been added by an agent' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: false)

            state_machine.advance
          end

          should_not_change('metric events') { metric_events.count }

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and a public comment has been added by a foreign agent' do
          before do
            ticket.add_comment(
              author:    foreign_agent,
              body:      'comment',
              is_public: true
            )

            state_machine.advance
          end

          it "enters the 'initiated' state" do
            assert_equal :initiated, state_machine.current
          end
        end

        describe 'and a public comment has been added by an agent' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: true)
          end

          describe 'and the metric has not been measured before' do
            describe 'and the state machine advances' do
              before { state_machine.advance }

              should_change("'activate' events", by: 1) do
                metric_events.activate.count
              end

              it "assigns the update time to the 'activate' event" do
                assert_equal ticket.updated_at, metric_events.activate.last.time
              end

              it "assigns the proper 'instance_id' to the 'activate' event" do
                assert_equal 1, metric_events.activate.last.instance_id
              end

              it "enters the 'initiated' state" do
                assert_equal :initiated, state_machine.current
              end
            end

            describe 'and the SLA has changed' do
              let(:change_audit) do
                SlaTargetChange.where(ticket_id: ticket.id).last
              end

              before do
                assign_policy

                state_machine.advance
              end

              should_change("'apply sla' events", by: 1) do
                metric_events.apply_sla.count
              end

              it 'assigns the proper policy metric' do
                assert_equal(
                  Zendesk::Sla::TicketMetric::PeriodicUpdateTime.id,
                  metric_events.apply_sla.last.policy_metric.metric.id
                )
              end

              it 'assigns the update time to the event' do
                assert_equal(
                  ticket.updated_at,
                  metric_events.apply_sla.last.time
                )
              end

              it "audits 'initial_target' properly" do
                assert_nil change_audit.initial_target
              end

              it "audits 'final_target' properly" do
                assert_equal(
                  {minutes: 20, in_business_hours: false},
                  change_audit.final_target
                )
              end

              it "assigns the proper 'instance_id' to the event" do
                assert_equal 1, metric_events.apply_sla.last.instance_id
              end

              should_not_change("past 'breach' events") do
                metric_events.breach.before(ticket.updated_at).count
              end

              should_change("future 'breach' events", by: 1) do
                metric_events.breach.after(ticket.updated_at).count
              end

              should_change('change audits', by: 1) do
                SlaTargetChange.where(ticket_id: ticket.id).count
              end
            end

            describe 'and the SLA has not changed' do
              before do
                assign_policy(policy: nil)

                state_machine.advance
              end

              should_not_change("'apply sla' events") do
                metric_events.apply_sla.count
              end

              should_not_change("'breach' events") do
                metric_events.breach.count
              end

              should_not_change('change audits') do
                SlaTargetChange.where(ticket_id: ticket.id).count
              end
            end
          end

          describe 'and the metric has been measured before' do
            before do
              metric_events.activate.create(
                account:     account,
                instance_id: 1,
                time:        ticket.created_at
              )
              metric_events.fulfill.create(
                account:     account,
                instance_id: 1,
                time:        ticket.created_at + 1.minute
              )
            end

            describe 'and the state machine advances' do
              before { state_machine.advance }

              it "assigns the proper 'instance_id' to the 'activate' event" do
                assert_equal 2, metric_events.activate.last.instance_id
              end
            end

            describe 'and the SLA has changed' do
              before do
                assign_policy

                state_machine.advance
              end

              it "assigns the proper 'instance_id' to the 'apply sla' event" do
                assert_equal 2, metric_events.apply_sla.last.instance_id
              end

              it 'assigns the proper policy metric' do
                assert_equal(
                  Zendesk::Sla::TicketMetric::PeriodicUpdateTime.id,
                  metric_events.apply_sla.last.policy_metric.metric.id
                )
              end

              it "assigns the proper 'instance_id' to the 'breach' event" do
                assert_equal 2, metric_events.breach.last.instance_id
              end
            end
          end
        end
      end

      describe 'and the ticket is finished' do
        before do
          ticket.status_id = StatusType.SOLVED

          state_machine.advance
        end

        describe 'and a comment has not been added' do
          before { state_machine.advance }

          should_not_change('metric events') { metric_events.count }

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and a comment has been added by an end user' do
          before do
            ticket.add_comment(author: end_user, body: 'comment')

            state_machine.advance
          end

          should_not_change('metric events') { metric_events.count }

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and a non-public comment has been added by an agent' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: false)

            state_machine.advance
          end

          should_not_change('metric events') { metric_events.count }

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and a public comment has been added by an agent' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: true)

            state_machine.advance
          end

          should_not_change('metric events') { metric_events.count }

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end
      end
    end

    describe "when in the 'activated' state" do
      before do
        metric_events.activate.create(
          account: ticket.account,
          time:    ticket.created_at
        )
      end

      it "starts in the 'activated' state" do
        assert_equal :activated, state_machine.current
      end

      describe 'and the ticket is working' do
        before { ticket.status_id = StatusType.PENDING }

        describe 'and a comment has not been added' do
          before { state_machine.advance }

          should_not_change('metric events') { metric_events.count }

          it "remains in the 'activated' state" do
            assert_equal :activated, state_machine.current
          end
        end

        describe 'and a comment has been added by an end user' do
          before do
            ticket.add_comment(author: end_user, body: 'comment')

            state_machine.advance
          end

          should_not_change('metric events') { metric_events.count }

          it "remains in the 'activated' state" do
            assert_equal :activated, state_machine.current
          end
        end

        describe 'and a non-public comment has been added by an agent' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: false)

            state_machine.advance
          end

          should_not_change('metric events') { metric_events.count }

          it "remains in the 'activated' state" do
            assert_equal :activated, state_machine.current
          end
        end

        describe 'and a public comment has been added by an agent' do
          before do
            metric_events.breach.create(
              account: ticket.account,
              time:    ticket.updated_at - 1.minute
            )
            metric_events.breach.create(
              account: ticket.account,
              time:    ticket.updated_at + 1.minute
            )

            ticket.add_comment(author: agent, body: 'comment', is_public: true)
          end

          describe 'and the metric has not been measured before' do
            before { state_machine.advance }

            should_change("'fulfill' events", by: 1) do
              metric_events.fulfill.count
            end

            it "assigns the update time to the 'fulfill' event" do
              assert_equal ticket.updated_at, metric_events.fulfill.last.time
            end

            it "assigns the proper 'instance_id' to the 'fulfill' event" do
              assert_equal 1, metric_events.fulfill.last.instance_id
            end

            should_change("'update status' events", by: 1) do
              metric_events.update_status.count
            end

            it "assigns the update time to the 'update status' event" do
              assert_equal(
                ticket.updated_at,
                metric_events.update_status.last.time
              )
            end

            it "assigns the proper 'instance_id' to the 'update status' event" do
              assert_equal 1, metric_events.update_status.last.instance_id
            end

            should_not_change("past 'breach' events") do
              metric_events.breach.before(ticket.updated_at).count
            end

            should_change("future 'breach' events", from: 1, to: 0) do
              metric_events.breach.after(ticket.updated_at).count
            end

            should_change("'activate' events", by: 1) do
              metric_events.activate.count
            end

            it "assigns the update time to the 'activate' event" do
              assert_equal ticket.updated_at, metric_events.activate.last.time
            end

            it "assigns the proper 'instance_id' to the 'activate' event" do
              assert_equal 2, metric_events.activate.last.instance_id
            end

            it "enters the 'initiated' state" do
              assert_equal :initiated, state_machine.current
            end
          end

          describe 'and the metric has been measured before' do
            before do
              metric_events.fulfill.create(
                account:     account,
                instance_id: 1,
                time:        ticket.created_at + 1.minute
              )
              metric_events.activate.create(
                account:     account,
                instance_id: 2,
                time:        ticket.created_at + 2.minutes
              )

              state_machine.advance
            end

            it "assigns the proper 'instance_id' to the 'fulfill' event" do
              assert_equal 2, metric_events.fulfill.last.instance_id
            end

            it "assigns the proper 'instance_id' to the 'update status' event" do
              assert_equal 2, metric_events.update_status.last.instance_id
            end

            it "assigns the proper 'instance_id' to the 'activate' event" do
              assert_equal 3, metric_events.activate.last.instance_id
            end
          end
        end
      end

      describe 'and the ticket is finished' do
        before { ticket.status_id = StatusType.SOLVED }

        describe 'and a comment has not been added' do
          before { state_machine.advance }

          should_change("'fulfill' events", by: 1) do
            metric_events.fulfill.count
          end

          should_change("'update status' events", by: 1) do
            metric_events.update_status.count
          end

          should_not_change("'activate' events") do
            metric_events.activate.count
          end

          it "enters the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and a comment has been added by an end user' do
          before do
            ticket.add_comment(author: end_user, body: 'comment')

            state_machine.advance
          end

          should_change("'fulfill' events", by: 1) do
            metric_events.fulfill.count
          end

          should_change("'update status' events", by: 1) do
            metric_events.update_status.count
          end

          should_not_change("'activate' events") do
            metric_events.activate.count
          end

          it "enters the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and a non-public comment has been added by an agent' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: false)

            state_machine.advance
          end

          should_change("'fulfill' events", by: 1) do
            metric_events.fulfill.count
          end

          should_change("'update status' events", by: 1) do
            metric_events.update_status.count
          end

          should_not_change("'activate' events") do
            metric_events.activate.count
          end

          it "enters the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and a public comment has been added by an agent' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: true)

            state_machine.advance
          end

          should_change("'fulfill' events", by: 1) do
            metric_events.fulfill.count
          end

          should_change("'update status' events", by: 1) do
            metric_events.update_status.count
          end

          should_not_change("'activate' events") do
            metric_events.activate.count
          end

          it "enters the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end
      end

      describe 'and the ticket has been finished' do
        before { ticket.update_column(:status_id, StatusType.SOLVED) }

        describe 'and it is advanced' do
          before { state_machine.advance }

          should_not_change('metric events') { metric_events.count(:all) }

          it "remains in the 'activated' state" do
            assert_equal :activated, state_machine.current
          end
        end
      end
    end
  end

  describe 'and the ticket is new' do
    before do
      ticket.stubs(id_changed?: true)

      state_machine.advance
    end

    should_change("'measure' events", by: 1) { metric_events.measure.count }
  end

  describe 'and the ticket is not new' do
    before do
      ticket.stubs(id_changed?: false)

      state_machine.advance
    end

    should_not_change("'measure' events") { metric_events.measure.count }
  end

  describe 'when the metric is measured for the ticket' do
    before do
      TicketMetric::Measure.create(
        account: ticket.account,
        ticket:  ticket,
        metric:  'periodic_update_time',
        time:    ticket.created_at
      )

      ticket.status_id = StatusType.OPEN
      ticket.add_comment(author: agent, body: 'comment', is_public: true)
    end

    describe 'and the state machine advances' do
      before { state_machine.advance }

      should_change('metric events') { metric_events.count }
    end
  end

  describe 'when the metric is not measured for the ticket' do
    before do
      ticket.status_id = StatusType.OPEN
      ticket.add_comment(author: agent, body: 'comment', is_public: true)
    end

    describe 'and the state machine advances' do
      before { state_machine.advance }

      should_not_change('metric events') { metric_events.count }
    end
  end

  describe 'when transitions at the same time are out of order' do
    before do
      TicketMetric::Measure.create(
        account: ticket.account,
        ticket:  ticket,
        metric:  'periodic_update_time',
        time:    ticket.created_at
      )

      metric_events.activate.create(
        account:     account,
        instance_id: 1,
        time:        ticket.created_at + 10.minutes
      )
      metric_events.activate.create(
        account:     account,
        instance_id: 2,
        time:        ticket.created_at + 20.minutes
      )
      metric_events.fulfill.create(
        account:     account,
        instance_id: 1,
        time:        ticket.created_at + 20.minutes
      )
    end

    it 'identifies the correct current state' do
      assert state_machine.is?(:activated)
    end
  end
end
