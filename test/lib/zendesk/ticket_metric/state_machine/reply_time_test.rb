require_relative '../../../../support/test_helper'
require_relative '../../../../support/business_hours_test_helper'
require_relative '../../../../support/ticket_metric_helper'

SingleCov.covered!

describe Zendesk::TicketMetric::StateMachine::ReplyTime do
  include BusinessHoursTestHelper
  include TicketMetricHelper

  fixtures :accounts,
    :tickets,
    :users

  let(:ticket)  { tickets(:minimum_1) }
  let(:account) { ticket.account }

  let(:agent)    { users(:minimum_agent) }
  let(:end_user) { users(:minimum_end_user) }

  let(:foreign_agent) { end_user.tap { |user| user.foreign_agent = true } }

  let(:light_agent) do
    agent.tap do |agent|
      agent.permission_set = PermissionSet.create_light_agent!(account)
    end
  end

  let(:metric_events) { ticket.metric_events.reply_time }

  let(:policy) { new_sla_policy(title: 'reply time') }

  let(:state_machine) do
    Zendesk::TicketMetric::StateMachine::ReplyTime.build(ticket)
  end

  before do
    Account.any_instance.stubs(has_permission_sets?: true)
    Account.any_instance.stubs(has_light_agents?: true)

    ticket.will_be_saved_by(agent)

    new_sla_policy_metric(
      policy:    policy,
      metric_id: Zendesk::Sla::TicketMetric::FirstReplyTime.id,
      target:    20
    )
    new_sla_policy_metric(
      policy:    policy,
      metric_id: Zendesk::Sla::TicketMetric::NextReplyTime.id,
      target:    40
    )
  end

  describe '#advance' do
    describe "when in the 'deactivated' state" do
      before do
        metric_events.fulfill.create(
          account:     ticket.account,
          instance_id: 1,
          time:        ticket.created_at
        )
      end

      it "starts in the 'deactivated' state" do
        assert_equal :deactivated, state_machine.current
      end

      describe 'and the ticket is new' do
        before { ticket.stubs(id_changed?: true) }

        describe 'and created by a light agent' do
          before do
            ticket.add_comment(author: light_agent, body: 'comment')

            state_machine.advance
          end

          it "enters the 'resumed' state" do
            assert_equal :resumed, state_machine.current
          end
        end

        describe 'and created by an agent with a private comment' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: false)

            state_machine.advance
          end

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and created by an agent with a public comment' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: true)

            state_machine.advance
          end

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe_with_arturo_enabled :sla_side_conversation_reply_time do
          describe 'and ticket is not via Side Conversation' do
            describe 'and created by an agent with a public comment' do
              before do
                ticket.add_comment(author: agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              it "remains in the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and created by a light-agent with a public comment' do
              before do
                ticket.add_comment(author: light_agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              it "remains in the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and created by an end-user with a public comment' do
              before do
                ticket.add_comment(body: 'comment')
                ticket.comment.stubs(from_end_user?: true)

                state_machine.advance
              end

              it "enters the 'resumed' state" do
                assert_equal :resumed, state_machine.current
              end
            end
          end

          describe 'and ticket is via Side Conversation' do
            before { ticket.via_id = ViaType.SIDE_CONVERSATION }

            describe 'and created by an agent with a public comment' do
              before do
                ticket.add_comment(author: agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              it "enters the 'resumed' state" do
                assert_equal :resumed, state_machine.current
              end
            end

            describe 'and created by a light-agent with a public comment' do
              before do
                ticket.add_comment(author: light_agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              it "enters the 'resumed' state" do
                assert_equal :resumed, state_machine.current
              end
            end

            describe 'and created by an end-user with a public comment' do
              before do
                ticket.add_comment(body: 'comment')
                ticket.comment.stubs(from_end_user?: true)

                state_machine.advance
              end

              it "enters the 'resumed' state" do
                assert_equal :resumed, state_machine.current
              end
            end
          end
        end

        describe_with_arturo_disabled :sla_side_conversation_reply_time do
          describe 'and ticket is not via Side Conversation' do
            describe 'and created by an agent with a public comment' do
              before do
                ticket.add_comment(author: agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              it "remains in the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and created by a light-agent with a public comment' do
              before do
                ticket.add_comment(author: light_agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              it "remains in the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and created by an end-user with a public comment' do
              before do
                ticket.add_comment(body: 'comment')
                ticket.comment.stubs(from_end_user?: true)

                state_machine.advance
              end

              it "enters the 'resumed' state" do
                assert_equal :resumed, state_machine.current
              end
            end
          end

          describe 'and ticket is via Side Conversation' do
            before { ticket.via_id = ViaType.SIDE_CONVERSATION }

            describe 'and created by an agent with a public comment' do
              before do
                ticket.add_comment(author: agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              it "remains in the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and created by a light-agent with a public comment' do
              before do
                ticket.add_comment(author: light_agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              it "remains in the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and created by an end-user with a public comment' do
              before do
                ticket.add_comment(body: 'comment')
                ticket.comment.stubs(from_end_user?: true)

                state_machine.advance
              end

              it "enters the 'resumed' state" do
                assert_equal :resumed, state_machine.current
              end
            end
          end
        end
      end

      describe 'and the ticket is working' do
        before { ticket.status_id = StatusType.OPEN }

        describe "and the ticket is via Side Conversation" do
          before { ticket.via_id = ViaType.SIDE_CONVERSATION }

          describe_with_arturo_enabled :sla_side_conversation_reply_time do
            describe 'and the latest comment is from an end user' do
              before do
                ticket.add_comment(body: 'comment')
                ticket.comment.stubs(from_end_user?: true)

                state_machine.advance
              end

              it "enters the 'resumed' state" do
                assert_equal :resumed, state_machine.current
              end
            end

            describe 'and the latest comment is from a light agent' do
              before do
                ticket.add_comment(author: light_agent, body: 'comment')

                state_machine.advance
              end

              it "enters the 'resumed' state" do
                assert_equal :resumed, state_machine.current
              end
            end

            describe 'and the latest comment is from an agent' do
              before do
                ticket.add_comment(author: agent, body: 'comment')

                state_machine.advance
              end

              it "enters the 'resumed' state" do
                assert_equal :resumed, state_machine.current
              end
            end
          end

          describe_with_arturo_disabled :sla_side_conversation_reply_time do
            describe 'and the latest comment is from an end user' do
              before do
                ticket.add_comment(body: 'comment')
                ticket.comment.stubs(from_end_user?: true)

                state_machine.advance
              end

              it "enters the 'resumed' state" do
                assert_equal :resumed, state_machine.current
              end
            end

            describe 'and the latest comment is from a light agent' do
              before do
                ticket.add_comment(author: light_agent, body: 'comment')

                state_machine.advance
              end

              it "remains in the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and the latest comment is from an agent' do
              before do
                ticket.add_comment(author: agent, body: 'comment')

                state_machine.advance
              end

              it "remains in the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end
          end
        end

        describe 'and a comment has not been added' do
          before { state_machine.advance }

          should_not_change('metric events') do
            ticket.metric_events.count(:all)
          end

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and the latest comment is from an end user' do
          before do
            ticket.add_comment(body: 'comment')

            ticket.comment.stubs(from_end_user?: true)
          end

          describe 'and the metric has not been fulfilled before' do
            before do
              metric_events.breach.create(
                account: ticket.account,
                time:    ticket.updated_at - 1.minute
              )
            end

            describe 'and the state machine advances' do
              before { state_machine.advance }

              should_change("'activate' events", by: 1) do
                metric_events.activate.count(:all)
              end

              it "assigns the ticket's 'updated_at' time to the event" do
                assert_equal ticket.updated_at, metric_events.activate.last.time
              end

              it "assigns the proper 'instance_id' to the event" do
                assert_equal(
                  metric_events.pluck(:instance_id).max,
                  metric_events.last.instance_id
                )
              end

              it "enters the 'resumed' state" do
                assert_equal :resumed, state_machine.current
              end
            end

            describe 'and the SLA has changed' do
              let(:change_audit) do
                SlaTargetChange.where(ticket_id: ticket.id).last
              end

              before do
                assign_policy

                state_machine.advance
              end

              should_change("'apply sla' events", by: 1) do
                metric_events.apply_sla.count(:all)
              end

              it 'assigns the proper policy metric' do
                assert_equal(
                  Zendesk::Sla::TicketMetric::NextReplyTime.id,
                  metric_events.apply_sla.last.policy_metric.metric.id
                )
              end

              it "assigns the ticket's 'updated_at' time to the event" do
                assert_equal(
                  ticket.updated_at,
                  metric_events.apply_sla.last.time
                )
              end

              it "assigns the proper 'instance_id' to the event" do
                assert_equal(
                  metric_events.pluck(:instance_id).max,
                  metric_events.apply_sla.last.instance_id
                )
              end

              it "audits 'initial_target' properly" do
                assert_nil change_audit.initial_target
              end

              it "audits 'final_target' properly" do
                assert_equal(
                  {minutes: 40, in_business_hours: false},
                  change_audit.final_target
                )
              end

              it "audits 'metric_id' properly" do
                assert_equal(
                  metric_events.apply_sla.last.policy_metric.metric_id,
                  change_audit.metric_id.to_i
                )
              end

              it "audits 'current_sla_policy' properly" do
                assert_equal policy.title, change_audit.current_sla_policy
              end

              should_not_change("past 'breach' events") do
                metric_events.breach.before(ticket.updated_at).count(:all)
              end

              should_change("future 'breach' events", by: 1) do
                metric_events.breach.after(ticket.updated_at).count(:all)
              end

              should_change('change audits', by: 1) do
                SlaTargetChange.where(ticket_id: ticket.id).count
              end
            end

            describe 'and the SLA has not changed' do
              before do
                assign_policy(policy: nil)

                state_machine.advance
              end

              should_not_change("'apply sla' events") do
                metric_events.apply_sla.count(:all)
              end

              should_not_change("'breach' events") do
                metric_events.breach.count(:all)
              end

              should_not_change('change audits') do
                SlaTargetChange.where(ticket_id: ticket.id).count
              end
            end
          end

          describe 'and the metric has been fulfilled before' do
            before do
              metric_events.activate.create(
                account:     ticket.account,
                instance_id: 1,
                time:        ticket.created_at
              )
              metric_events.fulfill.create(
                account:     ticket.account,
                instance_id: 1,
                time:        ticket.created_at + 1.minute
              )
              metric_events.activate.create(
                account:     ticket.account,
                instance_id: 2,
                time:        ticket.created_at + 2.minutes
              )
              metric_events.fulfill.create(
                account:     ticket.account,
                instance_id: 2,
                time:        ticket.created_at + 3.minutes
              )
            end

            describe 'and the state machine advances' do
              before { state_machine.advance }

              it "assigns the proper 'instance_id' to the 'activate' event" do
                assert_equal 3, metric_events.activate.last.instance_id
              end
            end

            describe 'and the SLA has changed' do
              before do
                assign_policy

                state_machine.advance
              end

              it "assigns the proper 'instance_id' to the 'apply sla' event" do
                assert_equal 3, metric_events.apply_sla.last.instance_id
              end

              it 'assigns the proper policy metric' do
                assert_equal(
                  Zendesk::Sla::TicketMetric::NextReplyTime.id,
                  metric_events.apply_sla.last.policy_metric.metric.id
                )
              end

              it "assigns the proper 'instance_id' to the 'breach' event" do
                assert_equal 3, metric_events.breach.last.instance_id
              end
            end
          end
        end

        describe 'and the latest comment is from an agent' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: true)

            state_machine.advance
          end

          should_not_change('metric events') do
            ticket.metric_events.count(:all)
          end

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end
      end

      describe 'and the ticket is finished' do
        before do
          ticket.status_id = StatusType.SOLVED

          state_machine.advance
        end

        describe 'and a comment has not been added' do
          before { state_machine.advance }

          should_not_change('metric events') do
            ticket.metric_events.count(:all)
          end

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and the latest comment is from an end user' do
          before do
            ticket.add_comment(author: end_user, body: 'comment')

            state_machine.advance
          end

          should_not_change('metric events') do
            ticket.metric_events.count(:all)
          end

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and the latest comment is from an agent' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: true)

            state_machine.advance
          end

          should_not_change('metric events') do
            ticket.metric_events.count(:all)
          end

          it "remains in the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end
      end
    end

    describe "when in the 'activated' state" do
      before do
        metric_events.activate.create(
          account: ticket.account,
          time:    ticket.created_at
        )
      end

      it "starts in the 'activated' state" do
        assert_equal :activated, state_machine.current
      end

      describe 'and the ticket is working' do
        before { ticket.status_id = StatusType.OPEN }

        describe_with_arturo_enabled :sla_side_conversation_reply_time do
          describe 'and ticket is via Side Conversation' do
            before { ticket.via_id = ViaType.SIDE_CONVERSATION }

            describe 'and the latest comment is from an agent' do
              before do
                ticket.add_comment(author: agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              should_change("'fulfill' events", by: 1) do
                metric_events.fulfill.count(:all)
              end

              should_change("'update status' events", by: 1) do
                metric_events.update_status.count(:all)
              end

              it "enters the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and the latest comment is from a light agent' do
              before do
                ticket.add_comment(author: light_agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              should_change("'fulfill' events", by: 1) do
                metric_events.fulfill.count(:all)
              end

              should_change("'update status' events", by: 1) do
                metric_events.update_status.count(:all)
              end

              it "enters the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and the latest comment is from the requester agent' do
              before do
                ticket.add_comment(author: users(:minimum_author), body: 'comment', is_public: true)

                state_machine.advance
              end

              should_not_change('metric events') do
                ticket.metric_events.count(:all)
              end

              it "remains in the 'activated' state" do
                assert_equal :activated, state_machine.current
              end
            end
          end
        end

        describe_with_arturo_disabled :sla_side_conversation_reply_time do
          describe 'and ticket is via Side Conversation' do
            before { ticket.via_id = ViaType.SIDE_CONVERSATION }

            describe 'and the latest comment is from an agent' do
              before do
                ticket.add_comment(author: agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              should_change("'fulfill' events", by: 1) do
                metric_events.fulfill.count(:all)
              end

              should_change("'update status' events", by: 1) do
                metric_events.update_status.count(:all)
              end

              it "enters the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and the latest comment is from a light agent' do
              before do
                ticket.add_comment(author: light_agent, body: 'comment', is_public: true)

                state_machine.advance
              end

              should_change("'fulfill' events", by: 1) do
                metric_events.fulfill.count(:all)
              end

              should_change("'update status' events", by: 1) do
                metric_events.update_status.count(:all)
              end

              it "enters the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and the latest comment is from the requester agent' do
              before do
                ticket.add_comment(author: users(:minimum_author), body: 'comment', is_public: true)

                state_machine.advance
              end

              should_not_change('metric events') do
                ticket.metric_events.count(:all)
              end

              it "remains in the 'activated' state" do
                assert_equal :activated, state_machine.current
              end
            end
          end
        end

        describe 'and a comment has not been added' do
          before { state_machine.advance }

          should_not_change('metric events') do
            ticket.metric_events.count(:all)
          end

          it "remains in the 'activated' state" do
            assert_equal :activated, state_machine.current
          end
        end

        describe 'and the latest comment is from an end user' do
          before do
            ticket.add_comment(author: end_user, body: 'comment')

            state_machine.advance
          end

          should_not_change('metric events') do
            ticket.metric_events.count(:all)
          end

          it "remains in the 'activated' state" do
            assert_equal :activated, state_machine.current
          end
        end

        describe 'and a public comment has been added by a foreign agent' do
          before do
            ticket.add_comment(
              author:    foreign_agent,
              body:      'comment',
              is_public: true
            )

            state_machine.advance
          end

          it "enters the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and the latest comment is from an agent' do
          before do
            metric_events.breach.create(
              account: ticket.account,
              time:    ticket.updated_at - 1.minute
            )
            metric_events.breach.create(
              account: ticket.account,
              time:    ticket.updated_at + 1.minute
            )
          end

          describe 'and the comment is public' do
            before do
              ticket.add_comment(
                author:    agent,
                body:      'comment',
                is_public: true
              )
            end

            describe 'and the metric has not been activated multiple times' do
              before { state_machine.advance }

              should_change("'fulfill' events", by: 1) do
                metric_events.fulfill.count(:all)
              end

              it "assigns the ticket's 'updated_at' time to the 'fulfill' event" do
                assert_equal ticket.updated_at, metric_events.fulfill.last.time
              end

              it "assigns the proper 'instance_id' to the 'fulfill' event" do
                assert_equal(
                  metric_events.activate.count(:all),
                  metric_events.fulfill.last.instance_id
                )
              end

              should_change("'update status' events", by: 1) do
                metric_events.update_status.count(:all)
              end

              it "assigns the ticket's 'updated_at' time to the 'update status' event" do
                assert_equal(
                  ticket.updated_at,
                  metric_events.update_status.last.time
                )
              end

              it "assigns the proper 'instance_id' to the 'update status' event" do
                assert_equal(
                  metric_events.activate.count(:all),
                  metric_events.update_status.last.instance_id
                )
              end

              should_not_change("past 'breach' events") do
                metric_events.breach.before(ticket.updated_at).count(:all)
              end

              should_change("future 'breach' events", from: 1, to: 0) do
                metric_events.breach.after(ticket.updated_at).count(:all)
              end

              it "enters the 'deactivated' state" do
                assert_equal :deactivated, state_machine.current
              end
            end

            describe 'and the metric has been activated multiple times' do
              before do
                metric_events.fulfill.create(
                  account:     ticket.account,
                  instance_id: 1,
                  time:        ticket.created_at + 1.minute
                )
                metric_events.activate.create(
                  account:     ticket.account,
                  instance_id: 2,
                  time:        ticket.created_at + 2.minutes
                )

                state_machine.advance
              end

              it "assigns the proper 'instance_id' to the 'fulfill' event" do
                assert_equal 2, metric_events.fulfill.last.instance_id
              end

              it "assigns the proper 'instance_id' to the 'update status' event" do
                assert_equal 2, metric_events.update_status.last.instance_id
              end
            end
          end

          describe 'and the comment is not public' do
            before do
              ticket.add_comment(
                author:    agent,
                body:      'comment',
                is_public: false
              )

              state_machine.advance
            end

            should_not_change("'fulfill' events") do
              ticket.metric_events.fulfill.count(:all)
            end

            it "remains in the 'activated' state" do
              assert_equal :activated, state_machine.current
            end
          end
        end
      end

      describe 'and the ticket is finished' do
        before { ticket.status_id = StatusType.SOLVED }

        describe 'and a comment has not been added' do
          before { state_machine.advance }

          should_change("'fulfill' events", by: 1) do
            metric_events.fulfill.count(:all)
          end

          should_change("'update status' events", by: 1) do
            metric_events.update_status.count(:all)
          end

          it "enters the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and the latest comment is from an end user' do
          before do
            ticket.add_comment(author: end_user, body: 'comment')

            state_machine.advance
          end

          should_change("'fulfill' events", by: 1) do
            metric_events.fulfill.count(:all)
          end

          should_change("'update status' events", by: 1) do
            metric_events.update_status.count(:all)
          end

          it "enters the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end

        describe 'and the latest comment is from an agent' do
          before do
            ticket.add_comment(author: agent, body: 'comment', is_public: true)

            state_machine.advance
          end

          should_change("'fulfill' events", by: 1) do
            metric_events.fulfill.count(:all)
          end

          should_change("'update status' events", by: 1) do
            metric_events.update_status.count(:all)
          end

          it "enters the 'deactivated' state" do
            assert_equal :deactivated, state_machine.current
          end
        end
      end

      describe 'and the ticket has been finished' do
        before { ticket.update_column(:status_id, StatusType.SOLVED) }

        describe 'and it is advanced' do
          before { state_machine.advance }

          should_not_change('metric events') { metric_events.count(:all) }

          it "remains in the 'activated' state" do
            assert_equal :activated, state_machine.current
          end
        end
      end
    end

    describe 'when the ticket is new' do
      before do
        ticket.stubs(id_changed?: true)

        state_machine.advance
      end

      describe 'and the state machine advances' do
        before { state_machine.advance }

        should_change("'measure' events", by: 1) do
          metric_events.measure.count(:all)
        end
      end

      describe "and remains in the 'deactivated' state" do
        before do
          ticket.add_comment(author: agent, body: 'comment', is_public: true)

          state_machine.advance
        end

        it "sets the 'fulfill' event 'instance_id' to 1" do
          assert_equal 1, metric_events.fulfill.last.instance_id
        end

        it "sets the 'fulfill' event time to the ticket creation time" do
          assert_equal ticket.created_at, metric_events.fulfill.last.time
        end

        should_change("'fulfill' events", by: 1) do
          metric_events.fulfill.count(:all)
        end
      end

      describe "and enters the 'activated' state" do
        before do
          ticket.status_id = StatusType.OPEN
          ticket.add_comment(author: end_user, body: 'comment')

          state_machine.advance
        end

        should_not_change("'fulfill' events") do
          metric_events.fulfill.count(:all)
        end
      end
    end

    describe 'when the ticket is not new' do
      before do
        ticket.stubs(id_changed?: false)

        state_machine.advance
      end

      describe 'and the state machine advances' do
        before { state_machine.advance }

        should_not_change("'measure' events") do
          metric_events.measure.count(:all)
        end
      end

      describe "and remains in the 'deactivated' state" do
        before do
          ticket.add_comment(author: agent, body: 'comment', is_public: true)

          state_machine.advance
        end

        should_not_change("'fulfill' events") do
          metric_events.fulfill.count(:all)
        end
      end

      describe "and enters the 'activated' state" do
        before do
          ticket.status_id = StatusType.OPEN
          ticket.add_comment(author: end_user, body: 'comment')

          state_machine.advance
        end

        should_not_change("'fulfill' events") do
          metric_events.fulfill.count(:all)
        end
      end
    end
  end
end
