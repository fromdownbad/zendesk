require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::GroupMemberships::Finder do
  fixtures :accounts, :users, :organizations

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:group) { groups(:minimum_group) }
  let(:source_type) { Organization.name }
  let(:params) { {} }
  let(:finder) { Zendesk::GroupMemberships::Finder.new(account, params) }

  describe "#membership_scope" do
    it "returns the correct scope" do
      assert_equal account, finder.membership_scope
    end

    describe "with user_id" do
      let(:params) { { user_id: user.id } }

      it "returns the correct scope" do
        assert_equal account.users.find(user.id), finder.membership_scope
      end
    end

    describe "with group_id" do
      let(:params) { { group_id: group.id } }

      it "returns the correct scope" do
        assert_equal account.groups.find(group.id), finder.membership_scope
      end
    end
  end
end
