require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::Gravatar do
  fixtures :users, :accounts

  describe "#url" do
    let(:user) { users(:minimum_end_user) }
    let(:account) { accounts(:minimum) }

    it "returns nil when the user has no email" do
      assert_nil Zendesk::Gravatar.url(User.new)
    end

    it "returns value when the account has setting turned on" do
      user.account.settings.have_gravatars_enabled = true
      user.account.settings.save!
      user.account.settings.reload

      refute_nil Zendesk::Gravatar.url(user)
    end

    it "returns nil when the account has setting turned off" do
      user.account.settings.have_gravatars_enabled = false
      user.account.settings.save!
      user.account.settings.reload

      assert_nil Zendesk::Gravatar.url(user)
    end

    it "defaults to the Zendesk user photo" do
      user = User.new(email: "hallomanden@example.org")
      user.stubs(:account).returns(account)
      user.email = "hallomanden@example.org"
      assert_equal "https://secure.gravatar.com/avatar/38184cd7b1f6af6438720d4a70441d14?size=80&default=https%3A%2F%2Fassets.zendesk.com%2Fimages%2F2016%2Fdefault-avatar-80.png&r=g", Zendesk::Gravatar.url(user)
    end
  end
end
