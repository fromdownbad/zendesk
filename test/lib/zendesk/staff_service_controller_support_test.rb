require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

class StaffServiceControllerSupportTestController < Api::V2::Internal::BaseController
  include Zendesk::StaffServiceControllerSupport

  allow_parameters :foo,
    requester_id: Parameters.bigid
  def foo
    head :ok
  end
end

class StaffServiceControllerSupportTest < ActionController::TestCase
  tests StaffServiceControllerSupportTestController
  use_test_routes

  describe 'StaffServiceControllerSupport' do
    let(:requester_id) { 1204128 }

    as_an_admin do
      should_be_forbidden [:get, :foo, requester_id: 123, format: 'json']
    end

    as_a_subsystem_user(user: 'account_service', account: :minimum) do
      should_be_forbidden [:get, :foo, requester_id: 123, format: 'json']
    end

    as_a_subsystem_user(user: 'metropolis', account: :minimum) do
      it 'returns ok' do
        get :foo, params: { requester_id: requester_id, format: 'json' }
        assert_response :ok
      end

      it 'fails with a 400 error when missing the requester_id param' do
        get :foo, format: 'json'
        assert_response 400

        body = JSON.parse(@response.body)

        assert_equal 'MissingRequesterId', body['error']
        assert_equal "'requester_id' is a required field", body['description']
      end
    end
  end
end
