require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 7

describe Zendesk::RateControl do
  let(:inner_time) { 0.1 }
  let(:outer_time) { 0.2 }
  let(:rate) { 2 }
  let(:rate_controller) { Zendesk::RateControl.new(rate_per_sec: rate) }
  let(:n) { 10 }

  def measure_n_times
    s = Time.now
    n.times do
      yield
    end
    n / (Time.now - s)
  end

  # Plus or minus 10%
  def approximately_equal(a, b)
    ((a < b * 1.1) && (a > b * 0.9))
  end

  it "should run strictly below specified rate" do
    effective_rate = measure_n_times do
      rate_controller.with_rate_control do
        Timecop.travel(Time.now + inner_time)
      end
    end

    assert_operator effective_rate, :<, rate
    assert_operator effective_rate, :>, 0
    assert approximately_equal(rate, effective_rate)
  end

  it "should run strictly below even with outside delays" do
    effective_rate = measure_n_times do
      rate_controller.with_rate_control do
        Timecop.travel(Time.now + inner_time)
      end
      Timecop.travel(Time.now + outer_time)
    end

    assert_operator effective_rate, :<, rate
    assert_operator effective_rate, :>, 0
    assert approximately_equal(rate, effective_rate)
  end

  describe "with slower than set rate" do
    let(:inner_time) { 1 }
    let(:n) { 5 }

    it "does not slow down anymore" do
      effective_rate = measure_n_times do
        rate_controller.with_rate_control do
          Timecop.travel(Time.now + inner_time)
        end
      end

      assert_operator effective_rate, :<, rate
      assert_operator effective_rate, :>, 0
      assert approximately_equal(1, effective_rate)
    end
  end
end
