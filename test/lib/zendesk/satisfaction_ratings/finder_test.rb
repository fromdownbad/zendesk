require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Tickets::Finder do
  fixtures :accounts

  let(:reason_code_enabled) { false }
  let(:account) { accounts(:minimum) }
  let(:params) { {} }
  let(:finder) { Zendesk::SatisfactionRatings::Finder.new(account, params) }

  before do
    Account.any_instance.stubs(:csat_reason_code_enabled?).returns(reason_code_enabled)
  end

  describe "#satisfaction_ratings" do
    let(:ratings) { account.satisfaction_ratings.non_deleted_with_archived }
    let(:finder_sql) { finder.satisfaction_ratings.to_sql }

    it "generates the correct sql query" do
      assert_equal ratings.to_sql, finder_sql
    end

    describe "with a valid score filter" do
      let(:params) { { score: "bad_without_comment" } }

      it "generates the a query including the score filter" do
        expected = ratings.where(score: SatisfactionType.BAD).to_sql
        assert_equal expected, finder_sql
      end
    end

    describe "with an invalid score filter" do
      let(:params) { { score: "its_not_a_real_score" } }

      it "raises KeyError" do
        assert_raises KeyError do
          finder.satisfaction_ratings
        end
      end
    end

    describe "with a reason code" do
      let(:reason_code) { [Satisfaction::Reason::OTHER] }
      let(:params) { { reason_code: reason_code } }

      describe "when csat_reason_code is enabled" do
        let(:reason_code_enabled) { true }

        it "generates the a sql query including the reason code" do
          expected = ratings.where(reason_code: reason_code).to_sql
          assert_equal expected, finder_sql
        end
      end

      describe "when csat_reason_code is disabled" do
        it "generates a query without a reason code" do
          assert_equal ratings.to_sql, finder_sql
        end
      end
    end

    describe "with a start time filter" do
      let(:params) { { start_time: 2.hours.ago.to_i } }

      it "generates a query filtering by start time" do
        expected = ratings.newer_than(Time.at(params[:start_time])).to_sql
        assert_equal expected, finder_sql
      end
    end

    describe "with an end time filter" do
      let(:params) { { end_time: 2.hours.ago.to_i } }

      it "generates a query filtering by end time" do
        expected = ratings.older_than(Time.at(params[:end_time])).to_sql
        assert_equal expected, finder_sql
      end
    end
  end
end
