require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Zendesk::UserPortalState do
  fixtures :accounts, :user_identities

  before do
    @account = accounts(:minimum)
    @user_portal_state = Zendesk::UserPortalState.new(@account)
    @user = User.new
  end

  describe "#send_verify_email?" do
    it "returns true if it has web portal restricted" do
      @account.stubs(:web_portal_restricted?).returns(true)
      assert @user_portal_state.send_verify_email?(@user)
    end

    it "returns true if it has HC enabled" do
      @account.stubs(:help_center_enabled?).returns(true)
      assert @user_portal_state.send_verify_email?(@user)
    end

    it "returns true if has no web portal or help center and user is agent" do
      @user_portal_state.stubs(:has_web_portal_or_help_center?).returns(false)
      @user.stubs(:is_agent?).returns(true)
      assert @user_portal_state.send_verify_email?(@user)
    end

    it "returns false if has no web portal or help center and user is nil" do
      @account.stubs(:web_portal_restricted?).returns(false)
      @account.stubs(:help_center_enabled?).returns(false)
      @user.stubs(:is_agent?).returns(false)
      refute @user_portal_state.send_verify_email?(@user)
    end

    it "returns false if has no web portal or help center and user is end user" do
      @user_portal_state.stubs(:has_web_portal_or_help_center?).returns(false)
      @user.stubs(:is_agent?).returns(false)
      refute @user_portal_state.send_verify_email?(@user)
    end
  end

  describe "#send_suspend_verify_email?" do
    it "returns true if it can send verify email" do
      @user_portal_state.stubs(:send_verify_email?).returns(true)
      assert @user_portal_state.send_suspend_verify_email?(@user)
    end

    it "returns false if it can not send verify email" do
      @user_portal_state.stubs(:send_verify_email?).returns(false)
      refute @user_portal_state.send_suspend_verify_email?(@user)
    end
  end

  describe "#send_identity_verification_email?" do
    let(:identity) { user_identities(:minimum_end_user) }

    it "returns true if send verify email is true" do
      @user_portal_state.stubs(:send_verify_email?).returns(true)
      assert @user_portal_state.send_identity_verification_email?(identity)
    end

    it "returns false if send verify email is false" do
      @user_portal_state.stubs(:send_verify_email?).returns(false)
      refute @user_portal_state.send_identity_verification_email?(identity)
    end
  end

  describe "#send_initial_identity_verification_email?" do
    let(:identity) { user_identities(:minimum_end_user) }

    it "returns true if can send verify email and user.send_verify_email is true and identity is not verified" do
      @user_portal_state.stubs(:send_verify_email?).returns(true)
      identity.user.stubs(:send_verify_email).returns(true)
      identity.stubs(:is_verified?).returns(false)
      assert @user_portal_state.send_initial_identity_verification_email?(identity)
    end

    it "returns false if can't send verify email" do
      @user_portal_state.stubs(:send_verify_email?).returns(false)
      refute @user_portal_state.send_initial_identity_verification_email?(identity)
    end

    it "returns false if can send verify email but user.send_verify_email is false" do
      @user_portal_state.stubs(:send_verify_email?).returns(true)
      identity.user.stubs(:send_verify_email).returns(false)
      refute @user_portal_state.send_initial_identity_verification_email?(identity)
    end

    it "returns false if can send verify email, user.send_verify_email is true and is verified" do
      @user_portal_state.stubs(:send_verify_email?).returns(true)
      identity.user.stubs(:send_verify_email).returns(true)
      identity.stubs(:is_verified?).returns(true)
      refute @user_portal_state.send_initial_identity_verification_email?(identity)
    end
  end

  describe "#show_auth_cancel_link?" do
    it "returns true if has_web_portal_or_help_center?" do
      @user_portal_state.expects(:has_web_portal_or_help_center?).returns(true)
      assert @user_portal_state.show_auth_cancel_link?
    end

    it "returns false if has no web portal or help center" do
      @user_portal_state.expects(:has_web_portal_or_help_center?).returns(false)
      refute @user_portal_state.show_auth_cancel_link?
    end
  end

  describe "#show_anonymous_modify_csat_link?" do
    it "returns true if has web portal or help center" do
      @user_portal_state.stubs(:has_web_portal_or_help_center?).returns(true)
      assert @user_portal_state.show_anonymous_modify_csat_link?
    end

    it "returns false if doesn't have web portal or help center" do
      @user_portal_state.stubs(:has_web_portal_or_help_center?).returns(false)
      refute @user_portal_state.show_anonymous_modify_csat_link?
    end
  end

  describe "#can_users_signup?" do
    it "returns true if has web portal or help center" do
      @user_portal_state.stubs(:has_web_portal_or_help_center?).returns(true)
      assert @user_portal_state.can_users_signup?
    end

    it "returns false if doesn't have web portal or help center" do
      @user_portal_state.stubs(:has_web_portal_or_help_center?).returns(false)
      refute @user_portal_state.can_users_signup?
    end
  end

  describe "#dropbox_uses_help_center_search?" do
    describe "with help center disabled" do
      before do
        @account.stubs(:help_center_state).returns(:disabled)
      end

      it 'returns false' do
        refute @user_portal_state.dropbox_uses_help_center_search?
      end
    end

    describe "with help center restricted" do
      before do
        @account.stubs(:help_center_state).returns(:restricted)
      end

      it "returns true" do
        assert @user_portal_state.dropbox_uses_help_center_search?
      end
    end

    describe "with help center enabled" do
      before do
        @account.stubs(:help_center_state).returns(:enabled)
      end

      it "returns true" do
        assert @user_portal_state.dropbox_uses_help_center_search?
      end
    end
  end

  describe "#shows_search_and_topic_suggestion_in_dropbox?" do
    describe "with help center disabled" do
      before do
        @account.stubs(:help_center_state).returns(:disabled)
      end

      it "returns true if web portal is restricted" do
        @account.stubs(:web_portal_state).returns(:restricted)
        assert @user_portal_state.shows_search_and_topic_suggestion_in_dropbox?
      end

      it "returns false if web portal is disabled" do
        @account.stubs(:web_portal_state).returns(:disabled)
        refute @user_portal_state.shows_search_and_topic_suggestion_in_dropbox?
      end
    end

    describe "with help center restricted" do
      before do
        @account.stubs(:help_center_state).returns(:restricted)
      end

      it "returns true if web portal is restricted" do
        @account.stubs(:web_portal_state).returns(:restricted)
        assert @user_portal_state.shows_search_and_topic_suggestion_in_dropbox?
      end

      it "returns true if web portal is disabled" do
        @account.stubs(:web_portal_state).returns(:disabled)
        assert @user_portal_state.shows_search_and_topic_suggestion_in_dropbox?
      end
    end

    describe "with help center enabled" do
      before do
        @account.stubs(:help_center_state).returns(:enabled)
      end

      it "returns true if web portal is restricted" do
        @account.stubs(:web_portal_state).returns(:restricted)
        assert @user_portal_state.shows_search_and_topic_suggestion_in_dropbox?
      end

      it "returns true if web portal is disabled" do
        @account.stubs(:web_portal_state).returns(:disabled)
        assert @user_portal_state.shows_search_and_topic_suggestion_in_dropbox?
      end
    end
  end

  describe "#has_web_portal_or_help_center?" do
    describe "with help center disabled" do
      before { @account.stubs(:help_center_state).returns(:disabled) }

      it "returns false if web portal is restricted" do
        @account.stubs(:web_portal_state).returns(:restricted)
        assert @user_portal_state.send(:has_web_portal_or_help_center?)
      end

      it "returns true if web portal is disabled" do
        @account.stubs(:web_portal_state).returns(:disabled)
        refute @user_portal_state.send(:has_web_portal_or_help_center?)
      end
    end

    describe "with help center restricted" do
      before { @account.stubs(:help_center_state).returns(:restricted) }

      it "returns false if web portal is restricted" do
        @account.stubs(:web_portal_state).returns(:restricted)
        assert @user_portal_state.send(:has_web_portal_or_help_center?)
      end

      it "returns true if web portal is disabled" do
        @account.stubs(:web_portal_state).returns(:disabled)
        refute @user_portal_state.send(:has_web_portal_or_help_center?)
      end
    end

    describe "with help center enabled" do
      before { @account.stubs(:help_center_state).returns(:enabled) }

      it "returns false if web portal is restricted" do
        @account.stubs(:web_portal_state).returns(:restricted)
        assert @user_portal_state.send(:has_web_portal_or_help_center?)
      end

      it "returns false if web portal is disabled" do
        @account.stubs(:web_portal_state).returns(:disabled)
        assert @user_portal_state.send(:has_web_portal_or_help_center?)
      end
    end
  end

  describe "#show_help_center_logo?" do
    describe "web portal disabled" do
      before do
        @account.stubs(:web_portal_state).returns(:disabled)
      end

      it "returns true if help_center is disabled" do
        @account.stubs(:help_center_state).returns(:disabled)
        refute @user_portal_state.show_help_center_logo?
      end

      it "returns false if help_center is restricted" do
        @account.stubs(:help_center_state).returns(:restricted)
        assert @user_portal_state.show_help_center_logo?
      end

      it "returns false if help_center is enabled" do
        @account.stubs(:help_center_state).returns(:enabled)
        assert @user_portal_state.show_help_center_logo?
      end
    end

    describe "web portal restricted" do
      before do
        @account.stubs(:web_portal_state).returns(:restricted)
      end

      it "returns false if help_center is disabled" do
        @account.stubs(:help_center_state).returns(:disabled)
        refute @user_portal_state.show_help_center_logo?
      end

      it "returns true if help_center is restricted" do
        @account.stubs(:help_center_state).returns(:restricted)
        assert @user_portal_state.show_help_center_logo?
      end

      it "returns true if help_center is enabled" do
        @account.stubs(:help_center_state).returns(:enabled)
        assert @user_portal_state.show_help_center_logo?
      end
    end
  end
end
