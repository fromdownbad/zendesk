require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 11

describe Branding do
  describe "#mobile_border_color" do
    before do
      @branding = Branding.new
    end

    it "darkens the tab_background_color" do
      @branding.tab_background_color = "72A737"
      assert_equal "#679632", @branding.mobile_border_color
    end

    it "returns #000000 if the tab_background_color is 000000" do
      @branding.tab_background_color = "000000"
      assert_equal "#000000", @branding.mobile_border_color
    end
  end
end
