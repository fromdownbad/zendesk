require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 12

describe Output do
  describe "#sanitized_type" do
    it "only return list or table" do
      assert_equal 'list', Output.sanitized_type('horse')
      assert_equal 'list', Output.sanitized_type(nil)
      assert_equal 'table', Output.sanitized_type('table')
    end
  end

  describe ".valid_columns" do
    subject { Output.valid_columns([:score, :blah, :status]) }

    it "rejects score" do
      refute subject.include?(:score)
    end

    it "rejects bogus column names" do
      refute subject.include?(:blah)
    end

    it "includes status" do
      assert subject.include?(:status)
    end

    describe "with a ticket field" do
      fixtures :ticket_fields

      before do
        @ticket_field = ticket_fields(:field_tagger_custom)
      end

      subject { Output.valid_columns([@ticket_field.id.to_s.to_sym, :status]) }

      it "keeps valid columns" do
        assert_equal 2, subject.size
      end
    end
  end

  describe "merging" do
    before do
      @output = Output.create(group: :status, group_asc: true, order: :id,
                              order_asc: true, columns: [:description, :requester, :created], type: 'table')
    end

    describe "#merge!" do
      before do
        @output.merge!(group_by: :created, group_order: :desc, sort_by: :updated, sort_order: :desc, columns: [:status, :updated])
      end

      it("set the group") { assert_equal :created, @output.group }
      it("set the order") { assert_equal :updated, @output.order }

      it("set the group_order") { refute @output.group_asc }
      it("set the sort_order") { refute @output.order_asc }

      it("set the columns") { assert_equal [:status, :updated], @output.columns }

      describe "order -> nice_id" do
        before do
          @output.merge!(sort_by: :id)
        end

        it "sets properly set order" do
          assert_equal :nice_id, @output.order
        end
      end
    end

    describe "#merge" do
      subject { @output.merge(group_by: :created, columns: [:status]) }

      it "creates a new object" do
        assert_not_equal @output.object_id, subject.object_id
      end

      it("set the group") { assert_equal :created, subject.group }
      it("keep the order") { assert_equal :nice_id, subject.order }

      it("keep the group_order") { assert subject.group_asc }
      it("keep the sort_order") { assert subject.order_asc }

      it("set the columns") { assert_equal [:status], subject.columns }
    end
  end

  describe ".default" do
    describe "custom options" do
      subject { Output.default(columns: [:score, :blah, :status], order: :id) }

      it "rejects score" do
        refute subject.columns.include?(:score)
      end

      it "rejects bogus column names" do
        refute subject.columns.include?(:blah)
      end

      it "sorts by nice id if passed id" do
        assert_equal :nice_id, subject.order
      end
    end

    describe "grouping" do
      subject { Output.default(group_by: :id, group_order: "asc") }

      it "groups by id" do
        assert_equal :id, subject.group
      end

      it "groups by order ASC" do
        assert_equal :asc, subject.group_order
      end
    end

    describe "sorting" do
      subject { Output.default(sort_by: :id, sort_order: "asc") }

      it "sorts by nice id" do
        assert_equal :nice_id, subject.order
      end

      it "sorts by order ASC" do
        assert_equal :asc, subject.sort_order
      end
    end
  end

  describe "#type" do
    it "uses sanitized_type" do
      output      = Output.new
      output.type = 'trumpet'

      Output.expects(:sanitized_type).returns('cello')
      assert_equal 'cello', output.type
    end
  end

  describe "#group" do
    it "always return nil for type 'list' (lists can't be grouped, only ordered)" do
      output       = Output.new
      output.type  = 'table'
      output.group = 'status'
      output.group_asc = true

      assert_equal 'status', output.group

      output.type = 'list'
      assert_nil output.group
    end
  end

  describe "#set_order" do
    it "behaves identically for sort_by and order" do
      assert_equal Output.new.set_order(sort_by: "subject", sort_order: "desc"),
        Output.new.set_order(order: "subject", desc: "1")
    end
  end

  describe "#sort_order" do
    before do
      @output = Output.new
    end

    it "is ascending when ascending order is enabled" do
      @output.order_asc = true
      assert_equal :asc, @output.sort_order
    end

    it "is descending when ascending order is disabled" do
      @output.order_asc = false
      assert_equal :desc, @output.sort_order
    end
  end

  # See https://support.zendesk.com/agent/tickets/908084
  describe "serialization" do
    before do
      @output = Output.new
      @output.rule_associations
      @output.fields_to_fom
      @output.rule_fields
    end

    it "does not raise exception when serializing and deserializing" do
      YAML.load(@output.to_yaml)
    end
  end
end
