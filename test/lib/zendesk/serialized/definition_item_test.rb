require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 17

describe 'DefinitionItem' do
  fixtures :accounts, :rules, :tickets

  it "builds new definitions from params" do
    item = DefinitionItem.build(operator: 'less_than', value: { '0' => '2' }, source: 'status_id')

    assert_equal 'less_than', item.operator
    assert_equal ['2'],       item.value
    assert_equal 'status_id', item.source

    item = DefinitionItem.build(field: 'status', operator: 'is', value: ['closed'])

    assert_equal 'status_id', item.source
    assert_equal 'is',        item.operator
    assert_equal ['4'],       item.value

    item = DefinitionItem.build(field: 'comment_mode_is_public', operator: 'is', value: ['false'])
    assert_equal 'comment_mode_is_public', item.source
    assert_equal 'is',        item.operator
    assert_equal ['false'],   item.value
  end

  describe "empty string values" do
    it "maps priority to the unset priority" do
      item = DefinitionItem.build(field: 'priority', operator: 'is', value: '')

      assert_equal 'priority_id', item.source
      assert_equal 'is',          item.operator
      assert_equal ['0'],         item.value
    end

    it "maps type to the unset ticket type" do
      item = DefinitionItem.build(field: 'type', operator: 'is', value: '')

      assert_equal 'ticket_type_id', item.source
      assert_equal 'is',             item.operator
      assert_equal ['0'],            item.value
    end
  end

  describe "initializer" do
    before do
      @item = DefinitionItem.new('comment_value', 'is', "Hello\r\nThis\u2028should be\u2029cleaned up.")
    end

    it "cleans input" do
      assert_equal ["Hello\nThis\nshould be\ncleaned up."], @item.value
    end
  end

  describe "#value" do
    let(:item) { DefinitionItem.new(source, 'is', item_value) }

    describe 'when the source is `satisfaction_score`' do
      let(:source)     { 'satisfaction_score' }
      let(:item_value) { 'bad_with_comment' }

      it 'returns the mapped value' do
        assert_equal [SatisfactionType.BADWITHCOMMENT.to_s], item.value
      end
    end

    describe 'when there is a mapping' do
      let(:source)     { 'status_id' }
      let(:item_value) { 'deleted' }

      it 'returns the mapped value' do
        assert_equal [StatusType.DELETED.to_s], item.value
      end
    end

    describe 'when there is no mapping' do
      let(:source)     { 'requester_id' }
      let(:item_value) { 'current_user' }

      it 'returns the value' do
        assert_equal [item_value], item.value
      end
    end
  end

  describe "#cache_key" do
    let(:item) { DefinitionItem.new('status_id', 'less_than', '2') }

    describe "same source, operator and value" do
      it "is equal" do
        assert_equal DefinitionItem.new('status_id', 'less_than', ['2']).cache_key, item.cache_key
      end
    end

    describe "different source" do
      it "does not be equal" do
        refute_equal DefinitionItem.new('assignee_id', 'less_than', ['2']).cache_key, item.cache_key
      end
    end

    describe "different operator" do
      it "does not be equal" do
        refute_equal DefinitionItem.new('status_id', 'greater_than', ['2']).cache_key, item.cache_key
      end
    end

    describe "different value" do
      it "does not be equal" do
        refute_equal DefinitionItem.new('status_id', 'less_than', ['1']).cache_key, item.cache_key
      end
    end
  end

  describe "#==" do
    let(:item) { DefinitionItem.new('status_id', 'less_than', '2') }

    describe "being compared to another object" do
      it "does not be equal" do
        refute_equal 1, item
      end
    end

    describe "same source, operator and value" do
      it "is equal" do
        assert_equal DefinitionItem.new('status_id', 'less_than', '2'), item
      end
    end

    describe "different source" do
      it "does not be equal" do
        refute_equal DefinitionItem.new('assignee_id', 'less_than', '2'), item
      end
    end

    describe "different operator" do
      it "does not be equal" do
        refute_equal DefinitionItem.new('status_id', 'greater_than', '2'), item
      end
    end

    describe "different value" do
      it "does not be equal" do
        refute_equal DefinitionItem.new('status_id', 'less_than', '1'), item
      end
    end
  end

  describe "merging" do
    before do
      @item = DefinitionItem.new('status_id', 'is', '2')
    end

    describe "#merge!" do
      describe "with the same operator, source" do
        before { @item.merge!(DefinitionItem.new('status_id', 'is', '4')) }

        it "updates the value" do
          assert_equal ['4'], @item.value
        end
      end

      describe "with a different operator" do
        before { @item.merge!(DefinitionItem.new('status_id', 'is_not', '4')) }

        it "does not update the value" do
          assert_equal ['2'], @item.value
        end
      end

      describe "with a different source" do
        before { @item.merge!(DefinitionItem.new('assignee_id', 'is', '4')) }

        it "does not update the value" do
          assert_equal ['2'], @item.value
        end
      end
    end
  end

  describe "group specific?" do
    before do
      @definition_item = DefinitionItem.new('recipient', 'is')
    end

    it "is true when querying for a group (using group_id or current_groups)" do
      group_specific_values = ['group_id', 'current_groups', ' current_groups ', [' current_groups '], 'CURRENT_groups']

      group_specific_values.each do |group_specific_value|
        @definition_item.value = group_specific_value
        assert(@definition_item.group_specific?, "Failed at #{group_specific_value.inspect}")
      end
    end

    it "is false when not querying for a group" do
      not_group_specific_values = [nil, 'not group_id', 'not current_groups']

      not_group_specific_values.each do |not_group_specific_value|
        @definition_item.value = not_group_specific_value
        assert_equal false, @definition_item.group_specific?, "Failed at #{not_group_specific_value.inspect}"
      end
    end
  end

  describe "user specific?" do
    before do
      @definition_item = DefinitionItem.new('recipient', 'is')
    end

    it "is true when querying for the current user'" do
      user_specific_values = ['current_user', ' current_user ', [' current_user ']]

      user_specific_values.each do |user_specific_value|
        @definition_item.value = user_specific_value
        assert(@definition_item.user_specific?, "Failed at #{user_specific_value.inspect}")
      end
    end

    it "is false when not querying for the current user" do
      not_user_specific_values = [nil, 'not current_user']

      not_user_specific_values.each do |not_user_specific_value|
        @definition_item.value = not_user_specific_value
        assert_equal false, @definition_item.user_specific?, "Failed at #{not_user_specific_value.inspect}"
      end
    end
  end

  it "renders proper humanized definitions" do
    assert_equal ["Comment is... is present (public or private)",
                  "Assignee is not (current user)",
                  "Assignee is not (requester)",
                  "Assignee Not changed (blank)",
                  "Status not changed from Solved",
                  "Email user is (assignee)"],
      rules(:trigger_notify_assignee_of_comment_update).definition.items.collect { |i| i.humanize(accounts(:minimum)).join(' ') }

    assert_equal ["Status is Open",
                  "Priority is Normal",
                  "Type is Question",
                  "Response/description  [{{ticket.account}}] {{ticket.requester.name}},\n\nYour request has been escalated to incident.\n\nCheers,\n{{current_user.name}},\n\n--------------------\nView status of your ticket at {{ticket.link}}",
                  "Group is N/A",
                  "Comment mode is Private",
                  "Assignee is (current user)"],
      rules(:macro_incident_escalation).definition.items.collect { |i| i.humanize(accounts(:minimum)).join(' ') }

    assert_equal ["Assignee is not (blank)",
                  "Status is not Solved",
                  "Status is not Closed",
                  "Description is not this_word_is_not_in_any_ticket",
                  "Description contains none of the following this_string_is_not_in_any_ticket"],
      rules(:view_assigned_working_tickets).definition.items.collect { |i| i.humanize(accounts(:minimum)).join(' ') }

    assert_equal ["Assignee changed (blank)",
                  "Assignee is not (current user)",
                  "Email user is (blank)"],
      rules(:trigger_notify_blank_email).definition.items.collect { |i| i.humanize(accounts(:minimum)).join(' ') }
  end

  describe "memoization_key" do
    let(:ticket) { tickets(:minimum_1) }
    let(:item1_memoization_key) { item1.memoization_key(ticket) }
    let(:item2_memoization_key) { item2.memoization_key(ticket) }

    describe "when source is 'description_includes_word'" do
      let(:item1) {  DefinitionItem.new('description_includes_word', 'is', '2') }

      describe "same source, operator and value" do
        let(:item2) { DefinitionItem.new('description_includes_word', 'is', '2') }

        it "is equal" do
          assert_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different source" do
        let(:item2) { DefinitionItem.new('comment_includes_word', 'is', '2') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different operator" do
        let(:item2) { DefinitionItem.new('description_includes_word', 'is_not', '2') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different value" do
        let(:item2) { DefinitionItem.new('description_includes_word', 'is', '4') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end
    end

    describe "when source is 'subject_includes_word'" do
      let(:item1) {  DefinitionItem.new('subject_includes_word', 'is', '2') }

      describe "same source, operator and value" do
        let(:item2) { DefinitionItem.new('subject_includes_word', 'is', '2') }

        it "is equal" do
          assert_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different source" do
        let(:item2) { DefinitionItem.new('description_includes_word', 'is', '2') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different operator" do
        let(:item2) { DefinitionItem.new('subject_includes_word', 'is_not', '2') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different value" do
        let(:item2) { DefinitionItem.new('subject_includes_word', 'is', '4') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end
    end

    describe "when source is 'comment_includes_word'" do
      let(:item1) {  DefinitionItem.new('comment_includes_word', 'is', '2') }

      describe "same source, operator and value" do
        let(:item2) { DefinitionItem.new('comment_includes_word', 'is', '2') }

        it "is equal" do
          assert_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different source" do
        let(:item2) { DefinitionItem.new('description_includes_word', 'is', '2') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different operator" do
        let(:item2) { DefinitionItem.new('comment_includes_word', 'is_not', '2') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different value" do
        let(:item2) { DefinitionItem.new('comment_includes_word', 'is', '4') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end
    end

    describe "when source is 'current_tags'" do
      let(:item1) {  DefinitionItem.new('current_tags', 'is', '2') }

      describe "same source, operator and value" do
        let(:item2) { DefinitionItem.new('current_tags', 'is', '2') }

        it "is equal" do
          assert_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different source" do
        let(:item2) { DefinitionItem.new('description_includes_word', 'is', '2') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different operator" do
        let(:item2) { DefinitionItem.new('current_tags', 'is_not', '2') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end

      describe "different value" do
        let(:item2) { DefinitionItem.new('current_tags', 'is', '4') }

        it "is not equal" do
          refute_equal item2_memoization_key, item1_memoization_key
        end
      end
    end
  end

  describe "memoizable?" do
    describe "when source is 'description_includes_word'" do
      it "is memoizable" do
        assert DefinitionItem.new('description_includes_word', 'is', ['foo']).memoizable?
      end
    end

    describe "when source is 'subject_includes_word'" do
      it "is memoizable" do
        assert DefinitionItem.new('subject_includes_word', 'is', ['foo']).memoizable?
      end
    end

    describe "when source is 'comment_includes_word'" do
      it "is memoizable" do
        assert DefinitionItem.new('comment_includes_word', 'is', ['foo']).memoizable?
      end
    end

    describe "when source is 'current_tags'" do
      it "is memoizable" do
        assert DefinitionItem.new('current_tags', 'is', ['foo']).memoizable?
      end
    end
  end

  describe "nullifies?" do
    it "nullifies 'is'" do
      @status_id_is_4 = DefinitionItem.new('status_id', 'is', ['4'])
      @status_id_is_0 = DefinitionItem.new('status_id', 'is', ['0'])
      assert @status_id_is_4.nullifies?(@status_id_is_0)
      assert @status_id_is_0.nullifies?(@status_id_is_4)
      refute @status_id_is_4.nullifies?(@status_id_is_4)
      refute @status_id_is_0.nullifies?(@status_id_is_0)
    end

    it "nullifies 'less_than'" do
      @status_id_less_than_2 = DefinitionItem.new('status_id', 'less_than', ['2'])
      @status_id_is_0 = DefinitionItem.new('status_id', 'is', ['0'])
      @status_id_is_2 = DefinitionItem.new('status_id', 'is', ['2'])
      @status_id_is_4 = DefinitionItem.new('status_id', 'is', ['4'])
      refute @status_id_is_0.nullifies?(@status_id_less_than_2)
      assert @status_id_is_2.nullifies?(@status_id_less_than_2)
      assert @status_id_is_4.nullifies?(@status_id_less_than_2)
    end

    it "nullifies 'greater_than'" do
      @status_id_greater_than_2 = DefinitionItem.new('status_id', 'greater_than', ['2'])
      @status_id_is_0 = DefinitionItem.new('status_id', 'is', ['0'])
      @status_id_is_2 = DefinitionItem.new('status_id', 'is', ['2'])
      @status_id_is_4 = DefinitionItem.new('status_id', 'is', ['4'])
      assert @status_id_is_0.nullifies?(@status_id_greater_than_2)
      assert @status_id_is_2.nullifies?(@status_id_greater_than_2)
      refute @status_id_is_4.nullifies?(@status_id_greater_than_2)
    end

    it "nullifies 'is_not'" do
      @status_id_is_not_3 = DefinitionItem.new('status_id', 'is_not', ['3'])
      @status_id_is_0 = DefinitionItem.new('status_id', 'is', ['0'])
      @status_id_is_3 = DefinitionItem.new('status_id', 'is', ['3'])
      @status_id_is_4 = DefinitionItem.new('status_id', 'is', ['4'])
      refute @status_id_is_0.nullifies?(@status_id_is_not_3)
      refute @status_id_is_4.nullifies?(@status_id_is_not_3)
      assert @status_id_is_3.nullifies?(@status_id_is_not_3)
    end

    it "nullifies 'includes'" do
      @current_tags_includes_123_456 = DefinitionItem.new('current_tags', 'includes', ['123 456'])

      # set tags
      @set_tags_123 = DefinitionItem.new('set_tags', nil, ['123'])
      @set_tags_abc = DefinitionItem.new('set_tags', nil, ['abc'])
      refute @set_tags_123.nullifies?(@current_tags_includes_123_456)
      assert @set_tags_abc.nullifies?(@current_tags_includes_123_456)

      # add tags
      @add_tags_123 = DefinitionItem.new('current_tags', nil, ['123'])
      @add_tags_abc = DefinitionItem.new('current_tags', nil, ['abc'])
      refute @add_tags_123.nullifies?(@current_tags_includes_123_456)
      refute @add_tags_abc.nullifies?(@current_tags_includes_123_456)

      # remove tags
      @remove_tags_abc = DefinitionItem.new('remove_tags', nil, ['abc'])
      @remove_tags_123 = DefinitionItem.new('remove_tags', nil, ['123'])
      @remove_tags_123_456 = DefinitionItem.new('remove_tags', nil, ['123 456'])
      refute @remove_tags_abc.nullifies?(@current_tags_includes_123_456)
      refute @remove_tags_123.nullifies?(@current_tags_includes_123_456)
      assert @remove_tags_123_456.nullifies?(@current_tags_includes_123_456)
    end

    it "nullifies 'not_includes'" do
      @current_tags_not_includes_123_456 = DefinitionItem.new('current_tags', 'not_includes', ['123 456'])

      # set tags
      @set_tags_123 = DefinitionItem.new('set_tags', nil, ['123'])
      @set_tags_abc = DefinitionItem.new('set_tags', nil, ['abc'])
      assert @set_tags_123.nullifies?(@current_tags_not_includes_123_456)
      refute @set_tags_abc.nullifies?(@current_tags_not_includes_123_456)

      # add tags
      @add_tags_123 = DefinitionItem.new('current_tags', nil, ['123'])
      @add_tags_abc = DefinitionItem.new('current_tags', nil, ['abc'])
      assert @add_tags_123.nullifies?(@current_tags_not_includes_123_456)
      refute @add_tags_abc.nullifies?(@current_tags_not_includes_123_456)

      # remove tags
      @remove_tags_123 = DefinitionItem.new('remove_tags', nil, ['123'])
      @remove_tags_abc = DefinitionItem.new('remove_tags', nil, ['abc'])
      refute @remove_tags_123.nullifies?(@current_tags_not_includes_123_456)
      refute @remove_tags_abc.nullifies?(@current_tags_not_includes_123_456)
    end

    it "does not nullify when both 'less_than'" do
      @status_id_less_than_2 = DefinitionItem.new('status_id', 'less_than', ['2'])
      @status_id_less_than_3 = DefinitionItem.new('status_id', 'less_than', ['3'])
      refute @status_id_less_than_3.nullifies?(@status_id_less_than_2)
    end

    it "does not nullify when different source" do
      @status_id_is_4 = DefinitionItem.new('status_id', 'is', ['4'])
      @priority_id_is_0 = DefinitionItem.new('priority_id', 'is', ['0'])
      refute @status_id_is_4.nullifies?(@priority_id_is_0)
      refute @priority_id_is_0.nullifies?(@status_id_is_4)
    end

    it "nullifies 'group_id' and 'group_id#current_groups'" do
      @group_1 = DefinitionItem.new('group_id', 'is', ['1'])
      @group_2 = DefinitionItem.new('group_id#current_groups', 'is', ['2'])
      assert @group_1.nullifies?(@group_2)
      assert @group_2.nullifies?(@group_1)
    end

    describe "when the condition's operator is 'not_present'" do
      let(:condition) { DefinitionItem.new('custom_field', 'not_present') }

      describe 'and the action sets a value' do
        let(:action) { DefinitionItem.new('custom_field', 'is', ['1']) }

        it "nullifies 'not_present'" do
          assert action.nullifies?(condition)
        end
      end

      describe 'and the action does not set a value' do
        let(:action) { DefinitionItem.new('custom_field', 'is') }

        it "does not nullify 'not_present'" do
          refute action.nullifies?(condition)
        end
      end
    end

    describe 'when the source is `satisfaction_score`' do
      let(:source)    { 'satisfaction_score' }
      let(:operator)  { 'is' }
      let(:condition) { DefinitionItem.new(source, operator, condition_value) }
      let(:action)    do
        DefinitionItem.new(source, 'is', [SatisfactionType.OFFERED.to_s])
      end

      describe 'and the operator is `is`' do
        let(:operator) { 'is' }

        describe 'and the condition value is `UNOFFERED`' do
          let(:condition_value) { [SatisfactionType.UNOFFERED.to_s] }

          it 'returns true' do
            assert action.nullifies?(condition)
          end
        end

        describe 'and the condition value is not `UNOFFERED`' do
          let(:condition_value) { [SatisfactionType.OFFERED.to_s] }

          it 'returns false' do
            refute action.nullifies?(condition)
          end
        end
      end

      describe 'and the operator is `less_than`' do
        let(:operator) { 'less_than' }

        describe 'and the condition value is `OFFERED`' do
          let(:condition_value) { [SatisfactionType.OFFERED.to_s] }

          it 'returns true' do
            assert action.nullifies?(condition)
          end
        end

        describe 'and the condition value is not `OFFERED`' do
          let(:condition_value) { [SatisfactionType.GOOD.to_s] }

          it 'returns false' do
            refute action.nullifies?(condition)
          end
        end
      end

      describe 'and the operator is not `is` nor `less_than`' do
        let(:operator)        { 'greater_than' }
        let(:condition_value) { [SatisfactionType.UNOFFERED.to_s] }

        it 'returns false' do
          refute action.nullifies?(condition)
        end
      end
    end
  end

  describe '#to_h' do
    it 'returns a hash representation of the definition item' do
      assert_equal(
        {source: 'status_id', operator: 'is', value: ['2']},
        DefinitionItem.new('status_id', 'is', '2').to_h
      )
    end
  end
end
