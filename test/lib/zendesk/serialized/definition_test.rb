require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 10

describe Definition do
  fixtures :accounts, :organizations

  let(:params) do
    {
      conditions_all: [
        {operator: "less_than",
         value: {"0" => "2"},
         source: "status_id"}
      ],
      conditions_any: [
        {operator: "is",
         value: {"0" => "current_groups"},
         source: "group_id"}
      ],
      actions: [
        {operator: "is_not",
         value: {"0" => ""},
         source: "group_id"}
      ]
    }
  end

  describe "merging" do
    let(:definition) { Definition.build(params) }

    describe "merge" do
      subject do
        definition.merge(
          Definition.build(
            actions: [{operator: "is_not", value: "4", source: "group_id" }]
          )
        )
      end

      it "only merge" do
        assert_equal 1, subject.actions.size
      end

      it "creates a new definition" do
        assert_not_equal definition.object_id, subject.object_id
      end

      it "creates new definition items" do
        assert_not_equal definition.items.map(&:object_id).sort,
          subject.items.map(&:object_id).sort
      end

      it "merges two conditions" do
        action = subject.actions.first

        assert_equal "is_not", action.operator
        assert_equal ["4"], action.value
        assert_equal "group_id", action.source
      end
    end

    describe "merge!" do
      let!(:original_items) { definition.items.dup }

      before do
        definition.merge!(
          Definition.build(
            actions: [{ operator: "is_not", value: "4", source: "group_id" }]
          )
        )
      end

      it "does not create new definition items" do
        assert_equal original_items.map(&:object_id).sort,
          definition.items.map(&:object_id).sort
      end

      it "merges two conditions" do
        action = definition.actions.first

        assert_equal "is_not", action.operator
        assert_equal ["4"], action.value
        assert_equal "group_id", action.source
      end
    end

    describe "add" do
      subject do
        definition.merge(
          Definition.build(
            actions: [{ operator: "is_not", value: "4", source: "user_id" }]
          )
        )
      end

      it "adds new action" do
        assert_equal 2, subject.actions.size
      end
    end
  end

  describe "building from parameters" do
    describe "from api/v2/rules/views/preview" do
      let(:definition) { Definition.build(subject) }
      let(:item)       { definition.conditions_all.first }

      describe "with named status" do
        subject do
          { conditions_all: [{operator: "is", value: "NEW", field: "status"}] }
        end

        it "maps condition fields" do
          assert_equal 'status_id', item.source
        end

        it "maps condition values" do
          assert_equal [StatusType.NEW.to_s], item.value
        end
      end

      describe "with normal parameters" do
        subject do
          {conditions_all: [{operator: "is",
                             value: StatusType.NEW,
                             field: "status_id"}]}
        end

        it "does not map condition fields" do
          assert_equal 'status_id', item.source
        end

        it "does not map condition values" do
          assert_equal [StatusType.NEW], item.value
        end
      end
    end

    it "builds conditions_all definition items" do
      definition = Definition.build(params)
      item       = definition.conditions_all.first

      assert_equal 'status_id', item.source
      assert_equal 'less_than', item.operator
      assert_equal ['2'], item.value
    end

    it "builds conditions_any definition items" do
      definition = Definition.build(params)
      item       = definition.conditions_any.first

      assert_equal 'group_id',           item.source
      assert_equal 'is',                 item.operator
      assert_equal ["current_groups"], item.value
    end

    it "builds actions definition items" do
      definition = Definition.build(params)
      item       = definition.actions.first

      assert_equal 'group_id', item.source
      assert_equal 'is_not',   item.operator
      assert_equal [''], item.value
    end

    it "sets up an empty array when missing a value" do
      params[:conditions_all].first.delete(:value)
      definition = Definition.build(params)

      item = definition.conditions_all.first
      assert_equal 'status_id', item.source
      assert_equal 'less_than', item.operator
      assert_equal [], item.value
    end

    it "rejects PICKCONDITONS and PICKACTIONS sources" do
      params[:conditions_all].first[:source] = 'PICKCONDITION'
      definition = Definition.build(params)

      assert_empty(definition.conditions_all)

      params[:actions].first[:source] = 'PICKACTION'
      definition = Definition.build(params)

      assert_empty(definition.actions)
    end

    it "rejects empty sources" do
      params[:conditions_all].first[:source] = ''
      definition = Definition.build(params)

      assert_empty(definition.conditions_all)
    end

    it "does not blow up on nil items" do
      params.delete(:conditions_all)
      definition = Definition.build(params)

      assert_equal [], definition.conditions_all
    end
  end

  describe 'when building from a ticket' do
    let(:account)  { accounts(:minimum) }
    let(:user)     { users(:minimum_admin) }
    let(:assignee) { users(:minimum_agent) }
    let(:group)    { groups(:minimum_group) }

    let(:ticket) do
      Ticket.new do |ticket|
        ticket.account        = account
        ticket.status_id      = StatusType.OPEN
        ticket.priority_id    = PriorityType.HIGH
        ticket.ticket_type_id = TicketType.INCIDENT
        ticket.assignee       = assignee
        ticket.group          = group
        ticket.current_tags   = 'tea'
      end
    end

    let(:comment) do
      Comment.new(
        body:      'hi',
        is_public: true,
        account:   account,
        author:    user
      )
    end

    let(:definition) { Definition.from_ticket(ticket, user) }
    let(:actions)    { definition.grouped_actions }

    before do
      account.stubs(has_extended_ticket_types?: true)

      ticket.stubs(comments: [comment])
    end

    it 'adds actions based on the ticket' do
      assert_equal [StatusType.OPEN],     actions[:status_id].first.value
      assert_equal [PriorityType.HIGH],   actions[:priority_id].first.value
      assert_equal [TicketType.INCIDENT], actions[:ticket_type_id].first.value
      assert_equal [assignee.id],         actions[:assignee_id].first.value
      assert_equal [group.id],            actions[:group_id].first.value
      assert_equal ['tea'],               actions[:current_tags].first.value

      assert_equal [comment.html_body], actions[:comment_value_html].first.value
      assert_nil                        actions[:comment_value]
      assert_equal(
        ['true'],
        actions[:comment_mode_is_public].first.value
      )
    end

    describe 'and it is a new ticket' do
      before { ticket.status_id = StatusType.NEW }

      it 'does not create an action from the status' do
        assert_nil actions[:status_id]
      end
    end

    describe 'and the ticket has the default priority' do
      before do
        ticket.priority_id = Zendesk::Types::PriorityType.find(
          Zendesk::Types::PriorityType.default_placeholder
        )
      end

      it 'does not create an action from the priority' do
        assert_nil actions[:priority_id]
      end
    end

    describe 'and the ticket does not have extended ticket types available' do
      before { account.stubs(has_extended_ticket_types?: false) }

      it 'does not create an action from the ticket type' do
        assert_nil actions[:ticket_type_id]
      end
    end

    describe 'and the ticket has the default ticket type' do
      before do
        ticket.ticket_type_id = Zendesk::Types::TicketType.find(
          Zendesk::Types::TicketType.default_placeholder
        )
      end

      it 'does not create an action from the ticket type' do
        assert_nil actions[:ticket_type_id]
      end
    end

    describe 'and there is no assignee' do
      before { ticket.assignee = nil }

      it 'does not create an action from the assignee' do
        assert_nil actions[:assignee]
      end
    end

    describe 'and there is no group' do
      before { ticket.group_id = nil }

      it 'does not create an action from the group' do
        assert_nil actions[:group_id]
      end
    end

    describe 'and there are no tags' do
      before { ticket.current_tags = [] }

      it 'does not create an action from the tags' do
        assert_nil actions[:set_current_tags]
      end
    end

    describe 'and the comment author is not the current user' do
      before { comment.author = User.new }

      it 'does not create an action from the comment' do
        assert_nil actions[:comment_value]
        assert_nil actions[:comment_value_html]
        assert_nil actions[:comment_mode_is_public]
      end
    end

    describe 'and the account does not have rich text comments' do
      before { user.account.settings.stubs(rich_text_comments?: false) }

      it 'creates an action from the comment value' do
        assert_equal [comment.body], actions[:comment_value].first.value
        assert_equal ['true'], actions[:comment_mode_is_public].first.value

        assert_nil actions[:comment_value_html]
      end
    end
  end

  describe "sorted conditions" do
    before { silence_warnings { Definition.const_set(:CONDITIONS_PRIORITY_MAP, Definition::CONDITIONS_PRIORITY_MAP.dup) } }

    let(:conditions) do
      [
        {
          operator: "not_includes",
          value: {"0" => ["None Of These"]},
          source: "comment_includes_word"
        },
        {
          operator: "less_than",
          value: {"0" => "2"},
          source: "status_id"
        }
      ]
    end

    describe "#sorted_conditions_all" do
      after { Definition::CONDITIONS_PRIORITY_MAP["status_id"] = 10 }

      it "returns the conditions in order of preference" do
        params = { conditions_all: conditions }
        definition = Definition.build(params)

        assert_equal "status_id", definition.sorted_conditions_all[0].sanitized_source

        Definition::CONDITIONS_PRIORITY_MAP["status_id"] = 1000
        definition = Definition.build(params)

        assert_equal "comment_includes_word", definition.sorted_conditions_all[0].sanitized_source
      end

      describe "when there are immutable conditions" do
        before { conditions << { operator: "is", value: "foo", source: "recipient" } }

        it "returns the conditions in order of preference" do
          params = { conditions_all: conditions }
          definition = Definition.build(params)

          assert_equal "recipient", definition.sorted_conditions_all[0].sanitized_source
          assert_equal "status_id", definition.sorted_conditions_all[1].sanitized_source
        end
      end
    end

    describe "#sorted_conditions_any" do
      after { Definition::CONDITIONS_PRIORITY_MAP["status_id"] = 10 }

      it "returns the conditions in order of preference" do
        params = { conditions_any: conditions }
        definition = Definition.build(params)

        assert_equal "status_id", definition.sorted_conditions_any[0].sanitized_source

        Definition::CONDITIONS_PRIORITY_MAP["status_id"] = 1000
        definition = Definition.build(params)

        assert_equal "comment_includes_word", definition.sorted_conditions_any[0].sanitized_source
      end
    end
  end

  describe "#==" do
    it "returns true when the contents of all the condition collections are the same" do
      assert_equal Definition.build(params), Definition.build(params)
    end

    it "returns false when the contents of any of the condition collections are different" do
      new_params = params.merge(actions: params[:actions].values_at(0..-2))

      refute_equal Definition.build(params), Definition.build(new_params)
    end
  end

  describe "#cache_key" do
    it "returns true when the contents of all the condition collections are the same" do
      assert_equal Definition.build(params).cache_key, Definition.build(params).cache_key
    end

    it "returns false when the contents are the same but in different orders" do
      new_params = params.dup
      new_params[:conditions_all], new_params[:conditions_any] = new_params[:conditions_any], new_params[:conditions_all]

      refute_equal Definition.build(params).cache_key, Definition.build(new_params).cache_key
    end

    it "returns false when all is nil, but any has conditions" do
      condition = { operator: "is", value: {"0" => "current_groups"}, source: "group_id" }
      left_params = { conditions_all: [], conditions_any: [condition], actions: [] }
      right_params = { conditions_all: [condition], conditions_any: [], actions: [] }

      refute_equal Definition.build(left_params).cache_key, Definition.build(right_params).cache_key
    end

    it "returns false when the contents of any of the condition collections are different" do
      new_params = params.merge(actions: params[:actions].values_at(0..-2))

      refute_equal Definition.build(params).cache_key, Definition.build(new_params).cache_key

      new_params = params.merge(conditions_all: params[:conditions_all].values_at(0..-2))

      refute_equal Definition.build(params).cache_key, Definition.build(new_params).cache_key

      new_params = params.merge(conditions_any: params[:conditions_any].values_at(0..-2))

      refute_equal Definition.build(params).cache_key, Definition.build(new_params).cache_key
    end
  end

  describe "#serialize_for_cia" do
    it "returns an empty representation for a Definition with no DefinitionItems" do
      expected = [[], [], []].to_json

      assert_equal expected, Definition.new.serialize_for_cia
    end

    it "returns the correct representation for a Definition with multiple DefinitionItems" do
      expected = [
        ["status_id less_than 2"],
        ["group_id is current_groups"],
        ["group_id is_not "]
      ].to_json

      assert_equal expected, Definition.build(params).serialize_for_cia
    end

    it "returns only the first item in the value array" do
      expected = [
        ["status_id less_than 2"],
        ["group_id is current_groups"],
        ["group_id is_not ", "notification_user is requester_id"]
      ].to_json

      params[:actions] << {
        operator: "is",
        source: "notification_user",
        value: {"0" => "requester_id",
                "1" => "blah blah"}
      }

      assert_equal expected, Definition.build(params).serialize_for_cia
    end

    it "does not raise an error when the value parameter is not an array" do
      expected = [
        ["status_id less_than 2"],
        ["group_id is current_groups"],
        ["group_id is_not ", "notification_user is 0"]
      ].to_json

      params[:actions] << {
        operator: "is",
        source: "notification_user",
        value: "0"
      }

      assert_equal expected, Definition.build(params).serialize_for_cia
    end

    it "ensures that the character count is less than 255" do
      definition_item_hash = {
        operator: "is",
        source: "notification_user",
        value: {"0" => "requester_id"}
      }

      3.times do
        params[:actions]        << definition_item_hash
        params[:conditions_all] << definition_item_hash
        params[:conditions_any] << definition_item_hash
      end

      json = Definition.build(params).serialize_for_cia
      assert_operator json.size, :<, 255
    end

    it "does not truncate the last element in conditions_all, conditions_any or actions while removing elements" do
      expected = [
        ["status_id less_than 2"],
        [],
        ["group_id is_not ",
         "notification_user is requester_id",
         "notification_user is requester_id",
         "notification_user is requester_id",
         "notification_user is requester_id",
         "notification_user is requester_id"]
      ].to_json

      params[:conditions_any] = []
      9.times do
        params[:actions] << {
          operator: "is",
          source: "notification_user",
          value: {"0" => "requester_id"}
        }
      end

      assert_equal expected, Definition.build(params).serialize_for_cia
    end
  end

  describe '#basic_comment_actions' do
    let(:definition) do
      Definition.new.tap do |definition|
        definition.actions.push(
          DefinitionItem.new('requester_id', 'is', 'current_user'),
          DefinitionItem.new('comment_value', 'is', 'Some comment'),
          DefinitionItem.new('comment_is_public', 'is', true),
          DefinitionItem.new('comment_value_html', 'is', '<p>So rich</p>'),
          DefinitionItem.new('comment_value', 'is', 'Other comment')
        )
      end
    end

    it 'returns the comment actions' do
      assert_equal(
        [
          DefinitionItem.new('comment_value', 'is', 'Some comment'),
          DefinitionItem.new('comment_value', 'is', 'Other comment')
        ],
        definition.basic_comment_actions
      )
    end
  end

  describe '#rich_comment_actions' do
    let(:definition) do
      Definition.new.tap do |definition|
        definition.actions.push(
          DefinitionItem.new('requester_id', 'is', 'current_user'),
          DefinitionItem.new('comment_value', 'is', 'Some comment'),
          DefinitionItem.new('comment_is_public', 'is', true),
          DefinitionItem.new('comment_value_html', 'is', '<p>So rich</p>'),
          DefinitionItem.new('comment_value', 'is', 'Other comment')
        )
      end
    end

    it 'returns the rich comment actions' do
      assert_equal(
        [DefinitionItem.new('comment_value_html', 'is', '<p>So rich</p>')],
        definition.rich_comment_actions
      )
    end
  end

  describe '#has_side_conversation_action?' do
    let(:definition) do
      Definition.new.tap do |definition|
        definition.actions.push(*actions)
      end
    end

    describe 'when a side conversation action is present' do
      let(:actions) do
        [
          DefinitionItem.new('requester_id', 'is', 'current_user'),
          DefinitionItem.new('side_conversation_slack', 'is', 'somethingsomething')
        ]
      end

      it 'returns true' do
        assert definition.has_side_conversation_action?
      end
    end

    describe 'when a side conversation action is not present' do
      let(:actions) do
        [
          DefinitionItem.new('requester_id', 'is', 'current_user'),
          DefinitionItem.new('comment_value_html', 'is', '<p>So rich</p>')
        ]
      end

      it 'returns false' do
        refute definition.has_side_conversation_action?
      end
    end
  end

  describe '#has_rich_comment_action?' do
    let(:definition) do
      Definition.new.tap do |definition|
        definition.actions.push(*actions)
      end
    end

    describe 'when a rich comment action is present' do
      let(:actions) do
        [
          DefinitionItem.new('requester_id', 'is', 'current_user'),
          DefinitionItem.new('comment_value_html', 'is', '<p>So rich</p>')
        ]
      end

      it 'returns true' do
        assert definition.has_rich_comment_action?
      end
    end

    describe 'when a rich comment action is not present' do
      let(:actions) do
        [
          DefinitionItem.new('requester_id', 'is', 'current_user'),
          DefinitionItem.new('comment_value', 'is', 'Some comment'),
          DefinitionItem.new('comment_is_public', 'is', true)
        ]
      end

      it 'returns false' do
        refute definition.has_rich_comment_action?
      end
    end
  end

  describe '#change_conditions?' do
    let(:definition) do
      Definition.new.tap do |definition|
        definition.conditions_all.push(*conditions)
      end
    end

    describe 'when at least one condition has a source of `update_type`' do
      let(:conditions) do
        [
          DefinitionItem.new('priority_id', 'is', PriorityType.HIGH),
          DefinitionItem.new('update_type', 'is', condition_value)
        ]
      end

      describe 'and the value is a string' do
        describe 'and it is `Change`' do
          let(:condition_value) { 'Change' }

          it 'returns true' do
            assert definition.change_conditions?
          end
        end

        describe 'and it is not `Change`' do
          let(:condition_value) { 'Create' }

          it 'returns false' do
            refute definition.change_conditions?
          end
        end
      end

      describe 'and the value is an array' do
        describe 'and the first element is `Change`' do
          let(:condition_value) { ['Change'] }

          it 'returns true' do
            assert definition.change_conditions?
          end
        end

        describe 'and the first element is not `Change`' do
          let(:condition_value) { ['Create'] }

          it 'returns false' do
            refute definition.change_conditions?
          end
        end
      end
    end

    describe 'when no conditions have source `update_type`' do
      let(:conditions) do
        [DefinitionItem.new('priority_id', 'is', PriorityType.LOW)]
      end

      it 'returns false' do
        refute definition.change_conditions?
      end
    end
  end

  describe '#satisfaction_prediction_condition?' do
    let(:definition) do
      Definition.new.tap do |definition|
        definition.conditions_all.push(*conditions)
      end
    end

    describe 'when a condition source is `satisfaction_probability`' do
      let(:conditions) do
        [DefinitionItem.new('satisfaction_probability', 'is', 0.9)]
      end

      it 'returns true' do
        assert definition.satisfaction_prediction_conditions?
      end
    end

    describe 'when a condition source is `current_via_id`' do
      let(:conditions) do
        [DefinitionItem.new('current_via_id', 'is', condition_value)]
      end

      describe 'and the value is the satisfaction prediction via type' do
        let(:condition_value) { [ViaType.SATISFACTION_PREDICTION.to_s] }

        it 'returns true' do
          assert definition.satisfaction_prediction_conditions?
        end
      end

      describe 'and the value is not the satisfaction prediction via type' do
        let(:condition_value) { [ViaType.WEB_FORM.to_s] }

        it 'returns false' do
          refute definition.satisfaction_prediction_conditions?
        end
      end
    end

    describe 'when no conditions have either source' do
      let(:conditions) do
        [
          DefinitionItem.new('priority_id', 'is', PriorityType.LOW),
          DefinitionItem.new('status_id', 'is', StatusType.OPEN)
        ]
      end

      it 'returns false' do
        refute definition.satisfaction_prediction_conditions?
      end
    end
  end

  describe "run_new_validations" do
    let(:account) { accounts(:minimum) }
    let(:errors) { stub('errors') }
    let(:container) { stub('container', errors: errors) }
    let(:current_user) { stub('current user') }

    describe "valid conditions" do
      it "does not record an error" do
        definition = Definition.build(conditions_all: [{ field: 'status_id', operator: 'greater_than', value: [StatusType.NEW]}])
        errors.expects(:add).never
        definition.run_new_validations(account, 'View', container, current_user)
      end
    end

    describe "invalid conditions" do
      it "does not allow a rule with a definition of status id less than new" do
        definition = Definition.build(conditions_all: [{ field: 'status_id', operator: 'less_than', value: [StatusType.NEW]}])
        errors.expects(:add).with(:base, "Invalid value 'New' in 'Status / Less than / New'")
        definition.run_new_validations(account, 'View', container, current_user)
      end

      it "does not allow a rule with a definition of status id greater than closed" do
        definition = Definition.build(
          conditions_all: [{field: 'status_id', operator: 'greater_than', value: [StatusType.CLOSED]}]
        )
        errors.expects(:add).with(:base, "Invalid value 'Closed' in 'Status / Greater than / Closed'")
        definition.run_new_validations(account, 'View', container, current_user)
      end

      it "does not allow a rule with a definition of group_id from a deleted group" do
        definition = Definition.build(
          conditions_all: [{field: 'group_id', operator: 'is', value: [123456]}]
        )
        errors.expects(:add).with(:base, "Group 123456 was deleted and cannot be used")
        definition.run_new_validations(account, 'View', container, current_user)
      end

      it "does not allow a rule with a definition of requester_id from a deleted user" do
        user = users(:minimum_end_user)
        user.update_attribute(:is_active, false)
        definition = Definition.build(
          conditions_all: [{field: 'requester_id', operator: 'is', value: [user.id]}]
        )
        errors.expects(:add).with(:base, "User #{user.id} was deleted and cannot be used")
        definition.run_new_validations(account, 'View', container, current_user)
      end

      it "does not allow a rule with a definition of assignee_id from a deleted user" do
        user = users(:minimum_end_user)
        user.update_attribute(:is_active, false)
        definition = Definition.build(
          conditions_all: [{field: 'assignee_id', operator: 'is', value: [user.id]}]
        )
        errors.expects(:add).with(:base, "User #{user.id} was deleted and cannot be used")
        definition.run_new_validations(account, 'View', container, current_user)
      end

      it "allows a rule with a definition of assignee_id from an agent that is not assignable if she is assigned any tickets (only applies to closed tickets)" do
        user = users(:minimum_agent)
        ActiveRecord::Relation.any_instance.stubs(:pluck).returns([])
        definition = Definition.build(
          conditions_all: [{field: 'assignee_id', operator: 'is', value: [user.id]}]
        )
        errors.expects(:add).never
        definition.run_new_validations(account, 'View', container, current_user)
      end

      it "does not allow a rule with a definition of organization_id from a deleted organization" do
        organization = organizations(:minimum_organization1)
        organization.update_attribute(:deleted_at, Time.now)
        definition = Definition.build(
          conditions_all: [{field: 'organization_id', operator: 'is', value: [organization.id]}]
        )
        current_user.expects(:agent_restriction?).at_least_once
        errors.expects(:add).with(:base, "Organization #{organization.id} was deleted and cannot be used")
        definition.run_new_validations(account, 'View', container, current_user)
      end

      it "allows querying for closed tickets assigned to a downgraded agent" do
        fired_agent = users(:minimum_end_user)
        definition = Definition.build(
          conditions_all: [{field: 'assignee', operator: 'is', value: [fired_agent.id]}]
        )
        errors.expects(:add).never
        definition.run_new_validations(account, 'View', container, current_user)
      end

      it "does not validate non-user-facing rule condition" do
        definition = Definition.build(
          conditions_all: [{field: 'group_is_set_with_no_assignee', operator: 'is', value: [123456]}]
        )
        ZendeskRules::Component::ComponentList.expects(:valid_definition?).never
        definition.run_new_validations(account, 'View', container, current_user)
      end
    end

    describe "user is supplied" do
      let(:user) { stub('user') }

      it "adds user to context" do
        definition = Definition.build(
          conditions_all: [{field: 'status_id', operator: 'greater_than', value: [StatusType.NEW]}]
        )
        definition.expects(:validate_item_list).with do |context, klass, all, account_arg, container_arg|
          (ZendeskRules::Definitions::Conditions == klass) &&
          (definition.conditions_all == all) &&
          (account == account_arg) &&
          (container == container_arg) &&
          (context.options[:component_type] == :condition) &&
          (context.options[:rule_type] == :view) &&
          (context.options[:validating] == true) &&
          (context.options[:condition_type] == :all)
        end

        definition.expects(:validate_item_list).with do |context, klass, any, account_arg, container_arg|
          (ZendeskRules::Definitions::Conditions == klass) &&
          (definition.conditions_any == any) &&
          (account == account_arg) &&
          (container == container_arg) &&
          (context.options[:component_type] == :condition) &&
          (context.options[:rule_type] == :view) &&
          (context.options[:validating] == true) &&
          (context.options[:condition_type] == :any)
        end

        definition.expects(:validate_item_list).with do |context, klass, actions, account_arg, container_arg|
          (ZendeskRules::Definitions::Actions == klass) &&
          (definition.actions == actions) &&
          (account == account_arg) &&
          (container == container_arg) &&
          (context.options[:rule_type] == :view) &&
          (context.options[:validating] == true) &&
          (context.options[:component_type] == :action)
        end

        definition.run_new_validations(account, 'View', container, user)
      end
    end

    describe "#build_error_target" do
      it "builds correctly from target_code" do
        definition = Definition.build(
          conditions_all: [{field: 'status_id', operator: 'greater_than', value: [StatusType.NEW]}]
        )
        assert_equal 'user', definition.send(:build_error_target, 'assignee_id')
        assert_equal 'organization', definition.send(:build_error_target, 'organization_id')
        assert_equal 'group', definition.send(:build_error_target, 'group_id')
      end
    end
  end
end
