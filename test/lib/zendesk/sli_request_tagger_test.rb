require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::SliRequestTagger do
  let(:tagger) { Zendesk::SliRequestTagger }
  after { Zendesk::SliRequestTagger.reset }

  describe '.set_tag' do
    it 'adds a tag to the list' do
      tagger.set_tag('my_tag', 10)
      assert_equal tagger.tags, ['my_tag:10']
      tagger.set_tag('my_second_tag', 20)
      assert_equal tagger.tags, ['my_tag:10', 'my_second_tag:20']
    end

    describe 'with repeated tags' do
      it 'only adds the latest value' do
        tagger.set_tag('my_tag', 10)
        tagger.set_tag('my_second_tag', 20)
        tagger.set_tag('my_tag', 50)
        assert_equal tagger.tags, ['my_tag:50', 'my_second_tag:20']
      end
    end
  end

  describe '.reset' do
    before { tagger.set_tag('my_tag', 10) }

    it 'deletes all tags from the list' do
      tagger.reset
      assert_equal tagger.tags, []
    end
  end
end
