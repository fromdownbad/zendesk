require_relative '../../../support/test_helper'
require_relative '../../../support/rule'
require_relative '../../../support/rules_test_helper'

SingleCov.covered!

describe Zendesk::SimplifiedEmailThreading::OptIn do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::DiffHelper

  fixtures :accounts, :groups, :rules

  let(:account) { accounts(:minimum) }
  let(:group)   { groups(:minimum_group) }
  let(:opt_in) { Zendesk::SimplifiedEmailThreading::OptIn.new(account.id) }

  let(:condition_1) do
    [
      DefinitionItem.new('comment_is_public', 'is', true)
    ]
  end

  let(:condition_2) do
    [
      DefinitionItem.new('group_id', 'is_not', '')
    ]
  end

  let(:original_placeholder_1)  { "{{ticket.comments_formatted}}" }
  let(:original_placeholder_2)  { "{{ticket.public_comments_formatted}}" }

  let(:replacement_placeholder_1)  { Zendesk::SimplifiedEmailThreading::OptIn::REPLACEMENT_PLACEHOLDER }
  let(:replacement_placeholder_2)  { Zendesk::SimplifiedEmailThreading::OptIn::PUBLIC_REPLACEMENT_PLACEHOLDER }

  let(:action_1) do
    [
      DefinitionItem.new('notification_user', 'is', ["assignee_id", "Email subject", "Email body: #{original_placeholder_1}"])
    ]
  end

  let(:action_2) do
    [
      DefinitionItem.new('notification_group', 'is', ["group_id", "Email subject", "Email body: #{original_placeholder_2}"])
    ]
  end

  let(:action_3) do
    [
      DefinitionItem.new('notification_user', 'is', ['group_id', 'Email subject', 'Email body: {{ticket.comments_formatted}} and {{ticket.latest_comment_formatted}}'])
    ]
  end

  let(:action_4) do
    [
      DefinitionItem.new('notification_group', 'is', ['group_id', 'Email subject', 'Email body: {{ticket.comments_formatted}} and {{ticket.latest_comment_formatted}}'])
    ]
  end

  let(:triggers_that_should_be_updated) do
    [{
         title: 'Notification User Trigger',
         definition: Definition.new(condition_1, [], action_1)
     },
     {
         title: 'Notification Group Trigger',
         definition: Definition.new(condition_2, [], action_2)
     },]
  end

  let(:triggers_that_should_not_be_updated) do
    [
      {
          title: 'Notification User Trigger',
          definition: Definition.new(condition_1, [], action_3)
      },
      {
          title: 'Notification Group Trigger',
          definition: Definition.new(condition_2, [], action_4)
      },

    ]
  end

  let(:standard_automation_conditions) do
    [DefinitionItem.new('status_id', 'less_than', [StatusType.CLOSED]),
     DefinitionItem.new('SOLVED', 'is', '24')]
  end

  let(:automations_that_should_be_updated) do
    [{
         title: 'Notification User Automation',
         definition: Definition.new(standard_automation_conditions, [], action_1)
     },
     {
         title: 'Notification Group Trigger',
         definition: Definition.new(standard_automation_conditions, [], action_2)
     }]
  end

  let(:automations_that_should_not_be_updated) do
    [{
         title: 'Notification User Automation',
         definition: Definition.new(standard_automation_conditions, [], action_3)
     },
     {
         title: 'Notification Group Trigger',
         definition: Definition.new(standard_automation_conditions, [], action_4)
     }]
  end

  let(:email_template_that_should_be_updated) do
    "Custom follower template for ({{ticket.id}}) {{ticket.latest_comment}}"
  end

  let(:updated_email_template) do
    "Custom follower template for ({{ticket.id}}) #{Zendesk::SimplifiedEmailThreading::OptIn::REPLACEMENT_PLACEHOLDER}"
  end

  let(:email_template_that_should_not_be_updated) do
    "Custom follower template for ({{ticket.id}}) {{ticket.latest_comment}} {{ticket.latest_comment}}"
  end

  let(:email_template_that_should_not_be_updated_v2) do
    "Custom follower template for ({{ticket.id}}) {{ticket.latest_comment}} {{ticket.latest_comment_rich}}"
  end

  let(:html_mail_template_that_should_be_updated) do
    "{{delimiter}} {{header}} {{content}} {{footer}}"
  end

  let(:html_mail_template_without_delimiter_that_should_be_updated) do
    "{{header}} {{content}} {{footer}}"
  end

  let(:updated_html_mail_template) do
    Zendesk::OutgoingMail::Variables::HTML_SIMPLIFIED_MAIL_TEMPLATE
  end

  let(:updated_html_mail_template_without_delimiter) do
    Zendesk::OutgoingMail::Variables::HTML_SIMPLIFIED_MAIL_TEMPLATE_WITHOUT_DELIMITER
  end

  let(:all_triggers) { triggers_that_should_be_updated + triggers_that_should_not_be_updated }
  let(:all_automations) { automations_that_should_be_updated + automations_that_should_not_be_updated }

  describe 'when opt in to Simplified Email Threading' do
    let(:statsd_client) { mock }
    let(:rule) { rules(:trigger_notify_requester_of_received_request) }

    before do
      account.simplified_email_opt_in_settings.create!(name: 'follower_template', value: updated_email_template)
      account.simplified_email_rule_bodies.create(rule_id: rule.id, rule_body: rule.definition.actions[0].value[2])
      Zendesk::SimplifiedEmailThreading::OptIn.any_instance.stubs(:statsd_client).returns(statsd_client)
    end

    it 'deletes all saved settings if it does not return empty lists' do
      assert_equal 1, account.simplified_email_opt_in_settings.count
      assert_equal 1, account.simplified_email_rule_bodies.count

      statsd_client.expects(:increment).with('destroyed_simplified_email_opt_in_settings').once
      statsd_client.expects(:increment).with('destroyed_simplified_email_rule_bodies').once

      opt_in.clear_previously_saved_settings(dry_run: false)

      assert_empty account.reload.simplified_email_opt_in_settings
      assert_empty account.reload.simplified_email_rule_bodies
    end

    it 'rolls back all changes if error raised within transaction' do
      Zendesk::SimplifiedEmailThreading::OptIn.any_instance.stubs(:clear_previously_saved_settings).raises(SomeOptInError)
      assert_raises(SomeOptInError) { opt_in.clear_previously_saved_settings(dry_run: false) }
      assert_equal 1, account.reload.simplified_email_opt_in_settings.count
      assert_equal 1, account.reload.simplified_email_rule_bodies.count
    end

    describe 'when running with dry_run: true' do
      it 'does not delete all saved settings' do
        opt_in.clear_previously_saved_settings(dry_run: true)

        assert_equal 1, account.reload.simplified_email_opt_in_settings.count
        assert_equal 1, account.reload.simplified_email_rule_bodies.count
      end
    end
  end

  describe 'when there are rules to update' do
    let(:args) do
      { dry_run: false }
    end

    before do
      reset_account_rules!(triggers: all_triggers, automations: all_automations)
    end

    it 'returns a list of rule ids that should be changed' do
      diff = opt_in.update_opt_in_rules(args)
      assert_equal triggers_that_should_be_updated.size + automations_that_should_be_updated.size, diff.size
    end

    describe 'it contains the right rule ids' do
      before do
        reset_account_rules!(triggers: triggers_that_should_be_updated)
      end

      it 'has the rule ids that need to be changed' do
        expected_updated_rule_ids = account.rules.map(&:id)
        diff = opt_in.update_opt_in_rules(args)
        assert_equal expected_updated_rule_ids, diff
      end

      it 'does not include rule ids that do not need to be changed' do
        expected_unchanged_rule_ids = []
        triggers_that_should_not_be_updated.each do |test_case|
          rule = create_trigger(account: account, title: test_case[:title], definition: test_case[:definition])
          expected_unchanged_rule_ids << rule.id
        end

        diff = opt_in.update_opt_in_rules(args)

        assert_equal expected_unchanged_rule_ids - diff, expected_unchanged_rule_ids
      end

      it 'stores the rule ids in the simplified_email_rule_bodies table' do
        expected_updated_rule_ids = account.rules.map(&:id)
        opt_in.update_opt_in_rules(args)

        simplified_email_rule_ids = account.simplified_email_rule_bodies.map(&:rule_id)

        assert_equal expected_updated_rule_ids, simplified_email_rule_ids
      end

      it 'stores a copy of the email body in the simplified_email_rule_bodies table' do
        expected_rule_bodies = account.rules.map { |rule| action_value(rule) }
        opt_in.update_opt_in_rules(args)

        simplified_email_rule_bodies = account.simplified_email_rule_bodies.map(&:rule_body)

        assert_equal expected_rule_bodies, simplified_email_rule_bodies
      end
    end

    describe 'rules with multiple notifying actions' do
      let(:notifying_rule_with_multiple_actions) do
        [
          {
              title: 'Notification User Trigger',
              definition: Definition.new(condition_1, [], action_1 + action_2)
          }
        ]
      end

      before do
        reset_account_rules!(triggers: notifying_rule_with_multiple_actions)
      end

      it 'does not add the rule to simplified_email_rule_bodies table' do
        opt_in.update_opt_in_rules(args)
        assert_empty account.simplified_email_rule_bodies
      end

      it 'does not modify the email bodies of the rule' do
        notifying_rule_body_1 = "Email body: #{original_placeholder_1}"
        notifying_rule_body_2 = "Email body: #{original_placeholder_2}"

        expected_rule_bodies = [notifying_rule_body_1, notifying_rule_body_2]
        opt_in.update_opt_in_rules(args)
        rule = account.rules.first
        opted_in_rule_bodies = rule.definition.actions.map { |action| action.value[2] }

        assert_equal expected_rule_bodies, opted_in_rule_bodies
      end
    end

    describe 'rules with multiple same-type placeholders' do
      let(:action) do
        [
          DefinitionItem.new('notification_group', 'is', ['group_id', 'Email subject', 'Email body: {{ticket.comments_formatted}} and {{ticket.comments_formatted}}'])
        ]
      end

      let(:rule_with_multiple_placeholders) do
        [
          {
              title: 'Notification User Trigger',
              definition: Definition.new(condition_1, [], action)
          }

        ]
      end

      before do
        reset_account_rules!(triggers: rule_with_multiple_placeholders)
      end

      it 'does not add the rule to simplified rules' do
        diff = opt_in.update_opt_in_rules(args)
        assert_empty diff
        assert_empty account.simplified_email_rule_bodies
      end
    end

    describe '#placeholders' do
      let(:all_triggers) { triggers_that_should_be_updated }
      let(:all_automations) { automations_that_should_be_updated }

      it 'replaces the placeholders for rules' do
        opt_in.update_opt_in_rules(args)
        updated_rules = account.rules.where(type: [Trigger, Automation])

        updated_rules.each do |rule|
          if action_source(rule) == "notification_user"
            assert_includes action_value(rule), replacement_placeholder_1
          elsif action_source(rule) == "notification_group"
            assert_includes action_value(rule), replacement_placeholder_2
          end
        end
      end

      it 'does not contain the previous placeholders in rules' do
        opt_in.update_opt_in_rules(args)
        updated_rules = account.rules.where(type: [Trigger, Automation])

        updated_rules.each do |rule|
          if action_source(rule) == "notification_user"
            assert_not_includes action_value(rule), original_placeholder_1
          elsif action_source(rule) == "notification_group"
            assert_not_includes action_value(rule), original_placeholder_2
          end
        end
      end

      describe 'when simplified_email_rule_body is invalid' do
        before do
          SimplifiedEmailRuleBody.any_instance.stubs(:valid?).returns(false)
        end

        it 'does not replace the placeholders for rules' do
          opt_in.update_opt_in_rules(args)
          updated_rules = account.rules.where(type: [Trigger, Automation], is_active: true)

          assert_empty account.simplified_email_rule_bodies

          updated_rules.each do |rule|
            if action_source(rule) == "notification_user"
              assert_not_includes action_value(rule), replacement_placeholder_1
            elsif action_source(rule) == "notification_group"
              assert_not_includes action_value(rule), replacement_placeholder_2
            end
          end
        end
      end
    end

    describe 'with other rule types' do
      let(:all_triggers) { triggers_that_should_be_updated + triggers_that_should_not_be_updated }
      let(:all_automations) { automations_that_should_be_updated + automations_that_should_not_be_updated }

      before do
        reset_account_rules!(triggers: all_triggers, automations: all_automations)
        create_macro(title: 'AA', owner: account, active: true, position: 1)
        assert_equal all_triggers.size + all_automations.size + 1, account.rules.size
      end

      it 'returns a list of rule ids that should be changed' do
        diff = opt_in.update_opt_in_rules(args)
        assert_equal triggers_that_should_be_updated.size + automations_that_should_be_updated.size, diff.size
      end

      it 'does not include the macro in the rules diff list or modify it' do
        macro = account.rules.where(type: :macro).first
        diff = opt_in.update_opt_in_rules(args)
        assert_not_includes diff, macro.id
      end
    end

    describe 'with dry run set as true' do
      let(:args) do
        { dry_run: true }
      end

      it 'returns a diff list for rules that should be changed' do
        diff = opt_in.update_opt_in_rules(args)
        assert_not_empty diff
        assert_equal triggers_that_should_be_updated.size + automations_that_should_be_updated.size, diff.size
      end

      it 'does not add the rules to simplified_email_rule_bodies table' do
        opt_in.update_opt_in_rules(args)
        assert_empty account.simplified_email_rule_bodies
      end

      describe '#placeholders' do
        let(:all_triggers) { triggers_that_should_be_updated }
        let(:all_automations) { automations_that_should_be_updated }

        it 'does not replace the placeholders' do
          opt_in.update_opt_in_rules(args)
          updated_rules = account.rules.where(type: [Trigger, Automation], is_active: true)

          updated_rules.each do |rule|
            if action_source(rule) == "notification_user"
              assert_not_includes action_value(rule), replacement_placeholder_1
            elsif action_source(rule) == "notification_group"
              assert_not_includes action_value(rule), replacement_placeholder_2
            end
          end
        end

        it 'keeps the original placeholders' do
          opt_in.update_opt_in_rules(args)
          updated_rules = account.rules.where(type: [Trigger, Automation], is_active: true)

          updated_rules.each do |rule|
            if action_source(rule) == "notification_user"
              assert_includes action_value(rule), original_placeholder_1
            elsif action_source(rule) == "notification_group"
              assert_includes action_value(rule), original_placeholder_2
            end
          end
        end
      end
    end
  end

  describe 'when there are no rules to update' do
    describe_with_and_without_arturo_enabled :email_simplified_threading do
      let(:all_triggers) { triggers_that_should_not_be_updated }
      let(:all_automations) { automations_that_should_not_be_updated }

      before do
        reset_account_rules!(triggers: all_triggers)
      end

      args = [{ dry_run: true },
              { dry_run: false }]

      args.each do |arg|
        describe "when dry run is #{arg[:dry_run]}" do
          it 'returns an empty rules list' do
            diff = opt_in.update_opt_in_rules(arg)
            assert_empty diff
          end

          it 'does not add new rules' do
            opt_in.update_opt_in_rules(arg)
            assert_equal all_triggers.size, account.rules.size
          end

          it 'does not add the rules to simplified_email_rule_bodies table' do
            opt_in.update_opt_in_rules(arg)
            assert_empty account.simplified_email_rule_bodies
          end
        end
      end
    end
  end

  describe 'when there are placeholders in the notification templates to update' do
    before do
      SimplifiedEmailOptInSetting.where(account_id: account.id).delete_all
    end

    describe_with_and_without_arturo_enabled :email_simplified_threading do
      describe 'when there are placeholders in both the follower notification template and the cc notification template to replace' do
        it 'replaces the placeholders in the both template and save the original templates' do
          account.update_attributes({
            cc_email_template: email_template_that_should_be_updated,
            follower_email_template: email_template_that_should_be_updated
          })

          opt_in.update_email_template(dry_run: false)

          assert_equal updated_email_template, account.reload.follower_email_template
          assert_equal updated_email_template, account.reload.cc_email_template

          settings = account.simplified_email_opt_in_settings

          assert_equal 2, settings.length
          assert_equal 'cc_email_template', settings[0].name
          assert_equal 'follower_email_template', settings[1].name
          assert_equal email_template_that_should_be_updated, settings[0].value
          assert_equal email_template_that_should_be_updated, settings[1].value
        end
      end

      describe 'when the account enables CCs/Followers and disables its rollback' do
        before do
          account.settings.ccs_followers_no_rollback = true
          account.settings.save!
        end

        it 'only saves the original follower template if there are changes and return true' do
          account.update_attributes({
            follower_email_template: email_template_that_should_be_updated,
            cc_email_template: email_template_that_should_be_updated,
          })

          did_change = opt_in.update_email_template(dry_run: false)

          assert did_change

          settings = account.simplified_email_opt_in_settings
          assert_equal 1, settings.length
          assert_equal 'follower_email_template', settings[0].name
          assert_equal email_template_that_should_be_updated, settings[0].value
        end

        it 'replaces the placeholders only in follower template' do
          account.update_attributes({
            follower_email_template: email_template_that_should_be_updated,
            cc_email_template: email_template_that_should_be_updated,
          })

          opt_in.update_email_template(dry_run: false)
          assert_equal email_template_that_should_be_updated, account.reload.cc_email_template
          assert_equal updated_email_template, account.reload.follower_email_template
        end

        it 'rolls back all changes if there is an exception creating simplified email opt-in setting' do
          account.update_attributes({cc_email_template: email_template_that_should_be_updated})
          Zendesk::SimplifiedEmailThreading::OptIn.any_instance.stubs(:create_simplified_email_opt_in_setting!).raises(SomeOptInError)
          assert_raises(SomeOptInError) { opt_in.update_email_template(dry_run: false) }
          assert_empty account.simplified_email_opt_in_settings
          assert_equal email_template_that_should_be_updated, account.reload.cc_email_template
        end

        it 'rolls back all changes if error raised within transaction' do
          account.update_attributes({cc_email_template: email_template_that_should_be_updated})
          Account.any_instance.stubs(:update_attributes).raises(SomeOptInError)
          assert_raises(SomeOptInError) { opt_in.update_email_template(dry_run: false) }
          assert_empty account.simplified_email_opt_in_settings
          assert_equal email_template_that_should_be_updated, account.reload.cc_email_template
        end

        describe 'when running with dry_run: true' do
          it 'does not create a simplified_email_opt_in_setting record nor update cc_email_template' do
            account.update_attributes({
              follower_email_template: email_template_that_should_be_updated,
              cc_email_template: email_template_that_should_be_updated,
            })

            will_change = opt_in.update_email_template(dry_run: true)

            assert will_change
            assert_empty account.simplified_email_opt_in_settings
            assert_equal email_template_that_should_be_updated, account.cc_email_template
          end
        end
      end

      describe 'when there are placeholders only in the follower notification template to update' do
        it 'replaces the placeholders in the follower template' do
          account.update_attributes({follower_email_template: email_template_that_should_be_updated})

          opt_in.update_email_template(dry_run: false)

          assert_equal updated_email_template, account.reload.follower_email_template
        end

        it 'saves the original follower template if there are changes and return true' do
          account.update_attributes({follower_email_template: email_template_that_should_be_updated})

          did_change = opt_in.update_email_template(dry_run: false)

          assert did_change

          settings = account.simplified_email_opt_in_settings
          assert_equal 1, settings.length
          assert_equal 'follower_email_template', settings[0].name
          assert_equal email_template_that_should_be_updated, settings[0].value
        end

        it 'does not create a simplified_email_opt_in_setting record nor update follower_email_template' do
          account.update_attributes({follower_email_template: updated_email_template})

          did_change = opt_in.update_email_template(dry_run: false)

          refute did_change
          assert_empty account.simplified_email_opt_in_settings
        end

        it "does not update the template if there are multiple instances of the same placeholders" do
          account.update_attributes({follower_email_template: email_template_that_should_not_be_updated_v2})

          did_change = opt_in.update_email_template(dry_run: false)

          refute did_change
        end

        it "does not update the template if there are multiple placeholders" do
          account.update_attributes({follower_email_template: email_template_that_should_not_be_updated_v2})

          did_change = opt_in.update_email_template(dry_run: false)

          refute did_change
        end

        it 'rolls back all changes if there is an exception creating simplified email opt-in setting' do
          account.update_attributes({follower_email_template: email_template_that_should_be_updated})
          Zendesk::SimplifiedEmailThreading::OptIn.any_instance.stubs(:create_simplified_email_opt_in_setting!).raises(SomeOptInError)
          assert_raises(SomeOptInError) { opt_in.update_email_template(dry_run: false) }
          assert_empty account.simplified_email_opt_in_settings
          assert_equal email_template_that_should_be_updated, account.reload.follower_email_template
        end

        it 'rolls back all changes if error raised within transaction' do
          account.update_attributes({follower_email_template: email_template_that_should_be_updated})
          Account.any_instance.stubs(:update_attributes).raises(SomeOptInError)
          assert_raises(SomeOptInError) { opt_in.update_email_template(dry_run: false) }
          assert_empty account.simplified_email_opt_in_settings
          assert_equal email_template_that_should_be_updated, account.reload.follower_email_template
        end

        describe 'when running with dry_run: true' do
          it 'does not create a simplified_email_opt_in_setting record nor update follower_email_template' do
            account.update_attributes({follower_email_template: email_template_that_should_be_updated})

            will_change = opt_in.update_email_template(dry_run: true)

            assert will_change
            assert_empty account.simplified_email_opt_in_settings
            assert_equal email_template_that_should_be_updated, account.follower_email_template
          end
        end
      end

      describe 'when there are placeholders only in the cc notification template to update' do
        it 'saves the original cc template if there are changes and return true' do
          account.update_attributes({
            follower_email_template: email_template_that_should_not_be_updated,
            cc_email_template: email_template_that_should_be_updated,
          })

          did_change = opt_in.update_email_template(dry_run: false)

          assert did_change

          settings = account.simplified_email_opt_in_settings
          assert_equal 1, settings.length
          assert_equal 'cc_email_template', settings[0].name
          assert_equal email_template_that_should_be_updated, settings[0].value
        end

        it 'replaces the placeholders in the cc template' do
          account.update_attributes({cc_email_template: email_template_that_should_be_updated})

          opt_in.update_email_template(dry_run: false)
          assert_equal updated_email_template, account.reload.cc_email_template
        end

        it 'rolls back all changes if there is an exception creating simplified email opt-in setting' do
          account.update_attributes({cc_email_template: email_template_that_should_be_updated})
          Zendesk::SimplifiedEmailThreading::OptIn.any_instance.stubs(:create_simplified_email_opt_in_setting!).raises(SomeOptInError)
          assert_raises(SomeOptInError) { opt_in.update_email_template(dry_run: false) }
          assert_empty account.simplified_email_opt_in_settings
          assert_equal email_template_that_should_be_updated, account.reload.cc_email_template
        end

        it 'rolls back all changes if error raised within transaction' do
          account.update_attributes({cc_email_template: email_template_that_should_be_updated})
          Account.any_instance.stubs(:update_attributes).raises(SomeOptInError)
          assert_raises(SomeOptInError) { opt_in.update_email_template(dry_run: false) }
          assert_empty account.simplified_email_opt_in_settings
          assert_equal email_template_that_should_be_updated, account.reload.cc_email_template
        end

        describe 'when running with dry_run: true' do
          it 'does not create a simplified_email_opt_in_setting record nor update cc_email_template' do
            account.update_attributes({cc_email_template: email_template_that_should_be_updated})

            will_change = opt_in.update_email_template(dry_run: true)

            assert will_change
            assert_empty account.simplified_email_opt_in_settings
            assert_equal email_template_that_should_be_updated, account.cc_email_template
          end
        end
      end

      describe 'when there is no placeholder in both the follower/cc and Legacy CC notification email to replace' do
        it 'does not create simplified_email_opt_in_setting record nor update template' do
          account.update_attributes({
            follower_email_template: email_template_that_should_not_be_updated,
            cc_email_template: email_template_that_should_not_be_updated
          })

          did_change = opt_in.update_email_template(dry_run: true)

          refute did_change
          assert_empty account.simplified_email_opt_in_settings
        end
      end
    end
  end

  describe 'when there is HTML mail template to update' do
    before do
      SimplifiedEmailOptInSetting.where(account_id: account.id).delete_all
    end

    describe_with_and_without_arturo_enabled :email_simplified_threading do
      it 'replaces the HTML mail template' do
        account.update_attributes({html_mail_template: html_mail_template_that_should_be_updated})

        opt_in.update_html_template(dry_run: false)

        assert_equal updated_html_mail_template, account.reload.html_mail_template
      end

      it 'saves the original HTML mail template and return true' do
        account.update_attributes({html_mail_template: html_mail_template_that_should_be_updated})

        did_change = opt_in.update_html_template(dry_run: false)

        assert did_change

        settings = account.simplified_email_opt_in_settings
        assert_equal 1, settings.length
        assert_equal 'html_mail_template', settings[0].name
        assert_equal html_mail_template_that_should_be_updated, settings[0].value
      end

      it 'rolls back all changes if there is an exception creating simplified email opt-in setting' do
        account.update_attributes({html_mail_template: html_mail_template_that_should_be_updated})
        Zendesk::SimplifiedEmailThreading::OptIn.any_instance.stubs(:create_simplified_email_opt_in_setting!).raises(SomeOptInError)
        assert_raises(SomeOptInError) { opt_in.update_html_template(dry_run: false) }
        assert_empty account.simplified_email_opt_in_settings
        assert_equal html_mail_template_that_should_be_updated, account.html_mail_template
      end

      it 'rolls back all changes if error raised within transaction' do
        account.update_attributes({html_mail_template: html_mail_template_that_should_be_updated})
        Account.any_instance.stubs(:save!).raises(SomeOptInError)
        assert_raises(SomeOptInError) { opt_in.update_html_template(dry_run: false) }
        assert_empty account.simplified_email_opt_in_settings
        assert_equal html_mail_template_that_should_be_updated, account.html_mail_template
      end

      describe 'when running with dry_run: true' do
        it 'does not create a simplified_email_opt_in_setting record nor update html_mail_template' do
          account.update_attributes({html_mail_template: html_mail_template_that_should_be_updated})

          will_change = opt_in.update_html_template(dry_run: true)

          assert will_change
          assert_empty account.simplified_email_opt_in_settings
          assert_equal html_mail_template_that_should_be_updated, account.html_mail_template
        end
      end

      describe 'when account has no_mail_delimiter enabled' do
        before { Account.any_instance.stubs(has_no_mail_delimiter_enabled?: true) }

        it 'replaces the HTML mail template with the right template' do
          account.update_attributes({html_mail_template: html_mail_template_without_delimiter_that_should_be_updated})

          opt_in.update_html_template(dry_run: false)

          assert_equal updated_html_mail_template_without_delimiter, account.reload.html_mail_template
        end
      end
    end
  end

  private

  class SomeOptInError < StandardError; end

  def reset_account_rules!(triggers: [], automations: [])
    Rule.without_arsi.delete_all

    triggers_for_account = triggers.map do |test_case|
      create_trigger(account: account, title: test_case[:title], definition: test_case[:definition])
    end

    automations_for_account = automations.map do |test_case|
      create_automation(account: account, title: test_case[:title], definition: test_case[:definition])
    end

    assert_equal account.rules.size, triggers_for_account.size + automations_for_account.size
  end

  def action_source(trigger)
    trigger.definition.actions.first.source
  end

  def action_value(trigger)
    trigger.definition.actions.first.value[2]
  end
end
