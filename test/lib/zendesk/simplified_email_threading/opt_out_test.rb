require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SimplifiedEmailThreading::OptOut do
  fixtures :accounts, :rules

  let(:account) { accounts(:minimum) }
  let(:opt_out) { Zendesk::SimplifiedEmailThreading::OptOut.new(account.id) }
  # The following triggers have a single {{ticket.comments_formatted}} placeholder
  let(:rule_1) { rules(:trigger_notify_requester_of_received_request) }
  let(:rule_2) { rules(:trigger_notify_requester_of_solved_ticket) }

  describe "#update_opt_out_rules" do
    let(:subject) { opt_out.update_opt_out_rules(dry_run: dry_run) }

    describe "when dry_run is true" do
      let(:dry_run) { true }

      describe "and there are no rules to update" do
        it "returns empty lists" do
          updated_rule_ids, non_updated_rule_ids = subject
          assert_empty updated_rule_ids
          assert_empty non_updated_rule_ids
        end
      end

      describe "and are rules to update" do
        before { setup_rules_to_update }

        it "does not update rules" do
          assert_equal 2, account.simplified_email_rule_bodies.size
          assert_includes opt_out.send(:notification_action, rule_1.reload).value[2], Zendesk::SimplifiedEmailThreading::OptIn::REPLACEMENT_PLACEHOLDER
          updated_rule_ids, non_updated_rule_ids = subject
          assert_equal [rule_1.id], updated_rule_ids
          assert_equal [rule_2.id], non_updated_rule_ids
          assert_equal 2, account.simplified_email_rule_bodies.size
          assert_includes opt_out.send(:notification_action, rule_1.reload).value[2], Zendesk::SimplifiedEmailThreading::OptIn::REPLACEMENT_PLACEHOLDER
        end

        describe "and a rule has been updated to not include a notification_user nor notification_group action" do
          before do
            rule_1.definition.actions.clear
            rule_1.save(validate: false)
            Trigger.any_instance.stubs(:valid?).returns(true)
          end

          it "includes the modified rule in the list of non-updated rules" do
            updated_rule_ids, non_updated_rule_ids = subject
            assert_empty updated_rule_ids
            assert_includes non_updated_rule_ids, rule_1.id
          end
        end
      end
    end

    describe "when dry_run is false" do
      let(:dry_run) { false }

      describe "and there are no rules to update" do
        it "returns empty lists" do
          updated_rule_ids, non_updated_rule_ids = subject
          assert_empty updated_rule_ids
          assert_empty non_updated_rule_ids
        end
      end

      describe "and are rules to update" do
        before { setup_rules_to_update }

        it "returns lists of updated and non-updated rule ids" do
          assert_equal 2, account.simplified_email_rule_bodies.size
          rule_body = opt_out.send(:notification_action, rule_1.reload).value[2]
          rule_body_spaces_count = rule_body.count(' ')
          rule_body_newline_count = rule_body.count('\n')
          assert_includes rule_body, Zendesk::SimplifiedEmailThreading::OptIn::REPLACEMENT_PLACEHOLDER
          updated_rule_ids, non_updated_rule_ids = subject
          assert_equal [rule_1.id], updated_rule_ids
          assert_equal [rule_2.id], non_updated_rule_ids
          assert_equal 0, account.simplified_email_rule_bodies.size
          updated_rule_body = opt_out.send(:notification_action, rule_1.reload).value[2]
          # Assert that whitespaces and newlines have been maintained after the
          # former rule body has been restored
          updated_rule_body_spaces_count = rule_body.count(' ')
          updated_rule_body_newline_count = rule_body.count('\n')
          assert_equal rule_body_spaces_count, updated_rule_body_spaces_count
          assert_equal rule_body_newline_count, updated_rule_body_newline_count
          refute_includes updated_rule_body, Zendesk::SimplifiedEmailThreading::OptIn::REPLACEMENT_PLACEHOLDER
        end
      end
    end
  end

  describe "#update_rule?" do
    describe "when a rule is invalid" do
      before { rule_1.stubs(:valid?).returns(false) }

      it "returns false" do
        refute opt_out.send(:update_rule?, rule_1)
      end
    end
  end

  describe 'when opting out from the simplified notification template' do
    let(:original_template) { "Custom follower template for ({{ticket.id}}) {{ticket.latest_comment}}" }
    let(:new_template) { "Custom follower template for ({{ticket.id}}) {{ticket.latest_comment_rich}}" }

    describe 'when only the original follower/cc notification template is saved in simplified email opt-in settings' do
      let(:email_template_key) { "follower_email_template" }

      let(:reset_email_template_to_before_opt_in) do
        reset_simplified_email_threading_settings(email_template_key, original_template)
      end

      let(:reset_email_template_to_after_opt_in) do
        update_simplified_email_threading_settings(email_template_key, original_template, new_template)
      end

      describe 'when dry_run is true' do
        it "does not make persisted changes but returns true if there will when dry_run is false" do
          reset_email_template_to_after_opt_in
          will_change = opt_out.update_email_template
          assert will_change

          original_template = account.simplified_email_opt_in_settings.find_by(name: email_template_key)&.value
          refute original_template.nil?
          assert_equal new_template, account.reload.follower_email_template
        end

        it "returns false if there won't be any persisted changes when dry_run is false" do
          reset_email_template_to_before_opt_in
          will_change = opt_out.update_email_template
          refute will_change
        end
      end

      describe 'when dry_run is false' do
        it "returns true if the opt-out changed the follower email template" do
          reset_email_template_to_after_opt_in
          did_change = opt_out.update_email_template(dry_run: false)
          assert did_change

          assert account.simplified_email_opt_in_settings.find_by(name: email_template_key).nil?
          assert_equal original_template, account.reload.follower_email_template
        end

        it "returns false if the opt-out didn't change the follower email template" do
          reset_email_template_to_before_opt_in
          did_change = opt_out.update_email_template(dry_run: false)
          refute did_change
        end

        it 'rolls back all changes if error raised within transaction' do
          reset_email_template_to_after_opt_in
          Account.any_instance.stubs(:update_attributes).raises(SomeOptOutError)
          assert_raises(SomeOptOutError) { opt_out.update_email_template(dry_run: false) }

          assert_equal new_template, account.reload.follower_email_template
          refute_nil account.reload.simplified_email_opt_in_settings.find_by(name: email_template_key)
        end

        it "rolls back all changes if there is an exception destroying simplified email opt-in settings" do
          reset_email_template_to_after_opt_in
          ActiveRecord::AssociationRelation.any_instance.stubs(:destroy_all).raises(SomeOptOutError)
          assert_raises(SomeOptOutError) { opt_out.update_email_template(dry_run: false) }

          assert_equal new_template, account.reload.follower_email_template
          refute_nil account.reload.simplified_email_opt_in_settings.find_by(name: email_template_key)
        end
      end
    end

    describe 'when only the original legacy cc notification template is saved in simplified email opt-in settings' do
      let(:email_template_key) { "cc_email_template" }

      let(:reset_email_template_to_before_opt_in) do
        reset_simplified_email_threading_settings(email_template_key, original_template)
      end

      let(:reset_email_template_to_after_opt_in) do
        update_simplified_email_threading_settings(email_template_key, original_template, new_template)
      end

      describe 'when dry_run is true' do
        it "does not make persisted changes but returns true if there will be when dry_run is false" do
          reset_email_template_to_after_opt_in
          will_change = opt_out.update_email_template
          assert will_change

          original_template = account.simplified_email_opt_in_settings.find_by(name: email_template_key)&.value
          refute original_template.nil?
          assert_equal new_template, account.reload.cc_email_template
        end

        it "returns false if there won't be any persisted changes when dry_run is false" do
          reset_email_template_to_before_opt_in
          will_change = opt_out.update_email_template
          refute will_change
        end
      end

      describe 'when dry_run is false' do
        it "returns true if the opt-out changed the CC email template" do
          reset_email_template_to_after_opt_in
          did_change = opt_out.update_email_template(dry_run: false)
          assert did_change

          assert account.simplified_email_opt_in_settings.find_by(name: email_template_key).nil?
          assert_equal original_template, account.reload.cc_email_template
        end

        it "returns false if the opt-out didn't change the CC email template" do
          reset_email_template_to_before_opt_in
          did_change = opt_out.update_email_template(dry_run: false)
          refute did_change
        end

        it 'rolls back all changes if error raised within transaction' do
          reset_email_template_to_after_opt_in
          Account.any_instance.stubs(:update_attributes).raises(SomeOptOutError)
          assert_raises(SomeOptOutError) { opt_out.update_email_template(dry_run: false) }

          assert_equal new_template, account.reload.cc_email_template
          refute_nil account.reload.simplified_email_opt_in_settings.find_by(name: email_template_key)
        end

        it "rolls back all changes if there is an exception destroying simplified email opt-in settings" do
          reset_email_template_to_after_opt_in
          ActiveRecord::AssociationRelation.any_instance.stubs(:destroy_all).raises(SomeOptOutError)
          assert_raises(SomeOptOutError) { opt_out.update_email_template(dry_run: false) }

          assert_equal new_template, account.reload.cc_email_template
          refute_nil account.reload.simplified_email_opt_in_settings.find_by(name: email_template_key)
        end
      end
    end

    describe 'when both follower/cc and Legacy CCs are saved in simplified email opt-in settings' do
      it 'restores both templates' do
        account.simplified_email_opt_in_settings.create(name: "follower_email_template", value: original_template)
        account.simplified_email_opt_in_settings.create(name: "cc_email_template", value: original_template)
        account.update_attribute("follower_email_template", new_template)
        account.update_attribute("cc_email_template", new_template)

        opt_out.update_email_template(dry_run: false)

        assert_equal original_template, account.reload.follower_email_template
        assert_equal original_template, account.reload.cc_email_template
      end
    end
  end

  describe 'when opting out from the HTML simplified mail template' do
    let(:original_template) { "{{delimiter}} {{header}} {{content}} {{footer}}" }
    let(:new_template) { "{{header}} {{content}} {{footer}}" }
    let(:html_mail_template_key) { "html_mail_template" }

    let(:reset_html_mail_template_to_before_opt_in) do
      reset_simplified_email_threading_settings(html_mail_template_key, original_template)
    end

    let(:reset_html_mail_template_to_after_opt_in) do
      update_simplified_email_threading_settings(html_mail_template_key, original_template, new_template)
    end

    describe "when dry_run is true" do
      it "does not make persisted changes but returns true if there will be when dry_run is false" do
        reset_html_mail_template_to_after_opt_in
        will_change = opt_out.update_html_template(dry_run: true)
        assert will_change

        original_template = account.simplified_email_opt_in_settings.find_by(name: html_mail_template_key)&.value
        refute_nil original_template
        assert_equal new_template, account.reload.html_mail_template
      end

      it "returns false if there won't be any persisted changes when dry_run is false" do
        reset_html_mail_template_to_before_opt_in
        will_change = opt_out.update_html_template(dry_run: true)
        refute will_change
      end
    end

    describe "when dry_run is false" do
      it "returns true if the opt-out changed the html template" do
        reset_html_mail_template_to_after_opt_in
        did_change = opt_out.update_html_template(dry_run: false)
        assert did_change

        assert_nil account.simplified_email_opt_in_settings.find_by(name: html_mail_template_key)
        assert_equal original_template, account.reload.html_mail_template
      end

      it "returns false if the opt-out didn't change the html template" do
        reset_html_mail_template_to_before_opt_in
        did_change = opt_out.update_html_template(dry_run: false)
        refute did_change
      end

      it "rolls back all changes if error raised within transaction" do
        reset_html_mail_template_to_after_opt_in
        Account.any_instance.stubs(:save!).raises(SomeOptOutError)
        assert_raises(SomeOptOutError) { opt_out.update_html_template(dry_run: false) }

        assert_equal new_template, account.reload.html_mail_template
        refute_nil account.reload.simplified_email_opt_in_settings.find_by(name: html_mail_template_key)
      end

      it "rolls back all changes if there is an exception destroying simplified email opt-in settings" do
        reset_html_mail_template_to_after_opt_in
        ActiveRecord::AssociationRelation.any_instance.stubs(:destroy_all).raises(SomeOptOutError)
        assert_raises(SomeOptOutError) { opt_out.update_html_template(dry_run: false) }

        assert_equal new_template, account.reload.html_mail_template
        refute_nil account.reload.simplified_email_opt_in_settings.find_by(name: html_mail_template_key)
      end
    end
  end

  private

  class SomeOptOutError < StandardError; end

  def reset_simplified_email_threading_settings(property_key, original_value)
    account.simplified_email_opt_in_settings.where(name: property_key).delete_all
    account.update_attributes(property_key => original_value)
  end

  def update_simplified_email_threading_settings(property_key, original_value, new_value)
    account.simplified_email_opt_in_settings.where(name: property_key).delete_all
    account.simplified_email_opt_in_settings.create(name: property_key, value: original_value)
    account.update_attributes(property_key => new_value)
  end

  # Replace the {{ticket.comments_formatted}} placeholder with
  # {{ticket.latest_comment_rich}}
  def replace_rule_placeholders!(rule)
    rule.definition.actions.first.value[2].gsub!(
      '{{ticket.comments_formatted}}',
      Zendesk::SimplifiedEmailThreading::OptIn::REPLACEMENT_PLACEHOLDER
    )
    rule.save!
  end

  def duplicate_notification_action!(rule)
    action = opt_out.send(:notification_action, rule)
    rule.definition.actions.push(action)
    rule.save!
  end

  def create_simplified_email_rule_body(rule, rule_body)
    account.simplified_email_rule_bodies.create! do |simplified_email_rule_body|
      simplified_email_rule_body.rule_id = rule.id
      simplified_email_rule_body.rule_body = rule_body
    end
  end

  def rule_body(rule)
    rule.definition.actions.first.value[2]
  end

  def setup_rules_to_update
    # Create the backup of the rule body
    create_simplified_email_rule_body(rule_1, rule_body(rule_1))
    # Modify the current rule
    replace_rule_placeholders!(rule_1)
    # Create the backup of the rule body
    create_simplified_email_rule_body(rule_2, rule_body(rule_2))
    # Modify the rule so it cannot be restored
    duplicate_notification_action!(rule_2)
  end
end
