require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::RedisDatadog' do
  describe "with redis instrumentation" do
    before do
      @client = FakeRedis::Redis.new
      @client.select(1)
      @client.del "redis-datadog-test1"
    end

    it "logs to datadog (write)" do
      ::Zendesk::RedisDatadog.statsd_client.expects(:timing).with do |command, value, tags|
        assert_equal(command.to_s, "command")
        assert(value.is_a?(Numeric))
        assert_equal(tags[:tags], ["command:hset"])
        true
      end
      @client.hset("redis-datadog-test1", "key1", 1.0)
    end

    it "logs to datadog (read)" do
      ::Zendesk::RedisDatadog.statsd_client.expects(:timing).with do |command, value, tags|
        assert_equal(command.to_s, "command")
        assert(value.is_a?(Numeric))
        assert_equal(tags[:tags], ["command:hgetall"])
        true
      end
      @client.hgetall("redis-datadog-test1")
    end
  end
end
