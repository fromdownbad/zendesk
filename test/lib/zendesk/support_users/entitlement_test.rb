require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::SupportUsers::Entitlement' do
  let(:entitlement) { Zendesk::SupportUsers::Entitlement.for(user, product) }

  describe '.for' do
    let(:user) { User.new }

    describe 'guide product' do
      let(:product) { 'guide' }

      it 'returns the expected entitlement' do
        assert entitlement.is_a?(Zendesk::SupportUsers::Entitlement::Guide)
      end
    end

    describe 'explore product' do
      let(:product) { 'explore' }

      it 'returns the expected entitlement' do
        assert entitlement.is_a?(Zendesk::SupportUsers::Entitlement::Explore)
      end
    end

    describe 'talk product' do
      let(:product) { 'talk' }

      it 'returns the expected entitlement' do
        assert entitlement.is_a?(Zendesk::SupportUsers::Entitlement::Talk)
      end
    end

    describe 'support product' do
      let(:product) { 'support' }

      it 'returns the expected entitlement' do
        assert entitlement.is_a?(Zendesk::SupportUsers::Entitlement::Support)
      end
    end

    describe 'chat product' do
      let(:product) { 'chat' }

      it 'returns the expected entitlement' do
        assert entitlement.is_a?(Zendesk::SupportUsers::Entitlement::Chat)
      end
    end

    describe 'other product' do
      let(:product) { 'anything' }

      it 'raises error' do
        e = assert_raises(StandardError) { entitlement }
        assert_equal 'Unrecognized product: anything', e.message
      end
    end
  end
end
