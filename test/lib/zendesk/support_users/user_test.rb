require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::User do
  fixtures :users, :accounts, :permission_sets

  let(:support_user) { Zendesk::SupportUsers::User.new(user_model, sync: sync) }
  let(:user_model) { User.new(account: account) }
  let(:account) { Account.new }
  let(:contributor_permission_set) { permission_sets(:multiproduct_contributor_permission_set) }
  let(:custom_permission_set) { permission_sets(:multiproduct_custom_permission_set) }
  let(:entitlements_sync_enabled) { true }
  let(:sync) { true }

  before { Zendesk::SupportAccounts::Account.any_instance.stubs(:entitlements_sync_enabled?).returns(entitlements_sync_enabled) }

  describe '.retrieve' do
    let(:user_model) { users(:minimum_end_user) }
    let(:retrieve) { Zendesk::SupportUsers::User.retrieve(user_model.id) }

    it 'finds User model by id' do
      User.expects(:find_by_id).with(user_model.id).returns(user_model)
      retrieve
    end

    it 'returns initialized SupportUsers::User object' do
      assert_instance_of Zendesk::SupportUsers::User, retrieve
    end
  end

  describe '.save!' do
    let(:save!) { support_user.save! }
    let(:changes) { {} }
    let(:save_result) { true }
    before do
      user_model.stubs(:save!).returns(save_result)
      user_model.stubs(:previous_changes).returns(changes)
    end

    it 'saves the user' do
      user_model.expects(:save!)
      save!
    end

    it 'returns result of the save' do
      assert_equal save_result, save!
    end

    describe 'initialising staff' do
      let(:staff_initializer) { Zendesk::SupportUsers::Internal::StaffInitializer.new(user_model) }

      before do
        support_user.stubs(:staff_initializer).returns(staff_initializer)
        staff_initializer.stubs(:perform?).returns(perform)
      end

      describe 'when needed' do
        let(:perform) { true }

        it 'initialises the staff' do
          staff_initializer.expects(:perform!)
          save!
        end

        describe 'on failure' do
          before do
            staff_initializer.stubs(:perform!).raises(Kragle::ResponseError)
          end

          it 'does not save the user' do
            user_model.expects(:save!).never
            assert_raises(Kragle::ResponseError) { save! }
          end
        end

        describe 'when user is newly created contributor' do
          let(:synchronization) { Zendesk::SupportUsers::Internal::GuideEntitlementsSynchronization.new(user_model) }

          before do
            support_user.stubs(:guide_entitlements_synchronization).returns(synchronization)
            user_model.stubs(:is_contributor?).returns(true)
            staff_initializer.expects(:perform!)
          end

          it 'syncs performs guide entitlement synchronization' do
            synchronization.expects(:perform!)
            save!
          end
        end
      end

      describe 'when not needed' do
        let(:perform) { false }

        it 'does not initialise the staff' do
          staff_initializer.expects(:perform!).never
          save!
        end
      end
    end

    describe 'synchronous entitlements sync' do
      let(:synchronization) { Zendesk::SupportUsers::Internal::UserEntitlementsSynchronization.new(user_model) }

      before do
        support_user.stubs(:user_entitlements_synchronization).returns(synchronization)
        synchronization.stubs(:perform_synchronously?).returns(perform)
      end

      describe 'when needed' do
        let(:perform) { true }

        it 'performs synchronization' do
          synchronization.expects(:perform_synchronously!)
          save!
        end

        describe 'overide no sync' do
          let(:sync) { false }

          it 'does not perform synchronization' do
            synchronization.expects(:perform_synchronously!).never
            save!
          end
        end

        describe 'on failure' do
          before do
            synchronization.stubs(:perform_synchronously!).raises(Kragle::ResponseError)
          end

          it 'does not save the user' do
            user_model.expects(:save!).never
            assert_raises(Kragle::ResponseError) { save! }
          end
        end
      end

      describe 'when not needed' do
        let(:perform) { false }

        it 'performs synchronization' do
          synchronization.expects(:perform_synchronously!).never
          save!
        end
      end
    end

    describe 'asynchronous entitlements sync' do
      describe_with_arturo_enabled :ocp_unified_entitlement_sync do
        let(:synchronization) { Zendesk::SupportUsers::Internal::EntitlementsSynchronization.new(user_model) }

        before do
          support_user.stubs(:entitlements_synchronization).returns(synchronization)
          synchronization.stubs(:perform?).returns(perform)
        end

        describe 'when needed' do
          let(:perform) { true }

          it 'performs synchronization' do
            synchronization.expects(:perform!)
            save!
          end

          describe 'overide no sync' do
            let(:sync) { false }

            it 'does not perform synchronization' do
              synchronization.expects(:perform!).never
              save!
            end
          end

          describe 'on failure' do
            before do
              synchronization.stubs(:perform!).raises(Kragle::ResponseError)
            end

            it 'does save the user' do
              user_model.expects(:save!).returns(:save_result)
              assert_raises(Kragle::ResponseError) { save! }
            end
          end
        end

        describe 'when not needed' do
          let(:perform) { false }

          it 'does not perform synchronization' do
            synchronization.expects(:perform!).never
            save!
          end
        end
      end

      describe_with_arturo_disabled :ocp_unified_entitlement_sync do
        let(:synchronization) { Zendesk::SupportUsers::Internal::UserEntitlementsSynchronization.new(user_model) }

        before do
          support_user.stubs(:user_entitlements_synchronization).returns(synchronization)
          synchronization.stubs(:perform_asynchronously?).returns(perform)
        end

        describe 'when needed' do
          let(:perform) { true }

          it 'performs synchronization' do
            synchronization.expects(:perform_asynchronously!)
            save!
          end

          describe 'overide no sync' do
            let(:sync) { false }

            it 'does not perform synchronization' do
              synchronization.expects(:perform_asynchronously!).never
              save!
            end
          end

          describe 'on failure' do
            before do
              synchronization.stubs(:perform_asynchronously!).raises(Kragle::ResponseError)
            end

            it 'does save the user' do
              user_model.expects(:save!).returns(:save_result)
              assert_raises(Kragle::ResponseError) { save! }
            end
          end
        end

        describe 'when not needed' do
          let(:perform) { false }

          it 'performs synchronization' do
            synchronization.expects(:perform_asynchronously!).never
            save!
          end
        end
      end
    end
  end

  describe '.save' do
    let(:save) { support_user.save(validate: validate) }
    let(:validate) { true }
    let(:changes) { {} }
    let(:save_result) { true }

    before do
      user_model.stubs(:save).returns(save_result)
      user_model.stubs(:previous_changes).returns(changes)
    end

    it 'saves the user' do
      user_model.expects(:save).with(validate: validate)
      save
    end

    it 'returns result of the save' do
      assert_equal save_result, save
    end

    describe 'syncing explore entitlements' do
      let(:synchronization) { Zendesk::SupportUsers::Internal::ExploreEntitlementsSynchronization.new(user_model) }

      before do
        support_user.stubs(:explore_entitlements_synchronization).returns(synchronization)
        synchronization.stubs(:perform?).returns(perform)
      end

      describe 'when needed' do
        let(:perform) { true }

        it 'performs synchronization' do
          synchronization.expects(:perform!)
          save
        end

        describe 'on failure' do
          before do
            synchronization.stubs(:perform!).raises(Kragle::ResponseError)
          end

          it 'does save the user' do
            user_model.expects(:save).returns(save_result)
            assert_raises(Kragle::ResponseError) { save }
          end
        end

        describe 'when sync is false' do
          let(:sync) { false }

          it 'does not perform synchronization' do
            synchronization.expects(:perform!).never
            save
          end
        end
      end

      describe 'when not needed' do
        let(:perform) { false }

        it 'does not perform synchronization' do
          synchronization.expects(:perform!).never
          save
        end
      end
    end

    describe 'syncing guide entitlements' do
      let(:synchronization) { Zendesk::SupportUsers::Internal::GuideEntitlementsSynchronization.new(user_model) }

      before do
        support_user.stubs(:guide_entitlements_synchronization).returns(synchronization)
        synchronization.stubs(:perform?).returns(perform)
      end

      describe 'when needed' do
        let(:perform) { true }

        it 'performs synchronization' do
          synchronization.expects(:perform!)
          save
        end

        describe 'on failure' do
          before do
            synchronization.stubs(:perform!).raises(Kragle::ResponseError)
          end

          it 'does save the user' do
            user_model.expects(:save).returns(save_result)
            assert_raises(Kragle::ResponseError) { save }
          end
        end

        describe 'when sync is false' do
          let(:sync) { false }

          it 'does not perform synchronization' do
            synchronization.expects(:perform!).never
            save
          end
        end
      end

      describe 'when not needed' do
        let(:perform) { false }

        it 'does not perform synchronization' do
          synchronization.expects(:perform!).never
          save
        end
      end
    end

    describe 'syncing talk entitlements' do
      let(:synchronization) { Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.new(user_model) }

      before do
        support_user.stubs(:talk_entitlements_synchronization).returns(synchronization)
        synchronization.stubs(:perform?).returns(perform)
      end

      describe 'when needed' do
        let(:perform) { true }

        it 'performs synchronization' do
          synchronization.expects(:perform!)
          save
        end

        describe 'on failure' do
          before do
            synchronization.stubs(:perform!).raises(Kragle::ResponseError)
          end

          it 'does save the user' do
            user_model.expects(:save).returns(save_result)
            assert_raises(Kragle::ResponseError) { save }
          end
        end

        describe 'when sync is false' do
          let(:sync) { false }

          it 'does not perform synchronization' do
            synchronization.expects(:perform!).never
            save
          end
        end
      end

      describe 'when not needed' do
        let(:perform) { false }

        it 'does not perform synchronization' do
          synchronization.expects(:perform!).never
          save
        end
      end
    end

    describe 'chat admin validation' do
      let(:chat_admin_validator) { Zendesk::SupportUsers::Internal::ChatAdminValidator.new(user_model) }

      before do
        support_user.stubs(:chat_admin_validator).returns(chat_admin_validator)
        chat_admin_validator.stubs(:perform?).returns(perform)
      end

      describe 'when needed' do
        let(:perform) { true }

        it 'performs validation' do
          chat_admin_validator.expects(:perform!)
          save
        end

        describe 'on failure' do
          before do
            chat_admin_validator.stubs(:perform!).raises(Kragle::ResponseError)
          end

          it 'does not save the user' do
            user_model.expects(:save).never
            assert_raises(Kragle::ResponseError) { save }
          end
        end
      end

      describe 'when not needed' do
        let(:perform) { false }

        it 'does not perform validation' do
          chat_admin_validator.expects(:perform!).never
          save
        end
      end
    end

    describe 'sell admin validation' do
      let(:sell_admin_validator) { Zendesk::SupportUsers::Internal::SellAdminValidator.new(user_model) }

      before do
        support_user.stubs(:sell_admin_validator).returns(sell_admin_validator)
        sell_admin_validator.stubs(:perform?).returns(perform)
      end

      describe 'when needed' do
        let(:perform) { true }

        it 'performs validation' do
          sell_admin_validator.expects(:perform!)
          save
        end

        describe 'on failure' do
          before do
            sell_admin_validator.stubs(:perform!).raises(Kragle::ResponseError)
          end

          it 'does not save the user' do
            user_model.expects(:save).never
            assert_raises(Kragle::ResponseError) { save }
          end
        end
      end

      describe 'when not needed' do
        let(:perform) { false }

        it 'does not perform validation' do
          sell_admin_validator.expects(:perform!).never
          save
        end
      end
    end

    describe 'agent workspace role validation' do
      let(:agent_workspace_role_validator) { Zendesk::SupportUsers::Internal::AgentWorkspaceRoleValidator.new(user_model) }

      before do
        support_user.stubs(:agent_workspace_role_validator).returns(agent_workspace_role_validator)
        agent_workspace_role_validator.stubs(:perform?).returns(perform)
      end

      describe_with_arturo_enabled :agent_workspace_role_restriction do
        describe 'when needed' do
          let(:perform) { true }

          it 'performs validation' do
            agent_workspace_role_validator.expects(:perform!)
            save
          end

          describe 'on failure' do
            before do
              agent_workspace_role_validator.stubs(:perform!).raises(Kragle::ResponseError)
            end

            it 'does not save the user' do
              user_model.expects(:save).never
              assert_raises(Kragle::ResponseError) { save }
            end
          end
        end

        describe 'when not needed' do
          let(:perform) { false }

          it 'does not perform validation' do
            agent_workspace_role_validator.expects(:perform!).never
            save
          end
        end
      end

      describe_with_arturo_disabled :agent_workspace_role_restriction do
        let(:perform) { true }

        it 'does not perform validation' do
          agent_workspace_role_validator.expects(:perform!).never
          save
        end
      end
    end

    describe 'user role validation' do
      let(:validator) { Zendesk::SupportUsers::Internal::RoleValidator.new(user_model) }
      let(:perform) { true }

      before do
        support_user.stubs(:role_validator).returns(validator)
        validator.stubs(:perform?).returns(perform)
      end

      it 'performs validation' do
        validator.expects(:perform!)
        save
      end

      describe 'on failure' do
        before do
          validator.stubs(:perform!).raises(StandardError)
        end

        it 'does not save the user' do
          user_model.expects(:save).never
          assert_raises(StandardError) { save }
        end
      end

      describe 'when not needed' do
        let(:perform) { false }

        it 'does not perform validation' do
          validator.expects(:perform!).never
          save
        end
      end
    end

    describe 'initialising staff' do
      let(:staff_initializer) { Zendesk::SupportUsers::Internal::StaffInitializer.new(user_model) }

      before do
        support_user.stubs(:staff_initializer).returns(staff_initializer)
        staff_initializer.stubs(:perform?).returns(perform)
      end

      describe 'when needed' do
        let(:perform) { true }

        it 'initialises the staff' do
          staff_initializer.expects(:perform!)
          save
        end

        describe 'on failure' do
          before do
            staff_initializer.stubs(:perform!).raises(Kragle::ResponseError)
          end

          it 'does not save the user' do
            user_model.expects(:save).never
            assert_raises(Kragle::ResponseError) { save }
          end
        end
      end

      describe 'when not needed' do
        let(:perform) { false }

        it 'does not initialise the staff' do
          staff_initializer.expects(:perform!).never
          save
        end
      end
    end

    describe 'synchronous entitlements sync' do
      let(:synchronization) { Zendesk::SupportUsers::Internal::UserEntitlementsSynchronization.new(user_model) }

      before do
        support_user.stubs(:user_entitlements_synchronization).returns(synchronization)
        synchronization.stubs(:perform_synchronously?).returns(perform)
      end

      describe 'when needed' do
        let(:perform) { true }

        it 'performs synchronization' do
          synchronization.expects(:perform_synchronously!)
          save
        end

        describe 'overide no sync' do
          let(:sync) { false }

          it 'does not perform synchronization' do
            synchronization.expects(:perform_synchronously!).never
            save
          end
        end

        describe 'on failure' do
          before do
            synchronization.stubs(:perform_synchronously!).raises(Kragle::ResponseError)
          end

          it 'does not save the user' do
            user_model.expects(:save).never
            assert_raises(Kragle::ResponseError) { save }
          end
        end
      end

      describe 'when not needed' do
        let(:perform) { false }

        it 'performs synchronization' do
          synchronization.expects(:perform_synchronously!).never
          save
        end
      end
    end

    describe 'asynchronous entitlements sync' do
      let(:synchronization) { Zendesk::SupportUsers::Internal::UserEntitlementsSynchronization.new(user_model) }

      before do
        support_user.stubs(:user_entitlements_synchronization).returns(synchronization)
        synchronization.stubs(:perform_asynchronously?).returns(perform)
      end

      describe 'when needed' do
        let(:perform) { true }

        it 'performs synchronization' do
          synchronization.expects(:perform_asynchronously!)
          save
        end

        describe 'overide no sync' do
          let(:sync) { false }

          it 'does not perform synchronization' do
            synchronization.expects(:perform_asynchronously!).never
            save
          end
        end

        describe 'on failure' do
          before do
            synchronization.stubs(:perform_asynchronously!).raises(Kragle::ResponseError)
          end

          it 'does save the user' do
            user_model.expects(:save).returns(save_result)
            assert_raises(Kragle::ResponseError) { save }
          end
        end
      end

      describe 'when not needed' do
        let(:perform) { false }

        it 'performs synchronization' do
          synchronization.expects(:perform_asynchronously!).never
          save
        end
      end
    end
  end

  describe '.delete!' do
    let(:user_model) { users(:minimum_end_user) }

    it 'deletes user' do
      user_model.expects(:delete!)
      support_user.delete!
    end
  end

  describe '#roles' do
    let(:user_model) { users(:minimum_admin) }

    it 'returns the users role' do
      assert_equal Role::ADMIN.id, support_user.roles
    end
  end

  describe '#assign_role' do
    let(:account) { accounts(:minimum) }
    let(:user_model) { users(:minimum_end_user) }
    let(:role) { Role::ADMIN.id }
    let(:audit_headers) { {actor_id: -1, ip_address: ''} }

    describe 'when entitlements sync is enabled' do
      let(:entitlements_sync_enabled) { true }

      it 'updates the users role, and syncs changes to Staff Service' do
        Omnichannel::EntitlementSyncJob.expects(:enqueue).with(account_id: account.id, user_id: user_model.id, audit_headers: audit_headers)
        support_user.assign_role(role)
        assert_equal role, support_user.roles
      end
    end

    describe 'when entitlements sync is not enabled' do
      let(:entitlements_sync_enabled) { false }

      it 'updates the users role' do
        support_user.assign_role(role)
        assert_equal role, support_user.roles
      end

      it 'does not sync user entitlement to Staff Service' do
        Omnichannel::EntitlementSyncJob.expects(:enqueue).never
        support_user.assign_role(role)
      end
    end
  end
end
