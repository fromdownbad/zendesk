require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::PermissionSet do
  fixtures :permission_sets

  let(:permission_set_model) { permission_sets(:multiproduct_custom_permission_set) }
  let(:role_sync_enabled) { true }

  before do
    Zendesk::SupportAccounts::Account.any_instance.stubs(:role_sync_enabled?).returns(role_sync_enabled)
  end

  describe '#save!' do
    subject { Zendesk::SupportUsers::PermissionSet.new(permission_set_model).save!(context: context) }

    let(:context) { 'api_create' }

    it 'saves the permission set' do
      permission_set_model.expects(:save!)
      subject
    end

    it 'syncs roles to core services' do
      Omnichannel::RoleSyncJob.expects(:enqueue).with(
        account_id: permission_set_model.account_id,
        permission_set_id: permission_set_model.id,
        role_name: permission_set_model.pravda_role_name,
        context: context,
        permissions_changed: []
      )
      subject
    end

    describe 'when role sync is disabled' do
      let(:role_sync_enabled) { false }

      it 'saves the permission set' do
        permission_set_model.expects(:save!)
        subject
      end

      it 'does not sync roles to core services' do
        Omnichannel::RoleSyncJob.expects(:euqueue).never
      end
    end
  end

  describe '#save' do
    subject { Zendesk::SupportUsers::PermissionSet.new(permission_set_model).save(context: context) }

    let(:context) { 'lotus_create' }

    it 'saves the permission set' do
      permission_set_model.expects(:save)
      subject
    end

    it 'syncs roles to core services' do
      Omnichannel::RoleSyncJob.expects(:enqueue).with(
        account_id: permission_set_model.account_id,
        permission_set_id: permission_set_model.id,
        role_name: permission_set_model.pravda_role_name,
        context: context,
        permissions_changed: []
      )
      subject
    end

    describe 'when role sync is disabled' do
      let(:role_sync_enabled) { false }

      it 'saves the permission set' do
        permission_set_model.expects(:save)
        subject
      end

      it 'does not sync roles to core services' do
        Omnichannel::RoleSyncJob.expects(:euqueue).never
      end
    end
  end

  describe '#destroy' do
    let(:permission_set_model) { permission_sets(:multiproduct_custom_permission_set) }
    subject { Zendesk::SupportUsers::PermissionSet.new(permission_set_model).destroy(context: context) }

    before do
      permission_set_model.users.each do |user|
        user.permission_set_id = nil
        user.save
      end
    end

    let(:context) { 'api_destroy' }

    it 'destroys the permission set' do
      permission_set_model.expects(:destroy)
      subject
    end

    it 'syncs roles to core services' do
      Omnichannel::RoleSyncJob.expects(:enqueue).with(
        account_id: permission_set_model.account_id,
        permission_set_id: permission_set_model.id,
        role_name: permission_set_model.pravda_role_name,
        context: context,
        permissions_changed: []
      )
      subject
    end

    describe 'when role sync is disabled' do
      it 'destroys the permission set' do
        permission_set_model.expects(:destroy)
        subject
      end

      it 'does not sync roles to core services' do
        Omnichannel::RoleSyncJob.expects(:enqueue).never
        subject
      end
    end
  end
end
