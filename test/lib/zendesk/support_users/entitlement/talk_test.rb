require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Entitlement::Talk do
  let(:user) { users(:with_paid_voice_and_zopim_subscription_owner) }
  let(:entitlement) { Zendesk::SupportUsers::Entitlement::Talk.new(user) }

  describe '#grant!' do
    it 'saves user seat' do
      UserSeat.any_instance.expects(:save).returns(true)
      assert entitlement.grant!
    end

    it 'syncs entitlement' do
      Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.any_instance.expects(:perform!)
      entitlement.grant!
    end

    describe 'when save failed' do
      before do
        UserSeat.any_instance.expects(:save).returns(false)
      end

      it 'does not sync entitlement' do
        Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.any_instance.expects(:perform!).never
        entitlement.grant!
      end
    end
  end

  describe '#revoke!' do
    let!(:user_seat) { user.user_seats.voice.create(account: user.account, seat_type: 'voice') }

    before do
      UserSeat.any_instance.stubs(:notify_voice)
    end

    it 'destroys user seat' do
      UserSeat.any_instance.expects(:destroy).returns(true)
      assert entitlement.revoke!
    end

    it 'syncs entitlement' do
      Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.any_instance.expects(:perform!)
      entitlement.revoke!
    end

    describe 'when destroy failed' do
      before do
        UserSeat.any_instance.expects(:destroy).returns(false)
      end

      it 'does not sync entitlement' do
        Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.any_instance.expects(:perform!).never
        entitlement.revoke!
      end
    end
  end

  describe '#payload' do
    let(:user) { User.new }
    let(:account) { Account.new }
    let(:products) do
      [
        stub(
          name: :talk,
          plan_settings: {
            'plan_type' => plan_type
          }
        )
      ]
    end
    let(:plan_type) { 6 }

    before do
      user.account = account
      account.stubs(:products).returns(products)
    end

    describe 'end user' do
      before do
        user.roles = Role::END_USER.id
      end

      it 'is nil' do
        assert_nil entitlement.payload
      end
    end

    describe 'contributor' do
      before do
        user.roles = Role::AGENT.id
        user.stubs(:is_contributor?).returns(true)
      end

      it 'is nil' do
        assert_nil entitlement.payload
      end
    end

    describe 'light agent' do
      before do
        user.roles = Role::AGENT.id
        user.stubs(:is_light_agent?).returns(true)
      end

      it 'is nil' do
        assert_nil entitlement.payload
      end
    end

    describe 'enterprise' do
      before do
        account.stubs(:has_permission_sets?).returns(true)
        user.roles = Role::AGENT.id
      end

      describe 'agent' do
        let(:expected) { { 'name' => 'agent', 'is_active' => is_active } }

        before do
          user.roles = Role::AGENT.id
        end

        describe 'without talk seat' do
          let(:is_active) { false }

          it 'is inactive agent' do
            assert_equal expected, entitlement.payload
          end
        end

        describe 'with talk seat' do
          let(:talk_seat) { UserSeat.new }
          let(:scope) { stub }
          let(:is_active) { true }

          before do
            user.stubs(:user_seats).returns(scope)
            scope.stubs(:voice).returns([talk_seat])
          end

          it 'is talk agent' do
            assert_equal expected, entitlement.payload
          end
        end
      end

      describe 'admin' do
        let(:expected) { { 'name' => role, 'is_active' => true } }

        before do
          user.roles = Role::ADMIN.id
        end

        describe 'without talk seat' do
          let(:role) { 'admin' }

          it 'is talk admin' do
            assert_equal expected, entitlement.payload
          end
        end

        describe 'with talk seat' do
          let(:talk_seat) { UserSeat.new }
          let(:scope) { stub }
          let(:role) { 'lead' }

          before do
            user.stubs(:user_seats).returns(scope)
            scope.stubs(:voice).returns([talk_seat])
          end

          it 'is talk lead' do
            assert_equal expected, entitlement.payload
          end
        end
      end
    end

    describe 'professional' do
      let(:plan_type) { 7 }

      before do
        account.stubs(:has_permission_sets?).returns(false)
      end

      describe 'agent' do
        let(:expected) { { 'name' => 'agent', 'is_active' => is_active } }

        before do
          user.roles = Role::AGENT.id
        end

        describe 'without talk seat' do
          let(:is_active) { false }

          it 'is inactive agent' do
            assert_equal expected, entitlement.payload
          end
        end

        describe 'with talk seat' do
          let(:talk_seat) { UserSeat.new }
          let(:scope) { stub }
          let(:is_active) { true }

          before do
            user.stubs(:user_seats).returns(scope)
            scope.stubs(:voice).returns([talk_seat])
          end

          it 'is talk agent' do
            assert_equal expected, entitlement.payload
          end
        end
      end

      describe 'admin' do
        let(:expected) { { 'name' => role, 'is_active' => true } }

        before do
          user.roles = Role::ADMIN.id
        end

        describe 'without talk seat' do
          let(:role) { 'admin' }

          it 'is talk admin' do
            assert_equal expected, entitlement.payload
          end
        end

        describe 'with talk seat' do
          let(:talk_seat) { UserSeat.new }
          let(:scope) { stub }
          let(:role) { 'lead' }

          before do
            user.stubs(:user_seats).returns(scope)
            scope.stubs(:voice).returns([talk_seat])
          end

          it 'is talk lead' do
            assert_equal expected, entitlement.payload
          end
        end
      end
    end

    describe 'legacy talk accounts' do
      let(:expected) { { 'name' => role, 'is_active' => true } }

      describe 'legacy' do
        let(:plan_type) { 9 }

        describe 'agent' do
          let(:role) { 'agent' }

          before do
            user.roles = Role::AGENT.id
          end

          it 'is talk agent' do
            assert_equal expected, entitlement.payload
          end
        end

        describe 'admin' do
          let(:role) { 'lead' }

          before do
            user.roles = Role::ADMIN.id
          end

          it 'is talk lead' do
            assert_equal expected, entitlement.payload
          end
        end
      end

      describe 'lite' do
        let(:plan_type) { 3 }

        describe 'agent' do
          let(:role) { 'agent' }

          before do
            user.roles = Role::AGENT.id
          end

          it 'is talk agent' do
            assert_equal expected, entitlement.payload
          end
        end

        describe 'admin' do
          let(:role) { 'lead' }

          before do
            user.roles = Role::ADMIN.id
          end

          it 'is talk lead' do
            assert_equal expected, entitlement.payload
          end
        end
      end
    end
  end
end
