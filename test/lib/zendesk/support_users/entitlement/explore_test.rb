require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Entitlement::Explore do
  let(:entitlement) { Zendesk::SupportUsers::Entitlement::Explore.new(user) }
  let(:user) { User.new }
  let(:expected) { { 'name' => role_name, 'is_active' => true } }

  describe '#payload' do
    describe 'end user' do
      before do
        user.roles = Role::END_USER.id
      end

      it 'is nil' do
        assert_nil entitlement.payload
      end

      describe 'with permission set leftover' do
        let(:permission_set) { PermissionSet.new }

        before do
          user.permission_set = permission_set
          permission_set.permissions.explore_access = 'full'
        end

        it 'is still nil' do
          assert_nil entitlement.payload
        end
      end
    end

    describe 'when modify_is_active is false' do
      before do
        user.roles = Role::AGENT.id
      end
      let(:role_name) { 'viewer' }
      let(:expected) { { 'name' => role_name } }

      it 'does not include is_active in payload' do
        assert_equal expected, entitlement.payload(false)
      end
    end

    describe 'agent' do
      let(:role_name) { 'viewer' }

      before do
        user.roles = Role::AGENT.id
      end

      it 'is viewer' do
        assert_equal expected, entitlement.payload
      end

      describe 'custom role' do
        let(:permission_set) { PermissionSet.new }

        before do
          user.permission_set = permission_set
          permission_set.permissions.explore_access = explore_access
        end

        describe 'full access' do
          let(:role_name) { 'admin' }
          let(:explore_access) { 'full' }

          it 'is admin' do
            assert_equal expected, entitlement.payload
          end
        end

        describe 'edit access' do
          let(:role_name) { 'studio' }
          let(:explore_access) { 'edit' }

          it 'is studio' do
            assert_equal expected, entitlement.payload
          end
        end

        describe 'readonly access' do
          let(:role_name) { 'viewer' }
          let(:explore_access) { 'readonly' }

          it 'is viewer' do
            assert_equal expected, entitlement.payload
          end
        end

        describe 'none access' do
          let(:explore_access) { 'none' }

          it 'marks current entitlement as inactive' do
            assert_nil entitlement.payload
          end
        end
      end
    end

    describe 'admin' do
      let(:role_name) { 'admin' }

      before do
        user.roles = Role::ADMIN.id
      end

      it 'is admin' do
        assert_equal expected, entitlement.payload
      end

      describe 'with misconfigured permission_set_id' do
        let(:permission_set) { PermissionSet.new }
        let(:explore_access) { 'readonly' }

        before do
          user.permission_set = permission_set
          permission_set.permissions.explore_access = explore_access
        end

        it 'is admin' do
          assert_equal expected, entitlement.payload
        end
      end
    end
  end
end
