require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Entitlement::Support do
  fixtures :users, :permission_sets

  let(:entitlement) { Zendesk::SupportUsers::Entitlement::Support.new(user) }
  let(:user) { User.new }
  let(:expected) { { 'name' => role_name, 'is_active' => true } }

  describe '#role' do
    let(:role) { entitlement.role }

    describe 'for end user' do
      let(:user) { users(:minimum_end_user) }

      it 'returns nil' do
        assert_nil role
      end
    end

    describe 'for admin' do
      let(:user) { users(:minimum_admin) }

      it 'returns admin' do
        assert_equal role, 'admin'
      end
    end

    describe 'for legacy agent' do
      let(:user) { users(:minimum_agent) }

      it 'returns agent' do
        assert_equal role, 'agent'
      end
    end

    describe 'for contributor' do
      let(:user) { users(:multiproduct_contributor) }

      it 'returns nil' do
        assert_nil role
      end
    end

    describe 'for agent with custom role' do
      let(:user) { users(:multiproduct_custom_agent) }

      it 'returns role name with custom_ prefix and record id' do
        assert_match /custom_\d+/, role
      end
    end

    describe 'for CP3 chat only agent' do
      let(:user) { users(:with_paid_zopim_subscription_chat_agent) }

      it 'returns nil' do
        assert_nil role
      end
    end

    describe 'for light agent' do
      let(:user) { users(:multiproduct_light_agent) }

      it 'returns light_agent' do
        assert_equal 'light_agent', role
      end
    end
  end

  describe '#payload' do
    describe 'when role is nil' do
      let(:user) { users(:minimum_end_user) }

      it 'returns is_active false' do
        assert_nil entitlement.payload
      end
    end

    describe 'when role is not nil' do
      let(:user) { users(:minimum_admin) }

      it 'returns admin' do
        assert_equal entitlement.payload, { 'name' => 'admin', 'is_active' => true }
      end
    end
  end
end
