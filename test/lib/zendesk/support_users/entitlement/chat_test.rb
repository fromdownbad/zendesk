require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Entitlement::Chat do
  fixtures :all

  let(:entitlement) { Zendesk::SupportUsers::Entitlement::Chat.new(user) }

  describe '.role' do
    let(:role) { entitlement.role }

    describe 'for end user' do
      let(:user) { users(:minimum_end_user) }

      it 'returns nil' do
        assert_nil role
      end
    end

    describe 'for CP3 chat admin' do
      let(:user) { users(:with_paid_zopim_subscription_chat_agent) }
      before { user.zopim_identity.is_administrator = true }

      it 'returns admin' do
        assert_equal 'admin', role
      end

      describe 'when zopim identity is disabled' do
        before { user.zopim_identity.is_enabled = false }

        it 'returns nil' do
          assert_nil role
        end
      end
    end

    describe 'for CP3 chat agent' do
      let(:user) { users(:with_paid_zopim_subscription_chat_agent) }

      it 'returns agent' do
        assert_equal 'agent', role
      end

      describe 'when zopim identity is disabled' do
        before { user.zopim_identity.is_enabled = false }

        it 'returns nil' do
          assert_nil role
        end
      end
    end

    describe 'when account has multiproduct flag true' do
      describe 'when user is end user' do
        let(:user) { users(:multiproduct_end_user) }

        it 'returns nil' do
          assert_nil role
        end
      end

      describe 'when user is contributor with cached chat entitlement as agent' do
        let(:user) { users(:multiproduct_contributor) }
        before { user.settings.chat_entitlement = 'agent' }

        it 'returns agent' do
          assert_equal 'agent', role
        end
      end
    end
  end
end
