require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Entitlement::Guide do
  let(:entitlement) { Zendesk::SupportUsers::Entitlement::Guide.new(user) }
  let(:user) { User.new(is_moderator: is_moderator) }
  let(:is_moderator) { false }

  describe '#payload' do
    let(:expected) { { 'name' => role, 'is_active' => true } }

    describe 'end user' do
      before do
        user.roles = Role::END_USER.id
      end

      it 'is nil' do
        assert_nil entitlement.payload
      end

      describe 'with permission set leftover' do
        let(:permission_set) { PermissionSet.new }

        before do
          user.permission_set = permission_set
          permission_set.permissions.forum_access = 'full'
        end

        it 'is still nil' do
          assert_nil entitlement.payload
        end
      end
    end

    describe 'when modify_is_active is false' do
      before do
        user.roles = Role::AGENT.id
      end
      let(:is_moderator) { true }
      let(:role) { 'admin' }
      let(:expected) { { 'name' => role } }

      it 'does not include is_active in payload' do
        assert_equal expected, entitlement.payload(false)
      end
    end

    describe 'agent for system role' do
      let(:permission_set) { PermissionSet.new(role_type: role_type) }

      before do
        user.roles = Role::AGENT.id
        user.stubs(:permission_set).returns(permission_set)
        user.stubs(:account).returns(Account.new)
      end

      describe 'light agent' do
        let(:role_type) { PermissionSet::Type::LIGHT_AGENT }

        describe_with_arturo_enabled :ocp_guide_light_agent_role_sync do
          let(:role) { 'light_agent' }

          it 'is light_agent' do
            assert_equal expected, entitlement.payload
          end
        end

        describe_with_arturo_disabled :ocp_guide_light_agent_role_sync do
          let(:role) { 'viewer' }

          it 'is viewer' do
            assert_equal expected, entitlement.payload
          end
        end
      end

      describe 'chat only agent' do
        let(:role_type) { PermissionSet::Type::CHAT_AGENT }
        let(:role) { 'viewer' }

        it 'is viewer' do
          assert_equal expected, entitlement.payload
        end
      end

      describe 'contributor' do
        let(:role_type) { PermissionSet::Type::CONTRIBUTOR }
        let(:role) { 'viewer' }

        it 'is viewer' do
          assert_equal expected, entitlement.payload
        end
      end
    end

    describe 'agent' do
      before do
        user.roles = Role::AGENT.id
      end

      describe 'is moderator' do
        let(:is_moderator) { true }
        let(:role) { 'admin' }

        it 'is admin' do
          assert_equal expected, entitlement.payload
        end

        describe 'custom role' do
          let(:permission_set) { PermissionSet.new }
          before do
            permission_set.permissions.forum_access = forum_access
            user.permission_set = permission_set
          end

          describe 'full access' do
            let(:forum_access) { 'full' }
            let(:role) { 'admin' }

            it 'is admin' do
              assert_equal expected, entitlement.payload
            end
          end

          describe 'edit access' do
            let(:forum_access) { 'edit-topics' }
            let(:role) { 'admin' }

            it 'is admin' do
              assert_equal expected, entitlement.payload
            end
          end

          describe 'readonly access' do
            let(:forum_access) { 'readonly' }
            let(:role) { 'agent' }

            it 'is agent' do
              assert_equal expected, entitlement.payload
            end
          end
        end
      end

      describe 'is not moderator' do
        let(:is_moderator) { false }
        let(:role) { 'agent' }

        it 'is agent' do
          assert_equal expected, entitlement.payload
        end

        describe 'custom role' do
          let(:permission_set) { PermissionSet.new }

          before do
            permission_set.permissions.forum_access = forum_access
            user.permission_set = permission_set
          end

          describe 'full access' do
            let(:forum_access) { 'full' }
            let(:role) { 'admin' }

            it 'is admin' do
              assert_equal expected, entitlement.payload
            end
          end

          describe 'edit access' do
            let(:forum_access) { 'edit-topics' }
            let(:role) { 'admin' }

            it 'is admin' do
              assert_equal expected, entitlement.payload
            end
          end

          describe 'readonly access' do
            let(:forum_access) { 'readonly' }
            let(:role) { 'agent' }

            it 'is agent' do
              assert_equal expected, entitlement.payload
            end
          end
        end
      end
    end

    describe 'admin' do
      before do
        user.roles = Role::ADMIN.id
      end

      describe 'is moderator' do
        let(:is_moderator) { true }
        let(:role) { 'admin' }

        it 'is admin' do
          assert_equal expected, entitlement.payload
        end
      end

      describe 'is not moderator' do
        let(:is_moderator) { false }
        let(:role) { 'admin' }

        it 'is admin' do
          assert_equal expected, entitlement.payload
        end
      end
    end
  end
end
