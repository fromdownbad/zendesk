require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Entitlement::Base do
  let(:entitlement) { Zendesk::SupportUsers::Entitlement::Base.new(user) }
  let(:user) { User.new }

  describe '#role' do
    it 'raises error' do
      assert_raises(StandardError) { entitlement.role }
    end
  end

  describe '#no_available_role?' do
    let(:role) { Zendesk::SupportUsers::Entitlement::NO_AVAILABLE_ROLE }

    before do
      entitlement.stubs(:role).returns(role)
    end

    it 'returns true' do
      assert entitlement.no_available_role?
    end

    describe 'when there is a role' do
      let(:role) { Zendesk::Entitlement::ROLES[:talk][:admin] }

      it 'returns false' do
        refute entitlement.no_available_role?
      end
    end
  end
end
