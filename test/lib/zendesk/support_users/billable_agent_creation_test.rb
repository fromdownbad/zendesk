require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::User do
  fixtures :users, :accounts, :permission_sets

  let(:support_user) { Zendesk::SupportUsers::User.new(user_model, sync: true) }
  let(:user_model) { User.new(account: account) }
  let(:account) { accounts(:support) }
  let(:cache_key) { "available_agent_seats_for_account_#{user_model.account.id}" }

  describe '#billable_agent_creation?' do
    subject { support_user.billable_agent_creation? }

    describe 'when user is agent' do
      describe 'when user is billable' do
        describe 'when user is newly created' do
          before do
            user_model.roles = Role::AGENT.id
          end

          it 'is true' do
            assert subject
          end
        end

        describe 'when user was not agent' do
          let(:user_model) { users(:support_sample_customer) }

          before do
            user_model.roles = Role::AGENT.id
          end

          it 'is true' do
            assert subject
          end
        end

        describe 'when user was agent' do
          describe 'when user was billable' do
            describe 'from admin to agent' do
              let(:user_model) { users(:support_admin) }

              before do
                user_model.roles = Role::AGENT.id
              end

              it 'is false' do
                refute subject
              end
            end

            describe 'from agent to admin' do
              let(:user_model) { users(:support_agent) }

              before do
                user_model.roles = Role::ADMIN.id
              end

              it 'is false' do
                refute subject
              end
            end

            describe 'from legacy agent to custom agent' do
              let(:user_model) { users(:multiproduct_support_agent) }

              before do
                user_model.permission_set = permission_sets(:multiproduct_custom_permission_set)
              end

              it 'is false' do
                refute subject
              end
            end

            describe 'from custom agent to legacy agent' do
              let(:user_model) { users(:multiproduct_custom_agent) }

              before do
                user_model.permission_set = nil
              end

              it 'is false' do
                refute subject
              end
            end
          end

          describe 'when user was not billable' do
            let(:user_model) { users(:multiproduct_light_agent) }

            before do
              user_model.permission_set = permission_sets(:multiproduct_custom_permission_set)
            end

            it 'is true' do
              assert subject
            end

            describe 'when user is doing reverse sync' do
              it 'is false' do
                user_model.reverse_sync = true
                refute subject
              end
            end
          end
        end
      end

      describe 'when user is not billable' do
        describe 'when user was billable agent' do
          let(:user_model) { users(:support_agent) }

          before do
            user_model.roles = Role::END_USER.id
          end

          it 'is false' do
            refute subject
          end
        end

        describe 'when user was not billable agent' do
          let(:user_model) { users(:multiproduct_end_user) }

          before do
            user_model.roles = Role::AGENT.id
            user_model.permission_set = permission_sets(:multiproduct_contributor_permission_set)
          end

          it 'is false' do
            refute subject
          end
        end
      end
    end

    describe 'when user is not agent' do
      describe 'when user is newly created' do
        before do
          user_model.roles = Role::END_USER.id
        end

        it 'is false' do
          refute subject
        end
      end
      describe 'user was end user' do
        let(:user_model) { users(:support_sample_customer) }

        before do
          user_model.name = 'a new name'
        end

        it 'is false' do
          refute subject
        end
      end
    end
  end

  describe '#reserve_seat' do
    subject { support_user.reserve_seat }

    describe 'when account is cp3 suite' do
      before do
        account.stubs(:has_active_suite?).returns(true)
      end

      describe 'and all seats were filled by Support and Chat' do
        before do
          Zendesk::StaffClient.any_instance.stubs(:seats_remaining_support_or_suite!).returns(0)
        end

        describe_with_arturo_enabled :ocp_check_seats_left_in_staff_service do
          it 'raises error' do
            assert_raise do
              subject
            end
          end
        end

        describe_with_arturo_disabled :ocp_check_seats_left_in_staff_service do
          it 'does not raise error' do
            subject
          end
        end
      end
    end

    describe 'when staff service is not available' do
      before do
        Zendesk::StaffClient.any_instance.stubs(:seats_remaining_support_or_suite!).raises(Kragle::ResponseError)
      end

      describe_with_arturo_enabled :ocp_check_seats_left_in_staff_service do
        it 'takes one seat from available seats' do
          subject
          assert_equal 3, Rails.cache.read(cache_key)
        end
      end

      describe_with_arturo_disabled :ocp_check_seats_left_in_staff_service do
        it 'takes one seat from available seats' do
          subject
          assert_equal 3, Rails.cache.read(cache_key)
        end
      end
    end

    describe 'when staff service has no value' do
      before do
        Zendesk::StaffClient.any_instance.stubs(:seats_remaining_support_or_suite!).returns(nil)
      end

      describe_with_arturo_enabled :ocp_check_seats_left_in_staff_service do
        it 'takes one seat from available seats' do
          subject
          assert_equal 3, Rails.cache.read(cache_key)
        end
      end

      describe_with_arturo_disabled :ocp_check_seats_left_in_staff_service do
        it 'takes one seat from available seats' do
          subject
          assert_equal 3, Rails.cache.read(cache_key)
        end
      end
    end

    describe 'when there are seats available' do
      before do
        Rails.cache.write(cache_key, 5)
      end

      it 'takes one seat from available seats' do
        subject
        assert_equal 4, Rails.cache.read(cache_key)
      end
    end

    describe 'when no seats available' do
      before do
        Rails.cache.write(cache_key, 0)
      end

      it 'raises error' do
        assert_raise do
          subject
        end
      end
    end
  end

  describe '#billable_agent_creation' do
    subject { support_user.billable_agent_creation }

    before do
      Rails.cache.write(cache_key, available_seats)
    end

    describe 'reserve_seat suceeded' do
      let(:available_seats) { 2 }
      let(:save_result) { true }

      before do
        user_model.stubs(:save!).returns(save_result)
      end

      it 'saves the model' do
        user_model.expects(:save!)
        subject
      end

      it 'does not release seat' do
        subject
        assert_equal available_seats - 1, Rails.cache.read(cache_key)
      end

      describe 'save failed' do
        let(:save_result) { false }

        it 'releases seat' do
          subject
          assert_equal available_seats, Rails.cache.read(cache_key)
        end
      end
    end

    describe 'reserve_seat failed' do
      let(:available_seats) { 0 }

      it 'raises error' do
        assert_raises ActiveRecord::RecordInvalid do
          subject
        end
      end

      it 'does not save model' do
        user_model.expects(:save!).never

        assert_raises ActiveRecord::RecordInvalid do
          subject
        end
      end

      it 'does not change available seats' do
        assert_raise do
          subject
        end

        assert_equal 0, Rails.cache.read(cache_key)
      end
    end
  end

  describe '#release_seat' do
    subject { support_user.release_seat }

    before do
      Rails.cache.write(cache_key, 5)
    end

    it 'releases the seat' do
      subject
      assert_equal 6, Rails.cache.read(cache_key)
    end

    describe 'when cache not available' do
      before do
        Rails.cache.delete(cache_key)
      end

      it 'does not write to cache' do
        subject
        assert_nil Rails.cache.read(cache_key)
      end
    end
  end
end
