require_relative "../../../support/test_helper"
require_relative '../../../support/entitlement_test_helper'
require_relative '../../../support/multiproduct_test_helper'

SingleCov.covered!

describe Zendesk::SupportUsers::EntitlementSynchronizer do
  include EntitlementTestHelper
  include MultiproductTestHelper
  fixtures :all

  describe '.perform' do
    let(:perform) { Zendesk::SupportUsers::EntitlementSynchronizer.perform(user, strategy: strategy) }

    describe 'push strategy' do
      let(:strategy) { :push }

      describe 'when user is an end user' do
        let(:user) { users(:multiproduct_end_user) }

        before do
          stub_staff_service_entitlements_get_request(entitlements_body, true)
        end

        describe 'and upgraded to agent' do
          let(:entitlements_body) { make_entitlements_response(support: nil) }

          before do
            user.roles = ::Role::AGENT.id
            user.save!
          end

          it 'syncs user entitlement to Staff Service' do
            Zendesk::StaffClient.any_instance.expects(:update_entitlements!).with(user.id, { 'support' => 'agent' }, false)
            perform
          end

          describe 'no support subscription' do
            before do
              user.account.stubs(:is_active?).returns(false)
            end

            it 'does not sync user entitlement to Staff Service' do
              Zendesk::StaffClient.any_instance.expects(:update_entitlements!).never
              perform
            end
          end
        end

        describe 'when user has only Support entitlement' do
          let(:entitlements_body) { make_entitlements_response(support: 'admin') }

          it 'removes Support entitlement in Staff Service with is_end_user query param' do
            Zendesk::StaffClient.any_instance.expects(:update_entitlements!).with(user.id, { 'support' => nil }, true)
            perform
          end
        end

        describe 'when user has Support and Chat entitlement' do
          let(:entitlements_body) { make_entitlements_response(support: 'admin', chat: 'agent') }

          it 'removes Support and Chat entitlements in Staff Service with is_end_user query param' do
            Zendesk::StaffClient.any_instance.expects(:update_entitlements!).with(user.id, { 'support' => nil, 'chat' => nil }, true)
            perform
          end
        end
      end

      describe 'when user is an agent' do
        let(:user) { users(:multiproduct_support_agent) }

        it 'syncs user entitlement to Staff Service' do
          Zendesk::StaffClient.any_instance.expects(:update_entitlements!).with(user.id, { 'support' => 'agent' }, false)
          perform
        end

        describe 'and upgraded to admin' do
          before do
            user.roles = ::Role::ADMIN.id
          end

          it 'syncs user entitlement to Staff Service' do
            Zendesk::StaffClient.any_instance.expects(:update_entitlements!).with(user.id, { 'support' => 'admin' }, false)
            perform
          end
        end

        describe 'when user is deleted' do
          before do
            user.stubs(:is_active?).returns(false)
          end

          it 'does not sync user entitlement to Staff Service' do
            Zendesk::StaffClient.any_instance.expects(:update_entitlements!).never
            perform
          end
        end
      end
    end

    describe 'pull strategy' do
      let(:strategy) { :pull }
      let(:entitlements_body) { make_entitlements_response(support: support_entitlement) }

      before do
        stub_staff_service_entitlements_get_request(entitlements_body)
        stub_account_service_products
      end

      describe 'non shell account' do
        let(:user) { users(:minimum_end_user) }
        let(:support_entitlement) { 'admin' }

        it 'does not upgrade the user in Support' do
          perform
          refute user.is_admin?, 'Expected user to stay end user'
        end

        describe_with_arturo_enabled :ocp_reverse_sync_non_multiproduct do
          it 'upgrades the user in Support' do
            perform
            assert user.is_admin?
          end
        end
      end

      describe 'reverse sync flag' do
        let(:user) { users(:multiproduct_end_user) }
        let(:support_entitlement) { 'agent' }

        it 'sets reverse_sync to true for user' do
          perform
          assert user.reverse_sync
        end
      end

      describe 'upgrade to admin' do
        let(:user) { users(:multiproduct_end_user) }
        let(:support_entitlement) { 'admin' }

        it 'upgrades the user in Support' do
          perform
          assert user.is_admin?, 'Expected user to become an admin'
        end
      end

      describe 'syncing admin for a billing admin' do
        let(:user) { users(:multiproduct_billing_admin) }
        let(:support_entitlement) { 'admin' }

        describe 'when permissions_allow_admin_permission_sets is disabled' do
          before { Arturo.disable_feature!(:permissions_allow_admin_permission_sets) }

          it 'removes the billing admin permission set' do
            perform
            assert user.is_admin?, 'Expected user to have roles value set to admin'
            assert_nil user.permission_set, 'Expected user not to have a billing admin permission set'
          end
        end

        describe 'when permissions_allow_admin_permission_sets is enabled' do
          before { Arturo.enable_feature!(:permissions_allow_admin_permission_sets) }

          it 'does not remove the billing admin permission set' do
            perform
            assert user.is_admin?, 'Expected user to have roles value set to admin'
            assert user.permission_set.try(:is_billing_admin?), 'Expected user to have a billing admin permission set'
          end
        end
      end

      describe 'upgrade to agent' do
        let(:user) { users(:multiproduct_end_user) }
        let(:support_entitlement) { 'agent' }

        it 'upgrades the user in Support' do
          perform
          assert user.is_agent?, 'Expected user to become an agent'
        end
      end

      describe 'upgrade to light agent' do
        let(:user) { users(:multiproduct_end_user) }
        let(:support_entitlement) { 'light_agent' }

        before do
          user.account.stubs(:has_light_agents?).returns(true)
        end

        it 'upgrades the user in Support' do
          perform
          assert user.is_light_agent?, 'Expected user to become a light agent'
        end
      end

      describe 'upgrade to custom role' do
        let(:user) { users(:multiproduct_end_user) }
        let(:custom_role) { permission_sets(:multiproduct_custom_permission_set) }
        let(:support_entitlement) { "custom_#{custom_role.id}" }

        it 'upgrades the user in Support' do
          perform
          assert_equal custom_role, user.permission_set
        end
      end

      describe 'upgrade from contributor' do
        let(:user) { users(:multiproduct_contributor) }
        let(:support_entitlement) { 'agent' }

        it 'upgrades user in Support' do
          perform
          assert user.is_agent?, 'Expected user to become an agent'
          refute user.is_contributor?, 'Expected user to no longer be contributor'
        end
      end

      describe 'lost entitlements' do
        let(:user) { users(:multiproduct_support_agent) }
        let(:support_entitlement) { nil }

        it 'downgrades user to Contributor' do
          perform
          assert user.is_contributor?, 'Expected user to become contributor'
        end
      end

      describe 'chat only agent' do
        let(:user) { users(:with_paid_zopim_subscription_chat_agent) }

        describe_with_arturo_enabled :ocp_reverse_sync_non_multiproduct do
          describe 'upgraded to agent' do
            let(:support_entitlement) { 'agent' }

            it 'upgrades user to agent' do
              perform
              assert user.is_agent?, 'Expected user to become an agent'
              refute user.is_chat_agent?, 'Expected user to no longer be chat only agent'
            end
          end

          describe 'stays chat agent' do
            let(:support_entitlement) { nil }

            it 'leaves agent as chat only agent' do
              perform
              assert user.is_chat_agent?, 'Expected user to stay chat only agent'
            end
          end
        end
      end

      describe 'explore entitlements' do
        describe 'with custom roles' do
          let(:user) { users(:multiproduct_end_user) }
          let(:custom_role) { permission_sets(:multiproduct_custom_permission_set) }
          let(:support_entitlement) { "custom_#{custom_role.id}" }

          before do
            user.account.stubs(:has_permission_sets?).returns(true)
          end

          describe 'support is changed' do
            it 'syncs explore entitlement' do
              Omnichannel::ExploreEntitlementSyncJob.expects(:perform)
              perform
            end

            it 'does not modify is_active state of explore entitlement' do
              Omnichannel::ExploreEntitlementSyncJob.expects(:enqueue).with(account_id: user.account.id, user_id: user.id, modify_is_active: false)
              perform
            end

            it 'syncs guide entitlement' do
              Omnichannel::GuideEntitlementSyncJob.expects(:perform)
              perform
            end

            it 'does not modify is_active state of guide entitlement' do
              Omnichannel::GuideEntitlementSyncJob.expects(:enqueue).with(account_id: user.account.id, user_id: user.id, modify_is_active: false)
              perform
            end
          end

          describe 'support role is not changed' do
            let(:user) { users(:multiproduct_custom_agent) }

            it 'does not sync explore entitlement' do
              Omnichannel::ExploreEntitlementSyncJob.expects(:perform).never
              perform
            end

            it 'does not sync guide entitlement' do
              Omnichannel::GuideEntitlementSyncJob.expects(:perform).never
              perform
            end
          end
        end

        describe 'without custom roles' do
          let(:user) { users(:multiproduct_contributor) }
          let(:support_entitlement) { 'agent' }

          it 'does not sync explore entitlement' do
            Omnichannel::ExploreEntitlementSyncJob.expects(:perform).never
            perform
          end

          it 'does not sync guide entitlement' do
            Omnichannel::GuideEntitlementSyncJob.expects(:perform).never
            perform
          end
        end
      end

      describe 'error' do
        let(:user) { users(:multiproduct_support_agent) }
        let(:support_entitlement) { nil }

        before do
          Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).raises(Kragle::ServerError)
        end

        it 'instruments what went wrong and re-raises error' do
          Rails.logger.expects(:error)
          assert_raises(Kragle::ServerError) { perform }
        end
      end
    end

    describe 'unrecognized strategy' do
      let(:user) { users(:multiproduct_support_agent) }
      let(:support_entitlement) { nil }
      let(:strategy) { :pulh }

      it 'raises an error' do
        assert_raises(RuntimeError) { perform }
      end
    end
  end
end
