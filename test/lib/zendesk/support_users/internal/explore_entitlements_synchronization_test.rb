require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::ExploreEntitlementsSynchronization do
  fixtures :users, :accounts, :permission_sets

  let(:synchronization) { Zendesk::SupportUsers::Internal::ExploreEntitlementsSynchronization.new(user_model) }
  let(:user_model) { User.new(account: account) }
  let(:account) { Account.new }

  before { user_model.roles = 4 }

  describe '#perform?' do
    let(:changes) { {} }

    before do
      user_model.stubs(:previous_changes).returns(changes)
    end

    describe 'account without custom roles enabled' do
      before do
        account.stubs(:has_permission_sets?).returns(false)
      end

      it 'returns false' do
        assert_equal false, synchronization.perform?
      end

      describe 'agent downgraded to end user' do
        let(:changes) { { 'roles' => [4, 0] } }
        before { user_model.roles = 0 }

        it 'returns false' do
          assert_equal false, synchronization.perform?
        end
      end

      describe 'user upgraded to admin' do
        let(:changes) { { 'roles' => [4, 2] } }
        before { user_model.roles = 2 }

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end

      describe 'user assigned new system role(LightAgent, ChatAgent, Contributor)' do
        let(:changes) { { 'permission_set_id' => [nil, 124] } }
        before { user_model.permission_set_id = 124 }

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end
    end

    describe 'account with custom roles enabled' do
      before do
        account.stubs(:has_permission_sets?).returns(true)
      end

      describe 'user role does not change' do
        let(:changes) { {} }

        it 'returns false' do
          assert_equal false, synchronization.perform?
        end
      end

      describe 'user assigned new custom role' do
        let(:changes) { { 'permission_set_id' => [123, 124] } }

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end

      describe 'user downgraded to end user' do
        let(:changes) { { 'roles' => [4, 0] } }
        before { user_model.roles = 0 }

        it 'returns true' do
          refute synchronization.perform?
        end
      end

      describe 'user upgraded to admin' do
        let(:changes) { { 'roles' => [4, 2] } }

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end
    end
  end

  describe '#perform!' do
    it 'enqueues a sync job' do
      Omnichannel::ExploreEntitlementSyncJob.expects(:enqueue)
      synchronization.perform!
    end
  end
end
