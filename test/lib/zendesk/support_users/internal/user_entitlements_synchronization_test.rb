require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::UserEntitlementsSynchronization do
  fixtures :users, :accounts, :permission_sets

  let(:synchronization) { Zendesk::SupportUsers::Internal::UserEntitlementsSynchronization.new(user_model) }
  let(:user_model) { User.new(account: account) }
  let(:account) { Account.new }
  let(:contributor_permission_set) { permission_sets(:multiproduct_contributor_permission_set) }
  let(:custom_permission_set) { permission_sets(:multiproduct_custom_permission_set) }
  let(:entitlements_sync_enabled) { true }

  before { Zendesk::SupportAccounts::Account.any_instance.stubs(:entitlements_sync_enabled?).returns(entitlements_sync_enabled) }

  describe '#perform_synchronously!' do
    let(:perform!) { synchronization.perform_synchronously! }

    it 'synchronizes the user' do
      Zendesk::SupportUsers::EntitlementSynchronizer.expects(:perform).with(user_model)
      perform!
    end

    describe 'failed synchronization' do
      before do
        Zendesk::SupportUsers::EntitlementSynchronizer.expects(:perform).raises(Kragle::ResponseError)
        user_model.name = 'Andy'
      end

      it 'raises an error' do
        assert_raises(ActiveRecord::RecordInvalid) { perform! }
        assert_equal 'Failed to save user Andy. Please try again.', user_model.errors.messages[:base].first
      end
    end
  end

  describe '#perform_asynchronously!' do
    let(:perform!) { synchronization.perform_asynchronously! }
    let(:user_id) { 78348943 }
    let(:account_id) { 98439834 }
    let(:audit_headers) { {actor_id: -1, ip_address: nil} }

    before do
      user_model.stubs(:id).returns(user_id)
      account.stubs(:id).returns(account_id)
    end

    it 'enqueues a job to synchronize the user' do
      Omnichannel::EntitlementSyncJob.expects(:enqueue).with(account_id: account_id, user_id: user_id, audit_headers: audit_headers)
      perform!
    end
  end

  describe '#perform_asynchronously?' do
    let(:perform?) { synchronization.perform_asynchronously? }
    let(:changes) { {} }
    before do
      user_model.stubs(:previous_changes).returns(changes)
    end

    describe_with_arturo_enabled :ocp_synchronous_entitlements_update do
      it 'returns false' do
        assert_equal false, perform?
      end
    end

    describe_with_arturo_disabled :ocp_synchronous_entitlements_update do
      describe 'for end user' do
        let(:user_model) { users(:multiproduct_end_user) }

        describe 'and user entitlement is not changed' do
          let(:changes) { { 'name' => ['end user', 'good end user'] } }

          it 'returns false' do
            assert_equal false, perform?
          end
        end

        describe 'and user entitlement is changed but not yet save' do
          before do
            user_model.roles = ::Role::ADMIN.id
          end

          it 'returns true' do
            assert(perform?)
          end
        end

        describe 'and user is upgraded to agent' do
          let(:changes) { { 'roles' => [0, 4] } }

          it 'returns true' do
            assert(perform?)
          end

          describe 'and entitlements sync is not enabled for account' do
            let(:entitlements_sync_enabled) { false }

            it 'returns false' do
              assert_equal false, perform?
            end
          end
        end

        describe 'and user is upgraded to admin' do
          let(:changes) { { 'roles' => [0, 2] } }

          it 'returns true' do
            assert(perform?)
          end

          describe 'and entitlements sync is not enabled for account' do
            let(:entitlements_sync_enabled) { false }

            it 'returns false' do
              assert_equal false, perform?
            end
          end
        end

        describe 'and user is upgraded to contributor' do
          let(:user_model) { users(:multiproduct_contributor) }
          let(:changes) { { 'roles' => [0, 4], 'permission_set_id' => [nil, contributor_permission_set.id] } }

          it 'returns false' do
            assert_equal false, perform?
          end
        end
      end

      describe 'for agent' do
        let(:user_model) { users(:multiproduct_support_agent) }

        describe 'and agent is downgraded to end user' do
          let(:changes) { { 'roles' => [4, 0] } }

          it 'returns true' do
            assert(perform?)
          end

          describe 'and entitlements sync is not enabled for account' do
            let(:entitlements_sync_enabled) { false }

            it 'returns false' do
              assert_equal false, perform?
            end
          end
        end

        describe 'and agent permission_set is changed' do
          let(:changes) { { 'permission_set_id' => [nil, custom_permission_set.id] } }

          it 'returns true' do
            assert(perform?)
          end

          describe 'and entitlements sync is not enabled for account' do
            let(:entitlements_sync_enabled) { false }

            it 'returns false' do
              assert_equal false, perform?
            end
          end
        end
      end

      describe 'for agent with contributor role' do
        let(:user_model) { users(:multiproduct_contributor) }

        describe 'and agent permission_set is changed' do
          let(:changes) { { 'permission_set_id' => [contributor_permission_set.id, custom_permission_set.id] } }

          it 'returns true' do
            assert(perform?)
          end

          describe 'and entitlements sync is not enabled for account' do
            let(:entitlements_sync_enabled) { false }

            it 'returns false' do
              assert_equal false, perform?
            end
          end
        end

        describe 'and agent is downgraded to end user' do
          let(:changes) { { 'roles' => [4, 0], 'permission_set_id' => [contributor_permission_set.id, nil] } }

          it 'returns true' do
            assert(perform?)
          end
        end
      end
    end
  end

  describe '#perform_synchronously?' do
    let(:perform?) { synchronization.perform_synchronously? }
    let(:changes) { {} }
    before do
      user_model.stubs(:previous_changes).returns(changes)
    end

    describe_with_arturo_disabled :ocp_synchronous_entitlements_update do
      it 'returns false' do
        assert_equal false, perform?
      end
    end

    describe_with_arturo_enabled :ocp_synchronous_entitlements_update do
      describe_with_arturo_disabled :central_admin_staff_mgmt_roles_tab do
        before do
          user_model.roles = ::Role::ADMIN.id
        end

        it 'returns false' do
          refute perform?
        end
      end

      describe_with_arturo_enabled :central_admin_staff_mgmt_roles_tab do
        describe 'for end user' do
          let(:user_model) { users(:multiproduct_end_user) }

          describe 'and user entitlement is not changed' do
            let(:changes) { { 'name' => ['end user', 'good end user'] } }

            it 'returns false' do
              assert_equal false, perform?
            end
          end

          describe 'and user entitlement is changed but not yet save' do
            before do
              user_model.roles = ::Role::ADMIN.id
            end

            it 'returns true' do
              assert(perform?)
            end
          end

          describe 'and user is upgraded to agent' do
            let(:changes) { { 'roles' => [0, 4] } }

            it 'returns true' do
              assert(perform?)
            end

            describe 'and entitlements sync is not enabled for account' do
              let(:entitlements_sync_enabled) { false }

              it 'returns false' do
                assert_equal false, perform?
              end
            end
          end

          describe 'and user is upgraded to admin' do
            let(:changes) { { 'roles' => [0, 2] } }

            it 'returns true' do
              assert(perform?)
            end

            describe 'and entitlements sync is not enabled for account' do
              let(:entitlements_sync_enabled) { false }

              it 'returns false' do
                assert_equal false, perform?
              end
            end
          end

          describe 'and user is upgraded to contributor' do
            let(:user_model) { users(:multiproduct_contributor) }
            let(:changes) { { 'roles' => [0, 4], 'permission_set_id' => [nil, contributor_permission_set.id] } }

            it 'returns false' do
              assert_equal false, perform?
            end
          end
        end

        describe 'for agent' do
          let(:user_model) { users(:multiproduct_support_agent) }

          describe 'and agent is downgraded to end user' do
            let(:changes) { { 'roles' => [4, 0] } }

            it 'returns true' do
              assert(perform?)
            end

            describe 'and entitlements sync is not enabled for account' do
              let(:entitlements_sync_enabled) { false }

              it 'returns false' do
                assert_equal false, perform?
              end
            end
          end

          describe 'and agent permission_set is changed' do
            let(:changes) { { 'permission_set_id' => [nil, custom_permission_set.id] } }

            it 'returns true' do
              assert(perform?)
            end

            describe 'and entitlements sync is not enabled for account' do
              let(:entitlements_sync_enabled) { false }

              it 'returns false' do
                assert_equal false, perform?
              end
            end
          end
        end

        describe 'for agent with contributor role' do
          let(:user_model) { users(:multiproduct_contributor) }

          describe 'and agent permission_set is changed' do
            let(:changes) { { 'permission_set_id' => [contributor_permission_set.id, custom_permission_set.id] } }

            it 'returns true' do
              assert(perform?)
            end

            describe 'and entitlements sync is not enabled for account' do
              let(:entitlements_sync_enabled) { false }

              it 'returns false' do
                assert_equal false, perform?
              end
            end
          end

          describe 'and agent is downgraded to end user' do
            let(:changes) { { 'roles' => [4, 0], 'permission_set_id' => [contributor_permission_set.id, nil] } }

            it 'returns true' do
              assert(perform?)
            end
          end
        end
      end
    end
  end
end
