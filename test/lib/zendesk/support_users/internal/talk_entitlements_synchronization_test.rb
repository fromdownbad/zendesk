require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization do
  fixtures :users, :accounts, :permission_sets

  let(:synchronization) { Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.new(user_model) }
  let(:user_model) { User.new(account: account) }
  let(:account) { Account.new }

  before { user_model.roles = 4 }

  describe '#perform?' do
    let(:user_changes) { {} }

    before do
      user_model.stubs(:previous_changes).returns(user_changes)
    end

    describe 'agent downgraded to end user' do
      let(:user_changes) { { 'roles' => [4, 0] } }
      before { user_model.roles = 0 }

      it 'returns false' do
        assert_equal false, synchronization.perform?
      end
    end

    describe 'user upgraded from end user' do
      let(:user_changes) { { 'roles' => [0, 4] } }

      it 'returns true' do
        assert(synchronization.perform?)
      end
    end

    describe 'when permission_set_id changes' do
      let(:user_changes) { { 'permission_set_id' => [124, nil] } }

      it 'returns true' do
        assert(synchronization.perform?)
      end
    end

    describe 'when user_seat changes' do
      let(:user_seat_model) { UserSeat.new(account: account, user: user_model, seat_type: seat_type) }
      let(:seat_type) { 'voice' }
      let(:synchronization) { Zendesk::SupportUsers::Internal::TalkEntitlementsSynchronization.new(user_model, user_seat: user_seat_model) }
      let(:user_seat_changes) { {} }

      before do
        user_seat_model.stubs(:previous_changes).returns(user_seat_changes)
      end

      describe 'user_seat created' do
        let(:user_seat_changes) { { 'seat_type' => [nil, 'voice'] } }

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end

      describe 'user_seat destroyed' do
        before do
          user_seat_model.stubs(:destroyed?).returns(true)
        end

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end

      describe 'when seat type is not talk' do
        let(:user_seat_changes) { { 'seat_type' => [nil, 'connect'] } }
        let(:seat_type) { 'connect' }

        it 'returns false' do
          refute(synchronization.perform?)
        end
      end
    end
  end

  describe '#perform!' do
    it 'enqueues a sync job' do
      Omnichannel::TalkEntitlementSyncJob.expects(:enqueue)
      synchronization.perform!
    end
  end
end
