require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::ChatAdminValidator do
  let(:account) { accounts(:multiproduct) }
  let(:user) { users(:multiproduct_contributor) }
  let(:chat_admin_validator) { Zendesk::SupportUsers::Internal::ChatAdminValidator.new(user) }

  describe '#perform?' do
    let(:new_roles) { Role::END_USER.id }
    let(:staff_count) { 1 }
    let(:chat_role) { 'admin' }
    let(:entitlements) do
      {
        chat: chat_role,
        connect: nil,
        explore: nil,
        guide: 'viewer',
        sell: nil,
        support: nil,
        talk: nil,
        acme_app: nil
      }
    end

    before do
      user.roles = new_roles
      Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).returns(entitlements)
      Zendesk::StaffClient.any_instance.stubs(:staff_count).returns(staff_count)
    end

    it 'returns true' do
      assert chat_admin_validator.perform?
    end

    describe 'when agent is not downgrading to end user' do
      let(:new_roles) { Role::ADMIN.id }

      it 'returns false' do
        refute chat_admin_validator.perform?
      end
    end

    describe 'when user is not chat admin' do
      let(:chat_role) { 'agent' }

      it 'returns false' do
        refute chat_admin_validator.perform?
      end
    end

    describe 'when user is not last chat admin' do
      let(:staff_count) { 2 }

      it 'returns false' do
        refute chat_admin_validator.perform?
      end
    end
  end

  describe '#perform!' do
    it 'makes account owner the new Chat admin' do
      Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).once.
        with(account.owner.id, chat: { name: 'admin', is_active: true })
      chat_admin_validator.perform!
    end
  end
end
