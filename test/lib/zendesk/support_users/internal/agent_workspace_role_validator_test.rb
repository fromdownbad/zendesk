require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::AgentWorkspaceRoleValidator do
  fixtures :users, :accounts

  let(:account) { accounts(:multiproduct) }
  let(:user) { users(:multiproduct_contributor) }
  let(:agent_workspace_role_validator) { Zendesk::SupportUsers::Internal::AgentWorkspaceRoleValidator.new(user) }
  let(:current_user) { users(:minimum_admin) }

  describe '#perform?' do
    let(:changes) { {} }
    let(:is_chat_agent) { false }
    let(:is_contributor) { false }
    let(:is_system_user) { false }

    before do
      user.stubs(is_chat_agent?: is_chat_agent)
      user.stubs(is_contributor?: is_contributor)
      user.stubs(:changes).returns(changes)
      CIA.stubs(:current_actor).returns(current_user)
      current_user.stubs(:is_system_user?).returns(is_system_user)
      Account.any_instance.stubs(:has_polaris?).returns(has_polaris)
    end

    describe 'when an account has agent workspace enabled and user is NOT a system user' do
      let(:has_polaris) { true }

      describe 'when user is updating roles' do
        let(:changes) { { 'permission_set_id' => [nil, 10176] } }

        describe 'when user is NOT updating role to chat-only or contributor' do
          let(:is_chat_agent) { false }
          let(:is_contributor) { false }

          it 'returns false' do
            refute agent_workspace_role_validator.perform?
          end
        end

        describe 'when user is updating role to chat-only' do
          let(:is_chat_agent) { true }
          let(:is_contributor) { false }

          it 'returns true' do
            assert agent_workspace_role_validator.perform?
          end
        end

        describe 'when user is updating role to contributor' do
          let(:is_chat_agent) { false }
          let(:is_contributor) { true }

          it 'returns true' do
            assert agent_workspace_role_validator.perform?
          end
        end
      end

      describe 'when user is NOT updating roles' do
        let(:changes) { { 'name' => 'Bobby Green' } }

        it 'returns false' do
          refute agent_workspace_role_validator.perform?
        end
      end
    end

    describe 'when an account does NOT have agent workspace enabled' do
      let(:has_polaris) { false }
      let(:is_chat_agent) { true }
      let(:is_contributor) { false }

      it 'returns false' do
        refute agent_workspace_role_validator.perform?
      end
    end

    describe 'when the current user is a system user' do
      let(:has_polaris) { true }
      let(:is_chat_agent) { true }
      let(:is_contributor) { false }
      let(:is_system_user) { true }

      it 'returns false' do
        refute agent_workspace_role_validator.perform?
      end
    end
  end

  describe '#perform!' do
    it 'raise ActiveRecord::RecordInvalid with correct error message' do
      assert_raises ActiveRecord::RecordInvalid do
        agent_workspace_role_validator.perform!
      end

      assert_equal ::I18n.t('activerecord.errors.models.users.unassignable_role'), user.errors.messages[:base].first
    end
  end
end
