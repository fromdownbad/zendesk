require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::AuditHeaders do
  class TestClassWithAuditHeaders
    include Zendesk::SupportUsers::Internal::AuditHeaders
  end

  describe '#audit_headers' do
    let(:subject) { TestClassWithAuditHeaders.new }
    let(:actor_id) { 123 }
    let(:ip_address) { '123.123.123.123' }

    before do
      CIA.stubs(:current_actor).returns(stub(id: actor_id))
      CIA.stubs(:current_transaction).returns(stub(fetch: ip_address))
    end

    it 'has audit_headers' do
      assert subject.respond_to? :audit_headers
    end

    it 'includes actor_id' do
      assert_equal subject.audit_headers[:actor_id], actor_id
    end

    it 'includes ip_address' do
      assert_equal subject.audit_headers[:ip_address], ip_address
    end
  end
end
