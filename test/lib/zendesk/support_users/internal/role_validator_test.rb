require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::RoleValidator do
  let(:account) { Account.new }
  let(:user) { User.new(account: account) }
  let(:roles) { ::Role::ADMIN.id }
  let(:permission_set_id) { 12345 }
  let(:validator) { Zendesk::SupportUsers::Internal::RoleValidator.new(user) }

  before do
    user.roles = roles
    user.permission_set_id = permission_set_id
  end

  describe '#perform?' do
    describe 'when user is not admin' do
      let(:roles) { ::Role::AGENT.id }

      it 'returns false' do
        refute validator.perform?
      end
    end

    describe 'when user is admin' do
      describe 'when permissions_allow_admin_permission_sets arturo is enabled' do
        before { Arturo.enable_feature!(:permissions_allow_admin_permission_sets) }

        it 'returns false' do
          refute validator.perform?
        end
      end

      describe 'when user does not have permission_set_id' do
        let(:permission_set_id) { nil }

        it 'returns false' do
          refute validator.perform?
        end
      end

      describe 'when user has permission_set_id' do
        it 'returns true' do
          assert validator.perform?
        end
      end
    end
  end

  describe 'perform!' do
    it 'changes user roles to agent' do
      assert user.is_admin?
      refute user.permission_set_id.nil?
      validator.perform!
      assert user.is_admin?
      assert user.permission_set_id.nil?
    end
  end
end
