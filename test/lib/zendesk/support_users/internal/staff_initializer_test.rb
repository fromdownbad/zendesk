require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::StaffInitializer do
  let(:staff_initializer) { Zendesk::SupportUsers::Internal::StaffInitializer.new(user) }
  let(:user) { User.new(account: account) }
  let(:account) { Account.new(multiproduct: multiproduct) }
  let(:multiproduct) { true }

  describe '#perform?' do
    let(:id) { nil }
    let(:multiproduct) { true }
    let(:is_agent) { true }

    before do
      user.stubs(:is_agent?).returns(is_agent)
      user.stubs(:id).returns(id)
    end

    describe_with_arturo_enabled :ocp_synchronous_entitlements_update do
      describe_with_arturo_enabled :central_admin_staff_mgmt_roles_tab do
        it 'is true' do
          assert staff_initializer.perform?
        end

        describe 'existing user' do
          let(:id) { stub }

          it 'is false' do
            refute staff_initializer.perform?
          end
        end

        describe 'end user' do
          let(:is_agent) { false }

          it 'is false' do
            refute staff_initializer.perform?
          end
        end

        describe 'non-shell account' do
          let(:multiproduct) { false }

          it 'is false' do
            refute staff_initializer.perform?
          end
        end
      end

      describe_with_arturo_disabled :central_admin_staff_mgmt_roles_tab do
        it 'is false' do
          refute staff_initializer.perform?
        end
      end
    end

    describe_with_arturo_disabled :ocp_synchronous_entitlements_update do
      it 'is false' do
        refute staff_initializer.perform?
      end
    end
  end

  describe '#perform!' do
    let(:contributor_role) { PermissionSet.new(name: 'contributor_test') }

    before do
      user.stubs(:save!)
      PermissionSet.stubs(:enable_contributor!).returns(contributor_role)
    end

    it 'saves the user' do
      user.expects(:save!)
      staff_initializer.perform!
    end

    it 'uses the Contributor role' do
      PermissionSet.expects(:enable_contributor!)
      staff_initializer.perform!
    end

    it 'does not assign restriction_id to user' do
      staff_initializer.perform!
      assert_nil user.restriction_id
    end

    describe 'with custom role' do
      let(:permission_set) { PermissionSet.new }
      let(:user) { User.new(permission_set: permission_set) }

      it 'does not modify permission set' do
        staff_initializer.perform!
        assert_equal permission_set, user.permission_set
      end
    end

    describe 'admin user' do
      before do
        user.roles = ::Role::ADMIN.id
      end

      it 'does not modify roles' do
        staff_initializer.perform!
        assert_equal ::Role::ADMIN.id, user.roles
      end
    end
  end
end
