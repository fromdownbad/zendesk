require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::GuideEntitlementsSynchronization do
  fixtures :users, :accounts, :permission_sets

  let(:synchronization) { Zendesk::SupportUsers::Internal::GuideEntitlementsSynchronization.new(user_model) }
  let(:user_model) { User.new(account: account) }
  let(:account) { Account.new }
  let(:entitlements_sync_enabled) { true }
  before do
    user_model.roles = 4
    Zendesk::SupportAccounts::Account.any_instance.stubs(:entitlements_sync_enabled?).returns(entitlements_sync_enabled)
  end

  describe '#perform?' do
    let(:changes) { {} }

    before do
      user_model.stubs(:previous_changes).returns(changes)
    end

    describe 'without custom role' do
      before do
        user_model.stubs(:permission_set).returns(nil)
      end

      describe 'downgrade to end user' do
        let(:changes) { { 'roles' => [4, 0] } }

        before do
          user_model.roles = 0
        end

        it 'returns false' do
          assert_equal false, synchronization.perform?
        end
      end

      describe 'roles column is changed' do
        let(:changes) { { 'roles' => [0, 4] } }

        it 'returns true' do
          assert(synchronization.perform?)
        end

        describe 'when entitlements sync is disabled' do
          let(:entitlements_sync_enabled) { false }

          it 'returns false' do
            assert_equal false, synchronization.perform?
          end
        end
      end

      describe 'is_moderator column is changed' do
        let(:changes) { { 'is_moderator' => [false, true] } }

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end

      describe 'permission set changes' do
        let(:changes) { { 'permission_set_id' => [123, nil] } }

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end
    end

    describe 'with custom role' do
      let(:permission_set) { PermissionSet.new }

      before do
        user_model.stubs(:permission_set).returns(permission_set)
      end

      describe 'roles column is changed' do
        let(:changes) { { 'roles' => [0, 4] } }

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end

      describe 'is_moderator column is changed' do
        let(:changes) { { 'is_moderator' => [false, true] } }

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end

      describe 'permission set changes' do
        let(:changes) { { 'permission_set_id' => [123, 124] } }

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end
    end
  end

  describe '#perform!' do
    it 'enqueues a sync job' do
      Omnichannel::GuideEntitlementSyncJob.expects(:enqueue)
      synchronization.perform!
    end
  end
end
