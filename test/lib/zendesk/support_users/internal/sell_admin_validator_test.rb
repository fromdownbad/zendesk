require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::SellAdminValidator do
  let(:account) { accounts(:multiproduct) }
  let(:user) { users(:multiproduct_contributor) }
  let(:sell_admin_validator) { Zendesk::SupportUsers::Internal::SellAdminValidator.new(user) }

  describe '#perform?' do
    let(:new_roles) { Role::END_USER.id }
    let(:staff_count) { 1 }
    let(:sell_role) { 'admin' }
    let(:entitlements) do
      {
        sell: sell_role,
        connect: nil,
        explore: nil,
        guide: 'viewer',
        chat: nil,
        support: nil,
        talk: nil,
        acme_app: nil
      }
    end

    before do
      user.roles = new_roles
      Zendesk::StaffClient.any_instance.stubs(:get_entitlements!).returns(entitlements)
      Zendesk::StaffClient.any_instance.stubs(:staff_count).returns(staff_count)
    end

    describe_with_arturo_enabled :ocp_last_sell_admin_downgrade do
      it 'returns true' do
        assert sell_admin_validator.perform?
      end

      describe 'when agent is not downgrading to end user' do
        let(:new_roles) { Role::ADMIN.id }

        it 'returns false' do
          refute sell_admin_validator.perform?
        end
      end

      describe 'when user is not sell admin' do
        let(:sell_role) { 'agent' }

        it 'returns false' do
          refute sell_admin_validator.perform?
        end
      end

      describe 'when user is not last sell admin' do
        let(:staff_count) { 2 }

        it 'returns false' do
          refute sell_admin_validator.perform?
        end
      end
    end

    describe_with_arturo_disabled :ocp_last_sell_admin_downgrade do
      it 'returns false' do
        refute sell_admin_validator.perform?
      end
    end
  end

  describe '#perform!' do
    it 'raises an error' do
      assert_raises(ActiveRecord::RecordInvalid) { sell_admin_validator.perform! }
      assert_equal 'Cannot downgrade the last Sell admin', user.errors.messages[:base].first
    end
  end
end
