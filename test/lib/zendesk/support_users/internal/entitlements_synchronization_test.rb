require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::EntitlementsSynchronization do
  let(:entitlements_synchronization) { Zendesk::SupportUsers::Internal::EntitlementsSynchronization.new(user_model) }
  let(:account) { Account.new }
  let(:user_model) { User.new(account: account) }
  let(:support_synchronization) { stub(perform_asynchronously?: support_sync) }
  let(:guide_synchronization) { stub(perform?: guide_sync) }
  let(:explore_synchronization) { stub(perform?: explore_sync) }
  let(:talk_synchronization) { stub(perform?: talk_sync) }
  let(:support_sync) { true }
  let(:guide_sync) { true }
  let(:explore_sync) { true }
  let(:talk_sync) { true }

  before do
    entitlements_synchronization.stubs(:support_entitlements_synchronization).returns(support_synchronization)
    entitlements_synchronization.stubs(:guide_entitlements_synchronization).returns(guide_synchronization)
    entitlements_synchronization.stubs(:explore_entitlements_synchronization).returns(explore_synchronization)
    entitlements_synchronization.stubs(:talk_entitlements_synchronization).returns(talk_synchronization)
  end

  describe '#perform!' do
    let(:audit_headers) { {actor_id: -1, ip_address: nil} }
    it 'enqueues EntitlementsSyncJob with account id, user id and products that need sync' do
      Omnichannel::EntitlementsSyncJob.expects(:enqueue).with(account_id: account.id, user_id: user_model.id, products: ['support', 'guide', 'explore', 'talk'], audit_headers: audit_headers)
      entitlements_synchronization.perform!
    end
  end

  describe '#perform?' do
    describe 'when none of the syncs need to perform' do
      let(:support_sync) { false }
      let(:guide_sync) { false }
      let(:explore_sync) { false }
      let(:talk_sync) { false }

      it 'returns false' do
        refute entitlements_synchronization.perform?
      end
    end

    describe 'when one of the syncs need to perform' do
      let(:guide_sync) { false }
      let(:explore_sync) { false }
      let(:talk_sync) { false }

      it 'returns true' do
        assert entitlements_synchronization.perform?
      end
    end

    describe 'when all of the syncs need to perform' do
      it 'returns true' do
        assert entitlements_synchronization.perform?
      end
    end
  end

  describe '#products_to_sync' do
    describe 'when none of the syncs need to perform' do
      let(:support_sync) { false }
      let(:guide_sync) { false }
      let(:explore_sync) { false }
      let(:talk_sync) { false }

      it 'returns empty array' do
        assert_equal entitlements_synchronization.products_to_sync, []
      end
    end

    describe 'when one of the syncs need to perform' do
      let(:guide_sync) { false }
      let(:explore_sync) { false }
      let(:talk_sync) { false }

      it 'returns array with the product that needs sync' do
        assert_equal entitlements_synchronization.products_to_sync, ['support']
      end
    end

    describe 'when all of the syncs need to perform' do
      it 'returns array with all the products that need sync' do
        assert_equal entitlements_synchronization.products_to_sync, ['support', 'guide', 'explore', 'talk']
      end
    end
  end
end
