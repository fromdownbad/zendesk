require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportUsers::Internal::SppLightAgentRoleValidator do
  let(:account) { Account.new(subdomain: 'asdf') }
  let(:user) { User.new(account: account) }
  let(:validator) { Zendesk::SupportUsers::Internal::SppLightAgentRoleValidator.new(user) }

  describe '#perform?' do
    describe 'when account is SPP' do
      before { account.stubs(:spp?).returns(true) }

      describe 'and user is light-agent' do
        before { user.stubs(:is_light_agent?).returns(true) }
        it 'returns true' do
          assert validator.perform?
        end
      end

      describe 'and user is NOT light-agent' do
        before { user.stubs(:is_light_agent?).returns(false) }
        it 'returns false' do
          refute validator.perform?
        end
      end
    end

    describe 'when account is NOT SPP' do
      before { account.stubs(:spp?).returns(false) }

      describe 'and user is light-agent' do
        before { user.stubs(:is_light_agent?).returns(true) }
        it 'returns false' do
          refute validator.perform?
        end
      end

      describe 'and user is NOT light-agent' do
        before { user.stubs(:is_light_agent?).returns(false) }
        it 'returns false' do
          refute validator.perform?
        end
      end
    end
  end

  describe 'perform!' do
    before do
      account.stubs(:spp?).returns(true)
      user.stubs(:is_light_agent?).returns(true)
    end

    describe 'when number of light agents remaining > 0' do
      before do
        validator.stubs(:seats_remaining_spp_light_agents).returns(1)
      end
      it 'an error is NOT raised' do
        validator.perform!
      end
    end

    describe 'when number of light agents remaining < 1' do
      before do
        validator.stubs(:seats_remaining_spp_light_agents).returns(0)
      end
      it 'an error is raised' do
        assert_raises ActiveRecord::RecordInvalid do
          validator.perform!
        end
      end
    end

    describe '#seats_remaining_spp_light_agents' do
      describe_with_arturo_enabled :ocp_spp_light_agent_cap_alternative do
        before do
          validator.send(:staff_client).expects(:seats_remaining_spp_light_agents!).never
          validator.send(:account_client).expects(:max_light_agents).returns(5).once
        end

        it 'spp light-agent seats remaining are retrieved from account services' do
          validator.perform!
        end
      end

      describe_with_arturo_disabled :ocp_spp_light_agent_cap_alternative do
        before do
          validator.send(:staff_client).expects(:seats_remaining_spp_light_agents!).returns(5).once
          validator.send(:account_client).expects(:max_light_agents).never
        end

        it 'spp light-agent seats remaining are retrieved from staff services' do
          validator.perform!
        end
      end
    end
  end
end
