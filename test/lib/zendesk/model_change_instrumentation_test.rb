require_relative '../../support/test_helper'

SingleCov.covered!

describe Zendesk::ModelChangeInstrumentation do
  let(:model) { User.new }
  let(:exec_context) { stub(caller_locations: trace) }
  let(:trace) { [] }
  let(:columns_to_instrument) { [:roles, :permission_set_id] }
  let(:source_of_instrument) { 'user_change' }
  let(:prevent_unknown_update_from) { [] }

  let(:instrumentation) do
    Zendesk::ModelChangeInstrumentation.new(
      model: model,
      exec_context: exec_context,
      prevent_unknown_update_from: prevent_unknown_update_from,
      columns_to_instrument: columns_to_instrument,
      source_of_instrument: source_of_instrument
    )
  end

  describe '#perform' do
    let(:version_sha) { SecureRandom.hex }
    let(:statsd_client) { stub_for_statsd }

    let(:perform) { instrumentation.perform }

    before do
      model.stubs(:previous_changes).returns(previous_changes)
      Zendesk::StatsD::Client.stubs(:new).with(namespace: 'model_change_instrumentation').returns(statsd_client)
    end

    describe 'when model is destroyed' do
      before do
        model.stubs(:destroyed?).returns(true)
      end

      let(:trace) do
        [
          "/opt/zendesk/versions/#{version_sha}/app/lib/zendesk/support/user.rb:10:in `save!`'",
          "/opt/zendesk/versions/#{version_sha}/app/app/controllers/api/v2/users_controller.rb:731:in `update'`",
        ]
      end

      let(:previous_changes) { {} }

      let(:tags) do
        ['source:lib/zendesk/support/user.rb', 'line:10', 'source_of_instrument:user_change']
      end

      it 'reports metrics to Datadog' do
        statsd_client.expects(:increment).with('user_changed', tags: tags).once
        perform
      end
    end

    describe 'when instrumented columns have not been changed' do
      let(:previous_changes) do
        { "name" => ['old_name', 'new_name'] }
      end

      it 'does not reports metrics' do
        Zendesk::StatsD::Client.expects(:new).never
        perform
      end
    end

    describe 'when one of instrumented columns has been changed' do
      let(:previous_changes) do
        { "roles" => [4, 2], "restriction_id" => [1, 0] }
      end

      describe 'and the first matching trace is within Cookie Monster namespace lib/zendesk/support' do
        let(:trace) do
          [
            "/opt/zendesk/versions/#{version_sha}/app/lib/zendesk/support/user.rb:10:in `save!`'",
            "/opt/zendesk/versions/#{version_sha}/app/app/controllers/api/v2/users_controller.rb:731:in `update'`",
          ]
        end

        let(:tags) do
          ['roles_changed:true', 'source:lib/zendesk/support/user.rb', 'line:10', 'source_of_instrument:user_change']
        end

        it 'reports change source metrics to Datadog, with project irrelevant paths stripped' do
          statsd_client.expects(:increment).with('user_changed', tags: tags).once
          perform
        end
      end

      describe 'and the first matching trace is a controller' do
        let(:trace) do
          [
            "/opt/zendesk/versions/#{version_sha}/app/app/models/user.rb:124:in `save!'",
            "/opt/zendesk/versions/#{version_sha}/app/app/controllers/api/v2/users_controller.rb:731:in `update'",
          ]
        end

        let(:tags) do
          ['roles_changed:true', 'source:app/controllers/api/v2/users_controller.rb', 'line:731', 'source_of_instrument:user_change']
        end

        it 'reports change source metrics to Datadog, with project irrelevant paths stripped' do
          statsd_client.expects(:increment).with('user_changed', tags: tags).once
          perform
        end
      end

      describe 'and the first matching trace is a rake task' do
        let(:trace) do
          [
            "/opt/zendesk/versions/#{version_sha}/app/app/models/user.rb:124:in `save'",
            "/opt/zendesk/versions/#{version_sha}/app/lib/tasks/user_batch_update.rake:39:in `task'",
          ]
        end

        let(:tags) do
          ['roles_changed:true', 'source:lib/tasks/user_batch_update.rake', 'line:39', 'source_of_instrument:user_change']
        end

        it 'reports change source metrics to Datadog, with project irrelevant paths stripped' do
          statsd_client.expects(:increment).with('user_changed', tags: tags).once
          perform
        end
      end

      describe 'and the first matching trace is a delayed job' do
        let(:trace) do
          [
            "/opt/zendesk/versions/#{version_sha}/app/app/models/user.rb:124:in `save'",
            "/opt/zendesk/versions/#{version_sha}/app/app/models/jobs/user_bulk_update_job.rb:39:in `work'",
          ]
        end

        let(:tags) do
          ['roles_changed:true', 'source:app/models/jobs/user_bulk_update_job.rb', 'line:39', 'source_of_instrument:user_change']
        end

        it 'reports change source metrics to Datadog, with project irrelevant paths stripped' do
          statsd_client.expects(:increment).with('user_changed', tags: tags).once
          perform
        end
      end

      describe 'and the first matching trace is a maintenance job' do
        let(:trace) do
          [
            "/opt/zendesk/versions/#{version_sha}/app/app/models/user.rb:124:in `save'",
            "/opt/zendesk/versions/#{version_sha}/app/lib/zendesk/maintenance/jobs/api_account_signup_cleanup_job.rb:7:in 'execute'",
          ]
        end

        let(:tags) do
          ['roles_changed:true', 'source:lib/zendesk/maintenance/jobs/api_account_signup_cleanup_job.rb', 'line:7', 'source_of_instrument:user_change']
        end

        it 'reports change source metrics to Datadog, with project irrelevant paths stripped' do
          statsd_client.expects(:increment).with('user_changed', tags: tags).once
          perform
        end
      end

      describe 'and the first matching trace is a delayed job in gem' do
        let(:trace) do
          [
            "/opt/zendesk/versions/#{version_sha}/app/app/models/user.rb:124:in `save'",
            "ruby/2.2.0/gems/zendesk_billing_core-0.1.392/lib/zendesk_billing_core/zuora/jobs/account_sync_job.rb:7:in 'execute'",
          ]
        end

        let(:tags) do
          ['roles_changed:true', 'source:lib/zendesk_billing_core/zuora/jobs/account_sync_job.rb', 'line:7', 'source_of_instrument:user_change']
        end

        it 'reports change source metrics to Datadog, with project irrelevant paths stripped' do
          statsd_client.expects(:increment).with('user_changed', tags: tags).once
          perform
        end
      end

      describe 'when there is matching trace with rails console' do
        let(:trace) do
          [
            "/opt/zendesk/versions/#{version_sha}/app/app/models/user.rb:124:in `save'",
            "/bundle/gems/railties-4.2.10/lib/rails/commands/console.rb:9:in `start'",
          ]
        end

        it 'does not raise exception to prevent it from happening' do
          perform
        end

        describe 'and prevent_unknown_update_from includes console' do
          let(:prevent_unknown_update_from) { [:console] }

          describe 'in production' do
            before do
              Rails.env.stubs(:production?).returns(true)
            end

            it 'does not raise exception to prevent it from happening' do
              perform
            end
          end

          describe 'in another environement' do
            before do
              Rails.env.stubs(:production?).returns(false)
            end

            it 'raises exception to prevent it from happening' do
              exception = assert_raise RuntimeError do
                perform
              end
              assert_equal 'Please do not update user directly in Rails console. Talk to team Bilby at #ask-bilby for more details.', exception.message
            end
          end
        end
      end

      describe 'when there is a file with word `job` under a gems lib folder' do
        let(:trace) do
          [
            "/opt/zendesk/versions/#{version_sha}/app/app/models/user.rb:124:in `save!'",
            "/bundle/gems/somegem-4.2.10/lib/somegem/perform/do.rb:9:in `perform_job'",
          ]
        end

        let(:tags) do
          ['roles_changed:true', 'source:unknown', 'line:', 'source_of_instrument:user_change']
        end

        it 'reports change source metrics to Datadog, with project irrelevant paths stripped' do
          statsd_client.expects(:increment).with('user_changed', tags: tags).once
          perform
        end
      end

      describe 'when there is matching trace with unexpected file extension' do
        let(:trace) do
          [
            "/opt/zendesk/versions/#{version_sha}/app/app/models/user.rrb:124:in `save'",
            "script/rails:20:in `<main>'",
          ]
        end

        let(:tags) do
          ['roles_changed:true', 'source:unknown', 'line:', 'source_of_instrument:user_change']
        end

        it 'logs stack trace' do
          Rails.logger.expects(:info).with("Unknown change detected for model user, trace: #{trace}")
          perform
        end

        it 'reports unknown source to Datadog' do
          statsd_client.expects(:increment).with('user_changed', tags: tags).once
          perform
        end
      end

      describe 'when there is no matching trace found' do
        let(:trace) do
          [
            "/opt/zendesk/versions/#{version_sha}/app/app/models/user.rb:124:in `save'",
            "script/rails:20:in `<main>'",
          ]
        end

        let(:tags) do
          ['roles_changed:true', 'source:unknown', 'line:', 'source_of_instrument:user_change']
        end

        it 'logs stack trace' do
          Rails.logger.expects(:info).with("Unknown change detected for model user, trace: #{trace}")
          perform
        end

        it 'reports unknown source to Datadog' do
          statsd_client.expects(:increment).with('user_changed', tags: tags).once
          perform
        end

        describe 'and prevent_unknown_update_from console is on' do
          let(:prevent_unknown_update_from) { [:console] }

          it 'does not raise exception to prevent it from happening' do
            perform
          end

          describe 'and prevent_unknown_update_from code is on' do
            let(:prevent_unknown_update_from) { [:console, :code] }
            let(:tags) do
              ['roles_changed:true', 'source:unknown', 'line:', 'source_of_instrument:user_change']
            end

            it 'logs stack trace' do
              Rails.logger.expects(:info).with("Unknown change detected for model user, trace: #{trace}")
              assert_raise RuntimeError do
                perform
              end
            end

            it 'reports unknown source to Datadog' do
              statsd_client.expects(:increment).with('user_changed', tags: tags).once
              assert_raise RuntimeError do
                perform
              end
            end

            it 'raises exception to prevent it from happening' do
              exception = assert_raise RuntimeError do
                perform
              end
              assert_equal 'Please do not update user directly in your code. Talk to team Bilby at #ask-bilby for more details.', exception.message
            end
          end
        end
      end
    end

    describe 'when all instrumented columns have been changed' do
      let(:previous_changes) do
        { "roles" => [4, 2], "permission_set_id" => [123, 54], "restriction_id" => [1, 0] }
      end

      describe 'and the first matching trace is a controller' do
        let(:trace) do
          [
            "/opt/zendesk/versions/#{version_sha}/app/app/models/user.rb:124:in `save!'",
            "/opt/zendesk/versions/#{version_sha}/app/app/controllers/api/v2/users_controller.rb:731:in `update'",
          ]
        end

        let(:tags) do
          ['roles_changed:true', 'permission_set_id_changed:true', 'source:app/controllers/api/v2/users_controller.rb', 'line:731', 'source_of_instrument:user_change']
        end

        it 'reports change source metrics to Datadog, with name of changed columns combined' do
          statsd_client.expects(:increment).with('user_changed', tags: tags).once
          perform
        end
      end
    end
  end
end
