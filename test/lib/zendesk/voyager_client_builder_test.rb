require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::VoyagerClientBuilder do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  let(:client) { Zendesk::VoyagerClientBuilder.build(account) }
  let(:host) { client.instance_variable_get(:@host) }

  describe ".build" do
    it "builds a regular client" do
      host.must_equal "localhost"
    end

    describe "with voyager_use_service_proxy" do
      let(:dc_url) { "http://localhost:8500/v1/kv/global/pod/1/dc?raw" }

      before { Arturo.enable_feature!(:voyager_use_service_proxy) }

      it "uses the service proxy" do
        stub_request(:get, dc_url).to_return(body: "my-dc")
        host.must_equal "voyager.pod-1.svc.my-dc.zdsystest.com"
      end

      it "fails when datacenter fetch fails" do
        stub_request(:get, dc_url).to_raise(Timeout::Error)
        assert_raises(Faraday::TimeoutError) { host }
      end
    end
  end

  describe ".location" do
    it "uses the host and port from the options" do
      Zendesk::VoyagerClientBuilder.stubs(:options).returns(host: 'test_host', port: 'test_port')
      assert_equal 'test_host:test_port', Zendesk::VoyagerClientBuilder.location(account)
    end
  end
end
