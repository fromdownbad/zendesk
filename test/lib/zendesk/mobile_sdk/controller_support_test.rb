require_relative "../../../support/test_helper"
require "zendesk/testing/factories/global_uid"
require "zendesk/o_auth/testing/client_factory"
require 'zendesk/o_auth/testing/token_factory'

SingleCov.covered!

class MobileSdkControllerSupportTestController < ActionController::Base
  include Zendesk::MobileSdk::ControllerSupport
  before_action :allow_only_mobile_sdk_clients_access, only: :access
  before_action :allow_only_mobile_sdk_tokens_access, only: :index

  def access
    head :ok
  end

  def index
    head :ok
  end

  def anonymous
    if anonymous_authenticated?
      render text: "anonymous"
    else
      render text: "jwt"
    end
  end

  def current_sdk_app
    app = current_mobile_sdk_app

    if app
      render json: app.to_json, status: 200
    end
  end
end

class MobileSdkControllerSupportTest < ActionController::TestCase
  fixtures :accounts, :account_settings, :brands, :oauth_clients, :mobile_sdk_auths, :mobile_sdk_apps
  tests MobileSdkControllerSupportTestController
  use_test_routes

  describe "MobileSdkControllerSupport" do
    let(:account) { accounts(:minimum_sdk) }
    let(:user) { account.owner }
    let(:oauth_client) { oauth_clients(:minimum_sdk) }
    let(:mobile_sdk_auth) { mobile_sdk_auths(:minimum_sdk) }
    let(:sdk_app) { mobile_sdk_apps(:minimum_sdk) }

    describe "#anonymous_authenticated?" do
      before do
        @controller.stubs(:current_account).returns(account)
        @token = FactoryBot.create(:token, user: user, client: oauth_client)
        @request.env[Rack::OAuth2::Server::Resource::ACCESS_TOKEN] = @token
      end

      describe "token for anonymous is provided" do
        before do
          @token.scopes = "sdk"
          @token.save!
          get :anonymous, params: { client_id: mobile_sdk_auth.client.identifier }
        end

        it "should be anonymous" do
          assert_response 200
          assert_equal "anonymous", @response.body
        end
      end

      describe "token for JWT is provided" do
        before do
          @token.scopes = "read write"
          @token.save!
          get :anonymous, params: { client_id: mobile_sdk_auth.client.identifier }
        end

        it "should be anonymous" do
          assert_response 200
          assert_equal "jwt", @response.body
        end
      end
    end

    describe "#current_mobile_sdk_app" do
      before do
        token = FactoryBot.create(:token, user: user, client: oauth_client)

        @controller.stubs(:current_account).returns(account)
        @controller.stubs(:oauth_access_token).returns(token)

        get :current_sdk_app
      end

      it "should be successful" do
        assert_response 200
      end

      it "should return the current mobile SDK app" do
        assert_equal sdk_app.to_json, @response.body
      end
    end

    describe "#allow_only_mobile_sdk_clients_access" do
      describe "correct client is provided" do
        before do
          @controller.stubs(:current_account).returns(account)
          get :access, params: { client_id: mobile_sdk_auth.client.identifier }
        end

        it "dos nothing" do
          assert_response 200
        end
      end

      describe "a non-sdk client is provided" do
        before do
          @controller.stubs(:current_account).returns(account)
          get :access, params: { client_id: nil }
        end

        it "responds with 450 Blocked" do
          assert_response 450
        end
      end

      describe "mobile_sdk_old_support overrides the block" do
        before do
          @controller.stubs(:current_account).returns(account)
          account.stubs(:has_mobile_sdk_old_support?).returns(true)
          get :access, params: { client_id: nil }
        end

        it "responds with 450 Blocked" do
          assert_response 200
        end
      end
    end

    describe "#allow_only_mobile_sdk_tokens_access" do
      describe "correct token is provided" do
        before do
          @controller.stubs(:current_account).returns(account)
          token = FactoryBot.create(:token, user: user, client: oauth_client)
          @controller.stubs(:oauth_access_token).returns(token)
          get :index, params: { client_id: mobile_sdk_auth.client.identifier }
        end

        it "dos nothing" do
          assert_response 200
        end
      end

      describe "a non-sdk token is provided" do
        before do
          @controller.stubs(:current_account).returns(account)
          token = FactoryBot.create(:token, user: user, client: FactoryBot.create(:client, user: user))
          @controller.stubs(:oauth_access_token).returns(token)
          get :index, params: { client_id: mobile_sdk_auth.client.identifier }
        end

        it "responds with 450 Blocked" do
          assert_response 450
        end
      end
    end
  end
end
