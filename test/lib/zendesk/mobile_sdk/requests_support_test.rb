require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::MobileSdk::RequestsSupport do
  # fixtures :accounts, :users, :tickets
  fixtures :all

  class MobileSdkRequestsSupportTestController < ActionController::Base
    include Zendesk::MobileSdk::RequestsSupport
    attr_reader :current_account, :current_user

    def initialize(account, user)
      @current_account = account
      @current_user = user
    end

    def ticket(token)
      ticket_scope(token)
    end

    def tickets(tokens)
      tickets_scope(tokens)
    end
  end

  describe "MobileSdkRequestsSupport" do
    let(:account) { accounts(:minimum) }
    let(:user) { users(:minimum_end_user) }
    let(:tokens) { user.tickets.map { |ticket| ticket.request_token.value } }
    let(:controller) { MobileSdkRequestsSupportTestController.new(account, user) }

    before do
      account.tickets.each { |tickets| RequestToken.create!(ticket: tickets) } # ensure that every ticket has a token
    end

    it "finds tickets by request_tokens" do
      assert_same_elements user.tickets, controller.tickets(tokens)
    end

    it "finds a ticket by its request_tokens" do
      assert_equal user.tickets.first, controller.ticket(tokens.first)
    end

    describe 'only find current user requests' do
      let(:other_user) { users(:minimum_end_user2) }
      let(:other_user_ticket) do
        account.tickets.create! do |t|
          t.requester = other_user
          t.description = 'HALP'
          t.subject = 'Need halp'
          t.will_be_saved_by(other_user)
        end
      end
      let(:other_user_token) { RequestToken.create!(ticket: other_user_ticket).value }

      it 'does not find tickets from other users' do
        assert_empty controller.tickets([other_user_token])
      end

      it 'does not find a ticket from other users' do
        assert_nil controller.ticket(other_user_token)
      end
    end
  end
end
