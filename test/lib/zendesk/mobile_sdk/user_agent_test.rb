require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::MobileSdk::UserAgent do
  describe "Initialization - Valid User Agents" do
    ["Android", "iOS"].each do |platform|
      it "supports mobile SDK for #{platform} user agent version 1 format" do
        user_agent = Zendesk::MobileSdk::UserAgent.new("Zendesk SDK for #{platform}")

        assert_equal user_agent.name, "zendesk_sdk_for_#{platform.downcase}"
        assert_nil user_agent.sdk_version
        assert_nil user_agent.cust_sdk_version
        assert_equal user_agent.os, platform.downcase
        assert_nil user_agent.os_version
        assert_nil user_agent.env
        assert_nil user_agent.unity_version
        assert_nil user_agent.variant
      end

      it "supports mobile SDK for #{platform} user agent version 2 format" do
        user_agent = Zendesk::MobileSdk::UserAgent.new("Zendesk SDK for #{platform}/1.0.0.1")

        assert_equal user_agent.name, "zendesk_sdk_for_#{platform.downcase}"
        assert_equal user_agent.sdk_version, "1.0.0.1"
        assert_nil user_agent.cust_sdk_version
        assert_equal user_agent.os, platform.downcase
        assert_nil user_agent.os_version
        assert_nil user_agent.env
        assert_nil user_agent.unity_version
        assert_nil user_agent.variant
      end

      it "supports mobile SDK for #{platform} user agent version 3 format" do
        user_agent = Zendesk::MobileSdk::UserAgent.new("Zendesk-SDK/1.0.0.2 #{platform}/8.1")

        assert_equal user_agent.name, "zendesk_sdk_for_#{platform.downcase}"
        assert_equal user_agent.sdk_version, "1.0.0.2"
        assert_nil user_agent.cust_sdk_version
        assert_equal user_agent.os, platform.downcase
        assert_equal user_agent.os_version, "8.1"
        assert_nil user_agent.env
        assert_nil user_agent.unity_version
        assert_nil user_agent.variant
      end

      it "supports mobile SDK for #{platform} user agent version 3 with custom string format" do
        user_agent = Zendesk::MobileSdk::UserAgent.new("Zendesk-SDK/1.0.0.2-SNAPSHOT #{platform}/8.1")

        assert_equal user_agent.name, "zendesk_sdk_for_#{platform.downcase}"
        assert_equal user_agent.sdk_version, "1.0.0.2"
        assert_equal user_agent.cust_sdk_version, "-SNAPSHOT"
        assert_equal user_agent.os, platform.downcase
        assert_equal user_agent.os_version, "8.1"
        assert_nil user_agent.env
        assert_nil user_agent.unity_version
        assert_nil user_agent.variant
      end

      it "supports mobile SDK for #{platform} user agent version 4 format" do
        user_agent = Zendesk::MobileSdk::UserAgent.new("Zendesk-SDK/1.0.0.2 #{platform}/8.1 Env/Production")

        assert_equal user_agent.name, "zendesk_sdk_for_#{platform.downcase}"
        assert_equal user_agent.sdk_version, "1.0.0.2"
        assert_nil user_agent.cust_sdk_version, nil
        assert_equal user_agent.os, platform.downcase
        assert_equal user_agent.os_version, "8.1"
        assert_equal user_agent.env, "production"
        assert_nil user_agent.unity_version
        assert_nil user_agent.variant
      end

      it "supports mobile SDK for #{platform} user agent version 4 with custom string format" do
        user_agent = Zendesk::MobileSdk::UserAgent.new("Zendesk-SDK/1.0.0.2-SNAPSHOT #{platform}/8.1 Env/Production")

        assert_equal user_agent.name, "zendesk_sdk_for_#{platform.downcase}"
        assert_equal user_agent.sdk_version, "1.0.0.2"
        assert_equal user_agent.cust_sdk_version, "-SNAPSHOT"
        assert_equal user_agent.os, platform.downcase
        assert_equal user_agent.os_version, "8.1"
        assert_equal user_agent.env, "production"
        assert_nil user_agent.unity_version
        assert_nil user_agent.variant
      end

      it "supports mobile SDK for #{platform} user agent version 5 format" do
        user_agent = Zendesk::MobileSdk::UserAgent.new("Zendesk-SDK/1.0.0.2 #{platform}/8.1 Env/Production Unity/1.4")

        assert_equal user_agent.name, "zendesk_sdk_for_#{platform.downcase}"
        assert_equal user_agent.sdk_version, "1.0.0.2"
        assert_nil user_agent.cust_sdk_version
        assert_equal user_agent.os, platform.downcase
        assert_equal user_agent.os_version, "8.1"
        assert_equal user_agent.env, "production"
        assert_equal user_agent.unity_version, "1.4"
        assert_nil user_agent.variant
      end

      it "supports mobile SDK for #{platform} user agent version 5 with custom string format" do
        user_agent = Zendesk::MobileSdk::UserAgent.new("Zendesk-SDK/1.0.0.2-SNAPSHOT #{platform}/8.1 Env/Production Unity/1.4")

        assert_equal user_agent.name, "zendesk_sdk_for_#{platform.downcase}"
        assert_equal user_agent.sdk_version, "1.0.0.2"
        assert_equal user_agent.cust_sdk_version, "-SNAPSHOT"
        assert_equal user_agent.os, platform.downcase
        assert_equal user_agent.os_version, "8.1"
        assert_equal user_agent.env, "production"
        assert_equal user_agent.unity_version, "1.4"
      end

      it "supports mobile SDK for #{platform} user agent version 6 format" do
        user_agent = Zendesk::MobileSdk::UserAgent.new("Zendesk-SDK/1.0.0.2 #{platform}/8.1 Env/Production Variant/Default")

        assert_equal user_agent.name, "zendesk_sdk_for_#{platform.downcase}"
        assert_equal user_agent.sdk_version, "1.0.0.2"
        assert_nil user_agent.cust_sdk_version
        assert_equal user_agent.os, platform.downcase
        assert_equal user_agent.os_version, "8.1"
        assert_equal user_agent.env, "production"
        assert_nil user_agent.unity_version
        assert_equal user_agent.variant, "default"
      end

      it "supports mobile SDK for #{platform} user agent version 7 format" do
        user_agent = Zendesk::MobileSdk::UserAgent.new("Zendesk-SDK/1.0.2 #{platform}/8.1 Variant/Support")

        assert_equal user_agent.name, "zendesk_sdk_for_#{platform.downcase}"
        assert_equal user_agent.sdk_version, "1.0.2"
        assert_nil user_agent.cust_sdk_version
        assert_equal user_agent.os, platform.downcase
        assert_equal user_agent.os_version, "8.1"
        assert_nil user_agent.unity_version
        assert_equal user_agent.variant, "support"
      end
    end
  end

  describe "Initialization - Invalid User Agent" do
    it "does not initialize anything" do
      user_agent = Zendesk::MobileSdk::UserAgent.new("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:40.0) Gecko/20100101 Firefox/40.0")

      assert_nil user_agent.name
      assert_nil user_agent.sdk_version
      assert_nil user_agent.os
      assert_nil user_agent.os_version
      assert_nil user_agent.env
    end
  end

  describe "#valid?" do
    [
      "Zendesk SDK for Android",
      "Zendesk SDK for iOS/1.0.0.1",
      "Zendesk-SDK/1.0.0.2 iOS/8.1",
      "Zendesk-SDK/1.0.0.2-SNAPSHOT iOS/8.1",
      "Zendesk-SDK/1.0.0.2-SNAPSHOT Android/18 Env/Production",
      "Zendesk-SDK/1.0.0.2-SNAPSHOT iOS/8.1 Env/Production Unity/1.4",
      "Zendesk-SDK/1.0.0.2-SNAPSHOT Android/8.1 Env/Production Variant/Default"
    ].each do |user_agent_string|
      it "returns true for #{user_agent_string}" do
        user_agent = Zendesk::MobileSdk::UserAgent.new(user_agent_string)

        assert user_agent.valid?
      end
    end

    [
      "Zendesk SDK for UNIX",                                               # v1 unsupported os
      "randomZendesk SDK for iOS ",                                         # v1 random string at the beggining
      "Zendesk SDK for UNIX/1.0.0.1",                                       # v2 unsupported os
      "Zendesk SDK for Android/",                                           # v2 no version
      "randomZendesk SDK for iOS/1.0.0.1",                                  # v2 random string at the beggining
      "Zendesk SDK for iOS/foo",                                            # v2 alphanumeric sdk version
      "Zendesk-SDK/1.0.0.1 UNIX/7",                                         # v3 unsupported os
      "Zendesk-SDK/ iOS/8.1",                                               # v3 no sdk version
      "Zendesk-SDK/1.0.0.1 iOS/",                                           # v3 no os version
      "Zendesk-SDK/bar Android/18",                                         # v3 alphanumeric sdk version
      "Zendesk-SDK/1.0.0.1-snapshot iOS/foo",                               # v3 alphanumeric os version (unsupported atm)
      "randomZendesk-SDK/1.0.0.1-snapshot iOS/8.1",                         # v3 random string at the beggining
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Staging",                            # v4 iOS unknown mode
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/",                                   # v4 iOS no mode
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Staging",                         # v4 Android development mode
      "Zendesk-SDK/1.0.0.1 Android/18 Env/",                                # v4 Android no mode
      "randomZendesk-SDK/1.0.0.1 iOS/8.1 Env/Development",                  # v4 random string at the beggining
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Development Unity",                  # v5 iOS development mode with unity w/o version
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Production Unity/foo",               # v5 iOS production mode with unity alphanumeric version
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Development Unity",               # v5 Android development mode with unity w/o version
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Production Unity/foo",            # v5 Android production mode with unity
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Development Unity/1.4 bar",          # v5 iOS development mode with unity and something else at the end
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Production Unity/1.4 bar",           # v5 iOS production mode with unity and something else at the end
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Development Unity/1.4 baz",       # v5 Android development mode with unity and something else at the end
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Production Unity/1.4 quux",       # v5 Android production mode with unity and something else at the end
      "randomZendesk-SDK/1.0.0.1 Android/18 Env/Production Unity/1.4",      # v5 random string at the beggining
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Development Variant",                # v6 iOS development mode with variant w/o variant str
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Production Variant/1",               # v6 iOS production mode with numeric variant str
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Development Variant",             # v6 Android development mode with variant w/o variant str
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Production Variant",              # v6 Android production mode with variant w/o variant str
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Development Variant/Default foo",    # v6 iOS development mode withvariant and something else at the end
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Production Variant/Default bar",     # v6 iOS production mode with variant and something else at the end
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Development Variant/Fabric baz",  # v6 Android development mode with unity and something else at the end
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Production Variant/Default quux", # v6 Android production mode with unity and something else at the endA
      "randomZendesk-SDK/1.0.0.1 Android/18 Env/Production Variant/Fabric"  # v6 random string at the beggining
    ].each do |user_agent_string|
      it "returns false for #{user_agent_string}" do
        user_agent = Zendesk::MobileSdk::UserAgent.new(user_agent_string)

        refute user_agent.valid?
      end
    end
  end

  describe "#invalid?" do
    [
      "Zendesk SDK for Android",
      "Zendesk SDK for iOS/1.0.0.1",
      "Zendesk-SDK/1.0.0.2 iOS/8.1",
      "Zendesk-SDK/1.0.0.2-SNAPSHOT iOS/8.1",
      "Zendesk-SDK/1.0.0.2-SNAPSHOT Android/18 Env/Production",
      "Zendesk-SDK/1.0.0.2-SNAPSHOT iOS/8.1 Env/Production Unity/1.4",
      "Zendesk-SDK/1.0.0.2-SNAPSHOT Android/8.1 Env/Production Variant/Default"
    ].each do |user_agent_string|
      it "returns false for #{user_agent_string}" do
        user_agent = Zendesk::MobileSdk::UserAgent.new(user_agent_string)

        refute user_agent.invalid?
      end
    end

    [
      "Zendesk SDK for UNIX",                                               # v1 unsupported os
      "randomZendesk SDK for iOS ",                                         # v1 random string at the beggining
      "Zendesk SDK for UNIX/1.0.0.1",                                       # v2 unsupported os
      "Zendesk SDK for Android/",                                           # v2 no version
      "randomZendesk SDK for iOS/1.0.0.1",                                  # v2 random string at the beggining
      "Zendesk SDK for iOS/foo",                                            # v2 alphanumeric sdk version
      "Zendesk-SDK/1.0.0.1 UNIX/7",                                         # v3 unsupported os
      "Zendesk-SDK/ iOS/8.1",                                               # v3 no sdk version
      "Zendesk-SDK/1.0.0.1 iOS/",                                           # v3 no os version
      "Zendesk-SDK/bar Android/18",                                         # v3 alphanumeric sdk version
      "Zendesk-SDK/1.0.0.1-snapshot iOS/foo",                               # v3 alphanumeric os version (unsupported atm)
      "randomZendesk-SDK/1.0.0.1-snapshot iOS/8.1",                         # v3 random string at the beggining
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Staging",                            # v4 iOS unknown mode
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/",                                   # v4 iOS no mode
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Staging",                         # v4 Android development mode
      "Zendesk-SDK/1.0.0.1 Android/18 Env/",                                # v4 Android no mode
      "randomZendesk-SDK/1.0.0.1 iOS/8.1 Env/Development",                  # v4 random string at the beggining
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Development Unity",                  # v5 iOS development mode with unity w/o version
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Production Unity/foo",               # v5 iOS production mode with unity alphanumeric version
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Development Unity",               # v5 Android development mode with unity w/o version
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Production Unity/foo",            # v5 Android production mode with unity
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Development Unity/1.4 bar",          # v5 iOS development mode with unity and something else at the end
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Production Unity/1.4 bar",           # v5 iOS production mode with unity and something else at the end
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Development Unity/1.4 baz",       # v5 Android development mode with unity and something else at the end
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Production Unity/1.4 quux",       # v5 Android production mode with unity and something else at the end
      "randomZendesk-SDK/1.0.0.1 Android/18 Env/Production Unity/1.4",      # v5 random string at the beggining
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Development Variant",                # v6 iOS development mode with variant w/o variant str
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Production Variant/1",               # v6 iOS production mode with numeric variant str
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Development Variant",             # v6 Android development mode with variant w/o variant str
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Production Variant",              # v6 Android production mode with variant w/o variant str
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Development Variant/Default foo",    # v6 iOS development mode withvariant and something else at the end
      "Zendesk-SDK/1.0.0.1 iOS/8.1 Env/Production Variant/Default bar",     # v6 iOS production mode with variant and something else at the end
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Development Variant/Fabric baz",  # v6 Android development mode with unity and something else at the end
      "Zendesk-SDK/1.0.0.1 Android/18 Env/Production Variant/Default quux", # v6 Android production mode with unity and something else at the endA
      "randomZendesk-SDK/1.0.0.1 Android/18 Env/Production Variant/Fabric"  # v6 random string at the beggining
    ].each do |user_agent_string|
      it "returns false for #{user_agent_string}" do
        user_agent = Zendesk::MobileSdk::UserAgent.new(user_agent_string)

        assert user_agent.invalid?
      end
    end
  end
end
