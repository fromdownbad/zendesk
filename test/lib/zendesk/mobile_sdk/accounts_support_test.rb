require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::MobileSdk::AccountsSupport do
  include Zendesk::MobileSdk::AccountsSupport

  let(:enterprise_account) { accounts(:extra_large) }
  let(:minimum_account) { accounts(:minimum_sdk) }

  describe "account_can_hide_closed_requests?" do
    [
      ZBC::Zendesk::PlanType::Team,
      ZBC::Zendesk::PlanType::Professional,
      ZBC::Zendesk::PlanType::Enterprise
    ].each do |plan|
      before do
        minimum_account.subscription.stubs(:plan_type).returns(plan.plan_type)
      end

      it "returns true when the account's plan is '#{plan.name}'" do
        assert(account_can_hide_closed_requests?(minimum_account))
      end
    end

    it "returns false when account's plan is 'essential'" do
      minimum_account.subscription.stubs(:plan_type).returns(ZBC::Zendesk::PlanType::Essential.plan_type)
      assert_equal false, account_can_hide_closed_requests?(minimum_account)
    end
  end

  describe "account_cannot_hide_closed_requests?" do
    [
      ZBC::Zendesk::PlanType::Team,
      ZBC::Zendesk::PlanType::Professional,
      ZBC::Zendesk::PlanType::Enterprise
    ].each do |plan|
      before do
        minimum_account.subscription.stubs(:plan_type).returns(plan.plan_type)
      end

      it "returns false when the account's plan is '#{plan.name}'" do
        assert_equal false, account_cannot_hide_closed_requests?(minimum_account)
      end
    end

    it "returns true when account's plan is 'essential'" do
      minimum_account.subscription.stubs(:plan_type).returns(ZBC::Zendesk::PlanType::Essential.plan_type)
      assert(account_cannot_hide_closed_requests?(minimum_account))
    end
  end

  describe "account_can_hide_referrer_logo?" do
    it "returns true when account is enterprise" do
      assert(account_can_hide_referrer_logo?(enterprise_account))
    end

    it "returns false when account is not enterprise" do
      assert_equal false, account_can_hide_referrer_logo?(minimum_account)
    end
  end

  describe "account_cannot_hide_referrer_logo?" do
    it "returns false when account is enterprise" do
      assert_equal false, account_cannot_hide_referrer_logo?(enterprise_account)
    end

    it "returns true when account is not enterprise" do
      assert(account_cannot_hide_referrer_logo?(minimum_account))
    end
  end

  describe "enterprise_account?" do
    it "returns true when account is enterprise" do
      assert(enterprise_account?(enterprise_account))
    end

    it "returns false when account is not enterprise" do
      assert_equal false, enterprise_account?(minimum_account)
    end
  end
end
