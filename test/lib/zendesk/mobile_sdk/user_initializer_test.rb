require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::MobileSdk::UserInitializer do
  include CustomFieldsTestHelper

  fixtures :all

  let(:current_account) { accounts(:minimum_sdk) }
  let(:current_user)    { current_account.users.new }
  let(:initializer) do
    Zendesk::MobileSdk::UserInitializer.new(current_account, current_user, params)
  end

  describe "#apply" do
    describe "Custom Fields" do
      before do
        # remove fixtures
        CustomField::Field.without_arsi.delete_all
        CustomField::Value.without_arsi.delete_all

        @account = current_account
        create_user_custom_field!(:text_cust_field, 'Sample test user field', 'Text')
        create_user_custom_field!(:decimal_cust_field, 'Sample decimal user field', 'Decimal')
        create_user_custom_field!(:integer_cust_field, 'Sample integer user field', 'Integer')
        create_user_custom_field!("system::text_cust_field", "txt.system_field", 'Text', is_system: 1)
      end

      let(:params) do
        {
          user: {
            user_fields: {
              text_cust_field: "Hello, world!",
              decimal_cust_field: 1.0,
              integer_cust_field: 10,
              "system::text_cust_field" => "Hello, system!"
            }
          }
        }
      end

      describe "as a non system user" do
        before { current_user.stubs(:is_system_user?).returns(false) }

        it "only applies non system custom field values to the user" do
          user = current_account.users.new
          initializer.apply(user)
          expected_user_fields_hash = {
            "decimal_cust_field" => 1.0,
            "integer_cust_field" => 10,
            "text_cust_field" => "Hello, world!",
            "system::text_cust_field" => nil
          }
          assert_equal(expected_user_fields_hash, user.custom_field_values.as_json(account: current_account))
        end
      end

      describe "as a system user" do
        before { current_user.stubs(:is_system_user?).returns(true) }

        it "apply changes to both system and non system custom field values" do
          user = current_account.users.new
          initializer.apply(user)
          expected_user_fields_hash = {
            "decimal_cust_field" => 1.0,
            "integer_cust_field" => 10,
            "text_cust_field" => "Hello, world!",
            "system::text_cust_field" => "Hello, system!"
          }
          assert_equal(expected_user_fields_hash, user.custom_field_values.as_json(account: current_account))
        end
      end

      describe "with different empty user_fields params" do
        [{}, [], "", nil].each do |empty_user_fields|
          let(:params) do
            {
              user: {
                user_fields: empty_user_fields
              }
            }
          end

          it "does not change the pre-existing custom field values" do
            user = current_account.users.new
            user.custom_field_values.update_from_hash("text_cust_field" => "Hello, world!")
            initializer.apply(user)

            expected_user_fields_hash = {
              "decimal_cust_field" => nil,
              "integer_cust_field" => nil,
              "text_cust_field" => "Hello, world!",
              "system::text_cust_field" => nil
            }
            assert_equal(expected_user_fields_hash, user.custom_field_values.as_json(account: current_account))
          end
        end
      end
    end
  end
end
