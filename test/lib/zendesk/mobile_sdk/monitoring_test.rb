require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'Zendesk::MobileSdk::Monitoring in Controller' do
  class ApiMobileDummyController < Api::Mobile::BaseController
    include Zendesk::MobileSdk::Monitoring

    skip_before_action :authenticate_user
    skip_before_action :enforce_ssl!

    allow_parameters :mobile_only, :skip

    def mobile_only
      render json: 'cool'
    end
  end

  tests ApiMobileDummyController
  use_test_routes

  fixtures :all

  before do
    @current_account = accounts(:minimum)
    @request.account = @current_account
    login(@current_account.owner)
  end

  describe "User Agent Instrumentation" do
    describe "Version 1 - Zendesk SDK for (Android|iOS)" do
      before { @request.headers['User-Agent'] = "Zendesk SDK for Android" }

      it "sends the correct data to statsd" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(
          "api_mobile_dummy.mobile_only",
          tags: [
            'action:mobile_only',
            'subdomain:minimum',
            'status:200',
            'mobile_app:zendesk_sdk_for_android',
            'mobile_app_version:none',
            'mobile_app_cust_version:none',
            'mobile_app_os:android',
            'mobile_app_os_version:none',
            'mobile_app_env:none',
            'mobile_app_unity_version:none',
            'mobile_app_variant:none'
          ]
        )
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.ApiMobileDummyController.mobile_only', anything).at_least(1)

        get :mobile_only, format: 'json'
        assert_response :ok
      end
    end

    describe "Version 2 - Zendesk SDK for (iOS|Android)/(n.n.n.n)" do
      before { @request.headers['User-Agent'] = "Zendesk SDK for iOS/1.0.0.1" }

      it "sends the correct data to statsd" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(
          "api_mobile_dummy.mobile_only",
          tags: [
            'action:mobile_only',
            'subdomain:minimum',
            'status:200',
            'mobile_app:zendesk_sdk_for_ios',
            'mobile_app_version:1.0.0.1',
            'mobile_app_cust_version:none',
            'mobile_app_os:ios',
            'mobile_app_os_version:none',
            'mobile_app_env:none',
            'mobile_app_unity_version:none',
            'mobile_app_variant:none'
          ]
        )
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.ApiMobileDummyController.mobile_only', anything).at_least(1)

        get :mobile_only, format: 'json'
        assert_response :ok
      end
    end

    describe "Version 3 - Zendesk-SDK/m.m.m.m (iOS|Android)/n.n" do
      before { @request.headers['User-Agent'] = "Zendesk-SDK/1.0.0.1 Android/18" }

      it "sends the correct data to statsd" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(
          "api_mobile_dummy.mobile_only",
          tags: [
            'action:mobile_only',
            'subdomain:minimum',
            'status:200',
            'mobile_app:zendesk_sdk_for_android',
            'mobile_app_version:1.0.0.1',
            'mobile_app_cust_version:none',
            'mobile_app_os:android',
            'mobile_app_os_version:18',
            'mobile_app_env:none',
            'mobile_app_unity_version:none',
            'mobile_app_variant:none'
          ]
        )
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.ApiMobileDummyController.mobile_only', anything).at_least(1)

        get :mobile_only, format: 'json'
        assert_response :ok
      end
    end

    describe "Version 3 with custom version segment - Zendesk-SDK/m.m.m.m-foo (iOS|Android)/n.n" do
      before { @request.headers['User-Agent'] = "Zendesk-SDK/1.0.0.1-SNAPSHOT Android/18" }

      it "sends the correct data to statsd" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(
          "api_mobile_dummy.mobile_only",
          tags: [
            'action:mobile_only',
            'subdomain:minimum',
            'status:200',
            'mobile_app:zendesk_sdk_for_android',
            'mobile_app_version:1.0.0.1',
            'mobile_app_cust_version:-SNAPSHOT',
            'mobile_app_os:android',
            'mobile_app_os_version:18',
            'mobile_app_env:none',
            'mobile_app_unity_version:none',
            'mobile_app_variant:none'
          ]
        )
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.ApiMobileDummyController.mobile_only', anything).at_least(1)

        get :mobile_only, format: 'json'
        assert_response :ok
      end
    end

    describe "Version 4 - Zendesk-SDK/m.m.m.m (iOS|Android)/n.n Env/(Production|Development)" do
      before { @request.headers['User-Agent'] = "Zendesk-SDK/1.0.0.1 Android/18 Env/Production" }

      it "sends the correct data to statsd" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(
          "api_mobile_dummy.mobile_only",
          tags: [
            'action:mobile_only',
            'subdomain:minimum',
            'status:200',
            'mobile_app:zendesk_sdk_for_android',
            'mobile_app_version:1.0.0.1',
            'mobile_app_cust_version:none',
            'mobile_app_os:android',
            'mobile_app_os_version:18',
            'mobile_app_env:production',
            'mobile_app_unity_version:none',
            'mobile_app_variant:none'
          ]
        )
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.ApiMobileDummyController.mobile_only', anything).at_least(1)

        get :mobile_only, format: 'json'
        assert_response :ok
      end
    end

    describe "Version 4 with custom version segment - Zendesk-SDK/m.m.m.m-foo (iOS|Android)/n.n Env/(Production|Development)" do
      before { @request.headers['User-Agent'] = "Zendesk-SDK/1.0.0.1-SNAPSHOT Android/18 Env/Production" }

      it "sends the correct data to statsd" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(
          "api_mobile_dummy.mobile_only",
          tags: [
            'action:mobile_only',
            'subdomain:minimum',
            'status:200',
            'mobile_app:zendesk_sdk_for_android',
            'mobile_app_version:1.0.0.1',
            'mobile_app_cust_version:-SNAPSHOT',
            'mobile_app_os:android',
            'mobile_app_os_version:18',
            'mobile_app_env:production',
            'mobile_app_unity_version:none',
            'mobile_app_variant:none'
          ]
        )
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.ApiMobileDummyController.mobile_only', anything).at_least(1)

        get :mobile_only, format: 'json'
        assert_response :ok
      end
    end

    describe "Version 5 Unity - Zendesk-SDK/m.m.m.m (iOS|Android)/n.n Env/(Production|Development) Unity/n.n" do
      before { @request.headers['User-Agent'] = "Zendesk-SDK/1.0.0.1-SNAPSHOT Android/18 Env/Production Unity/1.4" }

      it "sends the correct data to statsd" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(
          "api_mobile_dummy.mobile_only",
          tags: [
            'action:mobile_only',
            'subdomain:minimum',
            'status:200',
            'mobile_app:zendesk_sdk_for_android',
            'mobile_app_version:1.0.0.1',
            'mobile_app_cust_version:-SNAPSHOT',
            'mobile_app_os:android',
            'mobile_app_os_version:18',
            'mobile_app_env:production',
            'mobile_app_unity_version:1.4',
            'mobile_app_variant:none'
          ]
        )
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.ApiMobileDummyController.mobile_only', anything).at_least(1)

        get :mobile_only, format: 'json'
        assert_response :ok
      end
    end

    describe "Version 6 - Zendesk-SDK/m.m.m.m (iOS|Android)/n.n Env/(Production|Development) Variant/Default" do
      before { @request.headers['User-Agent'] = "Zendesk-SDK/1.0.0.1-SNAPSHOT Android/18 Env/Production Variant/Default" }

      it "sends the correct data to statsd" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(
          "api_mobile_dummy.mobile_only",
          tags: [
            'action:mobile_only',
            'subdomain:minimum',
            'status:200',
            'mobile_app:zendesk_sdk_for_android',
            'mobile_app_version:1.0.0.1',
            'mobile_app_cust_version:-SNAPSHOT',
            'mobile_app_os:android',
            'mobile_app_os_version:18',
            'mobile_app_env:production',
            'mobile_app_unity_version:none',
            'mobile_app_variant:default'
          ]
        )
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.ApiMobileDummyController.mobile_only', anything).at_least(1)

        get :mobile_only, format: 'json'
        assert_response :ok
      end
    end

    describe "Unknown User Agent Mobile SDK String" do
      before { @request.headers['User-Agent'] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.10; rv:40.0) Gecko/20100101 Firefox/40.0" }

      it "sends the correct data to statsd (but it does not fulfill the request)" do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with(
          "api_mobile_dummy.mobile_only",
          tags: [
            'action:mobile_only',
            'subdomain:minimum',
            'status:450',
            'mobile_app:none',
            'mobile_app_version:none',
            'mobile_app_cust_version:none',
            'mobile_app_os:none',
            'mobile_app_os_version:none',
            'mobile_app_env:none',
            'mobile_app_unity_version:none',
            'mobile_app_variant:none'
          ]
        )
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('sli.request.ApiMobileDummyController.mobile_only', anything).at_least(1)

        get :mobile_only, format: 'json'
        assert_response 450
      end
    end
  end
end
