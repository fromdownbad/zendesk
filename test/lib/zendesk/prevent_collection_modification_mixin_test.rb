require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'PreventCollectionModificationMixin' do
  describe Zendesk::PreventCollectionModificationMixin do
    class DummyModel
    end

    describe "when we include the mixin on the model" do
      it "adds apps requirement validation and before_destroy callback" do
        DummyModel.expects(:before_destroy).with(:prevent_destroy_collection_resource)
        DummyModel.expects(:validate).with(:prevent_modify_collection_resource)
        DummyModel.class_eval do
          include Zendesk::PreventCollectionModificationMixin
        end
      end
    end

    describe "when the model includes the mixin" do
      let(:dummy_model) do
        DummyModel.stubs(:before_destroy)
        DummyModel.stubs(:validate)
        DummyModel.class_eval do
          include Zendesk::PreventCollectionModificationMixin
        end
        DummyModel.new
      end

      describe "#prevent_modify_collection_resource" do
        let(:errors_stub) { stub }

        before do
          dummy_model.stubs(changed?: changed?, errors: errors_stub)
        end

        describe "when the model has been changed" do
          let(:changed?) { true }

          describe "when the model can be modified" do
            before do
              dummy_model.expects(:can_modify_collection?).returns(true)
            end

            it "returns without error" do
              errors_stub.expects(:add).never
              assert_nil dummy_model.send(:prevent_modify_collection_resource)
            end
          end

          describe "when the model cannot be modified" do
            before do
              dummy_model.expects(:can_modify_collection?).returns(false)
            end

            it "returns false and set an error" do
              errors_stub.expects(:add).with(:base, I18n.t("txt.admin.models.apps_requirements.validation.cannot_modify_requirement"))
              assert_equal false, dummy_model.send(:prevent_modify_collection_resource)
            end
          end
        end

        describe "when the model has not been changed" do
          let(:changed?) { false }

          it "returns without error" do
            assert_nil dummy_model.send(:prevent_modify_collection_resource)
          end
        end
      end

      describe "without an apps requirement" do
        before do
          dummy_model.stubs(is_apps_requirement?: false)
          dummy_model.stubs(current_user: stub(is_system_user?: false))
        end

        describe "#can_modify_collection?" do
          it "returns true" do
            assert(dummy_model.send(:can_modify_collection?))
          end
        end
      end

      describe "with an apps requirement" do
        before do
          dummy_model.stubs(is_apps_requirement?: true)
          dummy_model.stubs(current_user: stub(is_system_user?: is_system_user?))
        end

        describe "as a regular user" do
          let(:is_system_user?) { false }

          describe "#can_modify_collection?" do
            describe "when the model is only being reordered" do
              before do
                dummy_model.stubs(changed?: true, changed: ['position'])
              end

              it "returns true" do
                assert(dummy_model.send(:can_modify_collection?))
              end
            end

            describe "when model-allowed attributes are being changed" do
              before do
                dummy_model.stubs(
                  changed?: true,
                  changed: ['allowed_attribute'],
                  collection_allowed_changes: ['allowed_attribute']
                )
              end

              it "returns true" do
                assert(dummy_model.send(:can_modify_collection?))
              end
            end

            describe "when non-allowed attributes are being changed" do
              before do
                dummy_model.stubs(changed?: true, changed: ['disallowed_attribute'])
              end

              it "returns false" do
                assert_equal false, dummy_model.send(:can_modify_collection?)
              end
            end
          end
        end
      end
    end
  end
end
