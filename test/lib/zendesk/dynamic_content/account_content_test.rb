require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::DynamicContent::AccountContent do
  fixtures :tickets, :accounts, :users, :translation_locales, :groups, :addresses, :subscriptions, :user_identities

  describe ".cache" do
    let(:account) { accounts(:minimum) }
    let(:locale) { translation_locales(:brazilian_portuguese) }

    subject { Zendesk::DynamicContent::AccountContent.cache(account, locale) }

    before do
      locale = translation_locales(:brazilian_portuguese)

      texts = {
        "Welcome Wombat" => "welcome_wombat",
        "Slothy time!" => "sloths_are_cute",
        "What does the fox say?" => "smooth_as_eggs",
      }

      cms_texts = texts.map do |value, name|
        fallback_attributes = { is_fallback: true, nested: true, value: value, translation_locale_id: locale.id }
        Cms::Text.create!(name: name, fallback_attributes: fallback_attributes, account: account)
      end

      # variants for 2 of them
      fallback_locale = translation_locales(:english_by_zendesk)
      cms_texts[1].variants.create!(value: "de slothes ayr koot", translation_locale_id: fallback_locale.id)
      cms_texts[2].variants.create!(value: "Vat do foxes says?", translation_locale_id: fallback_locale.id)
    end

    it "creates a hash with the Cms::Text#identifier as the keys and proper Cms::Variant#value as the values" do
      assert_equal({
        "sloths_are_cute" => "Slothy time!",
        "smooth_as_eggs" => "What does the fox say?",
        "welcome_wombat" => "Welcome Wombat"
      }, subject)
    end

    describe "when current locale does not have one of the variants" do
      let(:locale) { translation_locales(:english_by_zendesk) }

      it "uses the fallback variant" do
        assert_equal({
          "sloths_are_cute" => "de slothes ayr koot",
          "smooth_as_eggs" => "Vat do foxes says?",
          "welcome_wombat" => "Welcome Wombat"
        }, subject)
      end
    end

    describe_with_arturo_disabled :cache_dynamic_content_account_content do
      it 'queries the DB each time' do
        assert_sql_queries(3) do
          3.times do
            Zendesk::DynamicContent::AccountContent.cache(account, locale)
          end
        end
      end
    end

    describe_with_arturo_enabled :cache_dynamic_content_account_content do
      it 'only queries the DB once' do
        # 1. account settings load
          # 2. content lookup
        assert_sql_queries(2) do
          5.times do
            Zendesk::DynamicContent::AccountContent.cache(account, locale)
          end
        end
      end

      describe 'updating a variant' do
        let(:variant) { account.cms_variants.last }

        it 'busts the cache' do
          # 1. variant load
          # 2. account settings load
          # 3. content lookup
          assert_sql_queries(3) do
            5.times do
              Zendesk::DynamicContent::AccountContent.cache(account, variant.translation_locale)
            end
          end

          variant.update_attributes!(value: "some new value")

          # 1. content lookup
          assert_sql_queries(1) do
            5.times do
              Zendesk::DynamicContent::AccountContent.cache(account, variant.translation_locale)
            end
          end
        end
      end
    end
  end
end
