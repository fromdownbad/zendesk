require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::DynamicContent::Renderer do
  fixtures :accounts, :translation_locales

  let(:account) { accounts(:minimum) }
  let(:english) { ENGLISH_BY_ZENDESK }

  describe '.render' do
    before do
      Zendesk::DynamicContent::Renderer.
        any_instance.
        stubs(:dc_cache).
        returns('user' => 'Sally', 'prompt' => 'your printer')
    end

    it 'expands standard dynamic content placeholders in a given string' do
      input_string = 'Hello, {{dc.user}}. How can I help with {{ dc.prompt }}?'
      expected_string = 'Hello, Sally. How can I help with your printer?'

      Zendesk::DynamicContent::Renderer.
        render(account, english, input_string).
        must_equal expected_string
    end

    it 'expands system dynamic content placeholders in a given string' do
      input_string = 'Please look at these views: {{zd.your_unsolved_tickets}}, {{zd.overdue_tasks}}'
      expected_string = 'Please look at these views: Your unsolved tickets, Overdue tasks'

      Zendesk::DynamicContent::Renderer.
        render(account, english, input_string).
        must_equal expected_string
    end

    it 'leaves regular curly braces alone if they are not valid dynamic content' do
      string = 'This is how a placeholder is formatted: {{ foo.bar }}'

      Zendesk::DynamicContent::Renderer.
        render(account, english, string).
        must_equal string
    end
  end

  describe '#render' do
    let(:renderer) { Zendesk::DynamicContent::Renderer.new(account, english) }

    before do
      renderer.stubs(:dc_cache).returns('user' => 'Sally', 'prompt' => 'your printer')
    end

    it 'expands standard dynamic content placeholders in a given string' do
      input_string = 'Hello, {{dc.user}}. How can I help with {{ dc.prompt }}?'
      expected_string = 'Hello, Sally. How can I help with your printer?'

      renderer.render(input_string).must_equal expected_string
    end

    it 'expands system dynamic content placeholders in a given string' do
      input_string = 'Please look at these views: {{zd.your_unsolved_tickets}}, {{zd.overdue_tasks}}'
      expected_string = 'Please look at these views: Your unsolved tickets, Overdue tasks'

      renderer.render(input_string).must_equal expected_string
    end

    it 'expands system dynamic content placeholders related to statuses' do
      ['new', 'open', 'pending', 'hold', 'solved'].each do |status|
        renderer.
          render("{{zd.status_#{status}}}").
          must_equal(I18n.t("type.status.#{status}"))
      end
    end

    it 'expands system dynamic content placeholders related to status descriptions' do
      ['new', 'open', 'pending', 'hold', 'solved'].each do |status|
        renderer.
          render("{{zd.status_#{status}_description}}").
          must_equal(I18n.t("type.custom_status.#{status}.description"))
      end
    end

    it 'expands both standard and system placeholders in the same string' do
      input_string = 'Hello, {{ dc.user }}. This is found in {{ zd.pending_tickets }}.'
      expected_string = 'Hello, Sally. This is found in Pending tickets.'

      renderer.render(input_string).must_equal expected_string
    end

    it 'leaves escaped placeholders in the string' do
      input_string = 'Dear {{dc.user }}. Please use this placeholder: \{{zd.pending_tickets}}'
      expected_string = 'Dear Sally. Please use this placeholder: \{{zd.pending_tickets}}'

      renderer.render(input_string).must_equal expected_string
    end

    it 'uses passed in dc_cache if given' do
      alt_renderer = Zendesk::DynamicContent::Renderer.new(account, english, dc_cache: { 'user' => 'Jim D. Jim' })
      input_string = 'Hello, {{dc.user}}. Use the view {{ zd.your_unsolved_tickets }}'
      expected_string = 'Hello, Jim D. Jim. Use the view Your unsolved tickets'

      alt_renderer.render(input_string).must_equal expected_string
    end
  end

  describe '#render_role_name' do
    let(:dc_renderer) { Zendesk::DynamicContent::Renderer.new(account, english, dc_cache: { 'the_cat' => 'Da Poop' }) }

    it 'expands dynamic content if the `has_dc_in_role_names` feature is on' do
      Arturo.enable_feature!(:dc_in_role_names)
      account.settings.default_roles_with_sys_dc = false
      account.save!
      input_string = 'Hello, {{dc.the_cat}}'
      expected_string = 'Hello, Da Poop'

      dc_renderer.render_role_name(input_string).must_equal expected_string
    end

    it 'expands dynamic content if the `default_roles_with_sys_dc` setting is true' do
      Arturo.disable_feature!(:dc_in_role_names)
      account.settings.default_roles_with_sys_dc = true
      account.save!
      input_string = 'Hello, {{dc.the_cat}}'
      expected_string = 'Hello, Da Poop'

      dc_renderer.render_role_name(input_string).must_equal expected_string
    end

    it 'returns the text if `has_dc_in_role_names` and `default_roles_with_sys_dc` are both false' do
      Arturo.disable_feature!(:dc_in_role_names)
      account.settings.default_roles_with_sys_dc = false
      account.save!
      input_string = 'Hello, {{dc.the_cat}}'

      dc_renderer.render_role_name(input_string).must_equal input_string
    end
  end
end
