require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::DynamicContent::Initializer do
  let(:account) { accounts(:minimum) }
  let(:fbattrs) { {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: 1 } }
  let(:text) { Cms::Text.create!(name: "I haz a reference", fallback_attributes: fbattrs, account: account) }
  let(:params) { {item_id: text.id, variant: { content: "Content A", locale_id: 2, active: true, is_fallback: false} } }

  before do
    @initializer = Zendesk::DynamicContent::Initializer.new(account, params)
  end

  describe "#new_variant" do
    it "returns a new variant" do
      new_variant = @initializer.new_variant
      assert_equal "Content A", new_variant.value
    end
  end

  describe "#variant" do
    it "finds a variant" do
      variant = @initializer.variant(text.variants.first.id)
      assert_equal "The fallback value", variant.value
    end
  end

  describe "#variants" do
    it "returns the variants on an item" do
      variants = @initializer.variants
      assert_equal text.variants, variants
    end
  end

  describe "#item" do
    it "returns a dynamic content item" do
      assert_equal text, @initializer.item
    end
  end

  describe "#scope" do
    it "returns the variants on an item" do
      assert_equal text.variants, @initializer.scope
    end
  end

  describe "#normalized_parameters" do
    it "maps params according to ATTRIBUTE_MAPPING" do
      params = { locale_id: 123, content: "Ohai!", default: true }
      expected = { translation_locale_id: 123, value: "Ohai!", is_fallback: true }
      assert_equal expected, @initializer.normalized_parameters(params)
    end
  end
end
