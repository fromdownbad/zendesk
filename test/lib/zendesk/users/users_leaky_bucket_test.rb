require_relative "../../../support/test_helper"
SingleCov.covered!

describe 'UsersLeakyBucket' do
  describe Zendesk::Users::UsersLeakyBucket do
    describe "when account rate limit is set" do
      let(:account) { accounts(:minimum) }
      before do
        Account.any_instance.stubs(:default_api_rate_limit).returns(700)
        Zendesk::Users::UsersLeakyBucket.configure_rate_limit(account.default_api_rate_limit)
      end

      it "calls Prop.configure with calculated values" do
        Prop.expects(:configure).with(:user_write_endpoints, strategy: :leaky_bucket, burst_rate: 351, threshold: 70, interval: 6.seconds)
        Zendesk::Users::UsersLeakyBucket.configure_rate_limit(account.default_api_rate_limit)
      end
    end
  end
end
