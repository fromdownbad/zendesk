require_relative "../../../../support/test_helper"
require 'zendesk_protobuf_clients/zendesk/protobuf/support/users/v2/user_events_pb'

SingleCov.covered!

describe "Zendesk::Users::Identities::UserIdentityEncoder" do
  let(:dummy_class) { Class.new { extend Zendesk::Users::Identities::UserIdentityEncoder } }

  describe "encode_identity_type" do
    it "encode valid entity type" do
      dummy_class.encode_identity_type("UserAnyChannelIdentity").must_equal ::Zendesk::Protobuf::Support::Users::V2::IdentityType::ANY_CHANNEL
      dummy_class.encode_identity_type("UserEmailIdentity").must_equal ::Zendesk::Protobuf::Support::Users::V2::IdentityType::EMAIL
      dummy_class.encode_identity_type("UserFacebookIdentity").must_equal ::Zendesk::Protobuf::Support::Users::V2::IdentityType::FACEBOOK
      dummy_class.encode_identity_type("UserForeignIdentity").must_equal ::Zendesk::Protobuf::Support::Users::V2::IdentityType::FOREIGN
      dummy_class.encode_identity_type("UserPhoneNumberIdentity").must_equal ::Zendesk::Protobuf::Support::Users::V2::IdentityType::PHONE_NUMBER
      dummy_class.encode_identity_type("UserTwitterIdentity").must_equal ::Zendesk::Protobuf::Support::Users::V2::IdentityType::TWITTER
      dummy_class.encode_identity_type("UserVoiceForwardingIdentity").must_equal ::Zendesk::Protobuf::Support::Users::V2::IdentityType::AGENT_FORWARDING
      dummy_class.encode_identity_type("UserSDKIdentity").must_equal ::Zendesk::Protobuf::Support::Users::V2::IdentityType::SDK
    end

    it "return unknown type when type is invalid" do
      dummy_class.encode_identity_type('WRONG TYPE').must_equal ::Zendesk::Protobuf::Support::Users::V2::IdentityType::UNKNOWN_IDENTITY_TYPE
    end
  end
end
