require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::Users::Identities::Initializer' do
  fixtures :accounts, :users

  describe "Initializer" do
    let(:account)  { accounts(:minimum) }
    let(:agent)    { users(:minimum_agent) }
    let(:end_user) { users(:minimum_end_user) }
    subject { Zendesk::Users::Identities::Initializer.new(account, agent, end_user, params) }

    describe "initialization" do
      describe "within identity" do
        let(:params) { { identity: { test: :hello }} }

        it "extracts identity" do
          assert_equal({ test: :hello }, subject.params)
        end
      end

      describe "in root" do
        let(:params) { { test: :hello } }

        it "keeps identity" do
          assert_equal({ test: :hello }, subject.params)
        end
      end
    end

    describe "#send_verification_email?" do
      describe "is_verified is true" do
        let(:params) { {verified: true} }

        it "returns false" do
          refute subject.send_verification_email?
        end
      end

      describe "is_verified is false" do
        let(:params) { {verified: false} }

        it "is true when user is_verified is false and account setting allows sending" do
          subject.user.stubs(is_verified?: false)
          subject.account.stubs(is_welcome_email_when_agent_register_enabled?: true)
          refute_equal subject.user, subject.current_user
          assert subject.current_user.is_agent?

          assert subject.send_verification_email?
        end

        it "is false when user is_verified is false and account setting does not allow sending" do
          subject.user.stubs(is_verified?: false)
          subject.account.stubs(is_welcome_email_when_agent_register_enabled?: false)
          refute_equal subject.user, subject.current_user
          assert subject.current_user.is_agent?

          refute subject.send_verification_email?
        end

        it "is true when current_user and user are equal" do
          assert subject.user.is_verified?
          subject = Zendesk::Users::Identities::Initializer.new(account, agent, agent, params)
          assert subject.current_user.is_agent?

          assert subject.send_verification_email?
        end

        describe "user is new" do
          subject { Zendesk::Users::Identities::Initializer.new(account, account.anonymous_user, account.users.new, params) }

          describe "account requires signup" do
            before do
              account.stubs(:is_signup_required?).returns(true)
              assert account.is_signup_required?
              refute subject.user.is_verified?
            end

            it "returns true" do
              assert subject.send_verification_email?
            end
          end

          describe "account does not require signup" do
            before do
              refute subject.user.is_verified?
            end

            it "returns false" do
              refute subject.send_verification_email?
            end
          end
        end

        describe "and account.is_welcome_email_when_agent_register_enabled? is false" do
          before { Account.any_instance.stubs(is_welcome_email_when_agent_register_enabled?: false) }

          it "is false if current_user is an agent" do
            assert subject.user.is_verified?
            refute_equal subject.user, subject.current_user
            assert subject.current_user.is_agent?

            refute subject.send_verification_email?
          end

          it "is true if current_user is not agent" do
            subject = Zendesk::Users::Identities::Initializer.new(account, end_user, agent, params)
            assert subject.user.is_verified?
            refute_equal subject.user, subject.current_user
            refute subject.current_user.is_agent?

            assert subject.send_verification_email?
          end
        end

        describe "and account.is_welcome_email_when_agent_register_enabled? is true" do
          before { Account.any_instance.stubs(is_welcome_email_when_agent_register_enabled?: true) }

          it "is true" do
            assert subject.user.is_verified?
            refute_equal subject.user, subject.current_user
            assert subject.current_user.is_agent?

            assert subject.send_verification_email?
          end
        end
      end
    end

    describe "email identity" do
      before do
        Account.any_instance.stubs(:is_welcome_email_when_agent_register_enabled?).returns(false)
      end

      let(:params) { { type: :email, value: "test@test.com", verified: true } }

      it "creates email identity" do
        assert_kind_of UserEmailIdentity, subject.identity
      end

      it "creates new email identity" do
        assert_equal "test@test.com", subject.identity.value
      end

      it "is verified" do
        assert subject.identity.is_verified?
      end

      it "does not send verification email" do
        subject.identity
        refute subject.user.send_verify_email
      end

      describe "unverified" do
        before { params[:verified] = false }

        it "does not send verification email if agent is adding secondary email to another user" do
          subject.identity
          refute subject.user.send_verify_email
        end

        it "sends verification email if agent is adding primary email to another user and account setting allows sending" do
          subject.user.stubs(:is_verified).returns(false)
          subject.account.stubs(:is_welcome_email_when_agent_register_enabled?).returns(true)
          subject.identity

          assert subject.user.send_verify_email
        end

        it "sends verification email if agent is adding secondary email for himself" do
          subject = Zendesk::Users::Identities::Initializer.new(account, agent, agent, params)

          subject.identity

          assert subject.user.send_verify_email
        end

        it "sends verification email if agent is adding primary email for himself" do
          subject = Zendesk::Users::Identities::Initializer.new(account, agent, agent, params)
          subject.user.stubs(:email).returns(nil)

          subject.identity

          assert subject.user.send_verify_email
        end
      end

      describe "primary" do
        it "sets the mark_primary variable of the identity" do
          params[:primary] = true

          UserEmailIdentity.any_instance.expects(:set_primary_when_verifying!).once

          subject.identity
        end

        it "does not set the mark_primary variable of the identity if primary param missing" do
          UserEmailIdentity.any_instance.expects(:set_primary_when_verifying!).never

          subject.identity
        end
      end
    end

    describe "phone identity" do
      let(:params) { { type: :phone_number, value: "1234567890" } }

      it "creates phone number identity" do
        assert_kind_of UserPhoneNumberIdentity, subject.identity
      end

      it "creates new phone number" do
        assert_equal "1234567890", subject.identity.value
      end
    end

    describe "twitter identity" do
      let(:params) { { type: :twitter, value: "tweetmaster" } }

      before { assert_kind_of UserTwitterIdentity, subject.identity }

      before_should "call identity.screen_name=" do
        UserTwitterIdentity.any_instance.expects(:screen_name=).with("tweetmaster")
      end
    end

    describe "facebook identity" do
      let(:params) { { type: :facebook, value: "123" } }

      it "creates email identity" do
        assert_kind_of UserFacebookIdentity, subject.identity
      end

      it "creates new email identity" do
        assert_equal "123", subject.identity.value
      end
    end

    describe "google identity" do
      let(:params) { { type: :google, value: "abc" } }

      it "creates email identity" do
        assert_kind_of UserEmailIdentity, subject.identity
      end

      it "creates new email identity" do
        assert_equal "abc", subject.identity.value
      end

      it "sets send_verify_email" do
        subject.identity

        assert subject.user.send_verify_email
      end

      it "builds a google profile" do
        assert subject.identity.google_profile
      end
    end

    describe "agent forwarding" do
      let(:params) { { type: :agent_forwarding, value: "abc" } }

      it "creates email identity" do
        assert_kind_of UserVoiceForwardingIdentity, subject.identity
      end

      it "creates new email identity" do
        assert_equal "abc", subject.identity.value
      end
    end

    describe "#value_for_type" do
      describe "v2" do
        describe "symbol" do
          let(:params) { { type: :google, value: :abc } }

          it "returns :abc" do
            assert_equal :abc, subject.send(:value_for_type, :google)
          end
        end

        describe "mixed case" do
          let(:params) { { type: "GOogle", value: :abc } }

          it "returns :abc" do
            assert_equal :abc, subject.send(:value_for_type, :google)
          end
        end

        describe "string" do
          let(:params) { { type: "google", value: :abc } }

          it "returns :abc" do
            assert_equal :abc, subject.send(:value_for_type, :google)
          end
        end

        describe "different type" do
          let(:params) { { type: :asmt, value: :abc } }

          it "returns :abc" do
            assert_nil subject.send(:value_for_type, :google)
          end
        end
      end

      describe "v1" do
        let(:params) { { google: :abc } }

        it "returns :abc" do
          assert_equal :abc, subject.send(:value_for_type, :google)
        end
      end
    end
  end
end
