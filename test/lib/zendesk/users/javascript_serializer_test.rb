require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 16

describe Zendesk::Users::JavascriptSerializer do
  fixtures :organizations

  before do
    Timecop.travel(Date.strptime('01/1/2013', '%m/%d/%Y'))

    @user = users(:minimum_agent)
    @serializer = Zendesk::Users::JavascriptSerializer.new(
      @user,
      is_assumed_identity: true,
      ssl_environment: true
    )

    @serializer.stubs(:locale_version).returns("locale/version")
    @admin = users(:minimum_admin)
    @serializer_admin = Zendesk::Users::JavascriptSerializer.new(
      @admin,
      is_assumed_identity: true,
      ssl_environment: true
    )

    @serializer_admin.stubs(:locale_version).returns("locale/version")
  end

  describe "#as_json" do
    before do
      @as_json = @serializer.as_json
    end

    it "returns a hash with the correct keys" do
      assert_equal [:account, :misc, :organization, :user, :voice], @as_json.keys.sort
    end

    before_should "be composed correctly" do
      @serializer.expects(:account_as_json).returns(account: {})
      @serializer.expects(:user_as_json).returns(user: {})
      @serializer.expects(:organization_as_json).returns(organization: {})
      @serializer.expects(:voice_as_json).returns(voice: {})
    end
  end

  describe "#user_as_json" do
    describe "for an agent" do
      before do
        @serializer.expects(:user).returns(@user).at_least_once
        @as_json = @serializer.send(:user_as_json)
      end

      it "returns a correctly structured hash" do
        assert_equal({
          accessibleForums: true,
          assumed: true,
          email: "minimum_agent@aghassipour.com",
          externalId: nil,
          first_name: "Agent",
          hasEmail: true,
          id: @user.id,
          localeIsDifferent: false,
          localeVersion: "locale/version",
          locale_id: 1,
          managePinnedOrder: false,
          name: "Agent Minimum",
          passwordExpiresAt: nil,
          restriction: 0,
          role: 4,
          isOwner: false,
          sharedOrganizations: [],
          tags: [],
          uses12HourClock: false,
          version: @user.cache_key,
          voiceClientName: "agent_#{@user.id}",
          voicePresenceName: "agent_#{@user.id}_presence",
          availabilityControlsEnabled: true
        }, @as_json)
      end
    end

    describe "for an admin" do
      before do
        @admin.expects(:accessible_forums_count).returns(0).at_least_once
        @serializer_admin.expects(:user).returns(@admin).at_least_once
        @as_json = @serializer_admin.send(:user_as_json)
      end

      it "returns a correctly structured hash" do
        assert_equal({
         id: @admin.id,
         externalId: nil,
         name: "Admin Man",
         first_name: "Admin",
         role: 2,
         accessibleForums: true,
         hasEmail: true,
         email: "minimum_admin@aghassipour.com",
         restriction: 0,
         uses12HourClock: false,
         passwordExpiresAt: nil,
         voiceClientName: "agent_#{@admin.id}",
         voicePresenceName: "agent_#{@admin.id}_presence",
         version: @admin.cache_key,
         localeVersion: "locale/version",
         sharedOrganizations: [],
         tags: [],
         managePinnedOrder: true,
         locale_id: 1,
         localeIsDifferent: false,
         assumed: true,
         isOwner: true,
         availabilityControlsEnabled: true
      }, @as_json)
      end
    end

    describe "for a null user" do
      before do
        @serializer.expects(:user).returns(nil)
        @as_json = @serializer.send(:user_as_json)
      end

      it "returns {} when the user is nil" do
        assert_equal({}, @as_json)
      end
    end
  end

  describe "#account_as_json" do
    describe "for a null user" do
      before do
        @serializer.expects(:user).returns(nil)
        @as_json = @serializer.send(:account_as_json)
      end

      it "returns {}" do
        assert_equal({}, @as_json)
      end
    end

    describe "for a null account" do
      before do
        @serializer.expects(:user).twice.returns(stub(account: nil))
        @as_json = @serializer.send(:account_as_json)
      end

      it "returns {}" do
        assert_equal({}, @as_json)
      end
    end

    describe "for an agent" do
      before do
        @time = Time.now
        @handle = MonitoredTwitterHandle.find_by_twitter_screen_name("lemur")
        @handle.mention_autoconversion_enabled_at = @time
        @handle.save!
        @serializer.expects(:user).returns(@user).at_least_once
        @as_json = @serializer.send(:account_as_json)
      end

      it "does not return collaboration as a feature" do
        refute @as_json[:features].include?("collaboration")
      end
    end
  end

  describe "#organization_as_json" do
    describe "for a null user" do
      before do
        @serializer.expects(:user).returns(nil)
        @as_json = @serializer.send(:organization_as_json)
      end

      it "returns {}" do
        assert_equal({}, @as_json)
      end
    end

    describe "for a null organization" do
      before do
        @serializer.expects(:user).twice.returns(stub(organization: nil))
        @as_json = @serializer.send(:organization_as_json)
      end

      it "returns {}" do
        assert_equal({}, @as_json)
      end
    end

    describe "for an user with an organization" do
      before do
        @organization = organizations(:minimum_organization1)
        @user.stubs(:organization).returns(@organization)

        @serializer.expects(:user).returns(@user).at_least_once
        @as_json = @serializer.send(:organization_as_json)
      end

      it "returns a correctly structured hash" do
        assert_equal({
          id: @organization.id,
          isShared: true,
          name: "minimum organization"
        }, @as_json)
      end
    end
  end

  describe "#shared_organizations_as_json" do
    before { Account.any_instance.stubs(:has_multiple_organizations_enabled?).returns(true) }
    describe "for a null user" do
      before do
        @as_json = @serializer.send(:shared_organizations_as_json, nil)
      end

      it "returns []" do
        assert_equal([], @as_json)
      end
    end

    describe "for a null organization" do
      before do
        @user.stubs(:organizations).returns(nil)
        @as_json = @serializer.send(:shared_organizations_as_json, @user)
      end

      it "returns []" do
        assert_equal([], @as_json)
      end
    end

    describe "for a user with multiple shared organizations" do
      before do
        @org1 = organizations(:minimum_organization1)
        @org2 = organizations(:minimum_organization2)
        @org2.stubs(is_shared: true)
        @user.stubs(:organizations).returns([@org1, @org2])

        @as_json = @serializer.send(:shared_organizations_as_json, @user)
      end

      it "returns a correctly structured hash" do
        assert_equal(
          [
            {
              id: @org1.id,
              isShared: true,
              name: "minimum organization"
            },
            {
              id: @org2.id,
              isShared: true,
              name: "organization 2"
            }
          ],
          @as_json
        )
      end
    end

    describe "for a user with multiple, unshared organizations but individual permission to view all" do
      before do
        @org1 = organizations(:minimum_organization1)
        @org2 = organizations(:minimum_organization2)
        @org1.stubs(is_shared: false)
        @org2.stubs(is_shared: false)
        @user.stubs(:organizations).returns([@org1, @org2])
        @user.stubs(:restriction_id).returns(2)

        @as_json = @serializer.send(:shared_organizations_as_json, @user)
      end

      it "stills return a correctly structured hash" do
        assert_equal(
          [
            {
              id: @org1.id,
              isShared: false,
              name: "minimum organization"
            },
            {
              id: @org2.id,
              isShared: false,
              name: "organization 2"
            }
          ],
          @as_json
        )
      end
    end
  end

  describe "#voice_as_json" do
    describe "for a null user" do
      before do
        @serializer.expects(:user).returns(nil)
        @as_json = @serializer.send(:voice_as_json)
      end

      it "returns {}" do
        assert_equal({}, @as_json)
      end
    end

    describe "for an admin" do
      before do
        @user.expects(:is_admin?).returns(true)
        @as_json = @serializer.send(:voice_as_json)
      end

      it "returns a correctly structured hash" do
        assert_equal "twilio_account_sid", @as_json[:account_sid]
        assert_equal "2010-04-01", @as_json[:api_version]
      end
    end
  end
end
