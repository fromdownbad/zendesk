require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'UpsertInitializer' do
  describe Zendesk::Users::UpsertInitializer do
    fixtures :accounts, :users, :user_identities

    let(:current_account) { accounts(:minimum) }
    let(:current_user) { current_account.owner }

    let(:active_span) { stub(:span) }
    let(:parent_span) { stub(name: 'rack.request') }

    before do
      Datadog.tracer.stubs(:active_span).returns(active_span)
      active_span.stubs(parent: parent_span)
      active_span.stubs(:set_tag)
      parent_span.stubs(:set_tag)
    end

    describe "initialization" do
      it "fallbacks to the anonymous user if no user is present" do
        assert_equal(
          current_account.anonymous_user,
          Zendesk::Users::Initializer.new(current_account, nil, {}).current_user
        )
      end
    end

    describe '#user' do
      describe 'new user' do
        it 'will build a new user' do
          params = { user: { name: '1234', external_id: '1234', identities: [{ type: 'email', value: '1234@test.com' }] } }
          new_user = Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).user
          assert new_user.new_record?
        end
      end

      describe 'existing user' do
        let(:user_one) { users(:minimum_end_user) }
        let(:identity_one) { user_identities(:minimum_end_user) }

        it 'retrieves that user' do
          params = {
            user: {
              id: user_one.id,
              name: 'new_minimum_end_user_1',
              external_id: 'new_external_id'
            }
          }
          updated_user = Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).user
          refute updated_user.new_record?
          assert_equal user_one.id, updated_user.id
        end
      end
    end

    describe 'Create' do
      it 'will create a new user' do
        params = { user: { name: '1234', external_id: '1234', identities: [{ type: 'email', value: '1234@test.com' }] } }
        new_user = Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).apply
        assert new_user.new_record?
      end

      describe 'with instrumentation' do
        let(:tag_args) { ['is_upsert_operation', true] }

        describe 'when the Datadog `active_span` is defined' do
          before { active_span.stubs(parent: parent_span) }

          describe 'and the parent_span is defined' do
            describe 'and the parent_span name is `rack.request`' do
              before do
                params = { user: { name: '12345', identities: [{ type: 'email', value: '12345@test.com' }] } }
                Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).apply
              end

              before_should 'set appropriate tags' do
                parent_span.stubs(:set_tag).with { |args| args != tag_args }
                parent_span.expects(:set_tag).with(*tag_args)
              end
            end

            describe 'and the parent_span name is not `rack.request`' do
              before do
                params = { user: { name: '123456', identities: [{ type: 'email', value: '123456@test.com' }] } }
                Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).apply
              end
              let(:parent_span) { stub(name: 'something') }

              before_should 'not try to set tags and active_span should set the correct tags' do
                active_span.stubs(:set_tag).with { |args| args != tag_args }
                parent_span.expects(:set_tag).with(*tag_args).never
                active_span.expects(:set_tag).with(*tag_args)
              end
            end
          end
        end
      end
    end

    describe '#handle_existing_user_name_update' do
      describe 'with overwrite_existing_name param' do
        it 'when existing name is same as overwrite_existing_name, should not remove name from params' do
          user_sanitize_one = users(:minimum_end_user)
          user_sanitize_one.name = 'sunshine_user'
          user_sanitize_one.save!

          params = { user: { id: user_sanitize_one.id, name: 'new_user_name', overwrite_existing_name: 'sunshine_user' } }
          initializer = Zendesk::Users::UpsertInitializer.new(current_account, current_user, params)
          initializer.apply

          expected_params = { user: { id: user_sanitize_one.id, name: 'new_user_name' } }
          assert_equal expected_params, initializer.params
        end

        it 'when existing name is not same as overwrite_existing_name, should remove name from params' do
          user_sanitize_two = users(:minimum_end_user)
          user_sanitize_two.name = 'not_sunshine_user'
          user_sanitize_two.save!
          params = { user: { id: user_sanitize_two.id, name: 'new_sunshine_name', overwrite_existing_name: 'sunshine_user' } }
          initializer = Zendesk::Users::UpsertInitializer.new(current_account, current_user, params)
          initializer.apply

          expected_params = { user: { id: user_sanitize_two.id } }
          assert_equal expected_params, initializer.params
        end
      end
    end

    describe 'Update:' do
      let(:user_one) { users(:minimum_end_user) }
      let(:identity_one) { user_identities(:minimum_end_user) }

      it 'user via id' do
        params = {
          user: {
            id: user_one.id,
            name: 'new_minimum_end_user_1',
            external_id: 'new_external_id'
          }
        }
        updated_user = Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).apply
        refute updated_user.new_record?
        assert_equal user_one.id, updated_user.id
      end

      it 'user via identity' do
        params = {
          user: {
            name: 'new_minimum_end_user_2',
            identities: [{ type: 'email', value: identity_one.value }]
          }
        }
        updated_user = Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).apply
        refute updated_user.new_record?
        assert_equal user_one.id, updated_user.id
      end

      describe '#update_user?' do
        let(:user_two) { users(:minimum_end_user) }
        let(:identity_two) { user_identities(:minimum_end_user) }

        it 'should be false, when we only provide the id' do
          params = { user: { id: user_two.id } }
          initializer = Zendesk::Users::UpsertInitializer.new(current_account, current_user, params)
          initializer.apply
          refute initializer.requires_save?
        end

        it 'should be false, when we provide the same name' do
          params = { user: { id: user_two.id, name: 'minimum_end_user' } }
          initializer = Zendesk::Users::UpsertInitializer.new(current_account, current_user, params)
          initializer.apply
          refute initializer.requires_save?
        end

        it 'should be false, when provide the existing identity' do
          params = { user: { id: user_two.id, identities: [{ type: 'email', value: identity_two.value }] } }
          initializer = Zendesk::Users::UpsertInitializer.new(current_account, current_user, params)
          initializer.apply
          refute initializer.requires_save?
        end

        it 'should be true, when updates the name' do
          params = { user: { id: user_two.id, name: 'minimum_end_user_update' } }
          check_assert_requires_save(params)
        end

        it 'should return true, when updates external_id' do
          params = { user: { id: user_two.id, external_id: 'new_external_id' } }
          check_assert_requires_save(params)
        end

        it 'should return true, when add new identity' do
          params = { user: { id: user_two.id, identities: [{ type: 'email', value: 'newemailaddress@test.com' }] } }
          check_assert_requires_save(params)
        end

        def check_assert_requires_save(params)
          initializer = Zendesk::Users::UpsertInitializer.new(current_account, current_user, params)
          initializer.apply
          assert initializer.requires_save?
        end
      end

      describe 'with instrumentation' do
        let(:tag_args_true) { ['is_upsert_operation', true] }
        let(:tag_args_false) { ['is_upsert_operation', false] }

        describe 'when the Datadog `active_span` is defined' do
          before { active_span.stubs(parent: parent_span) }

          describe 'and the parent_span is defined' do
            describe 'and the parent_span name is `rack.request`' do
              describe 'and update the user name' do
                before do
                  params = { user: { id: user_one.id, name: 'new_minimum_end_user_100' } }
                  Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).apply
                end

                before_should 'set appropriate tags' do
                  parent_span.stubs(:set_tag).with { |args| args != tag_args_true }
                  parent_span.expects(:set_tag).with(*tag_args_true)
                end
              end

              describe 'and not updating email' do
                before do
                  params = { user: { id: user_one.id, identities: [{ type: 'email', value: identity_one.value }] } }
                  Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).apply
                end

                before_should 'set appropriate tags' do
                  parent_span.stubs(:set_tag).with { |args| args != tag_args_false }
                  parent_span.expects(:set_tag).with(*tag_args_false)
                end
              end

              describe 'and only id is given in request' do
                before do
                  params = { user: { id: user_one.id } }
                  Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).apply
                end

                before_should 'set appropriate tags' do
                  parent_span.stubs(:set_tag).with { |args| args != tag_args_false }
                  parent_span.expects(:set_tag).with(*tag_args_false)
                end
              end
            end

            describe 'and the parent_span name is not `rack.request`' do
              let(:parent_span) { stub(name: 'something') }

              describe 'end update the name' do
                before do
                  params = { user: { id: user_one.id, name: 'new_minimum_end_user_1001' } }
                  Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).apply
                end

                before_should 'not try to set tags and active_span should set the tags' do
                  active_span.stubs(:set_tag).with { |args| args != tag_args_true }
                  parent_span.expects(:set_tag).with(*tag_args_true).never
                  active_span.expects(:set_tag).with(*tag_args_true)
                end
              end

              describe 'and not updating email' do
                before do
                  params = { user: { id: user_one.id, identities: [{ type: 'email', value: identity_one.value }] } }
                  Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).apply
                end

                before_should 'not try to set tags and active_span should set the tags' do
                  active_span.stubs(:set_tag).with { |args| args != tag_args_false }
                  parent_span.expects(:set_tag).with(*tag_args_false).never
                  active_span.expects(:set_tag).with(*tag_args_false)
                end
              end
            end
          end
        end
      end

      describe 'Should return error' do
        let(:user_two) { users(:minimum_agent) }
        let(:user_three) { users(:minimum_search_user) }
        let(:identity_two) { user_identities(:minimum_agent) }
        let(:identity_three) { user_identities(:minimum_search_user_phone) }

        before do
          user_two.update_attributes(external_id: 'external_id_two')
          user_three.update_attributes(external_id: 'external_id_three')
        end

        it 'when given identities belongs to multiple users' do
          params = {
            user: {
              name: 'new_minimum_end_user_2',
              identities: [{ type: 'email', value: identity_one.value }, { type: 'email', value: identity_two.value }]
            }
          }
          check_assert_raise(params)
        end

        it 'when different types of identities are given and belongs to multiple users' do
          params = {
            user: {
              name: 'new_minimum_end_user_3',
              identities: [{ type: 'email', value: identity_two.value }, { type: 'phone', value: identity_three.value }]
            }
          }
          check_assert_raise(params)
        end

        it 'when trying to update existing external_id of user' do
          params = { user: { id: user_two.id, external_id: 'new_external_id' } }
          check_assert_raise(params)
        end

        it 'when updating external_id which belongs to another user' do
          params = { user: { id: user_two.id, external_id: 'external_id_three' } }
          check_assert_raise(params)
        end

        it 'when empty external_id is given' do
          params = { user: { id: user_two.id, external_id: ''} }
          check_assert_raise(params)
        end

        it 'when identity type is not given' do
          params = { user: { name: 'new_minimum_end_user_3', identities: [{ value: identity_two.value }] } }
          check_assert_raise(params)
        end

        it 'when identity type is given as empty string' do
          params = { user: { name: 'new_minimum_end_user_3', identities: [{ type: '', value: identity_two.value }] } }
          check_assert_raise(params)
        end

        it 'when unknown identity type is given' do
          params = { user: { name: 'new_minimum_end_user_3', identities: [{ type: 'zendesk', value: identity_two.value }] } }
          check_assert_raise(params)
        end

        it 'when identity value is not given' do
          params = { user: { name: 'new_minimum_end_user_3', identities: [{ type: 'email' }] } }
          check_assert_raise(params)
        end

        it 'when identity value is given as empty string' do
          params = { user: { name: 'new_minimum_end_user_3', identities: [{ type: 'email', value: '' }] } }
          check_assert_raise(params)
        end

        it 'when updating identity which belongs to another user via user id' do
          params = { user: { id: user_two.id, identities: [{type: 'phone', value: identity_three.value}] } }
          check_assert_raise(params)
        end

        def check_assert_raise(params)
          assert_raise(Zendesk::Users::UpsertInitializer::UpsertError) do
            Zendesk::Users::UpsertInitializer.new(current_account, current_user, params).apply
          end
        end
      end
    end
  end
end
