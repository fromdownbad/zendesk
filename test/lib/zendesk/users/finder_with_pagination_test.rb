require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::Users::FinderWithPagination do
  fixtures :accounts, :users, :groups, :organization_memberships, :organizations

  let(:org_memberships) do
    [
      stub(organization_id: 12),
      stub(organization_id: 456)
    ]
  end

  before do
    @account = accounts(:minimum)
    @user    = users(:minimum_admin)
    @user.stubs(:account).returns(@account)
    @users   = stub('users')
    @result  = stub('result')
  end

  describe "#users" do
    before do
      @organizations = stub
      @organization  = stub
      @account.expects(:send).with(:organizations).returns(@organizations)
      @organization.expects(:users).returns(@users)
      # return the same "scope" from stubbed methods:
      @users.stubs(:order).returns(@users)
      @users.stubs(:includes).returns(@users)
    end

    it "calls paginate with pagination options" do
      @users.expects(:paginate).with(paginate_options).returns(@result)
      @organizations.expects(:find_by_id).with("123").once.returns(@organization)

      assert_equal(
        @result,
        Zendesk::Users::FinderWithPagination.new(@account, @user, organization: "123").users
      )
    end
  end

  describe "#many_users" do
    before do
      @finder = Zendesk::Users::FinderWithPagination.new(@account, @user, {})
    end

    it "calls find_users with pagination options" do
      @finder.expects(:find_users).with(expected_options(conditions: {
        id: [31415, 1, 3, 4]
      })).returns([@user])

      assert_equal [@user], @finder.many_users([31415, 1, 3, 4])
    end
  end

  describe "#find_by_association" do
    before do
      @users = stub('users')
      Zendesk::Users::Finder.any_instance.expects(:find_by_permissions).never
      Zendesk::Users::Finder.any_instance.stubs(:ensure_permissions)
      @groups = stub('groups')
      @group  = stub('group')
      @account.expects(:send).with(:groups).returns(@groups)
      @group.expects(:users).returns(@users)
      # return the same "scope" from stubbed methods:
      @users.stubs(:order).returns(@users)
      @users.stubs(:includes).returns(@users)
    end

    it "calls paginate with pagination options" do
      @groups.expects(:find_by_id).with("123").once.returns(@group)
      @users.expects(:paginate).with(paginate_options).returns(@result)
      assert_equal @result, Zendesk::Users::FinderWithPagination.new(@account, @user, group: "123").users
    end
  end

  describe "for a group and a role filter without stubs" do
    let(:for_end_user) do
      { role: [Role::END_USER.id], organization_id: organizations(:minimum_organization1).id }
    end
    let(:for_agent) { for_end_user.merge(role: [Role::AGENT.id]) }

    before do
      users(:minimum_agent).organizations << organizations(:minimum_organization1)
    end

    it "finds the correct users" do
      assert_equal 2, Zendesk::Users::FinderWithPagination.new(@account, @user, for_end_user).users.size
      assert_equal 1, Zendesk::Users::FinderWithPagination.new(@account, @user, for_agent).users.size
    end
  end

  def paginate_options
    { page: 1, per_page: 100 }
  end

  def expected_options(extra = {})
    { include: Zendesk::Serialization::UserSerialization::SERIALIZATION_FINDER_INCLUDES, order: { id: :asc }, conditions: {} }.merge(paginate_options).merge(extra)
  end
end
