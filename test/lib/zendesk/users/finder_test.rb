require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 4

describe Zendesk::Users::Finder do
  fixtures :accounts, :users, :groups, :organization_memberships, :organizations

  let(:org_memberships) do
    [
      stub(organization_id: 12),
      stub(organization_id: 456)
    ]
  end

  before do
    @account = accounts(:minimum)
    @user    = users(:minimum_admin)
    @user.stubs(:account).returns(@account)
    @users   = stub
    @result  = stub(use_index: stub)
  end

  describe '#initialize' do
    describe '#query' do
      it 'gets stripped' do
        assert_equal 'hello', Zendesk::Users::Finder.new(@account, @user, query: ' hello ').query
      end

      it 'gets set to nil when not present' do
        assert_nil Zendesk::Users::Finder.new(@account, @user, query: '  ').query
      end
    end
  end

  describe '#find_by_roles' do
    describe 'with default behaviour' do
      before do
        @finder = Zendesk::Users::Finder.new(@account, @user, {})
      end

      it "does not use `index_users_on_account_id_and_is_active_and_roles` on all roles" do
        refute_includes(
          @finder.find_by_roles({}).to_sql,
          "USE INDEX(index_users_on_account_id_and_is_active_and_roles)"
        )
      end

      it "uses `index_users_on_account_id_and_is_active_and_roles` for specific roles" do
        @finder = Zendesk::Users::Finder.new(@account, @user, type: 'agent')
        assert_includes(
          @finder.find_by_roles({}).to_sql,
          "USE INDEX(index_users_on_account_id_and_is_active_and_roles)"
        )
      end

      describe 'with include_inactive as an option' do
        describe_with_arturo_enabled(:api_enable_show_many_deleted_users) do
          it "does not use the `index_users_on_account_id_and_is_active_and_roles`" do
            refute_includes(
              @finder.find_by_roles(include_inactive: 1).to_sql,
              "USE INDEX(index_users_on_account_id_and_is_active_and_roles)"
            )
          end
        end

        describe_with_arturo_disabled(:api_enable_show_many_deleted_users) do
          it "does not use `index_users_on_account_id_and_is_active_and_roles`" do
            refute_includes(
              @finder.find_by_roles(include_inactive: 1).to_sql,
              "USE INDEX(index_users_on_account_id_and_is_active_and_roles)"
            )
          end
        end
      end
    end

    describe 'with legacyagents' do
      before do
        @finder = Zendesk::Users::Finder.new(@account, @user, legacyagents: 1)
      end

      it "uses the `index_users_on_account_id_and_is_active_and_roles`" do
        assert_includes(
          @finder.find_by_roles({}).to_sql,
          "USE INDEX(index_users_on_account_id_and_is_active_and_roles)"
        )
      end
    end

    describe 'with seat_type' do
      before do
        @finder = Zendesk::Users::Finder.new(@account, @user, seat_type: 'voice')
      end

      it "does not use the `index_users_on_account_id_and_is_active_and_roles`" do
        # the UserSeat will return an array, so we can't use `to_sql` to check
        # the index.
        assert @finder.find_by_roles({}).is_a?(Array)
      end
    end

    describe 'with all roles' do
      before do
        @finder1 = Zendesk::Users::Finder.new(@account, @user, role: [0, 2, 4])
        @finder2 = Zendesk::Users::Finder.new(@account, @user, role: ['end-user', 'agent', 'admin'])
      end

      it 'when all roles are given as numbers, uses the `index_users_on_account_id_id`' do
        assert_includes(@finder1.find_by_roles({}).to_sql, "USE INDEX(index_users_on_account_id_id)")
      end

      it 'when all roles are given as strings, uses the `index_users_on_account_id_id`' do
        assert_includes(@finder2.find_by_roles({}).to_sql, "USE INDEX(index_users_on_account_id_id)")
      end
    end

    describe 'with multiple roles' do
      it 'when end-user and agent roles are given, uses the `index_users_on_account_id_id`' do
        @finder = Zendesk::Users::Finder.new(@account, @user, role: ['end-user', 'agent'])
        assert_includes(@finder.find_by_roles({}).to_sql, "USE INDEX(index_users_on_account_id_id)")
      end

      it 'when end-user and admin roles are given, uses the `index_users_on_account_id_id`' do
        @finder = Zendesk::Users::Finder.new(@account, @user, role: ['end-user', 'admin'])
        assert_includes(@finder.find_by_roles({}).to_sql, "USE INDEX(index_users_on_account_id_id)")
      end

      it 'when agent and admin roles are given, uses the `index_users_on_account_id_and_is_active_and_roles`' do
        @finder = Zendesk::Users::Finder.new(@account, @user, role: ['agent', 'admin'])
        assert_includes(@finder.find_by_roles({}).to_sql, "USE INDEX(index_users_on_account_id_and_is_active_and_roles)")
      end
    end

    describe 'with only one role' do
      it 'when end-user is given, uses the `index_users_on_account_id_and_is_active_and_roles`' do
        @finder = Zendesk::Users::Finder.new(@account, @user, role: ['end-user'])
        assert_includes(@finder.find_by_roles({}).to_sql, "USE INDEX(index_users_on_account_id_and_is_active_and_roles)")
      end

      it 'when agent is given, uses the `index_users_on_account_id_and_is_active_and_roles`' do
        @finder = Zendesk::Users::Finder.new(@account, @user, role: ['agent'])
        assert_includes(@finder.find_by_roles({}).to_sql, "USE INDEX(index_users_on_account_id_and_is_active_and_roles)")
      end

      it 'when admin is given, uses the `index_users_on_account_id_and_is_active_and_roles`' do
        @finder = Zendesk::Users::Finder.new(@account, @user, role: ['admin'])
        assert_includes(@finder.find_by_roles({}).to_sql, "USE INDEX(index_users_on_account_id_and_is_active_and_roles)")
      end
    end

    describe 'with no role' do
      it 'should uses the `index_users_on_account_id`' do
        @finder = Zendesk::Users::Finder.new(@account, @user, {})
        assert_includes(@finder.find_by_roles({}).to_sql, "USE INDEX(index_users_on_account_id_id)")
      end
    end
  end

  describe '#users' do
    describe 'when passing an email address query' do
      before do
        Zendesk::Users::Finder.any_instance.expects(:find_by_association).never
        @finder = Zendesk::Users::Finder.new(@account, @user, query: @user.email)
      end

      it 'looks up the user by email address' do
        @finder.expects(:find_users).with(expected_options(conditions: {
          user_identities: { value: @user.email, type: 'UserEmailIdentity', account_id: @account.id },
          roles: [Role::END_USER.id, Role::AGENT.id, Role::ADMIN.id]
        })).returns(@result)

        @finder.users
      end
    end

    describe "when passing an external_id parameter" do
      before do
        Zendesk::Users::Finder.any_instance.expects(:find_by_association).never
        @finder = Zendesk::Users::Finder.new(@account, @user, external_id: '31415')
      end

      it 'looks up the user by external_id' do
        @finder.expects(:find_users).with(expected_options(conditions: {
          external_id: '31415', roles: [Role::END_USER.id, Role::AGENT.id, Role::ADMIN.id]
        })).returns(@result)

        assert_equal @result, @finder.users
      end
    end

    describe 'when passing an association parameter' do
      before do
        @users = stub
        Zendesk::Users::Finder.any_instance.expects(:find_by_permissions).never
        Zendesk::Users::Finder.any_instance.stubs(:ensure_permissions)
      end

      describe 'for a group' do
        before do
          @groups = stub
          @group  = stub
          @account.expects(:send).with(:groups).returns(@groups)
          @group.expects(:users).returns(@users)
        end

        describe 'when query is absent' do
          describe_with_arturo_enabled(:fix_group_filter_for_agent_search) do
            before do
              @users.expects(:where).never
              Zendesk::Users::Finder.expects(:fetch_users).
                with(@users, expected_options).
                returns(@result)
            end

            describe 'by id' do
              before do
                @groups.expects(:find_by_id).with('123').once.returns(@group)
              end

              it 'finds the group by id and returns the users of that group' do
                assert_equal @result, Zendesk::Users::Finder.new(@account, @user, group: '123').users
              end
            end

            describe 'by name' do
              before do
                @groups.expects(:find_by_name).with('hello').once.returns(@group)
              end

              it 'finds the group by name and returns the users of that group' do
                assert_equal @result, Zendesk::Users::Finder.new(@account, @user, group: 'hello').users
              end
            end
          end
        end

        describe 'when query is present' do
          describe_with_arturo_enabled(:fix_group_filter_for_agent_search) do
            before do
              fetch_user_stub = []
              @users.expects(:where).returns(fetch_user_stub)
              Zendesk::Users::Finder.expects(:fetch_users).
                with(fetch_user_stub, expected_options).
                returns(@result)
            end

            describe 'and group filter agent search arturo enabled' do
              before do
                @groups.expects(:find_by_id).with('123').once.returns(@group)
              end

              it 'returns the users of that group whose name contains the query' do
                assert_equal @result, Zendesk::Users::Finder.new(@account, @user, group: '123', query: 'pony').users
              end
            end
          end

          describe_with_arturo_disabled(:fix_group_filter_for_agent_search) do
            before do
              @users.expects(:where).never
              Zendesk::Users::Finder.expects(:fetch_users).
                with(@users, expected_options).
                returns(@result)
            end

            describe 'group filter agent search arturo disabled' do
              before do
                @groups.expects(:find_by_id).with('123').once.returns(@group)
              end

              it 'finds users of group' do
                assert_equal @result, Zendesk::Users::Finder.new(@account, @user, group: '123', query: 'pony').users
              end
            end
          end
        end
      end

      describe 'for an organization' do
        before do
          @organizations = stub
          @organization  = stub
          @account.expects(:send).with(:organizations).returns(@organizations)
          @organization.expects(:users).returns(@users)
          Account.any_instance.stubs(:has_fix_group_filter_for_agent_search?).returns(false)
        end

        describe 'by id' do
          describe 'as a non restricted agent' do
            before do
              Zendesk::Users::Finder.expects(:fetch_users).
                with(@users, expected_options).
                returns(@result)
            end

            it 'finds the organization by id and return the users of that organization' do
              @organizations.expects(:find_by_id).with('123').once.returns(@organization)

              assert_equal(
                @result,
                Zendesk::Users::Finder.new(@account, @user, organization: '123').users
              )
            end

            it 'performs a lookup by id for a fixnum' do
              @organizations.expects(:find_by_id).with(123).returns(@organization)

              assert_equal(
                @result,
                Zendesk::Users::Finder.new(@account, @user, organization: 123).users
              )
            end
          end

          describe 'as a organization restricted agent' do
            before do
              @user.organization_id = 456
              @user.stubs(organization_memberships: org_memberships)
              @user.restriction_id = RoleRestrictionType.ORGANIZATION
              Zendesk::Users::Finder.expects(:fetch_users).
                with(@users, expected_options(conditions: {organization_id: [12, 456]})).
                returns(@result)
            end

            it 'finds the organization by id and return the users of that organization' do
              @organizations.expects(:find_by_id).with('123').once.returns(@organization)
              assert_equal(
                @result,
                Zendesk::Users::Finder.new(@account, @user, organization: '123').users
              )
            end
          end
        end

        describe 'by name' do
          it 'finds the group by name and return the users of that group' do
            Zendesk::Users::Finder.expects(:fetch_users).
              with(@users, expected_options).
              returns(@result)
            @organizations.expects(:find_by_name).with('hello').once.returns(@organization)
            assert_equal(
              @result,
              Zendesk::Users::Finder.new(@account, @user, organization: 'hello').users
            )
          end
        end
      end

      describe 'for a group and a role filter' do
        before do
          @groups = stub
          @group  = stub
          @account.expects(:send).with(:groups).returns(@groups)
          @group.expects(:users).returns(@users)
          Zendesk::Users::Finder.expects(:fetch_users).
            with(@users, expected_options(conditions: { roles: [Role::AGENT.id] })).
            returns(@result)
          Account.any_instance.stubs(:has_fix_group_filter_for_agent_search?).returns(false)
        end

        it 'finds members of the group, just of the specified role' do
          @groups.expects(:find_by_id).with('123').once.returns(@group)
          assert_equal @result, Zendesk::Users::Finder.new(@account, @user, role: [Role::AGENT.id], group: '123').users
        end
      end
    end

    describe 'when passing a seat type' do
      before do
        @finder = Zendesk::Users::Finder.new(@account, @user, role: [Role::AGENT.id, Role::ADMIN.id], seat_type: 'voice')
      end

      it 'looks up agents by the seat type' do
        @finder.expects(:find_users).with(expected_options(conditions: {
          roles: [Role::AGENT.id, Role::ADMIN.id],
          seat_type: 'voice'
        })).returns(@result)

        @finder.users
      end
    end
  end

  describe 'when passing permissions' do
    describe 'and the permission_set exists' do
      before do
        Zendesk::Users::Finder.any_instance.expects(:find_by_roles).never
        @permission     = stub(id: '3')
        permission_sets = stub
        permission_sets.expects(:find_by_id).with(@permission.id).returns(@permission)
        @account.expects(:permission_sets).returns(permission_sets)
      end

      describe 'when permissions_allow_admin_permission_sets arturo is enabled' do
        before { Arturo.enable_feature!(:permissions_allow_admin_permission_sets) }

        it 'finds agents and admins with that permission set' do
          @finder = Zendesk::Users::Finder.new(@account, @user, permission_set: @permission.id)
          @finder.expects(:find_users).with(
            expected_options(conditions: { permission_set_id: @permission.id })
          ).returns(@result)
          assert_equal @result, @finder.users
        end
      end

      it 'finds agents with that permission set' do
        @finder = Zendesk::Users::Finder.new(@account, @user, permission_set: @permission.id)
        @finder.expects(:find_users).with(
          expected_options(conditions: { roles: Role::AGENT.id, permission_set_id: @permission.id })
        ).returns(@result)
        assert_equal @result, @finder.users
      end
    end

    describe 'and the permission set does not exist' do
      before do
        Zendesk::Users::Finder.any_instance.expects(:find_by_roles).never
        @permission     = stub(id: '3')
        permission_sets = stub
        permission_sets.expects(:find_by_id).with(@permission.id).returns(nil)
        @account.expects(:permission_sets).returns(permission_sets)
      end

      it 'returns no agents in the result set' do
        @finder = Zendesk::Users::Finder.new(@account, @user, permission_set: @permission.id)
        assert_equal 0, @finder.users.count
      end
    end
  end

  describe 'when passing :role' do
    [
      ['4', [Role::AGENT.id]],
      ['4/0', [Role::AGENT.id, Role::END_USER.id]],
      ['blerg/13', []],
      [['agent', 'end-user'], [Role::AGENT.id, Role::END_USER.id]]
    ].each do |role, result|
      it "finds users via role #{role}" do
        @result.expects(:use_index).returns(@result)
        @finder = Zendesk::Users::Finder.new(@account, @user, role: role)
        @finder.expects(:find_users).with(
          expected_options(conditions: { roles: result })
        ).returns(@result)
        @finder.users
      end
    end
  end

  describe 'when searching in a specific organization' do
    let(:organization) { organizations(:minimum_organization1) }

    before do
      @account.organizations << organization
      Account.any_instance.stubs(:has_fix_group_filter_for_agent_search?).returns(false)
    end

    it 'finds users via role' do
      @finder = Zendesk::Users::Finder.new(@account, @user, organization_id: organization.id, role: 'end-user')

      assert_equal 2, @finder.users.count
      @finder.users.must_include @account.users.find_by_name('minimum_end_user')
      @finder.users.must_include @account.users.find_by_name('minimum_end_user2')
    end

    it 'finds users via permission_set' do
      @permission     = stub(id: '3')
      permission_sets = stub
      permission_sets.expects(:find_by_id).with(@permission).returns(@permission)
      @account.expects(:permission_sets).returns(permission_sets)

      end_user = users(:minimum_end_user)
      end_user.update_column(:permission_set_id, 3)

      @finder = Zendesk::Users::Finder.new(@account, @user, organization_id: organization.id, permission_set: @permission)

      assert_equal 1, @finder.users.count
      @finder.users.must_include end_user
    end
  end

  describe 'when passing legacyagents' do
    describe 'and no query' do
      before do
        @finder = Zendesk::Users::Finder.new(@account, @user, query: nil, legacyagents: '1')
        @finder.expects(:find_by_database_scan).never
      end

      it 'finds the agents by lookup' do
        @result.expects(:use_index).returns(@result)
        @finder.expects(:find_users).with(
          expected_options(conditions: { roles: Role::AGENT.id, permission_set_id: nil })
        ).returns(@result)
        @finder.users
      end
    end

    describe 'and a query' do
      before do
        ZendeskSearch::Client.any_instance.stubs(:search).returns(count: 0, results: [])
        @finder = Zendesk::Users::Finder.new(@account, @user, query: 'pony', legacyagents: '1')
        @opts = expected_options(
          conditions: [
            'roles IN (?) AND (users.external_id = ? OR users.name LIKE ? OR '\
            'user_identities.value LIKE CONVERT(? USING LATIN1) AND user_identities.account_id = ?)',
            [Role::END_USER.id, Role::AGENT.id, Role::ADMIN.id], 'pony', '%pony%', 'pony%', @account.id
          ],
          references: :user_identities
        )
      end

      it 'builds a kosher SQL statement' do
        assert_equal [], @finder.users
      end

      it 'does not try to find the agents by lookup' do
        @finder.expects(:find_by_search).never
        @finder.expects(:find_users).with(@opts).returns(@result)
        @finder.users
      end

      describe 'for an organization restricted agent' do
        before do
          # need to use organization_membership fixtures instead of stubs here
          # as they will be needed during the permissions check
          org_memberships = [organization_memberships(:minimum), organization_memberships(:minimum2)]
          @user.organization_id = 456
          @user.stubs(organization_memberships: org_memberships)
          @user.restriction_id = RoleRestrictionType.ORGANIZATION

          @opts = expected_options(
            conditions: [
              'roles IN (?) AND ' \
              '(users.external_id = ? OR users.name LIKE ? OR user_identities.value LIKE CONVERT(? USING LATIN1) AND user_identities.account_id = ?)' \
              ' AND organization_id IN (?)',
              [Role::END_USER.id, Role::AGENT.id, Role::ADMIN.id], 'pony', '%pony%',
              'pony%', @account.id, org_memberships.map(&:organization_id)
            ]
          )
        end

        it 'adds the organization constraint to the conditions' do
          @finder.expects(:limited_user_count?).returns(true)
          @finder.expects(:find_by_search).never
          @users.expects(:references).returns(@users)
          Zendesk::Users::Finder.expects(:fetch_users).with(@users, @opts).returns(@result)
          @account.expects(:users).returns(@users)
          @finder.users
        end
      end
    end
  end

  describe 'when passing a query' do
    before do
      @finder = Zendesk::Users::Finder.new(@account, @user, query: 'pony')
    end

    describe 'and the account is allowed to scan the DB users' do
      before do
        @finder.expects(:allow_database_scan?).returns(true)
      end

      it 'scans the database' do
        @finder.expects(:find_by_search).never
        @finder.expects(:find_users).returns(@result)
        @finder.users
      end
    end

    describe 'and the account is not allowed to scan the DB users' do
      before do
        @finder.expects(:allow_database_scan?).returns(false)
      end

      it 'goes to the search index' do
        @finder.expects(:find_by_search).once
        @finder.users
      end
    end
  end

  describe '#many_users' do
    before do
      @finder = Zendesk::Users::Finder.new(@account, @user, {})
    end

    it 'looks up a list of users' do
      @finder.expects(:find_users).with(expected_options(conditions: {
        id: [31415, 1, 3, 4]
      })).returns([@user])

      assert_equal [@user], @finder.many_users([31415, 1, 3, 4])
    end

    it 'looks up a list of users by :external_ids' do
      @finder.expects(:find_users).with(expected_options(conditions: {
        external_id: [31415, 1, 3, 4]
      })).returns([@user])

      assert_equal [@user], @finder.many_users([31415, 1, 3, 4], key: :external_ids)
    end

    it 'raises error when user does not have permission' do
      forbidden_user = stub
      forbidden_user.stubs(:id).returns(1)
      @user.stubs(:can?).with(:view, forbidden_user).returns(false)
      @finder.expects(:find_users).with(expected_options(conditions: {
        id: [1]
      })).returns([forbidden_user])

      assert_raise(Access::Denied) { @finder.many_users([1]) }
    end
  end

  describe '#restricted_conditions' do
    let(:finder) { Zendesk::Users::Finder.new(@account, user, query: 'finger') }

    describe 'when the current user is a restricted agent' do
      let(:user) do
        stub(
          organization_id: 12,
          organization_memberships: org_memberships,
          restriction_id: RoleRestrictionType.ORGANIZATION,
          account: @account
        )
      end

      it 'adds organization restrictions to a hash' do
        assert_equal(
          {organization_id: [12, 456], moo: 'beep'},
          finder.restricted_conditions(moo: 'beep')
        )
      end

      it 'adds organization restrictions to an array' do
        assert_equal(
          ['some query ? AND organization_id IN (?)', 123, [12, 456]],
          finder.restricted_conditions(['some query ?', 123])
        )
      end
    end

    describe 'when the current user is a non-restricted agent' do
      let(:user) do
        stub(
          organization_id: 12,
          organization_memberships: org_memberships,
          restriction_id: nil,
          account: @account
        )
      end

      it 'does not modify the passed hash' do
        assert_equal(
          { moo: 'beep' },
          finder.restricted_conditions(moo: 'beep')
        )
      end

      it 'does not modify the passed array' do
        assert_equal(
          ['some query ?', 123],
          finder.restricted_conditions(['some query ?', 123])
        )
      end
    end
  end

  describe '#find_by_id_list' do
    before do
      @finder = Zendesk::Users::Finder.new(@account, @user, {})
    end

    describe 'with no key specified' do
      it 'defaults to id' do
        @finder.expects(:find_users).with(conditions: { id: [1, 2, 3] }).returns(@result)
        assert_equal @result, @finder.find_by_id_list([1, 2, 3], {})
      end
    end

    describe 'with external_ids specified as key' do
      it 'queries with condition on external_id' do
        @finder.expects(:find_users).with(conditions: { external_id: ['external1', 'external2', 'external3'] }).returns(@result)
        assert_equal @result, @finder.find_by_id_list(['external1', 'external2', 'external3'], key: :external_ids)
      end
    end

    describe 'with emails specified as key' do
      it 'queries with condition on email' do
        @finder.expects(:find_users).with(conditions: { email: ['a@x.com', 'b@y.com', 'c@z.com'] }).returns(@result)
        assert_equal @result, @finder.find_by_id_list(['a@x.com', 'b@y.com', 'c@z.com'], key: :emails)
      end
    end
  end

  def expected_options(extra = {})
    { include: Zendesk::Serialization::UserSerialization::SERIALIZATION_FINDER_INCLUDES, order: { id: :asc }, conditions: {} }.merge(extra)
  end
end
