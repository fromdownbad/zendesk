require_relative "../../../support/test_helper"
require_relative "../../../support/suite_test_helper"

SingleCov.covered!

describe Zendesk::Users::AgentDowngrader do
  include SuiteTestHelper
  fixtures :users, :accounts, :zopim_subscriptions, :permission_sets, :zopim_agents

  describe '.perform' do
    let(:account) { agent.account }
    let(:perform) { Zendesk::Users::AgentDowngrader.perform(agent: agent, products: products) }
    let(:contributor_permission_set) { account.permission_sets.where(role_type: PermissionSet::Type::CONTRIBUTOR).first }
    let(:support_user) { stub }

    before do
      Zendesk::SupportUsers::User.stubs(:new).returns(support_user)
      support_user.stubs(:save).returns(true)
    end

    def self.it_behaves_like_an_agent_downgrade_audit
      before { AgentDowngradeAudit.stubs(:from_user) }

      it 'creates an audit record' do
        AgentDowngradeAudit.expects(:from_user)
        perform
      end
    end

    describe 'when products passed in do not include Chat or Support' do
      let(:products) { [:connect, :guide] }
      let(:agent) { users(:with_paid_zopim_subscription_support_chat_agent) }

      it 'does nothing' do
        support_user.expects(:save).never
        perform
      end
    end

    describe 'when Chat and Support are passed in as products to downgrade entitlement for agent' do
      # This is only possible scenario for Zendesk Suite on CP3 downgrade
      let(:products) { [:chat, :support] }

      describe 'and agent is actually an end user' do
        let(:agent) { users(:minimum_end_user) }

        it 'does nothing' do
          support_user.expects(:save).never
          perform
        end
      end

      describe 'and account has zopim subscription (CP3), ' do
        before do
          agent.zopim_identity.stubs(:destroy)
        end

        describe 'and Support agent with chat enabled' do
          let(:agent) { users(:with_paid_zopim_subscription_support_chat_agent) }

          it 'destroys zopim identity' do
            agent.zopim_identity.expects(:destroy).once
            perform
          end

          it_behaves_like_an_agent_downgrade_audit

          it 'downgrades to End User' do
            perform
            assert_equal agent.roles, Role::END_USER.id
            assert_nil agent.permission_set_id
          end

          it 'saves change' do
            support_user.expects(:save).once
            perform
          end
        end

        describe 'and Chat only agent' do
          let(:agent) { users(:with_paid_zopim_subscription_chat_agent) }

          it 'destroys zopim identity' do
            agent.zopim_identity.expects(:destroy).once
            perform
          end

          it_behaves_like_an_agent_downgrade_audit

          it 'downgrades to End User' do
            perform
            assert_equal agent.roles, Role::END_USER.id
            assert_nil agent.permission_set_id
          end

          it 'saves change' do
            support_user.expects(:save).once
            perform
          end
        end
      end

      describe 'and Support agent' do
        let(:agent) { users(:minimum_agent) }

        it_behaves_like_an_agent_downgrade_audit

        it 'downgrades to End User' do
          perform
          assert_equal agent.roles, Role::END_USER.id
          assert_nil agent.permission_set_id
        end

        it 'saves change' do
          support_user.expects(:save).once
          perform
        end
      end

      describe 'and account is multiproduct' do
        let(:agent) { users(:multiproduct_contributor) }

        describe 'and agent is a CP4 Chat agent (managed in staff service)' do
          before do
            Zendesk::StaffClient.any_instance.stubs(:update_full_entitlements!)
            agent.update_entitlement(product: 'chat', new_entitlement: 'agent')
          end

          it 'disables the agent via staff service api call' do
            Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).once.
              with(agent.id, { chat: { is_active: false } }, skip_validation: false)
            perform
          end

          it_behaves_like_an_agent_downgrade_audit

          it 'leaves Support agent as Contributor' do
            assert_equal agent.permission_set, contributor_permission_set
            perform
            assert_equal agent.roles, Role::AGENT.id
            assert_equal agent.permission_set, contributor_permission_set
          end

          describe 'and agent is also a Support agent' do
            let(:agent) { users(:multiproduct_support_agent) }

            it_behaves_like_an_agent_downgrade_audit

            it 'changes Support agent from Agent to Contributor' do
              perform
              assert_equal agent.roles, Role::AGENT.id
              assert_equal agent.permission_set, contributor_permission_set
            end
          end
        end
      end

      describe 'and account is SPP' do
        let(:agent) { users(:multiproduct_contributor) }
        before do
          account.stubs(:spp?).returns(true)
          account.stubs(:multiproduct?).returns(false)
        end

        describe 'and agent is a CP4 Chat agent (managed in staff service)' do
          before do
            Zendesk::StaffClient.any_instance.stubs(:update_full_entitlements!)
            agent.update_entitlement(product: 'chat', new_entitlement: 'agent')
          end

          it 'disables the agent via staff service api call' do
            Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).once.
              with(agent.id, { chat: { is_active: false } }, skip_validation: false)
            perform
          end

          it_behaves_like_an_agent_downgrade_audit

          it 'leaves Support agent as Contributor' do
            assert_equal agent.permission_set, contributor_permission_set
            perform
            assert_equal agent.roles, Role::AGENT.id
            assert_equal agent.permission_set, contributor_permission_set
          end

          describe 'and agent is also a Support agent' do
            let(:agent) { users(:multiproduct_support_agent) }

            it_behaves_like_an_agent_downgrade_audit

            it 'changes Support agent from Agent to Contributor' do
              perform
              assert_equal agent.roles, Role::AGENT.id
              assert_equal agent.permission_set, contributor_permission_set
            end
          end
        end
      end
    end

    describe 'when Chat is passed in as product to downgrade entitlement for agent' do
      let(:products) { [:chat] }

      describe 'and agent is actually an end user' do
        let(:agent) { users(:minimum_end_user) }

        it 'does nothing' do
          support_user.expects(:save).never
          perform
        end
      end

      describe 'and Support agent' do
        let(:agent) { users(:minimum_agent) }

        it 'keeps the agent role' do
          perform
          assert_equal agent.roles, Role::AGENT.id
        end

        it 'saves change' do
          support_user.expects(:save).once
          perform
        end
      end

      describe 'and account has zopim subscription (CP3), ' do
        before do
          agent.zopim_identity.stubs(:destroy)
        end

        describe 'and Support agent with chat enabled' do
          let(:agent) { users(:with_paid_zopim_subscription_support_chat_agent) }

          it 'destroys zopim identity' do
            agent.zopim_identity.expects(:destroy).once
            perform
          end

          it 'keeps the agent role' do
            perform
            assert_equal agent.roles, Role::AGENT.id
          end

          it 'saves change' do
            support_user.expects(:save).once
            perform
          end
        end

        describe 'and Chat only agent' do
          let(:agent) { users(:with_paid_zopim_subscription_chat_agent) }

          it_behaves_like_an_agent_downgrade_audit

          it 'downgrades agent to end user' do
            perform
            assert_equal agent.roles, Role::END_USER.id
            assert_nil agent.permission_set_id
          end

          it 'saves the agent' do
            support_user.expects(:save).once
            perform
          end
        end
      end

      describe 'and account is SPP' do
        let(:agent) { users(:multiproduct_contributor) }
        before do
          account.stubs(:spp?).returns(true)
          account.stubs(:multiproduct?).returns(false)
        end

        describe 'and agent is a CP4 agent (managed in staff service)' do
          before do
            agent.update_entitlement(product: 'chat', new_entitlement: 'agent')
          end

          it 'disables the agent via staff service api call' do
            Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).once.
              with(agent.id, { chat: { is_active: false } }, skip_validation: false)
            perform
          end

          describe 'and the agent is the last Chat admin' do
            let(:unable_to_disable_last_admin_error) do
              Kragle::UnprocessableEntity.new.tap do |error|
                error.stubs(response: stub(body: { 'errors' => [{ 'title' => 'chat admin required' }] }))
              end
            end

            before do
              Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).once.
                with(agent.id, { chat: { is_active: false } }, skip_validation: false).
                raises(unable_to_disable_last_admin_error)
            end

            it 're-tries the disable operation and sets the owner to be the new Chat admin' do
              # tries again, this time skipping validation
              Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).once.
                with(agent.id, { chat: { is_active: false } }, skip_validation: true)
              # sets the owner to be the new Chat admin
              Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).once.
                with(account.owner.id, chat: { name: 'admin', is_active: true })
              perform
            end
          end
        end
      end

      describe 'and account is multiproduct' do
        let(:agent) { users(:multiproduct_contributor) }

        describe 'and agent is a CP4 agent (managed in staff service)' do
          before do
            agent.update_entitlement(product: 'chat', new_entitlement: 'agent')
          end

          it 'disables the agent via staff service api call' do
            Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).once.
              with(agent.id, { chat: { is_active: false } }, skip_validation: false)
            perform
          end

          describe 'and the agent is the last Chat admin' do
            let(:unable_to_disable_last_admin_error) do
              Kragle::UnprocessableEntity.new.tap do |error|
                error.stubs(response: stub(body: { 'errors' => [{ 'title' => 'chat admin required' }] }))
              end
            end

            before do
              Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).once.
                with(agent.id, { chat: { is_active: false } }, skip_validation: false).
                raises(unable_to_disable_last_admin_error)
            end

            it 're-tries the disable operation and sets the owner to be the new Chat admin' do
              # tries again, this time skipping validation
              Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).once.
                with(agent.id, { chat: { is_active: false } }, skip_validation: true)
              # sets the owner to be the new Chat admin
              Zendesk::StaffClient.any_instance.expects(:update_full_entitlements!).once.
                with(account.owner.id, chat: { name: 'admin', is_active: true })
              perform
            end
          end
        end
      end
    end

    describe 'when Support is passed in as product to downgrade entitlement for agent' do
      let(:products) { [:support] }
      describe 'when end user' do
        let(:agent) { users(:minimum_end_user) }

        it 'does nothing' do
          support_user.expects(:save).never
          perform
        end
      end

      describe 'when Support agent' do
        let(:agent) { users(:minimum_agent) }

        it 'deletes group memberships' do
          refute_empty agent.memberships
          perform
          assert_empty agent.memberships
        end

        it_behaves_like_an_agent_downgrade_audit

        it 'downgrades to End User' do
          perform
          assert_equal agent.roles, Role::END_USER.id
          assert_nil agent.permission_set_id
        end

        it 'saves change' do
          support_user.expects(:save).once
          perform
        end
      end

      describe 'when account has zopim subscription (CP3), ' do
        describe 'and Support agent with chat enabled' do
          let(:agent) { users(:with_paid_zopim_subscription_support_chat_agent) }

          describe_with_arturo_enabled(:ocp_chat_only_agent_deprecation) do
            it_behaves_like_an_agent_downgrade_audit

            it 'enables the Contributor role for the account and downgrades agent to Contributor' do
              assert_nil agent.permission_set

              perform
              assert_equal agent.roles, Role::AGENT.id
              assert_equal agent.permission_set, contributor_permission_set
            end

            it 'saves change' do
              support_user.expects(:save).once
              perform
            end

            it 'keeps zopim identity' do
              agent.zopim_identity.expects(:destroy).never
              perform
            end
          end

          describe_with_arturo_disabled(:ocp_chat_only_agent_deprecation) do
            it 'downgrades agent to ChatOnlyAgent' do
              assert_nil agent.permission_set
              chat_agent_permission_set = account.permission_sets.where(role_type: PermissionSet::Type::CHAT_AGENT).first

              perform
              assert_equal agent.roles, Role::AGENT.id
              assert_equal agent.permission_set, chat_agent_permission_set
            end

            it 'saves change' do
              support_user.expects(:save).once
              perform
            end
          end
        end

        describe 'and Chat only agent' do
          let(:agent) { users(:with_paid_zopim_subscription_chat_agent) }

          it 'keeps agent as ChatOnlyAgent' do
            perform
            assert_equal agent.roles, Role::AGENT.id
            assert_equal agent.permission_set.role_type, PermissionSet::Type::CHAT_AGENT
          end
        end

        describe 'with Contributor role' do
          let(:agent) { users(:with_paid_zopim_subscription_support_chat_agent) }

          before do
            PermissionSet.enable_contributor!(agent.account)
          end

          it_behaves_like_an_agent_downgrade_audit

          it 'downgrades to Contributor' do
            refute_equal agent.permission_set, contributor_permission_set

            perform
            assert_equal agent.roles, Role::AGENT.id
            assert_equal agent.permission_set, contributor_permission_set
          end

          it 'saves change' do
            support_user.expects(:save).once
            perform
          end

          it 'keeps zopim identity' do
            agent.zopim_identity.expects(:destroy).never
            perform
          end
        end
      end

      describe 'when multiproduct shell account' do
        describe 'and Support Admin' do
          let(:agent) { users(:multiproduct_support_admin) }

          it 'downgrades to Contributor' do
            refute_equal agent.permission_set, contributor_permission_set

            perform
            assert_equal agent.roles, Role::AGENT.id
            assert_equal agent.permission_set, contributor_permission_set
          end

          it 'saves change' do
            support_user.expects(:save).once
            perform
          end
        end

        describe 'and Support LightAgent' do
          let(:agent) { users(:multiproduct_light_agent) }

          it_behaves_like_an_agent_downgrade_audit

          it 'downgrades agent to Contributor' do
            refute_equal agent.permission_set, contributor_permission_set

            perform
            assert_equal agent.roles, Role::AGENT.id
            assert_equal agent.permission_set, contributor_permission_set
          end

          it 'saves change' do
            support_user.expects(:save).once
            perform
          end
        end

        describe 'and Support Agent' do
          let(:agent) { users(:multiproduct_support_agent) }

          it_behaves_like_an_agent_downgrade_audit

          it 'downgrades agent to Contributor' do
            refute_equal agent.permission_set, contributor_permission_set

            perform
            assert_equal agent.roles, Role::AGENT.id
            assert_equal agent.permission_set, contributor_permission_set
          end

          it 'saves change' do
            support_user.expects(:save).once
            perform
          end
        end
      end
    end
  end
end
