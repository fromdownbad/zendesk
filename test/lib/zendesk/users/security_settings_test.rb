require_relative '../../../support/test_helper'

SingleCov.covered!

describe 'Zendesk::Users::SecuritySettings' do
  before do
    @account         = mock('account')
    @role_settings   = mock('role settings')
    @security_policy = mock('security policy')
    @agent           = mock('agent')
    @end_user        = mock('end_user')

    @account.stubs(:role_settings).returns(@role_settings)
    @security_policy.stubs(:user=)
    @agent.stubs(:is_agent?).returns(true)
    @end_user.stubs(:is_agent?).returns(false)
  end

  describe "#security_policy" do
    describe "when the user is an agent" do
      before do
        @role_settings.stubs(:security_policy_id_for_role).with(:agent).
          returns(678)
        Zendesk::SecurityPolicy.stubs(:find).with(678, @account).
          returns(@security_policy)

        @security_settings = Zendesk::Users::SecuritySettings.new(
          @agent, @account
        )

        @security_settings.security_policy
      end

      it "returns the proper security policy" do
        assert_equal @security_policy, @security_settings.security_policy
      end

      before_should "set the user on the policy" do
        @security_policy.expects(:user=).with(@agent)
      end
    end

    describe "when the user is an end user" do
      before do
        @role_settings.stubs(:security_policy_id_for_role).with(:end_user).
          returns(543)
        Zendesk::SecurityPolicy.stubs(:find).with(543, @account).
          returns(@security_policy)

        @security_settings = Zendesk::Users::SecuritySettings.new(
          @end_user, @account
        )

        @security_settings.security_policy
      end

      it "returns the proper security policy" do
        assert_equal @security_policy, @security_settings.security_policy
      end

      before_should "set the user on the policy" do
        @security_policy.expects(:user=).with(@end_user)
      end
    end
  end

  describe "#login_allowed?" do
    describe "when the user is an agent" do
      before do
        @role_settings.stubs(:login_allowed_for_role?).with(:facebook, :agent).
          returns(true)

        @security_settings = Zendesk::Users::SecuritySettings.new(@agent, @account)
      end

      it "returns the setting value" do
        assert @security_settings.login_allowed?(:facebook)
      end
    end

    describe "when the user is an end user" do
      before do
        @role_settings.stubs(:login_allowed_for_role?).with(:facebook, :end_user).
          returns(true)

        @security_settings = Zendesk::Users::SecuritySettings.new(@end_user, @account)
      end

      it "returns the setting value" do
        assert @security_settings.login_allowed?(:facebook)
      end
    end
  end

  describe "#only_login_service_allowed?" do
    describe "when the user is an agent" do
      before do
        @role_settings.stubs(:only_login_service_allowed_for_role?).
          with(:facebook, :agent).returns(true)

        @security_settings = Zendesk::Users::SecuritySettings.new(@agent, @account)
      end

      it "returns the setting value" do
        assert @security_settings.only_login_service_allowed?(:facebook)
      end
    end

    describe "when the user is an end user" do
      before do
        @role_settings.stubs(:only_login_service_allowed_for_role?).
          with(:facebook, :end_user).returns(true)

        @security_settings = Zendesk::Users::SecuritySettings.new(@end_user, @account)
      end

      it "returns the setting value" do
        assert @security_settings.only_login_service_allowed?(:facebook)
      end
    end
  end
end
