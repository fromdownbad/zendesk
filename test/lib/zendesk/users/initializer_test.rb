require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'UserInitializer' do
  describe Zendesk::Users::Initializer do
    before { Account.any_instance.stubs(:has_apply_prefix_and_normalize_phone_numbers?).returns(true) }

    fixtures :accounts, :organizations

    let(:current_account) { accounts(:minimum) }
    let(:current_user)    { current_account.owner }
    let(:initializer) do
      Zendesk::Users::Initializer.new(current_account, current_user, params)
    end

    describe 'when change_by_staff_service flag is set to true' do
      let(:initializer) do
        Zendesk::Users::Initializer.new(current_account, current_user, params, true)

        describe '#sanitize_accessible_params' do
          let(:params) do
            {
              user: {
                time_zone: "Pacific Time (US & Canada)",
                locale_id: 4,
                name: 'Morty',
                email: 'morty@example.com'
              }
            }
          end

          before do
            @original_params = params.deep_dup
          end

          it 'skips sanitization and does not strip any parameter values' do
            initializer.send(:sanitize_accessible_params)
            assert_equal @original_params, initializer.params
          end
        end
      end
    end

    describe "initialization" do
      it "fallbacks to the anonymous user if no user is present" do
        assert_equal(
          current_account.anonymous_user,
          Zendesk::Users::Initializer.new(current_account, nil, {}).current_user
        )
      end
    end

    describe "#sanitize_suspension_params" do
      let(:params) do
        {user: { suspended: "hello" }}
      end

      before do
        @original_params = params.deep_dup
      end

      describe "and current_user is the same as user" do
        before do
          initializer.stubs(:user).returns(current_user)
          initializer.send(:sanitize_suspension_params)
        end

        it "does not allow to suspend" do
          assert_equal({ user: {}}, initializer.params)
        end
      end

      describe "and user is the account owner" do
        before do
          user = User.new
          user.expects(:is_account_owner?).returns(true)

          initializer.stubs(:user).returns(user)
          initializer.send(:sanitize_suspension_params)
        end

        it "does not allow to suspend" do
          assert_equal({ user: {}}, initializer.params)
        end
      end

      describe "and user is not the account owner and not the same user" do
        before do
          initializer.stubs(:user).returns(current_account.users.new)
          initializer.send(:sanitize_suspension_params)
        end

        it "allows to suspend" do
          assert_equal @original_params, initializer.params
        end
      end
    end

    describe "#sanitize_tags_params" do
      let(:params) do
        {user: { hello_tag: "hello", role_or_permission_set: 8 }}
      end

      before do
        @original_params = params.deep_dup
      end

      describe "when current_user is an end user" do
        before { current_user.roles = Role::END_USER.id }

        describe "and target user is an end user" do
          before do
            initializer.stubs(:user).returns(User.new)
            initializer.send(:sanitize_tags_params)
          end

          it "removes tags" do
            assert_equal({user: { role_or_permission_set: 8 }}, initializer.params)
          end

          describe "when they are trying to fool us" do
            let(:params) do
              { user: { tags: ["vip"], special_tag: "vip", role_or_permission_set: 8 } }
            end

            it "removes all tags" do
              assert_equal({ user: { role_or_permission_set: 8 }}, initializer.params)
            end
          end
        end

        describe "and target user is an agent" do
          before do
            initializer.stubs(:user).returns(User.new { |u| u.roles = Role::AGENT.id })
            initializer.send(:sanitize_tags_params)
          end

          it "removes tags" do
            assert_equal({user: { role_or_permission_set: 8 }}, initializer.params)
          end
        end

        describe "and target user is an admin" do
          before do
            initializer.stubs(:user).returns(User.new { |u| u.roles = Role::ADMIN.id })
            initializer.send(:sanitize_tags_params)
          end

          it "removes tags" do
            assert_equal({ user: { role_or_permission_set: 8 }}, initializer.params)
          end
        end
      end

      describe "when current_user is an agent" do
        before { current_user.roles = Role::AGENT.id }

        describe "and target user is an end user" do
          before do
            initializer.stubs(:user).returns(User.new)
            initializer.send(:sanitize_tags_params)
          end

          it "does not remove tags" do
            assert_equal @original_params, initializer.params
          end

          describe "with special cases" do
            it "does not remove tag when the word tag is in the middle of the tag" do
              params = { user: { hello_tag_hello: "hello", role_or_permission_set: 8 }}
              @original_params = params.deep_dup
              initializer = Zendesk::Users::Initializer.new(current_account, current_user, params)
              assert_equal @original_params, initializer.params
            end

            it "does not remove tag when there are multiple S" do
              params = { user: { sup_taggsssssssss: "hello", role_or_permission_set: 8 }}
              @original_params = params.deep_dup
              initializer = Zendesk::Users::Initializer.new(current_account, current_user, params)
              assert_equal @original_params, initializer.params
            end
          end
        end

        describe "and target user is an agent" do
          before do
            initializer.stubs(:user).returns(User.new { |u| u.roles = Role::AGENT.id })
            initializer.send(:sanitize_tags_params)
          end

          it "removes tags" do
            assert_equal({ user: { role_or_permission_set: 8 }}, initializer.params)
          end
        end

        describe "and target user is an admin" do
          before do
            initializer.stubs(:user).returns(User.new { |u| u.roles = Role::ADMIN.id })
            initializer.send(:sanitize_tags_params)
          end

          it "removes tags" do
            assert_equal({ user: { role_or_permission_set: 8 }}, initializer.params)
          end
        end
      end

      describe "when current_user is an admin" do
        before { current_user.roles = Role::ADMIN.id }

        describe "and target user is an end user" do
          before do
            initializer.stubs(:user).returns(User.new)
            initializer.send(:sanitize_tags_params)
          end

          it "does not remove tags" do
            assert_equal @original_params, initializer.params
          end
        end

        describe "and target user is an agent" do
          before do
            initializer.stubs(:user).returns(User.new { |u| u.roles = Role::AGENT.id })
            initializer.send(:sanitize_tags_params)
          end

          it "does not remove tags" do
            assert_equal @original_params, initializer.params
          end
        end

        describe "and target user is an admin" do
          before do
            initializer.stubs(:user).returns(User.new { |u| u.roles = Role::ADMIN.id })
            initializer.send(:sanitize_tags_params)
          end

          it "does not remove tags" do
            assert_equal @original_params, initializer.params
          end
        end
      end
    end

    describe "#sanitize_phone" do
      describe "When the phone number starts with x" do
        let (:params) do
          { user: { phone: "x123" } }
        end

        it "extension should be stripped and added seperately" do
          user = User.new
          initializer.stubs(:user).returns(user)
          initializer.send(:sanitize_phone)

          expected_params = { user: { phone: "x123" } }
          assert_equal expected_params, initializer.params
        end
      end

      describe "When the phone number constains an extension" do
        let (:params) do
          { user: { phone: "555x123" } }
        end

        it "extension should be stripped and added seperately" do
          user = User.new
          user.expects(:add_shared_phone_number_extension).with('x123')
          initializer.stubs(:user).returns(user)
          initializer.send(:sanitize_phone)

          expected_params = { user: { phone: "555" } }
          assert_equal expected_params, initializer.params
        end

        describe "When the extension constains whitespace" do
          let (:params) do
            { user: { phone: "555x 123" } }
          end

          it "extension should be stripped and added seperately" do
            user = User.new
            user.expects(:add_shared_phone_number_extension).with('x 123')
            initializer.stubs(:user).returns(user)
            initializer.send(:sanitize_phone)

            expected_params = { user: { phone: "555" } }
            assert_equal expected_params, initializer.params
          end
        end
      end

      describe "when direct_phone_line is false" do
        let (:params) do
          { user: { phone: "555", direct_phone_line: "false" } }
        end
        before do
          user = User.new
          user.stubs(:remove_phone_number_identity)
          initializer.stubs(:user).returns(user)
        end
        it 'changes shared_phone_number to true' do
          initializer.send(:sanitize_phone)
          expected_params = { user: { phone: "555" } }
          assert_equal expected_params, initializer.params
        end
      end

      describe "when direct_phone_line is true" do
        let (:params) do
          { user: { phone: "555", direct_phone_line: "true" } }
        end
        before do
          initializer.stubs(:user).returns(User.new)
        end
        it 'changes shared_phone_number to false' do
          initializer.send(:sanitize_phone)
          expected_params = { user: { phone: "555", identities: [{ type: :phone_number, value: "555"}] } }
          assert_equal expected_params, initializer.params
        end
      end

      describe "When account setting is TRUE" do
        before do
          Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(true)
        end

        describe "when E164 validation is FALSE" do
          let (:params) do
            { user: { phone: "+555" } }
          end

          before do
            initializer.stubs(:user).returns(User.new)
          end

          it "does not change the parameters" do
            initializer.send(:sanitize_phone)
            expected_params = { user: { phone: "+555" } }
            assert_equal expected_params, initializer.params
          end
        end

        describe "when E164 validation is TRUE" do
          describe "when shared_phone_number is TRUE" do
            let (:params) do
              { user: { phone: "+15556439870", shared_phone_number: true } }
            end

            it "cleans up the shared_phone_number param" do
              user = User.new
              initializer.stubs(:user).returns(user)
              user.expects(:remove_phone_number_identity).with(params[:user][:phone])
              initializer.send(:sanitize_phone)
              expected_params = { user: { phone: "+15556439870" } }
              assert_equal expected_params, initializer.params
            end
          end

          describe "when shared_phone_number is EMPTY" do
            let (:params) do
              { user: { phone: "+15556439870" } }
            end

            before do
              initializer.stubs(:user).returns(User.new)
            end

            it "adds identities to parameters, if unique" do
              user = User.new
              initializer.stubs(:user).returns(user)
              user.expects(:remove_phone_number_identity).with(params[:user][:phone])
              initializer.send(:sanitize_phone)
              expected_params = { user: { phone: "+15556439870", identities: [{ type: :phone_number, value: "+15556439870"}] } }
              assert_equal expected_params, initializer.params
            end

            it "does NOT change the parameters, if NOT unique" do
              Users::PhoneNumber.any_instance.stubs(:unique?).returns(false)
              initializer.send(:sanitize_phone)
              expected_params = { user: { phone: "+15556439870" } }
              assert_equal expected_params, initializer.params
            end
          end

          describe "when shared_phone_number is FALSE" do
            let (:params) do
              { user: { phone: "+15556439870", shared_phone_number: false } }
            end

            before do
              initializer.stubs(:user).returns(User.new)
            end

            it "adds identities to parameters and cleans up the shared_phone_number param" do
              initializer.expects(:remove_phone_number_identity).with(params[:user][:phone]).never
              initializer.send(:sanitize_phone)
              expected_params = { user: { phone: "+15556439870", identities: [{ type: :phone_number, value: "+15556439870"}] } }
              assert_equal expected_params, initializer.params
            end
          end
        end
      end

      describe "When account setting is FALSE" do
        before do
          Account.any_instance.stubs(:is_end_user_phone_number_validation_enabled?).returns(false)
        end
        describe "when shared_phone_number is TRUE" do
          let (:params) do
            { user: { phone: "+155", shared_phone_number: true } }
          end

          before do
            user = User.new
            user.stubs(:remove_phone_number_identity)
            initializer.stubs(:user).returns(user)
          end
          it "cleans up the shared_phone_number param" do
            initializer.send(:sanitize_phone)
            expected_params = { user: { phone: "+155" } }
            assert_equal expected_params, initializer.params
          end
        end

        describe "when shared_phone_number is EMPTY" do
          describe "when regex validation is TRUE" do
            let (:params) do
              { user: { phone: "+155" } }
            end

            before do
              user = User.new
              user.stubs(:remove_phone_number_identity)
              initializer.stubs(:user).returns(user)
            end

            it "adds identities to parameters, if unique" do
              initializer.send(:sanitize_phone)
              expected_params = { user: { phone: "+155", identities: [{ type: :phone_number, value: "+155"}] } }
              assert_equal expected_params, initializer.params
            end

            it "does NOT change the parameters, if NOT unique" do
              Users::PhoneNumber.any_instance.stubs(:unique?).returns(false)
              initializer.send(:sanitize_phone)
              expected_params = { user: { phone: "+155" } }
              assert_equal expected_params, initializer.params
            end
          end

          describe "when regex validation is FALSE" do
            let (:params) do
              { user: { phone: "+1" } }
            end

            before do
              initializer.stubs(:user).returns(User.new)
            end

            it "does NOT change the parameters" do
              initializer.send(:sanitize_phone)
              expected_params = { user: { phone: "+1" } }
              assert_equal expected_params, initializer.params
            end
          end
        end

        describe "when shared_phone_number is FALSE" do
          let (:params) do
            { user: { phone: "+155", shared_phone_number: false } }
          end

          before do
            initializer.stubs(:user).returns(User.new)
          end

          it "adds identities to parameters and cleans up the shared_phone_number param" do
            initializer.send(:sanitize_phone)
            expected_params = { user: { phone: "+155", identities: [{ type: :phone_number, value: "+155"}] } }
            assert_equal expected_params, initializer.params
          end
        end
      end
    end

    describe "#sanitize_email" do
      describe "when there's no email" do
        let (:params) do
          { user: { identities: [{ type: :email, value: "foo@example.com" }] } }
        end

        before do
          initializer.stubs(:user).returns(User.new)
          initializer.send(:sanitize_email)
        end

        it "has no effect" do
          expected_params = { user: { identities: [{ type: :email, value: "foo@example.com" }] } }
          assert_equal expected_params, initializer.params
        end
      end

      describe "when there's no identities array" do
        let (:params) do
          { user: { email: "foo@example.com" } }
        end

        before do
          initializer.stubs(:user).returns(User.new)
          initializer.send(:sanitize_email)
        end

        it "generates one and moves email" do
          expected_params = { user: { identities: [{ type: :email, value: "foo@example.com" }] } }
          assert_equal expected_params, initializer.params
        end
      end

      describe "when there's an email and identities array" do
        let (:params) do
          { user: { email: "foo@example.com", identities: [{ type: :email, value: "foo@example.com" }] } }
        end

        before do
          initializer.stubs(:user).returns(User.new)
          initializer.send(:sanitize_email)
        end

        it "moves email to the identities array" do
          expected_params = { user: { identities: [{ type: :email, value: "foo@example.com" }, {type: :email, value: "foo@example.com"}] } }
          assert_equal expected_params, initializer.params
        end
      end
    end

    describe "#apply_identities" do
      describe "when there are no identities" do
        let (:params) { { user: { } } }

        it "does nothing" do
          Zendesk::Users::Identities::Initializer.expects(:new).never
          initializer.stubs(:user).returns(User.new)
          initializer.send(:apply_identities)
        end
      end

      describe "when there are identities" do
        let (:params) do
          { user: { identities: [{ type: :email, value: "foo@example.com" }] } }
        end

        it "applies identities to the user" do
          initializer.stubs(:user).returns(User.new)
          initializer.send(:apply_identities)

          assert_equal 1, initializer.user.identities.size

          identity = initializer.user.identities.first
          assert identity.is_a?(UserEmailIdentity)
          assert "foo@example.com", identity.value
        end

        describe "when there are dupes" do
          let (:params) do
            { user: { identities: [
              { type: :email, value: "foo@example.com" },
              { type: :email, value: "minimum_end_user@aghassipour.com" },
              { type: :email, value: "foo@example.com" }
            ] } }
          end

          it "only applies unique identities to the user" do
            user = users(:minimum_end_user)
            identity_count = user.identities.size
            initializer.stubs(:user).returns(user)
            initializer.send(:apply_identities)
            assert_equal identity_count + 1, user.identities.size
          end
        end

        describe "when there are caps" do
          let (:params_cap) do
            { user: { identities: [
              { type: :email, value: "Foo@example.com" },
            ] } }
          end

          it "doesn't apply a new identity" do
            user = users(:minimum_end_user)
            identity_count = user.identities.size
            initializer.stubs(:user).returns(user)

            initializer.send(:apply_identities)

            initializer.stubs(:params).returns(params_cap)
            initializer.send(:apply_identities)

            assert_equal identity_count + 1, user.identities.size
          end
        end

        describe "when an existing identical identity has caps" do
          let (:params) do
            { user: { identities: [{ type: :email, value: "minimum_end_user@aghassipour.com" }] } }
          end

          it "doesn't apply a new identity" do
            user = users(:minimum_end_user)
            initializer.stubs(:user).returns(user)

            user.identities.to_a.map! do |identity|
              identity.value = identity.value.capitalize if identity.is_a?(UserEmailIdentity)
            end

            identity_count = user.identities.size
            initializer.stubs(:user).returns(user)
            initializer.send(:apply_identities)

            assert_equal identity_count, user.identities.size
          end
        end
      end
    end

    describe 'update a user that does not have any identities' do
      describe 'with verified: true' do
        let(:params) do
          { user: { verified: true } }
        end

        it 'should delete the verified param' do
          user = users(:inactive_user_redacted)
          assert_equal 0, user.identities.size
          expected_params = { user: {} }
          initializer.send(:apply, user)
          assert_equal expected_params, initializer.params
        end
      end
    end

    describe 'when is_verified present' do
      describe 'as false' do
        let(:params) do
          { user: { is_verified: "false", identities: [{ type: :email, value: "minimum_end_user@test.com" }] } }
        end

        it "deletes the verified param after applying it" do
          user = users(:minimum_end_user)
          initializer.stubs(:user).returns(user)
          expected_size = user.identities.size + 1
          initializer.send(:apply_identities)
          expected_params = { user: {} }
          assert_equal expected_params, initializer.params
          assert_equal expected_size, user.identities.size
          assert user.identities.first.value, user.email
          assert user.identities.first.is_verified
          assert_equal "minimum_end_user@test.com", user.identities.last.value
          assert_equal false, user.identities.last.is_verified
        end
      end

      describe 'as true' do
        let(:params) do
          { user: { is_verified: "true", identities: [{ type: :email, value: "minimum_end_user@test.com" }] } }
        end

        it "deletes the verified param after applying it" do
          user = users(:minimum_end_user)
          initializer.stubs(:user).returns(user)
          expected_size = user.identities.size + 1
          initializer.send(:apply_identities)
          expected_params = { user: {} }
          assert_equal expected_params, initializer.params
          assert_equal expected_size, user.identities.size
          assert user.identities.first.value, user.email
          assert user.identities.first.is_verified
          assert_equal "minimum_end_user@test.com", user.identities.last.value
          assert user.identities.last.is_verified
        end
      end
    end

    describe 'when verified present in params' do
      describe 'as false' do
        let(:params) do
          { user: { verified: false, email: 'minimum_end_user@test.com'} }
        end

        it "deletes the verified param after applying it" do
          user = users(:minimum_end_user)
          expected_size = user.identities.size + 1
          initializer.send(:apply, user)
          expected_params = { user: {} }
          assert_equal expected_params, initializer.params
          assert_equal expected_size, user.identities.size
          assert user.identities.first.value, user.email
          assert user.identities.first.is_verified
          assert_equal "minimum_end_user@test.com", user.identities.last.value
          assert_equal false, user.identities.last.is_verified
        end
      end

      describe 'as true' do
        let(:params) do
          { user: { verified: true, email: 'minimum_end_user@test.com'} }
        end

        it "deletes the verified param after applying it" do
          user = users(:minimum_end_user)
          expected_size = user.identities.size + 1
          initializer.send(:apply, user)
          expected_params = { user: {} }
          assert_equal expected_params, initializer.params
          assert_equal expected_size, user.identities.size
          assert user.identities.first.value, user.email
          assert user.identities.first.is_verified
          assert_equal "minimum_end_user@test.com", user.identities.last.value
          assert user.identities.last.is_verified
        end
      end
    end

    describe 'when creating a user with no identity' do
      before do
        @user = User.new
        @user.expects(:is_account_owner?).returns(true)
        initializer.stubs(:user).returns(@user)
      end
      describe 'verified is given' do
        describe 'as true' do
          let(:params) { { user: { verified: true } } }

          it "should delete the verified param" do
            initializer.send(:apply, @user)
            expected_params = { user: { skip_verify_email: false } }
            assert_equal expected_params, initializer.params
          end
        end

        describe 'as false' do
          let(:params) { { user: { verified: false } } }

          it "should delete the verified param" do
            initializer.send(:apply, @user)
            expected_params = { user: { skip_verify_email: false } }
            assert_equal expected_params, initializer.params
          end
        end
      end
    end

    describe 'when creating a user' do
      before do
        @user = User.new
        @user.expects(:is_account_owner?).returns(true)
        initializer.stubs(:user).returns(@user)
      end

      describe 'with verified' do
        describe 'as true' do
          describe 'with invalid identity type is given in identities' do
            let(:params) { { user: { verified: true, identities: [{type: 'emails', value: 'abc@test.com'}] } } }

            it "should delete the verified param" do
              initializer.send(:apply, @user)
              expected_params = { user: { skip_verify_email: false } }
              assert_equal expected_params, initializer.params
            end
          end

          describe 'with valid identity' do
            describe 'type is given' do
              let(:params) { { user: { verified: true, email: 'abc@test.com' } } }

              it "should delete the verified param" do
                initializer.send(:apply, @user)
                expected_params = { user: { skip_verify_email: false } }
                assert_equal expected_params, initializer.params
              end
            end

            describe 'type is given in identities' do
              let(:params) { { user: { verified: true, identities: [{type: 'email', value: 'abc@test.com'}] } } }

              it "should not delete the verified param" do
                initializer.send(:apply, @user)
                expected_params = { user: { skip_verify_email: false } }
                assert_equal expected_params, initializer.params
              end
            end
          end
        end

        describe 'as false' do
          describe 'with invalid identity' do
            describe 'type is given in identities' do
              let(:params) { { user: { verified: false, identities: [{type: 'emails', value: 'abc@test.com'}] } } }

              it "should delete the verified param" do
                initializer.send(:apply, @user)
                expected_params = { user: { skip_verify_email: false } }
                assert_equal expected_params, initializer.params
              end
            end
          end

          describe 'with valid identity' do
            describe 'type is given' do
              let(:params) { { user: { verified: false, email: 'abc@test.com' } } }

              it "should delete the verified param" do
                initializer.send(:apply, @user)
                expected_params = { user: { skip_verify_email: false } }
                assert_equal expected_params, initializer.params
              end
            end

            describe 'type is given in identities' do
              let(:params) { { user: { verified: false, identities: [{type: 'email', value: 'abc@test.com'}] } } }

              it "should delete the verified param" do
                initializer.send(:apply, @user)
                expected_params = { user: { skip_verify_email: false } }
                assert_equal expected_params, initializer.params
              end
            end
          end
        end
      end
    end

    describe "when verified flag is included in the params list and is false" do
      let(:params) do
        { user: {verified: false }}
      end

      it "Deletes the verified param after applying it" do
        user = users(:minimum_end_user)
        expected_size = user.identities.size
        initializer.send(:apply, user)
        assert_equal expected_size, user.identities.size
        assert user.identities.first.value, user.email
        assert_equal false, user.identities.first.is_verified
      end
    end

    describe "when verified flag is included in the params list and is true" do
      let(:params) do
        { user: {verified: true} }
      end

      it "Deletes the verified param after applying it" do
        user = users(:minimum_end_user)
        expected_size = user.identities.size
        initializer.send(:apply, user)
        assert_equal expected_size, user.identities.size
        assert user.identities.first.value, user.email
        assert user.identities.first.is_verified
      end
    end

    describe "#sanitize_password_params" do
      describe "when password is only in user" do
        let(:params) do
          { user: { password: "foo" } }
        end

        it "moves password to the root level" do
          initializer.stubs(:user).returns(User.new)
          initializer.send(:sanitize_password_params)
          expected_params = { user: {}, password: "foo" }
          assert_equal expected_params, initializer.params
        end
      end

      describe "when password is in base and user" do
        let(:params) do
          { user: { password: "foo" }, password: "bar" }
        end

        it "deletes password from user" do
          initializer.stubs(:user).returns(User.new)
          initializer.send(:sanitize_password_params)
          expected_params = { user: {}, password: "bar" }
          assert_equal expected_params, initializer.params
        end
      end
    end

    describe "#apply_group_association" do
      describe "when user has a group_ids entry" do
        let(:params) do
          { user: { group_ids: "foo" } }
        end

        it "assigns whatever is in group_ids to the user" do
          new_user = User.new
          new_user.expects(:groups=).with("foo")
          initializer.stubs(:user).returns(new_user)
          initializer.send(:apply_group_association)
          expected_params = { user: {} }
          assert_equal expected_params, initializer.params
        end
      end
    end

    describe "#sanitize_signature" do
      describe "when passed params with a plain signature" do
        let(:params) do
          {user: { signature: "Wibble" }}
        end

        before do
          initializer.stubs(:user).returns(current_user)
          initializer.send(:sanitize_signature)
        end

        it "moves the signature to the top level of the params" do
          assert_equal({ user: {}, signature: "Wibble" }, params)
        end
      end

      describe "when passed params with nested plain signature hash" do
        let(:params) do
          {user: { signature: { value: "Wibble" }}}
        end

        before do
          initializer.stubs(:user).returns(current_user)
          initializer.send(:sanitize_signature)
        end

        it "moves the signature to the top level of the params" do
          assert_equal({ user: {}, signature: "Wibble" }, params)
        end
      end

      describe "when passed params with an html signature" do
        let(:params) do
          {user: { html_signature: "<h2>Wibble</h2>" }}
        end

        before do
          initializer.stubs(:user).returns(current_user)
          initializer.send(:sanitize_signature)
        end

        it "moves the signature to the top level of the params" do
          assert_equal({ user: {}, html_signature: "<h2>Wibble</h2>" }, params)
        end
      end

      describe "when passed params with both a plain and an html signature" do
        let(:params) do
          {user: { signature: "Wibble", html_signature: "<h2>Wibble</h2>" }}
        end

        before do
          initializer.stubs(:user).returns(current_user)
          initializer.send(:sanitize_signature)
        end

        it "moves the rich signature to the top level of the params" do
          assert_equal({ user: {}, html_signature: "<h2>Wibble</h2>" }, params)
        end
      end

      describe "when passed params without signature" do
        let(:params) do
          {user: { name: "Bubbles" }}
        end

        before do
          initializer.stubs(:user).returns(current_user)
          initializer.send(:sanitize_signature)
        end

        it "does not wipe the signature" do
          assert_equal({ user: { name: "Bubbles" }}, params)
        end
      end
    end

    describe "#sanitize_accessible_params" do
      let(:params) do
        {
          user: {
            roles: Role::AGENT.id, name: "First Last",
            time_zone: "Pacific Time (US & Canada)",
            locale_id: 4, role_or_permission_set: 8, openid_url: "123",
            identities: [], groups: [], organizations: []
          }
        }
      end

      before do
        @original_params = params.deep_dup
      end

      describe "when current_user is an end user" do
        before do
          current_user.roles = Role::END_USER.id
          initializer.send(:sanitize_accessible_params)
        end

        it "strips anything but :locale_id, :time_zone, :name, :phone, :identities" do
          assert_equal(
            {
              user:               {
                time_zone: "Pacific Time (US & Canada)",
                name: "First Last",
                locale_id: 4,
                identities: []
              }
            },
            initializer.params
          )
        end
      end

      describe "when current_user is an agent" do
        before do
          current_user.roles = Role::AGENT.id
          initializer.send(:sanitize_accessible_params)
        end

        it "includes organizations" do
          assert_includes(
            initializer.params[:user].keys, :organizations
          )
        end

        it "strips out :role_or_permission_set" do
          refute_includes(
            initializer.params[:user].keys, :role_or_permission_set
          )
        end
      end

      describe "when current_user is an admin" do
        before do
          current_user.roles = Role::ADMIN.id
          initializer.send(:sanitize_accessible_params)
        end

        it "allows all parameters" do
          assert_equal @original_params, initializer.params
        end

        describe "with ID param" do
          before do
            params[:user][:id] = 123123
          end

          it "strips ID param" do
            initializer.send(:sanitize_accessible_params)
            assert_equal @original_params, initializer.params
          end
        end
      end
    end

    describe "#stage_params" do
      let(:params) do
        {
          user: {
            moderator: "mod",
            only_private_comments: "private_comments"
          }
        }
      end

      before { initializer.stubs(:user).returns(User.new) }

      it "maps moderator to is_moderator boolean value" do
        initializer.send(:stage_params)
        assert initializer.params[:user][:is_moderator]
      end

      it "maps only_private_comments to is_private_comments_only" do
        initializer.send(:stage_params)
        assert_equal "private_comments", initializer.params[:user][:is_private_comments_only]
      end

      it "maps ticket_restriction organization to RoleRestrictionType.ORGANIZATION" do
        params[:user][:ticket_restriction] = "organization"
        initializer.send(:stage_params)
        assert_equal Zendesk::Types::RoleRestrictionType.ORGANIZATION, initializer.params[:user][:restriction_id]
      end

      it "maps ticket_restriction groups to RoleRestrictionType.GROUPS" do
        params[:user][:ticket_restriction] = "groups"
        initializer.send(:stage_params)
        assert_equal Zendesk::Types::RoleRestrictionType.GROUPS, initializer.params[:user][:restriction_id]
      end

      it "maps ticket_restriction assigned to RoleRestrictionType.ASSIGNED" do
        params[:user][:ticket_restriction] = "assigned"
        initializer.send(:stage_params)
        assert_equal Zendesk::Types::RoleRestrictionType.ASSIGNED, initializer.params[:user][:restriction_id]
      end

      it "maps ticket_restriction requested to RoleRestrictionType.REQUESTED" do
        params[:user][:ticket_restriction] = "requested"
        initializer.send(:stage_params)
        assert_equal Zendesk::Types::RoleRestrictionType.REQUESTED, initializer.params[:user][:restriction_id]
      end

      it "maps ticket_restriction nil to RoleRestrictionType.NONE" do
        params[:user][:ticket_restriction] = nil
        initializer.send(:stage_params)
        assert_equal Zendesk::Types::RoleRestrictionType.NONE, initializer.params[:user][:restriction_id]
      end
    end

    describe "#prevent_undesired_ticket_access_changes" do
      let(:params) do
        {
          user: {
            ticket_restriction: "organization"
          }
        }
      end

      before { initializer.stubs(:user).returns(current_user) }

      it "deletes the ticket restriction param for light agents if the account doesn't have custom roles" do
        current_user.stubs(:has_permission_set?).returns(true)

        initializer.send(:stage_params)
        initializer.send(:prevent_undesired_ticket_access_changes)
        refute_includes(
          initializer.params[:user].keys, :restriction_id
        )
      end

      it "doesn't delete the ticket restriction param for light agents if the account has custom roles" do
        current_user.stubs(:has_permission_set?).returns(false)

        initializer.send(:stage_params)
        initializer.send(:prevent_undesired_ticket_access_changes)
        assert_includes(
          initializer.params[:user].keys, :restriction_id
        )
      end
    end

    describe "#apply" do
      let(:organization1) { organizations(:minimum_organization1) }
      let(:organization2) { organizations(:minimum_organization2) }
      let(:organization3) { organizations(:minimum_organization3) }
      let(:current_organizations) do
        [
          organization1,
          organization2
        ]
      end
      let(:new_user) { current_account.users.new }
      let(:params) do
        {
          user: {
            name: "dude",
            moderator: "mod",
            email: "help@gmail.com",
            via: Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET,
            user_fields: {"cf1" => "foobar", "system::sf1" => 'system_field_value'},
            identities: [
              { type: :google, value: "test@gmail.com" },
              { invalid: :help },
              { type: :email, value: "help@gmail.com" }
            ],
            active_brand_id: 2
          }
        }
      end

      before do
        current_user.roles = Role::ADMIN.id
        initializer.stubs(:sanitize_suspension_params)
      end

      it "apply via setting to User model if present" do
        initializer.send(:apply, new_user)
        assert_equal Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET, new_user.via
      end

      it "adds valid identities" do
        assert_difference "new_user.identities.size", 2 do
          initializer.send(:apply, new_user)
        end

        new_user.identities.each do |identity|
          assert_equal new_user, identity.user
        end
      end

      describe "with custom_role_id parameter" do
        before { new_user.stubs(custom_role_id: nil) }

        it "sets custom_role_id to nil" do
          initializer.send(:apply, new_user)
          assert_nil new_user.custom_role_id
        end
      end

      describe "apply custom_role_id to update an admin" do
        let(:agent_permission_set) { PermissionSet.create!(account: new_user.account, name: 'agent') }

        it "sets custom_role id and updates role to be an agent" do
          params[:user][:custom_role_id] = agent_permission_set.id
          initializer.send(:apply, new_user)
          assert_equal agent_permission_set.id, new_user.custom_role_id
          assert_equal "Agent", new_user.role
        end

        describe 'when permissions_allow_admin_permission_sets arturo is enabled' do
          before { Arturo.enable_feature!(:permissions_allow_admin_permission_sets) }

          describe 'an admin permission set' do
            let(:billing_admin_permission_set) { PermissionSet.create_billing_admin!(new_user.account) }

            it 'sets custom_role id and updates role to admin' do
              params[:user][:custom_role_id] = billing_admin_permission_set.id
              initializer.send(:apply, new_user)
              assert_equal billing_admin_permission_set.id, new_user.custom_role_id
              assert_equal "Admin", new_user.role
            end
          end

          describe 'an agent permission set' do
            it 'sets custom_role id and updates role to agent' do
              params[:user][:custom_role_id] = agent_permission_set.id
              initializer.send(:apply, new_user)
              assert_equal agent_permission_set.id, new_user.custom_role_id
              assert_equal "Agent", new_user.role
            end
          end
        end
      end

      describe "with custom fields enabled" do
        before do
          CustomField::Field.without_arsi.delete_all # remove fixtures
          CustomField::Value.without_arsi.delete_all # remove fixtures

          cf = current_account.custom_fields.for_user.build(key: "cf1", title: "twa!")
          cf.type = "Text"
          cf.save!
          cf = current_account.custom_fields.for_user.build(key: "system::sf1", title: "txt.system_field", is_system: 1)
          cf.type = "Text"
          cf.save!
        end

        describe "as a non system user" do
          before { current_user.stubs(:is_system_user?).returns(false) }

          it "apply non system custom field values to the user" do
            user = current_account.users.new
            initializer.send(:apply, user)
            assert_equal({ "cf1" => "foobar", "system::sf1" => nil }, user.custom_field_values.as_json(account: current_account))
          end
        end

        describe "as a system user" do
          before { current_user.stubs(:is_system_user?).returns(true) }

          it "apply changes to both system and non system custom field values" do
            user = current_account.users.new
            initializer.send(:apply, user)
            assert_equal({"cf1" => "foobar", "system::sf1" => "system_field_value"}, user.custom_field_values.as_json(account: current_account))
          end
        end

        describe "with empty user_fields params" do
          let(:params) do
            {
              user: {
                name: "dude",
                moderator: "mod",
                via: Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET,
                user_fields: {},
                identities: [
                  { type: :google, value: "test@gmail.com" },
                  { invalid: :help },
                  { type: :email, value: "help@gmail.com" }
                ]
              }
            }
          end
          before do
            @user = current_account.users.new
            @user.custom_field_values.update_from_hash("cf1" => 'value')
          end
          it "does not change the existing custom field values" do
            initializer.send(:apply, @user)
            assert_equal({ "cf1" => "value", "system::sf1" => nil }, @user.custom_field_values.as_json(account: current_account))
          end
        end
      end

      describe "organization_id parameter" do
        let(:user) { current_account.users.new }

        describe "is not present in params[:user]" do
          it "does not change the user's organization" do
            initializer.send(:apply, user)
            user.save!

            refute user.organization
            refute user.organization_id
          end
        end

        describe "is nil" do
          before { params[:user][:organization_id] = nil }

          it "unsets the user's organization" do
            user.update_attributes(name: "dude",
                                   organization: organization1)

            initializer.send(:apply, user)
            user.save!
            assert_nil user.reload.organization
            assert_nil user.organization_id
          end
        end

        describe "is a valid organization id" do
          before do
            params[:user][:organization_id] = organization1
            initializer.send(:apply, user)
            user.save!
          end

          it "sets the user's organization" do
            assert_equal organization1, user.organization
            assert_equal organization1.id, user.organization_id
          end
        end

        describe "current_user is organization restricted agent" do
          before do
            current_user.stubs(:agent_restriction?).with(:none).returns(false)
            current_user.stubs(:agent_restriction?).with(:assigned).returns(false)
            current_user.stubs(:agent_restriction?).with(:groups).returns(false)
            current_user.stubs(:agent_restriction?).with(:organization).returns(true)
            current_user.roles = Role::AGENT.id
            current_user.organization = organization1
          end

          it "assigns the user to the agent's organization when the organization_id key is not present" do
            initializer.send(:apply, user)

            user.save!
            assert_equal organization1.id, user.reload.organization_id
            assert_equal organization1, user.organization
          end

          it "assigns the user to the agent's organization even if an organization_id key is present" do
            params[:user][:organization_id] = organization2.id
            initializer.send(:apply, user)

            user.save!
            assert_equal organization1.id, user.reload.organization_id
            assert_equal organization1, user.organization
          end
        end
      end

      describe "organization parameter" do
        let(:user) { current_account.users.new }

        describe "is not present in params[:user]" do
          it "does not change the user's organization" do
            initializer.send(:apply, user)
            user.save!
            user.reload

            assert_nil user.organization
            assert_nil user.organization_id
          end
        end

        describe "is a valid organization name" do
          before do
            params[:user][:organization] = {name: organization1.name }
          end

          it "sets the user's organization" do
            initializer.send(:apply, user)
            user.save!
            user.reload

            assert_equal organization1, user.organization
          end
        end

        describe "is a nil value" do
          before do
            params[:user][:organization] = nil
          end

          it "does not blowup or set the users organization" do
            initializer.send(:apply, user)
            user.save!
            user.reload

            assert_nil user.organization
            assert_nil user.organization_id
          end
        end

        describe "does not contain the `name` key" do
          before do
            params[:user][:organization] = {foo: "bar"}
          end

          it "does not blowup or set the users organization" do
            initializer.send(:apply, user)
            user.save!
            user.reload

            assert_nil user.organization
            assert_nil user.organization_id
          end
        end

        describe "current_user is organization restricted agent" do
          before do
            current_user.stubs(:agent_restriction?).with(:none).returns(false)
            current_user.stubs(:agent_restriction?).with(:assigned).returns(false)
            current_user.stubs(:agent_restriction?).with(:groups).returns(false)
            current_user.stubs(:agent_restriction?).with(:organization).returns(true)
            current_user.roles = Role::AGENT.id
            current_user.organization = organization1
          end

          it "assigns the user to the agent's organization when the organization_id key is not present" do
            initializer.send(:apply, user)
            user.save!
            user.reload

            assert_equal organization1, user.organization
          end

          it "assigns the user to the agent's organization even if an organization_id key is present" do
            params[:user][:organization] = {name: organization2.name }
            initializer.send(:apply, user)
            user.save!
            user.reload

            assert_equal organization1, user.organization
          end
        end
      end

      describe "with multiple organizations enabled and params[:user][:organization_ids] present" do
        before { Account.any_instance.stubs(has_multiple_organizations_enabled?: true) }

        describe "with params[:user][:organization_ids] present" do
          before { params[:user][:organization_ids] = current_organizations.map(&:id) }

          it "adds valid organizations" do
            initializer.send(:apply, new_user)

            assert_same_elements(
              current_organizations.map(&:id),
              new_user.organization_memberships.map(&:organization_id)
            )
          end

          it "raises an error if organizations includes invalid organization ids" do
            params[:user][:organization_ids] = [current_organizations.first.id, 999]

            assert_raise(Users::OrganizationMemberships::OrganizationNotFound) do
              initializer.send(:apply, new_user)
            end
          end

          describe "and params[:user][:organization_id] is present" do
            before { params[:user][:organization_id] = organization3.id }

            it "ignores the organization_id parameter" do
              initializer.send(:apply, new_user)

              assert_equal organization1.id, new_user.organization_id
            end
          end
        end

        describe "params[:user][:organizations] present" do
          before { params[:user][:organizations] = current_organizations.map(&:id) }

          it "adds valid organizations" do
            initializer.send(:apply, new_user)

            assert_same_elements(
              current_organizations.map(&:id),
              new_user.organization_memberships.map(&:organization_id)
            )
          end
        end
      end

      describe "with a plain signature" do
        let(:params) do
          {
            user: {
              signature: "plain",
              role: "agent",
              name: "dude",
              moderator: "mod",
              via: Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET,
              user_fields: {"cf1" => "foobar", "system::sf1" => 'system_field_value'},
              identities: [
                { type: :google, value: "test@gmail.com" },
                { invalid: :help },
                { type: :email, value: "help@gmail.com" }
              ]
            }
          }
        end

        before { initializer.send(:apply, new_user) }

        it "sets a plain signature" do
          new_user.signature.must_be :present?
          new_user.signature.read_attribute(:value).must_equal 'plain'
          new_user.signature.read_attribute(:is_rich).must_equal false
        end
      end

      describe "with a rich signature" do
        let(:params) do
          {
            user: {
              html_signature: "rich",
              role: "agent",
              name: "dude",
              moderator: "mod",
              via: Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET,
              user_fields: {"cf1" => "foobar", "system::sf1" => 'system_field_value'},
              identities: [
                { type: :google, value: "test@gmail.com" },
                { invalid: :help },
                { type: :email, value: "help@gmail.com" }
              ]
            }
          }
        end

        before { initializer.send(:apply, new_user) }

        it "sets a rich signature" do
          new_user.signature.must_be :present?
          new_user.signature.read_attribute(:value).must_equal 'rich'
          new_user.signature.read_attribute(:is_rich).must_equal true
        end
      end

      describe "with both a plain and a rich signature" do
        let(:params) do
          {
            user: {
              html_signature: "rich",
              signature: "plain",
              role: "agent",
              name: "dude",
              moderator: "mod",
              via: Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET,
              user_fields: {"cf1" => "foobar", "system::sf1" => 'system_field_value'},
              identities: [
                { type: :google, value: "test@gmail.com" },
                { invalid: :help },
                { type: :email, value: "help@gmail.com" }
              ]
            }
          }
        end

        before { initializer.send(:apply, new_user) }

        it "sets a rich signature" do
          new_user.signature.must_be :present?
          new_user.signature.read_attribute(:value).must_equal 'rich'
          new_user.signature.read_attribute(:is_rich).must_equal true
        end
      end

      describe "with brand" do
        describe "with multibrand enabled and multiple active brands" do
          before { Account.any_instance.stubs(:has_multiple_active_brands?).returns(true) }

          it "it should set the active_brand_id on the user" do
            initializer.send(:apply, new_user)
            assert_equal 2, new_user.active_brand_id
          end
        end

        describe "without multibrand enabled" do
          before { Account.any_instance.stubs(:has_multiple_active_brands?).returns(false) }

          it "should not set the active_brand_id on the user" do
            initializer.send(:apply, new_user)
            assert_nil new_user.active_brand_id
          end
        end
      end

      describe "with user event publisher" do
        before do
          Account.any_instance.stubs(:has_user_and_organization_fields?).returns(true)
          cf = current_account.custom_fields.for_user.build(key: "cf1", title: "twa!")
          cf.type = "Text"
          cf.save!
          initializer.stubs(:user).returns(current_user)
          initializer.send(:sanitize_suspension_params)
        end
        describe "with account having user event publishing enabled" do
          let(:params) do
            {
                user: {
                    signature: "plain",
                    role: "agent",
                    name: "dude",
                    via: Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET,
                    user_fields: {"cf1" => "foobar"},
                    email: 'morty@example.com'
                }
            }
          end

          it "should publish domain events" do
            initializer.send(:apply, new_user)
            assert_equal new_user.domain_events.size, 1
          end
        end
      end

      describe "when custom field is changed" do
        before do
          Account.any_instance.stubs(:has_user_and_organization_fields?).returns(true)
          cf = current_account.custom_fields.for_user.build(key: "cf1", title: "twa!")
          cf.type = "Text"
          cf.save!
          initializer.stubs(:user).returns(current_user)
          initializer.send(:sanitize_suspension_params)
        end

        let(:params) do
          {
              user: {
                  signature: "plain",
                  role: "agent",
                  name: "dude",
                  via: Zendesk::Constants::ProvisioningChannel::GOOGLE_APP_MARKET,
                  user_fields: {"cf1" => "foobar"},
                  email: 'morty@example.com'
              }
          }
        end

        it "should publish domain events" do
          initializer.send(:apply, new_user)
          assert_equal new_user.domain_events.size, 1
          event = new_user.domain_events.first
          event.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::UserEvent)
          event.custom_field_changed.must_be_kind_of(Zendesk::Protobuf::Support::Users::V2::CustomFieldChanged)
          assert_equal event.custom_field_changed.current.text_value, "foobar"
        end
      end

      describe "if custom_created_at is present" do
        let(:custom_datetime) { 1.year.ago }

        before do
          travel_to Time.now
          @created_at = user.created_at
          params[:user][:custom_created_at] = custom_datetime
          params[:user][:migrated_from] = '<application name>'
          initializer.send(:apply, user)
          user.save!
        end

        after { travel_back }

        describe "with new user" do
          let(:user) { current_account.users.new }

          it "sets the custom_created_at and other settings" do
            assert_equal custom_datetime, user.created_at
            assert_equal Time.now.utc, user.updated_at
            assert_equal Time.now.utc.to_s, user.settings.migrated_at.to_s
            assert_equal params[:user][:migrated_from], user.settings.migrated_from
          end
        end

        describe "with an existing user" do
          let(:user) { users(:minimum_end_user) }

          it "does not set the custom_created_at" do
            assert_equal @created_at, user.created_at.to_time
            assert_equal Time.now.utc.iso8601, user.updated_at.iso8601
          end
        end
      end
    end
  end
end
