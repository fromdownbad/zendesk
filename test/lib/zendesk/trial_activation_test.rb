require_relative "../../support/test_helper"
require_relative "../../support/suite_test_helper"
require_relative "../../support/multiproduct_test_helper"

SingleCov.covered!

describe Zendesk::TrialActivation do
  include MultiproductTestHelper
  include SuiteTestHelper

  let(:subject) { Zendesk::TrialActivation }

  before do
    Timecop.freeze(Time.utc(2018, 10, 25))
    stub_staff_service_entitlement_sync
    UserObserver.stubs(:deliver_profile_admin_user_added)
  end

  describe '#activate_support_trial!' do
    let(:shell_account) { setup_shell_account(subdomain: 'shell-account', account_name: 'shell account') }
    let(:another_shell_account) { setup_shell_account(subdomain: 'another-shell-account', account_name: 'another shell account') }
    let(:billing_id)    { nil }
    let(:statsd_client) { stub_for_statsd }

    let(:expected_product_attributes) do
      {
        product: {
          state: :trial,
          plan_settings: {
            plan_type: 3,
            max_agents: 5,
            light_agents: false,
            insights: :good_data,
            suite: false,
            agent_workspace: false
          },
          trial_expires_at: Subscription::TrialManagement::TRIAL_DURATION.from_now.to_date.to_datetime.iso8601
        }
      }
    end

    let(:expected_spp_product_attributes) do
      {
        product: {
          state: :trial,
          plan_settings: {
            plan_type: 3,
            max_agents: 5,
            light_agents: false,
            insights: :good_data,
            suite: false,
            agent_workspace: false
          }
        }
      }
    end

    before do
      stub_account_service_account(shell_account, response_body: { account: { billing_id: billing_id } })
      stub_account_service_account(another_shell_account, response_body: { account: { billing_id: billing_id } })
      Rails.application.config.statsd.stubs(:client).returns(statsd_client)
    end

    it 'resets trial limit, and report metrices to DataDog' do
      TrialLimit.expects(:reset_limit_count).once
      statsd_client.expects(:increment).with('support_trial_activation', tags: ['result:success']).once
      subject.activate_support_trial!(shell_account)
    end

    it 'creates guide plan for account' do
      shell_account.expects(:create_guide_plan)
      subject.activate_support_trial!(shell_account)
    end

    it 'starts the suite trial' do
      shell_account.expects(:start_suite_trial)
      subject.activate_support_trial!(shell_account)
    end

    it 'creates subscription record and returns true' do
      result = subject.activate_support_trial!(shell_account)

      assert shell_account.subscription.present?
      assert result
    end

    describe 'when support product does not exist for shell account' do
      describe 'for spp trial' do
        before do
          Arturo.enable_feature!(:agent_workspace_for_support_only_trial)
          # spp trials are like suite trials, but are eligible for agent workspace activation here
          shell_account.settings.stubs(:suite_trial?).returns(true)
          shell_account.stubs(:spp?).returns(true)
          # chat must be absent
          stub_account_service_missing_product('chat')
        end

        it 'activates agent workspace' do
          shell_account.expects(:activate_agent_workspace_for_trial)
          subject.activate_support_trial!(shell_account)
        end
      end

      it 'activates agent workspace' do
        Arturo.enable_feature!(:agent_workspace_for_support_only_trial)
        stub_account_service_missing_product('chat')
        shell_account.expects(:activate_agent_workspace_for_trial)
        subject.activate_support_trial!(shell_account)
      end

      it 'activates agent workspace with recent chat product' do
        Arturo.enable_feature!(:agent_workspace_for_support_only_trial)
        stub_account_service_product('chat', {
          name: 'chat',
          state: 'active',
          created_at: 30.minutes.ago.iso8601
        })
        shell_account.expects(:activate_agent_workspace_for_trial)
        subject.activate_support_trial!(shell_account)
      end

      it 'creates not_started Support product record in Pravda, then update product to trial state' do
        shell_account.expects(:create_guide_plan) # Guide product creation interferes with expectations below

        payload = { product: { state: Zendesk::Accounts::Product::NOT_STARTED } }
        Zendesk::Accounts::Client.any_instance.expects(:create_product!).with('support', payload, context: 'trial_activation')
        Zendesk::Accounts::Client.any_instance.expects(:update_product!).with('support', expected_product_attributes, context: 'trial_activation')

        subject.activate_support_trial!(shell_account)
      end

      it 'creates correct number of tables' do
        subject.activate_support_trial!(shell_account)

        legacy_account = setup_legacy_account
        legacy_account.save!

        tables_to_ignore = %i[
          cia_attribute_changes
          cia_events
          durable_queue_audits
          fraud_reports
          remote_authentications
          tokens
          pre_account_creations
          trial_extras
          permission_sets
          permission_set_permissions
        ].freeze

        shell_tables = Zendesk::AccountDataFinder.find_data(shell_account)[:account_tables].keys - tables_to_ignore
        legacy_tables = Zendesk::AccountDataFinder.find_data(legacy_account)[:account_tables].keys - tables_to_ignore

        assert_same_elements shell_tables, legacy_tables
      end

      describe 'when account is in trial state' do
        let(:billing_id) { nil }

        it 'does not trigger a Zuora sync' do
          ZBC::Zuora::Jobs::AccountSyncJob.expects(:enqueue).never
          ZBC::Billing::Zuora::Ping.expects(:request).never
          subject.activate_support_trial!(shell_account)
        end
      end

      describe 'when account is purchased' do
        let(:billing_id)              { '123456' }
        let(:zuora_ooc_participation) { stub }
        let!(:account)                { shell_account }

        before do
          Billing::OOC::ZuoraParticipator.stubs(:call).
            with(account).
            returns(zuora_ooc_participation)
        end

        describe 'when not participating in Zuora Out-of-Classic' do
          let(:zuora_ooc_participation) { false }

          it 'triggers Zuora sync (via Classic)' do
            ZBC::Zuora::Jobs::AccountSyncJob.expects(:enqueue).
              with(billing_id).once
            subject.activate_support_trial!(shell_account)
          end
        end

        describe 'when participating in Zuora Out-of-Classic' do
          let(:zuora_ooc_participation) { true }

          it 'triggers Zuora sync (via Billing)' do
            ZBC::Billing::Zuora::Ping.expects(:request).
              with(billing_id).once
            subject.activate_support_trial!(shell_account)
          end
        end
      end
    end

    describe 'when Support product already exists in Pravda' do
      before { stub_account_service_support_product(shell_account, response_body: support_product_response) }

      describe 'and Support product has already started' do
        let(:support_product_response) do
          { product: FactoryBot.build(:support_trial_product, state: 'subscribed') }
        end

        it 'skip support product record update' do
          Zendesk::Accounts::Client.any_instance.expects(:update_product!).never

          subject.activate_support_trial!(shell_account)
        end
      end

      describe 'and Support product has not started' do
        let(:support_product_response) do
          { product: FactoryBot.build(:support_trial_product, state: 'not_started') }
        end

        it 'updates support product and sets default trial attributes' do
          Zendesk::Accounts::Client.any_instance.expects(:update_product!).with('support', expected_product_attributes, context: 'trial_activation')

          subject.activate_support_trial!(shell_account)
        end

        it 'updates support product and sets agent workspace as default' do
          Arturo.enable_feature!(:agent_workspace_for_support_only_trial)
          stub_account_service_missing_product('chat')
          shell_account.settings.expects(:save!).at_least_once
          expected_product_attributes.deep_merge!(product: { plan_settings: { agent_workspace: true }})
          Zendesk::Accounts::Client.any_instance.expects(:update_product!).with('support', expected_product_attributes, context: 'trial_activation')

          subject.activate_support_trial!(shell_account)
        end
      end
    end

    describe 'when failure' do
      describe 'in master db transaction' do
        before do
          Accounts::ShellAccountActivation.any_instance.stubs(:activate_accounts_db_records_in_transaction).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))
        end

        it 'sends failure metrics to datadog' do
          tags = ['result:failure', "account:#{shell_account.id}", 'exception:active_record/record_not_saved']
          statsd_client.expects(:increment).with('support_trial_activation', tags: tags).once
          ZendeskExceptions::Logger.expects(:record)

          assert_raises ActiveRecord::RecordNotSaved do
            subject.activate_support_trial!(shell_account)
          end
        end
      end

      describe 'in shard db transaction' do
        before do
          Accounts::ShellAccountActivation.any_instance.stubs(:activate_shard_db_records_in_transaction).raises(ActiveRecord::RecordNotSaved.new('RecordNotSaved'))
        end

        let(:support_product) do
          Zendesk::SupportAccounts::Product.retrieve(shell_account)
        end

        it 'leaves account with not_started state' do
          assert_raises ActiveRecord::RecordNotSaved do
            subject.activate_support_trial!(shell_account)
          end
          assert_equal :not_started, support_product.send(:state)
        end
      end
    end

    describe 'when the account is an SP&P account' do
      let(:expiry_date) { DateTime.current.to_date }
      let(:support_trial_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_trial_product, state: 'not_started', trial_expires_at: expiry_date)) }
      before do
        stub_account_service_support_product(shell_account, response_body: { product: support_trial_product })
        shell_account.stubs(:spp?).returns(true)
      end

      it 'does not update trial expiry date' do
        Zendesk::Accounts::Client.any_instance.expects(:update_product!).with('support', expected_spp_product_attributes, context: 'trial_activation')
        subject.activate_support_trial!(shell_account)
      end

      it 'updates subscription.trial_expires_on' do
        subject.activate_support_trial!(shell_account)
        assert_equal expiry_date, shell_account.subscription.reload.trial_expires_on
      end
    end
  end

  describe '#create_support_product' do
    let(:trial_account) { accounts(:trial) }
    let(:expected_product_attributes) do
      {
        product: {
          state: :trial,
          plan_settings: {
            plan_type: trial_account.subscription.plan_type,
            max_agents: trial_account.subscription.max_agents,
            light_agents: trial_account.subscription.has_light_agents?,
            insights: :good_data,
            suite: false,
            agent_workspace: false
          },
          trial_expires_at: trial_account.subscription.trial_expires_on.to_datetime.iso8601
        }
      }
    end

    it 'creates support product and sets default trial attributes' do
      Zendesk::Accounts::Client.any_instance.expects(:create_product!).
        with('support', expected_product_attributes, context: "trial_activation").returns(nil)

      subject.create_support_product(trial_account)
    end

    describe 'when new_accounts_use_explore Arturo is ON' do
      before do
        Arturo.enable_feature!(:new_accounts_use_explore)
        expected_product_attributes[:product][:plan_settings][:insights] = :explore
      end

      it 'creates support product and sets insights PlanSetting to explore' do
        Zendesk::Accounts::Client.any_instance.expects(:create_product!).
          with('support', expected_product_attributes, context: "trial_activation").returns(nil)

        subject.create_support_product(trial_account)
      end
    end

    describe 'when the subscription has no trial_expires_on' do
      let(:expected_product_attributes_with_default_trial_expires) do
        {
          product: {
            state: :trial,
            plan_settings: {
              plan_type: trial_account.subscription.plan_type,
              max_agents: trial_account.subscription.max_agents,
              light_agents: trial_account.subscription.has_light_agents?,
              insights: :good_data,
              suite: false,
              agent_workspace: false
            },
            # to_date.to_datetime is what ends up happening in the real code path
            trial_expires_at: Subscription::TrialManagement::TRIAL_DURATION.from_now.to_date.to_datetime.iso8601
          }
        }
      end

      it 'uses the default trial expiry' do
        trial_account.subscription.expects(:trial_expires_on).returns(nil)

        Zendesk::Accounts::Client.any_instance.expects(:create_product!).
          with('support', expected_product_attributes_with_default_trial_expires, context: "trial_activation").returns(nil)

        subject.create_support_product(trial_account)
      end
    end

    describe 'when 500 is raised by the client' do
      it 're-raises the error' do
        Zendesk::Accounts::Client.any_instance.expects(:create_product!).
          with('support', expected_product_attributes, context: "trial_activation").raises(Kragle::InternalServerError)

        assert_raises Kragle::InternalServerError do
          subject.create_support_product(trial_account)
        end
      end
    end

    describe 'when product already exists' do
      it 'completes the job' do
        Zendesk::Accounts::Client.any_instance.stubs(:create_product!).
          with('support', expected_product_attributes, context: "trial_activation").raises(Kragle::Conflict)
        subject.any_instance.expects(:instrument_skip)
        subject.create_support_product(trial_account)
      end
    end
  end

  def setup_legacy_account
    params = {
      account: {
        name: "Wombat Snowboards",
        subdomain: "wombatsnowboards",
        help_desk_size: "Small team"
      },
      owner: {
        name: "Sir Wombat",
        email: "wbsnowboards@tz.com",
        is_verified: "1"
      },
      address: {
        phone: "+1-123-456-7890",
        country_code: "US"
      }
    }
    initializer = Zendesk::Accounts::Initializer.new(params, "", nil)
    initializer.account
  end
end
