require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 6

describe 'ZendeskTicketFieldErrors' do
  fixtures :translation_locales

  describe Zendesk::TicketFieldErrors do
    describe "#add_to_ticket_field" do
      before do
        @field = TicketField.new(title_in_portal: "XXX", title: "YYY")
      end

      let(:subject) { Ticket.new }
      let(:account) { Account.new }

      it "adds to base" do
        subject.add_to_ticket_field_errors(@field, "foo", raw: true)
        assert_equal({base: ["XXX: foo"]}, hashify_errors(subject))
      end

      it "adds title for agents" do
        subject.add_to_ticket_field_errors(@field, "foo", for_agent: true, raw: true)
        assert_equal({base: ["YYY: foo"]}, hashify_errors(subject))
      end

      it "renders liquid if account has it activated" do
        subject.add_to_ticket_field_errors(@field, "foo", current_account: account, raw: true)
        assert_equal({base: ["XXX: foo"]}, hashify_errors(subject))
      end

      it "renders dynamic content if the field title contains it" do
        dc_field = TicketField.new(title_in_portal: "{{dc.foo}}", title: "{{dc.foo}}")
        Account.any_instance.expects(:cms_texts).once.returns(Cms::Text)

        subject.add_to_ticket_field_errors(dc_field, "bar", current_account: account, raw: true)
      end

      it "does nothing if no field is given" do
        subject.add_to_ticket_field_errors(nil, "foo", raw: true)
        assert_equal({}, hashify_errors(subject))
      end

      it "does nothing if no field has no title" do
        subject.add_to_ticket_field_errors(TicketField.new, "foo", raw: true)
        assert_equal({}, hashify_errors(subject))
      end

      it "returns Zendesk::TicketFieldErrors::Error" do
        subject.add_to_ticket_field_errors(@field, :invalid, current_account: account)
        assert_equal(subject.errors.get(:base).length, 1)
        error = subject.errors.get(:base).first
        assert_instance_of(Zendesk::TicketFieldErrors::Error, error)
      end

      describe "multilingual fields" do
        let(:account) { accounts(:minimum) }
        let(:description_string) { "txt.default.fields.description.title" }
        let(:description) { FieldDescription.new(title: "foo", title_in_portal: "foo") }
        let(:user_locale) { translation_locales(:french) }

        before do
          I18n.expects(:translation_locale).twice.returns(user_locale)
          I18n.expects(:t).with(description_string, locale: account.translation_locale).returns("foo")
          I18n.expects(:t).with(description_string, locale: user_locale).returns("translated value")
          I18n.expects(:t).with("activerecord.errors.messages.blank").returns("error")
        end

        it "translates the field title_in_portal for end-users" do
          subject.add_to_ticket_field_errors(
            description,
            :blank,
            current_account: account,
            current_user: User.new
          )
          assert_equal({base: ["translated value: error"]}, hashify_errors(subject))
        end

        it "translates the field title for agents" do
          subject.add_to_ticket_field_errors(
            description,
            :blank,
            for_agent: true,
            current_account: account,
            current_user: User.new
          )
          assert_equal({base: ["translated value: error"]}, hashify_errors(subject))
        end
      end
    end

    describe Zendesk::TicketFieldErrors::Error do
      describe "#detail" do
        it "returns the correct field key for a system field" do
          field = FieldSubject.new
          err = Zendesk::TicketFieldErrors::Error.new(field, :invalid)
          assert_equal({field_key: "subject"}, err.details)
        end

        it "returns the correct field key for a custom field" do
          field = TicketField.new
          field.id = 42
          err = Zendesk::TicketFieldErrors::Error.new(field, :invalid)
          assert_equal({field_key: 42}, err.details)
        end
      end
    end
  end

  def hashify_errors(object)
    object.errors.to_hash
  end
end
