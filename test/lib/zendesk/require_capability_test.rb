require_relative "../../support/test_helper"

SingleCov.covered!

class RequireCapabilityTestController < ApplicationController
  require_capability :ticket_form_default, only: [:index]

  def index
    head 200
  end

  def not_restricted
    head 200
  end
end

class RequireCapabilityTest < ActionController::TestCase
  tests RequireCapabilityTestController
  use_test_routes
  fixtures :all

  let(:capability) { :ticket_form_default }

  before do
    @account = @request.account = accounts(:minimum)
    login(:minimum_agent)
  end

  describe "With an account capability enabled" do
    before do
      Arturo.enable_feature!(capability)
      assert @account.send("has_#{capability}?")
    end

    it "allows access to the action" do
      get :index
      assert_response :success, @response.body
    end
  end

  describe "With an account capability disabled" do
    before do
      Arturo.disable_feature!(capability)
      refute @account.send("has_#{capability}?")
    end

    it "does not allow access to the action" do
      get :index
      assert_response :forbidden, @response.body
    end

    it "allows access to non-blacklisted actions" do
      get :not_restricted
      assert_response :success, @response.body
    end
  end
end
