require_relative '../../support/test_helper'

SingleCov.covered!

describe Zendesk::TicketMetric do
  describe '.supported' do
    it 'returns the supported metric names' do
      assert_equal(
        %i[
          agent_work_time
          pausable_update_time
          periodic_update_time
          reply_time
          requester_wait_time
          resolution_time
        ],
        Zendesk::TicketMetric.supported
      )
    end
  end

  describe '.launch?' do
    describe 'when the metric was present at launch' do
      let(:metric) { :requester_wait_time }

      it 'returns true' do
        assert Zendesk::TicketMetric.launch?(metric)
      end
    end

    describe 'when the metric was not present at launch' do
      let(:metric) { :periodic_update_time }

      it 'returns false' do
        refute Zendesk::TicketMetric.launch?(metric)
      end
    end
  end

  describe '.recurring?' do
    describe 'when the metric recurs' do
      let(:metric) { :reply_time }

      it 'returns true' do
        assert Zendesk::TicketMetric.recurring?(metric)
      end
    end

    describe 'when the metric does not recur' do
      let(:metric) { :requester_wait_time }

      it 'returns false' do
        refute Zendesk::TicketMetric.recurring?(metric)
      end
    end
  end
end
