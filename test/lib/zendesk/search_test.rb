require_relative "../../support/test_helper"
require 'zendesk/search'

SingleCov.covered!

describe Zendesk::Search do
  describe ".search" do
    before { @user = users(:minimum_end_user) }

    it "includes the current user's locale" do
      ZendeskSearch::Client.any_instance.expects(:search).
        with("foo", has_entries(locale: @user.translation_locale.locale)).
        returns(count: 0, results: []).
        at_least_once
      Zendesk::Search.search(@user, "foo")
    end
  end
end
