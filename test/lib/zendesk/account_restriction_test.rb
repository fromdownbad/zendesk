require_relative "../../support/test_helper"

SingleCov.covered!

class AccountRestrictionTestController < ActionController::Base
  include Zendesk::AccountRestriction

  def index
    User.find(params[:user_id])
    head :ok
  end

  private

  def current_account
    Account.find(params[:account_id])
  end
end

class AccountRestrictionTest < ActionController::TestCase
  tests AccountRestrictionTestController
  use_test_routes

  it "raises AccountRestrictionError when models from another account are loaded" do
    account = accounts(:support)
    user = users(:minimum_end_user)

    assert_raise AccountRestrictionError do
      get :index, params: { account_id: account.id, user_id: user.id }
    end
  end

  it "does not raise AccountRestrictionError when models from the current account are loaded" do
    account = accounts(:minimum)
    user = users(:minimum_end_user)

    get :index, params: { account_id: account.id, user_id: user.id.to_s }
  end
end
