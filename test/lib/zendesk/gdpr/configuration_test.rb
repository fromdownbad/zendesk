require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Gdpr::Configuration do
  describe '.setup' do
    it 'yields and saves the result to settings' do
      settings = { 'foo' => 'bar' }

      Zendesk::Gdpr::Configuration.setup do
        settings
      end

      assert_equal settings, Zendesk::Gdpr::Configuration.settings
    end
  end

  describe '.config_file' do
    it 'is set to gdpr.yml' do
      assert_equal 'gdpr.yml', Zendesk::Gdpr::Configuration.config_file
    end
  end
end
