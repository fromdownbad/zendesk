require_relative "../../support/test_helper"

SingleCov.covered!

describe 'CommentPublicity' do
  fixtures :accounts, :users, :tickets

  before do
    @ticket = tickets(:minimum_1)
    @via_id = ViaType.WEB_SERVICE
  end

  describe "#is_comment_public?" do
    describe "for anonymous user" do
      it "sets comment to public" do
        user = accounts(:minimum).anonymous_user
        is_public_param = false
        assert(Zendesk::CommentPublicity.is_comment_public?(user, is_public_param, @ticket, @via_id))
      end
    end

    describe "for an end-user" do
      it "sets comment to public" do
        user = users(:minimum_end_user)
        is_public_param = false
        assert(Zendesk::CommentPublicity.is_comment_public?(user, is_public_param, @ticket, @via_id))
      end
    end

    describe "for an admin" do
      before { @user = users(:minimum_admin) }

      it "sets the publicity the agent specified" do
        is_public_param = false
        assert_equal false, Zendesk::CommentPublicity.is_comment_public?(@user, is_public_param, @ticket, @via_id)
      end

      it "user cannot publicly comment so sets comment to private" do
        @user.stubs(:can?).with(:publicly, Comment).returns(false)
        is_public_param = false
        assert_equal false, Zendesk::CommentPublicity.is_comment_public?(@user, is_public_param, @ticket, @via_id)
      end

      describe "when the ticket is via MAIL and is_public was not specified" do
        before { @is_public_param = nil }

        it "sets comment to private if the ticket has an entry" do
          @ticket.entry = Entry.new
          assert_equal false, Zendesk::CommentPublicity.is_comment_public?(@user, @is_public_param, @ticket, ViaType.MAIL)
        end

        it "sets comment to public if the account has is_email_comment_public_by_default setting is true" do
          @ticket.account.stubs(:is_email_comment_public_by_default).returns(true)
          assert(Zendesk::CommentPublicity.is_comment_public?(@user, @is_public_param, @ticket, ViaType.MAIL))
        end

        it "sets comment to private if the account has is_email_comment_public_by_default setting is false" do
          @ticket.account.stubs(:is_email_comment_public_by_default).returns(false)
          assert_equal false, Zendesk::CommentPublicity.is_comment_public?(@user, @is_public_param, @ticket, ViaType.MAIL)
        end
      end

      describe "and ticket is not public" do
        before { Ticket.any_instance.stubs(:is_public).returns(false) }

        describe "and comment privacy was not specified" do
          before { @is_public_param = nil }

          it "sets comment to private if comment was made via the api" do
            assert_equal false, Zendesk::CommentPublicity.is_comment_public?(@user, @is_public_param, @ticket, @via_id)
          end

          it "sets comment to public if comment was NOT made the api or mail" do
            assert(Zendesk::CommentPublicity.is_comment_public?(@user, @is_public_param, @ticket, ViaType.WEB_FORM))
          end

          describe "and the ticket is via MAIL" do
            it "sets comment to private even if is_email_comment_public_by_default setting is true" do
              @ticket.account.stubs(:is_email_comment_public_by_default).returns(true)
              is_public_param = nil
              assert_equal false, Zendesk::CommentPublicity.is_comment_public?(@user, is_public_param, @ticket, ViaType.MAIL)
            end
          end
        end

        it "sets comment to privacy specified by the user" do
          is_public_param = true
          assert(Zendesk::CommentPublicity.is_comment_public?(@user, is_public_param, @ticket, @via_id))
        end
      end
    end
  end

  describe "#merge_comment_publicity_default" do
    before do
      @public_source = tickets(:minimum_2)
      @public_target = tickets(:minimum_3)
      create_private_merge_tickets
    end

    it "returns true if the target ticket is public and all source tickets are public" do
      assert(Zendesk::CommentPublicity.merge_comment_publicity_default(@public_target, [@public_source, @ticket]))
    end

    it "returns false if the target ticket is private" do
      assert_equal false, Zendesk::CommentPublicity.merge_comment_publicity_default(@private_target, [@public_source, @ticket])
    end

    it "returns false if any of the source tickets are private" do
      assert_equal false, Zendesk::CommentPublicity.merge_comment_publicity_default(@public_target, [@private_source, @public_source])
    end
  end

  describe "#last_comment_made_private" do
    before do
      @one_comment_ticket = tickets(:minimum_2)
      @last_comment = @one_comment_ticket.public_comments.last
      @agent = users(:minimum_agent)
    end

    it "returns false if the ticket has no audit" do
      assert_equal false, Zendesk::CommentPublicity.last_public_comment_made_private?(@ticket)
    end

    it "returns false if there are no events present in the audit" do
      @ticket.will_be_saved_by(@agent)
      assert_equal false, Zendesk::CommentPublicity.last_public_comment_made_private?(@ticket)
    end

    it "returns false if the most recent event was not a CommentPrivacyChange event" do
      @ticket.will_be_saved_by(@agent)
      event = Create.new(
        audit: @ticket.audit,
        ticket: @ticket
      )
      @ticket.audit.events << event

      assert_equal "Create", @ticket.audit.events.last.type
      assert_equal false, Zendesk::CommentPublicity.last_public_comment_made_private?(@ticket)
    end

    describe "and the last audit event was a CommentPrivacyChange event" do
      it "returns false if there is more than one public comment on the ticket" do
        @ticket.will_be_saved_by(@agent)
        last_comment = @ticket.public_comments.last
        event = CommentPrivacyChange.new(
          audit: @ticket.audit,
          ticket: @ticket,
          event_id: last_comment.id,
          value: false,
          value_previous: true
        )
        @ticket.audit.events << event
        @ticket.audit.save!
        @ticket.reload

        assert_equal "CommentPrivacyChange", @ticket.audit.events.last.type
        assert(@ticket.public_comments.count > 1)
        assert_equal false, Zendesk::CommentPublicity.last_public_comment_made_private?(@ticket)
      end

      it "returns false and there is only one public comment on the ticket but it is not the one that was changed" do
        @one_comment_ticket.will_be_saved_by(@agent)
        event = CommentPrivacyChange.new(
          audit: @one_comment_ticket.audit,
          ticket: @one_comment_ticket,
          event_id: 17,
          value: false,
          value_previous: true
        )
        @one_comment_ticket.audit.events << event
        @one_comment_ticket.audit.save(validate: false)
        @one_comment_ticket.reload

        assert_equal "CommentPrivacyChange", @one_comment_ticket.audit.events.last.type
        assert_equal 1, @one_comment_ticket.public_comments.count
        assert_not_equal @one_comment_ticket.public_comments.last.id, @one_comment_ticket.audit.events.last.value_reference
        assert_equal false, Zendesk::CommentPublicity.last_public_comment_made_private?(@one_comment_ticket)
      end

      it "returns true if there is only one public comment on the ticket and it is the one that was changed" do
        @one_comment_ticket.will_be_saved_by(@agent)
        event = CommentPrivacyChange.new(
          audit: @one_comment_ticket.audit,
          ticket: @one_comment_ticket,
          event_id: @last_comment.id,
          value: false,
          value_previous: true
        )
        @one_comment_ticket.audit.events << event
        @one_comment_ticket.audit.save!
        @one_comment_ticket.reload

        assert_equal "CommentPrivacyChange", @one_comment_ticket.audit.events.last.type
        assert_equal 1, @one_comment_ticket.public_comments.count
        assert_equal @one_comment_ticket.public_comments.last.id.to_s, @one_comment_ticket.audit.events.last.value_reference.to_s
        assert(Zendesk::CommentPublicity.last_public_comment_made_private?(@one_comment_ticket))
      end
    end
  end

  def create_private_merge_tickets
    @private_source = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: {
      comment: { public: false, value: "Private source wombat ticket" }
      }).ticket
    @private_source.save!
    @private_target = Zendesk::Tickets::Initializer.new(accounts(:minimum), users(:minimum_admin), ticket: {
      comment: { public: false, value: "Private target wombat ticket" }
      }).ticket
    @private_target.save!
    @private_target.reload
    @private_source.reload
    assert_equal false, @private_source.is_public
    assert_equal false, @private_target.is_public
  end
end
