require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::PIDEqualizer do
  let(:subject) do
    Zendesk::PIDEqualizer.new(
      p_arturo: :aurora_database_backoff_trx_sensor_p,
      i_arturo: :aurora_database_backoff_trx_sensor_i,
      d_arturo: :aurora_database_backoff_trx_sensor_d
    )
  end

  describe "PIDEqualizer" do
    describe "returns zero gains when arturos enabled and set to zero" do
      before do
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 0, symbol: "aurora_database_backoff_trx_sensor_p")
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 0, symbol: "aurora_database_backoff_trx_sensor_i")
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 0, symbol: "aurora_database_backoff_trx_sensor_d")
      end

      it 'gains equal 0 when deployment_precentage is 0' do
        subject.p_gain.must_equal(0.0)
        subject.i_gain.must_equal(0.0)
        subject.d_gain.must_equal(0.0)
      end
    end

    describe "returns zero gains when arturos disabled" do
      before do
        Arturo::Feature.create!(phase: "off", symbol: "aurora_database_backoff_trx_sensor_p")
        Arturo::Feature.create!(phase: "off", symbol: "aurora_database_backoff_trx_sensor_i")
        Arturo::Feature.create!(phase: "off", symbol: "aurora_database_backoff_trx_sensor_d")
      end

      it 'gains equal 0 when deployment_precentage is 0' do
        subject.p_gain.must_equal(0.0)
        subject.i_gain.must_equal(0.0)
        subject.d_gain.must_equal(0.0)
      end
    end

    describe "returns zero gains when arturos are not defined" do
      it 'gains equal 0 when deployment_precentage is 0' do
        subject.p_gain.must_equal(0.0)
        subject.i_gain.must_equal(0.0)
        subject.d_gain.must_equal(0.0)
      end
    end

    describe "returns zero gains when arturos enabled and percentage is not set" do
      before do
        Arturo::Feature.create!(phase: "on", symbol: "aurora_database_backoff_trx_sensor_p")
        Arturo::Feature.create!(phase: "on", symbol: "aurora_database_backoff_trx_sensor_i")
        Arturo::Feature.create!(phase: "on", symbol: "aurora_database_backoff_trx_sensor_d")
      end

      it 'gains equal 1.0 when phase is on' do
        subject.p_gain.must_equal(1.0)
        subject.i_gain.must_equal(1.0)
        subject.d_gain.must_equal(1.0)
      end
    end

    describe "returns float gains when arturos enabled and higher than zero" do
      before do
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 50, symbol: "aurora_database_backoff_trx_sensor_p")
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 30, symbol: "aurora_database_backoff_trx_sensor_i")
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 20, symbol: "aurora_database_backoff_trx_sensor_d")
      end

      it 'gains equal 0.5,0.3,0.2 when deployment_precentage is 50,30,20' do
        subject.p_gain.must_equal(0.5)
        subject.i_gain.must_equal(0.3)
        subject.d_gain.must_equal(0.2)
      end
    end
  end
end
