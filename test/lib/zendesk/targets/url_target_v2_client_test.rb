require_relative "../../../support/test_helper"
require "zendesk/targets/url_target_v2_client"
require "faraday_middleware/response/follow_redirects"

SingleCov.covered!

describe Zendesk::UrlTargetV2Client do
  fixtures :accounts, :tickets, :users, :events, :translation_locales

  let(:event) { events(:create_external_for_minimum_ticket_1) }
  let(:ticket) { event.ticket }
  let(:author) { event.author }
  let(:account) { event.account }
  let(:target) { UrlTargetV2.new(url: "https://wibble.org/a/b", method: "get", account: account) }
  let(:client) { Zendesk::UrlTargetV2Client.new(target: target, event: event, message: [["ping", "pong"]]) }

  describe "#full_url" do
    describe "for get requests" do
      before { target.method = "get" }

      describe "when there are url params" do
        before { target.url = "https://wibble.org/a/b?this=that" }

        it "returns the url with the url and message parameters" do
          assert_equal "https://wibble.org/a/b?this=that&ping=pong", client.send(:full_url)
        end
      end

      describe "when there are no message_params parameters" do
        it "returns the url with the message parameters" do
          assert_equal "https://wibble.org/a/b?ping=pong", client.send(:full_url)
        end
      end

      describe "when there are neither url nor message parameters" do
        before { client.stubs(message: nil) }

        it "returns the url without a query" do
          assert_equal "https://wibble.org/a/b", client.send(:full_url)
        end
      end
    end

    describe "for post put and patch requests" do
      before { target.method = "post" }

      describe "when there are url params" do
        before { target.url = "https://wibble.org/a/b?this=that" }

        it "returns the url with the url parameters" do
          assert_equal "https://wibble.org/a/b?this=that", client.send(:full_url)
        end
      end

      describe "when there is liquid in the url" do
        before do
          target.url = "https://wibble.org/a/{{ticket.requester.id}}/b?this=that"
          client.stubs(message: "{{ticket.requester.id}}")
        end

        it "returns the url with liquid rendered" do
          assert_equal "https://wibble.org/a/#{ticket.requester.id}/b?this=that", client.send(:full_url)
        end
      end
    end

    describe "with liquid markup in the url" do
      before { target.url = "https://wibble.org/a/{{ticket.id}}" }

      it "returns the rendered url" do
        assert_equal "https://wibble.org/a/#{ticket.nice_id}?ping=pong", client.send(:full_url)
      end
    end
  end

  describe "#locale" do
    let(:locale) { translation_locales(:spanish) }
    let(:account_locale) { translation_locales(:norwegian) }

    before do
      target.method = "get"
      ticket.account.stubs(translation_locale: account_locale)
    end

    it "returns the account locale if no locale parameter is sent" do
      assert_equal account_locale, client.send(:locale)
    end
  end

  describe "#invoke" do
    describe "redirection" do
      describe "when there are 3 or fewer redirects" do
        before do
          stub_request(:get,    client.send(:full_url))        .to_return(status: 301, headers: { location: "#{client.send(:full_url)}&one" })
          stub_request(:get, "#{client.send(:full_url)}&one")  .to_return(status: 301, headers: { location: "#{client.send(:full_url)}&two" })
          stub_request(:get, "#{client.send(:full_url)}&two")  .to_return(status: 301, headers: { location: "#{client.send(:full_url)}&three" })
          stub_request(:get, "#{client.send(:full_url)}&three").to_return(status: 200)
        end

        it "returns a successful response" do
          assert client.invoke.success?
        end
      end

      describe "when there are more than 3 redirects" do
        before do
          stub_request(:get,    client.send(:full_url))        .to_return(status: 301, headers: { location: "#{client.send(:full_url)}&one" })
          stub_request(:get, "#{client.send(:full_url)}&one")  .to_return(status: 301, headers: { location: "#{client.send(:full_url)}&two" })
          stub_request(:get, "#{client.send(:full_url)}&two")  .to_return(status: 301, headers: { location: "#{client.send(:full_url)}&three" })
          stub_request(:get, "#{client.send(:full_url)}&three").to_return(status: 301, headers: { location: "#{client.send(:full_url)}&four" })
          stub_request(:get, "#{client.send(:full_url)}&four") .to_return(status: 200)
        end

        it "raises an exception" do
          assert_raises FaradayMiddleware::RedirectLimitReached do
            client.invoke
          end
        end
      end
    end

    describe "basic auth" do
      before { Faraday::Connection.any_instance.stubs(get: stub) }

      describe "when there is a username and password on the target" do
        before do
          target.username = "agent"
          target.encrypted_password = "123456"
          target.encrypt!
        end

        it "sets the basic auth header" do
          Faraday::Connection.any_instance.expects(:basic_auth).once
          client.invoke
        end
      end

      describe "when there is a username and no password on the target" do
        before do
          target.username = "agent"
          target.encrypted_password = ""
          target.encrypt!
        end

        it "sets the basic auth header" do
          Faraday::Connection.any_instance.expects(:basic_auth).once
          client.invoke
        end
      end

      describe "when there is no username and password on the target" do
        it "doesn't set the basic auth header" do
          Faraday::Connection.any_instance.expects(:basic_auth).never
          client.invoke
        end
      end
    end

    describe "with a trailing / in url" do
      before do
        target.url = "https://wibble.org/a/b/"
        stub_request(:get, "https://wibble.org/a/b/?ping=pong").with(
          headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'User-Agent' => "Zendesk Target"
          }
        ).to_return(status: 200, body: "", headers: {})
      end

      it "uses the the trailing / in the made request" do
        # would throw WebMock::NetConnectNotAllowedError if the stub failed to match (i.e. faraday were using no trailing slash)
        client.invoke
      end
    end

    describe "content-type" do
      describe "for put patch and post requests" do
        before { target.method = "post" }

        describe "when the content-type is x-www-form-urlencoded" do
          before do
            target.content_type = "application/x-www-form-urlencoded"
            stub_request(:post, client.send(:full_url)).with(
              body: client.send(:body),
              headers: {
                'Accept' => '*/*',
                'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Content-Type' => "#{target.content_type}; charset=utf-8",
                'User-Agent' => "Zendesk Target"
              }
            ).to_return(status: 200, body: "", headers: {})
          end

          it "uses a content-type header and use client#query as the body" do
            assert_equal "#{target.content_type}; charset=utf-8", client.invoke.env[:request_headers]["content-type"]
          end
        end

        describe "when the content-type is not x-www-form-urlencoded" do
          before do
            target.content_type = "application/json"
            stub_request(:post, client.send(:full_url)).with(
              body: client.message.to_s,
              headers: {
                'Accept' => '*/*',
                'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                'Content-Type' => "#{target.content_type}; charset=utf-8",
                'User-Agent' => "Zendesk Target"
              }
            ).to_return(status: 200, body: "", headers: {})
          end

          it "uses a content-type header and use message as the body" do
            assert_equal "#{target.content_type}; charset=utf-8", client.invoke.env[:request_headers]["content-type"]
          end
        end
      end

      describe "for GET requests" do
        before do
          stub_request(:get, client.send(:full_url)).with(
            headers: {
              'Accept' => '*/*',
              'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
              'User-Agent' => "Zendesk Target"
            }
          ).to_return(status: 200, body: "", headers: {})
        end

        it "doesn't use a content-type" do
          assert_nil client.invoke.env[:request_headers]["content-type"]
        end
      end

      describe "logging" do
        before(:each) do
          target.method = "post"
          stub_request(:post, client.send(:full_url)).with(
            body: client.send(:body),
            headers: {
              'Accept' => '*/*',
              'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
              'Content-Type' => "#{target.content_type}; charset=utf-8",
              'User-Agent' => "Zendesk Target"
            }
          ).to_return(status: 200, body: "", headers: {})
        end

        describe "with `has_log_targets_v2_requests` arturo enabled" do
          before { Account.any_instance.stubs(:has_log_targets_v2_requests?).returns(true) }

          it "logs basic information about the request" do
            log_message = "target request created, { "\
              "target_id: #{target.id}, "\
              "method: #{target.method}, "\
              "url: #{client.send(:full_url)}, "\
              "content_type: #{target.content_type}; charset=utf-8, "\
              "body: #{client.send(:body)}}"

            Rails.logger.expects(:info).with(log_message).once
            client.invoke
          end
        end

        describe "with `has_log_targets_v2_requests` arturo disabled" do
          before { Account.any_instance.stubs(:has_log_targets_v2_requests?).returns(false) }

          it "does not log anything" do
            Rails.logger.expects(:info).never
            client.invoke
          end
        end
      end
    end

    describe "internal address" do
      let(:target) { UrlTargetV2.new(url: "http://192.168.42.45", method: "get", account: account) }

      it "raises AddressNotAllowed for non development environment" do
        assert_raise Faraday::RestrictIPAddresses::AddressNotAllowed do
          client.invoke
        end
      end

      it "allows internal address for development environment" do
        stub_request(:get, "http://192.168.42.45/?ping=pong")
        Rails.env.stubs(:development?).returns(true)
        client.invoke
      end
    end
  end

  describe "#url_query_with_message_params" do
    it "returns url encoded parameters" do
      client.stubs(message: [["\"\\'@%&=+', ", "f:g/as*[]d"]])
      assert_equal "%22%5C%27%40%25%26%3D%2B%27%2C+=f%3Ag%2Fas*%5B%5Dd", client.send(:url_query_with_message_params)
    end

    describe "when there are message url parameters" do
      before do
        target.url = "https://wibble.org/a/b?a=b&b=c"
        client.stubs(message: [["ping", "pong"]])
      end

      it "appends the message parameters to the url parameters" do
        assert_equal "a=b&b=c&ping=pong", client.send(:url_query_with_message_params)
      end

      describe "when there is an empty message" do
        before { client.stubs(message: []) }

        it "returns the url parameters" do
          assert_equal "a=b&b=c", client.send(:url_query_with_message_params)
        end
      end
    end

    describe "when there are no parameters" do
      before { target.url = "https://wibble.org/a/b" }

      it "returns the message parameters" do
        assert_equal "ping=pong", client.send(:url_query_with_message_params)
      end

      describe "when there is an empty message" do
        before { client.stubs(message: []) }

        it "returns an empty string" do
          assert_equal "", client.send(:url_query_with_message_params)
        end
      end
    end
  end

  describe "statsd client configuration" do
    it "should not create statsd client when it exists already" do
      statsd_client = stub_for_statsd
      client.stubs(:statsd_client).returns(statsd_client)
      assert_equal statsd_client, client.statsd_client
    end

    it "should create a new statsd client when it does not exist" do
      assert_kind_of Zendesk::StatsD::Client, client.statsd_client
    end
  end

  describe "circuit breaker middleware configuration" do
    it "client contains proper circuit breaker options" do
      expect_option = {
        name: 'circuit-breaker-url-target-v2-1',
        failure_threshold: 30,
        reset_timeout: 5,
        store: JohnnyFive::MemoryStore.instance
      }
      target.stub :id, 1 do
        Rails.cache.stub :try, nil, :dalli do
          assert_equal expect_option, client.circuit_options
        end
      end
    end
  end

  describe "target invocation with circuit breaker middleware" do
    before do
      @statsd_client = stub_for_statsd
      @class_statsd_client ||= stub_for_statsd
      Zendesk::UrlTargetV2Client.any_instance.stubs(:statsd_client).returns(@statsd_client)
      Zendesk::UrlTargetV2Client.class_variable_set(:@@statsd_client, @class_statsd_client)

      @client = Zendesk::UrlTargetV2Client.new(target: target, event: event, message: [["ping", "pong"]])
      @opts = {store: JohnnyFive::MemoryStore.send(:new), failure_threshold: 1, reset_timeout: 1000, name: 'circuit-breaker-url-target-v2-target-1'}
      stub_request(:get, @client.send(:full_url)).to_return(status: 400, headers: {})
      target.stubs(:account).returns(account)
    end

    describe "should be circuit breaking with feature url_target_v2_circuit_breaker enabled" do
      describe_with_arturo_enabled :url_target_v2_circuit_breaker do
        it "raise circuit breaker exception" do
          @client.stub :circuit_options, @opts do
            # 1st invoke
            refute @client.invoke.success?
            # 2nd invoke
            assert_raises JohnnyFive::CircuitTrippedError do
              @statsd_client.expects(:increment).with(:circuit_tripped, tags: %w[source:url_target_v2]).once
              @class_statsd_client.expects(:increment).with(:soft_circuit_tripped, tags: %w[source:url_target_v2]).never
              @client.invoke
            end
          end
        end
      end
    end

    describe "should not be circuit breaking with feature url_target_v2_circuit_breaker disabled" do
      describe_with_arturo_disabled :url_target_v2_circuit_breaker do
        it "target invocation is failed with error response" do
          @client.stub :circuit_options, @opts do
            # expect soft circuit tripped be invoked
            @statsd_client.expects(:increment).with(:circuit_tripped, tags: %w[source:url_target_v2]).never

            # we get two here because johnny five will report the metrics at time
            # when circuit is on but no further request comes in
            @class_statsd_client.expects(:increment).with(:soft_circuit_tripped, tags: %w[source:url_target_v2]).twice
            # 1st invoke
            refute @client.invoke.success?
            # 2nd invoke
            refute @client.invoke.success?
          end
        end
      end
    end
  end

  describe "#body" do
    describe "when the content-type is form encoded" do
      before { target.content_type = UrlTargetV2::CONTENT_TYPES["Form encoded"] }

      describe "when the message is an Array" do
        describe "when the message is empty" do
          before { client.stubs(message: []) }

          it "returns an empty string" do
            assert_equal "[]", client.send(:body)
          end
        end

        describe "when the message is not empty" do
          before { client.stubs(message: [["ping", "pong"]]) }

          it "returns the message as parameters" do
            assert_equal "ping=pong", client.send(:body)
          end
        end
      end

      describe "when the message is nil" do
        before { client.stubs(message: nil) }

        it "returns an empty string" do
          assert_equal "", client.send(:body)
        end
      end
    end

    describe "when the content-type is not form encoded" do
      before do
        target.content_type = UrlTargetV2::CONTENT_TYPES["JSON"]
        client.stubs(message: '{"ping":"pong"}')
      end

      it "returns the message" do
        assert_equal '{"ping":"pong"}', client.send(:body)
      end
    end
  end

  describe "adding safe_update to /api/v2/ticket targets" do
    let(:another_ticket) { ticket(:minimum_2) }

    before do
      Timecop.freeze
      Account.any_instance.stubs(:has_ticket_target_retry?).returns(true)
      event.account.stubs(:url).returns("https://dev.#{Zendesk::Net.config.zendesk_base_domain}")

      target.content_type = "application/json"
      target.url = "https://dev.#{Zendesk::Net.config.zendesk_base_domain}/api/v2/tickets/{{ticket.id}}.json"
      target.method = "put"
      target.save
      @message = "{\"ticket\":{\"status\":\"open\"}"
      @updated_message = "{\"ticket\":{\"status\":\"open\",\"safe_update\":true,\"updated_stamp\":\"#{ticket.updated_at.iso8601}\"}}"
    end

    describe "should update the ticket" do
      def verify_message
        @client = Zendesk::UrlTargetV2Client.new(target: target, event: event, message: @message)
        assert_equal @updated_message, @client.message
      end

      it "adds safe_update and updated_stamp" do
        @message = "{\"ticket\":{\"status\":\"open\"}"
        verify_message
      end

      it "adds safe_update and updated_stamp for the correct ticket even url does not contain extension" do
        target.url = "https://dev.#{Zendesk::Net.config.zendesk_base_domain}/api/v2/tickets/{{ticket.id}}"
        @message = "{\"ticket\":{\"status\":\"open\"}"
        verify_message
      end

      it "doesn't conflict with messages that already have an updated_stamp" do
        @message = "{\"ticket\":{\"status\":\"open\",\"safe_update\":true,\"updated_stamp\":\"#{(ticket.updated_at - 1.hours).iso8601}\"}}"
        verify_message
      end

      it "adds the correct updated_stamp" do
        another_ticket = tickets(:minimum_2)
        another_ticket.update_column(:updated_at, 2.minutes.ago)
        target.url = "https://dev.#{Zendesk::Net.config.zendesk_base_domain}/api/v2/tickets/#{another_ticket.nice_id}.json"
        @message = "{\"ticket\":{\"status\":\"open\"}"
        @updated_message = "{\"ticket\":{\"status\":\"open\",\"safe_update\":true,\"updated_stamp\":\"#{another_ticket.updated_at.iso8601}\"}}"
        verify_message
      end
    end

    describe "should leave the message alone" do
      after do
        @client = Zendesk::UrlTargetV2Client.new(target: target, event: event, message: @message)
        assert_equal @message, @client.message
      end

      it "only modifies the body when arturo ticket_target_retry is enabled" do
        Account.any_instance.stubs(:has_ticket_target_retry?).returns(false)
      end

      it "only modifies the body for tickets endpoint" do
        target.url = "https://dev.#{Zendesk::Net.config.zendesk_base_domain}/api/v2/users/{{requester.id}}.json"
      end

      it "does not modify endpoints nested under tickets" do
        target.url = "https://dev.#{Zendesk::Net.config.zendesk_base_domain}/api/v2/tickets/{{ticket.id}}/tags.json"
      end

      it "does not modify requests to other zendesk instances" do
        target.url = "https://example.#{Zendesk::Net.config.zendesk_base_domain}/api/v2/tickets/{{ticket.id}}.json"
      end

      it "doesn't conflict with messages that don't match the expected format" do
        @message = "[\"hello\",\"world\"]"
      end

      it "only modifies the body for zendesk" do
        target.url = "https://api.example.com/api/v2/users/{{requester.id}}.json"
      end

      it "only modifies the body for ticket updates" do
        target.method = "post"
      end
    end
  end
end
