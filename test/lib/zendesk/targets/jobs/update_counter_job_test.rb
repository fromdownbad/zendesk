require_relative "../../../../support/job_helper"

SingleCov.covered!

describe 'UpdateCounterJob' do
  fixtures :accounts, :targets
  let(:account) { accounts(:minimum) }
  let(:job) { Zendesk::Targets::Jobs::UpdateCounterJob }
  let(:target) { targets(:url_v2) }
  let(:targets_to_update) { Set.new }
  include PerformJob

  describe "Schedule" do
    it "runs once every 5 mins" do
      evaluated = ERB.new(File.read(Rails.root + 'config/resque-schedule.yml')).result
      resque_schedule = YAML.load(evaluated)
      assert_equal '*/5 * * * *', resque_schedule[job.name]["cron"]
      assert_equal "maintenance", resque_schedule[job.name]["queue"]
    end
  end

  describe "work" do
    describe "success" do
      it "the counter is updated accordingly" do
        Rails.cache.write(Target.cached_success_count_key(target.id), 4, raw: true)
        job.work
        target.reload
        assert_equal 4, target.sent
        assert_equal 0, target.failures
      end
    end

    describe "failure" do
      before do
        @target = UrlTargetV2.new(title: "Bulk Update Test Target", account: account, url: "http://www.example.com/a", method: "get")
      end

      it "the counter is updated accordingly with only failure in cache" do
        @target.sent = 8
        @target.failures = 5
        @target.error = ''
        @target.save

        Rails.cache.write("targets_to_update_shard#{account.shard_id}", [@target.id])
        Rails.cache.write(Target.cached_success_count_key(@target.id), 0, raw: true)
        Rails.cache.write(Target.cached_failure_count_key(@target.id), 5, raw: true)
        Rails.cache.write(Target.cached_exception_message_key(@target.id), "error msg")

        job.work
        @target.reload

        assert_equal 8, @target.sent
        assert_equal 10, @target.failures
        assert_equal "error msg", @target.error
      end

      it "the counter is updated accordingly with both success and failure in cache" do
        @target.sent = 8
        @target.failures = 5
        @target.error = ''
        @target.save

        Rails.cache.write("targets_to_update_shard#{account.shard_id}", [@target.id])
        Rails.cache.write(Target.cached_success_count_key(@target.id), 4, raw: true)
        Rails.cache.write(Target.cached_failure_count_key(@target.id), 1, raw: true)
        Rails.cache.write(Target.cached_exception_message_key(@target.id), "error msg")

        job.work
        @target.reload

        assert_equal 12, @target.sent
        assert_equal 1, @target.failures
        assert_equal "error msg", @target.error
      end
    end

    describe "Cache status" do
      it "resets the cache after updating the target" do
        Rails.cache.write(Target.cached_success_count_key(target.id), 4, raw: true)
        Rails.cache.write(Target.cached_failure_count_key(target.id), 4, raw: true)
        job.work
        assert_equal 0, Rails.cache.read(Target.cached_success_count_key(target.id))
        assert_equal 0, Rails.cache.read(Target.cached_failure_count_key(target.id))
        assert_nil Rails.cache.read(Target.cached_exception_message_key(target.id))
      end
    end

    describe "standard error" do
      it "standard error does not try to increase the failure count" do
        Rails.cache.write(Target.cached_failure_count_key(target.id), 2, raw: true)
        original_failures = target.failures
        error = StandardError.new 'generic error'
        Target.any_instance.expects(:save).raises(error)
        expect_time = '2020-06-11 01:02:33 +0000'
        expect_job_name = 'UpdateCounterJob'
        job.expects(:resque_log).with(
          [
            'exception',
            job: expect_job_name,
            time: expect_time,
            error_class: 'StandardError',
            error_message: 'generic error',
            target_id: target.id,
            account_id: target.account_id,
          ]
        ).once

        Zendesk::Targets::Jobs::UpdateCounterJob.stubs(:args_to_log).returns(
          job: expect_job_name,
          time: expect_time
        )

        job.work_on_shard(account.shard_id)

        assert_equal original_failures, target.reload.failures
      end

      it "record not found error does not try to increase the failure count" do
        Rails.cache.write(Target.cached_failure_count_key(target.id), 2, raw: true)
        error = ActiveRecord::RecordNotFound.new 'target not found'
        Target.expects(:find).raises(error)
        expect_time = '2020-06-11 01:02:33 +0000'
        expect_job_name = 'UpdateCounterJob'
        job.expects(:resque_log).with(
          [
            'exception',
            job: expect_job_name,
            time: expect_time,
            error_class: 'ActiveRecord::RecordNotFound',
            error_message: 'target not found',
            target_id: target.id,
          ]
        ).once

        Zendesk::Targets::Jobs::UpdateCounterJob.stubs(:args_to_log).returns(
          job: expect_job_name,
          time: expect_time
        )

        job.work_on_shard(account.shard_id)
      end
    end

    describe "instrument success" do
      it "call the statsd_client method after job executes successfully" do
        Rails.cache.write(Target.cached_success_count_key(target.id), 1, raw: true)
        job.expects(:statsd_client)
        job.work
      end
    end
  end

  describe "args_to_log" do
    it "returns job name and current time" do
      assert_equal({ job: "UpdateCounterJob", time: Time.now.to_s, }, job.args_to_log)
    end
  end

  describe "statsd_client" do
    it "returns job name and current time" do
      job.expects(:statsd_client).returns(Zendesk::StatsD::Client.new(namespace: :classic_target))
      job.statsd_client
    end
  end

  describe "cache read/reset" do
    before do
      Rails.cache.write(Target.cached_success_count_key(target.id), 1, raw: true)
      Rails.cache.write(Target.cached_failure_count_key(target.id), 4, raw: true)
      Rails.cache.write(Target.cached_exception_message_key(target.id), "error msg")
    end

    it "reads the cache and returns the attributes sent, failures, error" do
      cache_value = job.read_target_cache(target.id)
      assert_equal 1, cache_value[:sent]
      assert_equal 4, cache_value[:failures]
      assert_equal "error msg", cache_value[:error]
    end

    it "resets the cache values sent, failures, error" do
      cache_value = job.read_target_cache(target.id)
      job.reset_target_cache(target.id, cache_value)
      assert_equal 0, Rails.cache.read(Target.cached_success_count_key(target.id))
      assert_equal 0, Rails.cache.read(Target.cached_failure_count_key(target.id))
      assert_nil Rails.cache.read(Target.cached_exception_message_key(target.id))
    end
  end
end
