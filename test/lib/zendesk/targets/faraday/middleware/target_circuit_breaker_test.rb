require_relative "../../../../../support/test_helper"

SingleCov.covered! file: 'lib/zendesk/targets/faraday/middleware/target_circuit_breaker.rb'

require 'johnny_five'
require 'zendesk/targets/faraday/middleware/target_circuit_breaker'

describe FaradayMiddleware::TargetCircuitBreaker do
  describe "#initialize" do
    it "should be initialized circuit breaker middleware with correct options" do
      middleware = FaradayMiddleware::TargetCircuitBreaker.new(
        -> { true },
        options: {
          name: 'target-circuit-breaker-1',
          store: JohnnyFive::MemoryStore.instance,
          failure_threshold: 30,
          reset_timeout: 10
        }
      )

      app = middleware.instance_variable_get(:@app)
      assert app.call

      options = middleware.instance_variable_get(:@options)
      assert options[:is_failure].call(Faraday::Error.new('e'))
      refute options[:is_failure].call(StandardError.new)
      assert_equal JohnnyFive::MemoryStore.instance, options[:store]
      assert_equal 'target-circuit-breaker-1', options[:name]
      assert_equal 30, options[:failure_threshold]
      assert_equal 10, options[:reset_timeout]
    end

    it "should contain all 4xx and 5xx error codes as failed response error codes in circuit breaker middleware" do
      assert_equal 400...600, FaradayMiddleware::TargetCircuitBreaker::ERROR_STATUS_CODES
    end
  end

  describe "#call" do
    it "should run lambda with successful response and not trigger circuit breaker" do
      store = JohnnyFive::MemoryStore.send(:new)
      name = "target-circuit-breaker-#{(Time.now.to_f.round(3) * 1000).to_i}"
      middleware = FaradayMiddleware::TargetCircuitBreaker.new(
        -> (env) { env },
        options: {
          name: name,
          failure_threshold: 1,
          reset_timeout: 1000,
          store: store
        }
      )

      env = Faraday::Env.new
      env.stub :status, 200 do
        value = middleware.call(env)

        circuits = store.instance_variable_get(:@circuits)
        state = circuits[name][:state]

        assert_equal env, value
        assert_equal 0, state.failures
      end
    end

    it "should run lambda with failed response and triggers circuit breaker" do
      store = JohnnyFive::MemoryStore.send(:new)
      name = "target-circuit-breaker-#{(Time.now.to_f.round(3) * 1000).to_i}"
      middleware = FaradayMiddleware::TargetCircuitBreaker.new(
        -> (env) { env },
        options: {
          name: name,
          failure_threshold: 1,
          reset_timeout: 1000,
          store: store
        }
      )

      env = Faraday::Env.new
      env.stub :status, 400 do
        # 1st time runs middleware, response should be returned since
        # it's still under circuit breaker failure threshold
        value = middleware.call(env)

        circuits = store.instance_variable_get(:@circuits)
        state = circuits[name][:state]

        assert_equal env, value
        assert_equal 1, state.failures

        # 2nd time runs middleware, this should trigger circuit breaker
        assert_raises JohnnyFive::CircuitTrippedError do
          middleware.call(env)
        end
      end
    end

    it "should run lambda with timeout error and triggers circuit breaker" do
      store = JohnnyFive::MemoryStore.send(:new)
      name = "target-circuit-breaker-#{(Time.now.to_f.round(3) * 1000).to_i}"
      middleware = FaradayMiddleware::TargetCircuitBreaker.new(
        -> (_) { raise Faraday::TimeoutError, "timeout" },
        options: {
          name: name,
          failure_threshold: 1,
          reset_timeout: 1000,
          store: store
        }
      )

      env = Faraday::Env.new
      # 1st time runs middleware, this should not trigger circuit breaker since
      # it's still under circuit breaker failure threshold
      assert_raises Faraday::TimeoutError do
        middleware.call(env)
      end

      circuits = store.instance_variable_get(:@circuits)
      state = circuits[name][:state]

      assert_equal 1, state.failures

      # 2nd time runs middleware, this should trigger circuit breaker
      assert_raises JohnnyFive::CircuitTrippedError do
        middleware.call(env)
      end
    end
  end
end
