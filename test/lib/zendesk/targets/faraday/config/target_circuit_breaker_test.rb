require_relative "../../../../../support/test_helper"

SingleCov.covered! file: 'lib/zendesk/targets/faraday/config/target_circuit_breaker.rb'

require 'johnny_five'
require 'zendesk/targets/faraday/config/target_circuit_breaker'

describe FaradayConfiguration::TargetCircuitBreaker do
  describe "#circuit_store" do
    let(:obj) { Class.new { include FaradayConfiguration::TargetCircuitBreaker }.new }

    it "should create MemoryStore circuit store when dalli client is not available in rails cache" do
      Rails.cache.stub :try, nil, :dalli do
        assert_kind_of JohnnyFive::MemoryStore, obj.circuit_store
      end
    end

    it "should create MemcachedStore with default TTL when dalli client is present in rails cache" do
      Rails.cache.stub :try, {}, :dalli do
        ENV.stubs(:[]).with("TARGET_CIRCUIT_BREAKER_FAILURE_TTL").returns(nil)
        assert_kind_of JohnnyFive::MemcachedStore, obj.circuit_store
        assert_equal 900, obj.circuit_store.instance_variable_get(:@ttl)
      end
    end

    it "should create MemcachedStore with TTL from environment variable when dalli client is present in rails cache" do
      Rails.cache.stub :try, {}, :dalli do
        ENV.stubs(:[]).with("TARGET_CIRCUIT_BREAKER_FAILURE_TTL").returns('50')
        assert_kind_of JohnnyFive::MemcachedStore, obj.circuit_store
        assert_equal 50, obj.circuit_store.instance_variable_get(:@ttl)
      end
    end
  end

  describe "#circuit_options" do
    let(:obj) { Class.new { include FaradayConfiguration::TargetCircuitBreaker }.new }

    it "should create circuit breaker options with default values" do
      expect = {
        name: "circuit-breaker-1",
        store: JohnnyFive::MemoryStore.instance,
        failure_threshold: 30,
        reset_timeout: 5
      }

      Rails.cache.stub :try, nil, :dalli do
        assert_equal expect, obj.circuit_options(name: "circuit-breaker-1")
      end
    end

    it "should create circuit breaker options with option values only" do
      expect = {
        name: "circuit-breaker-1",
        store: JohnnyFive::MemoryStore.instance,
        failure_threshold: 25,
        reset_timeout: 35
      }
      ENV.stubs(:[]).with("TARGET_CIRCUIT_BREAKER_FAILURE_THRESHOLD").returns('5')
      ENV.stubs(:[]).with("TARGET_CIRCUIT_BREAKER_RESET_TIMEOUT").returns('15')
      Rails.cache.stub :try, nil, :dalli do
        assert_equal expect, obj.circuit_options(name: "circuit-breaker-1", failure_threshold: 25, reset_timeout: 35)
      end
    end

    it "should create circuit breaker options with option values and environment variables" do
      expect = {
        name: "circuit-breaker-1",
        store: JohnnyFive::MemoryStore.instance,
        failure_threshold: 5,
        reset_timeout: 15
      }
      ENV.stubs(:[]).with("TARGET_CIRCUIT_BREAKER_FAILURE_THRESHOLD").returns('5')
      ENV.stubs(:[]).with("TARGET_CIRCUIT_BREAKER_RESET_TIMEOUT").returns('15')
      Rails.cache.stub :try, nil, :dalli do
        assert_equal expect, obj.circuit_options(name: "circuit-breaker-1")
      end
    end
  end
end
