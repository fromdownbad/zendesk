require_relative "../../../../../support/test_helper"

SingleCov.covered!

require 'zendesk/targets/faraday/adapter/zendesk_target_http'
require 'raw_net_capture'

describe Faraday::Adapter::ZendeskTargetHttp do
  describe "#net_http_connection" do
    let(:raw_http_capture) { RawHTTPCapture.new }
    let(:zendesk_target_http) { Faraday::Adapter::ZendeskTargetHttp.new(stub, raw_http_capture: raw_http_capture) }

    before do
      Faraday::Adapter::NetHttp.any_instance.stubs(
        initialize: nil,
        net_http_connection: Net::HTTP.new(stub, stub)
      )
    end

    it "sets http's set_debug_output to the raw_http_capture parameter" do
      assert_equal raw_http_capture, zendesk_target_http.net_http_connection(stub).instance_variable_get("@debug_output")
    end
  end
end
