require_relative "../../../../support/test_helper"

SingleCov.covered! file: 'lib/zendesk/targets/metrics/statsd_client.rb'

require 'johnny_five'
require 'zendesk/targets/metrics/statsd_client'

describe TargetsMetrics::StatsDClient do
  let(:obj) { Class.new { include TargetsMetrics::StatsDClient }.new }

  describe "#create_statsd_client" do
    it "should create statsd client with target namespace" do
      client = obj.create_statsd_client
      assert_equal 'zendesk.classic_target', client.instance_variable_get(:@namespace)
    end
  end
end
