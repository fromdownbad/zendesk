require_relative "../../../support/test_helper"
require 'zendesk/targets/url_target_client'

SingleCov.covered! uncovered: 1

describe Zendesk::UrlTargetClient do
  fixtures :accounts, :tickets, :users, :events, :translation_locales, :targets

  before do
    @account = accounts(:minimum)
    @event   = events(:create_external_for_minimum_ticket_1)
    @ticket  = @event.ticket
    @user    = @event.author
    @target  = targets(:url_valid)
  end

  describe "#initialize" do
    it "fails when instantiated without URL or method" do
      begin
        Zendesk::UrlTargetClient.new(method: "get", account: @account, target: @target)
        flunk
      rescue StandardError => e
        assert ["KeyError", "IndexError"].member?(e.class.name)
      end

      begin
        Zendesk::UrlTargetClient.new(url: "http://moo.com/", account: @account, target: @target)
        flunk
      rescue StandardError => e
        assert ["KeyError", "IndexError"].member?(e.class.name)
      end

      assert Zendesk::UrlTargetClient.new(url: "http://moo.com/", method: "get", account: @account, target: @target)
    end
  end

  describe "account in headers" do
    let(:expected_header) { "Zendesk URL Target (#{@ticket.account.subdomain})" }

    describe "if we have a user or ticket" do
      before do
        Resolv::DNS.any_instance.stubs(:getaddress).with('foo.example.com').returns(Resolv::IPv4.create('1.2.3.4'))
        stub_request(:get, "https://foo.example.com").
          with(headers: {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => expected_header}).
          to_return(status: 200, body: "", headers: {})
      end

      it "sets the user agent to the subdomain when a ticket is provided" do
        client = Zendesk::UrlTargetClient.new(url: "https://foo.example.com", method: "get", ticket: @ticket, account: @account, target: @target)
        header = client.invoke.env[:request_headers]["user-agent"]
        assert_equal expected_header, header
      end

      it "sets the user agent to the subdomain when a user is provided" do
        client = Zendesk::UrlTargetClient.new(url: "https://foo.example.com", method: "get", user: @user, account: @account, target: @target)
        header = client.invoke.env[:request_headers]["user-agent"]
        assert_equal expected_header, header
      end
    end

    it "does not blow up without user or ticket" do
      Resolv::DNS.any_instance.stubs(:getaddress).with('foo.example.com').returns(Resolv::IPv4.create('1.2.3.4'))
      stub_request(:get, "https://foo.example.com").
        with(headers: {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => "Zendesk URL Target"}).
        to_return(status: 200, body: "", headers: {})

      client = Zendesk::UrlTargetClient.new(url: "https://foo.example.com", method: "get", account: @account, target: @target)
      header = client.invoke.env[:request_headers]["user-agent"]
      assert_equal "Zendesk URL Target", header
    end
  end

  describe "#base_url" do
    it "returns everything left of the query string delimiter" do
      client = Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b?x=y", method: "get", account: @account, target: @target)
      assert_equal "https://wibble.org/a/b", client.base_url
      client = Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b", method: "get", account: @account, target: @target)
      assert_equal "https://wibble.org/a/b", client.base_url
    end

    it "interpolates values" do
      client = Zendesk::UrlTargetClient.new(url: 'https://wibble.org/users/{{current_user.id}}', method: 'get', user: @user, ticket: @ticket, account: @account, target: @target)
      assert_equal "https://wibble.org/users/#{@user.id}", client.base_url
    end
  end

  describe "#url" do
    it "returns the full URL for GET requests and only the base URL for other requests" do
      client = Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b?x=y", method: "get", account: @account, target: @target)
      assert_equal "https://wibble.org/a/b?x=y", client.url
      client = Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b?x=y", method: "post", account: @account, target: @target)
      assert_equal "https://wibble.org/a/b", client.url
    end
  end

  describe "#locale_for_liquid" do
    before { @spanish = translation_locales(:spanish) }

    it "returns the match locale from the parameter" do
      client = Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b?x=y&locale=#{@spanish.id}", method: "get", account: @account, target: @target)
      assert_equal @spanish, client.locale
    end

    it "returns the account locale if the translation doesnt exists" do
      Account.any_instance.stubs(:translation_locale).returns(@spanish)
      client = Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b?x=y&locale=123456789", method: "get", user: @user, account: @account, target: @target)
      assert_equal @spanish, client.locale
    end

    it "returns the account locale if no locale parameter is sent" do
      Account.any_instance.stubs(:translation_locale).returns(@spanish)
      client = Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b?x=y", method: "get", user: @user, account: @account, target: @target)
      assert_equal @spanish, client.locale
    end

    it "returns nil if no user is present" do
      client = Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b?x=y", method: "get", account: @account, target: @target)
      refute client.locale
    end
  end

  describe "#invoke" do
    before do
      @client = Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b?x=y", method: "get", account: @account, target: @target)
    end

    it "does not throw error and follow redirect" do
      redirect_url = @client.url + '&more'

      stub_request(:get, @client.url).to_return(status: 301, headers: { location: redirect_url })
      stub_request(:get, redirect_url).to_return(status: 200)
      response = @client.invoke
      assert response.success?
    end

    it "sets basic auth header only when given username and password" do
      Faraday::Connection.any_instance.stubs(:get).returns(stub)

      Faraday::Connection.any_instance.expects(:basic_auth).never
      @client.invoke

      Faraday::Connection.any_instance.expects(:basic_auth).once
      @client.expects(:username).twice.returns("hello")
      @client.expects(:password).twice.returns("12345")
      @client.invoke
    end

    it "includes trailing / in request" do
      stub_request(:get, "https://wibble.org/a/b/?x=y").
        with(headers: {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => "Zendesk URL Target"}).
        to_return(status: 200, body: "", headers: {})

      # would throw WebMock::NetConnectNotAllowedError if the stub failed to match (i.e. faraday were using no trailing slash)
      Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b/?x=y", method: "get", account: @account, target: @target).invoke
    end

    describe "for PUT and POST requests" do
      before do
        @client = Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b?x=y", method: "post", account: @account, target: @target)
        stub_request(:post, "https://wibble.org/a/b").
          with(body: "x=y",
               headers: {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Content-Type' => 'application/x-www-form-urlencoded; charset=utf-8', 'User-Agent' => "Zendesk URL Target"}).
          to_return(status: 200, body: "", headers: {})
      end

      it "uses a content-type header specifying the charset" do
        assert_equal 'application/x-www-form-urlencoded; charset=utf-8', @client.invoke.env[:request_headers]["content-type"]
      end
    end

    describe "for GET requests" do
      before do
        stub_request(:get, "https://wibble.org/a/b?x=y").
          with(headers: { 'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => "Zendesk URL Target" }).
          to_return(status: 200, body: "", headers: {})
      end

      it "does not use a content-type" do
        assert_nil @client.invoke.env[:request_headers]["content-type"]
      end
    end
  end

  describe "statsd client configuration" do
    let(:client) { Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b?x=y", method: "get", account: @account, target: @target) }

    it "should not create statsd client when it exists already" do
      statsd_client = stub_for_statsd
      client.stubs(:statsd_client).returns(statsd_client)
      assert_equal statsd_client, client.statsd_client
    end

    it "should create a new statsd client when it does not exist" do
      assert_kind_of Zendesk::StatsD::Client, client.statsd_client
    end
  end

  describe "circuit breaker middleware configuration" do
    let(:client) { Zendesk::UrlTargetClient.new(url: "https://wibble.org/a/b?x=y", method: "get", account: @account, target: @target) }

    it "client contains proper circuit breaker options" do
      expect_option = {
        name: 'circuit-breaker-url-target-v1-1',
        failure_threshold: 30,
        reset_timeout: 5,
        store: JohnnyFive::MemoryStore.instance
      }
      @target.stub :id, 1 do
        Rails.cache.stub :try, nil, :dalli do
          assert_equal expect_option, client.circuit_options
        end
      end
    end
  end

  describe "target invocation with circuit breaker middleware" do
    before do
      full_url = "https://wibble.org/a/b?x=y"
      @statsd_client = stub_for_statsd
      @class_statsd_client ||= stub_for_statsd
      Zendesk::UrlTargetClient.any_instance.stubs(:statsd_client).returns(@statsd_client)
      Zendesk::UrlTargetClient.class_variable_set(:@@statsd_client, @class_statsd_client)

      @client = Zendesk::UrlTargetClient.new(url: full_url, method: "get", account: @account, target: @target)
      @opts = {store: JohnnyFive::MemoryStore.send(:new), failure_threshold: 1, reset_timeout: 1000, name: 'circuit-breaker-url-target-v1-target-1'}

      stub_request(:get, full_url).to_return(status: 400, headers: {})
      @target.stubs(:account).returns(@account)
    end

    describe "should be circuit breaking with feature url_target_circuit_breaker enabled" do
      describe_with_arturo_enabled :url_target_v1_circuit_breaker do
        it "raise circuit breaker exception" do
          @client.stub :circuit_options, @opts do
            # 1st invoke
            refute @client.invoke.success?
            # 2nd invoke
            assert_raises JohnnyFive::CircuitTrippedError do
              @statsd_client.expects(:increment).with(:circuit_tripped, tags: %w[source:url_target]).once
              @class_statsd_client.expects(:increment).with(:soft_circuit_tripped, tags: %w[source:url_target]).never
              @client.invoke
            end
          end
        end
      end
    end

    describe "should not be circuit breaking with feature url_target_circuit_breaker disabled" do
      describe_with_arturo_disabled :url_target_v1_circuit_breaker do
        it "target invocation is failed with error response" do
          @client.stub :circuit_options, @opts do
            # expect soft circuit tripped be invoked
            @statsd_client.expects(:increment).with(:circuit_tripped, tags: %w[source:url_target]).never
            @class_statsd_client.expects(:increment).with(:soft_circuit_tripped, tags: %w[source:url_target]).twice
            # 1st invoke
            refute @client.invoke.success?
            # 2nd invoke
            refute @client.invoke.success?
          end
        end
      end
    end
  end

  describe "parameter management" do
    before do
      comment = Comment.new(body: 'Did you go to A&M? I did.', author: User.new(name: "Pablo"), created_at: Time.now, account: @account)
      comment.stubs(:trusted?).returns true
      @ticket.stubs(:latest_public_comment).returns(comment)
      @spanish = translation_locales(:spanish)
      Account.any_instance.stubs(:translation_locale).returns(@spanish)
      @client = Zendesk::UrlTargetClient.new(
        url: "http://moo.com/a/?b=c d&e=f:g&t={{ticket.latest_public_comment_formatted}}", method: "get", params: { "h" => "i:j" },
        user: @user, ticket: @ticket, account: @account, target: @target
      )
    end

    describe "#parameters" do
      it "encodes additional parameters" do
        assert_equal("i%3Aj", @client.parameters["h"])
      end

      it "does not encode URL specified parameters" do
        assert_equal("f:g", @client.parameters["e"])
      end

      it "calls ticket context with the account locale" do
        Zendesk::Liquid::TicketContext.expects(:render).with("{{ticket.latest_public_comment_formatted | url_encode}}", @ticket, @user, true, @spanish)
        @client.parameters["t"]
      end

      it "resolves and encode Liquid content" do
        assert_match("Did+you+go+to+A%26M%3F+I+did.", @client.parameters["t"])
      end

      it "returns an empty hash when there's no query" do
        builder = Zendesk::UrlTargetClient.new(url: "http://moo.com/a/", method: "get", account: @account, target: @target)
        assert_equal({}, builder.parameters)
      end
    end

    describe "#query" do
      it "returns a string of the parameters do" do
        assert_match("b=c d&e=f:g&h=i%3Aj&t", @client.query)
        assert_match("Did+you+go+to+A%26M%3F+I+did.", @client.query)
      end

      it "returns a blank string when there are no parameters" do
        @client.expects(:parameters).returns({})
        assert_equal("", @client.query)
      end
    end
  end

  describe "with an attacker redirect" do
    before do
      Resolv::DNS.any_instance.stubs(:getaddress).with('foo.example.com').returns(Resolv::IPv4.create('1.2.3.4'))
      Resolv::DNS.any_instance.stubs(:getaddress).with('bad.example.com').returns(Resolv::IPv4.create('0.0.0.0'))
      Rails.env.stubs(:production?).returns(true) # We only resolve IPs for domains when in production

      stub_request(:get, "https://foo.example.com").
        to_return(status: 302, body: "", headers: {'Location' => 'https://bad.example.com' })
    end

    it "doesn't follow the redirect" do
      assert_raises Faraday::RestrictIPAddresses::AddressNotAllowed do
        Zendesk::UrlTargetClient.new(url: "https://foo.example.com", method: "get", ticket: @ticket, account: @account, target: @target).invoke
      end
    end
  end
end
