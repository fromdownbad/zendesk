require_relative "../../../support/test_helper"

SingleCov.covered! file: 'lib/zendesk/targets/helper.rb'

describe Zendesk::Targets::Helper do
  fixtures :accounts

  let(:trial) { accounts(:trial) }
  let(:minimum) { accounts(:minimum) }

  describe "#exceed_maximum_trial_targets_limit" do
    let(:obj) { Class.new { include Zendesk::Targets::Helper }.new }

    describe_with_arturo_enabled :trial_account_target_limit do
      it "should not exceed the trial account target limit since account is not trial account" do
        refute obj.exceed_maximum_trial_targets_limit?(minimum)
      end

      it "should not exceed the trial account target limit since account has less than two targets" do
        refute obj.exceed_maximum_trial_targets_limit?(trial)
      end

      it "should exceed the trial account target limit since account reaches maximum limit of targets" do
        [1, 2].each do |index|
          target = UrlTargetV2.new(
            title: "Url Target #{index}",
            account: accounts(:trial),
            url: "http://www.example.com/#{index}",
            method: "get"
          )
          target.save!
        end
        assert obj.exceed_maximum_trial_targets_limit?(trial)
      end
    end

    describe_with_arturo_disabled :trial_account_target_limit do
      it "should not exceed the trial account target limit even if account reaches maximum limit of targets since arturo is disabled" do
        [1, 2].each do |index|
          target = UrlTargetV2.new(
            title: "Url Target #{index}",
            account: accounts(:trial),
            url: "http://www.example.com/#{index}",
            method: "get"
          )
          target.save!
        end
        refute obj.exceed_maximum_trial_targets_limit?(trial)
      end
    end
  end
end
