require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Targets::Initializer do
  let(:account) { Account.new }
  let(:target) { BasecampTarget.new(account: account) }
  let(:initializer) { Zendesk::Targets::Initializer.new(account: account, via: :portal) }

  describe "#build" do
    it "raises a Targets::Initializer::InvalidTargetType if there's no type with the given name" do
      assert_raise(Zendesk::Targets::Initializer::InvalidTargetType) { build(type: 'wrong_target') }
    end

    it "is able to create types that need integration from normal backends" do
      build(type: 'ms_dynamics_target')
    end

    describe "for api" do
      let(:initializer) { Zendesk::Targets::Initializer.new(account: account, via: :api) }

      it "is able to create types that need integration" do
        assert_raise(Zendesk::Targets::Initializer::InvalidTargetType) { build(type: 'ms_dynamics_target') }
      end
    end

    it "initializes a target type" do
      assert_equal build.class, BasecampTarget
    end

    it "store target_url as url" do
      target = build(target_url: 'http://base.camp')
      assert_equal "http://base.camp", target.url
    end

    it "marks the target as active" do
      target = build
      assert target.is_active?
    end

    it "marks inactive targets as active" do
      target = build(active: false)
      refute target.is_active?
    end

    it "it pass the account to the target" do
      assert_equal account, build.account
    end
  end

  describe "#set" do
    it "updates the target" do
      set(target, title: "Foo")
      assert_equal "Foo", target.title
    end

    it "stores target_url as url" do
      set(target, target_url: "http://base.camp")
      assert_equal "http://base.camp", target.url
    end

    it "does not auto-mark the target as active" do
      target.is_active = false
      set(target, {})
      refute target.is_active?
    end

    it "marks the target as active" do
      target.is_active = false
      set(target, active: true)
      assert target.is_active?
    end

    it "marks the target as inactive" do
      target.is_active = true
      set(target, active: false)
      refute target.is_active?
    end

    describe "with target that has password" do
      let(:target) { UrlTarget.new(account: account, password: "zendesk_test") }

      it "update password with empty string when users mirror back the attributes they read" do
        set(target, password: "")
        assert_equal "", target.password
      end

      it "update password with value" do
        set(target, password: "foo")
        assert_equal "foo", target.password
      end
    end
  end

  describe "#target_types_map" do
    let(:valid_types) do
      files = Dir[Rails.root.join("app/models/targets/*")]
      files.each { |file| require File.expand_path(file, Rails.root) }
      valid_types = Target.descendants.map(&:name).sort
      valid_types.delete("OauthTarget")
      valid_types.delete("FlowdockTarget")
      valid_types
    end

    it "includes all targets except for OauthTarget and FlowdockTarget" do
      assert_equal valid_types, initializer.target_types_map.values.uniq.map(&:name).sort
    end

    it "maps http_target to UrlTargetV2" do
      assert_equal UrlTargetV2, initializer.target_types_map["http_target"]
    end
  end

  def set(target, attributes)
    initializer.set(target, attributes)
  end

  def build(attributes = {}, type = 'basecamp_target')
    initializer.build(attributes.reverse_merge(type: type))
  end
end
