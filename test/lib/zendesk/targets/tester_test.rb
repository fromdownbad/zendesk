require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 5

describe Zendesk::Targets::Tester do
  let(:user) { stub("user") }
  let(:event) { stub("event", :author= => user, :via_id= => :web_form) }
  let(:events) { stub("events", first: event) }
  let(:ticket) { stub("ticket", will_be_saved_by: nil, events: events) }
  let(:tickets) { stub("tickets", not_closed: stub("scope", first: ticket)) }
  let(:mailer) { stub("mailer") }
  let(:account) { stub("account", tickets: tickets) }

  let(:target) do
    stub("target",
      valid?: true,
      is_test_supported?: true,
      requires_authorization?: false,
      test_no_supported_reason: nil,
      send_test_message: nil)
  end

  let(:target_tester) { Zendesk::Targets::Tester.new(account: account, user: user, mailer: mailer) }

  before { ViaType.stubs(:WEB_FORM).returns(:web_form) }

  it "sets the test event user to the current user" do
    event.expects(:author=).with(user)
    execute
  end

  it "sets the test event via_id to ViaType.WEB_FORM" do
    event.expects(:via_id=).with(ViaType.WEB_FORM)
    execute
  end

  it "calls #send_test_message on the target" do
    target.expects(:send_test_message).with(anything, event)
    execute
  end

  it "raises NoTicketsInAccount if the account has no tickets" do
    account.tickets.stubs(:not_closed).returns(stub(first: nil))
    proc { execute }.must_raise(Zendesk::Targets::Tester::NoTicketsInAccount)
  end

  it "raises TestingNotSupported if the target doesn't support testing" do
    target.stubs(:is_test_supported?).returns(false)
    target.stubs(:test_no_supported_reason).returns("Not feeling up for it, man...")

    e = proc { execute }.must_raise(Zendesk::Targets::Tester::TestingNotSupported)
    e.reason.must_equal("Not feeling up for it, man...")
  end

  it "raises TargetInvalid if the target is invalid" do
    target.stubs(:valid?).returns(false)
    target.stubs(:errors).returns(stub(full_messages: ["Needs more pickles."]))

    e = proc { execute }.must_raise(Zendesk::Targets::Tester::TargetInvalid)
    e.error_messages.must_equal(["Needs more pickles."])
  end

  it "raises TestFailed if the target fails" do
    exception = StandardError.new("Campfire login failed.")
    target.stubs(:send_test_message).raises(exception)

    e = proc { execute }.must_raise(Zendesk::Targets::Tester::TestFailed)
    e.error.must_equal(exception)
  end

  it "raises TestFailed if pivotal story creation fails" do
    exception = StandardError.new("pivotal story creation failed for ticket")
    target.stubs(:send_test_message).raises(exception)

    e = proc { execute }.must_raise(Zendesk::Targets::Tester::TestFailed)
    e.error.must_equal(exception)
  end

  describe "#test_message" do
    describe "when a message is passed in" do
      before { execute(message: "Custom message") }

      it "returns the message" do
        assert_equal "Custom message", target_tester.send(:test_message)
      end
    end

    describe "when a message is not passed in" do
      before do
        Timecop.freeze
        execute
      end

      it "returns the default message" do
        assert_equal "Test message from Zendesk sent on: #{Time.now.utc.strftime("%Y-%m-%d %H:%M:%S")} UTC",
          target_tester.send(:test_message)
      end
    end
  end

  def execute(options = {})
    target_tester.test(target, options)
  end
end
