require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Idempotency::ErrorHandling do
  class ErrorHandlingTester
    include ActiveSupport::Rescuable
    include Zendesk::Idempotency::ErrorHandling

    def call
      yield
    rescue StandardError => e
      rescue_with_handler(e) || raise
    end
  end

  let(:handler) { ErrorHandlingTester.new }

  it 'renders an error when a parameter mismatch occurs' do
    handler.expects(:render).with(
      status: 400,
      json: {
        error: 'IdempotentRequestError',
        description: ::I18n.t('txt.error_message.controllers.idempotency.request_mismatch')
      }
    )
    handler.call { raise Zendesk::Idempotency::ParameterMismatchError }
  end
end
