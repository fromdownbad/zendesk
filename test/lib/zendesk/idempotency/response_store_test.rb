require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Idempotency::ResponseStore do
  let(:backend_client) { mock('backend_client') }
  let(:backend) { mock('backend', client: backend_client) }
  let(:logger) { mock('logger') }
  let(:expiry) { 15.minutes }
  let(:subject) { Zendesk::Idempotency::ResponseStore.new(backend: backend, logger: logger, expiry: expiry) }

  describe '#get' do
    describe 'error recovery' do
      it 'handles fetch failure' do
        backend_client.expects(:get).with('key').raises 'the ceiling'
        logger.
          expects(:error).
          with kind_of(RuntimeError), 'Store key could not be read', location: subject, metadata: { key: 'key' }
        assert_nil subject.get 'key'
      end

      it 'handles invalid json' do
        data = 'invalid json'
        backend_client.expects(:get).with('key').returns data
        logger.
          expects(:error).
          with kind_of(JSON::ParserError), 'Store key contained an invalid response value', location: subject, metadata: { key: 'key', value: data }
        assert_nil subject.get 'key'
      end

      it 'handles invalid response status' do
        data = '[400, { "foo": "bar" }, "hello world"]'
        backend_client.expects(:get).with('key').returns data
        logger.
          expects(:error).
          with kind_of(JSON::Schema::ValidationError), 'Store key contained an invalid response value', location: subject, metadata: { key: 'key', value: data }
        assert_nil subject.get 'key'
      end

      it 'handles invalid headers' do
        data = '[200, "foo: bar", "hello world"]'
        backend_client.expects(:get).with('key').returns data
        logger.
          expects(:error).
          with kind_of(JSON::Schema::ValidationError), 'Store key contained an invalid response value', location: subject, metadata: { key: 'key', value: data }
        assert_nil subject.get 'key'
      end
    end

    describe 'with valid store data' do
      it 'returns a cached response triplet' do
        backend_client.expects(:get).with('key').returns '["signature", 200, { "foo": "bar" }, "hello world"]'
        logger.expects(:error).never
        assert_equal ['signature', 200, { 'foo' => 'bar' }, 'hello world'], subject.get('key')
      end
    end
  end

  describe '#set' do
    let(:raw_body) { 'hello world ' }
    let(:body) { mock('body').tap { |body| body.stubs(:body).returns raw_body } }
    let(:headers) { { 'Location' => 'https://example.org' } }
    let(:response) { mock('response').tap { |response| response.expects(:to_a).returns [201, headers, body] } }
    let(:signature) { 'signature' }

    describe 'with operational store' do
      it 'stores expected data' do
        backend_client.expects(:set).with('key', [signature, 201, headers, raw_body].to_json, ex: expiry)
        assert_nil subject.set 'key', response, signature: signature
      end
    end

    describe 'with errors' do
      it 'stores expected data' do
        backend_client.stubs(:set).raises 'the ceiling'
        logger.
          expects(:error).
          with(kind_of(RuntimeError), 'Store key could not be set', location: subject, metadata: { key: 'key' }).
          returns mock('scope')
        assert_nil subject.set 'key', response, signature: signature
      end
    end
  end

  describe '#touch' do
    it 'sets expiry on key' do
      backend_client.expects(:expire).with('key', expiry)
      subject.touch 'key'
    end
  end
end
