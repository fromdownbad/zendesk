require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Idempotency::ErrorLog do
  let(:described_class) { Zendesk::Idempotency::ErrorLog }
  let(:error) { RuntimeError.exception('original error') }
  let(:metadata) { { key: :value } }

  it 'passes arguments for error' do
    Rails.logger.expects(:info).with('given error (original error) - key: value')
    described_class.error(error, 'given error', **metadata)
  end
end
