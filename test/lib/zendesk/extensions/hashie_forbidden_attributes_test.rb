require_relative "../../../support/test_helper"

SingleCov.covered!

describe Hashie::Mash do
  it 'should not respond to permitted?' do
    hashie_mash = Hashie::Mash.new

    refute hashie_mash.respond_to?(:permitted?)
  end

  it 'should raise a NoMethodError if the permitted? method is called' do
    hashie_mash = Hashie::Mash.new

    assert_raises(NoMethodError) { hashie_mash.permitted? }
  end

  it 'should still work with the original implementation correctly' do
    hashie_mash = Hashie::Mash.new

    hashie_mash[:test_key] = "test value"

    assert hashie_mash.respond_to?(:test_key)
    assert_equal hashie_mash.test_key, "test value"
  end
end
