require_relative "../../../support/test_helper"
require_relative "../../../support/signed_auth_helper"

SingleCov.covered! uncovered: 15

describe 'CIAIntegration' do
  include ZendeskBillingCore::ZuoraTestHelper

  fixtures :accounts, :account_settings, :brands, :account_property_sets, :users, :credit_cards, :rules, :tickets, :recipient_addresses, :role_settings, :ticket_forms

  def auditing(user, &block)
    CIA.audit(actor: user, effective_actor: user, &block)
  end

  def save(scope, property_name, value)
    scope.send("#{property_name}=", value)

    auditing users(:minimum_agent) do
      scope.save
    end
  end

  before do
    @account = accounts(:minimum)
  end

  describe "CIA Integration" do
    it "records changes" do
      @account.cia_attribute_changes.delete_all

      user = users(:support_agent)
      assert_not_equal @account.id, user.account_id

      CIA.audit actor: user, effective_actor: user do
        @account.subdomain = 'foobar'
        @account.save!
      end

      # correct event with account
      events = @account.cia_events
      assert_equal 1, events.size

      event = events.first
      assert_equal "update", event.action
      assert_equal @account.id, event.account_id

      # correct event with account
      changes = event.attribute_changes
      assert_equal 1, changes.size

      change = changes.first
      assert_equal "subdomain", change.attribute_name
      assert_equal @account.id, change.account_id
      assert_equal "minimum", change.old_value
      assert_equal "foobar", change.new_value
    end

    describe "User" do
      it "records changes" do
        user = users(:minimum_agent)
        auditing users(:minimum_agent) do
          user.roles = Role::END_USER.id
          user.save!
        end
        assert_equal 1, user.cia_events.size
      end

      it "does not record changes on end-users" do
        user = users(:minimum_end_user)
        CIA::Event.without_arsi.delete_all

        auditing users(:minimum_end_user) do
          user.password = "XXXX"
          user.valid? # populate crypted_password
          CIA.record(:create, user)
          CIA.record(:update, user)
        end

        assert_equal 0, CIA::Event.count(:all)
      end

      it "records changes of agents and other interesting people" do
        user = users(:minimum_agent)

        auditing users(:minimum_end_user) do
          user.password = "XXXX"
          user.valid? # populate crypted_password
          CIA.record(:create, user)
          CIA.record(:update, user)
        end

        assert_equal 2, CIA::Event.count(:all), CIA::Event.all.inspect
      end

      it "records changes for ex-agents" do
        user = users(:minimum_agent)

        auditing users(:minimum_agent) do
          user.roles = Role::END_USER.id
          CIA.record(:create, user)
          CIA.record(:update, user)
        end

        assert_equal 2, CIA::Event.count(:all)
      end

      it "records changes on the selected custom role" do
        user = users(:minimum_agent)

        permission_set = @account.permission_sets.create(name: "Custom Role")

        auditing users(:minimum_agent) do
          user.update_attributes!(permission_set: permission_set)
        end

        assert_equal 1, user.cia_events.count(:all)
      end

      it "records soft_delete as destroy" do
        user = users(:minimum_agent)
        auditing user do
          user.will_be_saved_by user
          user.delete!
        end
        assert_equal 1, user.cia_events.size # no update for password change, but destroy
        event = user.cia_events.last
        assert_equal "destroy", event.action
        assert_equal [], event.attribute_changes
      end
    end

    describe "Account" do
      it "records changes" do
        auditing users(:support_agent) do
          @account.update_attributes!(is_serviceable: false)
        end
        assert_equal 1, @account.cia_events.size
      end

      it "records changes to owner" do
        user = users(:minimum_admin_not_owner)
        auditing users(:support_agent) do
          @account.update_attributes!(owner: user)
        end
        assert_equal 1, @account.cia_events.size
      end

      it "records soft-deletion and restoration" do
        @account.subscription.stubs(:canceled_on).returns(Time.now)
        auditing users(:support_agent) do
          @account.soft_delete!
        end
        assert_equal 1, @account.cia_events.size
      end

      it "records changes to locale" do
        auditing users(:support_agent) do
          @account.update_attributes!(locale_id: 2)
        end
        assert_equal 1, @account.cia_events.size
      end
    end

    describe "Subscription" do
      it "records changes" do
        subscription = subscriptions(:minimum)
        subscription.stubs(:validate_pending_payment)
        auditing users(:support_agent) do
          subscription.update_attributes!(base_agents: subscription.max_agents + 1)
        end
        assert_equal 1, subscription.cia_events.size
      end
    end

    describe "Brand" do
      it "records changes" do
        auditing users(:support_agent) do
          brand = FactoryBot.create(:brand)
          brand.update_attributes!(active: false)
          brand.soft_delete!
          assert_equal 3, brand.cia_events.size
        end
      end
    end

    describe "TicketFieldCondition" do
      it "records non visible changes" do
        auditing users(:support_agent) do
          tfc = FactoryBot.create(:ticket_field_condition)
          assert_equal 1, tfc.cia_events.size
          refute tfc.cia_events.last.visible?
        end
      end
    end

    describe "ZendeskBillingCore::Zuora::Subscription" do
      it "records changes" do
        subscription = create_zuora_subscription(real: true)
        auditing users(:support_agent) do
          subscription.update_attributes!(max_agents: subscription.max_agents + 1)
        end
        assert_equal 1, subscription.cia_events.size
      end
    end

    describe "UserSetting" do
      it "records changes if it's an auditable property" do
        property_name = "suspended"
        user = users(:minimum_agent)
        save(user.settings, property_name, "1")

        assert event = user.settings.find_by_name(property_name).cia_events.first
        assert event.visible?
      end

      it "does not record changes if it's not an auditable property" do
        property_name = "keyboard_shortcuts_enabled"
        user = users(:minimum_agent)
        save(user.settings, property_name, "1")

        refute user.settings.find_by_name(property_name).cia_events.first
      end
    end

    describe "AccountSetting" do
      %w[
        allowed_mobile_sdk
        api_password_access
        api_token_access
        assumption_duration
        automatic_certificate_provisioning
        bcc_archive_address
        enable_agent_ip_restrictions
        enable_ip_mobile_access
        ip_restriction_enabled
        mobile_app_access
        prefer_lotus
        two_factor_enforce
        use_feature_framework_persistence
      ].each do |auditable_property|
        describe "auditable property #{auditable_property}" do
          it "records changes if it's an auditable property" do
            save(@account.settings, auditable_property, "1")

            assert event = @account.settings.find_by_name(auditable_property).cia_events.first
            assert event.visible?
          end

          it "records updates" do
            save(@account.settings, auditable_property, "1")

            save(@account.settings, auditable_property, "0")

            CIA::Event.count(:all).must_equal(2)

            assert event = @account.settings.find_by_name(auditable_property).cia_events.last
            event.attribute_change_hash.must_equal("value" => ["1", "0"])
            assert event.visible?
          end
        end
      end

      it "records invisible changes if it's not an auditable property" do
        property_name = "has_user_tags"
        save(@account.settings, property_name, "1")

        assert event = @account.settings.find_by_name(property_name).cia_events.first
        refute event.visible?
      end

      it "does not record changes to last_login" do
        property_name = "last_login"
        save(@account.settings, property_name, "2014-10-10")

        refute @account.settings.find_by_name(property_name).cia_events.first
      end

      it "does not record changes if default value is '0', 'false' or false and setting is created with false value" do
        ["0", "false", false].each do |default_value|
          AccountSetting.stubs(:default).returns(default_value)
          property_name = "api_token_access"
          save(@account.settings, property_name, "false")

          assert_equal 0, @account.settings.find_by_name(property_name).cia_events.size
        end
      end

      it "records changes if default value is 'false' and setting is created with true value" do
        AccountSetting.stubs(:default).returns("false")
        property_name = "api_token_access"
        save(@account.settings, property_name, "true")

        assert_equal 1, @account.settings.find_by_name(property_name).cia_events.size
      end

      describe "assumption_expiration setting" do
        it "records changes" do
          save(@account.settings, "assumption_expiration", Time.now)

          assert event = @account.settings.find_by_name("assumption_expiration").cia_events.first
          assert event.visible?
        end

        it "records updates" do
          now = Time.now
          tomorrow = now + 1.day
          save(@account.settings, "assumption_expiration", now)

          save(@account.settings, "assumption_expiration", tomorrow)

          CIA::Event.count(:all).must_equal(2)

          assert event = @account.settings.find_by_name("assumption_expiration").cia_events.last
          event.attribute_change_hash.must_equal("value" => [now.utc.to_s, tomorrow.utc.to_s])
          assert event.visible?
        end
      end
    end

    describe "AccountText" do
      it "does not record changes if the setting is created with a blank value" do
        property_name = "ip_restriction"
        @account.texts.send("#{property_name}=", "")

        auditing users(:minimum_agent) do
          @account.save
        end
        assert_equal 0, @account.texts.find_by_name(property_name).cia_events.size
      end

      it "records changes if the setting is created with a non blank value" do
        property_name = "ip_restriction"
        @account.texts.send("#{property_name}=", "foo")

        auditing users(:minimum_agent) do
          @account.save
        end
        assert_equal 1, @account.texts.find_by_name(property_name).cia_events.size
      end

      it "records changes if the setting is updated with a blank value" do
        property_name = "ip_restriction"
        @account.texts.send("#{property_name}=", "foo")

        auditing users(:minimum_agent) do
          @account.save
        end

        @account.texts.send("#{property_name}=", "")

        auditing users(:minimum_agent) do
          @account.save
        end
        assert_equal 2, @account.texts.find_by_name(property_name).cia_events.size
      end
    end

    describe "Facebook::Page" do
      it "records changes" do
        page = Facebook::Page.new(
          account: @account, graph_object_id: "33454", access_token: "231d2A",
          link: "pagelink", state: Facebook::Page::State::FRESH, category: "Revolution", picture: "pagepic", name: "Evaluate"
        )

        auditing users(:minimum_agent) do
          page.save!
        end
        assert_equal 1, page.cia_events.size
      end
    end

    describe "MonitoredTwitterHandle" do
      it "records changes" do
        mta = MonitoredTwitterHandle.new(access_token: "<token>", secret: "<secret>", account: @account, twitter_user_id: 1)

        auditing users(:minimum_agent) do
          mta.save!
        end
        assert_equal 1, mta.cia_events.size
      end
    end

    describe "Certificate" do
      before do
        @cert = @account.certificates.build
      end

      it "audits on create" do
        auditing users(:minimum_agent) do
          @cert.save!
        end

        assert_equal 1, @cert.cia_events.size
        assert_equal 'create', @cert.cia_events[0].action
      end

      it "audits on state" do
        @cert.save!

        auditing users(:minimum_agent) do
          @cert.state = 'temporary'
          @cert.save!
        end

        assert_equal 1, @cert.cia_events.size
        assert_equal 'update', @cert.cia_events[0].action
      end

      it "audits on sni_enabled" do
        @cert.save!

        auditing users(:minimum_agent) do
          @cert.sni_enabled = true
          @cert.save!
        end

        assert_equal 1, @cert.cia_events.size
        assert_equal 'update', @cert.cia_events[0].action
      end
    end

    describe "BusinessHours" do
      before do
        Account.any_instance.stubs(:has_multiple_schedules?).returns(true)
        @schedule = @account.schedules.create(
          name:      'Schedule',
          time_zone: @account.time_zone
        )
      end

      describe "Schedule" do
        it "records changes" do
          @schedule = nil

          auditing users(:minimum_agent) do
            @schedule = @account.schedules.create(
              name:      'Schedule',
              time_zone: @account.time_zone
            )
          end

          assert_equal 1, @schedule.cia_events.size
        end
      end

      describe "Workweek" do
        it "records changes" do
          CIA.audit actor: users(:minimum_agent) do
            @schedule.workweek.configure_as_default
          end

          assert_equal 1, @schedule.cia_events.size
        end
      end

      describe "Holiday" do
        it "records changes" do
          holiday = nil

          auditing users(:minimum_agent) do
            holiday = @schedule.holiday_calendar.add_holiday(
              name:       'Test Holiday',
              start_date: Date.today,
              end_date:   Date.today.next_day
            )
          end

          assert_equal 1, holiday.cia_events.size
        end
      end
    end

    describe "ticket_field" do
      it "records adds/deletes" do
        tf = FieldDecimal.new(account: @account, title: "title")

        auditing users(:minimum_agent) do
          tf.save!
        end

        assert_equal 1, tf.cia_events.size
      end
    end

    describe "organizations" do
      it "records adds/deletes" do
        org = Organization.new(account: @account, name: "Floo")

        auditing users(:minimum_agent) do
          org.save!
        end

        assert_equal 1, org.cia_events.size
      end
    end

    describe "UserEmailIdentity" do
      it "records changes for secondary email addresses" do
        user = users(:minimum_agent)
        identity = UserEmailIdentity.new(user: user, account: @account, value: "secondary@somedomain.com", is_verified: true)

        auditing users(:minimum_agent) do
          identity.save!
        end
        assert_equal 1, identity.cia_events.size
      end

      it "does not record changes for first email addresses" do
        user = users(:minimum_agent)
        user.identities.delete_all
        identity = UserEmailIdentity.new(user: user, account: @account, value: "secondary@somedomain.com", is_verified: true)

        auditing users(:minimum_agent) do
          identity.save!
        end
        assert_equal 0, identity.cia_events.size
      end
    end

    describe "PermissionSet" do
      it "records changes" do
        permission_set = PermissionSet.new(account: @account, name: "Custom Role")

        auditing users(:minimum_admin) do
          permission_set.save!
        end
        assert_equal 1, permission_set.cia_events.size
      end
    end

    describe "TicketForm" do
      it "records changes to default" do
        ticket_form = ticket_forms(:non_default_ticket_form)

        auditing users(:support_agent) do
          ticket_form.default = true
          ticket_form.save!
        end
        assert_equal 1, ticket_form.cia_events.size
        assert(ticket_form.cia_events.last.visible)
        assert_equal "default", ticket_form.cia_attribute_changes.last.attribute_name
      end
    end

    describe "Rule" do
      it "records changes with serialized definitions" do
        params = {
          conditions_all: [
            {source: "group_id",
             operator: "is",
             value: [""]},
            {source: "requester_id",
             operator: "is_not",
             value: [users(:minimum_agent).id]}
          ]
        }
        new_definition = Definition.build(params)

        rule = rules(:view_unassigned_to_group)
        refute_equal new_definition, rule.definition

        auditing(users(:support_agent)) do
          rule.definition = new_definition
          rule.save!
        end

        event = rule.cia_events.last

        assert_equal 1, rule.cia_events.size
        assert event.visible?
        definition = event.attribute_change_hash["definition"]
        assert definition
        refute_includes definition.first, "Definition" # did not use default serializer
        refute_includes definition.last, "Definition" # did not use default serializer
      end

      it "records destroy" do
        rule = rules(:trigger_notify_requester_of_comment_update)

        assert_difference "CIA::Event.count(:all)", +1 do
          auditing(users(:support_agent)) do
            rule.destroy
          end
        end

        assert rule.cia_events.last
      end
    end

    describe "RecipientAddress" do
      it "records create" do
        auditing users(:minimum_admin) do
          address = accounts(:minimum).recipient_addresses.create!(name: "Foobar", email: "bar@foo.com")
          assert_equal 1, address.cia_events.size
        end
      end
    end

    describe "SuspendedTicketNotification" do
      it "does not record create since we want to hide that from users" do
        auditing users(:minimum_admin) do
          notification = accounts(:minimum).create_suspended_ticket_notification
          assert_equal 0, notification.cia_events.size
        end
      end
    end

    describe "jobs" do
      class TestJob
        extend ZendeskJob::Resque::BaseJob

        priority :medium
        cattr_accessor :test

        def self.work(*_args)
          self.test = CIA.current_actor
        end

        def self.args_to_log(*_args)
          {"xxx" => 1}
        end
      end

      before { TestJob.test = nil }

      resque_inline false

      it "keeps same actor" do
        CIA.audit actor: users(:minimum_end_user) do
          TestJob.enqueue
        end
        assert_nil TestJob.test
        assert_nil CIA.current_actor

        Resque.perform_all

        assert_equal users(:minimum_end_user), TestJob.test
        assert_nil CIA.current_actor
      end

      describe ".find_user" do
        before { @actor = users(:minimum_end_user) }
        let(:user) { CIA.send(:find_user, @actor.id) }

        it "returns the user when on a shard" do
          assert_equal @actor, user
        end

        it "returns a fake system user when not on a shard" do
          ActiveRecord::Base.on_shard(nil) do
            assert_equal User.system_user_id, user.id
            assert_equal false, user.new_record?
          end
        end
      end

      it "is audited by a fake system user when unsharded" do
        actor = users(:minimum_end_user)

        ActiveRecord::Base.on_shard(nil) do
          CIA.audit(actor: actor) do
            TestJob.enqueue
          end

          assert_nil TestJob.test
          assert_nil CIA.current_actor

          Resque.perform_all

          assert_equal User.system_user_id, TestJob.test.id
          assert_nil CIA.current_actor
        end
      end
    end

    describe CIA::Event do
      it "has a via_id" do
        transaction = CIA::Event.new(via_id: 42)
        assert_equal 42, transaction.via_id
      end

      it "has a via_reference_id" do
        transaction = CIA::Event.new(via_reference_id: 42)
        assert_equal 42, transaction.via_reference_id
      end

      it "does not blow up with predictive loading" do
        actor   = users(:minimum_agent)
        ticket  = tickets(:minimum_1)

        event1  = CIA::Event.create!(account: @account, actor: actor, action: "update", source: ticket)
        event2  = event1.dup
        event2.save!
        event2.update_column(:source_type, "App::Installation")

        change1 = event1.attribute_changes.create account: @account, source: ticket, attribute_name: "foo", old_value: "old", new_value: "new"
        change2 = change1.dup
        change2.save!
        change2.update_column(:source_type, "App::Installation")

        revert('ActiveRecord::Relation.collection_observer') do
          ActiveRecord::Relation.collection_observer = PredictiveLoad::Loader

          events = CIA::Event.all
          events.first.source

          changes = CIA::AttributeChange.all
          changes.first.source
        end
      end

      it "truncates too long messages" do
        user = users(:minimum_agent)
        event = CIA::Event.create!(message: 'a' * 300, source: user, action: 'update', actor: user)
        event.message.size.must_equal 255
      end

      describe "#set_visibility" do
        before do
          @event = CIA::Event.new(account: accounts(:minimum), actor: users(:minimum_agent), action: "update")
        end

        def run_before_create_callbacks
          @event.send(:run_callbacks, :create)
        end

        it "marks boring updates as invisible" do
          @event.attribute_changes = [CIA::AttributeChange.new(attribute_name: "fooo", event: @event)]
          run_before_create_callbacks
          assert_equal false, @event.visible
        end

        it "marks interesting updates as visible" do
          @event.attribute_changes = [CIA::AttributeChange.new(attribute_name: "owner_id", event: @event)]
          run_before_create_callbacks
          assert(@event.visible)
        end

        it "marks non-updates as visible" do
          @event.action = :create
          run_before_create_callbacks
          assert(@event.visible)
        end

        it "does not touch visibility if it is set visible" do
          @event.visible = true
          @event.attribute_changes = []
          run_before_create_callbacks
          assert(@event.visible)
        end

        it "does not touch visibility if source namespace is marked as internal" do
          @event.action = "create"
          @event.visible = false
          @event.source_type = 'Voice::PlanBoost'
          run_before_create_callbacks
          assert_equal false, @event.visible
        end

        it "does not touch visibility if source type is marked as internal" do
          @event.action = "create"
          @event.visible = false
          @event.source_type = 'SubscriptionFeature'
          run_before_create_callbacks
          assert_equal false, @event.visible
        end

        it "keeps visibility as false by default if source type is marked as internal" do
          @event.action = "create"
          @event.source_type = 'CustomSecurityPolicy'
          run_before_create_callbacks
          assert_equal false, @event.visible
        end

        describe "#hide_sensitive_account_setting?" do
          describe_with_arturo_enabled :internal_audit_name_visibility_check do
            describe "when account setting name is marked as internal audit name" do
              before do
                @event.action = "create"
                @event.source_type = 'AccountSetting'
                @event.source = AccountSetting.new(name: 'risk_assessment')
              end

              it "sets visibility as false" do
                run_before_create_callbacks
                assert_equal false, @event.visible
              end

              describe_with_arturo_disabled :internal_audit_name_visibility_check do
                it "does not set the visibility to false" do
                  run_before_create_callbacks
                  assert(@event.visible)
                end
              end
            end

            describe "when account setting name is not marked as internal audit name" do
              before do
                @event.action = "create"
                @event.source_type = 'AccountSetting'
                @event.source = @account.settings.where(name: 'created_from_ip_address').first
              end

              it "keeps visibility as true by default" do
                run_before_create_callbacks
                assert(@event.visible)
              end
            end

            describe "when source type is not an account setting" do
              before do
                @event.action = "create"
                @event.source_type = "Ticket"
                @event.source = @account.tickets.first
              end

              it "keeps visibility as true by default" do
                run_before_create_callbacks
                assert(@event.visible)
              end
            end
          end
        end
      end

      describe ".exception_handler" do
        it "reports in production" do
          Rails.env.expects(:production?).returns true
          error = RuntimeError.new("a")
          ZendeskExceptions::Logger.expects(:record).with do |exception, params|
            assert_equal(error, exception)
            assert_equal(CIA, params[:location])
            assert_equal("Error during auditing", params[:message])
          end
          CIA.exception_handler.call(error)
        end

        it "raises everywhere else" do
          assert_raise RuntimeError, "a" do
            CIA.exception_handler.call(RuntimeError.new("a"))
          end
        end
      end

      describe "#source=" do
        it "fills name for ticket" do
          cia_event = CIA::Event.new(source: Ticket.new { |t| t.nice_id = 123 })
          assert_equal "#123", cia_event.source_display_name
        end

        it "fills name for user" do
          cia_event = CIA::Event.new(source: User.new { |t| t.name = "Peter Smith" })
          assert_equal "Peter Smith", cia_event.source_display_name
        end

        it "fills name for Rule" do
          cia_event = CIA::Event.new(source: Rule.new { |t| t.title = "I rule" })
          assert_equal "I rule", cia_event.source_display_name
        end

        it "fills name for View" do
          cia_event = CIA::Event.new(source: View.new { |t| t.title = "I rule" })
          assert_equal "I rule", cia_event.source_display_name
        end

        describe "automatic account assignment" do
          it "does not overwrite account" do
            cia_event = CIA::Event.new(account: accounts(:minimum))
            assert_equal accounts(:minimum), cia_event.account
          end

          it "picks account from source account" do
            cia_event = CIA::Event.new(source: accounts(:minimum))
            assert_equal accounts(:minimum), cia_event.account
          end

          it "picks account from source user" do
            cia_event = CIA::Event.new(source: User.new(account: accounts(:minimum)))
            assert_equal accounts(:minimum), cia_event.account
          end
        end
      end

      describe ".inexcact_source_type" do
        it "is Rule for Rule" do
          assert_equal "Rule", CIA::Event.inexact_source_type("Rule")
        end

        it "is Rule for Rule inherited" do
          assert_equal "Rule", CIA::Event.inexact_source_type("View")
        end

        it "is other for other" do
          assert_equal "User", CIA::Event.inexact_source_type("User")
        end
      end
    end

    describe CIA::AttributeChange do
      it "has alphabetical VISIBLE_ATTRIBUTES because it makes comparing missing easier" do
        assert_equal CIA::AttributeChange::VISIBLE_ATTRIBUTES.sort, CIA::AttributeChange::VISIBLE_ATTRIBUTES
      end
    end
  end
end

class CiaRequestTest < ActionDispatch::IntegrationTest
  include SignedAuthHelper

  fixtures :accounts, :users

  it "logs changes as monitor user" do
    Timecop.freeze # maybe stop srandom breaks on ci
    @account = accounts(:minimum)
    @assumed = users(:minimum_admin)

    ApplicationController.any_instance.stubs(:block_monitor_changes)

    assume_via_new_monitor(@assumed)

    # make a change
    assert_equal "Pacific Time (US & Canada)", @account.time_zone
    put "/settings/account/update_localization", params: { account: {time_zone: "Samoa"} }
    assert_equal "Samoa", @account.reload.time_zone

    # it was recorded as the monitor user
    event = CIA::Event.last
    assert_equal "Account", event.source_type
    assert_equal User.system.id, event.actor.id
    assert_equal 1234, event.via_reference_id
    assert_equal ViaType.MONITOR_EVENT, event.via_id
  end
end
