require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'DisableComplexSessionValues' do
  describe ".dump" do
    let(:secret) { "79f9016426579e6e3e2ce50988b72116" }
    let(:parent_store) do
      ActionDispatch::Request.new({
        'action_dispatch.key_generator' => ActiveSupport::KeyGenerator.new(secret),
        'action_dispatch.signed_cookie_salt' => ''
      }).cookie_jar
    end
    let(:store) { parent_store.signed }

    def call(value)
      store["x"] = value
    end

    def with_normal_serializer
      base = store
      old = base.instance_variable_get(:@serializer)
      base.instance_variable_set(:@serializer, Marshal)
      yield
    ensure
      base.instance_variable_set(:@serializer, old)
    end

    it "is fine with json serializable" do
      call("xxx")
      call(111)
      call(Date.new)
      call(Time.now)
      call([])
      call([1, ""])
      call(value: {})
      call(true)
      call(false)
      call(nil)
      call(value: {"y" => 1, "x" => {"x" => 1}})

      # rails flash uses array with symbols + uses hash keys that need to be symbols
      call(value: {"x" => {"y" => :t, "z" => [:x, :y]}})
    end

    it "is break with non json serializable" do
      assert_raises(ArgumentError) { call(Integer) }
      assert_raises(ArgumentError) { call(value: {x: Integer}) }
    end

    it "does not allow loading invalid objects" do
      with_normal_serializer { store["x"] = Definition.new }
      assert_raises(ArgumentError) { store["x"] }
    end

    it "allows loading valid objects" do
      with_normal_serializer { store["x"] = Marshal.dump(x: [:y]) }
      store["x"]
    end
  end
end
