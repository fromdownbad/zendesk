require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'ArDisallowPartialInserts' do
  fixtures :all

  it 'does not use partial inserts' do
    ticket = Ticket.new.tap do |t|
      t.account = accounts(:minimum)
      t.requester = users(:minimum_author)
      t.will_be_saved_by(t.requester)
      t.description = 'disallow partial inserts test'
    end

    Ticket.transaction do
      ticket.save!
      raise ActiveRecord::Rollback
    end

    # We've already attempted to save this Ticket, and we rolled back the change.
    # With partial inserts enabled, a subsequent save will attempt to ONLY insert
    # the "changed" fields.  The only changed field is the `id`, so this will
    # cause ActiveRecord to generate an INSERT statement that only sets the `id`.
    # This is a bug in ActiveRecord.  The `save` will fail.
    # If we've correctly disabled partial inserts, then a full INSERT will be
    # generated with includes all columns, and the `save` will succeed.
    assert ticket.save

    # If, for some reason, the `save` succeeded, but did a partial INSERT, it
    # would have created a row with an id, but without values for other fields.
    # This is not expected to actually happen.
    reloaded_ticket = Ticket.find(ticket.id)
    refute_equal 0, reloaded_ticket.account_id
    refute_nil reloaded_ticket.created_at
    refute_nil reloaded_ticket.updated_at
  end
end
