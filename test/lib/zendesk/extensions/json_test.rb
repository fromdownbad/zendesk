require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe 'Json' do
  describe "json hacks" do
    before do
      @time = Time.at(1234567890).in_time_zone("CET")
      @json_time = "2009/02/14 00:31:30 +0100"
    end

    describe Array do
      it "has to_json" do
        expected = "[1,\"2\",true,null,[1,\"2\",\"#{@json_time}\"]]"
        actual = [1, "2", true, nil, [1, "2", @time]].to_json
        assert_equal expected, actual
      end

      it "has as_json" do
        expected = [1, "2", true, nil, [1, "2", @json_time]]
        actual = [1, "2", true, nil, [1, "2", @time]].as_json
        assert_equal expected, actual
      end
    end

    describe TrueClass do
      it "has as_json" do
        assert(true.as_json)
      end

      it "has encode_json" do
        assert_equal "true", true.encode_json(1)
      end
    end

    describe FalseClass do
      it "has as_json" do
        assert_equal false, false.as_json
      end

      it "has encode_json" do
        assert_equal "false", false.encode_json(1)
      end
    end

    describe NilClass do
      it "has as_json" do
        assert_nil nil.as_json
      end

      it "has encode_json" do
        assert_equal "null", nil.encode_json(1)
      end
    end
  end
end
