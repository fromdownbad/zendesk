require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe 'TimeZone' do
  describe ActiveSupport::TimeZone do
    describe "it includes the new time zones" do
      it "includes Puerto Rico" do
        assert ActiveSupport::TimeZone["Puerto Rico"]
      end

      it "includes Oslo" do
        assert ActiveSupport::TimeZone["Oslo"]
      end
    end

    describe "#translated_name" do
      it 'returns the translated name' do
        tz = ActiveSupport::TimeZone['Pacific Time (US & Canada)']
        refute_includes tz.translated_name, "translation_missing:"
      end
    end

    describe "#cache_key" do
      let(:zone) { ActiveSupport::TimeZone["Pacific/Auckland"] }

      it "is the same for same" do
        assert_equal zone.cache_key, zone.cache_key
      end

      it "is different for different zones" do
        refute_equal zone.cache_key, ActiveSupport::TimeZone.all.first.cache_key
      end

      it "is different for different times" do
        dst = Timecop.travel("2013-01-01") { zone.cache_key }
        ndst = Timecop.travel("2013-06-01") { zone.cache_key }
        refute_equal dst, ndst
      end
    end

    describe '.all' do
      let(:collection) { ActiveSupport::TimeZone.all }

      it 'includes non-default cities' do
        time_zone = collection.detect { |tz| tz.name == 'Oslo' }
        time_zone.wont_be_nil
      end
    end

    describe '#moment_packed' do
      before do
        Timecop.freeze(Time.utc(2014, 12, 26))
      end

      it 'packages dst data into moment.js packed format ' do
        tz = ActiveSupport::TimeZone["Pacific Time (US & Canada)"]
        tz.moment_packed.must_match %r{America/Los_Angeles|PST PDT|80 70|}
      end

      it 'handles timezones that do not observe DST and have no DST transition data' do
        tz = ActiveSupport::TimeZone["UTC"]
        tz.moment_packed.must_equal "Etc/UTC|UTC|0|0|"
      end

      it 'handles timezones that currently do not observe DST but have historical transition data' do
        tz = ActiveSupport::TimeZone["Buenos Aires"]
        diff = '-03 -02|30 20|001010'
        tz.moment_packed.must_equal "America/Argentina/Buenos_Aires|#{diff}|1drD0 j3c0 uL0 1qN0 WL0"
      end
    end

    describe '#to_s' do
      it 'never returns unextracted translations' do
        ActiveSupport::TimeZone.all.map(&:to_s).select do |tz|
          tz.include?("translation missing:")
        end.must_equal([])
      end

      it 'returns the name with the formatted offset time' do
        Timecop.travel('2015-07-01 00:00:00')
        tz = ActiveSupport::TimeZone['Pacific Time (US & Canada)']
        tz.to_s.must_equal '(GMT-07:00) Pacific Time (US & Canada)'
      end

      it 'returns the name with the formatted offset time with DST' do
        Timecop.travel('2015-12-01 00:00:00')
        tz = ActiveSupport::TimeZone['Pacific Time (US & Canada)']
        tz.to_s.must_equal '(GMT-08:00) Pacific Time (US & Canada)'
      end
    end
  end
end
