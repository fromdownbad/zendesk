require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'Mail' do
  describe "mail hacks" do
    it "encodes in quoted-printable" do
      m = Mail.new(body: "a=1")
      m.content_transfer_encoding = "quoted-printable"
      assert_includes m.encoded, "Content-Transfer-Encoding: quoted-printable\r\n\r\na=3D1=\r\n"
    end

    it "encodes in 7-bit by default" do
      assert_includes Mail.new(body: "a=1").encoded, "Content-Transfer-Encoding: 7bit\r\n\r\na=1"
    end

    it "is able to switch content-transfer-encoding" do
      m = Mail.new(body: "a=1")
      m.content_transfer_encoding = "quoted-printable"
      assert_includes m.encoded, "Content-Transfer-Encoding: quoted-printable\r\n\r\na=3D1=\r\n"
      m.content_transfer_encoding = "7-bit"
      assert_includes Mail.new(body: "a=1").encoded, "Content-Transfer-Encoding: 7bit\r\n\r\na=1"
    end

    it "is able to encode utf-8" do
      mail = Mail.new(body: "åß∂åß∂åß∂´Ω≈√", content_transfer_encoding: "quoted-printable").encoded
      assert_includes mail.gsub("\r\n", "\n"), "Content-Type: text/plain;\n charset=UTF-8\nContent-Transfer-Encoding: quoted-printable\n\n=C3=A5=C3=9F=E2=88=82=C3=A5=C3=9F=E2=88=82=C3=A5=C3=9F=E2=88=82=C2=B4=CE=A9=\n=E2=89=88=E2=88=9A="
    end

    it "is able to encode empty body" do
      mail = Mail.new(body: nil, content_transfer_encoding: "quoted-printable").encoded
      assert_equal "", Mail::Message.new(mail).body.to_s
    end
  end
end
