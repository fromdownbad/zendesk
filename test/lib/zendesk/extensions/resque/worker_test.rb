require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Resque::Worker do
  before do
    Resque.redis.flushall
  end

  after do
    Resque::Worker.kill_all_heartbeat_threads
  end

  describe '#prune_dead_workers' do
    it "is not required if resque is version 2.0.0 or greater" do
      assert(
        Gem::Version.new(Resque::VERSION) < Gem::Version.new('2.0.0'),
        "Resque::Worker#prune_dead_workers extension is included in Resque 2.0.0, " \
        "see https://github.com/resque/resque/pull/1594"
      )
    end

    it "does not prune if another worker has pruned (started pruning) recently" do
      now = Time.now
      worker_a = Resque::Worker.new(:jobs)
      worker_a.to_s = 'worker_a:1:jobs'
      worker_a.register_worker
      worker_a.heartbeat!(now - Resque.prune_interval - 1)
      assert_equal 1, Resque.workers.size
      assert_equal [worker_a], Resque::Worker.all_workers_with_expired_heartbeats

      worker_b = Resque::Worker.new(:jobs)
      worker_b.to_s = 'worker_b:1:jobs'
      worker_b.register_worker
      worker_b.heartbeat!(now)
      assert_equal 2, Resque.workers.size

      worker_b.prune_dead_workers
      assert_equal [], Resque::Worker.all_workers_with_expired_heartbeats

      worker_c = Resque::Worker.new(:jobs)
      worker_c.to_s = "worker_c:1:jobs"
      worker_c.register_worker
      worker_c.heartbeat!(now - Resque.prune_interval - 1)
      assert_equal 2, Resque.workers.size
      assert_equal [worker_c], Resque::Worker.all_workers_with_expired_heartbeats

      worker_d = Resque::Worker.new(:jobs)
      worker_d.to_s = 'worker_d:1:jobs'
      worker_d.register_worker
      worker_d.heartbeat!(now)
      assert_equal 3, Resque.workers.size

      # worker_c does not get pruned because worker_b already pruned recently
      worker_d.prune_dead_workers
      assert_equal [worker_c], Resque::Worker.all_workers_with_expired_heartbeats
    end
  end
end
