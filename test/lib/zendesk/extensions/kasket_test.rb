require_relative "../../../support/test_helper"

SingleCov.covered!

describe Kasket do
  describe "#kasket_key_for_with_sharding" do
    describe "when sharded" do
      it "prefixs the shard" do
        assert_match /^shard.*id=11$/, User.kasket_key_for([[:id, 11]])
      end

      it "prefixs the shard to multiple ids" do
        keys = User.kasket_key_for([[:id, [11, 22, 33]]])
        assert_equal 3, keys.size
        assert_match /^shard.*id=11$/, keys[0]
        assert_match /^shard.*id=22$/, keys[1]
        assert_match /^shard.*id=33$/, keys[2]
      end
    end

    describe "when not sharded" do
      it "returns a clean key" do
        assert_match /^kasket-.*id=11$/, Arturo::Feature.kasket_key_for([[:id, 11]])
      end
    end
  end
end
