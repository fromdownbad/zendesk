require_relative "../../../support/test_helper"
require_relative "../../../support/archive_helper"

SingleCov.covered!

describe 'ActiveRecord::Base' do
  describe "#find_for_search_results" do
    fixtures :tickets, :users
    let(:ticket) { tickets(:minimum_1) }
    let(:ticket2) { tickets(:minimum_2) }

    describe "for archivable models" do
      let(:stubbed_tickets) { [ticket, ticket2] }

      it "loads archived records" do
        id = ticket.id
        archive_and_delete(ticket)
        result = Ticket.find_for_search_results(ids: [id], stub_only: false)
        assert result.all?(&:archived?)
        assert_equal result.map(&:id), [id]
      end
    end

    describe "for unarchivable models" do
      let(:user) { users(:minimum_end_user) }
      it "loads records" do
        id = user.id
        result = User.find_for_search_results(ids: [id])
        assert_equal result.map(&:id), [id]
      end
    end

    describe "with include_models" do
      let(:stubbed_tickets) { [ticket, ticket2] }

      it "loads included models" do
        result = Ticket.find_for_search_results(ids: stubbed_tickets.map(&:id), include_models: [:comments])
        assert result.all? { |ticket| ticket.association(:comments).loaded? }
        assert_equal result.map(&:id).sort, [ticket.id, ticket2.id].sort
      end
    end
  end
end
