require_relative "../../../support/test_helper"

SingleCov.covered!

describe I18n do
  fixtures :translation_locales

  let(:spanish) { translation_locales(:spanish) }

  before do
    I18n.locale = nil
    I18n.translation_locale = nil
  end

  describe '.translation_locale' do
    it 'defaults to ENGLISH_BY_ZENDESK' do
      I18n.translation_locale.must_equal ENGLISH_BY_ZENDESK
    end

    it 'returns the value stored in config' do
      I18n.translation_locale = spanish

      I18n.translation_locale.must_equal spanish
      I18n.config.translation_locale.must_equal spanish
    end
  end

  describe '.translation_locale=' do
    it 'updates the config' do
      I18n.translation_locale = spanish

      I18n.config.translation_locale.must_equal spanish
    end

    it 'also sets I18n.locale' do
      I18n.translation_locale = spanish

      I18n.locale.must_equal spanish.to_sym
    end
  end

  describe '.locale' do
    it 'defaults to ENGLISH_BY_ZENDESK' do
      I18n.locale.must_equal ENGLISH_BY_ZENDESK.to_sym
    end
  end

  describe '.locale=' do
    it 'updates the config' do
      I18n.locale = :"en-US-x-1"

      I18n.config.locale.must_equal :"en-US-x-1"
    end

    it 'accepts a locale symbol' do
      I18n.locale = :"en-US-x-1"

      I18n.locale.must_equal :"en-US-x-1"
    end

    it 'accepts a TranslationLocale' do
      I18n.locale = spanish

      I18n.locale.must_equal spanish.to_sym
    end

    it 'also sets I18n.translation_locale' do
      I18n.locale = spanish

      I18n.translation_locale.must_equal spanish
    end
  end

  describe '.with_locale' do
    it 'resets the translation_locale object' do
      I18n.translation_locale = spanish

      I18n.with_locale(ENGLISH_BY_ZENDESK) do
        I18n.translation_locale.must_equal ENGLISH_BY_ZENDESK
      end

      I18n.translation_locale.must_equal spanish
    end

    it 'resets the locale symbol' do
      I18n.translation_locale = spanish

      I18n.with_locale(ENGLISH_BY_ZENDESK) do
        I18n.locale.must_equal ENGLISH_BY_ZENDESK.to_sym
      end

      I18n.locale.must_equal spanish.to_sym
    end

    it 'does not go to the database when resetting the locale' do
      I18n.translation_locale = spanish

      assert_sql_queries(0) do
        I18n.with_locale(ENGLISH_BY_ZENDESK) { "foo" }
      end

      I18n.translation_locale.must_equal spanish
    end
  end
end
