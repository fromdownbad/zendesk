require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'KasketMultiVersion' do
  describe Kasket do
    let(:account) { accounts(:minimum) }
    let(:property) { account.account_property_set }

    it "has multiple keys for expiry" do
      assert_equal(
        [
          "shard_1/kasket-#{Kasket::Version::PROTOCOL}/account_property_sets/version=#{AccountPropertySet.column_names.join.sum}/id=#{property.id}",
          "shard_1/kasket-#{Kasket::Version::PROTOCOL}/account_property_sets/version=#{AccountPropertySet.column_names.join.sum}/account_id=#{account.id}",
          "shard_1/kasket-#{Kasket::Version::PROTOCOL}/account_property_sets/version=#{AccountPropertySet.column_names.join.sum}/account_id=#{account.id}/first",
        ],
        property.kasket_keys
      )
    end

    it "expires all version keys" do
      property.kasket_keys.each { |k| Kasket.cache.write(k, "XXX") }
      assert_equal ["XXX", "XXX", "XXX"], property.kasket_keys.map { |k| Kasket.cache.read(k) }

      property.save!

      assert_equal [Kasket::CONFIGURATION[:write_through] ? property.attributes_before_type_cast : nil, nil, nil].to_json, property.kasket_keys.map { |k| Kasket.cache.read(k) }.to_json
    end
  end
end
