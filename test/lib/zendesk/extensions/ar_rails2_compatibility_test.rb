require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 15

describe 'ArRails2Compatibility' do
  fixtures :users, :memberships, :groups

  describe "ordering" do
    def last_order(&block)
      sql_queries(&block).
        reverse.
        detect { |query| query =~ /ORDER/ }[/ORDER .*/]. # first queries with an ORDER
        sub(/ LIMIT.*/, "") # Remove the limit if present
    end

    it "orders associations" do
      last_order { User.first.groups.to_a }.
        must_equal "ORDER BY `groups`.`name` ASC"
    end

    it "orders associations with .paginate" do
      last_order { User.first.groups.order("id desc").paginate(page: 1, per_page: 1).length }.
        must_equal "ORDER BY id desc, `groups`.`name` ASC"
    end

    it "orders associations with groups" do
      last_order { User.first.groups.order("id desc").to_a }.
        must_equal "ORDER BY id desc, `groups`.`name` ASC"
    end

    it "orders on multiple associations with groups" do
      last_order { User.first.groups.order("id desc").order("id asc").to_a }.
        must_equal "ORDER BY id asc, id desc, `groups`.`name` ASC"
    end

    it "orders normal calls" do
      last_order { User.order("id desc").to_a }.
        must_equal "ORDER BY id desc"
    end

    it "orders normal calls with multiple orderings" do
      last_order { User.order("id asc").order("id desc").to_a }.
        must_equal "ORDER BY id desc, id asc"
    end

    it "orders named queries" do
      last_order { User.limited.order("id desc").to_a }.
        must_equal "ORDER BY id desc, users.name"
    end

    it "orders on named queries with multiple orderings" do
      last_order { User.limited.order("id asc").order("id desc").to_a }.
        must_equal "ORDER BY id desc, id asc, users.name"
    end
  end
end
