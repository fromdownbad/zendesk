require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'ArImplicitReadonly' do
  fixtures :accounts, :users, :groups, :entries

  describe "implicit readonly" do
    it "makes has_many writable" do
      refute groups(:minimum_group).users.first.groups.first.readonly?
    end

    it "makes foo writeable" do
      refute accounts(:minimum).entries.for_user(users(:minimum_end_user)).first.readonly?
    end
  end
end
