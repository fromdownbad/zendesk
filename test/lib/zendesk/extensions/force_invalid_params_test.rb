require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 3

class EncodingUploadTestController < ActionController::Base
  def file_upload
    file = params[:uploaded_data].try(:original_filename) || "NO-FILE"
    render plain: "#{file} -- #{file.encoding} -- #{file.valid_encoding?}"
  end

  def echo
    file = params[:filename]
    render plain: "#{file} -- #{file.encoding} -- #{file.valid_encoding?}"
  end

  def echo_path
    render plain: "#{requet.url} -- #{request.fullpath} -- #{request.encoding} -- #{request.fullpath.valid_encoding?}"
  end
end

describe "params encoding Integration" do
  fixtures :all
  prepend ControllerDefaultParams
  integrate_test_routes EncodingUploadTestController

  it "attempts to transcode file parameter filenames" do
    file = fixture_file_upload('small.png', 'image/png')
    file.stubs(original_filename: "\xC4\xEE\xEA\xF3\xEC\xE5\xED\xF2 small.png".force_encoding(Encoding::ISO_8859_1))

    post "/test/route/encoding_upload_test/file_upload", params: { uploaded_data: file }

    exp = "�������� small.png -- UTF-8 -- true"
    exp.sub!(/ /, "+") if RAILS5 # just the first one?!?

    assert_equal exp, response.body
  end

  it "does not try to transcode a file with no original_filename" do
    file = fixture_file_upload('small.png', 'image/png')
    file.stubs(:original_filename)

    post "/test/route/encoding_upload_test/file_upload", params: { uploaded_data: file }
  end

  it "attempts to transcode invalid UTF8 bytes from common character encodings" do
    non_utf8_params = { "filename" => "\xC4\xEE\xEA\xF3\xEC\xE5\xED\xF2 Microsoft Word.doc".force_encoding(Encoding::ISO_8859_1) }
    get "/test/route/encoding_upload_test/echo", params: non_utf8_params

    assert_equal "�������� Microsoft Word.doc -- UTF-8 -- true", response.body
  end
end
