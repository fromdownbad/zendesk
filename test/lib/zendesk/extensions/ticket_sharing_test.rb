require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 11

describe TicketSharing do
  fixtures :all

  def stub_agreement_request
    stub_request(:post, "https://example.com/agreements/").
      with(body: "{\"receiver_url\":null,\"sender_url\":null,\"status\":null,\"uuid\":null,\"access_key\":null,\"name\":null,\"current_actor\":null,\"sync_tags\":null,\"sync_custom_fields\":null,\"allows_public_comments\":null}",
           headers: {'Accept' => 'application/json', 'Content-Type' => 'application/json'})
  end

  it "makes a request" do
    stub_agreement_request
    agreement = TicketSharing::Agreement.new({})
    assert_equal 200, agreement.send_to("https://example.com")
  end

  it "makes a request with verify_none" do
    stub_agreement_request
    agreement = TicketSharing::NonVerifyAgreement.new({})
    Net::HTTP.any_instance.expects(:verify_mode=).with(OpenSSL::SSL::VERIFY_NONE)
    assert_equal 200, agreement.send_to("https://example.com")
  end

  it "fails with invalid response" do
    stub_agreement_request.to_return(status: 500)
    agreement = TicketSharing::NonVerifyAgreement.new({})
    assert_equal 500, agreement.send_to("https://example.com")
  end

  it "fails with unauthorized response" do
    stub_agreement_request.to_return(status: 401)
    agreement = TicketSharing::NonVerifyAgreement.new({})
    assert_equal 401, agreement.send_to("https://example.com")
  end

  it "raises with unknown error" do
    stub_agreement_request.to_return(status: 600)
    agreement = TicketSharing::NonVerifyAgreement.new({})
    assert_raise TicketSharing::Error do
      agreement.send_to("https://example.com")
    end
  end

  describe "#handle_response" do
    before do
      @agreement = Sharing::Agreement.new(account: accounts(:minimum))
      Net::HTTPResponse.any_instance.stubs(body: "message")
    end

    describe "with an Agreement" do
      [:send_to_partner, :update_partner].each do |method|
        describe "using method #{method}" do
          describe "with response code 200" do
            before do
              @response = Faraday::Response.new(status: 200, body: "message")
            end

            it "logs the correct info" do
              Rails.logger.expects(:info).with("Ticket Sharing Log: Client or Server Error (#{@response.status}) Account #{@agreement.account.id} Subdomain #{@agreement.account.subdomain} Agreement #{@agreement.id} Method #{method} Response Body: #{Nokogiri::HTML(@response.body).text}").never
              TicketSharing.handle_response(@agreement, method, @response)
            end
          end

          describe "with response code 400" do
            before do
              @response = Faraday::Response.new(status: 400, body: "message")
            end

            it "logs the correct info" do
              Rails.logger.expects(:info).with("Ticket Sharing Log: Client or Server Error (#{@response.status}) Account #{@agreement.account.id} Subdomain #{@agreement.account.subdomain} Agreement #{@agreement.id} Method #{method} Response Body: #{Nokogiri::HTML(@response.body).text}").once
              TicketSharing.handle_response(@agreement, method, @response)
            end
          end

          describe "with response code 500" do
            before do
              @response = Faraday::Response.new(status: 500, body: "message")
            end

            it "logs the correct info" do
              Rails.logger.expects(:info).with("Ticket Sharing Log: Client or Server Error (#{@response.status}) Account #{@agreement.account.id} Subdomain #{@agreement.account.subdomain} Agreement #{@agreement.id} Method #{method} Response Body: #{Nokogiri::HTML(@response.body).text}").once
              TicketSharing.handle_response(@agreement, method, @response)
            end
          end
        end
      end
    end

    describe "with a SharedTicket" do
      before do
        @shared_ticket = SharedTicket.new(agreement: @agreement, account: accounts(:minimum), ticket: tickets(:minimum_1))
      end

      [:send_to_partner, :update_partner, :unshare].each do |method|
        describe "using method #{method}" do
          describe "with response code 200" do
            before do
              @response = Faraday::Response.new(status: 200, body: "message")
            end

            it "logs the correct info" do
              Rails.logger.expects(:info).with("Ticket Sharing Log: Client or Server Error (#{@response.status}) Account #{@agreement.account.id} Subdomain #{@agreement.account.subdomain} Agreement #{@agreement.id} Shared Ticket #{@shared_ticket.id} Method #{method} Response Body: #{Nokogiri::HTML(@response.body).text}").never
              TicketSharing.handle_response(@shared_ticket, method, @response)
            end
          end

          describe "with response code 400" do
            before do
              @response = Faraday::Response.new(status: 400, body: "message")
            end

            it "logs the correct info" do
              Rails.logger.expects(:info).with("Ticket Sharing Log: Client or Server Error (#{@response.status}) Account #{@agreement.account.id} Subdomain #{@agreement.account.subdomain} Agreement #{@agreement.id} Shared Ticket #{@shared_ticket.id} Method #{method} Response Body: #{Nokogiri::HTML(@response.body).text}").once
              TicketSharing.handle_response(@shared_ticket, method, @response)
            end
          end

          describe "with response code 500" do
            before do
              @response = Faraday::Response.new(status: 500, body: "message")
            end

            it "logs the correct info" do
              Rails.logger.expects(:info).with("Ticket Sharing Log: Client or Server Error (#{@response.status}) Account #{@agreement.account.id} Subdomain #{@agreement.account.subdomain} Agreement #{@agreement.id} Shared Ticket #{@shared_ticket.id} Method #{method} Response Body: #{Nokogiri::HTML(@response.body).text}").once
              TicketSharing.handle_response(@shared_ticket, method, @response)
            end
          end
        end
      end
    end

    describe 'Retrying ticket sharing errors' do
      TicketSharing::RETRY_WHITELIST.each do |response_code|
        describe "A sharing response of #{response_code}" do
          let(:response) { Faraday::Response.new(status: response_code, body: "message") }

          it "properly raises an InternalSharingWarning" do
            @agreement.stubs(:with_zendesk_account?).returns(true)
            assert_raises TicketSharing::InternalSharingWarning do
              TicketSharing.handle_response(@agreement, :send_to_partner, response)
            end
          end
        end
      end

      TicketSharing::RETRY_AND_ERROR_WHITELIST.each do |response_code|
        describe "A sharing response of #{response_code}" do
          let(:response) { Faraday::Response.new(status: response_code, body: "message") }

          it "properly raises an InternalSharingError" do
            @agreement.stubs(:with_zendesk_account?).returns(true)
            assert_raises TicketSharing::InternalSharingError do
              TicketSharing.handle_response(@agreement, :send_to_partner, response)
            end
          end
        end
      end
    end
  end
end
