require_relative "../../../support/test_helper"

SingleCov.not_covered! # covers zendesk_channels/lib/zendesk/extensions/enumerable.rb

describe 'Enumerable' do
  describe "#join_preserving_safety" do
    it "joins strings according to the rails_xss string algebra" do
      refute ["apples", "bananas", "oranges", "kumquats"].map(&:html_safe).join.html_safe? # Enumerable#join_preserving_safety not neccessary apples this starts working automagically in the future
      assert ["apples", "bananas", "oranges", "kumquats"].map(&:html_safe).join_preserving_safety.html_safe?
      refute ["apples", "bananas", "oranges", "kumquats"].join_preserving_safety.html_safe?
      refute ["apples", "bananas", "oranges", "kumquats".html_safe].join_preserving_safety.html_safe?
      refute ["0", "apples".html_safe, "bananas"].join_preserving_safety.html_safe?
    end

    it "fails example a" do
      refute ["a", "b".html_safe].join_preserving_safety.html_safe?
    end

    it "fails example b" do
      refute ["a", "b"].join_preserving_safety.html_safe?
    end

    it "fails example c" do
      refute ["a".html_safe, "b"].join_preserving_safety.html_safe?
    end

    it "joins strings like #join" do
      assert_equal ('a'..'z').to_a.join, ('a'..'z').to_a.join_preserving_safety
      assert_equal ('1'..'9').to_a.join, ('1'..'9').to_a.join_preserving_safety
      assert_equal ["a"].join, ["a"].join_preserving_safety
      assert_equal [].join, [].join_preserving_safety
      assert_equal [1].join, [1].join_preserving_safety
    end
  end
end
