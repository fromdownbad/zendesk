require_relative "../../../support/test_helper"

SingleCov.not_covered!
SingleCov.covered! file: 'lib/zendesk/extensions/ar_skipped_callback_metrics/base_methods.rb'
SingleCov.covered! file: 'lib/zendesk/extensions/ar_skipped_callback_metrics/instrument.rb'
SingleCov.covered! uncovered: 1, file: 'lib/zendesk/extensions/ar_skipped_callback_metrics/relation_methods.rb'
SingleCov.covered! uncovered: 1, file: 'lib/zendesk/stats/active_record_stats/stats_subscriber.rb'

describe ArSkippedCallbackMetrics do
  let(:statsd_client) { stub_for_statsd }
  let(:span) { stub(:span) }

  # This is a stand in for active record, I don't want to redefine stubs there
  class MockArBase; end

  class MockObject < MockArBase
    include ArSkippedCallbackMetrics::Instrument

    def without_entity_publication
      allow_without_entity_publication do
        decrement!
      end
    end
  end

  class MockRelation < ActiveRecord::Relation
    attr_reader :model
    def initialize(model)
      @model = model
    end
  end

  before do
    ActiveRecordStats.subscribe
  end

  after do
    ActiveRecordStats.unsubscribe
  end

  before(:each) do
    ::Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
    Datadog.tracer.stubs(:active_span).returns(span)
  end

  describe '#without_entity_publication' do
    let(:subject) { MockObject.new }

    before do
      MockArBase.class_eval do
        define_method(:decrement!) { :super }
      end
    end

    it 'does not send metrics to datadog' do
      statsd_client.expects(:increment).never
      span.expects(:set_tag).never
      subject.without_entity_publication
    end

    it 'calls decrement! method' do
      subject.expects(:decrement!)
      subject.without_entity_publication
    end
  end

  describe 'instance methods' do
    let(:subject) { MockObject.new }

    before(:each) do
      span.expects(:get_tag).with("zendesk.ar_skip_callback").returns(nil)
    end

    describe 'does not override existing behavior' do
      before do
        statsd_client.expects(:increment)
        span.expects(:set_tag)

        MockArBase.class_eval do
          def delete(*args)
            yield if block_given?
            args.empty? ? :super : args # The behaviour doesn't matter here, I just want to get at the data
          end
        end
      end

      it 'calls super' do
        assert_equal :super, subject.delete
      end

      it 'passes arguments through' do
        assert_equal [:arg], subject.delete(:arg)
      end

      it 'passes block through' do
        obj = mock
        obj.expects(:call)
        subject.delete { obj.call }
      end
    end

    describe :decrement! do
      before do
        MockArBase.class_eval do
          define_method(:decrement!) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:decrement!"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#decrement!")
        subject.decrement!
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.decrement!
      end
    end

    describe :delete do
      before do
        MockArBase.class_eval do
          define_method(:delete) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:delete"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#delete")
        subject.delete
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.delete
      end
    end

    describe :increment! do
      before do
        MockArBase.class_eval do
          define_method(:increment!) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:increment!"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#increment!")
        subject.increment!
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.increment!
      end
    end

    describe :update_column do
      before do
        MockArBase.class_eval do
          define_method(:update_column) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:update_column"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#update_column")
        subject.update_column
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.update_column
      end
    end

    describe :update_columns do
      before do
        MockArBase.class_eval do
          define_method(:update_columns) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:update_columns"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#update_columns")
        subject.update_columns
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.update_columns
      end
    end

    describe :update_all do
      before do
        MockArBase.class_eval do
          define_method(:update_all) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:update_all"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#update_all")
        subject.update_all
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.update_all
      end
    end
  end

  describe 'class methods' do
    let(:subject) { MockObject }

    before(:each) do
      span.expects(:get_tag).with("zendesk.ar_skip_callback").returns(nil)
    end

    describe 'does not override existing behavior' do
      before do
        statsd_client.expects(:increment)
        span.expects(:set_tag)

        MockArBase.class_eval do
          def self.delete(*args)
            yield if block_given?
            args.empty? ? :super : args # The behaviour doesn't matter here, I just want to get at the data
          end
        end
      end

      it 'calls super' do
        assert_equal :super, subject.delete
      end

      it 'passes arguments through' do
        assert_equal [:arg], subject.delete(:arg)
      end

      it 'passes block through' do
        obj = mock
        obj.expects(:call)
        subject.delete { obj.call }
      end
    end

    describe :delete do
      before do
        MockArBase.class_eval do
          define_singleton_method(:delete) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:delete"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#delete")
        subject.delete
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.delete
      end
    end

    describe :delete_all do
      before do
        MockArBase.class_eval do
          define_singleton_method(:delete_all) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:delete_all"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#delete_all")
        subject.delete_all
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.delete_all
      end
    end

    describe :update_all do
      before do
        MockArBase.class_eval do
          define_singleton_method(:update_all) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:update_all"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#update_all")
        subject.update_all
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.update_all
      end
    end

    describe :decrement_counter do
      before do
        MockArBase.class_eval do
          define_singleton_method(:decrement_counter) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:decrement_counter"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#decrement_counter")
        subject.decrement_counter
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.decrement_counter
      end
    end

    describe :increment_counter do
      before do
        MockArBase.class_eval do
          define_singleton_method(:increment_counter) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:increment_counter"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#increment_counter")
        subject.increment_counter
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.increment_counter
      end
    end

    describe :update_counters do
      before do
        MockArBase.class_eval do
          define_singleton_method(:update_counters) { :super }
        end
      end

      it 'sends to datadog' do
        statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:update_counters"])
        span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#update_counters")
        subject.update_counters
      end

      it 'calls super' do
        statsd_client.expects(:increment)
        span.expects(:set_tag)
        assert_equal :super, subject.update_counters
      end
    end
  end

  describe ActiveRecord::Relation do
    describe 'for an uninstrumented relation' do
      let(:subject) { MockRelation.new(Object) }

      before do
        ActiveRecord::Relation.class_eval do
          define_method(:delete) { :super }
        end
      end

      it 'does not send stats' do
        subject.delete
      end

      it 'calls super' do
        assert_equal :super, subject.delete
      end
    end

    describe 'for a instrumented relation' do
      let(:subject) { MockRelation.new(MockObject) }

      before(:each) do
        span.expects(:get_tag).with("zendesk.ar_skip_callback").returns(nil)
      end

      describe 'does not override existing behavior' do
        before do
          statsd_client.expects(:increment)
          span.expects(:set_tag)

          Arsi::Relation.class_eval do # Mock has no connection so need to stub here
            def delete_all(*args)
              yield if block_given?
              args.empty? ? :super : args # The behaviour doesn't matter here, I just want to get at the data
            end
          end
        end

        it 'calls super' do
          assert_equal :super, subject.delete_all
        end

        it 'passes arguments through' do
          assert_equal [:arg], subject.delete_all(:arg)
        end

        it 'passes block through' do
          obj = mock
          obj.expects(:call)
          subject.delete_all { obj.call }
        end
      end

      describe :delete do
        before do
          ActiveRecord::Relation.class_eval do
            define_method(:delete) { :super }
          end
        end

        if Rails::VERSION::MAJOR == 4
          it 'sends to datadog' do
            statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:delete"])
            span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#delete")
            subject.delete
          end

          it 'calls super' do
            statsd_client.expects(:increment)
            span.expects(:set_tag)
            assert_equal :super, subject.delete
          end
        else # Rails 5.2+
          it 'does not send stats' do
            subject.delete
          end

          it 'calls super' do
            assert_equal :super, subject.delete
          end
        end
      end

      describe :delete_all do
        before do
          Arsi::Relation.class_eval do # Mock has no connection so need to stub here
            define_method(:delete_all) { :super }
          end
        end

        it 'sends to datadog' do
          statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:delete_all"])
          span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#delete_all")
          subject.delete_all
        end

        it 'calls super' do
          statsd_client.expects(:increment)
          span.expects(:set_tag)
          assert_equal :super, subject.delete_all
        end
      end

      describe :update_all do
        before do
          ActiveRecord::Relation.class_eval do
            define_method(:update_all) { :super }
          end
        end

        it 'sends to datadog' do
          statsd_client.expects(:increment).with('ar_skip_callback', tags: ["class:MockObject", "method:update_all"])
          span.expects(:set_tag).with("zendesk.ar_skip_callback", "MockObject#update_all")
          subject.update_all
        end

        it 'calls super' do
          statsd_client.expects(:increment)
          span.expects(:set_tag)
          assert_equal :super, subject.update_all
        end
      end
    end
  end
end
