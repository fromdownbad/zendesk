require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'LibmemcachedStore' do
  it 'has instrumentation disabled' do
    refute ActiveSupport::Cache::LibmemcachedStore.new.send(:instrument?)
  end
end
