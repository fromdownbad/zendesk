require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'ArTablesInString' do
  describe ActiveRecord::Associations do
    subject do
      Account.where(nil)
    end

    it "finds nothing for nothing" do
      assert_equal [], subject.tables_in_string("")
    end

    it "extracts real tables" do
      assert_equal ["tickets"], subject.tables_in_string("tickets.xxx = 1")
      assert_equal ["tickets"], subject.tables_in_string("select tickets.*")
      assert_equal ["tickets"], subject.tables_in_string("`tickets`.`xxx` = 1")
      assert_equal ["tickets"], subject.tables_in_string("'tickets'.'xxx' = 1")
      assert_equal ["tickets"], subject.tables_in_string("select 'tickets'.*")
      assert_equal ["ticket_jobs"], subject.tables_in_string("'ticket_jobs'.'xxx' = 1")
      assert_equal ["ticket_jobs", "user_datas"], subject.tables_in_string("'ticket_jobs'.'xxx' = 1 and user_datas.foo = 2")
    end

    it "ignores noise" do
      assert_equal [], subject.tables_in_string("raw_sql_.xxx = 1")
      assert_equal ["tickets"], subject.tables_in_string("tickets.email = 'foos.bars@gmail.com'")
      assert_equal ["tickets"], subject.tables_in_string("tickets.email = 'foo@gmails.com'")
      assert_equal ["tickets"], subject.tables_in_string("tickets.email = 'foos.balls.table@gmail.com'")
      assert_equal ["tickets"], subject.tables_in_string("tickets.email = 'email@users.co.uk'")
    end
  end
end
