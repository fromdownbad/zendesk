require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'ActiveRecordErrors' do
  fixtures :all
  describe ActiveModel::Errors do
    subject { User.new }

    describe "#to_json" do
      before do
        @identity = UserIdentity.new
        @identity.account = accounts(:minimum)
        @identity.user = users(:minimum_admin)

        refute @identity.valid?
      end

      it "returns a default JSON serialization" do
        exp = [["value", "cannot be blank"],
               ["value", " is not properly formatted"]]
        assert_equal exp, JSON.parse(@identity.errors.to_json)
      end

      it "has a more verbose structure when passed :version => 2" do
        assert_equal({
          "error"       => "RecordInvalid",
          "description" => "Record validation errors",
          "details"     => {
            "value" => [
              { "error" => "BlankValue", "description" => "cannot be blank" },
              { "description" => " is not properly formatted"}
            ]
          }
        }, JSON.parse(@identity.errors.to_json(version: 2)))
      end
    end

    describe "#all_on" do
      it "returns empty array" do
        assert_equal [], subject.errors[:base]
      end

      it "returns one" do
        subject.errors.add(:base, "xxx")
        assert_equal ["xxx"], subject.errors[:base]
      end

      it "returns multiple" do
        subject.errors.add(:base, "xxx")
        subject.errors.add(:base, "yyy")
        assert_equal ["xxx", "yyy"], subject.errors[:base]
      end
    end
  end
end
