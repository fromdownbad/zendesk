require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe ActiveRecord::Base do
  describe :blank? do
    it 'does no go through a crazy stack trace' do
      u = User.new
      u.expects(:respond_to?).never
      refute u.blank?
    end
  end
end
