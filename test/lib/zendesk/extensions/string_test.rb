require_relative "../../../support/test_helper"

SingleCov.covered!

describe String do
  describe "#tokenize" do
    it "works" do
      assert_equal 'foo bar'.tokenize.size, 2
      assert_equal 'foo;bar baz,hest'.tokenize.size, 4
      assert_equal 'foo bar bas:fisk gong:"wee haw"'.tokenize.size, 4
      assert_equal 'foo bar bas:fisk gong:"wee haw";splert'.tokenize.size, 5
    end
  end

  describe "#parenthesize" do
    it "works" do
      assert_equal({ foo: 'bar', baz: [1, 2, 3] }.to_json.parenthesize, '(' + { foo: 'bar', baz: [1, 2, 3] }.to_json + ')')
    end
  end

  describe "#to_json" do
    it "escapes control characters" do
      vertical_tab = "\v"
      string = "This is a string #{vertical_tab}"
      assert_equal %("This is a string \\u000B"), string.to_json
    end
  end

  describe "#pluralize" do
    it "works" do
      assert_equal 'minutes', 'minute'.pluralize
    end

    it "works with 0" do
      assert_equal '0 minutes', 'minute'.pluralize(0)
    end

    it "works with 1" do
      assert_equal '1 minute', 'minute'.pluralize(1)
    end

    it "works with big numbers" do
      assert_equal '410 minutes', 'minute'.pluralize(410)
    end

    it "works in spanish" do
      assert_equal '2 momentos', 'minute'.pluralize(2, 'momentos')
    end
  end

  describe "#snakecase" do
    it "is defined and working" do
      assert_equal "basic_auth", "BasicAuth".snakecase
      assert_equal "basic_auth/xxx", "BasicAuth::XXX".snakecase
      assert_equal "basic_auth", "BasicAuth".snake_case
      assert_equal "basic_auth/xxx", "BasicAuth::XXX".snake_case
    end
  end

  describe "#clean_separators" do
    subject { "Hello\r\nThis\u2028should be\u2029cleaned up." }

    it "cleans input with newline by default" do
      assert_equal "Hello\nThis\nshould be\ncleaned up.", subject.clean_separators
    end

    it "cleans input with newline" do
      assert_equal "Hello\nThis\nshould be\ncleaned up.", subject.clean_separators("\n")
    end

    it "cleans input with empty string" do
      assert_equal "HelloThisshould becleaned up.", subject.clean_separators('')
    end
  end

  describe "#strip_all" do
    subject { "\u200B \u200D No Leading Or Trailing Whitespaces \u200C \u202F \uFEFF" }

    it "removes all zero width whitespaces" do
      assert_equal "No Leading Or Trailing Whitespaces", subject.strip_all
    end
  end
end
