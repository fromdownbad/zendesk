require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'Array' do
  # This is not a real test, just making sure we did not screw up completely
  describe "#to_xml" do
    it "shows empty" do
      expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<nil-classes type=\"array\" count=\"0\"/>\n"
      assert_equal expected, [].to_xml
    end

    it "shows nested" do
      expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<records type=\"array\" count=\"1\">\n  <record>\n    <x type=\"array\" count=\"1\">\n      <x>\n        <x type=\"integer\">1</x>\n      </x>\n    </x>\n  </record>\n</records>\n"
      assert_equal expected, [{x: [{x: 1}]}].to_xml
    end
  end
end
