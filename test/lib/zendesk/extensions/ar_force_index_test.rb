require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'ActiveRecordForceIndex' do
  describe "#use_index" do
    it "allows me to specify an appropriate index" do
      result = Event.use_index(:index_events_on_ticket_id_and_type).to_sql
      assert_includes result, 'events USE INDEX(index_events_on_ticket_id_and_type)'
    end
  end
end
