require_relative "../../../support/test_helper"

SingleCov.covered!

class MobileAppsAccessTestController < ApplicationController
  skip_before_action :authenticate_user
  skip_before_action :verify_current_account

  allow_zendesk_mobile_app_access only: "unblocked"

  def unblocked
    head :ok
  end

  def blocked
    head :ok
  end
end

class MobileAppsAccessControllerTest < ActionController::TestCase
  tests MobileAppsAccessTestController
  use_test_routes

  describe "#prevent_zendesk_mobile_app_access" do
    describe "when an action is hit by a Zendesk mobile app" do
      before { @request.stubs(:user_agent).returns("Zendesk for iPhone 3.0") }

      describe "and the action is not permitted" do
        it "blocks" do
          get :blocked
          assert_response 450
        end
      end

      describe "and the action is permitted" do
        it "does not block" do
          get :unblocked
          assert_response :ok
        end
      end
    end

    describe "when a blocked action is hit by a regular browser" do
      before { @request.stubs(:user_agent).returns("MegaBrowse 2.0") }

      it "does not block" do
        get :blocked
        assert_response :ok
      end
    end
  end
end
