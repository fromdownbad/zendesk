require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'RackRequestInternalClient' do
  describe Rack::Request do
    describe "mixed-in methods" do
      before do
        @request = Rack::Request.new({ })
      end

      it "responds to new methods" do
        %w[
          internal_client?
          help_center?
          signed_internal_request?
        ].each do |method|
          assert @request.respond_to? method
        end
      end
    end

    describe "#internal_client?" do
      before do
        @request = Rack::Request.new({
          'HTTP_HOST' => 'test.rb',
          'rack.url_scheme' => 'http',
          'REQUEST_METHOD' => 'post'
        })
      end

      it "returns true when user agent is HelpCenter" do
        @request.stubs(help_center?: true)
        assert @request.internal_client?
      end

      it "returns true when request is signed" do
        @request.stubs(signed_internal_request?: true)
        assert @request.internal_client?
      end

      it "returns false when user agent is not HelpCenter or not signed" do
        @request.stubs(
          help_center?: false,
          signed_internal_request?: false
        )
        refute @request.internal_client?
      end
    end

    describe "#help_center?" do
      before do
        @request = Rack::Request.new({ })
      end

      it "returns false when the user is not HelpCenter" do
        @request.stubs(user_agent: 'JamesBond')
        refute @request.help_center?
      end

      it "returns true when the user is HelpCenter" do
        @request.stubs(user_agent: 'HelpCenter')
        assert @request.help_center?
      end
    end

    describe "#signed_internal_request?" do
      let(:env) do
        {
          'HTTP_HOST'       => 'mondocam.localhost:3000/api/v2/requests',
          'rack.url_scheme' => 'http',
          'REQUEST_METHOD'  => 'post'
        }
      end

      before do
        @request = Rack::Request.new(env)
      end

      describe "when there is an oauth token" do
        let(:token) { Zendesk::OAuth::Token.new }
        let(:env) do
          {
            'HTTP_HOST' => 'mondocam.localhost:3000/api/v2/requests',
            'rack.url_scheme' => 'http',
            Rack::OAuth2::Server::Resource::ACCESS_TOKEN => token
          }
        end

        describe "and that token is internal" do
          before do
            token.stubs(internal?: true)
          end

          it "returns true" do
            assert @request.signed_internal_request?
          end
        end

        describe "and that token is not internal" do
          it "returns false" do
            refute @request.signed_internal_request?
          end
        end
      end

      describe "when no system user signature is present" do
        before do
          @request.stubs(headers: { })
        end

        it "returns false" do
          refute @request.signed_internal_request?
        end
      end

      describe "when the system user signature is invalid" do
        let(:env) do
          {
            'HTTP_HOST'       => 'mondocam.localhost:3000/api/v2/requests',
            'rack.url_scheme' => 'http',
            'Date'            => 'Wed, 13 Nov 2013 13:32:36 -0800',
            'X-Zendesk-API'   => 'zendesk:XXXXXXXXXXXXXXXXXXXXXXXXXXX='
          }
        end

        it "returns false" do
          refute @request.signed_internal_request?
        end
      end

      describe "when the system user signature is valid" do
        let(:env) do
          {
            'HTTP_HOST'       => 'mondocam.localhost:3000',
            'rack.url_scheme' => 'http',
            'PATH_INFO'       => '/api/v2/requests',
            'REQUEST_METHOD'  => 'post',
            'Date'            => 'Wed, 13 Nov 2013 13:32:36 -0800',
            'X-Zendesk-API'   => 'zendesk:aHUKtt3hVFfMU4dXoIOaCx7D6hU='
          }
        end

        it "returns true" do
          assert_predicate @request, :signed_internal_request?
        end

        describe "and the request used the default port for the scheme" do
          let(:env) do
            {
              'HTTP_HOST'      => 'mondocam.localhost:443', # for rack 1.1
              'HTTPS'          => 'on',
              'REQUEST_METHOD' => 'post',
              'SERVER_PORT'    => 443,
              'PATH_INFO'      => '/api/v2/requests',
              'Date'           => 'Wed, 13 Nov 2013 13:32:36 -0800',
              'X-Zendesk-API'  => 'zendesk:Wo7VHJRj7OtTLjkWIrUeRKO6w4s='
            }
          end

          it "returns true" do
            assert_predicate @request, :signed_internal_request?
          end
        end

        describe "and the request used the implied port for the scheme" do
          let(:env) do
            {
              'HTTP_HOST'      => 'mondocam.localhost:443',
              'PATH_INFO'      => '/api/v2/requests',
              'HTTPS'          => 'on',
              'REQUEST_METHOD' => 'post',
              'Date'           => 'Wed, 13 Nov 2013 13:32:36 -0800',
              'X-Zendesk-API'  => 'zendesk:Wo7VHJRj7OtTLjkWIrUeRKO6w4s='
            }
          end

          it "returns true" do
            assert_predicate @request, :signed_internal_request?
          end
        end
      end
    end

    describe "#request_signature" do
      let(:env) do
        {
          'X-Zendesk-API'   => 'username:signature',
          'rack.url_scheme' => 'http'
        }
      end

      before do
        @request = Rack::Request.new(env)
      end

      it "extracts the request's signature from the header's X-Zendesk-API key" do
        assert_equal 'username:signature', @request.send(:request_signature)
      end

      describe "when the X-Zendesk-API key is not present" do
        let(:env) { {} }

        it "returns an empty-string" do
          assert_equal '', @request.send(:request_signature)
        end
      end
    end

    describe "#request_date" do
      let(:env) do
        { 'Date' => 'Wed, 13 Nov 2013 13:32:36 -0800' }
      end

      before do
        @request = Rack::Request.new(env)
      end

      it "extracts the request's timestamp from the header's Date key" do
        assert_equal 'Wed, 13 Nov 2013 13:32:36 -0800', @request.send(:request_date)
      end

      describe "when the Date key is not present" do
        let(:env) { {} }

        it "returns an empty-string" do
          assert_equal '', @request.send(:request_date)
        end
      end
    end

    describe "#request_host" do
      let(:env) { {} }

      before do
        @request = Rack::Request.new(env)
      end

      describe "when the request URL's port is not the default port for the scheme" do
        let(:env) do
          {
            'HTTP_HOST'       => 'mondocam.localhost:3000',
            'rack.url_scheme' => 'http',
            'PATH_INFO'       => '/api/v2/requests'
          }
        end

        it "returns the host name with the port information" do
          assert_equal 'mondocam.localhost:3000', @request.send(:request_host)
        end
      end

      describe "when the request URL's port is the default port for the scheme" do
        let(:env) do
          {
            'HTTP_HOST' => 'mondocam.localhost:443',
            'PATH_INFO' => '/api/v2/requests',
            'HTTPS'     => 'on'
          }
        end

        it "returns the host name without the port information" do
          assert_equal 'mondocam.localhost', @request.send(:request_host)
        end
      end

      describe "when the request URL's port is implied" do
        let(:env) do
          {
            'HTTP_HOST'       => 'mondocam.localhost:80',
            'PATH_INFO'       => '/api/v2/requests',
            'rack.url_scheme' => 'http'
          }
        end

        it "returns the host name without the port information" do
          assert_equal 'mondocam.localhost', @request.send(:request_host)
        end
      end

      describe "with a modern URI" do
        let(:env) do
          {
            'HTTP_HOST'       => 'mondocam.localhost:80',
            'PATH_INFO'       => '/requests/new',
            'QUERY_STRING'    => 'foo=(bar|baz)',
            'rack.url_scheme' => 'http',
          }
        end

        it "still works" do
          assert_equal 'mondocam.localhost', @request.send(:request_host)
        end
      end
    end

    describe "#signature" do
      let(:env) do
        {
          'HTTP_HOST'       => 'mondocam.localhost:80',
          'PATH_INFO'       => '/api/v2/requests',
          'REQUEST_METHOD'  => 'post',
          'rack.url_scheme' => 'http',
          'Date'            => 'Wed, 13 Nov 2013 13:32:36 -0800',
          'X-Zendesk-API'   => 'username:signature'
        }
      end

      before do
        @request = Rack::Request.new(env)

        Zendesk::Configuration.expects(:dig).with(:system_user_auth, 'zendesk', :signed).returns('secret')
      end

      it "uses the secret for the 'zendesk' user" do
        @request.send(:signature)
      end

      it "computes the signature using the correct arguments" do
        Zendesk::SystemUser::Authentication.expects(:signature).
          with('secret', 'post', 'mondocam.localhost', '/api/v2/requests',
            'Wed, 13 Nov 2013 13:32:36 -0800')
        @request.send(:signature)
      end

      it "prefixes the computed signature with 'zendesk:'" do
        assert_match(/zendesk\:/, @request.send(:signature))
      end
    end
  end
end
