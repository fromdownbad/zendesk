require_relative '../../../support/test_helper'

SingleCov.covered!

class RackMultipartParserTest < ActionDispatch::IntegrationTest
  let(:account) { accounts(:support) }

  it 'does not blow up if there is no name in a multipart request part' do
    uri = URI.join(account.url, api_v2_uploads_path)
    uri.query = 'filename=test.pdf'
    boundary = 'AAA'

    body = <<~STR.gsub("\n", "\r\n")

      --#{boundary}
      Content-Disposition: file
      Content-Type: application/pdf

      weird_pdf_data;
      --#{boundary}--

    STR

    headers = { 'Content-Type' => "multipart/form-data; boundary=#{boundary}", 'Content-Length' => body.bytesize }

    # Will raise exception without patch for Rails 4
    # NoMethodError: undefined method `force_encoding' for nil:NilClass
    # /bundle/gems/rack-1.6.8/lib/rack/multipart/parser.rb:201:in `tag_multipart_encoding'#
    post uri.to_s, params: body, headers: headers
    assert_response :created, response.body
  end
end
