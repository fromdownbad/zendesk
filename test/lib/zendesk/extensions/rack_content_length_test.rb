require_relative '../../../support/test_helper'

SingleCov.covered!

class RackContentLengthTest < ActionDispatch::IntegrationTest
  fixtures :accounts, :users

  let(:account) { accounts(:support) }
  let(:user) { users(:support_reports_end_user1) }

  before { login(user) }

  it 'adds the Content-Length header even if its Transfer-Encoding: chunked' do
    uri = URI.join(account.url, api_v2_uploads_path)
    uri.query = 'filename=test.txt'

    body = 'uploaded_data'
    headers = { 'Content-Type' => 'application/binary', 'Content-Length' => body.bytesize }

    post uri.to_s, params: body, headers: headers

    assert_response :created
    assert_includes response.headers, 'Content-Length'
  end
end
