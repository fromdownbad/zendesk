require_relative "../../../support/test_helper"

SingleCov.not_covered!

describe 'ArCollectionSpeedPatch' do
  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }

  describe "delegate fix" do
    it "it makes simple methods skip arel checks" do
      tickets = account.tickets
      tickets.expects(:arel).never
      tickets.expects(:scope).never
      tickets.index(1).must_be_nil
    end
  end

  describe "collection merge fix" do
    let(:ticket) { tickets(:minimum_1) }

    it "makes collection calls that we often use when pre-loading skip scope calls which are slow" do
      ActiveRecord::Associations::Association.any_instance.expects(:scope).never
      refute ticket.events.loaded?
    end

    it "does not break default scopes" do
      account.suspended_tickets.to_sql.must_include "`suspended_tickets`.`deleted` = 0"
    end

    it "keeps methods defined in scopes" do
      Subscription.first.payments.any_paid?
    end

    it "can query with collection proxy" do
      account.tickets.where(requester: account.users).to_sql.must_include "`tickets`.`requester_id` IN (SELECT `users`.`id` FROM `users` WHERE `users`.`account_id` = #{account.id} AND `users`.`is_active` = 1)"
    end

    it "can find archived" do
      audit = ticket.audits.first
      archive_and_delete(ticket)
      # run this twice to make sure we are not modifying the association or double-extending
      2.times do
        audits = account.tickets.find_by_nice_id(ticket.nice_id).audits
        audits.find(audit.id).must_equal audit
        audits.find(audit.id).must_equal audit

        # it is unclear if the expected result is tested anywhere...
        audits.send(:merge_association!) if RAILS4 # force the merge

        class << audits
          ancestors.count(ZendeskArchive::HasManyExtension).must_equal 1
        end
      end
    end
  end

  describe "arel  to_s patch" do
    it "does not trigger inspect on any activerecord object" do
      ActiveRecord::Base.expects(:inspect).never
      [accounts(:minimum).tickets.first].pre_load(:collaborations)
    end

    it "is a simple / static response" do
      Ticket.arel_table[:id].to_s.must_equal "<struct Arel::Attributes::Attribute ...>"
    end
  end
end
