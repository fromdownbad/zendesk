require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'ArPrimaryKey' do
  it 'uses id' do
    assert_equal 'id', User.primary_key
  end
end
