require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'ArUpdateAllWithUpdatedAt' do
  fixtures :accounts, :users

  describe ActiveRecord::Base do
    def assert_all_updated(scope)
      time = scope.pluck(:updated_at).uniq
      assert_equal 1, time.size
      assert_in_delta Time.now.to_i, time.first.to_i, 2
    end

    it "updates all" do
      Account.without_arsi.update_all_with_updated_at("name = 'foo'")
      assert_equal ["foo"], Account.pluck(:name).uniq
      assert_all_updated(Account)
    end

    it "updates all via hash" do
      Account.without_arsi.update_all_with_updated_at(name: 'foo')
      assert_equal ["foo"], Account.pluck(:name).uniq
      assert_all_updated(Account)
    end

    it "updates all with conditions" do
      Account.without_arsi.update_all_with_updated_at("name = 'foo'", "id != 1")
      assert_equal ["foo", "support account"], Account.pluck(:name).uniq
    end

    it "updates all with conditions and hash" do
      Account.without_arsi.update_all_with_updated_at({name: 'foo'}, "id != 1")
      assert_equal ["foo", "support account"], Account.pluck(:name).uniq
    end

    it "works on scopes" do
      Account.where("id != 1").without_arsi.update_all_with_updated_at(name: 'foo')
      assert_equal ["foo", "support account"], Account.pluck(:name).uniq
    end

    it "works on relations" do
      users(:with_groups_end_user).update_column(:updated_at, 10.days.ago)
      accounts(:minimum).users.update_all_with_updated_at(name: 'foo')
      assert_equal ["foo"], accounts(:minimum).users.pluck(:name).uniq

      assert_all_updated(accounts(:minimum).users)

      # others unchanged
      Rails.cache.clear # kasket
      user = users(:with_groups_end_user).reload
      assert_equal "with_groups_end_user", user.name
      assert_not_in_delta Time.now.to_i, user.updated_at.to_i, 10
    end
  end
end
