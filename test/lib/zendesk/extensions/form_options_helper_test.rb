require_relative "../../../support/test_helper"
require 'action_view/helpers'

SingleCov.covered!

describe ActionView::Helpers::FormOptionsHelper do
  fixtures :users, :forums

  def current_user;
    users(:minimum_admin);
  end

  def current_account;
    current_user.account;
  end

  describe "#options_from_collection_for_select_with_max_size" do
    before { @forum = forums(:solutions) }

    describe "with a short forum name" do
      before { @forum.stubs(:name).returns("This is a short name") }
      it "does not truncate the name and not include a title attribute" do
        assert_equal %(<option value="#{@forum.id}">This is a short name</option>),
          options_from_collection_for_select([@forum], :id, :name, nil, truncate: 100)
      end
    end

    describe "with a long forum name" do
      before { @forum.stubs(:name).returns("This is a very long forum name to test if we create a select with a truncated name with less than 100 characters") }
      it "truncates the name to characters (100) and include a title attribute with the full name" do
        assert_equal %(<option title="This is a very long forum name to test if we create a select with a truncated name with less than 100 characters" value="#{@forum.id}">This is a very long forum name to test if we create a select with a truncated name with less than...</option>),
          options_from_collection_for_select([@forum], :id, :name, nil, truncate: 100)
      end
    end

    describe "with a long forum name with html characters" do
      before { @forum.stubs(:name).returns("<p>This is a very long forum name to test if we create a select with a truncated name with less than 100 characters</p>") }
      it "truncates the name to 100 chars, include a title attribute with the full name and escape the html values" do
        assert_equal "<option title=\"&lt;p&gt;This is a very long forum name to test if we create a select with a truncated name with less than 100 characters&lt;/p&gt;\" value=\"#{@forum.id}\">&lt;p&gt;This is a very long forum name to test if we create a select with a truncated name with less t...</option>",
          options_from_collection_for_select([@forum], :id, :name, nil, truncate: 100)
      end
    end

    describe "html attributes" do
      before { @forum.stubs(:name).returns("Forum name") }

      it "adds a 'selected' attribute" do
        expected = "<option title=\"Forum name\" selected=\"selected\" value=\"#{@forum.id}\">Forum...</option>"

        assert_equal(expected, options_from_collection_for_select([@forum], :id, :name, @forum.id, truncate: 8))
      end

      it "adds a 'disabled' attribute" do
        expected = "<option title=\"Forum name\" disabled=\"disabled\" value=\"#{@forum.id}\">Forum...</option>"

        assert_equal(expected, options_from_collection_for_select([@forum], :id, :name, { disabled: @forum.id }, truncate: 8))
      end

      it "adds both a 'selected' and a 'disabled' attribute" do
        expected = "<option title=\"Forum name\" selected=\"selected\" disabled=\"disabled\" value=\"#{@forum.id}\">Forum...</option>"

        assert_equal(expected, options_from_collection_for_select([@forum], :id, :name, { selected: @forum.id, disabled: @forum.id }, truncate: 8))
      end
    end
  end
end
