require_relative "../../../support/test_helper"

SingleCov.covered!

describe Net::HTTP do
  describe 'open_timeout' do
    it 'is set by default' do
      Net::HTTP.new('example.com', 80).open_timeout.wont_be_nil
    end
  end
end
