require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'Unescape' do
  describe "URI::unescape" do
    it "unescapes ordinary text containing no special chars" do
      assert_equal "lorem_ipsum_dolor_sit_amet", URI.unescape("lorem_ipsum_dolor_sit_amet")
    end

    it "unescapes %XX sequences containing no special chars" do
      assert_equal "lorem_ipsum_dolor_sit_amet", URI.unescape("lorem%5Fipsum%5Fdolor%5Fsit%5Famet")
    end

    it "unescapes invalid UTF8 %XX sequences to replacement char" do
      assert_equal "\ufffd", URI.unescape('%ff')
      assert_equal "Hello, world\ufffd", URI.unescape('Hello, world%ff')
      assert_equal "ABC\ufffdXYZ", URI.unescape('ABC%ffXYZ')
    end

    it "unescapes valid UTF8 %XX sequences to Unicode chars" do
      assert_equal '你好', URI.unescape('%E4%BD%A0%E5%A5%BD')
    end

    it "unescapes valid Unicode chars mixed with valid UTF8 %XX sequences to Unicode chars" do
      assert_equal '你好, 再见', URI.unescape('%E4%BD%A0%E5%A5%BD, 再见')
    end

    it "unescapes string containing valid and invalid UTF8 %XX sequences" do
      assert_equal '你好, �', URI.unescape('%E4%BD%A0%E5%A5%BD, %FF')
    end

    it "unescapes invalid UTF8 %XX sequence mixed with valid Unicode chars to replacement char" do
      assert_equal '你好, �', URI.unescape('你好, %FF')
    end

    it "unescapes invalid UTF8 directly in string mixed with %XX sequence" do
      assert_equal '你好, �', URI.unescape("%E4%BD%A0%E5%A5%BD, \xFF")
    end
  end
end
