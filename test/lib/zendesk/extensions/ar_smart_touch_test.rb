require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'ArSmartTouch' do
  def assert_change(what)
    old = what.call
    yield
    assert_not_equal old, what.call
  end

  def assert_no_change(what)
    old = what.call
    yield
    assert_equal old, what.call
  end

  describe "#smart_touch" do
    before do
      Timecop.freeze(Time.at(Time.now.to_i))
      @model = Currency.create! do |c|
        c.rate = 1
        c.code = 'XX'
        c.rate_on = Time.now
        c.updated_at = 1.day.ago
      end
      @updated_at = lambda { @model.updated_at.to_i }
    end

    it "dos nothing for new records" do
      @model = Currency.new
      @model.smart_touch
      refute @model.updated_at
    end

    it "sets updated_at to now" do
      assert_change(@updated_at) { @model.smart_touch }
      assert_equal @model.updated_at, Time.now
      assert_equal @model.reload.updated_at, Time.now
    end

    it "does not update updated_at if the model is fresh" do
      @model.smart_touch
      # this is necessary because this test need to change the value without reloading the model
      @model.class.where(id: @model.id).update_all(updated_at: 1.day.ago)
      @model.smart_touch
      assert_equal @model.reload.updated_at, 1.day.ago
    end

    it "does not run validations/callbacks" do
      @model.rate = ' '
      refute @model.valid?
      assert_change(@updated_at) { @model.smart_touch }
    end

    it "does not update other records" do
      other = Currency.create! do |c|
        c.rate = 1
        c.code = 'XX2'
        c.rate_on = Time.now
        c.updated_at = 1.day.ago
      end

      assert_no_change(@updated_at) do
        other.smart_touch
        @model.reload
      end
    end

    it "raises with a model that does not have updated_at" do
      @model.expects(:updated_at).raises "XXX"
      assert_raise(RuntimeError) do
        @model.smart_touch
      end
    end

    it "clears kasket" do
      Timecop.freeze(Time.at(Time.now.to_i))
      @model = users(:minimum_end_user)
      User.where(id: @model.id).update_all(updated_at: 1.day.ago)
      Rails.cache.clear
      assert_equal 1.day.ago, @model.reload.updated_at
      @model.smart_touch
      assert_equal Time.current, @model.reload.updated_at
    end
  end

  describe "#touch_without_callbacks" do
    it "can touch other columns" do
      model = Currency.create! do |c|
        c.rate = 1
        c.code = 'XX'
        c.rate_on = 1.day.ago
        c.updated_at = 1.day.ago
      end
      old_u = model.updated_at
      old_r = model.rate_on
      model.touch_without_callbacks(:rate_on)
      model.updated_at.must_equal old_u
      model.rate_on.wont_equal old_r
    end
  end
end
