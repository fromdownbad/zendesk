require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'APFragmentCacheKey' do
  describe "fragment_cache_key" do
    it "adds current translation_locale" do
      TranslationLocale.any_instance.expects(:cache_key).returns("translation_locales/1-23")
      assert_equal "views/translation_locales/1-23/#{Time.zone.utc_offset}/xxx", ApplicationController.new.fragment_cache_key("xxx")
    end

    it "adds simple locale" do
      ::I18n.stubs(locale: "XX")
      ::I18n.stubs(translation_locale: nil)
      assert_equal "views/XX/#{Time.zone.utc_offset}/xxx", ApplicationController.new.fragment_cache_key("xxx")
    end

    it "uses full url without protocol for hash" do
      controller = ApplicationController.new

      account = Account.new
      account.subdomain = "sub"
      controller.stubs(current_account: account)

      controller.expects(:url_for).with(id: 1, host: "sub", account_id: account.id).returns "https://sub.foo.com/xxx/1"
      assert_includes controller.fragment_cache_key(id: 1), "sub.foo.com/xxx/1"
    end
  end

  describe "scoped_cache_key" do
    describe "the key contains account id and cache version when" do
      let(:account) { accounts(:minimum) }
      let(:user)    { users(:minimum_agent) }

      it "is a user" do
        assert_includes(user.scoped_cache_key(:identities), "/#{account.id}/#{account.settings.account_cache_version}")
      end

      it "is an account" do
        assert_includes(account.scoped_cache_key(:rules), "/#{account.id}/#{account.settings.account_cache_version}")
      end
    end
  end

  describe "cache_key" do
    it "includes account_id in cache_keys" do
      u = users(:minimum_agent)
      assert_includes(u.cache_key, "/#{u.account.id}")
    end

    it "includes created_at if account isn't available" do
      u = users(:minimum_agent)
      u.account = nil
      assert_includes(u.cache_key, "/#{u.created_at.to_f}")
    end

    it "includes current time if neither account nor created_at is defined" do
      u = users(:minimum_agent)
      u.account = nil
      u.created_at = nil
      Timecop.freeze(Time.now) do
        assert_includes(u.cache_key, "/#{Time.now.to_f}")
      end
    end
  end
end
