require_relative "../../../support/test_helper"

SingleCov.covered!

class RackPostFixTest < ActionDispatch::IntegrationTest
  it "does not blow up with weird headers sent from blackberry/webos simulator" do
    headers = {"CONTENT_TYPE" => "multipart/form-data; boundary=----WebKitFormBoundaryFJA6Tsy7iPpIVCxO", "GATEWAY_INTERFACE" => "CGI/1.1",
               "PATH_INFO" => "/home", "QUERY_STRING" => "", "REMOTE_ADDR" => "10.10.16.62", "REMOTE_HOST" => "10.10.16.62", "REQUEST_METHOD" => "GET",
               "SCRIPT_NAME" => "", "SERVER_NAME" => "support.localhost", "SERVER_PORT" => "3000", "SERVER_PROTOCOL" => "HTTP/1.1", "SERVER_SOFTWARE" => "WEBrick/1.3.1 (Ruby/1.9.3/2012-04-20)",
               "HTTP_HOST" => "support.localhost:3000", "HTTP_ACCEPT_ENCODING" => "gzip, deflate",
               "HTTP_COOKIE" => "_zendesk_shared_session=eyJpZCI6IjEwMWEwZTIyMTJlZjQ3OGM2NzVmODg0YjUxZGUyZTNlIiwiYWNj%0Ab3VudCI6MSwidXBkYXRlZF9hdCI6MTM0OTgwMDAyMH0%3D%0A--c5918d7bb8363999312214cfbdb335bf27b9241b; _zendesk_session=BAh7DjoPc2Vzc2lvbl9pZEkiJTM3OGI3ODgzMzQzYWFlYzdmOTEzZDk5MDU4OTA2YzdhBjoGRVQ6DGFjY291bnRpBkkiDmlzX21vYmlsZQY7BkZGSSITd2FyZGVuLm1lc3NhZ2UGOwZGewA6EHNvdXJjZV9wYWdlSSIKL2hvbWUGOwZGOhBfY3NyZl90b2tlbkkiMUpUUkphZ05FRjNBUm9jT2hpTzIyR1ZlSFlXeU5WeDhxS2V5d1YrMlE0UnM9BjsGRjoTdmVyaWZpZWRfaHVtYW5USSIKZmxhc2gGOwZGSUM6J0FjdGlvbkNvbnRyb2xsZXI6OkZsYXNoOjpGbGFzaEhhc2h7BjoLbm90aWNlSUM6HkFjdGl2ZVN1cHBvcnQ6OlNhZmVCdWZmZXIiHVJlcXVlc3QgIzUgInFxcSIgY3JlYXRlZAY7BlQGOgpAdXNlZHsGOwxGOgtsb2NhbGVpBg%3D%3D--bc5f8d4b31a015b8fb6dac45ea58d35336360d3d", "HTTP_ORIGIN" => "http://support.localhost:3000", "HTTP_USER_AGENT" => "Mozilla/5.0 (hp-desktop; Linux; hpwOS/2.0; U; en-US) AppleWebKit/534.6 (KHTML, like Gecko) wOSBrowser/234.83 Safari/534.6 Desktop/1.0", "HTTP_ACCEPT" => "application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5", "HTTP_REFERER" => "http://support.localhost:3000/anonymous_requests/new", "HTTP_ACCEPT_LANGUAGE" => "en-us,en;q=0.5", "HTTP_ACCEPT_CHARSET" => "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
               "HTTP_VERSION" => "HTTP/1.1", "REQUEST_PATH" => "/"}
    headers.each { |k, v| header k, v }
    visit "/home"
  end
end
