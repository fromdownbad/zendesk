require_relative "../../support/test_helper"
require 'zendesk/radar_factory'
require 'zendesk/agent_collision'

SingleCov.covered!

describe Zendesk::AgentCollision do
  fixtures :accounts, :tickets

  before do
    @ticket = tickets(:minimum_1)
  end

  describe ".ticket_viewed_by_user_ids" do
    before do
      mock_redis = mock
      mock_redis.expects(:hgetall).with('presence:/minimum/ticket/1').returns('456.dsfjkhdsfkajh' => {
          userId: 456,
          clientId: 'dsfjkhdsfkajh',
          online: true,
          sentry: 'sentry1',
          userType: 2,
          userData: { name: 'John Doe' }
        }.to_json)
      mock_redis.expects(:hmget).with('sentry:/radar', 'sentry1').returns([{
        name: 'sentry1',
        expiration: (Time.now.to_i + 100) * 1000,
        host: "precise64",
        port: "8000"
      }.to_json])

      ::Radar::Client.any_instance.stubs(:redis).returns(mock_redis)
    end

    it "return user_ids from radar" do
      assert_equal [456], Zendesk::AgentCollision.ticket_viewed_by_user_ids(@ticket)
    end
  end
end
