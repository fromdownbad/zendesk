require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Events::AuditEvents::ArrayValueTruncation do
  let(:klass) { EmailCcChange }
  let(:array_value) do
    [
      'user_one@one.com',
      'user_two@two.com',
      'user_three@three.com'
    ]
  end

  describe '#truncate_value' do
    subject { Zendesk::Events::AuditEvents::ArrayValueTruncation.truncate_value(array_value, klass) }

    describe 'when serialized value string is under the truncation limit' do
      it 'does not truncate the value array' do
        assert_equal array_value, subject
      end
    end

    describe 'when serialized value string is over the truncation limit' do
      before { Zendesk::Extensions::TruncateAttributesToLimit.stubs(limit: limit) }

      describe 'when the truncated serialized value string ends with a complete string from the original array' do
        # e.g.
        # ['user_one@one.com', 'user_two@two.com', 'user_three@three.com']
        # With a serialization byte limit of 50, truncates to:
        # '--- \n- user_one@one.com\n- user_two@two.com'
        let(:limit) { 45 }

        it 'truncates the value array and appends the truncated value indicator' do
          assert_equal ['user_one@one.com', 'user_two@two.com', '...'], subject
        end
      end

      describe 'when the truncated serialized value string ends with an incomplete string from the original array' do
        # e.g.
        # ['user_one@one.com', 'user_two@two.com', 'user_three@three.com']
        # With a serialization byte limit of 43, truncates to:
        # '--- \n- user_one@one.com\n- user_two@two.c'
        let(:limit) { 43 }

        it 'truncates the value array, removes the incomplete string, and appends the truncated value indicator' do
          assert_equal ['user_one@one.com', '...'], subject
        end
      end

      describe 'when the truncated serialized value string ends with a "-" serialization delimiter' do
        # e.g.
        # ['user_one@one.com', 'user_two@two.com', 'user_three@three.com']
        # With a serialization byte limit of 47, truncates to:
        # '---\n- user_one@one.com\n- user_two@two.com\n-'
        let(:limit) { 47 }

        it 'truncates the value array, ignores the final serialization delimiter, and appends the truncated value indicator' do
          assert_equal ['user_one@one.com', 'user_two@two.com', '...'], subject
        end
      end

      describe 'DataDog metrics' do
        let(:limit) { 44 }

        it 'increments zendesk.event_changes.truncated DataDog metric' do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with(:truncated, tags: ["type:#{klass}"])
          assert_equal ['user_one@one.com', '...'], subject
        end

        describe 'when there is an error with the truncation' do
          it 'increments zendesk.event_changes.error DataDog metric' do
            YAML.stubs(:load).raises(StandardError) do
              Zendesk::StatsD::Client.any_instance.expects(:increment).with(:error, tags: ["error:StandardError", "type:#{klass}"])
              assert_equal ['user_one@one.com', '...'], subject
            end
          end
        end
      end
    end
  end
end
