require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::ExternalErrorTranslations' do
  class TranslatorTestClass
    extend Zendesk::ExternalErrorTranslations
  end

  describe '#translate_error' do
    it "handles MalformedCSVError with unclosed quoted field" do
      csv = "\"name\n"
      e = assert_raise(CSV::MalformedCSVError) { CSV.parse(csv) }
      with_fake_translation("txt.external_errors.csv.malformed_csv_error.unclosed_quoted_field", "unclosed {{line}}") do
        assert_equal "unclosed 1", TranslatorTestClass.translate_error(e.message, ENGLISH_BY_ZENDESK)
      end
    end

    it "handles MalformedCSVError with illegal quoting" do
      csv = "x\""
      e = assert_raise(CSV::MalformedCSVError) { CSV.parse(csv) }
      with_fake_translation("txt.external_errors.csv.malformed_csv_error.illegal_quoting", "illegal {{line}}") do
        assert_equal "illegal 1", TranslatorTestClass.translate_error(e.message, ENGLISH_BY_ZENDESK)
      end
    end

    it "handles MalformedCSVError with excessive field size" do
      csv = "name\n\"Banananananananan\nanananana\""
      e = assert_raise(CSV::MalformedCSVError) { CSV.parse(csv, field_size_limit: 2) }
      with_fake_translation("txt.external_errors.csv.malformed_csv_error.field_size_exceeded", "field size {{line}}") do
        assert_equal "field size 2", TranslatorTestClass.translate_error(e.message, ENGLISH_BY_ZENDESK)
      end
    end

    it "handles MalformedCSVError with unquoted field newline" do
      csv = "name,stuff,blah\nsome\"\r\nthing,is,broken"
      e = assert_raise(CSV::MalformedCSVError) { CSV.parse(csv) }
      with_fake_translation("txt.external_errors.csv.malformed_csv_error.unquoted_fields_forbid_newlines", "unquoted newline {{line}}") do
        assert_equal "unquoted newline 2", TranslatorTestClass.translate_error(e.message, ENGLISH_BY_ZENDESK)
      end
    end

    it "handles MalformedCSVError with missing or stray quote" do
      csv = "name,stuff,blah\n\"some\n\"thing,\"\nis,broken"
      e = assert_raise(CSV::MalformedCSVError) { CSV.parse(csv) }
      with_fake_translation("txt.external_errors.csv.malformed_csv_error.missing_or_stray_quote", "missing quote {{line}}") do
        assert_equal "missing quote 2", TranslatorTestClass.translate_error(e.message, ENGLISH_BY_ZENDESK)
      end
    end

    it "handles an unknown error" do
      e = StandardError.new "random unhandled error"
      with_fake_translation("txt.external_errors.unknown_error", "unknown {{message}}") do
        assert_equal "unknown random unhandled error", TranslatorTestClass.translate_error(e.message, ENGLISH_BY_ZENDESK)
      end
    end
  end
end
