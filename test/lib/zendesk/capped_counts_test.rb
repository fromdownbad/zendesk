require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Zendesk::CappedCounts do
  describe "CappedNum" do
    it "is comparable with a fixnum" do
      assert Zendesk::CappedCounts::CappedNum.new(1, 10) < 5
      assert Zendesk::CappedCounts::CappedNum.new(8, 10) > 5
    end

    it "responds to capped?" do
      assert Zendesk::CappedCounts::CappedNum.new(10, 10).capped?
      refute Zendesk::CappedCounts::CappedNum.new(0, 10).capped?
    end
  end
end
