require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Zendesk::Voice::AccountUpdater do
  fixtures :accounts
  before do
    @account = accounts(:minimum)
    @account_id = @account.id
    @balance = 100
    @reference = "test_reference"
    Account.expects(:find).with(@account_id).returns(@account)
    @account_updater = Zendesk::Voice::AccountUpdater.new(@account_id, @balance, @reference)
  end

  describe "#voice_usage_builder" do
    before do
      @current_balance = 100
      @account_updater.expects(:current_balance).returns(@current_balance)
      @adjustment = -10
      @account_updater.expects(:calculate_balance).with(@current_balance).returns(@adjustment)
      @account_updater.expects(:log).returns("log")
      @voice_usage = @account_updater.voice_usage_builder
    end

    it "sets usage_type to 'noop'" do
      assert_equal "noop", @voice_usage.usage_type
    end

    it "sets account_id to accounts id" do
      assert_equal @account_id, @voice_usage.account_id
    end

    it "sets voice_reference_id to reference" do
      assert_equal @reference, @voice_usage.voice_reference_id
    end

    it "sets units to adjustment amount" do
      assert_equal @adjustment, @voice_usage.units
    end
  end

  describe "#current_balance" do
    before do
      where_stub = stub
      @last_zuora_voice_usage = stub(balance: 10)

      ZendeskBillingCore::Zuora::VoiceUsage.expects(:where).returns(where_stub)
      where_stub.expects(:order).returns(stub(first: @last_zuora_voice_usage))
    end

    it "returns the current balance" do
      assert_equal 10, @account_updater.current_balance
    end
  end

  describe "#calculate_balance" do
    before do
      @current_balance = 10
    end
    it "returns the adjustment amount" do
      assert_equal 99990, @account_updater.calculate_balance(@current_balance)
    end
  end

  describe '#save!' do
    before do
      @account_updater.expects(:log).returns("log")
      @account_updater.expects(:calculate_balance).returns(100)
    end

    describe 'when :serialize_voice_usage Arturo is OFF' do
      before do
        Arturo.disable_feature!(:serialize_voice_usage)
      end

      it 'should save the VoiceUsage record immediately' do
        @account_updater.save!
        assert_equal 1, ZendeskBillingCore::Zuora::VoiceUsage.where(account_id: @account.id).count
      end
    end

    describe 'when :serialize_voice_usage Arturo is ON' do
      before do
        Arturo.enable_feature!(:serialize_voice_usage)
      end

      it 'should save the VoiceUsage record immediately' do
        ZendeskBillingCore::Voice::UnprocessedUsage.expects(:create!).with(
          account_id:         @account.id,
          usage_type:         'noop',
          voice_reference_id: 'test_reference',
          units:              100
        )
        @account_updater.save!
      end
    end
  end
end
