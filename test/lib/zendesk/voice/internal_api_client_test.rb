require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 16

describe Zendesk::Voice::InternalApiClient do
  let(:account) { accounts(:minimum) }

  before { account.stubs(:has_voice_vip?).returns(true) }

  describe "#initialize" do
    it 'finds deleted accounts' do
      account.cancel!
      account.soft_delete!
      KragleConnection.expects(:build_for_account).returns(Kragle.new('voice'))
      Zendesk::Voice::InternalApiClient.new(account.subdomain)
    end

    it 'sets the `timeout` to the default value' do
      assert_equal(
        Zendesk::Voice::InternalApiClient::REQUEST_TIMEOUT,
        Zendesk::Voice::InternalApiClient.
          new(account.subdomain).
          send(:timeout)
      )
    end

    describe 'when initialized with `timeout`' do
      it 'sets the `timeout` to the provided value' do
        assert_equal(
          20,
          Zendesk::Voice::InternalApiClient.
            new(account.subdomain, timeout: 20.seconds).
            send(:timeout)
        )
      end
    end
  end

  describe "#voice_subscription_details" do
    it "makes a GET request" do
      stub_request(:get, "https://minimum.zendesk-test.com/api/v2/channels/voice/voice_account.json")
      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_subscription_details
    end

    describe 'in the case of a Faraday::ConnectionFailed Error' do
      it 'does NOT log an exception, and returns nil' do
        stub_request(:get, "https://minimum.zendesk-test.com/api/v2/channels/voice/voice_account.json").
          to_raise(Faraday::ConnectionFailed)
        ZendeskExceptions::Logger.expects(:record).never

        refute Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_subscription_details
      end
    end

    describe 'in the case of a Net::OpenTimeout Error' do
      it 'does NOT log an exception, and returns nil' do
        stub_request(:get, "https://minimum.zendesk-test.com/api/v2/channels/voice/voice_account.json").
          to_raise(Net::OpenTimeout)
        ZendeskExceptions::Logger.expects(:record).never

        refute Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_subscription_details
      end
    end

    it "returns nil in the case of a network error" do
      stub_request(:get, "https://minimum.zendesk-test.com/api/v2/channels/voice/voice_account.json").
        to_raise(ZendeskAPI::Error::NetworkError)
      ZendeskExceptions::Logger.expects(:record)

      refute Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_subscription_details
    end
  end

  describe "#create_sub_account" do
    it "makes a POST request" do
      stub_request(:post, "https://minimum.zendesk-test.com/api/v2/channels/voice/internal/sub_accounts.json")
      Zendesk::Voice::InternalApiClient.new(account.subdomain).create_sub_account
    end
  end

  describe "#update_sub_account" do
    it "makes a PUT request" do
      stub_request(:put, "https://minimum.zendesk-test.com/api/v2/channels/voice/internal/sub_accounts.json").
        with(body: { sub_account: { opted_in: true }})
      Zendesk::Voice::InternalApiClient.new(account.subdomain).update_sub_account(opted_in: true)
    end
  end

  describe "#suspend_sub_account" do
    it "makes a PUT request" do
      stub_request(:put, 'https://minimum.zendesk-test.com/api/v2/channels/voice/internal/sub_accounts/suspend.json')
      Zendesk::Voice::InternalApiClient.new(account.subdomain).suspend_sub_account
    end
  end

  describe "#get_sub_account" do
    it "makes a GET request" do
      Zendesk::Voice::InternalApiClient.new(account.subdomain).get_sub_account

      assert_requested(@sub_account_stub)
    end
  end

  describe "#create_trial" do
    it "makes a POST request" do
      request = stub_request(:post, 'https://minimum.zendesk-test.com/api/v2/channels/voice/internal/suite_trials.json')
      Zendesk::Voice::InternalApiClient.new(account.subdomain).create_trial

      assert_requested(request)
    end
  end

  describe "#get_voice_feature" do
    it "makes a GET request" do
      request = stub_request(
        :get,
        %r{/api/v2/channels/voice/internal/features/check/user_seats}
      )
      Zendesk::Voice::InternalApiClient.new(account.subdomain).get_voice_feature('user_seats')

      assert_requested(request)
    end
  end

  describe "#fetch_groups_phone_numbers" do
    it "makes a GET request" do
      request = stub_request(
        :get,
        %r{/api/v2/channels/voice/groups_phone_numbers.json}
      )
      Zendesk::Voice::InternalApiClient.new(account.subdomain).fetch_groups_phone_numbers

      assert_requested(request)
    end
  end

  describe "#create_sdk_embeddable" do
    it "makes a POST request" do
      request = stub_request(:post, 'https://minimum.zendesk-test.com/api/v2/channels/voice/talk_embeddables/sdk.json').
        with(
          body: {
            talk_embeddable: {
              app_id: 'mobile_sdk_app_id',
              snapcall_button_id: 'abcdefghijklmnopqrstuvwxyz123456',
              group_id: 1,
              phone_number_id: 1,
              enabled: true,
              capability: 3
            }
          }
        )

      Zendesk::Voice::InternalApiClient.new(account.subdomain).create_sdk_embeddable(
        app_id: 'mobile_sdk_app_id',
        snapcall_button_id: 'abcdefghijklmnopqrstuvwxyz123456',
        group_id: 1,
        phone_number_id: 1,
        enabled: true
      )

      assert_requested(request)
    end
  end

  describe "#update_sdk_embeddable" do
    it "makes a PUT request" do
      request = stub_request(:put, 'https://minimum.zendesk-test.com/api/v2/channels/voice/talk_embeddables/sdk/1.json').
        with(
          body: {
            talk_embeddable: {
              snapcall_button_id: 'abcdefghijklmnopqrstuvwxyz123456',
              group_id: 1,
              phone_number_id: 1,
              enabled: true,
            }
          }
        )

      Zendesk::Voice::InternalApiClient.new(account.subdomain).update_sdk_embeddable(
        1,
        snapcall_button_id: 'abcdefghijklmnopqrstuvwxyz123456',
        group_id: 1,
        phone_number_id: 1,
        enabled: true
      )

      assert_requested(request)
    end
  end

  describe "#delete_sdk_embeddable" do
    it "makes a DELETE request" do
      request = stub_request(:delete, 'https://minimum.zendesk-test.com/api/v2/channels/voice/talk_embeddables/sdk/1.json')

      Zendesk::Voice::InternalApiClient.new(account.subdomain).delete_sdk_embeddable(1)

      assert_requested(request)
    end
  end

  describe "#update remove_from_routing" do
    let(:group) { FactoryBot.create(:group, account: account) }

    it "makes a PUT request" do
      stub_request(:put, "https://minimum.zendesk-test.com/api/v2/channels/voice/internal/groups/#{group.id}/remove_from_routing.json")
      Zendesk::Voice::InternalApiClient.new(account.subdomain).remove_from_routing(group.id)
    end
  end

  describe '#voice_subscription_created' do
    let(:subscription) { FactoryBot.create(:voice_subscription, account: account) }
    before { ZendeskBillingCore::Zuora::Jobs::AddVoiceJob.expects(:enqueue) }

    it 'makes a PUT request' do
      stub_request(
        :put,
        'https://minimum.zendesk-test.com/api/v2/channels/voice/internal/sub_accounts/subscription_created.json'
      ).with(body: { billing_subscription: { plan_type: subscription.plan_type }})

      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_subscription_created(subscription.plan_type)
    end
  end

  describe '#lotus_feature_voice_soft_delete_recordings' do
    let(:ticket) { FactoryBot.build_stubbed(:ticket, nice_id: 13, account: account) }
    let(:url) { "https://minimum.zendesk-test.com/api/v2/channels/voice/recordings/destroy_by_ticket_nice_id/#{ticket.nice_id}" }

    it 'makes a DELETE request' do
      request = stub_request(:delete, url)

      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_soft_delete_recordings(ticket.nice_id)

      assert_requested(request)
    end

    it 'retries when there is an network error' do
      stub_request(:delete, url).
        to_raise(Faraday::Error::ConnectionFailed).then.
        to_return(status: 500).then.
        to_return(status: 200)

      response = Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_soft_delete_recordings(ticket.nice_id)

      assert_equal 200, response.status
    end
  end

  describe '#voice_fraud_report' do
    it 'makes PUT request if voice enabled' do
      account.expects(:voice_enabled?).returns(true)
      request = stub_request(
        :put, 'https://minimum.zendesk-test.com/api/v2/channels/voice/internal/voice_account/fraud_report.json'
      ).with(body: { fraud_report: 90 })
      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_fraud_report(account, 90)
      assert_requested(request)
    end

    it 'does not make PUT request if not voice enabled' do
      request = stub_request(
        :put, 'https://minimum.zendesk-test.com/api/v2/channels/voice/internal/voice_account/fraud_report.json'
      ).with(body: { fraud_report: 90 })
      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_fraud_report(account, 90)
      assert_not_requested(request)
    end
  end

  describe '#voice_fraud_score' do
    it 'makes PUT request for fraud score if voice enabled' do
      account.expects(:voice_enabled?).returns(true)

      request = stub_request(
        :put, 'https://minimum.zendesk-test.com/api/v2/channels/voice/internal/voice_account/fraud_score.json'
      ).with(body: { fraud_score: { risky: true } })

      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_fraud_score(account, true)

      assert_requested(request)
    end

    it 'does not make PUT request for fraud score if not voice enabled' do
      request = stub_request(
        :put, 'https://minimum.zendesk-test.com/api/v2/channels/voice/internal/voice_account/fraud_score.json'
      ).with(body: { fraud_score: { risky: true } })

      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_fraud_score(account, true)

      assert_not_requested(request)
    end

    it 'makes PUT request for fraud score if risky is true' do
      account.expects(:voice_enabled?).returns(true)

      request = stub_request(
        :put, 'https://minimum.zendesk-test.com/api/v2/channels/voice/internal/voice_account/fraud_score.json'
      ).with(body: { fraud_score: { risky: true } })

      Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_fraud_score(account, true)

      assert_requested(request)
    end
  end

  describe '#account_deleted' do
    it 'makes a DELETE request' do
      request = stub_request(
        :delete,
        "https://minimum.zendesk-test.com/api/v2/channels/voice/internal/sub_accounts.json?account_id=#{account.id}"
      )
      Zendesk::Voice::InternalApiClient.new(account.subdomain).account_deleted!
      assert_requested(request)
    end
  end

  describe 'instrument timeout errors' do
    let(:statsd_client) { stub_everything('statsd client') }

    before do
      Zendesk::StatsD::Client.stubs(:new).with(anything).returns(statsd_client)
    end

    it 'rescue faraday timeout errors, send them to statsd, and return nil response' do
      statsd_client.expects(:increment).with('error', anything)

      stub_request(:get, "https://minimum.zendesk-test.com/api/v2/channels/voice/voice_account.json").to_raise(Faraday::TimeoutError)
      assert_nil Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_subscription_details
    end

    it 'rescue kragle client errors, send them to statsd, and return nil response' do
      statsd_client.expects(:increment).with('error', anything)

      stub_request(:get, "https://minimum.zendesk-test.com/api/v2/channels/voice/voice_account.json").to_raise(Kragle::BadRequest)
      assert_nil Zendesk::Voice::InternalApiClient.new(account.subdomain).voice_subscription_details
    end
  end
end
