require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

class RechargeSupportTestController < ApplicationController
  include Zendesk::Voice::RechargeSupport

  before_action :require_no_recharge_settings, only: :no_settings
  before_action :require_recharge_settings, only: :needs_settings
  before_action :require_zuora_account, only: :needs_zuora

  skip_before_action :ensure_support_is_active!

  def no_settings
    head :ok
  end

  def needs_settings
    head :ok
  end

  def needs_zuora
    head :ok
  end

  def update
    if update_recharge_settings(params[:recharge_settings])
      head :ok
    end
  end
end

class RechargeSupportTest < ActionController::TestCase
  fixtures :accounts

  tests RechargeSupportTestController
  use_test_routes

  describe 'RechargeSupport' do
    let(:account) { accounts(:minimum) }

    as_an_admin do
      before do
        @request.account = account
      end

      describe '#require_no_recharge_settings' do
        describe 'when recharge settings exist' do
          before do
            account.create_voice_recharge_settings(
              enabled: true,
              amount: 50.00,
              minimum_balance: 5.00
            )

            post :no_settings, params: { recharge_settings: { enabled: true, amount: 100.00 } }
          end

          it('returns not_found') { assert_response :not_found }

          it 'returns error message' do
            actual   = JSON.parse(response.body).symbolize_keys
            expected = {
              domain: 'Voice',
              error:  'Recharge settings already exist'
            }
            assert_equal expected, actual
          end
        end

        describe 'when no recharge settings exist' do
          it 'returns ok' do
            post :no_settings, params: { recharge_settings: { enabled: true, amount: 100.00 } }
            assert_response :ok
          end
        end
      end

      describe '#require_recharge_settings' do
        describe 'when recharge settings exist' do
          before do
            account.create_voice_recharge_settings(
              enabled: true,
              amount: 50.00,
              minimum_balance: 5.00
            )
          end

          it 'returns ok' do
            put :needs_settings
            assert_response :ok
          end
        end

        describe 'when no recharge settings exist' do
          before do
            put :needs_settings
          end

          it('returns not_found') { assert_response :not_found }

          it 'returns error message' do
            actual   = JSON.parse(response.body).symbolize_keys
            expected = {
              domain: 'Voice',
              error:  'Recharge settings do not exist'
            }
            assert_equal expected, actual
          end
        end
      end

      describe '#require_zuora_account' do
        describe 'when account has zuora_subscription' do
          before do
            Account.any_instance.stubs(:zuora_subscription).returns(stub)
          end

          it 'returns ok' do
            post :needs_zuora
            assert_response :ok
          end
        end

        describe 'when account does not have a zuora_subscription' do
          before do
            post :needs_zuora
          end

          it('returns not_found') { assert_response :not_found }

          it 'returns error message' do
            actual   = JSON.parse(response.body).symbolize_keys
            expected = {
              domain: 'Voice',
              error:  'Recharge not supported on trial'
            }
            assert_equal expected, actual
          end
        end

        describe 'when account does not have a subscription (i.e. Chat-only)' do
          let(:account) { accounts(:shell_account_without_support) }

          before do
            post :needs_zuora
          end

          it 'returns not found status' do
            actual   = JSON.parse(response.body).symbolize_keys
            expected = {
              domain: 'Voice',
              error:  'Recharge not supported on trial'
            }
            assert_equal expected, actual
            assert_response :not_found
          end
        end
      end

      describe '#update_recharge_settings' do
        let(:client) { stub(:update_recharge_settings) }

        let(:update_params) { { enabled: true, amount: 100.0 } }

        before do
          Account.any_instance.stubs(:zuora_subscription).returns(
            stub(
              client: client,
              zuora_account_id: 'foo'
            )
          )
          ZendeskBillingCore::Zuora::Synchronizer.stubs(:new).returns(stub(synchronize_voice_recharge_settings!: true))
        end

        describe 'when update is successful' do
          it 'uses Zuora::Client to update custom fields on Zuora' do
            client.expects(:update_recharge_settings).with('enabled' => true, 'amount' => '100.0')
            put :update, params: { recharge_settings: update_params }
          end

          it 'returns true' do
            put :update, params: { recharge_settings: update_params }
            assert_response :ok
          end
        end
      end
    end
  end
end
