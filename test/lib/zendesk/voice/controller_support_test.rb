require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 12

class VoiceControllerSupportTestController < ApplicationController
  include Zendesk::Voice::ControllerSupport

  def index
    head :ok
  end
end

class VoiceControllerSupportTest < ActionController::TestCase
  fixtures :all

  tests VoiceControllerSupportTestController
  use_test_routes

  describe "VoiceControllerSupport" do
    before do
      UpdateAppsFeatureJob.stubs(:enqueue)
      Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)

      @request.account = @account = accounts(:minimum)
      @user = users(:minimum_agent)
      login @user
    end

    describe_with_arturo_enabled :voice_partner_edition do
      it "returns 403 forbidden for accounts with inactive Voice partner edition accounts" do
        FactoryBot.create(:voice_partner_edition_account, account: @account, active: false)

        get :index
        assert_response :forbidden
      end

      it "returns 200 OK for accounts with active Voice partner edition accounts" do
        FactoryBot.create(:voice_partner_edition_account, account: @account)

        get :index
        assert_response :ok
      end
    end

    describe "when the account has Patagonia pricing model" do
      before do
        @account.subscription.update_attribute(:pricing_model_revision, ZBC::Zendesk::PricingModelRevision::PATAGONIA)
      end

      it "returns 403 forbidden for the account with no access to CTI" do
        plan_type = Zendesk::Types::SubscriptionPlanType.find('Small')
        @account.subscription.update_attributes! plan_type: plan_type
        Zendesk::Features::SubscriptionFeatureService.new(@account).execute!

        get :index
        assert_response :forbidden
      end

      it "returns 200 OK for the account with access to CTI" do
        plan_type = Zendesk::Types::SubscriptionPlanType.find('Medium')
        @account.subscription.update_attributes! plan_type: plan_type

        get :index
        assert_response :ok
      end

      it "returns 200 OK for internal requests, regardless the account's plan type" do
        @controller.stubs(:internal_request?).returns(true)

        plan_type = Zendesk::Types::SubscriptionPlanType.find('Small')
        @account.subscription.update_attributes! plan_type: plan_type

        get :index
        assert_response :ok
      end
    end

    describe '#find_or_create_partner_edition_account' do
      describe "when account does not have a voice_partner_edition_account" do
        it 'does not create a partner edition account for internal requests' do
          assert @account.voice_partner_edition_account.nil?

          @controller.stubs(:internal_request?).returns(true)

          get :index
          assert_response :ok

          @account.reload
          refute @account.voice_partner_edition_account.present?
        end

        it 'returns partner_edition_account with active set to true and plan_type set to regular' do
          assert @account.voice_partner_edition_account.nil?

          get :index
          assert_response :ok
          @account.reload
          assert @account.voice_partner_edition_account.present?
          assert_equal @account.voice_partner_edition_account.plan_type, Voice::PartnerEditionAccount::PLAN_TYPE[:regular]
          assert @account.voice_partner_edition_account.active
        end
      end

      describe "when account has a voice_partner_edition_account" do
        it 'returns the existing partner_edition_account' do
          FactoryBot.create(:voice_partner_edition_account, account_id: @account.id)

          assert_no_difference 'Voice::PartnerEditionAccount.count' do
            get :index
            assert_response :ok
          end
        end
      end
    end
  end
end
