require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 6

describe Zendesk::Voice::BulkCsvUpdater do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    @account_id = @account.id
    @balance = 100
    @reference = "test_reference"
    @bulk_csv_updater = Zendesk::Voice::BulkCsvUpdater.new("../test")
  end

  describe "#parse_csv" do
    it "takes a file path and run CSV foreach row" do
      CSV.expects(:foreach).with("../test")
      @bulk_csv_updater.parse_csv
    end
  end

  describe "#update_credit" do
    before do
      @account_updater = stub(account: @account, log_string: "test")
      @bulk_csv_updater.expects(:account_updater).with(
        @account_id, @balance, @reference
      ).returns(@account_updater)
      ActiveRecord::Base.expects(:on_shard).with(@account.shard_id).at_least_once.yields
      @account_updater.expects(:save!)
      @bulk_csv_updater.expects(:log).with(@account_updater.log_string)
    end

    it "updates the credit on each account" do
      @bulk_csv_updater.update_credit(@account_id, @balance, @reference)
    end
  end
end
