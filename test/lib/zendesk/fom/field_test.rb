require_relative "../../../support/test_helper"
require 'application_controller'
require 'zendesk/fom/field'

SingleCov.covered! uncovered: 153

describe 'Zendesk::Fom' do
  fixtures :accounts, :users, :user_identities, :tickets, :groups, :memberships, :rules, :sequences, :taggings, :events, :ticket_fields, :ticket_field_entries, :organizations, :plans, :translation_locales, :brands

  describe 'CustomField' do
    let(:account) { accounts(:minimum) }

    describe "#can_be" do
      it "does not allow a partial credit card field to be activated without the pci_credit_card_custom_field feature" do
        field = FieldPartialCreditCard.create!(account: account, title: 'credit card field')
        fom = Zendesk::Fom::Field.for(field)
        assert_equal false, fom.can_be[:activated]
      end

      it "allows a partial credit card field to be activated with the pci_credit_card_custom_field feature" do
        Account.any_instance.stubs(has_pci_credit_card_custom_field?: true)
        field = FieldPartialCreditCard.create!(account: account, title: 'credit card field')
        fom = Zendesk::Fom::Field.for(field)
        assert(fom.can_be[:activated])
      end

      it "allows a normal custom field to be activated" do
        field = FieldText.create!(account: account, title: 'text field')
        fom = Zendesk::Fom::Field.for(field)
        assert(fom.can_be[:activated])
      end
    end

    describe '#value' do
      let(:field) { FieldText.create!(account: account, title: 'text') }
      let(:fom) { Zendesk::Fom::Field.for(field) }
      let(:ticket) { tickets(:minimum_1) }

      describe 'with no entry for the field' do
        it 'returns an empty string' do
          assert_equal '', fom.value(ticket)
        end
      end

      describe 'ticket field entry caching' do
        let(:entry) do
          TicketFieldEntry.create!(account: account, ticket: ticket, ticket_field: field, value: 'HALP!')
        end

        describe 'a raw export ticket render' do
          before do
            ticket.ticket_field_entries_by_ticket_field_id_cache =
              {
                field.id => {
                  id: entry.id,
                  value: entry.value
                }
              }
          end

          it 'contains the correct value' do
            assert_equal entry.value, fom.value(ticket)
          end

          it 'does not lookup entries from the DB' do
            assert_sql_queries(0) do
              fom.value(ticket)
            end
          end
        end

        describe 'a standard ticket render' do
          before do
            ticket.ticket_field_entries_by_ticket_field_id_cache =
              {
                field.id => entry
              }
          end

          it 'contains the correct value' do
            assert_equal entry.value, fom.value(ticket)
          end

          it 'does not lookup entries from the DB' do
            assert_sql_queries(0) do
              fom.value(ticket)
            end
          end
        end
      end
    end
  end

  describe "valid fields" do
    it "alls static fields be valid" do
      ticket = tickets(:minimum_1)

      Zendesk::Fom::Field.subclasses.each do |klass|
        if klass == Zendesk::Fom::CustomField
          klass = Zendesk::Fom::CustomField.new(ticket_fields(:field_tagger_custom))
        end
        assert klass.vars
        refute klass.identifier.blank?
        refute klass.db_field.blank?
        refute klass.label.blank?
        refute klass.title.blank?
        assert klass.is_active?
        klass.value(ticket) # Just checking that this does not throw an error...
      end
    end

    describe ".event_to_s" do
      before do
        @change = Change.new
        @change.stubs(type: "Change")
        @create = Create.new
        @create.stubs(type: "Create")
      end

      it "does not include the time portion" do
        @change.stubs(value: "2012-12-27 19:00:00")
        @change.stubs(value_previous: "2011-12-27 19:00:00")
        assert_equal "Due date changed from December 27, 2011 to December 27, 2012", Zendesk::Fom::DueDateField.event_to_s(@change, {})
      end
      it "does not raise an exception" do
        @change.stubs(value: nil)
        @change.stubs(value_previous: "2011-12-27 19:00:00")
        assert_equal "", Zendesk::Fom::DueDateField.event_to_s(@change, {})
      end
      it "returns no string at all" do
        @change.stubs(value: "2011-12-27 19:00:00")
        @change.stubs(value_previous: nil)
        assert_equal "Due date set to December 27, 2011", Zendesk::Fom::DueDateField.event_to_s(@change, {})
      end

      it "does not raise a missing translation if the new value of a PriorityField is '-'" do
        @change.stubs(value: '-', value_previous: 1)
        assert_equal "Priority changed from Low to -", Zendesk::Fom::PriorityField.event_to_s(@change, {})
      end

      it "gives the right message when setting a PriorityField for the first time" do
        @create.stubs(value: 1)
        assert_equal "Priority set to Low", Zendesk::Fom::PriorityField.event_to_s(@create, {})
      end

      it "gives the organization audit info when ticket is created" do
        org = organizations(:minimum_organization1)

        @create.stubs(value: org.id)
        assert_equal "Organization set to #{org.name}", Zendesk::Fom::OrganizationField.event_to_s(@create, {})
      end

      it "gives the right message when setting an organization for the first time" do
        org = organizations(:minimum_organization1)

        @create.stubs(value: org.id, value_previous: nil)
        assert_equal "Organization set to #{org.name}", Zendesk::Fom::OrganizationField.event_to_s(@create, {})
      end

      it "gives the audit info when ticket organization changes" do
        org1 = organizations(:minimum_organization1)
        org2 = organizations(:minimum_organization2)

        @change.stubs(value: org2.id, value_previous: org1.id)
        assert_equal "Organization changed from #{org1.name} to #{org2.name}", Zendesk::Fom::OrganizationField.event_to_s(@change, {})
      end

      it "has with_deleted scope for deleted orgs" do
        org1 = organizations(:minimum_organization1)
        org2 = organizations(:minimum_organization2)
        org2.soft_delete!

        @change.stubs(value: org2.id, value_previous: org1.id)
        assert_equal "Organization changed from #{org1.name} to #{org2.name}", Zendesk::Fom::OrganizationField.event_to_s(@change, {})
      end

      it "has with_deleted scope for deleted brands" do
        brand1 = FactoryBot.create(:brand, name: "Wombat")
        brand2 = FactoryBot.create(:brand, name: "Giraffes")
        brand2.soft_delete!

        @change.stubs(value: brand2.id, value_previous: brand1.id)
        assert_equal "Ticket brand changed from #{brand1.name} to #{brand2.name}", Zendesk::Fom::TicketBrandField.event_to_s(@change, {})
      end

      it "does not raise an error when organization is removed from ticket" do
        org = organizations(:minimum_organization1)

        @change.stubs(value: nil, value_previous: org.id)
        assert_equal "Organization changed from #{org.name} to -", Zendesk::Fom::OrganizationField.event_to_s(@change, {})
      end

      it "gives the right message when setting is_public on a ticket for the first time" do
        @create.stubs(value: '1', value_previous: nil)
        assert_equal "Customer visible set to Yes", Zendesk::Fom::TicketPublicityField.event_to_s(@create, {})
      end

      it "gives the audit info when is_public changes on a ticket" do
        @change.stubs(value: '0', value_previous: '1')
        assert_equal "Customer visible changed from Yes to No", Zendesk::Fom::TicketPublicityField.event_to_s(@change, {})
      end

      it "converts is_public to csv column" do
        ticket = tickets(:minimum_1)
        assert(Zendesk::Fom::TicketPublicityField.value_for_csv_column(ticket))
      end
    end
  end

  describe ".humanize_value" do
    let(:account) { accounts(:minimum) }
    let(:ticket) { tickets(:minimum_1) }

    describe "for multiselect fields" do
      let(:multiselect_field) do
        FieldMultiselect.create!(
          account: account,
          title: "Multiselect",
          custom_field_options: [
            CustomFieldOption.new(name: "Option1", value: "option1"),
            CustomFieldOption.new(name: "Option2", value: "option2")
          ]
        )
      end

      before do
        account.instance_variable_set :@field_taggers_cache, nil
      end

      it "returns all the option names in a comma separated string" do
        Timecop.travel(Time.now + 1.seconds)
        field = Zendesk::Fom::CustomField.new(multiselect_field)
        assert_equal "Option1, Option2", field.humanize_value("option1 option2")
      end

      it "only maps custom field tags to values once" do
        field = Zendesk::Fom::CustomField.new(multiselect_field)

        FieldTagger.expects(:taggers_tag_to_name_lookup).once.returns({})
        3.times do
          field.humanize_value("option1 option2")
        end
      end
    end

    describe "for dropdowns" do
      it "returns the name of the option" do
        field = Zendesk::Fom::CustomField.new(ticket_fields(:field_tagger_custom))
        assert_equal "Booring", field.humanize_value("booring")
      end

      it "only maps custom field tags to values once" do
        field = Zendesk::Fom::CustomField.new(ticket_fields(:field_tagger_custom))

        FieldTagger.expects(:taggers_tag_to_name_lookup).once.returns({})
        3.times do
          field.humanize_value('booring')
        end
      end
    end
  end
end
