require_relative "../../../support/test_helper"

SingleCov.not_covered! # as_json is defined inside of ticket_field.rb

describe 'TicketField serialization' do
  def assert_field(additional = {})
    assert_equal({
      "is_required"             => @field.is_required?,
      "position"                => @field.position,
      "title_in_portal"         => @field.title_in_portal,
      "created_at"              => @field.created_at.as_json,
      "title"                   => @field.title,
      "sub_type_id"             => @field.sub_type_id,
      "updated_at"              => @field.updated_at.as_json,
      "account_id"              => @account.id,
      "is_editable_in_portal"   => @field.is_editable_in_portal?,
      "is_visible_in_portal"    => @field.is_visible_in_portal?,
      "tag"                     => @field.tag,
      "id"                      => @field.id,
      "is_collapsed_for_agents" => @field.is_collapsed_for_agents?,
      "is_required_in_portal"   => @field.is_required_in_portal?,
      "type"                    => @field.class.name,
      "is_active"               => @field.is_active?,
      "description"             => @field.description,
      "is_exportable"           => @field.is_exportable?,
      "regexp_for_validation"   => @field.regexp_for_validation,
      "agent_description"       => @field.agent_description,
      "idempotence_key"         => @field.idempotence_key
    }.merge(additional), JSON.parse(@field.to_json))
  end

  describe "TicketFields" do
    fixtures :all

    before { @account = accounts(:minimum) }

    describe "FieldAssignee" do
      before do
        @field = FieldAssignee.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
      end
    end

    describe "FieldCheckbox" do
      before do
        @field = FieldCheckbox.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldDate" do
      before do
        @field = FieldDate.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldDecimal" do
      before do
        @field = FieldDecimal.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldDescription" do
      before do
        @field = FieldDescription.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldGroup" do
      before do
        @field = FieldGroup.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldInteger" do
      before do
        @field = FieldInteger.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldPriority" do
      before do
        @field = FieldPriority.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldRegexp" do
      before do
        @field = FieldRegexp.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldStatus" do
      before do
        @field = FieldStatus.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldSubject" do
      before do
        @field = FieldSubject.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldTagger" do
      before do
        @field = FieldTagger.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        custom_field_options = @field.custom_field_options.map do |o|
          {'id' => o.id, 'name' => o.name, 'value' => o.value}
        end
        assert_field("custom_field_options" => custom_field_options)
      end
    end

    describe "FieldText" do
      before do
        @field = FieldText.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldTextarea" do
      before do
        @field = FieldTextarea.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end

    describe "FieldTicketType" do
      before do
        @field = FieldTicketType.where(account_id: @account.id).first_or_create { |f| f.title = 'test field' }
        assert(@field.id, @field.errors.full_messages.join('. '))
      end

      it "supports v1 serialization" do
        assert_field
      end
    end
  end
end
