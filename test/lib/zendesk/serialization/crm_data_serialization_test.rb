require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Serialization::CrmDataSerialization do
  fixtures :users, :external_user_datas

  describe "SalesforceData" do
    before do
      @salesforce_data = external_user_datas(:external_user_data_1)
      @salesforce_data.sync!(records: ["a", "b"])
    end

    describe "with records" do
      before do
        @data = {records: [{label: "Birthday", value: "bla"}], label: "Salesforce", record_type: "Contact"}
        @records = {records: @data[:records]}
        @salesforce_data.sync!(@data)
      end

      it "serializes records to JSON" do
        assert_equal(@records.to_json, @salesforce_data.to_json(only: [:records], methods: :records))
      end
    end

    it "supports v1 serialization" do
      assert_equal({
        'id' => @salesforce_data.id,
        'account_id' => @salesforce_data.account_id,
        'user_id' => @salesforce_data.user_id,
        'sync_status' => @salesforce_data.sync_status,
        'data' => JSON.parse(@salesforce_data.data.to_json),
        'created_at' => @salesforce_data.created_at.as_json,
        'updated_at' => @salesforce_data.updated_at.as_json
      }, JSON.parse(@salesforce_data.to_json(version: 1)))
    end

    describe "when done" do
      it "supports v2 serialization" do
        assert_equal({
          'message' => 'Your request has been proccessed.',
          'records' => ["a", "b"],
          'state' => 'done',
          'link' => 'the link'
        }, JSON.parse(@salesforce_data.to_json(version: 2, link: 'the link')))
      end
    end

    describe "when pending" do
      before do
        @salesforce_data.sync_pending!
      end

      it "supports v2 serialization" do
        assert_equal({
          'message' => 'Your request is currently being processed.',
          'records' => ["a", "b"],
          'state' => 'pending'
        }, JSON.parse(@salesforce_data.to_json(version: 2)))
      end
    end

    describe "when errored" do
      before do
        @salesforce_data.sync_errored!(records: [], error: :soap_fault)
      end

      it "supports v2 serialization" do
        assert_equal({
          'message' => 'Failed to complete the request.',
          'records' => [],
          'state' => 'failed',
          'error' => 'soap_fault'
        }, JSON.parse(@salesforce_data.to_json(version: 2)))
      end
    end

    describe "with message" do
      it "supports v2 serialization" do
        assert_equal({
          'message' => 'the message',
          'records' => ["a", "b"],
          'state' => 'done',
          'link' => 'the link'
        }, JSON.parse(@salesforce_data.to_json(version: 2, link: 'the link', message: 'the message')))
      end
    end
  end
end
