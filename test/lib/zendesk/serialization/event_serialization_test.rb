require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Zendesk::Serialization::EventSerialization do
  fixtures :all

  def v2_serialize_user(user)
    user.as_json(
      version: 2
    ).deep_symbolize_keys
  end

  def deep_symbolize_keys(obj)
    case obj
    when Hash
      obj.symbolize_keys!
      obj.each do |key, value|
        obj[key] = deep_symbolize_keys(value)
      end
    when Array
      obj.map! { |value| deep_symbolize_keys(value) }
    end
    obj
  end

  describe "event subclass" do
    before do
      @account = accounts(:minimum)
    end

    describe "v2 via serialization" do
      before do
        @ticket = @account.tickets.last
        @author = @account.owner
        @event = Audit.create!(
          account: @account,
          ticket: @ticket,
          author: @author,
          value: 'value',
          via_id: ViaType.WEB_FORM,
          via_reference_id: 1,
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        )
      end

      describe "via linked problem" do
        before do
          @event.via_id = ViaType.LINKED_PROBLEM
          @event.via_reference_id = @ticket.id
        end

        it "serializes ticket nice_id and subject" do
          assert_equal({
            type: @event.via_id,
            id: @ticket.nice_id,
            title: @ticket.subject
          }, @event.as_json(version: 2).deep_symbolize_keys[:via])
        end
      end

      describe "via merge" do
        before do
          @event.via_id = ViaType.MERGE
          @event.via_reference_id = @ticket.id
        end

        it "serializes ticket nice_id and subject" do
          assert_equal({
            type: @event.via_id,
            id: @ticket.nice_id,
            title: @ticket.subject
          }, @event.as_json(version: 2).deep_symbolize_keys[:via])
        end
      end

      describe "via closed ticket" do
        before do
          @event.via_id = ViaType.CLOSED_TICKET
          @event.via_reference_id = @ticket.id
        end

        it "serializes ticket nice_id and subject" do
          assert_equal({
            type: @event.via_id,
            id: @ticket.nice_id,
            title: @ticket.subject
          }, @event.as_json(version: 2).deep_symbolize_keys[:via])
        end
      end

      describe "via rule" do
        before do
          @rule = @account.triggers.last
          @event.via_id = ViaType.RULE
          @event.via_reference_id = @rule.id
        end

        it "serializes rule id and title" do
          assert_equal({
            type: @event.via_id,
            id: @rule.id,
            title: @rule.title
          }, @event.as_json(version: 2).deep_symbolize_keys[:via])
        end
      end
    end

    describe "Audit" do
      before do
        @ticket = @account.tickets.last
        @author = @account.owner
        @event = Audit.create!(
          account: @account,
          ticket: @ticket,
          author: @author,
          value: 'value',
          via_id: ViaType.WEB_FORM,
          via_reference_id: 1,
          value_previous: 'IP address: 255.255.255.255',
          value_reference: 'value_reference'
        )
      end

      describe "v1 serialization" do
        it "works predictably" do
          assert_equal({
            'type' => 'Audit',
            'created_at' => @event.created_at.as_json,
            'via_id' => ViaType.WEB_FORM,
            'author_id' => @event.author.id,
            'is_public' => false,
            'via_reference_id' => 1,
            'author' => {'name' => @event.author.name, 'created_at' => @event.author.created_at.as_json},
            'value' => 'value',
            'value_previous' => 'IP address: 255.255.255.255',
            'value_reference' => 'value_reference'
          }, JSON.parse(@event.to_json))
        end

        it "does not map the nice id for audits created from a closed ticket" do
          @event.via_id = ViaType.CLOSED_TICKET
          @event.via_reference_id = @ticket.id

          assert_equal({
            'type' => 'Audit',
            'created_at' => @event.created_at.as_json,
            'via_id' => ViaType.CLOSED_TICKET,
            'author_id' => @event.author.id,
            'is_public' => false,
            'via_reference_id' => @ticket.id,
            'author' => {'name' => @event.author.name, 'created_at' => @event.author.created_at.as_json},
            'value' => 'value',
            'value_previous' => 'IP address: 255.255.255.255',
            'value_reference' => 'value_reference'
          }, JSON.parse(@event.to_json))
        end

        it "includes events when :filter is specified as :audits" do
          @event = events(:create_audit_for_minimum_ticket_1)
          json = JSON.parse(@event.to_json(version: 1, filter: :audits))
          assert json['events']
          assert_instance_of Array, json['events']
        end
      end

      describe "v2 serialization" do
        it "works predictably" do
          assert_equal({
            id: @event.id,
            ticket_id: @event.ticket.nice_id,
            created_at: @event.created_at,
            via: {type: @event.via_id},
            author: v2_serialize_user(@event.author),
            client: @event.client,
            events: []
          }, deep_symbolize_keys(@event.as_json(version: 2)))
        end
      end
    end

    describe "Cc" do
      before do
        @recipients = @account.end_users
        assert(@recipients.any?)
        @event = Cc.create!(
          account: @account,
          ticket: @account.tickets.last,
          audit: @account.tickets.last.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          value: @recipients.map(&:id),
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        )
      end

      it "supports v1 serialization" do
        assert_equal({
          'type' => 'Cc',
          'created_at' => @event.created_at.as_json,
          'via_id' => Zendesk::Types::ViaType.MAIL,
          'value' => @recipients.map(&:id),
          'author_id' => @event.author.id,
          'is_public' => false
        }, JSON.parse(@event.to_json))
      end

      it "supports v2 serialization" do
        assert_equal({
          id: @event.id,
          type: 'Cc',
          created_at: @event.created_at,
          via: {type: @event.via_id},
          recipients: @recipients.map { |user| {id: user.id, name: user.name} }
        }, deep_symbolize_keys(@event.as_json(version: 2)))
      end
    end

    describe "Change" do
      before do
        @ticket = @account.tickets.last
        @event = Change.create!(
          account: @account,
          ticket: @ticket,
          audit: @ticket.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          value: @ticket.id,
          value_previous: @ticket.id,
          value_reference: 'linked_id'
        ).reload
      end

      describe "v1 serialization" do
        it "does not rewrite linked_id" do
          assert_equal({
            'type' => 'Change',
            'created_at' => @event.created_at.as_json,
            'via_id' => Zendesk::Types::ViaType.MAIL,
            'author_id' => @event.author.id,
            'is_public' => false,
            'via_reference_id' => 1,
            'value' => @ticket.id.to_s,
            'value_previous' => @ticket.id.to_s,
            'value_reference' => 'linked_id'
          }, JSON.parse(@event.to_json))
        end
      end

      describe "v2 serialization" do
        it "rewrites linked_id" do
          assert_equal({
            id: @event.id,
            type: 'Change',
            created_at: @event.created_at,
            via: {type: @event.via_id},
            value: @ticket.nice_id,
            value_previous: @ticket.nice_id,
            field_name: 'problem_id'
          }, deep_symbolize_keys(@event.as_json(version: 2)))
        end
      end
    end

    describe "Comment" do
      before do
        Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(false)

        @event = Comment.create!(
          account: @account,
          ticket: @account.tickets.last,
          audit: @account.tickets.last.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          body: 'value',
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        )
      end

      describe "v1 serialization" do
        it "works predictably" do
          assert_equal({
            'type' => 'Comment',
            'created_at' => @event.created_at.as_json,
            'via_id' => Zendesk::Types::ViaType.MAIL,
            'author_id' => @event.author.id,
            'is_public' => false,
            'value' => 'value',
            'trusted' => true,
            'attachments' => []
          }, JSON.parse(@event.to_json))
        end

        it "includes author when :filter is specified as :comments" do
          json = JSON.parse(@event.to_json(version: 1, filter: :comments))
          assert json['author']
          assert_equal({'name' => @event.author.name, 'created_at' => @event.author.created_at.as_json}, json['author'])
        end
      end

      it "supports v2 serialization" do
        assert_equal({
          id: @event.id,
          type: 'Comment',
          created_at: @event.created_at,
          via: {type: @event.via_id},
          body: 'value',
          html_body: '<div class="zd-comment" dir="auto"><p>value</p></div>',
          is_public: false,
          attachments: []
        }, deep_symbolize_keys(@event.as_json(version: 2)))
      end
    end

    describe "Create" do
      before do
        @ticket = @account.tickets.last
        @event = Create.create!(
          account: @account,
          ticket: @ticket,
          audit: @ticket.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          value: @ticket.id,
          value_previous: 'value_previous',
          value_reference: 'linked_id'
        ).reload
      end

      describe "v1 serialization" do
        it "does not rewrite linked_id" do
          assert_equal({
            'type' => 'Create',
            'created_at' => @event.created_at.as_json,
            'via_id' => Zendesk::Types::ViaType.MAIL,
            'author_id' => @event.author.id,
            'is_public' => false,
            'via_reference_id' => 1,
            'value' => @ticket.id.to_s,
            'value_previous' => nil,
            'value_reference' => 'linked_id'
          }, JSON.parse(@event.to_json))
        end
      end

      describe "v2 serialization" do
        it "rewrites linked_id" do
          assert_equal({
            id: @event.id,
            type: 'Create',
            created_at: @event.created_at,
            via: {type: @event.via_id},
            value: @ticket.nice_id,
            field_name: 'problem_id'
          }, deep_symbolize_keys(@event.as_json(version: 2)))
        end
      end
    end

    describe "Error" do
      before do
        @event = Error.create!(
          account: @account,
          ticket: @account.tickets.last,
          audit: @account.tickets.last.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          value: 'value',
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        )
      end

      it "supports v1 serialization" do
        assert_equal({
          'type' => 'Error',
          'created_at' => @event.created_at.as_json,
          'via_id' => Zendesk::Types::ViaType.MAIL,
          'author_id' => @event.author.id,
          'is_public' => false,
          'via_reference_id' => 1,
          'value' => 'value',
          'value_previous' => 'value_previous',
          'value_reference' => 'value_reference'
        }, JSON.parse(@event.to_json))
      end

      it "supports v2 serialization" do
        assert_equal({
          id: @event.id,
          type: 'Error',
          created_at: @event.created_at,
          via: {type: @event.via_id},
          value: 'value'
        }, deep_symbolize_keys(@event.as_json(version: 2)))
      end
    end

    describe "External" do
      let(:trigger) { Trigger.last }

      before do
        @event = External.create!(
          account: @account,
          ticket: @account.tickets.last,
          audit: @account.tickets.last.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: trigger.id,
          value: 'value',
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        )
      end

      it "supports v1 serialization" do
        assert_equal({
          'type' => 'External',
          'created_at' => @event.created_at.as_json,
          'via_id' => Zendesk::Types::ViaType.MAIL,
          'author_id' => @event.author.id,
          'is_public' => false,
          'via_reference_id' => trigger.id,
          'value' => 'value',
          'value_previous' => 'value_previous',
          'value_reference' => 'value_reference'
        }, JSON.parse(@event.to_json))
      end

      it "supports v2 serialization" do
        assert_equal({
          id: @event.id,
          type: 'External',
          created_at: @event.created_at,
          via: {type: @event.via_id},
          resource: 'value',
          body: 'value_reference',
          success: 'value_previous'
        }, deep_symbolize_keys(@event.as_json(version: 2)))
      end
    end

    describe "LogMeInTranscript" do
      before do
        @event = LogMeInTranscript.create!(
          account: @account,
          ticket: @account.tickets.last,
          audit: @account.tickets.last.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          value: 'value',
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        )
      end

      it "supports v1 serialization" do
        assert_equal({
          'type' => 'LogMeInTranscript',
          'created_at' => @event.created_at.as_json,
          'via_id' => Zendesk::Types::ViaType.MAIL,
          'author_id' => @event.author.id,
          'is_public' => false,
          'via_reference_id' => 1,
          'value' => 'value',
          'value_previous' => 'value_previous',
          'value_reference' => 'value_reference'
        }, JSON.parse(@event.to_json))
      end

      it "supports v2 serialization" do
        assert_equal({
          id: @event.id,
          type: 'LogMeInTranscript',
          created_at: @event.created_at,
          via: {type: @event.via_id},
          body: 'value'
        }, deep_symbolize_keys(@event.as_json(version: 2)))
      end
    end

    describe "MacroReference" do
      before do
        @event = MacroReference.create!(
          account: @account,
          ticket: @account.tickets.last,
          audit: @account.tickets.last.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          value: 'value',
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        )
      end

      it "supports v1 serialization" do
        assert_equal({
          'type' => 'MacroReference',
          'created_at' => @event.created_at.as_json,
          'via_id' => Zendesk::Types::ViaType.MAIL,
          'author_id' => @event.author.id,
          'is_public' => false,
          'via_reference_id' => 1,
          'value' => 'value',
          'value_previous' => 'value_previous',
          'value_reference' => 'value_reference'
        }, JSON.parse(@event.to_json))
      end

      it "supports v2 serialization" do
        assert_equal({
          id: @event.id,
          type: 'MacroReference',
          created_at: @event.created_at,
          via: {type: @event.via_id},
          macro_id: 'value'
        }, deep_symbolize_keys(@event.as_json(version: 2)))
      end
    end

    describe "Notification" do
      before do
        @recipients = @account.end_users
        @event = Notification.create!(
          account: @account,
          ticket: @account.tickets.last,
          audit: @account.tickets.last.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          value: @recipients.map(&:id),
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        )
      end

      it "supports v1 serialization" do
        assert_equal({
          'type' => 'Notification',
          'created_at' => @event.created_at.as_json,
          'via_id' => Zendesk::Types::ViaType.MAIL,
          'author_id' => @event.author.id,
          'is_public' => false,
          'via_reference_id' => 1,
          'value' => @recipients.map(&:id),
          'value_previous' => 'value_previous',
          'value_reference' => 'value_reference'
        }, JSON.parse(@event.to_json))
      end

      it "supports v2 serialization" do
        assert_equal({
          id: @event.id,
          type: 'Notification',
          created_at: @event.created_at,
          via: {type: @event.via_id},
          subject: 'value_previous',
          body: 'value_reference',
          recipients: @recipients.map { |user| {id: user.id, name: user.name} }
        }, deep_symbolize_keys(@event.as_json(version: 2)))
      end
    end

    describe "Push" do
      before do
        @event = Push.create!(
          account: @account,
          ticket: @account.tickets.last,
          audit: @account.tickets.last.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          value: 'value',
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        )
      end

      it "supports v1 serialization" do
        assert_equal({
          'type' => 'Push',
          'created_at' => @event.created_at.as_json,
          'via_id' => Zendesk::Types::ViaType.MAIL,
          'author_id' => @event.author.id,
          'is_public' => false,
          'via_reference_id' => 1,
          'value' => 'value',
          'value_previous' => 'value_previous',
          'value_reference' => 'value_reference'
        }, JSON.parse(@event.to_json))
      end

      it "supports v2 serialization" do
        assert_equal({
          id: @event.id,
          type: 'Push',
          created_at: @event.created_at,
          via: {type: @event.via_id},
          value: 'value',
          value_reference: 'value_reference'
        }, deep_symbolize_keys(@event.as_json(version: 2)))
      end
    end

    describe "TicketSharingEvent" do
      before do
        @agreement = FactoryBot.create(:agreement, account: @account)

        @event = TicketSharingEvent.create!(
          account: @account,
          ticket: @account.tickets.last,
          audit: @account.tickets.last.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          value: @agreement.id,
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        ).reload
      end

      it "supports v1 serialization" do
        assert_equal({
          'type' => 'TicketSharingEvent',
          'created_at' => @event.created_at.as_json,
          'via_id' => Zendesk::Types::ViaType.MAIL,
          'author_id' => @event.author.id,
          'is_public' => false,
          'via_reference_id' => 1,
          'value' => @agreement.id.to_s,
          'value_previous' => 'value_previous',
          'value_reference' => 'value_reference'
        }, JSON.parse(@event.to_json))
      end

      it "supports v2 serialization" do
        assert_equal({
          id: @event.id,
          type: 'TicketSharingEvent',
          created_at: @event.created_at,
          via: {type: @event.via_id},
          agreement_id: @agreement.id,
        }, deep_symbolize_keys(@event.as_json(version: 2)))
      end
    end

    describe "Tweet" do
      before do
        ticket = tickets(:minimum_5)
        @recipients = @account.end_users
        @event = Tweet.create!(
          account: @account,
          ticket: ticket,
          audit: ticket.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          value: @recipients.map(&:id),
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        )
      end

      it "supports v1 serialization" do
        assert_equal({
          'type' => 'Tweet',
          'created_at' => @event.created_at.as_json,
          'via_id' => Zendesk::Types::ViaType.MAIL,
          'author_id' => @event.author.id,
          'is_public' => false,
          'via_reference_id' => 1,
          'value' => @recipients.pluck(:id),
          'value_previous' => 'value_previous',
          'value_reference' => "value_reference"
        }, JSON.parse(@event.to_json))
      end

      it "supports v2 serialization" do
        assert_equal({
          id: @event.id,
          type: 'Tweet',
          created_at: @event.created_at,
          via: {type: @event.via_id},
          sender_mth_id: 'value_previous',
          recipients: @recipients.map { |user| {id: user.id, name: user.name} },
          body: "value_reference"
        }, deep_symbolize_keys(@event.as_json(version: 2)))
      end
    end

    describe "TwitterDmEvent" do
      before do
        @recipients = @account.end_users
        @event = TwitterDmEvent.create!(
          account: @account,
          ticket: @account.tickets.last,
          audit: @account.tickets.last.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          value: @recipients.pluck(:id),
          value_previous: 'value_previous',
          value_reference: 'value_reference'
        )
      end

      it "supports v1 serialization" do
        assert_equal({
          'type' => 'TwitterDmEvent',
          'created_at' => @event.created_at.as_json,
          'via_id' => Zendesk::Types::ViaType.MAIL,
          'author_id' => @event.author.id,
          'is_public' => false,
          'via_reference_id' => 1,
          'value' => @recipients.pluck(:id),
          'value_previous' => 'value_previous',
          'value_reference' => "value_reference"
        }, JSON.parse(@event.to_json))
      end

      it "supports v2 serialization" do
        assert_equal({
          id: @event.id,
          type: 'TwitterDmEvent',
          created_at: @event.created_at,
          via: {type: @event.via_id},
          sender_mth_id: 'value_previous',
          recipients: @recipients.map { |user| {id: user.id, name: user.name} },
          body: "value_reference"
        }, deep_symbolize_keys(@event.as_json(version: 2)))
      end
    end

    describe "VoiceComment" do
      before do
        Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(false)

        @event = VoiceComment.create!(
          account: @account,
          ticket: @account.tickets.last,
          audit: @account.tickets.last.audits.last,
          author: @account.owner,
          via_id: Zendesk::Types::ViaType.MAIL,
          via_reference_id: 1,
          body: 'Call recording',
          value_previous: {'value' => 'previous'},
          value_reference: 'value_reference'
        )
        @attachment = attachments(:serialization_comment_attachment)
        @event.body = 'Call recording'
        @event.attachments << @attachment
      end

      describe "with body passed in" do
        it "supports v1 serialization" do
          assert_equal({
            'type' => 'VoiceComment',
            'created_at' => @event.created_at.as_json,
            'via_id' => Zendesk::Types::ViaType.MAIL,
            'author_id' => @event.author.id,
            'is_public' => false,
            'via_reference_id' => 1,
            'value_previous' => {'value' => 'previous'},
            'value_reference' => 'value_reference',
            'value' => "minimum five ticket (for search)\nCall Details:\n\nCall from: Unknown\nCall to: Unknown\nTime of call: \nLength of phone call: \n",
            'attachments' => [
              {
                'created_at' => @attachment.created_at.as_json,
                'size' => @attachment.size,
                'content_type' => @attachment.content_type,
                'token' => @attachment.token,
                'url' => @attachment.url,
                'id' => @attachment.id,
                'filename' => @attachment.filename,
                'is_public' => @attachment.is_public
              }
            ]
          }, JSON.parse(@event.to_json))
        end

        it "supports v2 serialization" do
          assert_equal({
            id: @event.id,
            type: 'VoiceComment',
            created_at: @event.created_at,
            via: {type: @event.via_id},
            body: "minimum five ticket (for search)\nCall Details:\n\nCall from: Unknown\nCall to: Unknown\nTime of call: \nLength of phone call: \n",
            html_body: "<div class=\"zd-comment\" dir=\"auto\"><p>minimum five ticket (for search)\n<br>Call Details:</p>\n\n<p>Call from: Unknown\n<br>Call to: Unknown\n<br>Time of call: \n<br>Length of phone call:</p></div>",
            data: {value: 'previous'},
            formatted_from: 'Unknown',
            transcription_visible: false,
            is_public: false,
            attachments: [{
              content_type: @attachment.content_type,
              display_filename: @attachment.display_filename,
              url: @attachment.url
            }]
          }, deep_symbolize_keys(@event.as_json(version: 2)))
        end
      end

      describe "without body passed in" do
        before do
          @event = VoiceComment.create!(
            account: @account,
            ticket: @account.tickets.last,
            audit: @account.tickets.last.audits.last,
            author: @account.owner,
            via_id: Zendesk::Types::ViaType.MAIL,
            via_reference_id: 1,
            value_previous: {'value' => 'previous'},
            value_reference: 'value_reference'
          )
          @attachment = attachments(:serialization_comment_attachment)
          @event.attachments << @attachment
        end

        it "supports v1 serialization" do
          assert_equal({
            'type' => 'VoiceComment',
            'created_at' => @event.created_at.as_json,
            'via_id' => Zendesk::Types::ViaType.MAIL,
            'author_id' => @event.author.id,
            'is_public' => false,
            'via_reference_id' => 1,
            'value_previous' => {'value' => 'previous'},
            'value_reference' => 'value_reference',
            'value' => "minimum five ticket (for search)\nCall Details:\n\nCall from: Unknown\nCall to: Unknown\nTime of call: \nLength of phone call: \n",
            'attachments' => [
              {
                'created_at' => @attachment.created_at.as_json,
                'size' => @attachment.size,
                'content_type' => @attachment.content_type,
                'token' => @attachment.token,
                'url' => @attachment.url,
                'id' => @attachment.id,
                'filename' => @attachment.filename,
                'is_public' => @attachment.is_public
              }
            ]
          }, JSON.parse(@event.to_json))
        end

        it "supports v2 serialization" do
          assert_equal({
            id: @event.id,
            type: 'VoiceComment',
            created_at: @event.created_at,
            via: {type: @event.via_id},
            body: "minimum five ticket (for search)\nCall Details:\n\nCall from: Unknown\nCall to: Unknown\nTime of call: \nLength of phone call: \n",
            html_body: "",
            data: {value: 'previous'},
            formatted_from: 'Unknown',
            transcription_visible: false,
            is_public: false,
            attachments: [{
              content_type: @attachment.content_type,
              display_filename: @attachment.display_filename,
              url: @attachment.url
            }]
          }, deep_symbolize_keys(@event.as_json(version: 2)))
        end
      end
    end
  end

  describe "Comments" do
    before do
      @comment = Comment.find(events(:serialization_comment).id)
      assert @comment.attachments.present?
    end

    it "includes attachment details when serialized as JSON" do
      assert JSON.parse(@comment.to_json)['attachments'].first['url'].present?
    end
  end
end
