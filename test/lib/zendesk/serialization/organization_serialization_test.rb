require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::Serialization::OrganizationSerialization do
  fixtures :organizations, :subscription_features

  before do
    @organization = organizations(:serialization_organization)
  end

  describe "with v1 serialization" do
    it "serializes to XML consistently" do
      document = Nokogiri::XML::Builder.new do |xml|
        xml.organization do
          xml.created_at(type: 'datetime') { xml.text(@organization.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.current_tags(nil: 'true')
          xml.details(nil: 'true')
          xml.external_id { xml.text(@organization.external_id) }
          xml.group_id(type: 'integer') { @organization.group_id }
          xml.id_(type: 'integer') { @organization.id }
          xml.is_shared(type: 'boolean') { xml.text(@organization.is_shared?) }
          xml.is_shared_comments(type: 'boolean') { xml.text(@organization.is_shared_comments?) }
          xml.name { xml.text(@organization.name) }
          xml.notes(nil: 'true')
          xml.updated_at(type: 'datetime') { xml.text(@organization.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
        end
      end

      expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
      actual   = Nokogiri::XML(@organization.to_xml, &:noblanks)

      assert_document_equality(expected, actual)
    end

    it "supports v1 serialization" do
      assert_equal({
        'name' => 'Serialization',
        'created_at' => @organization.created_at.as_json,
        'updated_at' => @organization.updated_at.as_json,
        'external_id' => @organization.external_id,
        'details' => nil,
        'current_tags' => '',
        'is_shared_comments' => false,
        'notes' => nil,
        'group_id' => @organization.group.id,
        'id' => @organization.id,
        'is_shared' => false
      }, JSON.parse(@organization.to_json))
    end
  end
end
