require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Serialization::ReportSerialization do
  before do
    @report = Report.new(title: "The Report") do |report|
      report.data = {
        legends: [{ color: "FF0D00", summable: false, name: "Working tickets" }],
        data: {
          Date.parse("Tue, 13 Dec 2011") => {"Working tickets" => 4},
          Date.parse("Sun, 04 Dec 2011") => {"Working tickets" => 6},
          Date.parse("Mon, 21 Nov 2011") => {"Working tickets" => 1},
          Date.parse("Mon, 05 Dec 2011") => {"Working tickets" => 4},
          Date.parse("Sat, 26 Nov 2011") => {"Working tickets" => 6}
        }
      }
    end
  end

  it "supports XML serialization" do
    xml = Nokogiri::XML(@report.to_xml)
    xml.to_s.must_include "<legend>Working tickets</legend>"
    values = xml.xpath("//value")
    assert values.any? { |v| v["date"] == "2011-11-21" && v["data"] == "1" }
  end

  it "supports JSON serialization" do
    data = JSON.parse(@report.to_json)
    assert_equal @report.title, data["title"]
  end
end
