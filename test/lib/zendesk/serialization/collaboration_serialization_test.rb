require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Serialization::CollaborationSerialization do
  before do
    @ticket_nice_id = 789
    @collaboration  = Collaboration.new
    @collaboration.user_id    = 456
    @collaboration.id         = 888
    @collaboration.ticket_id  = 123
    @collaboration.updated_at = Time.now
    @collaboration.created_at = Time.parse("2010-02-03 10:11:12")
    @collaboration.stubs(:ticket).returns(stub(nice_id: @ticket_nice_id))
  end

  it "serializes to XML consistently" do
    document = Nokogiri::XML::Builder.new do |xml|
      xml.collaboration do
        xml.id_(type: 'integer')         { @collaboration.id }
        xml.ticket_id(type: 'integer')   { xml.text(@ticket_nice_id) }
        xml.user_id(type: 'integer')     { xml.text(@collaboration.user_id) }
        xml.created_at(type: 'datetime') { xml.text(@collaboration.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
      end
    end

    expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
    actual   = Nokogiri::XML(@collaboration.to_xml, &:noblanks)

    assert_document_equality(expected, actual)
  end

  it "serializes to JSON consistently" do
    assert_equal({
      'id'         => @collaboration.id,
      'created_at' => @collaboration.created_at.as_json,
      'ticket_id'  => @ticket_nice_id,
      'user_id'    => @collaboration.user_id
    }, JSON.parse(@collaboration.to_json))
  end
end
