require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::Serialization::ForumSerialization do
  fixtures :forums

  before do
    @forum = forums(:serialization_forum)
  end

  it "serializes to XML consistently" do
    document = Nokogiri::XML::Builder.new do |xml|
      xml.forum do
        xml.id_(type: 'integer') { @forum.id }
        xml.name { xml.text(@forum.name) }
        xml.description { xml.text(@forum.description) }
        xml.is_locked(type: 'boolean')   { xml.text(@forum.is_locked) }
        xml.is_public(type: 'boolean')   { xml.text(@forum.is_public) }
        xml.use_for_suggestions(type: 'boolean') { xml.text(@forum.use_for_suggestions) }
        xml.display_type_id(type: 'integer') { xml.text(@forum.display_type_id) }
        xml.visibility_restriction_id(type: 'integer') { xml.text(@forum.visibility_restriction_id) }
        xml.entries_count(type: 'integer') { xml.text(@forum.entries_count) }
        xml.organization_id(type: 'integer') { xml.text(@forum.organization_id) }
        xml.category_id(type: 'integer') { xml.text(@forum.category_id) }
        xml.position(type: 'integer') { xml.text(@forum.position) }
        xml.translation_locale_id(type: 'integer') { xml.text(@forum.translation_locale_id) }
        xml.updated_at(type: 'datetime') { xml.text(@forum.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
      end
    end

    expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
    actual   = Nokogiri::XML(@forum.to_xml, &:noblanks)

    assert_document_equality(expected, actual)
  end
end
