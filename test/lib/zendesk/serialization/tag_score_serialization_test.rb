require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Serialization::TagScoreSerialization do
  before do
    @tag_score = TagScore.new(tag_name: "horse", score: 123, account_id: 45)
    @tag_score.id = 34
  end

  it "supports v1 serialization" do
    assert_equal({
      "id"     => 34,
      "name"   => "horse",
      "count"  => 123
    }, JSON.parse(@tag_score.to_json))
  end

  it "supports v2 serialization" do
    assert_equal({
      "tag_name" => @tag_score.tag_name,
      "score"    => @tag_score.score
    }, JSON.parse(@tag_score.to_json(version: 2)))
  end

  it "supports v1 XML serialization" do
    document = Nokogiri::XML::Builder.new do |xml|
      xml.tag do
        xml.id_(type: 'integer') { xml.text(@tag_score.id) }
        xml.count(@tag_score.score)
        xml.name(@tag_score.tag_name)
      end
    end

    expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
    actual   = Nokogiri::XML(@tag_score.to_xml, &:noblanks)

    assert_document_equality(expected, actual)
  end
end
