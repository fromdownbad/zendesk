require_relative "../../../../support/test_helper"
require "zendesk/search"

SingleCov.covered! uncovered: 1

describe Zendesk::Serialization::Search::ResultsSerialization do
  fixtures :tickets, :entries, :organizations

  describe "v2 serialization" do
    before do
      @ticket = tickets(:minimum_1)
      @entry = entries(:apples)
      @user = users(:minimum_end_user)
      @results = Zendesk::Search::Results.empty
    end

    it "serialize result_type" do
      @results.replace([@ticket, @entry, @user])
      json = @results.as_json(version: 2)
      assert_equal(3, json[:payload].size)
      assert_equal(:ticket, json[:payload][0][:result_type])
      assert_equal(:entry, json[:payload][1][:result_type])
      assert_equal(:user, json[:payload][2][:result_type])
      assert json[:count]
    end

    it "serializes requester info for Ticket results" do
      @results.replace([@ticket])
      json = @results.as_json(version: 2).with_indifferent_access

      assert_equal(1, json[:payload].size)
      assert_equal(:ticket, json[:payload][0][:result_type])
      assert_equal({"name" => "Author Minimum", "id" => @ticket.requester_id}, json[:payload][0][:result][:requester])
    end

    it "serializes forum info for Entry results" do
      @results.replace([@entry])
      json = @results.as_json(version: 2).with_indifferent_access

      assert_equal(1, json[:payload].size)
      assert_equal(:entry, json[:payload][0][:result_type])
      assert_equal({"name" => "Agents only", "id" => @entry.forum_id}, json[:payload][0][:result][:forum])
    end

    it "serialize organization info for User results" do
      @results.replace([@user])
      json = @results.as_json(version: 2).with_indifferent_access

      assert_equal(1, json[:payload].size)
      assert_equal(:user, json[:payload][0][:result_type])
      assert_equal({"name" => "minimum organization", 'tags' => [], "id" => @user.organization_id}, json[:payload][0][:result][:organization])
    end
  end

  describe "Results" do
    before do
      @results = Zendesk::Search::Results.empty
    end

    it "converts to json" do
      json = "[]"
      assert_equal json, @results.to_json
    end

    it "correctly convert mixed result sets to json" do
      results = Zendesk::Search::Results.empty
      results.replace([Entry.new, Ticket.new { |ticket| ticket.nice_id = 1; ticket.account = accounts(:minimum) }])
      decoded = ActiveSupport::JSON.decode(results.to_json)

      assert_equal 1, decoded.last['nice_id']
    end

    it "converts to xml" do
      xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<records type=\"array\" count=\"0\"/>\n"
      assert_equal xml, @results.to_xml
    end
  end
end
