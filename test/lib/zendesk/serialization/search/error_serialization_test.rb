require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Serialization::Search::ErrorSerialization do
  fixtures :tickets, :entries, :organizations

  before do
    @error = Zendesk::Search::Error.new(Timeout::Error.new)
  end

  it "converts to json" do
    json = '{"error":"Sorry, we could not complete your search query. Please try again in a moment."}'
    assert_equal json, @error.to_json
  end

  it "converts to xml" do
    xml = "<error><![CDATA[Sorry, we could not complete your search query. Please try again in a moment.]]></error>"
    assert_equal xml, @error.to_xml
  end

  it "wraps malicious xml in cdata tags" do
    expected = "<error><![CDATA[Invalid search: XML is a pain ]]]]><![CDATA[< in the assets]]></error>"
    assert_equal expected, Zendesk::Search::Error.new(ZendeskSearch::QueryError.new("XML is a pain ]]> in the assets")).to_xml
  end
end
