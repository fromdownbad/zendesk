require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Serialization::SmsNotificationSerialization do
  fixtures :accounts, :events, :users

  let!(:account) { accounts(:minimum) }

  before do
    @audit = events(:create_audit_for_minimum_ticket_1)
    @recipient = users(:minimum_end_user)
    @sms_notification = SmsNotification.new(recipients: [@recipient], body: 'This is a test message', audit: @audit, account: account)
  end

  it 'serializes correctly' do
    result = JSON.parse(@sms_notification.to_json(version: 2))

    assert_equal([{
      'id' => @recipient.id,
      'name' => @recipient.name
    }], result['recipients'])
    assert_equal('This is a test message', result['body'])
  end
end
