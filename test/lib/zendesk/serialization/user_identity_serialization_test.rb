require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Zendesk::Serialization::UserIdentitySerialization do
  fixtures :user_identities

  describe "User Email Identities" do
    before do
      @user_identity = user_identities(:serialization_user_identity)
    end

    it "serializes to XML consistently" do
      document = Nokogiri::XML::Builder.new do |xml|
        xml.user_email_identity do
          xml.account_id(type: 'integer') { xml.text(@user_identity.account_id) }
          xml.created_at(type: 'datetime') { xml.text(@user_identity.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.deliverable_state(type: 'integer') { xml.text(@user_identity.deliverable_state) }
          xml.id_(type: 'integer') { xml.text(@user_identity.id) }
          xml.is_verified(type: 'boolean') { xml.text(@user_identity.is_verified) }
          xml.updated_at(type: 'datetime') { xml.text(@user_identity.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.user_id(type: 'integer') { xml.text(@user_identity.user_id) }
          xml.value { xml.text(@user_identity.value) }
          xml.identity_type { xml.text(@user_identity.identity_type) }
        end
      end

      expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
      actual   = Nokogiri::XML(@user_identity.to_xml, &:noblanks)

      assert_document_equality(expected, actual)
    end
  end

  describe "User Twitter Identities" do
    before do
      @user_identity = user_identities(:serialization_user_twitter_identity)
      @user_identity.stubs(:screen_name).returns('zendesk_test')
    end

    it "serializes to XML consistently" do
      document = Nokogiri::XML::Builder.new do |xml|
        xml.user_twitter_identity do
          xml.id_(type: 'integer') { xml.text(@user_identity.id) }
          xml.account_id(type: 'integer') { xml.text(@user_identity.account_id) }
          xml.user_id(type: 'integer') { xml.text(@user_identity.user_id) }
          xml.value { xml.text(@user_identity.value) }
          xml.deliverable_state(type: 'integer') { xml.text(@user_identity.deliverable_state) }
          xml.screen_name { xml.text(@user_identity.screen_name) }
          xml.identity_type { xml.text(@user_identity.identity_type) }
          xml.is_verified(type: 'boolean') { xml.text(@user_identity.is_verified) }
          xml.created_at(type: 'datetime') { xml.text(@user_identity.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.updated_at(type: 'datetime') { xml.text(@user_identity.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
        end
      end

      expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
      actual   = Nokogiri::XML(@user_identity.to_xml, &:noblanks)

      assert_document_equality(expected, actual)
    end
  end

  describe "User Phone Number Identities" do
    before do
      @user_identity = user_identities(:serialization_user_phone_number_identity)
    end

    it "serializes to XML consistently" do
      document = Nokogiri::XML::Builder.new do |xml|
        xml.user_phone_number_identity do
          xml.id_(type: 'integer') { xml.text(@user_identity.id) }
          xml.account_id(type: 'integer') { xml.text(@user_identity.account_id) }
          xml.user_id(type: 'integer') { xml.text(@user_identity.user_id) }
          xml.deliverable_state(type: 'integer') { xml.text(@user_identity.deliverable_state) }
          xml.value { xml.text(@user_identity.value) }
          xml.identity_type { xml.text(@user_identity.identity_type) }
          xml.is_verified(type: 'boolean') { xml.text(@user_identity.is_verified) }
          xml.created_at(type: 'datetime') { xml.text(@user_identity.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.updated_at(type: 'datetime') { xml.text(@user_identity.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
        end
      end

      expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
      actual   = Nokogiri::XML(@user_identity.to_xml, &:noblanks)

      assert_document_equality(expected, actual)
    end
  end

  describe "User Voice Forwarding Identities" do
    before do
      @user_identity = user_identities(:serialization_user_voice_forwarding_identity)
    end

    it "serializes to XML consistently" do
      document = Nokogiri::XML::Builder.new do |xml|
        xml.user_voice_forwarding_identity do
          xml.id_(type: 'integer') { xml.text(@user_identity.id) }
          xml.account_id(type: 'integer') { xml.text(@user_identity.account_id) }
          xml.user_id(type: 'integer') { xml.text(@user_identity.user_id) }
          xml.deliverable_state(type: 'integer') { xml.text(@user_identity.deliverable_state) }
          xml.value { xml.text(@user_identity.value) }
          xml.identity_type { xml.text(@user_identity.identity_type) }
          xml.is_verified(type: 'boolean') { xml.text(@user_identity.is_verified) }
          xml.created_at(type: 'datetime') { xml.text(@user_identity.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.updated_at(type: 'datetime') { xml.text(@user_identity.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
        end
      end

      expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
      actual   = Nokogiri::XML(@user_identity.to_xml, &:noblanks)

      assert_document_equality(expected, actual)
    end
  end
end
