require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Serialization::PreviewResultsSerialization do
  fixtures :accounts, :users, :rules, :subscriptions, :tickets, :groups, :memberships, :organizations, :addresses, :ticket_fields, :ticket_field_entries, :account_property_sets

  describe "V1" do
    before do
      ticket   = tickets(:minimum_1)
      @results = Zendesk::Rules::Preview::Results.new(1, 10, 1)
      @results.replace([ticket])
    end

    it "converts to JSON" do
      json = ActiveSupport::JSON.decode(@results.to_json)

      assert_equal 1,     json['count']
      assert_equal Array, json['tickets'].class

      ticket = json['tickets'].first
      assert_equal 1, ticket['nice_id']
      assert_equal false, ticket.key?('ticket_field_entries'), "Should not include ticket field entries"
    end

    it "converts to XML" do
      tickets = parse_response(@results.to_xml, :xml)
      assert_equal Array, tickets.class

      ticket = tickets.first
      assert_equal 1,     ticket['nice_id']
      assert_equal false, ticket.key?('ticket_field_entries'), "Should not include ticket field entries"
    end
  end
end
