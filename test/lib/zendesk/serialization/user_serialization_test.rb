require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::Serialization::UserSerialization do
  fixtures :users, :accounts, :user_identities, :taggings, :signatures

  before do
    @user = users(:serialization_user)
    @user.account.settings.has_user_tags = true
  end

  it "serializes to XML consistently" do
    document = Nokogiri::XML::Builder.new do |xml|
      xml.user do
        xml.created_at(type: 'datetime') { xml.text(@user.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
        xml.details(nil: 'true')
        xml.external_id(nil: 'true')
        xml.id_(type: 'integer') { xml.text(@user.id) }
        xml.last_login(nil: 'true')
        xml.locale_id(type: 'integer', nil: true)
        xml.name { xml.text(@user.name) }
        xml.notes(nil: 'true')
        xml.openid_url(nil: 'true')
        xml.organization_id(type: 'integer', nil: true)
        xml.phone(nil: 'true')
        xml.restriction_id(type: 'integer') { xml.text(@user.restriction_id) }
        xml.roles(type: 'integer') { xml.text(@user.roles) }
        xml.time_zone { xml.text(@user.time_zone) }
        xml.updated_at(type: 'datetime') { xml.text(@user.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
        xml.uses_12_hour_clock(type: 'boolean') { xml.text(@user.uses_12_hour_clock) }
        xml.current_tags { xml.text(@user.current_tags) }
        xml.email { xml.text(@user.email) }
        xml.is_verified(type: 'boolean') { xml.text(@user.is_verified) }
        xml.custom_role_id(type: 'integer') { xml.text(@user.custom_role_id) }
        xml.is_active(type: 'boolean') { xml.text(@user.is_active) }
        xml.photo_url { xml.text("https://serialization.zendesk-test.com/images/2016/default-avatar-80.png") }
        xml.groups(type: 'array')
      end
    end

    expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
    actual   = Nokogiri::XML(@user.to_xml(methods: [:current_tags], include: [:groups]), &:noblanks)

    assert_document_equality(expected, actual)
  end

  it "serializes to JSON consistently" do
    user_json = {
      "locale_id" => nil,
      "name" => "Serialization User",
      "created_at" => "2009/10/11 12:13:14 +0000",
      "openid_url" => nil,
      "details" => nil,
      "last_login" => @user.last_login.strftime("%Y/%m/%d %H:%M:%S +0000"),
      "notes" => nil,
      "updated_at" => "2009/10/11 12:13:14 +0000",
      "external_id" => nil,
      "id" => @user.id,
      "restriction_id" => 0,
      "is_active" => true,
      "phone" => nil,
      "is_verified" => true,
      "time_zone" => "Copenhagen",
      "organization_id" => nil,
      "roles" => 0,
      "current_tags" => "foo",
      "custom_role_id" => @user.custom_role_id,
      "groups" => [],
      "uses_12_hour_clock" => false,
      "email" => "user@serialization.com",
      "photo_url" => "https://serialization.zendesk-test.com/images/2016/default-avatar-80.png"
    }

    attributes = JSON.parse(@user.to_json(methods: [:current_tags], include: [:groups]))
    assert_equal([], attributes.keys.sort - user_json.keys.sort,
      "A new attribute was exposed to the User API.")

    assert_equal(user_json, attributes)

    v2_user_json = user_json.merge(
      "tags" => ["foo"],
      "is_moderator" => @user.is_moderator,
      "is_verified" => @user.is_verified,
      "locale" => @user.locale,
      "alias" => @user.alias,
      "shared" => @user.foreign?,
      "signature" => @user.agent_signature(rich: false),
      "private_comments_only" => @user.private_comments_only,
      "photo" => nil
    )

    v2_user_json.delete("openid_url")
    v2_user_json.delete("current_tags")
    v2_user_json.delete("groups")

    attributes = JSON.parse(@user.to_json(version: 2))

    assert_equal([], attributes.keys.sort - v2_user_json.keys.sort,
      "A new attribute was exposed to the User API v2.")

    assert_equal(v2_user_json, attributes)
  end

  it "sanitizes data for chat system_user" do
    user_json = {
      "name" => "Serialization User",
      "organization_id" => nil,
      "id" => @user.id
    }
    current_user = stub(is_system_user?: true)
    attributes = JSON.parse(@user.to_json(version: 2, current_user: current_user, current_username: "chat"))
    assert_equal(user_json, attributes)
  end
end
