require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Serialization::PhotoSerialization do
  before do
    @photo = FactoryBot.create(:photo, filename: "large_1.png")
  end

  it "supports v1 serialization" do
    assert_equal({
      "created_at"      => @photo.created_at.as_json,
      "size"            => @photo.size,
      "content_type"    => @photo.content_type,
      "thumbnail"       => nil,
      "updated_at"      => @photo.updated_at.as_json,
      "account_id"      => @photo.account_id,
      "id"              => @photo.id,
      "user_id"         => @photo.user_id,
      "filename"        => @photo.filename,
      "height"          => @photo.height,
      "parent_id"       => nil,
      "stores"          => ["fs"],
      "md5"             => @photo.md5,
      "original_url"    => @photo.original_url,
      "public_filename" => @photo.public_filename,
      "width"           => @photo.width
    }, JSON.parse(@photo.to_json(version: 1)))
  end

  it "supports v2 serialization" do
    assert_equal({
      "created_at"   => @photo.created_at.as_json,
      "size"         => @photo.size,
      "content_type" => @photo.content_type,
      "id"           => @photo.id,
      "url"          => @photo.account.url(ssl: true) + @photo.public_filename,
      "thumbnails"   => @photo.thumbnails.map { |t| t.as_json(version: 2).stringify_keys! },
      "name"         => @photo.filename
    }, JSON.parse(@photo.to_json(version: 2)))
  end
end
