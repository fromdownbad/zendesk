require_relative "../../../support/test_helper"
require 'zendesk/search'

SingleCov.covered!

describe Zendesk::Serialization::ExternalLinkCollectionSerialization do
  fixtures :accounts, :users, :subscriptions, :payments, :addresses, :tickets,
    :linkings, :jira_issues

  before do
    ActiveSupport::TimeWithZone.stubs(:new).returns(Time.parse('2011-12-30T23:00:28Z UT'))
  end

  describe "Collection" do
    before do
      @ticket     = tickets(:minimum_1)
      @collection = ExternalLink::Collection.new(@ticket.linkings)
    end

    it "converts to json" do
      assert_equal JSON.parse(json(@ticket)), JSON.parse(@collection.to_json)
    end

    it "converts to xml" do
      expected = xml(@ticket).strip
      expected.sub!("<external-link>", '<external-link type="JiraIssue">')
      assert_equal parser.parse(expected), parser.parse(@collection.to_xml)
    end
  end

  def parser
    @parser ||= Nori.new(parser: :nokogiri)
  end

  def json(ticket)
    <<-JSON
      [{"created_at":"2011/12/30 23:00:28 +0000",
        "updated_at":"2011/12/30 23:00:28 +0000",
        "external_link_id":1,
        "external_link_type":"JiraIssue",
        "account_id":#{ticket.account.id},
        "external_link":{
          "created_at":"2011/12/30 23:00:28 +0000",
          "updated_at":"2011/12/30 23:00:28 +0000",
          "issue_id":"XYZ-15",
          "account_id":#{ticket.account.id},
          "id":1
        },
        "ticket_id":#{ticket.id},
        "id":1
       }]
    JSON
  end

  def xml(ticket)
    <<-XML
      <?xml version="1.0" encoding="UTF-8"?>
      <linkings type="array" count="1">
        <linking>
          <account-id type="integer">#{ticket.account.id}</account-id>
          <created-at type="datetime">2011-12-30T23:00:28Z</created-at>
          <external-link-id type="integer">1</external-link-id>
          <external-link-type>JiraIssue</external-link-type>
          <id type="integer">1</id>
          <ticket-id type="integer">#{ticket.id}</ticket-id>
          <updated-at type="datetime">2011-12-30T23:00:28Z</updated-at>
          <external-link>
            <account-id type="integer">#{ticket.account.id}</account-id>
            <created-at type="datetime">2011-12-30T23:00:28Z</created-at>
            <id type="integer">1</id>
            <issue-id>XYZ-15</issue-id>
            <updated-at type="datetime">2011-12-30T23:00:28Z</updated-at>
          </external-link>
        </linking>
      </linkings>
    XML
  end
end
