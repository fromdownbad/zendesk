require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 16

describe Zendesk::Serialization::TicketSerialization do
  fixtures :tickets, :monitored_twitter_handles, :channels_user_profiles

  before do
    @ticket = tickets(:serialization_ticket)
  end

  describe "v1" do
    before do
      Account.any_instance.stubs(:has_ticket_forms?).returns(false)
      Account.any_instance.stubs(:has_multibrand?).returns(false)
    end

    it "serializes to XML consistently" do
      document = Nokogiri::XML::Builder.new do |xml|
        xml.ticket do
          xml.nice_id(type: 'integer') { @ticket.id }
          xml.created_at(type: 'datetime') { xml.text(@ticket.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.updated_at(type: 'datetime') { xml.text(@ticket.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.assigned_at(type: 'datetime') { xml.text(@ticket.assigned_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.has_incidents
          xml.initially_assigned_at(type: 'datetime') { xml.text(@ticket.initially_assigned_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.status_updated_at(type: 'datetime') { xml.text(@ticket.status_updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.solved_at(type: 'datetime', nil: 'true')
          xml.due_date(type: 'datetime', nil: 'true')
          xml.group_id(type: 'integer') { @ticket.group_id }
          xml.entry_id(type: 'integer', nil: 'true')
          xml.submitter_id(type: 'integer') { @ticket.submitter_id }
          xml.assignee_id(type: 'integer') { @ticket.assignee_id }
          xml.requester_id(type: 'integer') { @ticket.requester_id }
          xml.number_of_incidents
          xml.organization_id(type: 'integer', nil: 'true')
          xml.via_id(type: 'integer') { @ticket.via_id }
          xml.status_id(type: 'integer') { @ticket.status_id }
          xml.priority_id(type: 'integer') { @ticket.priority_id }
          xml.ticket_type_id(type: 'integer') { @ticket.ticket_type_id }
          xml.updated_by_type_id(type: 'integer', nil: 'true')
          xml.problem_id(type: 'integer', nil: 'true')
          xml.resolution_time(type: 'integer', nil: 'true')
          xml.external_id(type: 'integer', nil: 'true')
          xml.original_recipient_address(type: 'integer', nil: 'true')
          xml.current_tags { xml.text(@ticket.current_tags) }
          xml.current_collaborators
          xml.description { xml.text(@ticket.description) }
          xml.recipient { xml.text(@ticket.recipient) }
          xml.subject { xml.text(@ticket.subject) }
          xml.ticket_field_entries(type: 'array')
          xml.channel
          xml.comments(type: 'array')
          xml.latest_agent_comment_added_at
          xml.latest_public_comment_added_at
          xml.linkings(type: 'array')
          xml.permissions(type: 'array')
          xml.via_reference_id(type: 'integer') { @ticket.via_reference_id }
          xml.is_public(type: 'boolean') { @ticket.is_public }
          xml.custom_status_id(type: 'integer', nil: 'true') { @ticket.custom_status_id }
        end
      end

      expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
      actual   = Nokogiri::XML(@ticket.to_xml, &:noblanks)

      assert_document_equality(expected, actual)
    end
  end

  describe "v2" do
    before do
      Account.any_instance.stubs(:has_ticket_forms?).returns(false)
      Account.any_instance.stubs(:has_multibrand?).returns(false)
    end

    it "serializes to XML consistently" do
      document = Nokogiri::XML::Builder.new do |xml|
        xml.ticket do
          xml.nice_id(type: 'integer') { @ticket.id }
          xml.created_at(type: 'datetime') { xml.text(@ticket.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.updated_at(type: 'datetime') { xml.text(@ticket.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.assigned_at(type: 'datetime') { xml.text(@ticket.assigned_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.has_incidents
          xml.initially_assigned_at(type: 'datetime') { xml.text(@ticket.initially_assigned_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.status_updated_at(type: 'datetime') { xml.text(@ticket.status_updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.solved_at(type: 'datetime', nil: 'true')
          xml.due_date(type: 'datetime', nil: 'true')
          xml.group { @ticket.group.to_xml(builder: xml) }
          xml.group_id(type: 'integer') { @ticket.group_id }
          xml.entry_id(type: 'integer', nil: 'true')
          xml.submitter_id(type: 'integer') { @ticket.submitter_id }
          xml.assignee { @ticket.assignee.to_xml(builder: xml) }
          xml.assignee_id(type: 'integer') { @ticket.assignee_id }
          xml.requester(type: 'integer') { @ticket.requester.to_xml(builder: xml) }
          xml.requester_id(type: 'integer') { @ticket.requester_id }
          xml.number_of_incidents
          xml.organization_id(type: 'integer', nil: 'true')
          xml.via_id(type: 'integer') { @ticket.via_id }
          xml.status_id(type: 'integer') { @ticket.status_id }
          xml.priority_id(type: 'integer') { @ticket.priority_id }
          xml.ticket_type_id(type: 'integer') { @ticket.ticket_type_id }
          xml.updated_by_type_id(type: 'integer', nil: 'true')
          xml.problem_id(type: 'integer', nil: 'true')
          xml.resolution_time(type: 'integer', nil: 'true')
          xml.external_id(type: 'integer', nil: 'true')
          xml.original_recipient_address(type: 'integer', nil: 'true')
          xml.current_tags { xml.text(@ticket.current_tags) }
          xml.current_collaborators
          xml.description { xml.text(@ticket.description) }
          xml.recipient { xml.text(@ticket.recipient) }
          xml.subject { xml.text(@ticket.subject) }
          xml.ticket_field_entries(type: 'array')
          xml.channel
          xml.comments(type: 'array')
          xml.latest_agent_comment_added_at
          xml.latest_public_comment_added_at
          xml.linkings(type: 'array')
          xml.permissions(type: 'array')
          xml.via_reference_id(type: 'integer') { @ticket.via_reference_id }
          xml.is_public(type: 'boolean') { @ticket.is_public }
          xml.custom_status_id(type: 'integer', nil: 'true') { @ticket.custom_status_id }
        end
      end

      expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
      actual   = Nokogiri::XML(@ticket.to_xml(version: 2), &:noblanks)

      assert_document_equality(expected, actual)
    end
  end

  describe "created via Twitter" do
    before do
      Account.any_instance.stubs(:has_ticket_forms?).returns(false)
      Account.any_instance.stubs(:has_multibrand?).returns(false)

      @ticket = tickets(:serialization_ticket)
      @tup = channels_user_profiles(:minimum_twitter_user_profile_1)
      @mth = monitored_twitter_handles(:serialization_monitored_twitter_handle)
    end

    it "serializes to XML consistently" do
      document = Nokogiri::XML::Builder.new do |xml|
        xml.ticket do
          xml.nice_id(type: 'integer') { @ticket.id }
          xml.created_at(type: 'datetime') { xml.text(@ticket.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.updated_at(type: 'datetime') { xml.text(@ticket.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.assigned_at(type: 'datetime') { xml.text(@ticket.assigned_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.has_incidents
          xml.initially_assigned_at(type: 'datetime') { xml.text(@ticket.initially_assigned_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.status_updated_at(type: 'datetime') { xml.text(@ticket.status_updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.solved_at(type: 'datetime', nil: 'true')
          xml.due_date(type: 'datetime', nil: 'true')
          xml.group_id(type: 'integer') { @ticket.group_id }
          xml.entry_id(type: 'integer', nil: 'true')
          xml.submitter_id(type: 'integer') { @ticket.submitter_id }
          xml.assignee_id(type: 'integer') { @ticket.assignee_id }
          xml.requester_id(type: 'integer') { @ticket.requester_id }
          xml.number_of_incidents
          xml.organization_id(type: 'integer', nil: 'true')
          xml.via_id(type: 'integer') { @ticket.via_id }
          xml.status_id(type: 'integer') { @ticket.status_id }
          xml.priority_id(type: 'integer') { @ticket.priority_id }
          xml.ticket_type_id(type: 'integer') { @ticket.ticket_type_id }
          xml.updated_by_type_id(type: 'integer', nil: 'true')
          xml.problem_id(type: 'integer', nil: 'true')
          xml.resolution_time(type: 'integer', nil: 'true')
          xml.external_id(type: 'integer', nil: 'true')
          xml.original_recipient_address(type: 'integer', nil: 'true')
          xml.current_tags { xml.text(@ticket.current_tags) }
          xml.current_collaborators
          xml.description { xml.text(@ticket.description) }
          xml.recipient { xml.text(@ticket.recipient) }
          xml.subject { xml.text(@ticket.subject) }
          xml.ticket_field_entries(type: 'array')
          xml.channel { xml.text(@tup.screen_name) }
          xml.comments(type: 'array')
          xml.latest_agent_comment_added_at
          xml.latest_public_comment_added_at
          xml.linkings(type: 'array')
          xml.permissions(type: 'array')
          xml.via_reference_id(type: 'integer') { @ticket.via_reference_id }
          xml.is_public(type: 'boolean') { @ticket.is_public }
          xml.custom_status_id(type: 'integer', nil: 'true') { @ticket.custom_status_id }
        end
      end

      expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
      actual   = Nokogiri::XML(@ticket.to_xml, &:noblanks)

      assert_document_equality(expected, actual)
    end
  end

  describe "account has ticket forms feature" do
    before do
      Account.any_instance.stubs(:has_ticket_forms?).returns(true)
      Account.any_instance.stubs(:has_multibrand?).returns(false)
    end

    it "serializes to XML consistently and include ticket_form_id" do
      document = Nokogiri::XML::Builder.new do |xml|
        xml.ticket do
          xml.nice_id(type: 'integer') { @ticket.id }
          xml.created_at(type: 'datetime') { xml.text(@ticket.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.updated_at(type: 'datetime') { xml.text(@ticket.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.assigned_at(type: 'datetime') { xml.text(@ticket.assigned_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.has_incidents
          xml.initially_assigned_at(type: 'datetime') { xml.text(@ticket.initially_assigned_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.status_updated_at(type: 'datetime') { xml.text(@ticket.status_updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.solved_at(type: 'datetime', nil: 'true')
          xml.due_date(type: 'datetime', nil: 'true')
          xml.group_id(type: 'integer') { @ticket.group_id }
          xml.entry_id(type: 'integer', nil: 'true')
          xml.submitter_id(type: 'integer') { @ticket.submitter_id }
          xml.assignee_id(type: 'integer') { @ticket.assignee_id }
          xml.requester_id(type: 'integer') { @ticket.requester_id }
          xml.number_of_incidents
          xml.organization_id(type: 'integer', nil: 'true')
          xml.via_id(type: 'integer') { @ticket.via_id }
          xml.status_id(type: 'integer') { @ticket.status_id }
          xml.priority_id(type: 'integer') { @ticket.priority_id }
          xml.ticket_type_id(type: 'integer') { @ticket.ticket_type_id }
          xml.updated_by_type_id(type: 'integer', nil: 'true')
          xml.problem_id(type: 'integer', nil: 'true')
          xml.resolution_time(type: 'integer', nil: 'true')
          xml.external_id(type: 'integer', nil: 'true')
          xml.original_recipient_address(type: 'integer', nil: 'true')
          xml.current_tags { xml.text(@ticket.current_tags) }
          xml.current_collaborators
          xml.description { xml.text(@ticket.description) }
          xml.recipient { xml.text(@ticket.recipient) }
          xml.subject { xml.text(@ticket.subject) }
          xml.ticket_field_entries(type: 'array')
          xml.channel
          xml.comments(type: 'array')
          xml.latest_agent_comment_added_at
          xml.latest_public_comment_added_at
          xml.linkings(type: 'array')
          xml.permissions(type: 'array')
          xml.ticket_form_id(type: 'integer') { @ticket.ticket_form_id }
          xml.via_reference_id(type: 'integer') { @ticket.via_reference_id }
          xml.is_public(type: 'boolean') { @ticket.is_public }
          xml.custom_status_id(type: 'integer', nil: 'true') { @ticket.custom_status_id }
        end
      end

      expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
      actual   = Nokogiri::XML(@ticket.to_xml, &:noblanks)

      assert_document_equality(expected, actual)
    end
  end

  describe "account has multibrand feature" do
    before do
      Account.any_instance.stubs(:has_ticket_forms?).returns(false)
      Account.any_instance.stubs(:has_multibrand?).returns(true)
    end

    it "serializes to XML consistently and include brand_id" do
      document = Nokogiri::XML::Builder.new do |xml|
        xml.ticket do
          xml.nice_id(type: 'integer') { @ticket.id }
          xml.created_at(type: 'datetime') { xml.text(@ticket.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.updated_at(type: 'datetime') { xml.text(@ticket.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.assigned_at(type: 'datetime') { xml.text(@ticket.assigned_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.has_incidents
          xml.initially_assigned_at(type: 'datetime') { xml.text(@ticket.initially_assigned_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.status_updated_at(type: 'datetime') { xml.text(@ticket.status_updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.solved_at(type: 'datetime', nil: 'true')
          xml.due_date(type: 'datetime', nil: 'true')
          xml.group_id(type: 'integer') { @ticket.group_id }
          xml.entry_id(type: 'integer', nil: 'true')
          xml.submitter_id(type: 'integer') { @ticket.submitter_id }
          xml.assignee_id(type: 'integer') { @ticket.assignee_id }
          xml.requester_id(type: 'integer') { @ticket.requester_id }
          xml.number_of_incidents
          xml.organization_id(type: 'integer', nil: 'true')
          xml.via_id(type: 'integer') { @ticket.via_id }
          xml.status_id(type: 'integer') { @ticket.status_id }
          xml.priority_id(type: 'integer') { @ticket.priority_id }
          xml.ticket_type_id(type: 'integer') { @ticket.ticket_type_id }
          xml.updated_by_type_id(type: 'integer', nil: 'true')
          xml.problem_id(type: 'integer', nil: 'true')
          xml.resolution_time(type: 'integer', nil: 'true')
          xml.external_id(type: 'integer', nil: 'true')
          xml.original_recipient_address(type: 'integer', nil: 'true')
          xml.current_tags { xml.text(@ticket.current_tags) }
          xml.current_collaborators
          xml.description { xml.text(@ticket.description) }
          xml.recipient { xml.text(@ticket.recipient) }
          xml.subject { xml.text(@ticket.subject) }
          xml.ticket_field_entries(type: 'array')
          xml.channel
          xml.comments(type: 'array')
          xml.latest_agent_comment_added_at
          xml.latest_public_comment_added_at
          xml.linkings(type: 'array')
          xml.permissions(type: 'array')
          xml.brand_id(type: 'integer') { @ticket.brand_id }
          xml.via_reference_id(type: 'integer') { @ticket.via_reference_id }
          xml.is_public(type: 'boolean') { @ticket.is_public }
          xml.custom_status_id(type: 'integer', nil: 'true') { @ticket.custom_status_id }
        end
      end

      expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
      actual   = Nokogiri::XML(@ticket.to_xml, &:noblanks)

      assert_document_equality(expected, actual)
    end
  end

  describe_with_arturo_enabled 'use_comment_body_in_xml_export' do
    it "serializes ticket comments with body and html-body, without value" do
      comment_xml = Nokogiri::XML(@ticket.to_xml).xpath('//comment[last()]')
      assert comment_xml.xpath("value").text.present?, 'Expected value to be present'
      assert comment_xml.xpath("body").text.present?, 'Expected body to be present'
      assert comment_xml.xpath("html-body").text.present?, 'Expected html-body to be present'
    end
  end

  describe 'permissions' do
    class TicketSerializer
      attr_accessor :current_user
      include Zendesk::Serialization::TicketSerialization
    end

    describe 'Ticket Serializer' do
      before do
        @user = users(:minimum_agent)
        @serializer = TicketSerializer.new
        @serializer.current_user = @user
      end

      describe 'ticket update permissions' do
        before do
          @user.stubs(:can?).returns(true)
        end

        describe 'when ticket can be updated and' do
          before do
            @serializer.stubs(:can_be_updated?).returns(true)
          end

          describe 'current user can edit properties' do
            before do
              @user.stubs(:can?).with(:edit_properties, @serializer).returns(true)
            end

            it 'is true' do
              assert @serializer.permissions[:can_update_ticket]
              assert @serializer.permissions[:can_edit_ticket_properties]
            end
          end

          describe 'current user can not edit properties' do
            before do
              @user.stubs(:can?).with(:edit_properties, @serializer).returns(false)
            end

            it 'is false' do
              refute @serializer.permissions[:can_update_ticket]
              refute @serializer.permissions[:can_edit_ticket_properties]
            end
          end
        end

        describe 'when ticket can not be updated and' do
          before do
            @serializer.stubs(:can_be_updated?).returns(false)
          end

          describe 'current user can edit properties' do
            before do
              @user.stubs(:can?).with(:edit_properties, @serializer).returns(true)
            end

            it 'is false' do
              refute @serializer.permissions[:can_update_ticket]
              refute @serializer.permissions[:can_edit_ticket_properties]
            end
          end

          describe 'current user can not edit properties' do
            before do
              @user.stubs(:can?).with(:edit_properties, @serializer).returns(true)
            end

            it 'is false' do
              refute @serializer.permissions[:can_update_ticket]
              refute @serializer.permissions[:can_edit_ticket_properties]
            end
          end
        end
      end
    end
  end
end
