require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Zendesk::Serialization::VoiceCommentSerialization do
  fixtures :users, :tickets, :organizations

  before do
    @account = accounts(:minimum)
    @agent   = users(:minimum_agent)
    @ticket  = tickets(:minimum_1)

    @recording_sid = "RE5d4706b2a7ce9c6085119352bc3f96c"
    @recording_url = "http://api.twilio.com/2010-04-01/Accounts/AC987414f141979a090af362442ae4b864/Recordings/#{@recording_sid}"
    @call          = mock('Call')
    @call_id       = 7890
    @now           = 'June 04, 2014 05:10:27 AM'

    @call.stubs(:voice_comment_data).returns(
      recording_url: @recording_url,
      recording_duration: "20",
      call_duration: "30",
      transcription_text: "foo: hi\nbar: bye\n",
      transcription_status: "completed",
      answered_by_id: @agent.id,
      call_id: @call_id,
      from: '+14155556666',
      to: '+14155557777',
      started_at: @now,
      outbound: false,
      recorded: true
    )

    @call.stubs(:account_id).returns(@account.id)

    @ticket.add_voice_comment(@call, author_id: @agent.id, body: "Call recording")
    @ticket.will_be_saved_by(@agents)
    @ticket.save!

    @voice_comment = @ticket.reload.voice_comments.last
  end

  describe "API v1 serialization" do
    describe "with body passed in" do
      it "renders value consistently with body" do
        expected = "minimum 1 ticket\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 am\nAnswered by: #{@agent.name}\nLength of phone call: 30 seconds\nListen to the recording: https://minimum.zendesk-test.com/v2/recordings/7890/RE5d4706b2a7ce9c6085119352bc3f96c\n\nTranscription:\n\nfoo: hi\nbar: bye\n\n"
        assert_equal expected, @voice_comment.as_json["value"].sub(" AM", " am")
      end
    end

    describe "without body passed in" do
      before do
        @ticket.add_voice_comment(@call, author_id: @agent.id)
        @ticket.will_be_saved_by(@agents)
        @ticket.save!

        @voice_comment = @ticket.reload.voice_comments.last
      end

      it "renders value consistently with subject" do
        expected = "minimum 1 ticket\nCall Details:\n\nCall from: +1 (415) 555-6666\nCall to: +1 (415) 555-7777\nTime of call: June 04, 2014 05:10:27 am\nAnswered by: #{@agent.name}\nLength of phone call: 30 seconds\nListen to the recording: https://minimum.zendesk-test.com/v2/recordings/7890/RE5d4706b2a7ce9c6085119352bc3f96c\n\nTranscription:\n\nfoo: hi\nbar: bye\n\n"
        assert_equal expected, @voice_comment.as_json["value"].sub(" AM", " am")
      end
    end
  end
end
