require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Serialization::GroupSerialization do
  fixtures :groups

  before do
    @group = groups(:serialization_group)
  end

  it "serializes to XML consistently" do
    document = Nokogiri::XML::Builder.new do |xml|
      xml.group do
        xml.id_(type: 'integer') { @group.id }
        xml.name { xml.text(@group.name) }
        xml.is_active(type: 'boolean') { xml.text(@group.is_active) }
        xml.created_at(type: 'datetime') { xml.text(@group.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
        xml.updated_at(type: 'datetime') { xml.text(@group.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
        xml.description(type: 'string')
      end
    end

    expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
    actual   = Nokogiri::XML(@group.to_xml, &:noblanks)

    assert_document_equality(expected, actual)
  end

  it "serializes to JSON consistently" do
    json = { "name" => @group.name, "created_at" => @group.created_at.as_json, "updated_at" => @group.updated_at.as_json, "id" => @group.id, "is_active" => @group.is_active, 'description' => '' }
    assert_equal json, JSON.parse(@group.to_json)
    assert_equal json, JSON.parse(@group.to_json(version: 1))
    json = { "name" => @group.name, "created_at" => @group.created_at.as_json, "updated_at" => @group.updated_at.as_json, "id" => @group.id }
    assert_equal json, JSON.parse(@group.to_json(version: 2))
  end
end
