require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::Serialization::EntrySerialization do
  describe "Entries" do
    fixtures :entries, :tickets

    before do
      @entry = entries(:serialization_entry)
    end

    it "serializes to XML consistently" do
      document = Nokogiri::XML::Builder.new do |xml|
        xml.entry do
          xml.id_(type: 'integer') { xml.text(@entry.id) }
          xml.forum_id(type: 'integer') { xml.text(@entry.forum_id) }
          xml.submitter_id(type: 'integer') { xml.text(@entry.submitter_id) }
          xml.organization_id(type: 'integer') { xml.text(@entry.organization_id) }
          xml.title { xml.text(@entry.title) }
          xml.body { xml.text(@entry.body) }
          xml.current_tags(nil: 'true')
          xml.hits(type: 'integer') { xml.text(@entry.hits) }
          xml.position(type: 'integer') { xml.text(@entry.position) }
          xml.is_public(type: 'boolean') { xml.text(@entry.is_public) }
          xml.is_highlighted(type: 'boolean') { xml.text(@entry.is_highlighted) }
          xml.is_locked(type: 'boolean') { xml.text(@entry.is_locked) }
          xml.is_pinned(type: 'boolean') { xml.text(@entry.is_pinned) }
          xml.flag_type_id(type: 'integer') { xml.text(@entry.flag_type_id) }
          xml.votes_count(type: 'integer') { xml.text(@entry.votes_count) }
          xml.posts_count(type: 'integer') { xml.text(@entry.posts_count) }
          xml.attachments(type: 'array')
          xml.created_at(type: 'datetime') { xml.text(@entry.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
          xml.updated_at(type: 'datetime') { xml.text(@entry.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
        end
      end

      expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
      actual   = Nokogiri::XML(@entry.to_xml, &:noblanks)

      assert_document_equality(expected, actual)
    end
  end

  %w[json xml].each do |serialization_format|
    describe "Serializing an Entry as #{serialization_format.upcase}" do
      before { @entry = Entry.new }

      %w[sanitize_on_display updater_id].each do |attribute_to_exclude|
        it "does not include its #{attribute_to_exclude} attribute" do
          @entry.expects(attribute_to_exclude).never

          @entry.send("to_#{serialization_format}")
        end
      end

      it "does not include its posts" do
        @entry.expects(:posts).never
        @entry.send("to_#{serialization_format}")
      end

      it "does not include the sanitize_on_display attribute for any of its posts" do
        @entry.posts.build
        @entry.posts.first.expects(:sanitize_on_display).never

        @entry.send("to_#{serialization_format}")
      end
    end
  end

  describe "Serializing a mixed collection with an Entry" do
    it "does not override other serialization options" do
      mixed_collection = [Entry.new, Ticket.new { |t| t.nice_id = 5; t.account = accounts(:minimum) }]

      # The passed in options hash is shared across all objects
      decoded = ActiveSupport::JSON.decode(mixed_collection.to_json({}))
      assert decoded.last['nice_id']
    end
  end
end
