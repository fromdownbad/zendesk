require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Serialization::PostSerialization do
  fixtures :posts

  before do
    @post = posts(:serialization_post)
  end

  it "serializes to XML consistently" do
    document = Nokogiri::XML::Builder.new do |xml|
      xml.post do
        xml.id_(type: 'integer') { xml.text(@post.id) }
        xml.account_id(type: 'integer') { xml.text(@post.account_id) }
        xml.forum_id(type: 'integer') { xml.text(@post.forum_id) }
        xml.entry_id(type: 'integer') { xml.text(@post.entry_id) }
        xml.user_id(type: 'integer') { xml.text(@post.user_id) }
        xml.is_informative(type: 'boolean') { xml.text(@post.is_informative) }
        xml.attachments(type: 'array')
        xml.body { xml.text(@post.body) }
        xml.created_at(type: 'datetime') { xml.text(@post.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
        xml.updated_at(type: 'datetime') { xml.text(@post.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")) }
      end
    end

    expected = Nokogiri::XML(document.to_xml.tr('_', '-'), &:noblanks)
    actual   = Nokogiri::XML(@post.to_xml, &:noblanks)

    assert_document_equality(expected, actual)
  end

  it "serializes to JSON consistently" do
    json = ActiveSupport::JSON.decode(@post.to_json)
    keys = ["created_at", "entry_id", "body", "updated_at", "account_id",
            "forum_id", "id", "user_id", "is_informative", "attachments"]

    assert_equal keys.sort, json.keys.sort
  end

  describe "collections" do
    before do
      posts = [@post]
      posts.stubs(:total_entries).returns(posts.size)

      @collection = Post.serializable_collection(posts)
    end

    it "serializes xml" do
      xml = Nori.new(parser: :nokogiri).parse(@collection.to_xml)

      assert_nil   xml['count']
      assert_equal Array, xml['posts'].class

      post = xml['posts'].first
      assert_equal @post.id,       post['id']
      assert_equal @post.body,     post['body']
    end

    it "serializes json" do
      json = ActiveSupport::JSON.decode(@collection.to_json)
      assert_equal 1,     json['count']
      assert_equal Array, json['posts'].class

      post = json['posts'].first
      assert_equal @post.id,       post['id']
      assert_equal @post.body,     post['body']
    end
  end
end
