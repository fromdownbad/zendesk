require_relative '../../support/test_helper'

SingleCov.covered!

describe Zendesk::UnicornLoadRateLimiting do
  describe '.unicorn_load_exceeds?' do
    before do
      Zendesk::UnicornLoadRateLimiting.stubs(:active_unicorns).returns(50.0)
      Zendesk::UnicornLoadRateLimiting.stubs(:total_unicorns).returns(100.0)
    end

    it 'returns true if the percent is over' do
      assert(Zendesk::UnicornLoadRateLimiting.unicorn_load_exceeds?(0.25))
    end

    it 'returns false if the percent is under' do
      assert_equal false, Zendesk::UnicornLoadRateLimiting.unicorn_load_exceeds?(0.75)
    end

    it 'returns false if the percent is equal' do
      assert_equal false, Zendesk::UnicornLoadRateLimiting.unicorn_load_exceeds?(0.50)
    end

    it 'logs and returns true if the calculation raises' do
      Zendesk::UnicornLoadRateLimiting.expects(:active_unicorns).raises('Whoopsies')
      ZendeskExceptions::Logger.expects(:record)

      assert(Zendesk::UnicornLoadRateLimiting.unicorn_load_exceeds?(0.25))
    end
  end

  describe '#active_unicorns' do
    before do
      Raindrops::Linux.stubs(:tcp_listener_stats).returns(results)
      ENV.stubs(:[]).with('UNICORN_TCP_PORT').returns('4080')
    end

    describe 'when stats are available' do
      let(:results) do
        {
          '0.0.0.0:1440' => stub(active: 4, queued: 1),
          '127.0.0.1:4080' => stub(active: 2, queued: 1)
        }
      end

      it 'returns the correct active count' do
        assert_equal 2, Zendesk::UnicornLoadRateLimiting.active_unicorns
      end
    end

    describe 'when stats are unavailable' do
      let(:results) { {'0.0.0.0:1440' => stub(active: 4, queued: 1)} }

      it 'raises an error' do
        assert_raises { Zendesk::UnicornLoadRateLimiting.active_unicorns }
      end
    end
  end

  describe '#total_unicorns' do
    it 'prefers to use UNICORN_WORKERS if set' do
      ENV.stubs(:[]).with('UNICORN_WORKERS').returns('20')
      assert_equal 20.0, Zendesk::UnicornLoadRateLimiting.total_unicorns
    end

    it 'raises of UNICORN_WORKERS is not set' do
      ENV.stubs(:[]).with('UNICORN_WORKERS').returns(nil)
      assert_raises { Zendesk::UnicornLoadRateLimiting.total_unicorns }
    end
  end
end
