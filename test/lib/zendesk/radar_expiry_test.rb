require_relative "../../support/test_helper"
require 'zendesk/radar_factory'

SingleCov.covered! uncovered: 5

describe 'Zendesk::RadarExpiry' do
  fixtures :accounts, :users

  before do
    @account = accounts(:minimum)
    @sut = Zendesk::RadarExpiry.new(@account)
    Timecop.freeze
  end

  describe '#expire_radar_cache_key' do
    it 'expires the key via Radar' do
      @sut.expects(:skip_expire_radar_cache_key?).returns(false)
      ::Radar::Client.any_instance.expects(:status).at_least_once.returns(stub(set: true, get: nil))
      @sut.expire_radar_cache_key(:macros)
    end

    it 'does not expire if skipping' do
      @sut.expects(:skip_expire_radar_cache_key?).returns(true)
      ::RadarFactory.expects(:create_radar_notification).never
      @sut.expire_radar_cache_key(:macros)
    end

    describe 'rate limiting' do
      before do
        @sut.stubs(:skip_expire_radar_cache_key?).returns(false)
      end

      it 'throttles updates based on an account scope' do
        Prop.expects(:throttle).with(:radar_expiry, "#{@account.id}/macros")
        @sut.expire_radar_cache_key(:macros)
      end

      it 'only allows 5 expires when called repeatedly' do
        ::Radar::Client.any_instance.expects(:status).times(5).returns(stub(set: true, get: nil))
        10.times do
          @sut.expire_radar_cache_key(:macros)
        end
      end
    end
  end

  describe '#skip_expire_radar_cache_key?' do
    it 'is true for a new account' do
      @account.expects(:new_record?).returns(true)
      assert @sut.send(:skip_expire_radar_cache_key?)
    end

    it 'is true if the account does not have a route yet' do
      @account.expects(:route).returns(nil)
      assert @sut.send(:skip_expire_radar_cache_key?)
    end

    it 'is true if pre_account_creation.is_binding? is true' do
      @account.expects(:pre_account_creation).returns(stub(is_binding?: true))
      assert @sut.send(:skip_expire_radar_cache_key?)
    end

    it 'is false otherwise' do
      refute @sut.send(:skip_expire_radar_cache_key?)
    end
  end

  describe "killswitch" do
    describe_with_arturo_enabled :disable_lotus_expiry do
      it "will not send anything to Radar" do
        ::Radar::Client.any_instance.expects(:status).never
        @sut.expire_radar_cache_key(:macros)
      end
    end
  end
end
