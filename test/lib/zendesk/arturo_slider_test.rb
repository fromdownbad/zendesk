require_relative "../../support/test_helper"
require_relative "../../support/arturo_slider_helper"

SingleCov.covered!

describe Zendesk::ArturoSlider do
  include ArturoSliderHelper

  before do
    rollout_arturo(
      :slider_test,
      deployment_percentage: 30
    )
  end

  describe '#arturo' do
    describe 'when arturo is present' do
      let(:slider) { Zendesk::ArturoSlider.new(:slider_test) }

      it 'returns the arturo record' do
        assert_kind_of(Arturo::Feature, slider.arturo)
      end
    end

    describe 'when arturo is not present' do
      let(:slider) { Zendesk::ArturoSlider.new(:not_found) }

      it 'returns nil' do
        assert_nil slider.arturo
      end
    end
  end

  describe '#enabled?' do
    let(:slider) { Zendesk::ArturoSlider.new(:not_found) }

    describe 'when arturo is not found' do
      it 'is not enabled' do
        refute slider.enabled?
      end
    end

    describe 'when arturo is found' do
      let(:slider) { Zendesk::ArturoSlider.new(:slider_test) }

      describe 'when arturo is off' do
        before { disable_arturo(:slider_test) }

        it 'is not enabled' do
          refute slider.enabled?
        end
      end

      describe 'when arturo is closed beta' do
        before { slider.arturo.stubs(:rollout?).returns(false) }

        it 'is not enabled' do
          refute slider.enabled?
        end
      end

      describe 'when arturo is rollout' do
        describe 'when there is a value set' do
          before { slider.stubs(:value).returns(1) }

          it 'is enabled' do
            assert slider.enabled?
          end
        end

        describe 'when disable_if returns true' do
          before do
            rollout_arturo(
              :slider_test,
              deployment_percentage: 30,
              external_beta_subdomains: 'whale'
            )
          end
          let(:slider) do
            Zendesk::ArturoSlider.new(
              :slider_test,
              disable_if: ->(slider) {
                slider.arturo.external_beta_subdomains.include?('whale')
              }
            )
          end

          it 'is disabled' do
            refute slider.enabled?
          end
        end

        describe 'when disable_if returns false' do
          before do
            rollout_arturo(
              :slider_test,
              deployment_percentage: 30,
              external_beta_subdomains: 'whale'
            )
          end
          let(:slider) do
            Zendesk::ArturoSlider.new(
              :slider_test,
              disable_if: ->(slider) {
                slider.arturo.external_beta_subdomains.include?('another')
              }
            )
          end

          it 'is enabled' do
            assert slider.enabled?
          end
        end
      end
    end
  end

  describe '#was_drawn?' do
    let(:slider) { Zendesk::ArturoSlider.new(:slider_test) }

    describe 'with an enabled slider' do
      let(:iterations) { 1000 }
      let(:arturo_percentage) { 80 }
      before do
        rollout_arturo(
          :slider_test,
          deployment_percentage: arturo_percentage
        )
      end

      it 'is drawn based on percentage' do
        results = (1..iterations).map do
          slider.was_drawn?
        end
        win_count = results.count { |r| r == true }
        win_percentage = (win_count / iterations.to_f) * 100
        # giving 5% margin of error
        assert_in_delta arturo_percentage, win_percentage, 4
      end
    end

    describe 'with a disabled slider' do
      before { slider.stubs(:enabled?).returns(false) }

      it 'does not get drawn' do
        refute slider.was_drawn?
      end
    end
  end

  describe 'Valid Range' do
    before do
      rollout_arturo(
        :slider_test,
        deployment_percentage: user_set_percentage
      )
    end

    describe 'with percentage below minimum limit' do
      let(:user_set_percentage) { 10 }
      let(:slider) { Zendesk::ArturoSlider.new(:slider_test, valid_range: Range.new(20, 100)) }

      it 'returns minimum limit' do
        assert_equal slider.value, 20
      end
    end

    describe 'with percentage over minimum limit' do
      let(:user_set_percentage) { 30 }
      let(:slider) { Zendesk::ArturoSlider.new(:slider_test, valid_range: Range.new(20, 100)) }

      it 'returns user percentage' do
        assert_equal slider.value, 30
      end
    end

    describe 'with percentage over maximum limit' do
      let(:user_set_percentage) { 90 }
      let(:slider) { Zendesk::ArturoSlider.new(:slider_test, valid_range: Range.new(20, 80)) }

      it 'returns maximum limit' do
        assert_equal slider.value, 80
      end
    end

    describe 'with percentage below maximum limit' do
      let(:user_set_percentage) { 70 }
      let(:slider) { Zendesk::ArturoSlider.new(:slider_test, valid_range: Range.new(20, 80)) }

      it 'returns user percentage' do
        assert_equal slider.value, 70
      end
    end
  end

  describe '#value' do
    let(:percentage_for_pod) { 40 }
    let(:valid_range) { Range.new(20, 100) }
    before do
      rollout_arturo(
        :slider_test,
        deployment_percentage: user_set_percentage,
        external_beta_subdomains: "+pod1:#{percentage_for_pod}%"
      )
    end
    let(:slider) { Zendesk::ArturoSlider.new(:slider_test, valid_range: valid_range) }

    describe 'when there is an overall percentage set' do
      let(:user_set_percentage) { 30 }

      describe 'when in a pod that has specific percentage' do
        with_env('ZENDESK_POD_ID' => '1') do
          it 'gives the value for specific pod' do
            assert_equal slider.value, percentage_for_pod
          end
        end
      end

      describe 'within a pod that does not have a percentage set' do
        with_env('ZENDESK_POD_ID' => '2') do
          it 'uses the the overall deployment percentage' do
            assert_equal slider.value, user_set_percentage
          end
        end
      end
    end

    describe 'when there the overall rollout is set to zero' do
      let(:user_set_percentage) { 0 }

      describe 'with a valid range over zero' do
        let(:valid_range) { Range.new(20, 100) }

        describe 'for the specific pod' do
          with_env('ZENDESK_POD_ID' => '1') do
            it 'gives the value for specific pod' do
              assert_equal slider.value, percentage_for_pod
            end
            it 'is enabled' do
              assert slider.enabled?
            end
          end
        end

        describe 'for other pods' do
          with_env('ZENDESK_POD_ID' => '2') do
            it 'uses the minimum valid value' do
              assert_equal slider.value, 20
            end

            it 'is enabled' do
              assert slider.enabled?
            end
          end
        end
      end

      describe 'with a valid range starting as zero' do
        let(:valid_range) { Range.new(0, 100) }

        describe 'for the specific pod' do
          with_env('ZENDESK_POD_ID' => '1') do
            it 'gives the value for specific pod' do
              assert_equal slider.value, percentage_for_pod
            end
            it 'is enabled' do
              assert slider.enabled?
            end
          end
        end

        describe 'for other pods' do
          with_env('ZENDESK_POD_ID' => '2') do
            it 'uses the minimum valid value' do
              assert_equal slider.value, 0
            end

            it 'is disabled' do
              refute slider.enabled?
            end
          end
        end
      end
    end

    describe 'when the pod-specific percentage is out of the valid range' do
      let(:percentage_for_pod) { 10 }
      let(:user_set_percentage) { 0 }

      with_env('ZENDESK_POD_ID' => '1') do
        it 'uses the valid range' do
          assert_equal slider.value, 20
        end
      end
    end
  end
end
