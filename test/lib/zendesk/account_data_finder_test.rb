require_relative "../../support/test_helper"
require_relative "../../support/suite_test_helper"
require_relative "../../support/multiproduct_test_helper"

SingleCov.covered!

describe Zendesk::AccountDataFinder do
  let(:account) { accounts(:trial) }
  let(:subject) { Zendesk::AccountDataFinder }

  describe '#find_data' do
    it 'finds account tables' do
      refute subject.find_data(account)[:account_tables].empty?
    end

    it 'finds shard tables' do
      refute subject.find_data(account)[:shard_tables].empty?
    end
  end
end
