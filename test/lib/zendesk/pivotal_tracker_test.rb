require_relative '../../support/test_helper'

SingleCov.covered!

describe Zendesk::PivotalTracker do
  let!(:project_json) { read_test_file('pivotal_project.json') }
  let!(:project_memberships_json) { read_test_file('pivotal_project_memberships.json') }
  let!(:project_labels_json) { read_test_file('pivotal_project_labels.json') }
  let!(:project_integrations_json) { read_test_file('pivotal_project_integrations.json') }
  let!(:valid_story_json) { read_test_file('pivotal_valid_story.json') }
  let!(:invalid_story_json) { read_test_file('pivotal_invalid_story.json') }

  before do
    @token = "some_token"
    @project_id = "some_project_id"
    @subject = Zendesk::PivotalTracker.new(@token)
  end

  describe "BASE_URL" do
    it "is defined" do
      assert Zendesk::PivotalTracker::BASE_URL, "https://www.pivotaltracker.com"
    end
  end

  describe "API_VERSION" do
    it "is defined" do
      assert Zendesk::PivotalTracker::API_VERSION, "/services/v5"
    end
  end

  describe "#project" do
    before do
      stub_request(
        :get,
        Zendesk::PivotalTracker::BASE_URL + "/services/v5/projects/#{@project_id}"
      ).to_return(status: 200, body: project_json)
    end

    it "returns the project" do
      assert @subject.project(@project_id)["id"], JSON.parse(project_json)["id"]
    end
  end

  describe "#project_memberships" do
    before do
      stub_request(
        :get,
        Zendesk::PivotalTracker::BASE_URL + "/services/v5/projects/#{@project_id}/memberships"
      ).to_return(status: 200, body: project_memberships_json)
    end

    it "returns the project's memberships" do
      members = @subject.project_memberships(@project_id)
      expected = JSON.parse(project_memberships_json)
      assert members, expected
    end
  end

  describe "#project_labels" do
    before do
      stub_request(
        :get,
        Zendesk::PivotalTracker::BASE_URL + "/services/v5/projects/#{@project_id}/labels"
      ).to_return(status: 200, body: project_labels_json)
    end

    it "returns the project's labels" do
      members = @subject.project_labels(@project_id)
      expected = JSON.parse(project_labels_json)
      assert members, expected
    end
  end

  describe "#project_labels" do
    before do
      stub_request(
        :get,
        Zendesk::PivotalTracker::BASE_URL + "/services/v5/projects/#{@project_id}/labels"
      ).to_return(status: 200, body: project_labels_json)
    end

    it "returns the project's labels" do
      members = @subject.project_labels(@project_id)
      expected = JSON.parse(project_labels_json)
      assert members, expected
    end
  end

  describe "#project_integrations" do
    before do
      stub_request(
        :get,
        Zendesk::PivotalTracker::BASE_URL + "/services/v5/projects/#{@project_id}/integrations"
      ).to_return(status: 200, body: project_integrations_json)
    end

    it "returns the project's integrations" do
      members = @subject.project_integrations(@project_id)
      expected = JSON.parse(project_integrations_json)
      assert members, expected
    end
  end

  describe "#create_story" do
    let(:headers) do
      {'Content-Type' => 'application/json', 'X-TrackerToken' => @token}
    end

    describe "with valid params" do
      let(:valid_params) do
        {
          "story_type" => "feature",
          "name" => "Example ticket name",
          "description" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id tempor libero. Ut tristique felis neque, in molestie nisl efficitur et. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis et tristique lectus. Quisque et augue eu tellus elementum tincidunt a vitae dui. Etiam at blandit felis. Ut non tortor ut mi finibus dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta nulla et tortor consectetur, vel aliquet velit fermentum. Fusce sed arcu vulputate, posuere mauris sit amet, cursus enim.",
          "labels" => ["new", {"id" => 20933133, "name" => "major label"}],
          "requested_by_id" => 3141060,
          "owner_ids" => [3141060],
          "integration_id" => 48924,
          "external_id" => 1
        }
      end

      before do
        stub_request(
          :post,
          Zendesk::PivotalTracker::BASE_URL + "/services/v5/projects/#{@project_id}/stories"
        ).with(body: valid_params, headers: headers).to_return(status: 200, body: valid_story_json)
      end

      it "returns the new ticket" do
        new_ticket = @subject.create_story(@project_id, valid_params)

        assert new_ticket['kind'], 'story'
        assert new_ticket['name'], 'Example ticket name'
      end
    end

    describe "with invalid params" do
      let(:invalid_params) { {} }

      before do
        stub_request(
          :post,
          Zendesk::PivotalTracker::BASE_URL + "/services/v5/projects/#{@project_id}/stories"
        ).with(body: invalid_params, headers: headers).to_return(status: 200, body: invalid_story_json)
      end

      it 'returns an json with kind = error' do
        failed_ticket = @subject.create_story(@project_id, invalid_params)
        assert failed_ticket['kind'], 'error'
      end
    end
  end
end
