require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::DatabaseBackoff do
  let(:subject) { Zendesk::DatabaseBackoff.new(minimum: 0.3) }
  let(:aurora_sensor) { stub('AuroraSensor', measure: 0) }
  let(:aurora_trx_sensor) { stub('AuroraTrxSensor', measure: 0) }
  let(:aurora_delete_trx_sensor) { stub('AuroraDeleteTrxSensor', measure: 0) }
  let(:slave_lag_sensor) { stub('SlaveDelaySensor', measure: 0) }
  let(:maxwell_lag_sensor) { stub('MaxwellFiltersLagSensor', measure: 0) }

  before do
    ZendeskDatabaseSupport::Sensors::AuroraTrxSensor.stubs(:new).returns(aurora_trx_sensor)
    ZendeskDatabaseSupport::Sensors::AuroraCpuSensor.stubs(:new).returns(aurora_sensor)
    ZendeskDatabaseSupport::Sensors::AuroraDeleteTrxSensor.stubs(:new).returns(aurora_delete_trx_sensor)
    ZendeskDatabaseSupport::Sensors::SlaveDelaySensor.stubs(:new).returns(slave_lag_sensor)
    ZendeskDatabaseSupport::Sensors::MaxwellFiltersLagSensor.stubs(:new).returns(maxwell_lag_sensor)
    # PidController uses time in its measurements. Simulate 1 second ticks between calls
    Process.stubs(:clock_gettime).returns(*100.times.to_a)
  end

  describe '#backoff_duration' do
    describe 'with healthy slave lag and cpu usage' do
      it 'returns the minimum value' do
        Array.new(4) { subject.backoff_duration }.must_equal([0.3, 0.3, 0.3, 0.3])
      end
    end

    describe 'without Aurora output' do
      it 'ignores the sensor output' do
        aurora_sensor.stubs(:measure).returns(nil)
        Array.new(4) { subject.backoff_duration }.must_equal([0.3, 0.3, 0.3, 0.3])
      end
    end

    describe 'when Aurora CPU is high' do
      it 'controls against the CPU load' do
        aurora_sensor.stubs(:measure).returns(100.0, 75.0, 60.0, 50.0)
        # Output need updating if you fidget with the PID tuning parameters
        Array.new(4) { subject.backoff_duration }.must_equal([7.5, 9.313, 10.563, 10.875])
      end

      it 'does not suffer integral windup' do
        aurora_sensor.stubs(:measure).returns(0.0, 0.0, 60.0, 100.0)
        Array.new(4) { subject.backoff_duration }.must_equal([0.3, 0.3, 6.25, 20.0])
      end

      describe 'with database_backoff_constant_duration' do
        before { Arturo.enable_feature!(:database_backoff_constant_duration) }
        it 'controls against the CPU load' do
          aurora_sensor.stubs(:measure).returns(100.0, 75.0, 60.0, 50.0)
          # Output need updating if you fidget with the PID tuning parameters
          Array.new(4) { subject.backoff_duration }.must_equal([12.0, 10.875, 10.75, 10.375])
        end
      end
    end

    describe 'when Aurora in flight transaction count is high' do
      before do
        Arturo.enable_feature!(:aurora_database_backoff_trx_sensor)
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 50, symbol: :aurora_database_backoff_trx_sensor_p)
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 30, symbol: :aurora_database_backoff_trx_sensor_i)
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 20, symbol: :aurora_database_backoff_trx_sensor_d)
      end

      it 'controls against the transaction count ' do
        aurora_trx_sensor.stubs(:measure).returns(100.0, 75.0, 60.0, 50.0)
        # Output need updating if you fidget with the PID tuning parameters
        Array.new(4) { subject.backoff_duration }.must_equal([37.5, 68.333, 93.0, 110.833])
      end

      it 'does not suffer integral windup' do
        aurora_trx_sensor.stubs(:measure).returns(0.0, 0.0, 60.0, 100.0)
        Array.new(4) { subject.backoff_duration }.must_equal([0.3, 0.3, 53.0, 139.167])
      end

      describe 'with database_backoff_constant_duration' do
        before { Arturo.enable_feature!(:database_backoff_constant_duration) }
        it 'controls against the in flight transaction count' do
          aurora_trx_sensor.stubs(:measure).returns(100.0, 75.0, 60.0, 50.0)
          # Output need updating if you fidget with the PID tuning parameters
          Array.new(4) { subject.backoff_duration }.must_equal([60.0, 57.5, 62.5, 66.0])
        end
      end
    end

    describe 'when delete transactions lag' do
      before do
        Arturo.enable_feature!(:aurora_database_backoff_delete_trx_sensor)
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 50, symbol: :aurora_database_backoff_delete_trx_sensor_p)
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 30, symbol: :aurora_database_backoff_delete_trx_sensor_i)
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 20, symbol: :aurora_database_backoff_delete_trx_sensor_d)
      end

      it 'controls against the transaction count ' do
        aurora_delete_trx_sensor.stubs(:measure).returns(3, 5, 10, 15)
        Array.new(4) { subject.backoff_duration }.must_equal([0.3, 2.933, 11.933, 25.233])
      end

      describe 'with database_backoff_constant_duration' do
        before { Arturo.enable_feature!(:database_backoff_constant_duration) }

        it 'controls against the in flight transaction count' do
          aurora_delete_trx_sensor.stubs(:measure).returns(7, 5, 3, 1)
          Array.new(4) { subject.backoff_duration }.must_equal([3.2, 2.4, 1.4, 0.3])
        end
      end

      describe 'when transaction lag within expected' do
        it 'should not backoff significantly' do
          aurora_delete_trx_sensor.stubs(:measure).returns(1, 1, 1, 1)
          Array.new(4) { subject.backoff_duration }.must_equal([0.3, 0.3, 0.3, 0.3])
        end
      end
    end

    describe 'when slave lag is high' do
      it 'controls against the slave lag' do
        slave_lag_sensor.stubs(:measure).returns(30.0, 15.0, 2.0, 1.0)
        Array.new(4) { subject.backoff_duration }.must_equal([14.0, 8.35, 1.95, 1.85])
      end
    end

    describe 'when Maxwell Filters is laggy' do
      before do
        Arturo.enable_feature!(:database_backoff_maxwell_filters_lag)
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 50, symbol: :database_backoff_maxwell_filters_lag_p)
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 30, symbol: :database_backoff_maxwell_filters_lag_i)
        Arturo::Feature.create!(phase: "rollout", deployment_percentage: 20, symbol: :database_backoff_maxwell_filters_lag_d)
      end

      it 'controls against the slave lag' do
        maxwell_lag_sensor.stubs(:measure).returns(60.0, 40.0, 20.0, 1.0)
        Array.new(4) { subject.backoff_duration }.must_equal([20.0, 26.667, 16.667, 0.3])
      end
    end
  end

  describe '#to_hash' do
    it 'provides a hash for logging' do
      subject.to_hash.must_be_kind_of Hash
    end
  end
end
