require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 24

describe Zendesk::Net::MultiSSL do
  self::ALL_ERRNO_ERRORS = Errno.constants.map { |constant| Errno.const_get(constant).new } # The names of the Errno:: classes depend on the environment in which Ruby runs.

  let(:url) { "https://xxx.com" }

  def call
    Zendesk::Net::MultiSSL.get_json(url)
  end

  def request_expectation(options = {})
    ssl_version = options.delete(:ssl_version) || 3
    method      = options.delete(:method) || :get
    times       = options.delete(:times) || 1
    Zendesk::Net::MultiSSL.expects(:http_request_with).with(ssl_version, method, url, options).times(times)
  end

  def assert_successful_request(options = {})
    request_expectation(options).returns(stub(body: '{"foo":"bar"}'))
    assert_equal({ "foo" => "bar" }, call)
  end

  describe "SystemCallErrors" do
    it "is a subclass of SystemCallError and start with Errno::" do
      self.class::ALL_ERRNO_ERRORS.each do |e|
        refute Zendesk::Net::MultiSSL.send(:non_errno_system_call_error?, e)
      end
    end
  end

  describe ".get_json" do
    it "returns parsed json" do
      stub_request(:get, url).to_return(body: { foo: "bar" }.to_json)
      assert_equal({ "foo" => "bar" }, call)
    end

    it "returns parsed json array" do
      stub_request(:get, url).to_return(body: [111].to_json)
      assert_equal([111], call)
    end

    describe "with query string" do
      let(:url) { "https://xxx.com?xxx=1" }

      it "returns parsed json for query-only" do
        stub_request(:get, "https://xxx.com/?xxx=1").to_return(body: { foo: "bar" }.to_json)
        assert_equal({ "foo" => "bar" }, call)
      end
    end

    describe "with path" do
      let(:url) { "https://xxx.com/xxx" }

      it "returns parsed json for path only" do
        stub_request(:get, url).to_return(body: { foo: "bar" }.to_json)
        assert_equal({ "foo" => "bar" }, call)
      end
    end

    it "returns nil for invalid html responses (error codes and such)" do
      ZendeskExceptions::Logger.expects(:record).never
      stub_request(:get, url).to_return(body: "<!DOCTYPE html>")
      assert_nil call
    end

    it "reports invalid json" do
      ZendeskExceptions::Logger.expects(:record)
      stub_request(:get, url).to_return(body: "{1232323}")
      assert_nil call
    end

    describe "with failing ssl" do
      it "fallbacks to ssl v1" do
        ZendeskExceptions::Logger.expects(:record).never
        request_expectation(ssl_version: 3).raises(OpenSSL::SSL::SSLError)
        assert_successful_request(ssl_version: 1)
      end

      describe "when ssl v3 failed" do
        before do
          ZendeskExceptions::Logger.expects(:record).never
          request_expectation(ssl_version: 3).raises(OpenSSL::SSL::SSLError)
          assert_successful_request(ssl_version: 1)
        end

        it "only try to request via ssl v1 on next try" do
          request_expectation(ssl_version: 3).never
          assert_successful_request(ssl_version: 1)
        end

        it "requests via ssl v3 if v1 failed" do
          # first try v1 + fallback to v3
          ZendeskExceptions::Logger.expects(:record).once
          request_expectation(ssl_version: 1).raises(OpenSSL::SSL::SSLError)
          assert_successful_request(ssl_version: 3)

          # second try only v3
          request_expectation(ssl_version: 1).never
          assert_successful_request(ssl_version: 3)
        end

        (Zendesk::Net::MultiSSL::RETRY_EXCEPTIONS - [SystemCallError] + self::ALL_ERRNO_ERRORS).sample do |e|
          describe "and second request gets a(n) #{e} exception" do
            it "retrys on second request" do
              # first try v1 + fallback to v3
              ZendeskExceptions::Logger.expects(:record).once
              request_expectation(ssl_version: 1).raises(OpenSSL::SSL::SSLError)
              assert_successful_request(ssl_version: 3)

              # second try retry
              request_expectation(times: Zendesk::Net::MultiSSL::MAX_RETRIES).raises(e)
              ZendeskExceptions::Logger.expects(:record).once
              Rails.logger.expects(:warn).times(Zendesk::Net::MultiSSL::MAX_RETRIES)
            end
          end
        end
      end
    end
  end

  describe ".get" do
    it "uses http_request with the correct method and return the raw Faraday response" do
      stub_request(:get, url).to_return(body: { foo: "bar" }.to_json)
      assert_equal({ "foo" => "bar" }.to_json, Zendesk::Net::MultiSSL.get(url).body)
    end

    (Zendesk::Net::MultiSSL::RETRY_EXCEPTIONS - [SystemCallError] + self::ALL_ERRNO_ERRORS).sample do |e|
      it "retrys the request with an #{e} exception" do
        request_expectation(times: Zendesk::Net::MultiSSL::MAX_RETRIES).raises(e)

        ZendeskExceptions::Logger.expects(:record).once
        Rails.logger.expects(:warn).times(Zendesk::Net::MultiSSL::MAX_RETRIES)
        Zendesk::Net::MultiSSL.get(url)

        ZendeskExceptions::Logger.expects(:record).twice
        Rails.logger.expects(:warn).times(Zendesk::Net::MultiSSL::MAX_RETRIES * 2)
        2.times { Zendesk::Net::MultiSSL.get(url) }
      end
    end
  end

  describe ".post" do
    let(:response) do
      Zendesk::Net::MultiSSL.post(
        url, body: "BODY", headers: { "Accept" => "json" }
      )
    end

    it "uses http_request with the correct method and pass the given options to the request" do
      stub_request(:post, url).
        with(body: "BODY", headers: { "Accept" => "json" }).
        to_return(body: "OK")

      assert_equal("OK", response.body)
    end

    describe "getting redirected" do
      before do
        stub_request(:post, "http://example.com").
          with(body: "BODY", headers: { "Accept" => "json" }).
          to_return(body: "OK")
      end

      it "redirects with post" do
        stub_request(:post, url).
          with(body: "BODY", headers: { "Accept" => "json" }).
          to_return(status: 302,
                    headers: {location: "http://example.com"},
                    body: "REDIRECT")

        assert_equal("OK", response.body)
      end

      it "redirects with post when status is not 302" do
        stub_request(:post, url).
          with(body: "BODY", headers: { "Accept" => "json" }).
          to_return(status: 301,
                    headers: {location: "http://example.com"},
                    body: "PERMANENT REDIRECT")

        assert_equal("OK", response.body)
      end
    end
  end

  describe "with an attacker redirect" do
    before do
      Resolv::DNS.any_instance.stubs(:getaddress).with('foo.example.com').returns(Resolv::IPv4.create('1.2.3.4'))
      Resolv::DNS.any_instance.stubs(:getaddress).with('bad.example.com').returns(Resolv::IPv4.create('0.0.0.0'))
      Rails.env.stubs(:production?).returns(true) # We only resolve IPs for domains when in production

      stub_request(:get, "https://foo.example.com").
        to_return(status: 302, body: "", headers: {'Location' => 'https://bad.example.com' })
    end

    it "doesn't follow the redirect" do
      ZendeskExceptions::Logger.expects(:record).never
      assert_raises Faraday::RestrictIPAddresses::AddressNotAllowed do
        Zendesk::Net::MultiSSL.get("https://foo.example.com", raise_errors: true)
      end
    end
  end
end
