require_relative '../../support/test_helper'

SingleCov.covered!

describe Zendesk::OffsetPaginationLimiter do
  fixtures :accounts

  let(:controller_name) { "users" }
  let(:current_account) { accounts(:minimum) }
  let(:page) { 1 }
  let(:per_page) { 100 }
  let(:log_only) { true }
  let(:limiter) do
    Zendesk::OffsetPaginationLimiter.new(
      entity: controller_name,
      account_id: current_account.id,
      account_subdomain: current_account.subdomain,
      page: page,
      per_page: per_page,
      log_only: log_only
    )
  end
  let(:statsd_client) { limiter.send(:statsd_client) }

  describe "#throttle_index_with_deep_offset_pagination" do
    describe "when log_only is true" do
      describe "when throttle returns true" do
        before do
          Prop.stubs(:throttle).returns(true)
          Prop.stubs(:throttle!).returns(true)
        end

        describe "for page=1 and per_page=100" do
          let(:page) { 1 }

          it "does not log" do
            Rails.logger.expects(:info).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not increment any metrics" do
            statsd_client.expects(:increment).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not call Prop#throttle!" do
            Prop.expects(:throttle!).never
            limiter.throttle_index_with_deep_offset_pagination
          end
        end

        describe "for page=10,001 and per_page=100" do
          let(:page) { 10_001 }

          it "logs the controller and pagination info" do
            Rails.logger.expects(:info).with(
              "[UsersController] Log only: would have rate limited #index by account: minimum, page: 10001, per_page: 100, throttle key: index_users_with_deep_offset_pagination_ceiling_limit"
            ).once
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "increments the appropriate metric" do
            statsd_client.expects(:increment).with("api_v2_users_index_ceiling_rate_limit_log_only", tags: ["account_id:#{current_account.id}"]).once
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not call Prop#throttle!" do
            Prop.expects(:throttle!).never
            limiter.throttle_index_with_deep_offset_pagination
          end
        end
      end

      describe "when throttle returns false" do
        before do
          Prop.stubs(:throttle).returns(false)
          Prop.stubs(:throttle!).returns(false)
        end

        describe "for page=1 and per_page=100" do
          let(:page) { 1 }

          it "does not log" do
            Rails.logger.expects(:info).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not increment any metrics" do
            statsd_client.expects(:increment).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not call Prop#throttle!" do
            Prop.expects(:throttle!).never
            limiter.throttle_index_with_deep_offset_pagination
          end
        end

        describe "for page=10,001 and per_page=100" do
          let(:page) { 10_001 }

          it "does not log" do
            Rails.logger.expects(:info).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not increment any metrics" do
            statsd_client.expects(:increment).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not call Prop#throttle!" do
            Prop.expects(:throttle!).never
            limiter.throttle_index_with_deep_offset_pagination
          end
        end
      end
    end

    describe "when log_only is false" do
      let(:log_only) { false }

      describe "when throttle returns true" do
        before do
          Prop.stubs(:throttle).returns(true)
          Prop.stubs(:throttle!).returns(true)
        end

        describe "for page=1 and per_page=100" do
          let(:page) { 1 }

          it "does not log" do
            Rails.logger.expects(:info).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not increment any metrics" do
            statsd_client.expects(:increment).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not call Prop#throttle!" do
            Prop.expects(:throttle!).never
            limiter.throttle_index_with_deep_offset_pagination
          end
        end

        describe "for page=10,001 and per_page=100" do
          let(:page) { 10_001 }

          it "does not log" do
            Rails.logger.expects(:info).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not increment any metrics" do
            statsd_client.expects(:increment).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not call Prop#throttle!" do
            Prop.expects(:throttle!).never
            limiter.throttle_index_with_deep_offset_pagination
          end
        end
      end

      describe "when throttle returns false" do
        before do
          Prop.stubs(:throttle).returns(false)
          Prop.stubs(:throttle!).returns(false)
        end

        describe "for page=1 and per_page=100" do
          let(:page) { 1 }

          it "does not log" do
            Rails.logger.expects(:info).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not increment any metrics" do
            statsd_client.expects(:increment).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not call Prop#throttle!" do
            Prop.expects(:throttle!).never
            limiter.throttle_index_with_deep_offset_pagination
          end
        end

        describe "for page=10,001 and per_page=100" do
          let(:page) { 10_001 }

          it "does not log" do
            Rails.logger.expects(:info).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not increment any metrics" do
            statsd_client.expects(:increment).never
            limiter.throttle_index_with_deep_offset_pagination
          end

          it "does not call Prop#throttle!" do
            Prop.expects(:throttle!).never
            limiter.throttle_index_with_deep_offset_pagination
          end
        end
      end
    end
  end
end
