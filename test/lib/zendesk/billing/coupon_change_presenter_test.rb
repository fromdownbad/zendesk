require_relative "../../../support/test_helper"
require_relative "../../../support/billing_test_helper"

SingleCov.covered!

describe Zendesk::Billing::CouponChangePresenter do
  let(:new_coupon_results) { [:success, :nonexistant, :inactive, :not_applicable, :another_coupon_active] }
  let(:previous_coupon_results) { [:invalidated, :still_applicable] }

  def result_codes
    "#{@presenter.previous_coupon_result}/#{@presenter.new_coupon_result}"
  end

  def set_new_coupon_result(result) # rubocop:disable Naming/AccessorMethodName
    @coupon_change.stubs(:new_coupon_result).returns(result)
  end

  def set_previous_coupon_result(result) # rubocop:disable Naming/AccessorMethodName
    @coupon_change.stubs(:previous_coupon_result).returns(result)
  end

  describe Zendesk::Billing::CouponChangePresenter do
    before do
      @coupon_change = mock
      [:previous_coupon_result, :previous_coupon_code, :new_coupon_result, :new_coupon_code].each do |method|
        @coupon_change.stubs(method).returns nil
      end

      @presenter = Zendesk::Billing::CouponChangePresenter.new(@coupon_change)
    end

    describe '#which_message' do
      it 'returns :new or :previous for all combinations of real and nil result pairs' do
        (new_coupon_results + [nil]).each do |new_result|
          set_new_coupon_result(new_result)

          (previous_coupon_results + [nil]).each do |previous_result|
            set_previous_coupon_result(previous_result)

            assert [:new, :previous].member?(@presenter.which_message), result_codes.inspect
          end
        end
      end
    end

    describe 'coupon_message' do
      it 'returns an array with a non empty String in #second and :warn or :notice in #first for all combinations of real result pairs' do
        new_coupon_results.each do |new_result|
          set_new_coupon_result(new_result)

          previous_coupon_results.each do |previous_result|
            set_previous_coupon_result(previous_result)

            msg = @presenter.coupon_message

            assert_kind_of String, msg.second, result_codes.inspect
            refute msg.second.empty?, result_codes.inspect
            assert [:notice, :warn].member?(msg.first), result_codes.inspect
          end
        end
      end
    end

    describe '#new_coupon_message' do
      it 'returns arrays with :notice or :warn in #first for all new coupon results' do
        new_coupon_results.each do |result|
          set_new_coupon_result(result)

          assert [:notice, :warn].member?(@presenter.new_coupon_message.first), result.inspect
        end
      end

      it 'returns nil for a nil new coupon result' do
        assert_nil @presenter.new_coupon_message
      end

      it "encodes HTML entities" do
        @presenter.expects(:new_coupon_result).returns(:inactive)
        @presenter.expects(:new_coupon_code).returns("Hello <b>THERE</b>!")
        @presenter.new_coupon_message.second.must_include "Hello &lt;b&gt;THERE&lt;/b&gt;!"
      end

      describe 'with future_tense true' do
        it 'returns arrays with #second containing non empty strings without the word "was" for all new coupon results' do
          new_coupon_results.each do |result|
            set_new_coupon_result(result)

            msg = @presenter.new_coupon_message(true).second

            assert_instance_of ActiveSupport::SafeBuffer, msg, result.inspect
            refute msg.empty?, result.inspect
            refute msg.index("was")
          end
        end
      end

      describe 'with future_tense true' do
        it 'returns arrays with #second containing non empty strings without the word "will be" for all new coupon results' do
          new_coupon_results.each do |result|
            set_new_coupon_result(result)

            msg = @presenter.new_coupon_message(false).second

            assert_instance_of ActiveSupport::SafeBuffer, msg, result.inspect
            refute msg.empty?, result.inspect
            refute msg.index("will be")
          end
        end
      end
    end

    describe '#previous_coupon_message' do
      it 'returns arrays with :notice or :warn in #first for all new coupon results' do
        previous_coupon_results.each do |result|
          set_previous_coupon_result(result)

          assert [:notice, :warn].member?(@presenter.previous_coupon_message.first), result.inspect
        end
      end

      it 'returns nil for a nil new coupon result' do
        assert_nil @presenter.previous_coupon_message
      end

      describe 'with future_tense true' do
        it 'returns arrays with #second containing non empty strings without the word "was" for all new coupon results' do
          previous_coupon_results.each do |result|
            set_previous_coupon_result(result)

            msg = @presenter.previous_coupon_message(true).second

            assert_instance_of String, msg, result.inspect
            refute msg.empty?, result.inspect
            refute msg.index("was")
          end
        end
      end

      describe 'with future_tense true' do
        it 'returns arrays with #second containing non empty strings without the word "will be" for all new coupon results' do
          previous_coupon_results.each do |result|
            set_previous_coupon_result(result)

            msg = @presenter.previous_coupon_message(false).second

            assert_instance_of String, msg, result.inspect
            refute msg.empty?, result.inspect
            refute msg.index("will be")
          end
        end
      end
    end
  end
end
