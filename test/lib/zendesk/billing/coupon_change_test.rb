require_relative "../../../support/test_helper"
require_relative "../../../support/billing_test_helper"

SingleCov.covered! uncovered: 6

describe Zendesk::Billing::CouponChange do
  include Zendesk::Billing
  include BillingTestHelper

  describe '#consider-ing a Subscription' do
    before do
      @subscription = FactoryBot.build :subscription
      @coupon_change = Zendesk::Billing::CouponChange.new(@subscription)
      def @coupon_change.exhausted_coupon_application # rubocop:disable Style/TrivialAccessors
        @exhausted_coupon_application;
      end
    end

    describe 'without an active coupon application' do
      describe 'or a new_coupon_code' do
        it 'exposes nil results' do
          assert_nil @coupon_change.previous_coupon_result
          assert_nil @coupon_change.new_coupon_result
        end

        it 'exposes nil active_coupon_application' do
          assert_nil @coupon_change.active_coupon_application
        end

        it 'leaves exhausted_coupon_application nil' do
          assert_nil @coupon_change.exhausted_coupon_application
        end
      end

      describe 'and a new, applicable coupon code' do
        before do
          @coupon = FactoryBot.build :active_discount_coupon
          Coupon.expects(:find_by_coupon_code).with(@coupon.coupon_code).returns(@coupon)

          @coupon_application = CouponApplication.new(coupon: @coupon)
          @coupon_application.expects(:is_valid_on?).with(@subscription).returns(true)
          @subscription.expects(:apply_coupon).with(@coupon).returns(@coupon_application)

          @coupon_change.consider(@coupon.coupon_code)
        end

        it 'leaves :previous_coupon_result nil' do
          assert_nil @coupon_change.previous_coupon_result
        end

        it 'sets :new_coupon_result to :success' do
          assert_equal :success, @coupon_change.new_coupon_result
        end

        it 'apply the coupon to the subscription, setting @active_coupon_application to the result' do
          assert_equal @coupon_application, @coupon_change.active_coupon_application
        end

        it 'leaves exhausted_coupon_application nil' do
          assert_nil @coupon_change.exhausted_coupon_application
        end
      end

      describe 'and a new_coupon_code for a non existant coupon' do
        before do
          Coupon.expects(:find_by_coupon_code).with('not here').returns(nil)

          CouponApplication.any_instance.expects(:new).never
          @subscription.expects(:apply_coupon).never

          @coupon_change.consider('not here')
        end

        it 'leaves :previous_coupon_result nil' do
          assert_nil @coupon_change.previous_coupon_result
        end

        it 'sets :new_coupon_result to :nonexistant' do
          assert_equal :nonexistant, @coupon_change.new_coupon_result
        end

        it 'does not apply the coupon to the subscription and leave @active_coupon_application nil' do
          assert_nil @coupon_change.active_coupon_application
        end

        it 'leaves exhausted_coupon_application nil' do
          assert_nil @coupon_change.exhausted_coupon_application
        end
      end

      describe 'and a new_coupon_code for an inactive coupon' do
        before do
          @coupon = FactoryBot.build :discount_coupon
          Coupon.expects(:find_by_coupon_code).with(@coupon.coupon_code).returns(@coupon)

          CouponApplication.any_instance.expects(:new).never
          @subscription.expects(:apply_coupon).never

          @coupon_change.consider(@coupon.coupon_code)
        end

        it 'leaves :previous_coupon_result nil' do
          assert_nil @coupon_change.previous_coupon_result
        end

        it 'sets :new_coupon_result to :inactive' do
          assert_equal :inactive, @coupon_change.new_coupon_result
        end

        it 'does not apply the coupon to the subscription and leave @active_coupon_application nil' do
          assert_nil @coupon_change.active_coupon_application
        end

        it 'leaves exhausted_coupon_application nil' do
          assert_nil @coupon_change.exhausted_coupon_application
        end
      end

      describe 'and a new_coupon_code that generates an invalid application' do
        before do
          @coupon = FactoryBot.build :active_discount_coupon
          Coupon.expects(:find_by_coupon_code).with(@coupon.coupon_code).returns(@coupon)

          @coupon_application = CouponApplication.new(coupon: @coupon)
          @coupon_application.expects(:is_valid_on?).with(@subscription).returns(false)
          @subscription.expects(:apply_coupon).with(@coupon).returns(@coupon_application)

          @coupon_change.consider(@coupon.coupon_code)
        end

        it 'leaves :previous_coupon_result nil' do
          assert_nil @coupon_change.previous_coupon_result
        end

        it 'sets :new_coupon_result to the lookup :not_applicable' do
          assert_equal :not_applicable, @coupon_change.new_coupon_result
        end

        it 'apply the coupon to the subscription yet leave @active_coupon_application nil' do
          assert_nil @coupon_change.active_coupon_application
        end

        it 'leaves exhausted_coupon_application nil' do
          assert_nil @coupon_change.exhausted_coupon_application
        end
      end
    end

    describe 'with an active coupon application' do
      before do
        Timecop.freeze(Time.now)

        @coupon = FactoryBot.build :active_discount_coupon
        @coupon_application = CouponApplication.new(coupon: @coupon, exhaustion_date: 2.years.from_now)
        @subscription.expects(:active_coupon_application).returns(@coupon_application)
        @coupon_change = Zendesk::Billing::CouponChange.new(@subscription)

        def @coupon_change.exhausted_coupon_application # rubocop:disable Style/TrivialAccessors
          @exhausted_coupon_application
        end
      end

      describe 'on an update that leaves the application valid' do
        before do
          @coupon_application.expects(:is_valid_on?).returns(true)
        end

        describe 'without a new_coupon_code' do
          before do
            @coupon_change.consider
          end

          it 'sets :previous_coupon_result to :still_applicable' do
            assert_equal :still_applicable, @coupon_change.previous_coupon_result
          end

          it 'leaves :new_coupon_result nil' do
            assert_nil @coupon_change.new_coupon_result
          end

          it 'sets @active_coupon_application to the preexisting one' do
            assert_equal @coupon_application, @coupon_change.active_coupon_application
          end

          it 'leaves exhausted_coupon_application nil' do
            assert_nil @coupon_change.exhausted_coupon_application
          end

          it 'leaves the existing coupon application\'s exhaustion date unchanged' do
            assert_equal 2.years.from_now.to_s, @coupon_application.exhaustion_date.to_s
          end
        end

        describe 'with a new_coupon_code matching the one already active' do
          before do
            @coupon_change.consider(@subscription)
          end

          it 'sets :previous_coupon_result to :still_applicable' do
            assert_equal :still_applicable, @coupon_change.previous_coupon_result
          end

          it 'sets :new_coupon_result to :already_active' do
            # assert_equal :already_active, @coupon_change.new_coupon_result # FIXME this test is broken since 2011
          end

          it 'sets @active_coupon_application to the preexisting one' do
            assert_equal @coupon_application, @coupon_change.active_coupon_application
          end

          it 'leaves exhausted_coupon_application nil' do
            assert_nil @coupon_change.exhausted_coupon_application
          end

          it 'leaves the existing coupon application\'s exhaustion date unchanged' do
            assert_equal 2.years.from_now.to_s, @coupon_application.exhaustion_date.to_s
          end
        end

        describe 'with a new_coupon_code mismatched to the one already active' do
          before do
            @coupon_change.consider(@coupon.coupon_code + "_or_not")
          end

          it 'sets :previous_coupon_result to :still_applicable' do
            assert_equal :still_applicable, @coupon_change.previous_coupon_result
          end

          it 'sets @active_coupon_application to the preexisting one' do
            assert_equal @coupon_application, @coupon_change.active_coupon_application
          end

          it 'leaves exhausted_coupon_application nil' do
            assert_nil @coupon_change.exhausted_coupon_application
          end

          it 'leaves the existing coupon application\'s exhaustion date unchanged' do
            assert_equal 2.years.from_now.to_s, @coupon_application.exhaustion_date.to_s
          end
        end
      end

      describe 'on an update that invalidates the application' do
        before do
          travel_to Time.now
          @coupon_application.expects(:is_valid_on?).returns(false)
        end

        after do
          travel_back
        end

        describe 'without a new_coupon_code' do
          before do
            @coupon_change.consider
          end

          it 'sets :previous_coupon_result to :invalidated' do
            assert_equal :invalidated, @coupon_change.previous_coupon_result
          end

          it 'leaves :new_coupon_result nil' do
            assert_nil @coupon_change.new_coupon_result
          end

          it 'sets active_coupon_application to nil' do
            assert_nil @coupon_change.active_coupon_application
          end

          it 'sets exhausted_coupon_application to the existing coupon application' do
            assert_equal @coupon_application, @coupon_change.exhausted_coupon_application
          end

          it 'sets the existing coupon application\'s exhaustion date to now' do
            assert_equal Time.zone.now, @coupon_application.exhaustion_date
          end
        end

        describe 'with a new, applicable coupon code' do
          before do
            @coupon = FactoryBot.build :active_discount_coupon
            Coupon.expects(:find_by_coupon_code).with(@coupon.coupon_code).returns(@coupon)

            @new_coupon_application = CouponApplication.new(coupon: @coupon)
            @new_coupon_application.expects(:is_valid_on?).with(@subscription).returns(true)
            @subscription.expects(:apply_coupon).with(@coupon).returns(@new_coupon_application)

            @coupon_change.consider(@coupon.coupon_code)
          end

          it 'sets :previous_coupon_result to :invalidated' do
            assert_equal :invalidated, @coupon_change.previous_coupon_result
          end

          it 'sets :new_coupon_result to :success' do
            assert_equal :success, @coupon_change.new_coupon_result
          end

          it 'apply the coupon to the subscription, setting @active_coupon_application to the result' do
            assert_equal @new_coupon_application, @coupon_change.active_coupon_application
          end

          it 'sets exhausted_coupon_application to the existing coupon application' do
            assert_equal @coupon_application, @coupon_change.exhausted_coupon_application
          end

          it 'sets the existing coupon application\'s exhaustion date to now' do
            assert_equal Time.zone.now, @coupon_application.exhaustion_date
          end
        end

        describe 'and a new_coupon_code for a non existant coupon' do
          before do
            Coupon.expects(:find_by_coupon_code).with('not here').returns(nil)

            CouponApplication.any_instance.expects(:new).never
            @subscription.expects(:apply_coupon).never

            @coupon_change.consider('not here')
          end

          it 'sets :previous_coupon_result to :invalidated' do
            assert_equal :invalidated, @coupon_change.previous_coupon_result
          end

          it 'sets :new_coupon_result to :nonexistant' do
            assert_equal :nonexistant, @coupon_change.new_coupon_result
          end

          it 'does not apply the coupon to the subscription and leave @active_coupon_application nil' do
            assert_nil @coupon_change.active_coupon_application
          end

          it 'sets exhausted_coupon_application to the existing coupon application' do
            assert_equal @coupon_application, @coupon_change.exhausted_coupon_application
          end

          it 'sets the existing coupon application\'s exhaustion date to now' do
            assert_equal Time.zone.now, @coupon_application.exhaustion_date
          end
        end

        describe 'and a new_coupon_code for an inactive coupon' do
          before do
            @coupon = FactoryBot.build :discount_coupon
            Coupon.expects(:find_by_coupon_code).with(@coupon.coupon_code).returns(@coupon)

            CouponApplication.any_instance.expects(:new).never
            @subscription.expects(:apply_coupon).never

            @coupon_change.consider(@coupon.coupon_code)
          end

          it 'sets :previous_coupon_result to :invalidated' do
            assert_equal :invalidated, @coupon_change.previous_coupon_result
          end

          it 'sets :new_coupon_result to :inactive' do
            assert_equal :inactive, @coupon_change.new_coupon_result
          end

          it 'does not apply the coupon to the subscription and leave @active_coupon_application nil' do
            assert_nil @coupon_change.active_coupon_application
          end

          it 'sets exhausted_coupon_application to the existing coupon application' do
            assert_equal @coupon_application, @coupon_change.exhausted_coupon_application
          end

          it 'sets the existing coupon application\'s exhaustion date to now' do
            assert_equal Time.zone.now, @coupon_application.exhaustion_date
          end
        end

        describe 'with a new_coupon_code that generates an invalid application' do
          before do
            @coupon = FactoryBot.build :active_discount_coupon
            Coupon.expects(:find_by_coupon_code).with(@coupon.coupon_code).returns(@coupon)

            @new_coupon_application = CouponApplication.new(coupon: @coupon)
            @new_coupon_application.expects(:is_valid_on?).with(@subscription).returns(false)
            @subscription.expects(:apply_coupon).with(@coupon).returns(@new_coupon_application)

            @coupon_change.consider(@coupon.coupon_code)
          end

          it 'sets :previous_coupon_result to :invalidated' do
            assert_equal :invalidated, @coupon_change.previous_coupon_result
          end

          it 'sets :new_coupon_result to the lookup :not_applicable' do
            assert_equal :not_applicable, @coupon_change.new_coupon_result
          end

          it 'apply the coupon to the subscription yet leave @active_coupon_application nil' do
            assert_nil @coupon_change.active_coupon_application
          end

          it 'sets exhausted_coupon_application to the existing coupon application' do
            assert_equal @coupon_application, @coupon_change.exhausted_coupon_application
          end

          it 'sets the existing coupon application\'s exhaustion date to now' do
            assert_equal Time.zone.now, @coupon_application.exhaustion_date
          end
        end
      end
    end
  end
end
