require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 3

describe 'ParameterWhitelist' do
  it 'uses the AllowedParameters module' do
    assert Zendesk::ParameterWhitelist == ::AllowedParameters
  end
end

class MyTestController < ApplicationController
  skip_before_action :authenticate_user
  include ::AllowedParameters

  allow_parameters :show, allowed: Parameters.string
  def show
    head 200
  end
end

class ParameterWhitelistTest < ActionController::TestCase
  tests MyTestController
  use_test_routes

  before do
    @account = accounts(:minimum)
    @request.account = @account
  end

  it "does not let invalid parameters pass through" do
    get :show, params: { allowed: {"hey" => "there"} }
    assert_response :bad_request
  end
end
