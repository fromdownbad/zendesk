require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::ChatTranscripts::ChatHistoryTicketEnumerator do
  let(:subject) { Zendesk::ChatTranscripts::ChatHistoryTicketEnumerator.new(chat_history) }

  describe '#with_first_agent_comment' do
    describe 'for agent comment' do
      let(:chat_history) do
        [{
           'type' => 'ChatMessage',
           'actor_id' => '456',
           'actor_type' => 'agent',
           'actor_name' => 'smith',
           'timestamp' => 1592033862_000,
           'message' => 'what good is a phone call if you are unable to speak?'
         }, {
          'type' => 'ChatMessage',
          'actor_id' => '123',
          'actor_type' => 'end-user',
          'actor_name' => 'neo',
          'timestamp' => 1592033863_000,
          'message' => '(mumbling sounds)'
         }, {
           'type' => 'ChatMessage',
           'actor_id' => '456',
           'actor_type' => 'agent',
           'actor_name' => 'smith',
           'timestamp' => 1592033865_000,
           'message' => 'you will cooperate with us, whether you like it or not'
         }]
      end

      it 'yields once' do
        ticket = mock
        block_calls = 0
        subject.with_first_agent_comment(ticket) do |virtual_ticket|
          block_calls += 1
          assert_equal virtual_ticket, ticket
          assert_predicate virtual_ticket, :comment_added?
          assert_equal Time.at(1592033862), virtual_ticket.updated_at
          assert_predicate virtual_ticket.comment, :public_reply?
          refute_predicate virtual_ticket.comment, :from_end_user?
          refute_predicate virtual_ticket.comment, :is_private?
        end
        assert_equal 1, block_calls, 'did not yield exactly once'
      end
    end

    describe 'for end-user' do
      let(:chat_history) do
        [{
           'type' => 'ChatMessage',
           'actor_id' => '123',
           'actor_type' => 'end-user',
           'actor_name' => 'mr anderson',
           'timestamp' => 1592033860_000,
           'message' => 'i want my phone call'
         }]
      end

      it 'does not yield' do
        ticket = mock
        block_called = false
        subject.with_first_agent_comment(ticket) { |_| block_called = true }
        refute block_called, 'yielded at least once'
      end
    end
  end
end
