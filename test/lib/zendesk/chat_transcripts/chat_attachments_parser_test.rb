require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::ChatTranscripts::ChatAttachmentsParser do
  fixtures :accounts, :tokens, :attachments

  let(:upload) do
    # force load association here, because after the attachments are added, they would have changed ownership
    tokens(:minimum_upload_token).tap { |u| u.attachments.to_a }
  end

  let(:other_upload) do
    # force load association here, because after the attachments are added, they would have changed ownership
    tokens(:other_minimum_upload_token).tap { |u| u.attachments.to_a }
  end
  let(:described_class) { Zendesk::ChatTranscripts::ChatAttachmentsParser }

  describe '#upload_tokens' do
    it 'returns empty array for empty history' do
      parser = described_class.new([])
      assert_equal [], parser.upload_tokens
    end

    it 'returns upload tokens from transcript' do
      parser = described_class.new([{
        type: 'ChatFileAttachment',
        actor_id: '123',
        actor_type: 'agent',
        actor_name: 'mr smith',
        timestamp: Time.zone.now.to_i,
        support_upload_token: upload.value
      }.with_indifferent_access, {
        type: 'ChatFileAttachment',
        actor_id: '456',
        actor_type: 'end-user',
        actor_name: 'mrs smith',
        timestamp: Time.zone.now.to_i,
        support_upload_token: other_upload.value
      }.with_indifferent_access])
      assert_equal ['minimum_upload_token', 'other_minimum_upload_token'], parser.upload_tokens
    end
  end

  describe '#attachments' do
    it 'returns empty attachments for empty history' do
      parser = described_class.new([])
      assert_equal [], parser.attachments(upload.account)
    end

    it 'return attachments for history with upload tokens' do
      parser = described_class.new([{
        type: 'ChatFileAttachment',
        actor_id: '123',
        actor_type: 'agent',
        actor_name: 'mr smith',
        timestamp: Time.zone.now.to_i,
        support_upload_token: upload.value
      }.with_indifferent_access, {
        type: 'ChatFileAttachment',
        actor_id: '456',
        actor_type: 'end-user',
        actor_name: 'mrs smith',
        timestamp: Time.zone.now.to_i,
        support_upload_token: other_upload.value
      }.with_indifferent_access, {
        type: 'ChatFileAttachment',
        actor_id: '456',
        actor_type: 'end-user',
        actor_name: 'mrs smith',
        timestamp: Time.zone.now.to_i,
      }.with_indifferent_access])
      attachments = parser.attachments(upload.account)
      assert_equal 3, attachments.size
      assert_equal upload.attachments + other_upload.attachments, attachments
    end
  end
end
