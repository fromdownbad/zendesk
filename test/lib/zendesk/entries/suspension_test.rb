require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::Entries::Suspension' do
  fixtures :accounts, :forums, :entries, :posts, :users

  describe "#run" do
    let(:suspended_entries) { [] }
    let(:whitelisted_entries) { [] }
    let(:suspended_posts) { [] }
    let(:whitelisted_posts) { [] }

    subject do
      Zendesk::Entries::Suspension.new(accounts(:minimum),
        :suspended_entries => suspended_entries,
        :whitelisted_entries => whitelisted_entries,
        :suspended_posts => suspended_posts,
        :whitelisted_posts => whitelisted_posts)
    end

    describe "with duplicate entry ids" do
      let(:suspended_entries) { [1] }
      let(:whitelisted_entries) { [1] }

      it "returns false from #run" do
        refute subject.run
        refute subject.success?
      end
    end

    describe "with duplicate post ids" do
      let(:suspended_posts) { [1] }
      let(:whitelisted_posts) { [1] }

      it "returns false from #run" do
        refute subject.run
        refute subject.success?
      end
    end

    describe "entries - when trying to suspend and whitelist one user" do
      let(:suspended_entries) { [entries(:technology).id] }
      let(:whitelisted_entries) { [entries(:sticky).id] }

      it "returns false from #run" do
        refute subject.run
        refute subject.success?
      end
    end

    describe "posts - when trying to suspend and whitelist one user" do
      let(:suspended_posts) { [posts(:ponies_post_1).id] }
      let(:whitelisted_posts) { [posts(:fish_post_1).id] }

      it "returns false from #run" do
        refute subject.run
        refute subject.success?
      end
    end

    describe "entries + posts - when trying to suspend and whitelist one user" do
      describe "whitelisted entry" do
        let(:suspended_posts) { [posts(:ponies_post_1).id] }
        let(:whitelisted_entries) { [entries(:sticky).id] }

        it "returns false from #run" do
          refute subject.run
          refute subject.success?
        end
      end

      describe "whitelisted post" do
        let(:suspended_entries) { [entries(:sticky).id] }
        let(:whitelisted_posts) { [posts(:ponies_post_1).id] }

        it "returns false from #run" do
          refute subject.run
          refute subject.success?
        end
      end
    end

    describe "valid suspension" do
      let(:suspended_entries) { [entries(:technology).id] }
      let(:user) { users(:minimum_end_user) }
      let(:post) { posts(:sticky_reply) }

      before do
        entries(:sticky) # Another entry by minimum_end_user
        forums(:announcements).soft_delete!

        suspend(post)

        subject.run
      end

      it "is successful" do
        subject.success?
      end

      it "deletes that entry" do
        assert Entry.with_deleted { entries(:technology) }.reload.deleted?
      end

      it "deletes all other entries by that user" do
        assert Entry.with_deleted { entries(:sticky) }.reload.deleted?
      end

      it "deletes all posts" do
        post.reload

        assert post.deleted?
        refute post.suspended?
      end

      it "suspends the user" do
        assert user.suspended?
      end

      it "updates the forum entries_count properly" do
        assert_equal 1, forums(:solutions).entries_count
      end

      it "updates the entries posts_count properly" do
        assert_equal 0, Entry.with_deleted { entries(:ponies) }.reload.posts_count
      end
    end

    describe "valid whitelist" do
      let(:whitelisted_entries) { [entries(:technology).id] }
      let(:user) { users(:minimum_end_user) }

      describe "with other suspended entries" do
        before do
          suspend(entries(:technology))
          suspend(entries(:sticky))

          subject.run
        end

        it "is successful" do
          subject.success?
        end

        it "unsuspends that entry" do
          refute entries(:technology).reload.deleted?
        end

        it "unsuspends all other entries by that user" do
          refute entries(:sticky).reload.deleted?
        end

        it "whitelists the user" do
          assert user.whitelisted_from_moderation?
        end
      end

      describe "with other deleted entries" do
        before do
          suspend(entries(:technology))
          entries(:sticky).soft_delete!

          subject.run
        end

        it "is successful" do
          subject.success?
        end

        it "unsuspends that entry" do
          refute entries(:technology).reload.deleted?
        end

        it "does not undelete all other entries by that user" do
          refute entries(:sticky).reload.suspended?
          assert entries(:sticky).deleted?
        end

        it "whitelists the user" do
          assert user.whitelisted_from_moderation?
        end
      end
    end

    describe "handle RecordInvalid" do
      let(:suspended_entries) { [entries(:technology).id] }
      let(:user) { users(:minimum_end_user) }

      before do
        Entry.any_instance.expects(:soft_delete!).raises(ActiveRecord::RecordInvalid.new(entries(:technology)))
      end

      it "returns false" do
        refute subject.run
      end
    end
  end

  private

  def suspend(obj)
    obj.send(:suspend)
    obj.save!
  end
end
