require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Zendesk::Entries::MonitorForumCleanup do
  extend Api::V2::TestHelper

  fixtures :accounts, :users, :forums, :entries

  let(:forum) { forums(:english_forum) }
  let(:params) { {    "account"          => accounts(:minimum),
                      "start_date"       => (Time.now.utc - 8.days).to_i,
                      "end_date"         => Time.now.utc.to_i,
                      "moderate_forum"   => forum.id,
                      "suspend_users"    => suspend_users } }

  before do
    @account = accounts(:minimum)
  end

  describe "#target_forum" do
    let(:suspend_users) { false }
    it "finds the correct forum" do
      result = Zendesk::Entries::MonitorForumCleanup.new(params)
      assert_equal forum, result.send(:target_forum)
    end

    it "handles non-existent forums" do
      forum.expects(:id).returns(110110101)
      result = Zendesk::Entries::MonitorForumCleanup.new(params)
    end
  end

  describe "#start_date and #end_date" do
    let(:suspend_users) { false }
    before do
      @result = Zendesk::Entries::MonitorForumCleanup.new(params)
    end

    it "parses timestamps correctly" do
      assert_equal @result.send(:start_date).class.to_s, "Time"
      assert_equal @result.send(:end_date).class.to_s, "Time"
    end
  end

  describe "#entries" do
    let(:suspend_users) { false }
    before do
      5.times.each do |i|
        entry = entries(:ponies).dup
        entry.current_user = users(:minimum_agent)
        entry.created_at = Time.now.utc - i.weeks
        entry.forum_id = forum.id
        entry.save!
      end
      @result = Zendesk::Entries::MonitorForumCleanup.new(params)
    end

    it "returns two entries" do
      assert_equal 2, @result.send(:entries).size
    end

    it "returns all entries" do
      Zendesk::Entries::MonitorForumCleanup.any_instance.expects(:start_date).returns(Time.now.utc - 60.weeks)
      Zendesk::Entries::MonitorForumCleanup.any_instance.expects(:end_date).returns(Time.now.utc)
      @result = Zendesk::Entries::MonitorForumCleanup.new(params)
      assert_equal 5, @result.send(:entries).size
    end

    it "returns all entries except one soft_deleted entry" do
      Entry.last.soft_delete!
      Zendesk::Entries::MonitorForumCleanup.any_instance.expects(:start_date).returns(Time.now.utc - 60.weeks)
      Zendesk::Entries::MonitorForumCleanup.any_instance.expects(:end_date).returns(Time.now.utc)
      @result = Zendesk::Entries::MonitorForumCleanup.new(params)
      assert_equal 4, @result.send(:entries).size
    end
  end

  describe "#run" do
      let(:suspend_users) { false }
      let(:current_user) { users(:minimum_agent) }
      let(:end_user) { users(:minimum_end_user) }
      before do
        5.times.each do |i|
          entry = entries(:ponies).dup
          entry.current_user = current_user
          entry.submitter = current_user
          entry.created_at = Time.now.utc - i.weeks
          entry.forum_id = forum.id
          entry.save!
        end
      end

      it "soft_deletes entries" do
        Entry.any_instance.stubs(:submitter).returns(end_user)
        Entry.any_instance.expects(:soft_delete).at_least(1)
        result = Zendesk::Entries::MonitorForumCleanup.new(params)
        result.run
      end

      describe "when suspend_users is true" do
        let(:suspend_users) { true }
        it "does not suspend agents" do
          Entry.any_instance.stubs(:submitter).returns(current_user)
          Zendesk::Entries::MonitorForumCleanup.any_instance.expects(:suspend_user).never
          result = Zendesk::Entries::MonitorForumCleanup.new(params)
          result.run
          refute current_user.suspended?
        end

        it "suspends end-users if spam entry submitter is an end-user" do
          Entry.any_instance.stubs(:submitter).returns(end_user)
          result = Zendesk::Entries::MonitorForumCleanup.new(params)
          result.run
          assert end_user.suspended?
        end

        it "only suspend a user once" do
          Entry.any_instance.stubs(:submitter).returns(end_user)
          User.any_instance.stubs(:suspended?).returns(true)
          User.any_instance.expects(:suspend).never
          result = Zendesk::Entries::MonitorForumCleanup.new(params)
          result.run
          assert end_user.suspended?
        end

        it "successfully soft_delete two entries" do
          Entry.any_instance.stubs(:submitter).returns(end_user)
          result = Zendesk::Entries::MonitorForumCleanup.new(params)
          result.run
          assert_equal 2, result.entries_removed
        end

        it "does not delete any agent-authored entries" do
          Entry.any_instance.stubs(:submitter).returns(current_user)
          result = Zendesk::Entries::MonitorForumCleanup.new(params)
          result.run
          assert_equal 0, result.entries_removed
        end
      end
    end
end
