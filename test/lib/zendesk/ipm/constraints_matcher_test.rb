require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Ipm::ConstraintsMatcher do
  def fetch_ipm_instance!(constraints = {})
    attributes = if constraints.is_a? Zendesk::Ipm::ConstraintSet
      { constraints: constraints }
    else
      constraints
    end

    if rand < 0.5
      FactoryBot.create(:ipm_feature_notification, attributes)
    else
      FactoryBot.create(:ipm_alert, attributes)
    end
  end

  describe "instance of an ipm model" do
    let(:constraints) do
      {
        pods:                '1',
        shards:              '2',
        plans:               '3',
        roles:               'Owner',
        account_types:       'trial',
        interfaces:          'lotus',
        languages:           '4',
        account_ids:         '5',
        exclude_account_ids: '6',
        internal_name:       'internal name'
      }
    end

    describe "with legacy data store" do
      let(:ipm) do
        constraint_set = Zendesk::Ipm::ConstraintSet.new(constraints)
        fetch_ipm_instance!(constraint_set)
      end

      before do
        assert ipm.ipm_constraints.blank?
      end

      describe "#account_excluded?" do
        let(:user) { stub }

        it "returns false when no exclusion set" do
          ipm.stubs(exclude_account_ids: nil)
          refute ipm.account_excluded?(user)
        end

        it "returns false when user.account_id is not in the set" do
          user.stubs(account_id: 5)
          refute ipm.account_excluded?(user)
        end

        it "returns true when user.account_id is in the set" do
          user.stubs(account_id: 6)
          assert ipm.account_excluded?(user)
        end
      end

      describe "#account_id_matches?" do
        let(:user) { stub }

        it "returns true when no inclusion set" do
          ipm.stubs(account_ids: nil)
          assert ipm.account_id_matches?(user)
        end

        it "returns true when user.account_id is in the set" do
          user.stubs(account_id: 5)
          assert ipm.account_id_matches?(user)
        end

        it "returns false when user.account_id is not in the set" do
          user.stubs(account_id: 6)
          refute ipm.account_id_matches?(user)
        end
      end

      describe "#language_matches?" do
        let(:user) { stub }

        it "returns true when no inclusion set" do
          ipm.stubs(languages: nil)
          assert ipm.language_matches?(user)
        end

        it "returns true when user.translation_locale.id is in the set" do
          user.stubs(translation_locale: stub(id: 4))
          assert ipm.language_matches?(user)
        end

        it "returns false when user.translation_locale.id is not in the set" do
          user.stubs(translation_locale: stub(id: 5))
          refute ipm.language_matches?(user)
        end
      end

      describe "#role_matches?" do
        let(:user) { stub }

        it "returns true when no inclusion set" do
          ipm.stubs(roles: nil)
          assert ipm.role_matches?(user)
        end

        it "returns true when user.role is in the set" do
          user.stubs(role: 'Owner', is_account_owner?: true)
          assert ipm.role_matches?(user)
        end

        it "returns false when user.role is not in the set" do
          user.stubs(role: 'Agent', is_account_owner?: false)
          refute ipm.role_matches?(user)
        end
      end

      describe "#account_type_matches?" do
        let(:account) { stub }
        let(:user)    { stub(account: account) }

        it "returns true when no inclusion set" do
          ipm.stubs(account_types: nil)
          assert ipm.account_type_matches?(user)
        end

        it "returns true when user.account.subscription.account_type is in the set" do
          user.account.stubs(subscription: stub(account_type: 'trial'))
          assert ipm.account_type_matches?(user)
        end

        it "returns false when user.account.subscription.account_type is not in the set" do
          user.account.stubs(subscription: stub(account_type: 'sponsored'))
          refute ipm.account_type_matches?(user)
        end
      end

      describe "#plan_matches?" do
        let(:account) { stub }
        let(:user)    { stub(account: account) }

        it "returns true when no inclusion set" do
          ipm.stubs(plans: nil)
          assert ipm.plan_matches?(user)
        end

        it "returns true when user.account.subscription.plan_type is in the set" do
          user.account.stubs(subscription: stub(plan_type: 3))
          assert ipm.plan_matches?(user)
        end

        it "returns false when user.account.subscription.plan_type is not in the set" do
          user.account.stubs(subscription: stub(plan_type: 4))
          refute ipm.plan_matches?(user)
        end
      end

      describe "#shard_matches?" do
        let(:user) { stub }

        it "returns true when no inclusion set" do
          ipm.stubs(shards: nil)
          assert ipm.shard_matches?(user)
        end

        it "returns true when user.account.shard_id is in the set" do
          user.stubs(account: stub(shard_id: 2))
          assert ipm.shard_matches?(user)
        end

        it "returns false when user.account.shard_id is not in the set" do
          user.stubs(account: stub(shard_id: 3))
          refute ipm.shard_matches?(user)
        end
      end

      describe "#pod_matches?" do
        it "returns true when no inclusion set" do
          ipm.stubs(pods: nil)
          assert ipm.pod_matches?
        end

        it "returns true when Zendesk::Configuration.fetch(:pod_id) is in the set" do
          Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(1)
          assert ipm.pod_matches?
        end

        it "returns false when Zendesk::Configuration.fetch(:pod_id) is not in the set" do
          Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(2)
          refute ipm.pod_matches?
        end
      end

      describe "#interface_matches?" do
        it "returns true when no inclusion set" do
          ipm.stubs(interfaces: nil)
          assert ipm.interface_matches?('lotus')
        end

        it "returns true when no interface set" do
          assert ipm.interface_matches?(nil)
        end

        it "returns true when interface is in the inclusion set" do
          assert ipm.interface_matches?('lotus')
        end

        it "returns false when interface is not in the inclusion set" do
          refute ipm.interface_matches?('classic')
        end
      end

      describe "when constraint set matches configuration" do
        let(:user) do
          users(:minimum_agent).tap do |u|
            u.account.stubs(shard_id: 2)
            u.stubs(role: 'Owner', is_account_owner?: true)
            u.account.subscription.stubs(account_type: 'trial')
            u.account.subscription.stubs(plan_type: 3)
            u.translation_locale.stubs(id: 4)
            u.stubs(account_id: 5)
          end
        end

        describe "#matches_user_and_interface?" do
          it "returns true" do
            assert ipm.matches_user_and_interface?(user, 'lotus')
          end

          describe "when account id is in the exclude set" do
            it "returns false" do
              ipm.expects(:account_excluded?).returns(true)
              refute ipm.matches_user_and_interface?(user, 'lotus')
            end
          end

          describe "when account interface does not match" do
            it "returns false" do
              refute ipm.matches_user_and_interface?(user, 'classic')
            end
          end

          describe "when account interface is not set" do
            it "returns true" do
              assert ipm.matches_user_and_interface?(user, nil)
            end
          end
        end
      end

      describe "when constraint set does not match configuration" do
        let(:user) do
          users(:minimum_agent).tap do |u|
            u.account.stubs(shard_id: 2)
            u.stubs(role: 'Owner', is_account_owner?: true)
            u.account.subscription.stubs(account_type: 'sponsored')
            u.account.subscription.stubs(plan_type: 3)
            u.translation_locale.stubs(id: 4)
            u.stubs(account_id: 5)
          end
        end

        describe "#matches_user_and_interface?" do
          it "returns false" do
            refute ipm.matches_user_and_interface?(user, 'lotus')
          end

          describe "when account id is not in the exclude set" do
            it "returns false" do
              ipm.expects(:account_excluded?).returns(false)
              refute ipm.matches_user_and_interface?(user, 'lotus')
            end
          end
        end
      end
    end

    describe "with new data store" do
      let(:ipm) do
        fetch_ipm_instance!(constraints)
      end

      before do
        assert_equal 10, ipm.ipm_constraints.count(:all)
      end

      describe "#account_excluded?" do
        let(:user) { stub }

        it "returns false when no exclusion set" do
          ipm.stubs(exclude_account_ids: nil)
          refute ipm.account_excluded?(user)
        end

        it "returns false when user.account_id is not in the set" do
          user.stubs(account_id: 5)
          refute ipm.account_excluded?(user)
        end

        it "returns true when user.account_id is in the set" do
          user.stubs(account_id: 6)
          assert ipm.account_excluded?(user)
        end
      end

      describe "#account_id_matches?" do
        let(:user) { stub }

        it "returns true when no inclusion set" do
          ipm.stubs(account_ids: nil)
          assert ipm.account_id_matches?(user)
        end

        it "returns true when user.account_id is in the set" do
          user.stubs(account_id: 5)
          assert ipm.account_id_matches?(user)
        end

        it "returns false when user.account_id is not in the set" do
          user.stubs(account_id: 6)
          refute ipm.account_id_matches?(user)
        end
      end

      describe "#language_matches?" do
        let(:user) { stub }

        it "returns true when no inclusion set" do
          ipm.stubs(languages: nil)
          assert ipm.language_matches?(user)
        end

        it "returns true when user.translation_locale.id is in the set" do
          user.stubs(translation_locale: stub(id: 4))
          assert ipm.language_matches?(user)
        end

        it "returns false when user.translation_locale.id is not in the set" do
          user.stubs(translation_locale: stub(id: 5))
          refute ipm.language_matches?(user)
        end
      end

      describe "#role_matches?" do
        let(:user) { stub }

        it "returns true when no inclusion set" do
          ipm.stubs(roles: nil)
          assert ipm.role_matches?(user)
        end

        it "returns true when user.role is in the set" do
          user.stubs(role: 'Owner', is_account_owner?: true)
          assert ipm.role_matches?(user)
        end

        it "returns false when user.role is not in the set" do
          user.stubs(role: 'Agent', is_account_owner?: false)
          refute ipm.role_matches?(user)
        end
      end

      describe "#account_type_matches?" do
        let(:account) { stub }
        let(:user)    { stub(account: account) }

        it "returns true when no inclusion set" do
          ipm.stubs(account_types: nil)
          assert ipm.account_type_matches?(user)
        end

        it "returns true when user.account.subscription.account_type is in the set" do
          user.account.stubs(subscription: stub(account_type: 'trial'))
          assert ipm.account_type_matches?(user)
        end

        it "returns false when user.account.subscription.account_type is not in the set" do
          user.account.stubs(subscription: stub(account_type: 'sponsored'))
          refute ipm.account_type_matches?(user)
        end
      end

      describe "#plan_matches?" do
        let(:account) { stub }
        let(:user)    { stub(account: account) }

        it "returns true when no inclusion set" do
          ipm.stubs(plans: nil)
          assert ipm.plan_matches?(user)
        end

        it "returns true when user.account.subscription.plan_type is in the set" do
          user.account.stubs(subscription: stub(plan_type: 3))
          assert ipm.plan_matches?(user)
        end

        it "returns false when user.account.subscription.plan_type is not in the set" do
          user.account.stubs(subscription: stub(plan_type: 4))
          refute ipm.plan_matches?(user)
        end
      end

      describe "#shard_matches?" do
        let(:user) { stub }

        it "returns true when no inclusion set" do
          ipm.stubs(shards: nil)
          assert ipm.shard_matches?(user)
        end

        it "returns true when user.account.shard_id is in the set" do
          user.stubs(account: stub(shard_id: 2))
          assert ipm.shard_matches?(user)
        end

        it "returns false when user.account.shard_id is not in the set" do
          user.stubs(account: stub(shard_id: 3))
          refute ipm.shard_matches?(user)
        end
      end

      describe "#pod_matches?" do
        it "returns true when no inclusion set" do
          ipm.stubs(pods: nil)
          assert ipm.pod_matches?
        end

        it "returns true when Zendesk::Configuration.fetch(:pod_id) is in the set" do
          Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(1)
          assert ipm.pod_matches?
        end

        it "returns false when Zendesk::Configuration.fetch(:pod_id) is not in the set" do
          Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(2)
          refute ipm.pod_matches?
        end
      end

      describe "#interface_matches?" do
        it "returns true when no inclusion set" do
          ipm.stubs(interfaces: nil)
          assert ipm.interface_matches?('lotus')
        end

        it "returns true when no interface set" do
          assert ipm.interface_matches?(nil)
        end

        it "returns true when interface is in the inclusion set" do
          assert ipm.interface_matches?('lotus')
        end

        it "returns false when interface is not in the inclusion set" do
          refute ipm.interface_matches?('classic')
        end
      end

      describe "when constraint set matches configuration" do
        let(:user) do
          users(:minimum_agent).tap do |u|
            u.account.stubs(shard_id: 2)
            u.stubs(role: 'Owner', is_account_owner?: true)
            u.account.subscription.stubs(account_type: 'trial')
            u.account.subscription.stubs(plan_type: 3)
            u.translation_locale.stubs(id: 4)
            u.stubs(account_id: 5)
          end
        end

        describe "#matches_user_and_interface?" do
          it "returns true" do
            assert ipm.matches_user_and_interface?(user, 'lotus')
          end

          describe "when account id is in the exclude set" do
            it "returns false" do
              ipm.expects(:account_excluded?).returns(true)
              refute ipm.matches_user_and_interface?(user, 'lotus')
            end
          end

          describe "when account interface does not match" do
            it "returns false" do
              refute ipm.matches_user_and_interface?(user, 'classic')
            end
          end

          describe "when account interface is not set" do
            it "returns true" do
              assert ipm.matches_user_and_interface?(user, nil)
            end
          end
        end
      end

      describe "when constraint set does not match configuration" do
        let(:user) do
          users(:minimum_agent).tap do |u|
            u.account.stubs(shard_id: 2)
            u.stubs(role: 'Owner', is_account_owner?: true)
            u.account.subscription.stubs(account_type: 'sponsored')
            u.account.subscription.stubs(plan_type: 3)
            u.translation_locale.stubs(id: 4)
            u.stubs(account_id: 5)
          end
        end

        describe "#matches_user_and_interface?" do
          it "returns false" do
            refute ipm.matches_user_and_interface?(user, 'lotus')
          end

          describe "when account id is not in the exclude set" do
            it "returns false" do
              ipm.expects(:account_excluded?).returns(false)
              refute ipm.matches_user_and_interface?(user, 'lotus')
            end
          end
        end
      end
    end
  end
end
