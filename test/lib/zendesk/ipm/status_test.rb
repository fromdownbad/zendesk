require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 5

describe 'Zendesk::Ipm::Status' do
  describe "an IPM notification model" do
    let(:model_class) do
      Class.new do
        extend ActiveModel::Callbacks
        include ActiveModel::Validations
        define_model_callbacks :save

        extend ActiveRecord::Scoping::Named::ClassMethods
        def self.logger;
          Rails.logger;
        end

        def self.dangerous_class_method?(*_args);
          false;
        end

        def self.attr_accessible(*_args) end

        include Zendesk::Ipm::Status

        def save
          run_callbacks :save
        end

        attr_accessor :enabled, :state, :start_date, :end_date

        def initialize(attributes = {})
          attributes.each do |name, value|
            send("#{name}=", value)
          end
        end

        def [](name)
          instance_variable_get(:"@#{name}")
        end

        def []=(name, value)
          instance_variable_set(:"@#{name}", value)
        end
      end
    end

    let(:model) do
      model_class.new
    end

    describe "#enabled?" do
      describe "when the state value is undefined" do
        let(:model) { model_class.new(state: nil) }

        describe "and it was set to true" do
          before do
            model.enabled = true
            model.save
          end

          it "retains that value" do
            assert(model.enabled?)
          end
        end

        describe "and it was set to false" do
          before do
            model.enabled = false
            model.save
          end

          it "retains that value" do
            assert_equal false, model.enabled?
          end
        end
      end
    end

    describe "#active?" do
      describe "when live" do
        it "returns true" do
          model.state = Zendesk::Ipm::Status::LIVE
          assert model.active?
        end
      end

      describe "when scheduled" do
        describe "and current date is within start and end date" do
          it "returns true" do
            model.state      = Zendesk::Ipm::Status::SCHEDULED
            model.start_date = Time.now - 1.day
            model.end_date   = Time.now + 1.day
            assert model.active?
          end
        end

        describe "and current date is not within start and end date" do
          it "returns false" do
            model.state      = Zendesk::Ipm::Status::SCHEDULED
            model.start_date = Time.now + 1.day
            model.end_date   = Time.now + 2.day
            refute model.active?
          end
        end
      end

      describe "when status is disabled" do
        it "returns false" do
          model.state = Zendesk::Ipm::Status::DISABLED
          refute model.active?
        end
      end

      describe "when neither live nor scheduled" do
        it "returns false (out of schedule)" do
          model.state = :unknown
          refute model.active?
        end
      end
    end

    describe "#status" do
      describe "when state is disabled" do
        it "returns :disabled" do
          model.state = Zendesk::Ipm::Status::DISABLED
          assert_equal :disabled, model.status
        end
      end

      describe "when state is live" do
        it "returns :live" do
          model.state = Zendesk::Ipm::Status::LIVE
          assert_equal :live, model.status
        end
      end

      describe "when state is scheduled" do
        before do
          model.state = Zendesk::Ipm::Status::SCHEDULED
        end

        describe "with only the start date defined" do
          before do
            model.start_date = Time.now - 1.day
            model.end_date   = nil
          end

          describe "when current date is on or after the start date" do
            it "returns :scheduled_active" do
              assert_equal :scheduled_active, model.status
            end
          end

          describe "when current date is before the start date" do
            it "returns :scheduled_inactive" do
              Time.stubs(:now).returns(model.start_date - 1.day)
              assert_equal :scheduled_inactive, model.status
            end
          end
        end

        describe "with only the end date defined" do
          before do
            model.start_date = nil
            model.end_date   = Time.now + 1.day
          end

          describe "when current date is on or before the end date" do
            it "returns :scheduled_active" do
              assert_equal :scheduled_active, model.status
            end
          end

          describe "when current date is after the end date" do
            it "returns :scheduled_inactive" do
              Time.stubs(:now).returns(model.end_date + 1.day)
              assert_equal :scheduled_inactive, model.status
            end
          end
        end

        describe "with no start or end date defined" do
          before do
            model.start_date = nil
            model.end_date   = nil
          end

          it "returns :scheduled_active" do
            assert_equal :scheduled_inactive, model.status
          end
        end

        describe "with start and end dates defined" do
          before do
            model.start_date = Time.now - 1.day
            model.end_date   = Time.now + 1.day
          end

          describe "when current date is within start and end dates" do
            it "returns :scheduled_active" do
              assert_equal :scheduled_active, model.status
            end
          end

          describe "when current date is not within start and end dates" do
            it "returns :scheduled_inactive" do
              Time.stubs(:now).returns(model.start_date - 1.day)
              assert_equal :scheduled_inactive, model.status
            end
          end
        end
      end
    end
  end
end
