require_relative "../../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::Ipm::Category' do
  describe "an IPM notification model" do
    let(:model_class) do
      Class.new do
        extend ActiveModel::Naming
        include ActiveModel::Validations

        def self.attr_accessible(*_args) end

        include Zendesk::Ipm::Category

        def initialize
          @errors = ActiveModel::Errors.new(self)
        end

        attr_accessor :category_code
        attr_reader   :errors

        # The following methods are needed to be minimally implemented

        def read_attribute_for_validation(attr)
          send(attr)
        end

        def self.human_attribute_name(attr, _options = {})
          attr
        end

        def self.lookup_ancestors
          [self]
        end

        def self.model_name
          ActiveModel::Name.new(self, nil, self.class.name)
        end
      end
    end

    let(:model) do
      model_class.new
    end

    describe "on validation" do
      describe "when category_code is nil" do
        let(:model) { model_class.new }

        before do
          model.valid?
        end

        it "has errors indicating the issue" do
          assert model.errors.full_messages.any? { |error|
            error == "category_code can't be blank"
          }
        end
      end

      describe "when category_code is not numeric" do
        let(:model) do
          model_class.new.tap { |m| m.category_code = :not_numeric }
        end

        before do
          model.valid?
        end

        it "has error indicating the issue" do
          assert model.errors.full_messages.any? { |error|
            error == 'category_code is not a number'
          }
        end
      end

      describe "when category_code is numeric" do
        describe "and value less than minimum range" do
          let(:model) do
            model_class.new.tap do |m|
              m.category_code = Zendesk::Ipm::Category::CATEGORIES.first.last - 1
            end
          end

          before do
            model.valid?
          end

          it "has error indicating the issue" do
            assert model.errors.full_messages.any? { |error|
              error == 'category_code must be greater than or equal to 1'
            }
          end
        end

        describe "and value is greater than maximum range" do
          let(:model) do
            model_class.new.tap do |m|
              m.category_code = Zendesk::Ipm::Category::CATEGORIES.last.last + 1
            end
          end

          before do
            model.valid?
          end

          it "has error indicating the issue" do
            assert model.errors.full_messages.any? { |error|
              error == 'category_code must be less than or equal to 5'
            }
          end
        end

        describe "and value is within range" do
          let(:model) do
            model_class.new.tap do |m|
              m.category_code = Zendesk::Ipm::Category::CATEGORIES.last.last
            end
          end

          before do
            model.valid?
          end

          it "does not have error indicating the issue" do
            assert_empty(model.errors)
          end
        end
      end
    end
  end
end
