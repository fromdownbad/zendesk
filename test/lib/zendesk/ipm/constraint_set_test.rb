require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe 'Zendesk::Ipm::ConstraintSet' do
  describe "initialization" do
    it "allows empty initialization" do
      constraint_set = Zendesk::Ipm::ConstraintSet.new({})

      assert_nil constraint_set.pods
      assert_nil constraint_set.shards
      assert_nil constraint_set.plans
      assert_nil constraint_set.roles
      assert_nil constraint_set.account_types
      assert_nil constraint_set.interfaces
    end

    it "allows initialization with string or symbol keys" do
      constraint_set = Zendesk::Ipm::ConstraintSet.new(
        :pods    => [1],
        'shards' => [2]
      )

      assert_equal [1], constraint_set.pods
      assert_equal [2], constraint_set.shards
    end

    it "sets known constraints" do
      hash = {
        pods:          [1],
        shards:        [1],
        plans:         [1],
        roles:         ['Admin'],
        account_types: ['trial'],
        interfaces:    ['lotus']
      }
      constraint_set = Zendesk::Ipm::ConstraintSet.new(hash)

      assert_equal [1], constraint_set.pods
      assert_equal [1], constraint_set.shards
      assert_equal [1], constraint_set.plans
      assert_equal ['Admin'], constraint_set.roles
      assert_equal ['trial'], constraint_set.account_types
      assert_equal ['lotus'], constraint_set.interfaces
    end

    it "ignores unknown constraint keys" do
      hash = {
        unknown: nil,
      }
      Zendesk::Ipm::ConstraintSet.new(hash) # no error
    end

    it "errors on invalid constraint values" do
      invalid_constraints = [
        {pods:          ['not an integer']},
        {shards:        ['not an integer']},
        {plans:         ['not an integer']},
        {roles:         ['unknown role']},
        {account_types: ['unknown type']},
        {interfaces:    ['unknown interface']}
      ]

      expected_error_type = Zendesk::Ipm::ConstraintSet::InvalidConstraintValue

      invalid_constraints.each do |hash|
        assert_raises expected_error_type do
          Zendesk::Ipm::ConstraintSet.new(hash)
        end
      end
    end
  end

  describe "serialization" do
    it "dumps only non-nil constraints to JSON" do
      constraint_set = Zendesk::Ipm::ConstraintSet.new(pods: [1], shards: [2])
      dump = Zendesk::Ipm::ConstraintSet.dump constraint_set
      assert_equal JSON.dump(pods: [1], shards: [2]), dump
    end

    it "loads from JSON" do
      dump = JSON.dump(pods: [1], shards: [2])
      constraint_set = Zendesk::Ipm::ConstraintSet.load dump

      assert_equal [1], constraint_set.pods
      assert_equal [2], constraint_set.shards

      assert_nil constraint_set.plans
      assert_nil constraint_set.roles
      assert_nil constraint_set.account_types
      assert_nil constraint_set.interfaces
    end
  end
end
