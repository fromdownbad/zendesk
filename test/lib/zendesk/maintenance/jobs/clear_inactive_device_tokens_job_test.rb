require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::ClearInactiveDeviceTokensJob do
  describe ".execute" do
    describe "on successful authorization" do
      before do
        config = Zendesk::Configuration.dig!(:urban_airship, :"com.zendesk.agent")
        PushNotifications::UrbanAirshipClient.expects(:new).with(config['app_key'], config['master_secret']).returns(
          mock(tokens_deactivated_since: [{"device_token" => "token for agent"}])
        )

        config = Zendesk::Configuration.dig!(:urban_airship, :"com.zendesk.inbox")
        PushNotifications::UrbanAirshipClient.expects(:new).with(config['app_key'], config['master_secret']).returns(
          mock(tokens_deactivated_since: [{"device_token" => "token for inbox"}])
        )

        PushNotifications::DeviceIdentifier.expects(:delete_all).with(token: ["token for agent"])
        PushNotifications::DeviceIdentifier.expects(:delete_all).with(token: ["token for inbox"])
      end

      it "fetches inactive device tokens from Urban Airship and delete them" do
        Zendesk::Maintenance::Jobs::ClearInactiveDeviceTokensJob.execute
      end
    end

    describe "on unsuccessful authorization" do
      let(:statsd_client) { stub_for_statsd }

      before do
        httparty_req = HTTParty::Request.new(Net::HTTP::Get, 'some_url')
        nethttp_resp = Net::HTTPResponse.new(1.0, 401, "Unauthorized")
        response = HTTParty::Response.new(
          httparty_req, nethttp_resp,
          lambda { {"ok" => false, "error" => "Unauthorized", "error_code" => 401} },
          body: ""
        )

        config = Zendesk::Configuration.dig!(:urban_airship, :"com.zendesk.agent")
        PushNotifications::UrbanAirshipClient.expects(:new).with(
          config['app_key'], config['master_secret']
        ).returns(
          mock(tokens_deactivated_since: response)
        )
      end

      it "increase Datadog counter if authorization fails" do
        Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
        statsd_client.expects(:increment).with(
          "failure", tags: ["exception:mobile_sdk.urban_airship.unauthorized"]
        ).once
        Zendesk::Maintenance::Jobs::ClearInactiveDeviceTokensJob.execute
      end
    end
  end
end
