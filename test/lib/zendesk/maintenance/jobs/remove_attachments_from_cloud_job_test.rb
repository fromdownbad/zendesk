require_relative '../../../../support/job_helper'
require_relative "../../../../support/attachment_test_helper"

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::RemoveAttachmentsFromCloudJob do
  include DataDeletionJobSupport

  fixtures :accounts, :users, :subscriptions

  let(:account) { accounts(:data_deletion_test_acct) }
  let(:attachment_count) { 10 }
  let(:job) { create_job(account, Zendesk::Maintenance::Jobs::RemoveAttachmentsFromCloudJob) }

  def create_attachment
    attachment = Attachment.new(
      uploaded_data: Zendesk::Attachments::CgiFileWrapper.new("#{Rails.root}/test/files/somefile.txt")
    )
    attachment.author = account.users.last
    attachment.account = account
    attachment.created_at = Time.now - 1.day
    attachment.save!
    attachment
  end

  before do
    stub_request(:put, %r{\.s3\.us-west-2\.amazonaws\.com/data/attachments/\d+/(\w|\.)+}).
      to_return(status: 200).
      times(attachment_count)

    attachment_count.times do
      attachment = create_attachment
      attachment.stores += [:s3]
      attachment.save!
    end
  end

  describe "reason = moved" do
    it "noops" do
      job.account.update_attribute(:shard_id, 2)
      job.job_audit.audit.reason = 'moved'
      job.job_audit.shard_id = 1

      job.expects(:erase_for_moved) # erase_for_moved is not doing anything ... but that is hard to test ...
      job.work
    end
  end

  it "does not delete during dry run" do
    job = create_job(account, Zendesk::Maintenance::Jobs::RemoveAttachmentsFromCloudJob, dry_run: true)
    Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.expects(:destroy_file).never
    job.work
  end

  it "deletes cancelled" do
    Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.expects(:destroy_file).times(attachment_count)
    job.send(:erase_for_canceled)
  end

  it "is re-entrant" do
    Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.expects(:destroy_file).times(attachment_count)
    job.send(:erase_for_canceled)
    Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.expects(:destroy_file).times(0)
    job.send(:erase_for_canceled)
  end

  it "ignores unknown attribute" do
    attachment = Attachment.where(account_id: account.id).first
    attachment.update_column("stores", "fs,xfiles")

    Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.expects(:destroy_file).times(attachment_count - 1)

    job.work
  end

  describe 'when errors encountered' do
    it "raises if cannot detroy the blob" do
      except = Aws::S3::Errors::InternalError.new(500, "Something bad happened")
      Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.expects(:destroy_file).raises(except)
      Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.stubs(:exist?).returns(true)
      Rails.logger.expects(:error).once
      assert_raises(Aws::S3::Errors::InternalError) { job.send :erase_for_canceled }
    end

    it "ignores if blob does not exist" do
      except = Aws::S3::Errors::InternalError.new(500, "Something bad happened")
      Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.expects(:destroy_file).times(attachment_count).raises(except)
      Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.stubs(:exist?).returns(false)
      job.send :erase_for_canceled
    end
  end

  describe '#verify_for_canceled' do
    it "verifies attachments removed" do
      Technoweenie::AttachmentFu::Backends::FileSystemBackend.any_instance.expects(:exist?).times(attachment_count).returns(false)
      Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.expects(:exist?).times(attachment_count).returns(false)

      job.send(:verify_for_canceled)
    end

    it "fails if attachment does exist" do
      Technoweenie::AttachmentFu::Backends::FileSystemBackend.any_instance.expects(:exist?).returns(false)
      Technoweenie::AttachmentFu::Backends::S3Backend.any_instance.expects(:exist?).returns(true)

      assert_raises RuntimeError do
        job.send(:verify_for_canceled)
      end
    end

    it "does not raise error for unknown store" do
      attachment = Attachment.where(account_id: account.id).first
      attachment.update_column("stores", "xfiles")

      Technoweenie::AttachmentFu::Backends::FileSystemBackend.
        any_instance.expects(:exist?).times(attachment_count - 1).returns(false)
      Technoweenie::AttachmentFu::Backends::S3Backend.
        any_instance.expects(:exist?).times(attachment_count - 1).returns(false)

      job.send(:verify_for_canceled)
    end
  end

  describe "#cloud_stored_classes" do
    it "lists all base attachent classes" do
      Dir['app/mode../../../../*.rb'].each do |f|
        require f.sub('app/models/', '').sub('.rb', '')
      end
      listed = Zendesk::Maintenance::Jobs::RemoveAttachmentsFromCloudJob.new(1).send(:cloud_stored_classes)
      actual = ActiveRecord::Base.descendants.select do |r|
        r < Technoweenie::AttachmentFu::InstanceMethods &&
          !(r.superclass < Technoweenie::AttachmentFu::InstanceMethods)
      end
      listed.map(&:name).sort.must_equal actual.map(&:name).sort
    end
  end
end
