require_relative "../../../../support/test_helper"
# Tests covered by Zendesk::Maintenance::Jobs::TicketDeletionJob.

SingleCov.not_covered!

# Just check that it executes
describe Zendesk::Maintenance::Jobs::TicketDeletionLowPriorityJob do
  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:tickets) do
    ticket = account.tickets.find_by_nice_id(1)
    ticket.will_be_saved_by(users(:minimum_agent))
    ticket.soft_delete!
    [ticket]
  end

  before do
    Account.any_instance.stubs(:has_ticket_scrubbing?).returns(true)
    Account.any_instance.stubs(:has_prevent_deletion_if_churned?).returns(false)
  end

  it 'performs' do
    job = Zendesk::Maintenance::Jobs::TicketDeletionLowPriorityJob.new(
      '12345', account_id: account.id, user_id: User.system.id,
               tickets_ids: tickets
    )

    job.perform
  end
end
