require_relative '../../../../support/job_helper'
require_relative '../../../../support/zopim_test_helper'
require_relative '../../../../support/chat_phase_3_helper'
require_relative '../../../../support/entitlement_test_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::ChatPhaseMismatchJob do
  include ZopimTestHelper
  include ChatPhase3Helper

  let(:job) { Zendesk::Maintenance::Jobs::ChatPhaseMismatchJob }
  let(:account_chat_ok) { accounts(:multiproduct) }
  let(:account_chat_broken) { accounts(:minimum) }
  let(:account_chat_trial) { accounts(:with_trial_zopim_subscription) }
  let(:current_chat_product) { stub(attributes: FactoryBot.build(:chat_trial_product)) }

  before do
    Arturo.enable_feature!(:chat_phase3_integration_mismatch_job)
  end

  describe '#work' do
    it 'calls it at least once' do
      Zendesk::Maintenance::Jobs::ChatPhaseMismatchJob.expects(:find_mismatched_zopim_integrations).at_least_once
      job.work
    end

    describe 'with arturo off' do
      before do
        Arturo.disable_feature!(:chat_phase3_integration_mismatch_job)
      end

      it 'never calls it' do
        Zendesk::Maintenance::Jobs::ChatPhaseMismatchJob.expects(:find_mismatched_zopim_integrations).never
        job.work
      end
    end

    describe 'with chat accounts' do
      before do
        create_zopim_integration(account_chat_ok, 3)
        create_zopim_integration(account_chat_broken, 2)
        create_zopim_integration(account_chat_trial, -1)
        Zendesk::Accounts::Client.any_instance.stubs(chat_product: current_chat_product)
      end

      describe 'with missing zopim integration' do
        let(:product_params) do
          [{'id' => 1,  'account_id' => account_chat_ok.id,     'name' => 'chat', 'active' => true, 'state' => 'trial', 'plan_settings' => { 'plan_type' => 0, 'max_agents' => 100, 'phase' => 3 }},
           {'id' => 8,  'account_id' => account_chat_trial.id,  'name' => 'chat', 'active' => true, 'state' => 'trial', 'plan_settings' => { 'plan_type' => 0, 'max_agents' => 100, 'phase' => -1 }},
           {'id' => 13, 'account_id' => account_chat_broken.id, 'name' => 'chat', 'active' => true, 'state' => 'trial', 'plan_settings' => { 'plan_type' => 0, 'max_agents' => 100, 'phase' => 3 }}]
        end
        let(:chat_products) { product_params.map { |p| Zendesk::Accounts::Product.new(p) } }
        before do
          Zendesk::Maintenance::Jobs::ChatPhaseMismatchJob.stubs(:chat_products).with(0).returns(chat_products)
          Zendesk::Maintenance::Jobs::ChatPhaseMismatchJob.stubs(:chat_products).with(13).returns([])
        end

        it 'finds it' do
          Zendesk::Maintenance::Jobs::ChatPhaseMismatchJob.expects(:log_errors).with([90538], 'zopim_integration.mismatch', anything)
          job.work
        end

        describe 'with bad queries' do
          before do
            ZopimIntegration.stubs(:where).returns(nil)
          end

          it 'catches errors' do
            Rails.logger.expects(:error).at_least_once
            job.work
          end
        end
      end
    end
  end
end
