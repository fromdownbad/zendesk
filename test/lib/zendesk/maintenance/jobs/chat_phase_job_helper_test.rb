require_relative '../../../../support/job_helper'

# the one uncovered line is chat_products since this is an external services call
SingleCov.covered! uncovered: 1

describe Zendesk::Maintenance::Jobs::ChatPhaseJobHelper do
  let (:chat_helper) { Object.new.extend(Zendesk::Maintenance::Jobs::ChatPhaseJobHelper) }

  describe 'chat_job_enabled?' do
    it 'returns true when phase is on' do
      Arturo::Feature.create!(symbol: :chat_phase3_verification_job, deployment_percentage: 50, phase: 'on')
      assert chat_helper.chat_job_enabled?(:chat_phase3_verification_job)
    end

    it 'returns true when deployment percentage is 100' do
      Arturo::Feature.create!(symbol: :chat_phase3_verification_job, deployment_percentage: 100, phase: 'off')
      assert chat_helper.chat_job_enabled?(:chat_phase3_verification_job)
    end
  end

  describe 'log_errors' do
    before do
      ZendeskExceptions::Logger.expects(:record).with(
        anything,
        location: chat_helper,
        message: 'exception-message: [1234]',
        fingerprint: '515506b150408358ef5f9836fe9a05b8a5900f42'
      )
      Zendesk::StatsD::Client.any_instance.expects(:gauge).with('chat.statsd', 1)
    end

    it 'calls statsd' do
      chat_helper.log_errors([1234], 'chat.statsd', 'exception-message')
    end
  end

  describe 'validate_with_block' do
    let(:params_first) { [{'id' => 1, 'account_id' => 1, 'name' => 'chat', 'active' => true, 'state' => 'trial', 'plan_settings' => { 'plan_type' => 0, 'max_agents' => 100, 'phase' => 3 }}] }
    let(:params_second) { [{'id' => 13, 'account_id' => 2, 'name' => 'chat', 'active' => true, 'state' => 'trial', 'plan_settings' => { 'plan_type' => 0, 'max_agents' => 100, 'phase' => -1 }}] }
    let(:products_first) { params_first.map { |p| Zendesk::Accounts::Product.new(p) } }
    let(:products_second) { params_second.map { |p| Zendesk::Accounts::Product.new(p) } }

    before do
      chat_helper.stubs(:chat_products).with(0).returns(products_first)
      chat_helper.stubs(:chat_products).with(1).returns(products_second)
      chat_helper.stubs(:chat_products).with(13).returns([])
    end

    it 'calls the block and counts phase 3 iterations' do
      block_iterations = 0
      chat_helper.validate_with_block do |products|
        block_iterations += 1 if products.first.class == Zendesk::Accounts::Product
      end
      assert_equal 1, block_iterations
    end
  end
end
