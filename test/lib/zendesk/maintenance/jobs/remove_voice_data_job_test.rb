require_relative "../../../../support/job_helper"

SingleCov.covered!

describe 'RemoveVoiceDataJob' do
  include DataDeletionJobSupport

  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:job) { create_job(account, Zendesk::Maintenance::Jobs::RemoveVoiceDataJob) }

  describe_with_arturo_enabled :voice_data_deletion do
    it 'sends request to voice when account is canceled and voice sub account is not nil' do
      FactoryBot.create(:voice_sub_account, account: account)
      Zendesk::Voice::InternalApiClient.any_instance.expects(:account_deleted!).once
      job.work
    end

    it 'does not send request to voice if voice sub account is nil' do
      Zendesk::Voice::InternalApiClient.any_instance.expects(:account_deleted!).never
      job.work
    end
  end

  it 'does not send request to voice if arturo is not enabled' do
    Zendesk::Voice::InternalApiClient.any_instance.expects(:account_deleted!).never
    job.work
  end
end
