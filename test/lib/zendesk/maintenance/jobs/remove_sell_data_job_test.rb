require_relative "../../../../support/job_helper"

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::RemoveSellDataJob do
  include DataDeletionJobSupport

  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:job) { create_job(account, Zendesk::Maintenance::Jobs::RemoveSellDataJob) }
  let(:audit_id) { job.audit.id }

  let(:delete_response) do
    OpenStruct.new(status: 200, body: { 'done' => false, 'backoff' => 0.2 }, headers: { })
  end

  let(:delete_response_last_page) do
    OpenStruct.new(status: 200, body: { 'done' => true, 'backoff' => 0 }, headers: { })
  end

  let!(:sell_internal_api_client) { Zendesk::Sell::InternalApiClient.new('minimum', timeout: 30.seconds) }
  let!(:statsd_client_data_deletion) { Zendesk::StatsD::Client.new(namespace: 'data_deletion', tags: %w[reason:canceled]) }
  let!(:statsd_client) { Zendesk::StatsD::Client.new(namespace: %w[account_data_deletion remove_sell_data_job], tags: %w[reason:canceled]) }

  before do
    Arturo.enable_feature!(:sell_data_deletion)

    Zendesk::Sell::InternalApiClient.stubs(:new).with('minimum', timeout: 30.seconds).returns(sell_internal_api_client)
    Zendesk::StatsD::Client.stubs(:new).with(namespace: 'data_deletion', tags: %w[reason:canceled]).returns(statsd_client_data_deletion)
    Zendesk::StatsD::Client.stubs(:new).with(namespace: %w[account_data_deletion remove_sell_data_job], tags: %w[reason:canceled]).returns(statsd_client)
  end

  describe 'account canceled' do
    before do
      job.job_audit.stubs(:shard_id).returns(1)
      job.job_audit.audit.stubs(:reason).returns('canceled')
    end

    it 'sends a request to sell to delete the data associated with an account' do
      sell_internal_api_client.expects(:delete_account).twice.returns(delete_response, delete_response_last_page)
      job.work
    end

    it 'increments started, completed metrics in datadog' do
      sell_internal_api_client.expects(:delete_account).twice.returns(delete_response, delete_response_last_page)
      statsd_client.expects(:increment).with('started')
      statsd_client.expects(:increment).with('completed')

      job.work
    end

    it 'increments backoff metric in datadog' do
      sell_internal_api_client.expects(:delete_account).twice.returns(delete_response, delete_response_last_page)
      statsd_client.expects(:gauge).with('backoff_duration', 0.2)

      job.work
    end

    it 'does not raise an error if feature toggle is off' do
      Arturo.disable_feature!(:sell_data_deletion)
      sell_internal_api_client.expects(:delete_account).never

      job.work
    end
  end

  describe 'dry_run' do
    it 'should never ask sell to delete an account' do
      job = create_job(account, Zendesk::Maintenance::Jobs::RemoveSellDataJob, dry_run: true)
      sell_internal_api_client.expects(:delete_account).with(account.id).never
      job.work
    end
  end

  describe 'kill switch enabled' do
    it 'raises' do
      Arturo.enable_feature! :account_deletion_kill_switch
      assert_raises Zendesk::DataDeletion::AccountDeletionKillSwitch::KillSwitchEnabled do
        job.send :erase_for_canceled
      end
    end
  end
end
