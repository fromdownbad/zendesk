require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::ComplianceUserDeletionRequeueJob do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  describe ".work_on_shard" do
    before do
      Zendesk::StatsD::Client.any_instance.expects(:distribution).with('requests_incomplete', anything, tags: ["shard_id:#{account.shard_id}"]).once
      Zendesk::StatsD::Client.any_instance.expects(:distribution).with('requests_overdue', anything, anything).once
      ZendeskExceptions::Logger.expects(:record).never
    end

    it "should process_queues" do
      Zendesk::Maintenance::Jobs::ComplianceUserDeletionRequeueJob.work_on_shard(account.shard_id)
    end
  end
end
