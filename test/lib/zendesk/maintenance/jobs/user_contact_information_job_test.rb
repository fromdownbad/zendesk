require_relative "../../../../support/job_helper"

SingleCov.covered!

describe 'UserContactInformationJob' do
  fixtures :accounts, :users, :user_identities

  describe ".work" do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_admin)
      @user_identity = @user.identities.first
      @job = Zendesk::Maintenance::Jobs::UserContactInformationJob
    end

    it "inserts data in table" do
      before = UserContactInformation.count(:all)
      @job.work
      after = UserContactInformation.count(:all)
      assert_operator before, :<, after
    end

    it "creates records for all eligible accounts" do
      @job.work
      expected = @account.users.last.name
      actual = UserContactInformation.find_by_name(expected)
      assert_not_nil actual.name
      assert_equal expected, actual.name
    end

    it "updates records for all eligible accounts " do
      row = UserContactInformation.create! do |new_contact_info|
        new_contact_info.account = @account
        new_contact_info.user_id = @user.id
        new_contact_info.email = @user_identity
        new_contact_info.name = "nametest"
        new_contact_info.phone = nil
        new_contact_info.role = @user.roles
        new_contact_info.is_email_verified = true
      end
      UserContactInformation.stubs(:get_user_contact_information_id).returns(row.id) # need this otherwise it just inserts
      @job.work
      actual = UserContactInformation.find_by_name("nametest")
      assert_nil actual
    end

    it "Nots update when the last_updated date is latest" do
      row = UserContactInformation.create! do |new_contact_info|
        new_contact_info.account = @account
        new_contact_info.user_id = @user.id
        new_contact_info.email = @user_identity
        new_contact_info.name = @user.name
        new_contact_info.phone = nil
        new_contact_info.role = @user.roles
        new_contact_info.is_email_verified = true
      end
      UserContactInformation.stubs(:get_user_contact_information_id).returns(row.id)

      @job.work
      assert_nil UserContactInformation.last.phone
    end

    it "Deletes a user from the table if that user is downgraded from an admin role" do
      admin = users(:minimum_admin_not_owner)
      @job.work_on_shard(@account.shard_id)
      admin.roles = 0
      admin.save!
      @job.work_on_shard(@account.shard_id)
      assert_empty UserContactInformation.where(account_id: @account.id, user_id: admin.id)
    end
  end

  describe "#update_user_contact_information " do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_admin)
      @user_identity = @user.identities.first
      @job = Zendesk::Maintenance::Jobs::UserContactInformationJob
    end

    it "inserts row in table" do
      rows = User.includes(:identities).where(users: {roles: 4, is_active: 1, account_id: @account.id}).limit(1)

      result = @job.update_user_contact_information(rows)
      expected = result.last.email
      actual = UserContactInformation.last.email
      assert_not_nil actual
      assert_equal expected, actual
    end

    it "inserts rows" do
      rows = User.includes(:identities).where(users: {roles: 4, is_active: 1, account_id: 1}).limit(3)

      result = @job.update_user_contact_information(rows)
      expected = result.last.email
      actual = UserContactInformation.last.email
      assert_not_nil actual
      assert_equal expected, actual
    end

    describe ".Data Update" do
      before do
        inserted_data = UserContactInformation.create! do |new_contact_info|
          new_contact_info.account = @account
          new_contact_info.user_id = @user.id
          new_contact_info.email = @user_identity
          new_contact_info.name = @user.name
          new_contact_info.phone = nil
          new_contact_info.role = @user.roles
          new_contact_info.is_email_verified = true
        end
        UserContactInformation.stubs(:get_user_contact_information_id).returns(inserted_data.id)
        @rows = User.includes(:identities).where(users: {roles: 4, is_active: 1, account_id: @account.id})
      end

      it "updates row in table" do
        rows = @rows.where('users.updated_at > ?', Time.now.utc - 20.days).limit(1)
        result = @job.update_user_contact_information(rows)
        actual = UserContactInformation.last.phone
        assert_not_nil result
        assert_equal "+15554398790", actual
      end

      it "dos not update row when date last updated is in future" do
        rows = @rows.where('users.updated_at > ?', Time.now.utc + 2.years).limit(1)
        result = @job.update_user_contact_information(rows)
        actual = UserContactInformation.last.phone
        assert_equal 0, result.count
        assert_nil actual
      end
    end
  end
end
