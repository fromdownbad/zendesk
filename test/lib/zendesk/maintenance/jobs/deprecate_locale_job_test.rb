require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Zendesk::Maintenance::Jobs::DeprecateLocaleJob do
  fixtures :all

  let(:spanish) { translation_locales(:spanish) }
  let(:japanese) { translation_locales(:japanese) }
  let(:french) { translation_locales(:french) }
  let(:account) { accounts(:minimum) }

  describe "#deprecate_locale" do
    describe "for a single account" do
      it "should directly call work_on_account" do
        Zendesk::Maintenance::Jobs::DeprecateLocaleJob.any_instance.expects(:work_on_all).never
        Zendesk::Maintenance::Jobs::DeprecateLocaleJob.any_instance.expects(:work_on_account).once

        Zendesk::Maintenance::Jobs::DeprecateLocaleJob.work_on_shard(account.shard_id, spanish.id, japanese.id, true, account.id)
      end
    end

    describe "with no account id provided" do
      before do
        Account.all.each do |a|
          a.update_column(:locale_id, spanish.id)
        end
      end

      it "should call work_on_all" do
        Zendesk::Maintenance::Jobs::DeprecateLocaleJob.any_instance.expects(:work_on_all).once

        Zendesk::Maintenance::Jobs::DeprecateLocaleJob.work_on_shard(account.shard_id, spanish.id, japanese.id, true)
      end
    end
  end

  describe "deprecate methods" do
    describe "dry run" do
      let(:deprecate_locale) { Zendesk::Maintenance::Jobs::DeprecateLocaleJob.new(spanish.id, japanese.id, true) }

      describe "#replace_in_allowed_locales" do
        before do
          Account.any_instance.stubs(:allowed_translation_locales).returns([spanish])
        end

        it "should not modify the allowed locales list" do
          deprecate_locale.replace_in_allowed_locales(account)
          account.reload

          assert_includes account.allowed_translation_locales, spanish
          refute_includes account.allowed_translation_locales, japanese
        end
      end

      describe "#replace_in_users" do
        before do
          user = account.users.last
          user.translation_locale = spanish
          user.save!
        end

        it "should not replace in the users" do
          user = account.users.last
          user.translation_locale = spanish
          user.save!

          assert_equal spanish, account.users.last.translation_locale

          deprecate_locale.replace_in_users(account)

          assert_equal spanish, account.users.find(user.id).translation_locale
        end
      end

      describe "#replace_in_tickets" do
        let(:ticket) { account.tickets.first }
        before { ticket.update_column(:locale_id, spanish.id) }

        it "should not replace in the ticket" do
          deprecate_locale.replace_in_tickets(account)
          ticket.reload.read_attribute(:locale_id).must_equal spanish.id
        end
      end

      describe "#replace_in_cms_variants" do
        before do
          fallback = {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: spanish.id}
          Cms::Text.create!(name: "Welcome Wombat", fallback_attributes: fallback, account: account)
        end

        it "should not replace in the cms variant" do
          deprecate_locale.replace_in_cms_variants(account)
          account.cms_variants.reload

          assert_equal spanish, account.cms_variants.last.translation_locale
        end
      end

      describe "#replace_in_forums" do
        before do
          forum = account.forums.first
          forum.translation_locale = spanish
          forum.save!
        end

        it "should not replace the locale in the forum" do
          deprecate_locale.replace_in_forums(account)
          account.forums.reload

          assert_equal spanish, account.forums.first.translation_locale
        end
      end

      describe "#replace_in_nps_recipients" do
        before do
          NpsRecipient.create! do |nr|
            nr.account_id = account.id
            nr.delivery_id = 1
            nr.email = "Test@test.com"
            nr.user_id = 1
            nr.name = "TEST"
            nr.locale_id = spanish.id
          end
        end

        it "should not modify the recipients" do
          deprecate_locale.replace_in_nps_recipients(account)

          assert_equal spanish.id, NpsRecipient.last.locale_id
        end
      end

      describe "#replace_in_rules" do
        before do
          Account.any_instance.stubs(:available_languages).returns([spanish, japanese])
          definition_with_locale = Definition.build(conditions_all: [{operator: "is_not", value: [spanish.id.to_s], source: "locale_id" }])
          @rule = account.rules.where(type: "View").first
          definition = @rule.definition.merge(definition_with_locale)
          @rule.update_column(:definition, definition)
        end

        it "should not modify the rule" do
          deprecate_locale.replace_in_rules(account)
          account.rules.reload

          assert_equal [spanish.id.to_s], account.rules.find(@rule.id).definition.conditions_all.last.value
        end
      end

      describe "#change_definition_item_locale" do
        describe "UserView definitions" do
          before do
            Account.any_instance.stubs(:available_languages).returns([spanish])
          end

          describe "with a locale_id matching the locale to deprecate" do
            before do
              @user_view = rules(:global_user_view)
              @user_view.definition.conditions_all[0] = OpenStruct.new.tap do |c|
                c.source = "locale_id"
                c.operator = "is"
                c.value = spanish.id
              end
              @user_view.save!
              @user_view.reload
            end

            it "should not modify the locale id" do
              deprecate_locale.replace_in_rules(account)
              account.reload

              assert_equal spanish.id, account.rules.find(@user_view.id).definition.conditions_all[0].value
            end
          end

          describe "with a locale_id not present operator" do
            before do
              @user_view = rules(:global_user_view)
              @user_view.definition.conditions_all[0] = OpenStruct.new.tap do |c|
                c.source = "locale_id"
                c.operator = "is_not_present"
                c.value = nil
              end
              @user_view.save!(validate: false)
              @user_view.reload
            end

            it "should not modify the locale id" do
              deprecate_locale.replace_in_rules(account)
              account.reload

              assert_nil account.rules.find(@user_view.id).definition.conditions_all[0].value
            end
          end
        end

        describe "other models inheriting from Rule" do
          before do
            @definition = Definition.build(actions: [
                                             {operator: "less_than",
                                              value: {"0" => "2"},
                                              source: "status_id"},
                                             {operator: "is",
                                              value: {"0" => "current_groups"},
                                              source: "group_id"},
                                             {operator: "is_not",
                                              value: ["12345"],
                                              source: "locale_id"}
                                           ])
            @items = @definition.actions
          end

          it "should skip items with source locale_id but not matching locale_id" do
            deprecate_locale.send(:change_definition_item_locale, @items)
            assert_equal ["12345"], @definition.actions.last.value
          end

          it "should not modify items with matching source" do
            @definition.actions.last.value[0] = spanish.id.to_s

            deprecate_locale.send(:change_definition_item_locale, @items)
            assert_equal [spanish.id.to_s], @definition.actions.last.value
          end
        end
      end

      describe "#change_definition_items" do
        before do
          @definition = Definition.build(
            conditions_all: [
              {operator: "less_than",
               value: {"0" => "2"},
               source: "status_id"}
            ],
            conditions_any: [
              {operator: "is",
               value: {"0" => "current_groups"},
               source: "group_id"}
            ],
            actions: [
              {operator: "is_not",
               value: {"0" => ""},
               source: "group_id"}
            ]
          )
        end

        it "should go through each definition" do
          deprecate_locale.expects(:change_definition_item_locale).at_least(3)
          deprecate_locale.send(:change_definition_items, @definition)
        end
      end

      describe "#replace_in_reports" do
        before do
          Account.any_instance.stubs(:available_languages).returns([spanish, japanese])

          report = {
            title: "stuffs",
            is_relative_interval: "true",
            relative_interval_in_days: "7",
            "from_date(1i)": "2015",
            "from_date(2i)": "1",
            "from_date(3i)": "27",
            "to_date(1i)": "2015",
            "to_date(2i)": "2",
            "to_date(3i)": "27"
          }

          report_sets = {
            1 => {
              legend: "Legend for this data series",
              state: "created_at",
              conditions: {
                1 => {
                  source: "locale_id",
                  operator: "is",
                  value: { 0 => spanish.id.to_s }
                },
                2 => {
                  source: "locale_id",
                  operator: "is_not",
                  value: { 0 => japanese.id.to_s }
                }
              }
            }
          }

          @report = account.reports.build(report)
          @report.author = account.users.first
          @report.sets = report_sets
          @report.save!
        end

        it "should not modify the report" do
          deprecate_locale.replace_in_reports(account)
          account.reload
          report = account.reports.find(@report.id)

          assert_equal spanish.id.to_s, report.definition.first[:data].first[2]
        end

        describe "#change_report_definition" do
          it "should go through each definition" do
            deprecate_locale.expects(:change_report_definition).at_least(1)
            deprecate_locale.send(:change_report_definition, @report.definition)
          end
        end

        describe "#change_report_definition_data" do
          it "should not touch locale_id if the id doesn't match" do
            deprecate_locale.send(:change_report_definition_data, @report.definition.first)

            assert_equal spanish.id.to_s, @report.definition.first[:data].first[2]
            assert_equal japanese.id.to_s, @report.definition.first[:data].last[2]
          end

          it "should not modify the matching locale definition" do
            deprecate_locale.send(:change_report_definition_data, @report.definition.first)

            assert_equal spanish.id.to_s, @report.definition.first[:data].first[2]
            assert_equal japanese.id.to_s, @report.definition.first[:data].last[2]
          end
        end
      end
    end

    describe "NOT dry run" do
      let(:deprecate_locale) { Zendesk::Maintenance::Jobs::DeprecateLocaleJob.new(spanish.id, japanese.id, false) }

      describe "#replace_in_allowed_locales" do
        describe "when the old locale is not enabled on the account" do
          before do
            Account.any_instance.stubs(:allowed_translation_locales).returns([french])
          end

          it "should not enable the new locale" do
            deprecate_locale.replace_in_allowed_locales(account)
            account.reload

            refute_includes account.allowed_translation_locales, spanish
            refute_includes account.allowed_translation_locales, japanese
          end
        end

        describe "when the old locale IS enabled on the account" do
          before do
            Account.any_instance.stubs(:allowed_translation_locales).returns([spanish, japanese])
          end

          it "should modify the allowed locales list" do
            deprecate_locale.replace_in_allowed_locales(account)
            account.reload

            refute_includes account.allowed_translation_locales, spanish
            assert_includes account.allowed_translation_locales, japanese
          end
        end
      end

      describe "#replace_in_users" do
        it "should replace in the users" do
          user = account.users.last
          user.translation_locale = spanish
          user.save!

          assert_equal spanish, account.users.last.translation_locale

          deprecate_locale.replace_in_users(account)

          assert_equal japanese, account.users.find(user.id).translation_locale
        end
      end

      describe "#replace_in_tickets" do
        let(:ticket) { account.tickets.first }

        before do
          ticket.update_column(:locale_id, spanish.id)
        end

        it "replaces in the ticket" do
          deprecate_locale.replace_in_tickets(account)
          assert_equal japanese.id, ticket.reload.read_attribute(:locale_id)
        end
      end

      describe "#replace_in_cms_variants" do
        before do
          fallback = {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: spanish.id}
          Cms::Text.create!(name: "Welcome Wombat", fallback_attributes: fallback, account: account)
        end

        it "should replace in the cms variant" do
          deprecate_locale.replace_in_cms_variants(account)
          account.cms_variants.reload

          assert_equal japanese, account.cms_variants.last.translation_locale
        end
      end

      describe "#replace_in_forums" do
        before do
          forum = account.forums.first
          forum.translation_locale = spanish
          forum.save!
        end

        it "should replace the locale in the forum" do
          deprecate_locale.replace_in_forums(account)
          account.forums.reload

          assert_equal japanese, account.forums.first.translation_locale
        end
      end

      describe "#replace_in_nps_recipients" do
        before do
          NpsRecipient.create! do |nr|
            nr.account_id = account.id
            nr.delivery_id = 1
            nr.email = "Test@test.com"
            nr.user_id = 1
            nr.name = "TEST"
            nr.locale_id = spanish.id
          end
        end

        it "should modify the recipients" do
          deprecate_locale.replace_in_nps_recipients(account)

          assert_equal japanese.id, NpsRecipient.last.locale_id
        end
      end

      describe "#replace_in_rules" do
        before do
          Account.any_instance.stubs(:available_languages).returns([spanish, japanese])
          @rule = account.rules.last
          definition_with_locale = Definition.build(conditions_all: [{operator: "is_not", value: [spanish.id.to_s], source: "locale_id" }])
          @rule.definition = @rule.definition.merge(definition_with_locale)
          @rule.save!
        end

        it "should not modify the rule" do
          deprecate_locale.replace_in_rules(account)
          account.rules.reload

          assert_equal [japanese.id.to_s], account.rules.find(@rule.id).definition.conditions_all.last.value
        end
      end

      describe "#change_definition_item_locale" do
        describe "UserView definitions" do
          before do
            Account.any_instance.stubs(:available_languages).returns([spanish, japanese])
          end

          describe "with a locale_id matching the locale to deprecate" do
            before do
              @user_view = rules(:global_user_view)
              @user_view.definition.conditions_all[0] = OpenStruct.new.tap do |c|
                c.source = "locale_id"
                c.operator = "is"
                c.value = spanish.id
              end
              @user_view.save!
            end

            it "should modify the locale id" do
              deprecate_locale.replace_in_rules(account)
              account.reload

              assert_equal japanese.id, account.rules.find(@user_view.id).definition.conditions_all[0].value
            end
          end

          describe "with a locale_id not present operator" do
            before do
              @user_view = rules(:global_user_view)
              @user_view.definition.conditions_all[0] = OpenStruct.new.tap do |c|
                c.source = "locale_id"
                c.operator = "is_not_present"
                c.value = nil
              end
              @user_view.save!(validate: false)
            end

            it "should not modify the locale id" do
              deprecate_locale.replace_in_rules(account)
              account.reload

              assert_nil account.rules.find(@user_view.id).definition.conditions_all[0].value
            end
          end
        end

        describe "other models inheriting from Rule" do
          before do
            @definition = Definition.build(actions: [
                                             {operator: "less_than",
                                              value: {"0" => "2"},
                                              source: "status_id"},
                                             {operator: "is",
                                              value: {"0" => "current_groups"},
                                              source: "group_id"},
                                             {operator: "is_not",
                                              value: ["12345"],
                                              source: "locale_id"}
                                           ])
            @items = @definition.actions
          end

          it "should skip items with source locale_id but not matching locale_id" do
            deprecate_locale.send(:change_definition_item_locale, @items)
            assert_equal ["12345"], @definition.actions.last.value
          end

          it "should modify items with matching source" do
            @definition.actions.last.value[0] = spanish.id.to_s

            deprecate_locale.send(:change_definition_item_locale, @items)
            assert_equal [japanese.id.to_s], @definition.actions.last.value
          end
        end
      end

      describe "#change_definition_items" do
        before do
          @definition = Definition.build(
            conditions_all: [
              {operator: "less_than",
               value: {"0" => "2"},
               source: "status_id"}
            ],
            conditions_any: [
              {operator: "is",
               value: {"0" => "current_groups"},
               source: "group_id"}
            ],
            actions: [
              {operator: "is_not",
               value: {"0" => ""},
               source: "group_id"}
            ]
          )
        end

        it "should go through each definition" do
          deprecate_locale.expects(:change_definition_item_locale).at_least(3)
          deprecate_locale.send(:change_definition_items, @definition)
        end
      end

      describe "#replace_in_reports" do
        before do
          Account.any_instance.stubs(:available_languages).returns([spanish, japanese])

          report = {
            title: "stuffs",
            is_relative_interval: "true",
            relative_interval_in_days: "7",
            "from_date(1i)": "2015",
            "from_date(2i)": "1",
            "from_date(3i)": "27",
            "to_date(1i)": "2015",
            "to_date(2i)": "2",
            "to_date(3i)": "27"
          }

          report_sets = {
            1 => {
              legend: "Legend for this data series",
              state: "created_at",
              conditions: {
                1 => {
                  source: "locale_id",
                  operator: "is",
                  value: { 0 => spanish.id.to_s }
                },
                2 => {
                  source: "locale_id",
                  operator: "is_not",
                  value: { 0 => japanese.id.to_s }
                }
              }
            }
          }

          @report = account.reports.build(report)
          @report.author = account.users.first
          @report.sets = report_sets
          @report.save!
        end

        it "should modify the report" do
          deprecate_locale.replace_in_reports(account)
          account.reload
          report = account.reports.find(@report.id)

          assert_equal japanese.id.to_s, report.definition.first[:data].first[2]
        end

        describe "#change_report_definition" do
          it "should go through each definition" do
            deprecate_locale.expects(:change_report_definition).at_least(1)
            deprecate_locale.send(:change_report_definition, @report.definition)
          end
        end

        describe "#change_report_definition_data" do
          it "should not touch locale_id if the id doesn't match" do
            deprecate_locale.send(:change_report_definition_data, @report.definition.first)

            assert_equal japanese.id.to_s, @report.definition.first[:data].first[2]
            assert_equal japanese.id.to_s, @report.definition.first[:data].last[2]
          end

          it "should modify the matching locale definition" do
            deprecate_locale.send(:change_report_definition_data, @report.definition.first)

            assert_equal japanese.id.to_s, @report.definition.first[:data].first[2]
            assert_equal japanese.id.to_s, @report.definition.first[:data].last[2]
          end
        end
      end
    end
  end

  describe "Deprecate the Locale" do
    before do
      Account.any_instance.stubs(:available_languages).returns([spanish, japanese])
      account.translation_locale = spanish
      account.allowed_translation_locales = [spanish, japanese]
      account.save!

      @user = account.users.last
      @user.translation_locale = spanish
      @user.save!

      @ticket = account.tickets.first
      @ticket.update_column(:locale_id, spanish.id)

      fallback = {is_fallback: true, nested: true, value: "The fallback value", translation_locale_id: spanish.id}
      Cms::Text.create!(name: "Welcome Wombat", fallback_attributes: fallback, account: account)

      @forum = account.forums.first
      @forum.translation_locale = spanish
      @forum.save!

      NpsRecipient.create! do |nr|
        nr.account_id = account.id
        nr.delivery_id = 1
        nr.email = "Test@test.com"
        nr.user_id = 1
        nr.name = "TEST"
        nr.locale_id = spanish.id
      end

      @rule = account.rules.last
      definition_with_locale = Definition.build(conditions_all: [{operator: "is_not", value: [spanish.id.to_s], source: "locale_id" }])
      @rule.definition = @rule.definition.merge(definition_with_locale)
      @rule.save!

      report = {
        title: "stuffs",
        is_relative_interval: "true",
        relative_interval_in_days: "7",
        "from_date(1i)": "2015",
        "from_date(2i)": "1",
        "from_date(3i)": "27",
        "to_date(1i)": "2015",
        "to_date(2i)": "2",
        "to_date(3i)": "27"
      }

      report_sets = {
        1 => {
          legend: "Legend for this data series",
          state: "created_at",
          conditions: {
            1 => {
              source: "locale_id",
              operator: "is",
              value: { 0 => spanish.id.to_s }
            }
          }
        }
      }

      @report = account.reports.build(report)
      @report.author = account.users.first
      @report.sets = report_sets
      @report.save!
    end

    it "should deprecate the old locale and update it with the new locale if use without dry run" do
      Zendesk::Maintenance::Jobs::DeprecateLocaleJob.work_on_shard(account.shard_id, spanish.id, japanese.id, false)
      account.reload

      user_new_locale = account.users.find(@user.id)
      cms_new_locale = account.cms_variants.last
      forum_new_locale = account.forums.find(@forum.id)
      rule_new_locale = account.rules.find(@rule.id)
      report_new_locale = account.reports.find(@report.id)

      assert_equal japanese, account.translation_locale
      assert_equal japanese, user_new_locale.translation_locale
      assert_equal japanese, cms_new_locale.translation_locale
      assert_equal japanese, forum_new_locale.translation_locale
      assert_equal [japanese.id.to_s], rule_new_locale.definition.conditions_all.last.value
      assert_equal japanese.id.to_s, report_new_locale.definition.first[:data].first[2]
    end

    it "should not deprecate the old locale and update it with the new locale if use with dry run" do
      Zendesk::Maintenance::Jobs::DeprecateLocaleJob.work_on_shard(account.shard_id, spanish.id, japanese.id, true)
      account.reload

      user_new_locale = account.users.find(@user.id)
      cms_new_locale = account.cms_variants.last
      forum_new_locale = account.forums.find(@forum.id)
      rule_new_locale = account.rules.find(@rule.id)
      report_new_locale = account.reports.find(@report.id)

      assert_equal spanish, account.translation_locale
      assert_equal spanish, user_new_locale.translation_locale
      assert_equal spanish, cms_new_locale.translation_locale
      assert_equal spanish, forum_new_locale.translation_locale
      assert_equal [spanish.id.to_s], rule_new_locale.definition.conditions_all.last.value
      assert_equal spanish.id.to_s, report_new_locale.definition.first[:data].first[2]
    end
  end
end
