require_relative '../../../../support/job_helper'
require_relative '../../../../support/zopim_test_helper'
require_relative '../../../../support/chat_phase_3_helper'
require_relative '../../../../support/entitlement_test_helper'
require_relative '../../../../support/suite_test_helper'
require 'zendesk/maintenance/jobs/base_maintenance_job'

SingleCov.covered! uncovered: 5

describe 'ZopimStatusSynchronizerJob' do
  include ZopimTestHelper
  include ChatPhase3Helper
  include EntitlementTestHelper
  include SuiteTestHelper

  fixtures :zopim_subscriptions

  let(:patch_status) { 200 }

  before do
    Timecop.freeze('2014-12-23')
    Zopim::AgentSyncJob.stubs(:work)
    Account.any_instance.stubs(has_chat_permission_set?: true)
    ZopimIntegration.any_instance.stubs(
      install_app: true,
      uninstall_app: true,
      ensure_not_phase_four: nil
    )
    ZBC::Zopim::Subscription.any_instance.stubs(:ensure_not_phase_four)

    ZBC::Zuora::Jobs::ZopimIdSyncJob.stubs(:enqueue)
    stub_staff_service_patch_request('multiproduct', patch_status)
    Zopim::Reseller.client.stubs(
      create_account!: Hashie::Mash.new(id: 1),
      account_agents!: [Hashie::Mash.new(id: 1, owner: true)],
      find_accounts!: []
    )
  end

  describe '#execute' do
    let(:account) { accounts(:minimum) }

    let(:zopim_subscription) { account.zopim_subscription }

    let(:regular_zopim_account) do
      Hashie::Mash.new(id: 42, plan: 'trial')
    end

    let(:expired_zopim_account) do
      Hashie::Mash.new(id: zopim_subscription.zopim_account_id, plan: 'lite')
    end

    before do
      setup_zopim_subscription(account)
      Zopim::Reseller.client.stubs(
        account!: regular_zopim_account,
        update_account!: true
      )
      Zopim::Reseller.client.stubs(:account!).
        with(id: zopim_subscription.zopim_account_id).
        returns(expired_zopim_account)
    end

    it 'expires zopim trial who got downgraded to lite' do
      Zendesk::Maintenance::Jobs::ZopimStatusSynchronizerJob.execute

      expired = ZBC::Zopim::Subscription.find(zopim_subscription.id)
      assert_equal ZBC::Zopim::PlanType::Lite.name, expired.plan_type
      assert_equal 1, expired.max_agents
    end

    describe 'expired subscription not on local pod' do
      before do
        Account.any_instance.stubs(:pod_local?).returns(false)
      end

      it 'does not expire zopim trial' do
        Zendesk::Maintenance::Jobs::ZopimStatusSynchronizerJob.execute

        not_expired = ZBC::Zopim::Subscription.find(zopim_subscription.id)
        assert_equal ZBC::Zopim::PlanType::Trial.name, not_expired.plan_type
      end
    end

    describe 'expired subscription with a cancelled/soft-deleted account' do
      before do
        ZBC::Zopim::Subscription.any_instance.stubs(:account).returns(nil)
      end

      it 'does not sync the subscription' do
        Zendesk::Maintenance::Jobs::ZopimStatusSynchronizerJob.execute
        not_expired = ZBC::Zopim::Subscription.find(zopim_subscription.id)
        assert_equal ZBC::Zopim::PlanType::Trial.name, not_expired.plan_type
      end
    end

    describe 'expired subscription where sync is throwing an exception' do
      before do
        ZBC::Zopim::Subscription.any_instance.expects(:expire_trial!).
          raises(RuntimeError)
      end

      it 'does not expire zopim trial and log an exception' do
        ZendeskExceptions::Logger.expects(:record)
        Zendesk::Maintenance::Jobs::ZopimStatusSynchronizerJob.execute
      end
    end

    describe 'zopim phase three subscription' do
      let(:account) { accounts(:multiproduct) }

      before do
        options = { plan_type: ZBC::Zopim::PlanType::Trial.name }
        @phase_3_zopim_subscription = create_phase_3_zopim_subscription(account, options)
      end

      it 'does not expire phase three accounts' do
        Zendesk::Maintenance::Jobs::MaintenanceLogger.any_instance.expects(:info).at_least_once
        Zendesk::Maintenance::Jobs::ZopimStatusSynchronizerJob.execute

        not_expired = ZBC::Zopim::Subscription.find(@phase_3_zopim_subscription.id)
        assert_equal ZBC::Zopim::PlanType::Trial.name, not_expired.plan_type
      end
    end

    describe 'over extended trials that are not converted to Lite' do
      before do
        Zopim::Reseller.client.stubs(:account!).
          with(id: zopim_subscription.zopim_account_id).
          returns(regular_zopim_account)

        zopim_subscription.created_at = (ZBC::Zopim::Subscription::TrialManagement::
          MAX_TRIAL_LENGTH_IN_DAYS + 1).days.ago

        zopim_subscription.save!
      end

      it 'expires zopim trial' do
        Zendesk::Maintenance::Jobs::ZopimStatusSynchronizerJob.execute

        expired = ZBC::Zopim::Subscription.find(zopim_subscription.id)
        assert_equal ZBC::Zopim::PlanType::Lite.name, expired.plan_type
        assert_equal 1, expired.max_agents
      end
    end
  end
end
