require_relative "../../../../support/job_helper"

SingleCov.covered! uncovered: 15

describe 'BoostExpiryJob' do
  fixtures :all

  describe "Boost expiry job" do
    let(:account) { accounts(:minimum) }

    let(:expired_boost) { { name: 'my_addon', boost_expires_at: 1.day.ago } }

    before do
      ActionMailer::Base.deliveries = []
      FeatureBoost.without_arsi.delete_all
    end

    describe "when a plan-level boost is present" do
      it "does not send emails when there are no expiring boost features" do
        BoostMailer.expects(:deliver_boost_expiry).never
        Zendesk::Maintenance::Jobs::BoostExpiryJob.execute
      end

      it "sends emails when BoostFeature will expire in 5 days" do
        account.create_feature_boost(valid_until: 5.days.from_now)
        BoostMailer.expects(:deliver_boost_expiry).once.with(account, 5)
        Zendesk::Maintenance::Jobs::BoostExpiryJob.execute
      end

      it "sends emails when BoostFeature will expire in a day" do
        account.create_feature_boost(valid_until: 1.days.from_now)
        BoostMailer.expects(:deliver_boost_expiry).once.with(account, 1)
        Zendesk::Maintenance::Jobs::BoostExpiryJob.execute
      end

      it "inactivates the boost when boost period ends" do
        account.create_feature_boost(valid_until: 1.days.ago)
        assert account.boosted?
        Zendesk::Maintenance::Jobs::BoostExpiryJob.execute
        refute account.reload.boosted?
      end

      describe "and boosted/expired add-ons are present" do
        before do
          account.subscription_feature_addons.create(expired_boost).save!(validate: false)
          assert_equal 1, account.subscription_feature_addons.count(:all)

          account.create_feature_boost(valid_until: 1.days.ago)
          assert account.boosted?
        end

        it "destroy any expired/boosted add-ons" do
          Zendesk::Maintenance::Jobs::BoostExpiryJob.execute
          assert_equal 0, account.subscription_feature_addons.count(:all)
          refute account.reload.boosted?
        end
      end
    end

    describe "when no plan-level boost are present" do
      before { account.subscription_feature_addons.create(expired_boost).save!(validate: false) }

      it "still destroy any expired/boosted add-ons" do
        assert_equal 1, account.subscription_feature_addons.count(:all)
        Zendesk::Maintenance::Jobs::BoostExpiryJob.execute
        assert_equal 0, account.subscription_feature_addons.count(:all)
      end
    end
  end
end
