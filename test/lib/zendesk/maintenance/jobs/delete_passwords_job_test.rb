require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'DeletePasswordsJob' do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:role_settings) { account.role_settings }
  let(:agent) { account.owner }
  let(:end_user) { account.end_users.first }
  let(:job) { Zendesk::Maintenance::Jobs::DeletePasswordsJob }

  def disable_agent_passwords!
    role_settings.update_attributes!(agent_zendesk_login: false, agent_password_allowed: false)
  end

  def disable_end_user_passwords!
    role_settings.update_attributes!(end_user_zendesk_login: false, end_user_password_allowed: false)
  end

  before do
    refute_nil agent.crypted_password
    refute_nil end_user.crypted_password
  end

  describe "when agent passwords are disabled" do
    before do
      disable_agent_passwords!
      job.work
    end

    it "does nothing initially" do
      refute_nil agent.reload.crypted_password
      refute_nil agent.reload.salt
    end

    describe "after 1 day" do
      before do
        Timecop.travel(Time.now + 1.day)
        job.work
      end

      it "removes agents passwords" do
        assert_nil agent.reload.crypted_password
        assert_nil agent.reload.salt
      end

      it "does not remove end user passwords" do
        refute_nil end_user.reload.crypted_password
        refute_nil end_user.reload.salt
      end
    end
  end

  describe "when end user passwords are disabled" do
    before do
      disable_end_user_passwords!
      job.work
    end

    it "does nothing initially" do
      refute_nil agent.reload.crypted_password
      refute_nil agent.reload.salt
    end

    describe "after 1 day" do
      before do
        Timecop.travel(Time.now + 1.day)
        job.work
      end

      it "does not remove agents passwords" do
        refute_nil agent.reload.crypted_password
        refute_nil agent.reload.salt
      end

      it "removes end user passwords" do
        assert_nil end_user.reload.crypted_password
        assert_nil end_user.reload.salt
      end
    end
  end
end
