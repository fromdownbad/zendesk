require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'UserCountJob' do
  fixtures :accounts

  describe "Count users for account" do
    before do
      @account = accounts(:minimum)
    end

    describe "#work" do
      it "overwrites work_on_account method" do
        Zendesk::Maintenance::Jobs::UserCountJob.work
      end

      it 'continues with rest of accounts if one of the accounts fails' do
        valid_accounts = Account.joins(:subscription).all.select(&:valid?)

        Zendesk::Maintenance::Jobs::UserCountJob.work

        success = valid_accounts.all? do |account|
          account.reload
          assert_equal account.users.count(:all), account.settings.user_count
        end
        assert success
      end
    end

    describe "#work_on_account" do
      it "adds the account's user count to settings" do
        Zendesk::Maintenance::Jobs::UserCountJob.work_on_account(@account)
        assert_equal @account.users.count(:all), @account.settings.user_count
      end

      it "does not run when account is not active" do
        @account.is_active = false
        @account.save!

        Zendesk::Maintenance::Jobs::UserCountJob.work_on_account(@account)
        refute @account.settings.user_count
      end

      it "does not run when account is expired" do
        @account.is_serviceable = false
        @account.save!

        Zendesk::Maintenance::Jobs::UserCountJob.work_on_account(@account)
        refute @account.settings.user_count
      end

      it 'does not run when the account is the system account' do
        @account = Account.find(Account.system_account_id)
        Zendesk::Maintenance::Jobs::UserCountJob.work_on_account(@account)
        refute @account.settings.user_count
      end

      it 'saves successfully when account owner email is not verified' do
        @account.owner.is_verified = false
        @account.owner.save!

        Zendesk::Maintenance::Jobs::UserCountJob.work_on_account(@account)
        assert_equal @account.users.count(:all), @account.settings.user_count
      end
    end

    describe '#args_to_log' do
      it 'works' do
        assert_equal({}, Zendesk::Maintenance::Jobs::UserCountJob.args_to_log)
      end
    end
  end
end
