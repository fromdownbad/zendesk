require_relative '../../../../support/job_helper'
require_relative '../../../../support/archive_helper'

SingleCov.covered!

describe 'RemoveRiakDataJob' do
  include DataDeletionJobSupport

  fixtures :accounts, :tickets

  describe 'Remove RIAK data job for an account' do
    before do
      @job = create_job(accounts(:minimum), Zendesk::Maintenance::Jobs::RemoveRiakDataJob)
    end

    around do |test|
      with_in_memory_archive_router(test)
    end

    describe 'reason = moved' do
      before do
        @job.account.update_attribute(:shard_id, 2)
        @job.job_audit.audit.reason = 'moved'
        @job.job_audit.shard_id = 1
      end

      it 'executes for inter-pod moves' do
        @job.stubs(:pod_local_move?).returns(false)
        Zendesk::Maintenance::Jobs::RemoveRiakDataJob.any_instance.expects(:do_erase).once
        @job.work
      end

      it 'noops for intra-pod moves' do
        Zendesk::Maintenance::Jobs::RemoveRiakDataJob.any_instance.expects(:do_erase).never
        @job.stubs(:pod_local_move?).returns(true)
        assert @job.send(:pod_local_move?)
        @job.work
      end
    end

    describe "dry run mode" do
      it "should not delete a thing" do
        TicketArchiveStub.any_instance.expects(:remove_from_archive).never
        TicketArchiveStub.any_instance.expects(:delete).never
        @job = create_job(accounts(:with_groups), Zendesk::Maintenance::Jobs::RemoveRiakDataJob, dry_run: true)
        @job.account.tickets.each { |ticket| archive_and_delete(ticket) }
        @job.work
      end
    end

    describe 'when archived tickets exist' do
      before do
        @num_archived_tickets = @job.account.tickets.count(:all)
        @job.account.tickets.each(&:archive!)
      end

      it 'removes an accounts archived tickets' do
        ZendeskArchive.router.expects(:delete).times(@num_archived_tickets)
        @job.send(:erase_for_canceled)
      end

      it 'raises if the kill switch is enabled' do
        Arturo.enable_feature! :account_deletion_kill_switch
        assert_raises Zendesk::DataDeletion::AccountDeletionKillSwitch::KillSwitchEnabled do
          @job.send(:erase_for_canceled)
        end
      end

      it 'verifys removed archived tickets' do
        ZendeskArchive.router.stubs(exists_in_any_store?: false)
        @job.send(:verify_for_canceled)
      end

      it 'verifys removed archived tickets failed' do
        ZendeskArchive.router.stubs(exists_in_any_store?: true)
        assert_raises RuntimeError do
          @job.send(:verify_for_canceled)
        end
      end

      it 'does not fail if stubs exist but archive records do not' do
        @job.account.ticket_archive_stubs.create! do |s|
          s.id = 123423
          s.nice_id = 1111
        end
        @job.send(:erase_for_canceled)
      end
    end
  end
end
