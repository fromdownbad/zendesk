require_relative '../../../../support/job_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::TalkUsageMonitoringJob do
  let(:job) { Zendesk::Maintenance::Jobs::TalkUsageMonitoringJob }

  describe 'cron schedule' do
    let(:resque_schedule) do
      YAML.load(ERB.new(File.read(Rails.root + 'config/resque-schedule.yml')).result)
    end

    it 'runs hourly' do
      assert_equal '0 * * * *', resque_schedule[job.name]['cron']
    end

    it 'run in the low priority queue' do
      assert_equal 'low', resque_schedule[job.name]['queue']
    end
  end

  describe '#work_on_shard' do
    let(:account_id_quarter) { Account.first.id }
    let(:account_id_month)   { Account.second.id }
    let(:account_id_week)    { Account.third.id }
    let(:account_id_day)     { Account.fourth.id }
    let(:account_id_hour)    { Account.fifth.id }
    let(:statsd_client)      { job.send(:statsd_client) }

    let(:usage_quarter) do
      # should be tagged with 'usage_older_than:quarter'
      ZBC::Voice::UnprocessedUsage.create!(account_id: account_id_quarter, usage_type: 'charge', units: -10)
    end

    let(:usage_month) do
      # should be tagged with 'usage_older_than:month'
      ZBC::Voice::UnprocessedUsage.create!(account_id: account_id_month, usage_type: 'charge', units: -10)
    end

    let(:usage_week) do
      # should be tagged with 'usage_older_than:week'
      ZBC::Voice::UnprocessedUsage.create!(account_id: account_id_week, usage_type: 'charge', units: -10)
    end

    let(:usage_day) do
      # should be tagged with 'usage_older_than:day'
      ZBC::Voice::UnprocessedUsage.create!(account_id: account_id_day, usage_type: 'charge', units: -10)
    end

    let(:usage_hour) do
      # should be tagged with 'usage_older_than:hour'
      ZBC::Voice::UnprocessedUsage.create!(account_id: account_id_hour, usage_type: 'charge', units: -10)
    end

    before do
      # so the usage records don't get immediately processed when created
      ZBC::Voice::UnprocessedUsage.any_instance.stubs(:enqueue_processing_job)

      Timecop.travel(4.months.ago) { usage_quarter }
      Timecop.travel(2.months.ago) { usage_month }
      Timecop.travel(2.weeks.ago)  { usage_week }
      Timecop.travel(2.days.ago)   { usage_day }
      Timecop.travel(2.hours.ago)  { usage_hour }
    end

    describe 'when scheduled_talk_usage_tracking Arturo is ON' do
      before do
        Arturo.enable_feature!(:scheduled_talk_usage_tracking)
      end

      def set_non_hour_statsd_expectations
        statsd_client.expects(:gauge).with(
          'stale_usages',
          1,
          tags: [
            "account_id:#{account_id_quarter}",
            'usage_older_than:quarter'
          ]
        )
        statsd_client.expects(:gauge).with(
          'stale_usages',
          1,
          tags: [
            "account_id:#{account_id_month}",
            'usage_older_than:month'
          ]
        )
        statsd_client.expects(:gauge).with(
          'stale_usages',
          1,
          tags: [
            "account_id:#{account_id_week}",
            'usage_older_than:week'
          ]
        )
        statsd_client.expects(:gauge).with(
          'stale_usages',
          1,
          tags: [
            "account_id:#{account_id_day}",
            'usage_older_than:day'
          ]
        )
      end

      def set_non_hour_job_expectations
        ZBC::Voice::Jobs::ProcessUsageJob.expects(:enqueue).
          with(usage_quarter.id, usage_quarter.account_id)
        ZBC::Voice::Jobs::ProcessUsageJob.expects(:enqueue).
          with(usage_month.id, usage_month.account_id)
        ZBC::Voice::Jobs::ProcessUsageJob.expects(:enqueue).
          with(usage_week.id, usage_week.account_id)
        ZBC::Voice::Jobs::ProcessUsageJob.expects(:enqueue).
          with(usage_day.id, usage_day.account_id)
      end

      describe 'when track_talk_usage_older_than_hour Arturo is ON' do
        before do
          Arturo.enable_feature!(:track_talk_usage_older_than_hour)
        end

        it 'reports all usages older than an hour' do
          set_non_hour_statsd_expectations
          set_non_hour_job_expectations
          statsd_client.expects(:gauge).with(
            'stale_usages',
            1,
            tags: [
              "account_id:#{account_id_hour}",
              'usage_older_than:hour'
            ]
          )

          ZBC::Voice::Jobs::ProcessUsageJob.expects(:enqueue).
            with(usage_hour.id, usage_hour.account_id)

          job.work
        end
      end

      describe 'when track_talk_usage_older_than_hour Arturo is OFF' do
        before do
          Arturo.disable_feature!(:track_talk_usage_older_than_hour)
        end

        it 'reports all usages older than a day, but does not report hour-old usages' do
          set_non_hour_statsd_expectations
          set_non_hour_job_expectations

          statsd_client.expects(:gauge).with(
            'stale_usages',
            1,
            tags: [
              "account_id:#{anything}",
              'usage_older_than:hour'
            ]
          ).never

          ZBC::Voice::Jobs::ProcessUsageJob.expects(:enqueue).
            with(usage_hour.id, usage_hour.account_id).never

          job.work
        end
      end

      describe 'when there are multiple stale usages for the same account' do
        let(:usage_week_extra) do
          # creating an extra stale usage to imitate multiple stale usages
          ZBC::Voice::UnprocessedUsage.create!(account_id: account_id_week, usage_type: 'charge', units: -10)
        end

        before do
          statsd_client.stubs(:gauge)
          ZBC::Voice::Jobs::ProcessUsageJob.stubs(:enqueue)

          Timecop.travel(2.weeks.ago) { usage_week_extra }
        end

        it 'reports all stale usages' do
          statsd_client.expects(:gauge).with(
            'stale_usages',
            2,
            tags: [
              "account_id:#{account_id_week}",
              'usage_older_than:week'
            ]
          )

          job.work
        end

        it 'enqueues the job to process the stale usages' do
          ZBC::Voice::Jobs::ProcessUsageJob.expects(:enqueue).
            with(usage_week.id, usage_week.account_id)

          job.work
        end
      end
    end

    describe 'when scheduled_talk_usage_tracking Arturo is OFF' do
      before do
        Arturo.disable_feature!(:scheduled_talk_usage_tracking)
      end

      it 'does nothing' do
        ZBC::Voice::UnprocessedUsage.expects(:where).never
        statsd_client.expects(:gauge).never
        ZBC::Voice::Jobs::ProcessUsageJob.expects(:enqueue).never

        job.work
      end
    end
  end
end
