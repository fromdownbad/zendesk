require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 6

describe 'AccountPruningJob' do
  fixtures :accounts, :subscriptions

  resque_inline false

  let(:subject) { Zendesk::Maintenance::Jobs::AccountPruningJob.new }
  let!(:multiproduct_account) { accounts(:multiproduct) }
  let(:support_trial_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_trial_product).with_indifferent_access) }
  let(:support_inactive_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:support_inactive_product).with_indifferent_access) }
  let(:chat_inactive_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:chat_inactive_product).with_indifferent_access) }

  let(:statsd_client) { stub_for_statsd }

  before do
    Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
  end

  describe "Customer Accounts (with sandbox)" do
    before do
      Zendesk::Maintenance::Jobs::AccountPruningJob.any_instance.stubs(:cancel_suspended_multiproduct_trial_accounts)
      Zendesk::Maintenance::Jobs::AccountPruningJob.any_instance.stubs(:soft_delete_cancelled_multiproduct_accounts)

      @sandbox = accounts(:trial)
      @sandbox.sandbox_master = accounts(:minimum)
      @sandbox.save!
    end

    describe "For canceled accounts" do
      before do
        @account = accounts(:minimum)
        @subscription = @account.subscription

        @account.stubs(:is_active?).returns(false)
        @account.stubs(:is_serviceable?).returns(false)
      end

      describe "that canceled more than 90 days ago" do
        before do
          @subscription.canceled_on = 91.days.ago
          @subscription.save!
        end

        describe "with sandbox cancelled sooner than 90 days" do
          before do
            @sandbox.subscription.canceled_on = 1.days.ago
            @sandbox.subscription.save!
          end

          it 'soft deletes sandbox, anyways' do
            # once for master, once for sandbox
            Account.any_instance.expects(:soft_delete!).twice
            Zendesk::Maintenance::Jobs::AccountPruningJob.work
          end

          describe "when dry_run" do
            before do
              Zendesk::Maintenance::Jobs::AccountPruningJob.any_instance.stubs(:dry_run).returns(true)
            end

            it 'does not soft delete master or sandbox accounts' do
              Account.any_instance.expects(:soft_delete!).never
              Zendesk::Maintenance::Jobs::AccountPruningJob.work
            end

            it 'does not call Pravda account service to set support product state to deleted' do
              Zendesk::Accounts::Client.any_instance.expects(:update_product).never
              Zendesk::Maintenance::Jobs::AccountPruningJob.work
            end
          end
        end

        describe "with a sandbox cancelled later than 90 days" do
          before do
            @sandbox.subscription.canceled_on = 91.days.ago
            @sandbox.subscription.save!
          end

          it 'softs deletes account and sandbox' do
            # once for master, once for sandbox
            Account.any_instance.expects(:soft_delete!).twice
            Zendesk::Maintenance::Jobs::AccountPruningJob.work
          end

          describe "when dry_run" do
            before do
              Zendesk::Maintenance::Jobs::AccountPruningJob.any_instance.stubs(:dry_run).returns(true)
            end

            it 'does not soft delete master or sandbox accounts' do
              Account.any_instance.expects(:soft_delete!).never
              Zendesk::Maintenance::Jobs::AccountPruningJob.work
            end

            it 'does not sync state with Pravda product records' do
              Zendesk::Accounts::Client.any_instance.expects(:update_product).never
              Zendesk::Maintenance::Jobs::AccountPruningJob.work
            end
          end
        end

        describe "when encountering an error" do
          before do
            @broken_account = accounts(:inactive)
            @broken_account.stubs(:soft_delete!).raises(RuntimeError, 'fake deletion error stubbed by test')
            Account.stubs(:canceled_for).returns([@broken_account, @account])
          end

          it 'skips that account and continue' do
            Zendesk::Maintenance::Jobs::AccountPruningJob.work
            @account.reload
            @broken_account.reload

            refute_nil(@account.deleted_at)
            assert_nil(@broken_account.deleted_at)
          end
        end
      end

      describe "canceled less than 90 days ago" do
        before do
          @subscription.canceled_on = 1.days.ago
          @subscription.save!
        end

        describe 'with sandbox canceled less than 90 days ago' do
          before do
            @sandbox.subscription.canceled_on = 1.days.ago
            @sandbox.subscription.save!
          end

          it 'does not soft delete account or sandbox' do
            Account.any_instance.expects(:soft_delete!).never
            Zendesk::Maintenance::Jobs::AccountPruningJob.work
          end
        end

        describe "with sandbox canceled more than 90 days ago" do
          before do
            @sandbox.subscription.canceled_on = 91.days.ago
            @sandbox.subscription.save!
          end

          it 'does not soft delete account or sandbox' do
            Account.any_instance.expects(:soft_delete!).never
            Zendesk::Maintenance::Jobs::AccountPruningJob.work
          end
        end
      end
    end

    describe "For soft deleted accounts" do
      before do
        @account = accounts(:minimum)
        @account.is_active = false
        @account.is_serviceable = false
        @account.save!
        @account.subscription.credit_card = CreditCard.new
        @account.subscription.canceled_on = 130.days.ago
        @account.subscription.save!
        @account.soft_delete!

        @sandbox.is_active = false
        @sandbox.is_serviceable = false
        @sandbox.save!
        @sandbox.soft_delete!

        @audit = @account.data_deletion_audits.first
        @sandbox_audit = @sandbox.data_deletion_audits.first
      end

      describe 'soft deleted more than 30 days ago' do
        before do
          @account.deleted_at = 31.days.ago
          @account.save!

          @audit.created_at = 31.days.ago
          @audit.save!
        end

        describe 'sandbox soft deleted less than 30 days ago' do
          before do
            @sandbox.deleted_at = 1.days.ago
            @sandbox.save!

            @sandbox_audit.created_at = 1.days.ago
            @sandbox_audit.save!
          end

          it 'hard deletes account' do
            DataDeletionAudit.any_instance.expects(:enqueue!).once
            Zendesk::Maintenance::Jobs::AccountPruningJob.work
          end
        end

        describe 'sandbox soft delete more than 30 days ago' do
          before do
            @sandbox.deleted_at = 31.days.ago
            @sandbox.save!

            @sandbox_audit.created_at = 31.days.ago
            @sandbox_audit.save!
          end

          it 'hard deletes account and sandbox' do
            DataDeletionAudit.any_instance.expects(:enqueue!).twice
            Zendesk::Maintenance::Jobs::AccountPruningJob.work
          end
        end
      end

      describe 'soft deleted less than 30 days ago' do
        describe 'sandbox soft deleted less than 30 days ago' do
          before do
            @sandbox.deleted_at = 1.days.ago
            @sandbox.save!

            @sandbox_audit.created_at = 1.days.ago
            @sandbox_audit.save!
          end

          it 'does not hard delete account or sandbox' do
            DataDeletionAudit.any_instance.expects(:enqueue!).never
            Zendesk::Maintenance::Jobs::AccountPruningJob.work
          end
        end
      end
    end
  end

  describe "Trials" do
    before do
      Account.any_instance.stubs(:products).returns([])
      Zendesk::Maintenance::Jobs::AccountPruningJob.any_instance.stubs(:suspend_multiproduct_accounts_with_no_active_products)
      Zendesk::Maintenance::Jobs::AccountPruningJob.any_instance.stubs(:cancel_suspended_multiproduct_trial_accounts)
      Zendesk::Maintenance::Jobs::AccountPruningJob.any_instance.stubs(:soft_delete_cancelled_multiproduct_accounts)

      @account = accounts(:trial)
      @account.expire_trial!
      @account.reload
    end

    describe ".expired_for" do
      it "selects the expired trials" do
        @account.subscription.trial_expires_on = 91.days.ago
        @account.subscription.save!

        assert_includes Account.expired_for(90.days), @account
      end

      it "rejects recently expired trials" do
        @account.subscription.trial_expires_on = 89.days.ago
        @account.subscription.save!

        refute_includes Account.expired_for(90.days), @account
      end

      it "rejects multiproduct accounts" do
        @account = accounts(:multiproduct)
        @account.expire_trial!
        @account.reload

        @account.subscription.trial_expires_on = 91.days.ago
        @account.subscription.save!

        refute_includes Account.expired_for(90.days), @account
      end

      it "rejects unexpired trials" do
        @account.subscription.trial_expires_on = Time.now + 2.days
        @account.subscription.save!

        refute_includes Account.expired_for(90.days), @account
      end
    end

    it "cancels an expired trial after retention period" do
      @account.subscription.trial_expires_on = 2.years.ago
      @account.subscription.save!

      Zendesk::Maintenance::Jobs::AccountPruningJob.work

      @account.reload
      refute @account.is_active
      refute_nil @account.subscription.canceled_on
    end

    it "follows deletion workflow" do
      # trial is expired, but not canceled
      @account.subscription.update_column(:trial_expires_on, 10.months.ago)
      Zendesk::Maintenance::Jobs::AccountPruningJob.work

      # trial is canceled
      @account.reload
      refute @account.is_active
      assert @account.subscription.canceled_on

      @account.subscription.update_column(:churned_on, 91.days.ago)
      Zendesk::Maintenance::Jobs::AccountPruningJob.work

      # trial is soft deleted
      @account.reload
      assert @account.deleted_at

      @account.update_column(:deleted_at, 31.days.ago)
      Zendesk::Maintenance::Jobs::AccountPruningJob.work

      # trial is hard deleting
      @account.reload
      assert_equal 'enqueued', @account.data_deletion_audits.first.status
    end

    describe_with_arturo_disabled :sell_skip_trial_cancellation do
      describe 'Support-first account with active Sell product' do
        let(:sell_trial_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:sell_trial_product).with_indifferent_access) }

        before do
          Account.any_instance.stubs(:products).returns([sell_trial_product])
        end

        it 'cancels the account' do
          @account.subscription.trial_expires_on = 2.years.ago
          @account.subscription.save!

          Zendesk::Maintenance::Jobs::AccountPruningJob.work

          @account.reload
          refute @account.is_active
          refute_nil @account.subscription.canceled_on
        end
      end
    end

    describe_with_arturo_enabled :sell_skip_trial_cancellation do
      describe 'Support-first account with active Sell product' do
        let(:sell_trial_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:sell_trial_product).with_indifferent_access) }

        before do
          Account.any_instance.stubs(:products).returns([sell_trial_product])
        end

        it 'does not cancel the account' do
          @account.subscription.trial_expires_on = 2.years.ago
          @account.subscription.save!

          Zendesk::Maintenance::Jobs::AccountPruningJob.work

          @account.reload
          assert @account.is_active
          assert_nil @account.subscription.canceled_on
        end
      end

      describe 'Support-first account without Sell product' do
        let(:chat_trial_phase3_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:chat_trial_phase3_product).with_indifferent_access) }

        before do
          Account.any_instance.stubs(:products).returns([chat_trial_phase3_product])
        end

        it 'cancels the account' do
          @account.subscription.trial_expires_on = 2.years.ago
          @account.subscription.save!

          Zendesk::Maintenance::Jobs::AccountPruningJob.work

          @account.reload
          refute @account.is_active
          refute_nil @account.subscription.canceled_on
        end
      end

      describe 'Support-first account with inactive Sell product' do
        let(:sell_trial_inactive_product) { Zendesk::Accounts::Product.new(FactoryBot.build(:sell_trial_inactive_product).with_indifferent_access) }

        before do
          Account.any_instance.stubs(:products).returns([sell_trial_inactive_product])
        end

        it 'cancels the account' do
          @account.subscription.trial_expires_on = 2.years.ago
          @account.subscription.save!

          Zendesk::Maintenance::Jobs::AccountPruningJob.work

          @account.reload
          refute @account.is_active
          refute_nil @account.subscription.canceled_on
        end
      end
    end
  end

  describe '#suspend_multiproduct_accounts_with_no_active_products' do
    before do
      Account.any_instance.stubs(:products)
      Zendesk::StatsD::Client.stubs(:new).with(namespace: 'account_pruning_job').returns(statsd_client)
      refute multiproduct_account.reload.billing_id
      assert multiproduct_account.reload.is_active?
      assert multiproduct_account.reload.is_serviceable?
    end

    describe 'when account has active products' do
      before { Account.any_instance.stubs(:active_products?).returns(true) }

      it 'does NOT suspend account' do
        subject.suspend_multiproduct_accounts_with_no_active_products
        assert multiproduct_account.reload.is_serviceable?
      end
    end

    describe 'when account has NO active products' do
      before { Account.any_instance.stubs(:active_products?).returns(false) }

      describe 'when trial account' do
        it 'suspends account' do
          subject.suspend_multiproduct_accounts_with_no_active_products
          refute multiproduct_account.reload.is_serviceable?
        end
      end

      describe 'when subscribed account' do
        before { multiproduct_account.update_attribute(:billing_id, 123) }

        it 'does NOT suspend account' do
          assert multiproduct_account.reload.billing_id
          subject.suspend_multiproduct_accounts_with_no_active_products
          assert multiproduct_account.reload.is_serviceable?
        end
      end
    end

    describe 'when exception is thrown' do
      before { Account.any_instance.stubs(:active_products?).raises(StandardError) }

      it 'logs error and metrics' do
        ZendeskExceptions::Logger.expects(:record).at_least_once
        Rails.logger.expects(:error).with(regexp_matches(/suspending multiproduct trial account/)).at_least_once
        statsd_client.expects(:increment).with('execute.fail', tags: ["task:suspend_multiproduct_accounts_with_no_active_products", "exception:standard_error"])
        subject.suspend_multiproduct_accounts_with_no_active_products
      end
    end
  end

  describe '#cancel_suspended_multiproduct_trial_accounts' do
    before do
      Account.any_instance.stubs(:products)
      Zendesk::StatsD::Client.stubs(:new).with(namespace: 'account_pruning_job').returns(statsd_client)
      multiproduct_account.update_attribute(:is_serviceable, false)
      refute multiproduct_account.reload.billing_id
      refute multiproduct_account.reload.is_serviceable?
      assert multiproduct_account.reload.is_active?
    end

    describe 'when inside retention period' do
      before { subject.stubs(:inside_retention_period?).returns(true) }

      it 'does not cancel account' do
        subject.cancel_suspended_multiproduct_trial_accounts
        assert multiproduct_account.reload.is_active?
      end
    end

    describe 'when outside retention period' do
      before { subject.stubs(:inside_retention_period?).returns(false) }

      it 'cancels account' do
        subject.cancel_suspended_multiproduct_trial_accounts
        refute multiproduct_account.reload.is_active?
      end

      describe 'when account is serviceable' do
        before { multiproduct_account.update_attribute(:is_serviceable, true) }

        it 'does not cancel account' do
          assert multiproduct_account.reload.is_serviceable?
          subject.cancel_suspended_multiproduct_trial_accounts
          assert multiproduct_account.reload.is_active?
        end
      end

      describe 'when subscribed account' do
        before { multiproduct_account.update_attribute(:billing_id, 123) }

        it 'does not cancel account' do
          assert multiproduct_account.reload.billing_id
          subject.cancel_suspended_multiproduct_trial_accounts
          assert multiproduct_account.reload.is_active?
        end
      end

      describe 'when has_prevent_deletion_if_churned? is true' do
        before do
          Account.any_instance.stubs(:has_prevent_deletion_if_churned?).returns(true)
        end

        it 'does not cancel account' do
          subject.cancel_suspended_multiproduct_trial_accounts
          assert multiproduct_account.reload.is_active?
        end
      end
    end

    describe 'when no products exist for accounts' do
      before do
        Account.any_instance.stubs(:active_products?).returns(false)
        Account.any_instance.stubs(:products).returns([])
      end

      it 'logs error and metrics' do
        ZendeskExceptions::Logger.expects(:record).at_least_once
        Rails.logger.expects(:error).with(regexp_matches(/cancel suspended multiproduct account/)).at_least_once
        statsd_client.expects(:increment).with('execute.fail', tags: ["task:cancel_suspended_multiproduct_trial_accounts", "exception:no_product_records"])
        subject.cancel_suspended_multiproduct_trial_accounts
      end
    end
  end

  describe '#soft_delete_cancelled_multiproduct_accounts' do
    before do
      Account.any_instance.stubs(:products).returns([])
      Zendesk::StatsD::Client.stubs(:new).with(namespace: 'account_pruning_job').returns(statsd_client)
    end

    describe 'when active account' do
      before { multiproduct_account.update_column(:is_active, true) }

      it 'does not soft delete account' do
        subject.expects(:soft_delete_account).with(multiproduct_account).never
        subject.soft_delete_cancelled_multiproduct_accounts
      end
    end

    describe 'when serviceable account' do
      before { multiproduct_account.update_column(:is_serviceable, true) }

      it 'does not soft delete account' do
        subject.expects(:soft_delete_account).with(multiproduct_account).never
        subject.soft_delete_cancelled_multiproduct_accounts
      end
    end

    describe 'when inactive and unserviceable account' do
      before do
        multiproduct_account.update_column(:is_active, false)
        multiproduct_account.update_column(:is_serviceable, false)
      end

      describe 'when inside retention period' do
        before { subject.stubs(:inside_retention_period?).returns(true) }

        it 'does not soft_delete account' do
          subject.expects(:soft_delete_account).with(multiproduct_account).never
          subject.soft_delete_cancelled_multiproduct_accounts
        end
      end

      describe 'when outside retention period' do
        before { subject.stubs(:inside_retention_period?).returns(false) }

        it 'soft deletes account' do
          subject.expects(:soft_delete_account).with(multiproduct_account).once
          subject.soft_delete_cancelled_multiproduct_accounts
        end

        describe 'when has_prevent_deletion_if_churned? is true' do
          before do
            Account.any_instance.stubs(:has_prevent_deletion_if_churned?).returns(true)
          end

          it 'does not soft_delete account' do
            subject.expects(:soft_delete_account).with(multiproduct_account).never
            subject.soft_delete_cancelled_multiproduct_accounts
          end
        end

        describe 'when active products exist for the account' do
          before { Account.any_instance.stubs(:products).returns([support_trial_product]) }

          it 'logs warning message' do
            Rails.logger.expects(:warn).at_least_once
            subject.soft_delete_cancelled_multiproduct_accounts
          end
        end
      end

      describe 'when no products exist for cancelled account' do
        before do
          multiproduct_account.update_column(:is_active, false)
          Account.any_instance.stubs(:products).returns([])
        end

        describe 'when account updated_at is outside of retention period' do
          before { multiproduct_account.update_column(:updated_at, 181.days.ago) }

          it 'soft deletes the account' do
            subject.expects(:soft_delete_account).with(multiproduct_account).once
            subject.soft_delete_cancelled_multiproduct_accounts
          end
        end

        describe 'when account updated_at is within retention period' do
          before { multiproduct_account.update_column(:updated_at, 1.days.ago) }

          it 'does not soft delete account' do
            subject.expects(:soft_delete_account).with(multiproduct_account).never
            subject.soft_delete_cancelled_multiproduct_accounts
          end
        end
      end
    end

    describe 'when an exception occurs' do
      before do
        multiproduct_account.update_column(:is_active, false)
        multiproduct_account.update_column(:is_serviceable, false)
        multiproduct_account.update_column(:updated_at, 181.days.ago)
        Account.any_instance.stubs(:products).raises(StandardError.new)
      end

      it 'logs error and metrics' do
        Rails.logger.expects(:warn).with(regexp_matches(/Failure for multiproduct account/)).at_least_once
        statsd_client.expects(:increment).with('execute.fail', tags: ["task:soft_delete_cancelled_multiproduct_accounts", "exception:standard_error"])
        subject.soft_delete_cancelled_multiproduct_accounts
      end
    end
  end

  describe '#soft_delete_old_sandbox_accounts' do
    let(:sandbox) do
      sandbox = accounts(:trial)
      sandbox.update_column(:sandbox_master_id, accounts(:minimum).id)
      sandbox
    end

    describe 'when active account' do
      before { sandbox.update_column(:is_active, true) }

      it 'does not soft delete account' do
        subject.expects(:soft_delete_account).with(sandbox).never
        subject.soft_delete_old_sandbox_accounts
      end
    end

    describe 'when serviceable account' do
      before { sandbox.update_column(:is_serviceable, true) }

      it 'does not soft delete account' do
        subject.expects(:soft_delete_account).with(sandbox).never
        subject.soft_delete_old_sandbox_accounts
      end
    end

    describe 'when inactive and unserviceable account' do
      before do
        sandbox.update_column(:is_active, false)
        sandbox.update_column(:is_serviceable, false)
      end

      describe 'when inside retention period' do
        before { sandbox.update_column(:updated_at, 89.days.ago) }

        it 'does not soft_delete account' do
          subject.expects(:soft_delete_account).with(sandbox).never
          subject.soft_delete_old_sandbox_accounts
        end
      end

      describe 'when outside retention period' do
        before { sandbox.update_column(:updated_at, 91.days.ago) }

        it 'soft deletes account' do
          subject.expects(:soft_delete_account).with(sandbox).once
          subject.soft_delete_old_sandbox_accounts
        end

        describe 'when has_prevent_deletion_if_churned? is true' do
          before do
            Account.any_instance.stubs(:has_prevent_deletion_if_churned?).returns(true)
          end

          it 'does not soft_delete account' do
            subject.expects(:soft_delete_account).with(sandbox).never
            subject.soft_delete_old_sandbox_accounts
          end
        end
      end
    end
  end

  describe '#inside_retention_period?' do
    let(:retention) { 5.day }

    before do
      Timecop.freeze
      support_trial_product.stubs(:state_updated_at).returns((retention + 1.day).ago)
      chat_inactive_product.stubs(:state_updated_at).returns((retention - 1.day).ago)
      support_inactive_product.stubs(:state_updated_at).returns(retention.ago)
    end

    it 'returns true when state_updated_at is after retention date' do
      assert subject.send(:inside_retention_period?, [support_trial_product, chat_inactive_product], retention)
    end

    it 'returns true when state_updated_at equals the retention date' do
      assert subject.send(:inside_retention_period?, [support_inactive_product], retention)
    end

    it 'returns false when state_updated_at is before retention date' do
      refute subject.send(:inside_retention_period?, [support_trial_product], retention)
    end

    it 'raises NoProductRecords exception if there are no products' do
      assert_raises NoProductRecords do
        subject.send(:inside_retention_period?, [], retention)
      end
    end
  end

  describe '#work' do
    let(:pruning_job) { Zendesk::Maintenance::Jobs::AccountPruningJob }

    describe 'when voltron_multiproduct_pruning_job is disabled' do
      before { Arturo.disable_feature!(:voltron_multiproduct_pruning_job) }

      it 'does not run multiproduct jobs' do
        pruning_job.any_instance.expects(:cancel_suspended_multiproduct_trial_accounts).never
        pruning_job.any_instance.expects(:soft_delete_cancelled_multiproduct_accounts).never
        pruning_job.work
      end
    end

    describe 'when voltron_multiproduct_pruning_job is enabled' do
      before { Arturo.enable_feature!(:voltron_multiproduct_pruning_job) }

      it 'runs multiproduct jobs' do
        pruning_job.any_instance.expects(:suspend_multiproduct_accounts_with_no_active_products).once
        pruning_job.any_instance.expects(:cancel_suspended_multiproduct_trial_accounts).once
        pruning_job.any_instance.expects(:soft_delete_cancelled_multiproduct_accounts).once
        pruning_job.work
      end
    end

    describe 'sandbox_pruning_job' do
      it 'happens' do
        pruning_job.any_instance.expects(:soft_delete_old_sandbox_accounts).once
        pruning_job.work
      end
    end
  end
end
