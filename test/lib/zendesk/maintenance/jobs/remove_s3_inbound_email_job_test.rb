require_relative '../../../../support/job_helper'

SingleCov.covered! uncovered: 4

describe Zendesk::Maintenance::Jobs::RemoveS3InboundEmailJob do
  include DataDeletionJobSupport

  fixtures :all

  let(:account) { accounts(:minimum) }

  zendesk_yml = {
    pods: {
      1 => {
        "aws_regions" => {
          "csv_export_store"                => ["s3"],
          "help_center_store"               => ["s3eu"],
          "inbound_email_store"             => ["s3"],
          "inbound_email_attachments_store" => ["s3"],
          "voyager_store"                   => ["s3usw1"]
        }
      },
      2 => {
        "aws_regions" => {
          "csv_export_store"                => ["s3eu"],
          "help_center_store"               => ["s3eu"],
          "inbound_email_store"             => ["s3eu"],
          "inbound_email_attachments_store" => ["s3eu"],
          "voyager_store"                   => ["s3eu"]
        }
      },
      3 => {
        "aws_regions" => {
          "csv_export_store"                => [],
          "help_center_store"               => [],
          "inbound_email_store"             => [],
          "inbound_email_attachments_store" => [],
          "voyager_store"                   => []
        }
      }
    }
  }

  before do
    # Pod 1's remote_files.yml
    @remote_files_yml = {
      'inbound_email' => {
        's3' => {
          'directory'             => 'test_bucket_1',
          'aws_access_key_id'     => 'x',
          'aws_secret_access_key' => 'y',
          'region'                => 'something-east-1',
          'primary'               => true
        },
      },
      'inbound_email_attachments' => {
        's3' => {
          'directory'             => 'test_bucket_2',
          'aws_access_key_id'     => 'x',
          'aws_secret_access_key' => 'y',
          'region'                => 'something-east-1',
          'primary'               => true
        }
      }
    }

    @job = create_job(account, Zendesk::Maintenance::Jobs::RemoveS3InboundEmailJob)
    @job.stubs(:config).returns(@remote_files_yml)
  end

  around do |test|
    Zendesk::Configuration.override(zendesk_yml) do
      test.call
    end
  end

  describe "#delete_category" do
    it 'calls deletion api' do
      Aws::S3::Client.any_instance.expects(:list_objects).
        with(bucket: 'test_bucket_1', prefix: "#{account.id}/").
        returns([stub(contents: [stub(key: "#{account.id}/xyz")])])

      Aws::S3::Client.any_instance.expects(:delete_objects).
        with(bucket: 'test_bucket_1', delete: {objects: [{key: "#{account.id}/xyz"}]})

      @job.send(:delete_category, 'inbound_email')
    end

    # https://github.com/aws/aws-sdk-ruby/issues/1114
    it 'does not call delete with empty keys' do
      Aws::S3::Client.any_instance.expects(:list_objects).
        with(bucket: 'test_bucket_1', prefix: "#{account.id}/").
        returns([stub(contents: [])])
      Aws::S3::Client.any_instance.expects(:delete_objects).never

      @job.send(:delete_category, 'inbound_email')
    end
  end

  describe "#verify_category" do
    it "works when everything is deleted" do
      Aws::S3::Client.any_instance.expects(:list_objects).
        with(bucket: 'test_bucket_1', prefix: "#{account.id}/").
        returns(stub(contents: []))

      @job.send(:verify_category, 'inbound_email')
    end

    it "fails when objects are left behind" do
      Aws::S3::Client.any_instance.expects(:list_objects).
        with(bucket: 'test_bucket_1', prefix: "#{account.id}/").
        returns(stub(contents: [stub(key: "#{account.id}/xyz")]))

      assert_raises RuntimeError do
        @job.send(:verify_category, 'inbound_email')
      end
    end
  end

  describe "#erase_for_canceled" do
    it 'deletes inbound_email and inbound_email_attachments for cancelled accounts' do
      @job.expects(:delete_category).with('inbound_email')
      @job.expects(:delete_category).with('inbound_email_attachments')
      @job.send(:erase_for_canceled)
    end
  end

  describe "#erase_for_moved" do
    it 'deletes inbound_email and inbound_email_attachments for accounts moved cross region' do
      @job.stubs(:different_stores?).returns(true)
      @job.expects(:delete_category).with('inbound_email')
      @job.expects(:delete_category).with('inbound_email_attachments')
      @job.send(:erase_for_moved)
    end

    it 'does not delete inbound_email and inbound_email_attachments for accounts moved inter region' do
      @job.stubs(:different_stores?).returns(false)
      @job.expects(:delete_category).never
      @job.send(:erase_for_moved)
    end
  end

  describe "#verify_for_canceled" do
    it 'verifies all categories' do
      @job.expects(:verify_category).with('inbound_email')
      @job.expects(:verify_category).with('inbound_email_attachments')
      @job.send(:verify_for_canceled)
    end
  end

  describe "#verify_for_moved" do
    it 'verifies all categories' do
      @job.stubs(:different_stores?).returns(true)
      @job.expects(:verify_category).with('inbound_email')
      @job.expects(:verify_category).with('inbound_email_attachments')
      @job.send(:verify_for_moved)
    end

    it "does not verify when not moved" do
      @job.stubs(:different_stores?).returns(false)
      @job.expects(:verify_category).never
      @job.send(:verify_for_moved)
    end
  end

  describe "#verify_primary_stores" do
    let(:pod) { '2' }

    it "gets the primary store for all categories" do
      @job.expects(:primary_store).with(pod, 'inbound_email').returns("s3").once
      @job.expects(:primary_store).with(pod, 'inbound_email_attachments').returns("s3").once

      @job.send(:verify_primary_stores, pod)
    end

    describe "when the categories have different primary stores" do
      before do
        @job.expects(:primary_store).with(pod, 'inbound_email').returns("s3").once
        @job.expects(:primary_store).with(pod, 'inbound_email_attachments').returns("s3eu1").once
      end

      it { assert_raises(ArgumentError) { @job.send(:verify_primary_stores, pod) } }
    end
  end

  describe "#primary_store" do
    let(:pod) { 2 }
    let(:category) { 'inbound_email' }

    it "detects the primary store" do
      store = @job.send(:primary_store, pod, category)
      assert_equal store, "s3eu"
    end

    describe "when there are no primary stores" do
      let(:pod) { 3 }
      it { assert_raises(ArgumentError) { @job.send(:primary_store, pod, category) } }
    end
  end

  describe "#store_details" do
    let(:store) { "s3" }
    let(:category) { 'inbound_email' }

    it "retrieves the store details" do
      store_details = @job.send(:store_details, store, category)

      assert store_details
      assert_equal store_details.fetch('directory'), 'test_bucket_1'
      assert_equal store_details.fetch('aws_access_key_id'), 'x'
      assert_equal store_details.fetch('aws_secret_access_key'), 'y'
      assert_equal store_details.fetch('region'), 'something-east-1'
      assert_equal store_details.fetch('primary'), true
    end
  end

  describe "#connection" do
    let(:category) { 'inbound_email' }
    let(:expected_store) do
      {
        access_key_id:      'x',
        secret_access_key:  'y',
        region:             'something-east-1'
      }
    end

    it "connects to AWS using the primary store" do
      Aws::S3::Client.expects(:new).with(expected_store)
      @job.send(:connection, category)
    end
  end

  describe "#bucket_name" do
    let(:category) { 'inbound_email' }

    it "retrieves the directory from the primary store" do
      assert_equal @job.send(:bucket_name, category), "test_bucket_1"
    end
  end

  describe "#different_stores?" do
    describe "when source and target are using the same store" do
      before do
        @job.stubs(pod_to_delete: 1)
        @job.stubs(pod_to_keep: 1)
      end

      it { refute @job.send(:different_stores?) }
    end

    describe "when source and target have different store" do
      before do
        @job.stubs(pod_to_delete: 1)
        @job.stubs(pod_to_keep: 2)
      end

      it { assert @job.send(:different_stores?) }
    end
  end

  describe '#pod_to_delete' do
    let(:a_shard) { mock }
    before { @job.stubs(shard_to_delete: a_shard) }

    it 'retrieves the pod' do
      @job.expects(:pod).with(a_shard).once
      @job.send(:pod_to_delete)
    end
  end

  describe '#pod_to_keep' do
    it 'retrieves the pod' do
      @job.expects(:pod).with(account.shard_id).once
      @job.send(:pod_to_keep)
    end
  end
end
