require_relative '../../../../support/job_helper'

SingleCov.covered!

describe 'RemoveShardDataJob' do
  include DataDeletionJobSupport

  fixtures :all

  before do
    @job = create_job(accounts(:minimum), Zendesk::Maintenance::Jobs::RemoveShardDataJob, interval: 0)
    @job.account.update_attribute :deleted_at, Time.now
    @job.stubs(:sleep)
  end

  after do
    @job.account.update_attribute :deleted_at, nil
  end

  describe '#erase_for_canceled' do
    subject { @job.send(:erase_for_canceled) }

    describe 'if kill switch is enabled' do
      before do
        Arturo.enable_feature! :account_deletion_kill_switch
      end

      it 'raises kill-switch error' do
        assert_raises Zendesk::DataDeletion::AccountDeletionKillSwitch::KillSwitchEnabled do
          subject
        end
      end
    end

    describe 'if account is not deleted' do
      before do
        @job.account.update_attribute :deleted_at, nil
      end

      it 'will raise an exception' do
        err = assert_raises RuntimeError do
          subject
        end
        err.message.must_equal 'can\'t delete data from non soft deleted accounts on the current shard'
      end
    end

    describe 'if in yucky dry_run mode' do
      before do
        @job = create_job(accounts(:minimum), Zendesk::Maintenance::Jobs::RemoveShardDataJob, interval: 0, dry_run: true)
        @job.stubs(:sleep)
      end

      it 'should not delete a thing' do
        @job.expects(:delete_table_data).never
        @job.expects(:remove_spokes).never
        @job.expects(:delete_account_ids).never
        subject
      end
    end

    it 'deletes data from sharded tables' do
      num_users = @job.account.users.count(:all)
      assert_difference -> { @job.account.reload.users.count(:all) }, (-1 * num_users) do
        subject
      end
    end

    it 'deletes account from account_ids table' do
      # paranoia is not always a bad thing
      count = ActiveRecord::Base.connection.execute("select count(*) from account_ids where id = #{@job.account.id}").first.first
      assert_equal 1, count

      subject

      count = ActiveRecord::Base.connection.execute("select count(*) from account_ids where id = #{@job.account.id}").first.first
      assert_equal 0, count
    end

    it 'removes spokes for account being deleted' do
      # support becomes a spoke account for @job.account
      subscription = Account.with_deleted { Account.find(1).subscription }
      subscription.update_column(:hub_account_id, @job.account.id)

      subject

      subscription.reload
      Subscription.unscoped do
        assert_nil subscription.hub_account_id
      end
    end
  end

  describe '#erase_for_moved' do
    # this assumes only shard 1 is valid in test config (which is true as of writing)
    let(:account_old_shard_id) { 1 }
    let(:account_new_shard_id) { 2 }

    subject { @job.send(:erase_for_moved) }

    # convert from a cancel audit to move audit means changing the reason and explicitly assigning a shard_id to the audit
    before do
      # job/audit attributes are not responsive to `update_attributes`
      @job.job_audit.audit.update_attribute(:reason, 'moved')
      @job.account.update_attribute(:deleted_at, nil)

      # shard_id is duplicated in the job and audit table :(
      @job.job_audit.update_attribute(:shard_id, account_old_shard_id)
      @job.job_audit.audit.update_attribute(:shard_id, account_old_shard_id)

      # this is the normative case, where exodus has changed the account's shard_id prior to enqueuing an audit
      # (note that shard 2 is not a valid shard in test)
      @job.account.update_attribute(:shard_id, account_new_shard_id)
    end

    describe 'when called on a non-account shard' do
      it 'should delete data from audit shard' do
        @job.shard_to_delete.must_equal(account_old_shard_id)

        ActiveRecord::Base.on_shard(@job.shard_to_delete) do
          num_users = @job.account.users.count(:all)
          assert_difference -> { @job.account.reload.users.count(:all) }, (-1 * num_users) do
            subject
          end
        end
      end

      describe 'if in yucky dry_run mode' do
        before do
          @job = create_job(accounts(:minimum), Zendesk::Maintenance::Jobs::RemoveShardDataJob, interval: 0, dry_run: true)
          @job.stubs(:sleep)
        end

        it 'should not delete a thing' do
          @job.expects(:delete_table_data).never
          @job.expects(:remove_spokes).never
          @job.expects(:delete_account_ids).never
          subject
        end
      end
    end

    describe 'when called while active shard is the account shard-id' do
      before do
        @job.account.update_attribute(:shard_id, account_old_shard_id)
      end

      it 'will raise an exception' do
        ActiveRecord::Base.on_shard(@job.shard_to_delete) do
          err = assert_raises RuntimeError do
            subject
          end
          err.message.must_equal "Do not clean up the accounts current shard."
        end
      end
    end
  end

  describe '#verify_for_canceled' do
    subject { @job.send(:verify_for_canceled) }

    before do
      @job.send :erase_for_canceled
    end

    it 'passes when data is deleted' do
      subject
    end

    it 'fails verify when all data is not deleted' do
      Category.new(account: @job.account, name: 'test').save!
      assert_raises RuntimeError do
        subject
      end
    end
  end

  describe '#find_tables' do
    subject { @job.send(:find_tables) }

    it 'will not include tables without account_id column' do
      subject.wont_include 'schema_migrations'
    end

    it 'will not include permanent_tables' do
      @job.send(:permanent_tables).each do |pt|
        subject.wont_include pt
      end
    end

    it 'will include tables with account_id column' do
      subject.must_include 'tickets'
    end
  end

  describe '#delete_table_data' do
    let(:table) { 'tickets' }

    subject { @job.send(:delete_table_data, table) }

    describe 'when there are multiple batches' do
      it 'will delete until there are no more rows' do
        @job.stubs(:delete_rows).with(table).returns(50, 0)
        @job.expects(:warn_maxwell).twice
        @job.expects(:sleep_to_database_backoff).twice

        subject
      end
    end

    describe 'when passed an invalid table name (if this is even possible)' do
      let(:table) { 'foo!' }

      it 'will raise if table name is invalid (though doubtful this can happen)' do
        err = assert_raises RuntimeError do
          subject
        end
        err.message.must_equal("Invalid table name specified for delete: '#{table}'")
      end
    end
  end

  describe '#delete_one_batch' do
    let(:table) { 'tickets' }
    let(:batch_size) { ::Zendesk::Maintenance::Jobs::RemoveShardDataJob::DEFAULT_BATCH }

    subject { @job.send(:delete_one_batch, table) }

    # method to avoid memoization of query result
    def account_move_shard_change
      ActiveRecord::Base.connection.select_one(
        "select account_id, shard_id from account_move_shard_changes where account_id = #{@job.account.id}"
      )
    end

    describe 'if kill switch is enabled' do
      before do
        Arturo.enable_feature! :account_deletion_kill_switch
      end

      it 'raises kill-switch error' do
        assert_raises Zendesk::DataDeletion::AccountDeletionKillSwitch::KillSwitchEnabled do
          subject
        end
      end
    end

    it 'times db ops' do
      @job.statsd_client.expects(:time).with('delete_rows', tags: ["table:#{table}"])
      @job.statsd_client.expects(:time).with('warn_maxwell')
      subject
    end

    it 'uses default batch size for the delete' do
      connection = Object.new
      connection.expects(:delete).with(regexp_matches(%r{.* LIMIT #{batch_size}.*}))
      connection.stubs(:insert)
      @job.stubs(:connection).returns(connection)

      subject
    end

    describe 'when called for a cancel audit' do
      it 'informs maxwell that audit is for a canceled account' do
        subject
        account_move_shard_change.must_equal('account_id' => @job.account.id, 'shard_id' => -1)
      end
    end

    describe 'when called for a move audit' do
      before do
        @job.job_audit.audit.update_attributes(reason: 'moved', shard_id: @job.account.shard_id == 1 ? 2 : 1)
      end

      it 'informs maxwell that audit is for a moved account' do
        subject
        account_move_shard_change.must_equal('account_id' => @job.account.id, 'shard_id' => @job.account.shard_id)
      end
    end
  end

  describe '#database_backoff' do
    describe 'when over re-enqueue threshold' do
      it 'raises a TemporaryExit' do
        Zendesk::DatabaseBackoff.any_instance.stubs(:backoff_duration).
          returns(Zendesk::Maintenance::Jobs::RemoveShardDataJob::REENQUEUE_THRESHOLD + 1)
        assert_raises(Zendesk::Maintenance::Jobs::TemporaryExit) { @job.send(:sleep_to_database_backoff) }
      end
    end

    describe 'when under minimum threshold' do
      it 'sleeps' do
        @job.expects(:sleep).with(Zendesk::Maintenance::Jobs::RemoveShardDataJob::MINIMUM_BACKOFF)
        @job.send :sleep_to_database_backoff
      end
    end

    describe 'when over minimum threshold' do
      it 'sleeps in bursts of 1 seconds' do
        Zendesk::DatabaseBackoff.any_instance.stubs(:backoff_duration).returns(5.1)
        @job.expects(:sleep).with(5.1 - 5.0) # This is _almost_ 0.1
        @job.expects(:sleep).with(1.0).times(5)
        @job.send :sleep_to_database_backoff
      end

      it 'stops sleeping if updated backoff is lt. already-slept duration' do
        Zendesk::DatabaseBackoff.any_instance.stubs(:backoff_duration).returns(5.0, 4.0, 2.0)
        @job.expects(:sleep).with(1.0).at_most(2)
        @job.send :sleep_to_database_backoff
      end

      it 'sends backoff to datadog' do
        Zendesk::DatabaseBackoff.any_instance.stubs(:backoff_duration).returns(5.1, 4.2, 0.5)
        @job.statsd_client.expects(:gauge).with('backoff_duration', 5.1, tags: ['when:on_sleep'])
        @job.statsd_client.expects(:gauge).with('backoff_duration', 4.2, tags: ['when:mid_sleep'])
        @job.statsd_client.expects(:gauge).with('backoff_duration', 0.5, tags: ['when:mid_sleep'])
        @job.stubs(:sleep)
        @job.send :sleep_to_database_backoff
      end
    end
  end

  describe '#maxwell_shard_id' do
    subject { @job.send(:maxwell_shard_id) }

    describe 'when cancelled reason' do
      it 'should return -1' do
        @job.job_audit.audit.update_attribute(:reason, 'canceled')
        assert_equal -1, subject
      end
    end

    describe 'when moved reason' do
      it 'should return -1' do
        @job.job_audit.audit.update_attribute(:reason, 'moved')
        assert_equal @job.account.shard_id, subject
      end
    end
  end
end
