require_relative '../../../../support/job_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::RemoveLegionDataJob do
  include DataDeletionJobSupport

  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let!(:job) { create_job(account, Zendesk::Maintenance::Jobs::RemoveLegionDataJob) }

  let(:delete_response) do
    OpenStruct.new(status: 200, body: { 'deleted_items' => 10 }, headers: { 'Backoff' => '0.2' })
  end

  let(:delete_response_last_page) do
    OpenStruct.new(status: 200, body: { 'deleted_items' => 0 }, headers: {'Backoff' => '0' })
  end

  describe 'reason = moved' do
    before do
      job.stubs(:pod_local_move?).returns(pod_local)

      job.account.update_attribute(:shard_id, 2)
      job.job_audit.audit.reason = 'moved'
      job.job_audit.shard_id = 1
    end

    let(:pod_local) { false }

    describe 'inter-pod move' do
      it 'runs the job' do
        job.work
      end
    end

    describe 'intra-pod move' do
      let(:pod_local) { true }

      it 'noops' do
        job.work
      end
    end
  end

  describe 'account canceled' do
    before do
      job.job_audit.stubs(:shard_id).returns(1)
      job.job_audit.audit.stubs(:reason).returns('canceled')
    end
  end

  describe 'dry_run' do
    it 'should never ask legion to delete an account' do
      job = create_job(account, Zendesk::Maintenance::Jobs::RemoveLegionDataJob, dry_run: true)
      job.work
    end
  end
end
