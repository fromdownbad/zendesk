require_relative "../../../../support/job_helper"

SingleCov.covered! uncovered: 9

describe 'AccountRiskAssessmentJob' do
  fixtures :accounts

  let(:job) { Zendesk::Maintenance::Jobs::AccountRiskAssessmentJob }
  let(:account) { accounts(:minimum) }

  let (:age_threshold) { Zendesk::Maintenance::Jobs::AccountRiskAssessmentJob::SAFE_ACCOUNT_AGE }
  let(:query_threshold) { Zendesk::Maintenance::Jobs::AccountRiskAssessmentJob::QUERY_AGE_THRESHOLD }

  let(:zuora_accounts) do
    [stub('zuora_account', id: "2c92a0fc5e09af91015e11ee72491f1c", account_number: "1980642", account_type__c: "Customer")]
  end

  let(:zuora_accounts_reseller) do
    [stub('zuora_account', id: "2c92a0fc5e09af91015e11ee72491f1c", account_number: "1980642", account_type__c: "Reseller Customer")]
  end

  let(:zuora_invoices) do
    [stub('zuora_invoice', id: "2c92a09a5e0d1b88015e123a80c725ce", account_id: "2c92a0fc5e09af91015e11ee72491f1c", adjustment_amount: "0", amount: "396", amount_without_tax: "396", balance: "0")]
  end

  describe ".work_on_shard" do
    before do
      job.stubs(:account_paid_invoice?).returns(false)
      job.stubs(:account_from_reseller?).returns(false)
      account = accounts(:minimum)
      Account.any_instance.stubs(:created_at).returns(1.day.ago)
      AccountSetting.where(account_id: account.id, name: 'risk_assessment').delete_all
      account.reload
      Arturo.enable_feature! :fraud_score_job_risk_assessment_daily_job
    end

    it "does not enqueue if risk assessment does not exist" do
      FraudScoreJob.expects(:enqueue_account).never
      job.work_on_shard(account.shard_id)
      account.reload
    end

    it "does not enqueue fraud score job if suspended" do
      account.settings.risk_assessment = ::Account::FraudSupport::SUSPEND_RISK_ASSESSMENT
      account.settings.save!
      job.work_on_shard(account.shard_id)
      account.reload
      FraudScoreJob.expects(:enqueue_account).never
    end

    it "does not enqueue fraud score job if whitelisted" do
      account.settings.risk_assessment = ::Account::FraudSupport::MONITOR_WHITELIST_RISK_ASSESSMENT
      account.settings.save!
      FraudScoreJob.expects(:enqueue_account).never
      job.work_on_shard(account.shard_id)
      account.reload
    end

    it "does not enqueue fraud score job if account less than safe age threshold" do
      account.settings.risk_assessment = ::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT
      account.settings.save!
      AccountSetting.where(account_id: account.id, name: 'risk_assessment').first.update_attribute('created_at', (age_threshold - 1).days.ago)
      FraudScoreJob.expects(:enqueue_account).never
      job.work_on_shard(account.shard_id)
      account.reload
    end

    it "does not enqueue fraud score job if account is not serviceable" do
      account.settings.risk_assessment = ::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT
      account.settings.save!
      AccountSetting.where(account_id: account.id, name: 'risk_assessment').first.update_attribute('created_at', (age_threshold - 1).days.ago)
      Account.any_instance.stubs(:is_serviceable).returns(false)
      FraudScoreJob.expects(:enqueue_account).never
      job.work_on_shard(account.shard_id)
      account.reload
    end

    it "does not enqueue fraud score job if older than query range" do
      account.settings.risk_assessment = ::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT
      account.settings.save!
      AccountSetting.where(account_id: account.id, name: 'risk_assessment').first.update_attribute('created_at', (query_threshold + 1).days.ago)
      FraudScoreJob.expects(:enqueue_account).never
      job.work_on_shard(account.shard_id)
      account.reload
      refute_equal account.settings.risk_assessment, ::Account::FraudSupport::SAFE_AGE_RISK_ASSESSMENT
    end

    it "enqueues fraud score job for daily job safe age if account greater than safe age threshold" do
      Account.any_instance.stubs(:created_at).returns((age_threshold + 1).days.ago)
      Rails.logger.expects(:info).with("Starting AccountRiskAssessmentJob for 1").once
      account.settings.risk_assessment = ::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT
      account.settings.save!
      FraudScoreJob.expects(:enqueue_account).with(account, Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB_SAFE_AGE)
      job.work_on_shard(account.shard_id)
      account.reload
    end

    it "enqueues fraud score job for daily job safe paid if account paid invoice when less than safe age threshold" do
      job.stubs(:account_paid_invoice?).returns(true)
      Rails.logger.expects(:info).with("Starting AccountRiskAssessmentJob for 1").once
      account.settings.risk_assessment = ::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT
      account.settings.save!
      AccountSetting.where(account_id: account.id, name: 'risk_assessment').first.update_attribute('created_at', (age_threshold - 1).days.ago)
      FraudScoreJob.expects(:enqueue_account).with(account, Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB_SAFE_PAID)
      job.work_on_shard(account.shard_id)
      account.reload
    end

    describe_with_arturo_disabled :fraud_score_job_risk_assessment_daily_job do
      describe "when account age is greater than safe age threshold" do
        before do
          Account.any_instance.stubs(:created_at).returns((age_threshold + 1).days.ago)
          account.settings.risk_assessment = ::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT
          account.settings.save!
        end

        it "directly performs fraud action" do
          FraudScoreJob.expects(:enqueue_account).with(account, Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB_SAFE_AGE).never
          job.work_on_shard(account.shard_id)
          assert_equal account.settings.reload.risk_assessment, ::Account::FraudSupport::SAFE_AGE_RISK_ASSESSMENT
          account.reload
        end
      end

      describe "account paid invoice and less than safe age threshold" do
        before do
          job.stubs(:account_paid_invoice?).returns(true)
          account.settings.risk_assessment = ::Account::FraudSupport::SUPPORT_RISKY_RISK_ASSESSMENT
          account.settings.save!
          AccountSetting.where(account_id: account.id, name: 'risk_assessment').first.update_attribute('created_at', (age_threshold - 1).days.ago)
        end

        it "directly performs fraud action" do
          FraudScoreJob.expects(:enqueue_account).with(account, Fraud::SourceEvent::RISK_ASSESSMENT_DAILY_JOB_SAFE_PAID).never
          job.work_on_shard(account.shard_id)
          assert_equal account.settings.reload.risk_assessment, ::Account::FraudSupport::SAFE_PAID_RISK_ASSESSMENT
          account.reload
        end
      end
    end
  end

  describe "#account_paid_invoice?" do
    before do
      job.stubs(:zuora_accounts).returns(zuora_accounts)
      job.stubs(:zuora_invoices).returns(zuora_invoices)
    end

    it "returns true if one zuora account and at least one invoice balance of zero" do
      job.work_on_shard(account.shard_id)
      assert job.send(:account_paid_invoice?)
    end

    it "returns false if no zuora accounts" do
      job.stubs(:zuora_accounts).returns([])
      refute job.send(:account_paid_invoice?)
    end

    it "returns false if multiple zuora accounts" do
      job.stubs(:zuora_accounts).returns(zuora_accounts + zuora_accounts)
      refute job.send(:account_paid_invoice?)
    end

    it "returns false if invoices contain no zero balances" do
      unpaid_invoice = [stub('invoice', id: "2c92a09a5e0d1b88015e123a80c725ce", account_id: "2c92a0fc5e09af91015e11ee72491f1c", adjustment_amount: "0", amount: "396", amount_without_tax: "396", balance: "15")]
      job.stubs(:zuora_invoices).returns(unpaid_invoice)
      refute job.send(:account_paid_invoice?)
    end

    it "returns false if error occurs" do
      error = Kragle::ResponseError
      job.stubs(:account_paid_invoice?).raises(error)
      assert_raises error do
        job.work_on_shard(account.shard_id)
        refute job.send(:account_paid_invoice?)
      end
    end
  end

  describe "#account_from_reseller??" do
    describe "when zuora account is from a reseller" do
      before { job.stubs(:zuora_accounts).returns(zuora_accounts_reseller) }

      it "returns true" do
        job.work_on_shard(account.shard_id)
        assert job.send(:account_from_reseller?)
      end
    end

    describe "when no zuora accounts exist" do
      before { job.stubs(:zuora_accounts).returns([]) }

      it "returns false" do
        refute job.send(:account_from_reseller?)
      end
    end

    describe "when error occurs" do
      before do
        @error = Kragle::ResponseError
        job.stubs(:account_from_reseller?).raises(@error)
      end

      it "rescues the error and returns false" do
        assert_raises @error do
          job.work_on_shard(account.shard_id)
          refute job.send(:account_from_reseller?)
        end
      end
    end
  end
end
