require_relative '../../../../support/job_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::UnsubscribeTwitterJob do
  include DataDeletionJobSupport

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:job) { create_job(account, Zendesk::Maintenance::Jobs::UnsubscribeTwitterJob) }

  def create_account_with_twitter!
    MonitoredTwitterHandle.create!(
        access_token: "<token>",
        secret: "<secret>",
        account: account,
        twitter_user_id: 1
      )
  end

  describe '#erase_for_canceled' do
    describe 'unsubscribe Twitter handle' do
      before do
        MonitoredTwitterHandle.where(account: account).delete_all
        Arturo.enable_feature! :channels_enable_twitter_events_endpoint
      end

      it 'invokes unsubscribe for each Twitter handle attached to the account' do
        create_account_with_twitter!
        Channels::TwitterAPI::TwitterProxy::InternalAPIClient.any_instance.expects(:unsubscribe).once
        job.send(:erase_for_canceled)
      end

      it 'did not invokes unsubscribe for account with no Twitter handles' do
        Channels::TwitterAPI::TwitterProxy::InternalAPIClient.any_instance.expects(:unsubscribe).never
        job.send(:erase_for_canceled)
      end
    end
  end
end
