require_relative "../../../../support/job_helper"

SingleCov.covered! uncovered: 4

describe 'AutomationJob' do
  include ArturoTestHelper
  fixtures :account_settings, :accounts

  def set_last_login_at(value) # rubocop:disable Naming/AccessorMethodName
    AccountSetting.where(name: "last_login").where("id <> ?", account_settings(:minimum_last_login).id).delete_all
    account_settings(:minimum_last_login).update_attribute(:value, value.to_s(:db))
  end

  describe "for an active unlocked account with recent login" do
    let(:account)      { accounts(:minimum) }
    let(:current_time) { Time.at(Time.now.to_i - (Time.now.to_i % 1.hour)) }

    before do
      set_last_login_at Time.now
      Timecop.freeze
      stub_occam_find([])
    end

    describe_with_arturo_enabled :pause_automation_execution do
      before { Zendesk::Maintenance::Jobs::AutomationJob.work }

      before_should "not fire automations" do
        Zendesk::Maintenance::Jobs::AutomationJob.expects(:process).with(account.id, current_time).never
      end
    end

    describe_with_arturo_disabled :pause_automation_execution do
      before { Zendesk::Maintenance::Jobs::AutomationJob.work }

      before_should "fire automations" do
        Zendesk::Maintenance::Jobs::AutomationJob.expects(:process).with(account.id, current_time)
      end
    end

    describe "with `shard_level_automation_job` arturo disabled" do
      before { Zendesk::Maintenance::Jobs::AutomationJob.work }

      before_should "not proccess at Shard level" do
        Zendesk::Maintenance::Jobs::AutomationJob.expects(:work_on_shard).never
      end
    end

    describe "with `shard_level_automation_job` arturo enabled" do
      before do
        Arturo.enable_feature!(:shard_level_automation_job)
      end

      describe 'when working' do
        before_should "re-enqueues AutomationJob with shard args" do
          Zendesk::Maintenance::Jobs::AutomationJob.expects(:enqueue).with('work_shard_id' => 1).once

          Zendesk::Maintenance::Jobs::AutomationJob.work
        end

        before_should "proccess at Shard level" do
          Zendesk::Maintenance::Jobs::AutomationJob.expects(:work_on_shard).with(1).at_least_once

          Zendesk::Maintenance::Jobs::AutomationJob.work
        end

        describe_with_arturo_enabled :pause_automation_execution do
          before { Zendesk::Maintenance::Jobs::AutomationJob.work }

          before_should "not fire automations" do
            Zendesk::Maintenance::Jobs::AutomationJob.expects(:process).with(account.id, current_time).never
          end
        end

        describe_with_arturo_disabled :pause_automation_execution do
          before { Zendesk::Maintenance::Jobs::AutomationJob.work }

          before_should "fire automations" do
            Zendesk::Maintenance::Jobs::AutomationJob.expects(:process).with(account.id, current_time)
          end
        end
      end
    end
  end

  describe "for an account without recent login " do
    let(:account) { accounts(:minimum) }

    before do
      set_last_login_at 20.days.ago
      Timecop.freeze
    end

    it "does not fire automation" do
      time = Time.at(Time.now.to_i - (Time.now.to_i % 1.hour))
      Zendesk::Maintenance::Jobs::AutomationJob.expects(:process).with(account.id, time).never
      Zendesk::Maintenance::Jobs::AutomationJob.work
    end

    describe "with `shard_level_automation_job` arturo enabled" do
      before do
        Arturo.enable_feature!(:shard_level_automation_job)
      end

      describe 'when working' do
        before_should "proccess at Shard level" do
          Zendesk::Maintenance::Jobs::AutomationJob.expects(:work_on_shard).with(1).at_least_once

          Zendesk::Maintenance::Jobs::AutomationJob.work
        end

        it "does not fire automation" do
          time = Time.at(Time.now.to_i - (Time.now.to_i % 1.hour))
          Zendesk::Maintenance::Jobs::AutomationJob.expects(:process).with(account.id, time).never
          Zendesk::Maintenance::Jobs::AutomationJob.work
        end
      end
    end
  end

  describe "when there is an exception" do
    let(:exception) { SystemExit.new }

    before do
      Zendesk::Maintenance::Jobs::AutomationJob.stubs(:available_accounts).raises(exception)
    end

    it "logs exception" do
      Zendesk::Maintenance::Jobs::AutomationJob.expects(:resque_error).with("Unable to finish AutomationJob: #{exception.inspect}")

      assert_raises SystemExit do
        Zendesk::Maintenance::Jobs::AutomationJob.work
      end
    end
  end
end
