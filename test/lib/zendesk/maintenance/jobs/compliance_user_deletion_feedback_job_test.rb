require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::ComplianceUserDeletionFeedbackJob do
  describe ".work" do
    before do
      Zendesk::PushNotifications::Gdpr::GdprFeedbackSubscriber.any_instance.expects(:process_queues)
    end

    it "should process_queues" do
      Zendesk::Maintenance::Jobs::ComplianceUserDeletionFeedbackJob.work
    end
  end
end
