require_relative '../../../../support/job_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::BaseDataDeletionParticipantJob do
  include DataDeletionJobSupport

  # used to test ADD participant
  class FakeDataDeletionParticipantJob < Zendesk::Maintenance::Jobs::BaseDataDeletionParticipantJob
    consul_service_name 'fake-data-deletion-participant'
    service_port '3000'
  end

  class FakeIntraPodDataDeletionParticipantJob < Zendesk::Maintenance::Jobs::BaseDataDeletionParticipantJob
    consul_service_name 'fake-data-deletion-participant'
    service_port '3000'

    def skip_on_intrapod_move?
      false
    end
  end

  fixtures :accounts
  let(:account) { accounts(:minimum) }

  def url(service, method_name)
    service_port = 3000
    "http://pod-1.#{service}.service.consul:#{service_port}/z/#{method_name}?account_id=#{account.id}"
  end

  def stub_calls(service, method_name, returns = [body: { done: true }])
    returns.each do |h|
      h[:headers] = { 'Content-Type' => 'application/json' }
      h[:body] = h[:body].to_json
    end

    stub_request(:post, url(service, method_name)).to_return(returns)
  end

  describe 'subclasses of BaseDataDeletionParticipantJob' do
    describe '.stage' do
      before do
        Zendesk::Maintenance::Jobs::TestParticipantJob = Class.new(Zendesk::Maintenance::Jobs::BaseDataDeletionParticipantJob)
      end

      subject { Zendesk::Maintenance::Jobs::TestParticipantJob.stage }

      it 'should return independent' do
        assert_equal :independent, subject
      end
    end

    describe '.queue' do
      before do
        Zendesk::Maintenance::Jobs::TestParticipantJob = Class.new(Zendesk::Maintenance::Jobs::BaseDataDeletionParticipantJob)
      end

      subject { Zendesk::Maintenance::Jobs::TestParticipantJob.instance_variable_get(:@queue) }

      it 'should return account_deletion' do
        assert_equal :account_deletion, subject
      end
    end
  end

  describe '.job_name' do
    subject { Zendesk::Maintenance::Jobs::BaseDataDeletionParticipantJob.job_name }

    it 'should return the formatted job name' do
      assert_equal 'base_data_deletion_participant_job', subject
    end
  end

  describe '.service_port' do
    describe 'when given invalid input' do
      it 'should raise an exception' do
        assert_raises(
          Zendesk::Maintenance::Jobs::BaseDataDeletionParticipantJob::InvalidServicePort
        ) { FakeDataDeletionParticipantJob.service_port('a025') }
      end
    end
  end

  describe '.consul_service_name' do
    it 'sets service_name' do
      assert_equal 'fake-data-deletion-participant', FakeDataDeletionParticipantJob.service_name
    end
  end

  describe '#erase_for_moved' do
    let(:method_name) { :erase_for_moved }
    subject { job.send(method_name) }

    describe 'for crosspod job' do
      let(:job) { create_job(account, FakeDataDeletionParticipantJob) }

      it 'should run job after crosspod moves' do
        job.stubs(:pod_local_move?).returns(false)
        stub_calls(job.class.service_name, method_name)
        subject
        assert_requested :post, url(job.class.service_name, method_name), times: 1
      end

      it 'should skip job after intrapod moves' do
        job.expects(:make_request).never
        subject
        assert_not_requested :post, url(job.class.service_name, method_name)
      end
    end

    describe 'for intrapod job' do
      let(:job) { create_job(account, FakeIntraPodDataDeletionParticipantJob) }

      it 'should run job after crosspod moves' do
        job.stubs(:pod_local_move?).returns(false)
        stub_calls(job.class.service_name, method_name)
        subject
        assert_requested :post, url(job.class.service_name, method_name), times: 1
      end

      it 'should run job after intrapod moves' do
        stub_calls(job.class.service_name, method_name)
        subject
        assert_requested :post, url(job.class.service_name, method_name), times: 1
      end
    end
  end

  describe '#erase_for_canceled' do
    let(:method_name) { :erase_for_canceled }
    subject { job.send(method_name) }

    describe 'for crosspod job' do
      let(:job) { create_job(account, FakeDataDeletionParticipantJob) }

      it 'should run job' do
        job.stubs(:pod_local_move?).returns(false)
        stub_calls(job.class.service_name, method_name)
        subject
        assert_requested :post, url(job.class.service_name, method_name), times: 1
      end
    end

    describe 'for intrapod job' do
      let(:job) { create_job(account, FakeIntraPodDataDeletionParticipantJob) }

      it 'should run job' do
        job.stubs(:pod_local_move?).returns(false)
        stub_calls(job.class.service_name, method_name)
        subject
        assert_requested :post, url(job.class.service_name, method_name), times: 1
      end
    end
  end

  describe '#delete_account' do
    let(:method_name) { :erase_for_moved }
    let(:job) { create_job(account, FakeDataDeletionParticipantJob) }
    subject { job.send(:delete_account, method_name) }

    describe 'when it responds with done' do
      it 'should run loop once and return' do
        job.stubs(:pod_local_move?).returns(false)
        stub_calls(job.class.service_name, method_name)
        subject
        assert_requested :post, url(job.class.service_name, method_name), times: 1
      end
    end

    describe 'when backoff is included' do
      it 'should backoff and complete' do
        backoff = 0.5
        stub_calls(job.class.service_name, method_name, [
                     { body: { done: false, backoff: backoff } },
                     { body: { done: false, backoff: 0.0 } },
                     { body: { done: true } }
                   ])

        job.expects(:sleep).with(backoff)

        subject
        assert_requested :post, url(job.class.service_name, method_name), times: 3
      end
    end
  end
end
