require_relative '../../../../support/test_helper'

SingleCov.covered! uncovered: 16

describe 'BaseDataDeleteJob' do
  include DataDeletionJobSupport

  class FakeDataDeletionJob < Zendesk::Maintenance::Jobs::BaseDataDeleteJob; end

  fixtures :accounts

  describe 'shard_to_delete' do
    let(:account) { accounts(:minimum) }
    before { @job = create_job(account, Zendesk::Maintenance::Jobs::BaseDataDeleteJob) }

    describe 'reason: canceled' do
      it 'returns the account shard id' do
        @job.send(:shard_to_delete).must_equal account.shard_id
      end

      it "doesn't return the audit shard id" do
        @job.job_audit.stubs(:shard_id).returns(99)
        assert account.shard_id != 99
        @job.send(:shard_to_delete).must_equal account.shard_id
      end

      it "succeeds if no audit shard_id is set" do
        @job.job_audit.stubs(:shard_id).returns(nil)
        @job.send(:shard_to_delete)
      end
    end

    describe 'reason: moved' do
      before do
        @job.job_audit.stubs(:reason).returns('moved')
      end

      it 'returns the audit shard id' do
        @job.job_audit.stubs(:shard_id).returns(99)
        @job.send(:shard_to_delete).must_equal 99
      end

      it "doesn't return the account shard id" do
        @job.job_audit.stubs(:shard_id).returns(99)
        assert account.shard_id != 99
        @job.send(:shard_to_delete).must_equal 99
      end

      it "fails if no audit shard id provided" do
        @job.job_audit.stubs(:shard_id).returns(nil)
        assert_raise RuntimeError do
          @job.send(:shard_to_delete)
        end
      end
    end

    describe 'reason: other' do
      before do
        @job.job_audit.stubs(:reason).returns('obscequiessence')
      end

      it 'fails' do
        @job.job_audit.stubs(:shard_id).returns(99)
        assert_raise RuntimeError do
          @job.send(:shard_to_delete)
        end
      end
    end
  end

  describe 'When removing data for an account' do
    let(:account) { accounts(:minimum) }

    before do
      @job = create_job(account, Zendesk::Maintenance::Jobs::BaseDataDeleteJob)

      @job.stubs(:sleep) # Prevent long running tests because of the retry schedule

      Zendesk::Maintenance::Jobs::BaseDataDeleteJob.stubs(:erase)
      Zendesk::Maintenance::Jobs::BaseDataDeleteJob.stubs(:verify)
      Zendesk::Maintenance::Jobs::BaseDataDeleteJob.stubs(:dry_run?).returns(false)

      # This would be ideal to integrate, but transactional_fixtures does not support threads and causes MySQL deadlock
      Zendesk::DataDeletion::JobHeartbeater.any_instance.stubs(:with_poll).yields
    end

    it 'records deletion time metric' do
      @job.job_audit.audit.update_attribute(:started_at, 2.days.ago)

      Zendesk::StatsD::Client.any_instance.expects(:distribution).
        with('deletion_time', 2, tags: ["job:#{@job.job_audit.job}"]).
        once
      @job.work
    end

    it 'appends rails logger with account_id and subdomain' do
      @job.work
      assert_equal(Rails.logger.attributes[:zendesk][:account_id], account.id)
      assert_equal(Rails.logger.attributes[:zendesk][:subdomain], account.subdomain)
    end

    it 'does not throw errors if a timestamp is missing for calculating deletion time metric' do
      @job.job_audit.audit.update_attribute(:started_at, nil)

      Zendesk::StatsD::Client.any_instance.expects(:distribution).never
      @job.work
    end

    it 'does not delete if has arturo prevent_deletion_if_churned on' do
      Account.any_instance.stubs(:has_prevent_deletion_if_churned?).returns(true)
      error = -> { @job.work }.must_raise(Zendesk::DataDeletion::PermanentError)
      error.message.must_equal 'Account deletion prevented by arturo feature: prevent_deletion_if_churned'
    end

    describe 'query comments' do
      before do
        Datadog.stubs(:tracer).returns(stub(active_span: stub(trace_id: 123)))
      end

      it 'adds comments to each SQL statement' do
        comment = {
          service: 'classic-resque',
          resource_name: 'BaseDataDeleteJob',
          account_id: account.id,
          trace_id: 123,
          shard: account.shard_id
        }

        ActiveRecord::Comments.expects(:comment).with(comment.to_json).once
        @job.work
      end

      it 'sets resource_name based on the child class name' do
        @job = create_job(account, FakeDataDeletionJob)

        comment = {
          service: 'classic-resque',
          resource_name: 'FakeDataDeletionJob',
          account_id: account.id,
          trace_id: 123,
          shard: account.shard_id
        }

        ActiveRecord::Comments.expects(:comment).with(comment.to_json).once
        @job.work
      end
    end

    describe "kill switch usage" do
      before { Arturo.enable_feature! :account_deletion_kill_switch }
      it 'does not delete if the kill switch has been turned on' do
        @job.expects(:erase_for_canceled).never
        @job.work
      end

      it 'does not raise for kill switch' do
        @job.work
      end
    end

    it 'deletes if has arturo prevent_deletion_if_churned off' do
      Account.any_instance.stubs(:has_prevent_deletion_if_churned?).returns(false)
      @job.work
    end

    it 'updates the audit when the job starts' do
      DataDeletionAuditJob.any_instance.expects(:start!)
      @job.work
    end

    it 'updates the audit if job completes' do
      DataDeletionAuditJob.any_instance.expects(:finish!)
      @job.work
    end

    it 'deletes failed jobs once complete' do
      audit = @job.job_audit.audit
      Timecop.freeze(Time.now - 1.hour) do
        @failed_job = audit.build_job_audit_for_klass(Zendesk::Maintenance::Jobs::BaseDataDeleteJob).tap do |job_audit|
          job_audit.status = 'failed'
          job_audit.save!
        end
      end

      @job.work

      job_audits = audit.job_audits.where(job: 'Zendesk::Maintenance::Jobs::BaseDataDeleteJob')
      assert_equal 1, job_audits.count
      assert job_audits.first.finished?
    end

    it 'fails the audit and raises if the job fails on StandardError' do
      @job.expects(:erase_for_canceled).once.raises(StandardError)
      @job.job_audit.expects(:fail!)

      assert_raise StandardError do
        @job.work
      end
    end

    it 'fails the audit if the job fails on RuntimeError' do
      @job.expects(:erase_for_canceled).once.raises(RuntimeError)
      @job.job_audit.expects(:fail!)
      @job.expects(:handle_runtime_exception)
      @job.work
    end

    it 'saves the error if a job fails' do
      @job.expects(:erase_for_canceled).once.raises(StandardError)

      assert_nil @job.job_audit.error
      assert_raises(StandardError) { @job.work }
      refute_nil @job.job_audit.error
    end

    it 'fails if given an audit of a different class' do
      @job.job_audit.stubs(:job).returns('Zendesk::Maintenance::Jobs::SomeOtherDeleteJob')
      assert_raise Zendesk::Maintenance::Jobs::InvalidJobError do
        @job.work
      end
    end

    it 'performs verify task on a retry schedule' do
      @job.expects(:verify_for_canceled).times(5).raises(StandardError)
      assert_raise StandardError do
        @job.work
      end
    end

    it 'breaks from retry schedule on success' do
      @job.expects(:verify_for_canceled).times(1)
      @job.work
    end
  end

  describe '#record_deletion_time' do
    let(:account) { accounts(:minimum) }

    before do
      @audit = account.data_deletion_audits.build_for_cancellation
      @audit.save!
    end

    let(:deletion_duration) { rand(1..40).days }

    it 'should record the deletion time from the initial job' do
      Timecop.freeze

      job_audit = @audit.build_job_audit_for_klass(FakeDataDeletionJob).tap(&:save!)
      job_audit.finish!
      @audit.update_attribute(:started_at, Time.now - deletion_duration)
      job = FakeDataDeletionJob.new(job_audit.id)
      Timecop.return

      Zendesk::StatsD::Client.any_instance.expects(:distribution).with('deletion_time', deletion_duration / 1.day, tags: ["job:#{job.class}"]).once
      job.record_deletion_time
    end
  end
end
