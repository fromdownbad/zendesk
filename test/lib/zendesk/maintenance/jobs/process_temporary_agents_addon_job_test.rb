require_relative '../../../../support/job_helper'

SingleCov.covered! uncovered: 1

describe Zendesk::Maintenance::Jobs::ProcessTemporaryAgentsAddonJob do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:job_class) { Zendesk::Maintenance::Jobs::ProcessTemporaryAgentsAddonJob }

  before do
    job_class.stubs(:shard_id).returns(1)
  end

  describe 'unstarted active temporary_zendesk_agents add-on' do
    before do
      account.subscription_feature_addons.create!(
        temporary_agent_addon_attributes(zuora_rate_plan_id: 'active_unstarted')
      )
    end

    it 'saves subscripiton & sets the status_id to SubscriptionFeatureAddon::STATUS[:started]' do
      Subscription.any_instance.expects(:save!)
      assert_nil account.subscription_feature_addons.first.status_id
      job_class.work
      assert_equal SubscriptionFeatureAddon::STATUS[:started], account.subscription_feature_addons.first.status_id
    end
  end

  describe 'started active temporary_zendesk_agents add-on' do
    before do
      account.subscription_feature_addons.create!(
        temporary_agent_addon_attributes(
          zuora_rate_plan_id: 'active_started',
          status_id:          SubscriptionFeatureAddon::STATUS[:started]
        )
      )
    end

    it 'leave the addon as is' do
      Subscription.any_instance.expects(:save!).never
      SubscriptionFeatureAddon.any_instance.expects(:update_column).never
      assert_equal SubscriptionFeatureAddon::STATUS[:started], account.subscription_feature_addons.first.status_id
      job_class.work
    end
  end

  describe 'started expired unprocessed temporary_zendesk_agents add-on' do
    before do
      account.subscription_feature_addons.create!(
        temporary_agent_addon_attributes(
          zuora_rate_plan_id: 'expired_unprocessed',
          expires_at:         30.days.ago,
          status_id:          SubscriptionFeatureAddon::STATUS[:started]
        )
      )
    end

    it 'saves subscripiton & sets the status_id to SubscriptionFeatureAddon::STATUS[:started]' do
      Subscription.any_instance.expects(:save!)
      assert_equal SubscriptionFeatureAddon::STATUS[:started], account.subscription_feature_addons.first.status_id
      job_class.work
      assert_equal SubscriptionFeatureAddon::STATUS[:expired], account.subscription_feature_addons.first.status_id
    end
  end

  describe 'started expired processed temporary_zendesk_agents add-on' do
    before do
      account.subscription_feature_addons.create!(
        temporary_agent_addon_attributes(
          zuora_rate_plan_id: 'started_expired_processed',
          expires_at:         30.days.ago,
          status_id:          SubscriptionFeatureAddon::STATUS[:expired]
        )
      )
    end

    it 'leave the addon as is' do
      Subscription.any_instance.expects(:save!).never
      SubscriptionFeatureAddon.any_instance.expects(:update_column).never
      assert_equal SubscriptionFeatureAddon::STATUS[:expired], account.subscription_feature_addons.first.status_id
      job_class.work
    end
  end

  def temporary_agent_addon_attributes(options = {})
    {
      name:               'temporary_zendesk_agents',
      zuora_rate_plan_id: options.fetch(:zuora_rate_plan_id, 'test'),
      quantity:           10,
      starts_at:          options.fetch(:starts_at, 60.days.ago),
      expires_at:         options.fetch(:expires_at, 60.days.from_now),
      status_id:          options.fetch(:status_id, nil)
    }
  end
end
