require_relative "../../../../support/job_helper"
require 'zendesk/maintenance/jobs/api_account_signup_cleanup_job'

SingleCov.covered! uncovered: 1

describe 'APIAccountSignupCleanupJob' do
  fixtures :accounts, :users, :user_identities, :subscriptions

  describe 'running the API Account Signup Cleanup Job' do
    before do
      @account = accounts(:minimum)
      @owner   = @account.owner
      assert @account.is_active?, "Fixture error!"
      assert @owner.is_active?, "Fixture error!"
      assert @owner.identities.first.is_verified?, "Fixture error!"
      assert_equal UserEmailIdentity.to_s, @owner.identities.first.type
      I18n.stubs(:t).returns("foo")
    end

    describe '25 hours after a user created an account and since verified their email address' do
      before do
        assert_equal 1, @owner.identities.length, "Fixture error!"
        assert @owner.identities.first.is_verified, "Fixture error!"
        Timecop.travel(@account.created_at + 25.hours)
        Zendesk::Maintenance::Jobs::APIAccountSignupCleanupJob.execute
      end

      it "does not delete the owner's password" do
        assert @owner.reload.crypted_password.present?
      end
    end

    describe '20 hours after a user who has not verified their email address created an account via Blackberry' do
      before do
        assert_equal 1, @owner.identities.length, "Fixture error!"
        @owner.identities.first.update_attribute(:is_verified, false)
        Timecop.travel(@account.created_at + 20.hours)
        @account.update_attribute(:source, Zendesk::Accounts::Source::BLACKBERRY)
        Zendesk::Maintenance::Jobs::APIAccountSignupCleanupJob.execute
      end

      it "does not delete the owner's password" do
        assert @owner.reload.crypted_password.present?
      end
    end

    describe '25 hours after a user who has not verified their email address created an account' do
      before do
        assert_equal 1, @owner.identities.length, "Fixture error!"
        @owner.identities.first.update_attribute(:is_verified, false)
        Timecop.travel(@account.created_at + 25.hours)
      end

      describe 'via the web' do
        before do
          @account.update_attribute(:source, 'Web')
          Zendesk::Maintenance::Jobs::APIAccountSignupCleanupJob.execute
        end

        it "does not delete the owner's password" do
          assert @owner.reload.crypted_password.present?
        end
      end

      describe 'via Blackberry' do
        before do
          @account.update_attribute(:source, Zendesk::Accounts::Source::BLACKBERRY)
          Zendesk::Maintenance::Jobs::APIAccountSignupCleanupJob.execute
          @owner.reload
        end

        it "deletes the owner's password" do
          assert @owner.crypted_password.blank?, @owner.attributes_before_type_cast.inspect
        end
      end

      describe 'skip shell accounts without Support product' do
        before do
          @shell_account = accounts(:shell_account_without_support)
          @shell_owner = @shell_account.owner
          @shell_owner.identities.first.update_attribute(:is_verified, false)
          @shell_account.update_attribute(:source, Zendesk::Accounts::Source::BLACKBERRY)
          Timecop.travel(@shell_account.created_at + 25.hours)

          assert @shell_account.is_active?
          assert_equal Zendesk::Accounts::Locking::LockState::OPEN, @shell_account.lock_state
          Zendesk::Maintenance::Jobs::APIAccountSignupCleanupJob.execute
          @shell_owner.reload
        end

        it 'does not delete owner password' do
          refute @shell_owner.crypted_password.blank?
        end
      end
    end
  end
end
