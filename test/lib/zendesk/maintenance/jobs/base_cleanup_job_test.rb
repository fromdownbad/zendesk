require_relative "../../../../support/job_helper"

SingleCov.covered! uncovered: 1

describe 'BaseCleanupJob' do
  describe "#work" do
    before do
      evaluated = ERB.new(File.read(Rails.root + 'config/resque-schedule.yml')).result
      resque_schedule = YAML.load(evaluated)

      # find all daily jobs from the schedule
      @daily_class_names = resque_schedule.map { |_k, v| v["args"] if v["cron"].match?(/0 \d?? \* \* \*/) }.compact
      @hourly_class_names = resque_schedule.map { |_k, v| v["args"] if v["cron"].match?(/\d?? \* \* \* \*/) }.compact
    end

    it "fires a bunch of cleanup jobs daily" do
      [AlertDismissal, Photo, ExpirableAttachment, TicketJoin, Activity, SuspendedTicket].each do |klass|
        klass.expects(:cleanup)
      end

      # run them
      @daily_class_names.each do |class_name|
        Zendesk::Maintenance::Jobs::BaseCleanupJob.work(class_name)
      end
    end

    it "fires a bunch of cleanup jobs hourly" do
      [Token, InboundEmail].each do |klass|
        klass.expects(:cleanup)
      end

      # run them
      @hourly_class_names.each do |class_name|
        Zendesk::Maintenance::Jobs::BaseCleanupJob.work(class_name)
      end
    end
  end
end
