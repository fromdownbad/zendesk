require_relative "../../../../support/job_helper"

SingleCov.covered!

describe 'SecurityPolicyMailerJob' do
  fixtures :accounts, :role_settings

  describe '#fetch_accounts' do
    before do
      @shell_account = accounts(:shell_account_without_support)
    end

    it 'does not include shell accounts that do not have a Support product' do
      assert @shell_account.is_active?
      assert_equal Zendesk::Accounts::Locking::LockState::OPEN, @shell_account.lock_state
      assert_includes Account.all, @shell_account

      num_shell_accounts = 0
      Zendesk::Maintenance::Jobs::SecurityPolicyMailerJob.fetch_accounts do |account|
        num_shell_accounts += 1 if account.id == @shell_account.id
      end

      assert_equal 0, num_shell_accounts
    end
  end

  describe '#execute' do
    let(:sent) { ActionMailer::Base.deliveries.last }
    let(:account) { accounts(:minimum) }

    before do
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries = []
      Zendesk::Maintenance::Jobs::SecurityPolicyMailerJob.stubs(:fetch_accounts).yields(account)
    end

    describe 'with expiring passwords' do
      before do
        User.any_instance.stubs(:password_expires_at).returns(3.days.from_now)
      end

      it 'delivers password expiring notifications' do
        Zendesk::Maintenance::Jobs::SecurityPolicyMailerJob.execute
        sent.subject.must_include %("Minimum account" password expiring)
      end
    end

    describe 'with no expiring passwords' do
      before do
        User.any_instance.stubs(:password_expires_at).returns(5.days.from_now)
      end

      it 'delivers no password expiring notifications' do
        Zendesk::Maintenance::Jobs::SecurityPolicyMailerJob.execute
        assert_nil sent
      end
    end

    describe 'with expiring password on a suspended user' do
      before do
        User.any_instance.stubs(:password_expires_at).returns(3.days.from_now)
        User.any_instance.stubs(:suspended?).returns(true)
      end

      describe "with :no_expiring_password_warnings_if_suspended disabled" do
        before { Arturo.disable_feature! :no_expiring_password_warnings_if_suspended }

        it 'delivers password expiring notifications' do
          Zendesk::Maintenance::Jobs::SecurityPolicyMailerJob.execute
          sent.subject.must_include %("Minimum account" password expiring)
        end
      end

      describe "with :no_expiring_password_warnings_if_suspended enabled" do
        before { Arturo.enable_feature! :no_expiring_password_warnings_if_suspended }

        it 'delivers no password expiring notifications' do
          User.any_instance.expects(:suspended?).returns(true)
          Zendesk::Maintenance::Jobs::SecurityPolicyMailerJob.execute
          assert_nil sent
        end
      end
    end

    describe 'when task execution raises errors' do
      before do
        Zendesk::Maintenance::Jobs::SecurityPolicyMailerJob.any_instance.stubs(:deliver_expiring_password_warnings).raises(StandardError, 'something is wrong')
      end

      it 'logs error and continues' do
        Zendesk::Maintenance::Jobs::MaintenanceLogger.any_instance.expects(:error).with("Failed to deliver password expiration warning for account #{account.subdomain} with error something is wrong")
        Zendesk::Maintenance::Jobs::SecurityPolicyMailerJob.execute
      end
    end
  end
end
