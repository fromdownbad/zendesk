require_relative '../../../../support/job_helper'
require 'zendesk/maintenance/jobs/revere_synchronization_job'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::RevereSynchronizationJob do
  fixtures :accounts, :users

  let(:subject) { Zendesk::Maintenance::Jobs::RevereSynchronizationJob }
  let(:api_client) { mock }
  let (:subscriber) do
    user = users(:minimum_agent)
    user.settings.revere_subscription = true
    user.settings.save!
    user
  end

  before do
    subject.stubs(:revere_api_client).returns(api_client)
    api_client.stubs(:unsubscribed).returns([])
    api_client.stubs(:delete_unsubscribed)
    api_client.stubs(:arturo_enabled?).returns(true)
  end

  describe 'unsubscribed' do
    let (:subscriber_revere_data) { Hashie::Mash.new(id: 2, zendesk_account_id: subscriber.account.id, zendesk_user_id: subscriber.id, subdomain: subscriber.account.subdomain, alert_destination: subscriber.email) }

    before do
      api_client.expects(:unsubscribed).returns([subscriber_revere_data])
    end

    it 'removes an unsubscribed user' do
      subject.work

      refute subscriber.reload.settings.revere_subscription
    end

    it 'deletes from revere' do
      api_client.expects(:delete_unsubscribed).with([2])

      subject.work
    end

    it 'ignores user that no longer exist' do
      # User#destroy tries to delete the profile image.
      stub_request(:any, /amazonaws.com/).
        to_return(status: 200)

      subscriber.destroy
      api_client.expects(:delete_unsubscribed).never

      subject.work
    end
  end

  describe 'account moves' do
    let(:move) do
      subscriber.account.account_moves.create! do |move|
        move.src_shard_id = 2
        move.target_shard_id = subscriber.account.shard_id
        move.state = 'done'
      end
    end

    before do
      move
    end

    it 'updates subscribers for a moved account' do
      api_client.expects(:update_subscription).with(subscriber, true)

      subject.work
    end

    it 'ignores an account move from a week ago' do
      move.updated_at = 1.week.ago
      move.save!

      api_client.expects(:update_subscription).never

      subject.work
    end

    it 'ignores an account with no valid subscribers' do
      subscriber.settings.revere_subscription = false
      subscriber.settings.save!

      api_client.expects(:update_subscription).never

      subject.work
    end
  end
end
