require_relative "../../../../support/job_helper"

SingleCov.covered! uncovered: 1

describe 'SatisfactionRatingIntentionJob' do
  fixtures :accounts, :users, :tickets

  describe Zendesk::Maintenance::Jobs::SatisfactionRatingIntentionJob do
    def create_intention
      accounts(:minimum).satisfaction_rating_intentions.create!(
        user: tickets(:minimum_1).requester,
        ticket: tickets(:minimum_1),
        score: SatisfactionType.GOOD
      )
    end

    describe ".work" do
      def call
        Zendesk::Maintenance::Jobs::SatisfactionRatingIntentionJob.work
      end

      it "rates tickets and cleans up" do
        # create intention
        intention = Timecop.travel(1.hour.ago) { create_intention }
        refute_equal intention.score, intention.ticket.satisfaction_score

        # tracks rating in statsd?
        stats = mock
        stats.expects(:count).with('rated', 1)
        SatisfactionRatingIntention.stubs(stats: stats)

        call

        # cleans up intention?
        assert_equal intention.score, intention.ticket.reload.satisfaction_score
        assert_raises(ActiveRecord::RecordNotFound) { intention.reload }
      end

      it "not rate for too new intentions" do
        intention = create_intention
        refute_equal intention.score, intention.ticket.satisfaction_score
        call
        refute_equal intention.score, intention.ticket.satisfaction_score
        intention.reload
      end

      it "not blow up when ratings fail" do
        Timecop.travel(1.hour.ago) { create_intention }
        SatisfactionRatingIntention.any_instance.expects(:rate!).raises(ArgumentError)
        SatisfactionRatingIntention.any_instance.expects(:destroy)
        ZendeskExceptions::Logger.expects(:record)
        call
      end

      it "not trip over itself when accidentally run in parallel" do
        intention = Timecop.travel(1.hour.ago) { create_intention }
        SatisfactionRatingIntention.stubs(:find_each).yields(intention)
        SatisfactionRatingIntention.any_instance.expects(:rate!)
        call
        call # not exactly parallel, but simulates it well enough for this
      end
    end
  end
end
