require_relative "../../../../support/job_helper"

SingleCov.covered!

describe 'VoicePartnerEditionTrialExpiryJob' do
  let(:account) { accounts(:minimum) }
  let(:partner_edition_account) do
    FactoryBot.create(
      :voice_partner_edition_account,
      account: account,
      active: true,
      trial: true,
      trial_expires_at: 1.day.from_now
    )
  end

  before do
    Zendesk::Accounts::Client.any_instance.stubs(:update_or_create_product!)
  end

  describe 'trial has expired' do
    before do
      partner_edition_account.update_attribute(:trial_expires_at, 1.day.ago)
    end

    it 'creates new record with active flag set to false' do
      Zendesk::Maintenance::Jobs::VoicePartnerEditionTrialExpiryJob.work_on_shard(account.shard_id)

      refute_equal partner_edition_account.id, account.voice_partner_edition_account.id
      assert_equal false, account.voice_partner_edition_account.active?
    end

    it 'marks old record as deleted' do
      Zendesk::Maintenance::Jobs::VoicePartnerEditionTrialExpiryJob.work_on_shard(account.shard_id)

      refute_nil partner_edition_account.reload.deleted_at
    end

    it 'does not deactive account if trial is false' do
      partner_edition_account.update_attribute(:trial, false)
      Zendesk::Maintenance::Jobs::VoicePartnerEditionTrialExpiryJob.work_on_shard(account.shard_id)

      partner_edition_account.reload
      assert_nil partner_edition_account.deleted_at
      assert(partner_edition_account.active)
    end
  end

  describe 'trial_expires_at is in the future' do
    it 'does not change active flag on voice partner edition account to false nor marks the existing as deleted' do
      Zendesk::Maintenance::Jobs::VoicePartnerEditionTrialExpiryJob.work_on_shard(account.shard_id)

      assert_equal partner_edition_account.id, account.voice_partner_edition_account.id
      assert(partner_edition_account.reload.active?)
    end
  end
end
