require_relative "../../../../support/job_helper"

SingleCov.covered! uncovered: 43

describe 'BaseMaintenanceJob' do
  describe '#record_recoverable_failure' do
    before do
      @base_job = Zendesk::Maintenance::Jobs::BaseMaintenanceJob.new(false, false)
      begin
        raise StandardError, "Duude!"
      rescue StandardError => e
        @exception = e
      end

      ZendeskExceptions::Logger.expects(:record).with do |exception, named_params|
        named_params[:location] == @base_job && !named_params[:message].empty? && exception == @exception
      end
    end

    describe 'called without continuation message' do
      before do
        @base_job.logger.expects(:error).twice
      end

      it 'makes the right calls' do
        @base_job.record_recoverable_failure("", @exception)
      end
    end

    describe 'called with a continuation message' do
      before do
        @base_job.logger.expects(:error).times(3)
      end

      it 'makes the right calls' do
        @base_job.record_recoverable_failure("I continued!!", @exception)
      end
    end
  end

  describe '#notify?' do
    before do
      @base_job = Zendesk::Maintenance::Jobs::BaseMaintenanceJob.new(false, false)
      @base_job.stubs(:recipients).returns([1, 2, 3])
    end

    it "returns false for test environment" do
      assert_equal false, @base_job.notify?
    end

    it "returns true for production environment" do
      Rails.expects(:env).returns(ActiveSupport::StringInquirer.new("production"))
      assert(@base_job.notify?)
    end
  end

  describe "#completed" do
    before do
      @base_job = Zendesk::Maintenance::Jobs::BaseMaintenanceJob.new(false, false)
    end

    it "defaults to true" do
      assert @base_job.completed
    end
  end
end
