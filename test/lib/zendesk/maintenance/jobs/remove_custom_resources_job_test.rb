require_relative '../../../../support/job_helper'

SingleCov.covered! uncovered: 1

describe Zendesk::Maintenance::Jobs::RemoveCustomResourcesJob do
  include DataDeletionJobSupport

  fixtures :accounts

  def mock_resp(cpu, done)
    mock.tap do |mock|
      mock.stubs(:body).returns(
        'data' => {
          'cpu' => cpu,
          'done' => done
        }
      )
    end
  end

  describe '#work' do
    path_regex = %r{/api/sunshine/private/account_deletion\?account_id=\d+&subdomain=.+&limit=\d+}

    before(:all) do
      Zendesk::Maintenance::Jobs::RemoveCustomResourcesJob::SLEEP_DEFAULT = 0.01
    end

    it 'does not call Custom Resources if dry run' do
      Faraday::Connection.any_instance.expects(:post).never

      job = create_job(accounts(:minimum), Zendesk::Maintenance::Jobs::RemoveCustomResourcesJob, dry_run: true)
      job.work
    end

    it 'does not call Custom Resources given arturo' do
      Arturo.disable_feature! :custom_resources_account_deletion

      Faraday::Connection.any_instance.expects(:post).never

      job = create_job(accounts(:minimum), Zendesk::Maintenance::Jobs::RemoveCustomResourcesJob, dry_run: false)
      job.work
    end

    it 'calls custom resources for as many times as done != done, when cpu is not available' do
      Arturo.enable_feature! :custom_resources_account_deletion

      Faraday::Connection.any_instance.
        expects(:post).
        with(regexp_matches(path_regex)).
        returns(
          mock_resp(nil, 'not_done'),
          mock_resp(nil, 'not_done'),
          mock_resp(nil, 'not_done'),
          mock_resp(nil, 'not_done'),
          mock_resp(nil, 'done')
        ).
        times(5)

      job = create_job(accounts(:minimum), Zendesk::Maintenance::Jobs::RemoveCustomResourcesJob)
      job.work
    end

    it 'calls custom resources for as many times as done != done, when cpu is available' do
      Arturo.enable_feature! :custom_resources_account_deletion

      Faraday::Connection.any_instance.
        expects(:post).
        with(regexp_matches(path_regex)).
        returns(
          mock_resp(40.0, 'not_done'),
          mock_resp(45.0, 'not_done'),
          mock_resp(50.0, 'not_done'),
          mock_resp(55.0, 'not_done'),
          mock_resp(60.0, 'done')
        ).
        times(5)

      job = create_job(accounts(:minimum), Zendesk::Maintenance::Jobs::RemoveCustomResourcesJob)
      job.work
    end
  end
end
