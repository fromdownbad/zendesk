require_relative "../../../../support/job_helper"

SingleCov.covered! uncovered: 1

describe 'SuspendedTicketNotificationJob' do
  fixtures :accounts

  describe "The suspended ticket notification job test" do
    before do
      @job = Zendesk::Maintenance::Jobs::SuspendedTicketNotificationJob
      SuspendedTicket.without_arsi.update_all(created_at: 1.year.ago)
      @minimum = accounts(:minimum)
      @minimum_suspended = @minimum.build_suspended_ticket_notification(frequency: SuspendedTicketNotification::Frequency::SOONISH.last, email_list: "a@example.com")
      @minimum_suspended_original_last_sent_at = @minimum_suspended.last_sent_at = 30.minutes.ago
      @minimum_suspended.save!

      @support = accounts(:support)
      @support_suspended = @support.build_suspended_ticket_notification(frequency: SuspendedTicketNotification::Frequency::NEVER.last, email_list: "a@example.com")
      @support_suspended_original_last_sent_at = @support_suspended.last_sent_at = 30.minutes.ago
      @support_suspended.save!

      @daily_account = accounts(:with_groups)
      @daily_account_suspended = @daily_account.build_suspended_ticket_notification(frequency: SuspendedTicketNotification::Frequency::DAILY.last, email_list: "a@example.com")
      @daily_account_suspended_original_last_sent_at = @daily_account_suspended.last_sent_at = 1.day.seconds.ago
      @daily_account_suspended.save!

      [@minimum, @support, @daily_account].each do |account|
        account.suspended_tickets.create!(from_name: "Hansi", from_mail: "hansi@example.com", content: "Wibble", cause: 10)
      end
    end

    it "enqueues notifications for accounts with a frequency" do
      SuspendedTicketsMailer.expects(:deliver_suspended_notification).twice
      @job.work
    end

    it "does not enqueue for accounts with a frequency and an empty email list" do
      SuspendedTicketNotification.without_arsi.update_all(email_list: "")
      SuspendedTicketsMailer.expects(:deliver_suspended_notification).never
      @job.work
    end

    it "works around locked accounts" do
      @minimum.lock!
      @daily_account.lock!
      SuspendedTicketsMailer.expects(:deliver_suspended_notification).never
      @job.work
    end

    it "finds the correct number of tickets" do
      assert_equal 1, @job.suspended_ticket_count(@minimum, 1.hour.ago)
    end

    it "runs every 10 minutes" do
      evaluated = ERB.new(File.read(Rails.root + 'config/resque-schedule.yml')).result
      resque_schedule = YAML.load(evaluated)
      assert_equal "*/10 * * * *", resque_schedule[@job.name]["cron"]
    end

    it "sets the last_sent_at correctly" do
      @job.work
      [@minimum_suspended, @daily_account_suspended].each(&:reload)

      # 3* for @minium_suspended because we specified 30 minutes ago and it's a 10 minute frequency
      assert_equal (@minimum_suspended_original_last_sent_at + 3 * @minimum_suspended.frequency.minutes).floor(10.minutes), @minimum_suspended.last_sent_at
      assert_equal (@daily_account_suspended_original_last_sent_at + @daily_account_suspended.frequency.minutes).floor(10.minutes), @daily_account_suspended.last_sent_at
    end
  end
end
