require_relative '../../../../support/job_helper'

SingleCov.covered! uncovered: 4

describe 'RemoveCertificatesJob' do
  include DataDeletionJobSupport

  fixtures :all

  before do
    @job = create_job(accounts(:minimum), Zendesk::Maintenance::Jobs::RemoveCertificatesJob)
  end

  it 'deletes certificates' do
    assert_difference '@job.account.certificates.count(:all)', -1 do
      @job.send(:erase_for_canceled)
    end
  end

  it 'do not delete a thing' do
    assert_no_difference '@job.account.certificates.count(:all)' do
      @job = create_job(accounts(:with_groups), Zendesk::Maintenance::Jobs::RemoveCertificatesJob, dry_run: true)
      @job.work
    end
  end
end
