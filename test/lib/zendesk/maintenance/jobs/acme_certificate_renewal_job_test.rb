require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'AcmeCertificateRenewalJob' do
  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:job) { Zendesk::Maintenance::Jobs::AcmeCertificateRenewalJob }

  before do
    AcmeCertificateJobStatus.expects(:enqueue_with_status!).never
  end

  def create_cert(state, auto, date)
    c = account.certificates.create
    c.state = state
    c.autoprovisioned = auto
    c.valid_until = date
    c.save!
    c
  end

  describe "with a hostmapped account" do
    before do
      account.update_attribute(:host_mapping, 'support.example.com')
    end

    it "only renews autoprovisioned certificates" do
      create_cert 'active', false, Date.today + 1
      job.work
    end

    it "does not renew revoked certificates" do
      create_cert 'revoked', true, Date.today + 1
      job.work
    end

    it "does not renew certificates not nearing expiration" do
      create_cert 'active', true, Date.today + 16
      job.work
    end

    it "renews a certificate nearing expiration" do
      create_cert 'active',  true, Date.today + 14
      AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id).once
      job.work
    end

    it "renews expired certificates" do
      create_cert 'active', true, Date.today - 1
      AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id).once
      job.work
    end
  end

  describe "when the account doesn't have hostmapping" do
    it "revokes the certificate" do
      create_cert 'active',  true, Date.today + 14
      job.work
      assert_empty account.certificates.active
    end
  end
end
