require_relative "../../../../support/job_helper"

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::CertificateMaintenanceJob do
  fixtures :accounts, :brands, :account_settings, :recipient_addresses

  let(:account) { accounts(:minimum) }
  let(:subject) { Zendesk::Maintenance::Jobs::CertificateMaintenanceJob }
  let(:certificate) { account.certificates.create! }
  let(:email) { ActionMailer::Base.deliveries.last }

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries         = []

    account.update_attribute(:host_mapping, 'www.google.com')
  end

  def self.generate_certificate(options)
    before do
      certificate.state           = 'active'
      certificate.valid_until     = options.fetch(:date, 1.month.from_now.utc)
      certificate.autoprovisioned = options.fetch(:autoprovisioned, false)
      certificate.save!
    end
  end

  describe "an autoprovisioned certificate" do
    describe "expires 1 month from now" do
      generate_certificate(date: 1.month.from_now.utc, autoprovisioned: true)

      before { subject.execute }

      it "does not send an email warning" do
        assert_empty ActionMailer::Base.deliveries
      end
    end

    describe "4 days prior to expiry" do
      generate_certificate(date: 4.days.from_now.utc, autoprovisioned: true)

      before { subject.execute }

      it "does not send an email warning" do
        assert_empty ActionMailer::Base.deliveries
      end
    end
  end

  describe "a customer provisioned certificate" do
    describe "expires 1 month from now" do
      generate_certificate(date: 1.month.from_now.utc)

      before do
        subject.execute
        certificate.reload
      end

      it "sets notification_sent_at" do
        assert_equal Time.now.to_date, certificate.notification_sent_at.to_date
      end

      it "sends the certificate expiration warning email" do
        email.subject.must_match /Your Zendesk SSL certificate is expiring in (28|29|30|31) days/
      end
    end

    describe "4 days prior to expiry" do
      generate_certificate(date: 4.days.from_now.utc)

      describe "account with host_mapping" do
        before do
          subject.execute
          certificate.reload
        end

        it "sets notification_sent_at" do
          assert_equal Time.now.to_date, certificate.notification_sent_at.to_date
        end

        it "sends the certificate replacement email" do
          email.subject.must_match /Your SSL certificate will soon be replaced with a Zendesk-provisioned SSL certificate/
        end
      end

      describe "account without host_mapping" do
        before do
          account.update_attribute(:host_mapping, '')
          subject.execute
          certificate.reload
        end

        it "does not set notification_sent_at" do
          assert_nil certificate.notification_sent_at
        end

        it "does not send an email warning" do
          assert_empty ActionMailer::Base.deliveries
        end

        it "revokes the certificate" do
          assert_equal 'revoked', certificate.state
        end
      end

      describe "a suspended account" do
        before do
          account.update_attributes!(is_serviceable: false)
          ActionMailer::Base.deliveries = []
          subject.execute
          certificate.reload
        end

        it "does not set notification_sent_at" do
          assert_nil certificate.notification_sent_at
        end

        it "does not send an email warning" do
          assert_empty ActionMailer::Base.deliveries
        end

        it "revokes the certificate" do
          assert_equal 'revoked', certificate.state
        end
      end

      describe "an expired account" do
        before do
          account.update_attributes!(is_serviceable: false, is_active: false)
          ActionMailer::Base.deliveries = []
          subject.execute
          certificate.reload
        end

        it "does not set notification_sent_at" do
          assert_nil certificate.notification_sent_at
        end

        it "does not send an email warning" do
          assert_empty ActionMailer::Base.deliveries
        end

        it "revokes the certificate" do
          assert_equal 'revoked', certificate.state
        end
      end
    end

    describe "5 days prior to expiry" do
      generate_certificate(date: 5.days.from_now.utc)
      before do
        subject.execute
      end

      it "does not send out email" do
        assert_empty ActionMailer::Base.deliveries
      end
    end

    describe "3 days prior to expiry" do
      generate_certificate(date: 3.days.from_now.utc)

      it "runs the job to provision the let's encrypt certificate" do
        AcmeCertificateJobStatus.expects(:enqueue_with_status!).with(account.id)
        subject.execute
      end
    end
  end
end
