require_relative '../../../../support/job_helper'

SingleCov.covered! uncovered: 4

describe Zendesk::Maintenance::Jobs::RemoveHCAttachmentsJob do
  include DataDeletionJobSupport

  fixtures :all

  before do
    Account.without_arsi.update_all(deleted_at: Time.now)
  end

  after do
    Account.without_arsi.update_all(deleted_at: nil)
  end

  describe 'when deleting help center attachments from the cloud' do
    before do
      @job = create_job(accounts(:minimum), Zendesk::Maintenance::Jobs::RemoveHCAttachmentsJob)
    end

    describe 'selects the right s3 bucket configuration' do
      let(:store) { @job.send(:store) }

      describe_with_arturo_disabled :help_center_replicated_s3_bucket do
        it 'should not get the new credentials' do
          assert_equal "FAKE_access_key_id", store.instance_variable_get(:@aws_access_key_id)
        end
      end

      describe_with_arturo_enabled :help_center_replicated_s3_bucket do
        it 'should get the new credentials' do
          assert_equal "FAKE_replicated_access_key_id", store.instance_variable_get(:@aws_access_key_id)
        end
      end
    end

    describe 'reason = moved' do
      it 'noops' do
        @job.account.update_attribute(:shard_id, 2)
        @job.job_audit.audit.reason = 'moved'
        @job.job_audit.shard_id = 1

        @job.expects(:erase_for_moved).once
        @job.work
      end
    end

    it 'deletes article attachments, setting assets and theme assets' do
      @job.expects(:delete_for_prefix).times(@job.send(:prefixes).count).returns(true)

      @job.send(:erase_for_canceled)
    end

    it 'verifys deleted' do
      Fog::Storage::AWS::Directories.any_instance.stubs(get: stub(files: stub(all: stub(any?: false))))
      @job.send(:verify_for_canceled)
    end

    it 'verifys deleted failed' do
      Fog::Storage::AWS::Directories.any_instance.stubs(get: stub(files: stub(all: stub(any?: true))))
      assert_raises RuntimeError do
        @job.send(:verify_for_canceled)
      end
    end

    describe "dry run mode" do
      it "should not delete a thing" do
        @job = create_job(accounts(:support), Zendesk::Maintenance::Jobs::RemoveHCAttachmentsJob, dry_run: true)
        @job.class.any_instance.expects(:delete_for_prefix).never
        @job.work
      end
    end
  end
end
