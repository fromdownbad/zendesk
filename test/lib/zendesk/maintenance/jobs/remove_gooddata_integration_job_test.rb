require_relative '../../../../support/job_helper'
require_relative '../../../../support/gooddata_session_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::RemoveGooddataIntegrationJob do
  include DataDeletionJobSupport
  include GooddataSessionHelper

  fixtures :all

  let(:gooddata_integration) { stub('gooddata_integration', project_id: 9) }

  before do
    @account = accounts(:minimum)
    @job = create_job(@account, Zendesk::Maintenance::Jobs::RemoveGooddataIntegrationJob)

    stub_gooddata_session
    @job.stubs(:account).returns(@account)
    @account.stubs(:gooddata_integration).returns(gooddata_integration)

    @gooddata_user = ::GooddataUser.create! do |u|
      u.account = @account
      u.gooddata_user_id = -1
      u.gooddata_project_id = @account.gooddata_integration.project_id
      u.user_id = @account.users.first.id
    end

    Zendesk::Gooddata::Client.v2.stubs(:get).
      with("/gdc/account/profile/#{@gooddata_user.gooddata_user_id}").
      returns(accountSetting: { login: '1111122222@gooddata.com' })
  end

  describe 'reason = moved' do
    it 'noops' do
      @job.account.update_attribute(:shard_id, 2)
      @job.job_audit.audit.reason = 'moved'
      @job.job_audit.shard_id = 1

      @job.expects(:erase_for_moved)
      @job.work
    end
  end

  describe '#work' do
    before do
      Zendesk::Gooddata::IntegrationProvisioning.any_instance.stubs(:gooddata_project_enabled?).returns(true)
      Zendesk::DataDeletion::JobHeartbeater.any_instance.stubs(:with_poll)
    end

    it 'logs the job occurrence to statsd' do
      Zendesk::StatsD::Client.any_instance.expects(:increment).with('count')

      @job.send(:work)
    end

    describe 'failure occurs during job' do
      before do
        Zendesk::DataDeletion::JobHeartbeater.any_instance.expects(:with_poll).raises(RuntimeError)
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('count')
      end

      it 'logs the failure to statsd' do
        Zendesk::StatsD::Client.any_instance.expects(:increment).with('failed')
        @job.job_audit.expects(:fail!)
        @job.send(:work)
      end
    end
  end

  describe '#erase_for_canceled' do
    before do
      Zendesk::Gooddata::IntegrationProvisioning.any_instance.stubs(:gooddata_project_enabled?).returns(true)
    end

    it 'deletes the gooddata integration for an account' do
      Zendesk::Gooddata::IntegrationProvisioning.any_instance.expects(:destroy_gooddata_integration)

      GoodData::User.any_instance.expects(:delete)

      @job.send(:erase_for_canceled)
    end

    it 'ignores project not found error' do
      Zendesk::Gooddata::IntegrationProvisioning.any_instance.stubs(:destroy_gooddata_integration).raises(GoodData::Error::NotFound)
      Rails.logger.expects(:warn).with("Skipping. GoodData project #{gooddata_integration.project_id} for account #{@account.id} does not exist.")

      @job.send(:erase_for_canceled)
    end

    describe 'when project_id is -1' do
      before do
        @account.gooddata_integration.stubs(:project_id).returns("-1")
      end

      it 'will delete users but not the project' do
        GoodData::User.any_instance.expects(:delete)
        @job.expects(:integration_provisioning).never
        @job.send(:statsd_client).expects(:increment).with('invalid_project_id').once
        @job.send(:erase_for_canceled)
      end
    end
  end

  describe '#verify_for_canceled' do
    describe 'for v2 or greater Gooddata integration' do
      before do
        Zendesk::Gooddata::IntegrationProvisioning.any_instance.stubs(:gooddata_project_enabled?).returns(true)
      end

      it 'verifies the gooddata integration for an account' do
        @job.expects(:verify_project_removed)

        @job.send(:verify_for_canceled)
      end
    end

    describe 'when project_id is "-1"' do
      before do
        @account.gooddata_integration.stubs(:project_id).returns("-1")
      end

      it 'will not verify project is deleted' do
        @job.expects(:verify_project_removed).never
        @job.send(:verify_for_canceled)
      end
    end
  end

  describe '#remove_gooddata_integration' do
    before do
      Zendesk::Gooddata::IntegrationProvisioning.any_instance.stubs(:gooddata_project_enabled?).returns(true)
    end

    it 'deletes the gooddata integration for an account' do
      Zendesk::Gooddata::IntegrationProvisioning.any_instance.expects(:destroy_gooddata_integration)

      @job.send(:remove_gooddata_integration)
    end

    it 'logs the foreign key information' do
      Zendesk::Gooddata::IntegrationProvisioning.any_instance.expects(:destroy_gooddata_integration)

      @job.key_log.expects(:info).with(
        [
          'Gooddata Project Deleted',
          {
            account: @account.id,
            subdomain: @account.subdomain,
            shard: @account.shard_id,
            record_type: 'gooddata_integration',
            storage_keys: 9
          }
        ]
      )

      @job.send(:remove_gooddata_integration)
    end
  end

  describe '#remove_gooddata_users' do
    it 'removes all gooddata users for an account' do
      GoodData::User.any_instance.expects(:delete)

      @job.send(:remove_gooddata_users)
    end
  end

  describe '#verify_project_removed' do
    it 'raises exception if project is not deleted' do
      GoodData::Project.any_instance.expects(:find).returns(project: {content: {state: 'ENABLED'}})

      assert_raises(RuntimeError) do
        @job.send(:verify_project_removed)
      end
    end

    it 'ignores project not found error' do
      GoodData::Project.any_instance.stubs(:find).raises(GoodData::Error::NotFound)

      assert(@job.send(:verify_project_removed))
    end

    it 'returns true if the project has the state of DELETED' do
      GoodData::Project.any_instance.stubs(:find).returns(project: {content: {state: 'DELETED'}})

      assert(@job.send(:verify_project_removed))
    end

    it 'returns true if the project has the state of ARCHIVED' do
      GoodData::Project.any_instance.stubs(:find).returns(project: {content: {state: 'ARCHIVED'}})

      assert(@job.send(:verify_project_removed))
    end
  end
end
