require_relative '../../../../support/job_helper'

SingleCov.covered! uncovered: 1

describe Zendesk::Maintenance::Jobs::RemoveSunshineEventsProfilesJob do
  include DataDeletionJobSupport
  let(:job) { Zendesk::Maintenance::Jobs::RemoveSunshineEventsProfilesJob.new(audit_id, options) }

  let(:data_deletion_job) { create_job(account, Zendesk::Maintenance::Jobs::RemoveSunshineEventsProfilesJob) }
  let(:audit_id) { data_deletion_job.audit.id }
  let(:dry_run) { false }
  let(:account_id) { 1 }
  let(:options) do
    {
      dry_run: dry_run
    }
  end
  let(:account) { accounts(:minimum) }
  let(:deletion_completed_status) { "COMPLETED" }
  let(:deletion_in_progress_status) { "IN_PROGRESS" }
  let(:deletion_failed_status) { "FAILED" }
  let!(:api_client) { Zendesk::Sunshine::AccountConfigClient.new(account: account) }
  let!(:stats_client) { Zendesk::StatsD::Client.new(namespace: %w[jobs remove_sunshine_events_profiles_job]) }

  before do
    Arturo.disable_feature!(:account_deletion_kill_switch)
    Arturo.enable_feature!(:sunshine_event_and_profiles_account_deletion)
    Zendesk::Sunshine::AccountConfigClient.stubs(:new).with(account: account).returns(api_client)
    api_client.stubs(:delete_account).returns(deletion_completed_status)
    Zendesk::StatsD::Client.stubs(:new).with(namespace: %w[jobs remove_sunshine_events_profiles_job]).returns(stats_client)
    stats_client.stubs(:increment)
  end

  describe "#verify_sunshine_events_and_profiles_deletion" do
    it 'raises data deletion error if not done' do
      assert_raises(Zendesk::Maintenance::Jobs::RemoveSunshineEventsProfilesJob::DataDeletionError) do
        job.verify_for_canceled
      end
    end

    it 'does not raise an error if job is completed' do
      job.erase_for_canceled

      assert_nil job.verify_for_canceled
    end

    it 'does not raise an error if feature toggle is off' do
      Arturo.disable_feature!(:sunshine_event_and_profiles_account_deletion)
      job.erase_for_canceled

      assert_nil job.verify_for_canceled
    end
  end

  describe '#delete_sunshine_events_and_profiles' do
    it 'deletes account' do
      status = job.erase_for_canceled

      assert(status)
    end

    it 'invokes delete account via sunshine account config' do
      api_client.expects(:delete_account).returns(deletion_completed_status)

      job.erase_for_canceled
    end

    it 'raises DataDeletionError if sunshine account config fails to delete' do
      api_client.expects(:delete_account).returns(deletion_failed_status)

      assert_raises(Zendesk::Maintenance::Jobs::RemoveSunshineEventsProfilesJob::DataDeletionError) do
        job.erase_for_canceled
      end
    end

    it 'waits until sunshine account config returns successful deletion' do
      api_client.stubs(:delete_account).returns(deletion_in_progress_status).then.returns(deletion_completed_status)
      Kernel.expects(:sleep).with(Zendesk::Maintenance::Jobs::RemoveSunshineEventsProfilesJob::WAIT_IN_SECONDS_FOR_RETRIES)

      job.erase_for_canceled
    end

    it 'increments started metric in datadog' do
      stats_client.expects(:increment).with('started', tags: %W[account_id:#{account.id} audit_id:#{audit_id}])

      job.erase_for_canceled
    end

    it 'increments completed metric in datadog' do
      stats_client.expects(:increment).with('completed', tags: %W[account_id:#{account.id} audit_id:#{audit_id}])

      job.erase_for_canceled
    end

    describe 'when kill switch is enabled' do
      before do
        Arturo.enable_feature!(:account_deletion_kill_switch)
      end

      it 'does not delete account' do
        assert_raises(Zendesk::DataDeletion::AccountDeletionKillSwitch::KillSwitchEnabled) do
          job.erase_for_canceled
        end
      end
    end

    describe 'when running in dry run mode' do
      let(:dry_run) { true }

      it 'returns false' do
        assert_equal false, job.erase_for_canceled
      end

      it 'does not delete account' do
        api_client.expects(:delete_account).never

        job.erase_for_canceled
      end
    end

    describe "when deletion is disabled via arturo" do
      before do
        Arturo.disable_feature!(:sunshine_event_and_profiles_account_deletion)
      end

      it 'does not delete account' do
        api_client.expects(:delete_account).never

        job.erase_for_canceled
      end

      it 'returns false' do
        assert_equal false, job.erase_for_canceled
      end
    end
  end
end
