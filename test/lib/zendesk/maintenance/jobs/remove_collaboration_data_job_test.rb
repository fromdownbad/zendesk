require_relative '../../../../support/job_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::RemoveCollaborationDataJob do
  include DataDeletionJobSupport

  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let!(:job) { create_job(account, Zendesk::Maintenance::Jobs::RemoveCollaborationDataJob) }
  let(:kragle) { mock('Faraday::Connection') }

  let(:delete_response) do
    OpenStruct.new(status: 200, headers: { 'Backoff' => '0.2' }, body: {
      'next_page' => "/accounts/#{account.id}?phase=users",
      'end_of_stream' => false
    })
  end

  let(:delete_response_last_page) do
    OpenStruct.new(status: 200, headers: {'Backoff' => '0' }, body: {
      'end_of_stream' => true
    })
  end

  def stub_two_step_delete
    kragle.expects(:delete).
      with("/accounts/#{account.id}").
      returns(delete_response)
    kragle.expects(:delete).
      with("/accounts/#{account.id}?phase=users").
      returns(delete_response_last_page)
  end

  before do
    Arturo.enable_feature! :exodus_collaboration_deletion
    Kragle.expects(:new).at_most(1).returns(kragle)
  end

  describe 'reason = moved' do
    before do
      job.stubs(:pod_local_move?).returns(pod_local)

      job.account.update_attribute(:shard_id, 2)
      job.job_audit.audit.reason = 'moved'
      job.job_audit.shard_id = 1
    end

    let(:pod_local) { false }

    describe 'inter-pod move' do
      it 'deletes through the collaboration_data service' do
        stub_two_step_delete
        job.work
      end

      it 'noops if the arturo is disabled' do
        Arturo.disable_feature! :exodus_collaboration_deletion
        kragle.expects(:delete).never
        job.work
      end
    end

    describe 'intra-pod move' do
      let(:pod_local) { true }

      it 'noops' do
        kragle.expects(:delete).never
        job.work
      end
    end
  end

  describe 'account canceled' do
    before do
      job.job_audit.stubs(:shard_id).returns(1)
      job.job_audit.audit.stubs(:reason).returns('canceled')
    end

    it 'sends a request to c11n_data to delete the data associated with an account' do
      stub_two_step_delete
      job.work
    end

    it 'noops if the arturo is disabled' do
      Arturo.disable_feature! :exodus_collaboration_deletion
      kragle.expects(:delete).never
      job.work
    end
  end

  describe 'dry_run' do
    it 'should never ask c11n_data to delete an account' do
      job = create_job(account, Zendesk::Maintenance::Jobs::RemoveCollaborationDataJob, dry_run: true)
      kragle.expects(:delete).never
      job.work
    end
  end

  describe 'kill switch enabled' do
    it 'raises' do
      Arturo.enable_feature! :account_deletion_kill_switch
      assert_raises Zendesk::DataDeletion::AccountDeletionKillSwitch::KillSwitchEnabled do
        job.send :erase_for_canceled
      end
    end
  end
end
