require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'TrialExpiry' do
  fixtures :accounts, :translation_locales

  describe Zendesk::Maintenance::Jobs::TrialExpiryJob do
    before do
      Subscription.without_arsi.update_all(is_trial: false)
      Subscription.any_instance.stubs(:validate_pending_payment)
      @account = accounts(:trial)
      Timecop.freeze Time.now
    end

    describe "when account is expired" do
      expired_patch_body = {
          product: {
              state: 'expired'
          }
      }

      before do
        @account.subscription.update_attributes!(is_trial: true, trial_expires_on: 1.day.ago)
      end

      it "expires and syncs product with account service" do
        Zendesk::Accounts::Client.any_instance.expects(:update_product).once.with('support', expired_patch_body, context: "account_expire_trial")
        Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
        refute @account.reload.is_serviceable
      end

      it "does not blow up with locked accounts" do
        Zendesk::Accounts::Client.any_instance.expects(:update_product).never
        @account.lock!
        Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
      end

      it "does not blow up when the account cannot be expired" do
        Account.any_instance.stubs(:expire_trial!).raises(StandardError)
        ZendeskExceptions::Logger.expects(:record)
        Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
      end

      describe 'for pre created accounts' do
        let(:locale) { translation_locales(:english_by_zendesk) }

        before do
          @account.update_attribute(:is_serviceable, false)
        end

        it 'does not expire accounts with PreAccountCreation::ProvisionStatus::INIT' do
          @account.create_pre_account_creation!(
            pod_id: 1,
            locale_id: locale.id,
            region: 'us',
            source: 'Test Account Creation',
            status: PreAccountCreation::ProvisionStatus::INIT
          )

          Zendesk::Accounts::Client.any_instance.expects(:update_product).never
          Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
          assert @account.subscription.reload.is_trial?
        end

        it 'does not expire accounts with PreAccountCreation::ProvisionStatus::ONGOING' do
          @account.create_pre_account_creation!(
            pod_id: 1,
            locale_id: locale.id,
            region: 'us',
            source: 'Test Account Creation',
            status: PreAccountCreation::ProvisionStatus::ONGOING
          )

          Zendesk::Accounts::Client.any_instance.expects(:update_product).never
          Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
          assert @account.subscription.reload.is_trial?
        end

        it 'expires bounded accounts i.e. PreAccountCreation::ProvisionStatus::FINISHED' do
          @account.create_pre_account_creation!(
            pod_id: 1,
            locale_id: locale.id,
            region: 'us',
            source: 'Test Account Creation',
            status: PreAccountCreation::ProvisionStatus::FINISHED
          )

          Zendesk::Accounts::Client.any_instance.expects(:update_product).once.with('support', expired_patch_body, context: "account_expire_trial")
          Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
          refute @account.subscription.reload.is_trial?
        end
      end

      describe 'for abusive accounts' do
        before do
          Account.any_instance.stubs(:abusive?).returns(true)
          @account.update_attribute(:is_serviceable, false)
        end

        it 'expires account' do
          assert @account.subscription.is_trial?
          refute @account.is_serviceable
          Zendesk::Accounts::Client.any_instance.expects(:update_product).once.with('support', expired_patch_body, context: "account_expire_trial")
          Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
          refute @account.subscription.reload.is_trial?
        end
      end

      describe "and account is multiproduct" do
        before do
          Zendesk::Accounts::Client.any_instance.expects(:update_product).once.with('support', expired_patch_body, context: "account_expire_trial")
          @account.multiproduct = true
          @account.save!
        end

        it "does not set is_servicable false" do
          Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
          assert @account.reload.is_serviceable
        end

        it "does not set is_active false" do
          Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
          assert @account.reload.is_active
        end
      end

      it "has pricing model version = PATAGONIA" do
        Zendesk::Accounts::Client.any_instance.expects(:update_product).once.with('support', expired_patch_body, context: "account_expire_trial")
        Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
        assert_equal ZBC::Zendesk::PricingModelRevision::PATAGONIA, @account.subscription.reload.pricing_model_revision
      end

      describe "and trial_expires_on is nil" do
        before { @account.subscription.update_attributes!(trial_expires_on: nil) }

        it "does not expire" do
          Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
          assert @account.reload.is_serviceable
          assert @account.subscription.is_trial?
        end

        it "logs exception" do
          ZendeskExceptions::Logger.expects(:record).once
          Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
        end

        describe "and other trials exist" do
          before do
            @account2 = accounts(:minimum)
            # make minimum account into a trial for this test
            CreditCard.where(account_id: @account2.id).destroy_all
            @account2.subscription.update_attributes!(is_trial: true, trial_expires_on: 1.day.ago)
          end

          it "continues processing other accounts" do
            Zendesk::Accounts::Client.any_instance.expects(:update_product).once.with('support', { product: { state: "expired" } }, context: "account_expire_trial")
            Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
            refute @account2.reload.is_serviceable
            refute @account2.subscription.reload.is_trial?
          end
        end
      end
    end

    describe "when account is not expired" do
      describe 'and expiration date is in the future' do
        before do
          @account.subscription.update_attributes(is_trial: true, trial_expires_on: Date.tomorrow)
        end

        it "does not expire" do
          Zendesk::Accounts::Client.any_instance.expects(:update_product).never
          Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
          assert @account.reload.is_serviceable
        end
      end

      describe 'and expiration date is today' do
        before do
          @account.subscription.update_attributes(is_trial: true, trial_expires_on: Date.today)
        end

        it "does not expire" do
          Zendesk::Accounts::Client.any_instance.expects(:update_product).never
          Zendesk::Maintenance::Jobs::TrialExpiryJob.execute
          assert @account.reload.is_serviceable
        end
      end
    end
  end
end
