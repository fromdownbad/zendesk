require_relative "../../../../support/job_helper"
require_relative "../../../../support/api_activity_test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::Maintenance::Jobs::ApiActivityToDatabaseJob do
  include ApiActivityTestHelper

  fixtures :accounts, :account_settings, :tickets, :api_activity_clients

  let(:job) { Zendesk::Maintenance::Jobs::ApiActivityToDatabaseJob }
  let(:account) { accounts(:minimum) }

  before do
    Account.any_instance.stubs(:has_api_activity_job?).returns(true)
  end

  describe ".work" do
    it "overwrites work_on_account method" do
      Zendesk::Maintenance::Jobs::ApiActivityToDatabaseJob.expects(:update_clients).at_least_once
      job.work
    end
  end

  describe "client save! fails" do
    before do
      @start_api_time = Time.local(2016, 10, @day_of_month, 4, 0)
      Timecop.freeze(@start_api_time)
      populate_client_data(account, @start_api_time, @start_api_time)
      populate_memcached(account, @start_api_time)
      ApiActivityClient.any_instance.stubs(:save!).raises(StandardError)
    end

    it "logs a resque error" do
      Zendesk::Maintenance::Jobs::ApiActivityToDatabaseJob.expects(:resque_error).with(regexp_matches(/ApiActivityToDatabaseJob: Failed to update client for account/)).at_least_once
      job.work
    end
  end

  describe "when memcached data is available" do
    before do
      @day_of_month = 15
      @start_api_time = Time.local(2016, 10, @day_of_month, 4, 0)
      Timecop.freeze(@start_api_time)
      populate_client_data(account, @start_api_time, @start_api_time)
      client_keys = populate_memcached(account, @start_api_time)
      saved_data_keys = account.api_activity_clients.pluck(:client_key)

      job.work

      @client_list = []
      @client_data_list = []
      @client_memcached_list = []
      client_keys.each do |client_key|
        client = account.api_activity_clients.find_by_client_key(client_key)
        @client_list << client
        @client_data_list << client if saved_data_keys.include?(client_key)
        @client_memcached_list << client unless saved_data_keys.include?(client_key)
      end
      @previous_hour = (@start_api_time.to_i / 1.hour) - 1
    end

    it "writes the previous hour data to api_activity_client table" do
      @client_list.each_with_index do |client, i|
        assert_equal(client.last_request_time, @start_api_time - (i + 1).hour)
        data = JSON.parse(client.data).with_indifferent_access
        assert_equal(data[@previous_hour.to_s][:count], 200 + i * 100)
        assert_equal(data[@previous_hour.to_s][:peak_count], 303)
      end
    end

    it "trims older data and counts last 24 hours correctly" do
      # the client data list is pre-populated with 240 entries - should be trimmed to two days (48 hours)
      @client_data_list.each do |client|
        data = JSON.parse(client.data)
        assert_equal(48, data.count)
        # the count for each hour of the day is equal to 1000+day_of_month
        # this ensures the code counts over the entire day without going into the previous or next day
        assert_equal((24000 + ((@day_of_month - 1) * 24)), client.total_count_last_24_hours)
      end

      # the memcached list only has one item and no data for yesterday
      @client_memcached_list.each do |client|
        data = JSON.parse(client.data)
        assert_equal(1, data.count)
        assert_equal(0, client.total_count_last_24_hours)
      end
    end
  end
end
