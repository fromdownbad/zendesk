require_relative "../../../../support/job_helper"
require_relative "../../../../support/billing_test_helper"

SingleCov.covered!

describe 'ShardedSubscriptionJob' do
  fixtures :all

  include BillingTestHelper

  it "builds a sharded_subscription table with all accounts" do
    ShardedSubscription.without_arsi.delete_all
    assert_equal 0, ShardedSubscription.count(:all)
    Zendesk::Maintenance::Jobs::ShardedSubscriptionJob.new(false, false).execute
    assert_equal Account.joins(:subscription).all.map(&:id).reject { |id| id == -1 }, ShardedSubscription.all.map(&:account_id)
  end

  it 'skips shell accounts that do not have a Support product' do
    shell_account = accounts(:shell_account_without_support)
    assert_equal Zendesk::Accounts::Locking::LockState::OPEN, shell_account.lock_state
    Zendesk::Maintenance::Jobs::ShardedSubscriptionJob.new(false, false).execute
    refute_includes ShardedSubscription.all.map(&:account_id), shell_account.id
  end

  it "changes when rebuilt with a changed subscription" do
    account = paying_account
    old_agents = account.subscription.max_agents
    account.save!
    account.subscription.save!
    Zendesk::Maintenance::Jobs::ShardedSubscriptionJob.new(false, false).execute
    assert_equal old_agents, account.sharded_subscription.max_agents

    new_agents = old_agents + 19
    account.subscription.base_agents = new_agents
    account.subscription.save!
    Zendesk::Maintenance::Jobs::ShardedSubscriptionJob.new(false, false).execute
    account.sharded_subscription.reload
    assert_equal new_agents, account.sharded_subscription.max_agents
  end

  it "deletes an account that no longer exists" do
    ss = ShardedSubscription.new
    ss.account_id     = 1234567890
    ss.max_agents     = 1234567890
    ss.plan_type      = 1234567890
    ss.account_type   = "test"
    ss.account_status = "test"
    ss.save(validate: false)

    assert_equal ss, ShardedSubscription.find_by_account_id(1234567890)
    Zendesk::Maintenance::Jobs::ShardedSubscriptionJob.new(false, false).execute
    assert_nil ShardedSubscription.find_by_account_id(1234567890)
  end

  it "steps around locked accounts" do
    account = paying_account
    Zendesk::Maintenance::Jobs::ShardedSubscriptionJob.new(false, false).execute

    assert account.sharded_subscription

    account.lock!
    Zendesk::Maintenance::Jobs::ShardedSubscriptionJob.new(false, false).execute
  end
end
