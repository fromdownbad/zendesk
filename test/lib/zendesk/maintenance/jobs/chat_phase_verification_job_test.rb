require_relative '../../../../support/job_helper'
require_relative '../../../../support/zopim_test_helper'
require_relative '../../../../support/chat_phase_3_helper'
require_relative '../../../../support/entitlement_test_helper'
require_relative '../../../../support/suite_test_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::ChatPhaseVerificationJob do
  include ZopimTestHelper
  include ChatPhase3Helper
  include EntitlementTestHelper
  include SuiteTestHelper

  fixtures :zopim_subscriptions

  let(:job) { Zendesk::Maintenance::Jobs::ChatPhaseVerificationJob }
  let(:account_chat_ok) { accounts(:multiproduct) }
  let(:account_chat_broken) { accounts(:minimum) }
  let(:current_chat_product) { stub(attributes: FactoryBot.build(:chat_trial_product)) }
  let(:account_bad_phase) { accounts(:with_ticket_form) }
  let(:expected_payload) { {} }
  let(:product_params) do
    [{'id' => 1,  'account_id' => account_chat_ok.id, 'name' => 'chat', 'active' => true, 'state' => 'trial', 'plan_settings' => { 'plan_type' => 0, 'max_agents' => 100, 'phase' => 3 }},
     {'id' => 8,  'account_id' => account_bad_phase.id, 'name' => 'chat', 'active' => true, 'state' => 'trial', 'plan_settings' => { 'plan_type' => 0, 'max_agents' => 100, 'phase' => -1 }},
     {'id' => 11, 'account_id' => account_chat_broken.id, 'name' => 'chat', 'active' => true, 'state' => 'trial', 'plan_settings' => { 'plan_type' => 0, 'max_agents' => 100, 'phase' => 3 }}]
  end
  trial_plan_type = ZBC::Zopim::PlanType::Trial

  def mock_products(params)
    params.map { |p| Zendesk::Accounts::Product.new(p) }
  end

  before do
    Arturo.enable_feature!(:chat_phase3_verification_job)
  end

  describe '#work' do
    it 'calls it once' do
      Zendesk::Maintenance::Jobs::ChatPhaseVerificationJob.expects(:validate_zopim_subscriptions).at_least_once
      job.work
    end

    describe 'when not on pod 5' do
      before do
        Rails.env.stubs(:production?).returns(true)
        Zendesk::Configuration.stubs(:pod_id).returns(12)
      end

      it 'never calls it' do
        Zendesk::Maintenance::Jobs::ChatPhaseVerificationJob.expects(:validate_zopim_subscriptions).never
        job.work
      end
    end

    describe 'with arturo off' do
      before do
        Arturo.disable_feature!(:chat_phase3_verification_job)
      end

      it 'never calls it' do
        Zendesk::Maintenance::Jobs::ChatPhaseVerificationJob.expects(:validate_zopim_subscriptions).never
        job.work
      end
    end

    describe 'with chat accounts' do
      before do
        stub_staff_service_patch_request(account_chat_ok.subdomain, '')
        Zendesk::Accounts::Client.any_instance.stubs(chat_product: current_chat_product)
        create_phase_3_zopim_subscription(account_chat_ok, plan_type: trial_plan_type.name)
      end

      describe 'with missing zopim subscription' do
        before do
          Zendesk::Maintenance::Jobs::ChatPhaseVerificationJob.stubs(:chat_products).with(0).returns(mock_products(product_params))
          Zendesk::Maintenance::Jobs::ChatPhaseVerificationJob.stubs(:chat_products).with(11).returns([])
        end

        it 'finds it' do
          Zendesk::Maintenance::Jobs::ChatPhaseVerificationJob.expects(:log_errors).with([90538], 'zopim_subscriptions.missing', anything)
          job.work
        end
      end

      describe 'with extra zopim subscriptions' do
        before do
          create_phase_3_zopim_subscription(account_chat_broken, plan_type: trial_plan_type.name)
          product_params.third['state'] = 'cancelled'
          Zendesk::Maintenance::Jobs::ChatPhaseVerificationJob.stubs(:chat_products).with(0).returns(mock_products(product_params))
          Zendesk::Maintenance::Jobs::ChatPhaseVerificationJob.stubs(:chat_products).with(11).returns([])
        end

        it 'finds it' do
          Zendesk::Maintenance::Jobs::ChatPhaseVerificationJob.expects(:log_errors).with([90538], 'zopim_subscriptions.extra', anything)
          job.work
        end

        it 'logs bad accounts' do
          ZendeskExceptions::Logger.expects(:record)
          job.try(:statsd_client).expects(:gauge)
          job.work
        end

        describe 'with bad queries' do
          before do
            ZBC::Zopim::Subscription.stubs(:where).returns(nil)
          end

          it 'catches errors' do
            Rails.logger.expects(:error).at_least(2)
            job.work
          end
        end
      end
    end
  end
end
