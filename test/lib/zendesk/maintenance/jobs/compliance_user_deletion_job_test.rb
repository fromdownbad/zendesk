require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::ComplianceUserDeletionJob do
  describe ".work" do
    before do
      Zendesk::PushNotifications::Gdpr::GdprUserDeletionSubscriber.any_instance.expects(:process_queues)
    end

    it "should process_queues" do
      Zendesk::Maintenance::Jobs::ComplianceUserDeletionJob.work
    end
  end
end
