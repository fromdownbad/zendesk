require_relative "../../../../support/job_helper"

SingleCov.covered! uncovered: 6

describe 'PerAccountJob' do
  fixtures :accounts

  describe "#fetch_accounts" do
    before do
      @shell_account = accounts(:shell_account_without_support)
    end

    it 'does not include shell accounts that do not have a Support product' do
      assert @shell_account.is_active?
      assert @shell_account.is_serviceable?
      assert_equal Zendesk::Accounts::Locking::LockState::OPEN, @shell_account.lock_state
      assert_includes Account.all, @shell_account

      num_shell_accounts = 0
      Zendesk::Maintenance::Jobs::PerAccountJob.fetch_accounts do |account|
        num_shell_accounts += 1 if account.id == @shell_account.id
      end

      assert_equal 0, num_shell_accounts
    end
  end
end
