require_relative "../../../../support/job_helper"

SingleCov.covered! uncovered: 4

describe Zendesk::Maintenance::Jobs::ThesaurusResetJob do
  fixtures :all

  let(:job) { Zendesk::Maintenance::Jobs::ThesaurusResetJob }
  let(:account) { accounts("minimum") }
  let(:accounts_used_by_the_job) do
    accounts = []
    job.send(:fetch_accounts) do |acc|
      accounts << acc
    end

    accounts
  end
  let(:number_of_accounts_used_by_the_job) { accounts_used_by_the_job.size }

  describe "#fetch_accounts" do
    let(:shell_account) { accounts(:shell_account_without_support) }

    it 'does not include shell accounts that do not have a Support product' do
      assert shell_account.is_active?
      assert shell_account.is_serviceable?
      assert_equal Zendesk::Accounts::Locking::LockState::OPEN, shell_account.lock_state
      assert_includes Account.all, shell_account

      assert_not_includes accounts_used_by_the_job, shell_account
    end
  end

  describe ".ranked_tags" do
    before { create_taggings }

    it "returns the correct hash" do
      expected = {
        "foo" => 2,
        "bar" => 1
      }
      assert_equal expected, job.ranked_tags(account)
    end

    it "adds a comment to the query" do
      assert_sql_queries(1, /ThesaurusResetJob maintenance job recalculating tag scores for account #{account.id} in shard #{account.shard_id}/) do
        job.work
      end
    end
  end

  describe "configurable limits" do
    it "uses the default ones" do
      assert_sql_queries(1, /LIMIT 20000/) do
        job.ranked_tags(account)
      end

      created_at_limit = Tagging.connection.quote 60.days.ago.beginning_of_day
      assert_sql_queries(1, /(created_at >= #{created_at_limit})/) do
        job.ranked_tags(account)
      end
    end

    describe "with configured limits" do
      let(:tagging_created_at_limit) { 7 }
      let(:tagging_amount_limit) { 100 }

      before do
        account.update_attributes!(settings: {
          tagging_created_at_limit: tagging_created_at_limit,
          tagging_amount_limit: tagging_amount_limit
        })
      end

      it "uses the configured limits" do
        assert_sql_queries(1, /LIMIT #{tagging_amount_limit}/) do
          job.ranked_tags(account)
        end

        created_at_limit = Tagging.connection.quote tagging_created_at_limit.days.ago.beginning_of_day
        assert_sql_queries(1, /(created_at >= #{created_at_limit})/) do
          job.ranked_tags(account)
        end
      end

      it "adds a log message" do
        # Make things easier for expectation
        Benchmark.stubs(:ms).returns(1)
        message = "ThesaurusResetJob finished on account: #{account.id} - "\
                  "Scores updated: 0 - "\
                  "Old scores deleted: 0 - "\
                  "Query time (ms): 1 - "\
                  "Update time (ms): 1"

        job.expects(:resque_log).with(message)
        job.work_on_account(account)
      end
    end
  end

  describe ".work" do
    before do
      create_taggings
      # Delete all scores to start with an empty set
      TagScore.without_arsi.delete_all
    end

    it "checks locked_state on each account" do
      assert_sql_queries(number_of_accounts_used_by_the_job, /SELECT  `accounts`.`lock_state` FROM `accounts`/) do
        job.work
      end
    end

    it "detects and skips locked accounts" do
      account.lock!
      job.expects(:clean_old_scores).never
      job.expects(:replace_into).never

      job.work_on_account(account)
    end

    it "doesn't check lock state all the time inside the loop" do
      # Be sure we execute the loop twice
      Hash.any_instance.stubs(:each_slice).returns([[["foo", 2]], [["bar", 1]]])
      job.expects(:insert_or_update).twice

      # Periodical only executes the lock state check once
      assert_sql_queries(1, /SELECT  `accounts`.`lock_state` FROM `accounts`/) do
        job.work_on_account(account)
      end
    end

    it "logs the information without raising errors" do
      # Make all accounts eligible for logging
      job.expects(:rand).times(number_of_accounts_used_by_the_job).returns(0.001)

      job.work
    end

    it "logs the correct information" do
      # Make things easier for expectation
      Account.any_instance.stubs(:id).returns(1)
      Benchmark.stubs(:ms).returns(1)
      # Make all accounts eligible for logging
      job.expects(:rand).times(number_of_accounts_used_by_the_job).returns(0.001)
      message = "ThesaurusResetJob finished on account: 1 - "\
                "Scores updated: 0 - "\
                "Old scores deleted: 0 - "\
                "Query time (ms): 1 - "\
                "Update time (ms): 1"
      job.expects(:resque_log).with(message).times(number_of_accounts_used_by_the_job)

      job.work
    end

    it "calls insert_or_update with the correct values" do
      time = Time.now
      Timecop.freeze(time)

      job.expects(:clean_old_scores).times(number_of_accounts_used_by_the_job)
      job.expects(:insert_or_update).with(
        [
          Tagging.send(:sanitize_sql, ["(?, ?, ?, ?)", account.id, 'foo', 2, time]),
          Tagging.send(:sanitize_sql, ["(?, ?, ?, ?)", account.id, 'bar', 1, time])
        ]
      )

      job.work
    end

    it "runs everyday at 6 AM UTC" do
      evaluated = ERB.new(File.read(Rails.root + 'config/resque-schedule.yml')).result
      resque_schedule = YAML.load(evaluated)
      assert_equal "0 6 * * *", resque_schedule[job.name]["cron"]
    end

    describe "with no tags" do
      before { delete_all_taggings }

      it "does not replace rows but cleans old scores" do
        job.expects(:insert_or_update).never
        job.expects(:clean_old_scores).times(number_of_accounts_used_by_the_job)
        job.work
      end
    end

    it "creates the correct tag_scores and deletes old ones" do
      # This old score should be deleted during the job execution
      account.tag_scores.create!(tag_name: "old_score", score: 5) { |t| t.updated_at = 2.weeks.ago }
      job.work
      assert_equal [["foo", 2], ["bar", 1]], account.tag_scores.reload.pluck(:tag_name, :score)

      # Create tagging and re-run job to test that we correctly replace the score
      Tagging.create!(account: account, tag_name: "bar", taggable: tickets("minimum_2"))
      Tagging.create!(account: account, tag_name: "bar", taggable: tickets("minimum_3"))
      job.work
      assert_equal [["bar", 3], ["foo", 2]], account.tag_scores.reload.pluck(:tag_name, :score)
    end

    describe_with_arturo_enabled :measure_tag_scores_job do
      it "calls populate" do
        job.expects(:populate).times(number_of_accounts_used_by_the_job)
        job.work
      end

      it "measures the execution time" do
        ZendeskAPM.expects(:trace).with(
          'populate',
          service: Zendesk::Maintenance::Jobs::ThesaurusResetJob::APM_SERVICE_NAME
        ).times(number_of_accounts_used_by_the_job)
        job.work
      end
    end
  end

  def delete_all_taggings
    Tagging.without_arsi.delete_all
  end

  def create_taggings
    delete_all_taggings

    Tagging.create!(account: account, tag_name: "foo", taggable: tickets("minimum_1"))
    Tagging.create!(account: account, tag_name: "foo", taggable: tickets("minimum_2"))
    Tagging.create!(account: account, tag_name: "bar", taggable: tickets("minimum_1"))

    Tagging.create!(account: account, tag_name: "old", taggable: tickets("minimum_1")) { |t| t.created_at = 6.months.ago }
  end
end
