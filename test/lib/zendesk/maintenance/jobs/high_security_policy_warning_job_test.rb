require_relative "../../../../support/job_helper"

SingleCov.covered!

describe 'HighSecurityPolicyWarningJob' do
  fixtures :accounts, :subscriptions, :users

  before do
    Timecop.freeze
    ActionMailer::Base.deliveries = []
    @account = accounts(:minimum)
    @account.update_attribute(:created_at, 60.days.ago)
    @job = Zendesk::Maintenance::Jobs::HighSecurityPolicyWarningJob.new(false, false)
  end

  describe "#accounts_created_60_days_ago" do
    it "returns all accounts created 60 days ago" do
      assert @job.send(:accounts_created_60_days_ago).include?(@account)
    end

    it "does not return accounts created before" do
      @account.update_attribute(:created_at, 61.days.ago)
      refute @job.send(:accounts_created_60_days_ago).include?(@account)
    end

    it "does not return accounts created after" do
      @account.update_attribute(:created_at, 59.days.ago)
      refute @job.send(:accounts_created_60_days_ago).include?(@account)
    end

    it "does not return inactive accounts" do
      @account.update_attribute(:is_active, false)
      refute @job.send(:accounts_created_60_days_ago).include?(@account)
    end

    it "does not return non serviceable accounts" do
      @account.update_attribute(:is_active, true)
      @account.update_attribute(:is_serviceable, false)
      refute @job.send(:accounts_created_60_days_ago).include?(@account)
    end

    describe "shell accounts that do not have a Support product" do
      before do
        @shell_account = accounts(:shell_account_without_support)
        @shell_account.update_attribute(:created_at, 60.days.ago)
        assert @shell_account.is_active?
        assert_equal Zendesk::Accounts::Locking::LockState::OPEN, @shell_account.lock_state
      end

      it "does not return shell accounts" do
        refute @job.send(:accounts_created_60_days_ago).include?(@shell_account)
      end
    end
  end

  describe "#execute" do
    before { ActionMailer::Base.perform_deliveries = true }

    describe "for accounts that have high security policy" do
      before do
        @high_security_policy = Zendesk::SecurityPolicy::High.new(@account)
        Account.any_instance.stubs(:agent_security_policy).returns(@high_security_policy)
        Account.any_instance.stubs(:end_user_security_policy).returns(@high_security_policy)

        @job.execute
      end

      should_change("the number of mail deliveries", by: 1) { ActionMailer::Base.deliveries.size }
    end

    Zendesk::SecurityPolicy.all.reject { |policy| policy.id == Zendesk::SecurityPolicy::High.id }.each do |level|
      describe "for accounts that with security policy id = #{level}" do
        before do
          @security_policy = level.new(@account)
          Account.any_instance.stubs(:agent_security_policy).returns(@security_policy)
          Account.any_instance.stubs(:end_user_security_policy).returns(@security_policy)

          @job.execute
        end

        should_not_change("the number of mail deliveries") { ActionMailer::Base.deliveries.size }
      end
    end
  end
end
