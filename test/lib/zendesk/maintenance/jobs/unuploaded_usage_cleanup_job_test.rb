require_relative '../../../../support/job_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::UnuploadedUsageCleanupJob do
  describe ".perform" do
    subject { Zendesk::Maintenance::Jobs::UnuploadedUsageCleanupJob.perform }

    let(:account)       { accounts(:minimum) }
    let(:redis_client)  { Zendesk::RedisStore.client }
    let(:statsd_client) { stub(:timing) }
    let(:metric_scope)  { "cleanup_job_cache_events" }

    let(:cache_manager) do
      ZBC::Zuora::Voice::UsageSubscriptionCacheManager.new(account, metric_scope)
    end

    let(:cache_manager_statsd_client) do
      cache_manager.send(:statsd_client)
    end

    before do
      redis_client.del("voice_usage_cleanup_job_status:shard_#{account.shard_id}")
      ::Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
      statsd_client.stubs(:timing)
      statsd_client.stubs(:increment)
    end

    describe "when the billing_voice_unuploaded_usage_cleanup_job_monitoring is ON" do
      before do
        Arturo.enable_feature! :billing_voice_unuploaded_usage_cleanup_job_monitoring
        ZBC::Zuora::Jobs::VoiceUsageJob.stubs(:enqueue)
      end

      describe "when the billing_voice_unuploaded_usage_cleanup_job is ON" do
        before { Arturo.enable_feature! :billing_voice_unuploaded_usage_cleanup_job }

        describe "and an instance of this job is already running" do
          before do
            redis_client.set("voice_usage_cleanup_job_status:shard_#{account.shard_id}", true, nx: true)
          end

          it "raises AlreadySweepingError" do
            assert_raises Zendesk::Maintenance::Jobs::UnuploadedUsageCleanupJob::AlreadySweepingError do
              subject
            end
          end
        end

        describe "and an instance of this job is NOT already running" do
          it "can acquire the lock" do
            assert_equal redis_client.set("voice_usage_cleanup_job_status:shard_#{account.shard_id}", true, nx: true), true
          end

          it "increments the acquire_lock counter" do
            statsd_client.expects(:increment).with("acquire_lock", tags: ["shard_id:#{account.shard_id}"])

            subject
          end

          describe "with an account with valid subscription name cached" do
            let!(:usage1) do
              Timecop.travel(5.hours.ago) do
                ZBC::Zuora::VoiceUsage.create!(
                  id: 100001,
                  account_id: account.id,
                  usage_type: "usage",
                  units: 1000
                )
              end
            end

            let!(:usage2) do
              Timecop.travel(5.hours.ago) do
                ZBC::Zuora::VoiceUsage.create!(
                  id: 100002,
                  account_id: account.id,
                  usage_type: "usage",
                  units: 1000
                )
              end
            end

            before do
              redis_client.set("#{account.id}:talk_usage_subscription_name", "ZD-S00111111")
            end

            it "uploads the usages" do
              subscription_name = cache_manager.subscription_name

              ZBC::Zuora::Jobs::VoiceUsageJob.expects(:enqueue).with(usage1.account_id, usage1.id, subscription_name)
              ZBC::Zuora::Jobs::VoiceUsageJob.expects(:enqueue).with(usage2.account_id, usage2.id, subscription_name)

              subject
            end

            it "releases the lock" do
              redis_client.expects(:del).with("voice_usage_cleanup_job_status:shard_#{account.shard_id}")
              statsd_client.expects(:increment).with("release_lock", tags: ["shard_id:#{account.shard_id}"])

              subject
            end
          end

          describe "with an account with valid subscription name NOT cached" do
            describe "and with valid subscription on Zuora" do
              let!(:usage3) do
                Timecop.travel(2.hours.ago) do
                  ZBC::Zuora::VoiceUsage.create!(
                    id: 100003,
                    account_id: account.id,
                    usage_type: "usage",
                    units: 1000
                  )
                end
              end

              let!(:usage4) do
                Timecop.travel(5.hours.ago) do
                  ZBC::Zuora::VoiceUsage.create!(
                    id: 100004,
                    account_id: account.id,
                    usage_type: "usage",
                    units: 1000
                  )
                end
              end

              let(:usage_subscription) do
                OpenStruct.new(
                  name:            "ZD-S00111111",
                  term_type:       "TERMED",
                  term_start_date: 3.months.ago,
                  term_end_date:   9.months.from_now
                )
              end

              let(:subscriptions) { [usage_subscription] }

              let(:valid_subscription_name_cache_message) do
                ZBC::Zuora::Voice::UsageSubscriptionCacheManager.
                  const_get(:CACHE_MESSAGES)[:valid_subscription_name]
              end

              before do
                redis_client.del("#{account.id}:talk_usage_subscription_name")
                ZBC::Zuora::Voice::UsageSubscriptionsFetcher.stubs(:call).with(account).returns(subscriptions)
              end

              it "uploads usages" do
                subscription_name = cache_manager.subscription_name

                ZBC::Zuora::Jobs::VoiceUsageJob.expects(:enqueue).with(usage3.account_id, usage3.id).never
                ZBC::Zuora::Jobs::VoiceUsageJob.expects(:enqueue).with(usage4.account_id, usage4.id, subscription_name)

                subject
              end

              it "increments the valid_subscription_name_cache_message counter" do
                cache_manager_statsd_client.expects(:increment).with("#{metric_scope}.#{valid_subscription_name_cache_message}")

                subject
              end

              it "releases the lock" do
                redis_client.expects(:del).with("voice_usage_cleanup_job_status:shard_#{account.shard_id}")
                statsd_client.expects(:increment).with("release_lock", tags: ["shard_id:#{account.shard_id}"])

                subject
              end
            end

            describe "and with missing valid subscription on Zuora" do
              let!(:usage5) do
                Timecop.travel(5.hours.ago) do
                  ZBC::Zuora::VoiceUsage.create!(
                    id: 100005,
                    account_id: account.id,
                    usage_type: "usage",
                    units: 1000
                  )
                end
              end

              let(:subscriptions) { [] }

              let(:missing_usage_subscription_cache_message) do
                ZBC::Zuora::Voice::UsageSubscriptionCacheManager.
                  const_get(:CACHE_MESSAGES)[:missing_usage_subscription]
              end

              before do
                redis_client.del("#{account.id}:talk_usage_subscription_name")
                ZBC::Zuora::Voice::UsageSubscriptionsFetcher.stubs(:call).with(account).returns(subscriptions)
                Voice::UsageSubscriptionCorrectorJob.stubs(:enqueue)
              end

              it "calls Voice::UsageSubscriptionCorrectorJob" do
                Voice::UsageSubscriptionCorrectorJob.expects(:enqueue).with(account.id)

                subject
              end

              it "does NOT upload usages" do
                ZBC::Zuora::Jobs::VoiceUsageJob.expects(:enqueue).never

                subject
              end

              it "increments the missing_usage_subscription_cache_message counter" do
                cache_manager_statsd_client.expects(:increment).with("#{metric_scope}.#{missing_usage_subscription_cache_message}")

                subject
              end

              it "releases the lock" do
                redis_client.expects(:del).with("voice_usage_cleanup_job_status:shard_#{account.shard_id}")
                statsd_client.expects(:increment).with("release_lock", tags: ["shard_id:#{account.shard_id}"])

                subject
              end
            end
          end

          describe "error handling" do
            let!(:usage5) do
              Timecop.travel(5.hours.ago) do
                ZBC::Zuora::VoiceUsage.create!(
                  id: 100001,
                  account_id: account.id,
                  usage_type: "usage",
                  units: 1000
                )
              end
            end

            let!(:usage6) do
              Timecop.travel(5.hours.ago) do
                ZBC::Zuora::VoiceUsage.create!(
                  id: 100002,
                  account_id: account.id,
                  usage_type: "usage",
                  units: 1000
                )
              end
            end

            before do
              redis_client.set("#{account.id}:talk_usage_subscription_name", "ZD-S00111111")
            end

            describe "when there is a failure with sweeping" do
              it "increments the sweeping_error counter" do
                Account.stubs(:find).raises(ActiveRecord::RecordNotFound)

                statsd_client.expects(:increment).with("sweeping_error")

                subject
              end
            end

            describe "when there is a failure with the query for collecting unuploaded usages" do
              it "increments the query_usages_error counter" do
                ZBC::Zuora::VoiceUsage.stubs(:where).raises(ActiveRecord::AdapterNotFound)

                statsd_client.expects(:increment).with("query_usages_error")

                subject
              end
            end

            describe "when there is a failure with uploading a collection of usages for an account" do
              it "increments the upload_usages_error counter" do
                ZBC::Zuora::Jobs::VoiceUsageJob.stubs(:enqueue).raises(StandardError)

                statsd_client.expects(:increment).with("upload_usages_error")

                subject
              end
            end
          end
        end
      end

      describe "when the billing_voice_unuploaded_usage_cleanup_job is OFF" do
        before { Arturo.disable_feature! :billing_voice_unuploaded_usage_cleanup_job }

        describe "and an instance of this job is already running" do
          it "raises AlreadySweepingError" do
            redis_client.set(
              "voice_usage_cleanup_job_status:shard_#{account.shard_id}",
              true,
              nx: true
            )

            assert_raises Zendesk::Maintenance::Jobs::UnuploadedUsageCleanupJob::AlreadySweepingError do
              subject
            end
          end
        end

        describe "and an instance of this job is NOT already running" do
          it "can acquire the lock" do
            assert_equal redis_client.set("voice_usage_cleanup_job_status:shard_#{account.shard_id}", true, nx: true), true
          end

          it "increments the acquire_lock counter" do
            statsd_client.expects(:increment).with("acquire_lock", tags: ["shard_id:#{account.shard_id}"])

            subject
          end

          describe "with an account with valid subscription name cached" do
            let!(:usage1) do
              Timecop.travel(5.hours.ago) do
                ZBC::Zuora::VoiceUsage.create!(
                  id: 100001,
                  account_id: account.id,
                  usage_type: "usage",
                  units: 1000
                )
              end
            end

            let!(:usage2) do
              Timecop.travel(5.hours.ago) do
                ZBC::Zuora::VoiceUsage.create!(
                  id: 100002,
                  account_id: account.id,
                  usage_type: "usage",
                  units: 1000
                )
              end
            end

            before do
              redis_client.set("#{account.id}:talk_usage_subscription_name", "ZD-S00111111")
            end

            it "does NOT upload the usages" do
              ZBC::Zuora::Jobs::VoiceUsageJob.expects(:enqueue).never

              subject
            end

            it "releases the lock" do
              redis_client.expects(:del).with("voice_usage_cleanup_job_status:shard_#{account.shard_id}")
              statsd_client.expects(:increment).with("release_lock", tags: ["shard_id:#{account.shard_id}"])

              subject
            end
          end

          describe "with an account with valid subscription name NOT cached" do
            describe "and with valid subscription on Zuora" do
              let!(:usage3) do
                Timecop.travel(2.hours.ago) do
                  ZBC::Zuora::VoiceUsage.create!(
                    id: 100003,
                    account_id: account.id,
                    usage_type: "usage",
                    units: 1000
                  )
                end
              end

              let!(:usage4) do
                Timecop.travel(5.hours.ago) do
                  ZBC::Zuora::VoiceUsage.create!(
                    id: 100004,
                    account_id: account.id,
                    usage_type: "usage",
                    units: 1000
                  )
                end
              end

              let(:usage_subscription) do
                OpenStruct.new(
                  name:            "ZD-S00111111",
                  term_type:       "TERMED",
                  term_start_date: 3.months.ago,
                  term_end_date:   9.months.from_now
                )
              end

              let(:subscriptions) { [usage_subscription] }

              let(:valid_subscription_name_cache_message) do
                ZBC::Zuora::Voice::UsageSubscriptionCacheManager.
                  const_get(:CACHE_MESSAGES)[:valid_subscription_name]
              end

              before do
                redis_client.del("#{account.id}:talk_usage_subscription_name")
                ZBC::Zuora::Voice::UsageSubscriptionsFetcher.stubs(:call).with(account).returns(subscriptions)
              end

              it "does NOT upload usages" do
                ZBC::Zuora::Jobs::VoiceUsageJob.expects(:enqueue).never

                subject
              end

              it "increments the valid_subscription_name_cache_message counter" do
                cache_manager_statsd_client.expects(:increment).with("#{metric_scope}.#{valid_subscription_name_cache_message}")

                subject
              end

              it "releases the lock" do
                redis_client.expects(:del).with("voice_usage_cleanup_job_status:shard_#{account.shard_id}")
                statsd_client.expects(:increment).with("release_lock", tags: ["shard_id:#{account.shard_id}"])

                subject
              end
            end

            describe "and with missing valid subscription on Zuora" do
              let!(:usage5) do
                Timecop.travel(5.hours.ago) do
                  ZBC::Zuora::VoiceUsage.create!(
                    id: 100005,
                    account_id: account.id,
                    usage_type: "usage",
                    units: 1000
                  )
                end
              end

              let(:subscriptions) { [] }

              let(:missing_usage_subscription_cache_message) do
                ZBC::Zuora::Voice::UsageSubscriptionCacheManager.
                  const_get(:CACHE_MESSAGES)[:missing_usage_subscription]
              end

              before do
                redis_client.del("#{account.id}:talk_usage_subscription_name")
                ZBC::Zuora::Voice::UsageSubscriptionsFetcher.stubs(:call).with(account).returns(subscriptions)
                Voice::UsageSubscriptionCorrectorJob.expects(:enqueue).once
              end

              it "does NOT upload usages" do
                ZBC::Zuora::Jobs::VoiceUsageJob.expects(:enqueue).never

                subject
              end

              it "increments the missing_usage_subscription_cache_message counter" do
                cache_manager_statsd_client.expects(:increment).with("#{metric_scope}.#{missing_usage_subscription_cache_message}")

                subject
              end

              it "releases the lock" do
                redis_client.expects(:del).with("voice_usage_cleanup_job_status:shard_#{account.shard_id}")
                statsd_client.expects(:increment).with("release_lock", tags: ["shard_id:#{account.shard_id}"])

                subject
              end
            end
          end
        end
      end
    end

    describe "when the billing_voice_unuploaded_usage_cleanup_job_monitoring is OFF" do
      before { Arturo.disable_feature! :billing_voice_unuploaded_usage_cleanup_job_monitoring }

      it "queries database" do
        statsd_client.expects(:timing).once
        ZBC::Zuora::VoiceUsage.expects(:where).once.returns(
          stub(
            select: stub(
              group_by: {}
            )
          )
        )

        subject
      end

      it "skips all accounts" do
        ZBC::Zuora::Jobs::VoiceUsageJob.expects(:enqueue).never
        Voice::UsageSubscriptionCorrectorJob.expects(:enqueue).never
        ZBC::Zuora::Voice::UsageSubscriptionCacheManager.expects(:new).never

        subject
      end

      it "releases the lock" do
        redis_client.expects(:del).with("voice_usage_cleanup_job_status:shard_#{account.shard_id}")
        statsd_client.expects(:increment).with("release_lock", tags: ["shard_id:#{account.shard_id}"])

        subject
      end
    end
  end
end
