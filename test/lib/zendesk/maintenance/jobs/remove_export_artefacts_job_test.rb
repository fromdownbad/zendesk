require_relative "../../../../support/job_helper"
require 'zendesk_voyager_api_client'

SingleCov.covered!

describe 'RemoveExportArtefactsJob' do
  include DataDeletionJobSupport

  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:job) { create_job(account, Zendesk::Maintenance::Jobs::RemoveExportArtefactsJob) }

  describe '#erase_for_moved' do
    subject { job.send(:erase_for_moved) }

    let(:response) { Voyager::API::APIResponse.new(Faraday::Response.new(status: 200)) }

    it 'sends request to voyager' do
      Voyager::API::Client.
        any_instance.
        expects(:clean_artefacts).
        with(shard_id: account.shard_id).
        returns(response)

      subject
    end

    describe 'when the response is 404' do
      let(:response) { Voyager::API::APIResponse.new(Faraday::Response.new(status: 404)) }

      before do
        Voyager::API::Client.
          any_instance.
          stubs(:clean_artefacts).
          with(shard_id: account.shard_id).
          returns(response)
      end

      it 'is nil' do
        assert_nil subject
      end
    end

    describe 'when the response is not successful' do
      let(:response) do
        Voyager::API::APIResponse.new(
          Faraday::Response.new(
            status: 500,
            body: { 'errors' => ['not successful'] }
          )
        )
      end

      before do
        Voyager::API::Client.
          any_instance.
          stubs(:clean_artefacts).
          with(shard_id: account.shard_id).
          returns(response)
      end

      it 'raises' do
        assert_raises(RuntimeError) do
          subject
        end
      end
    end
  end
end
