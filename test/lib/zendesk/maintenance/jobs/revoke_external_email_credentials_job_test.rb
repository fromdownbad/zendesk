require_relative '../../../../support/job_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::RevokeExternalEmailCredentialsJob do
  include DataDeletionJobSupport

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:job) { create_job(account, Zendesk::Maintenance::Jobs::RevokeExternalEmailCredentialsJob) }
  let(:user) { users(:minimum_agent) }

  def create_for_account!(username)
    account.external_email_credentials.new(
      encrypted_value: 'xxxx',
      username: username,
      current_user: user
    ).tap do |external_email_credential|
      external_email_credential.encrypted_value = 'xxxx'
      external_email_credential.save!
    end
  end

  describe '#erase_for_canceled' do
    describe 'with existing credentials' do
      before do
        3.times do |n|
          create_for_account!("foo#{n}@bar.com")
        end
      end

      it 'invokes RevokeExternalEmailCredentialJob for each credential' do
        ::RevokeExternalEmailCredentialJob.expects(:work).times(3)
        job.send(:erase_for_canceled)
      end
    end

    describe 'when credentials do not have encrypted_values' do
      before do
        ExternalEmailCredential.any_instance.stubs(:encrypted_value).returns(nil)
      end

      it 'invokes RevokeExternalEmailCredentialJob for each credential with an encrypted value' do
        ::RevokeExternalEmailCredentialJob.expects(:work).never
        job.send(:erase_for_canceled)
      end
    end

    describe 'with no credentials' do
      before do
        account.external_email_credentials.delete_all
      end

      it { assert job.send(:erase_for_canceled) }
    end
  end

  describe '#verify_for_canceled' do
    it 'returns true' do
      assert job.send(:verify_for_canceled)
    end
  end
end
