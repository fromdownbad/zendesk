require_relative "../../../../support/job_helper"
require_relative "../../../../support/api_activity_test_helper"

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::ApiActivityDeleteJob do
  include ApiActivityTestHelper

  fixtures :accounts, :account_settings, :tickets, :api_activity_clients

  let(:job) { Zendesk::Maintenance::Jobs::ApiActivityDeleteJob }
  let(:account) { accounts(:minimum) }
  let(:support_account) { accounts(:support) }

  before do
    Account.any_instance.stubs(:has_api_activity_job?).returns(true)
    Timecop.freeze(Time.now)
    @current_time = Time.now
  end

  describe ".work_on_shard" do
    describe "where last request time is old" do
      before do
        populate_client_data(account, @current_time - 1.hour, @current_time - 3.days)
        add_client_data(support_account, @current_time - 1.hour, @current_time - 1.day)
        add_client_data(support_account, @current_time - 1.week, @current_time - 1.week)
        job.work_on_shard(account.shard_id)
      end

      it "removes old clients and keeps new clients" do
        assert_equal(1, ApiActivityClient.count)
        assert_equal(support_account.id, ApiActivityClient.first.account_id)
      end
    end

    describe "where last request time is ok" do
      before do
        populate_client_data(account, @current_time - 1.hour, @current_time - 1.hour)
        add_client_data(support_account, @current_time - 1.hour, @current_time - 1.hour)
        job.work_on_shard(account.shard_id)
      end

      it "removes old clients and keeps new clients" do
        assert_equal(3, ApiActivityClient.count)
      end
    end
  end
end
