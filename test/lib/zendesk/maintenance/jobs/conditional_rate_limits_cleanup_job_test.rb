require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'ConditionalRateLimitsCleanupJob' do
  fixtures :accounts

  let(:account) { accounts(:minimum) }

  before do
    cleanup_time = Time.parse('2020-01-22T02:58:43+00:00')
    Timecop.freeze(cleanup_time)
  end

  describe "#work_on_account" do
    before :each do
      account.conditional_rate_limits = nil
      account.conditional_rate_limits = conditional_rate_limits
      account.texts.save!
    end

    describe "when account has expired conditional rate limits" do
      let(:conditional_rate_limits) do
        [
          {
            'prop_key' => 'limit_1',
            'limit' => 1,
            'interval_seconds' => 1,
            'expires_on' => '2020-01-21T02:58:43+00:00',
            'owner_email' => 'abc@zendesk.com',
            'properties' => {
              'request_method' => 'POST'
            }
          },
          {
            'prop_key' => 'limit_2',
            'limit' => 1,
            'interval_seconds' => 1,
            'expires_on' => '2020-02-25T02:58:43+00:00',
            'owner_email' => 'xyz@zendesk.com',
            'properties' => {
              'endpoint' => 'users'
            }
          }
        ]
      end

      it "deletes expired conditional rate limits" do
        expected_result = [
          {
            'prop_key' => 'limit_2',
            'limit' => 1,
            'interval_seconds' => 1,
            'expires_on' => '2020-02-25T02:58:43+00:00',
            'owner_email' => 'xyz@zendesk.com',
            'properties' => {
              'endpoint' => 'users'
            }
          }
        ]

        Rails.logger.expects(:append_attributes).
          with(conditional_rate_limit: { subdomain: 'minimum', prop_key: 'limit_1', expires_on: '2020-01-21T02:58:43+00:00' })

        MonitorMailer.expects(:deliver_rate_limit_notification).
          with(account,
            'subdomain' => 'minimum',
            'prop_key' => 'limit_1',
            'expires_on' => '2020-01-21T02:58:43+00:00',
            'owner_email' => 'abc@zendesk.com').times(1)

        Zendesk::Maintenance::Jobs::ConditionalRateLimitsCleanupJob.work_on_account(account)
        assert_equal account.conditional_rate_limits.length, 1
        assert_equal account.conditional_rate_limits, expected_result
      end
    end

    describe "when account does not have expired conditional rate limits" do
      let(:conditional_rate_limits) do
        [
          {
            'prop_key' => 'limit_1',
            'limit' => 1,
            'interval_seconds' => 1,
            'owner_email' => nil,
            'expires_on' => '2020-02-28T02:58:43+00:00',
            'properties' => {
              'request_method' => 'POST'
            }
          },
          {
            'prop_key' => 'limit_2',
            'limit' => 1,
            'interval_seconds' => 1,
            'owner_email' => 'abc@zendesk.com',
            'expires_on' => '2020-02-25T02:58:43+00:00',
            'properties' => {
              'endpoint' => 'users'
            }
          }
        ]
      end

      it "does not delete any conditional rate limits" do
        Zendesk::Maintenance::Jobs::ConditionalRateLimitsCleanupJob.work_on_account(account)
        assert_equal account.conditional_rate_limits.length, 2
      end
    end

    describe "when a conditional rate limit does not have an expire date setup" do
      let(:conditional_rate_limits) do
        [
          {
            'prop_key' => 'limit_1',
            'limit' => 1,
            'interval_seconds' => 1,
            'properties' => {
              'request_method' => 'GET'
            }
          }
        ]
      end

      it "does not delete the conditional rate limit" do
        Zendesk::Maintenance::Jobs::ConditionalRateLimitsCleanupJob.work_on_account(account)
        assert_equal account.conditional_rate_limits.length, 1
      end
    end

    describe "when a conditional rate limit does not have an owner email setup" do
      let(:conditional_rate_limits) do
        [
          {
            'prop_key' => 'limit_1',
            'limit' => 1,
            'interval_seconds' => 1,
            'expires_on' => '2020-01-21T02:58:43+00:00',
            'owner_email' => nil,
            'properties' => {
              'request_method' => 'GET'
            }
          }
        ]
      end

      it "does not send a notification email" do
        MonitorMailer.expects(:deliver_rate_limit_notification).
          with(account,
            subdomain: 'minimum',
            prop_key: 'limit_1',
            expires_on: '2020-01-21T02:58:43+00:00',
            owner_email: nil).never
        Zendesk::Maintenance::Jobs::ConditionalRateLimitsCleanupJob.work_on_account(account)
        assert_equal account.conditional_rate_limits.length, 0
      end
    end
  end

  describe '#args_to_log' do
    it 'works' do
      assert_equal({}, Zendesk::Maintenance::Jobs::ConditionalRateLimitsCleanupJob.args_to_log)
    end
  end
end
