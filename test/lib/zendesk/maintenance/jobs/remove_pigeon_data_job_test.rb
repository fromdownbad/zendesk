require_relative '../../../../support/job_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::RemovePigeonDataJob do
  include DataDeletionJobSupport

  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let!(:job) { create_job(account, Zendesk::Maintenance::Jobs::RemovePigeonDataJob) }

  let(:delete_response) do
    OpenStruct.new(status: 200, body: { 'count' => 10 })
  end

  let(:delete_response_last_page) do
    OpenStruct.new(status: 200, body: { 'count' => 0 })
  end

  describe 'reason = moved' do
    before do
      job.stubs(:pod_local_move?).returns(pod_local)

      job.account.update_attribute(:shard_id, 2)
      job.job_audit.audit.reason = 'moved'
      job.job_audit.shard_id = 1
    end

    let(:pod_local) { false }

    describe 'inter-pod move' do
      it 'deletes through pigeon' do
        Pigeon::Client.any_instance.expects(:delete_account).with(account.id).twice.returns(delete_response, delete_response_last_page)
        job.work
      end
    end

    describe 'intra-pod move' do
      let(:pod_local) { true }

      it 'noops' do
        Pigeon::Client.any_instance.expects(:delete_account).with(account.id).never
        job.work
      end
    end
  end

  describe 'account canceled' do
    before do
      job.job_audit.stubs(:shard_id).returns(1)
      job.job_audit.audit.stubs(:reason).returns('canceled')
    end

    it 'sends a request to pigeon to delete the data associated with an account' do
      Pigeon::Client.any_instance.expects(:delete_account).with(account.id).twice.returns(delete_response, delete_response_last_page)
      job.work
    end
  end

  describe 'dry_run' do
    it 'should never ask pigeon to delete an account' do
      job = create_job(account, Zendesk::Maintenance::Jobs::RemovePigeonDataJob, dry_run: true)
      Pigeon::Client.any_instance.expects(:delete_account).with(account.id).never
      job.work
    end
  end

  describe 'kill switch enabled' do
    it 'raises' do
      Arturo.enable_feature! :account_deletion_kill_switch
      assert_raises Zendesk::DataDeletion::AccountDeletionKillSwitch::KillSwitchEnabled do
        job.send :erase_for_canceled
      end
    end
  end
end
