require_relative "../../../../support/job_helper"

SingleCov.covered!

describe 'HourlyMaintenanceJob' do
  subject { Zendesk::Maintenance::Jobs::HourlyMaintenanceJob }

  describe "#work" do
    it 'runs all daily jobs successfully' do
      Zendesk::Maintenance::Jobs::HourlyMaintenanceJob::ALL_JOBS.each do |job|
        job.expects(:execute).with do |dryrun, console, time|
          !dryrun &&
          !console &&
          time
        end
      end

      subject.work
    end

    it 'records error when job fails' do
      ZendeskExceptions::Logger.expects(:record).at_least_once
      Zendesk::Maintenance::Jobs::HourlyMaintenanceJob::ALL_JOBS.each do |job|
        job.expects(:new).with do |dryrun, console, time|
          !dryrun &&
          !console &&
          time
        end.raises('Confirmed job is instantiated correctly.')
      end

      subject.work
    end
  end
end
