require_relative "../../../../support/job_helper"

SingleCov.covered! uncovered: 2

describe 'NotificationCheckerJob' do
  describe "notification checker job" do
    before do
      @job = Zendesk::Maintenance::Jobs::NotificationCheckerJob
      evaluated = ERB.new(File.read(Rails.root + 'config/resque-schedule.yml')).result
      @resque_schedule = YAML.load(evaluated)
    end

    it "runs every 30 minutes" do
      assert_equal "*/30 * * * *", @resque_schedule[@job.name]["cron"]
    end

    it "logs to statsn" do
      ActiveRecord::Base.on_shard(1) do
        @job.work_on_shard(1)
      end
    end
  end
end
