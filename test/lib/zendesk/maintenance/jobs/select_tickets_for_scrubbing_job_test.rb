require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'SelectTicketsForScrubbingJob' do
  fixtures :accounts, :tickets

  let(:account_one) { accounts(:minimum) }
  let(:account_two) { accounts(:minimum_sdk) }
  let(:account_three) { accounts(:serialization) }
  let(:ticket_one) { account_one.tickets.first }
  let(:ticket_two) { account_two.tickets.first }
  let(:ticket_three) { account_three.tickets.first }
  let(:deleted_ticket) { Ticket.unscoped { tickets(:minimum_deleted) } }

  subject { Zendesk::Maintenance::Jobs::SelectTicketsForScrubbingJob }

  let(:work) do
    [account_one, account_two, account_three].each do |account|
      subject.work_on_account(account)
    end
  end

  before do
    Timecop.travel 31.days.ago do
      [ticket_one, ticket_two].each do |ticket|
        ticket.will_be_saved_by(ticket.submitter)
        ticket.soft_delete!
      end

      # Should update before archiving
      deleted_ticket.update_column :updated_at, Time.now
      archive_and_delete(deleted_ticket)
    end

    Timecop.travel 15.days.ago do
      ticket_three.will_be_saved_by(ticket_three.submitter)
      ticket_three.soft_delete!
    end
  end

  def stub_arturos(value)
    [account_one, account_two, account_three].each do |account|
      account.stubs(:has_ticket_scrubbing?).returns(value)
    end
  end

  describe 'enqueuing jobs' do
    let(:kill_switch_enabled) { false }

    before do
      Arturo.stubs(:feature_enabled_for_dbcluster?).
        with(:kill_switch_select_tickets_for_scrubbing_job, '1.1').
        returns(kill_switch_enabled)

      @old_db_shards_to_clusters = DB_SHARDS_TO_CLUSTERS
      Object.send(:remove_const, :DB_SHARDS_TO_CLUSTERS)
      Object.const_set(:DB_SHARDS_TO_CLUSTERS, { account_one.shard_id => '1.1' }.freeze)
    end

    after do
      Object.send(:remove_const, :DB_SHARDS_TO_CLUSTERS)
      Object.const_set(:DB_SHARDS_TO_CLUSTERS, @old_db_shards_to_clusters)
    end

    describe 'when the DB cluster kill switch is enabled' do
      let(:kill_switch_enabled) { true }

      it 'does not enqueue jobs' do
        stub_arturos(true)

        Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_one.id, user_id: User.system.id, tickets_ids: [ticket_one.id, deleted_ticket.id]).never
        Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_two.id, user_id: User.system.id, tickets_ids: [ticket_two.id]).never
        Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_three.id, user_id: User.system.id, tickets_ids: [ticket_three.id]).never

        work
      end
    end

    it 'enqueues one job per account' do
      stub_arturos(true)

      Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_one.id, user_id: User.system.id, tickets_ids: [ticket_one.id, deleted_ticket.id])
      Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_two.id, user_id: User.system.id, tickets_ids: [ticket_two.id])
      Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_three.id, user_id: User.system.id, tickets_ids: [ticket_three.id]).never

      work
    end

    it 'enqueues no job without the arturo bit' do
      stub_arturos(false)

      Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_one.id, user_id: User.system.id, tickets_ids: [ticket_one.id, deleted_ticket.id]).never
      Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_two.id, user_id: User.system.id, tickets_ids: [ticket_two.id]).never
      Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_three.id, user_id: User.system.id, tickets_ids: [ticket_three.id]).never

      work
    end

    describe 'ticket_scrubbing_low_priority_job is enabled' do
      before do
        stub_arturos(true)
        account_one.stubs(:has_ticket_scrubbing_low_priority_job?).returns(true)
      end

      it 'enqueues low priority jobs' do
        Zendesk::Maintenance::Jobs::TicketDeletionLowPriorityJob.expects(:enqueue).with(account_id: account_one.id, user_id: User.system.id, tickets_ids: [ticket_one.id, deleted_ticket.id])
        Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_two.id, user_id: User.system.id, tickets_ids: [ticket_two.id])
        Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_three.id, user_id: User.system.id, tickets_ids: [ticket_three.id]).never

        work
      end
    end

    describe 'archiver is disabled' do
      before do
        ticket_one.account.settings.write_to_archive_v2_enabled = 'false'
        ticket_one.account.settings.save!
        ticket_one.account.must_equal deleted_ticket.account
      end

      it 'does not enqueue archived tickets' do
        stub_arturos(true)

        Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_one.id, user_id: User.system.id, tickets_ids: [ticket_one.id])
        Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:enqueue).with(account_id: account_two.id, user_id: User.system.id, tickets_ids: [ticket_two.id])

        work
      end
    end
  end

  describe '.args_to_log' do
    it 'does not add any new information for logging' do
      stub_arturos(true)
      Rails.logger.expects(:info).with('Enqueuing 2 ticket(s) for scrubbing for account 90538/minimum')

      subject.work_on_account(account_one)
    end
  end
end
