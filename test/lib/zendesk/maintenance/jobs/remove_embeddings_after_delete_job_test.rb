require_relative "../../../../support/job_helper"

SingleCov.covered!

describe 'RemoveEmbeddingsDataJob' do
  include DataDeletionJobSupport

  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:job) { create_job(account, Zendesk::Maintenance::Jobs::RemoveEmbeddingsAfterDeleteJob) }

  before do
    job.stubs(:pod_local_move?).returns(false)
    job.job_audit.audit.reason = 'moved'
    job.account.update_attribute(:shard_id, 2)
    job.job_audit.shard_id = 1
  end

  describe 'cross region move' do
    before do
      job.stubs(:same_region_move?).returns(false)
    end

    it 'sends requests to answerbot to delete embeddings' do
      Zendesk::AnswerBotService::InternalApiClient.any_instance.expects(:delete_embeddings).once
      job.work
    end
  end

  describe 'same region move' do
    before do
      job.stubs(:same_region_move?).returns(true)
    end

    it 'does not sends request to delete' do
      Zendesk::AnswerBotService::InternalApiClient.any_instance.expects(:delete_embeddings).never
      job.work
    end
  end
end
