require_relative "../../../../support/job_helper"
require 'zendesk/maintenance/jobs/compliance_move_job'

SingleCov.covered! uncovered: 2

describe Zendesk::Maintenance::Jobs::ComplianceMoveJob do
  fixtures :accounts, :subscriptions, :users, :payments

  before do
    @account = accounts(:minimum)
    @account2 = accounts(:multiproduct)
    @account3 = accounts(:with_groups)

    @churned_account = accounts(:trial)
    @churned_account.is_serviceable = false
    @churned_account.is_active = false
    @churned_account.save!
    @churned_account.soft_delete!
    @churned_account.reload
    @path = (Rails.env == 'development' || Rails.env == 'test') ? 'master' : Rails.env
  end

  def create_move(account, shard_id, pod_id, type)
    ComplianceMove.create!(
      account: account,
      account_subdomain: account.subdomain,
      src_shard_id: shard_id,
      src_pod_id: pod_id,
      move_type: type,
      move_scheduled: false
    )
  end

  describe '#is_churned?' do
    it 'recognized a delete account' do
      assert Zendesk::Maintenance::Jobs::ComplianceMoveJob.new(false, false).is_churned?(@churned_account)
    end

    it 'recognizes an active account' do
      refute Zendesk::Maintenance::Jobs::ComplianceMoveJob.new(false, false).is_churned?(@account)
    end
  end

  describe '#waiting' do
    it 'returns any unstarted compliance moves' do
      create_move(@account, 1, 1, 2)
      assert ComplianceMove.first
      assert_equal ComplianceMove.waiting.count, 1
    end
  end

  describe '#account_ids' do
    before do
      Zendesk::Maintenance::Jobs::ComplianceMoveJob.any_instance.stubs(
        eu_data_center_ids: [1],
        us_data_center_ids: [2],
        germany_data_center_ids: [3, 5],
        pci_ids: [2],
        advanced_security_ids: [3]
      )
    end

    it 'returns a hash of accounts, organized by tag' do
      compliance_move_job = Zendesk::Maintenance::Jobs::ComplianceMoveJob.new(false, false)
      account_ids = compliance_move_job.account_ids

      expected = { us: [2], eu: [1], de: [3, 5] }

      assert_equal expected, account_ids
    end
  end

  describe '#execute' do
    before do
      @addon = SubscriptionFeatureAddon.new
      @addon.boost_expires_at = (Time.now + 28.days).to_s
      @addon.account_id = @account.id
      @addon.name = 'eu_data_center'
      @addon.save!
      Account.any_instance.stubs(:pod_id).returns(2)
    end

    describe 'when the poddable_client gem returns data' do
      it 'will create a ComplianceMove' do
        compliance_move_job = Zendesk::Maintenance::Jobs::ComplianceMoveJob.new(false, false)
        compliance_move_job.execute
        assert_equal 1, ComplianceMove.count
      end
    end

    describe 'when the poddable_client gem returns an empty array' do
      before { PoddableClient::PodApi.any_instance.stubs(:get_pods).returns([]) }

      it 'will not create any ComplianceMoves' do
        compliance_move_job = Zendesk::Maintenance::Jobs::ComplianceMoveJob.new(false, false)
        compliance_move_job.execute
        assert_equal 0, ComplianceMove.count
      end
    end

    describe 'when the poddable_client gem returns nil' do
      before { PoddableClient::PodApi.any_instance.stubs(:get_pods).returns(nil) }

      it 'will not create any ComplianceMoves' do
        compliance_move_job = Zendesk::Maintenance::Jobs::ComplianceMoveJob.new(false, false)
        compliance_move_job.execute
        assert_equal 0, ComplianceMove.count
      end
    end

    describe 'if ComplianceMoves already exist' do
      before { create_move(@account3, 1, 1, 2) }

      it 'will purge them before creating any new moves' do
        assert_equal 1, ComplianceMove.count
        compliance_move_job = Zendesk::Maintenance::Jobs::ComplianceMoveJob.new(false, false)
        compliance_move_job.execute
        assert_equal 1, ComplianceMove.count
      end
    end

    after do
      ComplianceMove.without_arsi.delete_all
    end
  end

  describe '#purge_old_moves' do
    before do
      @account2 = accounts(:multiproduct)
      @account3 = accounts(:with_groups)

      create_move(@account, 305, 3, 2)
      create_move(@account2, 505, 5, 3)
      create_move(@account3, 805, 8, 4)

      Zendesk::Configuration.stubs(:fetch).with(:pod_id).returns(3)
      @compliance_move_job = Zendesk::Maintenance::Jobs::ComplianceMoveJob.new(false, false)
    end

    it 'will delete all existing pod-local ComplianceMoves, but none on other pods' do
      assert_equal 3, ComplianceMove.count
      assert_equal 1, ComplianceMove.where(src_pod_id: 3).count

      @compliance_move_job.purge_old_moves

      assert_equal 2, ComplianceMove.count
      assert_equal 0, ComplianceMove.where(src_pod_id: 3).count
    end

    after do
      ComplianceMove.without_arsi.delete_all
    end
  end

  describe '#create_compliance_moves' do
    describe 'for non-churned accounts' do
      before do
        @addon = SubscriptionFeatureAddon.new
        @addon.boost_expires_at = (Time.now + 28.days).to_s
        @addon.account_id = @account.id
      end

      describe 'for accounts with an eu_data_center addon' do
        before do
          @addon.name = 'eu_data_center'
          @addon.save!
        end

        describe 'if account is already in an eu pod' do
          before { Account.any_instance.stubs(:pod_id).returns(3) }

          it 'does not create a ComplianceMove' do
            compliance_move_job = Zendesk::Maintenance::Jobs::ComplianceMoveJob.new(false, false)
            compliance_move_job.create_compliance_moves

            assert_nil ComplianceMove.first
          end
        end

        describe 'if account is not already in an eu pod' do
          before { Account.any_instance.stubs(:pod_id).returns(2) }

          it 'creates the correct ComplianceMove' do
            compliance_move_job = Zendesk::Maintenance::Jobs::ComplianceMoveJob.new(false, false)
            compliance_move_job.create_compliance_moves
            assert_equal 2, ComplianceMove.first.move_type
          end
        end
      end
    end

    describe 'for churned accounts' do
      before do
        Account.any_instance.stubs(:pod_id).returns(3)
      end

      it 'ignores churned accounts' do
        Account.with_deleted do
          # This usually fails, cant' create a subscriptionfeatureaddon for a deleted account.
          #   NoMethodError: undefined method `is_sandbox?' for nil:NilClass
          # Lets just pretend the account was deleted _after_ the featureaddon was created.
          @addon = SubscriptionFeatureAddon.new
          @addon.name = 'eu_data_center'
          @addon.boost_expires_at = (Time.now + 28.days).to_s
          @addon.account_id = @churned_account.id
          @addon.save!
        end

        assert_equal 0, ComplianceMove.count
        Zendesk::Maintenance::Jobs::ComplianceMoveJob.execute
        assert_equal 0, ComplianceMove.count

        Account.with_deleted do # after_commit hooks blow up without this because account cannot be found
          @addon.destroy
        end
      end

      it 'removes previously created move for now-churned account' do
        assert_equal 0, ComplianceMove.count
        # again, lets pretend the account was churned after the ComplianceMove was created.
        create_move(@churned_account, 1, 1, 2)
        assert_equal 1, ComplianceMove.count

        Zendesk::Maintenance::Jobs::ComplianceMoveJob.execute
        assert_equal 0, ComplianceMove.count
      end
    end
  end
end
