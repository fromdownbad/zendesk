require_relative "../../../../support/test_helper"
require_relative "../../../../support/archive_helper"
require_relative "../../../../support/attachment_test_helper"

SingleCov.covered! uncovered: 4

describe Zendesk::Maintenance::Jobs::TicketDeletionJob do
  fixtures :all

  before do
    @account = accounts(:minimum)
    Arturo.disable_feature!(:prevent_deletion_if_churned)
    @ticket = @account.tickets.find_by_nice_id(1)
    @ticket.will_be_saved_by(users(:minimum_agent))
    @ticket.soft_delete!
  end

  def run_deletion_job(account, tickets)
    job = Zendesk::Maintenance::Jobs::TicketDeletionJob.new '12345', account_id: account.id, user_id: User.system.id, tickets_ids: tickets
    job.perform
  end

  describe "account with prevent_deletion_if_churned arturo set" do
    before do
      Arturo.enable_feature!(:prevent_deletion_if_churned)
    end

    it "creates a permanent deletion & subject_change audit" do
      assert_difference 'CIA::Event.count(:all)', 2 do
        tickets = Array(@ticket.id)
        run_deletion_job(@account, tickets)

        assert_equal 1, CIA::Event.where(action: 'subject_change').count
        assert_equal 1, CIA::Event.where(action: 'permanently_destroy').count
      end
    end

    it "runs scrub job of a soft deleted ticket but retains data" do
      tickets = Array(@ticket.id)
      run_deletion_job(@account, tickets)
      @ticket.reload

      Zendesk::Maintenance::Jobs::TicketDeletionJob.expects(:scrub_attributes).never
      assert_equal StatusType.DELETED, @ticket.status_id
      assert_equal 1, @ticket.taggings.count
      assert_equal 'SCRUBBED', @ticket.subject
      refute_equal 'x', @ticket.comments.last.send(:value).downcase
    end

    it "tracks and logs exceptions with ZendeskExceptions" do
      job = Zendesk::Maintenance::Jobs::TicketDeletionJob.new '12345', account_id: @account.id, user_id: User.system.id, tickets_ids: Array(@ticket.id)

      ZendeskExceptions::Logger.expects(:record)
      Zendesk::Scrub.stubs(:scrub_ticket_and_associations).raises(StandardError.new('ない'))
      job.send(:statsd_client).expects(:count).with('processed', 1, tags: ['succeeded:false'])
      job.send(:statsd_client).expects(:increment).with('destroy.error', tags: ['exception:standard_error'])

      job.perform
    end
  end

  describe "tickets" do
    it "scrubs a soft deleted ticket" do
      Zendesk::Scrub.expects(:scrub_ticket_and_associations).with(@ticket)
      tickets = Array(@ticket.id)
      run_deletion_job(@account, tickets)
    end

    it "creates a permanent deletion audit" do
      assert_difference 'CIA::Event.count(:all)', 1 do
        tickets = Array(@ticket.id)
        run_deletion_job(@account, tickets)
        audit = CIA::Event.last
        assert audit, "No audit was created"
        assert_equal "permanently_destroy", audit.action
        assert_equal User.system, audit.actor
        refute audit.visible?, "System user audit should not be visible"
      end
    end

    describe 'monitoring performance' do
      let(:span) { stub(:span) }

      it 'sets APM facet tags' do
        span.expects(:set_tag).with(Datadog::Ext::Analytics::TAG_ENABLED, true)
        span.expects(:set_tag).with('zendesk.account_id', @ticket.account.id)
        span.expects(:set_tag).with('zendesk.account_subdomain', @ticket.account.subdomain)
        span.expects(:set_tag).with("zendesk.subpoena", false)

        span.expects(:set_tag).with('ticket_deletion_job.tickets_ids', [@ticket.id])

        ZendeskAPM.
          expects(:trace).
          with(
            'ticket_deletion_job.work',
            service: Zendesk::Maintenance::Jobs::TicketDeletionJob::APM_SERVICE_NAME
          ).
          yields(span)

        Zendesk::Maintenance::Jobs::TicketDeletionJob.any_instance.expects(:process_ticket_ids)

        tickets = Array(@ticket.id)
        run_deletion_job(@account, tickets)
      end
    end
  end

  describe "soft-archived tickets" do
    before do
      # Archive, but leave the "tickets" record in place with status_id = 7
      @ticket.archive!
    end

    it "Scrubs the archived record in Riak, not the soft-archived tickets record" do
      run_deletion_job(@account, [@ticket.id])
      TicketArchiveStub.unscoped.find(@ticket.id).subject.must_equal 'SCRUBBED'
    end
  end

  describe "archived tickets" do
    before do
      archive_and_delete(@ticket)
    end

    it "deletes from riak" do
      tickets = Array(@ticket.id)
      Zendesk::Scrub.expects(:scrub_ticket_and_associations).with(@ticket)
      run_deletion_job(@account, tickets)
    end
  end

  describe "attachments" do
    describe_with_and_without_arturo_enabled :email_gdpr_remote_files do
      before do
        @attachment = create_attachment("#{Rails.root}/test/files/normal_1.jpg", @ticket.requester)
        @attachment.update_attribute :ticket_id, @ticket.id
        @attachment.update_column :source_type, "Comment"
        @attachment.update_column :source_id, @ticket.comments.last.id
        @tickets = Array(@ticket.id)
      end

      it "delete attachments" do
        run_deletion_job(@account, @tickets)
        @attachment.reload
        assert_empty @attachment.stores
      end
    end
  end

  describe "raw emails" do
    describe_with_and_without_arturo_enabled :email_gdpr_remote_files do
      before do
        @account = accounts(:minimum)
        @user = users(:minimum_end_user)
        @ticket = @account.tickets.new(requester: @user, description: "I'm going to be scrubbed soon")
        @ticket.will_be_saved_by(@user)
        @ticket.audit.metadata[:system][:raw_email_identifier] = '1/msg.eml'
        @ticket.audit.via_id = ViaType.MAIL
        @ticket.audit.save!
        @ticket.save!

        @ticket.will_be_saved_by(@user)
        @ticket.soft_delete!
      end

      it "deletes raw e-mails" do
        tickets = Array(@ticket.id)
        RemoteFiles::File.any_instance.expects(:delete_now!).twice
        run_deletion_job(@account, tickets)
      end
    end
  end

  describe "voice recordings" do
    before do
      @account = accounts(:minimum)
      @user = users(:minimum_end_user)
      @ticket = @account.tickets.new(requester: @user, description: "I'm going to be scrubbed soon")
      @ticket.will_be_saved_by(@user)
      @ticket.save!

      @ticket.will_be_saved_by(@user)
      @ticket.soft_delete!
    end

    describe_with_arturo_enabled :lotus_feature_voice_soft_delete_recordings do
      it "sends a request to Talk to soft delete recordings when a ticket has voice comments" do
        FactoryBot.create(:voice_comment, account: @account, ticket: @ticket)
        Zendesk::Voice::InternalApiClient.any_instance.expects(:voice_soft_delete_recordings).once.with(@ticket.nice_id)

        run_deletion_job(@account, [@ticket.id])
      end

      it "does not send a request to Talk when a ticket has no voice comments" do
        Zendesk::Voice::InternalApiClient.any_instance.expects(:voice_soft_delete_recordings).never

        run_deletion_job(@account, [@ticket.id])
      end
    end

    describe_with_arturo_disabled :lotus_feature_voice_soft_delete_recordings do
      it "does not send a request to Talk when the arturo is disabled" do
        FactoryBot.create(:voice_comment, account: @account, ticket: @ticket)
        Zendesk::Voice::InternalApiClient.any_instance.expects(:voice_soft_delete_recordings).never

        run_deletion_job(@account, [@ticket.id])
      end
    end
  end
end
