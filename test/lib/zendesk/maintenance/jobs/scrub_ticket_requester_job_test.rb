require_relative "../../../../support/test_helper"
require_relative '../../../../support/ticket_test_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob do
  include TicketTestHelper

  fixtures :accounts, :users, :ticket_fields, :sequences
  let(:user) { users(:minimum_agent) }
  let(:account) { accounts(:minimum) }

  let!(:ticket_a) do
    # Ticket closed 5 hours ago
    ticket_a = ticket_create(user)
    ticket_a.status_id = StatusType.CLOSED
    ticket_a.will_be_saved_by(user)
    ticket_a.save!
    ticket_a.update_column :status_updated_at, Time.now - 5.hours
    ticket_a.reload
  end

  let!(:ticket_b) do
    # Ticket closed 15 hours ago
    ticket_b = ticket_create(user)
    ticket_b.status_id = StatusType.CLOSED
    ticket_b.will_be_saved_by(user)
    ticket_b.save!
    ticket_b.update_column :status_updated_at, Time.now - 15.hours
    ticket_b.reload
  end

  let!(:ticket_c) do
    # Ticket updated 15 hours ago
    ticket_c = ticket_create(user)
    ticket_c.status_id = StatusType.OPEN
    ticket_c.will_be_saved_by(user)
    ticket_c.save!
    ticket_c.update_column :status_updated_at, Time.now - 15.hours
    ticket_c.reload
  end

  let!(:ticket_d) do
    # Ticket closed 45 hours ago and scrubbed
    ticket_d = ticket_create(user)
    ticket_d.status_id = StatusType.CLOSED
    ticket_d.will_be_saved_by(user)
    ticket_d.save!
    ticket_d.update_column :status_updated_at, Time.now - 45.hours
    ticket_d.reload
  end

  describe_with_arturo_enabled :ticket_user_scrubbing_after_close do
    before do
      feature = Arturo::Feature.find_or_create_by(symbol: 'ticket_metadata_deletion_at_close')
      feature.external_beta_subdomains = [account.subdomain]
      feature.save!
    end

    describe 'account has scrub_user_data_from_closed_ticket enabled' do
      before do
        Arturo.enable_feature!(:ticket_user_scrubbing_at_close)
        account.settings.delete_ticket_metadata_pii = true
        account.settings.save!
        account.save!
      end

      it "calls #work_on_account once" do
        Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.expects(:work_on_account).once
        Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.work
      end

      describe_with_arturo_enabled :ticket_user_scrubbing_killswitch do
        it "doesn't scrub the tickets inside the ticket range" do
          CIA.audit actor: User.system do
            CIA.record(:scrub_pii_ticket_close, ticket_d)
          end

          Zendesk::TicketAnonymizer::TicketScrubber.any_instance.expects(:scrub_ticket).never
          Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.work
        end

        describe "scrubbing job" do
          let(:anonymous_user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: account) }
          let(:mock_scrubber) { stub('ticket_scrubber', scrub_ticket: true) }

          it "doesn't scrub closed tickets inside the time range" do
            Zendesk::TicketAnonymizer::TicketScrubber.expects(:new).with(ticket: ticket_a).returns(mock_scrubber).never
            Zendesk::TicketAnonymizer::TicketScrubber.expects(:new).with(ticket: ticket_b).returns(mock_scrubber).never
            Zendesk::TicketAnonymizer::TicketScrubber.expects(:new).with(ticket: ticket_c).returns(mock_scrubber).never
            Zendesk::TicketAnonymizer::TicketScrubber.expects(:new).with(ticket: ticket_d).returns(mock_scrubber).never

            Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.work
          end
        end

        describe "register metrics in datadog" do
          it "doesn't send metrics to datadog if the process failed" do
            CIA.audit actor: User.system do
              CIA.record(:scrub_pii_ticket_close, ticket_d)
            end

            Zendesk::TicketAnonymizer::TicketScrubber.statsd_client.expects(:increment).never
            Zendesk::TicketAnonymizer::TicketScrubber.statsd_client.expects(:event).never
            Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:scrub_ticket).raises(ActiveRecord::RecordInvalid, 'fail').never

            Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.work
          end
        end

        it "doesn't send time metrics to datadog" do
          Zendesk::TicketAnonymizer::TicketScrubber.statsd_client.expects(:time).never

          Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.work
        end
      end

      describe_with_arturo_disabled :ticket_user_scrubbing_killswitch do
        it "scrubs the tickets inside the time range not scrubbed yet" do
          CIA.audit actor: User.system do
            CIA.record(:scrub_pii_ticket_close, ticket_d)
          end
          # There is only one ticket to scrub ticket_d was scrubbed
          Zendesk::TicketAnonymizer::TicketScrubber.any_instance.expects(:scrub_ticket).once
          Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.work
        end

        describe "scrubbing job" do
          let(:anonymous_user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create(account: account) }
          let(:mock_scrubber) { stub('ticket_scrubber', scrub_ticket: true) }

          it "scrubs closed tickets inside the time range" do
            Zendesk::TicketAnonymizer::TicketScrubber.expects(:new).with(ticket: ticket_a).returns(mock_scrubber).never
            Zendesk::TicketAnonymizer::TicketScrubber.expects(:new).with(ticket: ticket_b).returns(mock_scrubber).once
            Zendesk::TicketAnonymizer::TicketScrubber.expects(:new).with(ticket: ticket_c).returns(mock_scrubber).never
            Zendesk::TicketAnonymizer::TicketScrubber.expects(:new).with(ticket: ticket_d).returns(mock_scrubber).once

            Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.work
          end
        end

        describe "register metrics in datadog" do
          it "sends metrics to datadog if the process failed" do
            CIA.audit actor: User.system do
              CIA.record(:scrub_pii_ticket_close, ticket_d)
            end

            Zendesk::TicketAnonymizer::TicketScrubber.statsd_client.expects(:increment).with do |metric_name, _tags|
              metric_name == "failed_scrubbing_process"
            end.once

            Zendesk::TicketAnonymizer::TicketScrubber.statsd_client.expects(:event).with do |event_name, _message, _alert_type|
              event_name == "ticket scrubbing job failed"
            end.once

            Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:scrub_ticket).raises(ActiveRecord::RecordInvalid, 'fail').once
            Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.work
          end
        end

        it "sends time metrics to datadog" do
          Zendesk::TicketAnonymizer::TicketScrubber.statsd_client.expects(:time).with do |metric_name, _time|
            metric_name == "scrubbing_job_time"
          end.once

          Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.work
        end
      end
    end

    describe "account doesn't have delete_ticket_metadata_pii enabled" do
      before do
        account.settings.delete_ticket_metadata_pii = false
        account.settings.save!
        account.save!
      end

      it "calls #work_on_account once" do
        Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.expects(:work_on_account).once
        Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.work
      end

      it "doesn't run the scrubbing procces" do
        Zendesk::TicketAnonymizer::TicketScrubber.any_instance.stubs(:scrub_ticket).never
        Zendesk::Maintenance::Jobs::ScrubTicketRequesterJob.work
      end
    end
  end
end
