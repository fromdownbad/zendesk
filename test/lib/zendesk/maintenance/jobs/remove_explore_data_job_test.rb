require_relative "../../../../support/job_helper"

SingleCov.covered!

describe 'RemoveExploreDataJob' do
  include DataDeletionJobSupport

  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:job) { create_job(account, Zendesk::Maintenance::Jobs::RemoveExploreDataJob) }

  it 'sends request to explore' do
    Zendesk::Explore::InternalApiClient.any_instance.expects(:account_deleted!).once
    job.work
  end
end
