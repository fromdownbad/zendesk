require_relative '../../../../support/job_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::RemoveReferenceParticipantDataJob do
  include DataDeletionJobSupport

  fixtures :accounts
  let(:account) { accounts(:minimum) }
  let(:job) { create_job(account, Zendesk::Maintenance::Jobs::RemoveReferenceParticipantDataJob) }

  def url(service, method_name)
    "http://pod-1.#{service}.service.consul:8080/z/#{method_name}?account_id=#{account.id}"
  end

  def stub_calls(service, method_name, returns = [body: { done: true }])
    returns.each do |h|
      h[:headers] = { 'Content-Type' => 'application/json' }
      h[:body] = h[:body].to_json
    end

    stub_request(:post, url(service, method_name)).to_return(returns)
  end

  describe '.job_name' do
    subject { Zendesk::Maintenance::Jobs::RemoveReferenceParticipantDataJob.job_name }

    it 'should return the formatted job name' do
      assert_equal 'remove_reference_participant_data_job', subject
    end
  end

  describe 'when :data_deletion_endpoints_ready arturo is enabled' do
    describe 'when Rails.env is staging' do
      before do
        Arturo.enable_feature!(:data_deletion_endpoints_ready)
        Rails.env.stubs(:staging?).returns(true)
      end

      describe '#erase_for_moved' do
        let(:method_name) { :erase_for_moved }
        subject { job.send(method_name) }

        it 'should run job after crosspod moves' do
          job.stubs(:pod_local_move?).returns(false)
          stub_calls(job.class.service_name, method_name)
          subject
          assert_requested :post, url(job.class.service_name, method_name), times: 1
        end
      end

      describe '#erase_for_canceled' do
        let(:method_name) { :erase_for_canceled }
        subject { job.send(method_name) }

        it 'should run job after crosspod moves' do
          job.stubs(:pod_local_move?).returns(false)
          stub_calls(job.class.service_name, method_name)
          subject
          assert_requested :post, url(job.class.service_name, method_name), times: 1
        end
      end
    end

    describe 'when Rails.env is production' do
      before do
        Rails.env.stubs(:production?).returns(true)
      end

      describe '#erase_for_moved' do
        let(:method_name) { :erase_for_moved }
        subject { job.send(method_name) }

        it 'should skip job' do
          job.expects(:make_request).never
          subject
          assert_not_requested :post, url(job.class.service_name, method_name)
        end
      end

      describe '#erase_for_canceled' do
        let(:method_name) { :erase_for_canceled }
        subject { job.send(method_name) }

        it 'should skip job' do
          job.expects(:make_request).never
          subject
          assert_not_requested :post, url(job.class.service_name, method_name)
        end
      end
    end
  end

  describe 'when :data_deletion_endpoints_ready arturo is disabled' do
    before do
      Arturo.disable_feature!(:data_deletion_endpoints_ready)
    end

    describe '#erase_for_moved' do
      let(:method_name) { :erase_for_moved }
      subject { job.send(method_name) }

      it 'should skip job' do
        job.expects(:make_request).never
        subject
        assert_not_requested :post, url(job.class.service_name, method_name)
      end
    end

    describe '#erase_for_canceled' do
      let(:method_name) { :erase_for_canceled }
      subject { job.send(method_name) }

      it 'should skip job' do
        job.expects(:make_request).never
        subject
        assert_not_requested :post, url(job.class.service_name, method_name)
      end
    end
  end
end
