require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'TwoFactorJob' do
  fixtures :accounts

  describe 'two_factor_all_agents' do
    let(:job) { Zendesk::Maintenance::Jobs::TwoFactorJob }

    describe 'without chat phase 4 multiproduct account' do
      let(:account) { accounts(:minimum) }
      before { assert_nil account.settings.two_factor_all_agents }

      it 'is set to false if there is an agent who has not confirmed two factor' do
        User.any_instance.stubs(:otp_configured?).returns(false)

        job.work
        refute_nil account.settings.reload.two_factor_all_agents
        refute account.settings.two_factor_all_agents
      end

      it 'updates the setting updated_at timestamp if nothing changed' do
        account.settings.two_factor_all_agents = false
        account.settings.save!

        setting = account.settings.find_by_name('two_factor_all_agents')
        before_date = setting.updated_at
        Timecop.travel(Time.now + 5.seconds)

        User.any_instance.stubs(:otp_configured?).returns(false)

        job.work
        assert setting.reload.updated_at > before_date
      end

      it 'is set to true if current setting is false and all agents have configured two factor' do
        account.settings.two_factor_all_agents = false
        account.settings.save!

        User.any_instance.stubs(:otp_configured?).returns(true)

        job.work
        assert account.settings.reload.two_factor_all_agents?
      end

      it 'is set to false if current setting is true and no agents have configured two factor' do
        account.settings.two_factor_all_agents = true
        account.settings.save!

        User.any_instance.stubs(:otp_configured?).returns(false)

        job.work
        refute account.settings.reload.two_factor_all_agents?
      end
    end

    describe 'chat phase 4 multiproduct account' do
      let(:account) { accounts(:multiproduct) }
      let(:staff) { users(:multiproduct_support_agent) }

      it 'is set to false if there is an agent who has not configured two factor' do
        staff.stubs(:otp_configured?).returns(false)

        job.work
        refute account.reload.settings.two_factor_all_agents
      end

      it 'is set to true if all staff have configured two factor' do
        User.any_instance.stubs(:otp_configured?).returns(true)

        job.work
        assert account.reload.settings.two_factor_all_agents
      end
    end
  end
end
