require_relative "../../../../support/job_helper"

SingleCov.covered! uncovered: 18

describe Zendesk::Maintenance::Jobs::DailyTicketCloseOnShardJob do
  fixtures :accounts

  let(:job) { Zendesk::Maintenance::Jobs::DailyTicketCloseOnShardJob }

  describe 'scheduling' do
    it "runs once a day at 8 AM UTC" do
      evaluated = ERB.new(File.read(Rails.root + 'config/resque-schedule.yml')).result
      resque_schedule = YAML.load(evaluated)
      assert_equal "0 8 * * *", resque_schedule[job.name]["cron"]
      assert_equal "low", resque_schedule[job.name]["queue"]
    end
  end

  describe '.fetch_accounts' do
    describe 'shell accounts' do
      let(:shell_account) { accounts(:shell_account_without_support) }

      it 'excludes shell accounts without a Support product' do
        assert shell_account.is_active?
        assert_equal Zendesk::Accounts::Locking::LockState::OPEN, shell_account.lock_state
        assert_includes Account.all, shell_account

        num_shell_accounts = 0
        job.fetch_accounts(shell_account.shard_id) do |account|
          num_shell_accounts += 1 if account.id == shell_account.id
        end

        assert_equal 0, num_shell_accounts
      end
    end

    describe 'non-serviceable accounts' do
      let(:non_serviceable_account) { accounts(:minimum) }
      before { non_serviceable_account.update is_serviceable: false }

      it 'excludes non-serviceable accounts' do
        matched = 0
        job.fetch_accounts(non_serviceable_account.shard_id) do |account|
          matched += 1 if account.id == non_serviceable_account.id
        end
        assert_equal 0, matched
      end
    end
  end
end
