require_relative "../../../../support/job_helper"

SingleCov.covered!

describe 'DailyReportJob' do
  describe 'One report fails' do
    before do
      reports = Array.new(Zendesk::Maintenance::Jobs::DailyReportJob::REPORTS)
      reports.shift.expects(:execute).raises(StandardError, "Duude!")

      reports.each do |report|
        report.expects(:execute)
      end

      Zendesk::Maintenance::Jobs::DailyReportJob.any_instance.expects(:record_recoverable_failure)
    end

    it 'records the error and keep going' do
      Zendesk::Maintenance::Jobs::DailyReportJob.execute
    end
  end
end
