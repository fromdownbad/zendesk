require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'BacklogJob' do
  fixtures :accounts

  it "overwrites work_on_account method" do
    RollupBacklogJob.expects(:enqueue).at_least_once
    Zendesk::Maintenance::Jobs::BacklogJob.work
  end

  describe "BackLogJob for trial account" do
    before do
      @account = accounts(:trial)
    end

    it "does not run when account is expired" do
      @account.is_serviceable = false
      @account.save!

      RollupBacklogJob.expects(:enqueue).with(@account).never
      Zendesk::Maintenance::Jobs::BacklogJob.work_on_account(@account)
    end
  end
end
