require_relative "../../../../support/job_helper"

SingleCov.covered!

describe 'PerShardJob' do
  PerShardJob = Zendesk::Maintenance::Jobs::PerShardJob
  class SampleJob < PerShardJob
  end
  describe "#work" do
    it "enqueues jobs for each shard" do
      ActiveRecord::Base.stubs(:shard_names).returns([1, 2])
      ActiveRecord::Base.stubs(:switch_connection)
      SampleJob.expects(:enqueue).with('arg1', 'arg2', 'work_shard_id' => 1)
      SampleJob.expects(:enqueue).with('arg1', 'arg2', 'work_shard_id' => 2)
      SampleJob.work('arg1', 'arg2')
    end

    it "works on the shard if enqueued with shard_id" do
      SampleJob.expects(:work_on_shard).with(1, 'arg1', 'arg2')
      SampleJob.work('arg1', 'arg2', 'work_shard_id' => 1)
    end
  end
end
