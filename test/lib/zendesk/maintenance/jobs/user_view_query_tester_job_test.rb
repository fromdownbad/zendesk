require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Maintenance::Jobs::UserViewQueryTesterJob do
  fixtures :accounts, :account_settings, :brands, :recipient_addresses, :rules

  subject { Zendesk::Maintenance::Jobs::UserViewQueryTesterJob }

  let(:account) { accounts(:minimum) }
  let(:user_view) { account.user_views.first }

  describe ".work" do
    before do
      Arturo.enable_feature!(:user_views_query_tester)
      Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(true)
      account.settings.user_count = UserView::SAMPLING_THRESHOLD * 10
      account.save

      params = {
        id: user_view.id,
        user_view: {
          all: [{"field" => "created_at", "operator" => "greater_than", "value" => Time.parse("03/10/2013")}]
        }
      }
      initializer = Zendesk::Rules::UserViewInitializer.new(account, HashWithIndifferentAccess.new(params), :user_view)
      initializer.user_view.save!
    end

    it "sends a report e-mail" do
      @csv = nil
      MonitorMailer.expects(:deliver_user_view_query_test_report).with do |a, csv|
        @csv = csv
        a.id == account.id
      end

      subject.work

      assert_equal 1, subject.tester.results.length
      expected_csv = <<~CSV.strip
        Account,Shard,User View,Title,DB,USE INDEX: Select (s),USE INDEX: Count (s),Count,No USE INDEX: Select (s),No USE INDEX: Count (s),Count,USE INDEX First?,Time
        #{subject.tester.results.first.join(',')}
      CSV
      assert_equal expected_csv, @csv.strip
    end
  end

  describe ".args_to_log" do
    it "logs job name and time" do
      actual = subject.args_to_log
      assert_equal 'UserViewQueryTesterJob', actual[:job]
      assert actual[:time]
    end
  end
end
