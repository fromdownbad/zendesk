require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Maintenance::Util::SubdomainReleaser do
  fixtures :accounts, :routes

  let(:account) { accounts(:minimum) }
  let(:subject) { Zendesk::Maintenance::Util::SubdomainReleaser.new(account) }

  # ensure host-mapping is set for the account and routes; tests for nil will update to nil
  before do
    account.update_column(:deleted_at, Time.now)
    account.update_column(:host_mapping, "account-#{account.id}-host-mapping")
    account.routes.each { |r| r.update_column(:host_mapping, "route-#{r.id}-host-mapping") }
    reload_account
  end

  describe '#release_subdomain!' do
    let(:timestamp) { Time.now.to_i } # works because of lazy eval
    let(:appended) { "-#{timestamp}-deleted" }

    before do
      Timecop.freeze
    end

    it 'should not apply 2x' do
      release_subdomain!
      first_release_subdomain = account.subdomain
      first_release_timestamp = account.updated_at

      Timecop.travel Time.now + 1.hour do
        release_subdomain!
        assert_equal first_release_subdomain, account.subdomain
        assert_equal first_release_timestamp, account.updated_at
      end
    end

    it 'should handle nil account host-mapping' do
      account.update_column(:host_mapping, nil)
      reload_account

      release_subdomain!

      refute account.host_mapping
    end

    it 'should handle nil route host-mapping' do
      account.routes.first.update_column(:host_mapping, nil)
      reload_account

      release_subdomain!

      refute account.routes.first.host_mapping
    end

    it 'should rename account subdomain' do
      subdomain = account.subdomain

      release_subdomain!

      assert_equal release_name(subdomain), account.subdomain
      assert_equal timestamp, account.updated_at.to_i
    end

    it 'should rename account host-mapping' do
      host_mapping = account.host_mapping

      release_subdomain!

      assert_equal release_name(host_mapping), account.host_mapping
      assert_equal timestamp, account.updated_at.to_i
    end

    it 'should rename route subdomains' do
      route_subdomains = account.routes.map(&:subdomain)

      release_subdomain!

      account.routes.each_with_index do |r, n|
        assert_equal release_name(route_subdomains[n]), r.subdomain, "for route #{r.subdomain} (#{n})"
        assert_equal timestamp, r.updated_at.to_i, "for route #{r.subdomain} (#{n})"
      end
    end

    it 'should rename route host-mapping' do
      route_hostmappings = account.routes.map(&:host_mapping)

      release_subdomain!

      account.routes.each_with_index do |r, n|
        assert_equal release_name(route_hostmappings[n]), r.host_mapping, "for route #{r.subdomain} (#{n})"
        assert_equal timestamp, r.updated_at.to_i, "for route #{r.subdomain} (#{n})"
      end
    end

    it 'should rename route host-mapping when account.host_mapping is nil' do
      route_hostmappings = account.routes.map(&:host_mapping)
      account.update_column(:host_mapping, nil)

      release_subdomain!

      account.routes.each_with_index do |r, n|
        assert_equal release_name(route_hostmappings[n]), r.host_mapping, "for route #{r.subdomain} (#{n})"
        assert_equal timestamp, r.updated_at.to_i, "for route #{r.subdomain} (#{n})"
      end
    end

    it 'should rename long subdomains to stay within the DNS_CHARACTER_LIMIT' do
      account.update_columns(subdomain: longname(account.subdomain), host_mapping: longname(account.host_mapping))

      account.routes.each do |r|
        r.update_columns(subdomain: longname(r.subdomain), host_mapping: longname(r.host_mapping))
      end

      reload_account

      subdomain = account.subdomain
      assert_equal DNS_CHARACTER_LIMIT, subdomain.length, 'paranoia means never feeling lonely'
      host_mapping = account.host_mapping

      route_subdomains = account.routes.map(&:subdomain)
      route_hostmappings = account.routes.map(&:host_mapping)

      release_subdomain!

      assert_equal cut_release_name(subdomain), account.subdomain
      assert_equal cut_release_name(host_mapping), account.host_mapping

      account.routes.each_with_index do |r, n|
        assert_equal cut_release_name(route_subdomains[n]), r.subdomain, "for route #{r.subdomain} (#{n})"
        assert_equal cut_release_name(route_hostmappings[n]), r.host_mapping, "for route #{r.subdomain} (#{n})"
      end
    end
  end

  describe '#verify_subdomain_released!' do
    it 'will successfully verify all values are released' do
      release_subdomain!
      subject.verify_subdomain_released!
    end

    it 'will fail verify if subdomain not released' do
      release_subdomain!

      account.update_column(:subdomain, 'not-deleted')
      reload_account

      assert_raises { subject.verify_subdomain_released! }
    end

    it 'will fail verify if route subdomain not released' do
      release_subdomain!

      account.routes.first.update_column(:subdomain, 'not-deleted')
      reload_account

      assert_raises { subject.verify_subdomain_released! }
    end

    it 'will fail verify if account host-mapping not released' do
      release_subdomain!

      account.update_column(:host_mapping, 'not-deleted')
      reload_account

      assert_raises { subject.verify_subdomain_released! }
    end

    it 'will fail verify if route host-mapping not released' do
      release_subdomain!

      account.routes.first.update_column(:host_mapping, 'not-deleted')
      reload_account

      assert_raises { subject.verify_subdomain_released! }
    end
  end

  def release_subdomain!
    subject.release_subdomain!
    reload_account
  end

  def longname(val)
    times = DNS_CHARACTER_LIMIT - val.length - 1
    val + (0..times).map { [*('a'..'z')].sample }.join
  end

  def release_name(val)
    "#{val}#{appended}"
  end

  def cut_release_name(val)
    raise ArgumentError, "val #{val} wont be cut" if val.length <= DNS_CHARACTER_LIMIT - appended.length
    cut = val[0, DNS_CHARACTER_LIMIT - appended.length]
    "#{cut}#{appended}"
  end

  def reload_account
    account.clear_kasket_indices
    account.reload
  end
end
