require_relative "../../../../support/test_helper"

SingleCov.covered!

describe 'BatchClose' do
  fixtures :accounts, :tickets, :events

  def count_eligible(account, since)
    account.tickets.where('solved_at < ? AND status_id = ?', since.days.ago, StatusType.SOLVED).count(:all)
  end

  describe "The batch close util" do
    before do
      @account = accounts(:minimum)
      Ticket.where("account_id = ? AND status_id < ?", @account.id, StatusType.CLOSED).
        update_all(status_id: StatusType.SOLVED, updated_at: 10.days.ago, solved_at: 10.days.ago)

      assert_equal 0, count_eligible(@account, 12)
      assert count_eligible(@account, 5) > 0
    end

    let(:statsd_client_stub) { stub_for_statsd }
    before do
      Zendesk::Maintenance::Util::BatchClose.send(:statsd_client) # Satisfy SingleCov before stubbing
      Zendesk::Maintenance::Util::BatchClose.stubs(:statsd_client).returns(statsd_client_stub)
    end

    describe "when closing for a specific account" do
      it "batch closes all tickets for the given account" do
        assert_difference "@account.tickets.where(status_id: StatusType.CLOSED).count(:all)", count_eligible(@account, 5) do
          Zendesk::Maintenance::Util::BatchClose.close_for_account!(@account, solved_before: 5.days.ago)
        end
        assert_equal 0, @account.tickets.where('solved_at < ? AND status_id = ?', 5.days.ago, StatusType.SOLVED).count(:all)
      end

      it "adheres to :max_records" do
        eligible    = count_eligible(@account, 5)
        max_records = 2
        assert_difference "@account.tickets.where(status_id: StatusType.CLOSED).count(:all)", max_records do
          Zendesk::Maintenance::Util::BatchClose.close_for_account!(@account, solved_before: 5.days.ago, max_records: max_records)
        end
        assert_equal eligible - max_records, @account.tickets.where('solved_at < ? AND status_id = ?', 5.days.ago, StatusType.SOLVED).count(:all)
      end

      describe 'deleting ticket audit metadata' do
        describe 'when ticket save fails' do
          it "datadogs some stuff if ticket save gets wack" do
            Ticket.any_instance.stubs(:save).returns(false)
            statsd_client_stub.expects(:increment).with('brute_close_ticket', tags: ["success:false"]).times(5)

            Zendesk::Maintenance::Util::BatchClose.close_for_account!(@account, solved_before: 5.days.ago)
            # Make sure we did not all the metadata in the audits, since save failed we won't have deleted the data
            solved_tickets = @account.tickets.where('solved_at < ? AND status_id = ?', 5.days.ago, StatusType.SOLVED)
            solved_tickets.each do |ticket|
              ticket.audits.each do |a|
                next if a.value_previous.nil?
                JSON.parse(a.value_previous).each_value { |v| refute v.nil? }
              end
            end
          end

          it "logs something useful if ticket save gets wack" do
            err_ticket = tickets(:with_groups_1)
            Ticket.any_instance.stubs(:save).returns(false)
            Ticket.any_instance.stubs(:errors).returns(stub(full_messages: ["Needs more wombats."]))
            Zendesk::Maintenance::Util::BatchClose.stubs(:log)
            Rails.logger.expects(:info).with("DailyTicketCloseOnShardJob: Failed to close ticket #{err_ticket.id} for account #{err_ticket.account_id} with errors Needs more wombats. ")
            Zendesk::Maintenance::Util::BatchClose.send(:brute_close_ticket, err_ticket)
          end

          it "does not increment sucess record_count if ticket close fails" do
            Ticket.any_instance.stubs(:save).returns(false)
            statsd_client_stub.expects(:increment).with('brute_close_ticket', tags: ["success:false"]).times(5)

            success_count = Zendesk::Maintenance::Util::BatchClose.close_for_account!(@account, solved_before: 5.days.ago)
            assert_equal 0, success_count
          end
        end
      end
    end
  end
end
