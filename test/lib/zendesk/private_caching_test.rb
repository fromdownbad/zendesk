require_relative "../../support/test_helper"

SingleCov.covered!

class PrivateCachingController < ActionController::Base
  include Zendesk::PrivateCaching

  only_use_private_caches :index

  def index
    render plain: "X"
  end

  def show
    response.headers['Cache-Control'] = 'private, max-age=0, must-revalidate'
    render plain: "X"
  end
end

class PrivateCachingTest < ActionController::TestCase
  tests PrivateCachingController
  use_test_routes

  describe "Private caching" do
    it "sets the no-cache directive when responding to specified actions" do
      get :index
      assert_response :success
      assert_match /no-cache/, @response.headers['Cache-Control']
    end

    it "does not set the no-cache directive when not responding to unspecified actions" do
      get :show
      assert_response :success
      assert_equal 'private, max-age=0, must-revalidate', @response.headers['Cache-Control']
    end
  end
end
