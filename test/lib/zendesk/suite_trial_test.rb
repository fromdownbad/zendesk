require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::SuiteTrial do
  fixtures :accounts

  let(:account) { accounts(:trial) }
  let(:suite_trial) { Zendesk::SuiteTrial.new(account) }
  let(:account_client) { stub }
  let(:product_updated_at) { DateTime.new(2018, 1, 1) }
  let(:guide_product) do
    Zendesk::Accounts::Product.new(
      'name' => 'guide',
      'state' => 'trial',
      'plan_settings' => {
        'suite' => false,
        'plan_type' => 2
      },
      'updated_at' => product_updated_at
    )
  end

  before do
    Zendesk::Accounts::Client.stubs(:new).with(account.id, anything, anything).returns(account_client)
    account.settings.stubs(:save!)
    account_client.stubs(:create_product!)
    Timecop.freeze(2018, 1, 1, 9, 0, 0)
  end

  describe '#activate' do
    describe 'owner entitlements' do
      let(:staff_client) { stub }

      before do
        account_client.stubs(:products).returns([])
        Zendesk::StaffClient.stubs(:new).with(account).returns(staff_client)
      end

      describe_with_arturo_enabled :ocp_support_shell_account_creation do
        it 'gives owner Chat entitlement' do
          staff_client.expects(:update_full_entitlements!).with(
            account.owner_id,
            'chat' => { 'name' => 'admin' }
          ).once

          suite_trial.activate
        end
      end

      describe_with_arturo_disabled :ocp_support_shell_account_creation do
        it 'does not give owner Chat entitlement' do
          staff_client.expects(:update_full_entitlements!).never
          suite_trial.activate
        end
      end

      it 'activates support agent workspace flags' do
        account.expects(:activate_agent_workspace_for_trial)
        suite_trial.activate
      end

      it 'syncs Guide entitlement for account' do
        Omnichannel::GuideAccountSyncJob.expects(:enqueue).with(account_id: account.id)
        suite_trial.activate
      end
    end

    describe 'without any product in Account Service' do
      before do
        account_client.stubs(:products).returns([])
      end

      it 'creates a Support trial with correct params' do
        account_client.expects(:create_product!).with(
          'support',
          {
            product: {
              state: :trial,
              trial_expires_at: account.subscription.trial_expires_on.to_datetime.iso8601,
              plan_settings: {
                plan_type: account.subscription.plan_type,
                max_agents: account.subscription.max_agents,
                light_agents: account.subscription.has_light_agents?,
                insights: :good_data,
                suite: true,
                agent_workspace: true
              }
            }
          },
          context: "suite_trial_create"
        ).once

        suite_trial.activate
      end

      it 'creates a Guide trial with correct params' do
        account_client.expects(:create_product!).with(
          'guide',
          {
            product: {
              state: :trial,
              trial_expires_at: '2018-01-30T23:59:59+00:00',
              plan_settings: {
                plan_type: 2,
                suite: true
              }
            }
          },
          context: "suite_trial_create"
        ).once

        suite_trial.activate
      end

      it 'creates a Chat trial with correct params' do
        account_client.expects(:create_product!).with(
          'chat',
          {
            product: {
              state: :trial,
              trial_expires_at: '2018-01-31T09:00:00+00:00',
              plan_settings: {
                plan_type: 0,
                suite: true,
                phase: 3,
                max_agents: account.subscription.max_agents,
                agent_workspace: true
              }
            }
          },
          context: "suite_trial_create"
        ).once

        suite_trial.activate
      end

      it 'creates a Talk trial with correct params' do
        account_client.expects(:create_product!).with(
          'talk',
          {
            product: {
              state: :not_started,
              plan_settings: {
                plan_type: 7,
                suite: true
              }
            }
          },
          context: "suite_trial_create"
        ).once

        suite_trial.activate
      end

      it 'creates a Social Messaging trial with correct params' do
        account_client.expects(:create_product!).with(
          'social_messaging',
          {
            product: {
              state: :trial,
              trial_expires_at: account.subscription.trial_expires_on.to_datetime.iso8601,
              plan_settings: {
                whatsapp_numbers_purchased: 0,
                suite: true
              }
            }
          },
          context: "suite_trial_create"
        ).once

        suite_trial.activate
      end

      it "logs a metric when suite trial is activated" do
        suite_trial.expects(:instrument)
        suite_trial.activate
      end
    end

    describe 'when the product already exists' do
      before do
        account_client.stubs(:products).returns([guide_product])
      end

      it 'updates the product, setting suite:true' do
        account_client.expects(:update_product!).with(
          'guide',
          {
            product: {
              plan_settings: {
                suite: true
              }
            }
          },
          if_unmodified_since: product_updated_at,
          context: "suite_trial_update"
        ).once

        suite_trial.activate
      end
    end

    describe 'when product creation fails with AccountNotTrialEligible' do
      let(:response) do
        stub(body: {
          'errors' => [
            {
              'detail' => 'Failed to Provision chat for account - AccountNotTrialEligible'
            }
          ]
        })
      end
      let(:exception) do
        Kragle::UnprocessableEntity.new(
          '422 Unprocessable Entity',
          response
        )
      end

      before do
        account_client.stubs(:products).returns([])
        account_client.stubs(:create_product!).with('chat', anything, context: "suite_trial_create").raises(exception)
      end

      it 'ignores and continues' do
        suite_trial.activate
      end
    end

    describe 'when product creation fails with another exception' do
      let(:response) do
        stub(body: {
          'errors' => [
            {
              'detail' => 'something else happened'
            }
          ]
        })
      end
      let(:exception) do
        Kragle::UnprocessableEntity.new(
          '422 Unprocessable Entity',
          response
        )
      end

      before do
        account_client.stubs(:products).returns([])
        account_client.stubs(:create_product!).with('chat', anything, context: "suite_trial_create").raises(exception)
      end

      it 'raises' do
        assert_raises Kragle::UnprocessableEntity do
          suite_trial.activate
        end
      end
    end

    describe 'when there is a product creation/update at the same time' do
      before do
        account_client.stubs(:products).returns([]).then.
          returns([guide_product])
        account_client.stubs(:create_product!).with('guide', anything, context: "suite_trial_create").raises(Kragle::Conflict)
      end

      it 'recovers and tries again' do
        account_client.expects(:update_product!).with(
          'guide',
          {
            product: {
              plan_settings: {
                suite: true
              }
            }
          },
          if_unmodified_since: product_updated_at,
          context: "suite_trial_update"
        ).once

        suite_trial.activate
      end
    end
  end
end
