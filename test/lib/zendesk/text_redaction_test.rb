require_relative "../../support/test_helper"

SingleCov.covered!

describe Zendesk::TextRedaction do
  let(:text) { "the" }
  let(:body) { "The city of Athens is the capital of Greece" }

  describe "#redact" do
    it "replaces all occurrences of :text in :body without modifying :body" do
      assert_equal "▇▇▇ city of A▇▇▇ns is ▇▇▇ capital of Greece", Zendesk::TextRedaction.redact(body, text)
      assert_equal "The city of Athens is the capital of Greece", body
    end

    describe "with invalid UTF-8" do
      let(:body) { "The city of \xC5 is the capital of Greece" }

      it "replaces all occurrences of :text in :body without modifying :body" do
        assert_equal "▇▇▇ city of ▇ is ▇▇▇ capital of Greece", Zendesk::TextRedaction.redact(body, text)
        assert_equal "The city of \xC5 is the capital of Greece", body
      end
    end
  end

  describe "#redact!" do
    it "replaces all occurrences of :text in :body and modifying :body" do
      assert_equal "▇▇▇ city of A▇▇▇ns is ▇▇▇ capital of Greece", Zendesk::TextRedaction.redact!(body, text)
      assert_equal "▇▇▇ city of A▇▇▇ns is ▇▇▇ capital of Greece", body
    end

    describe "with invalid UTF-8" do
      let(:body) { "The city of \xC5 is the capital of Greece" }

      it "replaces all occurrences of :text in :body and modifying :body" do
        assert_equal "▇▇▇ city of ▇ is ▇▇▇ capital of Greece", Zendesk::TextRedaction.redact!(body, text)
        assert_equal "▇▇▇ city of ▇ is ▇▇▇ capital of Greece", body
      end
    end
  end

  describe "#redact_part_or_all!" do
    it "redacts full text iff text is nil and redact_full_comment is set to true" do
      assert_equal "▇▇▇▇▇", Zendesk::TextRedaction.redact_part_or_all!(body, nil, true)
    end

    it "does not full redact if text is not nil or redact_full_comment is false" do
      assert_equal "▇▇▇ city of A▇▇▇ns is ▇▇▇ capital of Greece", Zendesk::TextRedaction.redact_part_or_all!(body, text, true)
    end
  end
end
