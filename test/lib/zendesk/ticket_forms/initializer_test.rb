require_relative "../../../support/test_helper"
require 'zendesk/ticket_forms/initializer'

SingleCov.covered!

describe Zendesk::TicketForms::Initializer do
  fixtures :accounts, :custom_field_options, :ticket_fields, :brands

  before do
    @account      = accounts(:minimum)
    @brand        = brands(:minimum)
    @default_form = FactoryBot.create(:ticket_form, account: @account)

    @custom_ticket_field  = ticket_fields(:field_text_custom)
    @custom_ticket_field2 = ticket_fields(:field_checkbox_custom)
    @custom_ticket_field3 = ticket_fields(:field_textarea_custom)
    @system_ticket_field  = ticket_fields(:minimum_field_subject)

    @same_name_field = FieldText.create!(account: @account, title: 'Product name', is_active: true)

    # Set correct positions on account system fields to make tests more realistic.
    # Positions are hardcoded on account creation see accounts/base.rb#add_custom_fields
    @subject_field = @account.field_subject
    @subject_field.update_attribute(:position, 1)

    @account.field_description.update_attribute(:position, 2)
    @account.field_description.update_attribute(:title, "Description")
    @account.field_status.update_attribute(:position, 3)

    @ticket_type_field = @account.field_ticket_type
    @ticket_type_field.update_attribute(:position, 4)

    @priority_field = @account.field_priority
    @priority_field.update_attribute(:position, 5)

    @account.field_group.update_attribute(:position, 6)
    @account.field_assignee.update_attribute(:position, 7)
  end

  describe "Standard ticket form initialization" do
    before do
      @new_form = setup_form
      @new_form.save!
    end

    it "returns a new ticket form" do
      assert_equal TicketForm, @new_form.class
    end

    it "sets name" do
      assert "Wombat", @new_form.name
    end

    it "sets display name" do
      assert_equal "Wombats are everywhere", @new_form.display_name
    end

    it "sets position" do
      assert_equal 2, @new_form.position
    end

    it "sets default" do
      assert_equal false, @new_form.default
    end

    it "sets end_user_visible" do
      assert(@new_form.end_user_visible)
    end

    it "sets active" do
      assert(@new_form.active)
    end

    it "sets in_all_brands" do
      assert(@new_form.in_all_brands)
    end

    it "sets in_all_organizations" do
      assert(@new_form.in_all_organizations)
    end

    it "sets account_id" do
      assert_equal @account.id, @new_form.account_id
    end
  end

  describe "#ticket_form" do
    it "always add required fields to the initialized ticket form" do
      @new_form = setup_form
      @new_form.save!

      assert_equal @new_form.ticket_fields.count(:all), ::TicketField::SYSTEM_REQUIRED.count
    end

    it "adds posted fields to the initialized ticket form in order given" do
      @new_form = setup_form(ticket_field_ids: [@custom_ticket_field.id, @custom_ticket_field2.id])
      @new_form.save!

      new_form_field1 = @new_form.ticket_form_fields.find_by_ticket_field_id(@custom_ticket_field.id)
      new_form_field2 = @new_form.ticket_form_fields.find_by_ticket_field_id(@custom_ticket_field2.id)

      assert_equal ::TicketField::SYSTEM_REQUIRED.count + 2, @new_form.ticket_fields.count(:all)
      assert_equal new_form_field1.position + 1, new_form_field2.position

      ## Note: Ticket fields of a ticket form are returned ordered via their position in the form
      assert_equal @custom_ticket_field2.id, @new_form.ticket_fields.last.id
    end

    it "does not add posted field to initialized ticket form again if the field is a REQUIRED_FIELD" do
      @new_form = setup_form(ticket_field_ids: [@system_ticket_field.id])
      @new_form.save!

      assert_equal @new_form.ticket_fields.count(:all), ::TicketField::SYSTEM_REQUIRED.count
    end

    it "allows custom ordering of all required system fields and optional system fields" do
      @new_form = setup_form(ticket_field_ids: [@custom_ticket_field.id, @ticket_type_field.id, @priority_field.id, @subject_field.id, @account.field_assignee.id, @account.field_group.id, @account.field_description.id, @account.field_status.id])
      @new_form.save!

      form_fields = @new_form.ticket_form_fields.sort_by(&:position)
      ordered_fields = form_fields.map { |ff| TicketField.find(ff.ticket_field_id).type }
      assert_equal ["FieldText", "FieldTicketType", "FieldPriority", "FieldSubject", "FieldAssignee", "FieldGroup", "FieldDescription", "FieldStatus"], ordered_fields
    end

    it "adds any missing required system fields to the beginning of the list of ticket fields" do
      @new_form = setup_form(ticket_field_ids: [@custom_ticket_field.id, @ticket_type_field.id, @priority_field.id, @account.field_assignee.id, @account.field_group.id, @account.field_description.id])
      @new_form.save!

      form_fields = @new_form.ticket_form_fields.sort_by(&:position)
      ordered_fields = form_fields.map { |ff| TicketField.find(ff.ticket_field_id).type }
      assert_equal ["FieldSubject", "FieldStatus", "FieldText", "FieldTicketType", "FieldPriority", "FieldAssignee", "FieldGroup", "FieldDescription"], ordered_fields
    end

    it "allows adding two ticket fields with the same name" do
      FieldText.create!(account: @account, title: 'Product name', is_active: true)
      @new_form = setup_form(ticket_field_ids: [@custom_ticket_field.id, @same_name_field.id])
      @new_form.save!
      form_custom_fields = @new_form.ticket_fields - @account.ticket_fields.system_required

      assert_equal ::TicketField::SYSTEM_REQUIRED.count + 2, @new_form.ticket_fields.count(:all)
      assert_equal ["Product name", "Product name"], form_custom_fields.map(&:title)
    end

    describe "with conditions" do
      before do
        Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(true)
        @field_tagger_custom = ticket_fields(:field_tagger_custom)
        @field_tagger_custom_2 = FactoryBot.create(:field_tagger, account: @account)
        @cfo1 = custom_field_options(:field_tagger_custom_option_1)
      end

      it "adds no conditions when CTF is disabled" do
        Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(false)

        conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false
          ]
        ]

        @new_form = setup_form(
          ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
          agent_conditions: conditions,
          end_user_conditions: conditions
        )
        @new_form.save!

        assert_equal [], @new_form.agent_conditions
        assert_equal [], @new_form.end_user_conditions
      end

      it "adds agent conditions" do
        agent_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: true,
            required_on_statuses: { type: TicketFieldCondition::REQUIRED_ON_ALL_STATUSES }
          ]
        ]

        @new_form = setup_form(
          ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
          agent_conditions: agent_conditions
        )
        @new_form.save!

        assert_equal agent_conditions, @new_form.agent_conditions
      end

      it "adds end user conditions" do
        end_user_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false
          ]
        ]

        @new_form = setup_form(
          ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
          end_user_conditions: end_user_conditions
        )
        @new_form.save!

        assert_equal end_user_conditions, @new_form.end_user_conditions
      end

      it "adds agent conditions and end user conditions" do
        agent_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false,
            required_on_statuses: { type: TicketFieldCondition::REQUIRED_ON_NO_STATUSES }
          ]
        ]

        end_user_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false
          ]
        ]

        @new_form = setup_form(
          ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
          agent_conditions: agent_conditions,
          end_user_conditions: end_user_conditions
        )
        @new_form.save!

        assert_equal agent_conditions, @new_form.agent_conditions
        assert_equal end_user_conditions, @new_form.end_user_conditions
      end

      it "prevents adding end user conditions with bad values" do
        end_user_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.id, # cfo1.value would be valid
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false
          ]
        ]

        @new_form = setup_form(
          ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
          end_user_conditions: end_user_conditions
        )
        refute @new_form.save
        assert @new_form.ticket_field_conditions.first.errors.messages[:value]
      end

      it "prevents adding end user conditions with agent-only parent field" do
        @field_tagger_custom.is_visible_in_portal = false
        @field_tagger_custom.save!

        end_user_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false
          ]
        ]

        @new_form = setup_form(
          ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
          end_user_conditions: end_user_conditions
        )
        refute @new_form.save
        assert @new_form.ticket_field_conditions.first.errors.messages[:parent_field]
      end
    end

    it "add restricted brands to the initialized ticket form" do
      @new_form = setup_form(restricted_brand_ids: [@brand.id])
      @new_form.save!

      assert_equal [@brand], @new_form.restricted_brands
    end
  end

  describe "#updated_form" do
    before do
      @form = setup_form(ticket_field_ids: [@custom_ticket_field.id, @custom_ticket_field2.id])
      @form.save!
      @orig_ticket_fields_order = @form.ticket_fields
    end

    it "updates custom ticket fields with new position they are given in" do
      params = {id: @form.id, ticket_form: { ticket_field_ids: [@custom_ticket_field2.id, @custom_ticket_field.id] } }
      updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
      updated_form.save!

      ## Note: position of ticket_fields cannot be set explicity, position of the ticket field is determined
      ## by the order in which it is given
      updated_form_field1 = @form.ticket_form_fields.find_by_ticket_field_id(@custom_ticket_field.id)
      updated_form_field2 = @form.ticket_form_fields.find_by_ticket_field_id(@custom_ticket_field2.id)

      assert_not_equal @orig_ticket_fields_order, @form.ticket_fields.map(&:id)
      assert_equal (updated_form_field2.position + 1), updated_form_field1.position
    end

    it "updates properties of the ticket form itself" do
      params = { id: @form.id, ticket_form: { name: "Hello Wombat", default: true, ticket_field_ids: [@custom_ticket_field2.id, @custom_ticket_field.id] } }
      updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
      updated_form.save!

      assert_equal "Hello Wombat", updated_form.name
      assert(updated_form.default)
    end

    it "ONLYs update properties of the ticket form if ticket_field_ids param is not sent" do
      orig_ticket_field_count = @form.ticket_fields.count(:all)
      params = { id: @form.id, ticket_form: { name: "Hello Wombat", default: true }}
      updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
      updated_form.save!

      assert_equal "Hello Wombat", updated_form.name
      assert(updated_form.default)
      assert_equal orig_ticket_field_count, updated_form.ticket_fields.count(:all)
    end

    it "adds any new fields given to the ticket_form" do
      assert_difference("TicketFormField.count(:all)", 1) do
        params = { id: @form.id, ticket_form: { ticket_field_ids: [@custom_ticket_field.id, @custom_ticket_field2.id, @custom_ticket_field3.id] } }
        updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
        updated_form.save!
      end

      assert @form.ticket_fields.include?(@custom_ticket_field3)
      assert_equal @custom_ticket_field3, @form.ticket_fields.last
    end

    it "deletes any fields not in the given form that are not required fields" do
      assert_difference("TicketFormField.count(:all)", -1) do
        params = { id: @form.id, ticket_form: { ticket_field_ids: [@custom_ticket_field.id] } }
        updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
        updated_form.save!
      end

      refute @form.ticket_fields.include?(@custom_ticket_field2)
      assert_equal @custom_ticket_field, @form.ticket_fields.last
    end

    it "allows adding a ticket field with the same name as a current ticket field on the form" do
      params = { id: @form.id, ticket_form: { ticket_field_ids: [@custom_ticket_field.id, @custom_ticket_field2.id, @same_name_field.id] } }

      assert_difference("TicketFormField.count(:all)", 1) do
        @updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
        @updated_form.save!
      end

      form_custom_fields = @updated_form.ticket_fields - @updated_form.ticket_fields.system_required
      assert_equal ["Product name", "Premium", "Product name"], form_custom_fields.map(&:title)
    end

    describe "in account with multiple active brands" do
      before do
        Account.any_instance.stubs(:has_multiple_active_brands?).returns(true)
      end

      it "sets restricted_brand_ids" do
        params = { id: @form.id, ticket_form: {in_all_brands: false, restricted_brand_ids: [@brand.id]} }

        assert_difference("TicketFormBrandRestriction.count(:all)", 1) do
          @updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
          @updated_form.save!
        end

        assert_equal [brands(:minimum)], @form.restricted_brands

        params = { id: @form.id, ticket_form: {in_all_brands: true, restricted_brand_ids: nil} }

        assert_difference("TicketFormBrandRestriction.count(:all)", -1) do
          @updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
          @updated_form.save!
        end

        assert_equal [], @updated_form.restricted_brands
      end

      it "doesn't remove restricted brands if restricted_brand_ids is not in the params" do
        params = { id: @form.id, ticket_form: {in_all_brands: false, restricted_brand_ids: [@brand.id]} }

        assert_difference("TicketFormBrandRestriction.count(:all)", 1) do
          @updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
          @updated_form.save!
        end

        assert_equal [brands(:minimum)], @form.restricted_brands

        params = { id: @form.id, ticket_form: {in_all_brands: true} }

        assert_difference("TicketFormBrandRestriction.count(:all)", 0) do
          @updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
          @updated_form.save!
        end

        assert_equal [brands(:minimum)], @form.restricted_brands
      end
    end

    describe "Type and Priority fields" do
      before do
        params = { id: @form.id, ticket_form: { ticket_field_ids: [@ticket_type_field.id, @priority_field.id] } }
        updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
        updated_form.save!
      end

      it "is able to be deleted since they are system fields but not required fields" do
        assert_equal ::TicketField::SYSTEM_FIELDS.count, @form.ticket_fields.count(:all)
        assert @form.ticket_fields.include?(@ticket_type_field)
        assert @form.ticket_fields.include?(@priority_field)

        assert_difference("TicketFormField.count(:all)", -2) do
          params = { id: @form.id, ticket_form: { ticket_field_ids: [] } }
          updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
          updated_form.save!
        end

        refute @form.ticket_fields.include?(@ticket_type_field)
        refute @form.ticket_fields.include?(@priority_field)
      end

      it "is able to add type and priority fields in to a ticket form after they have been deleted" do
        params = { id: @form.id, ticket_form: { ticket_field_ids: [] } }
        updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
        updated_form.save!

        assert_difference("TicketFormField.count(:all)", 2) do
          params = { id: @form.id, ticket_form: { ticket_field_ids: [@ticket_type_field.id, @priority_field.id] } }
          updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
          updated_form.save!
        end

        assert @form.ticket_fields.include?(@ticket_type_field)
        assert @form.ticket_fields.include?(@priority_field)
      end
    end

    describe "Reordering system fields" do
      before do
        @part_params = { id: @form.id, ticket_form: { ticket_field_ids: [@priority_field.id, @ticket_type_field.id] } }
        @all_params = { id: @form.id, ticket_form: { ticket_field_ids: [@priority_field.id, @ticket_type_field.id, @subject_field.id, @account.field_description.id, @account.field_group.id, @account.field_assignee.id, @account.field_status.id] } }
      end

      describe "with all system fields passed in (including type and priority)" do
        before do
          updated_form = Zendesk::TicketForms::Initializer.new(@account, @all_params).updated_ticket_form
          updated_form.save!
        end
        it "will reorder all system fields in the order they were requested" do
          assert_equal @priority_field.id, @form.ticket_fields[0].id
          assert_equal @ticket_type_field.id, @form.ticket_fields[1].id
          assert_equal @subject_field.id, @form.ticket_fields[2].id
          assert_equal @account.field_description.id, @form.ticket_fields[3].id
          assert_equal @account.field_group.id, @form.ticket_fields[4].id
          assert_equal @account.field_assignee.id, @form.ticket_fields[5].id
          assert_equal @account.field_status.id, @form.ticket_fields[6].id
        end
      end

      describe "with all system fields passed in (without type and priority)" do
        before do
          params = { id: @form.id, ticket_form: { ticket_field_ids: [@subject_field.id, @account.field_description.id, @account.field_group.id, @account.field_assignee.id, @account.field_status.id] } }
          updated_form = Zendesk::TicketForms::Initializer.new(@account, params).updated_ticket_form
          updated_form.save!
        end
        it "will reorder all system fields in the order they were requested and remove type and priority" do
          assert_equal @subject_field.id, @form.ticket_fields[0].id
          assert_equal @account.field_description.id, @form.ticket_fields[1].id
          assert_equal @account.field_group.id, @form.ticket_fields[2].id
          assert_equal @account.field_assignee.id, @form.ticket_fields[3].id
          assert_equal @account.field_status.id, @form.ticket_fields[4].id
        end
      end
      describe "with a few system fields passed in" do
        before do
          updated_form = Zendesk::TicketForms::Initializer.new(@account, @part_params).updated_ticket_form
          updated_form.save!
        end
        it "will reorder system fields not present at the beginning of the form followed by the passed in system fields in the order they were requested" do
          # Note that because we only pass in 2 of the required fields then currently all other required fields will be
          # inserted in standard order before them reguardless of where they were prior to the update.
          # If they don't pass in all the fields then we don't know
          # where to put the new system field positions they're trying to order in relation to the old positions
          assert_equal @priority_field.id, @form.ticket_fields[5].id
          assert_equal @ticket_type_field.id, @form.ticket_fields[6].id
        end
      end

      describe "with no system fields passed in but some custom fields passed in" do
        before do
          params = { ticket_field_ids: [@account.field_description.id, @account.field_group.id, @subject_field.id, @account.field_assignee.id, @account.field_status.id] }
          @custom_form = setup_form(params)
          @custom_form.save!
          @orig_ticket_fields_order = @custom_form.ticket_fields
        end
        it "will reorder system fields to the account default order of system fields" do
          assert_equal ["FieldDescription", "FieldGroup", "FieldSubject", "FieldAssignee", "FieldStatus"], @orig_ticket_fields_order.map(&:type)

          custom_field_params = { id: @custom_form.id, ticket_form: { ticket_field_ids: [@custom_ticket_field2.id] } }
          updated_form = Zendesk::TicketForms::Initializer.new(@account, custom_field_params).updated_ticket_form
          updated_form.save!
          @custom_form.reload

          assert_equal ["FieldSubject", "FieldDescription", "FieldStatus", "FieldGroup", "FieldAssignee", "FieldCheckbox"], @custom_form.ticket_fields.map(&:type)
        end
      end
    end

    describe "with conditions" do
      before do
        Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(true)
        @field_tagger_custom = ticket_fields(:field_tagger_custom)
        @field_tagger_custom_2 = FactoryBot.create(:field_tagger, account: @account)
        @field_tagger_custom_3 = FactoryBot.create(:field_tagger, account: @account)
        @field_tagger_custom_4 = FactoryBot.create(:field_tagger, account: @account)
        @cfo1 = custom_field_options(:field_tagger_custom_option_1)
      end

      it "adds no conditions when CTF is disabled" do
        Account.any_instance.stubs(:has_native_conditional_fields_enabled?).returns(false)

        conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false
          ]
        ]

        form_params = {
          id: @form.id, ticket_form: {
            ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
            agent_conditions: conditions,
            end_user_conditions: conditions
          }
        }
        updated_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form
        updated_form.save!
        updated_form.reload

        assert_equal [], updated_form.agent_conditions
        assert_equal [], updated_form.end_user_conditions
      end

      it "adds agent conditions" do
        agent_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false,
            required_on_statuses: { type: TicketFieldCondition::REQUIRED_ON_NO_STATUSES }
          ]
        ]

        form_params = {
          id: @form.id, ticket_form: {
            ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
            agent_conditions: agent_conditions
          }
        }
        updated_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form
        updated_form.save!
        updated_form.reload

        assert_equal agent_conditions, updated_form.agent_conditions
      end

      it "adds end user conditions" do
        end_user_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false
          ]
        ]

        form_params = {
          id: @form.id, ticket_form: {
            ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
            end_user_conditions: end_user_conditions
          }
        }
        updated_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form
        updated_form.save!
        updated_form.reload

        assert_equal end_user_conditions, updated_form.end_user_conditions
      end

      it "adds agent conditions and end user conditions" do
        agent_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false,
            required_on_statuses: { type: TicketFieldCondition::REQUIRED_ON_NO_STATUSES }
          ]
        ]

        end_user_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false
          ]
        ]

        form_params = {
          id: @form.id, ticket_form: {
            ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
            agent_conditions: agent_conditions,
            end_user_conditions: end_user_conditions
          }
        }
        updated_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form
        updated_form.save!
        updated_form.reload

        assert_equal agent_conditions, updated_form.agent_conditions
        assert_equal end_user_conditions, updated_form.end_user_conditions
      end

      it "prevents adding end user conditions with bad values" do
        end_user_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.id, # cfo1.value would be valid
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false
          ]
        ]

        form_params = {
          id: @form.id, ticket_form: {
            ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
            end_user_conditions: end_user_conditions
          }
        }
        updated_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form
        refute updated_form.save
        assert updated_form.ticket_field_conditions.first.errors.messages[:value]
      end

      it "prevents adding end user conditions with agent-only parent field" do
        @field_tagger_custom.is_visible_in_portal = false
        @field_tagger_custom.save!

        end_user_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false
          ]
        ]

        form_params = {
          id: @form.id, ticket_form: {
            ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id],
            end_user_conditions: end_user_conditions
          }
        }
        updated_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form
        refute updated_form.save
        assert updated_form.ticket_field_conditions.first.errors.messages[:parent_field]
      end

      it "prevents adding conditions with fields not on the ticket form" do
        assert_equal false, @form.ticket_fields.include?(@field_tagger_custom)

        end_user_conditions = [
          parent_field_id: @field_tagger_custom.id,
          value: @cfo1.value,
          child_fields: [
            id: @field_tagger_custom_2.id,
            is_required: false
          ]
        ]

        form_params = {
          id: @form.id, ticket_form: {
            ticket_field_ids: [@field_tagger_custom_3.id, @field_tagger_custom_2.id],
            end_user_conditions: end_user_conditions
          }
        }
        updated_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form
        refute updated_form.save
        assert_equal ["is invalid"], updated_form.ticket_field_conditions.first.errors.messages[:parent_field]
      end

      describe "with pre-existing conditions" do
        before do
          @agent_conditions = [
            {
              parent_field_id: @field_tagger_custom.id,
              value: @cfo1.value,
              child_fields: [
                {
                  id: @field_tagger_custom_2.id,
                  is_required: true,
                  required_on_statuses: { type: TicketFieldCondition::REQUIRED_ON_ALL_STATUSES }
                },
                {
                  id: @field_tagger_custom_3.id,
                  is_required: true,
                  required_on_statuses: { type: TicketFieldCondition::REQUIRED_ON_ALL_STATUSES }
                }
              ]
            }
          ]
          @end_user_conditions = [
            parent_field_id: @field_tagger_custom.id,
            value: @cfo1.value,
            child_fields: [{ id: @field_tagger_custom_2.id, is_required: false, }]
          ]
          @form = setup_form(
            ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id, @field_tagger_custom_3.id, @field_tagger_custom_4.id],
            agent_conditions: @agent_conditions,
            end_user_conditions: @end_user_conditions
          )
          @form.save!
        end

        it "doesn't change conditions if input conditions is nil" do
          form_params = {
            id: @form.id, ticket_form: { }
          }
          updated_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form
          updated_form.save!
          updated_form.reload

          assert_equal @agent_conditions, updated_form.agent_conditions
          assert_equal @end_user_conditions, updated_form.end_user_conditions
        end

        it "updates conditions" do
          agent_conditions = [
            {
              parent_field_id: @field_tagger_custom.id,
              value: @cfo1.value,
              child_fields: [
                {
                  id: @field_tagger_custom_2.id,
                  is_required: true,
                  required_on_statuses: { type: TicketFieldCondition::REQUIRED_ON_ALL_STATUSES }
                }
              ]
            }
          ]
          form_params = {
            id: @form.id, ticket_form: { agent_conditions: agent_conditions }
          }
          updated_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form
          updated_form.save!
          updated_form.reload

          assert_equal agent_conditions, updated_form.agent_conditions
        end

        describe_with_arturo_setting_enabled :native_conditional_fields do
          it "prevents removing a parent ticket field from the form when it is being used in conditions" do
            form_params = { id: @form.id, ticket_form: { ticket_field_ids: [@field_tagger_custom_2.id, @field_tagger_custom_3.id] } }
            changed_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form

            error = assert_raises ActiveRecord::RecordInvalid do
              assert_equal false, changed_form.save!
            end
            assert_includes error.message, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_removed_because_conditions', field_name: @field_tagger_custom.title)
            # Ensure the ticket_form_field association was not destroyed
            assert(changed_form.ticket_form_fields.map(&:ticket_field_id).include?(@field_tagger_custom.id))
          end

          it "prevents removing a child ticket field from the form when it is being used in conditions" do
            form_params = { id: @form.id, ticket_form: {ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_3.id] } }
            changed_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form

            error = assert_raises ActiveRecord::RecordInvalid do
              assert_equal false, changed_form.save!
            end
            assert_includes error.message, I18n.t('txt.admin.models.ticket_field.ticket_field.field_cannot_be_removed_because_conditions', field_name: @field_tagger_custom_2.title)
          end

          it "does not prevent a ticket field not in conditions being removed from the form" do
            form_params = {
              id: @form.id, ticket_form: {
                ticket_field_ids: [@field_tagger_custom.id, @field_tagger_custom_2.id, @field_tagger_custom_3.id]
              }
            }
            changed_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form

            assert changed_form.save!
            changed_form.reload

            assert_equal false, changed_form.ticket_fields.include?(@field_tagger_custom_4)
          end

          it "does not validate if ticket_fields(s) are in conditions if there are no conditions on the form" do
            new_form = setup_form
            new_form.save!
            TicketForm.any_instance.expects(:validate_ticket_fields_in_conditions).never
            assert_empty(new_form.ticket_field_conditions)
          end
        end

        describe_with_arturo_setting_disabled :native_conditional_fields do
          it "does not validate if ticket field(s) are used in conditions" do
            TicketForm.any_instance.expects(:validate_ticket_fields_in_conditions).never
            form_params = { id: @form.id, ticket_form: { ticket_field_ids: [@field_tagger_custom_2.id, @field_tagger_custom_3.id] } }
            changed_form = Zendesk::TicketForms::Initializer.new(@account, form_params).updated_ticket_form
            changed_form.save!
          end
        end
      end
    end
  end

  describe "#cloned_form" do
    before do
      @params = {
        ticket_field_ids: [@custom_ticket_field.id, @custom_ticket_field2.id],
        restricted_brand_ids: [@brand.id]
      }
      @form = setup_form(@params)
      @form.save!
    end

    it "creates a new ticket form with the same values as the form given" do
      assert_difference('TicketForm.count(:all)', 1) do
        @clone = Zendesk::TicketForms::Initializer.new(@account, id: @form.id).cloned_ticket_form
        @clone.save!
      end

      assert_equal @form.name, @clone.name
      assert_equal @form.display_name, @clone.display_name
      assert_equal @form.end_user_visible, @clone.end_user_visible
      assert_equal @form.in_all_brands, @clone.in_all_brands
      assert_equal @form.position + 1, @clone.position
      assert_equal @form.ticket_fields, @clone.ticket_fields
      assert_equal @form.restricted_brands, @clone.restricted_brands
    end
  end

  def setup_form(options = {})
    params = {
      ticket_form: {
        name: "Wombat",
        display_name: "Wombats are everywhere",
        position: 2,
        default: false,
        active: true,
        end_user_visible: true,
        in_all_brands: true
      }
    }
    params[:ticket_form].merge!(options) if options
    Zendesk::TicketForms::Initializer.new(@account, params).ticket_form
  end
end
