require_relative "../../support/test_helper"

SingleCov.covered!

class MailInlineImagesControllerSupportTestController < ApplicationController
  include Zendesk::MailInlineImagesControllerSupport

  def show
    head :ok
  end
end

class MailInlineImagesControllerSupportTest < ActionController::TestCase
  tests MailInlineImagesControllerSupportTestController
  use_test_routes

  describe "MailInlineImagesControllerSupport" do
    let(:filename) { "attachment_2_square.png" }
    let(:message_id) { "7aa47a71-fe7c-49be-824e-d9bab93761fe" }
    let(:remote_file) { RemoteFiles::File.new(filename, content: "{}", content_type: 'text/plain') }
    let(:base_url) { "https://s3.amazonaws.com/zendesk-inbound-mail-attachments-staging" }

    before do
      @minimum_admin = users(:minimum_admin)
      login(@minimum_admin)
    end

    describe "#retrieve_remote_file" do
      let(:directory) { 'zendesk-eu-fra1-inbound-mail-attachments-production' }

      before do
        RemoteFiles::MemoryStore.any_instance.stubs(:options).returns(directory: directory)
      end

      describe "when the remote file cannot be found" do
        it "returns false" do
          MailInlineImagesControllerSupportTestController.any_instance.stubs(:base_url).returns(nil)

          refute @controller.send(:retrieve_remote_file)
        end
      end

      describe "when the remote file is found" do
        it "returns the remote file" do
          config = RemoteFiles::CONFIGURATIONS.values.first
          config.stubs(:file_from_url).returns(remote_file)
          remote_file.stubs(:retrieve!)
          MailInlineImagesControllerSupportTestController.any_instance.stubs(:base_url).returns(base_url)
          remote_file = @controller.send(:retrieve_remote_file)
          assert_equal filename, remote_file.identifier
        end
      end

      describe "when an exception is raised retrieving the base_url" do
        let(:configuration_count) { RemoteFiles::CONFIGURATIONS.values.count }

        describe "when the remote file cannot be found" do
          it "returns false" do
            MailInlineImagesControllerSupportTestController.
              any_instance.stubs(:base_url).
              raises(Excon::Error::BadRequest).
              then.
              returns(nil)

            refute @controller.send(:retrieve_remote_file)
          end
        end

        describe "when the remote file is found" do
          it "returns the remote file" do
            MailInlineImagesControllerSupportTestController.
              any_instance.
              stubs(:base_url).
              raises(Excon::Error::BadRequest).
              then.
              returns(base_url)

            RemoteFiles::CONFIGURATIONS.values.each do |config|
              config.stubs(:file_from_url).returns(remote_file)
              remote_file.stubs(:retrieve!)
            end

            remote_file = @controller.send(:retrieve_remote_file)
            assert_equal filename, remote_file.identifier
          end
        end
      end

      describe "when stores do not contain inbound mail attachments" do
        let(:directory) { 'zendesk-euc1-csv-export-production' }

        it "returns false" do
          refute @controller.send(:retrieve_remote_file)
        end
      end
    end

    describe "#base_url" do
      describe "when store is not a FogStore" do
        it "returns nil" do
          assert_nil @controller.send(:base_url, RemoteFiles::MemoryStore.new('test_memory_store'))
        end
      end

      describe "when store is a FogStore" do
        let(:bucket) { "my_test_bucket" }

        it "returns a base url" do
          store = RemoteFiles::FogStore.new(:fog)
          store[:provider] = 'AWS'
          store[:aws_access_key_id]     = 'access_key_id'
          store[:aws_secret_access_key] = 'secret_access_key'
          store.stubs(:directory).returns(OpenStruct.new(key: bucket))

          assert_equal "https://s3.amazonaws.com/#{bucket}", @controller.send(:base_url, store)
        end
      end
    end

    describe "#remote_file_filename" do
      it 'returns the file name for the remote file' do
        assert_equal filename, @controller.send(:remote_file_filename, remote_file)
      end
    end

    describe "#encoded_filename" do
      it "returns the encoded filename" do
        get :show, params: { message_id: message_id, filename: filename }

        assert_equal "90538%2F7aa47a71-fe7c-49be-824e-d9bab93761fe%2Fattachment_2_square.png", @controller.send(:encoded_filename)
      end
    end

    describe "#filename" do
      it "returns the filename from params" do
        get :show, params: { message_id: message_id, filename: filename }

        assert_equal filename, @controller.send(:filename)
      end
    end

    describe "#message_id" do
      it "returns the message_id from params" do
        get :show, params: { message_id: message_id, filename: filename }

        assert_equal message_id, @controller.send(:message_id)
      end
    end
  end
end
