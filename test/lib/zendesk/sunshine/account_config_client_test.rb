require_relative '../../../support/job_helper'

SingleCov.covered!

describe Zendesk::Sunshine::AccountConfigClient do
  let(:account_config_client) { Zendesk::Sunshine::AccountConfigClient.new(account: account) }
  let(:account) { accounts(:minimum) }
  let(:request_headers) do
    { 'Content-Type' => 'application/json' }
  end
  let(:status_completed) { 'completed' }

  describe '#delete_account when reason is canceled' do
    let(:reason) { 'canceled' }

    before do
      stub_request(:delete, "https://accounts.zendesk-test.com/api/sunshine/account_config/private/account/#{account.id}?reason=#{reason}").
        with(headers: {
          'X-Zendesk-Account-Id' => '90538'
        }).
        to_return(status: 200, body: {data: {status: status_completed}}.to_json, headers: request_headers)
    end

    it 'sends HTTP request to delete account in sunshine' do
      status = account_config_client.delete_account reason
      assert_equal status_completed, status
    end
  end
end
