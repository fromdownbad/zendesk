require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Sell::InternalApiClient do
  let(:account) { accounts(:minimum) }

  describe "#initialize" do
    it 'finds deleted accounts' do
      account.cancel!
      account.soft_delete!
      KragleConnection.expects(:build_for_sell).returns(Kragle.new('sell'))
      Zendesk::Sell::InternalApiClient.new(account.subdomain)
    end

    it 'sets the `timeout` to the default value' do
      assert_equal(
        Zendesk::Explore::InternalApiClient::REQUEST_TIMEOUT,
        Zendesk::Explore::InternalApiClient.
          new(account.subdomain).
          send(:timeout)
      )
    end

    describe 'when initialized with `timeout`' do
      it 'sets the `timeout` to the provided value' do
        assert_equal(
          20,
          Zendesk::Explore::InternalApiClient.
            new(account.subdomain, timeout: 20.seconds).
            send(:timeout)
        )
      end
    end
  end

  describe '#delete_account' do
    it 'makes a DELETE request' do
      request = stub_request(
        :post,
        "https://minimum.zendesk-test.com/api/sell/private/usuvator/api/v1/zendesk/account"
      )
      Zendesk::Sell::InternalApiClient.new(account.subdomain).delete_account
      assert_requested(request)
    end
  end
end
