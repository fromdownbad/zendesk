require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 20

describe Zendesk::SecurityPolicy do
  fixtures :accounts, :subscriptions, :organizations, :account_property_sets, :users, :user_identities, :groups

  # make sure passwords are read from old auditing, even if new auditing is enabled
  def clear_passwords_from_new_auditing
    CIA::AttributeChange.without_arsi.delete_all "attribute_name = 'crypted_password'"
  end

  def self.should_with_auditing(*args, &block)
    it(*args) do
      @user = users(:minimum_end_user)
      CIA.audit(actor: @user, effective_actor: @user) do
        instance_exec(&block)
      end
    end
  end

  def self.it_generates_a_valid_random_password
    it "generates a valid random password" do
      100.times do
        @policy.password = Users::Password::BCrypt.create(@policy.random_password)
        assert @policy.valid?, @policy.errors.inspect
      end
    end
  end

  describe "Finding a policy" do
    it "provides an instance of Low when passed the low id" do
      assert_equal Zendesk::SecurityPolicy::Low, Zendesk::SecurityPolicy.find(100).class
    end

    it "provides an instance of Medium when passed the medium id" do
      assert_equal Zendesk::SecurityPolicy::Medium, Zendesk::SecurityPolicy.find(200).class
    end

    it "provides and instance of High when passed the high id" do
      assert_equal Zendesk::SecurityPolicy::High, Zendesk::SecurityPolicy.find(300).class
    end

    it "provides and instance of Custom when passed the custom id and an account" do
      assert_equal Zendesk::SecurityPolicy::Custom, Zendesk::SecurityPolicy.find(400, accounts(:minimum)).class
    end
  end

  describe "Low" do
    before do
      Timecop.freeze
      @policy = Zendesk::SecurityPolicy::Low.new
    end

    let(:base_password_requirements) do
      [
        "must be at least 5 characters",
        "must be fewer than 128 characters",
        "must be different from email address",
      ]
    end

    let(:classic_password_requirements) do
      base_password_requirements << "10 attempts allowed before lockout"
    end

    describe '#password_requirements' do
      it "returns classic_password_requirements" do
        assert_equal classic_password_requirements, @policy.password_requirements
      end
    end

    describe '#requirements_in_words' do
      it "returns base_password_requirements" do
        assert_equal base_password_requirements, @policy.requirements_in_words[:password]
      end
    end

    it "requires the password to be at least 5 characters" do
      @policy.password = Users::Password::BCrypt.create('12345')
      assert @policy.valid?

      @policy.password = Users::Password::BCrypt.create('1234')
      refute @policy.valid?
    end

    it "requires the password to be different from the username" do
      @policy.password = Users::Password::BCrypt.create('agent@zendesk.com')
      @policy.user     = stub(email: 'agent@zendesk.com')

      refute @policy.valid?
    end

    it_generates_a_valid_random_password

    it "returns the default password attempt threshold" do
      assert_equal 10, @policy.password_attempt_threshold
    end

    describe "#level?" do
      it "returns true when called with the policy level" do
        assert @policy.level?(:low)
      end

      it "returns false when called with a different level" do
        refute @policy.level?(:medium)
      end
    end

    describe "#to_i" do
      it "returns the policy level ID" do
        assert_equal @policy.id, @policy.to_i
      end
    end

    describe "#to_s" do
      it "equals the string version of the policy level" do
        assert_equal @policy.id.to_s, @policy.to_s
      end
    end

    describe "with expire_old_passwords arturo" do
      let(:older_than_threshold) { '2016-10-01' }
      let(:user) { users(:minimum_end_user) }
      before { Arturo.stubs(:feature_enabled_for?).with(:expire_old_passwords, accounts(:minimum)).returns(true) }
      it "is expired for users older than threshold" do
        user.update_column(:created_at, older_than_threshold)

        assert_equal false, user.security_policy.password_expiring?
        assert(user.security_policy.password_expired?)
      end

      it "is expired for users with password older than threshold" do
        audit = user.password_changes.create!
        audit.update_column(:created_at, older_than_threshold)

        assert_equal false, user.security_policy.password_expiring?
        assert(user.security_policy.password_expired?)
      end
    end

    describe "without expire_old_passwords arturo" do
      let(:older_than_threshold) { '2016-10-01' }
      let(:user) { users(:minimum_end_user) }
      before { Arturo.stubs(:feature_enabled_for?).with(:expire_old_passwords, accounts(:minimum)).returns(false) }
      it "is not expired for users older than threshold" do
        user.update_column(:created_at, older_than_threshold)

        assert_equal false, user.security_policy.password_expired?
      end

      it "is not expired for users with password older than threshold" do
        audit = user.password_changes.create!
        audit.update_column(:created_at, older_than_threshold)

        assert_equal false, user.security_policy.password_expired?
      end
    end

    # rubocop:disable Style/CaseEquality
    describe "#===" do
      before do
        @matching_policy  = stub(id: 100)
        @different_policy = stub(id: 200)
      end

      it "returns true when the IDs are the same" do
        assert Zendesk::SecurityPolicy::Low === @matching_policy
        assert Zendesk::SecurityPolicy::Low.new(@account) === @matching_policy
      end

      it "returns false when the IDs are different" do
        refute Zendesk::SecurityPolicy::Low === @different_policy
        refute Zendesk::SecurityPolicy::Low.new(@account) === @different_policy
      end
    end
    # rubocop:enable Style/CaseEquality
  end

  describe "Medium" do
    before do
      @account = accounts(:minimum)
      Account.any_instance.stubs(:is_serviceable?).returns(true)
      @policy = Zendesk::SecurityPolicy::Medium.new
    end

    let(:base_password_requirements) do
      [
        "must be at least 6 characters",
        "must be fewer than 128 characters",
        "must be different from email address",
        "must include letters in mixed case and numbers",
        "must include a character that is not a letter or number",
      ]
    end

    let(:classic_password_requirements) do
      base_password_requirements << "10 attempts allowed before lockout"
    end

    describe '#password_requirements' do
      it "returns classic_password_requirements" do
        assert_equal classic_password_requirements, @policy.password_requirements
      end
    end

    describe '#requirements_in_words' do
      it "returns base_password_requirements" do
        assert_equal base_password_requirements, @policy.requirements_in_words[:password]
      end
    end

    it "requires the password to be at least 6 characters" do
      @policy.password = Users::Password::BCrypt.create('a2B45?')
      assert @policy.valid?

      @policy.password = Users::Password::BCrypt.create('a2B4?')
      refute @policy.valid?
    end

    it "requires the password to be different from the username" do
      @policy.password = Users::Password::BCrypt.create('agent@zendesk.com')
      @policy.user     = stub(email: 'agent@zendesk.com')

      refute @policy.valid?
    end

    it "requires the password to have an uppercase letter, lowercase letter, and a number" do
      @policy.password = Users::Password::BCrypt.create('a2B456?')
      assert @policy.valid?
      assert @policy.errors.blank?

      invalid_passwords = ['123456?', '123abc?', '123ABC?', 'abcABC?']
      invalid_passwords.each do |invalid_password|
        @policy.password = Users::Password::BCrypt.create(invalid_password)
        refute @policy.valid?, "#{invalid_password} should not be valid"
      end
    end

    it "requires the password to have a character that isn't a letter or number" do
      @policy.password = Users::Password::BCrypt.create('a2B456?')
      assert @policy.valid?
      assert @policy.errors.blank?

      @policy.password = Users::Password::BCrypt.create('a2B456')
      refute @policy.valid?, 'Should require a special character'
    end

    it_generates_a_valid_random_password

    it "does not be expired for new account with low" do
      user = users(:minimum_end_user)
      assert_equal Zendesk::SecurityPolicy::Low, user.security_policy.class
      assert_equal false, user.security_policy.password_expiring?
      assert_equal false, user.security_policy.password_expired?
    end

    it "does not be expired for new account with medium and no audits" do
      user = users(:minimum_end_user)

      role_settings = @account.role_settings
      role_settings.end_user_security_policy_id = @policy.id
      role_settings.save!

      assert_equal Zendesk::SecurityPolicy::Medium, user.security_policy.class
      assert_equal [], user.account.cia_events

      assert(user.security_policy.password_expiring?)
      assert_equal false, user.security_policy.password_expired?
    end

    should_with_auditing "expire older passwords when recently upgraded from a lower policy" do
      user = users(:minimum_end_user)

      # Upgrade the security policy
      role_settings = @account.role_settings
      role_settings.end_user_security_policy_id = @policy.class.id
      role_settings.save!

      Timecop.travel(1.hour.from_now) do
        assert(user.security_policy.password_expiring?)
        assert_equal false, user.security_policy.password_expired?
      end

      @policy.user     = user
      @policy.password = user.crypted_password
      assert(@policy.password_expiring?)
      assert_equal false, @policy.password_expired?

      Timecop.travel(5.days.from_now) do
        assert_equal false, @policy.password_expiring?
        assert(@policy.password_expired?)

        user.password = 'a2B456?'
        user.save!
        @policy.user.reload
        @policy.password = user.crypted_password

        refute @policy.password_expiring?
        refute @policy.password_expired?
      end
    end

    should_with_auditing "not expire older passwords when recently downgraded from a higher policy" do
      user = users(:minimum_end_user)
      user.stubs(:password_security_policy_id).returns(@policy.id)

      role_settings = @account.role_settings

      role_settings.end_user_security_policy_id = Zendesk::SecurityPolicy::High.id
      role_settings.save!

      role_settings.end_user_security_policy_id = Zendesk::SecurityPolicy::Medium.id
      role_settings.save!

      assert_equal false, user.security_policy.password_expiring?
      assert_equal false, user.security_policy.password_expired?
    end

    describe "#level?" do
      it "returns true when called with the policy level" do
        assert @policy.level?(:medium)
      end

      it "returns false when called with a different level" do
        refute @policy.level?(:high)
      end
    end
  end

  describe "High" do
    before do
      @policy = Zendesk::SecurityPolicy::High.new
    end

    let(:base_password_requirements) do
      [
        "must be different than the previous 5 passwords",
        "must be at least 6 characters",
        "must be fewer than 128 characters",
        "must be different from email address",
        "must include letters in mixed case and numbers",
        "must include a character that is not a letter or number",
        "expires after 90 days"
      ]
    end

    let(:classic_password_requirements) do
      base_password_requirements << "10 attempts allowed before lockout"
    end

    describe '#password_requirements' do
      it "returns classic_password_requirements" do
        assert_equal classic_password_requirements, @policy.password_requirements
      end
    end

    describe '#requirements_in_words' do
      it "returns base_password_requirements" do
        assert_equal base_password_requirements, @policy.requirements_in_words[:password]
      end
    end

    describe "password history" do
      should_with_auditing "require the password to be different from previous passwords" do
        policy_valid = lambda do
          clear_passwords_from_new_auditing
          @policy.valid?
        end

        old_password = Users::Password::BCrypt.create('a2B456?')

        # with long history and different passwords
        @user.password = old_password.unencrypted
        @user.save!

        password = Users::Password::BCrypt.create('a2B456?_2')
        @policy.user     = @user
        @policy.password = password
        assert policy_valid.call

        # with short history and same passwords
        @policy.user.password = password.unencrypted
        @policy.user.save!

        @policy.password_history_length = 1
        @policy.password = old_password
        assert policy_valid.call

        # with long history and same passwords
        @policy.password_history_length = 2
        refute policy_valid.call, 'Password history not enforced'
      end

      it "does not include the proposed password in its history while validating" do
        user = users(:minimum_end_user)
        user.password = 'a2B456?'
        user.save!
      end
    end

    it "requires the password to be different from the username" do
      user             = users(:minimum_end_user)
      @policy.password = Users::Password::BCrypt.create(user.email)
      @policy.user     = user

      refute @policy.valid?
    end

    should_with_auditing "not use other user's passwords when checking password history" do
      user       = users(:minimum_end_user)
      other_user = users(:minimum_agent)
      password   = Users::Password::BCrypt.create('a2B456?')

      @policy.user     = user
      @policy.password = password
      assert @policy.valid?

      user.password = password
      user.save!

      @policy.user = other_user
      assert @policy.valid?
    end

    should_with_auditing "expire old passwords" do
      password = 'a2B456?'

      @user.password = password
      @user.save!
      @user.reload

      @policy.user     = @user
      @policy.password = @user.crypted_password
      @user.stubs(:password_security_policy_id).returns(@policy.id)

      clear_passwords_from_new_auditing
      assert_equal false,  @policy.password_expiring?
      assert_equal false,  @policy.password_expired?

      Timecop.travel(85.days.from_now) do
        assert(@policy.password_expiring?)
        assert_equal false, @policy.password_expired?
      end

      Timecop.travel(90.days.from_now) do
        assert_equal false, @policy.password_expiring?
        assert(@policy.password_expired?)
      end

      Timecop.travel(1.year.from_now) do
        assert_equal false, @policy.password_expiring?
        assert(@policy.password_expired?)
      end
    end

    describe "#level?" do
      it "returns true when called with the policy level" do
        assert @policy.level?(:high)
      end

      it "returns false when called with a different level" do
        refute @policy.level?(:custom)
      end
    end

    it_generates_a_valid_random_password
  end

  describe "Custom" do
    before do
      Timecop.freeze
      @account = accounts(:minimum)
      @custom_security_policy = CustomSecurityPolicy.build_from_current_policy(@account)
      @custom_security_policy.password_duration = 90
      @custom_security_policy.save!
    end

    it "initializes the policy using the custom values" do
      @policy = Zendesk::SecurityPolicy::Custom.new(@account)

      assert_equal @policy.password_length, @custom_security_policy.password_length
      assert_equal_with_nil @policy.custom_policy_level, @custom_security_policy.policy_level
      assert_equal @policy.password_history_length, @custom_security_policy.password_history_length
      assert_equal @policy.password_duration, @custom_security_policy.password_duration
      assert_equal @policy.password_complexity, @custom_security_policy.password_complexity
      assert_equal @policy.password_in_mixed_case, @custom_security_policy.password_in_mixed_case
      assert_equal @policy.failed_attempts_allowed, @custom_security_policy.failed_attempts_allowed
    end

    it "requires the password to be different from the username" do
      @policy = Zendesk::SecurityPolicy::Custom.new(@account)

      @policy.password = Users::Password::BCrypt.create('agent@zendesk.com')
      @policy.user     = stub(email: 'agent@zendesk.com')

      refute @policy.valid?
    end

    it "uses the id as the policy_level if there's no custom_policy_level" do
      @policy = Zendesk::SecurityPolicy::Custom.new(@account)
      assert_equal Zendesk::SecurityPolicy::Custom.id, @policy.policy_level
    end

    it "uses the custom_policy_level as the policy_level if there's one" do
      @custom_security_policy.update_attribute(:policy_level, 401)

      @policy = Zendesk::SecurityPolicy::Custom.new(@account)
      assert_equal 401, @policy.policy_level
    end

    it "sets the policy_level as the new password_security_policy_id when updating user password" do
      @custom_security_policy.update_attribute(:policy_level, 401)

      user = @account.owner
      User.any_instance.stubs(:security_policy).returns(@custom_security_policy)

      user.password = "123456"
      user.encrypt_password

      assert_equal 401, user.password_security_policy_id
    end

    describe "Validations" do
      it "always include the minimum password length" do
        @policy = Zendesk::SecurityPolicy::Custom.new(@account)
        assert @policy.validations[:unencrypted_password].include?(:minimum_password_length)
      end

      it "does not require numbers or special characters when 'password_complexity' is 'no'" do
        @custom_security_policy.update_attribute(:password_complexity, CustomSecurityPolicy::NO_COMPLEXITY)
        @policy = Zendesk::SecurityPolicy::Custom.new(@account.reload)

        refute @policy.validations[:unencrypted_password].include?(Zendesk::SecurityPolicy::Validation::NumberComplexity)
        refute @policy.validations[:unencrypted_password].include?(Zendesk::SecurityPolicy::Validation::SpecialCharacter)
      end

      it "requires numbers only when 'password_complexity' is 'numbers only'" do
        @custom_security_policy.update_attribute(:password_complexity, CustomSecurityPolicy::NUMBERS_COMPLEXITY)
        @policy = Zendesk::SecurityPolicy::Custom.new(@account.reload)

        assert @policy.validations[:unencrypted_password].include?(Zendesk::SecurityPolicy::Validation::NumberComplexity)
        refute @policy.validations[:unencrypted_password].include?(Zendesk::SecurityPolicy::Validation::SpecialCharacter)
      end

      it "requires numbers and special characters 'password_complexity' is 'numbers + special characters'" do
        @custom_security_policy.update_attribute(:password_complexity, CustomSecurityPolicy::SPECIAL_COMPLEXITY)
        @policy = Zendesk::SecurityPolicy::Custom.new(@account.reload)

        assert @policy.validations[:unencrypted_password].include?(Zendesk::SecurityPolicy::Validation::NumberComplexity)
        assert @policy.validations[:unencrypted_password].include?(Zendesk::SecurityPolicy::Validation::SpecialCharacter)
      end

      it "does not require letters in mixed case when 'password_in_mixed_case' is disabled" do
        @custom_security_policy.update_attribute(:password_in_mixed_case, false)
        @policy = Zendesk::SecurityPolicy::Custom.new(@account.reload)

        refute @policy.validations[:unencrypted_password].include?(Zendesk::SecurityPolicy::Validation::MixedComplexity)
      end

      it "requires letters in mixed case when 'password_in_mixed_case' is enabled" do
        @custom_security_policy.update_attribute(:password_in_mixed_case, true)
        @policy = Zendesk::SecurityPolicy::Custom.new(@account.reload)

        assert @policy.validations[:unencrypted_password].include?(Zendesk::SecurityPolicy::Validation::MixedComplexity)
      end

      it "does not check passwords previously used when 'password_history_length' is 0" do
        @custom_security_policy.update_attribute(:password_history_length, 0)
        @policy = Zendesk::SecurityPolicy::Custom.new(@account.reload)

        refute @policy.validations[:password].include?(:password_not_previously_used)
      end

      it "checks passwords previously used when 'password_history_length' is different than 0" do
        @custom_security_policy.update_attribute(:password_history_length, 5)
        @policy = Zendesk::SecurityPolicy::Custom.new(@account.reload)

        assert @policy.validations[:password].include?(:password_not_previously_used)
      end
    end

    describe "Failed attempts allowed" do
      before do
        @policy = Zendesk::SecurityPolicy::Custom.new(@account)
      end

      it "returns the default password attempt threshold when is 0" do
        @policy.stubs(:failed_attempts_allowed).returns(0)
        assert_equal 10, @policy.password_attempt_threshold
      end

      it "returns the configured threshold when is different than 0" do
        @policy.stubs(:failed_attempts_allowed).returns(3)
        assert_equal 3, @policy.password_attempt_threshold
      end
    end

    describe "Password expiration" do
      describe "if password_duration is 0" do
        before do
          CustomSecurityPolicy.any_instance.stubs(:password_duration).returns(0)
          @policy = Zendesk::SecurityPolicy::Custom.new(@account)
          @policy.user = users(:minimum_end_user)
          @policy.stubs(:created_at).returns(1.week.ago)
          @policy.stubs(:password_created_at).returns(1.week.ago)
          @policy.user.stubs(:password_security_policy_id).returns(Zendesk::SecurityPolicy::Custom.id)
        end

        it "does not expire password" do
          Timecop.travel(1.year.from_now) do
            refute @policy.password_expired?
          end
        end
      end

      describe "if password_duration is greater_than 0" do
        before do
          CustomSecurityPolicy.any_instance.stubs(:password_duration).returns(90)
          @policy = Zendesk::SecurityPolicy::Custom.new(@account)
          @policy.user = users(:minimum_end_user)
          @policy.stubs(:created_at).returns(1.week.ago)
          @policy.stubs(:password_created_at).returns(1.week.ago)
          @policy.user.stubs(:password_security_policy_id).returns(Zendesk::SecurityPolicy::Custom.id)
        end

        it "expires password" do
          Timecop.travel(1.year.from_now) do
            assert @policy.password_expired?
          end
        end
      end

      describe "if the password complexity has been increased" do
        before do
          @custom_security_policy.update_attribute(:password_length, 10)

          @policy = Zendesk::SecurityPolicy::Custom.new(@account)
          @policy.user = users(:minimum_end_user)
          @policy.user.stubs(:password_security_policy_id).returns(Zendesk::SecurityPolicy::Custom.id)
        end

        it "increases the policy id and mark the password for expiration" do
          assert @policy.policy_level > @policy.user.password_security_policy_id
          assert @policy.password_expiring?
        end
      end
    end

    describe "#level?" do
      before { @policy = Zendesk::SecurityPolicy::Custom.new(@account) }

      it "returns true when called with the policy level" do
        assert @policy.level?(:custom)
      end

      it "returns false when called with a different level" do
        refute @policy.level?(:low)
      end
    end
  end
end
