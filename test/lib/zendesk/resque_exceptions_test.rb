require_relative '../../support/test_helper'

SingleCov.covered!

describe Zendesk::ResqueExceptions do
  class TestFailureJob
    extend ZendeskJob::Resque::BaseJob
    priority :immediate

    def self.args_to_log
      {}
    end

    def self.work
      raise StandardError, 'some bad bad exception'
    end
  end

  it 'tracks job exceptions' do
    Resque.inline = false
    TestFailureJob.enqueue
    # We overwrite Resque.push in tests, so we need to instantiate the job directly
    # See https://github.com/zendesk/zendesk/blob/1663166182f4bc126a1af1765ae3c74140c4bb24/config/initializers/resque.rb#L96-L104
    job = Resque::Job.new(:immediate, JSON.load(Resque.jobs.last.to_json))

    ZendeskExceptions::Logger.expects(:record)

    # Use Resque::Worker so Failure backends are called
    worker = Resque::Worker.new(:immediate)
    worker.fork_per_job = false
    # Returns true if the worker processed a job
    assert worker.work_one_job(job)
  end
end
