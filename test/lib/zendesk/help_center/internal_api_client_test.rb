require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Zendesk::HelpCenter::InternalApiClient do
  fixtures :accounts, :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:api_client) { Zendesk::HelpCenter::InternalApiClient.new(account: account, requester: ticket.requester, brand: ticket.brand) }
  let(:locale_response) { {"locales" => ["en-us", "en-gb"], "default_locale" => "en-us"} }

  describe '#connection' do
    let(:connection_as_ticket_requester) { api_client.send(:connection_as_ticket_requester) }
    let(:connection_as_user) { api_client.send(:connection_as_user) }

    it 'should have request signing turned on' do
      assert connection_as_ticket_requester.builder.handlers.include?(Kragle::Middleware::Request::Signing)
      assert connection_as_user.builder.handlers.include?(Kragle::Middleware::Request::Signing)
    end

    it 'should have Zendesk-Internal-User set' do
      assert_equal ticket.requester.id.to_s, connection_as_ticket_requester.headers['X-Zendesk-Internal-User']
      assert_equal ticket.requester.id.to_s, connection_as_user.headers['X-Zendesk-Internal-User']
    end
  end

  describe "#search_articles" do
    let(:options) { ['per_page=25', 'page=1'] }

    describe "on success" do
      it "adds pagination parameters" do
        stub_request(:get, "https://proxy-nlb.pod-1.zdsystest.com/hc/api/v2/articles/search.json?exclude=body&query=potato&locale=*&multibrand=true&per_page=33&page=2").
          to_return(status: 200, body: JSON.dump({}), headers: { 'Content-Type' => 'application/json' })

        assert_equal({}, api_client.search_articles("potato", per_page: 33, page: 2))
      end

      it "gets the results" do
        stub_request(:get, "https://proxy-nlb.pod-1.zdsystest.com/hc/api/v2/articles/search.json?exclude=body&query=potato&locale=*&multibrand=true&per_page=25&page=1").
          to_return(status: 200, body: JSON.dump(count: 100, results: [{ id: 1 }, { id: 2 }]), headers: { 'Content-Type' => 'application/json' })

        results = api_client.search_articles('potato', options)
        assert_equal({ 'count' => 100, 'results' => [{ 'id' => 1 }, { 'id' => 2 }] }, results)
      end

      it "escapes query params" do
        stub_request(:get, "https://proxy-nlb.pod-1.zdsystest.com/hc/api/v2/articles/search.json?exclude=body&query=potato%26sqli%261=1&locale=*&multibrand=true&per_page=25&page=1").
          to_return(status: 200, body: JSON.dump(count: 100, results: [{ id: 1 }, { id: 2 }]), headers: { 'Content-Type' => 'application/json' })

        results = api_client.search_articles('potato&sqli&1=1', options)
        assert_equal({ 'count' => 100, 'results' => [{ 'id' => 1 }, { 'id' => 2 }] }, results)
      end
    end

    describe "on failure" do
      it "should return empty object if the server returns a non 200 response" do
        stub_request(:get, "https://proxy-nlb.pod-1.zdsystest.com/hc/api/v2/articles/search.json?exclude=body&query=potato&locale=*&multibrand=true&per_page=25&page=1").
          to_return(status: 401, body: JSON.dump([]), headers: { 'Content-Type' => 'application/json' })

        assert_equal({}, api_client.search_articles("potato", options))
      end

      it "should return empty object if we can't reach HC" do
        stub_request(:get, "https://proxy-nlb.pod-1.zdsystest.com/hc/api/v2/articles/search.json?exclude=body&query=potato&locale=*&multibrand=true&per_page=25&page=1").
          to_raise(Kragle::ClientError)

        assert_equal({}, api_client.search_articles("potato", options))
      end

      it "should return empty object if there's no HC enabled" do
        stub_request(:get, "https://proxy-nlb.pod-1.zdsystest.com/hc/api/v2/articles/search.json?exclude=body&query=potato&locale=*&multibrand=true&per_page=25&page=1").
          to_raise(Kragle::ResourceNotFound)

        assert_equal({}, api_client.search_articles("potato", options))
      end
    end
  end

  describe "#fetch_article_ids_for_labels" do
    let(:labels) { ["foo", "boo"] }

    describe "on success" do
      let(:articles_response_page_1) do
        {
          'results' => [
            {
              id: '228141247',
              label_names: ['foo']
            },
            {
              id: '112338474',
              label_names: ['boo']
            }
          ],
          'page' => 1,
          'page_count' => 2,
          'next_page' => "https://minimum.zendesk-test.com/api/v2/help_center/articles/search.json?label_names=foo,boo&page=2&per_page=75"
        }
      end
      let(:articles_response_page_2) do
        {
          'results' => [
            {
              id: '777777777',
              label_names: ['boo']
            },
            {
              id: '888888888',
              label_names: ['foo']
            }
          ],
          'page' => 2,
          'page_count' => 2,
          'next_page' => nil
        }
      end

      before do
        stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/locales.json").
          to_return(status: 200, body: JSON.dump(locale_response), headers: { 'Content-Type' => 'application/json' })
        stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/articles/search.json?label_names=foo,boo&per_page=75").
          to_return(status: 200, body: JSON.dump(articles_response_page_1), headers: { 'Content-Type' => 'application/json' })
        stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/articles/search.json?label_names=foo,boo&page=2&per_page=75").
          to_return(status: 200, body: JSON.dump(articles_response_page_2), headers: { 'Content-Type' => 'application/json' })
      end

      it "should retrieve article ids for the given labels" do
        result = api_client.fetch_article_ids_for_labels(labels)

        assert_equal ["228141247", "112338474", "777777777", "888888888"], result
      end
    end

    describe "on failure" do
      it "should return nil if the server returns a non 200 response" do
        stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/articles/search.json?label_names=foo,boo&per_page=75").
          to_return(status: 401, body: JSON.dump([]), headers: { 'Content-Type' => 'application/json' })

        assert_empty api_client.fetch_article_ids_for_labels(labels)
      end
    end
  end

  describe "#fetch_articles_for_ids_and_locales" do
    describe '#when articles exists' do
      let(:returned_articles_en_us) do
        [{'id' => 1, 'title' => 'english title', 'url' => 'http://british', 'html_url' => 'http://stuff', 'body' => 'This is so rad bro.', 'locale' => 'en-us'},
         {'id' => 2, 'title' => 'another english title', 'url' => 'http://stuff', 'html_url' => 'http://stuff', 'body' => 'This is so rad bro.', 'locale' => 'en-us'}]
      end
      let(:returned_articles_en_gb) do
        [{'id' => 3, 'title' => 'title', 'url' => 'http://whatever', 'html_url' => 'http://stuff', 'body' => 'This is so rad bro.', 'locale' => 'en-gb'},
         {'id' => 4, 'title' => 'title', 'url' => 'http://extra_article', 'html_url' => 'http://dont_show_me', 'body' => 'Hi I am a super useful article.', 'locale' => 'en-gb'}]
      end
      let(:api_response_en_us) { {"articles" => returned_articles_en_us} }
      let(:api_response_en_gb) { {"articles" => returned_articles_en_gb} }
      let (:expected_result) do
        [{id: 1,
          title: "english title",
          url: "http://british",
          html_url: "http://stuff",
          body: "This is so rad bro."},
         {id: 2,
          title: "another english title",
          url: "http://stuff",
          html_url: "http://stuff",
          body: "This is so rad bro."},
         {id: 3,
          title: "title",
          url: "http://whatever",
          html_url: "http://stuff",
          body: "This is so rad bro."}]
      end

      before do
        stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/articles.json?ids=1,2&locale=en-us").
          to_return(status: 200, body: JSON.dump(api_response_en_us), headers: {'Content-Type' => 'application/json'})

        stub_request(:get, "https:///minimum.zendesk-test.com/hc/api/v2/articles.json?ids=3,4&locale=en-gb").
          to_return(status: 200, body: JSON.dump(api_response_en_gb), headers: {'Content-Type' => 'application/json'})
      end

      it "retrieves HC article data required for given article ids and locales" do
        actual = api_client.fetch_articles_for_ids_and_locales([{'locale' => 'en-us', 'article_id' => 1},
                                                                {'locale' => 'en-us', 'article_id' => 2},
                                                                {'locale' => 'en-gb', 'article_id' => 3},
                                                                {'locale' => 'en-gb', 'article_id' => 4}])
        assert_equal expected_result, actual
      end

      it "returns a maximum  of 3 articles" do
        result = api_client.fetch_articles_for_ids_and_locales([{'locale' => 'en-us', 'article_id' => 1},
                                                                {'locale' => 'en-us', 'article_id' => 2},
                                                                {'locale' => 'en-gb', 'article_id' => 3},
                                                                {'locale' => 'en-gb', 'article_id' => 4}])
        assert_equal 3, result.size
      end
    end

    describe '#when article locale is not specified' do
      let(:returned_articles) do
        [{'id' => 1, 'title' => 'english title', 'url' => 'http://british', 'html_url' => 'http://stuff', 'body' => 'This is so rad bro.', 'locale' => 'en-us'}]
      end

      let(:api_response) { {"articles" => returned_articles} }
      let (:expected_result) do
        [{id: 1, title: "english title", url: "http://british", html_url: "http://stuff", body: "This is so rad bro."}]
      end

      before do
        stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/locales.json").
          to_return(status: 200, body: JSON.dump(locale_response), headers: { 'Content-Type' => 'application/json' })
        stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/articles.json?ids=1&locale=en-us").
          to_return(status: 200, body: JSON.dump(api_response), headers: {'Content-Type' => 'application/json'})
      end

      it "retrieves HC article data using the default locale" do
        actual = api_client.fetch_articles_for_ids_and_locales([{'locale' => nil, 'article_id' => 1}])
        assert_equal expected_result, actual
      end
    end

    it "returns empty array if no articles are found" do
      api_response = {"articles" => []}
      stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/articles.json?ids=123&locale=en-us").
        to_return(status: 200, body: JSON.dump(api_response), headers: {'Content-Type' => 'application/json'})

      result = api_client.fetch_articles_for_ids_and_locales([{'locale' => 'en-us', 'article_id' => 123}])
      assert_equal [], result
    end

    it "returns empty array if exception is thrown while processing malformed api response" do
      stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/articles.json?ids=123&locale=en-us").
        to_return(status: 200, body: "a", headers: {'Content-Type' => 'application/json'})

      result = api_client.fetch_articles_for_ids_and_locales([{'locale' => 'en-us', 'article_id' => 123}])
      assert_empty result
    end
  end

  describe '#default_locale' do
    it 'retrieves the default locale for a help center' do
      stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/locales.json").
        to_return(status: 200, body: JSON.dump(locale_response), headers: { 'Content-Type' => 'application/json' })

      result = api_client.send :default_locale

      assert_equal "en-us", result
    end

    it 'defaults to en-us if help center doesnt respond with a 200' do
      locale_response = {"locales" => ["en-us", "en-gb"], "default_locale" => "en-au"}
      stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/locales.json").
        to_return(status: 500, body: JSON.dump(locale_response), headers: { 'Content-Type' => 'application/json' })

      result = api_client.send :default_locale

      assert_equal "en-us", result
    end
  end

  describe 'logging' do
    let(:statsd_key) { "resource_not_found" }
    let(:statsd_client_stub) { stub_for_statsd }
    let(:sample_response) { { article: article_content_1 } }
    let(:article_content_1) { { id: 123, title: 'title', html_url: 'http://whatever', body: 'This is so rad bro.', locale: 'en-us'} }

    it 'does not log to statsd_client when a resource request is ok' do
      stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/locales.json").
        to_return(status: 200, body: JSON.dump(locale_response), headers: { 'Content-Type' => 'application/json' })
      stub_request(:get, "https://minimum.zendesk-test.com/hc/api/v2/articles.json?ids=123&locale=en-us").
        to_return(status: 200, body: JSON.dump(sample_response), headers: { 'Content-Type' => 'application/json' })
      api_client.stubs(:statsd_client).returns(statsd_client_stub)
      statsd_client_stub.expects(:increment).never

      api_client.fetch_articles_for_ids_and_locales([{'article_id' => 123, 'locale' => 'en-us'}])
    end
  end

  describe "#answer_bot_setting_enabled?" do
    let(:ticket_form_id) { 1104 }
    let(:answer_bot_setting_endpoint) { "https://minimum.zendesk-test.com/hc/api/internal/answer_bot/settings.json" }
    let(:answer_bot_setting_response) { { "enabled" => true } }
    let(:answer_bot_setting_enabled?) { api_client.answer_bot_setting_enabled? }

    before do
      ticket.update_column(:ticket_form_id, ticket_form_id)
    end

    it "returns true if webform answer_bot setting is enabled" do
      stub_request(:get, answer_bot_setting_endpoint).
        to_return(status: 200, body: JSON.dump(answer_bot_setting_response), headers: { 'Content-Type' => 'application/json' })

      assert answer_bot_setting_enabled?
    end

    it "returns false if webform answer_bot setting is disabled" do
      stub_request(:get, answer_bot_setting_endpoint).
        to_return(status: 200, body: JSON.dump("enabled" => false), headers: { 'Content-Type' => 'application/json' })

      refute answer_bot_setting_enabled?
    end

    it "returns false if an exception is raised" do
      stub_request(:get, answer_bot_setting_endpoint).
        to_raise(Kragle::BadGateway)

      refute answer_bot_setting_enabled?
    end

    it "returns false if response is Forbidden" do
      stub_request(:get, answer_bot_setting_endpoint).
        to_raise(Kragle::Forbidden)

      refute answer_bot_setting_enabled?
    end
  end

  describe "#answer_bot_form_setting_enabled?" do
    let(:ticket_form_id) { 1104 }
    let(:brand_id) { ticket.brand_id }
    let(:answer_bot_form_setting_endpoint) do
      "https://minimum.zendesk-test.com/hc/api/internal/answer_bot/brand_settings/#{brand_id}/ticket_forms/#{ticket_form_id}.json"
    end
    let(:answer_bot_form_setting_enabled?) do
      api_client.answer_bot_form_setting_enabled?(brand_id: brand_id, form_id: ticket_form_id)
    end

    before do
      ticket.update_column(:ticket_form_id, ticket_form_id)
    end

    it "returns true if the answer bot form setting is enabled" do
      stub_request(:get, answer_bot_form_setting_endpoint).
        to_return(status: 200, body: JSON.dump("enabled" => true), headers: { 'Content-Type' => 'application/json' })

      assert answer_bot_form_setting_enabled?
    end

    it "returns false if the answer bot form setting is disabled" do
      stub_request(:get, answer_bot_form_setting_endpoint).
        to_return(status: 200, body: JSON.dump("enabled" => false), headers: { 'Content-Type' => 'application/json' })

      refute answer_bot_form_setting_enabled?
    end

    it "returns false if an exception is raised" do
      stub_request(:get, answer_bot_form_setting_endpoint).
        to_raise(Kragle::BadGateway)

      refute answer_bot_form_setting_enabled?
    end

    it "returns false if response is forbidden" do
      stub_request(:get, answer_bot_form_setting_endpoint).
        to_raise(Kragle::Forbidden)

      refute answer_bot_form_setting_enabled?
    end
  end
end
