require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::InboundMail::AuthorDeliverableState do
  include MailTestHelper
  fixtures :accounts

  before do
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = accounts(:minimum)
    @mail = Zendesk::Mail::InboundMessage.new
    @logger = Zendesk::InboundMail::MailLogger.new(@mail.logger)
    @deliverable_state = DeliverableStateType.TICKET_SHARING_PARTNER # any deliverable state
  end

  describe "author exists" do
    before do
      @state.from = address(users(:minimum_author).email)
      @state.author = users(:minimum_author)
    end

    it "marks identity with correct deliverable state" do
      Zendesk::InboundMail::AuthorDeliverableState.mark(state: @state, new_deliverable_state: @deliverable_state, logger: @logger)
      identity = @state.account.user_identities.where(value: @state.from.address).first
      identity.deliverable_state.must_equal @deliverable_state
    end
  end

  describe "author is supplied" do
    before do
      @state.from = address(users(:minimum_author).email)
      @author = users(:minimum_author)
    end

    it "does not search for the user" do
      Zendesk::InboundMail::AuthorDeliverableState.any_instance.expects(:find_or_create_author).never
      Zendesk::InboundMail::AuthorDeliverableState.mark(state: @state, new_deliverable_state: @deliverable_state, logger: @logger, author: @author)
    end

    it "marks identity with correct deliverable state" do
      Zendesk::InboundMail::AuthorDeliverableState.mark(state: @state, new_deliverable_state: @deliverable_state, logger: @logger, author: @author)
      identity = @author.reload.identities.where(value: @state.from.address).first
      identity.deliverable_state.must_equal @deliverable_state
    end
  end

  describe "no author exists" do
    before { @state.from = address("new_author@example.com") }

    describe "identity exists for email" do
      before do
        @state.account.users.create_with_email(@state.from.address)
      end

      it "marks identity with correct deliverable state" do
        Zendesk::InboundMail::AuthorDeliverableState.mark(state: @state, new_deliverable_state: @deliverable_state, logger: @logger)
        identity = @state.account.user_identities.where(value: @state.from.address).first
        identity.deliverable_state.must_equal @deliverable_state
      end
    end

    describe "completely new user" do
      it "marks identity with correct deliverable state" do
        Zendesk::InboundMail::AuthorDeliverableState.mark(state: @state, new_deliverable_state: @deliverable_state, logger: @logger)
        identity = @state.account.user_identities.where(value: @state.from.address).first
        identity.deliverable_state.must_equal @deliverable_state
      end
    end
  end
end
