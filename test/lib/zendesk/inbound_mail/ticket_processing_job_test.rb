require_relative '../../../support/test_helper'
require_relative '../../../support/job_helper'
require_relative '../../../support/collaboration_settings_test_helper'

SingleCov.covered! uncovered: 3

describe Zendesk::InboundMail::TicketProcessingJob do
  extend ArturoTestHelper

  include MailTestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:agent_email) do
    Zendesk::Mail::Address.new.tap do |address|
      address.name = account.owner.name
      address.address = account.owner.email
      address.original_name = nil
    end
  end
  let(:from_address) { nil }
  let(:suspension) { nil }
  let(:mail) { FixtureHelper::Mail.read('minimum_ticket.json') }

  before do
    account.is_open = true
    account.is_signup_required = false
    account.domain_blacklist = nil
    account.domain_whitelist = nil
    account.save!

    ActionMailer::Base.deliveries = []
    CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
    stub_request(:put, %r{\.amazonaws\.com/data/attachments})

    if from_address
      mail.from = from_address
      Zendesk::InboundMail::MailParsingQueue::ProcessingState.any_instance.stubs(
        from: Zendesk::Mail::Address.new.tap do |zendesk_address|
          zendesk_address.name = from_address.name
          zendesk_address.address = Zendesk::MtcMpqMigration::ZendeskMail::ZendeskAddressHelper.remove_plus_id(from_address.address)
          zendesk_address.original_name = nil
        end,
        from_header: Zendesk::Mail::Address.new.tap do |zendesk_address|
          zendesk_address.name = from_address.name
          zendesk_address.address = Zendesk::MtcMpqMigration::ZendeskMail::ZendeskAddressHelper.remove_plus_id(from_address.address)
          zendesk_address.original_name = nil
        end
      )
    end

    Zendesk::InboundMail::ProcessingState.any_instance.expects(:suspension).at_least_once.returns(suspension) unless suspension.nil?
  end

  class FakeAccountChain
    def self.execute(state, _mail)
      subscription = state.account.subscription
      Subscription.find(subscription.id)
      Subscription.find(subscription.id)
    end
  end

  class FakeShardChain
    def self.execute(state, _mail)
      ticket = state.account.tickets.first
      Ticket.find(ticket.id)
      Ticket.find(ticket.id)
    end
  end

  describe 'query caching' do
    before do
      Zendesk::InboundMail::TicketProcessingJob.any_instance.stubs(account: account)
      account.stubs(skip_update_account_suspension_state?: false)
    end

    it 'is enabled for the shard' do
      Zendesk::InboundMail::TicketProcessingJob.any_instance.stubs(:processing_chain).returns(FakeShardChain)
      assert_sql_queries 5 do
        process_mail(mail, account: account)
      end
    end

    it 'is enabled for the account' do
      Zendesk::InboundMail::TicketProcessingJob.any_instance.stubs(:processing_chain).returns(FakeAccountChain)
      assert_sql_queries 5 do
        process_mail(mail, account: account)
      end
    end
  end

  describe 'when subject field is turned off' do
    before { account.field_subject.update_attribute(:is_active, false) }

    it 'puts the email subject in the comment' do
      state  = process_mail(mail, account: account)
      ticket = state.ticket

      assert ticket
      assert_equal account, ticket.account

      assert ticket.subject.blank?
      assert "#400 Hello there: Hello world!\r\nMicrosoft Word fails to print my document", ticket.description
      assert ticket.via?(:mail)
    end
  end

  describe 'when subject field is turned on' do
    before { account.field_subject.update_attribute(:is_active, true) }

    it 'puts the email subject in the comment' do
      state  = process_mail(mail, account: account)
      ticket = state.ticket

      assert ticket
      assert_equal account, ticket.account

      assert_equal '#400 Hello there', ticket.subject
      assert_equal "Hello world!\r\nMicrosoft Word fails to print my document", ticket.description
      assert ticket.via?(:mail)
    end
  end

  describe 'replying to a mail' do
    it 'adds comments to the ticket mentioned in subject' do
      mail = FixtureHelper::Mail.read('plain_text_email_with_delimiter.json')
      mail.subject += tickets(:minimum_1).encoded_id
      should_add_comment_to_ticket(mail)

      mail = FixtureHelper::Mail.read('html_text_in_body_email_with_delimiter.json')
      mail.subject += tickets(:minimum_1).encoded_id
      should_add_comment_to_ticket(mail)
    end

    it 'adds comments to the ticket mentioned in the in_reply_to header' do
      ticket = should_create_ticket(mail) # rubocop:disable Lint/UselessAssignment

      mail.in_reply_to = mail.message_id
      mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id

      assert_difference('ticket.comments.count(:all)') do
        process_mail(mail, account: account)
      end
    end

    it 'assigns an agent if he responds' do
      user = users(:minimum_agent)
      ticket = tickets(:minimum_2)
      assert_nil ticket.assignee

      rules(:trigger_auto_assign_responding_agent).update_attribute(:is_active, true)
      mail.subject = "[#{ticket.encoded_id}] update"
      stub_mail_from Zendesk::Mail::Address.new(name: user.name, address: user.email)

      process_mail(mail, account: account)

      assert_equal user, ticket.reload.assignee
    end

    it 'does not assign a user if he responds' do
      ticket = tickets(:minimum_2)
      user = ticket.requester
      assert_nil ticket.assignee

      rules(:trigger_auto_assign_responding_agent).update_attribute(:is_active, true)
      mail.subject = "[#{ticket.encoded_id}] update"
      stub_mail_from Zendesk::Mail::Address.new(name: user.name, address: user.email)

      process_mail(mail, account: account)

      assert_nil ticket.assignee
    end
  end

  describe 'from an unknown user' do
    let(:from_address) { Zendesk::Mail::Address.new(address: 'irma@aghassipour.com') }

    describe "when it's a closed helpdesk" do
      before do
        account.is_open = false
        account.save!
      end

      it 'does not create tickets' do
        should_not_create_ticket(mail, account: account)
      end
    end

    describe "when it's an open helpdesk" do
      before do
        account.is_open = true
        account.save!
      end

      it 'creates the ticket and the user' do
        assert_difference('User.count(:all)') do
          ticket = should_create_ticket(mail)
          assert_equal 'irma@aghassipour.com', ticket.requester.email
        end
      end
    end
  end

  describe 'properties parsing' do
    let(:from_address) { Zendesk::Mail::Address.new(address: 'minimum_agent@aghassipour.com') }

    it 'supports tags' do
      mail.subject = 'Hello {{tags:0300}}'

      ticket = should_create_ticket(mail)

      assert_equal '0300', ticket.current_tags
    end

    it 'supports status' do
      mail.subject = 'Hello {{status:pending}}'

      ticket = should_create_ticket(mail)

      assert_equal StatusType.PENDING, ticket.status_id
    end

    it 'supports priority' do
      mail.subject = 'Hello {{priority:high}}'

      ticket = should_create_ticket(mail)

      assert_equal PriorityType.HIGH, ticket.priority_id
    end

    it 'supports assignee by name' do
      mail.subject = 'Hello {{assignee:"Agent Minimum"}}'

      ticket = should_create_ticket(mail)

      assert_equal users(:minimum_agent), ticket.assignee
    end

    it 'supports assignee by email' do
      mail.subject = 'Hello {{assignee:minimum_agent@aghassipour.com}}'

      ticket = should_create_ticket(mail)

      assert_equal users(:minimum_agent), ticket.assignee
    end

    it 'supports assignee by id' do
      mail.subject = "Hello {{assignee:#{users(:minimum_agent).id}}}"

      ticket = should_create_ticket(mail)

      assert_equal users(:minimum_agent), ticket.assignee
    end

    it 'supports requester' do
      mail.subject = 'Hello {{requester:hans.mortensen@aghassipour.com requester_name:"Morten Primdahl"}}'

      ticket = should_create_ticket(mail)

      assert_equal 'hans.mortensen@aghassipour.com', ticket.requester.email
      assert_equal 'Morten Primdahl', ticket.requester.name
    end

    it 'sets the organization of the requester' do
      requester     = users(:minimum_end_user)
      agent         = users(:minimum_agent)
      organization1 = organizations(:minimum_organization1)
      organization2 = organizations(:minimum_organization2)

      agent.update_attribute(:organization, organization1)
      requester.update_attribute(:organization, organization2)

      stub_mail_from Zendesk::Mail::Address.new(address: agent.email)
      mail.subject = "Hello {{requester:#{requester.email}}}"

      ticket = should_create_ticket(mail)

      assert_equal requester.email, ticket.requester.email
      assert_equal requester.organization, ticket.organization
    end

    describe 'an account with groups' do
      let(:mail) { FixtureHelper::Mail.read('with_group_ticket.json') }
      let(:account) { accounts(:with_groups) }

      it 'supports group' do
        mail.subject = 'Hello {{group:with_groups_1}}'

        ticket = should_create_ticket(mail)

        assert_equal groups(:with_groups_group1), ticket.group
      end
    end

    it "works with replies" do
      mail.subject = "re: #1 test #{tickets(:minimum_1).encoded_id} {{priority:high requester_name:Mikkel}}"

      ticket = should_add_comment_to_ticket(mail)

      assert_equal 'High', ticket.priority
      assert_equal 'Mikkel', ticket.requester.name
    end
  end

  describe 'taking an unusually long time' do
    it 'timeouts when the message can be suspended as unprocessable' do
      Timeout.expects(:timeout)
      process_mail(mail, account: account)
    end

    it "does not timeout when the message can't be suspended as unprocessable" do
      Timeout.expects(:timeout).never

      assert_raises ActiveRecord::RecordNotFound do
        process_mail(mail, account: nil)
      end
    end
  end

  it 'has blacklist rejection work with mails suspended due to loops' do
    account.update_attributes!(domain_blacklist: 'reject:minimum_end_user@aghassipour.com')
    state = process_mail(mail, account: account)

    assert state.rejected?
  end

  it 'has loop rejection work with mails suspended due to blacklisting' do
    account.update_attributes!(domain_blacklist: 'suspend:minimum_end_user@aghassipour.com')
    Zendesk::InboundMail::RateLimit.any_instance.expects(:greatly_exceeded?).returns true
    state = process_mail(mail, account: account)

    assert state.rejected?
  end

  it 'deletes all remote files associated with the message on rejection' do
    mail.headers['X-Spam-Zendesk-Score'] = ['1000']
    mail.expects(:delete_remote_files)
    process_mail(mail, account: account)
  end

  it 'gives whitelists higher precedence than blacklists' do
    account.update_attributes!(
      domain_blacklist: 'minimum_end_user@aghassipour.com',
      domain_whitelist: 'aghassipour.com'
    )

    Zendesk::InboundMail::ProcessingState.any_instance.expects(:from).times(1..10).returns(stub(address: 'minimum_end_user@aghassipour.com'))

    state = Zendesk::InboundMail::ProcessingState.new(account)

    Zendesk::InboundMail::Processors::BlacklistProcessor.execute(state, mail)
    assert_equal state.suspension, SuspensionType.BLOCKLISTED

    # Test that the whitelist supercedes the blacklist
    state = Zendesk::InboundMail::ProcessingState.new(account)
    state.sender_auth_passed = true

    Zendesk::InboundMail::Processors::WhitelistProcessor.execute(state, mail)
    Zendesk::InboundMail::Processors::BlacklistProcessor.execute(state, mail)
    assert_nil state.suspension

    # Test that the whitelist is superceded by blacklist with block: modifier
    account.domain_blacklist = 'block:minimum_end_user@aghassipour.com'
    state = Zendesk::InboundMail::ProcessingState.new(account)

    Zendesk::InboundMail::Processors::WhitelistProcessor.execute(state, mail)
    Zendesk::InboundMail::Processors::BlacklistProcessor.execute(state, mail)
    assert_equal state.suspension, SuspensionType.BLOCKLISTED
  end

  describe 'emails with recipients' do
    describe_with_arturo_enabled :email_ticket_email_ccs_suspension_threshold do
      let(:mail) { FixtureHelper::Mail.read('exceeds_account_configured_recipients.json') }
      let(:mail_recipients) { mail.recipients.count }

      before do
        account.ticket_email_ccs_suspension_threshold = ticket_email_ccs_suspension_threshold
        account.save!
      end

      describe 'when the ticket author is not an agent' do
        before do
          @state = process_mail(mail, account: account)
          @ticket = @state.ticket
        end

        describe 'when amount of email recipients is greater than the account-configured threshold' do
          let(:ticket_email_ccs_suspension_threshold) { mail_recipients - 1 }

          it "does not suspends the ticket" do
            assert_instance_of Ticket, @ticket
            assert_nil @state.suspension
          end
        end

        describe 'when amount of email recipients does not exceed the account-configured threshold' do
          let(:ticket_email_ccs_suspension_threshold) { mail_recipients + 1 }

          it 'does not suspend the ticket' do
            assert_nil @state.suspension
          end
        end

        describe 'when amount of email recipients is equal to the account-configured threshold' do
          let(:ticket_email_ccs_suspension_threshold) { mail_recipients }

          it 'does not suspend the ticket' do
            assert_nil @state.suspension
          end
        end
      end

      describe 'when the ticket author is an agent' do
        before do
          stub_mail_from Zendesk::Mail::Address.new(address: users(:minimum_agent).email)
          @state = process_mail(mail, account: account)
          @ticket = @state.ticket
        end

        describe 'when amount of email recipients is greater than the account-configured threshold' do
          let(:ticket_email_ccs_suspension_threshold) { mail_recipients - 1 }

          it 'does not suspend the ticket' do
            assert_nil @state.suspension
          end
        end
      end
    end

    describe_with_arturo_disabled :email_ticket_email_ccs_suspension_threshold do
      it 'has an account default threshold of 48' do
        assert_equal account.ticket_email_ccs_suspension_threshold, 48
      end

      describe 'when amount of email recipients is greater than account default threshold' do
        it 'does not suspend the ticket' do
          mail = FixtureHelper::Mail.read('mail_with_100_ccs.json')
          state = process_mail(mail, account: account)
          assert_nil state.suspension
        end
      end
    end
  end

  describe 'attempt to update a closed ticket' do
    let(:ticket) { tickets(:minimum_5) }

    before do
      assert ticket.status?(:closed)
      mail.to[0].address.sub!(/@/, "+id#{ticket.nice_id}@")
    end

    describe 'with authorized person' do
      let(:from_address) { Zendesk::Mail::Address.new(address: ticket.requester.email) }

      it 'creates a new ticket as a follow up to closed ticket' do
        followup_ticket = process_mail(mail, account: account).ticket
        assert followup_ticket
        assert_not_equal followup_ticket, ticket
        refute followup_ticket.new_record?
        assert followup_ticket.via?(:closed_ticket)
        assert_equal ticket, followup_ticket.followup_source
      end
    end

    describe 'with unrelated/unauthorized person' do
      before { User.any_instance.stubs(:can?).returns(false) }

      it 'creates a suspended ticket' do
        assert_difference('SuspendedTicket.count(:all)', 1) do
          process_mail(mail, account: account)
        end
      end
    end
  end

  describe 'make sure gmail confirmation code processing plays nicely' do
    let(:mail) { FixtureHelper::Mail.read('gmail_forwarding_verification.json') }
    let(:from_address) { Zendesk::Mail::Address.new(name: 'Google No Reply', address: 'forwarding-noreply@google.com') }

    it 'creates ticket that is not suspended' do
      ticket = process_mail(mail, account: account).ticket
      assert_instance_of Ticket, ticket
      assert_equal 'noreply@zendesk.com', ticket.requester.email
    end

    it "creates a suspended ticket when subject doesn't match" do
      mail.subject = 'random subject same sender'
      ticket = process_mail(mail, account: account).ticket
      assert_instance_of SuspendedTicket, ticket
    end
  end

  describe 'a mail delivery failure from an existing ticket and unknown sender' do
    let(:from_address) { Zendesk::Mail::Address.new(address: 'delivery@example.com') }

    before do
      account.domain_blacklist = nil
      account.save!

      mail.subject = 'Mail delivery failed: returning message to sender #1'
    end

    it 'does not blow up' do
      Zendesk::InboundMail::Unprocessable.expects(:register).never
      process_mail(mail, account: account)
    end

    it 'suspends as auto delivery failure' do
      ticket = process_mail(mail, account: account).ticket

      assert_instance_of SuspendedTicket, ticket
      assert_equal SuspensionType.AUTO_DELIVERY_FAILURE, ticket.cause, "Was: #{SuspensionType.to_s(ticket.cause)}"
    end
  end

  describe 'messages from users with multiple dots in their email' do
    let(:from_address) { Zendesk::Mail::Address.new(address: 'minimum....end_user@aghassipour.com') }

    it 'works' do
      state = process_mail(mail, account: account)
      assert state.author
      assert state.author.identities.present?
      assert_equal 'minimum....end_user@aghassipour.com', state.author.identities.first.value
      assert state.ticket
      assert state.ticket.id
    end
  end

  describe 'when emails have trailing dots' do
    let(:from_address) { Zendesk::Mail::Address.new(address: 'minimum_end_user.@aghassipour.com') }

    it 'accepts them, sadly' do
      state = process_mail(mail, account: account)
      assert state.author
      assert state.author.identities.present?
      assert_equal 'minimum_end_user.@aghassipour.com', state.author.identities.first.value
      assert state.ticket
      assert state.ticket.id
    end
  end

  it 'adds an event if requester language is detected' do
    account.save!
    mail.body = 'This is some text that should be detected as the default language.'

    state = process_mail(mail, account: account)

    locale_event = state.ticket.audits.last.events.to_a.find do |e|
      e.is_a?(Create) && e.value_reference == 'locale_id'
    end

    assert locale_event
    assert_equal ENGLISH_BY_ZENDESK.id.to_s, locale_event.value.to_s
    assert locale_event.value_previous.blank?
  end

  describe 'with email addresses creating a mapping to organization groups' do
    let(:from_address) { Zendesk::Mail::Address.new(address: 'hola@example.com') }

    before do
      group         = account.groups.create!(name: 'Tequila', is_active: true)
      @organization = account.organizations.create!(domain_names: 'example.com', name: 'Examples R Us', group: group)
    end

    it "uses new user's organization group with new tickets" do
      state = process_mail(mail, account: account)

      assert_instance_of Ticket, state.ticket
      assert_equal @organization, state.ticket.requester.organization
      assert_equal @organization.group, state.ticket.group
    end

    it "uses existing user's organization group with new tickets" do
      account.users.create!(name: 'Teddy Pendergrass', email: from_address.address, organization: @organization)

      state = process_mail(mail, account: account)

      assert_instance_of Ticket, state.ticket
      assert_equal @organization, state.ticket.requester.organization
      assert_equal @organization.group, state.ticket.group
    end
  end

  describe 'suspended tickets' do
    describe 'with attachments' do
      let(:mail) { FixtureHelper::Mail.read('missing_events_2.json') }

      before do
        Zendesk::InboundMail::Processors::SpamProcessor.any_instance.expects(:rspamd_probably_spam?).returns(true)
      end

      it 'adds the attachments to the suspended ticket' do
        state = process_mail(mail, account: account)

        assert_instance_of SuspendedTicket, state.ticket
        state.ticket.attachments.count(:all).must_equal 3
        state.ticket.attachments.each do |attachment|
          attachment.source_type.must_equal state.ticket.class.name
          attachment.source_id.must_equal state.ticket.id
          attachment.account.must_equal account
        end
      end

      it 'adds ATTACHMENT_TOO_BIG flags' do
        Attachment.any_instance.stubs(:valid?).returns(false)
        Attachment.any_instance.stubs(:valid_size?).returns(false)

        state = process_mail(mail, account: account)

        assert_instance_of SuspendedTicket, state.ticket
        state.ticket.attachments.count(:all).must_equal 0
        assert state.ticket.flags.include? EventFlagType.ATTACHMENT_TOO_BIG
      end

      describe 'from unknown users' do
        let(:from_address) { Zendesk::Mail::Address.new(address: 'spammer@example.com') }

        it 'does not create any user' do
          refute_difference 'User.count(:all)' do
            s = process_mail(mail, account: account)
            assert_instance_of SuspendedTicket, s.ticket
            s.ticket.attachments.count(:all).must_equal 3
          end
        end
      end
    end

    describe 'from unknown users' do
      let(:from_address) { Zendesk::Mail::Address.new(address: 'spammer@example.com') }

      before do
        Zendesk::InboundMail::Processors::SpamProcessor.any_instance.expects(:rspamd_probably_spam?).returns(true)
      end

      it 'does not create any user' do
        refute_difference 'User.count(:all)' do
          process_mail(mail, account: account)
        end
      end

      it 'avoids mail loops by not emailing signup notifications to automated systems' do
        account.update_attribute(:is_signup_required, true)
        mail.subject << 'Out of Office'

        refute_difference('ActionMailer::Base.deliveries.size') do
          ActionMailer::Base.perform_deliveries = true
          process_mail(mail, account: account)
        end
      end
    end

    describe 'updating existing tickets' do
      before do
        ticket = process_mail(mail, account: account).ticket
        assert ticket.audits.first.client.present?

        mail.in_reply_to = mail.message_id
        mail.message_id  = Zendesk::Mail::InboundMessage.generate_message_id
        mail.subject << ' and out of office'
      end

      it 'retains the client information' do
        assert_difference('SuspendedTicket.count(:all)', 1) do
          suspended_ticket = process_mail(mail, account: account).ticket
          assert_instance_of SuspendedTicket, suspended_ticket
          assert suspended_ticket.metadata.present?
        end
      end
    end

    describe 'when sent to a noreply address' do
      it 'is suspended' do
        mail.subject = 'No ticket id'
        state = process_mail(mail, account: account, recipient: "noreply@#{account.default_host}")
        assert_instance_of SuspendedTicket, state.ticket
        assert_equal SuspensionType.TO_NOREPLY, state.ticket.cause
      end
    end
  end

  describe 'forwarded tickets' do
    let(:mail) { FixtureHelper::Mail.read('forwards/apple_mail/apple_mail.json') }

    before do
      mail.to = Zendesk::Mail::Address.new(address: 'support@minimum.localhost')
    end

    describe 'agent' do
      let(:expected_description) { "Thoughts?: <https://support.zendesk.com/hc/en-us>\n\n\nWalt" }
      let(:from_address) { agent_email }

      before do
        account.settings.enable(:agent_forwardable_emails)
        account.save!
      end

      describe "with CCs/Followers settings disabled" do
        before do
          CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
        end

        it 'allows collaboration with unknown users if collaboration enabled but only for agents' do
          Account.any_instance.stubs(:is_collaboration_enabled?).returns(true)
          Account.any_instance.stubs(:is_collaborators_addable_only_by_agents?).returns(true)
          mail.cc = [
            Zendesk::Mail::Address.new(address: 'joemama@example.com'),
            Zendesk::Mail::Address.new(address: 'totally.new.guy@example.com')
          ]

          state = process_mail(mail, account: account)

          ticket = state.ticket
          assert ticket, state.inspect
          assert_nil state.suspension
          assert(ticket.valid?)

          collaborators = ticket.collaborators.map(&:email).sort
          assert_equal ['joemama@example.com', 'support@minimum.localhost', 'totally.new.guy@example.com'], collaborators
        end
      end

      describe "with CCs/Followers settings enabled" do
        before do
          CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
        end

        it 'allows collaboration with unknown users if CCs is enabled' do
          mail.cc = [
            Zendesk::Mail::Address.new(address: 'joemama@example.com'),
            Zendesk::Mail::Address.new(address: 'totally.new.guy@example.com')
          ]

          state = process_mail(mail, account: account)

          ticket = state.ticket
          assert ticket, state.inspect
          assert_nil state.suspension
          assert ticket.valid?

          collaborators = ticket.collaborators.map(&:email).sort
          assert_equal ['joemama@example.com', 'support@minimum.localhost', 'totally.new.guy@example.com'], collaborators
        end
      end

      it 'preserves agent forwarded processing with a suspended ticket' do
        Account.any_instance.stubs(:is_signup_required?).returns(true)
        Account.any_instance.stubs(has_email_rspamd?: true)

        mail.headers['X-Spam-Zendesk-Score'] = '65'

        state = process_mail(mail, account: account)

        assert state.ticket, state.inspect

        ticket = state.ticket

        assert_equal SuspensionType.SPAM, state.suspension
        assert(ticket.valid?, ticket.inspect)
        assert_equal false, ticket.new_record?
        assert_equal 'Thoughts?', ticket.subject
        assert_equal 'walt@example.com', ticket.from_mail
        assert_equal account.reply_address, ticket.original_recipient_address
        assert_equal "<https://support.zendesk.com/hc/en-us>\n\n\nWalt", ticket.content
      end

      it 'preserves agent forwarded ticket processing with a ticket' do
        state = process_mail(mail, account: account)

        assert_nil state.suspension
        assert state.ticket, state.inspect
        ticket = state.ticket

        assert(ticket.valid?, ticket.errors.inspect)
        assert_equal false, ticket.new_record?
        assert_equal account.owner, ticket.submitter
        assert_equal account.reply_address, ticket.original_recipient_address

        assert_equal 'walt@example.com', ticket.requester.email
        assert_equal expected_description, ticket.description
      end

      it 'allows properties to be set via the subject API' do
        api_assignee = account.owner
        mail.subject = "#{mail.subject} {{assignee:#{api_assignee.email} tags:\"melon mule\" requester_name:\"Morten Primdahl\"}}"
        Account.any_instance.stubs(:is_signup_required?).returns(false)

        state = process_mail(mail, account: account)

        assert_equal api_assignee, state.ticket.assignee
        assert_equal 'melon mule', state.ticket.current_tags
        assert_equal 'Morten Primdahl', state.ticket.requester.name
      end

      it 'is created when forwarding only an attachment' do
        # content may be added to the description due to inactive field subjects and inline attachments
        FieldSubject.any_instance.stubs(:is_active?).returns(true)
        body = "Hello\n\nBegin forwarded message:\n\n> From: \"Walt Whitman\" <walt@example.com>\n> Date: December 22, 2009 10:19:07 AM MST\n> To: \"'Morten Primd��hl'\" <mentor@example.com>\n> Subject: Thoughts?\n\n \n"
        mail.processing_state.stubs(full_plain_content: body)

        attachment = Zendesk::Mail::Part.new
        attachment.content_type = 'message/rfc822'
        attachment.file_name    = 'original.eml'
        attachment.body         = 'an email'

        mail.parts = [attachment]

        state  = process_mail(mail, account: account)
        ticket = state.ticket
        assert_equal '[No content]', ticket.description
        assert(ticket.valid?, ticket.errors.inspect)
        assert_equal false, ticket.new_record?
      end

      it 'preserves agent comments as private note' do
        state = process_mail(mail, account: account)
        ticket = state.ticket.reload
        assert_equal expected_description, ticket.description

        assert_equal 2, ticket.comments.size
        refute ticket.comments.last.is_public?
        assert_equal [
          expected_description,
          'Thoughts?'
        ], ticket.comments.map(&:body)
      end

      describe 'suspend and recover with agent comment' do
        let(:ticket) do
          state.ticket.author = users(:minimum_end_user)
          state.ticket.recover(users(:minimum_admin))
        end
        let(:state) { process_mail(mail, account: account) }

        before do
          mail.subject << 'Out of Office'
        end

        after do
          assert_equal [
            expected_description.sub('Thoughts?:', 'Thoughts?Out of Office:'),
            'Thoughts?'
          ], ticket.comments.map(&:body)
        end

        it 'recovers as private' do
          assert state.suspended?
          refute ticket.comments.last.is_public?
        end

        it 'recovers as public' do
          body = mail.processing_state.full_plain_content
          body[0...0] = "#public\n"
          mail.processing_state.stubs(full_plain_content: body)

          assert state.suspended?
          assert ticket.comments.last.is_public?
        end
      end
    end

    describe 'support_addresses' do
      let(:from_address) { Zendesk::Mail::Address.new(address: account.reply_address) }

      it 'is processes as coming from requester' do
        state = process_mail(mail, account: account)
        assert_equal 'walt@example.com', state.ticket.requester.email
      end
    end
  end

  describe "when suspension is present in state" do
    let(:suspension) { SuspensionType.CONTENT_PROCESSOR_TIMED_OUT }

    it "raises a Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout exception" do
      assert_raises Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout do
        process_mail(mail, account: account)
      end
    end
  end

  describe 'when account has ticket collaboration enabled and email CCs disabled' do
    describe 'but collaboration enabled not only for agents and new collaborator is invalid' do
      let(:minimum_ticket) { tickets(:minimum_1) }

      before do
        Account.any_instance.stubs(:is_collaboration_enabled?).returns(true)
        Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)
        Account.any_instance.stubs(:is_collaborators_addable_only_by_agents?).returns(false)
      end

      it 'flags the ticket' do
        should_create_ticket(mail) # minimum_1 ticket

        mail.subject    += "[#{minimum_ticket.encoded_id}]"
        mail.in_reply_to = minimum_ticket.generate_message_id
        mail.message_id  = Zendesk::Mail::InboundMessage.generate_message_id
        stub_mail_from Zendesk::Mail::Address.new(address: 'newenduser@minimum.localhost')

        process_mail(mail, account: account)
        refute minimum_ticket.reload.trusted?
      end
    end
  end

  describe 'when the account has ticket collaboration disabled and email CCs enabled' do
    describe 'but collaboration enabled not only for agents and new collaborator is invalid' do
      let(:from_address) { Zendesk::Mail::Address.new(address: 'newenduser@minimum.localhost') }
      let(:minimum_ticket) { tickets(:minimum_1) }

      before do
        Account.any_instance.stubs(:is_collaboration_enabled?).returns(false)
        Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
        Account.any_instance.stubs(:is_collaborators_addable_only_by_agents?).returns(false)
      end

      it 'flags the ticket' do
        should_create_ticket(mail) # minimum_1 ticket

        mail.subject    += "[#{minimum_ticket.encoded_id}]"
        mail.in_reply_to = minimum_ticket.generate_message_id
        mail.message_id  = Zendesk::Mail::InboundMessage.generate_message_id

        process_mail(mail, account: account)
        refute minimum_ticket.reload.trusted?
      end
    end
  end

  describe 'when CCs/Followers is enabled and new collaborator is invalid' do
    let(:from_address) { Zendesk::Mail::Address.new.tap { |address| address.address = 'newenduser@minimum.localhost' } }
    let(:minimum_ticket) { tickets(:minimum_1) }

    before { CollaborationSettingsTestHelper.enable_all_new_collaboration_settings }

    it 'flags the ticket' do
      mail.subject    += "[#{minimum_ticket.encoded_id}]"
      mail.in_reply_to = minimum_ticket.generate_message_id
      mail.message_id  = Zendesk::Mail::InboundMessage.generate_message_id

      process_mail(mail, account: account)
      refute minimum_ticket.reload.trusted?
    end
  end

  describe 'using the text API' do
    let(:mail) { FixtureHelper::Mail.read('text_api.json') }
    let(:ticket) { tickets(:minimum_1) }

    before do
      account.settings.enable(:agent_forwardable_emails)
      account.save!
      @group = FactoryBot.create(:group, account: account, name: 'smurfs')
      @group.users << users(:minimum_agent)

      Account.any_instance.stubs(:has_extended_ticket_types?).returns(true)
      Account.any_instance.stubs(:is_signup_required?).returns(false)
    end

    describe 'with a private ticket' do
      let(:from_address) { agent_email }

      before do
        ticket.attributes = { is_public: false }
        ticket.will_be_saved_by(account.owner)
        ticket.save

        mail.to          = Zendesk::Mail::Address.new(address: "support+id#{ticket.nice_id}@minimum.localhost")
        mail.subject     = 'Updating that thar ticket'
        mail.in_reply_to = ticket.generate_message_id

        process_mail(mail, account: account)
        ticket.reload
      end

      it 'defaults to private if the ticket is private' do
        refute ticket.comments.last.is_public?
      end
    end

    describe 'with a basic email' do
      let(:from_address) { agent_email }

      before do
        ticket.attributes = {assignee_id: nil}
        ticket.will_be_saved_by(account.owner)
        ticket.save

        mail.to          = Zendesk::Mail::Address.new(address: "support+id#{ticket.nice_id}@minimum.localhost")
        mail.subject     = 'Updating that thar ticket'
        mail.in_reply_to = ticket.generate_message_id

        process_mail(mail, account: account)
        ticket.reload
      end

      it 'allows setting the requester' do
        assert_equal users(:minimum_end_user), ticket.requester
      end

      it 'allows settings the assignee' do
        assert_equal users(:minimum_agent), ticket.assignee
      end

      it 'allows setting the group' do
        assert_equal @group, ticket.group
      end

      it 'allows setting the tags' do
        assert ticket.current_tags.include?('what')
        assert ticket.current_tags.include?('up')
      end

      it 'allows setting the type' do
        assert_equal 'Question', ticket.ticket_type
      end

      it 'allows setting the priority' do
        assert_equal 'High', ticket.priority
      end

      it 'allows setting the status' do
        assert_equal 'Pending', ticket.status
      end

      it 'allows setting the comment visibility' do
        refute ticket.comments.last.is_public?
      end

      it 'strips the tags from the comment left on the ticket' do
        assert_equal "This is a pretty basic email fixture.\n\nThis is using the text API.",
          ticket.comments.last.body
      end
    end

    describe 'an email without a comment' do
      let(:from_address) { agent_email }

      before do
        ticket.attributes = {assignee_id: nil}
        ticket.will_be_saved_by(account.owner)
        ticket.save

        mail.body = "#tags what up\n\n \n"
        mail.processing_state.stubs(plain_content: mail.body)
        mail.to = Zendesk::Mail::Address.new(address: "support+id#{ticket.nice_id}@minimum.localhost")
        mail.subject = 'Updating that thar ticket'
        mail.in_reply_to = ticket.generate_message_id
      end

      describe_with_arturo_enabled :email_hard_reject_text_api_with_no_content do
        it 'does not create a comment' do
          assert_no_difference('Comment.count(:all)') do
            process_mail(mail, account: account)
          end
        end

        it 'does not allow setting properties via text commands' do
          refute ticket.current_tags.include?('what')
          refute ticket.current_tags.include?('up')
        end
      end

      describe_with_arturo_disabled :email_hard_reject_text_api_with_no_content do
        before do
          assert_difference('Comment.count(:all)') do
            process_mail(mail, account: account)
          end
          ticket.reload
        end

        it 'allows setting properties via text commands' do
          assert ticket.current_tags.include?('what')
          assert ticket.current_tags.include?('up')
        end

        it 'strips the tags' do
          assert_equal '[No content]', ticket.comments.last.body
          refute                       ticket.comments.last.is_public?
        end
      end
    end

    describe "with a forwarded email from an agent that cannot comment publicly" do
      let(:mail) { FixtureHelper::Mail.read('forwards/with_text_api.json') }
      let(:from_address) { agent_email }

      before do
        mail.to = Zendesk::Mail::Address.new(address: 'support@minimum.localhost')
      end

      subject { process_mail(mail, account: account).ticket }

      it 'allows setting the requester' do
        assert_equal users(:minimum_end_user), subject.requester
      end

      it 'sets the requester as the author of the comment' do
        assert_equal users(:minimum_end_user), subject.comments.first.author
      end

      it 'allows settings the assignee' do
        assert_equal users(:minimum_agent), subject.assignee
      end

      it 'allows setting the group' do
        assert_equal @group, subject.group
      end

      it 'allows setting the tags' do
        assert subject.current_tags.include?('what')
        assert subject.current_tags.include?('up')
      end

      it 'allows setting the type' do
        assert_equal 'Question', subject.ticket_type
      end

      it 'allows setting the priority' do
        assert_equal 'High', subject.priority
      end

      it 'allows setting the status' do
        assert_equal 'Pending', subject.status
      end

      it 'does not allow setting the visibility of the first comment' do
        assert subject.comments.first.is_public?
      end

      it 'strips the tags from the comment left on the ticket' do
        assert_equal [
          "Thoughts?: Good Morning! :)\n\nAfoot and light-hearted I take to the open road,\nHealthy, free, the world before me,\nThe long brown path before me leading wherever I choose.\n\nHenceforth I ask not good-fortune, I myself am good-fortune,\nHenceforth I whimper no more, postpone no more, need nothing,\nDone with indoor complaints, libraries, querulous criticisms,\nStrong and content I travel the open road.\n\n\nWalt Whitman\nPoet\nwalt@signature.example.com",
          'Should read this at some point.'
        ], subject.comments.map(&:body)
      end
    end

    describe 'with an email referencing a different ticket from an agent' do
      let(:mail) { FixtureHelper::Mail.read('text_api.json') }
      let(:minimum_ticket_2) { tickets(:minimum_2) }
      let(:from_address) { Zendesk::Mail::Address.new(address: minimum_ticket_2.requester.email) }

      it 'allows setting ticket' do
        stripped_body = "Just an update from a different email...\n"
        mail.body = "#id #{minimum_ticket_2.nice_id}\n\n #{stripped_body}"
        mail.processing_state.stubs(plain_content: stripped_body)
        processed_ticket = process_mail(mail, account: account).ticket

        assert_equal 'Just an update from a different email...', processed_ticket.reload.comments.last.body
      end
    end
  end

  describe 'followers' do
    let(:mail) { FixtureHelper::Mail.read('cc_with_envelope.json') }
    let(:state) { process_mail(mail, account: account) }
    let(:ticket) { state.ticket }

    it "ignores CCs if they're from an invalid domain" do
      assert_equal 1, ticket.followers.size # One cc is from an invalid domain
    end

    describe 'when receiving an email from a new user and new CC' do
      let(:mail) { FixtureHelper::Mail.read('problem_cc.json') }

      it 'adds the CC as a collaborator and the sender as requester' do
        assert_equal 1, ticket.followers.size
        assert_equal false, ticket.collaborations.first.user.new_record?
        assert_equal 'f972f59cc68@example.com', ticket.followers.first.email
      end
    end

    describe 'when receiving duplicate email addresses in the CC header' do
      let(:mail) { FixtureHelper::Mail.read('problem_cc.json') }

      it 'only counts each unique address as a collaborator' do
        mail.cc = mail.cc * 2

        assert_equal 1, ticket.followers.size
      end
    end

    describe 'with blacklists and whitelists configured' do
      before do
        account.domain_blacklist = '*'
        account.domain_whitelist = 'mail.ru gmail.com decisionresearch.net noreply@site24x7.com aghassipour.com pmantrvel.com'
        account.cc_blacklist     = 'gurli@gmail.com erna@gmail.com'
        account.save!
      end

      describe 'and signup required' do
        before do
          account.update_attributes!(is_signup_required: true)
        end

        it 'adds all users as followers' do
          # Only Admin Man. Unknown CC's not allowed and from Cc not allowed
          mail.to = [
            Zendesk::Mail::Address.new(address: 'mus@gmail.com'),
            Zendesk::Mail::Address.new(address: 'support@minimum.localhost')
          ]
          mail.cc = [
            Zendesk::Mail::Address.new(address: 'minimum_admin@aghassipour.com'),
            Zendesk::Mail::Address.new(address: 'mads@gmail.com', name: 'Mads Madsen'),
            Zendesk::Mail::Address.new(address: 'minimum_end_user@aghassipour.com'),
            Zendesk::Mail::Address.new(address: 'minimum_end_user2@aghassipour.com'),
            Zendesk::Mail::Address.new(address: 'minimum_end_user3@aghassipour.com'),
            Zendesk::Mail::Address.new(address: 'minimum_end_user4@aghassipour.com'),
            Zendesk::Mail::Address.new(address: 'minimum_end_user5@aghassipour.com'),
            Zendesk::Mail::Address.new(address: 'minimum_end_user6@aghassipour.com'),
            Zendesk::Mail::Address.new(address: 'minimum_end_user7@aghassipour.com'),
            Zendesk::Mail::Address.new(address: 'minimum_end_user8@aghassipour.com'),
            Zendesk::Mail::Address.new(address: 'minimum_end_user9@aghassipour.com'),
            Zendesk::Mail::Address.new(address: 'minimum_end_user10@aghassipour.com')
          ]
          mail.subject = 'Hello'

          assert_equal 12, ticket.followers.size
          assert_equal 'minimum_admin@aghassipour.com', ticket.followers.first.email
          assert_equal 'mus@gmail.com support@minimum.localhost minimum_admin@aghassipour.com mads@gmail.com minimum_end_user@aghassipour.com minimum_end_user2@aghassipour.com minimum_end_user3@aghassipour.com minimum_end_user4@aghassipour.com minimum_end_user5@aghassipour.com minimum_end_user6@aghassipour.com minimum_end_user7@aghassipour.com minimum_end_user8@aghassipour.com minimum_end_user9@aghassipour.com minimum_end_user10@aghassipour.com'.split(' ').sort, ticket.recipients_for_latest_mail.split(' ').sort
          assert_equal ticket.recipients_for_latest_mail, ticket.audits.last.recipients
        end
      end

      describe 'and signup not required' do
        before do
          account.update_attribute(:is_signup_required, false)
        end

        it 'adds recipients who are on the whitelist as followers' do
          ActionMailer::Base.perform_deliveries = true
          # Scenario step 1: A new ticket gets created
          mail.to = [
            Zendesk::Mail::Address.new(address: 'mus@gmail.com'),
            Zendesk::Mail::Address.new(address: 'support@minimum.localhost')
          ]
          mail.cc = [
            Zendesk::Mail::Address.new(address: 'minimum_admin@aghassipour.com'),
            Zendesk::Mail::Address.new(address: 'mads@ag.com',     name: 'Mads Madsen'),
            Zendesk::Mail::Address.new(address: 'gurli@gmail.com', name: 'Gurli K')
          ]
          mail.subject = 'Hello'
          mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id

          # Original inbound email:
          #
          # Subject: "Hello"
          # From: EndUser "minimum_end_user@aghassipour.com"
          # To:
          #   Support "support@minimum.localhost"
          #   Mus "mus@gmail.com"
          # CC:
          #   Admin "minimum_admin@aghassipour.com"
          #   Mads "mads@ag.com"
          #   Gurli "gurli@gmail.com"

          assert_equal 2, ticket.followers.size
          assert_equal 'Admin Man', ticket.followers[0].name
          assert_equal 'Mus', ticket.followers[1].name

          # 2 emails got sent: one group notification, one "request received"
          # No cc notifications as they are already on the mail TO: or CC:
          # No welcome email, as CC's never receive welcome emails.
          #
          # 1) Subject: "Ticket assigned to Group"
          # From: Support "support@minimum.localhost"
          # To:
          #   Agent Minimum "minimum_agent@aghassipour.com"
          #   Admin "minimum_admin@aghassipour.com"
          #
          # 2) Subject: "Request received"
          # From: Support "support@minimum.localhost"
          # To: EndUser "minimum_end_user@aghassipour.com"
          assert_equal 2, ActionMailer::Base.deliveries.size

          # Scenario step 2: Requester replies with new collaborator added
          mail = FixtureHelper::Mail.read('minimum_ticket.json')
          mail.cc = [
            Zendesk::Mail::Address.new(address: 'ole@aghassipour.com', name: 'Ole Olsen'),
            Zendesk::Mail::Address.new(address: 'mads@ag.com',         name: 'Mads Madsen'),
            Zendesk::Mail::Address.new(address: 'mus@gmail.com')
          ]
          mail.subject = 're: ticket'
          mail.to[0].address.sub!(/@/, "+id#{ticket.nice_id}@")
          mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id
          stub_mail_from mail.sender

          ticket = should_add_comment_to_ticket(mail)
          assert_equal 3, ticket.followers.size # Mus, Admin and Ole

          # CC notification to Admin. Not Ole and mus, as they are on the mail TO: or CC:
          assert_equal 3, ActionMailer::Base.deliveries.size
          assert_equal users(:minimum_admin).email_address_with_name_without_quotes, ActionMailer::Base.deliveries.last['cc'].to_s

          # Scenario step 3: A collaborator replies
          mail = FixtureHelper::Mail.read('minimum_ticket.json')
          stub_mail_from Zendesk::Mail::Address.new(name: 'Ole Olsen', address: 'ole@aghassipour.com')
          mail.subject = 're: ticket'
          mail.to[0].address.sub!(/@/, "+id#{ticket.nice_id}@")
          mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id

          should_add_comment_to_ticket(mail)
          # 1 update to end-user and 2 cc notifications to admin and mus - not Ole!

          assert_equal 6, ActionMailer::Base.deliveries.size
        end
      end
    end

    describe 'when first email includes the requester and assignee inside the TO, CC headers' do
      let(:from_address) { Zendesk::Mail::Address.new(name: 'Requester', address: 'requester@example.com') }

      it 'excludes the double notification to the Requester' do
        ActionMailer::Base.perform_deliveries = true

        # Scenario step 1: A new ticket gets created
        mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id
        mail.subject    = 'New Ticket'
        mail.cc         = []
        mail.to         = [
          Zendesk::Mail::Address.new(name: 'Support', address: 'support@zdtestlocalhost.com'),
          Zendesk::Mail::Address.new(name: 'Collaborator', address: 'cc@example.com')
        ]

        # SUBJECT: New Ticket
        # FROM: requester@example.com
        # TO: support@zdtestlocalhost.com, cc@example.com
        original_ticket = process_mail(mail, account: account).ticket

        assert_equal 'requester@example.com', original_ticket.requester.email
        assert_equal 'cc@example.com', original_ticket.followers.first.email
        assert_equal 1, original_ticket.followers.size

        # Scenario step 2: Collaborator replies all including the Requester in TO:
        mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id
        mail.subject    = 'RE: New Ticket'
        mail.cc         = []
        mail.to         = [
          Zendesk::Mail::Address.new(name: 'Support', address: "support+id#{original_ticket.nice_id}@zdtestlocalhost.com"),
          Zendesk::Mail::Address.new(name: 'Requester', address: 'requester@example.com')
        ]
        stub_mail_from Zendesk::Mail::Address.new(name: 'Collaborator', address: 'cc@example.com')

        # SUBJECT: RE: New Ticket
        # FROM: cc@example.com
        # TO: support@zdtestlocalhost.com, requester@example.com
        new_ticket = should_add_comment_to_ticket(mail)

        assert_equal original_ticket.nice_id, new_ticket.nice_id
        assert_equal 'requester@example.com', new_ticket.requester.email
        assert_equal 'cc@example.com', new_ticket.followers.first.email
        assert_equal 2, new_ticket.followers.size

        # SUBJECT: Ticket assigned to minimum_group
        # FROM: support@zdtestlocalhost.com
        # TO: minimum_agent@aghassipour.com, minimum_admin@aghassipour.com
        # CC:

        # SUBJECT: New Ticket request received
        # FROM: support@zdtestlocalhost.com
        # TO: requester@example.com
        # CC:

        # This email get's rejected since Requester was a recipient from the email.
        #   SUBJECT: Collaborator has updated New Ticket
        #   FROM: support@zdtestlocalhost.com
        #   TO: requester@example.com
        #   CC:
        assert_equal 2, ActionMailer::Base.deliveries.size
      end

      it 'excludes the double notification to the Assignee' do
        ActionMailer::Base.perform_deliveries = true

        # Scenario step 1: A new ticket gets created.
        mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id
        mail.subject    = 'New Ticket'
        mail.cc         = []
        mail.to         = [
          Zendesk::Mail::Address.new(name: 'Support', address: 'support@zdtestlocalhost.com'),
          Zendesk::Mail::Address.new(name: 'Collaborator', address: 'cc@example.com')
        ]

        # SUBJECT: New Ticket
        # FROM: requester@example.com
        # TO: support@zdtestlocalhost.com, cc@example.com
        original_ticket = process_mail(mail, account: account).ticket

        # Assign Agent to this ticket
        agent = users(:minimum_agent)
        original_ticket.will_be_saved_by(agent)
        original_ticket.attributes = {assignee_id: agent.id}
        original_ticket.save!

        assert_equal agent.email, original_ticket.assignee.email
        assert_equal 'requester@example.com', original_ticket.requester.email
        assert_equal 'cc@example.com', original_ticket.followers.first.email
        assert_equal 1, original_ticket.followers.size

        # Scenario step 2: Collaborator replies all including the Assignee in TO:
        mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id
        stub_mail_from Zendesk::Mail::Address.new(name: 'Collaborator', address: 'cc@example.com')
        mail.subject    = 'RE: New Ticket'
        mail.cc         = []
        mail.to         = [
          Zendesk::Mail::Address.new(name: 'Support', address: "support+id#{original_ticket.nice_id}@zdtestlocalhost.com"),
          Zendesk::Mail::Address.new(name: 'Assignee', address: agent.email)
        ]

        # SUBJECT: RE: New Ticket
        # FROM: cc@example.com
        # TO: support@zdtestlocalhost.com, minimum_admin@aghassipour.com
        new_ticket = should_add_comment_to_ticket(mail)

        assert_equal original_ticket.nice_id, new_ticket.nice_id
        assert_equal agent.email, new_ticket.assignee.email
        assert_equal 'requester@example.com', new_ticket.requester.email
        assert_equal 2, new_ticket.followers.size
        assert new_ticket.followers.map(&:name).include?('Collaborator')
        assert new_ticket.followers.map(&:name).include?('Support')

        # SUBJECT: Ticket assigned to minimum_group
        # FROM: support@zdtestlocalhost.com
        # TO: minimum_agent@aghassipour.com, minimum_admin@aghassipour.com
        # CC:

        # SUBJECT: New Ticket request received
        # FROM: support@zdtestlocalhost.com
        # TO: requester@example.com
        # CC:

        # SUBJECT: Collaborator has updated New Ticket
        # FROM: support@zdtestlocalhost.com
        # TO: requester@example.com
        # CC:

        # This email don't include Assignee since he was in the original email.
        #   SUBJECT: Collaborator has updated New Ticket
        #   FROM: support@zdtestlocalhost.com
        #   TO: minimum_agent@aghassipour.com
        #   CC:
        assert_equal 3, ActionMailer::Base.deliveries.size
      end
    end

    describe 'when receiving email updates from an unknown user' do
      let(:mail) { FixtureHelper::Mail.read('basic.json') }
      let(:from_address) { Zendesk::Mail::Address.new(name: 'Argle Bargle', address: 'argle.bargle@example.org') }
      let!(:minimum_ticket) { tickets(:minimum_1) }

      before do
        mail.subject = 'Updating that thar ticket'
        mail.to[0].address.sub!(/@/, "+id#{minimum_ticket.nice_id}@")

        assert account.find_user_by_email(from_address.address).nil?
      end

      describe 'and In-Reply-To does not match a valid Message-Id' do
        before { mail.in_reply_to = Token.generate }

        it 'suspends the ticket and does not add the sender as a CC' do
          refute state.authorized?
          assert state.suspended?
          assert_equal SuspensionType.OTHER_USER_UPDATE, state.suspension
          assert minimum_ticket.reload.followers.detect { |user| user.email == from_address.address }.blank?
        end
      end

      describe 'and In-Reply-To matches a valid Message-Id' do
        before { mail.in_reply_to = minimum_ticket.generate_message_id }

        describe_with_arturo_enabled :email_make_other_user_comments_private do
          it 'updates the ticket with a private comment and does not add the sender as a CC' do
            assert state.authorized?
            refute state.suspended?
            assert ticket.comments.last.is_private?
            refute ticket.collaborators.any? { |user| user.email == from_address.address }
          end
        end

        describe_with_arturo_disabled :email_make_other_user_comments_private do
          it 'updates the ticket and adds the sender as a CC' do
            assert state.authorized?
            refute state.suspended?
            assert ticket.collaborators.any? { |user| user.email == from_address.address }
          end
        end

        it 'does not update the ticket comment author if the ticket is not new' do
          Zendesk::InboundMail::Processors::CommitProcessor.expects(:set_requester_as_author).never
          process_mail(mail, account: account)
        end

        describe 'and exceeding the CC limit' do
          # EM-1819: Questionable whether the following test inside the Arturo
          # enabled block is even necessary, as we'll never try to add more
          # users with an other user update now anyway.
          describe_with_arturo_enabled :email_make_other_user_comments_private do
            before do
              Zendesk::InboundMail::Ticketing::MailCollaboration.any_instance.expects(:existing_end_user_collaborators).never
            end

            it 'updates the ticket and does not add the sender as a CC' do
              assert state.authorized?
              refute state.suspended?
              assert ticket.collaborators.detect { |user| user.email == from_address.address }.blank?
            end
          end

          describe_with_arturo_disabled :email_make_other_user_comments_private do
            before do
              end_user_ccs = stub(size: 100)
              Zendesk::InboundMail::Ticketing::MailCollaboration.any_instance.expects(:existing_end_user_collaborators).at_least_once.returns(end_user_ccs)
            end

            it 'updates the ticket and does not add the sender as a CC' do
              assert state.authorized?
              refute state.suspended?
              assert ticket.collaborators.detect { |user| user.email == from_address.address }.blank?
            end
          end
        end

        describe 'in a potential loop' do
          describe_with_arturo_enabled :email_make_other_user_comments_private do
            before do
              Zendesk::InboundMail::Processors::CollaboratorProcessor.any_instance.stubs(suspend_for_netsuite_ccs?: true)
              Zendesk::InboundMail::Processors::CollaboratorProcessor.any_instance.stubs(current_user_is_netsuite?: true)
            end

            it 'suspends the ticket and does not add the sender as a CC' do
              assert state.authorized?
              assert state.suspended?
              assert_equal SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE, state.suspension
              assert minimum_ticket.reload.followers.detect { |user| user.email == from_address.address }.blank?
            end
          end

          describe_with_arturo_disabled :email_make_other_user_comments_private do
            before do
              Zendesk::InboundMail::Ticketing::MailCollaboration.any_instance.expects(:potential_netsuite_cc_loop?).at_least_once.returns(true)
            end

            it 'suspends the ticket and does not add the sender as a CC' do
              assert state.authorized?
              assert state.suspended?
              assert_equal SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE, state.suspension
              assert minimum_ticket.reload.followers.detect { |user| user.email == from_address.address }.blank?
            end
          end
        end

        describe 'but is some netsuite spam' do
          let(:from_address) { Zendesk::Mail::Address.new(name: 'Argle Bargle', address: 'random@messages.foo.netsuite.com') }

          before { Account.any_instance.expects(:has_allow_netsuite_ccs?).at_least_once.returns(false) }

          it 'suspends the ticket and does not add the sender as a CC' do
            assert state.authorized?
            assert state.suspended?
            assert_equal SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE, state.suspension
            assert minimum_ticket.reload.followers.detect { |user| user.email == from_address.address }.blank?
          end
        end

        describe 'from a mixed case address' do
          before { mail.from.address.upcase! }

          describe_with_arturo_enabled :email_make_other_user_comments_private do
            it 'updates the ticket with a private comment and does not add the sender as a CC' do
              assert state.authorized?
              refute state.suspended?
              assert ticket.comments.last.is_private?
              refute ticket.collaborators.any? { |user| user.email == mail.from.address.downcase }
            end
          end

          describe_with_arturo_disabled :email_make_other_user_comments_private do
            it 'updates the ticket and adds the sender as a CC' do
              assert state.authorized?
              refute state.suspended?
              assert ticket.comments.last.is_public?
              assert ticket.collaborators.any? { |user| user.email == mail.from.address.downcase }
            end
          end
        end

        describe 'when the account has ticket collaboration disabled and CCs/Followers settings disabled' do
          before do
            Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(false)
            account.update_attribute(:is_collaboration_enabled, false)
          end

          it 'adds a flagged comment' do
            count = minimum_ticket.audits.count(:all)

            assert state.authorized?
            refute state.suspended?
            refute state.submitter.can?(:add_follower, ticket)
            ticket.reload
            audit = ticket.audits.last

            assert_equal false, audit.trusted?
            assert_equal 'This is a pretty basic email fixture.', audit.comment.body
            assert_equal (count + 1), ticket.audits.count(:all)
            assert audit.flags.include? EventFlagType.OTHER_USER_UPDATE
          end
        end
      end

      describe 'flagged comment' do
        let(:encoded_refs) { ["<#{minimum_ticket.encoded_id.delete('-')}@#{Zendesk::Configuration.fetch(:host)}>"] }
        let(:original_body) { mail.body.dup.chomp }
        let(:body_with_encoded_id) { "[#{minimum_ticket.encoded_id}] #{original_body}" }
        let(:body_with_nice_id) { "#id #{minimum_ticket.nice_id}\n\n#{original_body}" }

        it 'adds an flagged comment when References contains a valid encoded ID' do
          mail.references = encoded_refs
          assert_untrusted_comment('This is a pretty basic email fixture.', minimum_ticket.reload)
        end

        it 'adds an flagged comment when the message body contains a valid encoded ID' do
          mail.processing_state.stubs(full_plain_content: original_body, plain_content: original_body)
          mail.body = body_with_encoded_id
          assert_untrusted_comment(original_body, minimum_ticket.reload)
        end

        it 'adds a suspended comment when the email is identified only by nice ID' do
          mail.processing_state.stubs(full_plain_content: original_body, plain_content: original_body)
          mail.body = body_with_nice_id
          assert_suspended_comment(original_body, minimum_ticket.reload)
        end

        describe 'when the user\'s email is a support address for a different Zendesk account' do
          describe 'when the user\'s email contains a plus id' do
            let(:from_address) do
              Zendesk::Mail::Address.new(
                name: "Other Zendesk Account",
                address: "support+id123456@other-account.zendesk-test.com"
              )
            end

            before do
              mail.body = body_with_encoded_id
              mail.headers["X-Zendesk-From-Account-Id"] = ["abc123"]
            end

            describe_with_arturo_enabled :email_make_other_user_comments_private do
              it 'creates a private comment, adds other_user_update and machine_generated flags, and does not update the ticket collaborators' do
                flags = ticket.reload.audits.last.flags

                assert state.authorized?
                refute state.suspended?
                assert state.comment?
                refute state.ticket.comments.last.is_public?
                assert_empty(ticket.collaborators)
                assert_equal Set.new([4, 2]), flags.to_set
              end
            end

            describe_with_arturo_disabled :email_make_other_user_comments_private do
              it 'creates a public comment, adds other_user_update and machine_generated flags, and adds the email\'s sender as a collaborator' do
                flags = ticket.reload.audits.last.flags

                assert state.authorized?
                refute state.suspended?
                assert state.comment?
                assert state.ticket.comments.last.is_public?
                assert ticket.collaborators.include?(state.author)
                assert_equal Set.new([4, 2]), flags.to_set
              end
            end
          end
        end
      end
    end
  end

  describe 'when processing looping emails' do
    before do
      # Necessary because the `minimum` account from the fixtures has `organization_whitelist`
      # "yahoo.com aghassipour.com svane.com test.com", and `mail.from.address` here
      # is "minimum_end_user@aghassipour.com".
      # account.stubs(:organization_whitelist).returns('')
      Account.any_instance.stubs(organization_whitelist: "")
    end

    it 'suspends looping mails' do
      Account.any_instance.expects(:loop_threshold).at_least_once.returns(10)
      Zendesk::InboundMail::Processors::WhitelistProcessor.any_instance.expects(:address_whitelisted?).at_least_once.returns(false)
      Account.any_instance.expects(:blacklists?).at_least_once.returns(false)

      process = lambda do
        mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id
        process_mail(mail, account: account)
      end

      # push it to the limit
      10.times do
        state = process.call
        refute state.whitelisted?
        refute state.suspended?
      end

      # suspended!
      state = process.call
      refute state.whitelisted?
      assert state.suspended?

      # some time has passed ...
      state.account.inbound_emails.each do |ie|
        ie.update_attribute(:created_at, 2.hours.ago.utc)
      end

      # everything is fine
      state = process.call
      refute state.whitelisted?
      refute state.suspended?
    end
  end

  describe 'converting html emails to zendesk comment markup' do
    let(:mail) { FixtureHelper::Mail.read('replies/gmail/date.json') }
    let(:state) { process_mail(mail, account: account) }

    before do
      account.settings.enable(:rich_content_in_emails)
      account.save!
    end

    describe 'with a multipart email' do
      let(:html_part) { mail.parts.detect { |p| p.mime_type == Mime[:html] } }
      let(:comment) { state.ticket.comments.last }

      before { html_part.body << '<script>alert(1)</script>' }

      it 'reads html part' do
        state.ticket.comments.size.must_equal 1
        comment.format.must_equal 'rich'
        comment.body.wont_include '<strong>'
        comment.html_body.must_include '<strong>'
        comment.html_body.wont_include '<script>'
        state.ticket.description.wont_include '<strong>' # plain text for preview
      end

      describe 'with inline attachments' do
        let(:inlined_attachment) { '<img src="https://minimum.zendesk-test.com/attachments/token/' }
        let(:original_url) { '<img src="http://minimum.zendesk-test.com/xxx.png"' }
        let(:inlined_url) { '<img src="https://d1s0i1st0s2szw.cloudfront.net/2f21c9ebe506aee0fc84061cee37a3b6a3aece3e?url=http%3A%2F%2Fminimum.zendesk-test.com%2Fxxx.png"' }
        let(:other_html) { '<span>FFF</span>' }

        before do
          mail.parts << Zendesk::Mail::Part.new.tap do |attachment|
            attachment.file_name    = 'test.txt'
            attachment.remote_url   = 'https://bucket.s3.amazonaws.com/test.txt'
            attachment.content_type = 'text/plain'
            attachment.headers['content-id'] = ['123']
          end
          Attachment.any_instance.stubs(copy_from_cloud_params: nil) # prevent s3 access
          Account.any_instance.stubs(:has_rtl_lang_html_attr?).returns(false)

          full_html_content = mail.processing_state.full_html_content
          full_html_content << '<span>FFF</span>'
          full_html_content << '<img src="cid:123" />'
          full_html_content << '<img src="http://minimum.zendesk-test.com/xxx.png" />'
          mail.processing_state.stubs(full_html_content: full_html_content, html_content: full_html_content)
        end

        it 'inlines attachments and does not proxy images as agent' do
          User.any_instance.stubs(is_agent?: true)
          body = comment.html_body
          body.must_include other_html
          body.must_include original_url
          body.must_include inlined_attachment
        end

        it 'inlines attachments and proxies images as enduser' do
          body = comment.html_body
          body.must_include other_html
          body.must_include inlined_url
          body.must_include inlined_attachment
        end

        it 'discards crazy doctypes and titles' do
          # TODO: content.rb will not return content if there is none - the test that was here was testing that. Will look into where the test should belong
          mail.processing_state.stubs(full_html_content: nil, html_content: nil, full_plain_content: nil, plain_content: nil)
          assert_nil comment.format
          comment.html_body.gsub(/[a-z]+=".*?"/, '').gsub(/\s+/, ' ').must_equal '<div ><p>Re: Thoughts?: [No content]</p></div>'
        end
      end

      describe 'when conversion to zendesk comment markup fails' do
        let(:text_body) { Zendesk::MtcMpqMigration::Content::MIME.new(mail).body }

        before do
          mail.processing_state.stubs(full_plain_content: text_body, plain_content: text_body)
          ZendeskCommentMarkup.stubs(:filter_input).raises(StandardError.new('blam'))
        end

        it "falls back to plain text" do
          state.ticket.comments.last.body.must_equal text_body
        end
      end
    end

    describe 'with an only html email' do
      let(:comment) { state.ticket.comments.last }

      before do
        mail.parts.shift
        html = mail.parts.shift
        mail.body = html.body
        mail.headers['content-type'] = 'text/html'
      end

      it 'reads html part' do
        state.ticket.comments.size.must_equal 1
        comment.format.must_equal 'rich'
        comment.body.wont_include '<strong>'
        comment.html_body.must_include '<strong>'
        comment.html_body.wont_include '<script>'
        state.ticket.description.wont_include '<strong>' # plain text for preview
      end
    end

    describe 'with an only plain text email' do
      let(:mail) { FixtureHelper::Mail.read('minimum_ticket.json') }

      it 'works as usual' do
        state.ticket.comments.size.must_equal 1
        state.ticket.comments.last.body.must_equal "#400 Hello there: Hello world!\r\nMicrosoft Word fails to print my document"
      end
    end

    it 'creates a suspended ticket that can be recovered as html' do
      mail.subject = 'OOO'
      suspended = state.ticket
      suspended.class.must_equal SuspendedTicket
      assert suspended.properties[:html]
      ticket = suspended.recover(users(:minimum_admin))
      comment = ticket.comments.last
      assert comment.rich?
      comment.html_body.must_include "<div class=\"zd-comment zd-comment-pre-styled\" dir=\"auto\">OOO: Can I use your tool also for german keywors and a forum with content in german language? How good is your content? Can I see a demo forum? <br><br>\n<div>2009/"
    end
  end

  describe 'with X-Zendesk-Disable-Triggers header' do
    it 'disables triggers' do
      mail = FixtureHelper::Mail.read('minimum_ticket.json') do |json|
        json['headers']['x-zendesk-disable-triggers'] = 'true'
      end
      state = process_mail(mail, account: account)
      assert_equal [true], state.ticket.audits.map(&:disable_triggers).uniq
    end

    it 'disables triggers on suspended tickets that get recovered' do
      mail = FixtureHelper::Mail.read('minimum_ticket.json') do |json|
        json['headers']['x-zendesk-disable-triggers'] = 'true'
        json['headers']['subject'] = 'Delivery Failure'
      end

      # was turned into a SuspendedTicket
      assert_difference 'SuspendedTicket.count(:all)', +1 do
        process_mail(mail, account: account)
      end

      # no triggers when recovering
      ticket = SuspendedTicket.last.recover(account.users.last, account.users.last)
      assert_equal [true], ticket.audits.map(&:disable_triggers).uniq
    end
  end

  describe 'with triggers on recipient' do
    let(:state) { process_mail(mail, account: account) }

    it 'applies a simple trigger' do
      rules(:trigger_set_tag_for_recipient).update_attribute('is_active', true)
      Account.any_instance.stubs(host_name: 'zdtestlocalhost.com')

      assert state.ticket.id
      state.ticket.reload
      ticket = state.ticket

      assert_equal 4, ticket.taggings.length
      assert_match /^support@/, ticket.recipient
      assert ticket.current_tags.index('recipient_detected')
    end

    describe 'automated_message' do
      describe 'public ticket' do
        let(:from_address) { Zendesk::Mail::Address.new(address: 'westernunionresponse@somdomain.com') }

        before { Notification.without_arsi.delete_all }

        it 'suspends and sends notifications on recovery' do
          assert state.suspended?

          assert_difference 'Notification.count(:all)', 3 do
            state.ticket.recover(users(:minimum_agent), users(:minimum_agent))
          end
        end
      end
    end
  end

  describe 'user processing for suspended tickets' do
    let(:mail) { FixtureHelper::Mail.read('forwards/apple_mail/apple_mail.json') }

    [
      {state_suspended: true,   existing_user: true,   set_author: true},
      {state_suspended: false,  existing_user: true,   set_author: true},
      {state_suspended: false,  existing_user: false,  set_author: true},
      {state_suspended: true,   existing_user: false,  set_author: false}
    ].each do |setup|
      describe "ticket is #{setup[:state_suspended] ? 'suspended' : 'not suspended'} and from #{setup[:existing_user] ? 'a pre-existing user' : 'a new user'}" do
        let(:user) { users(:minimum_end_user) }

        it(setup[:set_author] ? 'set the author' : 'not set the author') do
          stub_mail_from Zendesk::Mail::Address.new(address: user.email)
          UserIdentity.where(account_id: user.account_id, value: user.email).delete_all unless setup[:existing_user]
          mail.subject = mail.subject + 'Out of Office' if setup[:state_suspended]
          state = process_mail(mail, account: account)
          assert_equal state.author.present?, setup[:set_author]
        end
      end
    end
  end

  describe 'suspended? and flagged comment for a closed ticket' do
    let(:ticket) { tickets(:minimum_5) } # closed ticket

    it 'creates a new suspended ticket when nice ID is used' do
      assert ticket.status?(:closed)

      mail.to[0].address.sub!(/@/, "+id#{ticket.nice_id}@")
      state = process_mail(mail, account: account)
      state.stubs(:suspension).returns(SuspensionType.OTHER_USER_UPDATE)

      assert state.suspended?, 'should be suspended'
      assert_not_equal ticket.id, state.ticket.id
    end

    it 'creates a new suspended ticket when encoded ID is used' do
      assert ticket.status?(:closed)

      mail.to[0].address.sub!(/@/, "+id#{ticket.encoded_id}@")
      state = process_mail(mail, account: account)
      state.stubs(:suspension).returns(SuspensionType.OTHER_USER_UPDATE)

      assert state.suspended?, 'should be suspended'
      assert_not_equal ticket.id, state.ticket.id
    end
  end

  describe 'attachments' do
    it 'creates from embedded emails' do
      mail = FixtureHelper::Mail.read('embedded_emails.json')
      should_create_ticket_with_attachments(mail, 3, Mime[:eml].to_s)
    end

    it 'creates from root message of non-standard content type' do
      mail = FixtureHelper::Mail.read('content_type_pdf.json')
      should_create_ticket_with_attachments(mail, 1, 'application/pdf')
    end

    it 'adds a comment to the ticket' do
      should_create_ticket_with_attachments(FixtureHelper::Mail.read('plain_text_email_with_attachments.json'), 2)
      should_create_ticket_with_attachments(FixtureHelper::Mail.read('html_text_in_body_email_with_attachments.json'), 2)
      should_create_ticket_with_attachments(FixtureHelper::Mail.read('html_text_in_attachment_email_with_attachments.json'), 2)

      mail = FixtureHelper::Mail.read('html_text_in_attachment_email_with_attachments.json')
      mail.subject = "Re: #1 #{tickets(:minimum_1).encoded_id}"
      mail.message_id = Zendesk::Mail::InboundMessage.generate_message_id

      should_add_comment_to_ticket(mail)
    end

    describe 'that are over the size limit' do
      let(:mail) { FixtureHelper::Mail.read('plain_text_email_with_attachments.json') }

      before do
        Attachment.any_instance.stubs(:valid_size?).returns(false)
        @attachment = Zendesk::Mail::Part.new.tap do |attachment|
          attachment.file_name    = 'test.txt'
          attachment.body         = 'body'
          attachment.content_type = 'text/plain'
        end
        mail.parts << @attachment
      end

      it 'adds a translatable error' do
        state = process_mail(mail, account: account)
        assert state.ticket.audits.first.events.map(&:class).map(&:to_s).include?('TranslatableError')
      end
    end
  end

  describe "bad processing state" do
    before do
      @different_account = accounts(:support)
      refute_equal @different_account.id, account.id
      Zendesk::InboundMail::ProcessingState.any_instance.stubs(:account).returns(@different_account)
    end

    it "logs and exits" do
      mail.logger.expects(:error).with("ProcessingState's account#id: #{@different_account.id} does not match TicketProcessingJob's account_id: #{account.id}")
      assert_raises SystemExit do
        process_mail(mail, account: account)
      end
    end
  end

  describe "state initialization" do
    subject do
      Zendesk::InboundMail::TicketProcessingJob.new(
        mail: mail,
        account_id: account.id,
        route_id: account.routes.first.id,
        recipient: "support@#{account.subdomain}.zendesk-test.com"
      ).state
    end

    let(:address) { Zendesk::Mail::Address.new(name: "John", address: "john@example.com") }

    before do
      Zendesk::InboundMail::MailParsingQueue::ProcessingState.any_instance.stubs(
        from: address,
        from_header: address,
        suspension: SuspensionType.CONTENT_PROCESSOR_TIMED_OUT
      )
    end

    it "initializes from attribute" do
      subject.from.must_equal address
    end

    it "initializes from_header attribute" do
      subject.from_header.must_equal address
    end

    describe_with_arturo_disabled :email_mtc_add_from_header_state_variable do
      it "initializes from_header attribute" do
        assert_nil subject.from_header
      end
    end

    it "initializes suspension attribute" do
      subject.suspension.must_equal SuspensionType.CONTENT_PROCESSOR_TIMED_OUT
    end
  end

  def should_add_comment_to_ticket(mail)
    id = mail.to.first.address[/id(\d+)/, 1] || mail.subject[/#(\d+)/, 1]
    ticket = account.tickets.find_by_nice_id(id)

    assert ticket

    count = ticket.comments.count(:all)
    mail.subject += "[#{ticket.encoded_id}]"
    ticket = process_mail(mail, account: account).ticket
    ticket.reload
    assert_equal count + 1, ticket.comments.count(:all)

    assert_equal ticket.comments.last.via_id, Zendesk::Types::ViaType.MAIL
    refute ticket.comments.last.body.to_s.match(/This text should be stripped/m)

    ticket
  end

  def should_create_ticket(mail)
    assert_difference 'Ticket.count(:all)' do
      process_mail(mail, account: account)
    end

    ticket = Ticket.order('id DESC').first
    assert ticket

    valid_via_types_for_email = [Zendesk::Types::ViaType.MAIL, Zendesk::Types::ViaType.CLOSED_TICKET]

    assert valid_via_types_for_email.member?(ticket.via_id)
    assert valid_via_types_for_email.member?(ticket.comments.last.via_id)

    ticket
  end

  def should_not_create_ticket(mail, options = {})
    refute_difference('Ticket.count(:all)') { process_mail(mail, options) }
  end

  def should_create_ticket_with_attachments(mail, attachments_length, type = nil)
    ticket = should_create_ticket(mail)

    attachments = ticket.comments.last.attachments
    attachments = attachments.where(content_type: type) if type
    attachments.size.must_equal attachments_length

    ticket
  end

  def assert_untrusted_comment(body, ticket)
    count = ticket.audits.count(:all)
    state = process_mail(mail, account: account)

    assert state.authorized?
    refute state.suspended?

    audit = ticket.reload.audits.last

    refute audit.trusted?
    assert audit.flagged?
    assert_equal body, audit.comment.body
    assert_equal count + 1, ticket.audits.count(:all)
  end

  def assert_trusted_comment(body)
    count = ticket.audits.count(:all)
    state = process_mail(mail, account: account)

    assert state.authorized?
    refute state.suspended?
    assert_nil state.suspension

    audit = ticket.reload.audits.last

    assert audit.trusted?
    assert_equal body, audit.comment.body
    assert_equal count + 1, ticket.audits.count(:all)
  end

  def assert_suspended_comment(body, ticket)
    audits_count = ticket.audits.count(:all)
    suspensions_count = SuspendedTicket.count(:all)
    state = process_mail(mail, account: account)

    assert state.suspended?
    refute state.authorized?
    assert_equal SuspensionType.OTHER_USER_UPDATE, state.suspension

    ticket.reload

    assert_equal body, SuspendedTicket.last.content
    assert_equal ticket.audits.count(:all), audits_count
    assert_equal suspensions_count + 1, SuspendedTicket.count(:all)
  end
end
