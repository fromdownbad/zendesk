require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Chain do
  class MailChainTestProcessor
    def self.execute(state, _mail)
      state << name
    end
  end

  class MailChainTestProcessorOne < MailChainTestProcessor
  end

  class MailChainTestProcessorTwo < MailChainTestProcessor
  end

  def execute
    @chain.execute(state, mail)
  end

  let(:account) { accounts(:minimum) }
  let(:mail) { stub(logger: Rails.logger) }
  let(:state) { [] }

  before do
    @processors = [MailChainTestProcessorOne, MailChainTestProcessorTwo]
    @chain = Zendesk::InboundMail::Chain.new(*@processors)
    state.stubs(account: account)
  end

  it "has processors" do
    assert_equal @processors, @chain.processors
  end

  it "executes processors in order" do
    execute
    assert_equal %w[MailChainTestProcessorOne MailChainTestProcessorTwo], state
  end

  it "reports the processing time to DataDog" do
    statsd_client = Zendesk::StatsD::Client.any_instance
    statsd_client.expects(:time).with(:processors, tags: ['processor_name:mail_chain_test_processor_one']).once
    statsd_client.expects(:time).with(:processors, tags: ['processor_name:mail_chain_test_processor_two']).once

    execute
  end

  describe 'when there is an error' do
    before { MailChainTestProcessorOne.stubs(:execute).raises(error) }

    describe 'like Halt' do
      let(:error) { Zendesk::InboundMail::HardMailRejectException.new }

      it "handles the rejection and returns false" do
        Zendesk::InboundMail::FailureHandler::Rejection.expects(:handle).with(state, mail, error).once
        assert_equal false, execute
      end
    end

    describe 'like StandardError' do
      let(:error) { StandardError.new }

      it "logs the backtrace and re-raises the exception" do
        @chain.expects(:raise).once
        mail.logger.expects(:info).with('------------ MailChainTestProcessorOne started ------------').once
        error.stubs(:backtrace).returns(['/app/folder/file.rb'])

        mail.logger.expects(:warn).with("Unexpected exception: #{error.class} #{error.message}:").once
        mail.logger.expects(:warn).with(" * /app/folder/file.rb")

        execute
      end
    end
  end
end
