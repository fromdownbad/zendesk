require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 5

describe Zendesk::InboundMail::ValidationRule do
  fixtures :accounts

  let(:hard_rules) { Zendesk::InboundMail::ValidationRule.hard_rules }
  let(:soft_rules) { Zendesk::InboundMail::ValidationRule.soft_rules }
  let(:mail)       { Zendesk::Mail::InboundMessage.new }
  let(:account)    { accounts(:minimum) }

  def hard_rejected?(mail)
    hard_rules.any? { |rule| rule.deny?(mail, account) }
  end

  def soft_rejected?(mail)
    soft_rules.any? { |rule| rule.deny?(mail, account) }
  end

  [
    "Autoresponse",
    "Automatic reply",
    "Delivery Status Notification \(Failure\)",
    "Delivery Failure",
    "failure notice",
    "Returned mail",
    "Undeliverable mail",
    "Automaatne vastus"
  ].each do |subject|
    it "denies subject with typical mailer daemon message: #{subject}" do
      mail.subject = "Hello #{subject} elloH"
      assert hard_rejected?(mail), subject
    end
  end

  [
    "Undelivered Mail",
    "NO RESPONSE REQUIRED",
    "Mail Returned",
    "Automatic Response",
    "Samodejni odgovor",
    "NDN:",
  ].each do |subject|
    it "denies when subject starts with specific mailer daemon message: #{subject}" do
      mail.subject = "#{subject} Hello"
      assert hard_rejected?(mail), subject

      mail.subject = "Hello #{subject} Hello"
      refute hard_rejected?(mail), subject
    end
  end

  it "denies when sender contains an identified automated mailer" do
    senders = %w[automaticresponse@hello.com postmaster@example.org]
    senders.each do |sender|
      mail.from = sender
      assert hard_rejected?(mail), sender
    end
  end

  auto_vacation_subjects = ['OOO'] # 'OOO' is expected explicitly as the entire subject string
  auto_vacation_subjects += [
    "Ikke til stede: [Helpdesk] Re: Office 2007 installationen på adm. pc'er",
    'Autoreply: Request received!',
    'AUTO: Ding dong',
    'Absence du bureau',
    'Puhkusel',
    'Kontorist väljas',
    'Poza biurem',
    'RE: Ausência Temporária: away this week',
    'Automatisch antwoord bij afwezigheid',
    'Réponse en cas d\'absence',
    'Réponse automatique en cas d\'absence',
    'Réponse automatique',
    'Automaattinen vastaus',
    'On Sabbatical',
    'Fuera de la Oficina',
    'fuera de oficina',
    '自動応答',
    '自动答复',
    '自动回复',
    '自動回覆',
    '자동 회신',
    'Risposta automatica',
    'Resposta automática',
    'Автоответ',
    'Автоматический ответ',
    'Respuesta automática',
    'Odpowiedź automatyczna',
    'Otomatik Yanıt',
    'Absence du bureau',
    'Poissa:',
    'Poissa ',
    'Fuera de la-Oficina',
    'Automaatne vastus',
    'Automatyczna odpowiedź',
    'Automatyczna odpowiedz',
    'Vacation Response',
    'VACATION RESPONSE',
    'vacation response'
  ].map! do |reply|
    "#{reply}: Nobody home."
  end

  auto_vacation_subjects.each do |subject|
    it "denies when the subject contains the automated vacation response: #{subject}" do
      mail.subject = subject
      assert hard_rejected?(mail), subject
    end
  end

  describe "when mail is an Intercom notification" do
    let(:header_value) { 'notification' }

    before do
      mail.headers["X-Intercom-Bin"] = header_value
    end

    describe "with X-Intercom-Bin value 'notification'" do
      describe_with_arturo_enabled :email_suspend_mail_from_intercom do
        it "suspends" do
          assert hard_rejected?(mail)
        end
      end

      describe_with_arturo_disabled :email_suspend_mail_from_intercom do
        it "does not suspend" do
          refute hard_rejected?(mail)
        end
      end
    end
  end

  describe 'when rule specifies a whole word' do
    describe 'having a partial match' do
      let(:subject) { 'Subject sjukfrånvaro' }
      it 'does not suspend'
    end

    describe 'having a full match' do
      let(:subject) { 'Subject frånvaro' }
      it 'gets suspended'
    end
  end

  describe "#to_s" do
    it "returns the header name and the matched pattern" do
      mail.mailer = "MailChimp"
      rules = soft_rules.select { |soft_rule| soft_rule.deny?(mail, account) }
      assert_equal ["Header mailer matches (?-mix:Kayako|MailChimp|Benchmail Agent|Clang|Autoresponder) pattern"], rules.map(&:to_s)
    end
  end
end
