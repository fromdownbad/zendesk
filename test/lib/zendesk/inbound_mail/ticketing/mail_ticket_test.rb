require_relative "../../../../support/test_helper"
require_relative "../../../../support/arturo_test_helper"

SingleCov.covered! uncovered: 2

describe Zendesk::InboundMail::Ticketing::MailTicket do
  extend ArturoTestHelper

  fixtures :accounts, :users, :rules, :ticket_fields, :tickets

  let (:account) { accounts(:minimum) }

  before do
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = account
    @state.plain_content = "CONTENT"
    @state.account.stubs(nice_id_sequence: stub(next: 123))
    @state.recipient = "john@doe.com"

    @mail = Zendesk::Mail::InboundMessage.new

    @mail_ticket = Zendesk::InboundMail::Ticketing::MailTicket.new(@state, @mail)
  end

  def save_ticket(ticket)
    ticket.will_be_saved_by(user)
    Zendesk::InboundMail::Processors::TicketCreatorProcessor.new(@state, @mail).send(:propagate_created_at, ticket)
    ticket.requester = user
    ticket.save!
  end

  let(:user) { users(:minimum_end_user) }

  describe "#description" do
    let(:description) { @mail_ticket.send(:description, "CONTENT") }

    before do
      @mail.subject = "SUBJECT"
      @state.account.field_subject.is_active = true
    end

    it "uses content" do
      description.must_equal "CONTENT"
    end

    it "returns no-content when content is blank" do
      @mail_ticket.send(:description, "   ").must_equal NO_CONTENT
    end

    describe "when subject field is deactiveated" do
      before { @state.account.field_subject.is_active = false }

      it "inserts a subject line" do
        description.must_equal "SUBJECT: CONTENT"
      end

      it "does not insert a blank subject" do
        @mail.subject = ""
        description.must_equal "CONTENT"
      end
    end
  end

  describe "for an account with light-agents" do
    let(:account) { accounts(:multiproduct) }
    let(:agent) { users(:multiproduct_light_agent) }
    let(:user) { users(:multiproduct_end_user) }

    before { @state.stubs(:from).returns(stub(address: user.email)) }

    describe "when email is a forward from a light agent" do
      before do
        @state.agent_forwarded = true
        agent.stubs(:can?).with(:publicly, Comment).returns(false)
        @state.stubs(:submitter).returns(agent)
      end

      it "sets the comment to private" do
        ticket = @mail_ticket.to_ticket
        save_ticket(ticket)
        refute ticket.reload.comments.first.is_public
      end

      describe_with_arturo_disabled :email_light_agent_forwarding_first_comment_private do
        it "does not set the comment to private" do
          ticket = @mail_ticket.to_ticket
          save_ticket(ticket)
          assert ticket.reload.comments.first.is_public
        end
      end
    end

    describe "when email is not a forward" do
      before do
        @state.agent_forwarded = false
        agent.stubs(:can?).with(:publicly, Comment).returns(false)
        @state.stubs(:submitter).returns(agent)
      end

      describe_with_and_without_arturo_enabled :email_light_agent_forwarding_first_comment_private do
        it "does not set the comment to private" do
          ticket = @mail_ticket.to_ticket
          save_ticket(ticket)
          assert ticket.reload.comments.first.is_public
        end
      end
    end
  end

  describe "#to_ticket" do
    before { @state.stubs(:from).returns(stub(address: user.email)) }

    it "enables triggers if X-Zendesk-Disable-Triggers is not set" do
      ticket = @mail_ticket.to_ticket
      refute ticket.disable_triggers
      save_ticket(ticket)
      assert_equal [nil], ticket.audits.map(&:disable_triggers).uniq
    end

    it "disables triggers if X-Zendesk-Disable-Triggers is set" do
      @mail.headers['X-Zendesk-Disable-Triggers'] = 'true'
      ticket = @mail_ticket.to_ticket
      assert ticket.disable_triggers

      save_ticket(ticket)
      assert_equal [true], ticket.audits.map(&:disable_triggers).uniq
    end

    it "sets created_at from X-Zendesk-Created-At" do
      @mail.headers['X-Zendesk-Created-At'] = '1234567'
      ticket = @mail_ticket.to_ticket
      assert_equal 1234567, ticket.created_at.to_i

      save_ticket(ticket)
      assert_equal 1234567, ticket.created_at.to_i

      assert_equal [1234567], ticket.audits.map(&:created_at).map(&:to_i).uniq
    end

    it "sets brand_id" do
      @state.brand_id = 123
      ticket = @mail_ticket.to_ticket
      assert_equal ticket.brand_id, 123
    end

    it "removes +id from recipient" do
      @state.recipient = "john+id1@doe.zendesk-test.com"
      ticket = @mail_ticket.to_ticket
      ticket.recipient.wont_include "+id1"
    end

    describe "with rich_content_in_emails enabled" do
      before { @state.account.settings.stubs(rich_content_in_emails?: true) }

      it "stores the plain content" do
        @state.plain_content = "content"
        @state.html_content = "<div>content</div>"
        @mail_ticket.to_ticket.comment.original_plain_body.must_equal @state.plain_content
      end
    end

    describe "Mark ticket as unverified" do
      let(:end_user) { users(:minimum_end_user) }
      let(:end_user_identity) { end_user.identities.email.first }

      before do
        @state.stubs(:from).returns(stub(name: end_user.name, address: end_user.email))
        @state.stubs(:author).returns(end_user)
      end

      describe "when the from address is unverified" do
        before { UserEmailIdentity.any_instance.stubs(:is_verified?).returns(false) }

        it "marks ticket as unverified" do
          ticket = @mail_ticket.to_ticket
          save_ticket(ticket)
          assert ticket.unverified_creation.present?
        end

        it 'logs' do
          @mail.logger.expects(:info).with do |message|
            message.match(/^Ticket was marked as unverified! from address:.*, account:.*, identity:.*, message_id: <.+@zendesk-test\.com>$/)
          end

          ticket = @mail_ticket.to_ticket
          save_ticket(ticket)
        end
      end

      describe "when the from address is verified" do
        before { UserEmailIdentity.any_instance.stubs(:is_verified?).returns(true) }

        it "doesn't mark ticket as unverified" do
          ticket = @mail_ticket.to_ticket
          save_ticket(ticket)
          refute ticket.unverified_creation.present?
        end
      end

      describe "when the from address is new" do
        before { @state.stubs(:from).returns(stub(name: "John Doe", address: "john@doe.com")) }

        it "doesn't mark ticket as unverified" do
          ticket = @mail_ticket.to_ticket
          save_ticket(ticket)
          refute ticket.unverified_creation.present?
        end
      end
    end
  end

  describe "#metadata" do
    let(:metadata) { @mail_ticket.send(:metadata) }

    before do
      @mail.eml_url = "memory://inbound_email_store/identifier.eml"
      @mail.json_url = "memory://inbound_email_store/identifier.json"
      @mail.headers["Received"] = "from 10.0.0.1 (example.com [127.0.0.1])"
      @mail.headers["Received"] = "from 10.0.0.2 (example.com [127.0.0.1])"
      @mail.message_id          = "<12345@example.com>"
    end

    it "has metadata about raw s3 location" do
      assert_equal "identifier.eml", metadata[:raw_email_identifier]
    end

    it "has metadata about JSON S3 location" do
      assert_equal "identifier.json", metadata[:json_email_identifier]
    end

    it "has ip metadata" do
      assert_equal "10.0.0.2", metadata[:ip_address]
    end

    it "has message id metadata" do
      assert_equal "<12345@example.com>", metadata[:message_id]
    end

    it "does not record machine or automated for humans" do
      refute metadata.key?(:machine_generated)
    end

    it "records machine for machines" do
      @mail.headers["X-Zendesk-From-Account-Id"] = "blaaa"
      assert(metadata[:machine_generated])
    end

    describe "when the mail client is longer than the maximum allowed size" do
      let(:maximum_client_size) { Zendesk::InboundMail::Ticketing::MailTicket::AUDIT_METADATA_MAIL_CLIENT_SIZE_LIMIT }
      let(:client_over_maximum) { "Some client" * maximum_client_size }

      before { @mail.mailer = client_over_maximum }

      it "truncates the mail client metadata to be equal to the maximum allowed size" do
        assert_equal client_over_maximum.first(maximum_client_size), metadata[:client]
      end

      describe "when the mail client is not longer than the maximum allowed size" do
        before { @mail.mailer = "Zendesk Mailer" }

        it "adds the full client to the metadata" do
          assert_equal @mail.mailer, metadata[:client]
        end
      end
    end

    describe "when there is no mail client specified" do
      describe_with_and_without_arturo_enabled :email_truncate_long_x_mailer_in_audit_metadata do
        it "does not add client information to the metadata" do
          refute metadata.key?(:client)
        end
      end
    end
  end

  describe "#add_metadata" do
    let(:metadata) do
      {
        message_id: 'message_id',
        client: 'client',
        ip_address: 'ip'
      }.with_indifferent_access
    end

    before do
      @mail_ticket.expects(:metadata).returns(metadata)
    end

    describe "when state.ticket is a Ticket" do
      before do
        @ticket = Ticket.new(account: @state.account)
        @ticket.will_be_saved_by(User.new)
      end

      it "adds #metadata to the audits system metadata" do
        @mail_ticket.add_metadata(@ticket)
        assert_equal(metadata, @ticket.audit.metadata[:system])
      end
    end

    describe "when state.ticket is a SuspendedTicket" do
      before { @ticket = SuspendedTicket.new }

      it "adds metadata to the suspended ticket" do
        @mail_ticket.add_metadata(@ticket)
        assert_equal metadata, @ticket.metadata
      end
    end
  end

  describe "#to_suspended_ticket" do
    let(:suspended_ticket) { @mail_ticket.to_suspended_ticket }

    before { @state.stubs(:from).returns(stub(name: "name", address: "address@example.com")) }

    it "does not set anything" do
      suspended_ticket.properties.key?(:brand_id).must_equal false
      suspended_ticket.properties.key?(:agent_comment).must_equal false
      suspended_ticket.properties.key?(:submitter_id).must_equal false
      suspended_ticket.properties.key?(:from_zendesk).must_equal false
    end

    it "sets brand_id" do
      @state.brand_id = 123
      suspended_ticket.properties[:brand_id].must_equal 123
    end

    it "sets agent_comment and submitter_id" do
      @state.agent_comment = "Agent comment"
      @state.submitter = user
      suspended_ticket.properties[:agent_comment].must_equal value: "Agent comment", submitter_id: user.id, is_public: nil
    end

    it "removes +id from recipient" do
      @state.recipient = "john+id1@doe.zendesk-test.com"
      suspended_ticket.recipient.wont_include "+id1"
    end

    it "stores html" do
      @state.html_content = "<span>Hello</span>"
      suspended_ticket.content(raw: true).must_equal "<span>Hello</span>"
      suspended_ticket.content.must_equal "Hello"
      suspended_ticket.properties[:html].must_equal true
    end

    it "sets message_id ticket" do
      assert suspended_ticket.message_id
    end

    describe "when from name is missing" do
      before { @state.stubs(:from).returns(stub(name: nil, address: "address@example.com")) }

      it "uses #{Zendesk::InboundMail::Ticketing::MailTicket::UNKNOWN} as the from name" do
        suspended_ticket.from_name.must_equal Zendesk::InboundMail::Ticketing::MailTicket::UNKNOWN
      end
    end

    describe "when the state has no recipient" do
      before { @state.stubs(recipient: nil) }

      describe_with_arturo_enabled :email_suspended_ticket_ignore_nil_recipient do
        it "sets the suspended ticket's recipient to nil" do
          assert_nil suspended_ticket.recipient
        end
      end

      describe_with_arturo_disabled :email_suspended_ticket_ignore_nil_recipient do
        it "raises an exception when trying to strip a plus_id from the recipient and fails" do
          assert_raises(NoMethodError) { suspended_ticket }
        end
      end
    end
  end

  describe ".create_agent_comment_on_ticket" do
    let(:ticket) do
      @mail_ticket.to_ticket.tap do |ticket|
        save_ticket(ticket)
      end
    end

    let(:user) { users(:minimum_agent) }

    before { @state.stubs(:from).returns(stub(address: user.email)) }

    def create_agent_comment_on_ticket
      Zendesk::InboundMail::Ticketing::MailTicket.create_agent_comment_on_ticket(@state.account, ticket, "agent comment", user, false)
    end

    it "adds a comment authored by the given user" do
      num_comments = ticket.comments.size
      create_agent_comment_on_ticket
      assert_equal num_comments + 1, ticket.comments.size
      assert_equal user.id, ticket.comments.last.author.id
    end

    it "adds a flag to the comment if .flag_agent_not_in_group? is true" do
      Zendesk::InboundMail::Ticketing::MailTicket.stubs(:flag_agent_not_in_group?).returns(true)
      Ticket.any_instance.expects(:add_flags!)
      create_agent_comment_on_ticket
    end
  end

  describe ".flag_agent_not_in_group?" do
    # This user is in group with_groups_2
    let(:submitter) { users(:with_groups_agent_groups_restricted) }
    let(:ticket) { tickets(:with_groups_2) }

    def flag_agent_not_in_group?
      Zendesk::InboundMail::Ticketing::MailTicket.flag_agent_not_in_group?(ticket, submitter)
    end

    describe "when submitter is restricted to a group that is not the ticket's current group" do
      let(:ticket) { tickets(:with_groups_1) }

      it "is true" do
        assert flag_agent_not_in_group?
      end
    end

    describe "when submitter is restricted to a group that is the ticket's current group" do
      it "is false" do
        refute flag_agent_not_in_group?
      end
    end

    describe "when submitter is organization-restricted" do
      let(:submitter) { users(:with_groups_agent_organization_restricted) }

      it "is false" do
        assert submitter.agent_restriction?(:organization)
        refute flag_agent_not_in_group?
      end
    end

    describe "when submitter is not restricted" do
      let(:submitter) { users(:with_groups_agent1) }

      it "is false" do
        assert submitter.agent_restriction?(:none)
        refute flag_agent_not_in_group?
      end
    end
  end
end
