require_relative "../../../../support/test_helper"
require_relative "../../../../support/agent_test_helper"
require_relative "../../../../support/arturo_test_helper"
require_relative "../../../../support/collaboration_settings_test_helper"
require_relative "../../../../../lib/zendesk/inbound_mail/ticketing/mail_collaboration_update_rules"
require_relative "../../../../../lib/zendesk/inbound_mail/ticketing/mail_collaboration_update_types"

SingleCov.covered!

describe "Mail Collaboration Update Rules" do
  extend ArturoTestHelper

  include AgentTestHelper

  fixtures :accounts, :users, :tickets

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:submitter) { users(:minimum_agent) }
  let(:requester_is_on_email) { false }

  let(:update_rule_class) { Zendesk::InboundMail::Ticketing::MailCollaborationUpdateRule }
  let(:update_rule) { update_rule_class.new(ticket, submitter, requester_is_on_email) }

  before do
    CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
  end

  def assert_update_type(expected_type)
    assert update_rule.update_type.is_a?(expected_type)
  end

  def assert_log_message(message_suffix)
    update_rule.to_s.must_equal("MailCollaborationUpdateRule: #{message_suffix}")
  end

  def create_collaboration(ticket, user, collaborator_type)
    ticket.collaborations.build(ticket: ticket, user: user, collaborator_type: collaborator_type)
    ticket.will_be_saved_by(ticket.requester)
    ticket.save!
  end

  def build_comment(ticket, author, body, is_public)
    ticket.will_be_saved_by(author)
    ticket.add_comment(body: body, is_public: is_public, author: author)
  end

  def submitter_is_agent_participant_and_email_cc
    update_rule.send(:submitter_is_agent_participant_and_email_cc?)
  end

  describe "MailCollaborationUpdateRule" do
    describe "#conditions_are_met?" do
      it { refute update_rule.conditions_are_met? }
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::NoopUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("base rule") }
    end

    describe "#add_new_agents_update_type_class" do
      def assert_add_new_agents_update_type_class(expected_class)
        update_rule.send(:add_new_agents_update_type_class).must_equal expected_class
      end

      describe_with_arturo_setting_enabled :ticket_followers_allowed do
        it { assert_add_new_agents_update_type_class(Zendesk::InboundMail::Ticketing::AddNewAgentsAsFollowersUpdateType) }
      end

      describe_with_arturo_setting_disabled :ticket_followers_allowed do
        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          it { assert_add_new_agents_update_type_class(Zendesk::InboundMail::Ticketing::AddNewAgentsAsEmailCcsUpdateType) }
        end

        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          it { assert_add_new_agents_update_type_class(Zendesk::InboundMail::Ticketing::NoopUpdateType) }
        end
      end
    end

    describe "#submitter_is_agent?" do
      describe "when submitter is an agent" do
        it { assert update_rule.send(:submitter_is_agent?) }
      end

      describe "when submitter is a light agent" do
        let(:submitter) { create_light_agent }
        it { refute update_rule.send(:submitter_is_agent?) }
      end

      describe "when submitter is an end user" do
        let(:submitter) { users(:minimum_end_user) }
        it { refute update_rule.send(:submitter_is_agent?) }
      end
    end

    describe "#submitter_is_light_agent?" do
      describe "when submitter is an agent" do
        it { refute update_rule.send(:submitter_is_light_agent?) }
      end

      describe "when submitter is a light agent" do
        let(:submitter) { create_light_agent }
        it { assert update_rule.send(:submitter_is_light_agent?) }
      end

      describe "when submitter is an end user" do
        let(:submitter) { users(:minimum_end_user) }
        it { refute update_rule.send(:submitter_is_light_agent?) }
      end
    end
  end

  describe "LegacyCcs" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::LegacyCcs }

    describe "#conditions_are_met?" do
      describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
        it { assert update_rule.conditions_are_met? }
      end

      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        it { refute update_rule.conditions_are_met? }
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::LegacyCcsUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("CCs/Followers setting is disabled, adding CCs as legacy collaborations") }
    end
  end

  describe "FollowersAndEmailCcsSettingsDisabled" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::FollowersAndEmailCcsSettingsDisabled }

    describe "#conditions_are_met?" do
      describe_with_arturo_setting_disabled :ticket_followers_allowed do
        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          it { assert update_rule.conditions_are_met? }
        end

        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          it { refute update_rule.conditions_are_met? }
        end
      end

      describe_with_arturo_setting_enabled :ticket_followers_allowed do
        describe_with_and_without_arturo_setting_enabled :comment_email_ccs_allowed do
          it { refute update_rule.conditions_are_met? }
        end
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::NoopUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("individual CCs and Followers settings are disabled, so no collaborations can be added") }
    end
  end

  describe "NewTicket" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::NewTicket }

    describe "#conditions_are_met?" do
      describe "when ticket is new" do
        let(:ticket) { Ticket.new }

        it { assert update_rule.conditions_are_met? }

        describe "and ticket is a follow-up" do
          let(:source_ticket) { Ticket.new }

          before do
            ticket.account = account
            source_ticket.account = account
            ticket.set_followup_source_ticket(source_ticket)
          end

          it { refute update_rule.conditions_are_met? }
        end
      end

      describe "when ticket is persisted" do
        it { refute update_rule.conditions_are_met? }
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("ticket is new") }
    end
  end

  describe "AgentIsCcAndFollowerOrAssignee" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssignee }

    describe "#conditions_are_met?" do
      let(:submitter_is_agent_participant_and_email_cc) { true }

      before { Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssignee.any_instance.stubs(submitter_is_agent_participant_and_email_cc?: submitter_is_agent_participant_and_email_cc) }

      describe "when submitter_is_agent_participant_and_email_cc? is true" do
        it { assert update_rule.conditions_are_met? }
      end

      describe "when submitter_is_agent_participant_and_email_cc? is false" do
        let(:submitter_is_agent_participant_and_email_cc) { false }

        it { refute update_rule.conditions_are_met? }
      end
    end

    describe "#to_s" do
      it { assert_log_message("submitter is an agent already participating in the ticket but is also an email CC") }
    end

    describe "#submitter_is_agent_participant_and_email_cc?" do
      describe "when submitter is an agent" do
        describe "when submitter is the requester" do
          before do
            ticket.requester = submitter
            ticket.will_be_saved_by(ticket.assignee)
            ticket.save!
          end

          describe "when submitter is a follower" do
            let(:submitter) { users(:minimum_admin) }

            before { create_collaboration(ticket, submitter, CollaboratorType.FOLLOWER) }

            it { assert submitter_is_agent_participant_and_email_cc }
          end

          describe "when submitter is the assignee" do
            it { ticket.assignee.must_equal submitter }
            it { assert submitter_is_agent_participant_and_email_cc }
          end

          describe "when submitter is not a follower or assignee" do
            let(:submitter) { FactoryBot.create(:agent, account: account) }

            it { refute ticket.is_follower?(submitter) }
            it { assert_not_equal ticket.assignee, submitter }
            it { refute submitter_is_agent_participant_and_email_cc }
          end
        end

        describe "when submitter is an email CC" do
          let(:submitter) { users(:minimum_admin) }

          before { create_collaboration(ticket, submitter, CollaboratorType.EMAIL_CC) }

          describe "when submitter is a follower" do
            before { create_collaboration(ticket, submitter, CollaboratorType.FOLLOWER) }

            it { assert submitter_is_agent_participant_and_email_cc }
          end

          describe "when submitter is the assignee" do
            let(:submitter) { users(:minimum_agent) }

            it { assert submitter_is_agent_participant_and_email_cc }
          end

          describe "when submitter is not a follower or assignee" do
            let(:submitter) { FactoryBot.create(:agent, account: account) }

            it { refute ticket.is_follower?(submitter) }
            it { assert_not_equal ticket.assignee, submitter }
            it { refute submitter_is_agent_participant_and_email_cc }
          end
        end
      end

      describe "when submitter is an end user" do
        it { refute submitter_is_agent_participant_and_email_cc }
      end
    end
  end

  describe "AgentIsCcAndFollowerOrAssigneeWithRequesterOnEmail" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssigneeWithRequesterOnEmail }

    describe "#conditions_are_met?" do
      let(:submitter_is_agent_participant_and_email_cc) { true }

      before { Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssignee.any_instance.stubs(submitter_is_agent_participant_and_email_cc?: submitter_is_agent_participant_and_email_cc) }

      describe "when submitter_is_agent_participant_and_email_cc? is true" do
        describe "when requester_is_on_email is false" do
          it { refute update_rule.conditions_are_met? }
        end

        describe "when requester_is_on_email is true" do
          let(:requester_is_on_email) { true }

          it { assert update_rule.conditions_are_met? }
        end
      end

      describe "when submitter_is_agent_participant_and_email_cc? is false" do
        let(:submitter_is_agent_participant_and_email_cc) { false }

        describe "when requester_is_on_email is false" do
          it { refute update_rule.conditions_are_met? }
        end

        describe "when requester_is_on_email is true" do
          let(:requester_is_on_email) { true }

          it { refute update_rule.conditions_are_met? }
        end
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("submitter is an agent already participating in the ticket but is also an email CC, and requester is on the email") }
    end
  end

  describe "AgentIsCcAndFollowerOrAssigneeWithRequesterNotOnEmail" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssigneeWithRequesterNotOnEmail }

    describe "#conditions_are_met?" do
      let(:submitter_is_agent_participant_and_email_cc) { true }

      before { Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssignee.any_instance.stubs(submitter_is_agent_participant_and_email_cc?: submitter_is_agent_participant_and_email_cc) }

      describe "when submitter_is_agent_participant_and_email_cc? is true" do
        describe "when requester_is_on_email is false" do
          it { assert update_rule.conditions_are_met? }
        end

        describe "when requester_is_on_email is true" do
          let(:requester_is_on_email) { true }

          it { refute update_rule.conditions_are_met? }
        end
      end

      describe "when submitter_is_agent_participant_and_email_cc? is false" do
        let(:submitter_is_agent_participant_and_email_cc) { false }

        describe "when requester_is_on_email is false" do
          it { refute update_rule.conditions_are_met? }
        end

        describe "when requester_is_on_email is true" do
          let(:requester_is_on_email) { true }

          it { refute update_rule.conditions_are_met? }
        end
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::AddNewAgentsUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("submitter is an agent already participating in the ticket but is also an email CC, and requester is not on the email") }
    end
  end

  describe "FollowerOrAssignee" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::FollowerOrAssignee }

    describe "#conditions_are_met?" do
      describe "when submitter is a follower" do
        let(:submitter) { users(:minimum_admin) }

        before { create_collaboration(ticket, submitter, CollaboratorType.FOLLOWER) }

        it { assert update_rule.conditions_are_met? }
      end

      describe "when submitter is an assignee" do
        it { ticket.assignee.must_equal submitter }
        it { assert update_rule.conditions_are_met? }
      end

      describe "when submitter is not a follower or assignee" do
        let(:submitter) { FactoryBot.create(:agent, account: account) }

        it { refute ticket.is_follower?(submitter) }
        it { assert_not_equal ticket.assignee, submitter }
        it { refute update_rule.conditions_are_met? }
      end
    end

    describe "#to_s" do
      it { assert_log_message("submitter is a follower or the assignee") }
    end
  end

  describe "FollowerOrAssigneeWithPrivateComment" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithPrivateComment }

    describe "#conditions_are_met?" do
      let(:comment_is_public) { true }

      before { build_comment(ticket, submitter, "private comment", comment_is_public) }

      describe "when submitter is a follower or assignee" do
        describe "when current comment is private" do
          let(:comment_is_public) { false }

          it { assert update_rule.conditions_are_met? }
        end

        describe "when current comment is public" do
          it { refute update_rule.conditions_are_met? }
        end
      end

      describe "when submitter is not a follower or assignee" do
        let(:submitter) { FactoryBot.create(:agent, account: account) }

        it { refute ticket.is_follower?(submitter) }

        describe "when current comment is private" do
          let(:comment_is_public) { false }

          it { refute update_rule.conditions_are_met? }
        end
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::AddNewAgentsUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("submitter is a follower or the assignee, and comment is private") }
    end
  end

  describe "FollowerOrAssigneeWithOnlyFollowersAllowed" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithOnlyFollowersAllowed }

    describe "#conditions_are_met?" do
      describe "when submitter is a follower, assignee, or group member" do
        describe_with_arturo_setting_enabled :ticket_followers_allowed do
          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it { refute update_rule.conditions_are_met? }
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            it { assert update_rule.conditions_are_met? }
          end
        end

        describe_with_arturo_setting_disabled :ticket_followers_allowed do
          describe_with_and_without_arturo_setting_enabled :comment_email_ccs_allowed do
            it { refute update_rule.conditions_are_met? }
          end
        end
      end

      describe "when submitter is not a follower, assignee, or group member" do
        let(:submitter) { FactoryBot.create(:agent, account: account) }

        it { refute ticket.group_includes?(submitter) }
        it { refute ticket.is_follower?(submitter) }

        describe "when current comment is private" do
          let(:comment_is_public) { false }

          describe_with_arturo_setting_enabled :ticket_followers_allowed do
            describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
              it { refute update_rule.conditions_are_met? }
            end
          end
        end
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::AddNewAgentsUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("submitter is a follower or the assignee, adding all new users but email CCs are not allowed") }
    end
  end

  describe "FollowerOrAssigneeWithOnlyEmailCcsAllowed" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithOnlyEmailCcsAllowed }

    describe "#conditions_are_met?" do
      describe "when submitter is a follower, assignee, or group member" do
        describe_with_arturo_setting_enabled :ticket_followers_allowed do
          describe_with_and_without_arturo_setting_enabled :comment_email_ccs_allowed do
            it { refute update_rule.conditions_are_met? }
          end
        end

        describe_with_arturo_setting_disabled :ticket_followers_allowed do
          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it { assert update_rule.conditions_are_met? }
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            it { refute update_rule.conditions_are_met? }
          end
        end
      end

      describe "when submitter is not a follower, assignee, or group member" do
        let(:submitter) { FactoryBot.create(:agent, account: account) }

        it { refute ticket.group_includes?(submitter) }
        it { refute ticket.is_follower?(submitter) }

        describe_with_arturo_setting_disabled :ticket_followers_allowed do
          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it { refute update_rule.conditions_are_met? }
          end
        end
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::AddNewAsEmailCcsUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("submitter is a follower or the assignee, adding all new users as email CCs") }
    end
  end

  describe "FollowerOrAssigneeWithFollowersAndCcsAllowed" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithFollowersAndCcsAllowed }

    describe "#conditions_are_met?" do
      describe "when submitter is a follower, assignee, or group member" do
        describe_with_arturo_setting_enabled :ticket_followers_allowed do
          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it { assert update_rule.conditions_are_met? }
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            it { refute update_rule.conditions_are_met? }
          end
        end

        describe_with_arturo_setting_disabled :ticket_followers_allowed do
          describe_with_and_without_arturo_setting_enabled :comment_email_ccs_allowed do
            it { refute update_rule.conditions_are_met? }
          end
        end
      end

      describe "when submitter is not a follower, assignee, or group member" do
        let(:submitter) { FactoryBot.create(:agent, account: account) }

        it { refute ticket.group_includes?(submitter) }
        it { refute ticket.is_follower?(submitter) }

        describe_with_arturo_setting_enabled :ticket_followers_allowed do
          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it { refute update_rule.conditions_are_met? }
          end
        end
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("submitter is a follower or the assignee, adding all new users and both followers and email CCs are allowed") }
    end
  end

  describe "EmailCcOrRequester" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::EmailCcOrRequester }

    describe "#conditions_are_met?" do
      describe "when submitter is an email CC" do
        let(:submitter) { users(:minimum_end_user2) }

        before { create_collaboration(ticket, submitter, CollaboratorType.EMAIL_CC) }

        it { assert update_rule.conditions_are_met? }
      end

      describe "when submitter is the requester" do
        let(:submitter) { ticket.requester }

        it { assert update_rule.conditions_are_met? }
      end

      describe "when submitter is not an email CC or the requester" do
        let(:submitter) { users(:minimum_end_user2) }

        it { refute update_rule.conditions_are_met? }
      end
    end

    describe "#to_s" do
      it { assert_log_message("submitter is an email CC or requester") }

      describe "when there is a mismatch between requester_is_on_email and the ticket submitter check" do
        let(:requester_is_on_email) { false }
        let(:submitter) { ticket.requester }

        it { assert_log_message("WARNING: there is a mismatch detecting that the requester is on the email: submitter is an email CC or requester") }
      end
    end
  end

  describe "EmailCcOrRequesterAndRequesterIsOnEmail" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::EmailCcOrRequesterAndRequesterIsOnEmail }

    describe "#conditions_are_met?" do
      describe "when submitter is an email CC" do
        let(:submitter) { users(:minimum_end_user2) }

        before { create_collaboration(ticket, submitter, CollaboratorType.EMAIL_CC) }

        describe "when the requester is on the email" do
          let(:requester_is_on_email) { true }

          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it { assert update_rule.conditions_are_met? }
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            it { refute update_rule.conditions_are_met? }
          end
        end

        describe "when the requester is not on the email" do
          it { refute update_rule.conditions_are_met? }
        end
      end

      describe "when submitter is the requester" do
        let(:submitter) { ticket.requester }

        describe "when the requester is on the email" do
          let(:requester_is_on_email) { true }

          describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
            it { assert update_rule.conditions_are_met? }
          end

          describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
            it { refute update_rule.conditions_are_met? }
          end
        end

        # This should not technically be possible but we log in the case that it does happen since the
        # requester_is_on_email flag is detected in a different way to ticket.requested_by?(submitter)
        describe "when the requester is not on the email" do
          it { refute update_rule.conditions_are_met? }
        end
      end

      describe "when submitter is not an email CC or the requester" do
        let(:submitter) { users(:minimum_end_user2) }

        it { refute update_rule.conditions_are_met? }
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("submitter is an email CC or requester, and requester is on email") }
    end
  end

  describe "EmailCcOrRequesterAndSubmitterIsAgent" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::EmailCcOrRequesterAndSubmitterIsAgent }

    describe "#conditions_are_met?" do
      describe "when submitter is an agent" do
        let(:submitter) { users(:minimum_admin) }

        describe "when submitter is an email CC" do
          before { create_collaboration(ticket, submitter, CollaboratorType.EMAIL_CC) }

          it { assert update_rule.conditions_are_met? }
        end

        describe "when submitter is the requester" do
          before do
            ticket.requester = submitter
            ticket.will_be_saved_by(ticket.assignee)
            ticket.save!
          end

          it { assert update_rule.conditions_are_met? }
        end

        describe "when submitter is not an email CC or the requester" do
          it { refute update_rule.conditions_are_met? }
        end
      end

      describe "when submitter is an end user" do
        let(:submitter) { users(:minimum_end_user2) }

        describe "when submitter is an email CC" do
          before { create_collaboration(ticket, submitter, CollaboratorType.EMAIL_CC) }

          it { refute update_rule.conditions_are_met? }
        end
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::AddNewAgentsUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("submitter is an email CC or requester, and submitter is an agent") }
    end
  end

  describe "EmailCcOrRequesterNoop" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::EmailCcOrRequesterNoop }

    describe "#to_s" do
      it { assert_log_message("submitter is an email CC or requester, requester is not on email and submitter is not an agent, so skipping collaboration update") }
    end
  end

  describe "AssignedGroupAgent" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::AssignedGroupAgent }

    describe "#conditions_are_met?" do
      describe "when the ticket has a group" do
        before do
          ticket.group = groups(:minimum_group)
        end

        describe "when the submitter is a member of the group" do
          it { assert update_rule.conditions_are_met? }
        end

        describe "when the submitter is not a member of the group" do
          let(:submitter) { users(:minimum_admin_not_verified) }

          it { refute ticket.group_includes?(submitter) }
          it { refute update_rule.conditions_are_met? }
        end
      end
    end

    describe "#update_type" do
      describe_with_arturo_setting_enabled :ticket_followers_allowed do
        it { assert_update_type(Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsFollowersUpdateType) }
      end

      describe_with_arturo_setting_disabled :ticket_followers_allowed do
        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          it { assert_update_type(Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsEmailCcsUpdateType) }
        end

        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          it { assert_update_type(Zendesk::InboundMail::Ticketing::NoopUpdateType) }
        end
      end
    end

    describe "#to_s" do
      it { assert_log_message("submitter is a member of the assigned group") }
    end

    describe "#add_non_group_agents_update_type" do
      def assert_add_non_group_agents_update_type(expected_class)
        update_rule.send(:add_non_group_agents_update_type).must_equal expected_class
      end

      describe_with_arturo_setting_enabled :ticket_followers_allowed do
        it { assert_add_non_group_agents_update_type(Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsFollowersUpdateType) }
      end

      describe_with_arturo_setting_disabled :ticket_followers_allowed do
        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          it { assert_add_non_group_agents_update_type(Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsEmailCcsUpdateType) }
        end

        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          it { assert_add_non_group_agents_update_type(Zendesk::InboundMail::Ticketing::NoopUpdateType) }
        end
      end
    end
  end

  describe "SubmitterIsAgent" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::SubmitterIsAgent }

    describe "#conditions_are_met?" do
      describe "when submitter is an agent" do
        let(:submitter) { users(:minimum_admin) }

        it { assert update_rule.conditions_are_met? }
      end

      describe "when submitter is an end user" do
        let(:submitter) { users(:minimum_end_user2) }

        it { refute update_rule.conditions_are_met? }
      end
    end

    describe "#to_s" do
      it { assert_log_message("submitter is an agent not already participating") }
    end
  end

  describe "SubmitterIsAgentAndRequesterIsOnEmail" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::SubmitterIsAgentAndRequesterIsOnEmail }

    describe "#conditions_are_met?" do
      describe "when submitter is an agent" do
        let(:submitter) { users(:minimum_admin) }

        describe "when requester is on the email" do
          let(:requester_is_on_email) { true }

          it { assert update_rule.conditions_are_met? }
        end

        describe "when requester is not on the email" do
          it { refute update_rule.conditions_are_met? }
        end
      end

      describe "when submitter is an end user" do
        let(:submitter) { users(:minimum_end_user2) }

        describe "when requester is on the email" do
          let(:requester_is_on_email) { true }

          it { refute update_rule.conditions_are_met? }
        end
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::AddAuthorAsEmailCcUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("submitter is an agent not already participating, and requester is on email") }
    end
  end

  describe "SubmitterIsAgentAndFollowersIsEnabled" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::SubmitterIsAgentAndFollowersIsEnabled }

    describe "#conditions_are_met?" do
      describe "when submitter is an agent" do
        let(:submitter) { users(:minimum_admin) }

        describe_with_arturo_setting_enabled :ticket_followers_allowed do
          it { assert update_rule.conditions_are_met? }
        end

        describe_with_arturo_setting_disabled :ticket_followers_allowed do
          it { refute update_rule.conditions_are_met? }
        end
      end

      describe "when submitter is an end user" do
        let(:submitter) { users(:minimum_end_user2) }

        describe_with_and_without_arturo_setting_enabled :ticket_followers_allowed do
          it { refute update_rule.conditions_are_met? }
        end
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::AddAuthorAsFollowerUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("submitter is an agent not already participating, and followers are enabled") }
    end
  end

  describe "SubmitterIsAgentAndEmailCcsIsEnabled" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::SubmitterIsAgentAndEmailCcsIsEnabled }

    describe "#conditions_are_met?" do
      describe "when submitter is an agent" do
        let(:submitter) { users(:minimum_admin) }

        describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
          it { assert update_rule.conditions_are_met? }
        end

        describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
          it { refute update_rule.conditions_are_met? }
        end
      end

      describe "when submitter is an end user" do
        let(:submitter) { users(:minimum_end_user2) }

        describe_with_and_without_arturo_setting_enabled :comment_email_ccs_allowed do
          it { refute update_rule.conditions_are_met? }
        end
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::AddAuthorAsEmailCcUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("submitter is an agent not already participating, and email CCs are enabled") }
    end
  end

  describe "SubmitterIsEndUser" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::SubmitterIsEndUser }

    describe "#conditions_are_met?" do
      describe "when submitter is an end user" do
        let(:submitter) { users(:minimum_end_user2) }

        it { assert update_rule.conditions_are_met? }
      end

      describe "when submitter is an agent" do
        let(:submitter) { users(:minimum_admin) }

        it { refute update_rule.conditions_are_met? }
      end
    end

    describe "#update_type" do
      describe_with_arturo_enabled :email_make_other_user_comments_private do
        it { assert_update_type(Zendesk::InboundMail::Ticketing::NoopUpdateType) }
      end

      describe_with_arturo_disabled :email_make_other_user_comments_private do
        it { assert_update_type(Zendesk::InboundMail::Ticketing::AddAuthorAsEmailCcUpdateType) }
      end
    end

    describe "#to_s" do
      it { assert_log_message("submitter is an end user not already participating") }
    end
  end

  describe "SubmitterIsLightAgent" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::SubmitterIsLightAgent }

    describe "#conditions_are_met?" do
      describe "when submitter is an end user" do
        let(:submitter) { users(:minimum_end_user2) }

        it { refute update_rule.conditions_are_met? }
      end

      describe "when submitter is an agent" do
        let(:submitter) { users(:minimum_admin) }

        it { refute update_rule.conditions_are_met? }
      end

      describe "when submitter is a light agent" do
        let(:submitter) { create_light_agent }

        it { assert update_rule.conditions_are_met? }
      end
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::AddNewAgentsAsFollowersUpdateType) }

      describe_with_arturo_setting_disabled :ticket_followers_allowed do
        it { assert_update_type(Zendesk::InboundMail::Ticketing::NoopUpdateType) }
      end
    end

    describe "#to_s" do
      it { assert_log_message("submitter is a light agent and can only add private comments and followers") }
    end
  end

  describe "Noop" do
    let(:update_rule_class) { Zendesk::InboundMail::Ticketing::Noop }

    describe "#conditions_are_met?" do
      it "increments the update type not found counter for alerts in DataDog" do
        Zendesk::InboundMail::StatsD.client.expects(:increment).with("email_ccs_update_type_not_found")
        update_rule.conditions_are_met?
      end

      it { assert update_rule.conditions_are_met? }
    end

    describe "#update_type" do
      it { assert_update_type(Zendesk::InboundMail::Ticketing::NoopUpdateType) }
    end

    describe "#to_s" do
      it { assert_log_message("follower_and_email_cc_collaborations is enabled but no update type found. Submitter: #{submitter.inspect}") }
    end
  end
end
