require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 16

describe Zendesk::InboundMail::Ticketing::Followup do
  fixtures :accounts, :users, :rules, :ticket_fields, :tickets

  let(:account) { accounts(:minimum) }
  let(:end_user) { users(:minimum_author) }
  let(:end_user_2) { users(:minimum_end_user) }
  let(:agent) { users(:minimum_agent) }
  let(:admin) { users(:minimum_admin) }
  let(:admin_2) { users(:minimum_admin_not_verified) }
  let(:mail) { Zendesk::Mail::InboundMessage.new }

  let(:closed_ticket) do
    tickets(:minimum_1).tap do |ticket|
      ticket.status_id = StatusType.CLOSED
    end
  end

  let(:new_ticket) do
    Ticket.new.tap do |ticket|
      ticket.will_be_saved_by(end_user)
      ticket.requester = end_user
    end
  end

  let(:state) do
    Zendesk::InboundMail::ProcessingState.new.tap do |state|
      state.account = account
      state.plain_content = "CONTENT"
      state.account.stubs(nice_id_sequence: stub(next: 123))
      state.recipient = "john@doe.com"
      state.author = end_user
      state.closed_ticket = closed_ticket
      state.ticket = new_ticket
      state.ticket.comment = Comment.new(account: account, ticket: state.ticket)
      state.ticket.comment.html_body = "<div>CONTENT</div>"
    end
  end

  let(:followup) { Zendesk::InboundMail::Ticketing::Followup.new(state, mail, "a description") }

  before { closed_ticket.collaborations.delete_all }

  describe "#description" do
    it "uses br elements to separate the title and mail comment" do
      assert_includes followup.description, "<br />"
    end

    describe_with_arturo_disabled :email_followup_ticket_html_comment_fix do
      it "uses newlines to separate the title and mail comment" do
        assert_includes followup.description, "\n"
      end
    end
  end

  describe "#follower_ids" do
    before do
      closed_ticket.collaborations.create(collaborator_type: CollaboratorType.FOLLOWER, user_id: agent.id)
      closed_ticket.collaborations.create(collaborator_type: CollaboratorType.FOLLOWER, user_id: admin.id)
      closed_ticket.collaborations.create(collaborator_type: CollaboratorType.EMAIL_CC, user_id: end_user_2.id)
    end

    describe "with ticket_followers_allowed enabled" do
      before { account.stubs(has_ticket_followers_allowed_enabled?: true) }

      it { assert_empty followup.follower_ids - [agent.id, admin.id] }

      describe "when original requester is an agent and different from the new author" do
        before do
          state.author = end_user_2
          new_ticket.requester = end_user_2
          closed_ticket.requester = admin_2
        end
      end
    end

    describe "with ticket_followers_allowed disabled" do
      before { account.stubs(has_ticket_followers_allowed_enabled?: false) }

      it { assert_empty followup.follower_ids }
    end
  end

  describe "#email_cc_ids" do
    before do
      closed_ticket.collaborations.create(collaborator_type: CollaboratorType.FOLLOWER, user_id: admin.id)
      closed_ticket.collaborations.create(collaborator_type: CollaboratorType.EMAIL_CC, user_id: agent.id)
      closed_ticket.collaborations.create(collaborator_type: CollaboratorType.EMAIL_CC, user_id: end_user_2.id)
    end

    describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
      it { assert_equal [agent.id, end_user_2.id].sort, followup.email_cc_ids.sort }

      describe "when the original requester is different than the new author" do
        before do
          state.author = admin_2
          new_ticket.requester = admin_2
          closed_ticket.requester = end_user
        end

        it "adds original requester as an email CC" do
          assert_equal [agent.id, end_user.id, end_user_2.id].sort, followup.email_cc_ids.sort
        end
      end
    end

    describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
      it { assert_empty followup.email_cc_ids }
    end
  end

  describe "#all_collaborator_ids" do
    before do
      closed_ticket.collaborations.create(collaborator_type: CollaboratorType.FOLLOWER, user_id: admin.id)
      closed_ticket.collaborations.create(collaborator_type: CollaboratorType.FOLLOWER, user_id: agent.id)
      closed_ticket.collaborations.create(collaborator_type: CollaboratorType.EMAIL_CC, user_id: agent.id)
      closed_ticket.collaborations.create(collaborator_type: CollaboratorType.LEGACY_CC, user_id: admin_2.id)
    end

    it { assert_equal [agent.id, admin.id, admin_2.id].sort, followup.all_collaborator_ids.sort }

    describe "when the original requester is different than the new author" do
      before do
        state.author = end_user
        new_ticket.requester = end_user
        closed_ticket.requester = end_user_2
      end

      it "requester is treated as a legacy collaborator_id" do
        assert_equal [agent.id, admin.id, admin_2.id, end_user_2.id].sort, followup.all_collaborator_ids.sort
      end
    end
  end

  describe "#legacy_ccs_present?" do
    describe "with legacy CCs" do
      before do
        closed_ticket.collaborations.create(collaborator_type: CollaboratorType.FOLLOWER, user_id: admin.id)
        closed_ticket.collaborations.create(collaborator_type: CollaboratorType.EMAIL_CC, user_id: agent.id)
        closed_ticket.collaborations.create(collaborator_type: CollaboratorType.LEGACY_CC, user_id: admin_2.id)
      end

      it { assert followup.legacy_ccs_present? }
    end

    describe "with no legacy CCs" do
      before do
        closed_ticket.collaborations.create(collaborator_type: CollaboratorType.FOLLOWER, user_id: admin.id)
        closed_ticket.collaborations.create(collaborator_type: CollaboratorType.EMAIL_CC, user_id: agent.id)
      end

      it { refute followup.legacy_ccs_present? }
    end
  end
end
