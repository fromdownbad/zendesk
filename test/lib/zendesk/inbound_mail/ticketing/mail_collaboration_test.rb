require_relative "../../../../support/test_helper"
require_relative "../../../../support/agent_test_helper"
require_relative "../../../../support/collaboration_settings_test_helper"
require_relative "../../../../../lib/zendesk/inbound_mail/ticketing/mail_collaboration_update_types"

SingleCov.covered!

# Done this way to get around cleanliness test requirement that no constants be defined.
# It also helps to build a blacklist approach on specific types that don't match the commonality.
class AllUpdateTypes
  def self.update_types
    [
      Zendesk::InboundMail::Ticketing::LegacyCcsUpdateType,
      Zendesk::InboundMail::Ticketing::NoopUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewAgentsUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewAgentsAsEmailCcsUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewAgentsAsFollowersUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewAsEmailCcsUpdateType,
      Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType,
      Zendesk::InboundMail::Ticketing::AddAuthorAsFollowerUpdateType,
      Zendesk::InboundMail::Ticketing::AddAuthorAsEmailCcUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewEndUsersUpdateType,
      Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsEmailCcsUpdateType,
      Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsFollowersUpdateType
    ]
  end
end

describe Zendesk::InboundMail::Ticketing::MailCollaboration do
  include AgentTestHelper

  fixtures :accounts, :groups, :tickets, :users

  let(:account) { accounts(:minimum) }
  let(:group) { groups(:minimum_group) }
  let(:ticket) { tickets(:minimum_1) }

  let(:submitter) { users(:minimum_author) }
  let(:ccd_user) { users(:minimum_end_user) }
  let(:mail_address) { Zendesk::Mail::AddressParser.parse(ccd_user.email).address }
  let(:mail_collaboration_class) { Zendesk::InboundMail::Ticketing::MailCollaboration }

  let(:update_type_class) { Zendesk::InboundMail::Ticketing::NoopUpdateType }
  let(:update_type) { update_type_class.new }

  let(:email_cc_count) { 0 }
  let(:new_users) { [] }
  let(:logger) { Rails.logger }

  let(:mail_collaboration) do
    mail_collaboration_class.new(
      account, ticket, mail_address, update_type, email_cc_count, new_users, logger
    )
  end

  def create_email_cc(user, ticket)
    ticket.collaborations.build(
      user: user, ticket: ticket, collaborator_type: CollaboratorType.EMAIL_CC
    )
  end

  def create_follower(user, ticket)
    ticket.collaborations.build(
      user: user, ticket: ticket, collaborator_type: CollaboratorType.FOLLOWER
    )
  end

  # Unit-tests for class methods:

  describe ".build_all" do
    let(:submitter) { ticket.current_user }
    let(:recovering_suspended_ticket) { false }
    let(:requester_is_on_email) { false }

    subject do
      mail_collaboration_class.build_all(
        ticket,
        collaborator_addresses: [mail_address],
        requester_is_on_email: requester_is_on_email,
        logger: Rails.logger,
        recovering_suspended_ticket: recovering_suspended_ticket
      )
    end
    before { ticket.will_be_saved_by(ticket.requester) }

    it 'executes with an empty set of collaborator addresses' do
      subject.must_be_empty
    end

    describe 'when the update type has adds_only_author action' do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddAuthorAsFollowerUpdateType }
      before { mail_collaboration_class.stubs(mail_collaboration_update_type: update_type) }

      it { subject.must_be_empty }

      it 'adds the submitter as the author of the update' do
        assert update_type.adds_only_author?
        mail_collaboration_class.expects(:add_author_for_update_type).once
        subject.must_be_empty
      end

      describe 'and the update type has `edits_email_ccs` action' do
        let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddAuthorAsEmailCcUpdateType }

        it 'adds any new agent ccs as followers' do
          assert update_type.adds_only_author?
          mail_collaboration_class.expects(:add_new_agent_ccs_as_followers).once
          subject.must_be_empty
        end
      end
    end

    describe "when a suspended ticket is being recovered and the related ticket already has CCs" do
      let(:recovering_suspended_ticket) { true }
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType }
      let(:already_ccd_user) { users(:minimum_search_user) }
      let(:requester_is_on_email) { true }

      before do
        CollaborationSettingsTestHelper.enable_all_new_collaboration_settings
        mail_collaboration_class.stubs(mail_collaboration_update_type: update_type)
        create_email_cc(already_ccd_user, ticket)
        ticket.will_be_saved_by(ticket.requester)
        ticket.save!
      end

      describe "and the update type starts out as ResetEmailCcsUpdateType" do
        it "does not remove the existing CCs" do
          subject
          ticket.email_ccs.map(&:id).must_include already_ccd_user.id
        end

        it "adds new CCs as email CCs" do
          subject
          ticket.email_ccs.map(&:id).must_include ccd_user.id
        end
      end

      describe "and the update type does not start out as ResetEmailCcsUpdateType" do
        let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddAuthorAsEmailCcUpdateType }

        it "makes the changes of the original update type" do
          subject
          ticket.email_ccs.map(&:id).must_include already_ccd_user.id
          ticket.email_ccs.map(&:id).must_include submitter.id
          ticket.email_ccs.map(&:id).wont_include ccd_user.id
        end
      end
    end

    describe 'having collaborator addresses' do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAgentsAsEmailCcsUpdateType }
      let(:assignee) { ticket.assignee }
      let(:collaborator_addresses) do
        [
          { address: assignee.email,   name: assignee.name },
          { address: submitter.email,  name: submitter.name },
          { address: 'cc1@real.com', name: 'New Collaborator 1' },
        ].map do |collaborator|
          Zendesk::Mail::Address.new(collaborator)
        end
      end

      subject do
        mail_collaboration_class.build_all(
          ticket, logger: logger, collaborator_addresses: collaborator_addresses
        )
      end

      describe 'when the collaboration creation is deferred' do
        let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType }
        before { mail_collaboration_class.stubs(mail_collaboration_update_type: update_type) }

        it 'updates the collaborations' do
          assert update_type.defers_collaboration_creation?
          mail_collaboration_class.expects(:update_collaborations).once
          subject
        end
      end
    end
  end

  describe ".submitter_has_update_permission?" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType }

    subject do
      mail_collaboration_class.submitter_has_update_permission?(
        ticket, submitter, update_type, logger
      )
    end

    describe "when update_type is FOLLOWER_AND_EMAIL_CCS_DISABLED" do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::LegacyCcsUpdateType }

      it "returns true when given user can add collaborators to the given ticket" do
        User.any_instance.stubs(:can?).with(:add_collaborator, ticket).returns(true)
        assert subject
      end

      it "returns false when given user cannot add collaborators to the given ticket" do
        User.any_instance.stubs(:can?).with(:add_collaborator, ticket).returns(false)
        refute subject
      end
    end

    describe "when update_type edits email CCs" do
      before do
        update_type_class.any_instance.stubs(edits_email_ccs?: true)
        User.any_instance.stubs(:can?).with(:add_follower, ticket).returns(false)
      end

      it "returns true when the given user can add email CCs to the given ticket" do
        User.any_instance.stubs(:can?).with(:add_email_cc, ticket).returns(true)
        assert subject
      end

      it "returns false when given user cannot add email CCs to the given ticket" do
        User.any_instance.stubs(:can?).with(:add_email_cc, ticket).returns(false)
        refute subject
      end
    end

    describe "when update_type edits followers" do
      before do
        update_type_class.any_instance.stubs(edits_followers?: true)
        User.any_instance.stubs(:can?).with(:add_email_cc, ticket).returns(false)
      end

      it "returns true when given user can add followers to the given ticket" do
        User.any_instance.stubs(:can?).with(:add_follower, ticket).returns(true)
        assert subject
      end

      it "returns false when given user cannot add followers to the given ticket" do
        User.any_instance.stubs(:can?).with(:add_follower, ticket).returns(false)
        refute subject
      end
    end
  end

  describe ".resetting_email_ccs?" do
    subject { mail_collaboration_class.resetting_email_ccs?(update_type) }

    describe "when update type is ResetEmailCcsUpdateType" do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType }
      it { assert subject }
    end

    describe "when update type is AddAuthorAsFollowerUpdateType" do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddAuthorAsFollowerUpdateType }
      it { refute subject }
    end
  end

  describe ".attempting_to_add_as_new_email_cc?" do
    let(:end_user) { users(:minimum_end_user) }
    let(:agent) { users(:minimum_agent) }
    let(:settings) do
      {
        has_email_ccs_light_agents_v2: ticket.account.has_email_ccs_light_agents_v2?,
        has_light_agent_email_ccs_allowed_enabled: ticket.account.has_light_agent_email_ccs_allowed_enabled?
      }
    end

    describe "when attempting to add an agent as a legacy CC" do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType }

      it { refute mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, agent, settings) }
    end

    describe "when attempting to add legacy CCs with email_ccs feature disabled" do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::LegacyCcsUpdateType }
      it { refute mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, agent, settings) }
      it { refute mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, end_user, settings) }
    end
    # AddNewAsEmailCcsUpdateType
    describe "when attempting to add email CCs with email_ccs feature enabled" do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAsEmailCcsUpdateType }

      describe "when user is a light agent" do
        let(:agent) { create_light_agent }
        it { refute mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, agent, settings) }
      end

      describe "when user cannot publicly comment (most likely a light agent)" do
        before { agent.stubs(:can?).with(:publicly, Comment).returns(false) }
        it { refute mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, agent, settings) }
      end

      describe "when user can be added as an email cc" do
        it { assert mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, agent, settings) }
        it { assert mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, end_user, settings) }
      end

      describe "when end user was a legacy CC" do
        let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType }
        it { assert mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, end_user, settings) }
      end

      describe_with_arturo_setting_enabled :light_agent_email_ccs_allowed do
        describe "when user is a light agent" do
          let(:agent) { create_light_agent }
          it { assert mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, agent, settings) }
        end

        describe "when user cannot publicly comment" do
          before { agent.stubs(:can?).with(:publicly, Comment).returns(false) }
          it { assert mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, agent, settings) }
        end

        describe "when user can be added as an email cc" do
          it { assert mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, agent, settings) }
          it { assert mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, end_user, settings) }
        end

        describe "when end user was a legacy CC" do
          let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType }
          it { assert mail_collaboration_class.attempting_to_add_as_new_email_cc?(update_type, end_user, settings) }
        end
      end
    end
  end

  describe ".mail_collaboration_rules" do
    subject { mail_collaboration_class.mail_collaboration_rules(ticket) }

    describe_with_arturo_disabled("email_collaboration_by_light_agents_fixed") do
      it "defines the correct order of rules" do
        assert_equal [
          Zendesk::InboundMail::Ticketing::LegacyCcs,                                             # 01
          Zendesk::InboundMail::Ticketing::FollowersAndEmailCcsSettingsDisabled,                  # 02
          Zendesk::InboundMail::Ticketing::NewTicket,                                             # 03
          Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssigneeWithRequesterOnEmail,    # 04
          Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssigneeWithRequesterNotOnEmail, # 05
          Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithPrivateComment,                  # 06
          Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithOnlyFollowersAllowed,            # 07
          Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithOnlyEmailCcsAllowed,             # 08
          Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithFollowersAndCcsAllowed,          # 09
          Zendesk::InboundMail::Ticketing::EmailCcOrRequesterAndRequesterIsOnEmail,               # 10
          Zendesk::InboundMail::Ticketing::EmailCcOrRequesterAndSubmitterIsAgent,                 # 11
          Zendesk::InboundMail::Ticketing::EmailCcOrRequesterNoop,                                # 12
          Zendesk::InboundMail::Ticketing::AssignedGroupAgent,                                    # 13
          Zendesk::InboundMail::Ticketing::SubmitterIsAgentAndRequesterIsOnEmail,                 # 14
          Zendesk::InboundMail::Ticketing::SubmitterIsAgentAndFollowersIsEnabled,                 # 15
          Zendesk::InboundMail::Ticketing::SubmitterIsAgentAndEmailCcsIsEnabled,                  # 16
          Zendesk::InboundMail::Ticketing::SubmitterIsLightAgent,                                 # 17
          Zendesk::InboundMail::Ticketing::SubmitterIsEndUser,                                    # 18
          Zendesk::InboundMail::Ticketing::Noop                                                   # 19
        ], subject
      end
    end

    describe_with_arturo_enabled("email_collaboration_by_light_agents_fixed") do
      it "defines the correct order of rules" do
        assert_equal [
          Zendesk::InboundMail::Ticketing::LegacyCcs,                                             # 01
          Zendesk::InboundMail::Ticketing::FollowersAndEmailCcsSettingsDisabled,                  # 02
          Zendesk::InboundMail::Ticketing::NewTicket,                                             # 03
          Zendesk::InboundMail::Ticketing::SubmitterIsLightAgent,                                 # 04
          Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssigneeWithRequesterOnEmail,    # 05
          Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssigneeWithRequesterNotOnEmail, # 06
          Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithPrivateComment,                  # 07
          Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithOnlyFollowersAllowed,            # 08
          Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithOnlyEmailCcsAllowed,             # 09
          Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithFollowersAndCcsAllowed,          # 10
          Zendesk::InboundMail::Ticketing::EmailCcOrRequesterAndRequesterIsOnEmail,               # 11
          Zendesk::InboundMail::Ticketing::EmailCcOrRequesterAndSubmitterIsAgent,                 # 12
          Zendesk::InboundMail::Ticketing::EmailCcOrRequesterNoop,                                # 13
          Zendesk::InboundMail::Ticketing::AssignedGroupAgent,                                    # 14
          Zendesk::InboundMail::Ticketing::SubmitterIsAgentAndRequesterIsOnEmail,                 # 15
          Zendesk::InboundMail::Ticketing::SubmitterIsAgentAndFollowersIsEnabled,                 # 16
          Zendesk::InboundMail::Ticketing::SubmitterIsAgentAndEmailCcsIsEnabled,                  # 17
          Zendesk::InboundMail::Ticketing::SubmitterIsEndUser,                                    # 18
          Zendesk::InboundMail::Ticketing::Noop                                                   # 19
        ], subject
      end
    end
  end

  describe ".mail_collaboration_update_type" do
    let(:requester_is_on_email) { true }
    let(:subject) do
      mail_collaboration_class.mail_collaboration_update_type(
        ticket, submitter, requester_is_on_email, logger
      )
    end

    describe_with_arturo_setting_disabled(:follower_and_email_cc_collaborations) do
      it { assert_kind_of Zendesk::InboundMail::Ticketing::LegacyCcsUpdateType, subject }
    end

    describe_with_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
      let(:ticket_followers_allowed) { true }
      let(:comment_email_ccs_allowed) { true }

      before do
        Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: ticket_followers_allowed)
        Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: comment_email_ccs_allowed)
      end

      describe "when ticket_followers_allowed and comment_email_ccs_allowed settings are both disabled" do
        let(:ticket_followers_allowed) { false }
        let(:comment_email_ccs_allowed) { false }

        it { assert_kind_of Zendesk::InboundMail::Ticketing::NoopUpdateType, subject }
      end

      describe "when ticket is new" do
        let(:ticket) { Ticket.new(account: account) }

        before { assert ticket.new_record? }

        it { assert_kind_of Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType, subject }
      end

      describe "when ticket is persisted" do
        let(:ticket) { tickets(:minimum_1) }
        let(:is_public) { true }

        describe "when submitter is agent participant and existing email CC" do
          before { Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssignee.any_instance.stubs(submitter_is_agent_participant_and_email_cc?: true) }

          describe "when requester is on the email" do
            it { assert_kind_of Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType, subject }
          end

          describe "when requester is not on the email" do
            let(:requester_is_on_email) { false }

            it "uses add_new_agents_update_type_class" do
              Zendesk::InboundMail::Ticketing::AgentIsCcAndFollowerOrAssigneeWithRequesterNotOnEmail.any_instance.expects(add_new_agents_update_type_class: Zendesk::InboundMail::Ticketing::AddNewAgentsAsFollowersUpdateType)
              subject
            end
          end
        end

        describe "when submitter is a follower of the ticket" do
          let(:submitter) { users(:minimum_agent) }

          before do
            create_follower(submitter, ticket)

            ticket.will_be_saved_by(submitter)
            ticket.add_comment(body: "This is a new comment", is_public: is_public, author: submitter)
          end

          describe "when the comment created by the email is private" do
            let(:is_public) { false }

            it "uses add_new_agents_update_type_class" do
              Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithPrivateComment.any_instance.expects(add_new_agents_update_type_class: Zendesk::InboundMail::Ticketing::AddNewAgentsAsFollowersUpdateType)
              subject
            end
          end

          describe "when the comment created by the email is public" do
            describe "with ticket_followers_allowed disabled and comment_email_ccs_allowed enabled" do
              let(:ticket_followers_allowed) { false }
              it { assert_kind_of Zendesk::InboundMail::Ticketing::AddNewAsEmailCcsUpdateType, subject }
            end

            describe "with ticket_followers_allowed enabled and comment_email_ccs_allowed disabled" do
              let(:comment_email_ccs_allowed) { false }
              it { assert_kind_of Zendesk::InboundMail::Ticketing::AddNewAgentsAsFollowersUpdateType, subject }
            end

            describe "with ticket_followers_allowed and comment_email_ccs_allowed enabled" do
              it { assert_kind_of Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType, subject }
            end
          end
        end

        describe "when submitter is the assignee on the ticket" do
          before do
            ticket.stubs(:assignee).returns(submitter)
            ticket.will_be_saved_by(submitter)
            ticket.add_comment(body: "This is a new comment", is_public: is_public, author: submitter)
          end

          describe "when the comment created by the email is private" do
            let(:is_public) { false }

            it "uses add_new_agents_update_type_class" do
              Zendesk::InboundMail::Ticketing::FollowerOrAssigneeWithPrivateComment.any_instance.expects(add_new_agents_update_type_class: Zendesk::InboundMail::Ticketing::AddNewAgentsAsFollowersUpdateType)
              subject
            end
          end

          describe "when the comment created by the email is public" do
            describe "with ticket_followers_allowed disabled and comment_email_ccs_allowed enabled" do
              let(:ticket_followers_allowed) { false }
              it { assert_kind_of Zendesk::InboundMail::Ticketing::AddNewAsEmailCcsUpdateType, subject }
            end

            describe "with ticket_followers_allowed enabled and comment_email_ccs_allowed disabled" do
              let(:comment_email_ccs_allowed) { false }
              it { assert_kind_of Zendesk::InboundMail::Ticketing::AddNewAgentsAsFollowersUpdateType, subject }
            end

            describe "with ticket_followers_allowed and comment_email_ccs_allowed enabled" do
              it { assert_kind_of Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType, subject }
            end
          end
        end

        describe "when submitter is an email CC on the ticket" do
          before { create_email_cc(submitter, ticket) }

          describe "when requester is on the email" do
            it { assert_kind_of Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType, subject }
          end

          describe "when requester is not on the email" do
            let(:requester_is_on_email) { false }

            describe "when submitter is an agent but not participating any other way" do
              let(:ticket) { tickets(:minimum_2) }
              let(:submitter) { users(:minimum_agent) }
              it { assert_kind_of Zendesk::InboundMail::Ticketing::AddNewAgentsUpdateType, subject }
            end

            describe "when submitter is an end user not participating any other way" do
              let(:submitter) { users(:minimum_end_user) }
              it { assert_kind_of Zendesk::InboundMail::Ticketing::NoopUpdateType, subject }
            end
          end
        end

        describe "when submitter is the requester on the ticket" do
          before { Ticket.any_instance.stubs(:requested_by?).with(submitter).returns(true) }
          it { assert_kind_of Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType, subject }
        end

        describe "when submitter is an agent not participating any other way" do
          let(:ticket) { tickets(:minimum_2) }
          let(:submitter) { FactoryBot.create(:agent, account: ticket.account) }

          describe "when the ticket is assigned to the agent's group" do
            before do
              ticket.stubs(:group_includes?).with(submitter).returns(true)
            end

            it { assert_kind_of Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsFollowersUpdateType, subject }
          end

          describe "when requester is on the email" do
            it { assert_kind_of Zendesk::InboundMail::Ticketing::AddAuthorAsEmailCcUpdateType, subject }
          end

          describe "when requester is not on the email" do
            let(:requester_is_on_email) { false }
            it { assert_kind_of Zendesk::InboundMail::Ticketing::AddAuthorAsFollowerUpdateType, subject }

            describe "with ticket_followers_allowed disabled" do
              let(:ticket_followers_allowed) { false }
              it { assert_kind_of Zendesk::InboundMail::Ticketing::AddAuthorAsEmailCcUpdateType, subject }
            end
          end
        end

        describe "when submitter is an end user not participating any other way" do
          let(:submitter) { users(:minimum_end_user) }

          describe_with_arturo_enabled :email_make_other_user_comments_private do
            it { assert_kind_of Zendesk::InboundMail::Ticketing::NoopUpdateType, subject }
          end

          describe_with_arturo_disabled :email_make_other_user_comments_private do
            it { assert_kind_of Zendesk::InboundMail::Ticketing::AddAuthorAsEmailCcUpdateType, subject }
          end
        end
      end
    end
  end

  describe ".set_send_verify_email" do
    let(:saved_new_user) { users(:minimum_end_user) }
    let(:saved_new_users) { [saved_new_user] }

    def assert_send_verify_email
      User.any_instance.expects(:send_verify_email=)
      mail_collaboration_class.set_send_verify_email(ticket, saved_new_users)
    end

    def refute_send_verify_email
      User.any_instance.expects(:send_verify_email=).never
      mail_collaboration_class.set_send_verify_email(ticket, saved_new_users)
    end

    describe "when the ticket has no collaborations" do
      it { refute_send_verify_email }
    end

    describe "when the ticket has a collaboration with a saved new user" do
      before do
        create_email_cc(saved_new_user, ticket)
      end

      describe "when no saved_new_users are given" do
        let(:saved_new_users) { [] }
        it { refute_send_verify_email }
      end

      describe "when given saved_new_users contains the ticket's collaboration's user" do
        it { assert_send_verify_email }
      end

      describe "when given saved_new_users does not contain the ticket's collaboration's user" do
        let(:saved_new_users) { [users(:minimum_search_user)] }
        it { refute_send_verify_email }
      end
    end
  end

  describe ".add_author_for_update_type" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddAuthorAsEmailCcUpdateType }
    let(:update_type) { update_type_class.new }
    let(:expected_collaborator_type) { CollaboratorType.EMAIL_CC }

    subject do
      mail_collaboration_class.add_author_for_update_type(
        ticket, submitter, update_type, email_cc_count
      )
    end

    def assert_add_author_for_update_type
      Ticket.any_instance.expects(:additional_collaborators_with_type).with([submitter.id], expected_collaborator_type)
      subject
    end

    def refute_add_author_for_update_type
      Ticket.any_instance.expects(:additional_collaborators_with_type).never
      subject
    end

    describe 'when is already over the limit' do
      let(:email_cc_count) { 100 }

      it 'logs the error and aborts' do
        logger.expects(:warn).with("Unable to add author as an email CC, maximum of #{Ticket::MAX_COLLABORATORS_V2} email CCs reached")
        refute subject
      end

      it { refute_add_author_for_update_type }
    end

    describe 'when update type is AddAuthorAsEmailCcUpdateType' do
      it { assert_add_author_for_update_type }
    end

    describe 'when update_type is AddAuthorAsFollowerUpdateType' do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddAuthorAsFollowerUpdateType }
      let(:expected_collaborator_type) { CollaboratorType.FOLLOWER }
      it { assert_add_author_for_update_type }
    end

    describe 'when update_type is ResetEmailCcsUpdateType' do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType }
      it { refute_add_author_for_update_type }
    end
  end

  describe ".update_collaborations" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::LegacyCcsUpdateType }
    let(:update_type) { update_type_class.new }
    let(:collaborator) { users(:minimum_author) }
    let(:light_agent_collaborator) { create_light_agent }
    let(:collaborator_ids) { { can_comment_publicly: [collaborator.id], can_comment_privately: [light_agent_collaborator.id] } }
    let(:all_collaborator_ids) { collaborator_ids[:can_comment_publicly] + collaborator_ids[:can_comment_privately] }
    let(:saved_new_user) { User.new(account: account, name: "SavedNewUser", email: "saved_new_user@example.com") }
    let(:saved_new_users) { [saved_new_user] }
    let(:expected_collaborator_type) { CollaboratorType.EMAIL_CC }

    def call_update_collaborations
      mail_collaboration_class.update_collaborations(ticket, update_type, collaborator_ids, saved_new_users, logger)
    end

    def assert_additional_collaborators_with_type
      Ticket.any_instance.expects(:additional_collaborators_with_type).with(all_collaborator_ids, expected_collaborator_type)
      call_update_collaborations
    end

    it "calls set_send_verify_email" do
      mail_collaboration_class.expects(:set_send_verify_email).with(ticket, saved_new_users)
      call_update_collaborations
    end

    it "calls add_new_agent_ccs_as_followers" do
      mail_collaboration_class.expects(:add_new_agent_ccs_as_followers).with(ticket, logger)
      call_update_collaborations
    end

    describe "when update_type is ResetEmailCcsUpdateType" do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType }

      it "calls set with email CCs and additional with Followers" do
        Ticket.any_instance.expects(:set_collaborators_with_type).
          with(collaborator_ids[:can_comment_publicly], CollaboratorType.EMAIL_CC)
        Ticket.any_instance.expects(:additional_collaborators_with_type).
          with(collaborator_ids[:can_comment_privately], CollaboratorType.FOLLOWER)
        call_update_collaborations
      end

      describe "with no collaborators that can comment publicly" do
        let(:collaborator_ids) { { can_comment_publicly: [], can_comment_privately: [light_agent_collaborator.id] } }

        it "resets email CCs and calls additional with Followers" do
          Ticket.any_instance.expects(:set_collaborators_with_type).
            with(collaborator_ids[:can_comment_publicly], CollaboratorType.EMAIL_CC)
          Ticket.any_instance.expects(:additional_collaborators_with_type).
            with(collaborator_ids[:can_comment_privately], CollaboratorType.FOLLOWER)
          call_update_collaborations
        end
      end
    end

    describe "when update_type is AddNewAsLegacyUpdateType" do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType }
      let(:expected_collaborator_type) { CollaboratorType.LEGACY_CC }

      it { assert_additional_collaborators_with_type }
    end

    [
      Zendesk::InboundMail::Ticketing::AddNewAsEmailCcsUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewEndUsersUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewAgentsAsEmailCcsUpdateType
    ].each do |current_update_type_class|
      describe "when update_type_class is #{current_update_type_class}" do
        let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAsEmailCcsUpdateType }

        it "calls appropriate collaborator setters" do
          Ticket.any_instance.expects(:additional_collaborators_with_type).
            with(collaborator_ids[:can_comment_publicly], CollaboratorType.EMAIL_CC)
          Ticket.any_instance.expects(:additional_collaborators_with_type).
            with(collaborator_ids[:can_comment_privately], CollaboratorType.FOLLOWER)
          call_update_collaborations
        end
      end
    end

    describe "when update_type is AddNewAgentsAsFollowersUpdateType" do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAgentsAsFollowersUpdateType }
      let(:expected_collaborator_type) { CollaboratorType.FOLLOWER }

      it { assert_additional_collaborators_with_type }
    end
  end

  describe ".add_new_agent_ccs_as_followers" do
    subject { mail_collaboration_class.add_new_agent_ccs_as_followers(ticket, logger) }

    describe_with_arturo_setting_disabled :agent_email_ccs_become_followers do
      it "does not do anything" do
        mail_collaboration_class.expects(:new_agent_email_ccs).never
        refute subject
      end
    end

    describe_with_arturo_setting_enabled :agent_email_ccs_become_followers do
      let(:new_agent_email_cc) { users(:minimum_admin) }

      before do
        mail_collaboration_class.stubs(:new_non_follower_agent_email_ccs).with(ticket).returns([new_agent_email_cc])
      end

      def ticket_includes_follower_collaboration_for_user(user)
        ticket.collaborations.any? do |collaboration|
          collaboration.user_id == user.id && collaboration.collaborator_type == CollaboratorType.FOLLOWER
        end
      end

      it "adds a new follower collaboration for users returned by new_non_follower_agent_email_ccs" do
        refute ticket_includes_follower_collaboration_for_user(new_agent_email_cc)
        subject
        assert ticket_includes_follower_collaboration_for_user(new_agent_email_cc)
      end
    end
  end

  describe ".new_non_follower_agent_email_ccs" do
    subject { mail_collaboration_class.new_non_follower_agent_email_ccs(ticket) }

    describe "with a ticket" do
      let(:persisted_follower) { users(:minimum_admin) }
      let(:new_follower) { users(:minimum_admin_not_owner) }
      let(:new_end_user_email_cc) { users(:minimum_end_user) }
      let(:new_agent_email_cc) { users(:minimum_admin_not_verified) }

      before do
        # Followers must be enabled or the persisted follower will be created as LEGACY_CC
        CollaborationSettingsTestHelper.enable_all_new_collaboration_settings

        create_follower(persisted_follower, ticket)
        ticket.will_be_saved_by(ticket.assignee)
        ticket.save!

        create_follower(new_follower, ticket)
        create_email_cc(new_end_user_email_cc, ticket)
        create_email_cc(new_agent_email_cc, ticket)
      end

      it { refute subject.include?(persisted_follower) }
      it { refute subject.include?(new_follower) }
      it { refute subject.include?(new_end_user_email_cc) }
      it { assert subject.include?(new_agent_email_cc) }
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      describe "with a closed ticket" do
        let(:closed_ticket) { tickets(:minimum_2) }

        let(:existing_follower) { users(:minimum_admin) }
        let(:existing_agent_email_cc) { users(:minimum_admin_not_owner) }
        let(:new_end_user_email_cc) { users(:minimum_end_user) }
        let(:new_agent_email_cc) { users(:minimum_admin_not_verified) }

        before do
          closed_ticket.collaborations.build(ticket: ticket, user: existing_follower, collaborator_type: CollaboratorType.FOLLOWER)
          closed_ticket.collaborations.build(ticket: ticket, user: existing_agent_email_cc, collaborator_type: CollaboratorType.EMAIL_CC)
          closed_ticket.will_be_saved_by(closed_ticket.assignee)
          closed_ticket.save!

          ticket.stubs(followup_source: closed_ticket)
          ticket.via_id = ViaType.CLOSED_TICKET
        end

        describe "with a new followup ticket" do
          before do
            ticket.collaborations.build(ticket: ticket, user: existing_follower, collaborator_type: CollaboratorType.FOLLOWER)
            ticket.collaborations.build(ticket: ticket, user: existing_agent_email_cc, collaborator_type: CollaboratorType.EMAIL_CC)
            ticket.collaborations.build(ticket: ticket, user: new_end_user_email_cc, collaborator_type: CollaboratorType.EMAIL_CC)
            ticket.collaborations.build(ticket: ticket, user: new_agent_email_cc, collaborator_type: CollaboratorType.EMAIL_CC)
            ticket.stubs(new_record?: true)
          end

          it { refute subject.include?(existing_follower) }
          it { refute subject.include?(existing_agent_email_cc) }
          it { refute subject.include?(new_end_user_email_cc) }
          it { assert subject.include?(new_agent_email_cc) }
        end

        describe "with an existing followup ticket" do
          before do
            ticket.collaborations.build(ticket: ticket, user: existing_follower, collaborator_type: CollaboratorType.FOLLOWER)
            ticket.collaborations.build(ticket: ticket, user: existing_agent_email_cc, collaborator_type: CollaboratorType.EMAIL_CC)
            ticket.will_be_saved_by(ticket.assignee)
            ticket.save!

            ticket.collaborations.build(ticket: ticket, user: new_end_user_email_cc, collaborator_type: CollaboratorType.EMAIL_CC)
            ticket.collaborations.build(ticket: ticket, user: new_agent_email_cc, collaborator_type: CollaboratorType.EMAIL_CC)
          end

          it { refute subject.include?(existing_follower) }
          it { refute subject.include?(existing_agent_email_cc) }
          it { refute subject.include?(new_end_user_email_cc) }
          it { assert subject.include?(new_agent_email_cc) }
        end
      end
    end
  end

  # Unit-tests for instance methods:

  describe '#collaborator_info' do
    subject { mail_collaboration.send(:collaborator_info) }

    describe 'for persisted users' do
      it { assert_equal "#{ccd_user.id} #{ccd_user.identities.map(&:id)}", subject }
    end

    describe 'for new users' do
      before { mail_collaboration.stubs(user: User.new) }
      it { assert_equal 'new', subject }
    end
  end

  describe '#build' do
    subject { mail_collaboration.send(:build) }

    it 'builds a new collaboration using the user and ticket' do
      ticket.collaborations.expects(:build).with(user: ccd_user, ticket: ticket).once
      subject
    end

    it 'builds a new collaboration' do
      assert_empty ticket.collaborations
      subject
      assert_equal 1, ticket.collaborations.size
    end

    describe_with_arturo_enabled :email_fix_current_collaborators_mail_collaboration do
      it 'does not add the user to the current collaborators list' do
        subject
        assert ticket.current_collaborators.blank?
      end
    end

    describe_with_arturo_disabled :email_fix_current_collaborators_mail_collaboration do
      it 'adds the user to the current collaborators list' do
        subject
        assert_equal ccd_user.name.to_s, ticket.current_collaborators
      end
    end
  end

  describe '#add_or_skip' do
    describe_with_arturo_setting_enabled :light_agent_email_ccs_allowed do
      let(:user_address) { 'New Collaborator <collaborator@new.com>' }
      let(:mail_address) { Zendesk::Mail::AddressParser.parse(user_address).address }
      let(:new_users) { [] }
      let(:collaborator_ids) { {} }
      let(:has_email_ccs_light_agents_v2) { ticket.account.has_email_ccs_light_agents_v2? }
      let(:has_light_agent_email_ccs_allowed_enabled) { ticket.account.has_light_agent_email_ccs_allowed_enabled? }
      let(:settings) do
        {
          has_email_ccs_light_agents_v2: has_email_ccs_light_agents_v2,
          has_light_agent_email_ccs_allowed_enabled: has_light_agent_email_ccs_allowed_enabled
        }
      end

      before { logger.stubs(:info) }

      subject { mail_collaboration.send(:add_or_skip, ticket, collaborator_ids, settings) }

      describe 'when the mail collaboration is valid' do
        let(:collaborator_ids) { { can_comment_publicly: [submitter.id] } }

        before { mail_collaboration.stubs(account_allows_collaboration?: true) }

        describe 'but there is an unexpected error ' do
          let(:error) { StandardError.new('Woops!') }
          before { UserIdentity.stubs(:uncached).raises(error) }

          it 'logs the error and returns false' do
            logger.expects(:info).with("Rescuing other exception from attempt to save collaborator #{mail_address.address}: #{error.message}")
            refute subject
          end
        end

        describe 'but there is an expected error while saving' do
          let(:error) { ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation.new('Woops!') }
          before { UserIdentity.stubs(:uncached).raises(error) }

          it 'logs the error and tries to repair the user' do
            logger.expects(:info).with("Rescuing expected exception from attempt to save collaborator #{mail_address.address}: #{error.message}")
            mail_collaboration.expects(:repair_user).once
            subject
          end
        end

        describe 'when the collaboration creation is NOT deferred' do
          let(:update_type_class) { Zendesk::InboundMail::Ticketing::LegacyCcsUpdateType }

          it 'logs the message' do
            logger.expects(:info).with("Adding collaborator #{mail_address.address} #{mail_collaboration.collaborator_info}")
            mail_collaboration.expects(:build).once
            assert subject
          end

          it 'builds the collaboration' do
            assert_equal 0, ticket.collaborations.size
            assert subject
            assert_equal 1, ticket.collaborations.size
          end
        end

        describe 'when the collaboration creation is deferred' do
          let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType }

          it { assert subject }

          it "adds the collaboration user to collaborator_ids[:can_comment_publicly]" do
            subject
            assert_equal [submitter.id, mail_collaboration.user.id], collaborator_ids[:can_comment_publicly]
          end

          describe 'when the collaboration user is a light agent' do
            let!(:light_agent) { create_light_agent }
            let(:user_address) { light_agent.email }

            it { assert subject }

            it "adds the collaboration user to collaborator_ids[:can_comment_publicly]" do
              subject
              assert collaborator_ids[:can_comment_publicly].include?(light_agent.id)
            end

            it "does not populate collaborator_ids[:can_comment_privately]" do
              subject
              assert_nil collaborator_ids[:can_comment_privately]
            end

            describe_with_arturo_disabled :email_ccs_light_agents_v2 do
              let(:has_email_ccs_light_agents_v2) { false }

              it { assert subject }

              it "adds the collaboration user to collaborator_ids[:can_comment_privately]" do
                subject
                assert_equal [light_agent.id], collaborator_ids[:can_comment_privately]
              end

              it "does not add the collaboration user to collaborator_ids[:can_comment_publicly]" do
                subject
                refute collaborator_ids[:can_comment_publicly].include?(light_agent.id)
              end
            end
          end
        end
      end

      describe 'when the mail collaboration is invalid' do
        it 'logs the errors and returns false' do
          logger.expects(:info).with("Skipping collaborator #{mail_address.address} due to collaboration errors: [:collaboration_not_allowed]")
          refute subject
        end

        describe 'when the collaborator is the current user and errors include a potential_netsuite_cc_loop' do
          before do
            mail_collaboration.stubs(errors: [:potential_netsuite_cc_loop])
            ticket.stubs(current_user: mail_collaboration.user)
          end

          it 'raises an exception and stops adding the collaboration' do
            assert_raises(ArgumentError, 'Submitter is invalid collaborator') do
              subject
            end
          end
        end
      end
    end
  end

  describe '#replace_matching_user' do
    let(:mail_address) { Zendesk::Mail::AddressParser.parse(submitter.email).address }

    # It checks if the other user has any email identity similar to the current user
    describe_with_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
      it 'does not replace the current user' do
        refute_equal mail_collaboration.user,
          mail_collaboration.send(:replace_matching_user, users(:minimum_end_user))
      end

      it 'replaces the current user with the other_user' do
        assert_equal mail_collaboration.user,
          mail_collaboration.send(:replace_matching_user, submitter)
      end
    end

    # It checks if the other user has the same primary email than the current user
    describe_with_arturo_setting_disabled(:follower_and_email_cc_collaborations) do
      it 'does not replace the current user' do
        refute_equal mail_collaboration.user,
          mail_collaboration.send(:replace_matching_user, users(:minimum_end_user))
      end

      it 'replaces the current user with the other_user' do
        assert_equal mail_collaboration.user,
          mail_collaboration.send(:replace_matching_user, submitter)
      end
    end
  end

  # Unit-tests for protected methods:

  describe '#exceeds_limit?' do
    subject { mail_collaboration.send(:exceeds_limit?) }

    describe 'and the email_cc_count is >= Ticket::MAX_COLLABORATORS_V2' do
      let(:email_cc_count) { Ticket::MAX_COLLABORATORS_V2 }
      it { assert subject }
    end

    describe 'and the email_cc_count is < Ticket::MAX_COLLABORATORS_V2' do
      let(:email_cc_count) { Ticket::MAX_COLLABORATORS_V2 - 1 }
      it { refute subject }
    end

    describe 'when the update type is LegacyCcsUpdateType' do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::LegacyCcsUpdateType }

      # Simulate that this account only allows 2 collaborators
      before { account.settings.stubs(collaborators_maximum: 2) }

      describe 'and the existing email_ccs are >= the maximum' do
        before do
          create_email_cc(submitter, ticket)
          create_email_cc(ccd_user, ticket)
        end

        it { assert subject }
      end

      describe 'and the existing email_ccs are < the maximum' do
        before { create_email_cc(submitter, ticket) }
        it { refute subject }
      end
    end
  end

  describe '#account_allows_collaboration?' do
    let(:user_address) { 'New Collaborator <collaborator@new.com>' }
    let(:mail_address) { Zendesk::Mail::AddressParser.parse(user_address).address }

    subject { mail_collaboration.send(:account_allows_collaboration?) }

    before do
      # Destroy all organizations so that organization whitelisting does
      # not impact the outcome of this test
      ticket.account.organizations.destroy_all
    end

    describe 'when address is found in the account domain_whitelist' do
      before { ticket.account.update_attributes(domain_whitelist: mail_address.domain) }

      it 'returns true' do
        assert subject
      end
    end

    describe 'when the address is not found in the account domain_whitelist and the account has no organizations with the same domain or email as the address' do
      before { ticket.account.update_attributes(domain_whitelist: '') }

      it 'returns false' do
        refute subject
      end
    end

    before do
      # Prevent the domain_whitelist from impacting the outcome of this test
      ticket.account.update_attributes(domain_whitelist: '')
      # Destroy all existing organizations so that they do not impact
      # the outcome of this test
      ticket.account.organizations.destroy_all
    end

    describe 'when the account has an organization with the same domain as the address' do
      before do
        ticket.account.organizations.create!(
          name: 'new-org',
          domain_names: [mail_address.domain]
        )
      end

      it 'returns true' do
        assert subject
      end
    end

    describe 'when the account has an organization with the same email as the address' do
      before do
        ticket.account.organizations.create!(
          name: 'new-org',
          domain_names: [mail_address.address]
        )
      end

      it 'returns true' do
        assert subject
      end
    end
  end

  describe '#collaborator_is_assignee?' do
    let(:assignee) { users(:minimum_agent) }
    let(:author) { users(:minimum_author) }

    subject { mail_collaboration.send(:collaborator_is_assignee?) }
    before { ticket.stubs(assignee: assignee) }

    describe_with_and_without_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
      describe "when the collaborator address belongs to the assignee" do
        let(:mail_address) { Zendesk::Mail::AddressParser.parse(assignee.email).address }
        it { assert subject }
      end

      describe "when the collaborator address doesn't belong to the assignee" do
        let(:mail_address) { Zendesk::Mail::AddressParser.parse(author.email).address }
        it { refute subject }
      end
    end
  end

  describe '#collaborator_is_requester?' do
    let(:user1) { users(:minimum_author) }
    let(:user2) { users(:minimum_end_user) }
    let(:mail_address) { Zendesk::Mail::AddressParser.parse(user_address).address }

    subject { mail_collaboration.send(:collaborator_is_requester?) }

    before { ticket.stubs(requester: requester) }

    describe_with_and_without_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
      describe "when the user's address matches the requester's default email address" do
        let(:requester) { user1 }
        let(:user_address) { user1.email }
        it { assert subject }
      end

      describe "when the user's address doesn't match the requester's default email address" do
        let(:requester) { user1 }
        let(:user_address) { user2.email }
        it { refute subject }
      end

      describe "when the user's address matches one of the requester's secondary email addresses" do
        let(:requester) { user1 }
        let(:user_address) { user1.identities.email.last.value }
        it { assert subject }
      end

      describe "when the user's address doesn't match one of the requester's secondary email addresses" do
        let(:requester) { user2 }
        let(:user_address) { user1.identities.email.last.value }
        it { refute subject }
      end
    end

    describe "when the requester includes other identities than UserEmailIdentity" do
      # This user has multiple identities, including a UserPhoneNumberIdentity
      let(:requester) { users(:minimum_search_user) }
      let(:user_address) { '+14155558888' }

      describe_with_arturo_setting_enabled(:follower_and_email_cc_collaborations) do
        # It excludes non-email identities for the matching.
        it { refute subject }
      end

      describe_with_arturo_setting_disabled(:follower_and_email_cc_collaborations) do
        # It matches (incorrectly) against non-email identities
        it { assert subject }
      end
    end
  end

  describe '#user_valid?' do
    let(:ccd_user) { users(:minimum_end_user) }
    let(:mail_address) { Zendesk::Mail::AddressParser.parse(ccd_user.email).address }
    subject { mail_collaboration.send(:user_valid?) }

    describe 'when the user is valid' do
      it { assert subject }
    end

    describe 'when the user is invalid' do
      before do
        mail_collaboration.user.errors.add(:email)
        mail_collaboration.user.stubs(valid?: false)
      end

      describe 'when debugging_mode is enabled' do
        before { ticket.account.stubs(has_email_debugging_mode?: true) }

        it 'loggs the errors' do
          logger.expects(:info).with('Collaboration user is invalid with errors: Email:  is invalid')
          refute subject
        end
      end

      it { refute subject }
    end
  end

  describe "#user_role_allowed?" do
    let(:account) { accounts(:minimum) }
    let(:ccd_user) { users(:minimum_end_user) }
    let(:mail_address) { Zendesk::Mail::AddressParser.parse(ccd_user.email).address }
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType }
    let(:update_type) { update_type_class.new }
    subject { mail_collaboration.send(:user_role_allowed?) }

    (AllUpdateTypes.update_types - [
      Zendesk::InboundMail::Ticketing::AddNewAgentsAsEmailCcsUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewAgentsAsFollowersUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewAgentsUpdateType,
      Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsEmailCcsUpdateType,
      Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsFollowersUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewEndUsersUpdateType
    ]).each do |current_update_type_class|
      describe "when update_type_class is #{current_update_type_class}" do
        let(:update_type_class) { current_update_type_class }

        describe "when email address belongs to a user that is an end user" do
          it { assert subject }
        end

        describe "when email address belongs to a user that is an agent" do
          let(:ccd_user) { users(:minimum_admin) }
          it { assert subject }
        end
      end
    end

    [
      Zendesk::InboundMail::Ticketing::AddNewAgentsAsEmailCcsUpdateType,
      Zendesk::InboundMail::Ticketing::AddNewAgentsUpdateType,
      Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsEmailCcsUpdateType,
      Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsFollowersUpdateType
    ].each do |current_update_type_class|
      describe "when update_type_class is #{current_update_type_class}" do
        let(:update_type_class) { current_update_type_class }

        describe "when email address belongs to a user that is an end user" do
          it { refute subject }
        end

        describe "when email address belongs to a user that is an agent" do
          let(:ccd_user) { users(:minimum_admin) }
          it { assert subject }
        end
      end
    end

    describe "when update_type is AddNewEndUsersUpdateType" do
      let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewEndUsersUpdateType }

      describe "when email address belongs to a user that is an end user" do
        it { assert subject }
      end

      describe "when email address belongs to a user that is an agent" do
        let(:ccd_user) { users(:minimum_admin) }
        it { refute subject }
      end
    end
  end

  describe "#excluded_due_to_group?" do
    let(:account) { accounts(:minimum) }
    let(:ccd_user) { users(:minimum_agent) }
    let(:mail_address) { Zendesk::Mail::AddressParser.parse(ccd_user.email).address }
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsFollowersUpdateType }
    let(:update_type) { update_type_class.new }
    subject { mail_collaboration.send(:excluded_due_to_group?) }

    describe_with_arturo_setting_disabled(:follower_and_email_cc_collaborations) do
      it { refute subject }
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      (AllUpdateTypes.update_types - [
        Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsFollowersUpdateType
      ]).each do |current_update_type_class|
        describe "when update_type_class is #{current_update_type_class}" do
          let(:update_type_class) { current_update_type_class }
          it { refute subject }
        end
      end

      describe "when the ticket has an agent assignee" do
        it { refute subject }
      end

      describe "when the ticket does not have an agent assignee" do
        let(:ccd_user) { users(:minimum_admin) }
        before { Ticket.any_instance.stubs(assignee: nil) }

        describe "when the email address does not belong to a member of the ticket's group" do
          before { ticket.stubs(:group_includes?).with(ccd_user).returns(false) }
          it { refute subject }
        end

        describe "when the email address belongs to a member of the ticket's group" do
          it { assert ticket.group_includes?(ccd_user) }
          it { assert mail_collaboration.send(:excluded_due_to_group?) }
        end
      end
    end
  end

  describe '#repair_user' do
    let(:existing_user) { users(:minimum_end_user) }
    let(:invalid_user) { existing_user.clone }
    let(:invalid_user_identity) { invalid_user.identities.email.first }
    let(:mail_address) { Zendesk::Mail::AddressParser.parse(invalid_user_identity.value).address }

    subject { mail_collaboration.send(:repair_user) }

    before do
      # [EM-561] The cause for invalid (duplicate) users is that we sometimes
      # receive the same email with different message IDs at the same time, and
      # those get processed simultaneously by different worker hosts, creating a
      # race condition for which process gets to create the new users first.
      invalid_user.stubs(id: 9999)
      refute_equal invalid_user.id, existing_user.id
      mail_collaboration.user = invalid_user
    end

    it "logs the collaborator address or name and the errors found" do
      logger.stubs(:info)
      logger.expects(:info).with("Collaborator #{invalid_user_identity.value} is invalid with errors: ")
      subject
    end

    it "searches for an existing user (on that account) that has the same email address" do
      account.expects(:find_user_by_email).with(invalid_user_identity.value).returns(existing_user).once
      subject
    end

    describe 'when the existing user is invalid' do
      before do
        existing_user.stubs(valid?: false)
        account.stubs(find_user_by_email: existing_user)
      end

      it "doesn't replace the current user with the existing one" do
        logger.stubs(:info)
        logger.expects(:info).with("Could not find existing valid User for collaborator #{invalid_user_identity.value}")

        subject

        assert_equal invalid_user.id, mail_collaboration.user.id
        refute_equal existing_user.id, mail_collaboration.user.id
      end
    end

    describe 'when the existing user is valid' do
      before do
        existing_user.stubs(valid?: true)
        account.stubs(find_user_by_email: existing_user)
      end

      it 'replaces the current user with the existing one' do
        subject

        assert_equal existing_user.id, mail_collaboration.user.id
        refute_equal invalid_user.id, mail_collaboration.user.id
      end
    end
  end
end
