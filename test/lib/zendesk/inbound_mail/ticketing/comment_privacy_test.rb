require_relative "../../../../support/test_helper"
require_relative "../../../../support/arturo_test_helper"
require_relative "../../../../support/agent_test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Ticketing::CommentPrivacy do
  extend ArturoTestHelper
  include MailTestHelper
  include AgentTestHelper

  fixtures :accounts, :users, :rules, :ticket_fields, :tickets, :events

  let(:account)    { accounts(:minimum) }
  let(:agent)      { users(:minimum_agent) }
  let(:end_user)   { users(:minimum_end_user) }
  let(:ticket)     { tickets(:minimum_1) }
  let(:mail)       { Zendesk::Mail::InboundMessage.new }
  let(:message_id) { 'a_zendesky_message_id_sprut@zendesk.com' }

  let(:state) do
    Zendesk::InboundMail::ProcessingState.new.tap do |state|
      state.account = account
      state.ticket = ticket
    end
  end

  before do
    mail.subject     = 'Re: Email subject'
    mail.body        = 'Email body'
    mail.in_reply_to = message_id
    mail.references  = %w[<message.id.1234@zendesk> <different.message.id.4567@zendesk>]
  end

  describe "#add_flags!" do
    describe "when author is an email CC and requester is not in the email recipients" do
      before do
        state.author = end_user
        state.stubs(author_is_email_cc?: true)
      end

      describe "with comment_email_ccs_allowed enabled" do
        before { Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true) }

        describe "when author is requester" do
          before do
            ticket.requester = agent
            state.author = agent
          end

          it 'does not add the PRIVATE_COMMENT_REQUESTER_MISSING flag' do
            Zendesk::InboundMail::Ticketing::CommentPrivacy.add_flags!(state, mail)
            assert_empty state.flags
          end
        end

        describe "when author is not requester" do
          it "adds the PRIVATE_COMMENT_REQUESTER_MISSING flag" do
            Zendesk::InboundMail::Ticketing::CommentPrivacy.add_flags!(state, mail)
            assert_includes(state.flags, EventFlagType.PRIVATE_COMMENT_REQUESTER_MISSING)
          end
        end

        describe_with_arturo_disabled :email_private_comment_requester_missing_flag do
          it "does not add the PRIVATE_COMMENT_REQUESTER_MISSING flag" do
            Zendesk::InboundMail::Ticketing::CommentPrivacy.add_flags!(state, mail)
            assert_empty state.flags
          end
        end
      end

      describe "with comment_email_ccs_allowed disabled" do
        it "does not add the PRIVATE_COMMENT_REQUESTER_MISSING flag" do
          Zendesk::InboundMail::Ticketing::CommentPrivacy.add_flags!(state, mail)
          assert_empty state.flags
        end
      end
    end
  end

  describe ".comment_public?" do
    describe_with_arturo_enabled :email_end_user_comment_privacy_settings do
      let(:encoded_id_1) { "ABC123-DE45" }
      let(:default_host) { accounts(:minimum).default_hosts.first }

      describe "ccs_requester_excluded_public_comments ON" do
        before do
          Account.any_instance.stubs(has_ccs_requester_excluded_public_comments_enabled?: true)
          Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true)
        end

        describe "when only the support address is the recipient" do
          describe "when author is an email CC" do
            before do
              state.author = end_user
              state.stubs(author_is_email_cc?: true)
              mail.to = address("support+id#{encoded_id_1}@#{default_host}")
            end

            it "makes the comment public" do
              assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail)
            end

            it "does not check for the presence of the requester" do
              Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.expects(:requester_not_found?).never
            end
          end
        end

        describe "when there are other recipients apart from the support address & requester not present" do
          before do
            mail.to = address("support+id#{encoded_id_1}@#{default_host}")
            mail.cc << address('email_cc1@example.com')
            mail.cc << address('email_cc2@example.com')
          end

          describe "when author is an email CC" do
            before do
              state.author = end_user
              state.stubs(author_is_email_cc?: true)
            end

            it "makes the comment private" do
              refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail)
            end
          end
        end
      end

      describe "ccs_requester_excluded_public_comments OFF" do
        before do
          Account.any_instance.stubs(has_ccs_requester_excluded_public_comments_enabled?: false)
        end

        describe "with comment_email_ccs_allowed enabled" do
          before { Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true) }

          describe "when author is an email CC" do
            before do
              state.author = end_user
              state.stubs(author_is_email_cc?: true)
            end

            describe "and requester is not present on the email" do
              before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: true) }

              it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
            end

            describe "and requester is present on the email" do
              before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: false) }

              it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
            end
          end
        end
      end
    end

    describe "when author is an email CC and requester is not in the email recipients" do
      before do
        # ticket.requester is :minimum_author from users.yml fixture
        state.author = end_user
        state.stubs(author_is_email_cc?: true)
      end

      describe "with comment_email_ccs_allowed enabled" do
        before { Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true) }

        describe "when author is requester" do
          before do
            ticket.requester = agent
            state.author = agent
          end

          it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
        end

        describe "when author is not requester" do
          it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
        end
      end

      describe "with comment_email_ccs_allowed disabled" do
        it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
      end
    end

    describe "when author is an end user" do
      before { state.author = end_user }

      it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
    end

    describe "when author is an agent" do
      before { state.author = agent }

      describe "with comment_email_ccs_allowed enabled" do
        before { Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true) }

        describe "and parent Comment is public" do
          before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(parent_comments_public?: true) }

          describe 'and requester is present on the email' do
            before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: false) }

            it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end

          describe 'and requester is not present on the email' do
            before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: true) }

            it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end
        end

        describe "and parent Comment is private" do
          before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(parent_comments_public?: false) }

          describe 'and requester is present on the email' do
            before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: false) }

            it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end

          describe 'and requester is not present on the email' do
            before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: true) }

            it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end
        end
      end

      describe "with ticket_followers_allowed enabled" do
        before { Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true) }

        describe "and parent Comment is public" do
          before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(parent_comments_public?: true) }

          describe 'and requester is present on the email' do
            before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_on_email?: true) }

            it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end

          describe 'and requester is not present on the email' do
            before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_on_email?: false) }

            it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end
        end

        describe "and parent Comment is private" do
          before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(parent_comments_public?: false) }

          describe 'and requester is not present on the email' do
            before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: true) }

            it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end

          describe 'and requester is present on the email' do
            before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: false) }

            it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end
        end
      end

      describe "when the ticket has an entry" do
        before do
          state.ticket.entry = Entry.new
          state.author.abilities.stubs(:can?).with(:publicly, Comment).returns(true)
        end

        it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }

        describe "does not rely on parent comment privacy" do
          before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(parent_comments_public?: true) }

          describe "with comment_email_ccs_allowed enabled" do
            before { Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true) }
            it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end

          describe "with ticket_followers_allowed enabled" do
            before { Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true) }
            it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end
        end
      end

      describe "when agents can't publicly comment via email" do
        before { Account.any_instance.stubs(is_email_comment_public_by_default: false) }

        it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }

        describe "does not rely on parent comment privacy" do
          before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(parent_comments_public?: true) }

          describe "with comment_email_ccs_allowed enabled" do
            before { Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true) }
            it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end

          describe "with ticket_followers_allowed enabled" do
            before { Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true) }
            it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
          end
        end
      end

      describe "when agent is treated as end-user" do
        [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
          describe "via_id is #{via_id}" do
            before do
              Arturo.enable_feature!(:agent_as_end_user)
              ticket.update_column(:via_id, via_id)
              ticket.update_column(:via_reference_id, ViaType.HELPCENTER)
              ticket.update_column(:submitter_id, agent.id)
            end

            it "returns true" do
              assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail)
            end
          end
        end
      end

      describe "when an light agent is treated as end-user" do
        let(:light_agent) { create_light_agent }

        [ViaType.WEB_SERVICE, ViaType.WEB_FORM].each do |via_id|
          describe "via_id is #{via_id}" do
            before do
              Arturo.enable_feature!(:agent_as_end_user)
              ticket.update_column(:via_id, via_id)
              ticket.update_column(:via_reference_id, ViaType.HELPCENTER)
              ticket.update_column(:submitter_id, light_agent.id)
              state.author = light_agent
              state.author.abilities.stubs(:can?).with(:publicly, Comment).returns(false)
            end

            describe_with_arturo_enabled :email_allow_light_agent_to_add_private_comment do
              it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
            end

            describe_with_arturo_disabled :email_allow_light_agent_to_add_private_comment do
              it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail) }
            end
          end
        end
      end

      describe "when agent cannot publicly comment" do
        it "returns false" do
          state.author.abilities.stubs(:can?).with(:publicly, Comment).returns(false)
          refute Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail)
        end
      end

      describe "when :is_public is passed in" do
        it "returns the value of :is_public" do
          publicity_stub = stub('publicity')
          state.properties = { is_public: publicity_stub }
          assert_equal publicity_stub, Zendesk::InboundMail::Ticketing::CommentPrivacy.comment_public?(state, mail)
        end
      end
    end
  end

  describe "#parent_comments_public?" do
    let(:message_id_2) { 'a_zendesky_message_id_sprut_2@zendesk.com' }
    let(:message_id_3) { 'a_zendesky_message_id_sprut_3@zendesk.com' }
    let(:comment) { events(:create_comment_for_minimum_ticket_1) }
    let(:comment_2) do
      ticket.will_be_saved_by(ticket.requester)
      ticket.add_comment(is_public: true, body: "A comment.")
      comment_2 = ticket.comment
      ticket.save!
      comment_2
    end
    let(:comment_3) do
      ticket.will_be_saved_by(ticket.requester)
      ticket.add_comment(is_public: true, body: "Another comment.")
      comment_3 = ticket.comment
      ticket.save!
      comment_3
    end

    describe "when inbound mail is a reply to external email which had been processed by zendesk (associated with an inbound email record)" do
      let!(:inbound_email) do
        account.inbound_emails.create! do |i|
          i.message_id = message_id
          i.references = [message_id_2]
          i.from = "foo@bar.com"
          i.ticket = comment.ticket
          i.comment = comment
        end
      end
      let!(:inbound_email_2) do
        account.inbound_emails.create! do |i|
          i.message_id = message_id_2
          i.references = [message_id, 'a_message_id@somewhere.com']
          i.from = "baz@bar.com"
          i.ticket = comment_2.ticket
          i.comment = comment_2
        end
      end
      let!(:inbound_email_3) do
        account.inbound_emails.create! do |i|
          i.message_id = message_id_3
          i.references = [message_id, message_id_2]
          i.from = "baz@bar.com"
          i.ticket = comment_3.ticket
          i.comment = comment_3
        end
      end

      before { assert [comment, comment_2, comment_3].all? &:is_public? }

      describe "when an inbound email is found by in-reply-to message id" do
        before { mail.in_reply_to = message_id }

        describe "when all parent comments are public" do
          it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.parent_comments_public?(state, mail) }
        end

        describe "when any parent comment is private" do
          before { make_comment_private!(comment) }

          it { refute comment.is_public? }
          it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.parent_comments_public?(state, mail) }
        end
      end

      describe "when an inbound email is found by references" do
        before do
          mail.message_id = "foo_message_id@bar.com"
          mail.references = [message_id_3, message_id_2, message_id]
        end

        describe "when all parent comments are public" do
          it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.parent_comments_public?(state, mail) }
        end

        describe "when any parent comment is private" do
          before { make_comment_private!(comment) }

          it { refute comment.is_public? }
          it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.parent_comments_public?(state, mail) }
        end
      end
    end

    describe "when inbound mail is a reply to an associated OutboundEmail" do
      Zendesk::InboundMail::Ticketing::CommentPrivacy::SUPPORTED_EVENT_TYPES.each do |event_type|
        describe "when message source notification is a #{event_type} event" do
          let!(:notification) { create_notification(event_type.constantize, account, comment, agent) }
          let!(:notification_2) { create_notification(event_type.constantize, account, comment_2, agent) }
          let!(:notification_3) { create_notification(event_type.constantize, account, comment_3, agent) }

          before do
            mail.in_reply_to = message_id
            mail.references = [message_id_2, message_id_3, 'another_message_id@foo.com']

            notification.message_id = message_id
            notification_2.message_id = message_id_2
            notification_3.message_id = message_id_3

            OutboundEmail.for_ticket(ticket).where(message_id: message_id).first.update_column('created_at', 3.minutes.ago)
            OutboundEmail.for_ticket(ticket).where(message_id: message_id_2).first.update_column('created_at', 2.minutes.ago)
            OutboundEmail.for_ticket(ticket).where(message_id: message_id_3).first.update_column('created_at', 1.minutes.ago)
          end

          describe "and the associated comments are public" do
            it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.parent_comments_public?(state, mail) }
          end

          describe "and the comment associated with the 'In-Reply-To' message ID is private" do
            before { make_comment_private!(comment) }

            it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.parent_comments_public?(state, mail) }
          end

          describe "and the comment associated with the latest referenced message ID in the 'References' header is private" do
            # Mark the latest/newest comment private (comment_3 is created "after" comment_2)
            before { make_comment_private!(comment_3) }

            it { refute Zendesk::InboundMail::Ticketing::CommentPrivacy.parent_comments_public?(state, mail) }
          end

          describe "does not consider older 'References' header-associated private comments" do
            # Mark an older comment private (comment_2 is created "before" comment_3)
            before { make_comment_private!(comment_2) }

            it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.parent_comments_public?(state, mail) }
          end
        end
      end

      describe "when no associated OutboundEmail is found" do
        before do
          mail.in_reply_to = message_id
          mail.references = [message_id_2, message_id_3, 'another_message_id@foo.com']
        end

        it { assert Zendesk::InboundMail::Ticketing::CommentPrivacy.parent_comments_public?(state, mail) }
      end
    end

    def create_notification(notification_type, account, comment, agent)
      notification_type.new.tap do |event|
        event.account_id      = account.id
        event.parent_id       = comment.audit.id
        event.value           = [agent.id, comment.author.id].uniq
        event.value_previous  = 'A subject'
        event.value_reference = 'A body'
        event.save!
      end
    end

    def make_comment_private!(comment)
      comment.tap do |c|
        c.ticket.will_be_saved_by(c.author)
        c.is_public = false
      end.save!
    end
  end
end
