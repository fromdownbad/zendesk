require_relative "../../../../support/test_helper"
require_relative "../../../../../lib/zendesk/inbound_mail/ticketing/mail_collaboration_update_types"

SingleCov.covered!

describe "Mail Collaboration Update Types" do
  let(:update_type_class) { Zendesk::InboundMail::Ticketing::MailCollaborationUpdateType }
  let(:update_type) { update_type_class.new }

  let(:expected_adds_only_author) { false }
  let(:expected_defers_collaboration_creation) { false }
  let(:expected_edits_email_ccs) { false }
  let(:expected_edits_followers) { false }
  let(:expected_adds_only_agents) { false }
  let(:expected_adds_only_end_users) { false }

  def assert_properties_equal
    assert_equal expected_adds_only_author, update_type.adds_only_author?
    assert_equal expected_defers_collaboration_creation, update_type.defers_collaboration_creation?
    assert_equal expected_edits_email_ccs, update_type.edits_email_ccs?
    assert_equal expected_edits_followers, update_type.edits_followers?
    assert_equal expected_adds_only_agents, update_type.adds_only_agents?
    assert_equal expected_adds_only_end_users, update_type.adds_only_end_users?
  end

  describe "MailCollaborationUpdateType" do
    it { assert_properties_equal }
    it { refute update_type.restricts_by_user_role? }
  end

  describe "LegacyCcsUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::LegacyCcsUpdateType }

    it { assert_properties_equal }
    it { refute update_type.restricts_by_user_role? }
  end

  describe "NoopUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::NoopUpdateType }

    it { assert_properties_equal }
    it { refute update_type.restricts_by_user_role? }
  end

  describe "AddNewAsLegacyUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAsLegacyUpdateType }

    let(:expected_defers_collaboration_creation) { true }
    let(:expected_edits_email_ccs) { true }
    let(:expected_edits_followers) { true }

    it { assert_properties_equal }
    it { refute update_type.restricts_by_user_role? }
  end

  describe "AddNewAgentsUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAgentsUpdateType }

    let(:expected_defers_collaboration_creation) { true }
    let(:expected_adds_only_agents) { true }

    it { assert_properties_equal }
    it { assert update_type.restricts_by_user_role? }
  end

  describe "AddNewAgentsAsEmailCcsUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAgentsAsEmailCcsUpdateType }

    let(:expected_defers_collaboration_creation) { true }
    let(:expected_adds_only_agents) { true }
    let(:expected_edits_email_ccs) { true }

    it { assert_properties_equal }
    it { assert update_type.restricts_by_user_role? }
  end

  describe "AddNewAgentsAsFollowersUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAgentsAsFollowersUpdateType }

    let(:expected_defers_collaboration_creation) { true }
    let(:expected_adds_only_agents) { true }
    let(:expected_edits_followers) { true }

    it { assert_properties_equal }
    it { assert update_type.restricts_by_user_role? }
  end

  describe "AddNewAsEmailCcsUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewAsEmailCcsUpdateType }

    let(:expected_defers_collaboration_creation) { true }
    let(:expected_edits_email_ccs) { true }

    it { assert_properties_equal }
    it { refute update_type.restricts_by_user_role? }
  end

  describe "ResetEmailCcsUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::ResetEmailCcsUpdateType }

    let(:expected_defers_collaboration_creation) { true }
    let(:expected_edits_email_ccs) { true }

    it { assert_properties_equal }
    it { refute update_type.restricts_by_user_role? }
  end

  describe "AddAuthorAsFollowerUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddAuthorAsFollowerUpdateType }

    let(:expected_adds_only_author) { true }
    let(:expected_edits_followers) { true }

    it { assert_properties_equal }
    it { refute update_type.restricts_by_user_role? }
  end

  describe "AddAuthorAsEmailCcUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddAuthorAsEmailCcUpdateType }

    let(:expected_adds_only_author) { true }
    let(:expected_edits_email_ccs) { true }

    it { assert_properties_equal }
    it { refute update_type.restricts_by_user_role? }
  end

  describe "AddNewEndUsersUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNewEndUsersUpdateType }

    let(:expected_defers_collaboration_creation) { true }
    let(:expected_edits_email_ccs) { true }
    let(:expected_adds_only_end_users) { true }

    it { assert_properties_equal }
    it { assert update_type.restricts_by_user_role? }
  end

  describe "AddNonGroupAgentsAsFollowersUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsFollowersUpdateType }

    let(:expected_defers_collaboration_creation) { true }
    let(:expected_edits_followers) { true }
    let(:expected_adds_only_agents) { true }

    it { assert_properties_equal }
    it { assert update_type.restricts_by_user_role? }
  end

  describe "AddNonGroupAgentsAsEmailCcsUpdateType" do
    let(:update_type_class) { Zendesk::InboundMail::Ticketing::AddNonGroupAgentsAsEmailCcsUpdateType }

    let(:expected_defers_collaboration_creation) { true }
    let(:expected_edits_email_ccs) { true }
    let(:expected_adds_only_agents) { true }

    it { assert_properties_equal }
    it { assert update_type.restricts_by_user_role? }
  end
end
