require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Zendesk::InboundMail::SQSTicketProcessingWorker do
  # TODO: Remove tests for the AdrianTicketProcessingWorker once the MTC on k8s
  # project is complete and all relevant code has been rolled out.

  fixtures :all

  let(:ticket_processing_worker_class) { nil }
  let(:receive_count) { 1 }
  let(:account) { accounts(:minimum) }

  def queue_item(fixture, merge = {})
    json = JSON.load(File.read(FixtureHelper::Mail.path(fixture)))
    json.merge!(merge)

    # Use unrecognized address or recipient address processor sets the brand
    json['headers']['to'] = { 'address' => 'hello@world.com' }

    if ticket_processing_worker_class == Zendesk::InboundMail::SQSTicketProcessingWorker
      sqs_item = Zendesk::InboundMail::SQSItem.new(
        json.to_json,
        Time.now,
        "1/foo.eml",
        "1234abcd",
        receive_count
      )

      yield ticket_processing_worker_class.new(sqs_item)
    else
      Tempfile.create("test") do |f|
        JSON.dump(json, f)
        f.rewind
        yield ticket_processing_worker_class.new(Adrian::FileItem.new(f.path))
      end
    end
  end

  [
    Zendesk::InboundMail::AdrianTicketProcessingWorker,
    Zendesk::InboundMail::SQSTicketProcessingWorker,
  ].each do |worker_class|
    let(:ticket_processing_worker_class) { worker_class }
    let(:account) { accounts(:minimum) }

    before do
      Zendesk::Mail.logger = Logger.new(StringIO.new)
      Zendesk::StatsD::Client.any_instance.stubs(:increment)
    end

    it "converts JSON into InboundMessages" do
      queue_item 'minimum_ticket.json' do |worker|
        worker.work
        message = worker.message
        assert_equal "Hello world!\r\nMicrosoft Word fails to print my document", message.body
      end
    end

    describe "when email came in via a branded route" do
      def assert_brand_id_assigned(brand_id)
        queue_item 'minimum_ticket.json', 'route_id' => brand.route_id do |worker|
          assert_difference("Ticket.count(:all)", +1) { worker.work }
          assert_equal brand_id, Ticket.last.brand_id
        end
      end

      let(:brand) do
        account.brands.create!(name: "xxxx") do |b|
          b.build_route(subdomain: "sub2") { |r| r.account = account }
        end
      end

      it "assigns brand_id" do
        assert_brand_id_assigned brand.id
      end

      it "does not assign inactive brand" do
        brand.update_column(:active, false)
        assert_brand_id_assigned account.default_brand_id
      end
    end

    describe "#work" do
      describe "StatsD client" do
        it "reports processing and timing stats to DataDog" do
          Zendesk::StatsD::Client.any_instance.expects(:timing).with(:processing, anything, tags: []).once
          Zendesk::StatsD::Client.any_instance.expects(:sli_histogram_decremental).with(:json_processed, anything).once

          headers = { received: [""] }
          queue_item('minimum_ticket.json', headers: headers) do |worker|
            Zendesk::StatsD::Client.any_instance.expects(:time).with(:processors, anything).at_least(1)
            Zendesk::StatsD::Client.any_instance.expects(:histogram).twice
            worker.work
          end
        end

        describe "when getting create time from json fails" do
          it "raises an exception" do
            queue_item('minimum_ticket.json') do |worker|
              worker.expects(:json_create_time).twice.raises(ArgumentError)
              ZendeskExceptions::Logger.expects(:record).times(3)
              worker.work
            end
          end
        end

        describe_with_arturo_disabled :email_mtc_instrument_processing_duration_event_slo do
          it "reports processing and timing stats to DataDog" do
            Zendesk::StatsD::Client.any_instance.expects(:timing).with(:processing, anything, tags: []).never
            Zendesk::StatsD::Client.any_instance.expects(:increment).with(:json_processed, anything).never

            headers = { received: [""] }
            queue_item('minimum_ticket.json', headers: headers) do |worker|
              Zendesk::StatsD::Client.any_instance.expects(:time).with(:processing, tags: []).once
              Zendesk::StatsD::Client.any_instance.expects(:histogram).twice
              worker.work
            end
          end

          describe "when getting create time from json fails" do
            it "raises an exception" do
              queue_item('minimum_ticket.json') do |worker|
                worker.expects(:json_create_time).twice.raises(ArgumentError)
                ZendeskExceptions::Logger.expects(:record).times(3)
                worker.work
              end
            end
          end
        end

        describe "on timeout" do
          before { Timeout.expects(:timeout).raises(Timeout::Error) }

          it "reports :processing and :json_processed stats" do
            Zendesk::StatsD::Client.any_instance.expects(:timing).with(:processing, anything, tags: []).once
            Zendesk::StatsD::Client.any_instance.expects(:increment).with(:json_processed, anything).once

            headers = { received: [""] }

            queue_item('minimum_ticket.json', headers: headers) do |worker|
              Zendesk::StatsD::Client.any_instance.expects(:histogram).twice
              assert_raise(Timeout::Error) do
                worker.work
              end
            end
          end
        end
      end

      it "initiates health check" do
        queue_item('minimum_ticket.json') do |worker|
          worker.expects(:process_health_check!)
          worker.work
        end
      end
    end

    describe "#message" do
      it "sets the message's logger to an instance of Zendesk::InboundMail::AccountIDMessageLogger" do
        queue_item 'minimum_ticket.json' do |worker|
          worker.work
          message = worker.message

          assert_instance_of Zendesk::InboundMail::AccountIDMessageLogger, message.logger
        end
      end
    end

    describe "#process_health_check!" do
      let(:rss_reading) { 1.gigabyte }

      before { ZendeskJob::RssReader.stubs(rss: rss_reading) }

      describe_with_arturo_disabled :email_mtc_memory_auto_exit do
        describe_with_arturo_disabled :email_mtc_memory_gc_logging do
          it "does not gauge or log memory usage" do
            queue_item('minimum_ticket.json') do |worker|
              worker.logger.expects(:info).never
              ZendeskJob::RssReader.expects(:rss).never
              worker.send(:process_health_check!)
            end
          end
        end

        describe_with_arturo_enabled :email_mtc_memory_gc_logging do
          it "gauges and logs memory usage" do
            queue_item('minimum_ticket.json') do |worker|
              Zendesk::InboundMail::AccountIDMessageLogger.any_instance.expects(:info).with(regexp_matches(/MTC health check - current/)).once
              ZendeskJob::RssReader.expects(:rss).returns(rss_reading)
              Zendesk::InboundMail::StatsD.client.expects(:gauge).once
              worker.send(:process_health_check!)
            end
          end
        end

        describe_with_and_without_arturo_enabled :email_mtc_memory_gc_logging do
          describe "when memory usage is below the MTC_RSS_LIMIT" do
            it "does not increment the abort metric" do
              queue_item('minimum_ticket.json') do |worker|
                Zendesk::InboundMail::StatsD.client.expects(:increment).never
                worker.send(:process_health_check!)
              end
            end

            it "does not raise" do
              queue_item('minimum_ticket.json') do |worker|
                worker.send(:process_health_check!)
              end
            end
          end

          describe "when memory usage is above the MTC_RSS_LIMIT" do
            let(:rss_reading) { 3.gigabytes }

            it "does not raise" do
              queue_item('minimum_ticket.json') do |worker|
                worker.send(:process_health_check!)
              end
            end
          end
        end
      end

      describe_with_arturo_enabled :email_mtc_memory_gc do
        describe_with_arturo_enabled :email_mtc_memory_gc_logging do
          it "gauges and logs memory usage" do
            queue_item('minimum_ticket.json') do |worker|
              Zendesk::InboundMail::AccountIDMessageLogger.any_instance.expects(:info).with(regexp_matches(/MTC health check - current/)).once
              ZendeskJob::RssReader.expects(:rss).returns(rss_reading)
              worker.send(:process_health_check!)
            end
          end
        end

        describe_with_arturo_disabled :email_mtc_memory_gc_logging do
          it "does not gauge or log memory usage" do
            queue_item('minimum_ticket.json') do |worker|
              worker.logger.expects(:info).never
              ZendeskJob::RssReader.expects(:rss).never
              worker.send(:process_health_check!)
            end
          end
        end

        describe_with_and_without_arturo_enabled :email_mtc_memory_gc_logging do
          describe "when memory usage is below the MTC_RSS_LIMIT" do
            it "does not increment the abort metric" do
              queue_item('minimum_ticket.json') do |worker|
                Zendesk::InboundMail::StatsD.client.expects(:increment).never
                worker.send(:process_health_check!)
              end
            end

            it "does not raise" do
              queue_item('minimum_ticket.json') do |worker|
                worker.send(:process_health_check!)
              end
            end
          end

          describe "when memory usage is above the MTC_RSS_LIMIT" do
            let(:rss_reading) { 3.gigabytes }

            it "increments the abort metric and raises" do
              assert_raises(Zendesk::InboundMail::MemoryLimitError) do
                queue_item('minimum_ticket.json') do |worker|
                  Zendesk::InboundMail::StatsD.client.expects(:increment).once
                  Zendesk::InboundMail::AccountIDMessageLogger.any_instance.expects(:info).with(regexp_matches(/MTC health check - current/)).once
                  Zendesk::InboundMail::AccountIDMessageLogger.any_instance.expects(:warn).with(regexp_matches(/aborting/)).once
                  worker.send(:process_health_check!)
                end
              end
            end
          end
        end
      end
    end
  end

  describe Zendesk::InboundMail::SQSTicketProcessingWorker do
    let(:ticket_processing_worker_class) { Zendesk::InboundMail::SQSTicketProcessingWorker }

    before do
      Zendesk::Mail.logger = Logger.new(StringIO.new)
      Zendesk::StatsD::Client.any_instance.stubs(:increment)
    end

    describe "#perform" do
      subject do
        queue_item('minimum_ticket.json', &:perform)
      end

      before { Zendesk::InboundMail::SQSTicketProcessingWorker.any_instance.stubs(:work) }

      it "logs the receive count" do
        Zendesk::Mail.logger.expects(:info).with("SQS message receive count: 1")
        subject
      end

      describe "when the receive_count is less than the MAX_RECEIVES" do
        it "processes the item" do
          Zendesk::InboundMail::SQSTicketProcessingWorker.any_instance.unstub(:work)
          Zendesk::InboundMail::SQSTicketProcessingWorker.any_instance.expects(:work)
          subject
        end

        it "does not create SQSMaxReceivesError for work_done" do
          Zendesk::InboundMail::SQSMaxReceivesError.expects(:new).never
          Zendesk::InboundMail::SQSTicketProcessingWorker.any_instance.expects(:work_done).with(nil)
          subject
        end
      end

      describe "when the receive_count is equal to the MAX_RECEIVES" do
        let(:receive_count) { Zendesk::InboundMail::SQSTicketProcessingWorker::MAX_RECEIVES }

        it "does not process the item" do
          Zendesk::InboundMail::SQSTicketProcessingWorker.any_instance.unstub(:work)
          Zendesk::InboundMail::SQSTicketProcessingWorker.any_instance.expects(:work).never
          subject
        end

        it "creates a SQSMaxReceivesError for work_done" do
          error = Zendesk::InboundMail::SQSMaxReceivesError.new
          Zendesk::InboundMail::SQSMaxReceivesError.expects(new: error)
          Zendesk::InboundMail::SQSTicketProcessingWorker.any_instance.expects(:work_done).with(error)
          subject
        end
      end

      describe "when the receive_count is more than the MAX_RECEIVES" do
        let(:receive_count) { Zendesk::InboundMail::SQSTicketProcessingWorker::MAX_RECEIVES + 1 }

        it "does not process the item" do
          Zendesk::InboundMail::SQSTicketProcessingWorker.any_instance.unstub(:work)
          Zendesk::InboundMail::SQSTicketProcessingWorker.any_instance.expects(:work).never
          subject
        end

        it "creates a SQSMaxReceivesError for work_done" do
          error = Zendesk::InboundMail::SQSMaxReceivesError.new
          Zendesk::InboundMail::SQSMaxReceivesError.expects(new: error)
          Zendesk::InboundMail::SQSTicketProcessingWorker.any_instance.expects(:work_done).with(error)
          subject
        end
      end
    end
  end
end
