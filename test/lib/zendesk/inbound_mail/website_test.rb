require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Website do
  include Rack::Test::Methods

  let(:app) { Zendesk::InboundMail::Website.new }
  let(:response) { get '/' }

  it 'returns 404 status code' do
    assert_equal 404, response.status, 404
  end

  it 'returns a JSON response' do
    assert_equal 'application/json', response.headers['Content-Type']
  end

  it 'returns an object with a message' do
    assert_equal '{"message":"Not found"}', response.body
  end
end
