require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Unprocessable do
  include MailTestHelper

  def refute_translation_missing(str)
    refute str.start_with? "translation missing"
  end

  before do
    stub_request(:put, %r{\.amazonaws\.com/data/attachments/\d+/unprocessable-email.*})
  end

  it "supports a convenience method for recording an unprocessable email" do
    mail = FixtureHelper::Mail.read('minimum_ticket.json')
    store_mail_in_remote_files(mail)
    state = stub(account: accounts(:minimum), prefix: 'whatever')

    mail.from = Zendesk::Mail::Address.new(name: "Mmm Bob", address: "mmm.bob@example.com")
    InboundEmail.create!(account: state.account, from: mail.from, message_id: mail.message_id)

    assert Zendesk::InboundMail::Unprocessable.create(state.account, mail)

    suspended = SuspendedTicket.last
    assert_equal suspended.cause, SuspensionType.UNPROCESSABLE_EMAIL
    assert_equal "Mmm Bob", suspended.from_name
    assert_equal 1, suspended.attachments.count(:all)
    assert_equal mail.message_id, suspended.message_id
    assert suspended.attachments.first.account

    inbound = InboundEmail.find_by_message_id(mail.message_id)
    assert_equal SuspensionType.UNPROCESSABLE_EMAIL, inbound.category_id
  end

  describe "An unprocessable email" do
    let (:cause) { SuspensionType.UNPROCESSABLE_EMAIL }

    before do
      @mail = FixtureHelper::Mail.read('minimum_ticket.json')
      store_mail_in_remote_files(@mail)
      @account = accounts(:minimum)
      @mail.from = Zendesk::Mail::Address.new(name: "Mmm Bob", address: "mmm.bob@example.com")
      @inbound   = InboundEmail.create!(account: @account, from: @mail.from, message_id: @mail.message_id)
    end

    describe "that is created" do
      before do
        assert @suspended = Zendesk::InboundMail::Unprocessable.create(@account, @mail)
        Attachment.any_instance.stubs(source: @suspended, size: 50.megabytes)
      end

      it "suspends as unprocessable" do
        assert_equal cause, @suspended.cause
      end

      it "contains the mail's basic properties" do
        assert_equal "Mmm Bob",             @suspended.from_name
        assert_equal "mmm.bob@example.com", @suspended.from_mail
        assert_equal @mail.message_id,      @suspended.message_id
        assert_equal({}, @suspended.properties)
      end

      it "attaches the raw email" do
        assert_equal 1, @suspended.attachments.count(:all)
        assert @suspended.attachments.first.account
      end

      it "classifies the inbound email as unprocessable" do
        assert_equal cause, @inbound.reload.category_id
      end

      it "finds and uses the default content string" do
        expected_content = ::I18n.t("txt.suspended_ticket.content_unprocessable_email")
        refute_translation_missing expected_content
        assert_equal expected_content, @suspended.content
      end
    end

    describe "creating without an associated inbound email" do
      before do
        @inbound.destroy
      end

      it "succeeds" do
        suspended_ticket = Zendesk::InboundMail::Unprocessable.create(@account, @mail)
        assert_equal false, suspended_ticket.new_record?
      end
    end

    describe "creating without a remote eml" do
      before do
        @mail.stubs(eml_url: nil)
        @mail.account = accounts(:minimum)
      end

      it "succeeds" do
        @mail.logger.expects(:warn).with("Cannot attach eml, eml_url missing for unknown reason").once
        @mail.logger.expects(:warn).with("Registered unprocessable email: #{@mail.id}").once

        suspended_ticket = Zendesk::InboundMail::Unprocessable.create(@account, @mail)

        assert_equal false, suspended_ticket.new_record?
      end

      describe "with credit card redaction enabled" do
        before { @mail.account.settings.stubs(credit_card_redaction?: true) }

        it "succeeds and logs" do
          @mail.logger.expects(:info).with("Cannot attach eml, credit card redaction enabled (eml was redacted)").once
          suspended_ticket = Zendesk::InboundMail::Unprocessable.create(@account, @mail)
          assert_equal false, suspended_ticket.new_record?
        end
      end
    end

    describe "that cannot be created" do
      it "does not raise when the account is unknown" do
        @account = nil
        assert_nil Zendesk::InboundMail::Unprocessable.create(@account, @mail)
      end

      it "raises when the suspended ticket is invalid" do
        SuspendedTicket.any_instance.stubs(:valid?).returns(false)
        assert_raise(ActiveRecord::RecordInvalid) { Zendesk::InboundMail::Unprocessable.create(@account, @mail) }
      end

      describe "attached email is too large" do
        let(:statsd_client) { Zendesk::InboundMail::StatsD.client }

        before { Attachment.any_instance.stubs(source: @suspended, size: 51.megabytes) }

        it "does not raise when the suspended ticket attachment is too large" do
          statsd_client.expects(:increment).with(:unprocessable_email_over_size_limit)
          statsd_client.expects(:histogram).with(:unprocessable_email_size, 51.megabytes)

          Zendesk::InboundMail::Unprocessable.create(@account, @mail)
        end
      end
    end

    describe "that has a non-default suspension reason" do
      let (:cause) { SuspensionType.CONTENT_PROCESSOR_TIMED_OUT }

      before do
        assert @suspended = Zendesk::InboundMail::Unprocessable.create(@account, @mail, cause)
      end

      it "suspends with the custom cause" do
        assert_equal cause, @suspended.cause
      end

      it "finds and uses the custom content string" do
        expected_content = ::I18n.t("txt.suspended_ticket.content_content_processor_timed_out")
        refute_translation_missing expected_content
        assert_equal expected_content, @suspended.content
      end
    end

    describe "creating without a reply-to name" do
      before do
        @mail.reply_to.stubs(name: nil)
        @mail.account = accounts(:minimum)
      end

      it "succeeds" do
        @mail.logger.expects(:warn).with("Registered unprocessable email: #{@mail.id}").once

        suspended_ticket = Zendesk::InboundMail::Unprocessable.create(@account, @mail)

        assert_equal Zendesk::InboundMail::Ticketing::MailTicket::UNKNOWN, suspended_ticket.from_name
      end
    end
  end
end
