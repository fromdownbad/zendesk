require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Loggers::UserAbilities do
  fixtures :all

  let(:subject) do
    Zendesk::InboundMail::Loggers::UserAbilities.new(logger, ticket, user).log_user_can_add_comment
  end

  let(:agent) { users(:minimum_agent) }
  let(:admin) { users(:minimum_admin) }
  let(:end_user) { users(:minimum_end_user) }
  let(:ticket) { tickets(:minimum_1) }
  let(:logger) { Zendesk::InboundMail::MailLogger.new(stub('logger', :info)) }

  def expect_log(matcher)
    logger.expects(:log).with(matcher).once
  end

  def expect_user_can_add_comment_log
    expect_log('User can add comments to this ticket.')
  end

  def expect_user_cannot_add_comment_log
    expect_log('User cannot add comments to this ticket.')
  end

  describe '#log_use_can_add_comments' do
    describe 'with an agent user' do
      let(:user) { agent }

      describe 'and a new ticket' do
        before do
          ticket.stubs(new_record?: true)

          expect_log('User can edit ticket because it is a new record')
          expect_user_can_add_comment_log
        end

        it { subject }
      end

      describe 'that can view the ticket' do
        before { expect_log('User is agent and can view the ticket') }

        describe 'agent is an admin' do
          before do
            user.stubs(is_admin?: true)

            expect_log('User is admin')
            expect_user_can_add_comment_log
          end

          it { subject }
        end

        describe 'is the requester' do
          before do
            ticket.stubs(requester_id: agent.id)

            expect_log('User is or was the requester')
            expect_user_can_add_comment_log
          end

          it { subject }
        end

        describe 'was the requester' do
          before do
            ticket.stubs(requester_id: admin.id, requester_id_was: agent.id)

            expect_log('User is or was the requester')
            expect_user_can_add_comment_log
          end

          it { subject }
        end

        describe 'and agent is unrestricted' do
          before do
            user.stubs(:agent_restriction?).with(:none).returns(true)
            ticket.stubs(requester_id: admin.id, requester_id_was: admin.id)

            expect_log('User is an unrestricted agent')
            expect_user_can_add_comment_log
          end

          it { subject }
        end

        describe 'is a collaborator' do
          before do
            user.stubs(:agent_restriction?).with(:none).returns(false)
            ticket.stubs(:is_collaborator?).with(user).returns(true)

            expect_log('User is a collaborator.')
            expect_user_can_add_comment_log
          end

          it { subject }
        end

        describe 'and agent is restricted' do
          before { user.stubs(:agent_restriction?).with(:none).returns(false) }

          describe 'to assigned tickets and is assigned the ticket' do
            before do
              user.stubs(:agent_restriction?).with(:assigned).returns(true)
              ticket.stubs(assignee_id: agent.id, assignee_id_was: admin.id)

              expect_log('Restricted agent is restricted to assigned tickets and the ticket is or was assigned to the user.')
              expect_user_can_add_comment_log
            end

            it { subject }
          end

          describe 'to assigned tickets and is assigned the ticket' do
            before do
              user.stubs(:agent_restriction?).with(:assigned).returns(true)
              ticket.stubs(assignee_id: admin.id, assignee_id_was: agent.id)

              expect_log('Restricted agent is restricted to assigned tickets and the ticket is or was assigned to the user.')
              expect_user_can_add_comment_log
            end

            it { subject }
          end

          describe 'to tickets in the same group and is a part of the group' do
            before do
              user.stubs(:agent_restriction?).with(:assigned).returns(false)
              user.stubs(:agent_restriction?).with(:groups).returns(true)

              ticket.stubs(group_id: 1, group_id_was: 2)

              user.stubs(:in_group?).with(ticket.group_id).returns(true)
              user.stubs(:in_group?).with(ticket.group_id_was).returns(false)
              Access::Authorization.any_instance.stubs(:defer?).with(:assign_to_any_group, ticket).returns(false)

              expect_log('Restricted agent in group that ticket is assigned to.')
              expect_user_can_add_comment_log
            end

            it { subject }
          end

          describe 'to tickets in the same group and was a part of the group' do
            before do
              user.stubs(:agent_restriction?).with(:assigned).returns(false)
              user.stubs(:agent_restriction?).with(:groups).returns(true)

              ticket.stubs(group_id: 1, group_id_was: 2)

              user.stubs(:in_group?).with(ticket.group_id).returns(false)
              user.stubs(:in_group?).with(ticket.group_id_was).returns(true)
              Access::Authorization.any_instance.stubs(:defer?).with(:assign_to_any_group, ticket).returns(true)

              expect_log('Restricted agent was in group and can :assign_to_any_group.')
              expect_user_can_add_comment_log
            end

            it { subject }
          end

          describe 'to tickets in the same org and is a part of the org' do
            before do
              ticket.stubs(organization_id: 1)

              user.stubs(:agent_restriction?).with(:assigned).returns(false)
              user.stubs(:agent_restriction?).with(:groups).returns(false)
              user.stubs(:agent_restriction?).with(:organization).returns(true)

              user.stubs(:has_organizations?).with(ticket.organization_id).returns(true)

              expect_log('Restricted agent is a part of the organization the ticket is assigned to.')
              expect_user_can_add_comment_log
            end

            it { subject }
          end
        end

        describe 'can view tickets in the org the ticket is assigned to' do
          let(:ticket) { tickets(:minimum_2) }

          before do
            user.stubs(:agent_restriction?).with(:none).returns(false)
            user.stubs(:agent_restriction?).with(:assigned).returns(false)
            user.stubs(:agent_restriction?).with(:groups).returns(false)
            user.stubs(:agent_restriction?).with(:organization).returns(false)

            expect_log('User can view tickets from organization this ticket is assigned to')
            expect_user_can_add_comment_log
          end

          it { subject }
        end
      end

      describe 'that cannot view the ticket' do
        before { expect_log('User is agent and cannot view the ticket') }

        describe 'and agent is restricted' do
          before { user.stubs(:agent_restriction?).with(:none).returns(false) }

          describe 'to assigned tickets and is not assigned the ticket' do
            before do
              user.stubs(:agent_restriction?).with(:assigned).returns(true)

              ticket.stubs(assignee_id: admin.id, assignee_id_was: admin.id)

              expect_log('Restricted agent is restricted to assigned tickets and the ticket is not assigned to the user.')
              expect_user_cannot_add_comment_log
            end

            it { subject }
          end

          describe 'to tickets in the same group and is not a part of the group' do
            before do
              user.stubs(:agent_restriction?).with(:assigned).returns(false)
              user.stubs(:agent_restriction?).with(:groups).returns(true)

              user.stubs(in_group?: false)
              Access::Authorization.any_instance.stubs(:defer?).with(:assign_to_any_group, ticket).returns(false)

              expect_user_cannot_add_comment_log
            end

            it { subject }
          end

          describe 'to tickets in the same org and is not a part of the org' do
            before do
              ticket.stubs(organization_id: 1)

              user.stubs(:agent_restriction?).with(:assigned).returns(false)
              user.stubs(:agent_restriction?).with(:groups).returns(false)
              user.stubs(:agent_restriction?).with(:organization).returns(true)

              user.stubs(:has_organizations?).with(ticket.organization_id).returns(false)

              expect_log('Restricted agent is not a part of the organization the ticket is assigned to.')
              expect_user_cannot_add_comment_log
            end

            it { subject }
          end
        end
      end
    end

    describe 'with an end user' do
      let(:user) { end_user }

      describe 'and a new ticket' do
        before do
          ticket.stubs(new_record?: true)

          expect_log('User can edit ticket because it is a new record')
          expect_user_can_add_comment_log
        end

        it { subject }
      end

      describe 'is foreign' do
        before do
          user.stubs(foreign?: true)

          expect_log('User is foreign?')
          expect_user_can_add_comment_log
        end

        it { subject }
      end

      describe 'is the requester' do
        before do
          ticket.stubs(requester_id: end_user.id)

          expect_log('User is the requester')
          expect_user_can_add_comment_log
        end

        it { subject }
      end

      describe 'is is a part of the organization the ticket belongs to' do
        let(:ticket) { tickets(:minimum_2) }

        before do
          ticket.stubs(requester_id: agent.id)
          ticket.organization.stubs(is_shared_comments?: true)

          expect_log('Ticket is in the user\'s organization')
          expect_user_can_add_comment_log
        end

        it { subject }
      end

      describe 'is a collaborator' do
        before do
          ticket.stubs(:is_collaborator?).with(user).returns(true)

          expect_log('User is a collaborator')
          expect_user_can_add_comment_log
        end

        it { subject }
      end
    end
  end
end
