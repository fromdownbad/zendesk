require_relative "../../../../support/test_helper"
require_relative "../../../../support/agent_test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Loggers::AgentAccess do
  include AgentTestHelper

  fixtures :all

  let(:subject) do
    Zendesk::InboundMail::Loggers::AgentAccess.new(logger, ticket).log_edit_access
  end

  let(:agent) { users(:minimum_agent) }
  let(:light_agent) do
    users(:multiproduct_light_agent).tap do |user|
      user.stubs(is_light_agent?: true, is_agent?: true)
      user.stubs(:can?).with(:publicly, Comment).returns(false)
      user.stubs(:can?).with(:edit, ticket).returns(false)
    end
  end
  let(:end_user) { users(:minimum_end_user) }
  let(:ticket) { tickets(:minimum_1) }
  let(:comment) { events(:create_comment_for_minimum_ticket_1) }
  let(:logger) { Zendesk::InboundMail::MailLogger.new(stub('logger', :info, :warn)) }

  def expect_log(matcher)
    logger.expects(:log).with(matcher).once
  end

  def expect_can_save_log(restricted_agent: true)
    expect_log("Saving update from #{restricted_agent ? 'restricted ' : ''}agent should be successful.")
  end

  def expect_cannot_save_log
    logger.expects(:warn).with("Saving ticket/comment will fail because the author can't edit the ticket/make public comments and " \
      "doesn't have restricted agent edit access or flagged update access.").once
  end

  def expect_light_agent_granted_log
    expect_log('restricted agent edit access granted')
  end

  def expect_light_agent_denied_log(restricted_agent: true)
    expect_log("restricted agent edit access denied#{restricted_agent ? '' : ' (no restrictions)'}")
  end

  def display_email(user)
    "#{user.name} <#{user.email}>"
  end

  before do
    comment.author = author
    ticket.stubs(comment: comment)
  end

  describe '#log_light_agent_edit_access' do
    describe 'comment author is an agent' do
      let(:author) { agent }

      it 'logs author edit access should allow the ticket to be saved' do
        expect_log('Flagged update access denied, ticket is not flagged with an AuditFlagType that allows comments: ')
        expect_light_agent_denied_log(restricted_agent: false)
        expect_can_save_log(restricted_agent: false)

        subject
      end
    end

    describe 'comment author is an end user' do
      let(:author) { end_user }

      it 'does not log anything' do
        logger.expects(:log).never
        subject
      end
    end

    describe 'comment author is a light agent' do
      let(:author) { light_agent }

      Access::Validations::TicketValidator::FLAGGED_ATTRIBUTE_WHITELIST.each do |flagged_attribute|
        describe "when flagged attribute: #{flagged_attribute} changed" do
          before { ticket.stubs(changed: ['latest_recipients'], attributes_changed?: true) }

          it 'logs access granted when ticket flags allow comment' do
            ticket.stubs(flags_allow_comment?: true)

            expect_log('Flagged update access granted')
            expect_light_agent_denied_log
            expect_can_save_log

            subject
          end

          it 'logs access denial when ticket flags do not allow comment' do
            ticket.stubs(flags_allow_comment?: false)

            expect_log('Flagged update access denied, ticket is not flagged with an AuditFlagType that allows comments: ')
            expect_light_agent_denied_log
            expect_cannot_save_log

            subject
          end
        end
      end

      it 'logs access denial when disallowed attribute has changed' do
        ticket.stubs(flags_allow_comment?: true, changed: %w[assignee_id current_collaborators], attributes_changed?: true)
        disallowed_flagged_attributes = ['current_collaborators']
        disallowed_attributes = ['assignee_id']

        expect_log("Flagged update access denied, disallowed changed attributes: #{disallowed_flagged_attributes}")
        expect_log("Disallowed light agent changes to: #{disallowed_attributes}")
        expect_light_agent_denied_log
        expect_cannot_save_log

        subject
      end

      it 'logs access denial when current_collaborators have changed' do
        ticket.stubs(
          flags_allow_comment?: true,
          changed: ['current_collaborators'],
          attributes_changed?: true,
          current_collaborators: "#{display_email(end_user)},#{display_email(agent)}",
          current_collaborators_was: display_email(end_user)
        )
        disallowed_attributes = ['current_collaborators']
        was = [[end_user.email]]
        is = [[agent.email], [end_user.email]]

        expect_log("Flagged update access denied, disallowed changed attributes: #{disallowed_attributes}")
        expect_log("Disallowed light agent changes to collaborators from: #{was} to: #{is}")
        expect_light_agent_denied_log
        expect_cannot_save_log

        subject
      end

      it 'logs access denial when requester_id has changed' do
        ticket.stubs(
          flags_allow_comment?: true,
          changed: ['requester_id'],
          attributes_changed?: true,
          delta_changes: { "requester_id" => [end_user.id, light_agent.id] }
        )
        disallowed_attributes = ['requester_id']

        expect_log("Flagged update access denied, disallowed changed attributes: #{disallowed_attributes}")
        expect_log("Disallowed light agent changes to: #{disallowed_attributes}")
        expect_light_agent_denied_log
        expect_cannot_save_log

        subject
      end
    end
  end
end
