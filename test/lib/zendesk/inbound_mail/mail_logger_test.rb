require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 2 # The warn method

describe Zendesk::InboundMail::MailLogger do
  let(:logger) { Zendesk::InboundMail::MailLogger.new(Rails.logger) }

  describe "#error" do
    let(:error_message) { "this is an error" }

    subject { logger.error(error_message) }

    it "reports message exception to Rollbar" do
      ZendeskExceptions::Logger.expects(:record).with do |e, params|
        assert_equal(StandardError, e.class)
        assert_equal(Zendesk::InboundMail::MailLogger, params[:location].class)
        assert_equal(error_message, params[:fingerprint])
        assert_equal(error_message, params[:message])
      end
      subject
    end
  end
end
