require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Zendesk::InboundMail::Dispatcher do
  class DummyWorker < Adrian::Worker
    COMPLETED_WORK = [] # rubocop:disable Style/MutableConstant

    def self.clear!
      @error = nil
      COMPLETED_WORK.clear
    end

    class << self
      attr_writer :error
    end

    class << self
      attr_reader :error
    end

    def account
      Account.new
    end

    def message
      @message ||= Zendesk::Mail::InboundMessage.new
    end

    def work
      raise self.class.error if self.class.error
      COMPLETED_WORK << item.path
    end
  end

  def assert_worked_on_file(file)
    assert_equal 1, DummyWorker::COMPLETED_WORK.size
    assert_equal File.basename(file.path), File.basename(DummyWorker::COMPLETED_WORK.first)
  end

  def assert_no_work_done
    assert_equal [], DummyWorker::COMPLETED_WORK
  end

  before do
    DummyWorker.clear!
    @maildir     = Pathname.new(Dir.mktmpdir('email_directory_test'))
    @new_dir     = @maildir + 'new'
    @backlog_dir = @maildir + 'backlog'
    @retry_dir   = @maildir + 'retry'
    @failed_dir  = @maildir + 'failed'
    @done_dir    = @maildir + 'done'
    @dispatcher  = Zendesk::InboundMail::Dispatcher.new(json_queue_path: @maildir, stop_when_done: true)
  end

  it "creates the new directory" do
    assert @new_dir.directory?
  end

  it "creates the backlog directory" do
    assert @backlog_dir.directory?
  end

  it "creates the retry directory" do
    assert @retry_dir.directory?
  end

  it "creates the failed directory" do
    assert @failed_dir.directory?
  end

  it "creates the done directory" do
    assert @done_dir.directory?
  end

  it "picks up files from the new directory" do
    @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
    file = Tempfile.new(['email_file_test', '.json'], @new_dir)

    @dispatcher.start
    assert_worked_on_file(file)
  end

  it "picks up files from the backlog directory after the new directory" do
    @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
    backlog_file = Tempfile.new(['backlog_file', '.eml'], @backlog_dir)
    new_file     = Tempfile.new(['new_file',     '.eml'], @new_dir)

    @dispatcher.start

    processed_files = DummyWorker::COMPLETED_WORK.map { |p| File.basename(p) }
    assert_equal [File.basename(new_file.path), File.basename(backlog_file.path)], processed_files
  end

  describe "files in the retry directory" do
    before do
      @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
      @file = Tempfile.new(['email_file_test', '.json'], @retry_dir)
    end

    it 'is picked up if they are old' do
      time = 30.minutes.ago.to_i
      File.utime(time, time, @file.path)

      @dispatcher.start

      assert_worked_on_file(@file)
    end

    it 'is not picked up if they are young' do
      time = 1.minutes.ago.to_i
      File.utime(time, time, @file.path)

      @dispatcher.start

      assert_no_work_done
    end

    it 'becomes unprocessable if they are very old' do
      time = 3.days.ago.to_i
      File.utime(time, time, @file.path)

      @dispatcher.expects(:create_unprocessable).returns(true)
      @dispatcher.start

      assert_no_work_done
    end
  end

  it "uses the Zendesk::InboundMail::AdrianTicketProcessingWorker" do
    assert_equal Zendesk::InboundMail::AdrianTicketProcessingWorker, @dispatcher.worker_class
  end

  describe "when all goes well" do
    before do
      @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
      @file = Tempfile.new(['email_file_test', '.json'], @new_dir)
    end

    subject { @dispatcher.start }

    it "moves the file to done" do
      subject
      assert (@done_dir + File.basename(@file.path)).file?
    end

    it "increments :processed result:success statsd metric" do
      Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:success']).once
      subject
    end
  end

  describe "when an error happens inside adrian" do
    before do
      queue = Object.new
      def queue.pop
        raise 'oh no!'
      end

      @dispatcher.instance_variable_set(:@queue, queue)
    end

    it 'logs the exception' do
      ZendeskExceptions::Logger.expects(:record).with(instance_of(RuntimeError), message: 'Error dispatching work')
      assert_raises(RuntimeError) do
        @dispatcher.start
      end
    end
  end

  describe "when retry attempts keep failing, and an unprocessable suspension is created" do
    before do
      Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(instance_of(String), instance_of(Adrian::Queue::ItemTooOldError))
      DummyWorker.error = StandardError
      @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
      @file = Tempfile.new(['email_file_test', '.json'], @retry_dir)

      @dispatcher.expects(:create_unprocessable).returns(true)
      Timecop.travel(2.hours.from_now + 1) { @dispatcher.start }
    end

    it "moves the file to done" do
      assert (@done_dir + File.basename(@file.path)).file?
    end
  end

  describe "when retry attempts keep failing and the item is older than the max age, and an unprocessable suspension is not created" do
    subject { Timecop.travel(2.hours.from_now + 1) { @dispatcher.start } }

    before do
      Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(instance_of(String), instance_of(Adrian::Queue::ItemTooOldError))
      DummyWorker.error = StandardError
      @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
      @file = Tempfile.new(['email_file_test', '.json'], @retry_dir)

      @dispatcher.expects(:create_unprocessable).returns(false)
    end

    it "moves the file to failed" do
      subject
      assert (@failed_dir + File.basename(@file.path)).file?
    end

    describe "when the Arturo email_dispatcher_unprocessable_failed is disabled" do
      before { Arturo::Feature.create!(symbol: :email_dispatcher_unprocessable_failed, phase: "off") }

      it "does not increment the unprocessable_not_created metric, but increments the processed result:failed metric" do
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:unprocessable_not_created, tags: ["source:adrian_dispatcher"]).never
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:failed']).once
        subject
      end
    end

    describe "when the Arturo email_dispatcher_unprocessable_failed is enabled" do
      before { Arturo::Feature.create!(symbol: :email_dispatcher_unprocessable_failed, phase: "on") }

      it "increments the processed result:failed and unprocessable_not_created metrics" do
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:unprocessable_not_created, tags: ["source:adrian_dispatcher"]).once
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:failed']).once
        subject
      end
    end
  end

  describe "when an exception happens" do
    before do
      Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(instance_of(String), instance_of(StandardError))
      DummyWorker.error = StandardError
      @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
      @file = Tempfile.new(['email_file_test', '.json'], @new_dir)
      @dispatcher.start
    end

    it "moves the file to retry" do
      assert (@retry_dir + File.basename(@file.path)).file?
    end
  end

  describe "when a SignalException happens" do
    subject { @dispatcher.start }

    before do
      DummyWorker.error = Interrupt
      @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
      @file = Tempfile.new(['email_file_test', '.json'], @new_dir)
    end

    it 'logs a waring using DispatchFailureHandler' do
      Zendesk::InboundMail::DispatchFailureHandler.expects(:warn).once
      assert_raises(SystemExit) { subject }
    end

    it "increments :sigterm, :retries and :error statsd metrics then exits" do
      Zendesk::InboundMail::StatsD.client.expects(:increment).with(:sigterm).once
      Zendesk::InboundMail::StatsD.client.expects(:increment).with(:retries).once
      Zendesk::InboundMail::StatsD.client.expects(:increment).with(:error).once

      assert_raises(SystemExit) { subject }
    end

    it "moves the file to retry" do
      assert_raises(SystemExit) { subject }
      assert (@retry_dir + File.basename(@file.path)).file?
    end
  end

  describe "when a Zendesk::InboundMail::ProcessorTimeout happens" do
    before do
      DummyWorker.error = Zendesk::InboundMail::ProcessingTimeout
      @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
      @file = Tempfile.new(['email_file_test', '.json'], @new_dir)
    end

    it 'logs and notifies DispatchFailureHandler' do
      Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(instance_of(String), instance_of(DummyWorker.error))
      Zendesk::InboundMail::DispatchFailureHandler.expects(:notify).with(instance_of(String), instance_of(DummyWorker.error), 'failed and will retry', instance_of(Hash))
      assert_raises(SystemExit) { @dispatcher.start }
    end

    it "increments :retries, :error and :processing_timeout statsd metrics then exits" do
      Zendesk::InboundMail::DispatchFailureHandler.stubs(:log)
      Zendesk::InboundMail::DispatchFailureHandler.stubs(:notify)

      Zendesk::InboundMail::StatsD.client.expects(:increment).with(:retries).once
      Zendesk::InboundMail::StatsD.client.expects(:increment).with(:error).once
      Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processing_timeout, tags: ['type:generic']).once

      assert_raises(SystemExit) { @dispatcher.start }
    end
  end

  describe "when an exception happens on retry" do
    before do
      Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(instance_of(String), instance_of(StandardError))
      DummyWorker.error = StandardError
      @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
      @file = Tempfile.new(['email_file_test', '.json'], @retry_dir)
    end

    before do
      Zendesk::InboundMail::DispatchFailureHandler.expects(:notify).with(instance_of(String), instance_of(StandardError), "failed and will retry", instance_of(Hash))
      Timecop.travel(30.minutes.from_now + 1) { @dispatcher.start }
    end

    it "moves the file to retry" do
      assert (@retry_dir + File.basename(@file.path)).file?
    end
  end

  describe "when a Zendesk::InboundMail::SyntaxError happens" do
    before do
      # Notify exception trackets
      Zendesk::InboundMail::DispatchFailureHandler.expects(:notify).with(instance_of(String), instance_of(Zendesk::InboundMail::SyntaxError), "permanently failed", instance_of(Hash))

      # Log the exception
      Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(instance_of(String), instance_of(Zendesk::InboundMail::SyntaxError))
      DummyWorker.error = Zendesk::InboundMail::SyntaxError
      @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
      @file = Tempfile.new(['email_file_test', '.json'], @new_dir)
      @dispatcher.expects(:create_unprocessable).returns(false)
      @dispatcher.start
    end

    it "moves the file to failed" do
      assert (@failed_dir + File.basename(@file.path)).file?
    end
  end

  describe "when a Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout happens" do
    before do
      error_type = Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout
      Zendesk::InboundMail::DispatchFailureHandler.expects(:notify).with(instance_of(String), instance_of(error_type), "ContentProcessor timed out", instance_of(Hash))
      Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(instance_of(String), instance_of(error_type))
      DummyWorker.error = error_type
      @dispatcher.instance_variable_set(:@worker_class, DummyWorker)
      @dispatcher.expects(:create_unprocessable).returns(true)
      @file = Tempfile.new(['email_file_test', '.json'], @new_dir)
    end

    it "moves the file to done" do
      @dispatcher.start
      assert (@done_dir + File.basename(@file.path)).file?
    end

    it "increments :processed result:failed, and :processing_timeout statsd metrics" do
      Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:failed']).once
      Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processing_timeout, tags: ['type:content_processor'])
      @dispatcher.start
    end
  end
end
