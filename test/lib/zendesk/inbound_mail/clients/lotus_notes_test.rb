require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Mail::Clients::LotusNotes do
  subject { Zendesk::Mail::Clients::LotusNotes }

  let(:mail) { FixtureHelper::Mail.read("forwards/lotus_notes/lotus_notes_2013.json") }

  it "detects the client" do
    assert_equal Zendesk::Mail::Clients::LotusNotes, mail.client
  end
end
