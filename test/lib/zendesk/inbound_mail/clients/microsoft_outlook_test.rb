require_relative "../../../../support/test_helper"
require "nokogiri"

SingleCov.covered!

describe Zendesk::Mail::Clients::MicrosoftOutlook do
  extend ArturoTestHelper

  subject { Zendesk::Mail::Clients::MicrosoftOutlook }

  describe "forward_html_element" do
    let(:html_content) do
      mail.parts.detect do |part|
        part.mime_type == Mime[:html] && !part.attachment?
      end&.body&.to_s
    end

    before do
      mail.content.stubs(:html).returns(html_content)
      mail.account = Account.new
    end

    describe "when divRplyFwdMsg has a sibling" do
      let(:mail) { FixtureHelper::Mail.read("forwards/microsoft_outlook/outlook_dot_com_20200413_sample.json") }
      let(:div_content) { "Hello" }

      it "parses the content correctly" do
        forward_html = Zendesk::Mail::Clients::MicrosoftOutlook.forward_html_body(mail, nil)
        assert forward_html.include?(div_content)
        refute forward_html.include?("From:")
      end
    end

    describe "when divRplyFwdMsg does not have a sibling" do
      describe "example 1" do
        let(:mail) { FixtureHelper::Mail.read("forwards/microsoft_outlook/z1_5052185_sample_1.json") }
        let(:div_content) { "Hi Ashley" }

        it "parses the content correctly" do
          forward_html = Zendesk::Mail::Clients::MicrosoftOutlook.forward_html_body(mail, nil)
          assert forward_html.include?(div_content)
          refute forward_html.include?("From:")
        end
      end

      describe "example 2" do
        let(:mail) { FixtureHelper::Mail.read("forwards/microsoft_outlook/z1_5052185_sample_2.json") }
        let(:div_content) { "Success!" }

        it "parses the content correctly" do
          forward_html = Zendesk::Mail::Clients::MicrosoftOutlook.forward_html_body(mail, nil)
          assert forward_html.include?(div_content)
          refute forward_html.include?("From:")
        end
      end

      describe "example 3" do
        let(:mail) { FixtureHelper::Mail.read("forwards/microsoft_outlook/z1_5052185_sample_3.json") }
        let(:div_content) { "Hi Barry" }

        it "parses the content correctly" do
          forward_html = Zendesk::Mail::Clients::MicrosoftOutlook.forward_html_body(mail, nil)
          assert forward_html.include?(div_content)
          refute forward_html.include?("From:")
        end
      end
    end
  end
end
