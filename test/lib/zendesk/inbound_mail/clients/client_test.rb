require_relative "../../../../support/test_helper"
require_relative "../../../../support/mail_test_helper"

SingleCov.covered! uncovered: 17

describe Zendesk::Mail::Clients::Client do
  fixtures :accounts

  def self.client_and_variation_from_path(file_name)
    file_name =~ /(?:replies|forwards)\/(.*?)(?:\/(.*))?.(eml|json)/
    client = $1.camelize
    variation = $2
    [client, variation]
  end

  subject { Zendesk::Mail::Clients::Client }

  let(:mail) { Zendesk::Mail::InboundMessage.new }
  let(:account) { accounts(:minimum) }

  # TicketProcessingWorker normally would apply the account to the "mail"/InboundMessage object
  before { Zendesk::Mail::InboundMessage.any_instance.stubs(account: account) }

  describe '#basename' do
    it "returns the basename of the client" do
      assert_equal 'client', subject.basename
      assert_equal 'gmail', Zendesk::Mail::Clients::Gmail.basename
      assert_equal 'unknown', Zendesk::Mail::Clients::Unknown.basename
      assert_equal 'apple_mail', Zendesk::Mail::Clients::AppleMail.basename
    end
  end

  describe '#replying' do
    it "returns a Replier instance" do
      replier = subject.replying
      assert replier.instance_of?(Zendesk::InboundMail::Replier)

      second_replier = subject.replying
      assert_equal replier, second_replier
    end
  end

  describe "#forward_html_body" do
    describe "when there is an error extracting the forward body" do
      let(:header_text) { "" }

      it "returns nil" do
        mail.logger.stubs(:info)
        mail.logger.expects(:info).with(regexp_matches(/^Can't extract the forward's HTML body:*/)).once
        assert_nil subject.forward_html_body(mail, header_text)
      end
    end
  end

  describe '#inline_attachments?' do
    it { refute subject.inline_attachments? }
  end

  describe '#prefer_html?' do
    it { refute subject.prefer_html?(mail) }

    describe "from Outlook mailer" do
      it "is true when mailer contains the word outlook" do
        mail.mailer = "FooOutlookBar"
        assert subject.find_by_mail(mail).prefer_html?(mail)
      end

      it "is true when User-Agent contains the word outlook" do
        mail.headers["User-Agent"] = "FooOutlookBar"
        assert subject.find_by_mail(mail).prefer_html?(mail)
      end

      it "is false when the client is unknown and mailer and User-Agent are blank" do
        refute subject.find_by_mail(mail).prefer_html?(mail)
      end

      it "is false if mailer is present and not outlook" do
        mail.mailer = "FooInlookBar"
        refute subject.find_by_mail(mail).prefer_html?(mail)
      end

      it "is false when User-Agent is present and not outlook" do
        mail.headers["User-Agent"] = "FooInlookBar"
        refute subject.find_by_mail(mail).prefer_html?(mail)
      end
    end
  end

  describe '#autolinking?' do
    it { refute subject.autolinking?(mail) }
  end

  describe "Forwarded mail" do
    mail_fixture_path = "#{Rails.root}/test/files/inbound_mailer"

    before do
      @account = accounts(:minimum)
      @original_body = File.read(FixtureHelper::Mail.path("default.txt")).strip
    end

    describe 'when the forward mail is deeply nested' do
      before do
        @max_nokogiri_tree_depth = Zendesk::Mail::Clients::Client::MAX_NOKOGIRI_TREE_DEPTH
        Zendesk::Mail::Clients::Client.send(:remove_const, :MAX_NOKOGIRI_TREE_DEPTH)
        Zendesk::Mail::Clients::Client.const_set('MAX_NOKOGIRI_TREE_DEPTH', max_nokogiri_tree_depth)
        mail = FixtureHelper::Mail.read("#{mail_fixture_path}/forwards/aol_mail/aol_with_x_aol_sid.json") do |json|
          html_content = json['parts'].find { |part| part['headers']['content-type'].include?("text/html") }
          html_content['body'] = nest_html_content(html_content['body'], Nokogumbo::DEFAULT_MAX_TREE_DEPTH + 1)
          json
        end
        @forward = Zendesk::InboundMail::Forward.from_mail(mail, mail.content.text)
      end

      after do
        Zendesk::Mail::Clients::Client.send(:remove_const, :MAX_NOKOGIRI_TREE_DEPTH)
        Zendesk::Mail::Clients::Client.const_set(:MAX_NOKOGIRI_TREE_DEPTH, @max_nokogiri_tree_depth)
      end

      describe "and the maximum tree depth option is configured" do
        let(:max_nokogiri_tree_depth) { Nokogumbo::DEFAULT_MAX_TREE_DEPTH }

        it "raises a 'ArgumentError: Document tree depth limit exceeded' error" do
          assert_raises ArgumentError do
            @forward.html_body
          end
        end
      end

      describe "and the maximum tree depth is removed" do
        let(:max_nokogiri_tree_depth) { -1 }

        it "does not raise a 'ArgumentError: Document tree depth limit exceeded' error" do
          @forward.html_body
        end
      end
    end

    # Note: Generic client doesn't strip forwarding headers since we don't want to accidentally strip useful info.
    Dir["#{mail_fixture_path}/forwards/**/*.json"].each do |file_name|
      next if file_name =~ /eventually/
      mail = FixtureHelper::Mail.read(file_name)

      # Skip fixtures that weren't intended for this test
      next if mail.headers.key?("x-skip-forwards-test-assertions")

      client_name, variation = client_and_variation_from_path(file_name)
      begin
        client = mail.headers.key?("x-expected-client") ? mail.headers["x-expected-client"].first.strip.to_sym : client_name
        client = Zendesk::Mail::Clients.const_get(client)
      rescue NameError
        client = Zendesk::Mail::Clients::Generic
      end

      describe "from #{client_name} (#{variation})" do
        before do
          @mail = mail
          @client = client

          content_file_name = FixtureHelper::Mail.path(file_name.gsub(/json$/, "content.txt"))
          if File.exist?(content_file_name)
            @original_body = File.read(content_file_name).strip
          end

          @expected_author = (@mail.headers["x-expected-author"] || ["Walt Whitman <walt@example.com>"]).join
          @expected_subject = (@mail.headers["x-expected-subject"] || ["Thoughts?"]).join

          @plain_content = @mail.content.text
          @forward = Zendesk::InboundMail::Forward.from_mail(@mail, @plain_content)
        end

        it "detects the composing client" do
          assert_equal @client, @mail.client
        end

        it "provides original author" do
          assert_equal Zendesk::Mail::Address.parse(@expected_author).to_s, @forward.from.to_s
        end

        it "provides original subject" do
          assert_equal @expected_subject.to_s, @forward.subject.to_s
        end

        it "provides original body" do
          assert_equal @original_body.to_s, @forward.body.to_s
        end

        html_file_name = FixtureHelper::Mail.path(file_name.gsub(/json$/, "html_content"))
        if File.exist?(html_file_name)
          it "provides original html body" do
            skip("Not testing HTML part for #{file_name}") if html_file_name =~ /eventually/
            original_html_body = File.read(html_file_name).strip
            @forward.html_body.strip.html_must_equal original_html_body
          end
        else
          it "does not return the original html body" do
            assert_nil @forward.html_body
          end
        end
      end
    end
  end

  describe "Mail replies" do
    mail_fixture_path = "#{Rails.root}/test/files/inbound_mailer"

    Dir["#{mail_fixture_path}/replies/**/*.json"].each do |file_name|
      next if file_name =~ /eventually/ || file_name =~ /no_delimiter_parsing/
      mail = FixtureHelper::Mail.read(file_name)

      # Skip fixtures that weren't intended for this test
      next if mail.headers.key?("x-skip-replies-test-assertions")

      client, variation = client_and_variation_from_path(file_name)

      describe "from #{client} (#{variation})" do
        before do
          @client = Zendesk::Mail::Clients.const_get(client)
          @mail = mail
        end

        it "detects the composing client" do
          assert_equal @client, @mail.client
        end

        it "detects original text body" do
          reply_file_name = FixtureHelper::Mail.path(file_name.gsub(/json/, "content.txt"))

          message = "Thanks for the message!\n\n-- \nRegards, \n\nBuddy \nThe Buddha  \nSupport requests: support@example.com"
          message = File.read(reply_file_name) if File.exist?(reply_file_name)
          part = @mail.first_of_type(Mime[:text]) || @mail
          match = @mail.client.replying.body.match(part.body)

          assert match, "Failed to match #{@client}"
          assert_equal match.pre_match.rstrip, message, "Mismatch for #{@client}"
        end

        it "detects that it's a reply" do
          assert(@mail.reply?)
        end

        it "has a reasonable limit to prevent overly greedy reply header matching" do
          part = @mail.first_of_type(Mime[:text]) || @mail
          reply_header = @mail.client.replying.body.match(part.body.to_s).to_s
          limit = @mail.client.replying.limit

          assert reply_header.size * 1.5 < limit, "Limit excludes valid headers: size: #{reply_header.size} limit: #{limit} - #{reply_header}"
          assert reply_header.size * 3.75 > limit, "Limit allows invalid headers: size: #{reply_header.size} limit: #{limit} - #{reply_header}"
        end
      end
    end
  end

  private

  def nest_html_content(html_content, nesting)
    "#{'<div>' * nesting}#{html_content}#{'</div>' * nesting}"
  end
end
