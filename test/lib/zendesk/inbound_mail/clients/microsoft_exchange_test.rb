require_relative "../../../../support/test_helper"
require 'nokogiri'

SingleCov.covered! uncovered: 16

describe Zendesk::Mail::Clients::MicrosoftExchange do
  extend ArturoTestHelper

  subject { Zendesk::Mail::Clients::MicrosoftExchange }

  let(:examples_path) { "replies/microsoft_exchange" }
  let(:mail) { FixtureHelper::Mail.read("#{examples_path}/microsoft_office_outlook.json") }

  it "detects it's a reply" do
    assert mail.reply?
  end

  it "detects the client" do
    assert subject.composed?(mail), "Failed composed?"
    assert_equal Zendesk::Mail::Clients::MicrosoftExchange, mail.client
  end

  describe ".client_pattern" do
    it "matches beginning of line or whitespace followed by 'Outlook'" do
      assert_match subject.client_pattern, 'Outlook'
      assert_match subject.client_pattern, 'Microsoft Outlook'
      refute_match subject.client_pattern, 'Yahoo'
    end
  end

  describe ".prefer_html?" do
    it { assert subject.prefer_html?(nil) }
  end

  describe ".autolinking?" do
    it { assert subject.autolinking?(nil) }
  end

  describe ".composed?" do
    let(:mail) { Zendesk::Mail::InboundMessage.new }

    describe "when the mailer matches the client pattern" do
      before { mail.mailer = "Outlook" }
      it { assert subject.composed?(mail) }
    end

    describe "when the mailer does not match the client pattern" do
      before { mail.mailer = "Yahoo" }
      it { refute subject.composed?(mail) }
    end
  end

  describe "with reply-headers" do
    let(:part) { mail.first_of_type(Mime[:text]) }
    let(:reply_headers) { mail.client.replying.body.match(part.body.to_s).to_s }

    it "detects the reply-headers" do
      refute_blank reply_headers
    end

    it "limits the reply-headers size" do
      assert reply_headers.size < mail.client.replying.limit
    end

    it "detects the body" do
      assert mail.client.replying.body.match(part.body.strip), "Failed to match"
    end

    describe "in other languages" do
      let(:mail) { FixtureHelper::Mail.read("#{examples_path}/german_reply.json") }
      let(:part) { mail.first_of_type(Mime[:text]) }
      let(:reply_headers) { mail.client.replying.body.match(part.body.to_s).to_s }

      it "detects the reply-headers" do
        refute_blank reply_headers
      end

      it "limits the reply-headers size" do
        assert reply_headers.size < mail.client.replying.limit
      end

      it "detects the body" do
        assert mail.client.replying.body.match(part.body.to_s), 'Failed to match'
      end
    end
  end

  describe "forward_html_element" do
    let(:header_text) { "From: Guy, Bad [mailto:bad.guy@example.com]\nSent: Wednesday, October 26, 2016 12:54 PM\nTo: James Bond &lt;jbond@example.com&gt;\nCc: mom@example.com &lt;mom@example.com&gt;\nSubject: RE: Mission" }
    let(:html) { FixtureHelper::Files.read("inbound_mailer/forwards/microsoft_exchange/with_private_comment2.html_content") }

    before do
      mail.content.stubs(:html).returns(html)
      mail.account = Account.new
    end

    it 'does not contain private comment' do
      forward_html = Zendesk::Mail::Clients::MicrosoftExchange.forward_html_body(mail, header_text)
      refute forward_html.include?("Private Agent Comment"), "Forward HTML should not include private comment"
    end
  end
end
