require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Mail::Clients::Unknown do
  let(:mail) { FixtureHelper::Mail.read("replies/unknown.json") }

  it "detects it's a reply" do
    assert mail.reply?
  end

  it "uses the Generic client" do
    assert_equal Zendesk::Mail::Clients::Unknown, mail.client
  end

  describe "with reply-headers" do
    let(:reply_headers) { mail.client.replying.body.match(mail.body) }

    it "detects the reply-headers" do
      refute_blank reply_headers
    end

    it "limits the reply-headers size" do
      assert reply_headers.size < mail.client.replying.limit
    end

    it "detects the body" do
      assert mail.client.replying.body.match(mail.body.strip), 'Failed to match'
    end
  end

  describe "in another languages" do
    let(:mail) { FixtureHelper::Mail.read("replies/generic_spanish.json") }
    let(:reply_headers) { mail.client.replying.body.match(mail.body) }

    it "detects the reply-headers" do
      refute_blank reply_headers
    end

    it "limits the reply-headers size" do
      assert reply_headers.size < mail.client.replying.limit
    end

    it "detects the body" do
      assert mail.client.replying.body.match(mail.body.strip), 'Failed to match'
    end
  end
end
