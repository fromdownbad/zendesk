require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Mail::Clients::Gmail do
  fixtures :accounts

  subject { Zendesk::Mail::Clients::Gmail }

  let(:mail) { FixtureHelper::Mail.read('clients/gmail.json') }
  let(:account) { accounts(:minimum) }

  before { mail.account = Account.new }

  it "detects the correct client" do
    assert_equal subject, mail.client
  end

  it "prefers html content" do
    refute_match mail.content.text, 'This is the plaintext part'
  end

  describe "#forward_html_element" do
    let(:html_content) { part(mail, :html) }
    let(:plain_content) { part(mail, :properties) }
    let(:forward) { Zendesk::InboundMail::Forward.new(mail, plain_content) }
    let(:header_text) { forward.send(:header_text) }
    let(:mail) { FixtureHelper::Mail.read("forwards/gmail/gmail_with_signature.json") }
    let(:div_content) { "Hi there" }
    let(:signature_content) { "Austin Wang" }

    before do
      mail.content.stubs(:html).returns(html_content)
      mail.account = Account.new
    end

    describe "when there is no forwarded content" do
      let(:mail) { FixtureHelper::Mail.read("html_gmail_reply.json") }

      it "returns nil" do
        assert_nil subject.forward_html_body(mail, header_text)
      end
    end

    it "excludes the signature in the parsed content" do
      forward_html = subject.forward_html_body(mail, header_text)
      assert forward_html.include?(div_content)
      refute forward_html.include?(signature_content)
      refute forward_html.include?("From:")
    end

    describe 'for a gmail thread' do
      let(:mail) { FixtureHelper::Mail.read("forwards/gmail/gmail_thread_with_signature.json") }
      let(:div_content) { "Responding to end-user" }
      let(:signature_content) { "My Signature" }

      it "excludes the signature in the parsed content" do
        forward_html = subject.forward_html_body(mail, header_text)
        assert forward_html.include?(div_content)
        assert_equal 1, forward_html.scan(signature_content).size
      end
    end
  end

  private

  def part(mail, mime_type)
    mail.parts.detect do |part|
      part.mime_type == Mime[mime_type] && !part.attachment?
    end&.body&.to_s
  end
end
