require_relative "../../../../support/test_helper"
require 'nokogumbo'

SingleCov.covered!

describe Zendesk::Mail::Clients::Yahoo do
  subject { Zendesk::Mail::Clients::Yahoo }

  let(:examples_path) { "replies/yahoo" }
  let(:mail) { FixtureHelper::Mail.read("#{examples_path}/yahoo.json") }

  before { mail.account = Account.new }

  it "detects it's a reply" do
    assert mail.reply?
  end

  it "detects the client" do
    assert subject.composed?(mail), "Failed composed?"
    assert_equal Zendesk::Mail::Clients::Yahoo, mail.client
  end

  describe ".composed?" do
    let(:mail) { Zendesk::Mail::InboundMessage.new }

    describe "when the mailer matches the client pattern" do
      before { mail.mailer = "YMail" }
      it { assert subject.composed?(mail) }
    end

    describe "when the mailer does not match the client pattern" do
      before { mail.mailer = "Outlook" }
      it { refute subject.composed?(mail) }

      describe "having Yahoo's outbound spam guard header" do
        before { mail.headers['X-YMail-OSG'] = '.TGVJH0wbMwO...RNHXjdnhk-' }
        it { assert subject.composed?(mail) }
      end
    end
  end

  describe ".forward_html_element" do
    describe "with Yahoo's message container" do
      let(:mail) { FixtureHelper::Mail.read("#{examples_path}/yahoo_spanish.json") }
      let(:expected_html) do
        "<div dir=\"ltr\" id=\"yiv7203092573yui_3_16_0_ym19_1_1468444198648_5073\">Yahoo sends an email to Outlook<br clear=\"none\">\n</div>"
      end

      it "returns the HTML elements" do
        # NOTE: Nokogiri::HTML5 constant is provided by Nokogumbo, not Nokogiri.
        assert_equal expected_html, subject.forward_html_element(mail, nil).to_s
      end
    end
  end

  describe "with reply-headers" do
    let(:part) { mail.first_of_type(Mime[:text]) }
    let(:reply_headers) { mail.client.replying.body.match(part.body.to_s).to_s }

    it "detects the reply-headers" do
      refute_blank reply_headers
    end

    it "limits the reply-headers size" do
      assert reply_headers.size < mail.client.replying.limit
    end

    it "detects the body" do
      match = mail.client.replying.body.match(mail.body.strip)
      assert match, "Failed to match"
    end

    describe "in other languages" do
      let(:mail) { FixtureHelper::Mail.read("#{examples_path}/yahoo_spanish.json") }
      let(:part) { mail.first_of_type(Mime[:text]) }
      let(:reply_headers) { mail.client.replying.body.match(part.body.to_s).to_s }

      it "detects the reply-headers" do
        refute_blank reply_headers
      end

      it "limits the reply-headers size" do
        assert reply_headers.size < mail.client.replying.limit
      end

      it "detects the body" do
        assert mail.client.replying.body.match(part.body.to_s), 'Failed to match'
      end
    end
  end
end
