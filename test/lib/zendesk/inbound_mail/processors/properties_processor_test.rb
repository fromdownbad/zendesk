require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Zendesk::InboundMail::Processors::PropertiesProcessor do
  def execute
    Zendesk::InboundMail::Processors::PropertiesProcessor.new(@state, @mail).execute
  end

  let(:processor) { Zendesk::InboundMail::Processors::PropertiesProcessor.new(@state, @mail) }

  before do
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = accounts(:minimum)
    @user = @state.account.owner
    @mail = Zendesk::Mail::InboundMessage.new
  end

  describe "#parse_mail" do
    def parse_mail(*args)
      processor.send(:parse_mail, *args)
    end

    it "does not blow up when the mail doesn't have a subject" do
      @mail.subject = nil
      parse_mail
    end

    it "skips unknown properties" do
      parse_mail('Hello {{unknown_property:true}}').must_be_nil
    end

    it "skips uknown properties even in the presence of valid properties" do
      props = parse_mail('Wibble {{fibble:"oogle" tags:"hello there"}}')
      assert_equal 1, props.keys.size
      assert_equal props[:set_tags], "hello there"
    end

    it "does not build users with unparsable names" do
      props = parse_mail("Wiffle {{requester:unparsable name@example.com}} Woffle")
      assert props.nil?
    end

    it "handles bad content gracefully" do
      props = parse_mail('{{{{ EVIL " }}AND " { requester:"stupid}} MAN}')
      assert props.nil?
    end

    it "parses out multiple values correctly" do
      subject = 'Hello {{status:pending tags:"love hate tree" ticket_type:question public:fAlsE requester_name:"Morten Primdahl" requester_phone: 1234567890}}'
      props = parse_mail(subject)

      assert_equal props[:status_id], StatusType.PENDING
      assert_equal props[:ticket_type_id], TicketType.QUESTION
      assert_equal props[:set_tags], "love hate tree"
      assert_equal props[:is_public], false
      assert_equal props[:requester_name], "Morten Primdahl"
      assert_equal props[:requester_phone], "+1234567890"
    end

    it "parses key-value pairs with whitespace in between them" do
      subject = "re: [tech assistant] re: houston, apr 6-7 intro to nat gas trading & hedging {{status: solved}} [6wt9-hj3m]"
      props = parse_mail(subject)
      assert_equal props[:status_id], StatusType.SOLVED
    end

    it "considers quoted and unquoted values the same when quotes contain no white space" do
      props = parse_mail('Hello {{status:pending }}')
      assert_equal props[:status_id], StatusType.PENDING
      props = parse_mail('Hello {{status:"pending"}}')
      assert_equal props[:status_id], StatusType.PENDING
    end
  end

  describe "#execute" do
    it "extracts properties from multi-line subjects" do
      mail = FixtureHelper::Mail.read('multi_line_subject.json')
      expected_properties = { requester_email: "minimum_author@aghassipour.com", group_name: "minimum_group" }
      Zendesk::InboundMail::Processors::PropertiesProcessor.new(@state, mail).execute
      assert_equal expected_properties, @state.properties
    end

    describe "Formatting the mail's subject" do
      before do
        @mail = FixtureHelper::Mail.read('minimum_ticket.json')
      end

      it "removes all properties" do
        @mail.subject = "Wiffle {{assignee:12345}} Woffle"
        execute
        assert_equal 'Wiffle  Woffle', @mail.subject
      end

      it "works without properties" do
        @mail.subject = "Wiffle Woffle"
        execute
        assert_equal 'Wiffle Woffle', @mail.subject
      end
    end
  end

  describe "#resolve_user" do
    def resolve_user(*args)
      processor.send(:resolve_user, *args)
    end

    it "identifys an integer" do
      assert_equal({ requester_id: "123" }, resolve_user({}, :requester, "123"))
    end

    it "identifys an email address" do
      assert_equal({ requester_email: "123@boo.com" }, resolve_user({}, :requester, "123@boo.com"))
    end

    it "does not identify a bogus string" do
      assert_equal({}, resolve_user({}, :requester, "123xboo.com"))
    end
  end

  describe "#resolve_named_user" do
    it "identifys a string" do
      assert_equal({ assignee_name: "meh" }, processor.send(:resolve_named_user, {}, :assignee, "meh"))
    end

    it "does not identify blanks" do
      assert_equal({ }, processor.send(:resolve_named_user, {}, :assignee, ""))
    end
  end

  describe "#resolve_requester_name" do
    it "allows us to parse out a requester name" do
      assert_equal({ requester_name: "Morten" }, processor.send(:resolve_requester_name, {}, :requester_name, "Morten"))
      assert_equal({ requester_name: "Morten Primdahl" }, processor.send(:resolve_requester_name, {}, :requester_name, "Morten Primdahl"))
    end
  end

  describe "#resolve_requester_phone" do
    it "allows us to parse out a requester phone" do
      assert_equal({ requester_phone: "+14129996294" }, processor.send(:resolve_requester_phone, {}, :requester_phone, "14129996294"))
      assert_equal({ requester_phone: "+19845044818" }, processor.send(:resolve_requester_phone, {}, :requester_phone, "19845044818"))
    end
  end

  describe "#resolve_group" do
    it "identifys an integer" do
      assert_equal({ group_id: "123" }, processor.send(:resolve_group, {}, :group, "123"))
    end

    it "identifys a name" do
      assert_equal({ group_name: "Hallo" }, processor.send(:resolve_group, {}, :group, "Hallo"))
    end
  end

  describe "#resolve_access" do
    it "identifys an boolean" do
      assert_equal({ is_public: true  }, processor.send(:resolve_access, {}, :public, "true"))
      assert_equal({ is_public: false }, processor.send(:resolve_access, {}, :public, "false"))
    end

    it "defaults to true" do
      assert_equal({ is_public: true }, processor.send(:resolve_access, {}, :public, "hello"))
    end
  end

  describe "#resolve_tags" do
    it "identifys a string" do
      assert_equal({ set_tags: "foo far" }, processor.send(:resolve_tags, {}, :tags, "foo far"))
    end
  end

  describe "#resolve_state" do
    it "converts valid ticket type ids" do
      names = TicketType.fields.map { |t| TicketType[t] }.collect(&:name)
      names.each do |name|
        assert_equal({ ticket_type_id: TicketType.find(name) }, processor.send(:resolve_state, {}, :ticket_type, name))
      end
    end

    it "ignores invalid ticket type ids" do
      assert_equal({}, processor.send(:resolve_state, {}, :ticket_type, "drunk"))
    end

    it "converts valid status ids" do
      names = StatusType.fields.map { |t| StatusType[t] }.collect(&:name)
      names.each do |name|
        assert_equal({ status_id: StatusType.find(name) }, processor.send(:resolve_state, {}, :status, name))
      end
    end

    it "converts status on-hold" do
      assert_equal({ status_id: StatusType.HOLD }, processor.send(:resolve_state, {}, :status, 'on-hold'))
      assert_equal({ status_id: StatusType.HOLD }, processor.send(:resolve_state, {}, :status, 'hold'))
    end

    it "ignores invalid status ids" do
      assert_equal({}, processor.send(:resolve_state, {}, :status, "drunk"))
    end

    it "converts valid priorities" do
      names = PriorityType.fields.map { |t| PriorityType[t] }.collect(&:name)
      names.each do |name|
        assert_equal({ priority_id: PriorityType.find(name) }, processor.send(:resolve_state, {}, :priority, name))
      end
    end

    it "ignores invalid priority ids" do
      assert_equal({}, processor.send(:resolve_state, {}, :priority, "drunk"))
    end
  end
end
