require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::ExcessRecipientsProcessor do
  include ArturoTestHelper
  include MailTestHelper
  fixtures :accounts, :users

  def execute(state, mail)
    Zendesk::InboundMail::Processors::ExcessRecipientsProcessor.execute(state, mail)
  end

  def set_mail_ccs(mail, ccs = [])
    ccs.each { |cc| mail.cc.push(Zendesk::Mail::Address.new(address: cc)) }
  end

  let(:ccs) { [] }
  let(:ticket_email_ccs_suspension_threshold) { 5 }

  before do
    @account = accounts(:minimum)
    @account.ticket_email_ccs_suspension_threshold = ticket_email_ccs_suspension_threshold
    @mail = Zendesk::Mail::InboundMessage.new
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = @account
    @state.suspension = nil

    set_mail_ccs(@mail, ccs)
  end

  describe_with_arturo_enabled :email_ticket_email_ccs_suspension_threshold do
    describe 'when the ticket has already been suspended' do
      it 'does not overwrite the existing suspension' do
        @state.suspension = 20
        execute(@state, @mail)
        assert_equal 20, @state.suspension
        assert_not_equal SuspensionType.NUMBER_RECIPIENTS_EXCEEDS_THRESHOLD, @state.suspension
      end
    end

    describe 'when the ticket author is not an agent' do
      before do
        @state.author = users(:minimum_author)
      end

      describe 'when amount of email recipients is greater than the account-configured threshold' do
        let(:ccs) { Array.new(ticket_email_ccs_suspension_threshold + 1) { |n| "support#{n}@example.com" } }

        it "suspends the ticket" do
          execute(@state, @mail)
          assert_equal SuspensionType.NUMBER_RECIPIENTS_EXCEEDS_THRESHOLD, @state.suspension
        end

        describe "and whitelist applies to the email" do
          before { @state.whitelisted = true }

          it "does not suspend the ticket" do
            @mail.logger.expects(:info).with(regexp_matches(/^Amount of CCs/)).once
            @mail.logger.expects(:info).with(regexp_matches(/^Whitelisted/)).once
            execute(@state, @mail)
            assert_nil @state.suspension
          end
        end

        describe "and updating an existing ticket" do
          before { @state.closed_ticket = stub(new_record?: false) }

          it "does not suspend the ticket" do
            @mail.logger.expects(:info).with(regexp_matches(/^Amount of CCs/)).once
            @mail.logger.expects(:info).with(regexp_matches(/^Updating existing ticket/)).once
            execute(@state, @mail)
            assert_nil @state.suspension
          end
        end
      end

      describe 'when amount of email recipients is less than the account-configured threshold' do
        let(:ccs) { Array.new(ticket_email_ccs_suspension_threshold - 1) { |n| "support#{n}@example.com" } }

        it "does not suspend the ticket" do
          execute(@state, @mail)
          assert_nil @state.suspension
        end
      end

      describe 'when amount of email recipients is equal to the account-configured threshold' do
        let(:ccs) { Array.new(ticket_email_ccs_suspension_threshold) { |n| "support#{n}@example.com" } }

        it 'does not suspend the ticket' do
          execute(@state, @mail)
          assert_nil @state.suspension
        end
      end
    end

    describe 'when the ticket author is an agent' do
      before do
        @state.author = users(:minimum_agent)
      end

      describe 'when amount of email recipients is greater than the account-configured threshold' do
        let(:ccs) { Array.new(ticket_email_ccs_suspension_threshold + 1) { |n| "support#{n}@example.com" } }

        it "does not suspend the ticket" do
          execute(@state, @mail)
          assert_nil @state.suspension
        end
      end
    end
  end

  describe_with_arturo_disabled :email_ticket_email_ccs_suspension_threshold do
    let(:ccs) { Array.new(ticket_email_ccs_suspension_threshold + 1) { |n| "support#{n}@example.com" } }

    describe 'when amount of email recipients is greater than the account-configured threshold' do
      it "does not change the state of the ticket" do
        execute(@state, @mail)
        assert_nil @state.suspension
      end
    end
  end
end
