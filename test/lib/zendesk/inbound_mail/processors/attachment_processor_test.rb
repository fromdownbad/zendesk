require_relative "../../../../support/test_helper"
require_relative "../../../../support/mail_test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::InboundMail::Processors::AttachmentProcessor do
  include MailTestHelper

  fixtures :accounts, :tickets, :events

  let(:execute) { Zendesk::InboundMail::Processors::AttachmentProcessor.execute(@state, @mail) }
  let(:comment_attachment) { @state.ticket.comment.attachments.first }
  let(:account) { accounts(:minimum) }
  let(:light_agent) { users(:minimum_agent) }
  let(:html_reply) { nil }

  def self.it_should_have_comment_attachment
    it 'should have comment attachment' do
      execute
      assert_not_nil comment_attachment
    end
  end

  def self.it_should_not_have_any_attachments
    it 'should not have any attachments' do
      execute
      assert_nil comment_attachment
    end
  end

  before do
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = account
    @state.author  = User.new
    @state.author.stubs(:is_agent?).returns(true)
    @state.ticket = Ticket.new(account: account)
    @state.ticket.comment = Comment.new(account: account)
    @mail = Zendesk::Mail::InboundMessage.new

    @attachment = create_attachment
    @mail.parts = [@attachment]
  end

  it "skips if public attachments are disabled and author isn't an agent" do
    account.stubs(:is_attaching_enabled?).returns(false)
    @state.author.stubs(:is_agent?).returns(false)

    execute
    assert @state.ticket.comment.attachments.blank?
  end

  it "adds to suspended ticket if not suspended but using a suspended ticket" do
    @state.stubs(:untrusted?).returns(true)
    @state.stubs(:suspended?).returns(false)
    @state.ticket = SuspendedTicket.new
    execute
    ticket_attachment = @state.ticket.attachments.first

    assert_equal "test.txt", ticket_attachment.filename
    assert_equal accounts(:minimum), ticket_attachment.account
  end

  it "adds to a suspended ticket if suspended" do
    @state.stubs(:suspended?).returns(true)
    @state.ticket = SuspendedTicket.new
    execute
    ticket_attachment = @state.ticket.attachments.first

    assert_equal "test.txt", ticket_attachment.filename
    assert_equal accounts(:minimum), ticket_attachment.account
  end

  it "adds to ticket comment if not suspended" do
    execute
    assert_equal "test.txt", comment_attachment.filename
    assert_equal accounts(:minimum), comment_attachment.account
  end

  it "is added when no content type is specified" do
    @attachment.content_type = nil
    execute
    assert_equal 'text/plain', comment_attachment.content_type
  end

  describe "#resolve_cid_images" do
    let(:comment_html) { @state.ticket.comment.html_body(base_url: account.url).sub(%r{token/[^/]+/}, 'TOKEN') }
    let(:comment_html_body) { 'hello <img src="cid:123" alt="Inline image 1" width="67" height="64">' }

    before do
      @state.account.settings.stubs(rich_content_in_emails?: true)
      @attachment.headers['content-id'] = ['123']
      @state.ticket.comment.html_body = comment_html_body
    end

    it "resolves found images" do
      execute
      comment_html.must_equal "<div class=\"zd-comment\" dir=\"auto\">hello <img src=\"https://minimum.zendesk-test.com/attachments/TOKEN?name=test.txt\" alt=\"Inline image 1\" width=\"67\" height=\"64\"></div>"
    end

    describe_with_arturo_setting_enabled :no_mail_delimiter do
      let(:html_full) { nil }
      let(:email_json) do
        {
          processing_state: {
            content: {
              html_full: html_full || comment_html_body,
              html_quoted: comment_html_body,
              html_reply: html_reply || comment_html_body
            }
          }
        }
      end

      let(:comment_html) { @state.ticket.comment.html_body(base_url: account.url) }

      before { Zendesk::InboundMail::Processors::AttachmentProcessor.any_instance.stubs(html_from_parts: comment_html_body) }

      describe "for a non-suspended ticket" do
        let(:comment_html_body) { "hello <img src=\"#{account.url}/api/v2/mail_resources/123-4567/images/attachment_0_1.png\" alt=\"Inline image 1\" width=\"67\" height=\"64\">" }

        it "does not replace URLs" do
          execute
          comment_html.must_equal "<div class=\"zd-comment\" dir=\"auto\">hello <img src=\"https://minimum.zendesk-test.com/api/v2/mail_resources/123-4567/images/attachment_0_1.png\" alt=\"Inline image 1\" width=\"67\" height=\"64\"></div>"
        end
      end

      describe "for a suspended ticket" do
        let(:comment_html_body) { "hello <img src=\"cid:123\" alt=\"Inline image 1\" width=\"67\" height=\"64\">" }
        let(:html_reply) { "hello <img src=\"https://minimum.zendesk-test.com/attachments/token/?name=test.txt\" alt=\"Inline image 1\" width=\"67\" height=\"64\">" }

        before do
          @state.stubs(:suspended?).returns(true)
          @state.ticket = SuspendedTicket.new
          Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:json).returns(email_json.to_json)
        end

        describe "for an existing ticket" do
          before { @state.ticket.stubs(new_record?: false) }

          it "replaces the suspended ticket content" do
            execute
            @state.ticket.content.must_equal(html_reply)
          end
        end

        describe "for a new ticket" do
          before { @state.ticket.stubs(new_record?: true) }

          let(:html_full) { "full html <img src=\"https://minimum.zendesk-test.com/attachments/token/?name=test.txt\" alt=\"Inline image 1\" width=\"67\" height=\"64\">" }

          it "replaces the suspended ticket content" do
            execute
            @state.ticket.content.must_equal(html_full)
          end
        end
      end

      describe_with_arturo_enabled :email_enable_mtc_cid_resolution do
        let(:comment_html) { @state.ticket.comment.html_body(base_url: account.url) }
        let(:comment_html_body) { "hello <img src=\"cid:123\" alt=\"Inline image 1\" width=\"67\" height=\"64\">" }
        let(:html_reply) { "hello <img src=\"https://minimum.zendesk-test.com/attachments/token/?name=test.txt\" alt=\"Inline image 1\" width=\"67\" height=\"64\">" }
        let(:html_full) { "full html <img src=\"https://minimum.zendesk-test.com/attachments/token/?name=test.txt\" alt=\"Inline image 1\" width=\"67\" height=\"64\">" }

        before do
          Zendesk::InboundMail::Processors::AttachmentProcessor.any_instance.stubs(html_from_parts: comment_html_body)
          Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:json).returns(email_json.to_json)
        end

        describe "for an existing ticket" do
          before { @state.ticket.stubs(new_record?: false) }

          describe "when html_reply is present" do
            it "resolves found images with the correct src attribute from the html_reply" do
              Zendesk::InboundMail::Support.expects(:parse_json).once.returns(Zendesk::Mail::Serializers::InboundMessageSerializer.dump(@mail))
              execute
              comment_html.must_match /<div class="zd-comment" dir="auto">hello <img src="https:\/\/minimum.zendesk-test.com\/attachments\/token\/.*\/?name=test.txt" alt="Inline image 1" width="67" height="64"><\/div>/
            end
          end

          describe "when html_reply is absent" do
            let(:html_reply) { "" }

            it "resolves found images with the correct src attribute from the full_html" do
              Zendesk::InboundMail::Support.expects(:parse_json).once.returns(Zendesk::Mail::Serializers::InboundMessageSerializer.dump(@mail))
              execute
              comment_html.must_match /<div class="zd-comment" dir="auto">full html <img src="https:\/\/minimum.zendesk-test.com\/attachments\/token\/.*\/?name=test.txt" alt="Inline image 1" width="67" height="64"><\/div>/
            end
          end
        end

        describe "for a new ticket" do
          before { @state.ticket.stubs(new_record?: true) }

          it "resolves found images with the correct src attribute" do
            Zendesk::InboundMail::Support.expects(:parse_json).once.returns(Zendesk::Mail::Serializers::InboundMessageSerializer.dump(@mail))
            execute
            comment_html.must_match /<div class="zd-comment" dir="auto">full html <img src="https:\/\/minimum.zendesk-test.com\/attachments\/token\/.*\/?name=test.txt" alt="Inline image 1" width="67" height="64"><\/div>/
          end
        end
      end
    end

    describe_with_arturo_disabled :email_enable_mtc_cid_resolution do
      let(:comment_html) { @state.ticket.comment.html_body(base_url: account.url) }
      let(:comment_html_body) { "hello <img src=\"cid:123\" alt=\"Inline image 1\" width=\"67\" height=\"64\">" }

      before do
        Zendesk::InboundMail::Processors::AttachmentProcessor.any_instance.stubs(html_from_parts: comment_html_body)
      end

      it "resolves found images with the correct src attribute" do
        execute
        comment_html.must_match /<div class="zd-comment" dir="auto">hello <img src="https:\/\/minimum.zendesk-test.com\/attachments\/token\/.*\/?name=test.txt" alt="Inline image 1" width="67" height="64"><\/div>/
      end
    end

    describe_with_arturo_disabled :tiff_prevent_inline do
      it "marks the found images as inline" do
        execute
        @state.ticket.comment.attachments[0].inline.must_equal true
      end
    end

    describe_with_arturo_enabled :tiff_prevent_inline do
      it "marks the found non-TIFF images as inline" do
        execute
        @state.ticket.comment.attachments[0].inline.must_equal true
      end

      it "does not mark the found TIFF images as inline" do
        @attachment.headers['content-type'] = ['image/tiff']
        execute
        @state.ticket.comment.attachments[0].inline.must_equal false
      end
    end

    it "does not resolve unfound cid images" do
      @attachment.headers['content-id'] = ['124']
      execute
      comment_html.must_equal "<div class=\"zd-comment\" dir=\"auto\">hello </div>"
    end

    it "does not butcher html turning single quoted attributes into double quoted attributes" do
      @attachment.headers['content-id'] = ['124']
      @state.ticket.comment.html_body = 'hello <img src="cid:123" alt="Inline image 1" width="67" height="64"><span style=\'background: url(&quot;../images/a.png&quot;)\'>hello</span>'
      execute
      comment_html.must_equal "<div class=\"zd-comment\" dir=\"auto\">hello <span style=\"background: url(&quot;../images/a.png&quot;)\">hello</span></div>"
    end

    describe "when raw html body exceeds Nokogumbo::DEFAULT_MAX_TREE_DEPTH" do
      let(:comment_html_body) do
        nest_html_content(
          "hello <img src=\"cid:123\" alt=\"Inline image 1\" width=\"67\" height=\"64\">",
          Nokogumbo::DEFAULT_MAX_TREE_DEPTH + 1
        )
      end

      it "resolves found images" do
        nested_html_content = nest_html_content(
          "hello <img src=\"https://minimum.zendesk-test.com/attachments/TOKEN?name=test.txt\" alt=\"Inline image 1\" width=\"67\" height=\"64\">",
          Nokogumbo::DEFAULT_MAX_TREE_DEPTH + 1
        )
        expected = "<div class=\"zd-comment\" dir=\"auto\">#{nested_html_content}</div>"
        execute
        comment_html.must_equal expected
      end
    end

    describe "when a SystemStackError exception is raised" do
      before { Nokogiri::HTML5.stubs(:fragment).raises(SystemStackError).once }

      it "does not resolve cid images" do
        execute
        comment_html.must_equal "<div class=\"zd-comment\" dir=\"auto\">hello </div>"
      end
    end
  end

  describe "with account that can send real attachments" do
    before do
      @state.account.stubs(send_real_attachments?: true)
    end

    describe "with content type as TIFF and inline as true in the headers" do
      before do
        @state.account.settings.stubs(rich_content_in_emails?: true)
        @attachment = create_attachment("file_name" => "image.tiff", "content_type" => "image/tiff", "inline" => true)
        @mail.parts = [@attachment]
        @state.ticket.comment.format = "rich"
      end

      describe_with_arturo_enabled :tiff_prevent_inline do
        it "always sets attachment's inline attribute to false" do
          execute
          refute @state.ticket.comment.attachments[0].inline
        end
      end

      describe_with_arturo_disabled :tiff_prevent_inline do
        it "does not set attachment's inline attribute to false" do
          execute
          assert @state.ticket.comment.attachments[0].inline
        end
      end
    end

    describe "#ignore_quoted_attachments" do
      before do
        @state.account.settings.stubs(rich_content_in_emails?: true)

        @current_comment_attachment = create_attachment("file_name" => "current_comment_attachment.jpg", "content_id" => "123", "inline" => true)
        @previous_comment_attachment = create_attachment("file_name" => "previous_attachment.jpg", "content_id" => "456", "inline" => true)
        @attachment = create_attachment

        @mail.parts = [@current_comment_attachment, @previous_comment_attachment, @attachment, @attachment]
      end

      describe "with plain text comment" do
        before do
          @state.ticket.comment.html_body = ""

          assert_equal 4, @mail.parts.count
        end

        it 'includes all attachments' do
          execute
          ticket_attachments = @state.ticket.comment.attachments

          assert_equal 4, ticket_attachments.size
        end

        it 'ensures all attachments are not inline' do
          execute
          ticket_attachments = @state.ticket.comment.attachments

          assert ticket_attachments.none?(&:inline)
        end
      end

      describe "with rich text comment" do
        let(:comment_html_body) { 'hello <img src="cid:123" alt="Inline image 1" width="67" height="64">' }

        before do
          @state.ticket.comment.html_body = comment_html_body
          @state.ticket.comment.format = "rich"

          assert_equal 4, @mail.parts.count
        end

        it 'removes inline attachments not in the comment' do
          execute
          ticket_attachments = @state.ticket.comment.attachments

          assert_equal 3, ticket_attachments.size
          assert ticket_attachments.none? { |attachment| attachment.display_filename == 'previous_attachment.jpg' }
        end

        it 'processes all appended attachments' do
          execute
          ticket_attachments = @state.ticket.comment.attachments

          assert_equal 'test.txt', ticket_attachments[1].display_filename
          assert_equal 'test.txt', ticket_attachments[2].display_filename
        end

        it 'respects inline header attributes' do
          execute

          mail_parts_inline = @mail.parts.map do |part|
            [part.headers['file-name'].first, !!part.headers['inline'].try(:first)]
          end.to_h

          @state.ticket.comment.attachments.each do |attachment|
            assert attachment.inline == mail_parts_inline[attachment.display_filename]
          end
        end

        describe "when html body exceeds Nokogumbo::DEFAULT_MAX_TREE_DEPTH" do
          let(:comment_html_body) do
            nest_html_content(
              "hello <img src=\"cid:123\" alt=\"Inline image 1\" width=\"67\" height=\"64\">",
              Nokogumbo::DEFAULT_MAX_TREE_DEPTH + 1
            )
          end

          it 'removes inline attachments not in the comment' do
            execute
            ticket_attachments = @state.ticket.comment.attachments

            assert_equal 3, ticket_attachments.size
            assert ticket_attachments.none? { |attachment| attachment.display_filename == 'previous_attachment.jpg' }
          end
        end

        describe "when a SystemStackError exception is raised" do
          before { Nokogiri::HTML5.stubs(:fragment).raises(SystemStackError).twice }

          it 'does not remove inline attachments not in the comment' do
            execute
            ticket_attachments = @state.ticket.comment.attachments

            assert_equal 4, ticket_attachments.size
            assert ticket_attachments.any? { |attachment| attachment.display_filename == 'previous_attachment.jpg' }
          end
        end
      end
    end
  end

  describe "problems down below in Attachment" do
    before { @mail.parts = [@attachment, @attachment, @attachment] }
    describe "with attachments that fail validation" do
      before do
        Attachment.any_instance.stubs(:valid?).returns(true).then.returns(false).then.returns(false)
      end

      it "ends up with one decent attachment" do
        execute
        assert_equal 1, @state.ticket.comment.attachments.size
      end

      it "adds translatable error" do
        @state.ticket = tickets(:minimum_1)
        @state.ticket.comment = Comment.new(ticket: @state.ticket)
        @state.ticket.will_be_saved_by @state.ticket.submitter
        Attachment.any_instance.stubs(:valid_size?).returns(false)
        Attachment.any_instance.stubs(:filename).returns("foo.txt")

        execute
        assert @state.ticket.audit.events.first.is_a?(TranslatableError)
        assert_equal I18n.t("ticket.comment.flagged_reason_attachment_too_big", file: "foo.txt", account_limit: @state.ticket.account.max_attachment_megabytes.to_s), @state.ticket.audit.events.first.to_s
        assert_equal @state.flags, AuditFlags.new([EventFlagType.ATTACHMENT_TOO_BIG])
        assert_equal @state.flags_options, EventFlagType.ATTACHMENT_TOO_BIG => { message: { file: "foo.txt", account_limit: account.max_attachment_megabytes.to_s } }
      end
    end

    describe "with attachments that crash in :valid?" do
      before do
        Attachment.any_instance.stubs(:valid?).returns(true).then.raises(ArgumentError).then.returns(true)
      end

      it "removes the crashy ones" do
        execute
        assert_equal 2, @state.ticket.comment.attachments.size
      end
    end
  end

  describe "S3 attachments" do
    before do
      # attachments.yml has :fs as default in test
      Attachment.stubs(default_s3_stores: [:s3])

      @attachment = create_attachment("file_name" => "test.txt", "remote_url" => "https://bucket.s3-us-west-2.amazonaws.com/test.txt", "content_type" => "text/plain")

      @mail.parts = [@attachment]

      stub_request(:head, %r{\.amazonaws\.com/test\.txt}).to_return(headers: { content_length: "4", content_type: "text/plain" })
    end

    it "is copied from S3" do
      execute
      assert_equal "test.txt", comment_attachment.filename
      assert_equal accounts(:minimum), comment_attachment.account
      assert comment_attachment.s3_copy_source
      assert_equal "bucket", comment_attachment.s3_copy_source[:bucket]
      assert_equal "test.txt", comment_attachment.s3_copy_source[:key]
      assert_equal 4, comment_attachment.size
      assert_equal "text/plain", comment_attachment.content_type
      assert_equal comment_attachment.stores, [:s3]
    end
  end

  describe 'when agent spoofing is detected' do
    let(:comment_html) { @state.ticket.comment.html_body(base_url: account.url).sub(%r{token/[^/]+/}, 'TOKEN') }
    let(:comment_html_body) { 'hello <img src="cid:123" alt="Inline image 1" width="67" height="64">' }
    let(:statsd_client) { Zendesk::InboundMail::StatsD.client }

    before do
      @state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF)
    end

    describe 'when the public attachments are disabled' do
      before do
        account.stubs(:is_attaching_enabled?).returns(false)
      end

      it 'increments statsd' do
        statsd_client.expects(:increment).with('reply_to_processor.attachment_processor_skipped').once
        execute
      end

      it_should_not_have_any_attachments

      describe 'email comment contains an inline attachment' do
        before(:all) do
          Zendesk::InboundMail::Processors::AttachmentProcessor.any_instance.stubs(html_from_parts: comment_html_body)
          @state.ticket.comment.html_body = comment_html_body
          @state.ticket.comment.author = users(:minimum_agent)
          @state.account.settings.stubs(rich_content_in_emails?: true)
          @attachment.headers['content-id'] = ['123']
        end

        describe "for an existing ticket" do
          before { @state.ticket.stubs(new_record?: false) }

          it 'removes the broken image content' do
            execute
            comment_html.must_equal "<div class=\"zd-comment\" dir=\"auto\">hello</div>"
          end
        end

        describe "for a new ticket" do
          before { @state.ticket.stubs(new_record?: true) }

          it 'removes the broken image content' do
            execute
            comment_html.must_equal "<div class=\"zd-comment\" dir=\"auto\">hello</div>"
          end
        end
      end
    end

    describe 'when the public attachments are enabled' do
      before do
        account.stubs(:is_attaching_enabled?).returns(true)
      end
      it_should_have_comment_attachment

      describe 'email comment contains an inline attachment' do
        before(:all) do
          @attachment.headers['content-id'] = ['123']
          @state.account.settings.stubs(rich_content_in_emails?: true)
          @state.ticket.comment.author = users(:minimum_agent)
          @state.ticket.comment.html_body = comment_html_body
        end

        describe "for an existing ticket" do
          before { @state.ticket.stubs(new_record?: false) }

          it "resolves found images" do
            execute
            comment_html.must_equal "<div class=\"zd-comment\" dir=\"auto\">hello <img src=\"https://minimum.zendesk-test.com/attachments/TOKEN?name=test.txt\" alt=\"Inline image 1\" width=\"67\" height=\"64\"></div>"
          end
        end

        describe "for a new ticket" do
          before { @state.ticket.stubs(new_record?: true) }

          it "resolves found images" do
            execute
            comment_html.must_equal "<div class=\"zd-comment\" dir=\"auto\">hello <img src=\"https://minimum.zendesk-test.com/attachments/TOKEN?name=test.txt\" alt=\"Inline image 1\" width=\"67\" height=\"64\"></div>"
          end
        end
      end
    end

    describe 'when the original sender is a light agent' do
      before do
        @state.original_author = light_agent
        light_agent.stubs(:is_light_agent?).returns(true)
      end

      it_should_have_comment_attachment
    end
  end

  describe_with_arturo_disabled :email_reply_to_vulnerability_attachment_processor do
    it_should_have_comment_attachment

    describe 'email comment contains an inline attachment' do
      let(:comment_html) { @state.ticket.comment.html_body(base_url: account.url).sub(%r{token/[^/]+/}, 'TOKEN') }
      let(:comment_html_body) { 'hello <img src="cid:123" alt="Inline image 1" width="67" height="64">' }

      before(:all) do
        @attachment.headers['content-id'] = ['123']
        @state.account.settings.stubs(rich_content_in_emails?: true)
        @state.ticket.comment.author = users(:minimum_agent)
        @state.ticket.comment.html_body = comment_html_body
      end

      describe "for an existing ticket" do
        before { @state.ticket.stubs(new_record?: false) }

        it "resolves found images" do
          execute
          comment_html.must_equal "<div class=\"zd-comment\" dir=\"auto\">hello <img src=\"https://minimum.zendesk-test.com/attachments/TOKEN?name=test.txt\" alt=\"Inline image 1\" width=\"67\" height=\"64\"></div>"
        end
      end

      describe "for a new ticket" do
        before { @state.ticket.stubs(new_record?: true) }

        it "resolves found images" do
          execute
          comment_html.must_equal "<div class=\"zd-comment\" dir=\"auto\">hello <img src=\"https://minimum.zendesk-test.com/attachments/TOKEN?name=test.txt\" alt=\"Inline image 1\" width=\"67\" height=\"64\"></div>"
        end
      end
    end
  end
end
