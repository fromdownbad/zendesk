require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::InboundMail::Processors::StatsProcessor do
  include MailTestHelper

  fixtures :events, :tickets, :suspended_tickets

  def assert_timing_stats_when_from_gmail(state, mail, expected_tags, event_slo: true)
    mail.headers[Zendesk::InboundMail::Processors::StatsProcessor::X_ZENDESK_SOURCE] = '1'
    mail.headers["received"] = mail_fetcher_received_headers(6.minutes)

    statsd_client.expects(:histogram).
      with(:total_inbound_time, expected_elapsed_time(6.minutes), tags: expected_tags + ["source:gmail"]).once

    if event_slo
      statsd_client.expects(:increment).
        with(:total_inbound_email_processed, anything).once
    else
      statsd_client.expects(:increment).
        with(:total_inbound_email_processed, anything).never
    end

    execute(state, mail)
  end

  def assert_timing_stats_when_from_postfix(state, mail, expected_tags, event_slo: true)
    mail.headers["received"] = postfix_received_headers(5.minutes, 3.minutes, 6.minutes)

    statsd_client.expects(:histogram).
      with(:total_inbound_time, expected_elapsed_time(6.minutes), tags: expected_tags + ["source:postfix"]).once

    if event_slo
      statsd_client.expects(:increment).
        with(:total_inbound_email_processed, anything).once
    else
      statsd_client.expects(:increment).
        with(:total_inbound_email_processed, anything).never
    end

    execute(state, mail)
  end

  def assert_timing_stats_when_from_postfix_with_gmail_headers(state, mail, expected_tags, event_slo: true)
    mail.headers['X-Google-Smtp-Source'] = "foo"
    mail.headers["received"] = postfix_received_headers(5.minutes, 3.minutes, 6.minutes) +
      gmail_received_headers(7.minutes, 1.minutes)

    statsd_client.expects(:histogram).
      with(:total_inbound_time, expected_elapsed_time(6.minutes), tags: expected_tags + ["source:postfix"]).once

    if event_slo
      statsd_client.expects(:increment).
        with(:total_inbound_email_processed, anything).once
    else
      statsd_client.expects(:increment).
        with(:total_inbound_email_processed, anything).never
    end

    execute(state, mail)
  end

  def assert_timing_failure_stat_when_header_timing_missing(state, mail, expected_tags)
    statsd_client.expects(:increment).with("total_inbound_time.failed", tags: expected_tags)

    execute(state, mail)
  end

  def execute(state, mail)
    Zendesk::InboundMail::Processors::StatsProcessor.execute(state, mail)
  end

  # Mimic how headers are read as strings and converted to DateTime objects in StatsProcessor
  def expected_elapsed_time(minutes)
    Time.now - (Time.now - minutes).to_s.to_datetime
  end

  def mail_fetcher_received_headers(*minutes_ago)
    minutes_ago.map do |minutes|
      "by work19.pod14.use1.zdsys.com (Gmail Connector); #{(Time.now - minutes).rfc2822}"
    end
  end

  def gmail_received_headers(*minutes_ago)
    minutes_ago.map do |minutes|
      "by 2002:a50:d002:0:0:0:0:0 with SMTP id j2-v6csp146561edf;\n\t#{(Time.now - minutes).rfc2822}"
    end
  end

  def postfix_received_headers(*minutes_ago)
    minutes_ago.map do |minutes|
      "from in1.pod19.use1.zdsys.com (unknown [10.219.24.191])\n" \
        "\t(using TLSv1.2 with cipher ECDHE-RSA-AES256-GCM-SHA384 (256/256 bits))\n" \
        "\t(No client certificate requested)\n" \
        "\tby work2.pod19.use1.zdsys.com (Postfix) with ESMTPS id 1C875400C9ABE\n" \
        "\tfor <support+id145@warpdrivez3n.zendesk.com>;\n" \
        "\t#{(Time.now - minutes).rfc2822}" \
    end
  end

  let(:mail) { Zendesk::Mail::InboundMessage.new }
  let(:state) { Zendesk::InboundMail::ProcessingState.new }
  let(:ticket) { tickets(:minimum_1) }
  let(:account) { accounts(:minimum) }
  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }

  let(:ticket_threading_key)  { "ticket.threading" }
  let(:ticket_threading_tags) { { tags: %w[method:encoded_id location:message_ids] } }

  before do
    state.account = account
    state.ticket = ticket
    state.record_deferred_stat(ticket_threading_key, ticket_threading_tags)
    Timecop.freeze
  end

  describe "with a suspended ticket" do
    let(:ticket) { suspended_tickets(:minimum_unverified_by_unknown_author) }
    let(:expected_timing_tags) { ["type:suspension"] }

    before do
      state.stubs(suspended_ticket?: true)
      state.ticket.stubs(cause: SuspensionType.UNKNOWN_AUTHOR)

      statsd_client.expects(:increment).with(ticket_threading_key, ticket_threading_tags).once
      statsd_client.expects(:increment).with("ticket.suspended", tags: ["type:#{SuspensionType[SuspensionType.UNKNOWN_AUTHOR]}"]).once
      statsd_client.expects(:increment).with("ticket.commit", tags: ["type:suspended"]).once
    end

    it { assert_timing_stats_when_from_postfix(state, mail, expected_timing_tags) }

    it { assert_timing_stats_when_from_gmail(state, mail, expected_timing_tags) }

    it { assert_timing_failure_stat_when_header_timing_missing(state, mail, expected_timing_tags) }

    it { assert_timing_stats_when_from_postfix_with_gmail_headers(state, mail, expected_timing_tags) }

    describe_with_arturo_disabled :email_inbound_pipeline_record_event_based_slo do
      it { assert_timing_stats_when_from_postfix(state, mail, expected_timing_tags, event_slo: false) }

      it { assert_timing_stats_when_from_gmail(state, mail, expected_timing_tags, event_slo: false) }

      it { assert_timing_failure_stat_when_header_timing_missing(state, mail, expected_timing_tags) }

      it { assert_timing_stats_when_from_postfix_with_gmail_headers(state, mail, expected_timing_tags, event_slo: false) }
    end
  end

  describe "with a new ticket" do
    let(:expected_timing_tags) { ["type:ticket_or_comment"] }

    before do
      state.stubs(suspended_ticket?: false, followup_ticket?: false, new_ticket?: true)

      statsd_client.expects(:increment).with(ticket_threading_key, ticket_threading_tags).once
      statsd_client.expects(:increment).with("ticket.commit", tags: ["type:new"]).once
    end

    it "increments multiple deferred stats" do
      state.record_deferred_stat("foo", tags: ["bar"])
      state.record_deferred_stat("baz", tags: ["biz"])
      statsd_client.expects(:increment).with("foo", tags: ["bar"]).once
      statsd_client.expects(:increment).with("baz", tags: ["biz"]).once

      assert_timing_stats_when_from_postfix(state, mail, expected_timing_tags)
    end

    it { assert_timing_stats_when_from_postfix(state, mail, expected_timing_tags) }

    it { assert_timing_stats_when_from_gmail(state, mail, expected_timing_tags) }

    it { assert_timing_failure_stat_when_header_timing_missing(state, mail, expected_timing_tags) }

    it { assert_timing_stats_when_from_postfix_with_gmail_headers(state, mail, expected_timing_tags) }

    describe_with_arturo_disabled :email_inbound_pipeline_record_event_based_slo do
      it { assert_timing_stats_when_from_postfix(state, mail, expected_timing_tags, event_slo: false) }

      it { assert_timing_stats_when_from_gmail(state, mail, expected_timing_tags, event_slo: false) }

      it { assert_timing_failure_stat_when_header_timing_missing(state, mail, expected_timing_tags) }

      it { assert_timing_stats_when_from_postfix_with_gmail_headers(state, mail, expected_timing_tags, event_slo: false) }
    end
  end

  describe "with an existing ticket" do
    let(:expected_timing_tags) { ["type:ticket_or_comment"] }

    before do
      statsd_client.expects(:increment).with(ticket_threading_key, ticket_threading_tags).once
      statsd_client.expects(:increment).with("ticket.commit", tags: ["type:comment"]).once
    end

    it { assert_timing_stats_when_from_postfix(state, mail, expected_timing_tags) }

    it { assert_timing_stats_when_from_gmail(state, mail, expected_timing_tags) }

    it { assert_timing_failure_stat_when_header_timing_missing(state, mail, expected_timing_tags) }

    it { assert_timing_stats_when_from_postfix_with_gmail_headers(state, mail, expected_timing_tags) }

    describe_with_arturo_disabled :email_inbound_pipeline_record_event_based_slo do
      it { assert_timing_stats_when_from_postfix(state, mail, expected_timing_tags, event_slo: false) }

      it { assert_timing_stats_when_from_gmail(state, mail, expected_timing_tags, event_slo: false) }

      it { assert_timing_failure_stat_when_header_timing_missing(state, mail, expected_timing_tags) }

      it { assert_timing_stats_when_from_postfix_with_gmail_headers(state, mail, expected_timing_tags, event_slo: false) }
    end
  end
  describe "with a closed ticket (creating a followup)" do
    let(:expected_timing_tags) { ["type:ticket_or_comment"] }

    before do
      state.stubs(followp_ticket?: true)
      state.ticket.via_id = ViaType.CLOSED_TICKET

      statsd_client.expects(:increment).with(ticket_threading_key, ticket_threading_tags).once
      statsd_client.expects(:increment).with("ticket.commit", tags: %w[type:followup type:comment]).once
    end

    it { assert_timing_stats_when_from_postfix(state, mail, expected_timing_tags) }

    it { assert_timing_stats_when_from_gmail(state, mail, expected_timing_tags) }

    it { assert_timing_failure_stat_when_header_timing_missing(state, mail, expected_timing_tags) }

    it { assert_timing_stats_when_from_postfix_with_gmail_headers(state, mail, expected_timing_tags) }

    describe_with_arturo_disabled :email_inbound_pipeline_record_event_based_slo do
      it { assert_timing_stats_when_from_postfix(state, mail, expected_timing_tags, event_slo: false) }

      it { assert_timing_stats_when_from_gmail(state, mail, expected_timing_tags, event_slo: false) }

      it { assert_timing_failure_stat_when_header_timing_missing(state, mail, expected_timing_tags) }

      it { assert_timing_stats_when_from_postfix_with_gmail_headers(state, mail, expected_timing_tags, event_slo: false) }
    end
  end

  describe "logging information about an email from an agent sender with non-matching From and Reply-To header email addresses" do
    let(:account) { accounts(:minimum) }
    let(:agent_email) { address(state.account.owner.email) }
    let(:end_user_email) { address(users(:minimum_end_user).email) }
    let(:other_end_user_email) { address("other.end.user@email.com") }
    let(:mail) { FixtureHelper::Mail.read('minimum_ticket.json') }
    let(:statsd_client) { Zendesk::InboundMail::StatsD.client }
    let(:logger) { stub("logger", info: true, warn: true) }

    before do
      statsd_client.expects(:increment).with('ticket.threading', anything).once
      statsd_client.expects(:increment).with('ticket.commit', anything).once
      statsd_client.expects(:increment).with('total_inbound_time.failed', anything).once
    end

    describe_with_arturo_enabled :email_log_agent_sender_with_nonmatching_from_and_reply_to do
      describe "when state.author is an agent" do
        before do
          mail.stubs(logger: logger)
          mail.headers['reply-to'] = [agent_email]
          state.author = state.account.owner
        end

        describe "when the addresses in the From and Reply-To headers match" do
          before { mail.headers['from'] = [agent_email] }

          it "does not record a metric" do
            statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to').never
            execute(state, mail)
          end
        end

        describe "when the addresses in the From and Reply-To headers do not match" do
          before do
            mail.headers['from'] = [end_user_email]
          end

          describe "on a new ticket" do
            let(:new_ticket) { true }

            before do
              state.ticket = Ticket.new
              state.ticket.audit = state.ticket.audits.build(via_id: ViaType.Mail, author: state.author)
            end

            it "logs and records a metric" do
              logger.expects(:info).with("Author detected as agent, and From and Reply-To headers have different address values. "\
                                    "New Ticket: #{new_ticket}, From Header Address: #{end_user_email.address}, From Address User role: End user, Reply-To Header Address: #{agent_email.address}, "\
                                    "Suspended: false, SuspensionType: N/A, Anonymous users can submit support requests: true, "\
                                    "List of agent privilege actions performed in the email - Add/Modify CCs: false when only agents can add CCs setting is OFF, "\
                                    "Add Attachments: false when end users are not allowed to attach files setting is ON, Mail API: false, Email Forwarding: false")
              statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to', tags: ["subdomain:#{account.subdomain}", "new_ticket:#{new_ticket}", "from_user_role:End user"]).once
              execute(state, mail)
            end
          end

          describe "on a ticket update" do
            let(:new_ticket) { false }
            let(:support_address) { "foo@minimum.#{Zendesk::Configuration.fetch(:host)}" }

            before do
              mail.headers['from'] = [Zendesk::Mail::Address.new(address: support_address)]
              account.stubs(:is_open?).returns(false)
            end

            it "logs and records a metric" do
              logger.expects(:info).with("Author detected as agent, and From and Reply-To headers have different address values. "\
                                    "New Ticket: #{new_ticket}, From Header Address: #{support_address}, From Address User role: Support address, Reply-To Header Address: #{agent_email.address}, "\
                                    "Suspended: false, SuspensionType: N/A, Anonymous users can submit support requests: false, "\
                                    "List of agent privilege actions performed in the email - Add/Modify CCs: false when only agents can add CCs setting is OFF, "\
                                    "Add Attachments: false when end users are not allowed to attach files setting is ON, Mail API: false, Email Forwarding: false")
              statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to', tags: ["subdomain:#{account.subdomain}", "new_ticket:#{new_ticket}", "from_user_role:Support address"]).once
              execute(state, mail)
            end
          end

          describe "on an email forwarded by the agent on behalf of an end user" do
            before do
              state.agent_forwarded = true
              statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to', anything).once
            end

            it "logs message that agent has forwarded the email to support" do
              logger.expects(:info).with(regexp_matches(/Email Forwarding: true/))
              execute(state, mail)
            end
          end

          describe "on an email containing text api" do
            before do
              state.email_containing_text_api = true
              statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to', anything).once
            end

            it "logs message that mail api commands are present in the email" do
              logger.expects(:info).with(regexp_matches(/Mail API: true/))
              execute(state, mail)
            end
          end

          describe "on an email containing attachments" do
            before do
              mail.stubs(attachments: ['a'])
              statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to', anything).once
            end

            describe "with customers can attach files setting is enabled" do
              before { account.stubs(:is_attaching_enabled?).returns(false) }

              it "logs message that attachment is added" do
                logger.expects(:info).with(regexp_matches(/Add Attachments: true when end users are not allowed to attach files setting is OFF/))
                execute(state, mail)
              end
            end

            describe "with customers can attach files setting is disabled" do
              before { account.stubs(:is_attaching_enabled?).returns(true) }

              it "logs message that attachment is not added" do
                logger.expects(:info).with(regexp_matches(/Add Attachments: true when end users are not allowed to attach files setting is ON/))
                execute(state, mail)
              end
            end
          end

          describe "on a email containing ccs" do
            before do
              statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to', anything).once
            end

            describe "with customers can attach files setting is enabled" do
              before { account.stubs(:is_collaborators_addable_only_by_agents?).returns(true) }

              it "logs message that ccs are added" do
                logger.expects(:info).with(regexp_matches(/Add\/Modify CCs: false when only agents can add CCs setting is ON/))
                execute(state, mail)
              end
            end

            describe "with customers can attach files setting is disabled" do
              before { account.stubs(:is_collaborators_addable_only_by_agents?).returns(false) }

              it "logs message that ccs are not added" do
                logger.expects(:info).with(regexp_matches(/Add\/Modify CCs: false when only agents can add CCs setting is OFF/))
                execute(state, mail)
              end
            end
          end
        end
      end

      describe "when state.author is not an agent" do
        before do
          state.from = end_user_email
          mail.headers['reply-to'] = [end_user_email]
        end

        describe "when the addresses in the From and Reply-To headers match" do
          before { mail.headers['from'] = [end_user_email] }

          it "does not record a metric" do
            statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to').never
            execute(state, mail)
          end
        end

        describe "when the addresses in the From and Reply-To headers do not match" do
          before { mail.headers['from'] = [other_end_user_email] }

          it "does not record a metric" do
            statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to').never
            execute(state, mail)
          end
        end
      end
    end

    describe_with_arturo_disabled :email_log_agent_sender_with_nonmatching_from_and_reply_to do
      describe "when state.author is an agent" do
        before do
          state.from = agent_email
          mail.headers['reply-to'] = [agent_email]
        end

        describe "when the addresses in the From and Reply-To headers match" do
          before { mail.headers['from'] = [agent_email] }

          it "does not record a metric" do
            statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to').never
            execute(state, mail)
          end
        end

        describe "when the addresses in the From and Reply-To headers do not match" do
          before { mail.headers['from'] = [end_user_email] }

          it "does not record a metric" do
            statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to').never
            execute(state, mail)
          end
        end
      end

      describe "when state.author is not an agent" do
        before do
          state.from = end_user_email
          mail.headers['reply-to'] = [end_user_email]
        end

        describe "when the addresses in the From and Reply-To headers match" do
          before { mail.headers['from'] = [end_user_email] }

          it "does not record a metric" do
            statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to').never
            execute(state, mail)
          end
        end

        describe "when the addresses in the From and Reply-To headers do not match" do
          before { mail.headers['from'] = [other_end_user_email] }

          it "does not record a metric" do
            statsd_client.expects(:increment).with('user_processor.agent_sender_with_nonmatching_from_and_reply_to').never
            execute(state, mail)
          end
        end
      end
    end
  end
end
