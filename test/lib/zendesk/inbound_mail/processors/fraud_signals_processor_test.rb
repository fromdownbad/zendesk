require_relative "../../../../support/test_helper"

ENV['RSPAMD_API_ENDPOINT'] = 'http://127.0.0.1:9001'
ENV['RSPAMD_PASSWORD'] = 'GoodPassword'

SingleCov.covered!

describe Zendesk::InboundMail::Processors::FraudSignalsProcessor do
  include ArturoTestHelper
  include MailTestHelper

  def execute(state, mail)
    Zendesk::InboundMail::Processors::FraudSignalsProcessor.execute(state, mail)
  end

  let (:account) { accounts(:minimum) }

  let(:mail)  { Zendesk::Mail::InboundMessage.new }
  let(:state) { Zendesk::InboundMail::ProcessingState.new }
  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }
  let(:low_rspamd_score) { Zendesk::InboundMail::Processors::FraudSignalsProcessor::HIGH_RSPAMD_SCORE_THRESHOLD - 1.0 }
  let(:high_rspamd_score) { Zendesk::InboundMail::Processors::FraudSignalsProcessor::HIGH_RSPAMD_SCORE_THRESHOLD + 1.0 }
  let(:raw_email_identifier) { "1/abc123def456.eml" }
  let(:active_root_span) { stub(:root_span) }
  let(:tag_args) { ['zendesk.mail_ticket_creator.fraud_signals_processor.suspend', account.subdomain] }

  before do
    state.account = account
    state.recipient = 'support@minimum.zendesk-test.com'
    state.from = address("some.user@example.com")

    mail.eml_url = "memory://inbound_email_store/" + raw_email_identifier
    store_mail_in_remote_files(mail)

    Datadog.tracer.stubs(:active_root_span).returns(active_root_span)
    active_root_span.stubs(:set_tag)

    statsd_client.stubs(:increment)

    # https://github.com/zendesk/zendesk_jobs/blob/v2.1.0/lib/zendesk_jobs/common/datadog_trace_search.rb
    # RspamdFeedbackJob inherits from the BaseJob which invokes this method, resulting in a call to set_tag,
    # which causes "unexpected invocation: set_tag(zendesk.account_id, -1)"
    RspamdFeedbackJob.stubs(:around_perform_00000000_add_datadog_facets)
  end

  def assert_trains_rspamd
    RspamdFeedbackJob.expects(:report_spam).with(
      user: User.system,
      identifier: raw_email_identifier,
      recipient: state.recipient,
      classifier: Zendesk::InboundMail::RspamdTrain::RSPAMD_BAYES_CLASSIFIER_COMMON
    ).once

    execute(state, mail)
  end

  def refute_trains_rspamd
    RspamdFeedbackJob.expects(:report_spam).never

    execute(state, mail)
  end

  def assert_sets_datadog_apm_span_tags
    active_root_span.expects(:set_tag).with(*tag_args).once
    execute(state, mail)
  end

  def refute_sets_datadog_apm_span_tags
    active_root_span.expects(:set_tag).with(*tag_args).never
    execute(state, mail)
  end

  describe "when the state is already suspended for ATSD detected spam" do
    before { state.suspension = SuspensionType.MALICIOUS_PATTERN_DETECTED }

    it "does not check any of the remaining rules" do
      Zendesk::InboundMail::Processors::FraudSignalsProcessor::RULES.each do |rule|
        Zendesk::InboundMail::Processors::FraudSignalsProcessor.any_instance.expects(rule).never
      end

      execute(state, mail)
    end
  end

  describe "when the state is already suspended for SIGNUP_REQUIRED" do
    before { state.suspension = SuspensionType.SIGNUP_REQUIRED }

    describe_with_arturo_disabled :email_fraud_signals_processor_scan_signup_suspended_emails do
      it "does not check any of the remaining rules" do
        Zendesk::InboundMail::Processors::FraudSignalsProcessor::RULES.each do |rule|
          Zendesk::InboundMail::Processors::FraudSignalsProcessor.any_instance.expects(rule).never
        end

        execute(state, mail)
      end
    end

    describe_with_arturo_enabled :email_fraud_signals_processor_scan_signup_suspended_emails do
      it "checks the rules" do
        Zendesk::InboundMail::Processors::FraudSignalsProcessor::RULES.each do |rule|
          Zendesk::InboundMail::Processors::FraudSignalsProcessor.any_instance.expects(rule).once
        end

        execute(state, mail)
      end
    end
  end

  describe "when setting datadog apm tracing tags" do
    describe "when the content is suspended" do
      before do
        Arturo::Feature.
          stubs(:find_feature).
          with(:orca_fraud_signals_processor_received_header_patterns).
          returns(
            Arturo::Feature.new(
              symbol: "orca_fraud_signals_processor_received_header_patterns",
              external_beta_subdomains: ["some-bad-domain\\.com"]
            )
          )
        mail.headers["received"] = [
          "from 127.0.0.1 to some-internal-something.localhost",
          "by spammer@some-bad-domain.com to poor@innocent.victim",
          "to something.org from someone.net"
        ]
        Arturo::Feature.stubs(:find_feature).with(:email_fraud_signals_processor_received_headers).returns(true)
      end

      describe_with_arturo_enabled :orca_classic_fraud_signals_span do
        it { assert_sets_datadog_apm_span_tags }
      end
      describe_with_arturo_disabled :orca_classic_fraud_signals_span do
        it { refute_sets_datadog_apm_span_tags }
      end
    end

    describe "when the content is not suspended" do
      describe_with_and_without_arturo_enabled :orca_classic_fraud_signals_span do
        it { refute_sets_datadog_apm_span_tags }
      end
    end
  end

  describe "blocking based on high Rspamd score and presence of 'from localhost (unknown' in Received headers" do
    let(:high_rspamd_score_headers) do
      ["Yes, score=#{high_rspamd_score}; RCVD_VIA_SMTP_AUTH(8.00)[]; HAS_REPLYTO(8.14)[info@fake.test]"]
    end
    let(:low_rspamd_score_headers) do
      ["Yes, score=#{low_rspamd_score}; RCVD_VIA_SMTP_AUTH(2.00)[]; HAS_REPLYTO(5.14)[info@fake.test]"]
    end
    let(:received_headers_localhost_unknown_in_bottom_two) do
      [
        "from me to you",
        "from localhost (unknown [123.456.78.90]) to someone else",
        "from some server to another server"
      ]
    end
    let(:received_headers_localhost_unknown_not_in_bottom_two) do
      [
        "from localhost (unknown [123.456.78.90]) to someone else",
        "from some server to another server",
        "from me to you"
      ]
    end

    describe_with_arturo_enabled :email_fraud_signals_processor_high_rspamd_localhost_unknown do
      describe "when Rspamd score is present and high" do
        before { mail.headers["X-Rspamd-Status"] = high_rspamd_score_headers }

        describe "when 'from localhost (unknown' is in one of the bottom two Received headers" do
          before { mail.headers["Received"] = received_headers_localhost_unknown_in_bottom_two }

          it "suspends and increments a DataDog metric" do
            statsd_client.
              expects(:increment).
              with(
                :fraud_signals_processor_suspend,
                tags: ["reason:high_rspamd_locahost_unknown"]
              ).
              once

            execute(state, mail)

            assert_equal SuspensionType.MALICIOUS_PATTERN_DETECTED, state.suspension
          end

          describe_with_arturo_enabled :email_train_rspamd_in_mtc do
            it { assert_trains_rspamd }
          end

          describe_with_arturo_disabled :email_train_rspamd_in_mtc do
            it { refute_trains_rspamd }
          end
        end

        describe "when 'from localhost (unknown' is not in one of the bottom two Received headers" do
          before { mail.headers["Received"] = received_headers_localhost_unknown_not_in_bottom_two }

          it "does nothing" do
            statsd_client.
              expects(:increment).
              with(
                :fraud_signals_processor_suspend,
                tags: ["reason:high_rspamd_locahost_unknown"]
              ).
              never
            execute(state, mail)

            assert_nil state.suspension
          end

          describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
            it { refute_trains_rspamd }
          end
        end
      end

      describe "when Rspamd score is present and low" do
        before { mail.headers["X-Rspamd-Status"] = low_rspamd_score_headers }

        describe "when 'from localhost (unknown' is in one of the bottom two Received headers" do
          before { mail.headers["Received"] = received_headers_localhost_unknown_in_bottom_two }

          it "does nothing" do
            statsd_client.
              expects(:increment).
              with(
                :fraud_signals_processor_suspend,
                tags: ["reason:high_rspamd_locahost_unknown"]
              ).
              never
            execute(state, mail)

            assert_nil state.suspension
          end

          describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
            it { refute_trains_rspamd }
          end
        end

        describe "when 'from localhost (unknown' is not in one of the bottom two Received headers" do
          before { mail.headers["Received"] = received_headers_localhost_unknown_not_in_bottom_two }

          it "does nothing" do
            statsd_client.
              expects(:increment).
              with(
                :fraud_signals_processor_suspend,
                tags: ["reason:high_rspamd_locahost_unknown"]
              ).
              never
            execute(state, mail)

            assert_nil state.suspension
          end

          describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
            it { refute_trains_rspamd }
          end
        end
      end

      describe "when Rspamd score is not present" do
        # TODO: Does "no rspamd score here" need to be contained in an Array? - Yes
        [nil, ["X-Rspamd-Score header without score"]].each do |rspamd_header|
          before { mail.headers["X-Rspamd-Score"] = rspamd_header }

          describe "when 'from localhost (unknown' is in one of the bottom two Received headers" do
            before { mail.headers["Received"] = received_headers_localhost_unknown_in_bottom_two }

            it "does nothing" do
              statsd_client.
                expects(:increment).
                with(
                  :fraud_signals_processor_suspend,
                  tags: ["reason:high_rspamd_locahost_unknown"]
                ).
                never
              execute(state, mail)

              assert_nil state.suspension
            end

            describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
              it { refute_trains_rspamd }
            end
          end

          describe "when 'from localhost (unknown' is not in one of the bottom two Received headers" do
            before { mail.headers["Received"] = received_headers_localhost_unknown_not_in_bottom_two }

            it "does nothing" do
              statsd_client.
                expects(:increment).
                with(
                  :fraud_signals_processor_suspend,
                  tags: ["reason:high_rspamd_locahost_unknown"]
                ).
                never
              execute(state, mail)

              assert_nil state.suspension
            end

            describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
              it { refute_trains_rspamd }
            end
          end
        end
      end
    end

    describe_with_arturo_disabled :email_fraud_signals_processor_high_rspamd_localhost_unknown do
      describe "when Rspamd score is present and high and 'from localhost (unknown' is in one of the bottom two Received headers" do
        before do
          mail.headers["X-Rspamd-Status"] = high_rspamd_score_headers
          mail.headers["Received"] = received_headers_localhost_unknown_in_bottom_two
        end

        describe_with_arturo_enabled :email_fraud_signals_processor_high_rspamd_localhost_unknown_log_only do
          it "logs the rspamd score and bottom two Received headers but does not suspend or increment DataDog matches" do
            mail.logger.
              expects(:info).
              with(
                "High rspamd score and 'from localhost (unknown' detected in log-only mode: Rspamd score: #{high_rspamd_score}; Bottom two Received headers: #{received_headers_localhost_unknown_in_bottom_two.last(2)}"
              )
            statsd_client.
              expects(:increment).
              with(
                :fraud_signals_processor_suspend,
                tags: ["reason:high_rspamd_locahost_unknown"]
              ).
              never
            execute(state, mail)

            assert_nil state.suspension
          end
        end

        describe_with_arturo_disabled :email_fraud_signals_processor_high_rspamd_localhost_unknown_log_only do
          it "does nothing" do
            mail.logger.
              expects(:info).
              with(
                "High rspamd score and 'from localhost (unknown' detected in log-only mode: Rspamd score: #{high_rspamd_score}; Bottom two Received headers: #{received_headers_localhost_unknown_in_bottom_two.last(2)}"
              ).
              never
            statsd_client.
              expects(:increment).
              with(
                :fraud_signals_processor_suspend,
                tags: ["reason:high_rspamd_locahost_unknown"]
              ).
              never
            execute(state, mail)

            assert_nil state.suspension
          end

          describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
            it { refute_trains_rspamd }
          end
        end
      end
    end
  end

  describe "blocking based on presence of regruhosting.ru in Received header and presence of X-PHP-Originating-Script header" do
    describe "with regruhosting.ru in Received header and with X-PHP-Originating-Script present" do
      before do
        mail.headers["received"] = [
          "from 127.0.0.1 to some-internal-something.localhost",
          "by regruhosting.ru to poor@innocent.victim",
          "to something.org from someone.net"
        ]
        mail.headers["x-php-originating-script"] = "present"
      end

      describe_with_arturo_enabled :email_fraud_signals_processor_regruhosting_php_mailer do
        it "suspends for spam and sends a spammy pattern match of 'regruhosting_and_x_php_originating_script' to DataDog by tagging" do
          statsd_client.
            expects(:increment).
            with(
              :fraud_signals_processor_suspend,
              tags: ["reason:regruhosting_and_x_php_originating_script"]
            ).
            once

          execute(state, mail)

          assert_equal SuspensionType.MALICIOUS_PATTERN_DETECTED, state.suspension
        end

        describe_with_arturo_enabled :email_train_rspamd_in_mtc do
          it { assert_trains_rspamd }
        end

        describe_with_arturo_disabled :email_train_rspamd_in_mtc do
          it { refute_trains_rspamd }
        end
      end

      describe_with_arturo_disabled :email_fraud_signals_processor_regruhosting_php_mailer do
        it "does nothing" do
          execute(state, mail)

          assert_nil state.suspension
        end

        describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
          it { refute_trains_rspamd }
        end
      end
    end

    describe "with regruhosting.ru in Received header and without X-PHP-Originating-Script present" do
      before do
        mail.headers["received"] = [
          "from 127.0.0.1 to some-internal-something.localhost",
          "by regruhosting.ru to poor@innocent.victim",
          "to something.org from someone.net"
        ]
      end

      describe_with_and_without_arturo_enabled :email_fraud_signals_processor_regruhosting_php_mailer do
        it "does nothing" do
          execute(state, mail)

          assert_nil state.suspension
        end

        describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
          it { refute_trains_rspamd }
        end
      end
    end

    describe "without regruhosting.ru in Received header and with X-PHP-Originating-Script present" do
      before do
        mail.headers["received"] = [
          "from 127.0.0.1 to some-internal-something.localhost",
          "by gmail.com to poor@innocent.victim",
          "to something.org from someone.net"
        ]
        mail.headers["x-php-originating-script"] = "present"
      end

      describe_with_and_without_arturo_enabled :email_fraud_signals_processor_regruhosting_php_mailer do
        it "does nothing" do
          execute(state, mail)

          assert_nil state.suspension
        end

        describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
          it { refute_trains_rspamd }
        end
      end
    end

    describe "without regruhosting.ru in Received header and without X-PHP-Originating-Script present" do
      before do
        mail.headers["received"] = [
          "from 127.0.0.1 to some-internal-something.localhost",
          "by gmail.com to poor@innocent.victim",
          "to something.org from someone.net"
        ]
      end

      describe_with_and_without_arturo_enabled :email_fraud_signals_processor_regruhosting_php_mailer do
        it "does nothing" do
          execute(state, mail)

          assert_nil state.suspension
        end

        describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
          it { refute_trains_rspamd }
        end
      end
    end
  end

  describe "blocking based on spammy Received header patterns" do
    before do
      Arturo::Feature.
        stubs(:find_feature).
        with(:orca_fraud_signals_processor_received_header_patterns).
        returns(
          Arturo::Feature.new(
            symbol: "orca_fraud_signals_processor_received_header_patterns",
            external_beta_subdomains: [
              "some-bad-domain\\.com",
              "\\[185\\.43\\.221\\.158\\]",
              "spam\\.site"
            ]
          )
        )
    end

    describe "when there are spammy pattern matches in the Received headers" do
      before do
        mail.headers["received"] = [
          "from 127.0.0.1 to some-internal-something.localhost",
          "by spammer@some-bad-domain.com to poor@innocent.victim",
          "to something.org from someone.net"
        ]
      end

      describe_with_arturo_enabled :email_fraud_signals_processor_received_headers do
        describe "when there is a single spammy pattern match in the Received headers" do
          it "suspends for spam and sends the spammy pattern match to DataDog by tagging" do
            statsd_client.
              expects(:increment).
              with(
                :fraud_signals_processor_suspend,
                tags: [
                  "reason:spammy_received_header_pattern",
                  "spammy_pattern:some-bad-domain.com"
                ]
              ).
              once

            execute(state, mail)

            assert_equal SuspensionType.MALICIOUS_PATTERN_DETECTED, state.suspension
          end

          describe_with_arturo_enabled :email_train_rspamd_in_mtc do
            it { assert_trains_rspamd }
          end

          describe_with_arturo_disabled :email_train_rspamd_in_mtc do
            it { refute_trains_rspamd }
          end
        end

        describe "when there are multiple spammy pattern matches in the Received headers" do
          before do
            mail.headers['Received'] << "from [185.43.221.158] to someone.else"
          end

          it "suspends for spam and sends all of the spammy pattern matches to DataDog by tagging" do
            statsd_client.
              expects(:increment).
              with(
                :fraud_signals_processor_suspend,
                tags: [
                  "reason:spammy_received_header_pattern",
                  "spammy_pattern:some-bad-domain.com",
                  "spammy_pattern:[185.43.221.158]"
                ]
              ).
              once

            execute(state, mail)

            assert_equal SuspensionType.MALICIOUS_PATTERN_DETECTED, state.suspension
          end

          describe_with_arturo_enabled :email_train_rspamd_in_mtc do
            it { assert_trains_rspamd }
          end

          describe_with_arturo_disabled :email_train_rspamd_in_mtc do
            it { refute_trains_rspamd }
          end
        end
      end

      describe_with_arturo_disabled :email_fraud_signals_processor_received_headers do
        describe_with_arturo_enabled :email_fraud_signals_processor_received_headers_log_only do
          it "logs the spammy pattern matches but does not suspend or increment DataDog matches" do
            mail.logger.
              expects(:info).
              with(
                "some-bad-domain.com is not an ip address, FraudSafelist check has been skipped"
              )
            mail.logger.
              expects(:info).
              with(
                "Received header fraud signals detected in log-only mode: [\"some-bad-domain.com\"]"
              )
            statsd_client.
              expects(:increment).
              with(
                :fraud_signals_processor_suspend,
                tags: [
                  "spammy_pattern:some-bad-domain.com",
                ]
              ).
              never
            execute(state, mail)

            assert_nil state.suspension
          end

          describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
            it { refute_trains_rspamd }
          end
        end

        describe_with_arturo_disabled :email_fraud_signals_processor_received_headers_log_only do
          it "does nothing" do
            mail.logger.
              expects(:info).
              with(
                "Received header fraud signals detected in log-only mode: [\"some-bad-domain.com\"]"
              ).
              never
            statsd_client.
              expects(:increment).
              with(
                :fraud_signals_processor_suspend,
                tags: [
                  "spammy_pattern:some-bad-domain.com",
                ]
              ).
              never
            execute(state, mail)

            assert_nil state.suspension
          end

          describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
            it { refute_trains_rspamd }
          end
        end
      end
    end

    describe "when there are no spammy pattern matches in the Received headers" do
      describe_with_and_without_arturo_enabled :email_fraud_signals_processor_received_headers do
        before do
          mail.headers["received"] = [
            "from 127.0.0.1 to some-internal-something.localhost",
            "to something.gov from someone.net"
          ]
        end

        it "does nothing" do
          execute(state, mail)

          assert_nil state.suspension
        end

        describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
          it { refute_trains_rspamd }
        end
      end
    end

    describe "when there is a safe IP in the Arturo and Received headers" do
      before do
        mail.headers["received"] = ["from [10.210.80.219] to some-internal-something.localhost"]
        Arturo::Feature.
          stubs(:find_feature).
          with(:orca_fraud_signals_processor_received_header_patterns).
          returns(
            Arturo::Feature.new(
              symbol: "orca_fraud_signals_processor_received_header_patterns",
              external_beta_subdomains: [
                "some-bad-domain\\.com",
                "\\[10\\.210\\.80\\.219\\]",
                "spam\\.site"
              ]
            )
          )
      end
      describe_with_arturo_enabled :email_fraud_signals_processor_received_headers do
        it "does nothing" do
          execute(state, mail)
          assert_nil state.suspension
        end

        it "logs the offending safe IP pattern" do
          mail.logger.expects(:info).with('10.210.80.219 was found to be a safelisted IP address, skipping action')
          execute(state, mail)
        end

        describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
          it { refute_trains_rspamd }
        end

        describe_with_arturo_disabled :orca_classic_ip_safelist do
          it "should still block based on the IP" do
            execute(state, mail)

            assert_equal SuspensionType.MALICIOUS_PATTERN_DETECTED, state.suspension
          end

          describe_with_arturo_enabled :email_train_rspamd_in_mtc do
            it { assert_trains_rspamd }
          end

          describe_with_arturo_disabled :email_train_rspamd_in_mtc do
            it { refute_trains_rspamd }
          end
        end
      end
    end

    describe "with a malformed regex in the Arturo external_beta_subdomains" do
      before do
        Arturo::Feature.
          stubs(:find_feature).
          with(:orca_fraud_signals_processor_received_header_patterns).
          returns(
            Arturo::Feature.new(
              symbol: "orca_fraud_signals_processor_received_header_patterns",
              external_beta_subdomains: ["gwwwmc.\\xyz"]
            )
          )
        mail.headers["received"] = ["from someone to someone else"]
      end

      it "rescues the RegexpError, increments a DataDog metric, and does not suspend" do
        mail.logger.
          expects(:info).
          with(
            "FraudSignalsProcessor Received header regex error! RegexpError invalid hex escape: /gwwwmc.\\xyz/i"
          )
        statsd_client.
          expects(:increment).
          with(
            :fraud_signals_processor_received_header_error,
            tags: ["class:RegexpError"]
          ).
          once
        execute(state, mail)

        assert_nil state.suspension
      end

      describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
        it { refute_trains_rspamd }
      end
    end

    describe "when any non-RegexpError exception is raised while scanning Received headers for spammy patterns" do
      before do
        Zendesk::InboundMail::Processors::FraudSignalsProcessor.
          any_instance.
          expects(:received_headers).
          raises(StandardError)
      end

      it "rescues the exception, increments a DataDog metrics, and does not suspend" do
        mail.logger.
          expects(:info).
          with(
            "FraudSignalsProcessor Received header pattern matching errored for non-RegexpError reasons! StandardError StandardError"
          )
        statsd_client.
          expects(:increment).
          with(
            :fraud_signals_processor_received_header_error,
            tags: ["class:StandardError"]
          ).
          once
        execute(state, mail)

        assert_nil state.suspension
      end

      describe_with_and_without_arturo_enabled :email_train_rspamd_in_mtc do
        it { refute_trains_rspamd }
      end
    end
  end
end
