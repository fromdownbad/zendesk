require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::LanguageTextProcessor do
  fixtures :translation_locales, :accounts, :remote_authentications, :users, :user_identities

  describe "#execute" do
    def execute_user
      Zendesk::InboundMail::Processors::UserProcessor.execute(@state, @mail)
    end

    def execute
      Zendesk::InboundMail::Processors::LanguageTextProcessor.execute(@state, @mail)
    end

    def execute_for_cld_language(lang)
      Zendesk::InboundMail::Processors::LanguageTextProcessor.any_instance.expects(:set_cld_language).with(lang)
      execute_user
      execute
    end

    before do
      @mail = FixtureHelper::Mail.read('minimum_ticket.json')
      @state = Zendesk::InboundMail::ProcessingState.new
      @account = @state.account = accounts(:minimum)

      @english = translation_locales(:english_by_zendesk)
      @brazilian = translation_locales(:brazilian_portuguese)
      @zh_trad = translation_locales(:chinese_traditional)

      Account.any_instance.stubs(:available_languages).returns([@english, @brazilian, @zh_trad])

      @state.author = User.new
      @state.account.stubs(:is_open?).returns(true)
      @state.account.stubs(:is_signup_required?).returns(false)
      @state.plain_content = @mail.body + " add some more english text to make sure the language is detected"
      @state.from = Zendesk::Mail::Address.new(address: "new_user@example.com")
    end

    it "blows up when content is empty" do
      @state.plain_content = ""
      execute_user
      execute
      refute @state.locale_set_by_detection?
    end

    it "sets locale using detection if there's no language header" do
      execute_user
      execute
      assert_equal @english.id, @state.author.locale_id
      assert_equal @english.id, @state.locale_detected.to_i
      assert @state.locale_set_by_detection?
    end

    it "sets locale using detection if author is nil(SignupRequired case)" do
      @state.author = nil
      execute_user
      execute
      assert_equal @english.id, @state.locale_detected.to_i
      assert @state.locale_set_by_detection?
    end

    it "does not set locale if the detected language is not an account available language" do
      @state.plain_content = "Olá. Estou tendo problemas para imprimir do Microsoft Word"
      Account.any_instance.stubs(:available_languages).returns([@english])
      @state.author.expects(:translation_locale=).never
      execute_user
      execute
      refute @state.locale_set_by_detection?
    end

    it "overwrites locale if author already has a locale" do
      @state.author.locale_id = 4
      @state.plain_content = "Hello there how are you? I'm a japanese user with english text"
      execute
      assert @state.locale_set_by_detection?
    end

    it "overwrites locale for existing user without locale" do
      @state.author.stubs(persisted?: true, locale_id: nil)
      @state.plain_content = "Hello there how are you? I'm a japanese user with english text"
      execute
      assert @state.locale_set_by_detection?
    end

    it "does not overwrite locale if author is an existing user with locale" do
      @state.author.stubs(persisted?: true, locale_id: 4)
      @state.plain_content = "Hello there how are you? I'm a japanese user with english text"
      execute
      refute @state.locale_set_by_detection?
    end

    it "sets locale even when the result is unreliable" do
      @state.plain_content = "Sub-category: 宣傳活動或優惠碼"
      refute CLD.detect_language(@state.plain_content)[:reliable]

      execute_user
      execute

      assert_equal @zh_trad.id, @state.locale_detected.to_i
      assert @state.locale_set_by_detection?
    end

    it "only detects the part of the text before the delimiter in lowercase" do
      to_detect = @state.plain_content
      @state.plain_content += "[[classifier_delimiter]] blah blah blah"
      CLD.expects(:detect_language).with(to_detect).returns(code: "en", reliable: true)
      execute_user
      execute
    end

    it "changes the detected locale of norwegian from nb to no" do
      @state.plain_content = "Ikke svar pÃ¥ denne e-posten. Meldingen har blitt generert automatisk og er kun til informasjon."
      execute_for_cld_language("no")
    end

    it "changes the detected locale of hebrew from iw to he" do
      @state.plain_content = "בתאריך יום ג׳, 16 ביוני 2020 ב-9:23 מאת‬"
      execute_for_cld_language("he")
    end

    it "changes the detected locale of Traditional Chinese from zh-Hant to zh-tw" do
      @state.plain_content = "聰鷹電話紅貓敵後戰場總統"
      execute_for_cld_language("zh-tw")
    end

    it "detects Simplified Chinese" do
      @state.plain_content = "账号资讯请求"
      execute_for_cld_language("zh")
    end

    it "removes URLs before trying to detect the language" do
      @state.plain_content = "Hola mis amigos https://this-is-a-really-long-url.com/foo/bar/baz/hello/world/123/foo/456/test?query_string=this-is-a-query-string"
      CLD.expects(:detect_language).with("Hola mis amigos").once.returns(code: 'es', reliable: true)
      execute
    end

    it "detects the correct language with the URLs striped" do
      @state.plain_content = <<~TEXT.delete("\n")
        Bonjour, Je n ai plus aucune nouvelle de ma commande ?
        Pouvez vous m en apporter et me donner une date de livraison ??
        Cordialement Mr Faure Le mercredi 11 novembre 2015, Service Client -
        Teezily <support@example.com> a écrit : >
        <https://ci4.example.com/proxy/wrDEQWqjB9WKMgBEJAp0CsTJ3wGVZoOftPgugp5TNedkx1yccLe
        -tJTqdThDXxx0oDZgRk6Q4x8S5fVo9XpuByA-ykPuYRR2GjGVbfJdoA0PUy_65cnDHV9ThSuK=s0-d-e1
        -ft#http://www.example.com/assets/logo-42e79aba0eb9dc4f4da236141ec6e45e.png > > >
        Teezily > > Votre engagement prend des couleurs >
        < https://ci6.example.com/proxy/G8wDE5CW42FkprjOSZYFqDA_mE9nVr5gd-mI88yVJ-
        etMEpcGhPDk6oTTPprhF9SmP0FjUwepfiFJvAXcUMmoAOa8LZSkA=s0-d-e1-
        ft#http://www.example.com/assets/mailer/twitter.png>
        <https://ci3.example.com/proxy/MUmO1sL02PocjPRScDkrBEpn9IZ3ZJUQsyH9TRcNn_7PT
        _uYfvHL0GfImGlZxeueX_FLGeuCfb8AogIVcF8dgXq6CLiALDs=s0-d-e1-ft#http://www.example.com/assets/mailer/facebook.png>
        <https://ci3.example.com/proxy/pq5PDtYC1gVU16bspELLSOAW_fSGL2APDi-MOakK2zsWGFVdYBASP43LUJNIbnfryBvZfUXzqS56
        -ILLVToFAWyYC3lnkWFV=s0-d-e1-ft#http://www.example.com/assets/mailer/instagram.png
      TEXT

      Zendesk::InboundMail::Processors::LanguageTextProcessor.any_instance.expects(:set_cld_language).with("fr")
      execute
    end

    describe "the account's default locale" do
      before do
        @british_english = translation_locales(:british_english)
        @japanese = translation_locales(:japanese_x)
      end

      it "prefers the account default locale when it matches the detected locale" do
        @state.account.stubs(:translation_locale).returns(@english)
        Account.any_instance.stubs(:available_languages).returns([@british_english, @english, @brazilian])
        execute_user
        execute
        assert_equal @english.id, @state.locale_detected.to_i
      end

      it "uses the first matching locale when the account default locale does not match the detected locale" do
        @state.account.stubs(:translation_locale).returns(@japanese)
        Account.any_instance.stubs(:available_languages).returns([@british_english, @english, @japanese])
        execute_user
        execute
        assert_equal @british_english.id, @state.locale_detected.to_i
      end
    end
  end
end
