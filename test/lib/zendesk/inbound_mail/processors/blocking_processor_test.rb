require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::BlockingProcessor do
  fixtures :all

  let(:account) { accounts(:minimum) }

  before do
    @mail = FixtureHelper::Mail.read('minimum_ticket.json')
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = account
    @state.account.settings.accept_wildcard_emails = true
    @state.recipient = @mail.to.first.address
    @state.from = @mail.reply_to
  end

  it "does not reject allowed email addresses" do
    assert_pass
  end

  it "rejects blocked email recipient addresses" do
    with_blocked_address(@state.recipient) do
      assert_rejected
    end
  end

  # Preversing older tests for now.
  it "rejects emails for noreply@twitter.zendesk.com" do
    account.update_attribute(:subdomain, 'twitter')
    @state.recipient = "noreply@twitter.zendesk.com"
    @mail.to = Zendesk::Mail::Address.new(address: "noreply@twitter.zendesk.com")

    assert_rejected
  end

  describe "wildcard rejects" do
    before do
      @state.recipient = account.backup_email_address.sub(/.*@/, "foo@")
    end

    let(:account_route) { account.routes.create!(subdomain: "foobrand") }
    let(:route_address) { "foobar@#{account_route.default_host}" }

    describe "logging missing state.recipient for [EM-3458]" do
      before { @state.recipient = nil }

      let(:processor) { Zendesk::InboundMail::Processors::BlockingProcessor.new(@state, @mail) }

      it "does not detect blocked wildcard usage" do
        refute processor.send(:blocked_wildcard_usage?)
      end
    end

    it "does not reject wild card emails" do
      assert_pass
    end

    it "does not reject wild card route emails" do
      @state.recipient = route_address
      assert_pass
    end

    describe "when not accepting wildcard emails" do
      before do
        account.settings.accept_wildcard_emails = false
      end

      it "rejects wild card account emails" do
        assert_rejected
      end

      it "rejects wildcard route emails" do
        @state.recipient = route_address
        assert_rejected
      end

      it "does not reject wild card emails from mail_fetcher (or any other outside mailbox)" do
        @state.recipient = "foobar@gmail.com"
        assert_pass
      end

      it "does not reject emails to know recipient_address with id identifier" do
        @state.recipient = account.backup_email_address.sub("@", "+id123@")
        assert_pass
      end

      it "does not reject emails to know recipient_address" do
        account.recipient_addresses.create!(email: @state.recipient)
        assert_pass
      end

      it "does not reject emails to noreply address since they will be suspended" do
        email = Zendesk::Mailer::AccountAddress.new(account: account).noreply_email
        @state.recipient = email
        assert_pass
      end
    end
  end

  def assert_rejected
    assert_raise(Zendesk::InboundMail::HardMailRejectException) { assert_pass }
  end

  def assert_pass
    Zendesk::InboundMail::Processors::BlockingProcessor.execute(@state, @mail)
  end

  def with_blocked_address(address)
    list = Zendesk::InboundMail::Processors::BlockingProcessor::BLOCK_RECIPIENT_LIST
    list << address
    yield
  ensure
    list.delete(address)
  end
end
