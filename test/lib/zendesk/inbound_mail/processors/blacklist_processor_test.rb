require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::BlacklistProcessor do
  fixtures :accounts

  before do
    @account = accounts(:minimum)
    @account.domain_blacklist = 'minimum_end_user@aghassipour.com google.com'
    @account.domain_whitelist = nil
    @account.is_open          = true

    @mail      = Zendesk::Mail::InboundMessage.new
    @mail.from = Zendesk::Mail::Address.new(address: "minimum_end_user@aghassipour.com")

    @state            = Zendesk::InboundMail::ProcessingState.new
    @state.account    = @account
    @state.suspension = nil
  end

  it "does not suspend addresses not blacklisted" do
    @account.domain_blacklist = 'block:minimum_end_user suspend:@ reject:aghassipour block:aghassipour'
    Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail)

    assert_nil @state.suspension
  end

  describe "with malformed addresses" do
    before do
      @mail.from = Zendesk::Mail::Address.new(address: "Pure Water - Distribuidor Oficial PURA")
      @account.domain_blacklist = 'minimum_end_user@aghassipour.com google.com block:example.com'
    end

    it "does not blow up" do
      Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail)
    end
  end

  describe "with addresses blacklisted by the account domain lists" do
    before do
      @state.account.stubs(:blacklists?).returns(true)
    end

    it "is suspended" do
      @state.stubs(:whitelisted?).returns(false)
      Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail)

      assert_equal SuspensionType.BLOCKLISTED, @state.suspension
    end

    it 'is not skipped if already suspended' do
      @state.stubs(:suspended?).returns(true)
      Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail)

      assert_equal SuspensionType.BLOCKLISTED, @state.suspension
    end

    it "is skipped if the account is closed" do
      @state.account.stubs(:is_open?).returns(false)
      Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail)

      assert_nil @state.suspension, "Should skip closed accounts"
    end

    describe "and whitelisted" do
      before do
        @state.expects(:whitelisted?).at_least_once.returns(true)
      end

      it "is skipped" do
        Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail)
        assert_nil @state.suspension
      end

      describe "and explicitly listing domains" do
        before do
          @mail.reply_to = Zendesk::Mail::Address.new(address: "mentor@google.com")
        end

        it "suspends domains" do
          @state.account.domain_blacklist = 'suspend:google.com reject:google.com'
          Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail)

          assert_equal SuspensionType.BLOCKLISTED, @state.suspension
        end

        it "blocks domains" do
          @state.account.domain_blacklist = 'block:google.com reject:google.com'
          Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail)

          assert_equal SuspensionType.BLOCKLISTED, @state.suspension
        end

        it "rejects domains" do
          @state.account.domain_blacklist = 'reject:google.com'
          assert_raise(Zendesk::InboundMail::HardMailRejectException) { Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail) }
        end
      end

      describe "explicitly listing addresses" do
        it "suspends addresses" do
          @state.account.domain_blacklist = 'suspend:minimum_end_user@aghassipour.com reject:minimum_end_user@aghassipour.com'
          Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail)

          assert_equal SuspensionType.BLOCKLISTED, @state.suspension
        end

        it "blocks addresses" do
          @state.account.domain_blacklist = 'block:minimum_end_user@aghassipour.com reject:minimum_end_user@aghassipour.com'
          Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail)

          assert_equal SuspensionType.BLOCKLISTED, @state.suspension
        end

        it "rejects addresses" do
          @state.account.domain_blacklist = 'reject:minimum_end_user@aghassipour.com'
          assert_raise(Zendesk::InboundMail::HardMailRejectException) { Zendesk::InboundMail::Processors::BlacklistProcessor.execute(@state, @mail) }
        end
      end
    end
  end
end
