require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::GoogleVerificationBlockingProcessor do
  include ArturoTestHelper
  include MailTestHelper

  fixtures :accounts, :recipient_addresses

  let(:recipient_address) { recipient_addresses(:not_default) }

  before do
    @mail = mail
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = recipient_address.account
    @state.ticket = Ticket.new
  end

  let(:processor) { Zendesk::InboundMail::Processors::GoogleVerificationBlockingProcessor.new(@state, @mail) }

  describe '#execute' do
    let(:mail) { FixtureHelper::Mail.read('google_verification_email.json') }

    describe 'for Google verification email is intended for the account' do
      it 'email is suspended' do
        processor.expects(:suspend).once
        processor.execute
      end

      describe 'when the state is already suspended' do
        before { @state.suspension = SuspensionType.SPAM }

        it 'does not suspend again' do
          processor.expects(:suspend).never
          processor.execute
        end
      end

      describe 'when the email is a reply to a closed ticket' do
        before do
          @state.ticket = nil
          @state.closed_ticket = Ticket.new
        end

        it 'suspends the email' do
          processor.expects(:suspend).once
          processor.execute
        end
      end

      # This exploit is for a ticket comment only
      describe 'when email is not for an existing ticket' do
        before { @state.ticket = nil }

        it 'email is not suspended' do
          processor.expects(:suspend).never
          processor.execute
        end
      end
    end

    describe 'for Google verification email is intended for a different account' do
      before { @state.account = accounts(:support) }

      it 'email is not suspended' do
        processor.expects(:suspend).never
        processor.execute
      end
    end

    describe 'when email does not have Google reply-to address' do
      before { @mail.headers['reply-to'] = address('Joe Blow <joe@blow.com>') }

      it 'email is not suspended' do
        processor.expects(:suspend).never
        processor.execute
      end

      describe 'when email has Google from address' do
        before { @mail.headers['from'] = address('<noreply@google.com>') }

        it 'email is suspended' do
          processor.expects(:suspend).once
          processor.execute
        end
      end
    end

    describe 'when email does not have Google message-id' do
      before { @mail.headers['message-id'] = address('<lkajw23lkjrlw@nah.com>') }

      it 'email is not suspended' do
        processor.expects(:suspend).never
        processor.execute
      end
    end

    describe 'when email has multiple Zendesk plus ID in the delivered-to' do
      before { @mail.headers['delivered-to'] = address('support+id113+id222@minimum.zendesk-test.com') }

      it 'email is suspended' do
        processor.expects(:suspend).once
        processor.execute
      end
    end

    describe 'when email does not have a html part' do
      before do
        @mail.parts = [
          Zendesk::Mail::Part.new.tap do |part|
            part.content_type = 'text/plain'
            part.body = 'Your Google verification code is 105650'
          end
        ]
      end

      it 'email is suspended' do
        processor.expects(:suspend).once
        processor.execute
      end
    end
  end
end
