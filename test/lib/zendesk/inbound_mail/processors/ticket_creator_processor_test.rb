require_relative "../../../../support/test_helper"
require_relative "../../../../support/collaboration_settings_test_helper"

SingleCov.covered! uncovered: 0

describe Zendesk::InboundMail::Processors::TicketCreatorProcessor do
  fixtures :accounts, :subscriptions, :users, :user_identities, :tickets, :ticket_fields, :translation_locales,
    :brands, :organizations, :organization_memberships, :ticket_field_entries

  def execute
    Zendesk::InboundMail::Processors::TicketCreatorProcessor.new(@state, @mail).execute
  end

  def ensure_ticket
    Zendesk::InboundMail::Processors::TicketCreatorProcessor.new(@state, @mail).send(:ensure_ticket)
  end

  let(:account) { accounts(:minimum) }
  let(:state_author) { nil }

  before do
    CollaborationSettingsTestHelper.disable_all_new_collaboration_settings
    Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns([])

    account.default_brand = brands(:minimum)
    @closed = tickets(:minimum_5)

    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = account
    @state.plain_content = 'Email body'
    @state.recipient = @state.account.reply_address
    @state.author = state_author || @state.account.owner
    @state.from = Zendesk::Mail::Address.new(address: @state.author.email)
    @state.closed_ticket = @closed
    @state.original_recipient_address = "steven@example.com"
    @state.ticket = Ticket.new(account: account)

    @mail = Zendesk::Mail::InboundMessage.new
    @mail.subject = 'Hello email world'
    @state.message_id = @mail.message_id

    @japanese = translation_locales(:japanese)
    @english = translation_locales(:english_by_zendesk)
    Account.any_instance.stubs(:available_languages).returns([@english, @japanese])
  end

  describe "with a ticket" do
    before do
      @state.closed_ticket = nil
    end

    it "adds message content as comment" do
      ticket = Ticket.new(account: account)
      @state.ticket = ticket

      execute
      assert_equal ticket, @state.ticket
      assert_equal @state.plain_content, @state.ticket.comment.body
    end

    it "propagates changed created_at" do
      ticket = Ticket.new(created_at: Time.at(123456789), account: account)
      @state.ticket = ticket

      execute
      assert_equal ticket, @state.ticket
      assert_equal 123456789, @state.ticket.created_at.to_i
      assert_equal 123456789, @state.ticket.audit.created_at.to_i
      assert_equal 123456789, @state.ticket.comment.created_at.to_i
    end

    describe "with a flag and authorized" do
      before do
        @state.ticket = tickets(:minimum_1)
        @state.authorize!
        @state.add_flag(EventFlagType.OTHER_USER_UPDATE)
        execute
      end

      it "does not build a suspended ticket" do
        refute_suspended_ticket
      end

      it "builds a flagged comment" do
        assert @state.flagged?
      end
    end

    describe "with a valid suspension and authorized but likely unintended update to a closed ticket" do
      before do
        @state.closed_ticket = @closed
        @state.suspension = SuspensionType.OTHER_USER_UPDATE
        @state.authorize!
        execute
      end

      it "builds a suspended ticket" do
        assert_suspended_ticket
      end

      it "does not add a flagged comment" do
        refute @state.flagged?
      end

      it "associates a proper ticket nice_id" do
        assert_equal @closed.nice_id, @state.ticket.ticket_id
      end
    end

    describe "with a valid suspension and authorized" do
      before do
        @state.ticket = tickets(:minimum_1)
        @state.suspension = SuspensionType.SPAM
        @state.authorize!
        execute
      end

      it "builds a suspended ticket" do
        assert_suspended_ticket
      end

      it "does not add add flagged comment" do
        refute @state.flagged?
      end
    end
  end

  describe "without a ticket" do
    before do
      @state.account.field_subject.stubs(:is_active?).returns(true)
      @state.ticket = nil
      @state.closed_ticket = nil
    end

    it "builds a new ticket" do
      execute
      ticket = @state.ticket

      assert_equal Ticket,         ticket.class
      assert_equal @state.plain_content, ticket.description
    end

    it "is valid when there is no content" do
      @state.plain_content = ''
      execute
      ticket = @state.ticket

      assert_equal '[No content]', ticket.description
      assert(ticket.valid?)
    end

    it "builds a suspended ticket when suspended" do
      @state.stubs(:suspended?).returns(true)
      execute
      @state.ticket.class.must_equal SuspendedTicket
    end

    it "sets the ticket's translation_locale if locale_set_by_detection?" do
      @state.stubs(:locale_set_by_detection?).returns(true)
      @state.author = users(:minimum_end_user)
      japanese_locale = translation_locales(:japanese)
      @state.author.stubs(:translation_locale).returns(japanese_locale)
      execute
      @state.ticket.translation_locale.must_equal japanese_locale
    end

    describe "when the account is created after unverified account owner rollout date" do
      before { @state.account.stubs(:created_at).returns(DateTime.new(2019, 7, 9)) }

      describe_with_arturo_enabled :email_suspend_ticket_from_unverified_account_owner do
        describe "when the account owner is verified" do
          it "does not suspend the ticket" do
            execute
            refute @state.suspended?
          end
        end

        describe "when the account owner is unverified" do
          before { @state.account.owner.stubs(:is_verified?).returns(false) }

          it "suspends the ticket" do
            execute
            @state.suspension.must_equal SuspensionType.ACCOUNT_OWNER_EMAIL_UNVERIFIED
          end
        end
      end

      describe_with_arturo_disabled :email_suspend_ticket_from_unverified_account_owner do
        describe "when the account owner is verified" do
          it "does not suspend the ticket" do
            execute
            refute @state.suspended?
          end
        end

        describe "when the account owner is unverified" do
          before { @state.account.owner.stubs(:is_verified?).returns(false) }

          it "does not suspend the ticket" do
            execute
            refute @state.suspended?
          end
        end
      end
    end

    describe "when the account is created before unverified account owner rollout date" do
      before { @state.account.stubs(:created_at).returns(DateTime.new(2000, 1, 1)) }

      describe_with_and_without_arturo_enabled :email_suspend_ticket_from_unverified_account_owner do
        describe "when the account owner is verified" do
          it "does not suspend the ticket" do
            execute
            refute @state.suspended?
          end
        end

        describe "when the account owner is unverified" do
          before { @state.account.owner.stubs(:is_verified?).returns(false) }

          it "does not suspend the ticket" do
            execute
            refute @state.suspended?
          end
        end
      end
    end
  end

  describe "with a new ticket" do
    before do
      @state.ticket = Ticket.new(account: account)
    end

    it "builds a suspended ticket if it has a suspension" do
      @state.suspension = SuspensionType.OTHER_USER_UPDATE
      execute
      @state.ticket.class.must_equal SuspendedTicket
    end

    it "builds a regular ticket if it has a flag and no suspension" do
      @state.add_flag(EventFlagType.MACHINE_GENERATED)
      execute
      @state.ticket.class.must_equal Ticket
    end
  end

  describe_with_and_without_arturo_setting_enabled :email_make_other_user_comments_private do
    describe "with a closed ticket and a collaborator" do
      before do
        @state.ticket = nil
        @state.stubs(:authorized?).returns(true)

        # When the :email_make_other_user_comments_private Arturo is enabled,
        # the @state will be flagged as EventFlagType.OTHER_USER_UPDATE, so
        # the @state.author will only be able to comment privately on the ticket
        unless @state.account.has_email_make_other_user_comments_private?
          @state.author.abilities.expects(:can?).with(:publicly, Comment).returns(true)
        end
      end

      describe "and collaboration is enabled, the ticket" do
        before do
          author_can(
            edit: false,
            add_collaborator: true,
            view: true
          )
        end

        it "is not suspended" do
          execute
          refute_suspended_ticket
        end

        it "is flagged" do
          execute
          assert @state.flagged?
          assert_equal @state.flags, AuditFlags.new([EventFlagType.OTHER_USER_UPDATE])
        end

        it "uses rich content for the followup ticket" do
          execute
          assert @state.ticket.comment.rich?
          assert_equal "This is a follow-up to your previous request ##{@closed.nice_id} \"#{@closed.title}\"<br /><br /><div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello email world: Email body</p></div>", @state.ticket.description
          assert_equal ViaType.CLOSED_TICKET, @state.ticket.via_id
        end

        describe_with_arturo_disabled :email_followup_ticket_html_comment_fix do
          it "uses plain content for the followup ticket" do
            execute
            refute @state.ticket.comment.rich?
            assert_equal "This is a follow-up to your previous request ##{@closed.nice_id} \"#{@closed.title}\"\n\nHello email world: Email body", @state.ticket.description
            assert_equal ViaType.CLOSED_TICKET, @state.ticket.via_id
          end
        end
      end

      describe "and collaborator can_only_create_comments, the ticket" do
        before do
          author_can(
            edit: false,
            add_collaborator: false,
            only_create_ticket_comments: true,
            view: true
          )
          execute
        end

        it "is not suspended" do
          refute_suspended_ticket
        end

        it "is flagged" do
          assert @state.flagged?
          assert_equal @state.flags, AuditFlags.new([EventFlagType.OTHER_USER_UPDATE])
        end

        it "is a followup ticket" do
          assert_equal ViaType.CLOSED_TICKET, @state.ticket.via_id
        end
      end

      describe "and collaboration is disabled, the ticket" do
        before do
          author_can(
            edit: false,
            add_collaborator: false,
            only_create_ticket_comments: false,
            view: false
          )
          execute
        end

        it "is not suspended" do
          refute_suspended_ticket
        end

        it "is flagged" do
          assert @state.flagged?
          assert_equal @state.flags, AuditFlags.new([EventFlagType.OTHER_USER_UPDATE])
        end

        it "is a followup ticket" do
          assert_equal ViaType.CLOSED_TICKET, @state.ticket.via_id
        end
      end
    end
  end

  describe "with existing tickets" do
    before do
      @state.closed_ticket = nil
      @state.ticket = Ticket.new(account: @state.account)
    end

    it "validates" do
      @state.account.stubs(:is_serviceable?).returns(false)
      execute
      assert_equal SuspendedTicket, @state.ticket.class
    end

    it "adds comment" do
      execute
      assert_equal Comment, @state.ticket.comment.class
    end

    it "adds comments without content" do
      @state.plain_content = ''
      execute
      comment = @state.ticket.comment

      assert_equal Comment,        comment.class
      assert_equal '[No content]', comment.body
      assert(comment.valid?)
    end

    it "has current via id of Mail" do
      execute
      assert_equal ViaType.MAIL, @state.ticket.current_via_id
    end

    describe "comment privacy" do
      before do
        Account.any_instance.stubs(
          has_follower_and_email_cc_collaborations_enabled?: false,
          has_comment_email_ccs_allowed_enabled?: false,
          has_ticket_followers_allowed_enabled?: false
        )
      end

      describe "when the author is an agent" do
        before do
          @state.author.abilities.stubs(:can?).with(:edit, @state.ticket).returns(true)
          @state.author.stubs(is_agent?: true)
        end

        describe "that can publicly comment" do
          before { @state.author.abilities.stubs(:can?).with(:publicly, Comment).returns(true) }

          describe "replying to a notification" do
            before do
              Account.any_instance.stubs(
                has_follower_and_email_cc_collaborations_enabled?: true
              )
            end

            describe "where the parent comment is private" do
              before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(parent_comments_public?: false) }

              describe "with comment_email_ccs_allowed enabled" do
                before do
                  Account.any_instance.stubs(
                    has_follower_and_email_cc_collaborations_enabled?: true,
                    has_comment_email_ccs_allowed_enabled?: true
                  )
                end

                describe 'and requester is not present on the email' do
                  before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: true) }

                  it "adds a private comment" do
                    ticket = ensure_ticket
                    refute ticket.comment.is_public?
                  end
                end

                describe 'and requester is present on the email' do
                  before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: false) }

                  it "adds a public comment" do
                    ticket = ensure_ticket
                    assert ticket.comment.is_public?
                  end
                end
              end

              describe "with ticket_followers_allowed enabled" do
                before { Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true) }

                describe 'and requester is not present on the email' do
                  before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: true) }

                  it "adds a private comment" do
                    ticket = ensure_ticket
                    refute ticket.comment.is_public?
                  end
                end

                describe 'and requester is present on the email' do
                  before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: false) }

                  it "adds a public comment" do
                    ticket = ensure_ticket
                    assert ticket.comment.is_public?
                  end
                end
              end
            end

            describe "where the parent comment is public" do
              before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(parent_comments_public?: true) }

              describe 'and requester is present on the email' do
                before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: true) }

                describe "with comment_email_ccs_allowed enabled" do
                  before do
                    Account.any_instance.stubs(
                      has_follower_and_email_cc_collaborations_enabled?: true,
                      has_comment_email_ccs_allowed_enabled?: true
                    )
                  end

                  it "adds a public comment" do
                    ticket = ensure_ticket
                    assert ticket.comment.is_public?
                  end
                end

                describe "with ticket_followers_allowed enabled" do
                  before { Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true) }

                  it "adds a public comment" do
                    ticket = ensure_ticket
                    assert ticket.comment.is_public?
                  end
                end
              end

              describe 'and requester is present on the email' do
                before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: false) }

                it "adds a public comment" do
                  ticket = ensure_ticket
                  assert ticket.comment.is_public?
                end
              end
            end
          end
        end
      end

      describe "when the author is an end user" do
        before { @state.author.stubs(is_agent?: false) }

        describe "and the author is an email cc" do
          before do
            @state.stubs(author_is_email_cc?: true)
            Account.any_instance.stubs(
              has_follower_and_email_cc_collaborations_enabled?: true,
              has_comment_email_ccs_allowed_enabled?: true
            )
          end

          describe "and the requester is not in the recipients" do
            it "adds a private comment" do
              @mail.stubs(:direct_recipients).returns(["foo@example.com"])
              ticket = ensure_ticket

              refute_equal @state.ticket.requester, @state.author
              refute ticket.comment.is_public?
            end
          end
        end
      end
    end
  end

  describe "with existing closed tickets" do
    let(:end_user) { users(:minimum_end_user) }
    let(:end_user_2) { users(:minimum_author) }
    let(:agent) { users(:minimum_admin) }

    before do
      Account.any_instance.stubs(is_collaboration_enabled?: true)
      @state.closed_ticket.set_collaborators = [end_user_2.id, agent.id]
      @state.author = end_user_2
      @state.ticket = nil
    end

    it "stores the closed ticket id as the via_reference_id on the new ticket" do
      @state.account.field_subject.stubs(:is_active?).returns(true)
      execute
      assert_equal @closed.id, @state.ticket.via_reference_id
    end

    it "stores the original via id on the new ticket" do
      @state.account.field_subject.stubs(:is_active?).returns(true)
      execute
      assert_equal ViaType.MAIL, @state.ticket.original_via_id
    end

    describe "with legacy CCs" do
      before do
        @state.closed_ticket.collaborations.delete_all
        @state.closed_ticket.set_collaborators = [end_user_2.id, agent.id]
        @state.author = end_user
        assert @state.closed_ticket.collaborations.all? { |collaboration| collaboration.collaborator_type == CollaboratorType.LEGACY_CC }
      end

      describe "with ticket collaboration disabled and CCs/Followers settings enabled" do
        before do
          @state.account.stubs(
            is_collaboration_enabled?: false,
            has_follower_and_email_cc_collaborations_enabled?: true,
            has_comment_email_ccs_allowed_enabled?: true,
            has_ticket_followers_allowed_enabled?: true
          )
          execute
        end

        it "copies followers and email CCs as legacy CCs (will be transformed when ticket is saved)" do
          assert @state.ticket.collaborations.all? { |collaboration| collaboration.collaborator_type == CollaboratorType.LEGACY_CC }
        end

        it "copies the correct collaborations" do
          assert_empty @state.ticket.collaborations.map(&:user_id) - [agent.id, end_user_2.id]
        end
      end
    end

    describe "with an anonymized closed ticket" do
      let(:anonymous_user) { Zendesk::TicketAnonymizer::AnonymousUser.find_or_create account: @state.account }

      before do
        @state.closed_ticket.requester = anonymous_user
      end

      describe_with_arturo_enabled :skip_anonymized_ticket_followup do
        it "bypasses followup creation and doesn't set ViaType as followup" do
          execute
          assert_equal @state.author, @state.ticket.requester
          assert_not_equal anonymous_user, @state.ticket.requester
          assert_equal 'Mail', @state.ticket.via
          assert_equal ViaType.MAIL, @state.ticket.via_id, "expected ViaType Mail, got #{@state.ticket.via}"
        end
      end

      describe_with_arturo_disabled :skip_anonymized_ticket_followup do
        it "creates a followup ticket with requester as anonymous user and sets ViaType as followup" do
          execute
          assert_equal 'Anonymous User', @state.ticket.requester.name
          assert_equal ViaType.CLOSED_TICKET, @state.ticket.via_id, "expected ViaType Closed Ticket, got #{@state.ticket.via}"
        end
      end
    end

    describe "with followers and email CCs" do
      before do
        @state.closed_ticket.collaborations.delete_all
        @state.closed_ticket.collaborations.create(collaborator_type: CollaboratorType.FOLLOWER, user_id: agent.id)
        @state.closed_ticket.collaborations.create(collaborator_type: CollaboratorType.EMAIL_CC, user_id: end_user_2.id)
      end

      describe "with follower_and_email_cc_collaborations disabled" do
        before do
          @state.account.stubs(has_follower_and_email_cc_collaborations_enabled?: false)
          execute
        end

        it "copies followers and email CCs as legacy CCs" do
          assert @state.ticket.collaborations.all? { |collaboration| collaboration.collaborator_type == CollaboratorType.LEGACY_CC }
        end

        it "copies the correct collaborations" do
          assert_empty @state.ticket.collaborations.map(&:user_id) - [agent.id, end_user_2.id]
        end
      end

      describe "with follower_and_email_cc_collaborations enabled" do
        before { @state.account.stubs(has_follower_and_email_cc_collaborations_enabled?: true) }

        describe "with comment_email_ccs_allowed and ticket_followers_allowed enabled" do
          before do
            @state.account.stubs(
              has_comment_email_ccs_allowed_enabled?: true,
              has_ticket_followers_allowed_enabled?: true
            )
            execute
          end

          it "copies followers and email CCs with the correct types" do
            assert @state.ticket.collaborations.select { |c| c.user_id == agent.id && c.collaborator_type == CollaboratorType.FOLLOWER }.present?
            assert @state.ticket.collaborations.select { |c| c.user_id == end_user_2.id && c.collaborator_type == CollaboratorType.EMAIL_CC }.present?
          end

          it "copies the correct collaborations" do
            assert_empty @state.ticket.collaborations.map(&:user_id) - [agent.id, end_user_2.id]
          end
        end
      end
    end

    describe "ticket validity" do
      it "assigns a valid ticket (using subject)" do
        @state.account.field_subject.stubs(:is_active?).returns(true)
        ticket = ensure_ticket

        assert_equal ViaType.CLOSED_TICKET, ticket.via_id
        assert_equal StatusType.NEW, ticket.status_id
        assert_equal @mail.subject, ticket.subject
        assert_equal "This is a follow-up to your previous request ##{@closed.nice_id} \"#{@closed.title}\"<br /><br /><div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Email body</p></div>", ticket.description
        assert_equal @state.account, ticket.account
        assert_equal @state.author, ticket.submitter
        assert_equal @state.closed_ticket.requester, ticket.requester

        # When you update an existing closed ticket, we create a follow up ticket instead.
        # The new follow up ticket gets the old collaborators set as well as the person
        # reopening the ticket as a collaborator, unless this is the original requester.
        old_collabs = @state.closed_ticket.collaborations.map(&:user_id)
        new_collabs = ticket.collaborations.map(&:user_id) + [ticket.submitter.id]
        assert_equal old_collabs.sort.uniq, new_collabs.sort.uniq

        assert_equal @state.closed_ticket.current_tags, ticket.current_tags
        assert_equal @state.closed_ticket.recipient, ticket.recipient
        assert_equal @state.original_recipient_address, ticket.original_recipient_address

        # NOTE: follow-up ticket must contain all active ticket_fields from the account and the original ticket
        old_fields = @state.closed_ticket.ticket_field_entries.map(&:ticket_field_id) + account.ticket_fields.custom.active.map(&:id)
        new_fields = ticket.ticket_field_entries.map(&:ticket_field_id).sort
        assert_equal old_fields.uniq.sort, new_fields

        assert ticket.valid?, ticket.errors.to_xml
        assert_equal Ticket, ticket.class
      end

      it "has correct subject and description when not using subject" do
        @state.account.field_subject.stubs(:is_active?).returns(false)
        ticket = ensure_ticket
        assert_equal "This is a follow-up to your previous request ##{@closed.nice_id} \"#{@closed.title}\"<br /><br /><div class=\"zd-comment\" dir=\"auto\"><p dir=\"auto\">Hello email world: Email body</p></div>", ticket.description
        assert_equal '', ticket.subject
      end

      describe_with_arturo_disabled :email_followup_ticket_html_comment_fix do
        it "has correct subject and description when not using subject" do
          @state.account.field_subject.stubs(:is_active?).returns(false)
          ticket = ensure_ticket
          assert_equal "This is a follow-up to your previous request ##{@closed.nice_id} \"#{@closed.title}\"\n\nHello email world: Email body", ticket.description
          assert_equal '', ticket.subject
        end
      end

      it "does not include ticket field entries whose ticket fields are missing" do
        next_ticket_field_id = @state.account.ticket_fields.last.id + 1
        ticket_fields = Zendesk::InboundMail::Ticketing::Followup.new(@state, @mail, nil).ticket_fields
        ticket_fields.push(ticket_field_id: next_ticket_field_id, value: "deleted")
        Zendesk::InboundMail::Ticketing::Followup.any_instance.stubs(:ticket_fields).returns(ticket_fields)

        ticket = ensure_ticket
        refute_includes ticket.ticket_field_entries.map(&:ticket_field_id), next_ticket_field_id
      end

      it "can have an initial private comment if a light agent replies" do
        @state.author = users(:minimum_agent)
        # Mock a light agent who can't make public comments
        User.any_instance.stubs(:can?).returns(false)
        ticket = ensure_ticket

        refute ticket.comment.is_public, 'Expected light agent follow to have a private initial comment'
      end
    end

    it "adds a comment" do
      ticket = ensure_ticket
      comment = ticket.comment
      assert_equal "This is a follow-up to your previous request #5 \"minimum five ticket (for se...\"\n\n\nHello email world: Email body", comment.body
      assert(comment.is_public)
    end

    describe_with_arturo_disabled :email_followup_ticket_html_comment_fix do
      it "adds a comment" do
        ticket = ensure_ticket
        comment = ticket.comment
        assert_equal "This is a follow-up to your previous request ##{@closed.nice_id} \"#{@closed.title}\"\n\nHello email world: Email body", comment.body
        assert(comment.is_public)
      end
    end

    describe "comment privacy" do
      before do
        Account.any_instance.stubs(
          has_follower_and_email_cc_collaborations_enabled?: false,
          has_comment_email_ccs_allowed_enabled?: false,
          has_ticket_followers_allowed_enabled?: false
        )
      end

      describe "when the author is an agent" do
        before do
          @state.author.abilities.stubs(:can?).with(:edit, @state.ticket).returns(true)
          @state.author.stubs(is_agent?: true)
        end

        describe "that can publicly comment" do
          before { @state.author.abilities.stubs(:can?).with(:publicly, Comment).returns(true) }

          describe "replying to a notification with follower_and_email_cc_collaborations enabled" do
            before do
              Account.any_instance.stubs(
                has_follower_and_email_cc_collaborations_enabled?: true
              )
            end

            describe "where the parent comment is private" do
              before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(parent_comments_public?: false) }

              describe "with comment_email_ccs_allowed enabled" do
                before { Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true) }

                describe 'and requester is not present on the email' do
                  before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: true) }

                  it "adds a private comment" do
                    ticket = ensure_ticket
                    refute ticket.comment.is_public?
                  end
                end

                describe 'and requester is present on the email' do
                  before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: false) }

                  it "adds a public comment" do
                    ticket = ensure_ticket
                    assert ticket.comment.is_public?
                  end
                end
              end

              describe "with ticket_followers_allowed enabled" do
                before { Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true) }

                describe 'and requester is not present on the email' do
                  before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: true) }

                  it "adds a private comment" do
                    ticket = ensure_ticket
                    refute ticket.comment.is_public?
                  end
                end

                describe 'and requester is present on the email' do
                  before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: false) }

                  it "adds a public comment" do
                    ticket = ensure_ticket
                    assert ticket.comment.is_public?
                  end
                end
              end
            end

            describe "where the parent comment is public" do
              before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(parent_comments_public?: true) }

              describe 'and requester is present on the email' do
                before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: false) }

                describe "with follower_and_email_cc_collaborations and comment_email_ccs_allowed enabled" do
                  before do
                    Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true)
                    author_collaboration = @state.closed_ticket.collaborations.find { |c| c.user_id == @state.author.id }
                    author_collaboration.collaborator_type = CollaboratorType.FOLLOWER
                  end

                  it "adds a public comment" do
                    ticket = ensure_ticket
                    assert ticket.comment.is_public?
                  end
                end

                describe "with ticket_followers_allowed enabled" do
                  before { Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true) }

                  it "adds a public comment" do
                    ticket = ensure_ticket
                    assert ticket.comment.is_public?
                  end
                end
              end

              describe 'and requester is not present on the email' do
                before { Zendesk::InboundMail::Ticketing::CommentPrivacy.any_instance.stubs(requester_not_found?: true) }

                it "adds a public comment" do
                  ticket = ensure_ticket
                  assert ticket.comment.is_public?
                end
              end
            end
          end
        end
      end

      describe "when the author is an end user" do
        before { @state.author.stubs(is_agent?: false) }

        describe "and the author is an email cc" do
          before do
            @state.stubs(author_is_email_cc?: true)
            Account.any_instance.stubs(
              has_follower_and_email_cc_collaborations_enabled?: true,
              has_comment_email_ccs_allowed_enabled?: true
            )
          end

          describe "and the requester is not in the recipients" do
            it "adds a private comment" do
              @mail.stubs(:direct_recipients).returns(["foo@example.com"])
              ticket = ensure_ticket

              refute_equal @state.ticket.requester, @state.author
              refute ticket.comment.is_public?
            end
          end
        end
      end
    end

    it "has the correct author for a comment" do
      @state.author = users(:minimum_admin)
      ticket = ensure_ticket

      assert_not_equal @state.author, ticket.requester
      assert ticket.collaborations.map(&:user_id).include?(@state.author.id)

      comment = ticket.comment
      assert_equal @state.author, comment.author
    end
  end

  describe "metadata" do
    before do
      @metadata = {
        message_id: 'message_id',
        client: 'client',
        ip_address: 'ip'
      }

      Zendesk::InboundMail::Ticketing::MailTicket.any_instance.expects(:metadata).returns(@metadata)
    end

    describe "on a ticket audit" do
      before do
        @state.ticket = Ticket.new(account: account)
        execute
      end

      it "adds MailTicket#metadata to metadata[:system]" do
        assert_equal @metadata.with_indifferent_access, @state.ticket.audit.metadata[:system]
      end
    end

    describe "when html_content is present" do
      before do
        @state.html_content = "<div>Email body</div>"
        @state.ticket = Ticket.new(account: account)
      end

      it "sets the comment format to rich" do
        execute
        assert "rich", @state.ticket.comment.format
      end

      describe_with_arturo_disabled :email_exclude_inline_attachments_fix do
        it "does not set the comment format to rich" do
          execute
          refute_equal "rich", @state.ticket.comment.format
        end
      end
    end

    describe "on a suspended ticket" do
      before do
        @state.account.stubs(:is_serviceable?).returns(false)
        execute

        assert_instance_of SuspendedTicket, @state.ticket
      end

      it "adds MailTicket#metadata to client" do
        assert_equal @metadata, @state.ticket.metadata
      end
    end
  end

  describe "#suspend_ticket" do
    let(:ticket) { Zendesk::InboundMail::Processors::TicketCreatorProcessor.new(@state, @mail).send(:suspend_ticket) }

    it "assigns a suspended ticket" do
      @state.properties = { set_tags: "hello there" }
      assert_equal ticket.class, SuspendedTicket
      assert_equal @state.account,       ticket.account
      assert_equal @mail.subject,        ticket.subject
      assert_equal @state.from.name,     ticket.from_name
      assert_equal @state.from.address,  ticket.from_mail
      assert_equal @state.author,        ticket.author
      assert_equal @state.recipient,     ticket.recipient
      assert_equal_with_nil @state.suspension, ticket.cause
      assert_equal @state.properties,    ticket.properties.except(:disable_triggers)
      assert_equal @state.plain_content, ticket.content
      assert_equal @closed.nice_id,      ticket.ticket_id
      assert_equal @state.message_id,    ticket.message_id
    end

    it "marks as redacted when the mail has redactions" do
      @mail.headers['X-Zendesk-Redacted'] = true
      assert(ticket.properties[:redacted])
    end

    it "does not mark as redacted when the mail has no redactions" do
      assert_nil @mail.headers['X-Zendesk-Redacted']
      assert_equal false, ticket.properties.key?(:redacted)
    end

    it "assigns translation locale to suspended_ticket if locale is detected" do
      @state.expects(:locale_detected).times(2).returns(@japanese.id)
      @state.expects(:locale_set_by_detection?).returns(true)
      assert_equal @state.locale_detected, ticket.properties[:locale_id]
    end

    it "assigns known authors" do
      refute @state.author.new_record?
      assert_equal @state.author, ticket.author
    end

    it "does not assign new authors" do
      @state.author = @state.account.users.new(name: 'new author', email: 'new@example.com')
      assert_nil ticket.author
    end

    describe "machine tickets" do
      it "marks suspended ticket as from machine with machine header" do
        @mail.headers['X-Zendesk-From-Account-Id'] = 'hash'
        assert ticket.properties[:from_zendesk]
        assert ticket.to_ticket.requester.machine?
      end

      it "does not mark suspended ticket as from machine without machine header" do
        assert_nil ticket.properties[:from_zendesk]
        refute ticket.to_ticket.requester.machine?
      end
    end
  end

  describe "#ensure_valid_ticket_group_for_restricted_agent" do
    def ensure_valid_ticket_group_for_restricted_agent
      Zendesk::InboundMail::Processors::TicketCreatorProcessor.new(@state, @mail).send(:ensure_valid_ticket_group_for_restricted_agent)
    end

    before do
      @some_group_id = 50
      @state.submitter.stubs(:default_group_id).returns(@some_group_id)
      @state.submitter.stubs(:restriction_id).returns(RoleRestrictionType.GROUPS)
      @state.stubs(:agent_forwarded).returns(true)
      @state.ticket.group_id = nil
      @state.closed_ticket = nil
    end

    describe "suspended ticket" do
      before do
        @state.ticket = SuspendedTicket.new
      end

      describe "forwarded messaged from agent who is groups restricted" do
        it "does not raise error" do
          ensure_valid_ticket_group_for_restricted_agent
        end
      end
    end

    describe "forwarded messaged from agent who is groups restricted" do
      it "changes ticket's group id to match agent's group id" do
        execute
        @state.ticket.group_id.must_equal @some_group_id
      end
    end

    describe "not agent forwarded" do
      before do
        @state.stubs(:agent_forwarded).returns(false)
      end

      it "does nothing" do
        ensure_valid_ticket_group_for_restricted_agent
        @state.ticket.group_id.must_be_nil
      end
    end

    describe "agent not group restricted" do
      before do
        @state.submitter.stubs(:restriction_id).returns(RoleRestrictionType.NONE)
      end

      it "does nothing" do
        ensure_valid_ticket_group_for_restricted_agent
        @state.ticket.group_id.must_be_nil
      end
    end
  end

  describe "#ensure_valid_ticket_group" do
    def ensure_valid_ticket_group
      Zendesk::InboundMail::Processors::TicketCreatorProcessor.new(@state, @mail).send(:ensure_valid_ticket_group)
    end

    before do
      @state.account = accounts(:with_groups)
      @state.ticket.group_id = 999
    end

    let(:mail_logger) { Zendesk::InboundMail::MailLogger.any_instance }
    let(:default_group) { @state.account.default_group }

    describe_with_arturo_disabled :email_replace_invalid_group do
      it "does not replace the invalid group" do
        ensure_valid_ticket_group
        refute_equal default_group, @state.ticket.group
      end
    end

    describe_with_arturo_enabled :email_replace_invalid_group do
      it "replaces the invalid group with the default group" do
        ensure_valid_ticket_group
        assert_equal default_group, @state.ticket.group
      end

      it "logs" do
        mail_logger.expects(:log).with("Default group ##{default_group.id}: #{default_group.inspect}")
        mail_logger.expects(:log).with("Replacing ticket's invalid group ##{@state.ticket.group_id} with the account's default group ##{default_group.id}")
        ensure_valid_ticket_group
      end
    end
  end

  describe "#ensure_valid_operation" do
    # ideally test via execute
    def ensure_valid_operation(ticket = @state.ticket)
      Zendesk::InboundMail::Processors::TicketCreatorProcessor.new(@state, @mail).send(:ensure_valid_operation, ticket)
    end

    before do
      Zendesk::Accounts::Client.any_instance.stubs(:abusive).returns(false)
      @state.account.stubs(:id).returns(121)
      @state.ticket.stubs(:id).returns(4)
      @state.author.stubs(:id).returns(10)
    end

    it "suspends if account is unserviceable" do
      @state.account.stubs(:is_serviceable?).returns(false)
      Zendesk::InboundMail::MailLogger.any_instance.expects(:log).with("Unable to update ticket #{@state.ticket.id} because the account #{@state.account.id} is unserviceable")
      @state.account.settings.is_abusive = false
      ensure_valid_operation
      assert_equal SuspensionType.UNSERVICEABLE_ACCOUNT, @state.suspension
    end

    it "logs abusive user trial limit version of log message" do
      @state.account.stubs(:is_serviceable?).returns(false)
      @state.account.settings.is_abusive = true
      Zendesk::InboundMail::MailLogger.any_instance.expects(:log).with("Unable to update ticket #{@state.ticket.id} because the account #{@state.account.id} is unserviceable due to exceeding user trial limit")
      ensure_valid_operation
    end

    it "does not suspend the ticket if the comment if from a light agent" do
      @state.account.stubs(:is_serviceable?).returns(true)
      author_can(edit: false, only_create_ticket_comments: true, view: true)

      assert ensure_valid_operation
    end

    describe "when author is not a collaborator" do
      before do
        author_can(edit: false, view: false)
      end

      it "sets suspension if the mail was not authorized by encoded ID" do
        author_can(only_create_ticket_comments: false)

        ensure_valid_operation

        @state.suspension.must_equal SuspensionType.OTHER_USER_UPDATE
        assert @state.suspended?
        refute @state.flagged?
      end

      describe_with_and_without_arturo_setting_enabled :follower_and_email_cc_collaborations do
        before do
          if @state.account.has_follower_and_email_cc_collaborations_enabled?
            Account.any_instance.stubs(
              has_follower_and_email_cc_collaborations_enabled?: true,
              has_comment_email_ccs_allowed_enabled?: true,
              has_ticket_followers_allowed_enabled?: true
            )

            author_can(add_collaborator: true, add_follower: true, add_email_cc: true)
          else
            author_can(add_collaborator: false, only_create_ticket_comments: false)
          end
        end

        it "flags the ticket if the mail was authorized by encoded ID" do
          @state.stubs(:authorized?).returns(true)

          ensure_valid_operation

          @state.flags.must_include EventFlagType.OTHER_USER_UPDATE
          assert @state.flagged?
          refute @state.suspended?
        end
      end
    end
  end

  def author_can(**abilities)
    abilities.map { |ability, can| @state.author.abilities.expects(:can?).with(ability, @state.ticket || @state.closed_ticket).returns(can) }
  end

  def assert_suspended_ticket
    @state.ticket.class.must_equal SuspendedTicket
    assert @state.suspended?
  end

  def refute_suspended_ticket
    @state.ticket.class.must_equal Ticket
    refute @state.suspended?
  end

  describe "#save_author" do
    let(:state_author) { User.new(account: account, name: "State Author", email: "newuser@example.com") }

    it "saves a new author" do
      refute @state.author.persisted?
      execute
      refute @state.author.new_record?, @state.author.errors.full_messages.to_s
    end

    it "saves if locale has changed" do
      @state.author = users(:minimum_agent)
      @state.author.locale_id = 4
      assert @state.author.locale_id_changed?
      @state.author.expects(:save!)
      execute
    end

    it "does not save if the ticket is going to be suspended" do
      @state.stubs(:suspended?).returns(true)
      execute
      assert @state.author.new_record?
    end

    describe "if an error was detected when trying to save" do
      before do
        Zendesk::InboundMail::Adapters::AuthorAdapter.any_instance.expects(:save_with_instrumentation!).
          raises(ZendeskDatabaseSupport::MappedDatabaseExceptions::KeyConstraintViolation, 'test')
      end

      it "repairs the author to the existing User" do
        new_author = User.new(account: account, name: "NewUser", email: "newuser@example.com")
        new_author.save!
        execute
        assert_equal new_author.id, @state.author.id
      end

      it "updates the Author locale ID with the detected locale" do
        execute
        assert_equal_with_nil @state.author.locale_id, @state.locale_detected
      end
    end

    it "uses newly saved User as requester on a followup ticket" do
      @state.ticket = nil
      @state.stubs(:authorized?).returns(true)
      execute
      @state.ticket.requester.must_equal @state.author
    end
  end
end
