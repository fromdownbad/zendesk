require_relative "../../../../support/test_helper"
require_relative "../../../../support/reply_parser_test_helper"
require "mocha"

SingleCov.covered! uncovered: 3

describe Zendesk::InboundMail::Processors::ContentProcessor do
  extend ReplyParserTestHelper

  fixtures :tickets

  let(:instrument_delimiter) { false }
  let(:path) { "minimum_ticket.json" }
  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }
  let(:mail_processing_state) do
    Zendesk::InboundMail::MailParsingQueue::ProcessingState.new(
      content: {
        html_full: "<html><body><div>foo</div><div>bar</div></body></html>",
        plain_full: "foo bar",
        html_reply: "<div>foo</div>",
        plain_reply: "foo",
        quoted_html_reply: '<div class="zendesk_quoted">bar</div>',
        quoted_plain_reply: "> bar"
      }
    )
  end

  def execute
    @state.account.stubs(has_email_instrument_delimiter?: instrument_delimiter)
    Zendesk::InboundMail::Processors::ContentProcessor.execute(@state, @mail)
  end

  before do
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = accounts(:minimum)
    @mail = FixtureHelper::Mail.read(path)
    @mail.account = @state.account
    @mail.stubs(processing_state: mail_processing_state) unless mail_processing_state.nil?
  end

  describe 'with a ticket' do
    before { @state.ticket = tickets(:minimum_1) }

    it 'logs' do
      @mail.logger.expects(:info).with('Setting plain text content using parsed plain text reply')
      execute
    end

    it 'sets plain text from reply content' do
      execute
      assert_equal @state.plain_content, @mail.processing_state.plain_content
    end

    it 'does not set HTML content' do
      execute
      assert_nil @state.html_content
    end

    describe "REPLY_PARSER_TRUNCATED flag" do
      describe "with mail_no_delimiter enabled" do
        before { Account.any_instance.stubs(has_no_mail_delimiter_enabled?: true) }

        it "adds flag when message is a reply" do
          execute
          assert_includes(@state.flags, EventFlagType.REPLY_PARSER_TRUNCATED)
        end

        it "does not add a flag when the quoted content is the same as the reply" do
          @mail.processing_state.stubs(quoted_plain_content: "foo")
          execute
          refute_includes(@state.flags, EventFlagType.REPLY_PARSER_TRUNCATED)
        end

        it "does not add a flag when full message is used" do
          @mail.processing_state.stubs(plain_content: "")
          execute
          refute_includes(@state.flags, EventFlagType.REPLY_PARSER_TRUNCATED)
        end
      end

      it "does not add the flag when message is a reply" do
        execute
        refute_includes(@state.flags, EventFlagType.REPLY_PARSER_TRUNCATED)
      end
    end

    describe 'with rich content enabled' do
      before { @state.account.settings.stubs(rich_content_in_emails?: true) }

      it 'logs' do
        @mail.logger.expects(:info).at_least_once
        @mail.logger.expects(:info).with('Setting HTML content using parsed HTML reply')
        execute
      end

      it 'sets html from reply message content' do
        execute
        assert_equal @state.html_content, @mail.processing_state.html_content
      end

      describe 'with a message forwarded into the thread (Fwd)' do
        before { @mail.stubs(subject: 'Fwd: a forwarded message') }

        it 'logs' do
          @mail.logger.expects(:info).at_least_once
          @mail.logger.expects(:info).with('Using full content - suspected forward')
          execute
        end

        it 'sets html from full message contents' do
          execute
          assert_equal @state.html_content, @mail.processing_state.full_html_content
        end
      end

      describe "when message subject indicates a Danish/Norwegian forward" do
        let(:mail_processing_state) { nil }

        describe "and headers in message body are Finnish" do
          let(:path) { "replies/microsoft_exchange/finnish.json" }

          it "does not detect mail as forward nor set html from full message contents" do
            execute
            assert_not_equal @state.html_content, @mail.processing_state.full_html_content
          end
        end

        describe "and headers in message body are Norwegian" do
          let(:path) { "replies/microsoft_exchange/norwegian.json" }

          it "detects mail as forward and sets html from full message contents" do
            execute
            assert_equal @state.html_content, @mail.processing_state.full_html_content
          end
        end
      end

      describe 'when html reply is empty' do
        let(:mail_processing_state) do
          Zendesk::InboundMail::MailParsingQueue::ProcessingState.new(
              content: {
                  html_full: "<html><body><div>lala</div><div>bar</div></body></html>",
                  plain_full: "foo bar",
                  html_reply: "<div><br></div><div><br></div>",
                  plain_reply: "foo"
              }
            )
        end

        it 'falls back to the full html content' do
          execute
          assert_equal @state.html_content, @mail.processing_state.full_html_content
        end
      end

      describe 'when html reply has text' do
        let(:mail_processing_state) do
          Zendesk::InboundMail::MailParsingQueue::ProcessingState.new(
              content: {
                  html_full: "<html><body><div>foo</div><div>bar</div></body></html>",
                  plain_full: "foo bar",
                  html_reply: "<div><br></div><div><br>lalala</div>",
                  plain_reply: "foo"
              }
            )
        end

        it 'sets the html reply content' do
          execute
          assert_equal @state.html_content, @mail.processing_state.html_content
        end
      end

      describe 'when html reply has only img' do
        let(:mail_processing_state) do
          Zendesk::InboundMail::MailParsingQueue::ProcessingState.new(
              content: {
                  html_full: "<html><body><div>foo</div><div>bar</div></body></html>",
                  plain_full: "foo bar",
                  html_reply: "<div dir=\"ltr\"><div><img src=\"https://nsundarampod998.zendesk-staging.com/attachments/token/E6QQj9MZdUPUPGHeSHmEiGoJZ/?name=Screen+Shot+2020-03-16+at+1.04.38+PM.png\" alt=\"Screen Shot 2020-03-16 at 1.04.38 PM.png\" width=\"536\" height=\"267\"><br></div></div><br>",
                  plain_reply: "foo"
              }
            )
        end

        it 'sets the html reply content' do
          execute
          assert_equal @state.html_content, @mail.processing_state.html_content
        end
      end

      describe "when the html reply only contains an attachment" do
        let(:path) { "replies/attachment_only.json" }
        let(:mail_processing_state) { nil }

        it "sets a placeholder as the html content" do
          execute
          assert_equal ::I18n.t("txt.email.content_processor.no_content"), @state.html_content
        end

        describe_with_arturo_disabled :email_attachment_only_reply_fix do
          it "sets the full html content as html content" do
            execute
            assert_equal @mail.processing_state.full_html_content, @state.html_content
          end
        end
      end

      describe "when the plain reply only contains an attachment" do
        let(:path) { "replies/attachment_only.json" }
        let(:mail_processing_state) { nil }

        it "sets a placeholder as the plain content" do
          execute
          assert_equal ::I18n.t("txt.email.content_processor.no_content"), @state.plain_content
        end

        describe_with_arturo_disabled :email_attachment_only_reply_fix do
          it "sets the full plain content as plain content" do
            execute
            assert_equal @mail.processing_state.full_plain_content, @state.plain_content
          end
        end
      end

      describe "REPLY_PARSER_TRUNCATED flag" do
        describe "with mail_no_delimiter enabled" do
          before { Account.any_instance.stubs(has_no_mail_delimiter_enabled?: true) }

          it "adds flag when message is a reply" do
            execute
            assert_includes(@state.flags, EventFlagType.REPLY_PARSER_TRUNCATED)
          end

          it "does not add a flag when full message is used" do
            @mail.processing_state.stubs(html_content: "")
            execute
            refute_includes(@state.flags, EventFlagType.REPLY_PARSER_TRUNCATED)
          end

          it "does not add a flag when the quoted content is the same as the reply" do
            @mail.processing_state.stubs(quoted_html_content: "<div>foo</div>")
            execute
            refute_includes(@state.flags, EventFlagType.REPLY_PARSER_TRUNCATED)
          end
        end

        it "does not add the flag when message is a reply" do
          execute
          refute_includes(@state.flags, EventFlagType.REPLY_PARSER_TRUNCATED)
        end

        describe "when html_content and full_html_content are null" do
          before do
            @mail.processing_state.stubs(html_content: "")
            @mail.processing_state.stubs(full_html_content: "")
          end

          describe "when plain_content and quoted_plain_content are different" do
            before do
              @mail.processing_state.stubs(plain_content: "foo")
              @mail.processing_state.stubs(quoted_plain_content: "> bar")
              @mail.processing_state.stubs(full_plain_content: "foo bar")
            end

            it "adds flag if plain_content is present and is not same as quoted_plain_content" do
              execute
              assert_includes(@state.flags, EventFlagType.REPLY_PARSER_TRUNCATED)
              assert_equal(@state.plain_content, "foo")
              assert @state.html_content.nil?
            end
          end

          describe "when plain_content is null and full_plain_content is present" do
            before do
              @mail.processing_state.stubs(plain_content: "")
              @mail.processing_state.stubs(full_plain_content: "foo bar")
            end

            it "uses the full_plain_content and does not add flag" do
              execute
              refute_includes(@state.flags, EventFlagType.REPLY_PARSER_TRUNCATED)
              assert_equal(@state.plain_content, "foo bar")
              assert @state.html_content.nil?
            end
          end
        end
      end
    end
  end

  describe 'with a closed ticket' do
    before do
      @state.ticket = nil
      @state.closed_ticket = stub(new_record?: false)
    end

    it 'logs' do
      @mail.logger.expects(:info).with('Setting plain text content using parsed plain text reply')
      execute
    end

    it 'uses truncated plain content to set plain content' do
      execute
      assert_equal @state.plain_content, @mail.processing_state.plain_content
    end

    it 'does not set HTML content' do
      execute
      assert_nil @state.html_content
    end

    describe 'with rich content enabled' do
      before do
        @state.account.settings.stubs(rich_content_in_emails?: true)
      end

      it 'logs' do
        @mail.logger.expects(:info).at_least_once
        @mail.logger.expects(:info).with('Setting HTML content using parsed HTML reply')
        execute
      end

      it 'uses truncated html content to set html content' do
        execute
        assert_equal @state.html_content, @mail.processing_state.html_content
      end
    end
  end

  describe 'with new tickets' do
    it 'logs' do
      @mail.logger.expects(:info).with('Using full content - new ticket')
      execute
    end

    it 'uses full plain content to set plain content' do
      execute
      assert_equal @state.plain_content, @mail.processing_state.full_plain_content
    end

    it 'does not set HTML content' do
      execute
      assert_nil @state.html_content
    end

    describe 'with rich content enabled' do
      before do
        @state.account.settings.stubs(rich_content_in_emails?: true)
      end

      it 'logs' do
        @mail.logger.expects(:info).at_least_once
        @mail.logger.expects(:info).with('Setting HTML content using full HTML content')
        execute
      end

      it 'uses full html content to set html content' do
        execute
        assert_equal @state.html_content, @mail.processing_state.full_html_content
      end
    end
  end
end
