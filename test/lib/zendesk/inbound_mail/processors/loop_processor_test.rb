require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::LoopProcessor do
  fixtures :accounts, :recipient_addresses
  include MailTestHelper

  def execute
    Zendesk::InboundMail::Processors::LoopProcessor.execute(@state, @mail)
  end

  def assert_suspended(state = SuspensionType.LOOP)
    execute
    assert_equal state, @state.suspension
  end

  def assert_delivered
    execute
    assert_nil @state.suspension
  end

  def assert_reject
    assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
  end

  def stub_inbound_count(num)
    ActiveRecord::Relation.any_instance.expects(count: num)
  end

  let(:account) { accounts(:minimum) }

  before do
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = account
    @state.from = address('user@example.zendesk.com')
    @mail = FixtureHelper::Mail.read('minimum_ticket.json')
    @mail.stubs(account: account)
  end

  describe "with account that has disable_account_mail_rate_limit Arturo flag" do
    before do
      @normal_limit = 2
      @state.account.stubs(:loop_threshold).returns(@normal_limit)
      @state.account.stubs(:has_email_ignore_rate_limits?).returns(true)
    end

    it "doesn't hard reject or suspend" do
      ActiveRecord::Relation.any_instance.expects(:count).never
      assert_delivered
    end
  end

  describe "detecting address looping" do
    let(:mentor) { "mentor@example.zendesk.com" }
    let(:somebody_else) { "somebody_else@example.zendesk.com" }
    let(:zendesk_message_id) { "<JPVGRER3VJ_5806398f94e7a_9e763f8ebcacd32c1417a5_sprut@#{Zendesk::Configuration.fetch(:host)}>" }

    it "does not suspend if sender is different from recipient" do
      @mail.from = address(mentor)
      @mail.to = address(somebody_else)
      assert_delivered
    end

    it "logs when message-id looks like a notification from us" do
      @mail.to = address(somebody_else)
      @mail.from = address(mentor)
      @mail.message_id = zendesk_message_id
      @mail.logger.expects(:warn).once

      execute
    end

    it "does not log when X-Zendesk-From-Account-Id header is present" do
      @mail.to = address(somebody_else)
      @mail.from = address(mentor)
      @mail.message_id = zendesk_message_id
      @mail.headers["X-Zendesk-From-Account-Id"] = "bla"
      @mail.logger.expects(:warn).never

      execute
    end
  end

  describe "detecting excessive usage by email sender" do
    let(:normal_limit) { 2 }

    before do
      @normal_limit = normal_limit
      @state.account.stubs(:loop_threshold).returns(@normal_limit)
    end

    it "logs metrics and hard rejects when normal usage is greatly exceeded" do
      Zendesk::StatsD::Client.any_instance.expects(:histogram).with(:percent_exceeded, 300).once
      @mail.logger.expects(:info).with(regexp_matches(/Inbound mail rate limit/)).once
      stub_inbound_count(@normal_limit * 2 + 1)
      assert_reject
    end

    describe_with_arturo_disabled :email_log_inbound_mail_rate_limit do
      it "does not log metrics and hard rejects when normal usage is greatly exceeded" do
        Zendesk::StatsD::Client.any_instance.expects(:histogram).with(:percent_exceeded, 300).never
        @mail.logger.expects(:info).with(regexp_matches(/Inbound mail rate limit/)).never
        stub_inbound_count(@normal_limit * 2 + 1)
        assert_reject
      end
    end

    it "suspends when exceeding normal usage" do
      stub_inbound_count(@normal_limit + 1)
      assert_suspended
    end

    it "ignores when not exceeding normal usage" do
      stub_inbound_count(@normal_limit)
      assert_delivered
    end

    describe 'when rate limit is nearly exhausted' do
      let(:normal_limit) { 100 }

      it "logs metrics" do
        Zendesk::StatsD::Client.any_instance.expects(:histogram).with(:rate_limit_utilization, 88).once
        @mail.logger.expects(:info).with(regexp_matches(/Inbound mail rate limit nearly exhausted/)).once
        stub_inbound_count(88)
        assert_delivered
      end

      describe_with_arturo_disabled :email_log_inbound_mail_rate_limit do
        it "does not log metrics" do
          Zendesk::StatsD::Client.any_instance.expects(:histogram).with(:rate_limit_utilization, 88).never
          @mail.logger.expects(:info).with(regexp_matches(/Inbound mail rate limit nearly exhausted/)).never
          stub_inbound_count(88)
          assert_delivered
        end
      end
    end

    describe "from a known busy address" do
      before do
        busy_address = 'shiftmanager@jam.co.uk'
        @state.from.stubs(:address).returns(busy_address)
        @busy_limit = 120
        @mail.from  = address(busy_address)
      end

      it "does not suspend when exceeding normal usage" do
        stub_inbound_count(@normal_limit + 1)
        assert_delivered
      end

      it "suspends when exceeding busy usage" do
        stub_inbound_count(@busy_limit + 1)
        assert_suspended
      end

      it "is hard rejected even when whitelisted" do
        stub_inbound_count(@busy_limit * 2 + 1)
        assert_reject
      end
    end

    describe "from a sender whitelisted by the account" do
      before do
        @state.account.stubs(:domain_whitelist).returns(@state.from.address)
        @whitelist_limit = 200
      end

      it "only whitelist exact addresses" do
        @state.account.stubs(:domain_whitelist).returns(@state.from.domain)
        stub_inbound_count(@whitelist_limit - 1)
        assert_reject
      end

      describe_with_and_without_arturo_enabled :email_account_whitelist_multiplier do
        it "does not suspend when exceeding normal usage" do
          stub_inbound_count(@whitelist_limit - 1)
          assert_delivered
        end

        it "suspends when exceeding whitelist usage" do
          stub_inbound_count(@whitelist_limit + 1)
          assert_suspended
        end
      end
    end
  end

  describe "#reject_sender_is_same_account" do
    describe "when message ID matches known Zendesk outbound message ID patterns" do
      before { @mail.stubs(zendesk_message_id?: true) }

      it "rejects when From is a support address for the account receiving the email" do
        @mail.reply_to = nil
        @mail.from = address('support@minimum.zendesk-test.com')
        assert_reject
      end

      it "rejects when Reply-To is a support address for the account receiving the email" do
        @mail.reply_to = address('support@minimum.zendesk-test.com')
        assert_reject
      end

      it "does not reject when Reply-To is a support address of a different account" do
        @mail.reply_to = @mail.from = address('support@withgroups.zendesk-test.com')
        assert_delivered
      end
    end

    describe "when message ID does not match known Zendesk outbound message ID patterns" do
      before { @mail.stubs(zendesk_message_id?: false) }

      it "does not reject even if From is a support address for the account receiving the email" do
        @mail.reply_to = nil
        @mail.from = address('support@minimum.zendesk-test.com')
        assert_delivered
      end
    end
  end
end
