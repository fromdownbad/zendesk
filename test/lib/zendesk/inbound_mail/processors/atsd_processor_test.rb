require_relative "../../../../support/test_helper"

ENV['RSPAMD_API_ENDPOINT'] = 'http://127.0.0.1:9001'
ENV['RSPAMD_PASSWORD'] = 'GoodPassword'

SingleCov.covered! uncovered: 1

describe Zendesk::InboundMail::Processors::ATSDProcessor do
  include ArturoTestHelper
  include MailTestHelper

  fixtures :tickets, :events, :users

  def execute(state, mail)
    Zendesk::InboundMail::Processors::ATSDProcessor.execute(state, mail)
  end

  def does_not_get_suspended(state, mail)
    execute(state, mail)
    assert_nil state.suspension
  end

  def gets_suspended_by(state, mail, suspension_type)
    execute(state, mail)
    assert_equal suspension_type, state.suspension
  end

  def gets_hard_rejected(state, mail)
    assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute(state, mail) }
  end

  def does_not_get_hard_rejected(state, mail)
    execute(state, mail)
  end

  def does_not_execute_spam_detection(state, mail)
    Fraud::SpamDetector.any_instance.expects(:reject_ticket?).never
    Fraud::SpamDetector.any_instance.expects(:suspend_ticket?).never

    execute(state, mail)
  end

  let(:state) { Zendesk::InboundMail::ProcessingState.new }
  let(:mail)  { Zendesk::Mail::InboundMessage.new }
  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }

  before do
    account = accounts(:minimum)
    state.account = account
    state.from = address(users(:minimum_agent).email)
    state.recipient = "support@minimum.zendesk-test.com"
    state.ticket = tickets(:minimum_1)
  end

  describe_with_arturo_enabled :spam_detector_inbound_spam do
    describe 'with an existing ticket' do
      it { does_not_execute_spam_detection(state, mail) }
      it { does_not_get_suspended(state, mail) }
      it { does_not_get_hard_rejected(state, mail) }
    end

    describe 'with a closed ticket' do
      before { state.closed_ticket = tickets(:minimum_1) }

      it { does_not_execute_spam_detection(state, mail) }
      it { does_not_get_suspended(state, mail) }
      it { does_not_get_hard_rejected(state, mail) }
    end

    describe 'with a new ticket' do
      before do
        state.ticket.stubs(persisted?: false)
        state.ticket.audit = events(:create_audit_for_minimum_ticket_1)
        Zendesk::InboundMail::Ticketing::MailTicket.new(state, mail).add_metadata(state.ticket)
      end

      describe 'when not spam' do
        it { does_not_get_suspended(state, mail) }
        it { does_not_get_hard_rejected(state, mail) }
      end

      describe 'when detected as spam with high confidence' do
        before { Fraud::SpamDetector.any_instance.stubs(reject_ticket?: true) }

        it { gets_hard_rejected(state, mail) }

        describe 'with an existing SPAM suspension' do
          before do
            state.stubs(suspended?: true)
            state.suspension = SuspensionType.SPAM
          end

          it { does_not_execute_spam_detection(state, mail) }
        end
      end

      describe 'when detected as spam with lower confidence' do
        before do
          Fraud::SpamDetector.any_instance.stubs(reject_ticket?: false)
          Fraud::SpamDetector.any_instance.stubs(suspend_ticket?: true)
        end

        it { gets_suspended_by(state, mail, SuspensionType.MALICIOUS_PATTERN_DETECTED) }

        describe 'with potential metadata present on the mail object' do
          before do
            mail.cc = [
              Zendesk::Mail::Address.new(address: 'someone@example.com', name: 'Someone'),
              Zendesk::Mail::Address.new(address: 'someone.else@example.com', name: 'Someone Else')
            ]
            mail.message_id = 'abc123def456@zendesk-test.com'
            mail.mailer = 'Outlook'
          end

          it 'stores the metadata on the resulting suspended ticket' do
            execute(state, mail)

            assert_equal 2, state.ticket.metadata[:ccs].size
            assert_equal "Outlook", state.ticket.metadata[:client]
            assert_equal "abc123def456@zendesk-test.com", state.ticket.metadata[:message_id]
          end
        end

        describe 'training Rspamd based on a SpamDetector suspension result' do
          let(:raw_email_identifier) { '1/abc123def456.eml' }

          before do
            state.recipient = 'support@minimum.zendesk-test.com'
            state.ticket.stubs(original_raw_email_identifier: raw_email_identifier)
          end

          describe_with_arturo_enabled :email_train_rspamd_in_mtc do
            it 'enqueues an RspamdFeedbackJob to train spam' do
              RspamdFeedbackJob.expects(:report_spam).with(
                user: User.system,
                identifier: raw_email_identifier,
                recipient: state.recipient,
                classifier: Zendesk::InboundMail::RspamdTrain::RSPAMD_BAYES_CLASSIFIER_COMMON
              ).once

              execute(state, mail)
            end
          end

          describe_with_arturo_disabled :email_train_rspamd_in_mtc do
            it 'does not enqueue an RspamdFeedbackJob to train spam' do
              RspamdFeedbackJob.expects(:report_spam).never

              execute(state, mail)
            end
          end
        end
      end

      describe_with_arturo_disabled :spam_detector_inbound_spam do
        describe 'it does not execute spam detection' do
          it { does_not_execute_spam_detection(state, mail) }
        end
      end

      describe_with_arturo_disabled :email_atsd_processor do
        describe 'it does not execute spam detection' do
          it { does_not_execute_spam_detection(state, mail) }
        end
      end
    end
  end
end
