require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::LanguageHeaderProcessor do
  fixtures :translation_locales, :accounts, :remote_authentications, :users, :user_identities

  def execute
    Zendesk::InboundMail::Processors::UserProcessor.execute(@state, @mail)
    yield if block_given?
    Zendesk::InboundMail::Processors::LanguageHeaderProcessor.execute(@state, @mail)
  end

  before do
    @mail = FixtureHelper::Mail.read('minimum_ticket.json')
    @state = Zendesk::InboundMail::ProcessingState.new
    @account = @state.account = accounts(:minimum)
    @author = @state.account.owner
    @english = translation_locales(:english_by_zendesk)
    @brazilian = translation_locales(:brazilian_portuguese)
    @norwegian = translation_locales(:norwegian)
    Account.any_instance.stubs(:available_languages).returns([@english, @brazilian])
    @state.account.stubs(:is_open?).returns(true)
    @state.account.stubs(:is_signup_required?).returns(false)
    @mail.headers["X-Accept-Language"] = "pt"
  end

  describe "when the author is an existent user" do
    before { @state.from = Zendesk::Mail::Address.new(address: @author.email) }

    it "does not change the locale" do
      locale_id = translation_locales(:japanese).id
      @author.update_column(:locale_id, locale_id)
      execute
      @state.author.locale_id.must_equal locale_id
    end

    it "changes the locale when author did not have a locale" do
      @author.locale_id.must_be_nil
      execute
      @state.author.locale_id.wont_equal nil
    end
  end

  describe "when the author is a new user" do
    before { @state.from = Zendesk::Mail::Address.new(address: "new_user@example.com") }

    it "set locale if X-Accept-Language is an account available language" do
      execute
      assert_equal @brazilian.id, @state.author.locale_id
    end

    it "set locale if Accept-Language is an account available language" do
      @mail.headers.delete("X-Accept-Language")
      @mail.headers["Accept-Language"] = "pt"
      execute
      assert_equal @brazilian.id, @state.author.locale_id
    end

    it "prefer the first locale in the language header" do
      @mail.headers["X-Accept-Language"] = "pt-BR, en-US"
      execute
      assert_equal @brazilian.id, @state.author.locale_id
    end

    it "take Accept-Language in case the mail has both headers" do
      @mail.headers["X-Accept-Language"] = "en-US"
      @mail.headers["Accept-Language"] = "pt-BR"
      execute
      assert_equal @brazilian.id, @state.author.locale_id
    end

    it "set locale if the language header contains at least one of the  account available language" do
      @mail.headers["X-Accept-Language"] = "de-DE, de, en-us, en, pt, pt-BR"
      execute
      assert_equal @english.id, @state.author.locale_id
    end

    describe "When matching Norwegian languages" do
      before { Account.any_instance.stubs(:available_languages).returns([@norwegian, @english]) }

      it "accepts Bokmal locale and set language to Norwegian" do
        @mail.headers["X-Accept-Language"] = "nb-no, nb, en-us, en"
        execute
        assert_equal @norwegian.id, @state.author.locale_id
      end

      it "accepts Nynorsk locale and set language to Norwegian" do
        @mail.headers["X-Accept-Language"] = "nn-no, nn, en-us, en"
        execute
        assert_equal @norwegian.id, @state.author.locale_id
      end
    end

    it "does not set locale if the language header is not an account available language" do
      Account.any_instance.stubs(:available_languages).returns([@english])
      execute
      assert_nil @state.author.locale_id
    end

    it "support language headers with no space after the comma" do
      @mail.headers["X-Accept-Language"] = "en-US,fr-CA,fr-FR,fr-BE,fr-CH"
      execute
      assert_equal @english.id, @state.author.locale_id
    end

    it "support language headers with spaces between language and locale" do
      @mail.headers["X-Accept-Language"] = "en - us"
      execute
      assert_equal @english.id, @state.author.locale_id
    end

    ["|en-us|", "", "Something, very - strange"].each do |format|
      it "does not raise errors for headers with unknown format" do
        @mail.headers["X-Accept-Language"] = format
        execute
        @state.author.locale_id.must_be_nil
      end
    end

    it "does not set locale if author already has a locale" do
      execute do
        @state.author.locale_id = 4
        @state.author.expects(:translation_locale=).never
      end
    end
  end
end
