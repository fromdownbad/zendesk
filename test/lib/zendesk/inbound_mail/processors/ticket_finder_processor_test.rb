require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 13

describe Zendesk::InboundMail::Processors::TicketFinderProcessor do
  fixtures :all

  extend ArturoTestHelper
  include MailTestHelper

  let(:ticket_finder_processor) { Zendesk::InboundMail::Processors::TicketFinderProcessor }
  let(:account) { accounts(:minimum) }
  let(:default_host) { account.default_hosts.first }
  let(:state) { Zendesk::InboundMail::ProcessingState.new account }
  let(:requester) { users(:minimum_end_user) }
  let(:mail) do
    mail = Zendesk::Mail::InboundMessage.new
    mail.to      = address("support@#{default_host}")
    mail.from    = address(requester.email)
    mail.subject = 'email subject'
    mail.body    = 'email body'
    mail
  end
  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }
  let(:ticket) { tickets(:minimum_1) }
  let(:merged_ticket) { stub(id: '') }
  let(:audit) { stub(via_reference_id: '987') }

  before do
    merged_ticket.stubs(:closed?).returns(false)
    # TicketProcessingWorker normally would apply the account to the "mail"/InboundMessage object
    mail.account = account
  end

  describe '#execute' do
    let(:message_id_string) { 'reference' }
    let(:ticket_id) { 123 }
    let(:in_reply_to) { nil }
    let(:create_inbound_email) do
      account.inbound_emails.create! do |i|
        i.message_id  = message_id_string
        i.from        = requester.email
        i.in_reply_to = in_reply_to
        i.ticket_id   = ticket_id
      end
    end

    before do
      ticket.stubs(:closed?).returns(false)
    end

    describe 'finding a ticket by encrypted email token' do
      let(:message_id)  { ticket.generate_message_id }
      let(:reply_to_id) { ticket.generate_message_id(false) }
      let(:encoded_id)  { ticket.encoded_id.tr('-', '') }
      let(:location_tag) { 'message_ids' }
      let(:version) { 'encoded_id' }

      describe 'message_id in references' do
        before do
          mail.references = [message_id]
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }
        it { authorizes_state }
        it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
      end

      describe 'reply-to-id in references' do
        before do
          mail.references = [reply_to_id]
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }
        it { authorizes_state }
        it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
      end

      describe 'message_id in references with multiple message_ids' do
        before do
          mail.references = ["#{message_id} <somethingelse@example.com>"]
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }
        it { authorizes_state }
        it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
      end

      describe 'message_id in the in-reply-to' do
        before do
          mail.in_reply_to = message_id
          assert mail.references.blank?
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }
        it { authorizes_state }
        it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
      end

      describe 'does not find with nothing' do
        before do
          Ticket.expects(:find_by_encoded_id).never
          ticket_finder_processor.execute(state, mail)
        end

        it { does_not_find_ticket }
        it { does_not_authorize_state }
        it { does_not_include_tags_in_deferred_stats }
      end

      describe 'non-Zendesk message_id' do
        before do
          mail.references = [message_id.sub('zendesk', 'foobar')]
          Ticket.expects(:find_by_encoded_id).never
          ticket_finder_processor.execute(state, mail)
        end

        it { does_not_find_ticket }
        it { does_not_authorize_state }
        it { does_not_include_tags_in_deferred_stats }
      end

      describe 'multiple identical message_ids' do
        before do
          mail.references = [
            message_id.sub(encoded_id, 'XXXXXYYYYY'),
            reply_to_id.sub(encoded_id, 'XXXXXYYYYY')
          ]
          Ticket.expects(:find_by_encoded_id)
          ticket_finder_processor.execute(state, mail)
        end

        it { does_not_find_ticket }
        it { does_not_authorize_state }
        it { does_not_include_tags_in_deferred_stats }
      end

      describe 'message id is in a non-Zendesk format' do
        before do
          mail.in_reply_to = '<blah@example.com>'
          assert mail.references.blank?
          ticket_finder_processor.execute(state, mail)
        end

        it { does_not_find_ticket }
        it { does_not_authorize_state }
        it { does_not_include_tags_in_deferred_stats }
      end
    end

    describe 'finds by message ids' do
      let(:location_tag) { 'references' }
      let(:version) { 'inbound_email' }

      describe 'without message_ids' do
        before do
          mail.in_reply_to = nil
          mail.references = []
          ticket_finder_processor.execute(state, mail)
        end

        it { does_not_find_ticket }
        it { does_not_authorize_state }
        it { does_not_include_tags_in_deferred_stats }
      end

      describe 'blank message id in message ids' do
        before do
          create_inbound_email
          mail.references = ['reference', '']
          account.tickets.expects(:find_by_id).with(123).returns(ticket)
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }
        it { authorizes_state }
        it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
      end

      describe 'no matching tickets' do
        let(:ticket_id) { nil }

        before do
          create_inbound_email
          mail.references = ['reference']
          account.tickets.expects(:find_by_id).never
          ticket_finder_processor.execute(state, mail)
        end

        it { does_not_find_ticket }
        it { does_not_authorize_state }
        it { does_not_include_tags_in_deferred_stats }
      end

      describe 'message_id in reference header' do
        before do
          create_inbound_email
          account.tickets.expects(:find_by_id).with(123).returns(ticket)
        end

        describe 'matched-case reference' do
          before do
            mail.references = ['reference']
            ticket_finder_processor.execute(state, mail)
          end

          it { finds_ticket }
          it { authorizes_state }
          it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
        end

        describe 'mixed case reference' do
          before do
            mail.references = ['ReFeRenCE']
            ticket_finder_processor.execute(state, mail)
          end

          it { finds_ticket }
          it { authorizes_state }
          it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
        end
      end

      describe 'inbound mail exists where message_id in in_reply_to' do
        let(:message_id_string) { '<message_id>' }

        before do
          create_inbound_email
          account.tickets.expects(:find_by_id).with(123).returns(ticket)
        end

        describe 'matched-case message_id' do
          before do
            mail.in_reply_to = '<message_id>'
            ticket_finder_processor.execute(state, mail)
          end

          it { finds_ticket }
          it { authorizes_state }
          it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
        end

        describe 'mixed_case message_id' do
          before do
            mail.in_reply_to = '<Message_Id>'
            ticket_finder_processor.execute(state, mail)
          end

          it { finds_ticket }
          it { authorizes_state }
          it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
        end
      end

      describe 'prioritizes in_reply_to over reference' do
        let(:create_inbound_email) do
          account.inbound_emails.create! do |i|
            i.message_id  = 'in_reply_to_message_id'
            i.from        = requester.email
            i.ticket_id   = 114
          end

          account.inbound_emails.create! do |i|
            i.message_id  = 'reference'
            i.from        = requester.email
            i.ticket_id   = 113
          end
        end

        before do
          create_inbound_email
          mail.references  = ['reference']
          mail.in_reply_to = 'in_reply_to_message_id'
          account.tickets.expects(:find_by_id).with(114).returns(ticket)
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }
        it { authorizes_state }
        it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
      end

      describe 'match inbound mail with in_reply_to & from' do
        let(:message_id_string) { 'does not match' }

        describe 'a gmail address' do
          let(:in_reply_to) { 'test@gmail.com' }
          before do
            mail.in_reply_to = in_reply_to
            create_inbound_email
            account.tickets.expects(:find_by_id).with(123).returns(ticket)
            ticket_finder_processor.execute(state, mail)
          end

          it { finds_ticket }
          it { authorizes_state }
          it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
        end

        describe 'a zendesk address' do
          let(:in_reply_to) { 'test@zendesk.com' }

          before do
            mail.in_reply_to = in_reply_to
            create_inbound_email
            mail.stubs(:from_zendesk?).returns(true)
            account.tickets.expects(:find_by_id).with(123).returns(ticket)
            ticket_finder_processor.execute(state, mail)
          end

          it { finds_ticket }
          it { authorizes_state }
          it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
        end
      end

      describe 'without in_reply_to' do
        let(:in_reply_to) { nil }

        describe 'a gmail address' do
          before do
            create_inbound_email
            account.tickets.expects(:find_by_id).with(123).never
            ticket_finder_processor.execute(state, mail)
          end

          it { does_not_find_ticket }
          it { does_not_authorize_state }
          it { does_not_include_tags_in_deferred_stats }
        end

        describe 'a zendesk address' do
          before do
            mail.stubs(:from_zendesk?).returns(true)
            create_inbound_email
            account.tickets.expects(:find_by_id).with(123).never
            ticket_finder_processor.execute(state, mail)
          end

          it { does_not_find_ticket }
          it { does_not_authorize_state }
          it { does_not_include_tags_in_deferred_stats }
        end
      end
    end

    describe 'finding a ticket via encoded ID' do
      describe 'when the encoded ID is inside the email\'s body' do
        let(:encoded_id) { ticket.encoded_id }
        let(:encoded_id_without_hyphen) { ticket.encoded_id.delete('-') }
        let(:invalid_encoded_id) { ticket.encoded_id.next }
        let(:invalid_encoded_id_without_hyphen) { invalid_encoded_id.delete('-') }
        let(:version) { 'encoded_id' }
        let(:location_tag) { 'body' }

        describe 'when the encoded ID is found enclosed in square brackets' do
          let(:encoded_id_square_brackets_body) { "Hello! [#{encoded_id}] Thanks!" }

          ['text/html', 'text/plain'].each do |content_type|
            describe "when the encoded ID is found inside the email's #{content_type} part" do
              before do
                part = Zendesk::Mail::Part.new.tap do |new_part|
                  new_part.content_type = content_type
                  new_part.body = encoded_id_square_brackets_body
                end

                mail.parts = [part]

                ticket_finder_processor.execute(state, mail)
              end

              it { finds_ticket }
              it { authorizes_state }
              it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
              it { sets_the_id_location }
            end

            describe "when the encoded ID is found inside the email's body with content-type #{content_type}" do
              before do
                mail.content_type = content_type
                mail.body = encoded_id_square_brackets_body

                ticket_finder_processor.execute(state, mail)
              end

              it { finds_ticket }
              it { authorizes_state }
              it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
              it { sets_the_id_location }
            end
          end
        end

        describe 'when there are two encoded ID-like strings enclosed in square brackets, only the second of which is tied to an actual ticket' do
          let(:two_encoded_id_in_square_brackets_body) { "Hello! [#{invalid_encoded_id}] Thanks! [#{encoded_id}] " }

          ['text/html', 'text/plain'].each do |content_type|
            describe "when the encoded ID is found inside the email's #{content_type} part" do
              before do
                part = Zendesk::Mail::Part.new.tap do |new_part|
                  new_part.content_type = content_type
                  new_part.body = two_encoded_id_in_square_brackets_body
                end

                mail.parts = [part]

                ticket_finder_processor.execute(state, mail)
              end

              it { finds_ticket }
              it { authorizes_state }
              it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
              it { sets_the_id_location }
            end

            describe "when the encoded ID is found inside the email's body with content-type #{content_type}" do
              before do
                mail.content_type = content_type
                mail.body = two_encoded_id_in_square_brackets_body

                ticket_finder_processor.execute(state, mail)
              end

              it { finds_ticket }
              it { authorizes_state }
              it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
              it { sets_the_id_location }
            end
          end
        end
      end

      describe 'when the encoded ID is inside the email\'s recipient header(s) (To, CC, BCC)' do
        let(:extra_candidate) { 'Auto00-Name' }
        let(:version) { 'encoded_id' }

        before { assert_equal ticket.encoded_id.length, 11 }

        describe 'ID in the recipient address' do
          let(:location_tag) { 'recipients' }

          describe 'when the encoded ID does not match a real ticket in the database' do
            before do
              mail.to = address(
                mail.to.first.address.sub('@', "+id#{extra_candidate}@")
              )
              ticket_finder_processor.any_instance.expects(:find_ticket_by_recipient_nice_id).once
              ticket_finder_processor.execute(state, mail)
            end

            it { does_not_find_ticket }
            it { does_not_authorize_state }
            it { does_not_include_tags_in_deferred_stats }
          end

          describe 'when the encoded ID matches a real ticket in the database' do
            before do
              mail.to = address(
                mail.to.first.address.sub('@', "+id#{ticket.encoded_id}@")
              )
            end

            describe 'when the mail sender is an agent' do
              before { state.from = address("minimum_agent@aghassipour.com") }

              describe 'when the mail sender can edit the ticket' do
                before do
                  ticket_finder_processor.any_instance.expects(:find_ticket_by_recipient_nice_id).never
                  state.agent.stubs(:can?).with(:edit, ticket).returns(true)

                  ticket_finder_processor.execute(state, mail)
                end

                it { finds_ticket }
                it { authorizes_state }
                it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
              end

              describe 'when the mail sender cannot edit the ticket' do
                describe 'when the mail sender is a light agent' do
                  before do
                    state.agent.stubs(:is_light_agent?).returns(true)
                    state.agent.stubs(:can?).with(:edit, ticket).returns(false)
                  end

                  describe 'when the mail sender can view the ticket' do
                    before do
                      ticket_finder_processor.any_instance.expects(:find_ticket_by_recipient_nice_id).never
                      state.agent.stubs(:can?).with(:view, ticket).returns(true)

                      ticket_finder_processor.execute(state, mail)
                    end

                    it { finds_ticket }
                    it { authorizes_state }
                    it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
                  end

                  describe 'when the mail sender cannot view the ticket' do
                    before do
                      ticket_finder_processor.any_instance.expects(:find_ticket_by_recipient_nice_id).once
                      records_stats_for_ignored_recipient_encoded_id_match_ticket("light_agent")
                      state.agent.stubs(:can?).with(:view, ticket).returns(false)

                      ticket_finder_processor.execute(state, mail)
                    end

                    it { does_not_find_ticket }
                    it { does_not_authorize_state }
                    it { does_not_include_tags_in_deferred_stats }
                  end
                end

                describe 'when the mail sender is not a light agent' do
                  before do
                    ticket_finder_processor.any_instance.expects(:find_ticket_by_recipient_nice_id).once
                    records_stats_for_ignored_recipient_encoded_id_match_ticket("agent")
                    state.agent.stubs(:can?).with(:edit, ticket).returns(false)

                    ticket_finder_processor.execute(state, mail)
                  end

                  it { does_not_find_ticket }
                  it { does_not_authorize_state }
                  it { does_not_include_tags_in_deferred_stats }
                end
              end
            end

            describe 'when the mail sender is not an agent' do
              before do
                state.from = address("minimum_end_user@aghassipour.com")
                records_stats_for_ignored_recipient_encoded_id_match_ticket("end_user")
                ticket_finder_processor.any_instance.expects(:find_ticket_by_recipient_nice_id).once

                ticket_finder_processor.execute(state, mail)
              end

              it { does_not_find_ticket }
              it { does_not_authorize_state }
              it { does_not_include_tags_in_deferred_stats }
            end
          end
        end

        describe 'when the encoded ID is inside the email\'s Subject header' do
          let(:location_tag) { 'subject' }

          describe 'with a single candidate' do
            before do
              mail.subject = mail.subject + " [#{ticket.encoded_id}]"
              state.account.tickets.expects(:find_by_encoded_id).
                with(ticket.encoded_id, logger: mail.logger).once.returns(ticket)
              state.account.tickets.expects(:find_by_nice_id).never
              ticket_finder_processor.execute(state, mail)
            end

            it { finds_ticket }
            it { authorizes_state }
            it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
          end

          describe 'with multiple candidates' do
            before do
              mail.subject = "[#{extra_candidate}]" + mail.subject + " [#{ticket.encoded_id}]"

              state.account.tickets.expects(:find_by_encoded_id).
                with(extra_candidate, logger: mail.logger).once
              state.account.tickets.expects(:find_by_encoded_id).
                with(ticket.encoded_id, logger: mail.logger).once.returns(ticket)
              state.account.tickets.expects(:find_by_nice_id).never
              ticket_finder_processor.execute(state, mail)
            end

            it { finds_ticket }
            it { authorizes_state }
            it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
          end

          describe 'when the id begins with a number' do
            let(:encoded_id) do
              (1..100).each do |i|
                id = Ticket::IdMasking.mask(i)
                break id if id =~ /^\d/
              end
            end

            before do
              ticket.stubs(:encoded_id).returns(encoded_id)
              mail.subject = mail.subject + " [#{ticket.encoded_id}]"
              state.account.tickets.expects(:find_by_encoded_id).
                with(ticket.encoded_id, logger: mail.logger).once.returns(ticket)
              state.account.tickets.expects(:find_by_nice_id).never
              ticket_finder_processor.execute(state, mail)
            end

            it { finds_ticket }
            it { authorizes_state }
            it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
          end
        end
      end
    end

    describe '#find_ticket_by_recipient_nice_id (in address)' do
      let(:location_tag) { 'recipients' }
      let(:version) { 'nice_id' }

      describe 'correct nice_id' do
        before do
          mail.to = address(mail.to.first.address.sub('@', '+id1234@'))
          state.account.tickets.expects(:find_by_nice_id).
            with('1234').once.returns(ticket)
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }
        it { does_not_authorize_state }
        it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
        it { assert_nil state.suspension }
      end

      describe 'with incorrect id in to' do
        before do
          mail.to = address('foo+id1234@other.zendesk-test.com')
          state.account.tickets.expects(:find_by_nice_id).never
          ticket_finder_processor.execute(state, mail)
        end

        it { does_not_find_ticket }
        it { does_not_authorize_state }
      end

      describe 'finds with correct_id in cc after incorrect_id' do
        before do
          mail.to = address('foo+id1234@other.zendesk-test.com')
          mail.cc = [address("foo+id1222@#{state.account.default_host}")]
          state.account.tickets.expects(:find_by_nice_id).
            with('1222').once.returns(ticket)
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }
        it { does_not_authorize_state }
        it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }
        it { assert_nil state.suspension }
      end

      describe 'when sender address' do
        before do
          mail.to = [address("foo+id1222@#{state.account.default_host}")]
          mail.from = address(requester.email)
        end

        describe 'is a different case to the envelope address' do
          before do
            mail.headers['Envelope-From'] = requester.email.upcase
            state.account.tickets.expects(:find_by_nice_id).
              with('1222').once.returns(ticket)
            ticket_finder_processor.execute(state, mail)
          end

          it { finds_ticket }
          it { assert_nil state.suspension }
        end

        describe 'is different from envelope-from address' do
          before do
            mail.headers['Envelope-From'] = '<other@example.com>'
            state.account.tickets.expects(:find_by_nice_id).
              with('1222').once.returns(ticket)
          end

          it 'suspends a possible forgery' do
            ticket_finder_processor.execute(state, mail)
            assert_equal SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE, state.suspension
          end

          describe_with_arturo_disabled :email_suspend_recipient_nice_id_forgery do
            before { ticket_finder_processor.execute(state, mail) }

            it { assert_nil state.suspension }
          end
        end

        describe 'is different from return-path address' do
          before do
            mail.headers['Return-Path'] = '<other@example.com>'
            state.account.tickets.expects(:find_by_nice_id).
              with('1222').once.returns(ticket)
          end

          it 'suspends a possible forgery' do
            ticket_finder_processor.execute(state, mail)
            assert_equal SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE, state.suspension
          end

          describe_with_arturo_disabled :email_suspend_recipient_nice_id_forgery do
            before { ticket_finder_processor.execute(state, mail) }

            it { assert_nil state.suspension }
          end
        end
      end
    end

    describe '#find_ticket_by_body_nice_id' do
      let(:with_body_nice_id) { "#id #{ticket.nice_id}\n foobar baz" }
      let(:location_tag) { 'body' }
      let(:version) { 'nice_id' }

      before do
        mail.content_type = 'text/html'
        state.from = mail.from
        account.tickets.stubs(:find_by_nice_id).
          with(ticket.nice_id.to_s).returns(ticket)
      end

      describe 'nice_id in body' do
        before do
          mail.body = with_body_nice_id
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }
        it { does_not_authorize_state }
        it { includes_ticket_threading_tags_in_deferred_stats(location_tag, version) }

        it 'removes body nice_id' do
          assert_equal 'foobar baz', mail.content.text
        end
      end

      describe 'when part of multiple commands' do
        before do
          mail.body = "#foobar sdfsfd\n#id #{ticket.nice_id}\nthis junk to my email !?"
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }
      end

      describe 'when leading whitespace' do
        before do
          mail.body = "   \n\n\t\n" + with_body_nice_id
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }
      end

      describe 'when agent is being spoofed' do
        before do
          mail.body  = with_body_nice_id
          state.from = address(users(:minimum_agent).email)
          ticket_finder_processor.execute(state, mail)
        end

        it { does_not_find_ticket }

        it 'does not change content' do
          assert_equal with_body_nice_id, mail.content.text # show user that it was not processed
        end
      end

      describe 'nice_id not at start of body' do
        before do
          mail.body = "why did someone add\n#id #{ticket.nice_id}\nthis junk to my email !?"
        end

        it { does_not_find_ticket }
      end

      describe 'when not a nice_id' do
        before do
          mail.body = "#id #{ticket.nice_id} this junk to my email !?"
          ticket_finder_processor.execute(state, mail)
        end

        it { does_not_find_ticket }
      end
    end

    describe 'throttling ticket updates' do
      before do
        ticket_finder_processor.any_instance.stubs(:throttle_enabled?).returns(true)
        state.from = mail.reply_to
      end

      describe 'already suspended' do
        before { state.stubs(:suspended?).returns(true) }

        it 'does not attempt to throttle' do
          ticket_finder_processor.any_instance.expects(:throttle).never
          ticket_finder_processor.execute(state, mail)
        end
      end

      describe 'when no ticket is found' do
        before { state.stubs(:ticket).returns(nil) }

        it 'does not attempt to throttle' do
          ticket_finder_processor.any_instance.expects(:throttle).never
          ticket_finder_processor.execute(state, mail)
        end
      end

      describe "throttles the sender's email address per ticket" do
        before do
          state.stubs(:ticket).returns(ticket)
          9.times do
            Prop.throttle!(
              :email_sender_ticket_updates,
              "#{state.from.address}:#{state.ticket.id}"
            )
          end
          ticket_finder_processor.execute(state, mail)
        end

        it 'throttles and suspends' do
          assert_nil state.suspension
          ticket_finder_processor.execute(state, mail)
          assert_equal SuspensionType.LOOP, state.suspension
        end
      end
    end

    describe '#handle_closed_ticket' do
      describe 'with an archived ticket' do
        before do
          ticket.stubs(:closed?).returns(true)
          ticket.stubs(:archived?).returns(true)
          ticket_finder_processor.any_instance.expects(ticket_for_update: ticket)

          expects_chain(ticket, :audits, :last).returns(audit)
          audit.expects(:via?).with(:merge).returns(false)

          ticket_finder_processor.execute(state, mail)
        end

        it 'references the closed ticket' do
          assert state.closed_ticket.present?
        end
      end

      describe 'with a non-archived ticket' do
        before do
          ticket.stubs(:closed?).returns(true)
          ticket_finder_processor.any_instance.expects(ticket_for_update: ticket)
          expects_chain(ticket, :audits, :latest).returns(audit)
        end

        describe 'merged' do
          before do
            audit.expects(:via?).with(:merge).returns(true)
            Ticket.expects(:find_by_id).with(audit.via_reference_id).returns(merged_ticket)
          end

          describe 'the open merged ticket' do
            before do
              merged_ticket.expects(:closed?).returns(false)
              ticket_finder_processor.execute(state, mail)
            end

            it 'provides the open merged ticket' do
              assert_equal merged_ticket, state.ticket
            end
          end

          describe 'merged but closed tickets' do
            before do
              merged_ticket.expects(:closed?).returns(true)
              ticket_finder_processor.execute(state, mail)
            end

            it { does_not_find_ticket }
          end
        end

        describe 'closed ticket is found' do
          before do
            audit.expects(:via?).with(:merge).returns(false)
            ticket_finder_processor.execute(state, mail)
          end

          it 'references the closed ticket' do
            assert state.closed_ticket.present?
          end
        end
      end

      describe "with a closed ticket" do
        before do
          mail.to[0].address.sub!(/@/, "+id1234@")
          account.tickets.stubs(:find_by_nice_id).returns(ticket)
          ticket.stubs(:closed?).returns(true)
          ticket.stubs(:audits).returns(stub(latest: audit))
        end

        describe 'ticket has not been merged' do
          before do
            audit.expects(:via?).with(:merge).returns(false)
            ticket_finder_processor.execute(state, mail)
          end

          it 'assigns ticket to state.closed_ticket' do
            assert_equal ticket, state.closed_ticket
          end

          it { does_not_find_ticket }
        end

        describe 'that has been merged' do
          before do
            audit.expects(:via?).with(:merge).returns(true)
            Ticket.expects(:find_by_id).returns(merged_ticket)
            ticket_finder_processor.execute(state, mail)
          end

          it "assigns state.ticket" do
            assert_equal merged_ticket, state.ticket
          end

          it "does not assign state.closed_ticket" do
            assert_nil state.closed_ticket
          end
        end

        describe "and a suspended author" do
          before do
            ticket.requester.expects(:suspended?).returns(true)
            ticket_finder_processor.execute(state, mail)
          end

          it { does_not_find_ticket }

          it 'does not assign state.closed_ticket' do
            assert_nil state.closed_ticket
          end
        end
      end
    end

    describe "forgery protection" do
      before { mail.to[0].address.sub!(/@/, "+id#{ticket.nice_id}@") }

      describe 'emails from end users' do
        before do
          state.from = address(requester.email)
          account.tickets.expects(:find_by_nice_id).
            with(ticket.nice_id.to_s).once.returns(ticket)
          ticket_finder_processor.execute(state, mail)
        end

        it { finds_ticket }

        it 'does not suspend emails' do
          assert_nil state.suspension
        end
      end

      describe 'with emails from agents' do
        before do
          state.from = address(users(:minimum_agent).email)
        end

        describe 'when the ticket is only findable by its nice id' do
          before do
            account.tickets.expects(:find_by_nice_id).
              with(ticket.nice_id.to_s).once.returns(ticket)
            ticket_finder_processor.execute(state, mail)
          end

          it { finds_ticket }

          it 'suspends as a possible forgery' do
            assert_equal SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE, state.suspension
          end
        end

        describe 'when the ticket is a new ticket' do
          before do
            mail.to[0].address.sub!(/@/, "+id007@")
            ticket_finder_processor.execute(state, mail)
          end

          it 'does not suspend email' do
            assert_nil state.suspension
          end

          it { does_not_find_ticket }
        end
      end
    end
  end

  def finds_ticket
    assert_equal ticket, state.ticket
  end

  def does_not_find_ticket
    assert_nil state.ticket
  end

  def authorizes_state
    assert state.authorized?
  end

  def does_not_authorize_state
    refute state.authorized?
  end

  def includes_ticket_threading_tags_in_deferred_stats(location, version)
    assert_equal dstat(location, version), state.deferred_stats
  end

  def does_not_include_tags_in_deferred_stats
    assert_empty state.deferred_stats
  end

  def dstat(location, version)
    {
      'ticket.threading' => {
        tags: ["method:#{version}", "location:#{location}"]
      }
    }
  end

  def sets_the_id_location
    assert_equal location_tag.to_sym, state.id_location
  end

  def records_stats_for_ignored_recipient_encoded_id_match_ticket(sender_role)
    Zendesk::StatsD::Client.any_instance.expects(:increment).with("ticket.ignore_recipient_encoded_id_matched_ticket", tags: ["sender_role:#{sender_role}"]).once
  end

  def records_stats_for_recipient_nice_id(user_type)
    Zendesk::StatsD::Client.any_instance.expects(:increment).with("ticket.recipient_nice_id_threading", tags: ["user_type:#{user_type}"]).once
  end
end
