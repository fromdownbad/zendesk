require_relative "../../../../support/test_helper"
require_relative '../../../../support/rule'

SingleCov.covered! uncovered: 5

describe Zendesk::InboundMail::Processors::CommitProcessor do
  include TestSupport::Rule::Helper

  fixtures :users, :accounts, :tickets, :remote_authentications, :subscriptions, :groups, :memberships,
    :user_identities, :sequences, :ticket_fields, :organizations

  def execute
    Zendesk::InboundMail::Processors::CommitProcessor.execute(@state, @mail)
  end

  let(:ticket_threading_key)  { "ticket.threading" }
  let(:ticket_threading_tags) { { tags: %w[method:encoded_id location:message_ids] } }

  before do
    @state         = Zendesk::InboundMail::ProcessingState.new
    @mail          = Zendesk::Mail::InboundMessage.new
    @statsd_client = Zendesk::InboundMail::StatsD.client
    @state.record_deferred_stat(ticket_threading_key, ticket_threading_tags)
  end

  describe "suspended ticket" do
    before do
      @state.ticket = SuspendedTicket.new(
        from_name: "Johnny Appleseed",
        from_mail: "iloveapples@example.com",
        content: "An apple a day...",
        cause: SuspensionType.UNKNOWN_AUTHOR
      )
      @state.account        = accounts(:minimum)
      @state.ticket.account = @state.account
      @state.stubs(:suspended?).returns(true)
    end

    it "does not increment ticket commit stats" do
      @statsd_client.expects(:increment).never
      execute
    end

    it "does not assign ticket current_user" do
      @state.ticket.expects(:current_user=).never
      execute
    end

    it "raises an exception if the ticket is invalid" do
      @state.ticket.stubs(:valid?).returns(false)
      assert_raise(ActiveRecord::RecordInvalid) { execute }
    end

    it "saves the ticket" do
      execute
      refute @state.ticket.new_record?
    end

    it "updates account views" do
      @state.account.expects(:expire_scoped_cache_key).with(:views).at_least_once
      execute
    end

    describe "when untrusted? => true" do
      before do
        @state.stubs(:suspended?).returns(false)
      end

      it "saves the ticket" do
        execute
        refute @state.ticket.new_record?
      end
    end

    describe "when flagged" do
      before do
        @flags_options = {
          message: {
            file: "big_file.txt",
            account_limit: 4096
          }
        }
        @state.add_flag(EventFlagType.ATTACHMENT_TOO_BIG, @flags_options)
      end

      it "captures multiple flags" do
        @state.add_flag(EventFlagType.OTHER_USER_UPDATE)
        execute
        @state.ticket.flags.must_equal AuditFlags.new([EventFlagType.ATTACHMENT_TOO_BIG, EventFlagType.OTHER_USER_UPDATE])
      end

      it "captures a flag" do
        execute
        @state.ticket.flags.must_equal AuditFlags.new([EventFlagType.ATTACHMENT_TOO_BIG])
      end

      it "captures flags_options" do
        execute
        @state.ticket.flags_options[EventFlagType.ATTACHMENT_TOO_BIG].must_equal @flags_options
      end
    end
  end

  describe "ticket" do
    before do
      @state.account = accounts(:minimum)
      @state.author  = users(:minimum_agent)
      @state.ticket  = @state.account.tickets.new(ticket_attrs)
      @state.ticket.audits.clear
      @state.ticket.will_be_saved_by(@state.author)
      # Ensure validations run correctly. This is normally set in TicketCreator processor.
      @state.ticket.via_id = ViaType.Mail
    end

    def ticket_attrs(options = {})
      tickets(:minimum_1).attributes.symbolize_keys.
        update(options).
        except(:account_id,
          :nice_id,
          :latest_recipients,
          :email_ccs,
          :followers,
          *Ticket.ignored_columns)
    end

    it "adds a redaction event when redacted" do
      @mail.expects(:redacted?).returns(true)
      execute
      comment         = @state.ticket.comments.last
      redaction_event = comment.redaction_event
      assert redaction_event
    end

    it "does not add a redaction event when not redacted" do
      @mail.expects(:redacted?).returns(false)
      execute
      comment         = @state.ticket.comments.last
      redaction_event = comment.redaction_event
      refute redaction_event
    end

    it "adds the redaction tag when redacted" do
      @mail.expects(:redacted?).returns(true)
      execute
      @state.ticket.tag_array.must_include "system_credit_card_redaction"
    end

    it "does not add the redaction tag when not redacted" do
      execute
      @state.ticket.tag_array.wont_include "system_credit_card_redaction"
    end

    it "does not choke when missing comments" do
      @state.ticket.expects(:comments).times(5).returns([])
      # Sequence#next will record exception, but CommitProcessor should not
      ZendeskExceptions::Logger.expects(:record).with do |exception, params|
        assert_equal(Sequence::SequenceTransactionError, exception.class)
        assert_equal("Sequence#next called within transaction", params[:message])
      end
      execute
    end

    it "has the author as current_user" do
      assert @state.author
      execute

      assert_equal @state.ticket.current_user, @state.author
    end

    describe "when group_id is invalid" do
      before do
        @state.account = accounts(:with_groups)
        @state.ticket.group_id = 999
      end

      describe_with_arturo_enabled :email_check_invalid_group do
        it "logs the commit error details" do
          @mail.logger.stubs(:error)
          @mail.logger.expects(:error).with(regexp_matches(/^Ticket is invalid.*/)).once
          @mail.logger.expects(:error).with(regexp_matches(/^Invalid group #999.*/)).once

          assert_raise(ActiveRecord::RecordInvalid) { execute }
        end

        it "adds 'group' to the ticket's invalid reasons" do
          @statsd_client.stubs(:increment)
          @statsd_client.expects(:increment).with('ticket.invalid', tags: ['detail:group']).once

          assert_raise(ActiveRecord::RecordInvalid) { execute }
        end
      end
    end

    describe "is invalid" do
      before { @state.ticket.stubs(:valid?).returns(false) }

      it "raises an exception if ticket is invalid" do
        assert_raise(ActiveRecord::RecordInvalid) { execute }
      end

      describe "statsd" do
        describe "with invalid author" do
          it "increments statsd ticket.invalid (with tags: author) stat" do
            @state.author.stubs(:new_record?).returns(true)
            @state.author.stubs(:valid?).returns(false)
            @statsd_client.expects(:increment).with("ticket.invalid", tags: ["detail:author"]).once
            assert_raises(ActiveRecord::RecordInvalid) { execute }
          end
        end

        describe "with organization errors" do
          before { @state.ticket.errors[:organization_id] = "error" }

          describe "when requester orgs are invalid" do
            it "increments statsd ticket.invalid (with tags: requester_orgs) stat" do
              @statsd_client.expects(:increment).with("ticket.invalid", tags: ["detail:requester_orgs"]).once
              assert_raises(ActiveRecord::RecordInvalid) { execute }
            end

            it "logs requester errors" do
              @mail.logger.expects(:error).with(regexp_matches(/^Ticket is invalid.*/)).twice
              @mail.logger.expects(:error).with(regexp_matches(/^requester orgs:.*/)).once
              assert_raises(ActiveRecord::RecordInvalid) { execute }
            end
          end

          describe "when requester or memberships are invalid" do
            it "increments statsd ticket.invalid (with tags: requester_or_memberships) stat" do
              @state.ticket.requester.stubs(:organization_memberships).returns(nil)
              @statsd_client.expects(:increment).with("ticket.invalid", tags: ["detail:requester_or_memberships"]).once
              assert_raises(ActiveRecord::RecordInvalid) { execute }
            end
          end
        end

        describe "when the ticket is invalid for a reason not explicitly described in the code" do
          it "increments statsd ticket.invalid (with tags: unknown) stat" do
            @statsd_client.expects(:increment).with("ticket.invalid", tags: ["detail:unknown"]).once
            assert_raises(ActiveRecord::RecordInvalid) { execute }
          end
        end
      end
    end

    it "refreshes account views" do
      Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:expire_view_caches)
      execute
    end

    should_eventually "rollback ticket save if an error occurs during the commit" do
      @state.ticket.expects(:properties=).raises(ActiveRecord::RecordInvalid)
      refute_difference "Ticket.count(:all)" do
        assert_raise(ActiveRecord::RecordInvalid) { execute }
      end
    end

    it "saves with ViaType.MAIL audit" do
      execute
      assert @state.ticket.via?(:mail)
    end

    it "saves valid tickets" do
      assert @state.ticket.valid?, @state.ticket.errors.full_messages.to_s
      assert_difference "Ticket.count(:all)" do
        execute
      end
    end

    it "sets ticket's application_logger to the mail logger" do
      Ticket.any_instance.expects(:application_logger=).at_least_once
      execute
    end

    it "resets the application_logger to nil after processor execution" do
      execute
      assert_nil @state.ticket.instance_variable_get(:@application_logger)
    end

    describe "save fails" do
      before { @state.ticket.expects(:save!).raises(ArgumentError) }

      it "records and raises an exception when ticket save fails" do
        assert @state.ticket.valid?
        assert_raise(ArgumentError) { execute }
      end

      it "increments statsd ticket.error stat when ticket save fails" do
        @statsd_client.expects(:increment).with("ticket.error", tags: ["detail:ArgumentError", "source:commit_ticket"]).once
        assert_raise(ArgumentError) { execute }
      end

      it "deletes newly saved users" do
        saved_user = users(:minimum_end_user2)
        @state.add_saved_new_users(saved_user)
        assert saved_user.is_active
        assert_raise(ArgumentError) { execute }
        refute saved_user.is_active
      end

      it "skips nil objects in the saved users array" do
        @state.saved_new_users = [nil]

        # would be a NoMethodError if nil was not being skipped
        assert_raise(ArgumentError) { execute }
      end

      it "resets the application_logger to nil after processor execution" do
        assert_raise(ArgumentError) { execute }
        assert_nil @state.ticket.instance_variable_get(:@application_logger)
      end
    end

    describe "#agent_comment" do
      let(:comments) { @state.ticket.reload.audits.map { |a| a.events.select { |x| x.is_a?(Comment) }.map { |c| [c.body, c.is_public?] } } }
      before { @state.agent_comment = "A private comment" }

      it "adds it a separate audit to not confuse lotus/apis" do
        execute
        assert_equal [[["minimum 1 ticket", true]], [["A private comment", false]]], comments
      end

      it "adds a public comment when #public was used" do
        @state.properties = { is_public: true }
        execute
        assert_equal [[["minimum 1 ticket", true]], [["A private comment", true]]], comments
      end

      it "does not add a comment to a closed ticket...zd946133" do
        Ticket.any_instance.stubs(:status_id).returns(StatusType.CLOSED)
        Zendesk::InboundMail::Ticketing::MailTicket.expects(:create_agent_comment_on_ticket).with(any_parameters).never
        execute
      end

      it "does add an agent comment on email forwarding when agent has restrictions" do
        @state.agent_forwarded = true
        @state.author = users(:minimum_end_user)
        @state.submitter = users(:with_groups_agent_assigned_restricted)
        execute
      end
    end

    describe "authors" do
      before do
        author = @state.account.users.new
        author.email = "author@example.com"
        author.name  = "Frederick Pants"

        @state.author = author
      end

      it "saves if new record" do
        execute
        refute @state.author.new_record?, @state.author.errors.full_messages.to_s
      end

      it "saves if locale has changed" do
        @state.author = users(:minimum_agent)
        @state.author.locale_id = 4
        assert @state.author.locale_id_changed?
        @state.author.expects(:save!)
        execute
      end

      it "raises an exception when invalid" do
        @state.author.stubs(:valid?).returns(false)
        assert_raise(ActiveRecord::RecordInvalid) { execute }
      end

      it "is not required" do
        @state.author = nil
        assert_difference "Ticket.count(:all)" do
          execute
        end
      end
    end

    describe "properties" do
      before do
        @state.properties = { status_id: StatusType.PENDING }
        @ticket           = @state.ticket
        @author           = @state.author
      end

      it "is set when the author is an agent" do
        @author.stubs(:is_agent?).returns(true)
        execute
        assert_equal StatusType.PENDING, @ticket.status_id
      end

      it "is not set when author isn't an agent" do
        @author.stubs(:is_agent?).returns(false)
        execute
        assert_not_equal StatusType.PENDING, @ticket.status_id
      end

      it "is not set when author is a light agent" do
        @author.stubs(:is_agent?).returns(true)
        @author.stubs(:is_light_agent?).returns(true)
        execute
        assert_not_equal StatusType.PENDING, @ticket.status_id
      end

      it "does not choke on empty properties" do
        @state.properties = nil
        execute
      end

      it "rolls back attempts to solve invalid tickets" do
        @state.ticket.assignee = nil
        @state.properties = { status_id: StatusType.Solved }
        @mail.logger.expects(:error).never
        execute
      end

      describe "when a TextAPI command is present" do
        before { Zendesk::InboundMail::Processors::CommitProcessor.any_instance.stubs(:log) }

        describe "#requester command" do
          describe "when account has organization and requester tags enabled" do
            before do
              Account.any_instance.stubs(:has_user_and_organization_fields?).returns(true)
              Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
              Account.any_instance.stubs(:has_tickets_inherit_requester_tags?).returns(true)
            end

            describe "and organization tags are present for original requester and mail API requester " do
              before do
                @original_requester = @state.ticket.requester
                # Set the requester's organization and organization tags
                @state.ticket.requester.organization = organizations(:minimum_organization3)
                @state.ticket.requester.organization.tags = "hello here"
                @state.ticket.requester.organization.save!
                @state.ticket.requester.save!
                # Set the agent's organization and organization tags
                @agent = users(:minimum_admin_not_owner)
                @agent.organization = organizations(:minimum_organization2)
                @agent.organization.tags = "bye there"
                @agent.organization.save!
                @agent.save!
                # Ensure the agent does not have any configured tags
                assert_empty @agent.tags
              end

              it "applies tags from the the mail API requester organization to the ticket" do
                @state.properties = { requester_email: @agent.email }
                execute
                assert_equal @agent.organization.tags.join(" "), @state.ticket.current_tags
              end
            end
          end
        end

        describe "#tags command" do
          describe "when a trigger adds additional tags" do
            before do
              conditions = build_definition_item(field: "status_id", operator: "less_than", value: StatusType.SOLVED)
              definition = build_trigger_definition(
                conditions_all: [conditions],
                conditions_any: [conditions],
                actions: [DefinitionItem.new("set_tags", nil, [tags])]
              )
              create_trigger(account: @state.account, title: "set_tags trigger", active: true, definition: definition)
            end

            describe "and tags added by trigger do not match mail API tags" do
              let(:tags) { "test abc def ghi" }

              it "logs that tags were not applied" do
                @state.properties = { set_tags: "test abc def" }
                Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with("TextAPI properties not applied: [:set_tags]").once
                execute
                assert_equal "abc def ghi test", @state.ticket.set_tags
              end
            end

            describe "and tags added by trigger match mail API tags" do
              let(:tags) { "xyz abc jkl ghi def" }

              it "does not log that tags were not applied" do
                # note: duplicate tags do not get applied
                @state.properties = { set_tags: "xyz abc jkl ghi def jkl" }
                Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with("TextAPI properties not applied: [:set_tags]").never
                execute
                assert_equal "abc def ghi jkl xyz", @state.ticket.set_tags
              end
            end
          end

          describe "when tags are unsorted and duplicates are present" do
            it "adds the correct tags" do
              @state.properties = { set_tags: "xyz abc jkl ghi def jkl" }
              Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with("TextAPI properties not applied: [:set_tags]").never
              execute
              assert_equal "abc def ghi jkl xyz", @state.ticket.set_tags
            end
          end

          describe "when used with the #assignee mail API command with an invalid assignee" do
            it "adds the correct tags" do
              @state.properties = { assignee_email: "email_not_in_account@example.com", set_tags: "xyz abc jkl ghi def jkl" }
              Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with(regexp_matches(/^Tentatively applied TextAPI properties*/)).once
              Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with(regexp_matches(/^Could not apply TextAPI properties*/)).never
              Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with("TextAPI properties not applied: [:set_tags]").never
              execute
              assert_equal "abc def ghi jkl xyz", @state.ticket.set_tags
            end
          end

          describe "when tags are absent" do
            it "does not add tags" do
              @state.properties = { set_tags: "" }
              Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with("TextAPI properties not applied: [:set_tags]").never
              execute
              assert_nil @state.ticket.set_tags
            end
          end
        end

        describe "and #assignee command sets an assignee that does not exist" do
          it "logs the correct message" do
            @state.properties = { assignee_email: "email_not_in_account@example.com" }
            Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with(regexp_matches(/^Tentatively applied TextAPI properties*/)).never
            Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with(regexp_matches(/^Could not apply TextAPI properties*/)).once
            # The following is never called because the #valid? check in lib/zendesk/tickets/properties.rb removes the invalid assignee
            Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with("TextAPI properties not applied: [:assignee]").never
            execute
          end
        end

        describe "and #assignee command sets a valid assignee" do
          it "logs the correct message" do
            @state.properties = { assignee_email: users(:minimum_agent).email }
            Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with(regexp_matches(/^Tentatively applied TextAPI properties*/)).once
            Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with(regexp_matches(/^Could not apply TextAPI properties*/)).never
            Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with("TextAPI properties not applied: [:assignee]").never
            execute
            assert_equal users(:minimum_agent).email, @state.ticket.assignee.email
          end
        end

        describe "and #assignee command sets an assignee that is ineligible to be an assignee" do
          it "logs the correct message" do
            # Set the requester's organization
            @state.ticket.requester.organization = organizations(:minimum_organization3)
            @state.ticket.requester.save!
            # Set the agent's organization
            agent = users(:minimum_admin_not_owner)
            agent.organization = organizations(:minimum_organization2)
            agent.save!
            # Attempt to set the assignee to an agent that is in a different organization
            @state.properties = { assignee_email: agent.email }
            Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with(regexp_matches(/^Tentatively applied TextAPI properties*/)).once
            Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with(regexp_matches(/^Could not apply TextAPI properties*/)).never
            Zendesk::InboundMail::Processors::CommitProcessor.any_instance.expects(:log).with("TextAPI properties not applied: [:assignee]").once
            execute
          end
        end
      end
    end

    describe "flags" do
      before do
        @state.add_flag(EventFlagType.DEFAULT_UNTRUSTED)
      end

      it "flags the ticket" do
        execute
        assert @state.ticket.audits.last.flagged?
      end
    end

    describe "flags_options" do
      before do
        @state.add_flag(EventFlagType.ATTACHMENT_TOO_BIG)
        @state.flags_options.merge!(EventFlagType.ATTACHMENT_TOO_BIG => { message: { key: "something" } })
      end

      it "adds options for the flag" do
        execute
        assert_equal @state.ticket.audits.first.metadata[:flags_options].keys.first, EventFlagType.ATTACHMENT_TOO_BIG.to_s
      end
    end

    it "does not increment ticket commit stats" do
      @statsd_client.expects(:increment).never
      execute
    end

    describe "updating a ticket" do
      before do
        execute

        # setup a new processing state
        processing_state               = Zendesk::InboundMail::ProcessingState.new
        processing_state.ticket        = @state.ticket.reload
        processing_state.account       = accounts(:minimum)
        processing_state.author        = users(:minimum_agent)
        processing_state.plain_content = "some content"
        processing_state.ticket.audit  = processing_state.ticket.audits.build(via_id: ViaType.Mail, author: processing_state.author)
        processing_state.ticket.add_comment(Zendesk::InboundMail::Ticketing::MailComment.new(processing_state, @mail).attributes)
        processing_state.record_deferred_stat(ticket_threading_key, ticket_threading_tags)

        @state = processing_state
      end

      it "does not increment ticket commit stats" do
        @statsd_client.expects(:increment).never
        execute
      end
    end
  end
end
