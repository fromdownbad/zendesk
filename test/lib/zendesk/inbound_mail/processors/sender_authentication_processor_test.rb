require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::SenderAuthenticationProcessor do
  include ArturoTestHelper
  include MailTestHelper

  def execute
    subject.execute(state, mail)
  end

  def does_not_get_suspended
    execute
    assert_nil state.suspension
  end

  def gets_suspended_by(suspension_type)
    execute
    assert_equal suspension_type, state.suspension
  end

  def gets_hard_rejected
    assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
  end

  def authentication_result(options = {})
    result = options.fetch(:result, [:pass])
    query = options.fetch(:query, [:spf_pass, :dkim_pass, :aligned])
    origin = options.fetch(:origin, :not_from_zendesk)
    sender_rule = options[:sender_rule] || 'default'

    auth_result = Zendesk::InboundMail::SenderAuthentication::Result.new(result, query, sender_rule)
    auth_result.origin = origin
    auth_result
  end

  subject { Zendesk::InboundMail::Processors::SenderAuthenticationProcessor }

  let(:account) { accounts(:minimum) }
  let(:from_address) { "no-reply@example.com" }

  let(:state) do
    Zendesk::InboundMail::ProcessingState.new.tap do |state|
      state.account = account
      state.ticket = Ticket.new(account: state.account)
      state.from = Zendesk::Mail::Address.new(address: from_address)
    end
  end

  let(:mail) do
    Zendesk::Mail::InboundMessage.new.tap do |mail|
      mail.account = account
      mail.from = [address("no-reply@example.com")]
      mail.reply_to = [address("no-reply@subdomain.example.com")]
    end
  end

  let(:statsd_client) { mock('statsd_client') }

  let(:logger) { stub("logger", info: true, warn: true) }

  let(:dmarc_fail) { false }

  let(:header_reader) do
    stub(
      values: ["DKIM_PASS", "SPF_PASS"],
      header: "XSpamStatus",
      dmarc_fail_and_reject?: dmarc_fail,
      dmarc: :dmarc_pass,
      dmarc_policy: :dmarc_policy_missing
    )
  end

  before do
    state.account.settings.enable(:email_sender_authentication)
    state.account.save!

    statsd_client.stubs(:increment)
    Zendesk::StatsD::Client.stubs(:new).with(namespace: [:mail_ticket_creator, :sender_authentication]).returns(statsd_client)
    Zendesk::InboundMail::SenderAuthentication::HeaderReaderFactory.stubs(create: header_reader)
  end

  describe "when sender authentication" do
    before do
      mail.stubs(logger: logger)
      Zendesk::InboundMail::SenderAuthentication::Processor.stubs(call: auth_result)
    end

    describe 'has email_sender_authentication setting disabled' do
      let(:auth_result) { authentication_result }

      before do
        account.settings.email_sender_authentication = false
      end

      it 'logs' do
        logger.expects(:info).with('Skipping SenderAuthenticationProcessor')
        execute
      end

      describe 'with disreputable sender rules' do
        let(:auth_result) { authentication_result(result: [:suspend], query: [:spf_fail, :dkim_none, :aligned], sender_rule: :disreputable) }

        it 'logs' do
          logger.expects(:info).with("subdomain.example.com is subject to mandatory sender authentication due to sender rule: 'disreputable'")
          execute
        end

        describe 'with suspend results' do
          let(:auth_result) { authentication_result(result: [:suspend], query: [:spf_fail, :dkim_none, :aligned], sender_rule: :disreputable) }

          it { gets_suspended_by SuspensionType.FAILED_EMAIL_AUTHENTICATION }
        end

        describe 'with reject results' do
          let(:auth_result) { authentication_result(result: [:reject], query: [:spf_fail, :dkim_none, :aligned], sender_rule: :disreputable) }

          it { gets_hard_rejected }
        end
      end

      describe_with_arturo_enabled :email_sender_authentication_logging do
        it 'logs' do
          logger.expects(:info).with('Account has email_sender_authentication_logging Arturo enabled')
          execute
        end

        describe 'with suspend results' do
          let(:auth_result) { authentication_result(result: [:suspend], query: [:spf_fail, :dkim_none, :aligned]) }

          it { does_not_get_suspended }
        end

        describe 'with reject results' do
          let(:auth_result) { authentication_result(result: [:reject], query: [:spf_fail, :dkim_none, :aligned]) }

          it 'does not reject' do
            execute
          end
        end

        describe 'with flag results' do
          let(:auth_result) { authentication_result(result: [:suspend, :flag], query: [:spf_fail, :dkim_none, :aligned]) }

          it 'does not flag' do
            execute
            refute state.flags.include?(EventFlagType.POTENTIAL_MESSAGE_SPOOFING)
          end
        end
      end
    end

    describe 'passes' do
      let(:auth_result) { authentication_result }

      it { does_not_get_suspended }

      describe_with_arturo_enabled :email_sender_auth_report_metrics_v2 do
        it 'logs' do
          message = "Outcome: pass, account: #{state.account.id}, from: subdomain.example.com, whitelisted: false, flagged: false, dry_run: false, auth_header: XSpamStatus, rule: default, sender_role: end_user, auth_results: not_from_zendesk, spf_pass, dkim_pass, aligned, dmarc_pass, and dmarc_policy_missing"
          logger.expects(:info).with(message)
          execute
        end
      end

      describe_with_arturo_disabled :email_sender_auth_report_metrics_v2 do
        it 'logs' do
          message = "Outcome: pass, account: #{state.account.id}, from: subdomain.example.com, whitelisted: false, flagged: false, dry_run: false, auth_header: XSpamStatus, rule: default, auth_results: not_from_zendesk, spf_pass, dkim_pass, and aligned"
          logger.expects(:info).with(message)
          execute
        end
      end

      describe_with_arturo_enabled :email_sender_auth_report_metrics_v2 do
        it "increments statsd sender authentication header counter" do
          statsd_client.expects(:increment).with(
            'x_spam_status',
            tags: [
              'action:pass',
              'origin:not_from_zendesk',
              'SPF:spf_pass',
              'DKIM:dkim_pass',
              'dry_run:false',
              'DMARC:dmarc_pass',
              'DMARC_policy:dmarc_policy_missing',
              'sender_role:end_user'
            ]
          ).once

          execute
        end
      end

      describe_with_arturo_disabled :email_sender_auth_report_metrics_v2 do
        it "increments statsd sender authentication header counter" do
          statsd_client.expects(:increment).with(
            'x_spam_status',
            tags: [
              'action:pass',
              'origin:not_from_zendesk',
              'SPF:spf_pass',
              'DKIM:dkim_pass',
              'dry_run:false'
            ]
          ).once

          execute
        end
      end

      it 'passes sender authentication' do
        execute
        assert state.sender_auth_passed?
      end

      describe 'when there is no reply-to address' do
        let(:mail) do
          Zendesk::Mail::InboundMessage.new.tap do |mail|
            mail.account = accounts(:minimum)
            mail.from = [address("no-reply@example.com")]
          end
        end

        it 'logs the from domain' do
          logger.expects(:info).with(regexp_matches(/from: example.com/))
          execute
        end
      end
    end

    describe 'suspends and flags' do
      let(:auth_result) { authentication_result(result: [:suspend, :flag], query: [:spf_fail, :dkim_none, :aligned]) }

      it { gets_suspended_by SuspensionType.FAILED_EMAIL_AUTHENTICATION }

      it 'includes the user email in the flag message' do
        execute
        assert_equal state.flags_options, EventFlagType.POTENTIAL_MESSAGE_SPOOFING => { message: { userEmail: 'no-reply@example.com' } }
      end

      describe_with_arturo_enabled :email_sender_auth_report_metrics_v2 do
        it 'logs' do
          message = "Outcome: suspend, account: #{state.account.id}, from: subdomain.example.com, whitelisted: false, flagged: true, dry_run: false, auth_header: XSpamStatus, rule: default, sender_role: end_user, auth_results: not_from_zendesk, spf_fail, dkim_none, aligned, dmarc_pass, and dmarc_policy_missing"
          logger.expects(:info).with(message)

          execute
        end
      end

      describe_with_arturo_disabled :email_sender_auth_report_metrics_v2 do
        it 'logs' do
          message = "Outcome: suspend, account: #{state.account.id}, from: subdomain.example.com, whitelisted: false, flagged: true, dry_run: false, auth_header: XSpamStatus, rule: default, auth_results: not_from_zendesk, spf_fail, dkim_none, and aligned"
          logger.expects(:info).with(message)

          execute
        end
      end

      describe_with_arturo_enabled :email_sender_auth_report_metrics_v2 do
        it "increments statsd sender authentication header counter" do
          statsd_client.expects(:increment).with(
            'x_spam_status',
            tags: [
              'action:suspend',
              'origin:not_from_zendesk',
              'SPF:spf_fail',
              'DKIM:dkim_none',
              'dry_run:false',
              'DMARC:dmarc_pass',
              'DMARC_policy:dmarc_policy_missing',
              'sender_role:end_user'
            ]
          ).once

          execute
        end
      end

      describe_with_arturo_disabled :email_sender_auth_report_metrics_v2 do
        it "increments statsd sender authentication header counter" do
          statsd_client.expects(:increment).with(
            'x_spam_status',
            tags: [
              'action:suspend',
              'origin:not_from_zendesk',
              'SPF:spf_fail',
              'DKIM:dkim_none',
              'dry_run:false'
            ]
          ).once

          execute
        end
      end

      it 'fails sender authentication' do
        execute
        refute state.sender_auth_passed?
      end

      describe 'when there is no reply-to address' do
        let(:mail) do
          Zendesk::Mail::InboundMessage.new.tap do |mail|
            mail.account = accounts(:minimum)
            mail.from = [address("no-reply@example.com")]
          end
        end

        it 'logs the from domain' do
          logger.expects(:info).with(regexp_matches(/from: example.com/))
          execute
        end
      end

      describe 'with email_sender_authentication setting disabled' do
        before { account.settings.email_sender_authentication = false }

        it 'does not get flagged' do
          execute
          refute state.flagged?
        end
      end
    end

    describe 'audits' do
      let(:auth_result) { authentication_result(result: [:pass, :audit], query: [:spf_fail, :dkim_none, :unaligned]) }

      it 'increments spf identifier alignment count' do
        statsd_client.expects(:increment).with('spf_identifier_alignment_non_compliant')
        execute
      end

      it 'logs' do
        logger.expects(:info).with('SPF Identifier Alignment not compliant. From/Reply-To does not match Envelope-From/Return-Path. not_from_zendesk, spf_fail, dkim_none, and unaligned')
        execute
      end
    end

    describe "suspends" do
      let(:auth_result) { authentication_result(result: [:suspend], query: [:spf_fail, :dkim_none, :aligned]) }

      it { gets_suspended_by SuspensionType.FAILED_EMAIL_AUTHENTICATION }

      describe_with_arturo_enabled :email_sender_auth_report_metrics_v2 do
        it "logs sender auth details" do
          message = "Outcome: suspend, account: #{state.account.id}, from: subdomain.example.com, whitelisted: false, flagged: false, dry_run: false, auth_header: XSpamStatus, rule: default, sender_role: end_user, auth_results: not_from_zendesk, spf_fail, dkim_none, aligned, dmarc_pass, and dmarc_policy_missing"
          logger.expects(:info).with(message)

          execute
        end
      end

      describe_with_arturo_disabled :email_sender_auth_report_metrics_v2 do
        it "logs sender auth details" do
          message = "Outcome: suspend, account: #{state.account.id}, from: subdomain.example.com, whitelisted: false, flagged: false, dry_run: false, auth_header: XSpamStatus, rule: default, auth_results: not_from_zendesk, spf_fail, dkim_none, and aligned"
          logger.expects(:info).with(message)

          execute
        end
      end

      it 'logs that authentication failed' do
        logger.expects(:warn).with("FAILED_EMAIL_AUTHENTICATION: not_from_zendesk, spf_fail, dkim_none, and aligned -- suspending")

        execute
      end

      it "fails sender authentication" do
        execute
        assert !state.sender_auth_passed?
      end

      describe_with_arturo_enabled :email_sender_auth_report_metrics_v2 do
        it "increments statsd sender authentication header counter" do
          statsd_client.expects(:increment).with(
            'x_spam_status',
            tags: [
              'action:suspend',
              'origin:not_from_zendesk',
              'SPF:spf_fail',
              'DKIM:dkim_none',
              'dry_run:false',
              'DMARC:dmarc_pass',
              'DMARC_policy:dmarc_policy_missing',
              'sender_role:end_user'
            ]
          ).once

          execute
        end
      end

      describe_with_arturo_disabled :email_sender_auth_report_metrics_v2 do
        it "increments statsd sender authentication header counter" do
          statsd_client.expects(:increment).with(
            'x_spam_status',
            tags: [
              'action:suspend',
              'origin:not_from_zendesk',
              'SPF:spf_fail',
              'DKIM:dkim_none',
              'dry_run:false'
            ]
          ).once

          execute
        end
      end

      describe 'when there is no reply_to address' do
        let(:mail) do
          Zendesk::Mail::InboundMessage.new.tap do |mail|
            mail.account = accounts(:minimum)
            mail.from = [address("no-reply@example.com")]
          end
        end

        it 'logs the from domain' do
          logger.expects(:info).with(regexp_matches(/from: example.com/))
          execute
        end
      end

      describe 'with email_sender_authentication setting disabled' do
        before { account.settings.email_sender_authentication = false }
        it { does_not_get_suspended }
      end

      describe_with_arturo_enabled :email_mtc_sender_auth_dmarc do
        describe 'when DMARC failed' do
          let(:dmarc_fail) { true }

          it 'still suspends for the original FAILED_EMAIL_AUTHENTICATION reason' do
            gets_suspended_by SuspensionType.FAILED_EMAIL_AUTHENTICATION
          end
        end
      end
    end

    describe 'rejects' do
      let(:auth_result) { authentication_result(result: [:reject], query: [:spf_fail, :dkim_none, :aligned]) }

      it { gets_hard_rejected }

      describe_with_arturo_enabled :email_sender_auth_report_metrics_v2 do
        it 'logs' do
          message = "Outcome: reject, account: #{state.account.id}, from: subdomain.example.com, whitelisted: false, flagged: false, dry_run: false, auth_header: XSpamStatus, rule: default, sender_role: end_user, auth_results: not_from_zendesk, spf_fail, dkim_none, aligned, dmarc_pass, and dmarc_policy_missing"
          logger.expects(:info).with(message)

          assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
        end
      end

      describe_with_arturo_disabled :email_sender_auth_report_metrics_v2 do
        it 'logs' do
          message = "Outcome: reject, account: #{state.account.id}, from: subdomain.example.com, whitelisted: false, flagged: false, dry_run: false, auth_header: XSpamStatus, rule: default, auth_results: not_from_zendesk, spf_fail, dkim_none, and aligned"
          logger.expects(:info).with(message)

          assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
        end
      end

      describe_with_arturo_enabled :email_sender_auth_report_metrics_v2 do
        it "increments statsd sender authentication header counter" do
          statsd_client.expects(:increment).with(
            'x_spam_status',
            tags: [
              'action:reject',
              'origin:not_from_zendesk',
              'SPF:spf_fail',
              'DKIM:dkim_none',
              'dry_run:false',
              'DMARC:dmarc_pass',
              'DMARC_policy:dmarc_policy_missing',
              'sender_role:end_user'
            ]
          ).once

          assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
        end
      end

      describe_with_arturo_disabled :email_sender_auth_report_metrics_v2 do
        it "increments statsd sender authentication header counter" do
          statsd_client.expects(:increment).with(
            'x_spam_status',
            tags: [
              'action:reject',
              'origin:not_from_zendesk',
              'SPF:spf_fail',
              'DKIM:dkim_none',
              'dry_run:false'
            ]
          ).once

          assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
        end
      end

      it "fails sender authentication" do
        assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
        assert !state.sender_auth_passed?
      end

      describe 'when there is no reply_to address' do
        let(:mail) do
          Zendesk::Mail::InboundMessage.new.tap do |mail|
            mail.account = accounts(:minimum)
            mail.from = [address("no-reply@example.com")]
          end
        end

        it 'logs the from address' do
          logger.expects(:info).with(regexp_matches(/from: example.com/))
          assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
        end
      end

      describe 'with email_sender_authentication setting disabled' do
        before { account.settings.email_sender_authentication = false }

        it 'does not reject' do
          execute
        end
      end
    end

    describe 'detects failed DMARC check' do
      let(:auth_result) { authentication_result }
      let(:dmarc_fail) { true }

      describe_with_arturo_enabled :email_mtc_sender_auth_dmarc do
        describe 'and the sender is an agent' do
          let(:from_address) { users(:minimum_agent).email }

          it 'logs that DMARC failed with a rejection or quarantine policy' do
            logger.expects(:info).with(regexp_matches(/\AMail was not already suspended for sender auth but dmarc=fail/)).once
            execute
          end

          it 'logs that sender is an agent' do
            logger.expects(:info).with(regexp_matches(/sender is an agent/)).once
            execute
          end

          it 'increments metrics for detecting DMARC failure and suspending for it' do
            statsd_client.expects(:increment).with('sender_auth_processor.detect_dmarc_fail', tags: ["from:agent"]).once
            statsd_client.expects(:increment).with('sender_auth_processor.suspend_dmarc_fail').once
            execute
          end

          it { gets_suspended_by SuspensionType.FAILED_DMARC_AUTHENTICATION }
        end

        describe 'and the sender is not an agent' do
          it 'logs that DMARC failed with a rejection or quarantine policy' do
            logger.expects(:info).with(regexp_matches(/\AMail was not already suspended for sender auth but dmarc=fail/)).once
            execute
          end

          it 'logs that sender is not an agent' do
            logger.expects(:info).with(regexp_matches(/sender is not an agent/)).once
            execute
          end

          it 'increments metrics for detecting DMARC failure' do
            statsd_client.expects(:increment).with('sender_auth_processor.detect_dmarc_fail', tags: ["from:enduser"]).once
            execute
          end

          it { does_not_get_suspended }
        end
      end

      describe_with_arturo_disabled :email_mtc_sender_auth_dmarc do
        describe_with_arturo_enabled :email_mtc_sender_auth_dmarc_logging do
          describe 'and the sender is an agent' do
            let(:from_address) { users(:minimum_agent).email }

            it 'logs that sender is an agent' do
              logger.expects(:info).with(regexp_matches(/sender is an agent/)).once
              execute
            end

            it 'increments metrics for detecting DMARC failure' do
              statsd_client.expects(:increment).with('sender_auth_processor.detect_dmarc_fail', tags: ["from:agent"]).once
              execute
            end
          end

          it 'logs that DMARC failed with a rejection or quarantine policy' do
            logger.expects(:info).with(regexp_matches(/\AMail was not already suspended for sender auth but dmarc=fail/)).once
            execute
          end

          it 'logs that sender is not an agent' do
            logger.expects(:info).with(regexp_matches(/sender is not an agent/)).once
            execute
          end

          it 'increments metrics for detecting DMARC failure' do
            statsd_client.expects(:increment).with('sender_auth_processor.detect_dmarc_fail', tags: ["from:enduser"]).once
            execute
          end

          it { does_not_get_suspended }
        end

        describe_with_arturo_disabled :email_mtc_sender_auth_dmarc_logging do
          it 'does not log that DMARC failed with a rejection or quarantine policy' do
            logger.expects(:info).with(regexp_matches(/\AMail was not already suspended for sender auth but dmarc=fail/)).never
            execute
          end

          it 'does not increment metrics for detecting DMARC failure' do
            statsd_client.expects(:increment).with('sender_auth_processor.detect_dmarc_fail', tags: ["from:enduser"]).never
            execute
          end

          it { does_not_get_suspended }
        end
      end
    end
  end
end
