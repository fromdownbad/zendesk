require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::ForwardingVerificationProcessor do
  fixtures :accounts, :recipient_addresses

  let(:recipient_address) { recipient_addresses(:not_default) }

  before do
    @mail = FixtureHelper::Mail.read('minimum_ticket.json')
    @mail.subject = "Some subject"
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = recipient_address.account
  end

  describe "mail forwarding" do
    it "lets mails with incorrect code pass" do
      @mail.body = "Strange code #{RecipientAddresses::ForwardingStatus::CODE_START}blablub"
      assert_pass
    end

    it "catchs correct mails" do
      @mail.body = "bla bla bla #{recipient_address.forwarding_code} bla"
      assert_rejected
    end

    it "catchs correct mails with token in parts" do
      @mail.parts = [stub(body: "bla bla bla #{recipient_address.forwarding_code} bla")]
      assert_rejected
    end

    it "catchs correct with encoded characters" do
      @mail.body = "bla ˙∆˙∆ßå∂˙ß˚∆˜≈∆ç≈bla bla #{recipient_address.forwarding_code} bla".force_encoding('ASCII-8BIT')
      @mail.parts = [stub(body: "ÓÎ˙¨˚∆ÔÓ˚˙")]
      assert_rejected
    end

    it "does not blow up with invalid chars in binary bodies" do
      @mail.body = "bla"
      @mail.parts = [
        stub(body: "\xE1"),
        stub(body: "bla #{recipient_address.forwarding_code} blub")
      ]
      assert_rejected
    end

    it "handles mails" do
      ActionMailer::Base.perform_deliveries = true
      ActionMailer::Base.deliveries.clear

      recipient_address.verify_forwarding_status!
      assert_equal :waiting, recipient_address.reload.forwarding_status
      assert_equal 1, ActionMailer::Base.deliveries.size

      # mail comes in ...
      mail = ActionMailer::Base.deliveries.last
      mail = stub(
        subject: mail.subject,
        body: mail.body,
        parts: mail.parts,
        logger: @mail.logger
      )
      assert_raises Zendesk::InboundMail::HardMailRejectException do
        Zendesk::InboundMail::Processors::ForwardingVerificationProcessor.new(recipient_address, mail).execute
      end
      assert_equal :verified, recipient_address.reload.forwarding_status
    end
  end

  def assert_rejected
    assert_raise(Zendesk::InboundMail::HardMailRejectException) { assert_pass }
  end

  def assert_pass
    Zendesk::InboundMail::Processors::ForwardingVerificationProcessor.new(@state, @mail).execute
  end
end
