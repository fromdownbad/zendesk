require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::SyntaxErrorProcessor do
  before do
    @state = stub
  end

  describe "a clean email" do
    before do
      @state.stubs(:suspended?).returns(false)
      @mail = stub(has_errors?: false)
    end

    it "does not do anything" do
      Zendesk::InboundMail::Processors::SyntaxErrorProcessor.execute(@state, @mail)
    end
  end

  describe "a legit email with errors" do
    before do
      @state.stubs(:suspended?).returns(false)
      @mail = stub(has_errors?: true, error_messages: 'error message')
    end

    it "raises a SyntaxError" do
      assert_raise Zendesk::InboundMail::SyntaxError do
        Zendesk::InboundMail::Processors::SyntaxErrorProcessor.execute(@state, @mail)
      end
    end
  end

  describe "a spam email with errors" do
    before do
      @state.stubs(:suspended?).returns(true)
      @mail = stub(has_errors?: true, error_messages: 'error message')
    end

    it "does not raise a SyntaxError" do
      Zendesk::InboundMail::Processors::SyntaxErrorProcessor.execute(@state, @mail)
    end
  end
end
