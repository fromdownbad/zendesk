require_relative "../../../../support/test_helper"
require_relative "../../../../support/agent_test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::ReplyToProcessor do
  extend ArturoTestHelper
  include AgentTestHelper

  fixtures :tickets, :recipient_addresses, :users, :user_identities

  let(:account) { accounts(:reply_to) }
  let(:agent) { users(:reply_to_agent) }
  let(:end_user) { users(:reply_to_end_user) }
  let(:another_end_user) { users(:reply_to_another_end_user) }
  let(:from_email_1) { 'foo@test.zendesk.com' }
  let(:from_email_2) { 'foobar@test.zendesk.com' }
  let(:light_agent) { users(:reply_to_light_agent) }
  let(:support) { recipient_addresses(:reply_to) }

  before do
    @mail = FixtureHelper::Mail.read('reply_to/email_with_reply_to_different.json')
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = account
    @state.ticket
  end

  def from_email_same
    @state.from = Zendesk::Mail::Address.new(address: from_email_1)
    @state.from_header = Zendesk::Mail::Address.new(address: from_email_1)
  end

  def from_email_different_and_reply_to_an_agent(from_email = from_email_2, reply_to = nil)
    @state.from = Zendesk::Mail::Address.new(address: reply_to || agent.email)
    @state.from_header = Zendesk::Mail::Address.new(address: from_email)
  end

  def from_email_different_and_reply_to_an_end_user(from_email = from_email_2)
    @state.from = Zendesk::Mail::Address.new(address: reply_to || end_user.email)
    @state.from_header = Zendesk::Mail::Address.new(address: from_email)
  end

  def from_email_different_and_reply_not_an_agent
    @state.from = Zendesk::Mail::Address.new(address: from_email_1)
    @state.from_header = Zendesk::Mail::Address.new(address: from_email_2)
  end

  def from_and_from_header_address_are_whitelisted
    @state.whitelisted = true
    @state.from_header_whitelisted = true
  end

  def from_and_from_header_address_are_not_whitelisted
    @state.whitelisted = false
    @state.from_header_whitelisted = false
  end

  def from_header_address_is_not_whitelisted
    @state.whitelisted = true
    @state.from_header_whitelisted = false
  end

  def random_plus_id(address)
    random = SecureRandom.random_number(100_000)
    "#{address.split('@')[0]}+#{random}@#{address.split('@')[1]}"
  end

  def execute
    Zendesk::InboundMail::Processors::ReplyToProcessor.execute(@state, @mail)
  end

  def self.it_should_increment_statsd_counter(flagged: true, match_without_plus_id: false)
    before do
      @statsd_client = Zendesk::InboundMail::StatsD.client
      Zendesk::StatsD::Client.stubs(:new).
        with(namespace: 'mail_ticket_creator').
        returns(@statsd_client)
    end

    it 'should increment statsd counter' do
      @statsd_client.expects(:increment).with('reply_to_processor.reply_to_and_from_header_variance', tags: ["flagged:#{flagged}"]).once

      if match_without_plus_id == true
        @statsd_client.expects(:increment).with('reply_to_processor.reply_to_and_from_match_after_plus_id_removal').once
      end

      execute
    end
  end

  def self.it_should_not_increment_statsd_counter
    before do
      @statsd_client = Zendesk::InboundMail::StatsD.client
      Zendesk::StatsD::Client.stubs(:new).
        with(namespace: 'mail_ticket_creator').
        returns(@statsd_client)
    end

    it 'should not increment statsd counter' do
      @statsd_client.expects(:increment).never
      execute
    end
  end

  def self.it_should_set_a_flag
    it 'should set a flag' do
      execute
      assert @state.flagged?
      assert_equal AuditFlags.new([EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF]), @state.flags
    end
  end

  def self.it_should_not_set_a_flag
    it 'should not set a flag' do
      execute
      refute @state.flagged?
      refute_equal AuditFlags.new([EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF]), @state.flags
    end
  end

  def self.it_should_set_end_user_possible_spoof_flag
    it 'should set end user possible spoof flag' do
      execute
      assert_equal AuditFlags.new([EventFlagType.REPLY_TO_END_USER_POSSIBLE_SPOOF]), @state.flags
    end
  end

  def self.it_should_not_set_end_user_possible_spoof_flag
    it 'should not set end user possible spoof flag' do
      execute
      refute_equal AuditFlags.new([EventFlagType.REPLY_TO_END_USER_POSSIBLE_SPOOF]), @state.flags
    end
  end

  def self.it_should_not_set_original_author
    it 'does not set state.original_author' do
      execute
      assert_nil @state.original_author
    end
  end

  def self.it_does_set_original_author
    it 'does set state.original_author' do
      execute
      assert @state.original_author.is_a?(User)
      assert_equal @state.from_header.address, @state.original_author.email
    end
  end

  def self.it_should_log_message_about_skipping_reply_to_processor
    it 'does set state.original_author' do
      logger = stub('logger', :info)
      Zendesk::InboundMail::MailLogger.stubs(new: logger)
      logger.expects(:log).with('Skipping ReplyToProcessor').once
      execute
    end
  end

  def self.it_should_log_the_outcome(new_ticket: true, flagged: true, from_user_role: 'unknown', from_an_light_agent: false, from_an_support_address: false)
    it 'should log message the outcome' do
      logger = stub('logger', :info)
      Zendesk::InboundMail::MailLogger.stubs(new: logger)

      logger.expects(:log).with('Account has email_reply_to_vulnerability_check Arturo enabled').once
      logger.expects(:warn).once
      logger.expects(:log).with("Outcome: email #{flagged ? 'is' : 'is not'} flagged, new ticket: #{new_ticket}, from: #{@state.from_header&.address}, "\
        "from_whitelisted: #{@state.from_header_whitelisted?}, reply_to: #{@state.from&.address}, whitelisted: #{@state.whitelisted?}, "\
        "from_user_role: #{from_user_role}, from_an_light_agent: #{from_an_light_agent}, from_an_support_address: #{from_an_support_address}").once

      execute
    end
  end

  def self.it_should_not_log_the_outcome
    it 'should not log the outcome' do
      logger = stub('logger', :info)
      Zendesk::InboundMail::MailLogger.stubs(new: logger)

      logger.expects(:log).with('Account has email_reply_to_vulnerability_check Arturo enabled').once
      logger.expects(:log).with(regexp_matches(/Outcome/)).never
      execute
    end
  end

  describe_with_arturo_disabled :email_reply_to_vulnerability_check do
    it_should_log_message_about_skipping_reply_to_processor
    it_should_not_increment_statsd_counter
    it_should_not_set_a_flag
    it_should_not_set_original_author
  end

  describe_with_arturo_enabled :email_reply_to_vulnerability_check do
    describe 'with from and reply-to emails being same' do
      before do
        from_email_same
      end

      it_should_not_log_the_outcome
      it_should_not_increment_statsd_counter
      it_should_not_set_original_author
    end

    describe 'with from and from_header email address being different, reply-to an agent and not whitelisted' do
      before do
        from_email_different_and_reply_to_an_agent
        from_and_from_header_address_are_not_whitelisted
      end

      it_should_log_the_outcome
      it_should_increment_statsd_counter
      it_should_set_a_flag
    end

    describe 'with from and from_header email address being different, reply-to an agent and from_header is not whitelisted' do
      before do
        from_email_different_and_reply_to_an_agent
        from_header_address_is_not_whitelisted
      end

      it_should_log_the_outcome
      it_should_increment_statsd_counter
      it_should_set_a_flag

      describe 'with from_header email also an agent' do
        before do
          admin = users(:reply_to_admin)
          from_email_different_and_reply_to_an_agent(admin.email, agent.email)
        end

        it_should_log_the_outcome(from_user_role: 'Account owner', flagged: false)
        it_should_increment_statsd_counter(flagged: false)
        it_should_not_set_a_flag
        it_does_set_original_author
      end

      describe 'with from_header email a light agent' do
        before do
          light_agent.stubs(:is_light_agent?).returns(true)
          User.any_instance.stubs(is_light_agent?: true)
          from_email_different_and_reply_to_an_agent(light_agent.email, agent.email)
        end

        it_should_log_the_outcome(from_user_role: 'Agent', from_an_light_agent: true)
        it_should_increment_statsd_counter
        it_should_set_a_flag
        it_does_set_original_author
      end

      describe 'with from_header email having a plus id' do
        let(:logger) { stub('logger', :warn, :info) }
        before do
          Zendesk::MtcMpqMigration.config.stubs(:zendesk_host).returns('chaghassipour.com')
          Zendesk::InboundMail::MailLogger.stubs(new: logger)
          logger.expects(:warn).once
          logger.expects(:log).with('Account has email_reply_to_vulnerability_check Arturo enabled').once
          logger.expects(:log).with(regexp_matches(/new ticket/)).once
        end

        describe 'with from_address with the plus id of an agent' do
          before do
            @from_email = random_plus_id(agent.email)
            from_email_different_and_reply_to_an_agent(@from_email, agent.email)
            logger.expects(:log).with("Note, email #{@from_email} and #{agent.email} are same after removing plus ids").
              once
          end

          it_should_increment_statsd_counter(match_without_plus_id: true)
          it_should_set_a_flag
        end

        describe 'with from_address with the plus id of a light agent' do
          before do
            @from_email = random_plus_id(light_agent.email)
            from_email_different_and_reply_to_an_agent(@from_email, agent.email)
            light_agent.stubs(:is_light_agent?).returns(true)
            User.any_instance.stubs(is_light_agent?: true)
          end

          it_should_set_a_flag
        end

        describe 'with from_address with the plus id of support' do
          before do
            @from_email = random_plus_id(support.email)
            from_email_different_and_reply_to_an_agent(@from_email, agent.email)
          end

          it_should_set_a_flag
        end
      end

      describe 'with nil from_header' do
        before do
          @state.from_header = nil
        end

        it_should_not_log_the_outcome
        it_should_not_increment_statsd_counter
        it_should_not_set_a_flag
        it_should_not_set_original_author
      end
    end

    describe 'with from and from_header email address being different, reply-to an agent and whitelisted' do
      before do
        from_email_different_and_reply_to_an_agent
        from_and_from_header_address_are_whitelisted
      end

      it_should_not_log_the_outcome
      it_should_not_increment_statsd_counter
      it_should_not_set_a_flag
    end

    describe 'with from and from_header email address being different and reply-to not an agent' do
      before do
        from_email_different_and_reply_not_an_agent
        from_and_from_header_address_are_not_whitelisted
      end

      it_should_not_log_the_outcome
      it_should_not_increment_statsd_counter
      it_should_not_set_a_flag
    end

    describe 'with from_header an support address and reply-to not an agent' do
      before do
        from_email_different_and_reply_to_an_agent
        from_and_from_header_address_are_not_whitelisted
        @state.from_header.address = support.email
        @state.ticket = tickets(:minimum_1)
      end

      it_should_log_the_outcome(new_ticket: false, flagged: false, from_an_support_address: true)
      it_should_increment_statsd_counter(flagged: false)
      it_should_not_set_a_flag
    end

    describe_with_arturo_enabled :email_reply_to_vulnerability_end_user_impersonation do
      describe 'with different originator fields and reply-to an end user' do
        before do
          @state.from = Zendesk::Mail::Address.new(address: end_user.email)
        end

        describe 'with from an anonymous user' do
          before do
            @state.from_header = Zendesk::Mail::Address.new(address: from_email_1)
          end

          it_should_set_end_user_possible_spoof_flag
        end

        describe 'with from an end user' do
          before do
            @state.from_header = Zendesk::Mail::Address.new(address: another_end_user.email)
          end

          it_should_set_end_user_possible_spoof_flag
        end

        describe 'with from an agent' do
          before do
            @state.from_header = Zendesk::Mail::Address.new(address: agent.email)
          end

          it_should_not_set_end_user_possible_spoof_flag
        end

        describe 'with from an support address' do
          before do
            @state.from_header = Zendesk::Mail::Address.new(address: support.email)
          end

          it_should_not_set_end_user_possible_spoof_flag
        end
      end

      describe 'with same originator fields and reply-to an end user' do
        before do
          @state.from = Zendesk::Mail::Address.new(address: end_user.email)
          @state.from_header = Zendesk::Mail::Address.new(address: end_user.email)
        end

        it_should_not_set_end_user_possible_spoof_flag
      end

      describe 'with different originator fields and reply-to an agent' do
        before do
          from_email_different_and_reply_to_an_agent
        end

        it_should_not_set_end_user_possible_spoof_flag
      end
    end

    describe_with_arturo_disabled :email_reply_to_vulnerability_end_user_impersonation do
      describe 'with different originator fields and reply-to an end user' do
        before do
          @state.from = Zendesk::Mail::Address.new(address: end_user.email)
        end

        describe 'with from an anonymous user' do
          before do
            @state.from_header = Zendesk::Mail::Address.new(address: from_email_1)
          end

          it_should_not_set_end_user_possible_spoof_flag
        end

        describe 'with from an end user' do
          before do
            @state.from_header = Zendesk::Mail::Address.new(address: another_end_user.email)
          end

          it_should_not_set_end_user_possible_spoof_flag
        end

        describe 'with from an agent' do
          before do
            @state.from_header = Zendesk::Mail::Address.new(address: agent.email)
          end

          it_should_not_set_end_user_possible_spoof_flag
        end

        describe 'with from an support address' do
          before do
            @state.from_header = Zendesk::Mail::Address.new(address: support.email)
          end

          it_should_not_set_end_user_possible_spoof_flag
        end
      end
    end
  end
end
