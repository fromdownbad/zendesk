require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::IdentifierProcessor do
  fixtures(:tickets, :events)

  def execute
    Zendesk::InboundMail::Processors::IdentifierProcessor.execute(@state, @mail)
  end

  before do
    account = accounts(:minimum)
    @mail = FixtureHelper::Mail.read('minimum_ticket.json')
    @ticket = tickets(:minimum_1)
    @comment = events(:create_comment_for_minimum_ticket_1)
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.message_id = '12345'
    @state.account = account
    @state.ticket = @ticket
    @inbound_email = @state.inbound_email = account.inbound_emails.create!(message_id: @state.message_id, from: 'awesome')
  end

  it "associates the ticket with the inbound email record" do
    execute
    @inbound_email.reload

    assert_equal @ticket.id, @inbound_email.ticket_id
  end

  it "associates the comment with the inbound email record" do
    @state.comment = @comment

    execute
    @inbound_email.reload

    assert_equal @comment.id, @inbound_email.comment.id
  end

  it "updates the inbound email record's category" do
    execute
    @inbound_email.reload

    assert_equal InboundEmail.categories[:ham], @inbound_email.category_id
  end

  describe "suspended" do
    before do
      @state.suspension = SuspensionType.SPAM
      execute
      @inbound_email.reload
    end

    it "does not try to associate the ticket" do
      assert_nil @inbound_email.ticket_id
    end

    it "updates the category" do
      assert_equal SuspensionType.SPAM, @inbound_email.category_id
    end
  end
end
