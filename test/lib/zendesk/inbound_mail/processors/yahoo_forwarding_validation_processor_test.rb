require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::YahooForwardingValidationProcessor do
  include ArturoTestHelper
  fixtures :accounts, :recipient_addresses, :tokens

  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }

  before do
    @account          = accounts(:minimum)
    @mail             = FixtureHelper::Mail.read("yahoo_forwarding_verification.json")
    @mail_new         = FixtureHelper::Mail.read("yahoo_forwarding_verification_new.json")
    @from_address     = "no-reply@cc.yahoo-inc.com" # value from fixture file

    @mail.account     = @account
    @mail_new.account = @account
    @state            = Zendesk::InboundMail::ProcessingState.new
    @state.account    = @account
    @state.from       = Zendesk::Mail::Address.new(name: "No-reply", address: "no-reply@cc.yahoo-inc.com")
  end

  it "increments the statsd log with the `confirmation:new` tag if the confirmation link in the email matches the new regex" do
    @state.message_id = @mail_new.message_id
    statsd_client.stubs(:increment)
    statsd_client.expects(:increment).with(:received_yahoo_verification_email, tags: ['confirmation_link:new']).once
    Zendesk::InboundMail::Processors::YahooForwardingValidationProcessor.execute(@state, @mail_new)
  end

  it "increments the statsd log with the `confirmation:old` tag if the confirmation link in the email matches the old regex" do
    @state.message_id = @mail.message_id
    statsd_client.stubs(:increment)
    statsd_client.expects(:increment).with(:received_yahoo_verification_email, tags: ['confirmation_link:old']).once
    Zendesk::InboundMail::Processors::YahooForwardingValidationProcessor.execute(@state, @mail)
  end

  describe_with_arturo_enabled :email_yahoo_forwarding_validation_processor_new_regex do
    it "whitelists new (current) format of Yahoo forwarding verification email so it is not rejected in subsequent processors" do
      @state.message_id = @mail_new.message_id
      Zendesk::InboundMail::Processors::YahooForwardingValidationProcessor.execute(@state, @mail_new)
      assert @state.whitelisted?
      assert_equal "noreply@zendesk.com", @state.from.address

      # making sure that it did not cache content in a bad state
      refute @mail_new.content.instance_variable_get(:@content)
    end

    it "increments the statsd log with `whitelisted:new` tag" do
      @state.message_id = @mail_new.message_id
      statsd_client.stubs(:increment)
      statsd_client.expects(:increment).with(:received_yahoo_verification_email, tags: ['whitelisted:new']).once
      Zendesk::InboundMail::Processors::YahooForwardingValidationProcessor.execute(@state, @mail_new)
    end

    it "does not whitelist old format of Yahoo forwarding verification email" do
      @state.message_id = @mail.message_id
      Zendesk::InboundMail::Processors::YahooForwardingValidationProcessor.execute(@state, @mail)
      refute @state.whitelisted?

      # making sure that it did not cache content in a bad state
      refute @mail.content.instance_variable_get(:@content)
    end
  end

  describe_with_arturo_disabled :email_yahoo_forwarding_validation_processor_new_regex do
    it "whitelists old format of Yahoo forwarding verification email so it is not rejected in subsequent processors" do
      @state.message_id = @mail.message_id
      Zendesk::InboundMail::Processors::YahooForwardingValidationProcessor.execute(@state, @mail)
      assert @state.whitelisted?
      assert_equal "noreply@zendesk.com", @state.from.address

      # making sure that it did not cache content in a bad state
      refute @mail.content.instance_variable_get(:@content)
    end

    it "increments the statsd log with `whitelisted:old` tag" do
      @state.message_id = @mail.message_id
      statsd_client.stubs(:increment)
      statsd_client.expects(:increment).with(:received_yahoo_verification_email, tags: ['whitelisted:old']).once
      Zendesk::InboundMail::Processors::YahooForwardingValidationProcessor.execute(@state, @mail)
    end

    it "does not whitelist new (current) format of Yahoo forwarding verification email" do
      @state.message_id = @mail_new.message_id
      Zendesk::InboundMail::Processors::YahooForwardingValidationProcessor.execute(@state, @mail_new)
      refute @state.whitelisted?

      # making sure that it did not cache content in a bad state
      refute @mail_new.content.instance_variable_get(:@content)
    end
  end

  it "does not whitelist when sent from a different yahoo address" do
    @state.from.address = "other@yahoo.com"
    @state.message_id = @mail.message_id
    Zendesk::InboundMail::Processors::YahooForwardingValidationProcessor.execute(@state, @mail)
    refute @state.whitelisted?
  end
end
