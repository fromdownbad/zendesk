require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 11

describe Zendesk::InboundMail::Processors::SpamProcessor do
  include ArturoTestHelper
  include MailTestHelper

  subject { Zendesk::InboundMail::Processors::SpamProcessor }

  let(:spam_processor) { subject.new(@state, @mail) }
  let(:account) { accounts('minimum') }
  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }
  let(:logger) { @mail.logger }

  def execute
    subject.execute(@state, @mail)
  end

  def does_not_get_suspended
    execute
    assert_nil @state.suspension
  end

  def gets_suspended_by(suspension_type)
    execute
    assert_equal suspension_type, @state.suspension
  end

  def gets_hard_rejected
    assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
  end

  before do
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = account
    @state.account.stubs(subdomain: 'example')
    @state.account.stubs(spam_threshold_multiplier: 1)

    @state.stubs(whitelisted?: false)
    @mail = Zendesk::Mail::InboundMessage.new
    @mail.account = account

    statsd_client.stubs(:increment)
    logger.stubs(:info)
  end

  describe '#execute' do
    describe_with_arturo_disabled(:email_skip_spam_processor) do
      it "is not disabled" do
        statsd_client.expects(:increment).with('spam_processor.disabled').never
        execute
      end
    end

    describe 'for emails fetched from Mail Fetcher (Gmail Connector)' do
      before { @mail.headers['X-Zendesk-Source'] = '1' }

      it "logs a message and increments a metric" do
        logger.expects(:info).with("Skipping SpamProcessor for emails fetched from Mail Fetcher (Gmail Connector)")
        statsd_client.expects(:increment).with('spam_processor.skip_mail_fetcher_email').once
        execute
      end
    end

    describe_with_arturo_enabled(:email_skip_spam_processor) do
      it "logs a message and increments a metric" do
        logger.expects(:warn).with("WARNING! Account '#{account.subdomain}' has the Spam Processor disabled (via email_skip_spam_processor Arturo) -- warning")
        statsd_client.expects(:increment).with('spam_processor.disabled').once
        execute
      end
    end
  end

  describe '#filter_with_cloudmark' do
    before do
      @mail.headers['X-Spam-Zendesk-Score'] = 1.0

      # Cloudmark considers this as obvious spam.
      @mail.headers['X-CMAE-Score'] = 100.0
    end

    describe_with_arturo_disabled(:email_rspamd) do
      it "is not replaced by Rspamd" do
        statsd_client.expects(:increment).with('spam_processor.rspamd.enabled').never
        execute
      end
    end

    describe_with_arturo_enabled(:email_rspamd) do
      it { does_not_get_suspended }

      it 'tracks the result with a metric' do
        statsd_client.expects(:increment).
          with(:cloudmark, tags: ['result:obviously_spam', 'enabled:false', 'value:100.0', 'threshold:95.0']).
          once
        execute
      end

      it "replaces Cloudmark with Rspamd" do
        logger.expects(:warn).with("Skipping Cloudmark because '#{account.subdomain}' has Rspamd enabled (via email_rspamd Arturo) -- warning")
        statsd_client.expects(:increment).with('spam_processor.rspamd.enabled').once
        execute
      end
    end

    describe 'when missing the Cloudmark score' do
      before { @mail.headers['X-CMAE-Score'] = nil }

      it 'tracks the result with a metric' do
        statsd_client.expects(:increment).
          with(:cloudmark, tags: ["result:missing_score", "enabled:true"]).
          once
        execute
      end
    end

    describe 'for obvious spam' do
      before { @mail.headers['X-CMAE-Score'] = 100.0 }

      it { gets_suspended_by SuspensionType.SPAM }

      it 'tracks the result with a metric' do
        statsd_client.expects(:increment).
          with(:cloudmark, tags: ['result:obviously_spam', 'enabled:true', 'value:100.0', 'threshold:95.0']).
          once
        execute
      end

      describe 'that may be legitimate' do
        before { @state.stubs(whitelisted?: true) }
        it { does_not_get_suspended }
      end
    end

    describe 'for probable spam' do
      before { @mail.headers['X-CMAE-Score'] = 95.0 }

      describe_with_arturo_disabled(:email_update_cloudmark_obvious_spam_threshold) do
        it 'tracks the result with a metric' do
          statsd_client.expects(:increment).
            with(:cloudmark, tags: ['result:probably_spam', 'enabled:true', 'value:95.0']).
            once
          execute
        end

        it { does_not_get_suspended }
      end

      describe_with_arturo_enabled(:email_update_cloudmark_obvious_spam_threshold) do
        it 'tracks the result with a metric' do
          statsd_client.expects(:increment).
            with(:cloudmark, tags: ['result:obviously_spam', 'enabled:true', 'value:95.0', 'threshold:95.0']).
            once
          execute
        end

        it { gets_suspended_by(SuspensionType.SPAM) }
      end
    end

    describe 'when the Cloudmark score is not high enough for us to suspend' do
      before do
        cloudmark_spam_threshold = Zendesk::InboundMail::CloudmarkSpamScore::PROBABLE_THRESHOLD
        non_spammy_cloudmark_score = cloudmark_spam_threshold - 1.0
        @mail.headers['X-CMAE-Score'] = non_spammy_cloudmark_score
      end

      describe 'when the Rspamd score is high enough to suspend but not high enough to reject' do
        before do
          rspamd_suspension_threshold = subject::HARD_THRESHOLD * subject::SOFT_THRESHOLD
          suspendable_rspamd_score = rspamd_suspension_threshold + 1.0
          @mail.headers['X-Spam-Zendesk-Score'] = suspendable_rspamd_score
        end

        it 'ignores the Rspamd score and does not suspend' do
          does_not_get_suspended
        end
      end

      describe 'when the Rspamd score exceeds the configured threshold' do
        before do
          logger.stubs(:info)

          rejectable_rspamd_score = subject::HARD_THRESHOLD + 1.0
          @mail.headers['X-Spam-Zendesk-Score'] = rejectable_rspamd_score
        end

        it 'overrides Cloudmark and suspends the email' do
          statsd_client.expects(:increment).with(:cloudmark_overruled, tags: ["spam_suspension:false"]).once
          logger.expects(:info).with(regexp_matches(/^Cloudmark overruled due to Rspamd score.*/)).once

          gets_suspended_by(SuspensionType.SPAM)
        end

        describe 'when the Rspamd score is greater than multiplier * HARD_THRESHOLD only because account.spam_threshold_multiplier < 1' do
          before do
            spam_threshold_multiplier = 0.5
            @state.account.stubs(spam_threshold_multiplier: spam_threshold_multiplier)

            rspamd_score = subject::HARD_THRESHOLD * spam_threshold_multiplier + 0.01
            @mail.headers['X-Spam-Zendesk-Score'] = rspamd_score
          end

          it 'does not suspend or hard reject the email' do
            does_not_get_suspended
          end
        end
      end
    end
  end

  describe 'with Rspamd enabled for the account' do
    describe_with_arturo_enabled :email_rspamd do
      describe 'when the email has an obviously spammy Cloudmark score but not a spammy Rspamd score' do
        before do
          @mail.headers['X-Spam-Zendesk-Score'] = 0.0
          @mail.headers['X-CMAE-Score'] = Zendesk::InboundMail::CloudmarkSpamScore::PROBABLE_THRESHOLD
        end

        it { does_not_get_suspended }
      end

      describe 'when the email has an obviously spammy Rspamd score' do
        before { @mail.headers['X-Spam-Zendesk-Score'] = subject::HARD_THRESHOLD + 1.0 }

        it { gets_hard_rejected }
      end

      describe 'when the email has a probably spammy Rspamd score' do
        before do
          rspamd_suspension_threshold = subject::HARD_THRESHOLD * subject::SOFT_THRESHOLD
          suspendable_rspamd_score = rspamd_suspension_threshold + 1.0
          @mail.headers['X-Spam-Zendesk-Score'] = suspendable_rspamd_score
        end

        it { gets_suspended_by(SuspensionType.SPAM) }
      end

      describe 'when the Rspamd score is greater than multiplier * HARD_THRESHOLD only because account.spam_threshold_multiplier < 1' do
        before do
          spam_threshold_multiplier = 0.5
          @state.account.stubs(spam_threshold_multiplier: spam_threshold_multiplier)

          rspamd_score = subject::HARD_THRESHOLD * spam_threshold_multiplier + 0.01
          @mail.headers['X-Spam-Zendesk-Score'] = rspamd_score
        end

        it 'suspends the email instead of rejecting it' do
          gets_suspended_by(SuspensionType.SPAM)
        end
      end

      describe 'HARD_THRESHOLD' do
        it { assert_equal(20.0, subject::HARD_THRESHOLD) }
      end
    end
  end

  describe 'Rspamd' do
    it 'uses X-Spam-Zendesk-Score' do
      @mail.headers['X-Spam-Zendesk-Score'] = 3.0
      assert_equal 3.0, spam_processor.send(:spam_score)
    end
  end

  describe 'messages as spam' do
    def multiplier
      spam_processor.send(:multiplier)
    end

    let!(:normal_threshold) { subject.new(@state, @mail).send(:multiplier) }

    before do
      @state.stubs(whitelisted?: false)
      @state.author = stub(is_agent?: false, is_verified?: false)
    end

    describe 'authorized?' do
      before do
        @state.ticket = Ticket.new { |t| t.updated_at = 2.weeks.ago }
        @state.authorize!
      end

      it 'is less likely to be spam' do
        assert_operator normal_threshold, :<, multiplier
      end

      it 'is equally likely to be spam when old' do
        @state.ticket.updated_at = 8.months.ago
        assert_equal normal_threshold, multiplier
      end
    end

    it 'is less likely if submitted by an agent' do
      @state.submitter = stub(is_agent?: true, is_verified?: true)
      assert_operator normal_threshold, :<, multiplier
    end

    it 'is less likely if whitelisted' do
      @state.stubs(whitelisted?: true)
      assert_operator normal_threshold, :<, multiplier
    end

    it 'is less likely if the author is verified' do
      @state.author.stubs(is_verified?: true)
      assert_operator normal_threshold, :<, multiplier
    end

    it 'is less likely if the author is an agent' do
      @state.author.stubs(is_verified?: true)
      @state.author.stubs(is_agent?: true)
      assert_operator normal_threshold, :<, multiplier
    end

    it 'is less likely if the account is more tolerant of spam' do
      @state.account.stubs(spam_threshold_multiplier: 1.5)
      assert_operator normal_threshold, :<, multiplier
    end

    it 'accumulates all non-spam indicators' do
      @state.stubs(whitelisted?: true)
      @state.account.stubs(spam_threshold_multiplier: 1.5)
      @state.author = stub(is_agent?: true, is_verified?: true)
      assert_equal 12.0, multiplier
    end
  end

  it 'hard rejects emails with high spam scores' do
    @mail = FixtureHelper::Mail.read('spam_score.json')
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = accounts('minimum')
    gets_hard_rejected
  end

  describe 'ignoring the spam multiplier when conditions exist to make it > 1.0' do
    before do
      @state.unstub(:whitelisted?)

      requester = users(:minimum_end_user)
      @ticket = @state.account.tickets.create! do |t|
        t.requester = requester
        t.subject = 'Refund please!'
        t.description = 'Can I have a refund please?'
        t.via_id = ViaType.MAIL
        t.will_be_saved_by(requester)
      end

      mail_submitter = users(:minimum_agent)
      @mail.from = users(:minimum_agent).email
      @mail.subject = 'Sure!'
      @mail.body = 'This seems fine. Have a full million dollar refund!'
      @mail.headers['X-Rspamd-Authentication-Results'] = 'Rspamd sender auth results here'
      @mail.headers['X-Rspamd-Status'] = 'Rspamd results here'

      @state.plain_content = @mail.body

      # Set the state.submitter to be an agent, which increases the multiplier by 4x
      @state.submitter = mail_submitter

      # Mark the state as allowlisted, which increases the multiplier by 2x
      @state.whitelisted = true

      # Authorize the state. Combined with the ticket above being created recently,
      # this will add 2 points to the multiplier.
      @state.authorize!
    end

    describe 'when the email is to be threaded as a comment on an existing ticket' do
      before { @state.ticket = @ticket }

      describe_with_arturo_enabled :email_ignore_spam_multiplier do
        describe 'when the Rspamd score is greater than the soft threshold' do
          before { @mail.headers['X-Spam-Zendesk-Score'] = subject::HARD_THRESHOLD * subject::SOFT_THRESHOLD + 0.1 }

          describe 'when the the Cloudmark score is equal to or over the obvious spam threshold' do
            before { @mail.headers['X-CMAE-Score'] = 100.0 }

            it 'logs and records the multiplier ignore metric' do
              statsd_client.expects(:increment).with('spam_processor.ignore_multiplier').once
              @mail.logger.expects(:info).with(regexp_matches(/Resetting multiplier/)).once

              execute
            end

            it 'resets the spam multiplier to 1.0' do
              execute

              assert_equal 1.0, spam_processor.send(:multiplier)
            end

            describe_with_and_without_arturo_enabled :email_rspamd do
              it { gets_suspended_by(SuspensionType.SPAM) }
            end
          end

          describe 'when the the Cloudmark score is under the obvious spam threshold' do
            before { @mail.headers['X-CMAE-Score'] = 0.0 }

            it 'does not log or record the multiplier ignore metric' do
              statsd_client.expects(:increment).with('spam_processor.ignore_multiplier').never
              @mail.logger.expects(:info).with(regexp_matches(/Resetting multiplier/)).never

              execute
            end

            it 'does not reset the spam multiplier to 1.0' do
              execute

              assert_operator 1.0, :<, spam_processor.send(:multiplier)
            end

            describe_with_and_without_arturo_enabled :email_rspamd do
              it { does_not_get_suspended }
            end
          end
        end

        describe 'when the Rspamd score is less than the soft threshold' do
          before { @mail.headers['X-Spam-Zendesk-Score'] = subject::HARD_THRESHOLD * subject::SOFT_THRESHOLD - 0.1 }

          describe 'when the the Cloudmark score is equal to or over the obvious spam threshold' do
            before { @mail.headers['X-CMAE-Score'] = 100.0 }

            it 'does not log or record the multiplier ignore metric' do
              statsd_client.expects(:increment).with('spam_processor.ignore_multiplier').never
              @mail.logger.expects(:info).with(regexp_matches(/Resetting multiplier/)).never

              execute
            end

            it 'does not reset the spam multiplier to 1.0' do
              execute

              assert_operator 1.0, :<, spam_processor.send(:multiplier)
            end

            describe_with_and_without_arturo_enabled :email_rspamd do
              it { does_not_get_suspended }
            end
          end

          describe 'when the the Cloudmark score is under the obvious spam threshold' do
            before { @mail.headers['X-CMAE-Score'] = 0.0 }

            it 'does not log or record the multiplier ignore metric' do
              statsd_client.expects(:increment).with('spam_processor.ignore_multiplier').never
              @mail.logger.expects(:info).with(regexp_matches(/Resetting multiplier/)).never

              execute
            end

            it 'does not reset the spam multiplier to 1.0' do
              execute

              assert_operator 1.0, :<, spam_processor.send(:multiplier)
            end

            describe_with_and_without_arturo_enabled :email_rspamd do
              it { does_not_get_suspended }
            end
          end
        end

        describe 'when Rspamd and Cloudmark headers and other info to be logged are missing' do
          before do
            @mail.headers['X-Spam-Zendesk-Score'] = nil
            @mail.headers['X-CMAE-Score'] = nil
            @mail.headers['X-Rspamd-Authentication-Results'] = nil
            @mail.headers['X-Rspamd-Status'] = nil
            @mail.subject = nil
            @state.plain_content = nil
          end

          describe_with_and_without_arturo_enabled :email_rspamd do
            it 'does not blow up with exceptions' do
              execute
            end
          end
        end
      end

      describe_with_arturo_disabled :email_ignore_spam_multiplier do
        describe_with_arturo_enabled :email_ignore_spam_multiplier_logging do
          describe 'when conditions are such that the multiplier would be reset to 1.0 if :email_ignore_spam_multiplier were enabled' do
            before do
              @mail.headers['X-Spam-Zendesk-Score'] = subject::HARD_THRESHOLD * subject::SOFT_THRESHOLD + 0.1
              @mail.headers['X-CMAE-Score'] = 100.0
            end

            it 'records logs and records a log-only mode metric' do
              statsd_client.expects(:increment).with('spam_processor.ignore_multiplier_log_only').once
              @mail.logger.expects(:info).with(regexp_matches(/LOG-ONLY: Cloudmark and Rspamd both detect spam/)).once

              execute
            end

            it 'does not log or record the actual multiplier ignore metric' do
              statsd_client.expects(:increment).with('spam_processor.ignore_multiplier').never
              @mail.logger.expects(:info).with(regexp_matches(/Resetting multiplier/)).never

              execute
            end

            it 'does not reset the spam multiplier to 1.0' do
              execute

              assert_operator 1.0, :<, spam_processor.send(:multiplier)
            end

            describe_with_and_without_arturo_enabled :email_rspamd do
              it { does_not_get_suspended }
            end
          end
        end

        describe_with_arturo_disabled :email_ignore_spam_multiplier_logging do
          describe 'when conditions are such that the multiplier would be reset to 1.0 if :email_ignore_spam_multiplier were enabled' do
            before do
              @mail.headers['X-Spam-Zendesk-Score'] = subject::HARD_THRESHOLD * subject::SOFT_THRESHOLD + 0.1
              @mail.headers['X-CMAE-Score'] = 100.0
            end

            it 'does not log anything or record any metrics, log-only or otherwise' do
              statsd_client.expects(:increment).with('spam_processor.ignore_multiplier_log_only').never
              statsd_client.expects(:increment).with('spam_processor.ignore_multiplier').never
              @mail.logger.expects(:info).with(regexp_matches(/LOG-ONLY: Cloudmark and Rspamd both detect spam/)).never
              @mail.logger.expects(:info).with(regexp_matches(/Resetting multiplier/)).never
            end

            it 'does not reset the spam multiplier to 1.0' do
              execute

              assert_operator 1.0, :<, spam_processor.send(:multiplier)
            end

            describe_with_and_without_arturo_enabled :email_rspamd do
              it { does_not_get_suspended }
            end
          end
        end
      end
    end

    describe 'when the email is not set to be threaded as a comment on an existing ticket' do
      before { @state.ticket = nil }

      describe_with_and_without_arturo_enabled :email_ignore_spam_multiplier do
        describe_with_and_without_arturo_enabled :email_ignore_spam_multiplier_logging do
          describe 'when conditions are such that the multiplier would be reset to 1.0 if :email_ignore_spam_multiplier were enabled and state.ticket were not nil' do
            before do
              @mail.headers['X-Spam-Zendesk-Score'] = subject::HARD_THRESHOLD * subject::SOFT_THRESHOLD + 0.1
              @mail.headers['X-CMAE-Score'] = 100.0
            end

            it 'does not log anything or record any metrics, log-only or otherwise' do
              statsd_client.expects(:increment).with('spam_processor.ignore_multiplier_log_only').never
              statsd_client.expects(:increment).with('spam_processor.ignore_multiplier').never
              @mail.logger.expects(:info).with(regexp_matches(/LOG-ONLY: Cloudmark and Rspamd both detect spam/)).never
              @mail.logger.expects(:info).with(regexp_matches(/Resetting multiplier/)).never
            end

            it 'does not reset the spam multiplier to 1.0' do
              execute

              assert_operator 1.0, :<, spam_processor.send(:multiplier)
            end

            describe_with_and_without_arturo_enabled :email_rspamd do
              it { does_not_get_suspended }
            end
          end
        end
      end
    end
  end
end
