require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::ValidatingProcessor do
  include MailTestHelper
  include ArturoTestHelper

  fixtures :accounts, :brands

  def execute
    Zendesk::InboundMail::Processors::ValidatingProcessor.execute(@state, @mail)
  end

  before do
    @state = Zendesk::InboundMail::ProcessingState.new
    @account = accounts('minimum')
    @author = @account.owner
    Account.any_instance.stubs(:default_brand).returns(brands('minimum'))
    @state.account = @account
    @state.author = @author
    @state.from = Zendesk::Mail::Address.new(name: "MailingListGuy", address: @author.email)
    @mail = Zendesk::Mail::InboundMessage.new
    @mail.body = 'Not blank'
  end

  it "suspends when recipient matches account's noreply address" do
    @state.recipient = "noreply@#{@state.account.default_host}"
    execute

    assert_equal(SuspensionType.TO_NOREPLY, @state.suspension)
  end

  describe "hard rules" do
    before do
      # hard rules run even if the message is whitelisted, soft rules do not
      # whitelisting the message makes sure the soft rules aren't carrying the load here
      @state.whitelisted = true
    end

    it "suspends messages delivered From mail delivery system users" do
      ['postmaster@example.com', 'PostMaster@example.com'].each do |user|
        @mail.from = address(user)
        execute

        assert_equal SuspensionType.SYSTEM_USER, @state.suspension, "failed to catch #{user}"
        @state.suspension = nil
      end
    end

    [
      "Out of Office", "Out of the Office", "out of the office", "Auto Response", "Email received",
      "Auto-Reply", "Autosvar", "Holiday Message", "John Dolk træffes ikke.", "Automatic reply", "Fuera de Oficina",
      "out-of-office"
    ].each do |subject|
      it "suspends messages with Subjects indicating vacation auto responses #{subject}" do
        @mail.subject = subject
        execute
        assert_equal SuspensionType.AUTO_VACATION, @state.suspension, "failed to catch #{subject}"
        @state.suspension = nil
      end
    end

    ["sign out of Office365", "out of office365"].each do |subject|
      it "doesn't suspend messages with subjects similar to out-of-office responses: #{subject}" do
        @mail.subject = subject
        execute
        assert_nil @state.suspension, "incorrectly suspended: #{subject}"
      end
    end

    describe "undeliverable" do
      [
        "Undelivered Mail Returned",
        "Automatic Response",
        "Delivery Status Notification (Failure)",
        "Delivery Failure",
        "failure notice",
        "Returned mail",
        "Undeliverable mail",
        "Mail delivery failed",
        "mail delivery failed",
        "NDN:",
      ].each do |subject|
        it "suspend messages with Subjects indicating automated delivery failure #{subject}" do
          @mail.subject = subject
          execute
          assert_equal SuspensionType.AUTO_DELIVERY_FAILURE, @state.suspension, "failed to catch #{subject}"
        end
      end
    end

    it 'suspends messages with non blank X-Autoreply set' do
      @mail.headers["X-AutoReply"] = "yes"
      execute

      assert_equal SuspensionType.AUTO_MAILER, @state.suspension
    end

    describe_with_arturo_disabled :email_suspend_mail_from_auto_response do
      it "doesn't suspend emails with X-Auto-Response-Suppress header" do
        @mail.headers["X-Auto-Response-Suppress"] = "yes"
        execute
        assert @state.suspension.nil?
      end
    end

    it 'suspends messages with non blank X-Auto-Response-Suppress set' do
      @mail.headers["X-Auto-Response-Suppress"] = "yes"
      execute
      assert_equal SuspensionType.AUTO_MAILER, @state.suspension
    end

    describe_with_arturo_disabled :email_suspend_mail_from_intercom do
      it "doesn't suspend emails from Intercom" do
        @mail.headers["X-Intercom-Bin"] = "anything"
        execute
        assert @state.suspension.nil?
      end
    end

    it 'suspends notifications from Intercom' do
      @mail.headers["X-Intercom-Bin"] = "notification"
      execute
      assert_equal SuspensionType.AUTO_MAILER, @state.suspension
    end

    it 'should not suspend valid emails from Intercom' do
      @mail.headers["X-Intercom-Bin"] = "anything"
      execute
      assert @state.suspension.nil?
    end

    it 'suspends messages with non blank X-Autorespond set' do
      @mail.headers["X-Autorespond"] = "yes"
      execute

      assert_equal SuspensionType.AUTO_MAILER, @state.suspension
    end

    it 'suspends messages with non blank X-Autoresponder set' do
      @mail.headers["X-Autoresponder"] = "yes"
      execute

      assert_equal SuspensionType.AUTO_MAILER, @state.suspension
    end

    it 'suspends messages with a X-NetSuite header' do
      @mail.headers["X-NetSuite"] = "c=3777083; s=be10002.bos; v=2015.2.0.57; t=CaptureMessageReply; ec=false"
      execute

      assert_equal SuspensionType.AUTO_MAILER, @state.suspension
    end
  end

  describe "soft rules" do
    describe 'with account that has reject_bounced_mail_from_amazon Arturo flag' do
      before do
        @mail.headers["X-Amazon-Auto-Reply"] = "true"
        @account.stubs(has_reject_bounced_mail_from_amazon?: true)
      end

      it 'suspends Amazon bounces' do
        execute
        assert_equal SuspensionType.AUTO_DELIVERY_FAILURE, @state.suspension
      end
    end

    describe 'with account that does not have reject_bounced_mail_from_amazon Arturo flag' do
      before do
        @mail.headers["X-Amazon-Auto-Reply"] = "true"
        @account.stubs(has_reject_bounced_mail_from_amazon?: false)
      end

      it 'does not suspend Amazon bounces' do
        execute
        assert @state.suspension.nil?
      end
    end

    [
      "daemon@example.com",
      "no-reply@example.com",
      "Auto-reply from wswanson@ns.sympatico.ca <wswanson@ns.sympatico.ca>",
      "postmaster@example.com",
      "noreply@example.com",
      "wowcall@wowcall.com",
      "bandwidth@pixiepoppins.org",
      "helpdesk@textcu.be",
      "Smeege  via Freedcamp <welcome@freedcamp.com>",
      '"Doodle" <mailer@doodle.com>',
    ].each do |system_user|
      it "suspends From automated system user #{system_user}" do
        @mail.from = Zendesk::Mail::Address.parse(system_user)
        execute
        assert_equal SuspensionType.SYSTEM_USER, @state.suspension, "Failed to catch #{system_user}"
        @state.suspension = nil
      end
    end

    describe_with_arturo_disabled :email_suspend_mail_from_welcome_freedcamp do
      it "doesn't suspend emails from welcome@freedcamp.com" do
        @mail.from = Zendesk::Mail::Address.parse("Smeege  via Freedcamp <welcome@freedcamp.com>")
        execute
        assert @state.suspension.nil?
      end
    end

    describe_with_arturo_disabled :email_suspend_mail_from_mailer_doodle do
      it "doesn't suspend emails from mailer@doodle.com" do
        @mail.from = Zendesk::Mail::Address.parse('"Doodle" <mailer@doodle.com>')
        execute
        assert @state.suspension.nil?
      end
    end

    ["Kayako SupportSuite v3.04.10", "Benchmail Agent", "Concentric Autoresponder (2.0)"].each do |agent|
      it "suspends from known automailer #{agent}" do
        @state.suspension = nil
        assert @state.suspension.nil?

        @mail.mailer = agent
        execute
        assert_equal SuspensionType.AUTO_MAILER, @state.suspension, "Failed to catch #{agent}"
      end
    end

    ["iobox.fi", "iobox.se"].each do |domain|
      it "suspends from #{domain} because they keep getting us blacklisted by SORBS" do
        @mail.headers["From"] = [address("hello@#{domain}")]
        execute
        assert_equal SuspensionType.BLOCKLISTED, @state.suspension
      end
    end

    it "suspends from Ticket automailers" do
      @mail.headers["RT-Ticket"] = "asdfsd"
      execute
      assert_equal SuspensionType.AUTO_MAILER, @state.suspension
    end

    it "does not suspend with blank ticket automailer" do
      @mail.headers["RT-Ticket"] = ""
      execute
      assert_nil @state.suspension
    end

    it "suspends cmWinServer001" do
      @mail.message_id = '<4b9506cfcc583_9c02b0080e487e@cmWinServer001.foo>'
      execute
      assert_equal SuspensionType.AUTO_MAILER, @state.suspension
    end

    describe "Auto-Submitted" do
      it "suspends auto-submitted messages" do
        @mail.headers["Auto-Submitted"] = "yes"
        execute
        assert_equal SuspensionType.AUTO_MAILER, @state.suspension
      end

      it "suspends auto-submitted messages with more text" do
        @mail.headers["Auto-Submitted"] = "certainly"
        execute
        assert_equal SuspensionType.AUTO_MAILER, @state.suspension
      end

      it "does not suspend auto-submitted messages with no" do
        @mail.headers["Auto-Submitted"] = "nO"
        execute
        assert_nil @state.suspension
      end

      it "does not suspend auto-submitted messages with blank" do
        @mail.headers["Auto-Submitted"] = ""
        execute
        assert_nil @state.suspension
      end

      it "does not suspend when marked as machine" do
        @mail.headers["Auto-Submitted"] = "yes"
        @mail.headers["X-Zendesk-From-Account-Id"] = 'bla'
        execute
        assert_nil @state.suspension
      end

      # This is a bummer, but it impacts a lot of customers.
      describe "from outlook.com" do
        before { @mail.message_id = "<26a570b@DB.eurprd03.prod.outlook.com>" }

        it "ignores the auto generated flag" do
          @mail.headers["Auto-Submitted"] = "auto-generated"
          execute
          assert_nil @state.suspension
        end

        it "does not ignore the auto replied flag" do
          @mail.headers["Auto-Submitted"] = "auto-replied"
          execute
          assert_equal SuspensionType.AUTO_MAILER, @state.suspension
        end
      end

      describe "from exchangelabs.com" do
        before { @mail.message_id = "<26a570b@DB.eurprd03.prod.exchangelabs.com>" }

        it "ignores the auto generated flag" do
          @mail.headers["Auto-Submitted"] = "auto-generated"
          execute
          assert_nil @state.suspension
        end

        it "does not ignore the auto replied flag" do
          @mail.headers["Auto-Submitted"] = "auto-replied"
          execute
          assert_equal SuspensionType.AUTO_MAILER, @state.suspension
        end
      end

      describe "when suspend_automated_inbound_emails_from_zendesks is enabled" do
        before do
          Account.any_instance.stubs(:has_suspend_automated_inbound_emails_from_zendesks?).returns(true)
        end

        def execute_with_delivery_context(header_value)
          @mail.headers["X-Delivery-Context"] = header_value
          execute
        end

        def assert_suspends_with_delivery_context(header_value)
          execute_with_delivery_context(header_value)
          assert_equal SuspensionType.FROM_ZENDESK_ACCOUNT, @state.suspension
        end

        def refute_suspends_with_delivery_context(header_value)
          execute_with_delivery_context(header_value)
          assert_not_equal SuspensionType.FROM_ZENDESK_ACCOUNT, @state.suspension
        end

        describe "when mail was sent from a Zendesk" do
          before do
            @mail.headers["X-Mailer"] = "Zendesk Mailer"
            @mail.headers["X-Zendesk-From-Account-Id"] = Zendesk::Mail.obfuscate_id(123)
          end

          it "suspends verification emails from another Zendesk" do
            assert_suspends_with_delivery_context("verify-email-33712181")
          end

          it "suspends password reset emails from another Zendesk" do
            assert_suspends_with_delivery_context("new-password-284904896")
          end

          it "suspends device login emails from another Zendesk" do
            assert_suspends_with_delivery_context("device-299662775")
          end

          it "suspends password changed by admin emails from another Zendesk" do
            assert_suspends_with_delivery_context("user-profile-crypted-password-changed-378245359")
          end

          it "does not suspend with other delivery context values" do
            refute_suspends_with_delivery_context("other")
          end
        end

        describe "when mail was not sent from another Zendesk" do
          it "does not suspend" do
            refute_suspends_with_delivery_context("verify-email-33712181")
          end
        end
      end
    end
  end

  it "does not suspend a well intended email" do
    mail = FixtureHelper::Mail.read('minimum_ticket.json')
    Zendesk::InboundMail::Processors::ValidatingProcessor.execute(@state, mail)
    refute @state.suspended?
  end

  it "suspends out of office emails" do
    mail = FixtureHelper::Mail.read('out_of_office.json')
    Zendesk::InboundMail::Processors::ValidatingProcessor.execute(@state, mail)
    assert @state.suspended?
  end

  it "suspends blank emails" do
    mail = FixtureHelper::Mail.read('blank.json')
    Zendesk::InboundMail::Processors::ValidatingProcessor.execute(@state, mail)

    assert_equal SuspensionType.BLANK, @state.suspension
  end

  it "does not suspend blank emails with non-blank parts" do
    mail = FixtureHelper::Mail.read('google_apps_group.json')
    mail.subject = nil
    assert mail.body.blank?

    Zendesk::InboundMail::Processors::ValidatingProcessor.execute(@state, mail)
    refute @state.suspended?
  end

  it "does not suspend Google Groups emails" do
    mail = FixtureHelper::Mail.read('google_apps_group.json')
    Zendesk::InboundMail::Processors::ValidatingProcessor.execute(@state, mail)
    refute @state.suspended?
  end

  it "suspends Google Groups emails if there's an X-Autorespond header" do
    mail = FixtureHelper::Mail.read('google_apps_group.json')
    mail.headers["X-Autorespond"] = "My name is Fred"
    Zendesk::InboundMail::Processors::ValidatingProcessor.execute(@state, mail)
    assert @state.suspended?
  end

  describe "bulk mail" do
    ["junk", "bulk", "list", "auto_reply"].each do |bulk_mail|
      before { @mail.headers["Precedence"] = bulk_mail }

      it "suspends #{bulk_mail} mail" do
        execute
        assert(@state.suspended?)
      end
    end

    it "suspends email list bulk mail that's not from Google" do
      execute
      assert(@state.suspended?)
    end

    it "suspends email list bulk mail that's not from a permitted bulk mailer" do
      @mail.headers["message-id"] = ["<123456@mail333.us4.mandrillapp.com>"]
      @mail.expects(:from_google_apps_group?).returns(false)
      execute
      assert_equal false, @state.suspended?
    end

    it "does not suspend when whitelisted" do
      @mail.headers["Precedence"] = "bulk"
      @state.stubs(:whitelisted?).returns(true)

      assert_equal false, @state.suspended?
    end
  end

  describe "#filter" do
    let(:state) { Zendesk::InboundMail::ProcessingState.new }
    let(:mail) { Zendesk::Mail::InboundMessage.new }
    let(:account) { accounts('minimum') }
    let(:rules) { Zendesk::InboundMail::ValidationRule.soft_rules }
    let(:validating_processor) { Zendesk::InboundMail::Processors::ValidatingProcessor.new(state, mail) }

    before do
      mail.x_mailer = 'Autoresponder'
      validating_processor.stubs(account: account)
    end

    it "suspends the ticket" do
      validating_processor.send(:filter, rules)
      assert state.suspended?
    end
  end
end
