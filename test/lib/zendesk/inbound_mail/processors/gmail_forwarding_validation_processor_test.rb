require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::GmailForwardingValidationProcessor do
  fixtures :accounts, :recipient_addresses, :tokens

  before do
    @account            = accounts(:minimum)
    @mail               = FixtureHelper::Mail.read("gmail_forwarding_verification.json")
    @from_address       = "forw_source@gmail.com" # value from fixture file
    @verification_token = "693586521" # value from fixture file

    @state              = Zendesk::InboundMail::ProcessingState.new
    @state.account      = @account
    @state.message_id   = @mail.message_id
    @state.from         = Zendesk::Mail::Address.new(name: "Google No Reply", address: "forwarding-noreply@google.com")
  end

  it "does not raise and reject google forwarding code email" do
    Zendesk::InboundMail::Processors::GmailForwardingValidationProcessor.execute(@state, @mail)
    assert @state.whitelisted?
    assert_equal "noreply@zendesk.com", @state.from.address
  end

  it "handles missing subject" do
    @mail.subject = nil
    Zendesk::InboundMail::Processors::GmailForwardingValidationProcessor.execute(@state, @mail)
    refute @state.whitelisted?
  end

  it "matches verification emails with subjects not starting and ending with token and email respectively" do
    @mail.subject = "Gmail vidarebefordran konfirmation: (#693586521) - forw_source@gmail.com sande dig en emajl"
    Zendesk::InboundMail::Processors::GmailForwardingValidationProcessor.execute(@state, @mail)
    assert @state.whitelisted?
    assert_equal "noreply@zendesk.com", @state.from.address
  end

  it "matches verification emails with subjects that contain the code without the # symbol in front" do
    @mail.subject = "(252681546) Bestätigen der Weiterleitung aus Acme GmbH - E-Mail-Empfang von info@acme.com"
    Zendesk::InboundMail::Processors::GmailForwardingValidationProcessor.execute(@state, @mail)
    assert @state.whitelisted?
    assert_equal "noreply@zendesk.com", @state.from.address
  end

  it "matches verification emails with subjects that contain the code with '# ' in front" do
    @mail.subject = "(# 450355567) Подтверждение пересылки something.com – получение почты с адреса hello@something.com."
    Zendesk::InboundMail::Processors::GmailForwardingValidationProcessor.execute(@state, @mail)
    assert @state.whitelisted?
    assert_equal "noreply@zendesk.com", @state.from.address
  end

  it "does not trigger processor for emails sent from different from email addresses" do
    @state.from.address = "<other@google.com>"
    Zendesk::InboundMail::Processors::GmailForwardingValidationProcessor.execute(@state, @mail)
    refute @state.whitelisted?
  end

  it "does not trigger on subjects not containing expected verification token" do
    @mail.subject = "(#AD444FSAF) Gmail Forwarding Confirmation - Receive Mail from forw_source@gmail.com"
    Zendesk::InboundMail::Processors::GmailForwardingValidationProcessor.execute(@state, @mail)
    refute @state.whitelisted?
  end

  it 'stores forwarding token data ' do
    Zendesk::InboundMail::Processors::GmailForwardingValidationProcessor.execute(@state, @mail)

    result = @account.forwarding_verification_tokens.where(from_email: @from_address)
    assert_equal 1, result.size

    assert_equal @verification_token, result.first.value
    assert_equal @from_address, result.first.from_email
    assert_equal @verification_token, result.first.value
  end
end
