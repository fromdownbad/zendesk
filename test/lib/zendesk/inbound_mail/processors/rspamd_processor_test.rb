require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::RspamdProcessor do
  include ArturoTestHelper
  include MailTestHelper

  subject { Zendesk::InboundMail::Processors::RspamdProcessor }

  let(:state) { Zendesk::InboundMail::ProcessingState.new }
  let(:mail)  { Zendesk::Mail::InboundMessage.new }
  let(:account) { accounts('minimum') }
  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }
  let(:logger) { mail.logger }
  let(:rspamd_reject_threshold) { Zendesk::InboundMail::SpamHelper::RSPAMD_REJECT_THRESHOLD }
  let(:rspamd_suspend_threshold) { Zendesk::InboundMail::SpamHelper::RSPAMD_SUSPEND_THRESHOLD }

  before do
    statsd_client.stubs(:increment)
    Zendesk::StatsD::Client.stubs(:new).with(namespace: 'mail_ticket_creator').returns(statsd_client)

    state.account = account
    state.account.stubs(subdomain: 'example')
    state.account.stubs(spam_threshold_multiplier: 1.0)

    state.stubs(whitelisted?: false)
    mail.account = account
  end

  def execute
    subject.execute(state, mail)
  end

  def does_not_get_suspended
    execute
    assert_nil state.suspension
  end

  def gets_suspended_by(suspension_type)
    execute
    assert_equal suspension_type, state.suspension
  end

  def gets_hard_rejected
    assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
  end

  def records_processor_skipped_metric
    statsd_client.expects(:increment).with('rspamd_processor.disabled').once

    execute
  end

  before do
    statsd_client.stubs(:increment)
    Zendesk::StatsD::Client.stubs(:new).with(namespace: 'mail_ticket_creator').returns(statsd_client)
  end

  # TODO: Update this testing strategy after moving suspend/reject logic to SpamResultsProcessor
  describe_with_arturo_disabled :email_new_spam_processors do
    describe 'when the email has an Rspamd score above the hard rejection threshold' do
      before { mail.headers['X-Spam-Zendesk-Score'] = rspamd_reject_threshold + 0.1 }

      it { does_not_get_suspended }
    end
  end

  describe_with_arturo_enabled :email_new_spam_processors do
    describe_with_and_without_arturo_enabled :email_rspamd do
      describe_with_arturo_enabled :email_skip_spam_processor do
        describe 'when the email has an Rspamd score above the hard rejection threshold' do
          before { mail.headers['X-Spam-Zendesk-Score'] = rspamd_reject_threshold + 0.1 }

          it { does_not_get_suspended }

          it { records_processor_skipped_metric }
        end

        describe 'when the email has an Rspamd score above the suspension threshold' do
          before { mail.headers['X-Spam-Zendesk-Score'] = rspamd_suspend_threshold + 0.1 }

          it { does_not_get_suspended }

          it { records_processor_skipped_metric }
        end

        describe 'when the email has an Rspamd score below the suspension threshold' do
          before { mail.headers['X-Spam-Zendesk-Score'] = rspamd_suspend_threshold - 0.1 }

          it { does_not_get_suspended }

          it { records_processor_skipped_metric }
        end
      end
    end

    describe 'when the email is from Mail Fetcher (Gmail Connector)' do
      before do
        mail.headers[Zendesk::InboundMail::SpamHelper::SOURCE_HEADER] = Zendesk::InboundMail::SpamHelper::SOURCE_HEADER_VALUE_FOR_MAIL_FETCHER
      end

      it 'records a metric about skipping Mail Fetcher emails' do
        statsd_client.expects(:increment).with('rspamd_processor.skip_mail_fetcher_email').once

        execute
      end
    end

    describe 'with Rspamd enabled for the account' do
      describe_with_arturo_enabled :email_rspamd do
        describe 'with a multiplier of 1.0' do
          describe 'when the email has an Rspamd score above the hard rejection threshold' do
            before { mail.headers['X-Spam-Zendesk-Score'] = rspamd_reject_threshold + 0.1 }

            it { gets_hard_rejected }
          end

          describe 'when the email has an Rspamd score above the suspension threshold' do
            before { mail.headers['X-Spam-Zendesk-Score'] = rspamd_suspend_threshold + 0.1 }

            it { gets_suspended_by(SuspensionType.SPAM) }
          end

          describe 'when the email has an Rspamd score below the suspension threshold' do
            before { mail.headers['X-Spam-Zendesk-Score'] = rspamd_suspend_threshold - 0.1 }

            it { does_not_get_suspended }
          end
        end

        describe 'with a multiplier less than 1.0' do
          let(:multiplier_value) { 0.5 }
          before { subject.any_instance.stubs(:multiplier).returns(multiplier_value) }

          describe 'when the Rspamd score is greater than (multiplier * RSPAMD_REJECT_THRESHOLD)' do
            describe 'when the Rspamd score is greater than the RSPAMD_REJECT_THRESHOLD' do
              before { mail.headers['X-Spam-Zendesk-Score'] = rspamd_reject_threshold + 0.1 }

              it { gets_hard_rejected }
            end

            describe 'when the Rspamd score is less than the RSPAMD_REJECT_THRESHOLD' do
              before do
                mail.headers['X-Spam-Zendesk-Score'] = multiplier_value * rspamd_reject_threshold + 0.1

                # Ensure that this test is set up properly, even if RSPAMD_REJECT_THRESHOLD
                # and/or RSPAMD_SUSPEND_THRESHOLD are changed in the future
                assert mail.headers['X-Spam-Zendesk-Score'].first <= rspamd_reject_threshold
              end

              it 'gets suspended instead of hard rejected' do
                gets_suspended_by(SuspensionType.SPAM)
              end
            end
          end

          describe 'when the Rspamd score is less than (multiplier * RSPAMD_REJECT_THRESHOLD)' do
            describe 'when the Rspamd score is greater than (multiplier * RSPAMD_SUSPEND_THRESHOLD)' do
              before do
                mail.headers['X-Spam-Zendesk-Score'] = multiplier_value * rspamd_suspend_threshold + 0.1

                # Ensure that this test is set up properly, even if RSPAMD_REJECT_THRESHOLD
                # and/or RSPAMD_SUSPEND_THRESHOLD are changed in the future
                assert mail.headers['X-Spam-Zendesk-Score'].first <= rspamd_suspend_threshold
              end

              it { gets_suspended_by(SuspensionType.SPAM) }
            end

            describe 'when the Rspamd score is less than (multiplier * RSPAMD_SUSPEND_THRESHOLD)' do
              before do
                mail.headers['X-Spam-Zendesk-Score'] = multiplier_value * rspamd_suspend_threshold - 0.1
              end

              it { does_not_get_suspended }
            end
          end
        end

        describe 'with a multiplier greater than 1.0' do
          let(:multiplier_value) { 2.0 }
          before { subject.any_instance.stubs(:multiplier).returns(multiplier_value) }

          describe 'when the Rspamd score is greater than the RSPAMD_REJECT_THRESHOLD' do
            describe 'when the Rspamd score is greater than (multiplier * RSPAMD_REJECT_THRESHOLD)' do
              before { mail.headers['X-Spam-Zendesk-Score'] = multiplier_value * rspamd_reject_threshold + 0.1 }

              it { gets_hard_rejected }
            end

            describe 'when the Rspamd score is less than (multiplier * RSPAMD_REJECT_THRESHOLD)' do
              before do
                mail.headers['X-Spam-Zendesk-Score'] = multiplier_value * rspamd_reject_threshold - 0.1

                # Ensure that this test is set up properly, even if RSPAMD_REJECT_THRESHOLD
                # and/or RSPAMD_SUSPEND_THRESHOLD are changed in the future
                assert mail.headers['X-Spam-Zendesk-Score'].first > rspamd_reject_threshold
              end

              it { gets_suspended_by(SuspensionType.SPAM) }
            end
          end

          describe 'when the Rspamd score is greater than the RSPAMD_SUSPEND_THRESHOLD' do
            describe 'when the Rspamd score is greater than (multiplier * RSPAMD_SUSPEND_THRESHOLD)' do
              before do
                mail.headers['X-Spam-Zendesk-Score'] = multiplier_value * rspamd_suspend_threshold + 0.1

                # Ensure that this test is set up properly, even if RSPAMD_REJECT_THRESHOLD
                # and/or RSPAMD_SUSPEND_THRESHOLD are changed in the future
                assert mail.headers['X-Spam-Zendesk-Score'].first <= multiplier_value * rspamd_reject_threshold
              end

              it { gets_suspended_by(SuspensionType.SPAM) }
            end

            describe 'when the Rspamd score is less than (multiplier * RSPAMD_SUSPEND_THRESHOLD)' do
              before do
                mail.headers['X-Spam-Zendesk-Score'] = multiplier_value * rspamd_suspend_threshold - 0.1

                # Ensure that this test is set up properly, even if RSPAMD_REJECT_THRESHOLD
                # and/or RSPAMD_SUSPEND_THRESHOLD are changed in the future
                assert mail.headers['X-Spam-Zendesk-Score'].first > rspamd_suspend_threshold
              end

              it { does_not_get_suspended }
            end
          end
        end

        describe 'when the Rspamd score would be high enough to reject the email if the email were not whitelisted' do
          before do
            logger.stubs(:info)
            mail.headers['X-Spam-Zendesk-Score'] = rspamd_reject_threshold + 0.1
            state.stubs(:whitelisted?).returns(true)
          end

          ['account', 'organization', 'zopim'].each do |whitelist_type|
            describe "when the state is #{whitelist_type}" do
              before do
                state.stubs("#{whitelist_type}_whitelisted".to_sym).returns(true)
              end

              it 'suspends the email instead of rejecting' do
                gets_suspended_by(SuspensionType.SPAM)
              end
            end
          end
        end

        describe 'when the Rspamd score would be high enough to suspend - but not high enough to reject - the email if the email were not whitelisted' do
          before do
            logger.stubs(:info)
            mail.headers['X-Spam-Zendesk-Score'] = rspamd_suspend_threshold + 0.1
            state.stubs(:whitelisted?).returns(true)
          end

          ['account', 'organization', 'zopim'].each do |whitelist_type|
            describe "when the state is #{whitelist_type}" do
              before do
                state.stubs("#{whitelist_type}_whitelisted".to_sym).returns(true)
              end

              it { does_not_get_suspended }
            end
          end
        end
      end
    end

    describe 'with Rspamd disabled for the account' do
      describe_with_arturo_disabled :email_rspamd do
        before { mail.headers['X-Spam-Zendesk-Score'] = rspamd_suspend_threshold + 0.1 }

        describe 'when the email has a Cloudmark score header' do
          before { mail.headers['X-CMAE-Score'] = 0.0 }

          it 'ignores the Rspamd score' do
            does_not_get_suspended
          end
        end

        describe 'when the email is missing a Cloudmark score header' do
          before { mail.headers['X-CMAE-Score'] = nil }

          it 'falls back on the Rspamd score, taking whatever action it would if the :email_rspamd Arturo were enabled' do
            gets_suspended_by(SuspensionType.SPAM)
          end
        end
      end
    end
  end
end
