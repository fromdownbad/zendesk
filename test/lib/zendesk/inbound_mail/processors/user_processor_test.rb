require_relative "../../../../support/test_helper"

# Lines missing coverage:
# lib/zendesk/inbound_mail/processors/user_processor.rb:102
SingleCov.covered! uncovered: 1

describe Zendesk::InboundMail::Processors::UserProcessor do
  extend ArturoTestHelper

  include MailTestHelper

  fixtures :users, :organizations, :user_identities

  let(:state) { Zendesk::InboundMail::ProcessingState.new }
  let(:mail) { FixtureHelper::Mail.read('minimum_ticket.json') }

  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }
  let(:mail_logger) { Zendesk::InboundMail::MailLogger.any_instance }

  before do
    @account = state.account = accounts(:minimum)
    @author = state.account.owner
  end

  def execute
    Zendesk::InboundMail::Processors::UserProcessor.execute(state, mail)
  end

  def with_new_user
    state.account.stubs(:is_open?).returns(true)
    state.account.stubs(:is_signup_required?).returns(false)
    state.from = address("eric@example.com")
  end

  def with_new_intercom_user
    with_new_user
    state.from = address("notifications@intercom-mail.com")
    mail.reply_to = address("notifications+user_588a64eeefdf81492b977c1f_ff78279749ffb3c3b48f201ca95a4b520c061110@intercom-mail.com")
    mail.headers["X-Intercom-Bin"] = "notification"
  end

  def identity_for_email(identities, email)
    identities.find { |identity| identity.is_a?(UserEmailIdentity) && identity.value == email }
  end

  it "assigns existing author using from address" do
    state.from = address(@author.email)
    execute
    assert_equal @author, state.author
  end

  it "assigns new author using from address" do
    with_new_user
    execute

    assert state.author.new_record?
    assert state.author.is_a?(User)
    assert_equal 'eric@example.com', state.author.email
  end

  it "suspends if author is suspended" do
    User.any_instance.expects(:suspended?).returns(true)
    state.from = address(@author.email)
    execute

    assert_equal SuspensionType.SPAM, state.suspension
  end

  it "suspends if author organization is suspended" do
    @author = users(:minimum_end_user)
    Organization.any_instance.expects(:suspended?).returns(true)
    state.from = address(@author.email)
    execute

    assert_equal SuspensionType.SPAM, state.suspension
  end

  describe "Email Takeover Vulnerability" do
    before do
      mail_logger.stubs(:log)
      state.account = account
    end

    describe "when the author has a single email identity" do
      describe "and the mail is from a verified identity" do
        let(:account) { accounts(:multiproduct) }
        let(:author) { users(:end_user_with_single_identity) }
        let(:verified_identity) { user_identities(:single_email_identity).value }

        before { state.from = address(verified_identity) }

        describe_with_arturo_enabled(:email_takeover_vulnerability_stats) do
          it "logs that the author was found using a verified identity" do
            mail_logger.expects(:log).with("Total identities found: 1, verified: 1, unverified: 0")
            mail_logger.expects(:log).with("Author ##{author.id} found!")
            execute
          end
        end

        describe_with_arturo_disabled(:email_takeover_vulnerability_stats) do
          it "logs that the author was found" do
            mail_logger.expects(:log).with("Author ##{author.id} found!")
            execute
          end
        end
      end

      describe "and the mail is from an unverified identity" do
        let(:account) { accounts(:minimum) }
        let(:author) { users(:to_be_verified) }
        let(:unverified_identity) { user_identities(:to_be_verified).value }

        # Simulates when the From: address has a different case than the identity.
        before { state.from = address('Report_End_User1@Zendesk.com') }

        describe_with_arturo_enabled(:email_takeover_vulnerability_stats) do
          it "logs that the author was found using an unverified identity" do
            mail_logger.expects(:log).with("Total identities found: 1, verified: 0, unverified: 1")
            mail_logger.expects(:log).with("Author ##{author.id} found!")
            execute
          end
        end

        describe_with_arturo_disabled(:email_takeover_vulnerability_stats) do
          it "logs that the author was found" do
            mail_logger.expects(:log).with("Author ##{author.id} found!")
            execute
          end
        end
      end
    end

    describe "when the author has multiple email identities" do
      let(:account) { accounts(:multiproduct) }
      let(:author) { users(:end_user_with_multiple_identities) }
      let(:verified_identity) { author.email }
      let(:unverified_identity) { user_identities(:secondary_email_identity).value }

      describe "and the mail is from a verified identity" do
        before { state.from = address(verified_identity) }

        describe_with_arturo_enabled(:email_takeover_vulnerability_stats) do
          it "logs that the author was found using a verified identity" do
            mail_logger.expects(:log).with("Total identities found: 2, verified: 1, unverified: 1")
            mail_logger.expects(:log).with("The author's address '#{verified_identity}' is verified")
            mail_logger.expects(:log).with("Author ##{author.id} found!")
            execute
          end
        end

        describe_with_arturo_disabled(:email_takeover_vulnerability_stats) do
          it "logs that the author was found" do
            mail_logger.expects(:log).with("Author ##{author.id} found!")
            execute
          end
        end
      end

      describe "and the mail is from an unverified identity" do
        before { state.from = address(unverified_identity) }

        describe_with_arturo_enabled(:email_takeover_vulnerability_stats) do
          it "records the case using logs and metrics" do
            mail_logger.expects(:log).with("Total identities found: 2, verified: 1, unverified: 1")
            mail_logger.expects(:log).with("The author's address '#{unverified_identity}' is unverified")
            mail_logger.expects(:log).with("The author's address '#{unverified_identity}' is not the default email identity")
            mail_logger.expects(:log).with("Author ##{author.id} found, but matching an address that hasn't been verified: '#{unverified_identity}'")

            expected_tags = ["total:2", "total_verified:1", "total_unverified:1"]
            statsd_client.expects(:increment).with('user_processor.email_takeover_vulnerability.likely', tags: expected_tags).once
            mail_logger.expects(:warn).with("Possible email takeover vulnerability! Mail from '#{unverified_identity}' assigned to User ##{author.id} using '#{author.email}' as the default identity.")
            execute
          end
        end

        describe_with_arturo_disabled(:email_takeover_vulnerability_stats) do
          it "logs that the author was found" do
            mail_logger.expects(:log).with("Author ##{author.id} found!")
            execute
          end
        end
      end
    end
  end

  describe "building an unknown author" do
    before { with_new_user }

    describe "when the state is already suspended" do
      before do
        state.stubs(:suspended?).returns(true)
        state.suspension = SuspensionType.SPAM
        @end_user = @account.end_users.first
        state.from = address(@end_user.email)
      end

      it "stills set the author id" do
        execute
        assert_equal state.author.id, @end_user.id
      end

      it "does not override the existing reason for suspension" do
        state.expects(:suspension=).never
        execute
      end

      it "does not set state.author if the author is a new user" do
        state.from = address("foo@baz.com")
        state.stubs(:suspended_ticket?).returns(true)
        state.expects(:author=).with(nil)
        execute
      end
    end

    it "suspends if author is an automated system" do
      ['daemon', 'system administrator', 'postmaster'].each do |automated_system|
        state.expects(:author=).with(nil).once
        state.from = mail.from = address("#{automated_system}@example.com")
        execute

        assert_equal SuspensionType.SYSTEM_USER, state.suspension, "Failed on #{automated_system}"
      end
    end

    it "does not suspend if user is not an automated system" do
      state.from = mail.from = address("eric@example.com")
      execute

      assert_nil state.suspension
    end

    it "does not break if From mail header is missing" do
      mail.from = nil
      execute
    end

    it "suspends if account is closed" do
      state.account.stubs(:is_open?).returns(false)
      execute

      assert_equal SuspensionType.UNKNOWN_AUTHOR, state.suspension
    end

    it "suspends if account requires signup" do
      state.account.stubs(:is_signup_required?).returns(true)
      execute

      assert_equal SuspensionType.SIGNUP_REQUIRED, state.suspension
    end

    describe "if an agent forwarded this email" do
      before do
        state.agent_forwarded = true
      end

      it "does not suspend if account requires signup" do
        state.account.stubs(:is_signup_required?).returns(true)
        execute

        assert_nil state.suspension
      end
    end

    describe "Requester name length" do
      describe "when name is too short (less than 2 characters)" do
        before do
          with_new_user
          state.from.name = 'a'
          execute
        end

        it "prepends Unknown string to the name" do
          assert_equal "Unknown a", state.author.name
        end
      end

      describe "when name exceeds the limit" do
        let(:max_name_length) { Zendesk::Extensions::TruncateAttributesToLimit.limit(User.columns_hash.fetch('name')) }
        let(:requester_name) { ("a" * (max_name_length + 1)).to_s }
        let(:mail_logger) { Zendesk::InboundMail::MailLogger.any_instance }
        let(:logger) { stub("logger", :log) }

        before do
          Zendesk::InboundMail::MailLogger.stubs(new: logger)
          with_new_user
          state.from.name = requester_name
        end

        it "truncates the name" do
          execute
          assert_equal max_name_length, state.author.name.length
        end

        it "adds an audit flag" do
          logger.expects(:log).with("Truncating requester name since its length #{requester_name.length} exceeds limit #{max_name_length}.").once
          execute
          assert state.flagged?
          assert_equal AuditFlags.new([EventFlagType.REQUESTER_NAME_TRUNCATED]), state.flags
        end
      end
    end

    it "builds the author user" do
      execute
      author = state.author

      assert_equal @account, author.account
      assert_equal 'eric@example.com', author.email
      assert_equal 'Eric', author.name
      assert author.valid?
    end
  end

  describe "machine response" do
    support_addresses = %W[support@zendesk.com help@foo-123.#{Zendesk::Configuration.fetch(:host)}]

    def stub_from_address(from_address, clear_other_email_identities = false)
      @author.identities.email.delete_all if clear_other_email_identities
      @author.email = from_address
      state.from = address(from_address)
      Zendesk::InboundMail::ProcessingState.any_instance.stubs(:author).returns(@author)
      Zendesk::Mail::InboundMessage.any_instance.stubs(:from).returns(state.from)
    end

    [true, false].each do |primary_email_identity|
      support_addresses.each do |support_address|
        it "marks #{support_address} as a machine when it is #{primary_email_identity ? '' : 'not '}the primary email identity" do
          stub_from_address(support_address, primary_email_identity)
          execute
          if primary_email_identity
            assert @author.reload.machine?
          else
            assert identity_for_email(@author.reload.identities, support_address).machine?
          end
          assert state.flags.include?(EventFlagType.MACHINE_GENERATED)
        end
      end
    end

    it "does not mark zendesk employee accounts as a machine" do
      stub_from_address("jdoe@zendesk.com")
      execute
      refute @author.reload.machine?
      refute state.flagged?
    end

    it "does not mark collaboration emails as machine" do
      mail.message_id = "<c11n+a3778not-this-one8-8a30-6fe7e02c219a+e027cc13-ec26-4331-9e51-8f05cb9e1176@#{Zendesk::Configuration.fetch(:host)}>"
      stub_from_address("help@foo-123.#{Zendesk::Configuration.fetch(:host)}")
      execute
      @author.reload.wont_be :machine?
      state.wont_be :flagged?
    end

    describe "with from_zendesk_account_id header" do
      before { mail.headers["X-Zendesk-From-Account-Id"] = "blablub" } # just a hash, we don't know which account

      describe "when author is an existing user" do
        before { state.from = address(@author.email) }

        describe "machine user" do
          before { execute }

          it "assigns author to state" do
            assert_equal @author, state.author
          end

          it "marks author as a machine" do
            assert @author.reload.machine?
          end

          it "marks author email identity as a machine" do
            assert identity_for_email(state.author.reload.identities, @author.email).machine?
          end

          it "adds MACHINE_GENERATED flag" do
            assert state.flags.include?(EventFlagType.MACHINE_GENERATED)
          end
        end

        describe "when the To Zendesk has an active ticket sharing agreement" do
          before do
            Sharing::Agreement.any_instance.stubs(remote_account: stub(id: 123, subdomain: 'jackpot'))
            sharing_agreements = MiniTest::Mock.new
            accepted_sharing_agreements = [FactoryBot.build(:agreement, status_id: Sharing::Agreement::STATUS_MAP[:accepted], account: @account)]
            sharing_agreements.expect(:accepted, accepted_sharing_agreements)
            Account.any_instance.stubs(sharing_agreements: sharing_agreements)
            Account.any_instance.stubs(ticket_sharing_partner_support_addresses: [state.from.address.dup])
          end

          describe "when the mail is from the account that is ticket sharing with the To Zendesk" do
            before do
              from_account_obfuscated_id = Zendesk::Mail.obfuscate_id(123)
              mail.headers["X-Zendesk-From-Account-Id"] = from_account_obfuscated_id
            end

            describe "where user identity for address in mail.from exists" do
              it "marks identity as ticket sharing partner and hard rejects" do
                identity = @account.user_email_identities.where(value: state.from.address).first
                refute_equal DeliverableStateType.TICKET_SHARING_PARTNER, identity.deliverable_state
                assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
                identity.reload
                assert_equal DeliverableStateType.TICKET_SHARING_PARTNER, identity.deliverable_state
              end
            end
          end

          it "does not reject when the mail is from another unshared Zendesk account" do
            mail.headers["X-Zendesk-From-Account-Id"] = Zendesk::Mail.obfuscate_id(456)
            execute
          end

          it "does not reject when the mail is not from another Zendesk" do
            execute
          end
        end
      end

      describe "when author is a new user" do
        before do
          with_new_user
          execute
        end

        it "marks author as a machine" do
          assert state.author.machine?
        end

        it "author is a new record" do
          assert state.author.new_record?
        end

        it "marks author email identity as a machine" do
          assert identity_for_email(state.author.identities, state.from.address).machine?
        end

        it "adds MACHINE_GENERATED flag" do
          assert state.flags.include?(EventFlagType.MACHINE_GENERATED)
        end
      end
    end

    describe "without from_zendesk_account_id header" do
      describe "when author is an existing user" do
        before do
          state.from = address(@author.email)
          execute
        end

        it "assigns author to state" do
          assert_equal @author, state.author
        end

        it "does not mark author as a machine" do
          refute @author.reload.machine?
        end

        it "does not mark author email identity as a machine" do
          refute @author.identities.email.any?(&:machine?)
        end

        it "does not add MACHINE_GENERATED flag" do
          refute state.flags.include?(EventFlagType.MACHINE_GENERATED)
        end
      end

      describe "when author is a new user" do
        before do
          with_new_user
          execute
        end

        it "does not mark author as a machine" do
          refute state.author.machine?
        end

        it "does not mark author email identity as a machine" do
          refute state.author.identities.any? { |identity| identity.type == "UserEmailIdentity" && identity.machine? }
        end

        it "builds a new record" do
          assert state.author.new_record?
        end

        it "does not add MACHINE_GENERATED flag" do
          refute state.flags.include?(EventFlagType.MACHINE_GENERATED)
        end
      end
    end

    describe "from an external intercom machine user" do
      before do
        with_new_intercom_user
        execute
      end

      it "marks author as a machine" do
        assert state.author.machine?
      end

      it "author is a new record" do
        assert state.author.new_record?
      end

      it "adds MACHINE_GENERATED flag" do
        assert state.flags.include?(EventFlagType.MACHINE_GENERATED)
      end
    end

    describe "From address length" do
      let(:max_email_address_length) { Zendesk::InboundMail::Processors::UserProcessor::MAX_EMAIL_ADDRESS_LENGTH }
      let(:error_message_regex) { /Sender email address is over the limit of characters allowed/ }
      let(:domain) { '@z3nmail.com' }
      let(:logger) { stub("logger", :log) }

      before do
        Zendesk::InboundMail::MailLogger.stubs(new: logger)
        state.from = from_address
      end

      describe "when it exceeds the limit" do
        let(:from_address) { address("#{"0" * (max_email_address_length + 1 - domain.length)}#{domain}") }

        it "hard rejects the incoming email" do
          assert_raise(Zendesk::InboundMail::HardMailRejectException) do
            statsd_client.expects(:increment).with('user_processor.reject_from_address_too_long').once
            logger.expects(:warn).with(regexp_matches(error_message_regex), "rejecting").once
            execute
          end
        end
      end

      describe "when it is within the acceptable limit" do
        let(:from_address) { address("#{"0" * (max_email_address_length - domain.length)}#{domain}") }

        it "does not hard reject the incoming email" do
          statsd_client.expects(:increment).with('user_processor.reject_from_address_too_long').never
          Zendesk::InboundMail::Processors::BaseProcessor.any_instance.expects(:reject!).never
          execute
        end
      end
    end
  end

  describe "logging possible reply-to spoof" do
    let(:logger) { stub('logger', :info, :warn) }

    before do
      author = users(:minimum_end_user)
      state.from = address(author.email)
      state.original_author = users(:minimum_end_user)

      Zendesk::InboundMail::MailLogger.stubs(new: logger)
      logger.expects(:log).at_least_once
    end

    spoof_logging_conditions = [
      {possible_spoof_flag?: true, anonymous_sender?: true, account_open?: false, log_expected?: true},
      {possible_spoof_flag?: true, anonymous_sender?: true, account_open?: true, log_expected?: false},
      {possible_spoof_flag?: true, anonymous_sender?: false, account_open?: false, log_expected?: false},
      {possible_spoof_flag?: false, anonymous_sender?: false, account_open?: false, log_expected?: false},
      {possible_spoof_flag?: false, anonymous_sender?: true, account_open?: false, log_expected?: false},
      {possible_spoof_flag?: false, anonymous_sender?: false, account_open?: true, log_expected?: false},
    ]

    spoof_logging_conditions.each do |condition|
      it "when spoof flag: #{condition[:possible_spoof_flag?]}, anonymous sender: #{condition[:anonymous_sender?]}, account open: #{condition[:account_open?]}, then log expected: #{condition[:log_expected?]}" do
        state.flags << EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF if condition[:possible_spoof_flag?]
        state.account.stubs(:is_open?).returns(condition[:account_open?])
        state.original_author = nil if condition[:anonymous_sender?]

        expected_log_message = "Account is not open but anonymous user tried to impersonate as an agent and added a comment"
        if condition[:log_expected?]
          logger.expects(:log).with(expected_log_message).once
        else
          logger.expects(:log).with(expected_log_message).never
        end

        execute
      end
    end
  end
end
