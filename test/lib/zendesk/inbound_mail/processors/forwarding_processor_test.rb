require_relative "../../../../support/test_helper"
require_relative "../../../../support/agent_test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::ForwardingProcessor do
  extend ArturoTestHelper
  include AgentTestHelper

  fixtures :tickets, :recipient_addresses

  let(:account) { Account.new }

  before do
    # TicketProcessingWorker normally would apply the account to the "mail"/InboundMessage object
    Zendesk::Mail::InboundMessage.any_instance.stubs(account: account)
  end

  def from_agent
    @state.from = Zendesk::Mail::Address.new(address: @state.account.owner.email)
  end

  def from_possibly_spoofed_agent
    from_agent
    @state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF)
  end

  def from_support_address
    @mail.from = Zendesk::Mail::Address.new(address: @state.account.reply_address)
    @state.from = @mail.from
  end

  def from_wildcard_support_address
    @mail.from = Zendesk::Mail::Address.new(address: "something@#{@state.account.subdomain}.#{Zendesk::Configuration.fetch(:host)}")
    @state.from = @mail.from
  end

  def from_other_zendesk_address
    @mail.from = Zendesk::Mail::Address.new(address: "news@somewhere.#{Zendesk::Configuration.fetch(:host)}")
    @state.from = @mail.from
  end

  def enable_agent_forwarding
    @state.account.settings.enable(:agent_forwardable_emails)
    @state.account.save!
  end

  def execute
    Zendesk::InboundMail::Processors::ForwardingProcessor.execute(@state, @mail)
  end

  def instance
    Zendesk::InboundMail::Processors::ForwardingProcessor.new(@state, @mail)
  end

  def assert_suspended(state = SuspensionType.LOOP)
    execute
    assert_equal state, @state.suspension
  end

  def assert_delivered
    execute
    assert_nil @state.suspension
  end

  def self.normal_email_processing
    it "does not overwrite the mail's subject with original subject" do
      assert_equal 'Fwd: Thoughts?', @mail.subject
    end

    it "does not set original sender as requester" do
      assert_equal @state.from.address, @state.from.address
    end

    it "does not set agent_forwarded" do
      refute @state.agent_forwarded
    end
  end

  before do
    @mail  = FixtureHelper::Mail.read('forwards/apple_mail/apple_mail.json')
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = accounts(:minimum)
    @state.from = Zendesk::Mail::Address.new(address: "foo@bar.com")
    @state.plain_content = @mail.content.text

    Zendesk::InboundMail::Forward.any_instance.stubs(:body).returns('Forwarded message body')
    refute @state.account.settings.agent_forwardable_emails?
  end

  describe "finding last header key bug" do
    before do
      @mail = FixtureHelper::Mail.read('forwards/gmail/gmail.json')
      @state.plain_content = @mail.content.text
      enable_agent_forwarding
      from_agent
      @state.account.settings.stubs(rich_content_in_emails?: true)
    end

    describe_with_arturo_disabled :email_find_last_header_key_after_first do
      it "leaves part of the agent comment in public comment content" do
        execute
        assert @state.html_content.include? "Should also read this."
      end
    end

    describe_with_arturo_enabled :email_find_last_header_key_after_first do
      it "removes all of the agent comment" do
        execute
        refute @state.html_content.include? "Should also read this."
      end
    end
  end

  describe "disabled" do
    describe "with forwards from agents" do
      before do
        from_agent
        execute
      end
      normal_email_processing
    end

    describe "with forward from normal address" do
      before do
        enable_agent_forwarding
        execute
      end
      normal_email_processing
    end
  end

  describe "agent_comment" do
    before do
      from_support_address
      Zendesk::InboundMail::Forward.any_instance.stubs(:comment).returns('Comment')
    end

    describe "when agent can comment publicly" do
      before { @state.stubs(agent: stub(can?: true)) }

      it "does not suspend the ticket" do
        execute
        assert_nil @state.suspension
      end

      describe_with_and_without_arturo_enabled :email_disable_light_agent_forwarding do
        it "sets for comment from agent" do
          execute
          assert_equal 'Comment', @state.agent_comment
        end
      end
    end

    describe "when agent can not comment publicly" do
      before { @state.stubs(agent: stub(can?: false)) }

      it "suspends the ticket" do
        execute
        assert_suspended SuspensionType.FROM_SUPPORT_ADDRESS
      end

      it "does not set comment from agent" do
        execute
        assert_nil @state.agent_comment
      end

      describe_with_arturo_disabled :email_disable_light_agent_forwarding do
        it "sets for comment from agent" do
          execute
          assert_equal 'Comment', @state.agent_comment
        end
      end
    end
  end

  describe "with agent forwarding" do
    before { enable_agent_forwarding }

    describe "with forwards from agents as new tickets" do
      before do
        from_agent
        @forwarded_message_body = 'Forwarded message body'
        Zendesk::InboundMail::Forward.any_instance.stubs(:body).returns(@forwarded_message_body)
      end

      describe "when agent can not comment publicly" do
        before { @state.stubs(agent: stub(can?: false)) }

        it "suspends the ticket" do
          execute
          assert_suspended SuspensionType.LIGHT_AGENT_FORWARDED
        end
      end

      it "overwrites the mail's subject with original subject" do
        execute
        assert @state.agent_forwarded
        assert @state.account.settings.agent_forwardable_emails?
        assert_equal 'Thoughts?', @mail.subject
      end

      it "sets content to original body" do
        execute
        assert_equal @forwarded_message_body, @state.plain_content
      end

      it "sets original sender as requester" do
        execute
        assert_equal 'walt@example.com', @state.from.address
      end

      it "sets agent as submitter" do
        execute
        agent = @state.account.owner
        assert_equal agent, @state.submitter, @state.inspect
      end

      it "works with empty subjects" do
        @mail.subject = 'Fwd:'
        execute

        assert_equal @forwarded_message_body, @state.plain_content
        assert_equal 'walt@example.com', @state.from.address
        assert_equal @state.account.owner, @state.submitter, @state.inspect
      end

      it "sets agent_forwarded" do
        execute
        assert @state.agent_forwarded
      end

      describe "when the forward is from a support address" do
        before do
          @original_from = @state.from
          Zendesk::InboundMail::Forward.any_instance.expects(:from).times(3).
            returns(Zendesk::Mail::Address.new(address: @state.account.reply_address))
          execute
        end

        it "does not change the from address" do
          assert_equal @state.from, @original_from
        end
      end

      describe "when the forward is not html" do
        let(:some_html) { "" }

        before { Zendesk::InboundMail::Forward.any_instance.stubs(:html_body).returns(some_html) }

        describe "when rich_content_in_emails setting is enabled" do
          before do
            @state.account.settings.stubs(rich_content_in_emails?: true)
          end

          it "logs a message about fallback" do
            logger = stub('logger', :log)
            logger.expects(:warn).with(regexp_matches(/Failing back to/))
            Zendesk::InboundMail::MailLogger.stubs(new: logger)
            execute
          end
        end
      end

      describe "when the forward is html" do
        let(:some_html) { "<div>some html</div>" }

        before { Zendesk::InboundMail::Forward.any_instance.stubs(:html_body).returns(some_html) }

        describe "when rich_content_in_emails setting is enabled" do
          before do
            @state.account.settings.stubs(rich_content_in_emails?: true)
          end

          it "sets html_content to original html_body" do
            execute
            assert_equal some_html, @state.html_content
          end
        end

        describe "when rich_content_in_emails setting is disabled" do
          it "does not set html_content" do
            execute
            assert_nil @state.html_content
          end
        end
      end
    end

    describe "with forwards from agents to existing" do
      before { from_agent }

      describe "ticket" do
        before do
          @state.ticket = tickets(:minimum_1)
          @original_state = @state.clone
          @original_subject = @mail.subject
          execute
        end

        it "dos nothing" do
          refute @state.agent_forwarded
          assert_equal @original_state.from, @state.from
          assert_equal @original_state.plain_content, @state.plain_content
          assert_equal @original_subject, @mail.subject
        end
      end

      describe "closed ticket" do
        before do
          @state.closed_ticket = tickets(:minimum_1)
          @original_state = @state.clone
          @original_subject = @mail.subject
          execute
        end

        it "dos nothing" do
          refute @state.agent_forwarded
          assert_equal @original_state.from, @state.from
          assert_equal @original_state.plain_content, @state.plain_content
          assert_equal @original_subject, @mail.subject
        end
      end
    end

    describe "as a agent with non-forwards from agents" do
      before do
        from_agent
        @state.plain_content = 'original content'
        @mail.subject = "Not a damn forward."
        @original_state = @state.clone
        @original_subject = @mail.subject
      end

      it "dos nothing" do
        execute
        refute @state.agent_forwarded
        assert_equal @original_state.from, @state.from
        assert_equal @original_state.plain_content, @state.plain_content
        assert_equal @original_subject, @mail.subject
      end

      describe "with invalid forwards" do
        before do
          @original_subject = @mail.subject = "Fwd: still not a forward"
          @state.plain_content = "HAHAHA"
        end

        it "dos nothing" do
          execute
          refute @state.agent_forwarded
          assert_equal @original_state.from,    @state.from
          assert_equal @original_subject,       @mail.subject
        end
      end
    end

    describe "with forwards from non-agents" do
      before do
        @original_subject = @mail.subject
        @state.plain_content = 'original content'
        @state.original_recipient_address = @state.account.owner.email
        @original_state = @state.clone
        execute
      end

      it "dos nothing" do
        refute @state.agent_forwarded
        assert_equal @original_subject, @mail.subject
        assert_equal @original_state.plain_content, @state.plain_content
        assert_equal @original_state.from, @state.from
      end
    end

    describe "with a possibly spoofed agent" do
      before do
        from_possibly_spoofed_agent
      end

      it "logs a message about possibly spoofed agent forward" do
        logger = stub('logger', :log)
        logger.expects(:log).with(regexp_matches(/Forwarded email is from an agent who was specified in the reply to, proceeding but could be a possible spoof/)).once
        Zendesk::InboundMail::MailLogger.stubs(new: logger)
        execute
      end
    end
  end

  describe "forwards from light agents/agents with private comments" do
    let(:light_agent) { create_light_agent }

    before do
      enable_agent_forwarding
      @state.from = Zendesk::Mail::Address.new(address: light_agent.email)
    end

    it "does not set agent_forwarded" do
      execute
      refute @state.agent_forwarded
    end

    describe_with_arturo_disabled :email_disable_light_agent_forwarding do
      it "sets agent_forwarded" do
        execute
        assert @state.agent_forwarded
      end

      it "logs a message about forwarding from light agents" do
        logger = stub('logger', :log)
        logger.expects(:log).with(regexp_matches(/Forwarded email is from an agent with no public comment access/))
        Zendesk::InboundMail::MailLogger.stubs(new: logger)
        execute
      end
    end
  end

  describe "from support address" do
    before { from_support_address }

    it "sets original sender as requester" do
      assert_delivered
      assert_equal 'walt@example.com', @state.from.address
    end

    it "suspends unforwarded emails" do
      @mail.subject = "Not a forward."
      assert_suspended SuspensionType.FROM_SUPPORT_ADDRESS
    end

    it "suspends agent-forwarded email if forward was from a support address" do
      Zendesk::InboundMail::Forward.any_instance.stubs(:from).returns(@mail.from)
      assert_suspended SuspensionType.FROM_SUPPORT_ADDRESS
    end
  end

  describe "from wildcard support address" do
    before { from_wildcard_support_address }

    it "suspends unforwarded emails" do
      @mail.subject = "Not a forward."
      assert_suspended SuspensionType.FROM_SUPPORT_ADDRESS
    end
  end

  describe "from other zendesk address" do
    before { from_other_zendesk_address }

    it "delivers unforwarded emails" do
      @mail.subject = "Not a forward."
      assert_delivered
    end
  end

  describe "#execute" do
    let(:is_forward) { false }

    before do
      @instance = instance
      @instance.stubs(:parse_forward).returns(is_forward)
    end

    describe "when email is a forward" do
      let(:is_forward) { true }

      it "attempts to suspend" do
        @instance.expects(:suspend_if_sender_is_account_address)
        @instance.execute
      end
    end

    describe "when email is not a forward" do
      it "attempts to suspend" do
        @instance.expects(:suspend_if_sender_is_account_address)
        @instance.execute
      end
    end
  end
end
