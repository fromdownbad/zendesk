require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::ReplyParserInstrumentationProcessor do
  let(:state) { Zendesk::InboundMail::ProcessingState.new }
  let(:mail)  { Zendesk::Mail::InboundMessage.new }
  let(:account) { accounts('minimum') }
  let(:ticket) { Ticket.new(account: account) }

  subject { Zendesk::InboundMail::Processors::ReplyParserInstrumentationProcessor }

  before { state.account = account }

  describe_with_arturo_enabled :email_instrument_delimiter do
    describe 'when ticket is present' do
      before { state.ticket = ticket }

      it 'instruments reply parsing' do
        Zendesk::ReplyParserInstrumentation.expects(:new).once.returns(stub(instrument: stub))
        subject.execute(state, mail)
      end

      describe 'and client is exempt from delimiter truncation' do
        before { mail.stubs(:client).returns(Zendesk::MtcMpqMigration::Content::CLIENTS_EXEMPT_FROM_DELIMITER_TRUNCATION.first) }

        it 'does not instrument reply parsing' do
          Zendesk::ReplyParserInstrumentation.expects(:new).never
          subject.execute(state, mail)
        end
      end

      describe_with_arturo_setting_enabled :no_mail_delimiter do
        it 'does not instrument reply parsing' do
          Zendesk::ReplyParserInstrumentation.expects(:new).never
          subject.execute(state, mail)
        end
      end

      describe_with_arturo_setting_disabled :no_mail_delimiter do
        it 'instruments reply parsing' do
          Zendesk::ReplyParserInstrumentation.expects(:new).once.returns(stub(instrument: stub))
          subject.execute(state, mail)
        end
      end
    end

    describe 'when ticket is absent' do
      it 'does not instrument reply parsing' do
        Zendesk::ReplyParserInstrumentation.expects(:new).never
        subject.execute(state, mail)
      end
    end
  end

  describe_with_arturo_disabled :email_instrument_delimiter do
    it 'does not instrument reply parsing' do
      Zendesk::ReplyParserInstrumentation.expects(:new).never
      subject.execute(state, mail)
    end

    describe_with_and_without_arturo_setting_enabled :no_mail_delimiter do
      it 'does not instrument reply parsing' do
        Zendesk::ReplyParserInstrumentation.expects(:new).never
        subject.execute(state, mail)
      end
    end
  end
end
