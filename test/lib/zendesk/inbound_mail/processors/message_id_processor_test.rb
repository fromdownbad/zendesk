require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::InboundMail::Processors::MessageIdProcessor do
  include ArturoTestHelper

  def execute
    Zendesk::InboundMail::Processors::MessageIdProcessor.execute(@state, @mail)
  end

  let(:logger) { stub("logger", info: true, warn: true) }

  before do
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.from = User.new
    @state.message_id = 12345
    @state.account    = accounts(:minimum)
    @state.from.stubs(:address).returns('user@example.zendesk.com')
    @mail = FixtureHelper::Mail.read('minimum_ticket.json')
    @mail.stubs(logger: logger)
  end

  it "rejects mail with duplicate message ids" do
    execute
    assert_raise(Zendesk::InboundMail::DuplicateMailRejectException) { execute }
  end

  it "does not reject mail with duplicate message ids when previous processing failed" do
    execute
    InboundEmail.any_instance.expects(:failed?).returns(true)
    refute_difference('InboundEmail.count(:all)') { execute }
    assert @state.inbound_email
  end

  it "rejects on mysql duplicate entry errors" do
    execute
    InboundEmail.expects(:find_by_sql).returns []
    assert_raises Zendesk::InboundMail::DuplicateMailRejectException do
      execute
    end.message.must_include "(race condition)"
  end

  describe_with_arturo_enabled :email_replace_message_id_null_string do
    before do
      Zendesk::Mail::InboundMessage.stubs(:generate_message_id).returns('54321')
    end

    it "replaces message_id with new generated string for 'null'" do
      @mail.message_id = '<null>'
      logger.expects(:info).with('Message ID was null, updating it to 54321')
      execute
      assert_equal '54321', @state.inbound_email.message_id
      assert_equal '54321', @state.message_id
    end

    it "replaces message_id with new generated string for '$null'" do
      @mail.message_id = '<$null>'
      logger.expects(:info).with('Message ID was null, updating it to 54321')
      execute
      assert_equal '54321', @state.inbound_email.message_id
      assert_equal '54321', @state.message_id
    end
  end

  describe_with_and_without_arturo_enabled :email_set_inbound_email_from_address do
    it "stores mail as an inbound email record" do
      @mail.in_reply_to = '<foo@bar.com>'
      @mail.references = ["<xxx@yyy.com>", "<aaa@bbb.com>"]
      assert_difference('InboundEmail.count(:all)') { execute }
      assert @state.inbound_email
      assert_equal '<foo@bar.com>', @state.inbound_email.in_reply_to
      assert_equal ['<xxx@yyy.com>', '<aaa@bbb.com>'], @state.inbound_email.references
    end

    it "does not store broken reply-to values that would match too many incoming mails" do
      @mail.in_reply_to = 'bla'
      assert_difference('InboundEmail.count(:all)') { execute }
      assert @state.inbound_email
      assert_nil @state.inbound_email.in_reply_to
    end

    it "does not store invalid and too long references" do
      @mail.references = ["<xxx@yyy.com>", "<x@y>", "<#{"a" * 1100}@bbb.com>"]
      assert_difference('InboundEmail.count(:all)') { execute }
      assert @state.inbound_email
      assert_equal ["<xxx@yyy.com>"], @state.inbound_email.references
    end
  end

  describe_with_arturo_disabled :email_set_inbound_email_from_address do
    it 'uses the state.from address to set the inbound_email from address' do
      assert_difference('InboundEmail.count(:all)') { execute }
      assert @state.inbound_email
      assert_equal 'user@example.zendesk.com', @state.inbound_email.from
    end
  end

  describe_with_arturo_enabled :email_set_inbound_email_from_address do
    it 'uses the mail.from address to set the inbound_email from address' do
      assert_difference('InboundEmail.count(:all)') { execute }
      assert @state.inbound_email
      assert_equal 'minimum_end_user@aghassipour.com', @state.inbound_email.from
    end
  end
end
