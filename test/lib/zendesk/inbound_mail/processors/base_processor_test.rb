require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 8

describe Zendesk::InboundMail::Processors::BaseProcessor do
  fixtures :accounts, :recipient_addresses

  let(:account) { accounts(:minimum) }
  let(:state) { Zendesk::InboundMail::ProcessingState.new(account) }
  let(:mail) { FixtureHelper::Mail.read('minimum_ticket.json') }
  let(:instance) { Zendesk::InboundMail::Processors::BaseProcessor.new(state, mail) }

  describe '.execute' do
    it "initializes new instance and sends 'execute' message to it" do
      Zendesk::InboundMail::Processors::BaseProcessor.any_instance.expects(:execute).once
      Zendesk::InboundMail::Processors::BaseProcessor.execute(state, mail)
    end
  end

  describe '#execute' do
    it "requires override" do
      base_processor = Zendesk::InboundMail::Processors::BaseProcessor.new(state, mail)
      assert_raise(NotImplementedError) { base_processor.execute }
    end
  end

  describe '#in_zendesk_subdomain?' do
    it "is true for an address in the account's subdomain" do
      assert instance.send(:in_zendesk_subdomain?, "foo@minimum.#{Zendesk::Configuration.fetch(:host)}")
    end

    it "is false for an address is another account's subdomain" do
      refute instance.send(:in_zendesk_subdomain?, "foo@withgroups.#{Zendesk::Configuration.fetch(:host)}")
    end

    it "is false for an address in the Zendesk instance domain but not a subdomain" do
      refute instance.send(:in_zendesk_subdomain?, "foo@#{Zendesk::Configuration.fetch(:host)}")
    end

    it "is false for an address not in a Zendesk domain" do
      refute instance.send(:in_zendesk_subdomain?, "foo@example.com")
    end
  end

  describe '#wildcard_support_address?' do
    describe "when the account has the accept_wildcard_emails setting enabled" do
      before do
        account.settings.accept_wildcard_emails = true
        account.save!
      end

      it "is true if the address is in a Zendesk subdomain" do
        instance.stubs(:in_zendesk_subdomain?).returns(true)
        assert instance.send(:wildcard_support_address?, "foo@minimum.#{Zendesk::Configuration.fetch(:host)}")
      end

      it "is false if the address is not in a Zendesk subdomain" do
        instance.stubs(:in_zendesk_subdomain?).returns(false)
        refute instance.send(:wildcard_support_address?, "foo@example.com")
      end
    end

    describe "when the account has the accept_wildcard_emails setting disabled" do
      before do
        account.settings.accept_wildcard_emails = false
        account.save!
      end

      it "is false even if the address is in a Zendesk subdomain" do
        instance.stubs(:in_zendesk_subdomain?).returns(true)
        refute instance.send(:wildcard_support_address?, "foo@minimum.#{Zendesk::Configuration.fetch(:host)}")
      end
    end
  end

  describe '#from_account_address?' do
    it "is true for a recipient address" do
      assert instance.send(:from_account_address?, 'support@zdtestlocalhost.com')
    end

    it "is true for an address that is not a defined support address but is a wildcard support address" do
      instance.stubs(:wildcard_support_address?).returns(true)
      address = "foo@minimum.#{Zendesk::Configuration.fetch(:host)}"
      refute account.recipient_email?(address)
      assert instance.send(:from_account_address?, address)
    end

    it "is false for an address that is not a recipient address or a wildcard support address" do
      instance.stubs(:wildcard_support_address?).returns(false)
      address = 'foo@example.com'
      refute account.recipient_email?(address)
      refute instance.send(:from_account_address?, address)
    end

    describe 'when email address domain is a default host on the account' do
      it 'returns `true`' do
        instance.stubs(:wildcard_support_address?).returns(false)
        address = "noreply@minimum.#{Zendesk::Configuration.fetch(:host)}"
        refute account.recipient_email?(address)
        assert instance.send(:from_account_address?, address)
      end
    end
  end
end
