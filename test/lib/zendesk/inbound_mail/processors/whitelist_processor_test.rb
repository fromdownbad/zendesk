require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::WhitelistProcessor do
  include ArturoTestHelper
  include MailTestHelper

  fixtures :all

  def execute
    Zendesk::InboundMail::Processors::WhitelistProcessor.new(@state, @mail).execute
  end

  let(:logger) { stub("logger", info: true, warn: true) }
  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }

  before do
    @mail = FixtureHelper::Mail.read('site247.json')
    @mail.stubs(logger: logger)

    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = accounts('minimum')
    @state.from = Zendesk::Mail::Address.new(address: "eric@example.com")
    @state.account.settings.enable(:email_sender_authentication)
    @state.sender_auth_passed = true
    @state.account.save!
  end

  it "skips if suspended" do
    @state.account.stubs(:whitelists?).returns(true)
    @state.stubs(:suspended?).returns(true)
    execute
    assert_equal false, @state.whitelisted
    assert_equal false, @state.from_header_whitelisted
  end

  it "whitelists if account whitelists the from address" do
    @state.account.stubs(:whitelists?).returns(true)
    execute
    assert @state.whitelisted?
  end

  it "whitelists if account whitelists the from_header address" do
    @state.account.stubs(:whitelists?).returns(true)
    Zendesk::InboundMail::Processors::WhitelistProcessor.any_instance.expects(:perform_whitelist_check_on_from_header?).returns(true)
    execute
    assert @state.from_header_whitelisted
  end

  describe "when mail is an automated Zopim message" do
    before do
      @mail.from = Zendesk::Mail::Address.new(address: "noreply@zopim.com")
      Zendesk::InboundMail::Processors::WhitelistProcessor.any_instance.expects(:sender_whitelisted?).returns(false)
      Zendesk::InboundMail::Processors::WhitelistProcessor.any_instance.expects(:perform_whitelist_check_on_from_header?).returns(true)
    end

    it "whitelists the mail" do
      execute
      assert @state.whitelisted?
    end

    it "whitelists the from_header address" do
      execute
      assert @state.from_header_whitelisted
    end
  end

  describe "sender whitelisting" do
    subject do
      execute
      @state.whitelisted?
    end

    before do
      @mail.reply_to = Zendesk::Mail::Address.new(address: "reply_to@example.com")
      @mail.from = Zendesk::Mail::Address.new(address: "from@example.com")
    end

    describe "when the mail reply to address is on the account domain whitelist" do
      before do
        @state.account.domain_whitelist = "reply_to@example.com"
        @state.account.save!
      end

      it "whitelists the mail" do
        assert subject
      end
    end

    describe "when the mail from address is on the account domain whitelist" do
      before do
        @state.account.domain_whitelist = "from@example.com"
        @state.account.save!
      end

      it "whitelists the mail" do
        assert subject
      end
    end

    describe "when the mail reply to address is organization whitelisted" do
      before do
        @state.account.organizations.create!(name: "some-org", domain_names: ["reply_to@example.com"])
        @state.account.reload
      end

      it "whitelists the mail" do
        assert subject
      end
    end

    describe "when th email from address is organization whitelisted" do
      before do
        @state.account.organizations.create!(name: "some-org", domain_names: ["from@example.com"])
        @state.account.reload
      end

      it "whitelists the mail" do
        assert subject
      end
    end

    describe "when processing state's from address is on the account domain whitelist" do
      before do
        @state.account.domain_whitelist = @state.from.address
        @state.account.save!
      end

      it "whitelists the mail" do
        assert subject
      end
    end

    describe "when processing state's from address is organization whitelisted" do
      before do
        @state.account.organizations.create!(name: "some-org", domain_names: [@state.from.address])
      end

      it "whitelists the mail" do
        assert subject
      end
    end

    describe "when neither reply_to nor from address is on the account domain whitelist" do
      it "does not whitelist the mail" do
        refute subject
      end
    end
  end

  describe "from_header whitelisting" do
    subject do
      execute
      @state.from_header_whitelisted
    end

    before do
      @state.from_header = Zendesk::Mail::Address.new(address: "from@example.com")
      @state.from = address(users(:minimum_agent).email)
    end

    describe "when the from_header address is on the account domain whitelist" do
      before do
        @state.account.domain_whitelist = "from@example.com"
        @state.account.save!
      end

      it "whitelists the from_header address" do
        assert subject
      end
    end

    describe "when the from_header address is organization whitelisted" do
      before do
        @state.account.organizations.create!(name: "some-org", domain_names: ["from@example.com"])
        @state.account.reload
      end

      it "whitelists the from_header address" do
        assert subject
      end
    end

    describe "when from_header address is not on the account domain whitelist or organization whitelisted" do
      it "does not whitelist the from_header address" do
        refute subject
      end
    end

    describe "when from_header and from address matches" do
      before do
        @state.from, @state.from_header = address(users(:minimum_agent).email)
      end

      it "skips from_header whitelisting" do
        refute subject
      end
    end

    describe "when from address is not an agent" do
      before do
        @state.from_header = Zendesk::Mail::Address.new(address: "fromheader@example.com")
        @state.from = Zendesk::Mail::Address.new(address: "from@example.com")
      end

      it "skips from_header whitelisting" do
        refute subject
      end
    end

    describe "when from_header address is nil" do
      before do
        @state.from_header = nil
        @state.from = Zendesk::Mail::Address.new(address: "from@example.com")
      end

      it "skips from_header whitelisting" do
        refute subject
      end
    end

    describe_with_arturo_disabled :email_mtc_add_from_header_state_variable do
      it "skips from_header whitelisting" do
        refute subject
      end
    end
  end

  it "whitelists using mail subject" do
    @mail.subject = 'Hello'
    @state.account.stubs(:domain_whitelist).returns('gmail.com subject:"hello" subject:"awesome"')

    assert Zendesk::InboundMail::Processors::WhitelistProcessor.new(@state, @mail).send(:mail_subject_whitelisted?)

    @mail.subject = "[Valueshop ApS] Meddelelse fra kontaktformular"
    @state.account.stubs(:domain_whitelist).returns('gmail.com subject:"[Valueshop ApS]"')

    assert Zendesk::InboundMail::Processors::WhitelistProcessor.new(@state, @mail).send(:mail_subject_whitelisted?)
  end

  it "does not blow up when the mail doesn't have a subject" do
    @mail.subject = nil
    @state.account.stubs(:domain_whitelist).returns('gmail.com subject:"hello" subject:"awesome"')
    Zendesk::InboundMail::Processors::WhitelistProcessor.new(@state, @mail).send(:mail_subject_whitelisted?)
  end

  it "does not whitelist non-listed non-agent addresses" do
    @state.account.stubs(:domain_whitelist).returns('')
    @state.account.stubs(:whitelist?).returns(false)
    @state.account.stubs(:agents?).returns(stub)
    @state.account.expects(:find_user_by_email).with('eric@example.com', scope: @state.account.agents).returns(nil)
    execute

    assert_equal false, @state.whitelisted?
  end

  it "whitelists non-listed agent addresses" do
    @state.account.expects(:find_user_by_email).with('eric@example.com', scope: @state.account.agents).returns(stub)
    execute

    assert(@state.whitelisted?)
  end

  it "prevents suspension" do
    @state.account = accounts(:minimum)
    @state.recipient = "support@minimum.localhost"

    Zendesk::InboundMail::Processors::ValidatingProcessor.new(@state, @mail).execute
    assert @state.suspended?

    @state.suspension = nil
    @state.from       = @mail.reply_to

    execute
    assert @state.whitelisted?

    Zendesk::InboundMail::Processors::ValidatingProcessor.new(@state, @mail).execute
    assert @state.whitelisted?
  end

  describe 'whitelist investigation' do
    before do
      Zendesk::InboundMail::Processors::WhitelistProcessor.any_instance.stubs(:address_whitelisted?).returns(true)
      @state.sender_auth_header_results = [:not_from_zendesk, :spf_pass, :dkim_none, :aligned]
    end

    it 'logs' do
      logger.expects(:info).with("Whitelist alignment info: true,not_from_zendesk,spf_pass,dkim_none,aligned")
      execute
    end
  end

  describe "when sender authentication fails" do
    before do
      @state.account.stubs(:whitelists?).returns(true)
      @state.account.stubs(:from_header_whitelisted).returns(true)
      @state.sender_auth_passed = false
    end

    describe 'when sender authentication is not enabled' do
      before do
        @state.account.settings.disable(:email_sender_authentication)
        Zendesk::InboundMail::Processors::WhitelistProcessor.any_instance.expects(:perform_whitelist_check_on_from_header?).returns(true)
      end

      it "still executes the whitelist processor" do
        execute
        assert @state.whitelisted?
        assert @state.from_header_whitelisted
      end
    end

    it "skips the whitelist processor" do
      execute
      refute @state.whitelisted?
      refute @state.from_header_whitelisted
    end

    it "logs skipping the whitelist processor" do
      logger.expects(:info).with("WhitelistProcessor skipped due to failed sender auth")
      execute
    end
  end
end
