require_relative "../../../../support/test_helper"
require "nokogumbo"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::TextApiProcessor do
  fixtures :users, :accounts

  def self.should_extract_property(text, properties)
    it "extracts properties #{properties.inspect} from #{text.inspect}" do
      @mail.body = @state.plain_content = text

      Zendesk::InboundMail::Processors::TextApiProcessor.execute(@state, @mail)

      properties.each do |name, value|
        assert_equal value, @state.properties[name]
      end
    end
  end

  def execute
    Zendesk::InboundMail::Processors::TextApiProcessor.execute(@state, @mail)
  end

  let(:agent) { users(:minimum_agent) }
  let(:user)  { users(:minimum_end_user) }
  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }

  describe "when the author is an end user" do
    let(:body_with_text_api) { "#status pending\n\nHello World" }

    before do
      @logger = stub('logger', info: true, warn: true)
      @mail = Zendesk::Mail::InboundMessage.new
      @mail.stubs(logger: @logger)
      @state = Zendesk::InboundMail::ProcessingState.new
      @state.account = accounts(:minimum)
      @state.plain_content = body_with_text_api
      @state.author = user
      execute
    end

    it "sets the comment body to the email body including text api commands" do
      assert_equal(body_with_text_api, @state.plain_content)
    end
  end

  describe "when the author is an agent" do
    before do
      @logger        = stub('logger', info: true, warn: true)
      @mail          = Zendesk::Mail::InboundMessage.new
      @mail.stubs(logger: @logger)
      @state         = Zendesk::InboundMail::ProcessingState.new
      @state.account = accounts(:minimum)
      @state.author  = agent
      @state.sender_auth_passed = true
      @state.sender_auth_header_results = [:not_from_zendesk, :spf_pass, :dkim_pass, :aligned]
    end

    should_extract_property "#open\nhello there!",    status_id: StatusType.OPEN
    should_extract_property "#pending\nhello there!", status_id: StatusType.PENDING
    should_extract_property "#hold\nhello there!",    status_id: StatusType.HOLD
    should_extract_property "#on-hold\nhello there!", status_id: StatusType.HOLD
    should_extract_property "#solved\nhello there!",  status_id: StatusType.SOLVED
    should_extract_property "#archived\nhello there!", status_id: StatusType.SOLVED
    should_extract_property "#done\nhello there!", status_id: StatusType.SOLVED

    should_extract_property "#OPEN\nhello there!", status_id: StatusType.OPEN
    should_extract_property "#Done\nhello there!", status_id: StatusType.SOLVED
    should_extract_property "#On-Hold\nhello there!", status_id: StatusType.HOLD

    describe "when the email was forwarded by the agent on behalf of an end user" do
      before do
        @state.agent_comment = "#status pending\n\nHello World"
        execute
      end

      it "processes the text api commands" do
        assert_equal "Hello World", @state.agent_comment
      end
    end

    it "skips invalid requester_emails" do
      @state.plain_content = "#requester bogus_email@\n\nhello.com is my website"
      execute
      assert_equal({}, @state.properties)
    end

    it "cleans up requester_emails that contain markup" do
      @state.plain_content = "#requester c.r@example.com<mailto:c.r@example.com><mailto:c.r@example.com>\n\nHello"
      execute
      assert_equal "c.r@example.com", @state.properties[:requester_email], @state.properties
    end

    it "is skipped when the content is too big to process" do
      @state.plain_content = 'h' * 2_000_000
      Zendesk::InboundMail::TextApi::Message.expects(:new).never
      execute
    end

    it "is skipped when the content doesn't begin with a tag" do
      @state.plain_content = 'open'
      Zendesk::InboundMail::TextApi::Message.expects(:new).never
      execute
    end

    it "is not skipped when the content has noise before the tag" do
      @state.plain_content = "\n #open"
      execute
      assert_equal StatusType.OPEN, @state.properties[:status_id]
    end

    it "does not set the comment to false if there is content" do
      @state.plain_content = "#open plus a bunch of content"
      execute
      assert_equal({status_id: 1}, @state.properties)
    end

    it 'records the metrics for the commands processed' do
      statsd_client.expects(:increment).with('text_api_processor.processed', tags: ["command:status"]).once
      statsd_client.expects(:increment).with('text_api_processor.processed', tags: ["command:public"]).once
      statsd_client.expects(:increment).with('text_api_processor.processed', tags: ["command:assignee"]).once
      statsd_client.expects(:increment).with('text_api_processor.emails_containing_text_api').once
      @state.plain_content = "#status pending\n#public false\n#assignee #{agent.email}\nya ya ya"
      execute
    end

    describe_with_arturo_disabled :email_hard_reject_text_api_with_no_content do
      it "sets the comment as false if the content is blank and there is no attachments" do
        @state.plain_content = "#open"
        @mail.expects(:attachments)
        execute
        assert_equal({is_public: "false", status_id: 1}, @state.properties)
      end

      it "does not set the comment to false if there is an attachment" do
        @state.plain_content = "#open"
        @mail.expects(:attachments).returns(["Simulation of an attachment"])
        execute
        assert_equal({status_id: 1}, @state.properties)
      end
    end

    describe_with_arturo_enabled :email_hard_reject_text_api_with_no_content do
      describe "there are only attachments after the api commands" do
        before do
          @state.plain_content = "#open"
          @mail.stubs(attachments: ['a'])
        end

        it "processes the text api commands" do
          statsd_client.expects(:increment).with('text_api_processor.processed', tags: ["command:open"]).once
          statsd_client.expects(:increment).with('text_api_processor.emails_containing_text_api').once
          execute
        end
      end

      describe "there is no content after the api commands" do
        before do
          @state.plain_content = "#open"
        end

        it { assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute } }
      end

      describe "when HTML content is not being used" do
        before do
          @state.account.settings.stubs(rich_content_in_emails?: false)
        end

        describe "when there is no other content after the commands" do
          before do
            @state.plain_content = "#status pending\n#public false\n#assignee #{agent.email}"
            @state.html_content = <<~HTML
              <html>
                <body>
                  <div dir="ltr">#status pending<div>#public false</div></div>
                  <div>#assignee <a href="#{agent.email}">#{agent.email}</a></div>
                </body>
              </html>
            HTML
          end

          it { assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute } }
        end

        describe "when the plain text and HTML portions have different content" do
          describe "when the plain text has no other content after the commands" do
            before do
              @state.plain_content = "#status pending\n#public false\n#assignee #{agent.email}"
              @state.html_content = <<~HTML
                <html>
                  <body>
                    <div dir="ltr">#status pending<div>#public false</div></div>
                    <div>#assignee <a href="#{agent.email}">#{agent.email}</a></div>
                    <div> Meow Meow Meow </div>
                  </body>
                </html>
              HTML
            end

            it { assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute } }
          end
        end
      end
    end

    describe_with_arturo_enabled :email_log_agent_sender_with_nonmatching_from_and_reply_to do
      it "sets state.email_containing_text_api as true" do
        @state.plain_content = "#open plus a bunch of content"
        execute
        @state.email_containing_text_api.must_equal true
      end
    end

    describe_with_arturo_disabled :email_log_agent_sender_with_nonmatching_from_and_reply_to do
      it "sets state.email_containing_text_api as false" do
        @state.plain_content = "#open plus a bunch of content"
        execute
        @state.email_containing_text_api.must_equal false
      end
    end

    describe "when HTML content is being used" do
      before do
        @state.account.settings.stubs(rich_content_in_emails?: true)
      end

      describe "when there is no other content after the commands" do
        before do
          @state.plain_content = "#status pending\n#public false\n#assignee #{agent.email}"
          @state.html_content = <<~HTML
            <html>
              <body>
                <div dir="ltr">#status pending<div>#public false</div></div>
                <div>#assignee <a href="#{agent.email}">#{agent.email}</a></div>
              </body>
            </html>
          HTML
        end

        describe_with_arturo_enabled :email_hard_reject_text_api_with_no_content do
          it { assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute } }
        end

        describe_with_arturo_disabled :email_hard_reject_text_api_with_no_content do
          before do
            execute
          end

          it "strips the commands from the plain text content" do
            @state.plain_content.must_be_empty
          end

          it "uses nil for the HTML content" do
            @state.html_content.must_be_nil
          end

          it "stores the commands in the properties" do
            @state.properties.must_equal status_id: 2, is_public: false, assignee_email: agent.email
          end
        end
      end

      describe "when there is content after the commands" do
        before do
          @state.plain_content = "#status pending\n#public false\n#assignee #{agent.email}\n#requester #{user.email}\n\nThis is the content"
          @state.html_content = <<~HTML
            <div dir="ltr">
              <span>#status pending</span>
              <div>#assignee <a href="#{agent.email}">#{agent.email}</a></div>
              <div>#public false</div>
              <div>#requester #{user.email}</div>
              <div><br></div>
              <div>This is the content.</div>
            </div>
          HTML
        end

        it 'logs the sender authentication results' do
          @logger.expects(:info).with('Sender auth header results: not_from_zendesk, spf_pass, dkim_pass, and aligned').once
          execute
        end

        it 'logs the sender authentication status' do
          @logger.expects(:info).with('Email contains TextAPI commands and Sender Authentication status is PASSED').once
          execute
        end

        it "strips the commands from the plain text content" do
          execute
          @state.plain_content.must_equal "This is the content"
        end

        it "strips the commands and following whitespace from the HTML content" do
          execute
          @state.html_content.wont_match Regexp.union(
            [
              "<br>",
              "#status pending",
              "#public false",
              "#assignee",
              "<a href=\"mailto:#{agent.email}\">#{agent.email}</a>",
              "#requester",
              user.email
            ]
          )
        end

        it "stores the commands in the properties" do
          execute
          @state.properties.must_equal status_id: 2, is_public: false, assignee_email: agent.email, requester_email: user.email
        end
      end

      describe "when the HTML tag is malformed " do
        before do
          @state.plain_content = "#status pending\n#public false\n#assignee #{agent.email}\n\nThis is the content"
          @state.html_content = <<~HTML
            <html>
              <body>
                <div dir="ltr">#status pending<div>#public false</div></div>
                <div>#assignee</div>
                <div>#{agent.email}</div>
                <div><br></div>
                <div>This is the content.</div>
              </body>
            </html>
          HTML
          execute
        end

        it "strips the commands from the plain text content" do
          @state.plain_content.must_equal "This is the content"
        end

        it "#assignee tag is scrubbed" do
          @state.html_content.wont_include "#assignee"
        end

        it "malformed tag value remains (separate line/block)" do
          @state.html_content.must_include agent.email
        end

        it "stores the commands in the properties, using the plain text processing" do
          @state.properties.must_equal status_id: 2, is_public: false, assignee_email: agent.email
        end
      end
    end
  end

  describe 'when there is a possible spoof detected' do
    let(:comment) { "#status pending\n\nHello World" }
    before do
      @logger = stub('logger', info: true, warn: true)
      @mail = Zendesk::Mail::InboundMessage.new
      @mail.stubs(logger: @logger)
      @state = Zendesk::InboundMail::ProcessingState.new
      @state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF)
      @state.author = agent
      @state.agent_comment = comment
      @state.account = accounts(:minimum)
      @state.sender_auth_passed = true
      @state.sender_auth_header_results = [:not_from_zendesk, :spf_pass, :dkim_pass, :aligned]
    end

    it 'does not process with the text api commands' do
      @logger.expects(:info).with('Discontinuing TextApiProcessor as agent spoof detected.').once
      execute
      assert_equal comment, @state.agent_comment
    end

    it 'increments statsd' do
      statsd_client.expects(:increment).with('reply_to_processor.text_api_processor_skipped').once
      execute
    end

    describe_with_arturo_disabled :email_reply_to_vulnerability_text_api_processor do
      it 'processes with the text api commands' do
        @logger.expects(:info).with('Discontinuing TextApiProcessor as agent spoof detected.').never
        execute
        assert_equal 'Hello World', @state.agent_comment
      end
    end
  end
end

describe "A TextApiProcessor instance" do
  describe "the author is an agent" do
    before do
      @state = Zendesk::InboundMail::ProcessingState.new
      @state.plain_content      = "#tag Some content"
      @state.account            = accounts(:minimum)
      @state.message_id         = '12345@example.com'
      @state.author             = users(:minimum_agent)
      @state.sender_auth_passed = true
      @state.sender_auth_header_results = [:not_from_zendesk, :spf_pass, :dkim_pass, :aligned]
      @message = stub("message", stripped_text: "Some content", properties: {}, unknown_tags: [], invalid_values: [], known_tags: [])
      @logger = stub('logger', info: true)
      @mail = stub("fail hail mail rail snail wail", logger: @logger, headers: {})
      @processor = Zendesk::InboundMail::Processors::TextApiProcessor.new(@state, @mail)
      Zendesk::InboundMail::TextApi::Message.stubs(:new).returns(@message)
    end

    describe 'when sender authentication is enabled and the sender is not authenticated' do
      before do
        @state.account.settings.enable(:email_sender_authentication)
        @state.sender_auth_passed = false
      end

      it 'logs that textAPI commands will be ignored' do
        @logger.expects(:info).with("Sender authentication failed - ignoring text API commands")
        @processor.execute
      end

      it 'skips' do
        Zendesk::InboundMail::TextApi::Message.expects(:new).never
        @processor.execute
      end
    end

    describe "the email is not suspended" do
      it "sets the properties on the state" do
        @message.stubs(:properties).returns(wombat: "foo")
        @logger.expects(:info).with("Using tags wombat")
        @processor.execute

        assert_equal({ wombat: 'foo' }, @state.properties)
      end

      it "sets the content on the state to the stripped text" do
        @message.expects(:stripped_text).returns("stripped text")
        @processor.execute

        assert_equal 'stripped text', @state.plain_content
      end

      it "logs unknown API tags in the content" do
        @message.stubs(:unknown_tags).returns(["midget"])
        @logger.expects(:info).with("Unknown tag 'midget'")
        @processor.execute
      end

      it "logs invalid values in the content" do
        @message.stubs(:invalid_values).returns([["status", "rocking"]])
        @logger.expects(:info).with(%(Invalid value "rocking" for tag 'status'))
        @processor.execute
      end
    end

    describe "the email is suspended" do
      before do
        @state.author = nil
        @state.stubs(agent: users(:minimum_agent), suspended?: true)
      end

      it "sets the properties on the state" do
        @message.stubs(:properties).returns(wombat: "foo")
        @logger.expects(:info).with("Using tags wombat")
        @processor.execute

        assert_equal({ wombat: 'foo' }, @state.properties)
      end

      it "sets the content on the state to the stripped text" do
        @message.expects(:stripped_text).returns("stripped text")
        @processor.execute

        assert_equal 'stripped text', @state.plain_content
      end

      it "logs unknown API tags in the content" do
        @message.stubs(:unknown_tags).returns(["midget"])
        @logger.expects(:info).with("Unknown tag 'midget'")
        @processor.execute
      end

      it "logs invalid values in the content" do
        @message.stubs(:invalid_values).returns([["status", "rocking"]])
        @logger.expects(:info).with(%(Invalid value "rocking" for tag 'status'))
        @processor.execute
      end
    end
  end
end
