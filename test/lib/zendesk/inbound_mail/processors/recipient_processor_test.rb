require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::InboundMail::Processors::RecipientProcessor do
  fixtures :routes, :accounts, :recipient_addresses, :brands, :account_settings
  include MailTestHelper

  let(:recipient_address) { recipient_addresses(:not_default) }
  let(:recipient_email) { recipient_address.email }
  let(:brand) do
    account = recipient_address.account
    Brand.create!(name: "Foo") do |b|
      b.account_id = account.id
      b.build_route(subdomain: "foobars") { |r| r.account = account }
    end
  end

  def execute
    Zendesk::InboundMail::Processors::RecipientProcessor.execute(@state, @mail)
  end

  before do
    @state   = Zendesk::InboundMail::ProcessingState.new
    @mail    = FixtureHelper::Mail.read('chopped.json')
    @account = accounts(:minimum)
    @state.account = @account
  end

  it "sanitizes slightly invalid emails (might no longer be necessary with new mail_parsing queue ...)" do
    @mail.to = [address("'#{recipient_email}'")]
    execute
    assert_equal recipient_email, @state.original_recipient_address
  end

  describe "with recognised recipient address" do
    before do
      @mail.to = address(recipient_email)
    end

    it "uses that recipient address as the original recipient address" do
      execute
      assert_equal recipient_email, @state.original_recipient_address
    end

    it "bumps the recipient addresses verified-at" do
      Timecop.freeze
      recipient_address.update_column(:forwarding_verified_at, 2.hours.ago)
      execute
      assert_equal Time.now.to_i, recipient_address.reload.forwarding_verified_at.to_i
    end

    it "supports delivered-to recipient detection for mail_fetcher" do
      recipient = "morten@example.com"
      @mail.headers['Delivered-To'] = address(recipient)
      recipient_address.update_column(:email, recipient)
      execute
      @state.original_recipient_address.must_equal recipient
      @state.brand_id.must_equal recipient_address.brand.id
    end

    describe "branded recipient address" do
      before { recipient_address.update_column(:brand_id, brand.id) }

      it "sets brand_id" do
        execute
        assert_equal brand.id, @state.brand_id
      end

      it "does not set inactive brand_id" do
        brand.update_column(:active, false)
        execute
        assert_equal @account.default_brand_id, @state.brand_id
      end
    end
  end

  describe "with x_been_there header matching existing recipient address and to field containing the default recipient address" do
    before do
      @mail.to                     = [address(recipient_email.sub('@', 'alias@')), address(@account.default_recipient_address.email)]
      @mail.headers['X-BeenThere'] = [address('foo@example.com'), address(recipient_email)]
    end

    it "uses the X-BeenThere header address as the original recipient address if account has arturo enabled" do
      Account.any_instance.stubs(:has_google_group_alias_x_been_there?).returns(true)
      execute
      assert_equal recipient_email, @state.original_recipient_address
    end

    it "uses the default recipient address as the original recipient address if account's arturo isn't enabled" do
      Account.any_instance.stubs(:has_google_group_alias_x_been_there?).returns(false)
      execute
      assert_equal @account.default_recipient_address.email, @state.original_recipient_address
    end
  end

  describe "order" do
    it "finds in order of recipients when multiple recipients are supported" do
      original = "original@recepient.com"
      @account.recipient_addresses.create!(email: original)
      @mail.to = address(original)
      @mail.headers['Envelope-To'] = address(recipient_email)
      execute
      assert_equal original, @state.original_recipient_address
    end

    it "de-prioritizes backup addresses since they are usually the last receiver" do
      @mail.to = address(@account.backup_email_address)
      @mail.headers['Envelope-To'] = address(recipient_email)
      execute
      assert_equal recipient_email, @state.original_recipient_address
    end

    describe "with address on default host" do
      let(:default_host_address) { @account.recipient_addresses.create!(email: "info@#{@account.default_host}") }

      it "de-prioritizes zendesk addresses since they are usually the last receiver" do
        @mail.to = address(default_host_address.email)
        @mail.headers['Envelope-To'] = address(recipient_email)
        execute
        assert_equal recipient_email, @state.original_recipient_address
      end

      it "de-prioritizes backup zendesk addresses over default_host addresses" do
        @mail.to = address(@account.backup_email_address)
        @mail.headers['Envelope-To'] = address(default_host_address.email)
        execute
        assert_equal default_host_address.email, @state.original_recipient_address
      end
    end
  end

  describe "extracting recipients" do
    it "does not find unknown recipients" do
      @mail.headers["X-Envelope-To"] = [address("morten@example.zendesk.com")]
      execute
      assert_nil @state.original_recipient_address
    end

    describe "when no recipients are available in forwarding headers" do
      before do
        @mail.headers.delete("Envelope-to")
        execute
        assert_nil @state.original_recipient_address
      end

      it "uses the state's recipient" do
        @state.recipient = 'morten@example.com'
        execute
        assert_equal 'morten@example.com', @state.original_recipient_address
      end

      it "does not use the state's recipient when the address is directly to Zendesk" do
        @state.recipient = "morten@example.#{Zendesk::Configuration.fetch(:host)}"
        execute
        assert_nil @state.original_recipient_address
      end
    end
  end

  describe "for a follow-up ticket" do
    before { @mail.to = address("support+id12345@foobars.zendesk-test.com") }

    describe "for an account with multiple brands" do
      before { recipient_address.update_column(:brand_id, brand.id) }

      it 'sets the correct brand on the ticket' do
        execute
        assert_equal brand.id, @state.brand_id
      end
    end
  end
end
