require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::InboundMail::Processors::NPSSurveyAutoReplyIgnoreProcessor do
  fixtures :accounts, :account_settings, :recipient_addresses

  describe "#execute" do
    let(:recipient_address) { recipient_addresses(:default) }

    before do
      @mail                                   = FixtureHelper::Mail.read('minimum_ticket.json')
      @state                                  = Zendesk::InboundMail::ProcessingState.new
      @state.account                          = recipient_address.account
      @nps_survey_auto_reply_ignore_processor = Zendesk::InboundMail::Processors::NPSSurveyAutoReplyIgnoreProcessor.new(@state, @mail)
    end

    describe "when the recipient is account nps address" do
      before do
        @state.recipient = recipient_address.email.gsub('@', '+nps12@')
      end

      describe "when the message matches one of the hard rules" do
        before do
          @mail.subject = 'OOO'
        end

        it "raises Zendesk::InboundMail::HardMailRejectException exception" do
          assert_raise Zendesk::InboundMail::HardMailRejectException do
            @nps_survey_auto_reply_ignore_processor.execute
          end
        end
      end

      describe "when the message doesn't match any hard rule" do
        before do
          @mail.subject = "Excellent survey question"
        end

        it "does not raise exception" do
          @nps_survey_auto_reply_ignore_processor.execute
        end
      end
    end

    describe "when the recipient isn't account nps address" do
      it "does not raise an exception" do
        @nps_survey_auto_reply_ignore_processor.execute
      end
    end
  end
end
