require_relative "../../../../support/test_helper"
require_relative "../../../../support/collaboration_settings_test_helper"
require_relative "../../../../../lib/zendesk/inbound_mail/ticketing/mail_collaboration"
require_relative "../../../../../lib/zendesk/inbound_mail/ticketing/mail_collaboration_update_types"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::CollaboratorProcessor do
  include ArturoTestHelper
  include MailTestHelper
  fixtures :all

  def execute
    Zendesk::InboundMail::Processors::CollaboratorProcessor.execute(@state, @mail)
  end

  def instance
    Zendesk::InboundMail::Processors::CollaboratorProcessor.new(@state, @mail)
  end

  def create_email_cc(user, ticket)
    ticket.collaborations.build(
      user: user, ticket: ticket, collaborator_type: CollaboratorType.EMAIL_CC
    )
  end

  def create_follower(user, ticket)
    ticket.collaborations.build(
      user: user, ticket: ticket, collaborator_type: CollaboratorType.FOLLOWER
    )
  end

  def add_secondary_email_identity(user)
    @secondary_email = 'secondary-email@example.com'
    @mail.cc << address(@secondary_email)
    user.identities << UserEmailIdentity.new(user: @state.ticket.requester, account: @state.ticket.account, value: @secondary_email)
    user.save!
  end

  let(:mail_collaboration) { Zendesk::InboundMail::Ticketing::MailCollaboration }
  let(:processor) { Zendesk::InboundMail::Processors::TicketCreatorProcessor }
  let(:account) { accounts(:minimum) }
  let(:ticket) { Ticket.new(account: account) }
  let(:author) { users(:minimum_agent) }
  let(:from_email) { @state.ticket.requester.email }
  let(:to_recipients) { ['a@example.com', 'b@example.com', 'c@example.com', 'd@example.com', 'e@example.com', 'f@example.com'] }
  let(:cc_recipients) { ['1@example.com', '2@example.com', '3@example.com', '4@example.com', '5@example.com', '6@example.com'] }
  let(:agent_email_ccs_become_followers) { false }

  before do
    Account.any_instance.stubs(is_collaboration_enabled?: true, is_collaborators_addable_only_by_agents?: false)
    Account.any_instance.stubs(is_signup_required?: false, is_open?: true, allows_collaborator?: true)
    Account.any_instance.stubs(has_agent_email_ccs_become_followers_enabled?: agent_email_ccs_become_followers)
    CollaborationSettingsTestHelper.disable_all_new_collaboration_settings

    @state = Zendesk::InboundMail::ProcessingState.new
    account.update_attributes(domain_whitelist: 'example.com twitter.com zendesk-test.com aghassipour.com')
    @state.account = account
    @state.account.update_attributes(settings: { collaborators_maximum: 5 })
    @state.author = author
    @state.ticket = ticket
    @state.ticket.add_comment(body: "Collaborator Processor test comment", is_public: true, author: @state.author)
    @mail = Zendesk::Mail::InboundMessage.new

    @state.ticket.requester = User.new(account: account, name: "Requester", email: "requester@example.com")
    @state.stubs(:from).returns(Zendesk::Mail::Address.parse(@from_email))
    @state.ticket.will_be_saved_by(@state.author)

    @mail.to = to_recipients.map { |address| address(address) }
    @mail.cc = cc_recipients.map { |address| address(address) }

    # TicketProcessingWorker normally would apply the account to the "mail"/InboundMessage object
    Zendesk::Mail::InboundMessage.any_instance.stubs(account: @state.account)
  end

  # Tests that have the same behavior for the new CCs feature
  describe_with_and_without_arturo_setting_enabled :follower_and_email_cc_collaborations do
    before do
      if @state.account.has_follower_and_email_cc_collaborations_enabled?
        Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
        Account.any_instance.stubs(:has_ticket_followers_allowed_enabled?).returns(true)
      end
    end

    it "always updates recipients_for_latest_mail, even if author can't add collaborators" do
      Account.any_instance.stubs(:is_collaborators_addable_only_by_agents?).returns(true)
      @state.author.stubs(:is_agent?).returns(false)
      execute
      assert_equal (to_recipients + cc_recipients).sort.join(' '), @state.ticket.recipients_for_latest_mail
    end

    it "always updates recipients_for_latest_mail, even if author is a light_agent" do
      Account.any_instance.stubs(:is_collaborators_addable_only_by_agents?).returns(true)
      @state.author.stubs(:is_light_agent?).returns(true)
      execute
      assert_equal (to_recipients + cc_recipients).sort.join(' '), @state.ticket.recipients_for_latest_mail
    end

    it "saves new users" do
      execute
      assert @state.ticket.collaborations.first.user.persisted?
    end

    def setup_invalid_collaboration
      # Stub find_user_by_email to return nil first for MailCollaboration#user and then the valid user for MailCollaboration#repair_user
      Account.any_instance.stubs(:find_user_by_email).returns(nil, users(:minimum_end_user))
      Zendesk::InboundMail::Ticketing::MailCollaboration.any_instance.stubs(valid?: true)
    end

    describe "when handling invalid users" do
      let(:to_recipients) { ['minimum_end_user@aghassipour.com'] }
      let(:cc_recipients) { [] }

      it "fixes a collaboration whose user is invalid due to a conflicting existing user" do
        setup_invalid_collaboration
        @state.ticket.collaborations.must_be_empty
        execute
        @state.ticket.collaborations.first.user.must_equal users(:minimum_end_user)
      end
    end

    describe "with light agent response" do
      before do
        @state.author.stubs(is_light_agent?: true, is_private_comments_only: true)
        @state.ticket = tickets(:minimum_1)
        @state.ticket.will_be_saved_by(@state.author)
      end

      describe "for new tickets" do
        before { @state.ticket.stubs(:new_record?).returns(true) }

        it "allows modifications to collaborators" do
          execute
          assert @state.ticket.changed.include?("current_collaborators")
        end
      end

      describe "for existing tickets" do
        let(:admin) { users(:minimum_admin) }

        before do
          @state.ticket.stubs(:new_record?).returns(false)
          @mail.cc << address(admin.email)
        end

        describe "author is not the current requester" do
          before { @state.ticket.add_comment(body: "Collaborator Processor test comment", is_public: false, author: @state.author) }

          it "handles modifications to collaborators based on follower_and_email_cc_collaborations setting" do
            execute

            if @state.account.has_follower_and_email_cc_collaborations_enabled?
              assert @state.ticket.changed.include?("current_collaborators"), "Light agents should be able to add "\
              "followers when follower_and_email_cc_collaborations is enabled"
            else
              refute @state.ticket.changed.include?("current_collaborators"), "Light agents should not be able to "\
              "update collaborators when follower_and_email_cc_collaborations is disabled"
            end
          end
        end

        describe "author is the current requester" do
          before do
            @state.requester_is_on_email = true
            @state.ticket.requester = @state.author
            @state.ticket.add_comment(body: "Collaborator Processor test comment", is_public: true, author: @state.author)
            execute
          end

          it { assert @state.ticket.changed.include?("current_collaborators") }
        end
      end
    end

    describe "with ignored collaborators" do
      let(:collaborator_emails) { @state.ticket.collaborations.map { |c| c.user.email } }

      before do
        @original_sender = "eric@example.com"
        @state.ticket.assignee  = User.new(account: @state.account, name: "Assignee", email: "assignee@example.com")
        @state.ticket.requester = User.new(account: @state.account, name: "Requester", email: "requester@example.com")

        @mail.to         = address("recipient@example.com")
        @state.recipient = "recipient@example.com"
        @mail.from       = address(@state.ticket.requester.email)
        @mail.cc         = [
          address(@original_sender),
          address(@state.ticket.requester.email),
          address(@state.ticket.assignee.email)
        ]
      end

      describe "with bad reply addresses for tickets possibly affected by reply_to bug" do
        before do
          @bad_reply_address = "privacy+bncbaabbx442gjqkgqe6i5iara@twitter.com"
          @state.ticket = tickets(:minimum_1)
          @state.ticket.will_be_saved_by(@state.ticket.requester)
          @state.ticket.created_at = DateTime.parse('Novemeber 4 2013')
          @state.account.stubs(:subdomain).returns('twitter')
          @mail.from = address(@state.ticket.requester.email)
          @mail.cc = [
            address("hello@example.com"),
            address(@bad_reply_address)
          ]
        end

        it "does not include bad reply addresses for tickets possibly affected by reply_to bug" do
          execute

          collaborator_emails.wont_include @bad_reply_address
        end

        describe_with_arturo_disabled :has_email_extensible_followers do
          it "does not include bad reply addresses for tickets possibly affected by reply_to bug" do
            execute

            collaborator_emails.wont_include @bad_reply_address
          end
        end
      end

      it "includes bad reply addresses for tickets not possibly affected by the reply_to bug" do
        bad_reply_address = "privacy+bncbaabbx442gjqkgqe6i5iara@twitter.com"
        @state.ticket = tickets(:minimum_1)
        @state.ticket.will_be_saved_by(@state.ticket.requester)
        @state.ticket.created_at = Time.now
        @state.account.stubs(:subdomain).returns('twitter')
        @mail.from = address(@state.ticket.requester.email)
        @mail.cc = [
          address("hello@example.com"),
          address(bad_reply_address)
        ]

        execute

        collaborator_emails.must_include bad_reply_address
      end

      it "does not blow up when checking reply_to bug for new tickets" do
        bad_reply_address = "privacy+bncbaabbx442gjqkgqe6i5iara@twitter.com"
        @state.ticket.created_at = nil
        @state.account.stubs(:subdomain).returns('twitter')
        @mail.cc = [
          address("hello@example.com"),
          address(bad_reply_address)
        ]

        execute

        collaborator_emails.size.must_equal 2
        collaborator_emails.must_include bad_reply_address
      end

      ["Delivered-To", "X-Forwarded-For", "X-BeenThere"].each do |header|
        it "does not include original recipient specified in #{header}" do
          @mail.headers[header] = [address(@original_sender)]
          execute
          collaborator_emails.wont_include @original_sender
        end
      end

      it "does not include state recipient" do
        execute
        collaborator_emails.wont_include @state.recipient
      end

      it "does not include ticket assignee" do
        add_secondary_email_identity(@state.ticket.assignee)
        assert_equal 2, @state.ticket.assignee.identities.size

        execute

        @state.ticket.assignee.identities.map(&:value).each do |email|
          collaborator_emails.wont_include email
        end
      end

      it "does not include ticket requester" do
        add_secondary_email_identity(@state.ticket.requester)
        assert_equal 2, @state.ticket.requester.identities.size
        execute

        @state.ticket.requester.identities.map(&:value).each do |email|
          collaborator_emails.wont_include email
        end
      end

      it "does not include denied collaborator email addresses" do
        Account.any_instance.stubs(:allows_collaborator?).returns(false)
        execute
        @state.ticket.collaborations.must_equal []
      end

      it "does not include secondary addresses for existing collaborators" do
        add_secondary_email_identity(@state.author)
        execute
        collaborator_emails.wont_include @secondary_email
      end

      it "does not include the original recipient address" do
        @mail.to = address("original_recipient@example.com")
        @state.stubs(:original_recipient_address).returns('original_recipient@example.com')

        execute
        collaborator_emails.must_equal ["eric@example.com"]
      end

      it "does not include original recipients, including +id1234 addresses" do
        plus_address = "support+id1234@minimum.localhost"
        @state.recipient = plus_address
        @mail.to = address(plus_address)
        @mail.cc = address("cc@example.com")

        execute

        collaborator_emails.must_equal ["cc@example.com"]
      end

      it "does not include recipient addresses for the account" do
        @state.recipient = recipient_addresses(:default).email

        @mail.to = [
          address(recipient_addresses(:backup).email.sub('@', '+id1234@')),
          address(recipient_addresses(:backup).email.sub('@', '+id1234+1234@'))
        ]
        @mail.cc = address("cc@example.com")

        execute

        collaborator_emails.must_equal ["cc@example.com"]
      end

      it "does not include +id addresses for zendesk.com" do
        @state.recipient = recipient_addresses(:default).email
        recipient_addresses(:not_default).update_column(:email, 'support@zendesk-test.com')

        @mail.to = [
          address('support+id1234@zendesk-test.com')
        ]
        @mail.cc = address("cc@example.com")

        execute

        collaborator_emails.must_equal ["cc@example.com"]
      end

      describe "with invalid addresses" do
        before do
          @mail.cc = [
            address("foo@domain.invalid"),
            address("foo@anything.invalid")
          ]
        end

        it "does not include @domain.invalid addresses" do
          execute

          collaborator_emails.must_be_empty
        end

        it "does not include @domain.invalid addresses" do
          execute

          collaborator_emails.must_be_empty
        end
      end
    end

    describe "saving latest recipients" do
      it "includes to and cc addresses" do
        execute

        assert_match to_recipients.join(' '), @state.ticket.recipients_for_latest_mail
        assert_match cc_recipients.join(' '), @state.ticket.recipients_for_latest_mail
      end
    end

    describe "when adding a new user as collaborator" do
      before do
        @mail.to          = address("recipient@example.com")
        @state.recipient  = 'recipient@example.com'
        @mail.cc          = address("new@example.com")
      end

      it "skips if account is closed" do
        Account.any_instance.stubs(:is_open?).returns(false)
        execute
        assert_equal [], @state.ticket.collaborations
      end

      it "skips if account denies address" do
        Account.any_instance.stubs(:allows?).returns(false)
        execute
        assert_equal [], @state.ticket.collaborations
      end

      it "does not overwrite existing users" do
        existing_user = @state.account.users.create!(name: 'new', email: 'new@example.com')
        execute
        assert_equal 1, @state.ticket.collaborations.size
        assert_equal existing_user.id, @state.ticket.collaborations.first.user_id
        assert @state.ticket.collaborations.first.valid?
      end

      it "skips if invalid" do
        @mail.cc = address("invalid")
        execute
        assert_equal [], @state.ticket.collaborations
      end

      it "tries to send a verification email if signup is required" do
        Account.any_instance.stubs(:is_signup_required?).returns(true)
        execute
        assert @state.ticket.collaborations.first.user.send_verify_email
      end
    end

    describe "when agent forwards a new ticket" do
      describe "with agents primary id" do
        before do
          @mail            = FixtureHelper::Mail.read('forwards/apple_mail/apple_agent_forwarding.json')
          @state           = Zendesk::InboundMail::ProcessingState.new
          @state.account   = accounts(:minimum)
          @state.from      = address(@state.account.owner.email)
          @state.author    = users(:minimum_agent)
          @state.recipient = @state.account.reply_address
          @state.plain_content = @mail.content.text

          @state.account.settings.enable(:agent_forwardable_emails)
          @state.account.save!

          @forwarded_message_body = 'Forwarded message body'
          Zendesk::InboundMail::Forward.any_instance.stubs(:body).returns(@forwarded_message_body)
        end

        it "does not add agent as collaborator by default" do
          Zendesk::InboundMail::Processors::ForwardingProcessor.execute(@state, @mail)
          assert @state.agent_forwarded

          Zendesk::InboundMail::Processors::TicketCreatorProcessor.execute(@state, @mail)
          assert_equal Ticket, @state.ticket.class

          execute

          collaborations = @state.ticket.collaborations.map { |c| c.user.email }
          refute collaborations.member?(@state.account.owner.email)
          assert_equal 1, collaborations.size
        end

        it "adds agent as collaborator if they CC themselves" do
          @mail.cc << @state.from
          Zendesk::InboundMail::Processors::ForwardingProcessor.execute(@state, @mail)
          assert @state.agent_forwarded

          Zendesk::InboundMail::Processors::TicketCreatorProcessor.execute(@state, @mail)
          assert_equal Ticket, @state.ticket.class

          execute

          collaborations = @state.ticket.collaborations.map { |c| c.user.email }
          assert collaborations.member?(@state.account.owner.email)
          assert_equal 2, collaborations.size
        end
      end

      describe "with secondary id forwarding" do
        before do
          @state.agent_forwarded = true
          @state.author.identities << UserEmailIdentity.new(user: @state.submitter, value: "other_id@example.com")
          @state.submitter = @state.author
          @state.from = address(@state.submitter.identities.last.value)
          assert_equal 2, @state.submitter.identities.size
        end

        describe "and added primary identity value as a cc, the agent" do
          before do
            @mail.cc << address(@state.submitter.identities.first.value)
          end

          it "is added as a collaborator" do
            execute
            assert_includes @state.ticket.collaborations.map(&:user), @state.submitter
          end
        end

        describe "and added secondary identity value as a cc, the agent" do
          before do
            @mail.cc << address(@state.submitter.identities.last.value)
          end

          it "is added as a collaborator" do
            execute
            assert_includes @state.ticket.collaborations.map(&:user), @state.submitter
          end
        end

        describe "and not added self as a cc, the agent" do
          it "is not added as a collaborator" do
            execute
            refute_includes @state.ticket.collaborations.map(&:user), @state.submitter
          end
        end
      end
    end

    describe 'when there is an exception' do
      let(:exception) { ArgumentError.new('KA-BOOM!') }
      let(:suspended_ticket) { mock }

      it 'does the glorious ticket suspension hackery' do
        # Creates a new suspended ticket
        processor.expects(:new).with(@state, @mail).returns(suspended_ticket).once
        suspended_ticket.expects(:suspend_ticket).once

        # Simulates a KA-BOOM!
        @state.stubs(:suspended?).raises(exception)
        execute

        # Flags the mail
        assert_equal SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE, @state.suspension
      end
    end
  end

  # Tests that have different behavior for new CCs feature
  describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
    it "adds collaborators until collaborator limit is reached" do
      execute
      assert_equal 5, @state.ticket.collaborations.size
    end

    describe "when the author can't add collaborators" do
      let(:author) { users(:minimum_end_user) }

      before do
        Account.any_instance.stubs(is_collaborators_addable_only_by_agents?: true)
        @state.author = author
      end

      it "skips collaboration creation" do
        assert @state.account.is_collaboration_enabled? # Legacy CCs are enabled
        assert_equal 0, @state.ticket.collaborations.size # No previous CCs
        execute
        assert_equal [], @state.ticket.collaborations
      end
    end
  end

  describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
    describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
      let(:cc_recipients) do
        [].tap do |recipients|
          (Ticket::MAX_COLLABORATORS_V2 + 1).times do |i|
            recipients << "#{i}@example.com"
          end
        end
      end

      it "enforces a limit of #{Ticket::MAX_COLLABORATORS_V2} for email CCs" do
        execute
        assert_equal Ticket::MAX_COLLABORATORS_V2, @state.ticket.collaborations.size
        assert_equal Ticket::MAX_COLLABORATORS_V2, @state.ticket.collaborations.select { |c| c.collaborator_type == CollaboratorType.EMAIL_CC }.size
      end

      describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
        it "is flagged when the limit of #{Ticket::MAX_COLLABORATORS_V2} for email CCs is exceeded" do
          actual_flag_options = HashWithIndifferentAccess.new(EventFlagType.EMAIL_CCS_EXCEEDED_LIMIT => {
            message: { max_ccs_limit: Ticket::MAX_COLLABORATORS_V2 },
            trusted: false
          })

          execute
          assert_equal @state.ticket.audit.flags, AuditFlags.new([EventFlagType.EMAIL_CCS_EXCEEDED_LIMIT])
          assert_equal @state.ticket.audit.flags_options, actual_flag_options
        end
      end

      describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
        it "is not flagged when the limit of #{Ticket::MAX_COLLABORATORS_V2} for email CCs is exceeded" do
          execute
          assert_empty @state.ticket.audit.flags
        end
      end

      describe "when followers are present" do
        before do
          @state.ticket.collaborations.build(collaborator_type: CollaboratorType.FOLLOWER, user_id: users(:minimum_agent))
          @state.ticket.collaborations.build(collaborator_type: CollaboratorType.FOLLOWER, user_id: users(:minimum_admin))
        end

        it "does not count follower collaborations in enforcement of email CCs limit #{Ticket::MAX_COLLABORATORS_V2}" do
          execute
          assert_equal Ticket::MAX_COLLABORATORS_V2 + 2, @state.ticket.collaborations.size
          assert_equal 2, @state.ticket.collaborations.select { |c| c.collaborator_type == CollaboratorType.FOLLOWER }.size
        end

        it "enforces a limit of #{Ticket::MAX_COLLABORATORS_V2} for email CCs" do
          execute
          assert_equal Ticket::MAX_COLLABORATORS_V2 + 2, @state.ticket.collaborations.size
          assert_equal Ticket::MAX_COLLABORATORS_V2, @state.ticket.collaborations.select { |c| c.collaborator_type == CollaboratorType.EMAIL_CC }.size
        end
      end

      describe "with existing email CCs on the ticket" do
        let(:end_user) { users(:minimum_end_user) }
        let(:end_user_2) { users(:minimum_end_user2) }

        before do
          @state.ticket = tickets(:minimum_1)
          @state.ticket.collaborations.delete_all
          @state.ticket.collaborations.create(collaborator_type: CollaboratorType.EMAIL_CC, user_id: end_user.id)
          @state.ticket.collaborations.create(collaborator_type: CollaboratorType.EMAIL_CC, user_id: end_user_2.id)
          @state.ticket.will_be_saved_by(@state.author)
          @state.ticket.stubs(comment: events(:create_comment_for_minimum_ticket_1))
        end

        it "enforces a limit of #{Ticket::MAX_COLLABORATORS_V2} for email CCs" do
          execute
          assert_equal Ticket::MAX_COLLABORATORS_V2, @state.ticket.collaborations.size
          assert_equal Ticket::MAX_COLLABORATORS_V2, @state.ticket.collaborations.select { |c| c.collaborator_type == CollaboratorType.EMAIL_CC }.size
        end

        it "does not overwrite existing email CCs" do
          execute
          assert_includes @state.ticket.collaborations.map(&:user_id), end_user.id
          assert_includes @state.ticket.collaborations.map(&:user_id), end_user_2.id
        end
      end
    end
  end

  it "skips unless account collaboration is enabled" do
    Account.any_instance.stubs(:is_collaboration_enabled?).returns(false)
    Account.any_instance.stubs(:has_follower_and_email_cc_collaborations_enabled?).returns(false)
    execute
    assert_equal [], @state.ticket.collaborations
  end

  describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
    it "updates ticket current collaborators" do
      execute
      current_collaborators = 'Unknown A, Unknown B, Unknown C, Unknown D, Unknown E'
      assert_equal current_collaborators, @state.ticket.current_collaborators
    end

    it "adds existing user as collaborator" do
      @mail.to          = address("recipient@example.com")
      @state.recipient  = 'recipient@example.com'
      @mail.cc          = address("new@example.com")
      existing_user = @state.account.users.create!(name: 'new', email: 'new@example.com')
      execute

      assert_equal 'new', @state.ticket.current_collaborators
      assert_equal existing_user.id, @state.ticket.collaborations.first.user_id
      assert @state.ticket.collaborations.first.valid?
    end
  end

  describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
    describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
      describe_with_arturo_setting_enabled :ticket_followers_allowed do
        it "updates ticket current collaborators" do
          execute
          current_collaborators =
            "Unknown 1 <1@example.com>, Unknown 2 <2@example.com>, Unknown 3 <3@example.com>, Unknown 4 <4@example.com>, " \
            "Unknown 5 <5@example.com>, Unknown 6 <6@example.com>, Unknown A <a@example.com>, Unknown B <b@example.com>, " \
            "Unknown C <c@example.com>, Unknown D <d@example.com>, Unknown E <e@example.com>, Unknown F <f@example.com>"
          assert_equal current_collaborators, @state.ticket.current_collaborators
        end

        it "adds existing user as collaborator" do
          @mail.to          = address("recipient@example.com")
          @state.recipient  = 'recipient@example.com'
          @mail.cc          = address("new@example.com")
          existing_user = @state.account.users.create!(name: 'new', email: 'new@example.com')
          execute

          assert_equal 'new <new@example.com>', @state.ticket.current_collaborators
          assert_equal existing_user.id, @state.ticket.collaborations.first.user_id
          assert @state.ticket.collaborations.first.valid?
        end
      end
    end
  end

  describe "when author is a new end-user collaborator (CC'ed externally)" do
    before do
      @end_user = users(:minimum_end_user)
      @state.author = @end_user
      @state.author.stubs(email: @end_user.email)
      @state.ticket.stubs(:current_user).returns(@end_user)
      cc_recipients << @end_user.email
      @mail.cc = cc_recipients.map { |address| address(address) }
    end

    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      it "adds collaborators until collaborator limit is reached" do
        execute
        assert_equal 5, @state.ticket.collaborations.size
      end

      it "does not include agent-collaborators as part of the limit" do
        cc_recipients.unshift(users(:minimum_agent).email)
        cc_recipients.unshift(users(:minimum_admin).email)
        @mail.cc = cc_recipients.map { |address| address(address) }

        execute

        assert_equal 7, @state.ticket.collaborations.size
      end
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
        describe_with_arturo_setting_enabled :ticker_followers_allowed do
          describe "for a new ticket" do
            it "adds collaborators beyond old limit" do
              execute
              assert_equal 13, @state.ticket.collaborations.size
            end
          end

          describe "for an existing ticket" do
            before do
              @state.ticket = tickets(:minimum_1)
              @state.ticket.stubs(:current_user).returns(@end_user)
            end

            describe_with_arturo_enabled :email_make_other_user_comments_private do
              it "does not add the author as an email CC" do
                execute
                refute_includes @state.ticket.collaborations.map(&:user), @end_user
              end
            end

            describe_with_arturo_disabled :email_make_other_user_comments_private do
              it "adds only the author as an email CC" do
                execute
                @state.ticket.collaborations.first.user.must_equal @end_user
              end
            end
          end
        end
      end
    end
  end

  describe "when the mail object has duplicated collaborators" do
    before do
      @duplicate_address = cc_recipients.first
      @mail.to = address(to_recipients.first)
      @mail.cc = @mail.to + @mail.cc
    end

    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      it "does not add duplicate collaborators" do
        execute

        assert_equal 1, @state.ticket.collaborations.select { |collaboration| collaboration.user.email == @duplicate_address }.size
        assert_equal 5, @state.ticket.collaborations.size
      end
    end

    describe_with_arturo_disabled :has_email_extensible_followers do
      it "does not add duplicate collaborators" do
        execute

        assert_equal 1, @state.ticket.collaborations.select { |collaboration| collaboration.user.email == @duplicate_address }.size
        assert_equal 5, @state.ticket.collaborations.size
      end
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
        it "does not add duplicate collaborators" do
          execute

          assert_equal 1, @state.ticket.collaborations.select { |collaboration| collaboration.user.email == @duplicate_address }.size
          assert_equal 7, @state.ticket.collaborations.size
        end
      end
    end
  end

  describe "with new email from potential loop source" do
    before do
      @mail.from = address("random@messages.foo.netsuite.com")
      @state.ticket.will_be_saved_by(@state.ticket.requester)
      @state.ticket.current_user.stubs(:email).returns("random@messages.foo.netsuite.com")
    end

    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      it "does not suspend" do
        @state.ticket.current_user.expects(:can?).with(:add_collaborator, @state.ticket).returns(true)
        execute
        refute @state.suspended?
      end
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
        it "does not suspend" do
          @state.ticket.current_user.expects(:can?).with(:add_email_cc, @state.ticket).returns(true)
          @state.ticket.current_user.expects(:can?).with(:add_follower, @state.ticket).returns(true)
          execute
          refute @state.suspended?
        end
      end
    end
  end

  describe "when ticket is suspended" do
    before do
      @state.stubs(:suspended?).returns(true)
      @state.ticket = SuspendedTicket.new(account: @state.account)
      @state.ticket.properties ||= {}
      @state.ticket.metadata ||= {}
    end

    describe_with_and_without_arturo_setting_enabled :follower_and_email_cc_collaborations do
      before do
        if @state.account.has_follower_and_email_cc_collaborations_enabled?
          Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
        end
      end

      # Technically for CCs/Followers is_collaborators_addable_only_by_agents? will always
      # return false so these tests do not matter when CCs/Followers is enabled (especially
      # since they are just stubbing the return value of the is_collaborators_addable_only_by_agents?
      # method), but will leave them for now.
      describe "account allows for ticket collaborators" do
        describe "account allows end-user to add collaborators" do
          it "adds collaborators to ticket's metadata" do
            execute
            assert_equal 12, @state.ticket.metadata[:ccs].size
          end
        end

        describe "when account does not allow end-user to add collaborators" do
          before { Account.any_instance.stubs(is_collaborators_addable_only_by_agents?: true) }

          describe "the ticket author is an agent" do
            before { @state.author.stubs(is_agent?: true) }

            it "adds collaborators to ticket's metadata" do
              execute
              assert_equal 12, @state.ticket.metadata[:ccs].size
            end
          end

          describe "the ticket author is an end-user" do
            before { @state.author.stubs(is_agent?: false) }

            it "skips adding collaborators" do
              execute
              assert_blank @state.ticket.metadata[:ccs]
            end
          end
        end
      end
    end

    describe "when account does not allow for ticket collaborators" do
      before { Account.any_instance.stubs(is_collaboration_enabled?: false) }

      describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
        it "skips adding collaborators" do
          execute
          assert_blank @state.ticket.metadata[:ccs]
        end
      end
    end
  end

  describe "#suspended_ticket_collaboration_enabled?" do
    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      describe "when old collaboration setting is enabled" do
        before do
          Account.any_instance.stubs(is_collaboration_enabled?: true)
        end

        it { assert instance.send(:suspended_ticket_collaboration_enabled?) }
      end

      describe "when old collaboration setting is disabled" do
        before do
          Account.any_instance.stubs(is_collaboration_enabled?: false)
        end

        it { refute instance.send(:suspended_ticket_collaboration_enabled?) }
      end
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      describe_with_arturo_setting_disabled :comment_email_ccs_allowed do
        describe_with_arturo_setting_disabled :ticket_followers_allowed do
          it { refute instance.send(:suspended_ticket_collaboration_enabled?) }
        end
      end

      describe_with_arturo_setting_enabled :comment_email_ccs_allowed do
        it { assert instance.send(:suspended_ticket_collaboration_enabled?) }
      end

      describe_with_arturo_setting_enabled :ticket_followers_allowed do
        it { assert instance.send(:suspended_ticket_collaboration_enabled?) }
      end
    end
  end

  # Removes collaborators that break CollaboratorExclusion rules:
  # ExcludedAddress, BadReplyAddressAndTicketReplyBug, InvalidDomain, DuplicatedRequesterAddress
  describe '#valid_collaborator_addresses' do
    # Remove plus-IDs from Reply-To and From addresses (The glorious plus ID bug)
    before { @mail.reply_to = address('support+id123456@zendesk-test.com') }

    it "removes the plus IDs before doing the exclusion" do
      execute
      addresses = @state.ticket.collaborations.map { |collaboration| collaboration.user.email }
      assert_includes addresses, 'support@zendesk-test.com'
    end
  end

  # Tests for new CCs functionality (follower and email CCs)
  describe "with agents CC'ed on the email and the main CCs settings enabled" do
    let(:to_recipients) { ['a@example.com'] }
    let(:cc_recipients_with_no_agents) { ['1@example.com'] }
    let(:cc_recipients_with_one_agent) { ['1@example.com', 'minimum_admin@aghassipour.com'] }
    let(:cc_recipients_with_multiple_agents) { ['1@example.com', 'minimum_admin@aghassipour.com', 'minimum_admin_not_owner@aghassipour.com'] }

    before do
      Account.any_instance.stubs(has_follower_and_email_cc_collaborations_enabled?: true)
      Account.any_instance.stubs(has_ticket_followers_allowed_enabled?: true)
      Account.any_instance.stubs(has_comment_email_ccs_allowed_enabled?: true)
    end

    describe "with agent_email_ccs_become_followers setting enabled" do
      let(:agent_email_ccs_become_followers) { true }

      describe "without agents CC'ed on the email" do
        before { @mail.cc = cc_recipients_with_no_agents.map { |address| address(address) } }

        it "does not create any new follower collaborations" do
          execute

          assert_equal 2, @state.ticket.collaborations.size
          refute @state.ticket.collaborations.
            map(&:collaborator_type).
            any? { |type| type == CollaboratorType.FOLLOWER }
        end
      end

      def collaboration_exists?(ticket, email, collaborator_type)
        ticket.collaborations.any? do |c|
          c.user.emails_include?(email) && c.collaborator_type == collaborator_type
        end
      end

      describe "with one agent CC'ed on the email" do
        before { @mail.cc = cc_recipients_with_one_agent.map { |address| address(address) } }

        it "adds both follower and email_cc collaborations for the agent" do
          execute
          assert_equal 4, @state.ticket.collaborations.size
          assert collaboration_exists?(@state.ticket, "minimum_admin@aghassipour.com", CollaboratorType.EMAIL_CC)
          assert collaboration_exists?(@state.ticket, "minimum_admin@aghassipour.com", CollaboratorType.FOLLOWER)
        end
      end

      describe "with more than one agent CC'ed on the email" do
        before { @mail.cc = cc_recipients_with_multiple_agents.map { |address| address(address) } }

        it "adds both follower and email_cc collaborations for each agent" do
          execute
          assert_equal 6, @state.ticket.collaborations.size
          assert collaboration_exists?(@state.ticket, "minimum_admin@aghassipour.com", CollaboratorType.EMAIL_CC)
          assert collaboration_exists?(@state.ticket, "minimum_admin@aghassipour.com", CollaboratorType.FOLLOWER)
          assert collaboration_exists?(@state.ticket, "minimum_admin_not_owner@aghassipour.com", CollaboratorType.EMAIL_CC)
          assert collaboration_exists?(@state.ticket, "minimum_admin_not_owner@aghassipour.com", CollaboratorType.FOLLOWER)
        end
      end
    end

    describe "with agent_email_ccs_become_followers setting disabled" do
      describe "with any number of agents CC'ed on the email" do
        before { @mail.cc = cc_recipients_with_multiple_agents.map { |address| address(address) } }

        it "does not add any new follower collaborations for the agent(s)" do
          execute

          assert_equal 4, @state.ticket.collaborations.size
          refute @state.ticket.collaborations.
            map(&:collaborator_type).
            any? { |type| type == CollaboratorType.FOLLOWER }
        end
      end
    end
  end

  # When "third-parties" try to update a ticket that belongs to other, without
  # been added as a collaborator, then the ticket gets flaged as OTHER_USER_UPDATE
  describe 'when third-parties update the ticket (private_comment_security_fix)' do
    before do
      @state.flags << EventFlagType.OTHER_USER_UPDATE
      @state.ticket.stubs(new_record?: false)
    end

    describe_with_arturo_setting_enabled(:email_make_other_user_comments_private) do
      it 'stops the collaboration update' do
        mail_collaboration.expects(:build_all).never
        execute
      end

      describe 'when the third-party has a netsuite email address' do
        let(:from_email) { 'bob@abc.netsuite.com' }
        let(:cc_recipients) { [from_email] }
        let(:suspended_ticket) { mock }

        before do
          @state.from = address('Bob <bob@abc.netsuite.com>')
          author.email = from_email
          author.save!
        end

        it 'applies the private comment security fix' do
          # Creates a new suspended ticket
          processor.expects(:new).with(@state, @mail).returns(suspended_ticket).once
          suspended_ticket.expects(:suspend_ticket).once

          # Skips the collaboration
          mail_collaboration.expects(:build_all).never
          execute

          # Flags the mail
          assert_equal SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE, @state.suspension
        end
      end
    end

    describe_with_arturo_setting_disabled(:email_make_other_user_comments_private) do
      it "doesn't apply the private comment security fix" do
        execute

        # It doesn't flags the mail
        refute @state.flags.include?(SuspensionType.UNAUTHENTICATED_EMAIL_UPDATE)
        assert @state.flags.include?(EventFlagType.OTHER_USER_UPDATE)
      end
    end
  end

  def self.it_should_stop_the_collaboration_update
    it 'should stop the collaboration update' do
      mail_collaboration.expects(:build_all).never
      execute
    end
  end

  def self.it_should_not_increment_statsd
    it 'should not increment reply_to_processor statsd' do
      statsd_client.expects(:increment).never
      execute
    end
  end

  def self.it_should_increment_statsd
    it 'should increment reply_to_processor statsd' do
      statsd_client.expects(:increment).with('reply_to_processor.collaborator_processor_skipped').once
      execute
    end
  end

  def self.it_should_update_the_ticket_collaborators(expected_collaborators: 5)
    it 'updates the ticket collaborators' do
      execute
      assert_equal expected_collaborators, @state.ticket.collaborations.size
    end
  end

  # When the sender of email impersonates as an agent by adding the agents' email address in reply-to header,
  # the email will be flagged as possible reply-to agent spoof and should skip collaboration updates
  describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
    let(:statsd_client) { Zendesk::InboundMail::StatsD.client }

    before do
      @state.author = users(:minimum_agent)
      @state.ticket = tickets(:minimum_1)
      @state.ticket.will_be_saved_by(@state.author)
      @state.ticket.requester = @state.author
    end

    describe 'for a new ticket' do
      before { @state.ticket.stubs(new_record?: true) }

      describe 'when the email is flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
        before { @state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) }

        it_should_stop_the_collaboration_update
        it_should_increment_statsd
      end

      describe 'when the email is not flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
        it_should_update_the_ticket_collaborators
        it_should_not_increment_statsd
      end

      describe 'when email_reply_to_vulnerability_collaborator_processor Arturo is disabled' do
        before { Account.any_instance.stubs(has_email_reply_to_vulnerability_collaborator_processor?: false) }

        describe 'when the email is flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
          before { @state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) }

          it_should_update_the_ticket_collaborators
          it_should_not_increment_statsd
        end

        describe 'when the email is not flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
          it_should_update_the_ticket_collaborators
          it_should_not_increment_statsd
        end
      end
    end

    describe 'for an existing ticket' do
      let(:user) { users(:minimum_admin) }

      before do
        @state.ticket.stubs(:new_record?).returns(false)
        @mail.cc << address(user.email)
        @state.ticket.add_comment(body: "test comment", is_public: true, author: @state.author)
      end

      describe 'when the emails is flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
        before { @state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) }

        it_should_stop_the_collaboration_update
        it_should_increment_statsd
      end

      describe 'when the emails is not flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
        it_should_update_the_ticket_collaborators(expected_collaborators: 6)
        it_should_not_increment_statsd
      end

      describe 'when email_reply_to_vulnerability_collaborator_processor Arturo is disabled' do
        before { Account.any_instance.stubs(has_email_reply_to_vulnerability_collaborator_processor?: false) }

        describe 'when the emails is flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
          before { @state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) }

          it_should_update_the_ticket_collaborators(expected_collaborators: 6)
          it_should_not_increment_statsd
        end

        describe 'when the emails is not flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
          it_should_update_the_ticket_collaborators(expected_collaborators: 6)
          it_should_not_increment_statsd
        end
      end
    end
  end

  describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
    before do
      Account.any_instance.stubs(:has_comment_email_ccs_allowed_enabled?).returns(true)
      Account.any_instance.stubs(:has_ticket_followers_allowed_enabled?).returns(true)
      @state.author = users(:minimum_agent)
      @state.ticket = tickets(:minimum_1)
      @state.ticket.will_be_saved_by(@state.author)
      @state.ticket.requester = @state.author
    end

    describe 'for a new ticket' do
      before { @state.ticket.stubs(new_record?: true) }

      describe 'when the email is flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
        before { @state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) }

        it_should_stop_the_collaboration_update
      end

      describe 'when the email is not flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
        it_should_update_the_ticket_collaborators(expected_collaborators: 12)
      end

      describe 'when email_reply_to_vulnerability_collaborator_processor Arturo is disabled' do
        before { Account.any_instance.stubs(has_email_reply_to_vulnerability_collaborator_processor?: false) }

        describe 'when the email is flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
          before { @state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) }

          it_should_update_the_ticket_collaborators(expected_collaborators: 12)
        end

        describe 'when the email is not flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
          it_should_update_the_ticket_collaborators(expected_collaborators: 12)
        end
      end
    end

    describe 'for an existing ticket' do
      let(:user) { users(:minimum_admin) }

      before do
        @state.ticket.stubs(:new_record?).returns(false)
        @mail.cc = address(user.email)
        @state.ticket.add_comment(body: "test comment", is_public: true, author: @state.author)
      end

      describe 'when the emails is flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
        before { @state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) }

        it_should_stop_the_collaboration_update
      end

      describe 'when the emails is not flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
        it_should_update_the_ticket_collaborators(expected_collaborators: 1)
      end

      describe 'when email_reply_to_vulnerability_collaborator_processor Arturo is disabled' do
        before { Account.any_instance.stubs(has_email_reply_to_vulnerability_collaborator_processor?: false) }

        describe 'when the emails is flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
          before { @state.add_flag(EventFlagType.REPLY_TO_AGENT_POSSIBLE_SPOOF) }

          it_should_update_the_ticket_collaborators(expected_collaborators: 1)
        end

        describe 'when the emails is not flagged as REPLY_TO_AGENT_POSSIBLE_SPOOF' do
          it_should_update_the_ticket_collaborators(expected_collaborators: 1)
        end
      end
    end
  end
end
