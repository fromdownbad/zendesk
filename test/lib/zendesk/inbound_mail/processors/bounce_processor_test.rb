require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 9

describe Zendesk::InboundMail::Processors::BounceProcessor do
  include ArturoTestHelper

  fixtures :accounts, :recipient_addresses, :targets, :tickets

  def address(email)
    Zendesk::Mail::Address.new(address: email)
  end

  def execute
    Zendesk::InboundMail::Processors::BounceProcessor.execute(@state, mail)
  end

  def instance
    Zendesk::InboundMail::Processors::BounceProcessor.new(@state, mail)
  end

  def assert_delivered
    instance ? instance.execute : execute
    @state.suspension.must_be_nil
  end

  def assert_rejected(instance = nil)
    assert_raise(Zendesk::InboundMail::HardMailRejectException) do
      instance ? instance.execute : execute
    end
  end

  def assert_logs_to_statsd(metric = :bounce, status_code = '5.0.0', bounce_type = "hard", target_deliverability_action = "no_target", reject_only = "no", bounce_via = "email_body")
    Zendesk::StatsD::Client.any_instance.expects(:increment).with(metric, tags: ["bounce_via:#{bounce_via}", "bounce_type:#{bounce_type}", "status_code:#{status_code}", "target_deliverability_action:#{target_deliverability_action}", "reject_only:#{reject_only}"]).once
    assert_rejected(@instance)
  end

  def refute_logs_to_statsd(metric = :bounce)
    Zendesk::StatsD::Client.any_instance.expects(:increment).with(metric).never
    assert_delivered
  end

  def assert_deliverable(recipient)
    assert_equal(DeliverableStateType.DELIVERABLE, recipient.reload.deliverable_state)
  end

  def assert_undeliverable(recipient)
    assert_equal(DeliverableStateType.UNDELIVERABLE, recipient.reload.deliverable_state)
  end

  def assert_zero_undeliverable_count(recipient)
    assert_equal 0, recipient.reload.undeliverable_count
  end

  let(:account) { accounts(:minimum) }
  let(:example_address) { "someone@example.com" }
  let(:recipient) { user_identities(:minimum_end_user) }
  let(:bouncing_email) { recipient.value }
  let(:hard_bounce_content) do
    %(
      foo\n
      Status: 5.0.0\n
      An error occurred while trying to deliver the mail to the following recipients: #{bouncing_email}

      500-5.0.0 The user you are trying to contact is receiving mail at a rate that
      500-5.0.0 prevents additional messages from being delivered. Please resend your
      500-5.0.0 message at a later time. If the user is able to receive mail at that
      500-5.0.0 time, your message will be delivered. For more information, please
      500-5.0.0 visit
      500 5.0.0  https://support.google.com/mail/?p=ReceivingRate e64si1652721ybi.316 - gsmtp
      said: 550-5.1.1 The email account that you tried to reach does not exist.
    )
  end

  let(:soft_bounce_content) do
    %(
      foo\n
      Status: 5.2.0\n
      An error occurred while trying to deliver the mail to the following recipients: #{bouncing_email}

      Diagnostic-Code: smtp; 550-5.1.1 The email account that you tried to reach does not exist. Please try
        520-5.2.0 double-checking the recipient's email address for typos or
        520-5.2.0 unnecessary spaces. Learn more at
        520 5.2.0  https://support.google.com/mail/?p=NoSuchUser t30si2927556uat.28 - gsmtp

    )
  end

  # This is a real example from qq.com when they limit us based on outbound IP
  let(:reject_only_subject) { "DELIVERY FAILURE: 550 Ip frequency limited." }

  let(:mail) { FixtureHelper::Mail.read('minimum_ticket.json') }

  before do
    @state = Zendesk::InboundMail::ProcessingState.new
    @state.account = account
    @state.plain_content = "valid content"
    @state.message_id = mail.message_id

    # Remove when email_update_all_targets_on_bounce gets GA'd
    account.stubs(has_email_update_all_targets_on_bounce?: false)

    # Remove when `email_switch_bounce_processing*` arturos get removed
    account.stubs(has_email_switch_bounce_processing_logging_mode?: false)
    account.stubs(has_email_switch_bounce_processing_debugging_mode?: false)
  end

  describe "neither subject nor from indicate delivery failure" do
    it "does not reject" do
      assert_delivered
    end
  end

  describe "only subject indicates delivery failure" do
    before do
      mail.subject = "Delivery Status Notification (Failure)"
    end

    it "does not reject" do
      assert_delivered
    end
  end

  describe "only from indicates delivery failure" do
    before do
      mail.from = address("mailer-daemon@example.com")
    end

    it "does not reject" do
      assert_delivered
    end
  end

  describe "when Subject and From indicates a delivery failure notification" do
    before do
      mail.subject = "Delivery Status Notification (Failure)"
      mail.from = address("mailer-daemon@example.com")
      mail.logger.stubs(:info)
    end

    describe "from address matches undeliverable mail senders" do
      [
        "mailer-daemon@example.com",
        "postmaster@example.com",
        "failurenotice@example.com",
        "Failurenotice@example.com",
        "internet@example.com",
        "noreply@example.com",
        "gateway@example.com",
        "post.office@example.com",
        "sold.noreply@example.com",
      ].each do |from|
        it "rejects with from address #{from}" do
          mail.from = address(from)
          assert_rejected
        end
      end
    end

    it "does not reject with regular from address" do
      mail.from = address(example_address)
      assert_delivered
    end

    describe "subject matches undeliverable notification patterns" do
      [
        "Undeliverable:",
        "Undelivered Mail",
        "Mail Returned",
        "Delivery Failure",
        "failure notice",
        "Returned mail",
        "Undeliverable mail",
        "Mail delivery failed",
        "Delivery Status Notification (Failure)",
        "delivery status notification (failure)",
        "NDN:", "NDN: Whatever",
      ].each do |subject|
        it "rejects with subject '#{subject}'" do
          mail.subject = subject
          assert_rejected
        end
      end
    end

    describe "subject contains beginning-only undeliverable notification patterns after the beginning" do
      [
        "Undeliverable:",
        "Undelivered Mail",
        "Mail Returned",
        "NDN:",
      ].each do |subject|
        it "does not reject with subject '#{subject}'" do
          subject = "foo #{subject} bar"
          mail.subject = subject
          assert_delivered
        end
      end
    end

    describe "#update_target_deliverability!" do
      # This is bad, because we're testing only the first target.
      let(:email_target) { @state.account.targets.email.active.first }

      before do
        @instance = instance
        email_target.update_attributes(email: bouncing_email)
      end

      def call_update_target_deliverability!
        instance.send(:update_target_deliverability!, bouncing_email)
      end

      describe "with a hard bounce" do
        before { @state.plain_content = hard_bounce_content }

        it "deactivates the target" do
          assert email_target.is_active
          TargetsMailer.expects(:deliver_target_disabled).once.with(email_target)
          call_update_target_deliverability!
          refute email_target.reload.is_active
        end

        describe "statsd logging" do
          it "increments target_deliverability_action:deactivated stat for a hard bounce" do
            TargetsMailer.expects(:deliver_target_disabled).once.with(email_target)
            assert_logs_to_statsd(:bounce, "5.0.0", "hard", "deactivated")
          end

          it "increments target_deliverability_action:no_target stat if target not attached to email address" do
            email_target.update_attributes(email: "different@example.com")
            assert_logs_to_statsd(:bounce, "5.0.0", "hard", "no_target")
          end

          it "increments target_deliverability_action:inactive_target stat when target is inactive" do
            email_target.update_attributes(is_active: false)
            assert_logs_to_statsd(:bounce, "5.0.0", "hard", "inactive_target")
          end
        end
      end

      describe "with a soft bounce" do
        before { @state.plain_content = soft_bounce_content }

        it "increments target failures" do
          email_target.failures.must_equal 0
          call_update_target_deliverability!
          email_target.reload.failures.must_equal 1
        end

        it "deactivates the target when max of #{Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT} soft bounces have been reached" do
          email_target.update_attributes(failures: Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT - 1)
          assert email_target.is_active
          TargetsMailer.expects(:deliver_target_disabled).once.with(email_target)
          call_update_target_deliverability!
          refute email_target.reload.is_active
        end

        it "does not deactivate the target when max of #{Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT} soft bounces has yet to be reached" do
          email_target.update_attributes(failures: 1)
          call_update_target_deliverability!
          assert email_target.reload.is_active
          email_target.failures.must_equal 2
        end

        describe "statsd logging" do
          it "increments target_deliverability_action:failure_count_incremented stat" do
            assert_logs_to_statsd(:bounce, "5.2.0", "soft", "failure_count_incremented")
          end

          it "increments target_deliverability_action:deactivated stat when max of #{Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT} soft bounces has been reached" do
            TargetsMailer.expects(:deliver_target_disabled).once.with(email_target)
            email_target.update_attributes(failures: Zendesk::InboundMail::MAX_UNDELIVERABLE_COUNT - 1)
            assert_logs_to_statsd(:bounce, "5.2.0", "soft", "deactivated")
          end

          it "increments target_deliverability_action:inactive_target stat when target is inactive" do
            email_target.update_attributes(is_active: false)
            assert_logs_to_statsd(:bounce, "5.2.0", "soft", "inactive_target")
          end
        end
      end
    end
  end

  describe "#undeliverable_recipient" do
    def assert_found_email(email_body)
      @state.from = address("mailer-daemon@test.com")
      @state.plain_content = email_body

      instance.send(:undeliverable_recipient).wont_be_nil
    end

    describe "body contains known failed recipient patterns" do
      [
        "An error occurred while trying to deliver the mail to the following recipients:",
        "Final-Recipient: rfc822;",
        "Delivery to the following recipient has been delayed:",
        "Delivery to the following recipients failed.",
        "Delivery to the following recipient failed permanently:",
        "Unable to deliver message to the following recipients, due to being unable to connect successfully to the destination mail server.",
        "The following address failed: <",
        "Diagnostic information for administrators: Generating server: blah.blah\n\n",
        "Recipient Address:"
      ].each do |content|
        it "finds the failed recipient address" do
          assert_found_email("#{content}\n#{example_address}\n")
        end
      end

      it "finds the failed recipient address in Zendesk bounce messages" do
        assert_found_email("If you do so, please include this problem report. You can\ndelete your own text from the attached returned message.\n\n                   The mail system\n\n<#{example_address}>")
      end
    end

    describe "when failed address is enclosed by different types of or no whitespace" do
      before do
        @content = "An error occurred while trying to deliver the mail to the following recipients:"
      end

      it "finds the failed address when separated by a space instead of a newline" do
        assert_found_email("#{@content} #{example_address}\n")
      end

      it "finds the failed address when followed by a space instead of a newline" do
        assert_found_email("#{@content}\n#{example_address} ")
      end

      it "finds the failed address when no whitespace precedes it" do
        assert_found_email("#{@content}#{example_address}\n")
      end

      it "finds the failed address when no whitespace follows it" do
        assert_found_email("#{@content}\n#{example_address}")
      end

      it "finds the failed address when preceded by two newlines" do
        assert_found_email("#{@content}\n\n#{example_address}")
      end
    end
  end

  describe "#status_code" do
    let(:mail) { FixtureHelper::Mail.read('bounce_email.json') }
    before { @instance = instance }

    def assert_status_code_class(class_code)
      @instance.send(:status_code_class).must_equal class_code
    end

    def assert_status_code_subjects(subjects)
      @instance.send(:status_code_subjects).must_equal subjects
    end

    def stub_content(content)
      @state.plain_content = content
    end

    describe "when body does not contain known status code pattern" do
      before do
        stub_content("No status code")
      end

      it "does not find class or subjects" do
        assert_status_code_class("0")
        assert_status_code_subjects("0")
      end
    end

    ["2", "4", "5"].each do |status_code_class|
      describe "when body contains status code with class #{status_code_class}" do
        it "finds the class" do
          stub_content("Status: #{status_code_class}.0.0")
          assert_status_code_class(status_code_class)
        end
      end
    end

    [
      "Status:", "The error that the other server returned was: 000", "Remote Server returned '000",
      "The following address failed: <a@example.com>: 000",
      "Diagnostic information for administrators: Generating server: blah.blah\n\na@example.com #000-",
      "Diagnostic information for administrators: Generating server: blah.blah\n\na@example.com\n\n#000 "
    ].each do |content|
      describe "when body contains known status code pattern: #{content}" do
        before do
          stub_content("#{content} 5.1.0")
        end

        it "finds the class and subjects" do
          assert_status_code_class("5")
          assert_status_code_subjects("1")
        end
      end
    end

    describe "when body contains status code with multi-digit subject" do
      before do
        stub_content("Status: 5.123.0")
      end

      it "finds the class and subjects" do
        assert_status_code_class("5")
        assert_status_code_subjects("123")
      end
    end

    describe "when body contains status code with multi-digit detail" do
      before do
        stub_content("Status: 5.0.123")
      end

      it "finds the class and subjects" do
        assert_status_code_class("5")
        assert_status_code_subjects("0")
      end
    end

    describe "when body contains status code with multi-digit subject and detail" do
      before do
        stub_content("Status: 5.123.456")
      end

      it "finds the class and subjects" do
        assert_status_code_class("5")
        assert_status_code_subjects("123")
      end
    end

    describe "#reject_only_bounce?" do
      before { @instance = instance }

      [
        "Ip Frequency Limited",
        "DELIVERY FAILURE: 550 Ip frequency limited."
      ].each do |subject|
        it "is reject_only when the subject matches '#{subject}'" do
          @instance.stubs(:subject).returns(subject)
          @instance.send(:reject_only_bounce?).must_equal true
        end
      end

      [
        "Ip",
        "DELIVERY FAILURE:"
      ].each do |subject|
        it "is not reject_only when the subject matches '#{subject}'" do
          @instance.stubs(:subject).returns(subject)
          @instance.send(:reject_only_bounce?).must_equal false
        end
      end
    end

    describe "when body contains alternate whitespace before the status code" do
      before do
        stub_content("The error that the other server returned was:\n400\n4.0.0")
      end

      it "finds the class and subjects" do
        assert_status_code_class("4")
        assert_status_code_subjects("0")
      end
    end
  end

  describe "#hard_bounce?" do
    before { @instance = instance }

    def stub_status_code(status_code_class, status_code_subjects)
      @instance.stubs(:status_code_class).returns(status_code_class)
      @instance.stubs(:status_code_subjects).returns(status_code_subjects)
    end

    def assert_hard_bounce(status_code_class, status_code_subjects)
      stub_status_code(status_code_class, status_code_subjects)
      @instance.send(:hard_bounce?).must_equal true
    end

    def refute_hard_bounce(status_code_class, status_code_subjects)
      stub_status_code(status_code_class, status_code_subjects)
      @instance.send(:hard_bounce?).must_equal false
    end

    it "is a soft bounce when status code class is 2 or 4" do
      refute_hard_bounce("2", "0")
      refute_hard_bounce("4", "0")
    end

    it "is a hard bounce when status code class is 5 and subject is not 2" do
      assert_hard_bounce("5", "0")
      assert_hard_bounce("5", "12")
    end

    it "is a soft bounce when status code class is 5 and subject is 2" do
      refute_hard_bounce("5", "2")
    end

    it "doesn't care if the email is considered reject-only" do
      @instance.stubs(:subject).returns("Ip frequency limited")
      assert_hard_bounce("5", "0")
      refute_hard_bounce("5", "2")
    end
  end

  describe "#soft_bounce?" do
    describe "when is a hard bounce?" do
      let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email.json') }
      it { refute instance.send(:soft_bounce?) }
    end

    describe "when is a soft bounce?" do
      let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email_soft.json') }
      it { assert instance.send(:soft_bounce?) }
    end
  end

  describe "#reject_only_bounce?" do
    before { @instance = instance }

    [
      "Ip Frequency Limited",
      "DELIVERY FAILURE: 550 Ip frequency limited."
    ].each do |subject|
      it "is reject_only when the subject matches '#{subject}'" do
        @instance.stubs(:subject).returns(subject)
        @instance.send(:reject_only_bounce?).must_equal true
      end
    end

    [
      "Ip",
      "DELIVERY FAILURE:"
    ].each do |subject|
      it "is not reject_only when the subject matches '#{subject}'" do
        @instance.stubs(:subject).returns(subject)
        @instance.send(:reject_only_bounce?).must_equal false
      end
    end
  end

  describe "metrics for Zendesk welcome email bounces" do
    before do
      @instance = instance
      @instance.stubs(:detected_as_bounce?).returns(true)
    end

    describe_with_arturo_enabled :email_log_welcome_email_bounce do
      describe "when the email is a bounce notification in response to a Zendesk welcome email" do
        before { @instance.stubs(:response_to_zendesk_welcome_mail?).returns(true) }

        it "increments the statsd client" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('welcome_email_bounce').once
          assert_rejected(@instance)
        end
      end

      describe "when the email is not a bounce notification in response to a Zendesk welcome email" do
        before { @instance.stubs(:response_to_zendesk_welcome_mail?).returns(false) }

        it "does not increment the statsd client" do
          Zendesk::StatsD::Client.any_instance.expects(:increment).with('welcome_email_bounce').never
          assert_rejected(@instance)
        end
      end
    end

    describe_with_arturo_disabled :email_log_welcome_email_bounce do
      [true, false].each do |response_to_zendesk_welcome_mail|
        describe "when it is #{response_to_zendesk_welcome_mail} that the email is a bounce notification in response to a Zendesk welcome email" do
          before { @instance.stubs(:response_to_zendesk_welcome_mail?).returns(response_to_zendesk_welcome_mail) }

          it "does not increment the statsd client" do
            Zendesk::StatsD::Client.any_instance.expects(:increment).with('welcome_email_bounce').never
            assert_rejected(@instance)
          end
        end
      end
    end
  end

  describe "#response_to_zendesk_welcome_mail?" do
    before do
      @instance = instance
      mail.account = accounts(:minimum)
      @state.plain_content = mail.content.text
    end

    describe "when a bounced Zendesk welcome email is detected in the bounce notification email's body" do
      let(:mail) { FixtureHelper::Mail.read('zendesk_welcome_email_body_bounce_email.json') }

      it { assert instance.send(:response_to_zendesk_welcome_mail?) }
    end

    describe "when a bounced Zendesk welcome email is detected in an attached copy of the original welcome email" do
      let(:mail) { FixtureHelper::Mail.read('zendesk_welcome_email_original_email.json') }
      it { assert instance.send(:response_to_zendesk_welcome_mail?) }
    end

    describe "when a bounced Zendesk welcome email is detected in an attached delivery status notification" do
      let(:mail) { FixtureHelper::Mail.read('zendesk_welcome_email_dsm_bounce_email.json') }
      it { assert instance.send(:response_to_zendesk_welcome_mail?) }
    end

    describe "when no bounced Zendesk welcome email is present" do
      let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email_soft.json') }
      it { refute instance.send(:response_to_zendesk_welcome_mail?) }
    end
  end

  describe "when Subject and From indicates a delivery failure notification" do
    let(:new_identity) { UserIdentity.find_by_email(bouncing_email) }

    before do
      mail.subject = "Delivery Status Notification (Failure)"
      mail.from = address("mailer-daemon@example.com")
      @instance = instance
    end

    describe "when email_switch_bounce_processing_logging_mode Arturo is enabled" do
      before do
        account.stubs(has_email_switch_bounce_processing_logging_mode?: true)
        @state.plain_content = hard_bounce_content
      end

      # Stats will indicate that Target deliverability got "skipped"
      it { assert_logs_to_statsd(:bounce, "5.0.0", "hard", "skipped", "no") }

      it "rejects and logs the warn message" do
        # One for the rejection itself, and the other for "Update identity and targets deliverability is disabled!"
        @instance.expects(:warn).twice
        assert_rejected(@instance)
      end

      it "rejects but does not update the identity deliverability" do
        @instance.expects(:update_identity_deliverability).never
        assert_rejected(@instance)
      end

      it "rejects but does not update the email targets deliverability" do
        @instance.expects(:update_email_targets_deliverability).never
        @instance.expects(:update_target_deliverability).never
        assert_rejected(@instance)
      end
    end

    it "rejects and does look for failed recipient" do
      @instance.expects(:undeliverable_recipient).twice
      assert_rejected(@instance)
    end

    describe "when body does not contain known status code pattern" do
      before { @state.plain_content = "Status: 8.0.0 An error occurred while trying to deliver the mail to the following recipients: sample@xyz.com\nX-Mailer: Zendesk Mailer\n" }

      it 'it tries to match the status code with the existing patterns' do
        @instance.expects(:status_code_match_data).twice
        assert_rejected(@instance)
      end

      it 'does not log bounce stats' do
        @instance.expects(:log_bounce_stats).never
        assert_rejected(@instance)
      end
    end

    describe "hard bounce" do
      let(:plain_content) { hard_bounce_content }

      before { @state.plain_content = plain_content }

      describe "when email_switch_bounce_processing_debugging_mode Arturo is enabled" do
        before { account.stubs(has_email_switch_bounce_processing_debugging_mode?: true) }

        it "rejects and logs the bounce information" do
          @instance.expects(:log_bounce_information).once
          assert_rejected(@instance)
        end

        describe "#log_bounce_information" do
          before { @instance.stubs(:log) }

          it "logs all the useful data" do
            @instance.expects(:log).with(regexp_matches(/\[DEBUG\] BounceProcessor\: .*/)).once
            assert_rejected(@instance)
          end
        end
      end

      describe "when the UserIdentity already exists" do
        it "is undeliverable" do
          assert_rejected(@instance)
          assert_undeliverable(recipient)
        end

        it { assert_logs_to_statsd(:bounce, "5.0.0", "hard") }

        describe "if the subject indicates this is a reject-only bounce" do
          before do
            mail.subject = reject_only_subject
            assert_rejected(@instance)
          end

          it { assert_deliverable(recipient) }

          it { assert_zero_undeliverable_count(recipient) }

          it { assert_logs_to_statsd(:bounce, "5.0.0", "hard", "no_target", "yes") } # This is still a hard bounce, even if we're ignoring it
        end
      end

      describe "when the UserIdentity doesn't already exist" do
        let(:bouncing_email) { "hard-bounce@example.com" }

        before { assert_nil(UserIdentity.find_by_email(bouncing_email)) }

        describe "if the bounce is a response to a Zendesk-sent mail" do
          let(:plain_content) { hard_bounce_content + "\nX-Mailer: Zendesk Mailer" }

          describe "when the Zendesk is open" do
            before { assert_rejected(@instance) }

            it { assert_present(new_identity) }

            it { new_identity.deliverable_state.must_equal DeliverableStateType.UNDELIVERABLE }

            it { new_identity.value.must_equal 'hard-bounce@example.com' }

            it { new_identity.user.name.must_equal 'Hard-bounce' }

            it { assert_logs_to_statsd(:bounce, "5.0.0", "hard") }
          end

          describe "when the Zendesk is closed (only Agents can add Users)" do
            before do
              @state.account.stubs(is_open?: false)
              assert_rejected(@instance)
            end

            it { refute_present(new_identity) }
          end
        end

        describe "if the bounce is not a response to a Zendesk-sent mail" do
          before { assert_rejected(@instance) }

          it { refute_present(new_identity) }

          it { assert_logs_to_statsd(:bounce, "5.0.0", "hard") }
        end

        describe "if the subject indicates this is a reject-only bounce" do
          before { mail.subject = reject_only_subject }

          it { refute_present(new_identity) }
        end
      end
    end

    describe "soft bounce" do
      let(:plain_content) { soft_bounce_content }

      before { @state.plain_content = plain_content }

      describe "when the UserIdentity already exists" do
        it "increments the undeliverable_count for the recipient" do
          assert_difference('recipient.reload.undeliverable_count', +1) do
            assert_rejected(@instance)
          end
        end

        it { assert_logs_to_statsd(:bounce, "5.2.0", "soft") }

        describe "if the subject indicates this is a reject-only bounce" do
          before do
            mail.subject = reject_only_subject
            assert_rejected(@instance)
          end

          it { assert_deliverable(recipient) }

          it { assert_zero_undeliverable_count(recipient) }
        end
      end

      describe "when the UserIdentity doesn't already exist" do
        let(:bouncing_email) { "soft-bounce@example.com" }

        before do
          assert_nil(UserIdentity.find_by_email(bouncing_email))
          assert_rejected(@instance)
        end

        describe "if the bounce is a response to a Zendesk-sent mail" do
          let(:plain_content) { soft_bounce_content + "\nX-Mailer: Zendesk Mailer" }

          it { assert_present(new_identity) }

          it { new_identity.undeliverable_count.must_equal 1 }

          it { new_identity.value.must_equal 'soft-bounce@example.com' }

          it { new_identity.user.name.must_equal 'Soft-bounce' }

          it { assert_logs_to_statsd(:bounce, "5.2.0", "soft") }
        end

        describe "if the bounce is not a response to a Zendesk-sent mail" do
          it { refute_present(new_identity) }

          it { assert_logs_to_statsd(:bounce, "5.2.0", "soft") }
        end

        describe "if the subject indicates this is a reject-only bounce" do
          before { mail.subject = reject_only_subject }

          it { refute_present(new_identity) }
        end
      end
    end
  end

  describe "when an Attachment indicates a delivery failure notification" do
    let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email.json') }

    before { @instance = instance }

    it "rejects and does look for failed recipient" do
      @instance.expects(:undeliverable_recipient).twice
      assert_rejected(@instance)
    end

    describe "hard bounce" do
      it { assert_logs_to_statsd(:bounce, "5.1.1", "hard", "no_target", "no", "attachment") }
    end

    describe "soft bounce" do
      let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email_soft.json') }
      it { assert_logs_to_statsd(:bounce, "5.2.0", "soft", "no_target", "no", "attachment") }
    end
  end

  describe "#delivery_status_messages" do
    let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email.json') }

    describe "when the email has no delivery status attachments" do
      let(:mail) { FixtureHelper::Mail.read('bounce_email.json') }

      it "returns an empty array" do
        assert_equal [], instance.send(:delivery_status_messages)
      end
    end

    describe "when the email has delivery status attachments" do
      # Email has three attachments: 1) icon.png, 2) DSN file, 3) Original eml
      let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email.json') }

      it "retrieves the DSN attachments" do
        attachments = instance.send(:delivery_status_messages)

        # DSN file: `unnamed_attachment_1`
        assert_equal 1, attachments.size
        assert_equal [mail.attachments.second], attachments

        # This verifies the memoization
        instance.send(:delivery_status_messages)
      end
    end
  end

  describe "#detected_as_bounce?" do
    describe "when Subject and From indicates a delivery failure notification" do
      before do
        @instance = instance
        @instance.stubs(subject_undeliverable?: true)
        @instance.stubs(from_undeliverable?: true)
      end

      it { assert @instance.send(:detected_as_bounce?) }
    end

    describe "when only one of the headers looks like a bounce" do
      it 'is not detected as a bounce' do
        instance.stubs(subject_undeliverable?: true)
        instance.stubs(from_undeliverable?: false)
        refute instance.send(:detected_as_bounce?)

        instance.stubs(subject_undeliverable?: false)
        instance.stubs(from_undeliverable?: true)
        refute instance.send(:detected_as_bounce?)
      end
    end
  end

  describe "#delivery_status_msg_attached?" do
    describe "when the delivery status attachment indicates a failure notification" do
      let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email.json') }
      it { assert instance.send(:delivery_status_msg_attached?) }
    end
  end

  describe "#bounce_via" do
    describe "when the email was detected as bounce and it has delivery status attachment" do
      let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email.json') }

      before do
        @instance = instance
        @instance.stubs(detected_as_bounce?: true)
      end

      it { assert_equal :email_body, @instance.send(:bounce_via) }
    end

    describe "when the email has delivery status attachments" do
      let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email.json') }
      it { assert_equal :attachment, instance.send(:bounce_via) }
    end

    describe "when the email has no delivery status attachments" do
      let(:mail) { FixtureHelper::Mail.read('minimum_ticket.json') }
      it { assert_equal :email_body, instance.send(:bounce_via) }
    end
  end

  describe "#bounce_body" do
    describe "when the email has delivery status attachments" do
      let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email.json') }
      let(:delivery_status_message) { instance.send(:delivery_status_messages).first }

      it "returns the delivery status message as the bounce body" do
        assert_equal delivery_status_message.body, instance.send(:bounce_body)
        refute_equal @state.plain_content, instance.send(:bounce_body)
      end
    end

    describe "when the email has no delivery status attachments" do
      let(:mail) { FixtureHelper::Mail.read('bounce_email.json') }

      it "returns email's plain-text content as the bounce body" do
        assert_equal @state.plain_content, instance.send(:bounce_body)
      end
    end
  end

  describe "#update_email_targets_deliverability!" do
    before do
      # This account has 3 targets (2 active and 1 inactive)
      account.stubs(has_email_update_all_targets_on_bounce?: true)
      @instance = instance
    end

    describe "having a soft bounce" do
      before do
        Zendesk::InboundMail::Processors::BounceProcessor.any_instance.stubs(soft_bounce?: true)
        assert @instance.send(:soft_bounce?)
      end

      it "incrementes failure count on all active targets" do
        EmailTarget.any_instance.expects(:message_failed).twice
        @instance.send(:update_email_targets_deliverability!, example_address)
      end

      it "increments 'bounce.targets' metric by the number of targets modified" do
        Zendesk::StatsD::Client.any_instance.expects(:count).with('bounce.targets', 2, tags: ["action:failure_count_incremented"]).once
        @instance.send(:update_email_targets_deliverability!, example_address)
      end
    end

    describe "having a hard bounce" do
      before do
        Zendesk::InboundMail::Processors::BounceProcessor.any_instance.stubs(soft_bounce?: false)
        refute @instance.send(:soft_bounce?)
      end

      it "deactivates all active targets" do
        EmailTarget.any_instance.expects(:deactivate).with(send_notification: true).twice
        @instance.send(:update_email_targets_deliverability!, example_address)
      end

      it "increments 'bounce.targets' metric by the number of targets modified" do
        TargetsMailer.expects(:deliver_target_disabled).twice
        Zendesk::StatsD::Client.any_instance.expects(:count).with('bounce.targets', 2, tags: ["action:deactivated"]).once
        @instance.send(:update_email_targets_deliverability!, example_address)
      end
    end
  end

  describe "#description" do
    let(:content) do
      "
      --94eb2c092b64d880c0054b312970
      Content-Type: text/plain; charset=UTF-8


      ** Address not found **

      Your message wasn't delivered to mariozaizar@gmail.com because the address couldn't be found. Check for typos or unnecessary spaces and try again.

      Learn more here: https://support.google.com/mail/?p=NoSuchUser

      The response from the remote server was:
      550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces. Learn more at https://support.google.com/mail/?p=NoSuchUser t30si2927556uat.28 - gsmtp

      --94eb2c092b64d880c0054b312970
      "
    end

    describe "when the email has delivery status attachments" do
      # Email has three attachments: 1) icon.png, 2) DSN file, 3) Original eml
      let(:mail) { FixtureHelper::Mail.read('dsm_bounce_email.json') }

      it "returns the full text of a delivery status attachment" do
        expected_description = "Reporting-MTA: dns; googlemail.com\nArrival-Date: Mon, 20 Mar 2017 15:36:50 -0700 (PDT)\nX-Original-Message-ID: <CACGNpndTPhN+yA7=H2QjN2EDWM1xvR3cq9BducOxY+1afje2nA@mail.gmail.com>\n\nFinal-Recipient: rfc822; hagfdsghkasfgdkasudfgahscvakshgdfasghkdfcvgcgscdgcvg@gmail.com\nAction: failed\nStatus: 5.1.1\nRemote-MTA: dns; gmail-smtp-in.l.google.com. (2607:f8b0:400c:c0c::1b, the\n server for the domain gmail.com.)\nDiagnostic-Code: smtp; 550-5.1.1 The email account that you tried to reach does not exist. Please try\n 550-5.1.1 double-checking the recipient's email address for typos or\n 550-5.1.1 unnecessary spaces. Learn more at\n 550 5.1.1  https://support.google.com/mail/?p=NoSuchUser t30si2927556uat.28 - gsmtp\nLast-Attempt-Date: Mon, 20 Mar 2017 15:36:51 -0700 (PDT)\n"

        assert_equal expected_description, instance.send(:description)
      end
    end

    describe "when the email does not have delivery status attachments" do
      let(:mail) { FixtureHelper::Mail.read('bounce_email.json') }

      before { @state.plain_content = content }

      it "returns the full text of a delivery status attachment" do
        expected_description = content

        assert_equal expected_description, instance.send(:description)
      end
    end
  end
end
