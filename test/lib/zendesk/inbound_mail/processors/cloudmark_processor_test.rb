require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Processors::CloudmarkProcessor do
  include ArturoTestHelper
  include MailTestHelper

  subject { Zendesk::InboundMail::Processors::CloudmarkProcessor }

  let(:state) { Zendesk::InboundMail::ProcessingState.new }
  let(:mail)  { Zendesk::Mail::InboundMessage.new }
  let(:account) { accounts('minimum') }
  let(:statsd_client) { Zendesk::InboundMail::StatsD.client }
  let(:logger) { mail.logger }
  let(:cloudmark_obvious_spam_threshold) { Zendesk::InboundMail::CloudmarkSpamScore.new(mail).threshold }
  let(:cloudmark_probable_spam_threshold) { Zendesk::InboundMail::CloudmarkSpamScore::PROBABLE_THRESHOLD }

  before do
    statsd_client.stubs(:increment)
    Zendesk::StatsD::Client.stubs(:new).with(namespace: 'mail_ticket_creator').returns(statsd_client)

    state.account = account

    state.stubs(whitelisted?: false)
    mail.account = account
  end

  def execute
    subject.execute(state, mail)
  end

  def does_not_get_suspended
    execute
    assert_nil state.suspension
  end

  def gets_suspended_by(suspension_type)
    execute
    assert_equal suspension_type, state.suspension
  end

  def gets_hard_rejected
    assert_raise(Zendesk::InboundMail::HardMailRejectException) { execute }
  end

  def records_cloudmark_metric(result:, enabled:, value: nil, threshold: nil)
    tags = [
      "result:#{result}",
      "enabled:#{enabled}"
    ]
    tags << "value:#{value}" if value
    tags << "threshold:#{threshold}" if threshold

    statsd_client.expects(:increment).with(:cloudmark, tags: tags).once
    execute
  end

  def records_cloudmark_replaced_by_rspamd
    logger.expects(:warn).with("Skipping Cloudmark because '#{account.subdomain}' has Rspamd enabled (via email_rspamd Arturo) -- warning") # .once
    statsd_client.expects(:increment).with('spam_processor.rspamd.enabled').once

    execute
  end

  def expects_cloudmark_skipped_by_multiplier_metric(multiplier)
    statsd_client.
      expects(:increment).
      with(:cloudmark, tags: ['result:skipped_by_multiplier', "multiplier:#{multiplier}"]).
      once

    execute
  end

  def records_processor_skipped_metric
    statsd_client.expects(:increment).with('cloudmark_processor.disabled').once

    execute
  end

  # TODO: Update this testing strategy after moving suspend/reject logic to SpamResultsProcessor
  describe_with_arturo_disabled :email_new_spam_processors do
    describe 'with a Cloudmark score considered obviously spammy' do
      before { mail.headers['X-CMAE-Score'] = cloudmark_obvious_spam_threshold }

      it { does_not_get_suspended }
    end
  end

  describe_with_arturo_enabled :email_new_spam_processors do
    describe_with_and_without_arturo_enabled :email_rspamd do
      describe_with_arturo_enabled :email_skip_spam_processor do
        describe 'with a Cloudmark score considered obviously spammy' do
          before { mail.headers['X-CMAE-Score'] = cloudmark_obvious_spam_threshold }

          it { does_not_get_suspended }

          it { records_processor_skipped_metric }
        end

        describe 'when the Cloudmark score is not spammy' do
          before { mail.headers['X-CMAE-Score'] = 0.0 }

          it { records_processor_skipped_metric }
        end
      end

      describe 'when the email is from Mail Fetcher (Gmail Connector)' do
        before do
          mail.headers[Zendesk::InboundMail::SpamHelper::SOURCE_HEADER] = Zendesk::InboundMail::SpamHelper::SOURCE_HEADER_VALUE_FOR_MAIL_FETCHER
        end

        it 'records a metric about skipping Mail Fetcher emails' do
          statsd_client.expects(:increment).with('cloudmark_processor.skip_mail_fetcher_email').once

          execute
        end
      end
    end

    describe 'when Rspamd is enabled for the account' do
      describe_with_arturo_enabled :email_rspamd do
        describe 'with a Cloudmark score considered obviously spammy' do
          before { mail.headers['X-CMAE-Score'] = cloudmark_obvious_spam_threshold }

          it { does_not_get_suspended }

          it { records_cloudmark_replaced_by_rspamd }

          it 'records the Cloudmark metric' do
            records_cloudmark_metric(
              result: 'obviously_spam',
              enabled: 'false',
              value: cloudmark_obvious_spam_threshold,
              threshold: cloudmark_obvious_spam_threshold
            )
          end

          describe 'when the multiplier is less than 1.0' do
            before { subject.any_instance.stubs(:multiplier).returns(0.5) }

            it { does_not_get_suspended }

            it 'records the Cloudmark metric' do
              records_cloudmark_metric(
                result: 'obviously_spam',
                enabled: 'false',
                value: cloudmark_obvious_spam_threshold,
                threshold: cloudmark_obvious_spam_threshold
              )
            end
          end

          describe 'when the multiplier is greater than 1.0' do
            before { subject.any_instance.stubs(:multiplier).returns(2.0) }

            it { does_not_get_suspended }

            it { expects_cloudmark_skipped_by_multiplier_metric(2.0) }
          end
        end

        describe 'with a Cloudmark score considered probably spammy' do
          before { mail.headers['X-CMAE-Score'] = cloudmark_probable_spam_threshold }

          it { does_not_get_suspended }

          it { records_cloudmark_replaced_by_rspamd }

          it 'records the Cloudmark metric' do
            records_cloudmark_metric(
              result: 'probably_spam',
              enabled: 'false',
              value: cloudmark_probable_spam_threshold
            )
          end
        end

        describe 'when the Cloudmark score is missing' do
          before { mail.headers['X-CMAE-Score'] = nil }

          it 'records the Cloudmark metric' do
            records_cloudmark_metric(
              result: 'missing_score',
              enabled: 'false'
            )
          end
        end
      end
    end

    describe 'when Rspamd is disabled for the account' do
      describe_with_arturo_disabled :email_rspamd do
        describe 'with a Cloudmark score considered obviously spammy' do
          before { mail.headers['X-CMAE-Score'] = cloudmark_obvious_spam_threshold }

          it { gets_suspended_by SuspensionType.SPAM }

          it 'records the Cloudmark metric' do
            records_cloudmark_metric(
              result: 'obviously_spam',
              enabled: 'true',
              value: cloudmark_obvious_spam_threshold,
              threshold: cloudmark_obvious_spam_threshold
            )
          end

          describe 'when the multiplier is less than 1.0' do
            before { subject.any_instance.stubs(:multiplier).returns(0.5) }

            it { gets_suspended_by SuspensionType.SPAM }

            it 'records the Cloudmark metric' do
              records_cloudmark_metric(
                result: 'obviously_spam',
                enabled: 'true',
                value: cloudmark_obvious_spam_threshold,
                threshold: cloudmark_obvious_spam_threshold
              )
            end
          end

          describe 'when the multiplier is greater than 1.0' do
            before { subject.any_instance.stubs(:multiplier).returns(2.0) }

            it { does_not_get_suspended }

            it { expects_cloudmark_skipped_by_multiplier_metric(2.0) }
          end

          describe 'when the state is whitelisted' do
            before { state.stubs(whitelisted?: true) }

            it { does_not_get_suspended }

            it { expects_cloudmark_skipped_by_multiplier_metric(2.0) }

            describe 'when the Cloudmark score would be high enough to suspend the email if the email were not whitelisted' do
              before { logger.stubs(:info) }

              ['account', 'organization', 'zopim'].each do |whitelist_type|
                describe "when the state is #{whitelist_type} whitelisted" do
                  before do
                    state.stubs("#{whitelist_type}_whitelisted".to_sym).returns(true)
                  end
                end
              end
            end
          end
        end

        describe 'with a Cloudmark score considered probably spammy' do
          before { mail.headers['X-CMAE-Score'] = cloudmark_probable_spam_threshold }

          it { does_not_get_suspended }

          it 'records the Cloudmark metric' do
            records_cloudmark_metric(
              result: 'probably_spam',
              enabled: 'true',
              value: cloudmark_probable_spam_threshold
            )
          end
        end

        describe 'when the Cloudmark score is missing' do
          before { mail.headers['X-CMAE-Score'] = nil }

          it 'records the Cloudmark metric' do
            records_cloudmark_metric(
              result: 'missing_score',
              enabled: 'true'
            )
          end
        end
      end

      describe 'when the Rspamd score is high enough to reject the email despite Rspamd not being enabled' do
        before do
          logger.stubs(:info)
          mail.headers['X-Spam-Zendesk-Score'] = subject::RSPAMD_REJECT_THRESHOLD + 1.0
        end

        def rspamd_reject_overrides_cloudmark_result(mail_already_suspended_by_cloudmark)
          statsd_client.expects(:increment).with(:cloudmark_overruled, tags: ["spam_suspension:#{mail_already_suspended_by_cloudmark}"]).once
          unless mail_already_suspended_by_cloudmark
            logger.expects(:info).with(regexp_matches(/^Cloudmark overruled due to Rspamd score.*/)).once
          end

          gets_suspended_by(SuspensionType.SPAM)
        end

        describe 'when the Cloudmark score is not spammy' do
          before { mail.headers['X-CMAE-Score'] = 0.0 }

          it { rspamd_reject_overrides_cloudmark_result(false) }
        end

        describe 'when the Cloudmark score is probably spammy' do
          before { mail.headers['X-CMAE-Score'] = cloudmark_probable_spam_threshold }

          it { rspamd_reject_overrides_cloudmark_result(false) }
        end

        describe 'when the Cloudmark score is obviously spammy' do
          before { mail.headers['X-CMAE-Score'] = cloudmark_obvious_spam_threshold }

          it { rspamd_reject_overrides_cloudmark_result(true) }
        end
      end

      describe 'when the Rspamd score would be high enough to reject the email despite Rspamd not being enabled if the email were not whitelisted' do
        before do
          logger.stubs(:info)
          mail.headers['X-Spam-Zendesk-Score'] = subject::RSPAMD_REJECT_THRESHOLD + 1.0
          mail.headers['X-CMAE-Score'] = 0.0
          state.stubs(:whitelisted?).returns(true)
        end

        ['account', 'organization', 'zopim'].each do |whitelist_type|
          describe "when the state is #{whitelist_type} whitelisted" do
            before do
              state.stubs("#{whitelist_type}_whitelisted".to_sym).returns(true)
            end
          end
        end
      end
    end
  end
end
