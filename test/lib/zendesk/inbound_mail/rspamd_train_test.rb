require_relative '../../../support/test_helper'

ENV['RSPAMD_API_ENDPOINT'] = 'http://127.0.0.1:9001'
ENV['RSPAMD_SPAM_RESOURCE'] = '/learnspam'
ENV['RSPAMD_HAM_RESOURCE'] = '/learnham'
ENV['RSPAMD_PASSWORD'] = 'GoodPassword'
SingleCov.covered!

describe Zendesk::InboundMail::RspamdTrain do
  let(:email) { Zendesk::Mailer::RawEmailAccess.new('test') }
  let(:recipient) { 'test@support.zendesk.com' }

  describe 'Test success cases for Zendesk::InboundMail::RspamdTrain' do
    before do
      Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:eml).returns('test')

      stub_request(:post, 'http://127.0.0.1:9001/learnham').to_return(status: 200)
      stub_request(:post, 'http://127.0.0.1:9001/learnspam').to_return(status: 200)
      stub_request(:post, 'http://127.0.0.1:9001/fuzzyadd').to_return(status: 200)
    end

    %w[spam ham].each do |classification|
      %w[common peruser].each do |classifier|
        %w[bayes fuzzy].each do |backend|
          describe "call #{classification}, train #{classification} email - success" do
            subject { Zendesk::InboundMail::RspamdTrain.new(raw_email: email, classification: classification, recipient: recipient, classifier: classifier, backend: backend) }

            it 'calls request' do
              subject.expects(:call).returns(OpenStruct.new(status: 200, body: ''))
              subject.call
            end

            it 'returns a response' do
              assert subject.call.is_a? Faraday::Response
            end

            it "returns learned #{classification} if method is #{classification}, #{classifier}, #{backend}" do
              assert subject.call.status == 200
              Zendesk::StatsD::Client.any_instance.expects(:increment).with("#{classification}.#{backend}.#{classifier}.success")
              subject.call
            end
          end
        end
      end
    end
  end

  describe 'Test Faraday::ClientError for Zendesk::InboundMail::RspamdTrain' do
    before do
      Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:eml).returns('test')

      stub_request(:post, 'http://127.0.0.1:9001/learnham').to_raise(Faraday::ClientError)
      stub_request(:post, 'http://127.0.0.1:9001/learnspam').to_raise(Faraday::ClientError)
      stub_request(:post, 'http://127.0.0.1:9001/fuzzyadd').to_raise(Faraday::ClientError)
    end

    %w[spam ham].each do |classification|
      %w[common peruser].each do |classifier|
        %w[bayes fuzzy].each do |backend|
          describe "call #{classification}, train #{classification} email - failed" do
            subject { Zendesk::InboundMail::RspamdTrain.new(raw_email: email, classification: classification, recipient: recipient, classifier: classifier, backend: backend) }

            it "increments error metric #{classification} if method is #{classification}, #{classifier}, #{backend}" do
              Zendesk::StatsD::Client.any_instance.expects(:increment).with("#{classification}.error")
              subject.call
            end
          end
        end
      end
    end
  end

  describe 'Test failed cases for Zendesk::InboundMail::RspamdTrain' do
    before do
      Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:eml).returns('test')

      stub_request(:post, 'http://127.0.0.1:9001/learnham').to_return(status: 404)
      stub_request(:post, 'http://127.0.0.1:9001/learnspam').to_return(status: 404)
      stub_request(:post, 'http://127.0.0.1:9001/fuzzyadd').to_return(status: 404)
    end

    %w[spam ham].each do |classification|
      %w[common peruser].each do |classifier|
        %w[bayes fuzzy].each do |backend|
          describe "call #{classification}, train #{classification} email - failed" do
            subject { Zendesk::InboundMail::RspamdTrain.new(raw_email: email, classification: classification, recipient: recipient, classifier: classifier, backend: backend) }

            it 'calls request' do
              subject.expects(:call).returns(OpenStruct.new(status: 404, body: ''))
              subject.call
            end

            it 'returns a response' do
              assert subject.call.is_a? Faraday::Response
            end

            it "returns failed to learn #{classification} if method is #{classification}, #{classifier}, #{backend}" do
              assert subject.call.status == 404
              Zendesk::StatsD::Client.any_instance.expects(:increment).with("#{classification}.#{backend}.#{classifier}.failed")
              subject.call
            end
          end
        end
      end
    end
  end

  describe 'Test error cases for Zendesk::InboundMail::RspamdTrain - 204' do
    before do
      Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:eml).returns('test')

      stub_request(:post, 'http://127.0.0.1:9001/learnham').to_return(status: 204)
      stub_request(:post, 'http://127.0.0.1:9001/learnspam').to_return(status: 204)
    end

    %w[spam ham].each do |classification|
      %w[common peruser].each do |classifier|
        %w[bayes].each do |backend|
          describe "call #{classification}, train #{classification} email - not enough tokens" do
            subject { Zendesk::InboundMail::RspamdTrain.new(raw_email: email, classification: classification, recipient: recipient, classifier: classifier, backend: backend) }

            it 'calls request' do
              subject.expects(:call).returns(OpenStruct.new(status: 204, body: ''))
              subject.call
            end

            it 'returns a response' do
              assert subject.call.is_a? Faraday::Response
            end

            it "returns failed to learn #{classification} if method is #{classification}, #{classifier}, #{backend}" do
              assert subject.call.status == 204
              Zendesk::StatsD::Client.any_instance.expects(:increment).with("#{classification}.#{backend}.#{classifier}.not_enough_tokens")
              subject.call
            end
          end
        end
      end
    end
  end

  describe 'Test error cases for Zendesk::InboundMail::RspamdTrain - 208' do
    before do
      Zendesk::Mailer::RawEmailAccess.any_instance.stubs(:eml).returns('test')

      stub_request(:post, 'http://127.0.0.1:9001/learnham').to_return(status: 208)
      stub_request(:post, 'http://127.0.0.1:9001/learnspam').to_return(status: 208)
    end

    %w[spam ham].each do |classification|
      %w[common peruser].each do |classifier|
        %w[bayes].each do |backend|
          describe "call #{classification}, train #{classification} email - sample has bee already learned" do
            subject { Zendesk::InboundMail::RspamdTrain.new(raw_email: email, classification: classification, recipient: recipient, classifier: classifier, backend: backend) }

            it 'calls request' do
              subject.expects(:call).returns(OpenStruct.new(status: 208, body: ''))
              subject.call
            end

            it 'returns a response' do
              assert subject.call.is_a? Faraday::Response
            end

            it "returns failed to learn #{classification} if method is #{classification}, #{classifier}, #{backend}" do
              assert subject.call.status == 208
              Zendesk::StatsD::Client.any_instance.expects(:increment).with("#{classification}.#{backend}.#{classifier}.already_trained")
              subject.call
            end
          end
        end
      end
    end
  end
end
