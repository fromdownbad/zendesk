require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 46

describe Zendesk::MtcMpqMigration::ZendeskMail::ProcessorChainSupport do
  include MailTestHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:mail) do
    Zendesk::Mail::InboundMessage.new.tap do |message|
      message.account = account
    end
  end

  describe "#from_external_machine?" do
    describe "with an intercom email" do
      let(:header_value) { "notification" }
      let(:long_id) { "588a64eeefdf81492b977c1f_ff78279749ffb3c3b48f201ca95a4b520c061110" }

      before { mail.headers["X-Intercom-Bin"] = header_value }

      describe "where the header value is 'notification'" do
        [:user, :admin].each do |intercom_user_type|
          let(:plus_id) { "+#{intercom_user_type}_#{long_id}" }

          before do
            mail.reply_to = address("notifications#{plus_id}@intercom-mail.com")
          end

          it { assert mail.from_external_machine? }
        end
      end

      describe "where the header value is 'proven_users'" do
        let(:header_value) { "proven_users" }
        let(:reply_to) { "n+u_#{long_id}@example.intercom-mail.com" }

        before do
          mail.reply_to = address(reply_to)
        end

        describe "when reply-to starts with 'n+u_' (known format as of 20190801)" do
          describe "and contains a subdomain" do
            it { assert mail.from_external_machine? }
          end

          describe "and does not contain a subdomain" do
            let(:reply_to) { "n+u_#{long_id}@intercom-mail.com" }

            it { assert mail.from_external_machine? }
          end
        end

        describe "when reply-to starts with 'notifications+user_' (known legacy format)" do
          let(:reply_to) { "notifications+user_#{long_id}@example.intercom-mail.com" }

          describe "and contains a subdomain" do
            it { assert mail.from_external_machine? }
          end

          describe "and does not contain a subdomain" do
            let(:reply_to) { "notifications+user_#{long_id}@intercom-mail.com" }

            it { assert mail.from_external_machine? }
          end
        end
      end
    end

    describe "with regular email" do
      before { mail.reply_to = address("john@example.com") }

      it { refute mail.from_external_machine? }
    end
  end
end
