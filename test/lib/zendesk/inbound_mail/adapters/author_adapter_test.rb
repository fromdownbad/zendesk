require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::Adapters::AuthorAdapter do
  let(:subject) do
    Zendesk::InboundMail::Adapters::AuthorAdapter.new(processing_state: state,
                                                      account: account,
                                                      mail_logger: logger,
                                                      statsd_client: statsd_client,
                                                      save_attrs: save_attrs)
  end

  let(:state) do
    Zendesk::InboundMail::ProcessingState.new.tap do |processing_state|
      processing_state.author = author
    end
  end
  let(:account)       { accounts(:minimum) }
  let(:logger)        { stub('logger', log: true) }
  let(:statsd_client) { stub_for_statsd }
  let(:email)         { 'email@example.com' }
  let(:save_attrs) { [] }

  describe '#save_with_intrumentation!' do
    describe 'the author is present' do
      describe 'the author is a new record' do
        let(:author) do
          User.new.tap do |author|
            author.account = account
            author.email   = email
            author.name    = 'bob'
          end
        end

        describe 'a user email identity already exists for that email address' do
          before do
            UserEmailIdentity.create!(account: account, user: account.users.first, value: email)
          end

          it { assert_raises(ActiveRecord::RecordInvalid) { subject.save_with_instrumentation! } }
        end

        describe 'a user email identity does not exist for that email address' do
          it 'saves the author' do
            subject.save_with_instrumentation!
            refute author.reload.new_record?
          end
        end
      end

      describe 'a whitelisted update attribute is specified' do
        let(:save_attrs) { [:locale_id] }

        describe 'the attribute specified for update has been changed' do
          let(:author) do
            user = users(:minimum_end_user)
            user.stubs(new_record?: false, locale_id_changed?: true)
            user
          end

          it 'saves the author' do
            author.expects(:save!)
            subject.save_with_instrumentation!
          end
        end

        describe 'an attribute not specified for update has been changed' do
          let(:author) do
            user = users(:minimum_end_user)
            user.stubs(new_record?: false, name_changed?: true)
            user
          end

          it 'does not save the author' do
            author.expects(:save!).never
            subject.save_with_instrumentation!
          end
        end
      end

      describe 'a non-whitelisted update attribute is specified' do
        let(:save_attrs) { [:name] }

        describe 'the attribute specified for update has been changed' do
          let(:author) do
            user = users(:minimum_end_user)
            user.stubs(new_record?: false, name_changed?: true)
            user
          end

          it 'does not save the author' do
            author.expects(:save!).never
            subject.save_with_instrumentation!
          end
        end

        describe 'an attribute not specified for update has been changed' do
          let(:author) do
            user = users(:minimum_end_user)
            user.stubs(new_record?: false, locale_id_changed?: true)
            user
          end

          it 'does not save the author' do
            author.expects(:save!).never
            subject.save_with_instrumentation!
          end
        end
      end

      describe 'the author has changed' do
        let(:author) do
          user = users(:minimum_end_user)
          user.stubs(new_record?: false, locale_id_changed?: false, changed?: true)
          user
        end

        it 'does not save the author' do
          author.expects(:save!).never
          subject.save_with_instrumentation!
        end
      end

      describe 'the author has not changed' do
        let(:author) do
          user = users(:minimum_end_user)
          user.stubs(new_record?: false, locale_id_changed?: false, changed?: false)
          user
        end

        it 'does not save the author' do
          author.expects(:save!).never
          subject.save_with_instrumentation!
        end
      end
    end
  end
end
