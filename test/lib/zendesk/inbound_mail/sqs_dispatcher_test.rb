require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1 # for the sleep call when it's polling in a loop

describe Zendesk::InboundMail::SQSDispatcher do
  class TestWorker
    COMPLETED_WORK = [] # rubocop:disable Style/MutableConstant

    attr_reader :item

    def self.clear!
      @error = nil
      COMPLETED_WORK.clear
    end

    class << self
      attr_writer :error
    end

    class << self
      attr_reader :error
    end

    def initialize(item)
      @item = item
    end

    def report_to(boss)
      @boss = boss
    end

    def perform
      exception = nil

      begin
        work
      rescue Exception => e # rubocop:disable Lint/RescueException
        exception = e
      end

      @boss.work_done(item, self, exception) if @boss
    end

    def json_to_parse
      item.value
    end

    def account
      Account.new
    end

    def message
      @message ||= Zendesk::Mail::InboundMessage.new
    end

    def work
      raise self.class.error if self.class.error
      COMPLETED_WORK << item.path
    end
  end

  let(:sqs_message_path) { "1/44f3a88f68f38a522ccf29a5533d2f6ce6ee3c41-529.json".freeze }
  let(:sqs_message_event_time) { "2019-09-23T18:27:38.756Z".freeze }
  let(:sqs_message_body_hash) do
    {
      "Records" => [
        {
          "awsRegion" => "us-west-2".freeze,
          "eventTime" => sqs_message_event_time,
          "s3" => {
            "object" => {
              "key" => sqs_message_path
            },
          }
        }
      ]
    }
  end

  let(:sqs_message_body) { sqs_message_body_hash.to_json }
  let(:sqs_message_receipt_handle) { "1234".freeze }
  let(:sqs_message_receive_count) { 1 }

  class TestSQSPayload
    attr_accessor :data
  end

  let(:sqs_payload_data) do
    {
      body:
        {
          "Message" => [
            {
              "Body" => sqs_message_body,
              "ReceiptHandle" => sqs_message_receipt_handle,
              "Attributes" =>
                {
                  "ApproximateReceiveCount" => sqs_message_receive_count
                }
            }
          ]
        }
    }
  end

  let(:sqs_payload) do
    TestSQSPayload.new.tap do |sqs_payload_instance|
      sqs_payload_instance.data = sqs_payload_data
    end
  end

  # When there are no email JSONs in the SQS queue, the "Message" key in the
  # response from SQS is an empty array.
  let(:sqs_payload_for_empty_queue_data) { { body: { "Message" => [] } } }

  let(:sqs_payload_for_empty_queue) do
    TestSQSPayload.new.tap do |sqs_payload_instance|
      sqs_payload_instance.data = sqs_payload_for_empty_queue_data
    end
  end

  let(:sqs) do
    Fog::AWS::SQS.new(
      aws_access_key_id: "accesskey",
      aws_secret_access_key: "secretkey"
    )
  end

  let(:remote_file_content) { "email content" }
  let(:remote_file) do
    mock = Minitest::Mock.new
    mock.expect :content, remote_file_content
  end

  let(:use_test_sqs) { true }
  let(:sqs_queue_url) { "queue_url" }
  let(:configure_sqs_queue_url) { true }
  let(:create_unprocessable_fails) { false }

  let(:dispatcher) do
    sqs_dispatcher = Zendesk::InboundMail::SQSDispatcher.new(stop_when_done: true)
    sqs_dispatcher.instance_variable_set(:@sqs, sqs) if use_test_sqs
    sqs_dispatcher
  end

  let(:support_mail_processing_statsd_client) { dispatcher.send(:support_mail_processing_statsd_client) }

  def assert_no_work_done
    subject
    assert_equal [], TestWorker::COMPLETED_WORK
  end

  before do
    Fog.mock!

    ENV["MPQ_MTC_SQS_QUEUE_URL"] = sqs_queue_url if configure_sqs_queue_url

    TestWorker.clear!

    Zendesk::InboundMail::SQSDispatcher.any_instance.stubs(create_unprocessable: true) unless create_unprocessable_fails
  end

  describe "Without SQS access and secret access keys configured" do
    it "logs whether env vars are configured for AWS SQS access" do
      Zendesk::Mail.logger.expects(:info).with(regexp_matches(/Using SQS Dispatcher/))
      Zendesk::Mail.logger.expects(:info).with(regexp_matches(/Env vars configured for MTC SQS access/))
      subject
    end
  end

  after do
    ENV.delete("MPQ_MTC_SQS_QUEUE_URL") if configure_sqs_queue_url
  end

  describe "#start" do
    let(:receive_message_successful) { true }
    let(:sqs_queue_contains_queued_mail) { true }
    let(:failure_types) { [] }

    before do
      dispatcher.instance_variable_set(:@worker_class, TestWorker)

      if failure_types.include?("permanent")
        dispatcher.expects(:handle_permanent_failure)
      elsif failure_types.include?("unexpected")
        dispatcher.expects(:handle_unexpected_failure)
      else
        dispatcher.expects(:handle_permanent_failure).never
        dispatcher.expects(:handle_unexpected_failure).never
      end
    end

    subject do
      if receive_message_successful
        if sqs_queue_contains_queued_mail
          sqs.stubs(:receive_message).with(sqs_queue_url).returns(sqs_payload)
        else
          sqs.stubs(:receive_message).with(sqs_queue_url).returns(sqs_payload_for_empty_queue)
        end
      else
        sqs.stubs(:receive_message).raises(StandardError, "oh no!")
      end
      dispatcher.start
    end

    it "attempts to retrieve the specified file from S3" do
      RemoteFiles::File.expects(:new)
      subject
    end

    describe "when the SQS queue contains queued mail" do
      describe "when S3 remote_file retrieval is successful" do
        before do
          dispatcher.stubs(remote_file_retrieve: remote_file)
        end

        it "creates an SQSItem with the necessary data" do
          Zendesk::InboundMail::SQSItem.expects(:new).with(
            remote_file_content,
            sqs_message_event_time,
            sqs_message_path,
            sqs_message_receipt_handle,
            sqs_message_receive_count
          )
          dispatcher.stubs(:delegate_work)
          subject
        end

        it "processes messages received by the SQS client" do
          subject

          assert_equal 1, TestWorker::COMPLETED_WORK.size
          assert_equal sqs_message_path, TestWorker::COMPLETED_WORK.first
        end

        it "increments :processed result:success metric and not the :sqs_no_receive_count metric" do
          Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:success']).once
          Zendesk::InboundMail::StatsD.client.expects(:increment).with(:sqs_no_receive_count).never
          subject
        end

        it "records a histogram metric noting the time needed to retrieve the mail JSON from AWS S3" do
          Zendesk::InboundMail::StatsD.client.expects(:histogram).with(:s3_json_retrieval_time, kind_of(Float)).once
          subject
        end

        it "decrements the metric tracking the MTC queue size in SQS" do
          support_mail_processing_statsd_client.expects(:decrement).with(:mpq_mtc_sqs_queue_size)
          subject
        end

        describe "when the ApproximateReceiveCount is not part of the SQS message received" do
          let(:sqs_payload_data) do
            {
              body:
                {
                  "Message" => [
                    {
                      "Body" => sqs_message_body,
                      "ReceiptHandle" => sqs_message_receipt_handle
                    }
                  ]
                }
            }
          end

          it "creates an SQSItem with the necessary data and uses a default receive_count of 1" do
            Zendesk::InboundMail::SQSItem.expects(:new).with(
              remote_file_content,
              sqs_message_event_time,
              sqs_message_path,
              sqs_message_receipt_handle,
              sqs_message_receive_count
            )
            dispatcher.stubs(:delegate_work)
            subject
          end

          it "increments both the :processed result:success and :sqs_no_receive_count metrics" do
            Zendesk::InboundMail::StatsD.client.expects(:increment).with(:sqs_no_receive_count).once
            Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:success']).once
            subject
          end
        end
      end

      describe "when the SQS queue does not contain queued mail" do
        let(:sqs_queue_contains_queued_mail) { false }

        it "does not create an SQS item to pass to the worker" do
          Zendesk::InboundMail::SQSItem.expects(:new).never
          dispatcher.expects(:delegate_work).never
          subject
        end

        it "does not process any messages" do
          subject

          assert_empty TestWorker::COMPLETED_WORK
        end

        it "does not increment the :processed result:success metric" do
          Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:success']).never
          subject
        end

        it "does not attempt to retrieve any remote files" do
          dispatcher.expects(:remote_file_retrieve).never
          subject
        end

        it "does not decrement the metric tracking the MTC queue size in SQS" do
          support_mail_processing_statsd_client.expects(:decrement).with(:mpq_mtc_sqs_queue_size).never
          subject
        end
      end
    end

    describe "when a StandardError occurs in the worker" do
      let(:worker_error) { StandardError.new }
      let(:failure_types) { ["unexpected"] }

      before do
        dispatcher.stubs(remote_file_retrieve: remote_file)
        TestWorker.error = worker_error
      end

      it { assert_no_work_done }

      after { TestWorker.error = nil }
    end

    describe "when a SyntaxError occurs in the worker" do
      let(:worker_error) { Zendesk::InboundMail::SyntaxError.new }
      let(:failure_types) { ["permanent"] }

      before do
        dispatcher.stubs(remote_file_retrieve: remote_file)
        TestWorker.error = worker_error
      end

      it { assert_no_work_done }

      after { TestWorker.error = nil }
    end

    describe "when a StandardError occurs in SQS" do
      let(:receive_message_successful) { false }

      it 'logs the error' do
        ZendeskExceptions::Logger.expects(:record).with(instance_of(StandardError), message: 'Error receiving SQS message')
        subject
      end

      it { assert_no_work_done }
    end

    describe "when a StandardError occurs in S3" do
      before do
        dispatcher.stubs(:remote_file_retrieve).raises(StandardError, "oh no!")
      end

      it 'logs the error' do
        ZendeskExceptions::Logger.expects(:record).with(instance_of(StandardError), message: 'Error receiving SQS message')
        subject
      end

      it "increments the metric tracking the MTC queue size in SQS" do
        support_mail_processing_statsd_client.expects(:increment).with(:mpq_mtc_sqs_queue_size).once
        subject
      end

      it { assert_no_work_done }
    end

    describe "when a processing timeout error occurs in the worker" do
      let(:worker_error) { Zendesk::InboundMail::ProcessingTimeout.new }
      let(:failure_types) { ["unexpected"] }

      before do
        dispatcher.stubs(remote_file_retrieve: remote_file)
        TestWorker.error = worker_error
      end

      it 'does not delete the message from SQS so that it will be retried' do
        sqs.expects(:delete_message).never
        subject
      end

      it { assert_no_work_done }

      after { TestWorker.error = nil }
    end
  end

  describe "#handle_unexpected_failure" do
    let(:sqs_item) do
      Zendesk::InboundMail::SQSItem.new(
        remote_file_content,
        sqs_message_event_time,
        sqs_message_path,
        sqs_message_receipt_handle,
        sqs_message_receive_count
      )
    end

    let(:worker) { TestWorker.new(sqs_item) }
    let(:worker_error) { StandardError.new }

    subject { dispatcher.handle_unexpected_failure(sqs_item, worker, worker_error) }

    describe "when handling a StandardError" do
      it 'does not delete the message from SQS so that it will be retried' do
        sqs.expects(:delete_message).never
        subject
      end

      it 'logs the exception' do
        Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(sqs_message_path, worker_error)
        subject
      end

      it 'increments the retries, error, and processing_error.error metrics' do
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:retries)
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:error)
        Zendesk::InboundMail::StatsD.client.expects(:increment).with("processing_error.error", tags: ["detail:StandardError"])
        subject
      end

      it "increments the metric tracking the MTC queue size in SQS" do
        support_mail_processing_statsd_client.expects(:increment).with(:mpq_mtc_sqs_queue_size).once
        subject
      end
    end

    describe "when a processing timeout error occurs in the worker" do
      let(:worker_error) { Zendesk::InboundMail::ProcessingTimeout.new }

      def subject_with_exit
        assert_raises SystemExit do
          subject
        end
      end

      it 'does not delete the message from SQS so that it will be retried' do
        sqs.expects(:delete_message).never
        subject_with_exit
      end

      it 'logs the exception' do
        Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(sqs_message_path, worker_error)
        subject_with_exit
      end

      it 'increments the retries, error, and processing_error.error, and processing_timeout metrics' do
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:retries)
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:error)
        Zendesk::InboundMail::StatsD.client.expects(:increment).with("processing_error.error", tags: ['detail:Zendesk::InboundMail::ProcessingTimeout'])
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processing_timeout, tags: ['type:generic'])
        subject_with_exit
      end

      it "increments the metric tracking the MTC queue size in SQS" do
        support_mail_processing_statsd_client.expects(:increment).with(:mpq_mtc_sqs_queue_size).once
        subject_with_exit
      end
    end
  end

  describe "#handle_permanent_failure" do
    let(:sqs_item) do
      Zendesk::InboundMail::SQSItem.new(
        remote_file_content,
        sqs_message_event_time,
        sqs_message_path,
        sqs_message_receipt_handle,
        sqs_message_receive_count
      )
    end

    let(:worker) { TestWorker.new(sqs_item) }
    let(:worker_error) { Zendesk::InboundMail::SQSMaxReceivesError.new }

    subject { dispatcher.handle_permanent_failure(sqs_item, worker, worker_error) }

    describe "when the email has been retried the max number of times" do
      it 'does not delete the message from SQS so that it will go into the DLQ' do
        sqs.expects(:delete_message).never
        subject
      end

      it 'logs the exception' do
        Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(sqs_message_path, worker_error)
        subject
      end

      it 'increments the processed metric but not the processing_error.error metric' do
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:failed']).once
        Zendesk::InboundMail::StatsD.client.expects(:increment).with("processing_error.error", tags: ['detail:Zendesk::InboundMail::SQSMaxReceivesError']).never
        subject
      end

      it "tries to create an unprocessable suspended ticket" do
        dispatcher.unstub(:create_unprocessable)
        dispatcher.expects(:create_unprocessable)
        subject
      end

      describe "when unprocessable suspended ticket is created" do
        it 'logs that the suspended ticket was created' do
          dispatcher.send(:logger).expects(:info).with(regexp_matches(/Unhandled exception while Processing mail/))
          dispatcher.send(:logger).expects(:info).with("Created unprocessable suspended ticket")
          subject
        end
      end

      describe "when unprocessable suspended ticket is not created" do
        let(:create_unprocessable_fails) { true }

        it 'increments the :processed result:failed and :unprocessable_not_created metrics' do
          Zendesk::InboundMail::StatsD.client.expects(:increment).with(:unprocessable_not_created, tags: ["source:sqs_dispatcher"]).once
          Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:failed']).once
          subject
        end

        it 'logs that the suspended ticket was not created' do
          dispatcher.send(:logger).expects(:info).with(regexp_matches(/Unhandled exception while Processing mail/))
          Zendesk::Mail.logger.expects(:info).with("Could not create unprocessable suspended ticket")
          subject
        end
      end
    end

    describe "when a Zendesk::InboundMail::SyntaxError happens" do
      let(:worker_error) { Zendesk::InboundMail::SyntaxError.new }

      before { sqs.stubs(:delete_message) }

      it 'deletes the message from SQS so that it will not be retried' do
        sqs.unstub(:delete_message)
        sqs.expects(:delete_message).once
        subject
      end

      it 'logs the exception' do
        Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(sqs_message_path, worker_error)
        subject
      end

      it 'increments the processed and processing_error.error metric' do
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:failed']).once
        Zendesk::InboundMail::StatsD.client.expects(:increment).with("processing_error.error", tags: ['detail:Zendesk::InboundMail::SyntaxError']).once
        subject
      end

      it "tries to create an unprocessable suspended ticket" do
        dispatcher.unstub(:create_unprocessable)
        dispatcher.expects(:create_unprocessable)
        subject
      end

      describe "when unprocessable suspended ticket is created" do
        it 'logs that the suspended ticket was created' do
          dispatcher.send(:logger).expects(:info).with(regexp_matches(/Unhandled exception while Processing mail/))
          dispatcher.send(:logger).expects(:info).with("Created unprocessable suspended ticket")
          subject
        end
      end

      describe "when unprocessable suspended ticket is not created" do
        let(:create_unprocessable_fails) { true }

        it 'increments the :processed result:failed and :unprocessable_not_created metrics' do
          Zendesk::InboundMail::StatsD.client.expects(:increment).with(:unprocessable_not_created, tags: ["source:sqs_dispatcher"]).once
          Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:failed']).once
          Zendesk::InboundMail::StatsD.client.expects(:increment).with('processing_error.error', tags: ['detail:Zendesk::InboundMail::SyntaxError']).once
          subject
        end

        it 'logs that the suspended ticket was not created' do
          dispatcher.send(:logger).expects(:info).with(regexp_matches(/Unhandled exception while Processing mail/))
          Zendesk::Mail.logger.expects(:info).with("Could not create unprocessable suspended ticket")
          subject
        end
      end
    end

    describe "when a Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout happens" do
      let(:worker_error) { Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout.new }

      before { sqs.stubs(:delete_message) }

      it 'deletes the message from SQS so that it will not be retried' do
        sqs.unstub(:delete_message)
        sqs.expects(:delete_message).once
        subject
      end

      it 'logs the exception' do
        Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(sqs_message_path, worker_error)
        subject
      end

      it 'increments the processed, processing_error.error, and processing_timeout metrics' do
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:failed']).once
        Zendesk::InboundMail::StatsD.client.expects(:increment).with("processing_error.error", tags: ['detail:Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout']).once
        Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processing_timeout, tags: ['type:content_processor']).once
        subject
      end

      it "tries to create an unprocessable suspended ticket" do
        dispatcher.unstub(:create_processable)
        dispatcher.expects(:create_unprocessable)
        subject
      end

      describe "when unprocessable suspended ticket is created" do
        it 'logs that the suspended ticket was created' do
          dispatcher.send(:logger).expects(:info).with(regexp_matches(/Unhandled exception while Processing mail/))
          dispatcher.send(:logger).expects(:info).with("Created unprocessable suspended ticket")
          subject
        end
      end

      describe "when unprocessable suspended ticket is not created" do
        let(:create_unprocessable_fails) { true }

        it 'increments the :processed result:failed, processing_timeout, processing_error.error, and :unprocessable_not_created metrics' do
          Zendesk::InboundMail::StatsD.client.expects(:increment).with(:unprocessable_not_created, tags: ["source:sqs_dispatcher"]).once
          Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processed, tags: ['result:failed']).once
          Zendesk::InboundMail::StatsD.client.expects(:increment).with(:processing_timeout, tags: ['type:content_processor']).once
          Zendesk::InboundMail::StatsD.client.expects(:increment).with('processing_error.error', tags: ['detail:Zendesk::MtcMpqMigration::Errors::ContentProcessorTimeout']).once
          subject
        end

        it 'logs that the suspended ticket was not created' do
          dispatcher.send(:logger).expects(:info).with(regexp_matches(/Unhandled exception while Processing mail/))
          Zendesk::Mail.logger.expects(:info).with("Could not create unprocessable suspended ticket")
          subject
        end
      end
    end
  end

  describe "#work_done" do
    let(:failure_handler) { Zendesk::InboundMail::QueueFailureHandler.new }
    let(:sqs_item) { {} }
    let(:worker) { TestWorker.new(sqs_item) }
    let(:worker_error_class) { StandardError }
    let(:worker_error) { worker_error_class.new }

    subject { dispatcher.work_done(sqs_item, worker, worker_error) }

    before { dispatcher.instance_variable_set(:@failure_handler, failure_handler) }

    def on_failure(*error_classes)
      failure_handler.add_rule(*error_classes, &Proc.new)
    end

    describe "when the failure handler handles the exception class" do
      it "uses the failure handler to handle the exception" do
        handled = false
        on_failure(worker_error_class) do |handler_item, handler_worker, handler_exception|
          assert_equal sqs_item, handler_item
          assert_equal worker, handler_worker
          assert_equal worker_error, handler_exception
          handled = true
        end
        subject
        assert handled
      end
    end

    describe "when the failure handler does not handle the exception class" do
      it "raises" do
        assert_raises do
          subject
        end
      end
    end
  end

  it "uses the Zendesk::InboundMail::SQSTicketProcessingWorker" do
    assert_equal Zendesk::InboundMail::SQSTicketProcessingWorker, dispatcher.worker_class
  end

  describe "With SQS access and secret access key configured" do
    let(:use_test_sqs) { false }

    before do
      ENV['EMAIL_AWS_SQS_ACCESS_KEY'] = "1234abcd"
      ENV['EMAIL_AWS_SQS_SECRET_KEY'] = "1234abcd"
    end

    after do
      ENV.delete('EMAIL_AWS_SQS_ACCESS_KEY')
      ENV.delete('EMAIL_AWS_SQS_SECRET_KEY')
    end

    it "creates the SQS client" do
      assert dispatcher.instance_variable_get(:@sqs).present?
    end

    it "does not include the session token in the options" do
      Fog::AWS::SQS.expects(:new).with({aws_access_key_id: "1234abcd", aws_secret_access_key: "1234abcd", region: 'us-west-2'})
      dispatcher
    end

    it "does not log whether env vars are configured for AWS SQS access" do
      Zendesk::Mail.logger.expects(:info).with(regexp_matches(/Using SQS Dispatcher/))
      Zendesk::Mail.logger.expects(:info).with(regexp_matches(/Env vars configured for MTC SQS access/)).never
      subject
    end

    describe "and AWS session token configured" do
      before { ENV['EMAIL_AWS_SESSION_TOKEN'] = "1234abcd" }

      after { ENV.delete('EMAIL_AWS_SESSION_TOKEN') }

      it "creates the SQS client" do
        assert dispatcher.instance_variable_get(:@sqs).present?
      end

      it "includes the session token in the options" do
        Fog::AWS::SQS.expects(:new).with(
          {
            aws_access_key_id: "1234abcd",
            aws_secret_access_key: "1234abcd",
            aws_session_token: "1234abcd",
            region: 'us-west-2'
          }
        )

        dispatcher
      end
    end
  end

  describe "Without SQS queue URL configured" do
    let(:use_test_sqs) { false }
    let(:configure_sqs_queue_url) { false }

    it "does not attempt to process from SQS" do
      sqs.expects(:receive_message).never
      dispatcher.start
    end
  end

  # TODO: Leaving the rest of the tests commented out below so that we can be sure to give coverage to similar
  # cases once we have more of the failure handling implemented in SQSDispatcher.
  #
  # describe "when an exception happens" do
  #  before do
  #    Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(instance_of(String), instance_of(StandardError))
  #    TestWorker.error = StandardError
  #    @dispatcher.instance_variable_set(:@worker_class, TestWorker)
  #    @file = Tempfile.new(['email_file_test', '.json'], @new_dir)
  #    @dispatcher.start
  #  end
  #
  #  it "moves the file to retry" do
  #    assert (@retry_dir + File.basename(@file.path)).file?
  #  end
  # end
  #
  # describe "when a SignalException happens" do
  #  subject { @dispatcher.start }
  #
  #  before do
  #    TestWorker.error = Interrupt
  #    @dispatcher.instance_variable_set(:@worker_class, TestWorker)
  #    @file = Tempfile.new(['email_file_test', '.json'], @new_dir)
  #  end
  #
  #  it 'logs a waring using DispatchFailureHandler' do
  #    Zendesk::InboundMail::DispatchFailureHandler.expects(:warn).once
  #    assert_raises(SystemExit) { subject }
  #  end
  #
  #  it "increments :sigterm, :retries and :error statsd metrics then exits" do
  #    Zendesk::InboundMail::StatsD.client.expects(:increment).with(:sigterm).once
  #    Zendesk::InboundMail::StatsD.client.expects(:increment).with(:retries).once
  #    Zendesk::InboundMail::StatsD.client.expects(:increment).with(:error).once
  #
  #    assert_raises(SystemExit) { subject }
  #  end
  #
  #  it "moves the file to retry" do
  #    assert_raises(SystemExit) { subject }
  #    assert (@retry_dir + File.basename(@file.path)).file?
  #  end
  # end
  #
  # describe "when an exception happens on retry" do
  #  before do
  #    Zendesk::InboundMail::DispatchFailureHandler.expects(:log).with(instance_of(String), instance_of(StandardError))
  #    TestWorker.error = StandardError
  #    @dispatcher.instance_variable_set(:@worker_class, TestWorker)
  #    @file = Tempfile.new(['email_file_test', '.json'], @retry_dir)
  #  end
  #
  #  before do
  #    Zendesk::InboundMail::DispatchFailureHandler.expects(:notify).with(instance_of(String), instance_of(StandardError), "failed and will retry", instance_of(Hash))
  #    Timecop.travel(30.minutes.from_now + 1) { @dispatcher.start }
  #  end
  #
  #  it "moves the file to retry" do
  #    assert (@retry_dir + File.basename(@file.path)).file?
  #  end
  # end
end
