require_relative "../../../../support/test_helper"

SingleCov.covered! file: 'lib/zendesk/inbound_mail/rules/base.rb'

describe Zendesk::InboundMail::Rules::Base do
  let(:logger) { stub('logger', :log) }
  let(:base) { Zendesk::InboundMail::Rules::Base.new(logger) }

  describe '#execute' do
    subject { base.execute }

    it 'raises a NotImplementedError' do
      assert_raises(NotImplementedError) { subject }
    end
  end
end
