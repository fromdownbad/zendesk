require_relative "../../../../support/test_helper"

SingleCov.covered! file: 'lib/zendesk/inbound_mail/rules/chain.rb'

describe Zendesk::InboundMail::Rules::Chain do
  class ZerothRule
    def execute(state, *args)
      # does something
    end
  end

  class FirstRule < ZerothRule; end

  class SecondRule < ZerothRule; end

  let(:state) { stub('State') }
  let(:first_rule) { FirstRule.new }
  let(:second_rule) { SecondRule.new }
  let(:rules) { [first_rule, second_rule] }
  let(:chain) { Zendesk::InboundMail::Rules::Chain.new(*rules) }

  describe '#execute' do
    subject { chain.execute(state, arguments) }

    describe 'argument passing' do
      let(:rules) { [first_rule] }
      let(:arguments) { { key: :value } }

      it 'passes the arguments to the rules' do
        first_rule.expects(:execute).with(state, arguments)
        subject
      end
    end

    describe 'all rules get executed' do
      let(:rules) { [first_rule, second_rule] }
      let(:arguments) { { key: :value } }

      it 'executes all rules' do
        first_rule.expects(:execute)
        second_rule.expects(:execute)
        subject
      end
    end

    describe 'returns' do
      let(:arguments) { { key: :value } }

      describe 'true if any rule returns true' do
        describe 'first rule only' do
          before do
            first_rule.stubs(:execute).returns(true)
            second_rule.stubs(:execute).returns(false)
          end

          it { assert subject }
        end

        describe 'second rule only' do
          before do
            first_rule.stubs(:execute).returns(false)
            second_rule.stubs(:execute).returns(true)
          end

          it { assert subject }
        end

        describe 'both rules' do
          before do
            first_rule.stubs(:execute).returns(true)
            second_rule.stubs(:execute).returns(true)
          end

          it { assert subject }
        end
      end

      describe 'false if all rules return false' do
        before do
          first_rule.stubs(:execute).returns(false)
          second_rule.stubs(:execute).returns(false)
        end

        it { assert_equal false, subject }
      end
    end
  end
end
