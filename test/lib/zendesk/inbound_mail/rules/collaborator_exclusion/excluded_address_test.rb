require_relative "../../../../../support/test_helper"

SingleCov.covered! file: 'lib/zendesk/inbound_mail/rules/collaborator_exclusion/excluded_address.rb'

describe Zendesk::InboundMail::Rules::CollaboratorExclusion::ExcludedAddress do
  let(:logger) { stub('logger', :log) }
  let(:rule) { Zendesk::InboundMail::Rules::CollaboratorExclusion::ExcludedAddress.new(logger) }

  describe '#execute' do
    let(:address) { 'johnd@zendesk.com' }

    subject { rule.execute(nil, address: address, excluded: excluded) }

    describe 'address is excluded' do
      let(:excluded) { ['johnd@zendesk.com', 'janem@zendesk.com'] }

      it 'returns true' do
        assert subject
      end

      it 'creates a log' do
        rule.expects(:log).with("Already received email, skipping: #{address}")
        subject
      end
    end

    describe 'address is not excluded' do
      let(:excluded) { ['janem@zendesk.com', 'mikkel@zendesk.com'] }

      it 'returns nil' do
        assert_blank subject
      end

      it 'does not create a log' do
        rule.expects(:log).never
        subject
      end
    end
  end
end
