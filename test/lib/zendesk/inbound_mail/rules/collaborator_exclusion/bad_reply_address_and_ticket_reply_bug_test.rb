require_relative "../../../../../support/test_helper"

SingleCov.covered! file: 'lib/zendesk/inbound_mail/rules/collaborator_exclusion/bad_reply_address_and_ticket_reply_bug.rb'

describe Zendesk::InboundMail::Rules::CollaboratorExclusion::BadReplyAddressAndTicketReplyBug do
  let(:logger) { stub('logger', :log) }
  let(:account) { accounts(:minimum) }
  let(:ticket) { Ticket.new(account: account) }
  let(:state) { stub('State', account: account, ticket: ticket) }
  let(:rule) { Zendesk::InboundMail::Rules::CollaboratorExclusion::BadReplyAddressAndTicketReplyBug.new(logger) }

  describe '#execute' do
    let(:subdomain) { 'zendesk' }
    let(:address) { "johnd@#{subdomain}.com" }

    subject { rule.execute(state, address: address) }

    describe 'address is a bad reply address and affected by a reply_to bug' do
      let(:subdomain) { 'supercell' }
      let(:address) { 'abramova.ann@bk.ru' }
      let(:created_at) { DateTime.parse('2013-11-04 23:59:59') }

      before do
        account.stubs(:subdomain).returns(subdomain)
        ticket.stubs(:created_at).returns(created_at)
        ticket.stubs(:new_record?).returns(false)
      end

      it 'returns true' do
        assert subject
      end

      it 'creates a log' do
        rule.expects(:log).with("Bad reply address introduced by a bug, skipping: #{address}")
        subject
      end
    end

    describe 'address is not bad reply address but affected by reply_to bug' do
      let(:created_at) { DateTime.parse('2013-11-04 23:59:59') }

      before do
        account.stubs(:subdomain).returns(subdomain)
        ticket.stubs(:created_at).returns(created_at)
        ticket.stubs(:new_record?).returns(false)
      end

      it 'returns true' do
        assert_blank subject
      end

      it 'does not create a log' do
        rule.expects(:log).never
        subject
      end
    end

    describe 'ticket not affected by reply_to bug but is a bad reply address' do
      let(:subdomain) { 'supercell' }
      let(:address) { 'abramova.ann@bk.ru' }

      before do
        account.stubs(:subdomain).returns(subdomain)
        ticket.stubs(:created_at).returns(created_at)
        ticket.stubs(:new_record?).returns(false)
      end

      describe 'existing ticket' do
        let(:created_at) { DateTime.parse('2013-11-05 00:00:01') }

        it 'returns true' do
          assert_blank subject
        end

        it 'does not create a log' do
          rule.expects(:log).never
          subject
        end
      end

      describe 'new ticket' do
        let(:created_at) { DateTime.current }

        before do
          ticket.stubs(:new_record?).returns(true)
        end

        it 'returns true' do
          assert_blank subject
        end

        it 'does not create a log' do
          rule.expects(:log).never
          subject
        end
      end
    end
  end
end
