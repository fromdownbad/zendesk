require_relative "../../../../../support/test_helper"

SingleCov.covered! file: 'lib/zendesk/inbound_mail/rules/collaborator_exclusion/invalid_domain.rb'

describe Zendesk::InboundMail::Rules::CollaboratorExclusion::InvalidDomain do
  let(:logger) { stub('logger', :log) }
  let(:account) { accounts(:minimum) }
  let(:address) { 'mikkel@zenbest.com' }
  let(:state) { stub('State', account: account) }
  let(:rule) { Zendesk::InboundMail::Rules::CollaboratorExclusion::InvalidDomain.new(logger) }

  describe '#execute' do
    subject { rule.execute(state, address: address) }

    describe 'address is not a invalid address' do
      it 'returns nil' do
        assert_blank subject
      end

      it 'does not create a log' do
        rule.expects(:log).never
        subject
      end
    end

    describe 'address is a invalid address' do
      before do
        UserEmailIdentity.stubs(:domain_invalid_address?).returns(true)
      end

      it 'returns true' do
        assert subject
      end

      it 'creates a log' do
        rule.expects(:log).with("Invalid @domain.invalid email address, skipping: #{address}")
        subject
      end
    end
  end
end
