require_relative "../../../../../support/test_helper"

SingleCov.covered! uncovered: 6, file: 'lib/zendesk/inbound_mail/rules/collaborator_exclusion/duplicated_requester_address.rb'

describe Zendesk::InboundMail::Rules::CollaboratorExclusion::DuplicatedRequesterAddress do
  let(:logger) { stub('logger', :log) }
  let(:account) { accounts(:minimum) }
  let(:address) { 'mikkel@zendesk-test.com' }
  let(:ticket) { Ticket.new(account: account) }
  let(:state) { Zendesk::InboundMail::ProcessingState.new(account) }
  let(:rule) { Zendesk::InboundMail::Rules::CollaboratorExclusion::DuplicatedRequesterAddress.new(logger) }

  describe '#execute' do
    subject { rule.execute(state, address: address) }

    describe 'duplicated requester address' do
      before { rule.stubs(:duplicate_of_requester_address?).returns(true) }

      it 'returns true' do
        assert subject
      end

      it 'creates a log' do
        rule.expects(:log).with("Collaborator address: #{address} is a duplicate of the requester address")
        subject
      end
    end

    describe 'not duplicated requester address' do
      before { rule.stubs(:duplicate_of_requester_address?).returns(false) }

      it 'returns nil' do
        assert_blank subject
      end

      it 'does not create a log' do
        rule.expects(:log).never
        subject
      end
    end
  end

  describe '#duplicate_of_requester_address?' do
    let(:ticket_author) { users(:minimum_end_user) }

    before do
      rule.stubs(:ticket_author).returns(ticket_author)
    end

    it 'is true if given address matches primary address of the ticket author' do
      ticket_author.email.must_equal 'minimum_end_user@aghassipour.com'
      assert rule.send(:duplicate_of_requester_address?, state, 'minimum_end_user@aghassipour.com', ticket)
    end

    it 'is true if given address matches secondary email address of the ticket author' do
      assert rule.send(:duplicate_of_requester_address?, state, 'deliverable@example.com', ticket)
    end

    it 'is true if given address contains a plus ID but matches an email of the ticket author when the ID is stripped' do
      ticket_author.identities << UserEmailIdentity.new(user: ticket_author, account: account, value: 'minimum_end_user@zendesk-test.com')
      ticket_author.identities << UserEmailIdentity.new(user: ticket_author, account: account, value: 'deliverable@zendesk-test.com')
      ticket_author.save!

      assert rule.send(:duplicate_of_requester_address?, state, 'minimum_end_user+id1@zendesk-test.com', ticket)
      assert rule.send(:duplicate_of_requester_address?, state, 'deliverable+id123@zendesk-test.com', ticket)
    end

    it 'sets the requester_is_on_email field of the ProcessingState when true' do
      refute state.requester_is_on_email
      rule.send(:duplicate_of_requester_address?, state, 'minimum_end_user@aghassipour.com', ticket)
      assert state.requester_is_on_email
    end
  end
end
