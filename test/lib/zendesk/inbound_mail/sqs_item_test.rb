require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::SQSItem do
  let(:logger) { Rails.logger }
  let(:path) { "1/abcd1234.json" }
  let(:receipt_handle) { "abcd1234" }
  let(:created_at) { Time.now.to_s }
  let(:item_value) { "abcd1235" }
  let(:receive_count) { 1 }
  let(:sqs_item) { Zendesk::InboundMail::SQSItem.new(item_value, created_at, path, receipt_handle, receive_count) }

  describe "initialization" do
    it "does not set a logger" do
      sqs_item.logger.must_be_nil
    end

    it "sets value" do
      sqs_item.value.must_equal item_value
    end

    it "sets created_at" do
      sqs_item.created_at.must_equal created_at
    end

    it "sets path" do
      sqs_item.path.must_equal path
    end

    it "sets receipt_handle" do
      sqs_item.receipt_handle.must_equal receipt_handle
    end

    it "sets receive_count" do
      sqs_item.receive_count.must_equal receive_count
    end
  end

  describe "#name" do
    it "returns the base name of the path" do
      assert_equal "abcd1234.json", sqs_item.name
    end
  end

  describe "#==" do
    it "determines object equality using the name" do
      sqs_item_2 = Zendesk::InboundMail::SQSItem.new(item_value, created_at, "2/abcd1234.json", receipt_handle, receive_count)
      assert sqs_item == sqs_item_2
    end
  end
end
