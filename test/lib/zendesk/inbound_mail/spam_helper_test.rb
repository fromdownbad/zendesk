require_relative '../../../support/test_helper'

# This test file exists only to test SpamHelper#multiplier.
# All of the rest of the functionality in the SpamHelper is tested
# inside the classes that include this module.
SingleCov.covered! uncovered: 31

describe Zendesk::InboundMail::SpamHelper do
  include MailTestHelper

  # Use RspamdProcessor as the subject because SpamHelper is included in it
  subject { Zendesk::InboundMail::Processors::RspamdProcessor }

  let(:state) { Zendesk::InboundMail::ProcessingState.new }
  let(:mail)  { Zendesk::Mail::InboundMessage.new }
  let(:account) { accounts('minimum') }

  before { state.account = account }

  def assert_multiplier(float)
    multiplier = subject.new(state, mail).multiplier
    assert_equal float, multiplier
  end

  describe '#multiplier' do
    describe 'when the state is whitelisted' do
      before { state.whitelisted = true }

      it { assert_multiplier 2.0 }
    end

    describe 'when the submitter is verified' do
      describe 'when the submitter is an agent' do
        before do
          state.submitter = users(:minimum_agent)
          state.submitter.stubs(is_verified: true)
        end

        it { assert_multiplier 4.0 }
      end

      describe 'when the submitter is not an agent' do
        before do
          state.submitter = users(:minimum_end_user)
          state.submitter.stubs(is_verified: true)
        end

        it { assert_multiplier 2.0 }
      end
    end

    describe 'when the state is authorized' do
      before do
        state.ticket = account.tickets.new
        state.stubs(authorized?: true)
      end

      describe 'when the state has a ticket updated in the past six months' do
        before { state.ticket.stubs(updated_at: 1.day.ago) }

        it { assert_multiplier 3.0 }
      end

      describe 'when the state does not have a ticket updated in the past six months' do
        before { state.ticket.stubs(updated_at: 1.year.ago) }

        it { assert_multiplier 1.0 }
      end
    end

    describe 'with different account spam_threshold_multiplier values' do
      [0.5, 1.0, 2.0].each do |spam_threshold_multiplier_value|
        describe "with an account spam_threshold_multiplier value of #{spam_threshold_multiplier_value}" do
          before { state.account.stubs(:spam_threshold_multiplier).returns(spam_threshold_multiplier_value) }

          it { assert_multiplier spam_threshold_multiplier_value }
        end
      end
    end
  end
end
