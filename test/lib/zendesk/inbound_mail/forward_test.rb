require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe Zendesk::InboundMail::Forward do
  extend ArturoTestHelper

  class MailForwardBlankClient < Zendesk::Mail::Clients::Client
  end

  let(:example_message_content) do
    <<~EML


      Begin forwarded message:
      From: Becky E <becky@example.com>
      Date: December 17, 2009 3:30:49 PM MST
      To: support@example.com
      Cc: Clifford M <4crystalmudball@example.com>, mrevans@example.com
      Subject: Changes to MRL Offer Letter

      > Ho ho ho!

      > > Older quote: 2 is > 1
    EML
  end

  let(:gmail_plaintext_forward) do
    <<~EML

      This customer needs help.

      ---------- Forwarded message ----------
      From: **John Doe** \\<john.doe@example.com\\>
      Date: Thu, Mar 31, 2016 at 2:35 PM
      Subject: A different test (agent forwarded html)
      To: Jimmy Johns \\<jimmy.johns@zendesk.com\\>

      # Setting up SPF for Zendesk to send email on behalf of your email domain

      ![Avatar](https://support.zendesk.com/system/photos/5502/1672/Jim.jpg)

      by Jim Johns

      John
    EML
  end

  let(:exchange_plaintext_forward) do
    <<~EML
      Test de transfert

      Jîm Doe
      Directeur de projet, Responsable de l’exploitation, RSSI
      [cid:image008.jpg@01D26768.5D66BEC0]<http://www.example.com/>
      www.example.com
      example – 11 A, Somewhere - 38240 Street
      Tel : +33 (0)5 55 55 55 55  - Fax : +33 (0)5 55 55 55 56

      Rejoignez-nous sur les réseaux sociaux :
      [Titre : https://www.example.com/foo/  - Description : https://www.example.com/foo/]<https://www.example.com/foo/>    [cid:image013.png@01D26768.5D66BEC0] <https://www.example.com/foo/bar>       [cid:image014.png@01D26768.5D66BEC0] <https://www.linkedin.com/company/example>

      Ce courrier électronique et les pièces jointes sont confidentiels et susceptibles de contenir des informations couvertes par le secret professionnel ; ils sont transmis à l'intention exclusive de leurs destinataires. Toute analyse, copie, diffusion ou toute autre utilisation ou action sur le fondement des informations transmises est interdite. Si vous avez reçu ce courrier électronique par erreur, merci d'en avertir l'émetteur en répondant à ce courrier et de détruire toute copie de ce message.
      [cid:image005.png@01D320F8.A5A5EF90]Ensemble, agissons pour l’environnement : n’imprimez ce mail que si nécessaire.

      De : Jane Doe <jane.doe@example.com>
      Envoyé : mercredi 29 avril 2020 15:46
      À : Jîm Doe <jim.doe@example.com>
      Objet : Agent Light

      Test de transfert

    EML
  end

  let(:parseable_exchange_html_forward) do
    <<~HTML
      <html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns:m="http://schemas.microsoft.com/office/2004/12/omml" xmlns="http://www.w3.org/TR/REC-html40">
      <head>
      </head>
      <body lang="FR" link="#0563C1" vlink="#954F72">
      <div class="WordSection1">
        <p class="MsoNormal"><span style="mso-fareast-language:EN-US">Test de transfert
          <o:p></o:p>
        </span></p>
        <p class="MsoNormal"><span style="mso-fareast-language:EN-US">
          <o:p>&nbsp;</o:p>
        </span></p>
        <p class="MsoNormal"><span style="color:#1F4E79">Jîm Doe
          <o:p></o:p>
        </span></p>
        <p class="MsoNormal">
          <span style="font-size:9.0pt;color:#1F4E79">Directeur de projet, Responsable de l’exploitation, RSSI
            <o:p></o:p>
          </span></p>
        <p class="MsoNormal">
          <a href="http://www.example.com/"><span style="color:#1F497D;text-decoration:none"><img border="0" width="205" height="60" style="width:2.1354in;height:.625in" id="Image_x0020_6" src="cid:image001.jpg@01D61E3D.7735EF30" alt="cid:image008.jpg@01D26768.5D66BEC0"></span></a>
          <o:p></o:p>
        </p>
        <p class="MsoNormal"><a href="www.example.com"><b><span style="color:#0563C1">www.example.com</span></b></a><b>
          <o:p></o:p>
        </b></p>
        <p class="MsoNormal">
          <span style="color:#1F4E79">example </span><span style="font-size:9.0pt;color:#1F4E79">– 11 A, Somewhere - 38240 Street
          <o:p></o:p>
        </span></p>
        <p class="MsoNormal">
          <span style="font-size:9.0pt;color:#1F4E79">Tel&nbsp;: &#43;33 (0)5 55 55 55 55 &nbsp;- </span><span style="font-size:10.0pt;color:#1F497D">Fax : &#43;33 (0)5 55 55 55 56</span><span style="font-size:9.0pt;color:#70AD47">
          <o:p></o:p>
        </span></p>
        <p class="MsoNormal"><span style="font-size:9.0pt;color:#70AD47">
          <o:p>&nbsp;</o:p>
        </span></p>
        <p class="MsoNormal"><b><span style="font-size:9.0pt">Rejoignez-nous sur&nbsp;les réseaux sociaux&nbsp;:
          <o:p></o:p>
        </span></b></p>
        <p class="MsoNormal">
          <a href="https://www.example.com/foo/"><b><span style="font-size:9.0pt;color:windowtext;text-decoration:none"><img border="0" width="27" height="27" style="width:.2812in;height:.2812in" id="Image_x0020_2" src="cid:image002.jpg@01D61E3D.7735EF30" alt="Titre&nbsp;: https://www.example.com/foo/  - Description&nbsp;: https://www.example.com/foo/ "></span></b></a>&nbsp;&nbsp;&nbsp;<b><span style="font-size:9.0pt">&nbsp;</span></b><a href="https://www.example.com/foo/bar"><b><span style="font-size:9.0pt;color:windowtext;text-decoration:none"><img border="0" width="22" height="28" style="width:.2291in;height:.2916in" id="Image_x0020_3" src="cid:image003.png@01D61E3D.7735EF30" alt="cid:image013.png@01D26768.5D66BEC0"></span></b></a>
          <b><span style="font-size:9.0pt">&nbsp; &nbsp;&nbsp;&nbsp;</span></b><a href="https://www.linkedin.com/company/example"><b><span style="font-size:9.0pt;color:windowtext;text-decoration:none"><img border="0" width="27" height="27" style="width:.2812in;height:.2812in" id="Image_x0020_4" src="cid:image004.png@01D61E3D.7735EF30" alt="cid:image014.png@01D26768.5D66BEC0"></span></b></a><b><span style="font-size:9.0pt">&nbsp;&nbsp;<o:p></o:p>
        </span></b></p>
        <p class="MsoNormal"><i><span style="font-size:8.0pt">
          <o:p>&nbsp;</o:p>
        </span></i></p>
        <p class="MsoNormal">
          <i><span style="font-size:8.0pt;color:black">Ce courrier électronique et les pièces jointes sont confidentiels et susceptibles de contenir des informations couvertes par le secret professionnel ; ils sont transmis à l'intention exclusive de leurs destinataires. Toute analyse, copie, diffusion ou toute autre utilisation ou action sur le fondement des informations transmises est interdite. Si vous avez reçu ce courrier électronique par erreur, merci d'en avertir l'émetteur en répondant à ce courrier et de détruire toute copie de ce message.
            <o:p></o:p>
          </span></i></p>
        <p class="MsoNormal">
          <i><span style="font-size:8.0pt"><img border="0" width="20" height="22" style="width:.2083in;height:.2291in" id="Image_x0020_1" src="cid:image005.png@01D61E3D.7735EF30" alt="cid:image005.png@01D320F8.A5A5EF90">Ensemble, agissons pour l’environnement&nbsp;: n’imprimez ce mail que si nécessaire.</span></i>
          <o:p></o:p>
        </p>
        <p class="MsoNormal"><span style="mso-fareast-language:EN-US">
          <o:p>&nbsp;</o:p>
        </span></p>
        <p class="MsoNormal"><b>De&nbsp;:</b> Jane Doe &lt;jane.doe@example.com&gt; <br>
          <b>Envoyé&nbsp;:</b> mercredi 29 avril 2020 15:46<br> <b>À&nbsp;:</b> Jîm Doe &lt;jim.doe@example.com&gt;<br>
          <b>Objet&nbsp;:</b> Agent Light
          <o:p></o:p>
        </p>
        <p class="MsoNormal">
          <o:p>&nbsp;</o:p>
        </p>
        <div>
          <p class="MsoNormal">Test de transfert
            <o:p></o:p>
          </p>
        </div>
      </div>
      </body>
      </html>
    HTML
  end

  let(:remaining_exchange_html) do
    <<~HTML
      <p class="MsoNormal">
        <o:p>&nbsp;</o:p>
      </p>
      <div>
        <p class="MsoNormal">Test de transfert
          <o:p></o:p>
        </p>
      </div>
    HTML
  end

  let(:account) { Account.new }

  before do
    @mail         = Zendesk::Mail::InboundMessage.new
    @forward      = Zendesk::InboundMail::Forward.new(@mail, nil)
    @mail.account = account
  end

  describe "from a client without defined forwarding patterns" do
    before do
      @mail.stubs(:client).returns(MailForwardBlankClient)
      @mail.subject = 'Fwd: Hi'
      @forward.plain_content = <<~EML
        ------Original Message------

        From: someone@example.com

        Hello
      EML
    end

    it "does not blow up" do
      @forward.valid?
    end
  end

  describe "forward?" do
    it "is true when subject begins with Fwd" do
      @mail.subject = "Fwd: Hello?"
      assert(@forward.suspected_forward?)
    end

    it "is true when the subject begins with Fw" do
      @mail.subject = "Fw: Hello?"
      assert(@forward.suspected_forward?)
    end

    describe "subject with a full-width colon" do
      it "returns true" do
        @mail.subject = "转发：Faulty robot"
        assert(@forward.suspected_forward?)
      end

      it "returns true" do
        # Full-width colon followed by a space
        @mail.subject = "转发： Faulty robot"
        assert(@forward.suspected_forward?)
      end

      it "returns false" do
        @mail.subject = "Re：转发：Faulty robot"
        refute(@forward.suspected_forward?)
      end
    end

    it "is case insensitive" do
      @mail.subject = "FwD Hello?"
      assert(@forward.suspected_forward?)
    end

    it "stills be true when non-word characters appear before the Fwd tag" do
      @mail.subject = " [ Fwd:] Hello?"
      assert(@forward.suspected_forward?)
    end

    it "is true when the subject after the tag is blank" do
      @mail.subject = 'Fwd:'
      assert(@forward.suspected_forward?)
    end

    it 'is true when the subject uses a Norwegian forwarding marker' do
      @mail.subject = 'VS: the cat ate my aglets'
      assert @forward.suspected_forward?
    end

    it "is false when the subject doesn't begin with the Fwd tag" do
      @mail.subject = "Re: Hello?"
      assert_equal false, @forward.suspected_forward?

      @mail.subject = "Re: Fwd: Hello?"
      assert_equal false, @forward.suspected_forward?

      @mail.subject = "[Re] Fwd: Hello?"
      assert_equal false, @forward.suspected_forward?
    end
  end

  describe "valid?" do
    describe "when body is empty" do
      before do
        @mail.subject = "Fwd: Snap into it"
        @forward.from = Zendesk::Mail::Address.parse("Randy Savage <machoman@slimjim.com>")
        @forward.stubs(:body).returns(nil)
      end

      it "is false" do
        assert_equal false, @forward.valid?
      end

      it "is true if there are attachments" do
        @attachment = Zendesk::Mail::Part.new.tap do |attachment|
          attachment.file_name    = "test.txt"
          attachment.body         = "body"
          attachment.content_type = "text/plain"
        end
        @mail.parts = [@attachment]

        assert(@forward.valid?)
      end
    end
  end

  describe "#errors" do
    it "is provided when there is no forwarded address" do
      @forward.plain_content = ''
      assert_equal [:no_subject, :no_from_address], @forward.errors
    end

    it "is provided when there is no forwarded content" do
      @forward.plain_content = "Some comment\n\n--- Begin Forwarded Message\nFrom: somebody@example.com"

      assert_equal [:no_subject, :no_content], @forward.errors
    end

    describe "when a Timeout error is raised" do
      before do
        @mail.stubs(:subject).raises(Zendesk::InboundMail::ProcessingTimeout)
      end

      it { assert_equal [:processing_timeout], @forward.errors }
    end

    it "is provided when there is no forwarded content" do
      @forward.plain_content = "Some comment\n\n--- Begin Forwarded Message\nFrom: somebody@example.com"
      assert_equal [:no_subject, :no_content], @forward.errors
    end
  end

  describe "International forwarded emails" do
    it "does not hang" do
      mail = FixtureHelper::Mail.read("forwards/agent_forwarding_timeout.json")
      plain_content = mail.body.to_s
      Zendesk::InboundMail::Forward.new(mail, plain_content)
    end
  end

  describe "Gmail emails with linebreaks in the from line" do
    it "detects the forward correctly" do
      %w[forwards/gmail/linebreak_example_1.json forwards/gmail/linebreak_example_2.json forwards/gmail/linebreak_example_3.json].each do |json|
        mail = FixtureHelper::Mail.read(json)
        plain_content = mail.body.to_s
        Zendesk::InboundMail::Forward.new(mail, plain_content)
      end
    end
  end

  describe "subject keywords" do
    it "is at the start of the subject" do
      @mail.subject = 'Re: Fwd: NOTICE: Failed credit card transaction'
      refute @forward.subject
    end

    it "excludes invalid forward tags" do
      @mail.subject = 'Fwded message this is not'
      refute @forward.subject
    end

    it "includes variations of Fw and Fwd" do
      supported_keywords = %w[DOORST Doorst doorst]
      supported_keywords += %w[ENC Enc enc]
      supported_keywords += %w[FS Fs fs]
      supported_keywords += %w[FWD Fwd fwd FW Fw fw]
      supported_keywords += %w[I i]
      supported_keywords += %w[İLT]
      supported_keywords += %w[PD Pd pd]
      supported_keywords += %w[RV Rv rv]
      supported_keywords += %w[TOVÁBBÍTÁS Továbbítás továbbítás]
      supported_keywords += %w[TR Tr tr]
      supported_keywords += %w[VB Vb vb]
      supported_keywords += %w[VL Vl vl]
      supported_keywords += %w[VS Vs vs]
      supported_keywords += %w[WG Wg wg]
      supported_keywords += %w[YML Yml yml]
      supported_keywords += %w[ΠΡΘ]
      supported_keywords += %w[НА]
      supported_keywords += %w[הועבר]
      supported_keywords += %w[轉寄]
      supported_keywords += %w[转发]
      supported_keywords += ["إعادة توجيه"]
      supported_keywords.each do |supported_keyword|
        @mail.subject = "#{supported_keyword}: Hello world!"
        assert_equal 'Hello world!', @forward.subject, "Forwarding keyword variation failed: #{supported_keyword}"
      end
    end
  end

  it "has subject with keywords removed" do
    @mail.subject = 'Fwd: Naughty v Nice list'
    assert_equal 'Naughty v Nice list', @forward.subject

    @mail.subject = 'Fwd:Naughty v Nice list'
    assert_equal 'Naughty v Nice list', @forward.subject

    @mail.subject = 'Fwd Naughty v Nice list'
    assert_equal 'Naughty v Nice list', @forward.subject
  end

  it "reverts body to pre-forwarded appearance" do
    @forward.plain_content = <<~EML

      Begin forwarded message:
      From: Becky E <becky@example.com>
      Date: December 17, 2009 3:30:49 PM MST
      To: support@example.com
      Cc: Clifford M <4crystalmudball@example.com>, mrevans@example.com
      Subject: Changes to MRL Offer Letter

      Ho ho ho!

    EML

    assert_equal 'Ho ho ho!', @forward.body
  end

  it "has body without forwarding quotes" do
    @forward.plain_content = example_message_content

    assert_equal "Ho ho ho!\n\n> Older quote: 2 is > 1", @forward.body
  end

  it "handles leading whitespace" do
    # do not convert the <<- format since it will possibly loose whitespace on empty lines
    @forward.plain_content = "Von: Peter [mailto:Peter@foo.com] \nGesendet: Freitag, 8. August 2014 11:27\nAn: 'Andreas'\nBetreff: AW: something\nelse\n \nHallo Peter"
    assert_equal "Hallo Peter", @forward.body
  end

  describe "#comment" do
    let(:forwarded) { "Von: Peter [mailto:Peter@foo.com] \nGesendet: Freitag, 8. August 2014 11:27\nAn: 'Andreas'\nBetreff: AW: something\nelse\n \nHallo Peter" }

    it "extracts agent comment" do
      @forward.plain_content = "Silly spammer ...\n\n#{forwarded}"
      assert_equal "Hallo Peter", @forward.body
      assert_equal "Silly spammer ...", @forward.comment
    end

    it "ignores empty comment" do
      @forward.plain_content = "\n  \n   \n  \n#{forwarded}"
      assert_equal "Hallo Peter", @forward.body
      assert_nil @forward.comment
    end
  end

  describe "using LotusNotes client" do
    it "does not include quotes or forward body" do
      @mail.stubs(:client).returns(Zendesk::Mail::Clients::LotusNotes)
      @forward.plain_content = example_message_content

      assert_equal "Ho ho ho!\n\n> Older quote: 2 is > 1", @forward.body
    end
  end

  describe "#html_body" do
    describe "forwarded from Gmail client" do
      before { @forward.plain_content = gmail_plaintext_forward }

      it "returns nil with when mail.content.html is nil" do
        Zendesk::MtcMpqMigration::Content.any_instance.stubs(:html).returns(nil)
        assert_nil @forward.html_body
      end

      it "returns nil with invalid forward" do
        Zendesk::MtcMpqMigration::Content.any_instance.stubs(:html).returns("qwerty")
        assert_nil @forward.html_body
      end
    end

    describe "forwarded from Exchange client" do
      describe "with &nbsp; in the forward headers" do
        before do
          @forward.plain_content = exchange_plaintext_forward
          @mail.stubs(:client).returns(Zendesk::Mail::Clients::MicrosoftExchange)
          Zendesk::MtcMpqMigration::Content.any_instance.stubs(:html).returns(parseable_exchange_html_forward)
        end

        it "uses [[:space:]] regexp matching" do
          assert_html_equal remaining_exchange_html, @forward.html_body
        end

        describe_with_arturo_disabled :email_escape_forward_header_spaces_fix do
          it "does not find the forward header" do
            assert Nokogiri.HTML(@forward.html_body).text.include? "De : Jane Doe <jane.doe@example.com>"
          end
        end
      end
    end
  end

  describe "original sender" do
    it "is from the most recent forwarded message" do
      @forward.plain_content = 'From: rudolph@example.com boo! ..
      From: santa@example.com'
      assert_equal 'rudolph@example.com', @forward.from.address, @forward.from.inspect
    end

    it "extracts from name" do
      @forward.plain_content = 'From: Mr. Jolly <santa@example.com>'
      assert_equal 'santa@example.com', @forward.from.address
      assert_equal 'Mr. Jolly', @forward.from.name
    end

    it "extracts from quoted printable leftovers" do
      @forward.plain_content = 'From: Example das Am=E9ricas [mailto:santa@example.com]=20'
      assert_equal 'santa@example.com', @forward.from.address
      assert_equal 'Example das Am=E9ricas', @forward.from.name
    end

    it "extracts from mailto" do
      @forward.plain_content = 'From: mailto:[santa@example.com]'
      assert_equal 'santa@example.com', @forward.from.address, 'Spaces around From'
      assert_equal "Santa", @forward.from.name
    end

    describe "in various languages" do
      messages = ["From: rudolph@example.com"]
      messages << "Da: rudolph@example.com"
      messages << "De: rudolph@example.com"
      messages << "Od: rudolph@example.com"
      messages << "Fra: rudolph@example.com"
      messages << "Frá: rudolph@example.com"
      messages << "Från: rudolph@example.com"
      messages << "Von: rudolph@example.com"
      messages << "寄件人: rudolph@example.com"
      messages << "发件人: rudolph@example.com"
      messages << "מאת: rudolph@example.com"
      messages << "Van: rudolph@example.com"
      messages << "送信者: rudolph@example.com"
      messages << "差出人: rudolph@example.com"
      messages << "Kimden: rudolph@example.com"
      messages << "Dari: rudolph@example.com"
      messages << "จาก: rudolph@example.com"
      messages << "보낸 사람: rudolph@example.com"
      messages << "द्वारा: rudolph@example.com"
      messages << "От кого: rudolph@example.com"
      messages << "Lähettäjä: rudolph@example.com"

      messages.each do |message|
        it "is extracted from '#{message}'" do
          @forward.plain_content = message
          from = @forward.from
          assert_not_nil from, "forward.from is nil"
          assert_equal 'rudolph@example.com', from.address
        end
      end
    end

    it "does not be extracted unless next to the From: header" do
      @forward.plain_content = 'From: The Red Menace
                    lots of words and new lines
                    that are next next to the header
                    santa@example.com'
      assert_nil @forward.from
    end
  end
end
