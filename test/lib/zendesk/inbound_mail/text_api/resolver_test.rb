require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 2

describe Zendesk::InboundMail::TextApi::Resolver do
  before do
    @resolver = Zendesk::InboundMail::TextApi::Resolver.new
  end

  describe "an unknown tag" do
    before { @tag = "unknown" }

    it "raises Resolver::UnknownTag" do
      assert_raise(Zendesk::InboundMail::TextApi::Resolver::UnknownTag) { resolve("whatever") }
    end
  end

  describe "tag: public" do
    before { @tag = "public" }

    it "resolves the value `yes' to true" do
      assert_equal resolve("yes"), [:is_public, true]
    end

    it "resolves the value `true' to true" do
      assert_equal resolve("true"), [:is_public, true]
    end

    it "resolves the value `no' to false" do
      assert_equal resolve("no"), [:is_public, false]
    end

    it "resolves the value `false' to false" do
      assert_equal resolve("false"), [:is_public, false]
    end

    it "resolves the value `nil` to true" do
      assert_equal resolve(nil), [:is_public, true]
    end
  end

  describe "tag: assignee" do
    before { @tag = "assignee" }

    it "maps an email address to :assignee_email" do
      assert_equal resolve("bodie@example.com"), [:assignee_email, "bodie@example.com"]
    end

    it "maps an id to :assignee_id" do
      assert_equal resolve("23"), [:assignee_id, 23]
    end
  end

  describe "tag: requester" do
    before { @tag = "requester" }

    describe 'having an invalid email address' do
      let(:tag_value) { 'foo@bar.com>' }

      it 'sanitizes the :requester_email' do
        assert_equal resolve(tag_value), [:requester_email, 'foo@bar.com']
      end
    end

    it "maps an email address to :requester_email" do
      assert_equal resolve("bodie@example.com"), [:requester_email, "bodie@example.com"]
    end

    it "maps an id to :requester_id" do
      assert_equal resolve("23"), [:requester_id, 23]
    end

    it "ignores all content after the first space because some mail clients auto-link the email address" do
      assert_equal resolve("foo@bar.com (mailto:foo@bar.com)"), [:requester_email, "foo@bar.com"]
    end
  end

  describe "tag: group" do
    before { @tag = "group" }

    it "maps a group name to :group_name" do
      assert_equal resolve("smurfs"), [:group_name, "smurfs"]
    end

    it "maps an id to :group_id" do
      assert_equal resolve("42"), [:group_id, 42]
    end
  end

  describe "tag: status" do
    before { @tag = "status" }

    it "finds the status type with StatusType" do
      StatusType.expects(:find).with("open").returns(:open)
      assert resolve("open")
    end

    it "downcases the value" do
      StatusType.expects(:find).with("open").returns(:open)
      assert resolve("OPEN")
    end

    it "returns the status type" do
      StatusType.expects(:find).returns(:foo)
      assert_equal resolve("open"), [:status_id, :foo]
    end

    it "raises Resolve::InvalidValue if the value is nil" do
      StatusType.expects(:find).returns(nil)
      assert_raise(Zendesk::InboundMail::TextApi::Resolver::InvalidValue) { resolve(nil) }
    end

    it "raises Resolver::InvalidValue if the value is not found" do
      StatusType.expects(:find).returns(nil)
      assert_raise(Zendesk::InboundMail::TextApi::Resolver::InvalidValue) { resolve("rocking") }
    end
  end

  describe "Short tags for StatusType" do
    describe "tag: open" do
      before { @tag = "open" }

      it "finds the status type with StatusType" do
        StatusType.expects(:find).returns(:open)
        assert resolve('')
      end

      it "returns the status_id" do
        StatusType.expects(:find).returns(:open)
        assert_equal resolve(''), [:status_id, :open]
      end
    end

    describe "tag: pending" do
      before { @tag = "pending" }

      it "finds the status type with StatusType" do
        StatusType.expects(:find).returns(:pending)
        assert resolve('')
      end

      it "returns the status_id" do
        StatusType.expects(:find).returns(:pending)
        assert_equal resolve(''), [:status_id, :pending]
      end
    end

    describe "tag: hold" do
      before { @tag = "hold" }

      it "finds the status type with StatusType" do
        StatusType.expects(:find).returns(:hold)
        assert resolve('')
      end

      it "returns the status_id" do
        StatusType.expects(:find).returns(:hold)
        assert_equal resolve(''), [:status_id, :hold]
      end
    end

    describe "tag: on-hold" do
      before { @tag = "on-hold" }

      it "finds the status type with StatusType" do
        StatusType.expects(:find).returns(:hold)
        assert resolve('')
      end

      it "returns the status_id" do
        StatusType.expects(:find).returns(:hold)
        assert_equal resolve(''), [:status_id, :hold]
      end
    end

    describe "tag: solved" do
      before { @tag = "solved" }

      it "finds the status type with StatusType" do
        StatusType.expects(:find).returns(:solved)
        assert resolve('')
      end

      it "returns the status_id" do
        StatusType.expects(:find).returns(:solved)
        assert_equal resolve(''), [:status_id, :solved]
      end
    end
  end

  describe "Short tags for priority" do
    describe "tag: low" do
      before { @tag = "low" }

      it "finds the priority type with PriorityType" do
        PriorityType.expects(:find).returns(:low)
        assert resolve('')
      end

      it "returns the priority_id" do
        PriorityType.expects(:find).returns(:low)
        assert_equal resolve(''), [:priority_id, :low]
      end
    end

    describe "tag: normal" do
      before { @tag = "normal" }

      it "finds the priority type with PriorityType" do
        PriorityType.expects(:find).returns(:normal)
        assert resolve('')
      end

      it "returns the priority_id" do
        PriorityType.expects(:find).returns(:normal)
        assert_equal resolve(''), [:priority_id, :normal]
      end
    end

    describe "tag: urgent" do
      before { @tag = "urgent" }

      it "finds the priority type with PriorityType" do
        PriorityType.expects(:find).returns(:urgent)
        assert resolve('')
      end

      it "returns the priority_id" do
        PriorityType.expects(:find).returns(:urgent)
        assert_equal resolve(''), [:priority_id, :urgent]
      end
    end

    describe "tag: high" do
      before { @tag = "high" }

      it "finds the priority type with PriorityType" do
        PriorityType.expects(:find).returns(:high)
        assert resolve('')
      end

      it "returns the priority_id" do
        PriorityType.expects(:find).returns(:high)
        assert_equal resolve(''), [:priority_id, :high]
      end
    end
  end

  describe "Short tags for is_public" do
    describe "tag: note" do
      before { @tag = "note" }

      it "sets is_public to false" do
        assert_equal resolve(''), [:is_public, false]
      end
    end
  end

  describe "Short tags for TicketType" do
    describe "tag: problem" do
      before { @tag = "problem" }

      it "finds the priority type with TicketType" do
        TicketType.expects(:find).returns(:problem)
        assert resolve('')
      end

      it "returns the ticket_type_id" do
        TicketType.expects(:find).returns(:problem)
        assert_equal resolve(''), [:ticket_type_id, :problem]
      end
    end

    describe "tag: question" do
      before { @tag = "question" }

      it "finds the priority type with TicketType" do
        TicketType.expects(:find).returns(:question)
        assert resolve('')
      end

      it "returns the ticket_type_id" do
        TicketType.expects(:find).returns(:question)
        assert_equal resolve(''), [:ticket_type_id, :question]
      end
    end

    describe "tag: incident" do
      before { @tag = "incident" }

      it "finds the priority type with TicketType" do
        TicketType.expects(:find).returns(:incident)
        assert resolve('')
      end

      it "returns the ticket_type_id" do
        TicketType.expects(:find).returns(:incident)
        assert_equal resolve(''), [:ticket_type_id, :incident]
      end
    end

    describe "tag: task" do
      before { @tag = "task" }

      it "finds the priority type with TicketType" do
        TicketType.expects(:find).returns(:task)
        assert resolve('')
      end

      it "returns the ticket_type_id" do
        TicketType.expects(:find).returns(:task)
        assert_equal resolve(''), [:ticket_type_id, :task]
      end
    end
  end

  describe "tag: priority" do
    before { @tag = "priority" }

    it "finds the priority type with PriorityType" do
      PriorityType.expects(:find).with("critical").returns(:critical)
      assert resolve("critical")
    end

    it "downcases the value" do
      PriorityType.expects(:find).with("critical").returns(:critical)
      assert resolve("CRITICAL")
    end

    it "returns the status type" do
      PriorityType.expects(:find).returns(:critical)
      assert_equal resolve("critical"), [:priority_id, :critical]
    end

    it "raises Resolve::InvalidValue if the value is nil" do
      PriorityType.expects(:find).returns(nil)
      assert_raise(Zendesk::InboundMail::TextApi::Resolver::InvalidValue) { resolve(nil) }
    end

    it "raises Resolver::InvalidValue if the value is not found" do
      PriorityType.expects(:find).returns(nil)
      assert_raise(Zendesk::InboundMail::TextApi::Resolver::InvalidValue) { resolve("ultrahigh") }
    end
  end

  describe "tag: type" do
    before { @tag = "type" }

    it "finds the priority type with TicketType" do
      TicketType.expects(:find).with("question").returns(:question)
      assert resolve("question")
    end

    it "downcases the value" do
      TicketType.expects(:find).with("question").returns(:question)
      assert resolve("QUESTION")
    end

    it "returns the status type" do
      TicketType.expects(:find).returns(:question)
      assert_equal resolve("question"), [:ticket_type_id, :question]
    end

    it "raises Resolve::InvalidValue if the value is nil" do
      TicketType.expects(:find).returns(nil)
      assert_raise(Zendesk::InboundMail::TextApi::Resolver::InvalidValue) { resolve(nil) }
    end

    it "raises Resolver::InvalidValue if the value is not found" do
      TicketType.expects(:find).returns(nil)
      assert_raise(Zendesk::InboundMail::TextApi::Resolver::InvalidValue) { resolve("godzilla") }
    end
  end

  describe "tag: tags" do
    before { @tag = "tags" }

    it "returns the provided tags" do
      assert_equal resolve("foo bar baz"), [:set_tags, "foo bar baz"]
    end

    it "downcases the tags" do
      assert_equal resolve("FOO bAr BaZ"), [:set_tags, "foo bar baz"]
    end

    it "clears existing tags when no tags are provided" do
      assert_equal resolve(nil), [:set_tags, ""]
    end
  end

  describe "tag: tag" do
    before { @tag = "tag" }

    it "returns the provided tags" do
      assert_equal resolve("foo bar baz"), [:set_tags, "foo bar baz"]
    end

    it "downcases the tags" do
      assert_equal resolve("FOO bAr BaZ"), [:set_tags, "foo bar baz"]
    end

    it "clears existing tags when no tags are provided" do
      assert_equal resolve(nil), [:set_tags, ""]
    end
  end

  def resolve(value)
    @resolver.resolve(@tag, value)
  end
end
