require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::RspamdSpamScore do
  before do
    @message = Zendesk::Mail::InboundMessage.new
    @score   = Zendesk::InboundMail::RspamdSpamScore.new(@message)
  end

  describe 'ham?' do
    before { @message.headers['X-Rspamd-Status'] = 'No, 4.0 some random string with coma, and another one, Yes, 5.0, Yes, 7.0' }

    it { assert @score.ham? }
    it { refute @score.spam? }
    it { assert_equal 'ham', @score.classification }
  end

  describe 'spam?' do
    before { @message.headers['X-Rspamd-Status'] = 'Yes, 6.0, some random string with coma No, 5.0 and No, 4.0' }

    it { assert @score.spam? }
    it { refute @score.ham? }
    it { assert_equal 'spam', @score.classification }
  end

  describe 'empty header' do
    before { @message.headers.delete('X-Rspamd-Status') }

    it { assert @score.ham? }
    it { refute @score.spam? }
    it { assert_equal 'ham', @score.classification }
  end
end
