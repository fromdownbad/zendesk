require_relative "../../../support/test_helper"
require_relative "../../../support/arturo_test_helper"

SingleCov.covered! uncovered: 7

describe Zendesk::InboundMail::FailureHandler do
  extend ArturoTestHelper

  fixtures :tickets, :accounts

  def handle_hard_rejection
    Zendesk::InboundMail::FailureHandler::Rejection.handle(state, mail, Zendesk::InboundMail::HardMailRejectException.new)
  end

  def handle_duplicate_mail_rejection
    Zendesk::InboundMail::FailureHandler::Rejection.handle(state, mail, Zendesk::InboundMail::DuplicateMailRejectException.new)
  end

  let(:account) { accounts(:minimum) }
  let(:ticket) { tickets(:minimum_1) }
  let(:state) { Zendesk::InboundMail::ProcessingState.new(account) }
  let(:message_id) { "<messageid@someplace.com>" }
  let(:mail) { stub(logger: Rails.logger, message_id: message_id, delete_remote_files: true) }
  let(:statsd_client) { mock }
  let(:inbound_email) { ticket.inbound_emails.create!(message_id: message_id, from: ticket.requester.email) }

  describe ".handle" do
    before do
      Zendesk::InboundMail::StatsD.stubs(:client).returns(statsd_client)
      state.inbound_email = inbound_email
      statsd_client.stubs(:increment)
    end

    describe "with a hard rejection" do
      it 'increments hard rejected ticket stats' do
        statsd_client.expects(:increment).with("ticket.rejected", tags: ["type:hard"])
        handle_hard_rejection
      end

      it 'set state as rejected' do
        state.expects(:rejected!).once
        handle_hard_rejection
      end

      it "deletes remote files" do
        mail.expects(:delete_remote_files).once
        handle_hard_rejection
      end

      it 'rejects and saves inbound email' do
        inbound_email.expects(:reject).once
        inbound_email.expects(:save!).once
        handle_hard_rejection
      end

      describe 'when state has suspension reason and inbound_email is not yet categorized' do
        before do
          state.suspension = 1
          inbound_email.category_id = nil
        end

        it 'marks inbound email with suspension type' do
          inbound_email.expects(:mark_by_system).with(1)
          handle_hard_rejection
        end

        it 'increments marked_by_system stats' do
          statsd_client.expects(:increment).with(:marked_by_system, tags: ["suspension_type:not_on_whitelist"])
          handle_hard_rejection
        end
      end
    end

    describe "with a duplicate mail rejection" do
      it 'increments soft rejected ticket stats' do
        statsd_client.expects(:increment).with("ticket.rejected", tags: ["type:soft"])
        handle_duplicate_mail_rejection
      end

      it 'set state as rejected' do
        state.expects(:rejected!).once
        handle_duplicate_mail_rejection
      end

      it "does not delete remote files" do
        mail.expects(:delete_remote_files).never
        handle_duplicate_mail_rejection
      end

      it 'rejects and saves inbound email' do
        inbound_email.expects(:reject).once
        inbound_email.expects(:save!).once
        handle_hard_rejection
      end
    end
  end
end
