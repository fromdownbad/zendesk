require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::InboundMail::DispatchFailureHandler do
  let(:path) { '/data/zendesk/json_mail/retry/cur/234.567.work111.pod999.example.com.json' }
  let(:exception_message) { 'whoops, something is wrong' }
  let(:exception) { StandardError.new(exception_message) }
  let(:additional_message) { nil }

  before do
    exception.stubs(backtrace: ['method_a:2', 'method_b:3'])
  end

  describe '.notify' do
    subject { Zendesk::InboundMail::DispatchFailureHandler.notify(path, exception, additional_message) }
    let(:statsd_client) { stub(increment: true) }

    before do
      Zendesk::InboundMail::StatsD.stubs(:client).returns(statsd_client)
      ZendeskExceptions::Logger.stubs(:record)
    end

    it 'notifies Datadog of processing errors' do
      statsd_client.expects(:increment).with("processing_error.error", tags: ["detail:StandardError"])
      subject
    end

    it 'records exception with more detailed fingerprint' do
      ZendeskExceptions::Logger.expects(:record).with do |_e, params|
        assert_equal(exception_message, params[:fingerprint])
      end
      subject
    end

    it "records metadata when it is provided" do
      metadata = { message_id: "<5eb22f5b3eadf_811025c420ac_sprut@zendesk.com>" }
      ZendeskExceptions::Logger.expects(:record).with do |_e, params|
        assert_equal(exception_message, params[:fingerprint])
        assert_equal(metadata, params[:metadata])
      end
      Zendesk::InboundMail::DispatchFailureHandler.notify(path, exception, additional_message, metadata)
    end

    it 'records message prepended with exception info' do
      ZendeskExceptions::Logger.expects(:record).with do |_e, params|
        assert_equal("#{exception_message}: processing mail at #{path}", params[:message])
      end
      subject
    end

    describe 'when additional message is passed in' do
      let(:additional_message) { 'permanently failed' }

      it 'records message prepended with exception info and then additional message' do
        ZendeskExceptions::Logger.expects(:record).with do |_e, params|
          assert_equal("#{exception_message}: permanently failed: processing mail at #{path}", params[:message])
        end
        subject
      end
    end
  end

  describe '.log' do
    let(:logger) { stub }

    subject { Zendesk::InboundMail::DispatchFailureHandler.log(path, exception) }

    before { Zendesk::Mail.expects(:logger).returns(logger) }

    it 'logs exception info' do
      logger.expects(:info).with(
        "Unhandled exception while Processing mail at #{path}, StandardError: #{exception_message} \nmethod_a:2\nmethod_b:3"
      )
      subject
    end

    describe "when the exception does not have a backtrace" do
      before do
        exception.unstub(:backtrace)
        exception.stubs(backtrace: nil)
      end

      it 'logs exception info' do
        logger.expects(:info).with(
          "Unhandled exception while Processing mail at #{path}, StandardError: #{exception_message} \n"
        )
        subject
      end
    end
  end
end
