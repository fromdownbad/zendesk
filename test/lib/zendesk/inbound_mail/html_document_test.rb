require_relative "../../../support/test_helper"
require "zendesk/inbound_mail/html_document"

SingleCov.covered! uncovered: 14

describe Zendesk::InboundMail::HTMLDocument do
  let(:forward) do
    <<~EML
      <html>
        <head></head>
        <body>
          <div>This customer needs help.
            <blockquote>
              <div class="remove_if_empty_2 html_up_to_1">
                <br>
                <div class="remove_surrounding_containers_1">
                  ---------- Forwarded message ----------<br>
                  <span><b>From:</b></span> <b>John Doe</b> <span>&lt;<a href=\"mailto:john.doe@example.com\">john.doe@example.com</a>&gt;</span><br>
                  <span><b>Date:</b></span> <span>Thu, Mar 31, 2016 at 2:35 PM</span><br>
                  <span><b>Subject:</b></span> <span>A different test (agent forwarded html)</span><br>
                  <span><b class="next_element_with_text_1">To:</b></span> <span class="next_element_with_text_2 remove_self_and_preceding_2">Jimmy Johns &lt;<a class="remove_self_and_preceding_1" href=\"mailto:jimmy.johns@zendesk.com\">jimmy.johns@zendesk.com</a>&gt;</span><br>
                  <br><br>
                  <div class="next_element_with_text_3">
                    <h1 style=\"font-size:24px;" class="remove_if_empty_3">
                      <font>HTML email</font>
                    </h1>
                    <div>
                      <div style=\"color:rgb(51,51,51);\">
                        <font class="next_element_with_text_4">Some text</font>
                      </div>
                    </div>
                    John
                  </div>
                </div>
                <br>
                <span class="remove_if_empty_4"><img src="foo.gif"></span>
                <span class="remove_if_empty_1">&nbsp;</span>
              </div>
            </blockquote>
          </div>
        </body>
      </html>
    EML
  end

  let(:remaining_remove_self_and_preceding) do
    <<~EML
      <html>
        <head></head>
        <body>
          <div>
            <blockquote>
              <div class="remove_if_empty_2 html_up_to_1">
                <div class="remove_surrounding_containers_1">
                  <span class="next_element_with_text_2 remove_self_and_preceding_2">&gt;</span><br>
                  <br><br>
                  <div class="next_element_with_text_3">
                    <h1 style=\"font-size:24px;" class="remove_if_empty_3">
                      <font>HTML email</font>
                    </h1>
                    <div>
                      <div style=\"color:rgb(51,51,51);\">
                        <font class="next_element_with_text_4">Some text</font>
                      </div>
                    </div>
                    John
                  </div>
                </div>
                <br>
                <span class="remove_if_empty_4"><img src="foo.gif"></span>
                <span class="remove_if_empty_1">&nbsp;</span>
              </div>
            </blockquote>
          </div>
        </body>
      </html>
    EML
  end

  let(:doc) { Nokogiri::HTML5.parse(forward) }

  describe ".next_element_with_text" do
    let(:element)      { doc.at_css('.next_element_with_text_1') }
    let(:next_element) { Zendesk::InboundMail::HTMLDocument.next_element_with_text(element) }

    before { element.text.strip.must_equal "To:" }

    it { next_element.matches?(".next_element_with_text_2") }

    it { assert Zendesk::InboundMail::HTMLDocument.next_element_with_text(next_element).matches?(".next_element_with_text_3") }

    it { Zendesk::InboundMail::HTMLDocument.next_element_with_text(doc.at_css('.next_element_with_text_4')).text.strip.must_equal "John" }
  end

  describe ".remove_self_and_preceding_elements" do
    let(:element) { doc.at_css('.remove_self_and_preceding_1') }

    it "removes preceding elements and self" do
      Zendesk::InboundMail::HTMLDocument.remove_self_and_preceding_elements(element)
      doc.to_html.html_must_equal remaining_remove_self_and_preceding
    end

    it "does not remove non-empty content" do
      container = Zendesk::InboundMail::HTMLDocument.remove_self_and_preceding_elements(element)
      assert container.matches?(".remove_self_and_preceding_2")
    end

    it { Zendesk::InboundMail::HTMLDocument.remove_self_and_preceding_elements(nil).must_be_nil }
  end

  describe ".remove_if_empty" do
    it "removes element with non breaking spaces" do
      Zendesk::InboundMail::HTMLDocument.remove_if_empty(doc.at_css(".remove_if_empty_1"))
      doc.css(".remove_if_empty_1").must_be_empty
    end

    it "returns parent" do
      parent = Zendesk::InboundMail::HTMLDocument.remove_if_empty(doc.at_css(".remove_if_empty_1"))
      assert parent.matches?(".remove_if_empty_2")
    end

    it "does not remove non-empty element" do
      element = doc.at_css(".remove_if_empty_3")

      same_element = Zendesk::InboundMail::HTMLDocument.remove_if_empty(element)

      same_element.must_equal element
      assert doc.at_css(".remove_if_empty_3").present?
    end

    it "does not remove element that contains only an image" do
      element = doc.at_css(".remove_if_empty_4")

      same_element = Zendesk::InboundMail::HTMLDocument.remove_if_empty(element)

      same_element.must_equal element
      assert doc.at_css(".remove_if_empty_4").present?
    end

    it "does not remove the body" do
      Zendesk::InboundMail::HTMLDocument.remove_if_empty(doc.at_css("body")).must_be_nil
      assert doc.at_css("body").present?
    end

    it { Zendesk::InboundMail::HTMLDocument.remove_if_empty(nil).must_be_nil }
  end

  describe ".remove_surrounding_containers" do
    let(:remaining_remove_surrounding_containers) do
      <<~EML
        <html>
          <head></head>
          <body>
            <div class="remove_surrounding_containers_1">
              <span class="next_element_with_text_2 remove_self_and_preceding_2">&gt;</span><br>
              <br><br>
              <div class="next_element_with_text_3">
                <h1 style=\"font-size:24px;" class="remove_if_empty_3">
                  <font>HTML email</font>
                </h1>
                <div>
                  <div style=\"color:rgb(51,51,51);\">
                    <font class="next_element_with_text_4">Some text</font>
                  </div>
                </div>
                John
              </div>
            </div>
            <br>
            <span class="remove_if_empty_4"><img src="foo.gif"></span>
            <span class="remove_if_empty_1">&nbsp;</span>
          </body>
        </html>
      EML
    end

    let(:doc) { Nokogiri::HTML5.parse(remaining_remove_self_and_preceding) }

    it "removes div, p, and blockquote tags" do
      Zendesk::InboundMail::HTMLDocument.remove_surrounding_containers(doc.at_css(".remove_surrounding_containers_1").parent)
      doc.to_html.html_must_equal remaining_remove_surrounding_containers
    end

    it { Zendesk::InboundMail::HTMLDocument.remove_surrounding_containers(nil).must_be_nil }
  end

  describe ".ancestor_with_equivalent_text" do
    let(:ancestor_with_equivalent_text) do
      <<~EML
        <html>
          <head></head>
          <body>
            <div>
              <br><br>
              <div>
                <span class="ancestor_with_equivalent_text_5">Foo</span>
                <h1 style=\"font-size:24px;" class="ancestor_with_equivalent_text_2">
                  <font class="ancestor_with_equivalent_text_1">HTML email</font>
                </h1>
                <div class="ancestor_with_equivalent_text_4">
                  <div style=\"color:rgb(51,51,51);\">
                    <span><font class="ancestor_with_equivalent_text_3">Some text</font></span>
                  </div>
                </div>
                John
              </div>
            </div>
          </body>
        </html>
      EML
    end

    let(:doc) { Nokogiri::HTML5.parse(ancestor_with_equivalent_text) }

    it { assert Zendesk::InboundMail::HTMLDocument.ancestor_with_equivalent_text(doc.at_css(".ancestor_with_equivalent_text_1")).matches? ".ancestor_with_equivalent_text_2" }

    it { assert Zendesk::InboundMail::HTMLDocument.ancestor_with_equivalent_text(doc.at_css(".ancestor_with_equivalent_text_3")).matches? ".ancestor_with_equivalent_text_4" }

    it { assert Zendesk::InboundMail::HTMLDocument.ancestor_with_equivalent_text(doc.at_css(".ancestor_with_equivalent_text_5")).matches? ".ancestor_with_equivalent_text_5" }

    it { assert Zendesk::InboundMail::HTMLDocument.ancestor_with_equivalent_text(nil).must_be_nil }
  end

  describe ".remove_leading_brs" do
    let(:with_leading_brs) do
      <<~EML
        <html>
          <head></head>
          <body>
            <br>
            <div>
              <br><br>
              <span><img class="remove_leading_brs_2" src="foo.gif"></span><br>
              <br><br>
              <div>
                <h1 style=\"font-size:24px;" class="remove_if_empty_3">
                  <font>HTML email</font>
                </h1>
                John
              </div>
            </div>
            <br>
            <span class="remove_if_empty_1">&nbsp;</span>
          </body>
        </html>
      EML
    end

    let(:without_leading_brs) do
      <<~EML
        <html>
          <head></head>
          <body>
            <div>
              <span><img class="remove_leading_brs_2" src="foo.gif"></span><br>
              <br><br>
              <div>
                <h1 style=\"font-size:24px;" class="remove_if_empty_3">
                  <font>HTML email</font>
                </h1>
                John
              </div>
            </div>
            <br>
            <span class="remove_if_empty_1">&nbsp;</span>
          </body>
        </html>
      EML
    end

    let(:doc) { Nokogiri::HTML5.parse(with_leading_brs) }

    before { Zendesk::InboundMail::HTMLDocument.remove_leading_brs(doc) }

    it { doc.to_html.html_must_equal without_leading_brs }

    it { assert doc.at_css("img.remove_leading_brs_2").present? }
  end

  describe ".html_up_to" do
    let(:html_up_to) do
      <<~EML
        <html>
          <head></head>
          <body>
            <div>This customer needs help.
              <blockquote>
                <div class="remove_if_empty_2 html_up_to_1">
                  <br>
                  <div class="remove_surrounding_containers_1">
                  </div>
                </div>
              </blockquote>
            </div>
          </body>
        </html>
      EML
    end

    it "keeps only content up to target" do
      beginning_of_header = /---------- Forwarded message ----------/
      Zendesk::InboundMail::HTMLDocument.html_up_to(doc.at_css("body"), doc.search("//text()").detect { |el| el.text.match(beginning_of_header) })
      doc.to_html.html_must_equal html_up_to
    end
  end

  describe ".content_present?" do
    describe "when there is content" do
      [
        "some content",
        "<div>some content</div>",
        "<style type='text/css' style='/* display:none; */'> P {margin-top:0;margin-bottom:0;} </style><div><br>Hello World</div><div><br></div>"
      ].each do |content|
        it "returns true for: #{content}" do
          assert Zendesk::InboundMail::HTMLDocument.content_present?(content)
        end
      end
    end

    describe "when there is no content" do
      [
        "<div></div>",
        "<div><br></div><div><br></div>",
        "<div class=\"WordSection1\">\n<p class=\"MsoNormal\"><span style=\"font-size:11.0pt;font-family:&quot;Calibri&quot;,sans-serif;color:#1F497D\"><o:p>&nbsp;</o:p></span></p>\n<p class=\"MsoNormal\"><span style=\"font-size:11.0pt;font-family:&quot;Calibri&quot;,sans-serif;color:#1F497D\"><o:p>&nbsp;</o:p></span></p>\n\n\n\n\n</div>\n\n",
        "<p dir=\"ltr\"></p>\n<br><br><div class=\"device_aol_et_org_dt_dd_quote\"></div><hr style=\"border:0;height:1px;color:#999;background-color:#999;width:100%;margin:0 0 9px 0;padding:0;\">",
        "<style type='text/css' style='/* display:none; */'> P {margin-top:0;margin-bottom:0;} </style><div><br></div><div><br></div>"
      ].each do |content|
        it "returns false for: #{content}" do
          refute Zendesk::InboundMail::HTMLDocument.content_present?(content)
        end
      end
    end
  end
end
