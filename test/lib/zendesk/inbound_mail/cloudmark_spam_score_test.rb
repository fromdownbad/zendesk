require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::InboundMail::CloudmarkSpamScore do
  before { mail.headers['X-CMAE-Score'] = spam_score }

  let(:spam_score) { '70.0' }
  let(:account) { accounts(:minimum) }
  let(:mail) do
    inbound_message = Zendesk::Mail::InboundMessage.new
    inbound_message.account = account
    inbound_message
  end

  subject { Zendesk::InboundMail::CloudmarkSpamScore.new(mail) }

  describe 'definition' do
    subject { Zendesk::InboundMail::CloudmarkSpamScore }

    it { assert_equal 'X-CMAE-Score', subject::HEADER_KEY }
    it { assert_equal 90.0, subject::PROBABLE_THRESHOLD }
    it { assert_equal 99.0, subject::OBVIOUS_THRESHOLD }
    it { assert_equal 95.0, subject::OBVIOUS_THRESHOLD_V2 }
  end

  describe 'spam thresholds' do
    describe_with_and_without_arturo_enabled(:email_update_cloudmark_obvious_spam_threshold) do
      describe 'with a spam score of +99' do
        let(:spam_score) { '99.0' }

        # It's spam
        it { assert subject.probably_spam? }
        it { assert subject.obviously_spam? }
      end

      describe 'with a spam score of 90' do
        let(:spam_score) { '90.0' }

        # It's probably spam
        it { assert subject.probably_spam? }
        it { refute subject.obviously_spam? }
      end

      describe 'with a spam score of 85' do
        let(:spam_score) { '85.0' }

        # It's not spam
        it { refute subject.probably_spam? }
        it { refute subject.obviously_spam? }
      end
    end

    describe_with_arturo_disabled(:email_update_cloudmark_obvious_spam_threshold) do
      describe 'with a spam score of 95' do
        let(:spam_score) { '95.0' }

        # It's probably spam
        it { assert subject.probably_spam? }
        it { refute subject.obviously_spam? }
      end
    end

    describe_with_arturo_enabled(:email_update_cloudmark_obvious_spam_threshold) do
      describe 'with a spam score of 95' do
        let(:spam_score) { '95.0' }

        # It's spam
        it { assert subject.probably_spam? }
        it { assert subject.obviously_spam? }
      end
    end
  end

  describe '#threshold' do
    describe_with_arturo_disabled(:email_update_cloudmark_obvious_spam_threshold) do
      it { assert_equal 99.0, subject.threshold }
    end

    describe_with_arturo_enabled(:email_update_cloudmark_obvious_spam_threshold) do
      it 'logs a message about using the new threshold ' do
        mail.logger.stubs(:info)
        mail.logger.expects(:info).with('CloudmarkSpamScore using OBVIOUS_THRESHOLD_V2 to determine obviously_spam')
        subject.threshold
      end

      it { assert_equal 95.0, subject.threshold }
    end
  end

  describe '#value' do
    it 'provides the value as a float' do
      assert_equal 70.0, subject.value
    end

    describe 'when the header is empty' do
      before { mail.headers.delete('X-CMAE-Score') }
      it { assert_nil subject.value }
    end

    describe 'when the value is zero' do
      before { mail.headers['X-CMAE-Score'] = '0' }
      it { assert_equal 0, subject.value }
    end
  end
end
