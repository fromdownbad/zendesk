require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::InboundMail::SenderAuthentication::Matrix do
  include MailTestHelper

  subject { Zendesk::InboundMail::SenderAuthentication::Matrix.new(header_reader, sender_domain, account) }
  let(:account) { accounts(:minimum) }
  let(:sender_domain) { 'example.com' }

  let(:headers) { { } }

  let(:json) do
    {
      'body' => 'Hello world',
      'errors' => { },
      'headers' => headers,
      'id' => 'a',
      'parts' => []
    }.with_indifferent_access
  end

  let(:mail) do
    mail = Zendesk::Mail::Serializers::InboundMessageSerializer.load(json)
    mail.account = Account.new
    mail
  end

  describe '#matrix' do
    let(:combination) { spf_values.product(dkim_values, alignment_values) }
    let(:rules) { subject.matrix[rule].keys }

    describe 'authentication_results' do
      let(:header_reader) { Zendesk::InboundMail::SenderAuthentication::HeaderReaders::AuthenticationResults.new(mail) }
      let(:spf_values) { %i[spf_none spf_pass spf_fail] }
      let(:dkim_values) { %i[dkim_none dkim_pass dkim_fail] }
      let(:alignment_values) { %i[aligned unaligned] }

      it 'loads YAML into memory' do
        YAML.expects(:load_file).with(Rails.root.join('config/sender_authentication/authentication_results.yml')).once.returns({})
        2.times { subject.matrix }
      end

      describe 'default rules' do
        let(:rule) { 'default' }

        it 'has all permutations of spf, dkim and alignment values' do
          assert rules == combination
        end
      end

      describe 'strict rules' do
        let(:rule) { 'strict' }

        it 'has all permutations of spf, dkim and alignment values' do
          assert rules == combination
        end
      end
    end

    describe 'missing_authentication' do
      let(:header_reader) { Zendesk::InboundMail::SenderAuthentication::HeaderReaders::MissingAuthentication.new(mail) }
      let(:spf_values) { %i[spf_none] }
      let(:dkim_values) { %i[dkim_none] }
      let(:alignment_values) { %i[aligned unaligned] }

      it 'loads YAML into memory' do
        YAML.expects(:load_file).with(Rails.root.join('config/sender_authentication/missing_authentication.yml')).once.returns({})
        2.times { subject.matrix }
      end

      describe 'default rules' do
        let(:rule) { 'default' }

        it 'has all permutations of spf, dkim and alignment values' do
          assert rules == combination
        end
      end
    end

    describe 'x_rspamd_authentication_results' do
      let(:header_reader) { Zendesk::InboundMail::SenderAuthentication::HeaderReaders::XRspamdAuthenticationResults.new(mail) }
      let(:spf_values) { %i[spf_none spf_forwarded spf_pass spf_fail] }
      let(:dkim_values) { %i[dkim_none dkim_pass dkim_fail dkim_forwarded] }
      let(:alignment_values) { %i[aligned unaligned] }

      it 'loads YAML into memory' do
        YAML.expects(:load_file).with(Rails.root.join('config/sender_authentication/x_rspamd_authentication_results.yml')).once.returns({})
        2.times { subject.matrix }
      end

      describe 'default rules' do
        let(:rule) { 'default' }

        it 'has all permutations of spf, dkim and alignment values' do
          assert rules == combination
        end
      end

      describe 'strict rules' do
        let(:rule) { 'strict' }

        it 'has all permutations of spf, dkim and alignment values' do
          assert rules == combination
        end
      end
    end
  end

  describe '#lookup' do
    let(:header_reader) { Zendesk::InboundMail::SenderAuthentication::HeaderReaders::XRspamdAuthenticationResults.new(mail) }
    let(:account) { accounts(:minimum) }

    def self.disreputable_senders
      Zendesk::InboundMail::SenderAuthentication::Matrix.senders.select do |_, v|
        v['x_rspamd_authentication_results'] == 'disreputable'
      end.keys
    end

    before do
      header_reader.stubs(spf_status: :spf_none, dkim_status: :dkim_none, spf_alignment: :aligned)
    end

    it 'returns a Result' do
      assert subject.lookup.is_a?(Zendesk::InboundMail::SenderAuthentication::Result)
    end

    it 'passes' do
      assert subject.lookup.action == :pass
    end

    it 'uses default sender rules' do
      assert subject.lookup.sender_rule == :default
    end

    %w[
      subdomain.zendesk.com
      zendesk.com
      subdomain.zendesk-staging.com
      zendesk-staging.com
    ].each do |host|
      describe 'for a sending domain that is subject to strict policy' do
        let(:sender_domain) { host }

        it 'rejects' do
          assert subject.lookup.action == :reject
        end

        it 'uses strict sender rules' do
          assert subject.lookup.sender_rule == :strict
        end
      end
    end

    disreputable_senders.each do |host|
      describe 'for a sending domain that is subject to disreputable policy' do
        let(:sender_domain) { host }

        it 'suspends' do
          assert subject.lookup.action == :suspend
        end

        it 'uses disreputable sender rules' do
          assert subject.lookup.sender_rule == :disreputable
        end

        describe_with_arturo_disabled :email_sender_auth_disreputable_senders do
          it 'passes' do
            assert subject.lookup.action == :pass
          end

          it 'uses default sender rules' do
            assert subject.lookup.sender_rule == :default
          end
        end
      end
    end
  end
end
