require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::InboundMail::SenderAuthentication::HeaderReaderFactory do
  describe '#create' do
    subject { Zendesk::InboundMail::SenderAuthentication::HeaderReaderFactory.create(mail) }

    let(:mail) do
      Zendesk::Mail::Serializers::InboundMessageSerializer.load(json).tap do |mail|
        mail.account = Account.new
      end
    end

    let(:headers) do
      {
        'from' => [address('john.doe@example.com')],
        'return-path' => ['john.doe@example.com']
      }
    end

    let(:json) do
      {
        'body' => 'blah blah blah',
        'errors' => { },
        'headers' => headers,
        'id' => 'a',
        'parts' => []
      }.with_indifferent_access
    end

    describe 'with X-RSpamd-Authentication-Results headers' do
      let(:headers) do
        {
          'x-rspamd-authentication-results' => ['No, score=2.2 required=5.0 tests=RDNS_NONE autolearn=disabled version=3.3.2']
        }
      end

      it { assert subject.is_a?(Zendesk::InboundMail::SenderAuthentication::HeaderReaders::XRspamdAuthenticationResults) }
    end

    describe 'with Authentication-Results header' do
      describe 'with X-Zendesk-Source headers' do
        let(:headers) do
          {
            'Authentication-Results' => ['mx.google.com; spf=softfail (google.com: domain of jimmy@example.com does not designate 198.21.1.220 as permitted sender) smtp.mailfrom=jimmy@example.com; dkim=pass header.i=@example.com; dmarc=pass (p=NONE dis=NONE) header.from=example.com'],
            'X-Zendesk-Source' => ['1']
          }
        end

        it { assert subject.is_a?(Zendesk::InboundMail::SenderAuthentication::HeaderReaders::AuthenticationResults) }
      end

      describe 'without X-Zendesk-Source headers' do
        let(:headers) do
          {
            'Authentication-Results' => ['mx.google.com; spf=softfail (google.com: domain of jimmy@example.com does not designate 198.21.1.220 as permitted sender) smtp.mailfrom=jimmy@example.com; dkim=pass header.i=@example.com; dmarc=pass (p=NONE dis=NONE) header.from=example.com']
          }
        end

        it { assert subject.is_a?(Zendesk::InboundMail::SenderAuthentication::HeaderReaders::MissingAuthentication) }
      end
    end

    describe 'with no known sender auth headers' do
      let(:headers) { {} }

      it { assert subject.is_a?(Zendesk::InboundMail::SenderAuthentication::HeaderReaders::MissingAuthentication) }
    end
  end
end
