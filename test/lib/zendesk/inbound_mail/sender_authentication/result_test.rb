require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::SenderAuthentication::Result do
  let(:result) do
    result = Zendesk::InboundMail::SenderAuthentication::Result.new([:pass, :flag], [:spf_pass, :dkim_pass, :aligned], :default)
    result.origin = :from_zendesk
    result
  end

  it "will return the original query as an array" do
    assert result.to_a == [:from_zendesk, :spf_pass, :dkim_pass, :aligned]
  end

  it "will store the spf_status" do
    assert result.spf_status == :spf_pass
  end

  it "will store the dkim_status" do
    assert result.dkim_status == :dkim_pass
  end

  it "will store the spf_alignment" do
    assert result.spf_alignment == :aligned
  end

  it "will store the action" do
    assert result.action == :pass
  end

  it "will store the sender rule used" do
    assert result.sender_rule == :default
  end

  it "can query if it passed sender authentication" do
    assert result.pass?
  end

  it "can query if it should be suspended" do
    refute result.suspend?
  end

  it "can query if it should be rejected" do
    refute result.reject?
  end

  it "can query if it should be flagged" do
    assert result.flag?
  end

  it "can query if it should be audited" do
    refute result.audit?
  end
end
