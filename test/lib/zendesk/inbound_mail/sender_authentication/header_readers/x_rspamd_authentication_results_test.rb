require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::InboundMail::SenderAuthentication::HeaderReaders::XRspamdAuthenticationResults do
  include MailTestHelper

  let(:mail) do
    mail = Zendesk::Mail::Serializers::InboundMessageSerializer.load(json)
    mail.account = Account.new
    mail
  end

  let(:x_rspamd_authentication_results_reader) do
    Zendesk::InboundMail::SenderAuthentication::HeaderReaders::XRspamdAuthenticationResults.new(mail)
  end

  let(:headers) { { } }

  let(:json) do
    {
      'body' => 'This is a good email body',
      'errors' => { },
      'headers' => headers,
      'id' => 'good-id-from-good-people',
      'parts' => []
    }.with_indifferent_access
  end

  describe '#dkim_status' do
    before { headers['x-rspamd-authentication-results'] = authentication_results }
    subject { x_rspamd_authentication_results_reader.dkim_status }

    describe 'with no dkim result, or if it\'s none' do
      [
        'in1.pod1.usw1.zdsys.com; spf=pass (in1.pod1.usw1.zdsys.com: a reason) smtp.mailfrom=example@host.example.com',
        'in1.pod1.usw1.zdsys.com; dkim=none smtp.mailfrom=example@host.example.com'
      ].each do |result|
        let(:authentication_results) { [result] }
        it { assert_equal :dkim_none, subject }
      end
    end

    describe 'with passes' do
      let(:authentication_results) { ['in1.pod1.usw1.zdsys.com; dkim=pass smtp.mailfrom=example@host.example.com'] }
      it { assert_equal :dkim_pass, subject }
    end

    describe 'with fails' do
      let(:authentication_results) { ['in1.pod1.usw1.zdsys.com; dkim=fail smtp.mailfrom=example@host.example.com'] }
      it { assert_equal :dkim_fail, subject }

      describe 'when there are no recipient headers' do
        before do
          headers['to'] = nil
          headers['cc'] = nil
          headers['delivered-to'] = [address('support@example.zendesk.com')]
        end

        it { assert_equal :dkim_fail, subject }

        describe_with_arturo_disabled :email_sender_auth_no_recipient_fix do
          it { assert_equal :dkim_forwarded, subject }
        end
      end

      describe 'when Delivered-To does not match any recipient address' do
        before do
          headers['to'] = [address('support@example.com')]
          headers['cc'] = [address('john.doe@example.com'), address('jane.doe@example.com')]
          headers['delivered-to'] = [address('support@example.zendesk.com')]
        end

        it { assert_equal :dkim_forwarded, subject }

        describe 'and there are missing recipient headers' do
          before do
            headers['to'] = nil
          end

          it { assert_equal :dkim_forwarded, subject }
        end

        describe 'and there are multiple To addresses' do
          before do
            headers['to'] = [address('support@example.com'), address('support2@example.com')]
          end

          it { assert_equal :dkim_forwarded, subject }
        end

        describe 'and some Delivered-To addresses match other recipient addresses' do
          before do
            headers['to'] = [address('support@example.com'), address('customercare@example.com')]
            headers['delivered-to'] = [address('support@example.zendesk.com'), address('customercare@example.com')]
          end

          it { assert_equal :dkim_forwarded, subject }
        end
      end
    end

    describe 'with temperrors or permerrors' do
      [
        'in1.pod1.usw1.zdsys.com; dkim=temperror smtp.mailfrom=example@host.example.com',
        'in1.pod1.usw1.zdsys.com; dkim=permerror smtp.mailfrom=example@host.example.com'
      ].each do |result|
        let(:authentication_results) { [result] }
        it { assert_equal :dkim_none, subject }
      end
    end
  end

  describe '#spf_status' do
    before { headers['x-rspamd-authentication-results'] = authentication_results }
    subject { x_rspamd_authentication_results_reader.spf_status }

    describe 'with no spf results, or spf is none, neutral or softfail' do
      [
        'in1.pod1.usw1.zdsys.com; smtp.mailfrom=example@host.example.com',
        'in1.pod1.usw1.zdsys.com; spf=none smtp.mailfrom=example@host.example.com',
        'in1.pod1.usw1.zdsys.com; spf=neutral smtp.mailfrom=example@host.example.com',
        'in1.pod1.usw1.zdsys.com; spf=softfail smtp.mailfrom=example@host.example.com'
      ].each do |result|
        let(:authentication_results) { [result] }
        it { assert_equal subject, :spf_none }
      end
    end

    describe 'with temperrors' do
      let(:authentication_results) { ['in1.pod1.usw1.zdsys.com; spf=temperror (in1.pod1.usw1.zdsys.com: temporary error) smtp.mailfrom=example@host.example.com'] }
      it { assert_equal :spf_none, subject }

      it 'increments metric' do
        mail.statsd_client.expects(:increment).with(:spf_error, tags: ['error:temperror'])
        subject
      end
    end

    describe 'with permerrors' do
      let(:authentication_results) { ['in1.pod1.usw1.zdsys.com; spf=permerror(in1.pod1.usw1.zdsys.com: permanent error) smtp.mailfrom=example@host.example.com'] }
      it { assert_equal :spf_none, subject }

      it 'increments metric' do
        mail.statsd_client.expects(:increment).with(:spf_error, tags: ['error:permerror'])
        subject
      end
    end

    describe 'with passes' do
      let(:authentication_results) { ['in1.pod1.usw1.zdsys.com; spf=pass (in1.pod1.usw1.zdsys.com: permitted) smtp.mailfrom=example@host.example.com'] }
      it { assert_equal :spf_pass, subject }
    end

    describe 'with fails' do
      let(:authentication_results) { ['in1.pod1.usw1.zdsys.com; spf=fail (in1.pod1.usw1.zdsys.com: SPF failed) smtp.mailfrom=example@host.example.com'] }
      it { assert_equal :spf_fail, subject }

      describe 'when there are no recipient headers' do
        before do
          headers['to'] = nil
          headers['cc'] = nil
          headers['delivered-to'] = [address('support@example.zendesk.com')]
        end

        it { assert_equal :spf_fail, subject }

        describe_with_arturo_disabled :email_sender_auth_no_recipient_fix do
          it { assert_equal :spf_forwarded, subject }
        end
      end

      describe 'when delivered-to does not match recipient address' do
        before do
          headers['to'] = [address('support@example.com')]
          headers['cc'] = [address('john.doe@example.com'), address('jane.doe@example.com')]
          headers['delivered-to'] = [address('support@example.zendesk.com')]
        end

        it { assert_equal :spf_forwarded, subject }

        describe 'and there are missing recipient headers' do
          before do
            headers['to'] = nil
          end

          it { assert_equal :spf_forwarded, subject }
        end

        describe 'and there are multiple To addresses' do
          before do
            headers['to'] = [address('support@example.com'), address('support2@example.com')]
          end

          it { assert_equal :spf_forwarded, subject }
        end

        describe 'and some Delivered-To addresses match other recipient addresses' do
          before do
            headers['to'] = [address('support@example.com'), address('customercare@example.com')]
            headers['delivered-to'] = [address('support@example.zendesk.com'), address('customercare@example.com')]
          end

          it { assert_equal :spf_forwarded, subject }
        end
      end
    end

    describe '#skip?' do
      let(:authentication_results) { ['in1.pod1.usw1.zdsys.com'] }
      subject { x_rspamd_authentication_results_reader.skip? }

      describe 'when REPLY symbol is present in X-Rspamd-Status' do
        before do
          headers['x-rspamd-status'] = ['No, score=-4.00; ZENDESK_CUSTOM(0.00)[]; REPLY(-4.00)[]']
        end

        it { assert subject }
      end

      describe 'when REPLY symbol is not present in X-Rspamd-Status' do
        before do
          headers['x-rspamd-status'] = ['No, score=-4.00; FAKE_REPLY(1.00)[]; ZENDESK_CUSTOM(0.00)[]; TO_DN_NONE(0.00)[]']
        end

        it { refute subject }
      end
    end

    describe '#dmarc_fail_and_reject?' do
      subject { x_rspamd_authentication_results_reader.dmarc_fail_and_reject? }

      describe 'when no DMARC results are present in the X-Rspamd-Authentication-Results' do
        let(:authentication_results) { ['in1.pod1.usw1.zdsys.com; dkim=pass smtp.mailfrom=example@host.example.com'] }

        it { refute subject }
      end

      describe 'when dmarc=none is present in the X-Rspamd-Authentication-Results' do
        let(:authentication_results) do
          ['in1.pod1.usw1.zdsys.com; dmarc=none; smtp.mailfrom=example@host.example.com']
        end

        it { refute subject }
      end

      describe 'when dmarc=fail is present in the X-Rspamd-Authentication-Results' do
        let(:dmarc_policy) { "none" }

        describe 'when there is a semicolon after the parenthesis at the end of the policy' do
          let(:authentication_results) do
            ["in1.pod1.usw1.zdsys.com; dmarc=fail reason=\"No valid SPF, No valid DKIM\" header.from=example.com (policy=#{dmarc_policy}); smtp.mailfrom=example@host.example.com"]
          end

          describe 'and the policy is none' do
            it { refute subject }
          end

          describe 'and the policy is reject' do
            let(:dmarc_policy) { "reject" }

            it { assert subject }
          end

          describe 'and the policy is quarantine' do
            let(:dmarc_policy) { "quarantine" }

            it { assert subject }
          end
        end

        describe 'when there is no semicolon after the parenthesis at the end of the policy' do
          let(:authentication_results) do
            ["in1.pod1.usw1.zdsys.com; dmarc=fail reason=\"No valid SPF, No valid DKIM\" header.from=example.com (policy=#{dmarc_policy}) smtp.mailfrom=example@host.example.com"]
          end

          describe_with_arturo_enabled :email_mtc_sender_auth_dmarc_v2 do
            describe 'and the policy is none' do
              it { refute subject }
            end

            describe 'and the policy is reject' do
              let(:dmarc_policy) { 'reject' }

              it { assert subject }
            end

            describe 'and the policy is quarantine' do
              let(:dmarc_policy) { 'quarantine' }

              it { assert subject }
            end
          end

          describe_with_arturo_disabled :email_mtc_sender_auth_dmarc_v2 do
            describe 'and the policy is none' do
              it { refute subject }
            end

            describe 'and the policy is reject' do
              let(:dmarc_policy) { 'reject' }

              it { refute subject }
            end

            describe 'and the policy is quarantine' do
              let(:dmarc_policy) { 'quarantine' }

              it { refute subject }
            end
          end
        end
      end
    end

    describe '#dmarc' do
      subject { x_rspamd_authentication_results_reader.dmarc }

      describe 'when no DMARC results are present in the X-Rspamd-Authentication-Results' do
        let(:authentication_results) { ['in1.pod1.usw1.zdsys.com; dkim=pass smtp.mailfrom=example@host.example.com'] }

        it { assert_equal :dmarc_missing, subject }
      end

      describe 'when the DMARC value is pass' do
        let(:authentication_results) do
          ["in1.pod1.usw1.zdsys.com; dmarc=pass (policy=none); smtp.mailfrom=example@host.example.com"]
        end

        it { assert_equal :dmarc_pass, subject }
      end

      describe 'when the DMARC value is fail' do
        let(:authentication_results) do
          ["in1.pod1.usw1.zdsys.com; dmarc=fail reason=\"No valid SPF, No valid DKIM\" header.from=example.com (policy=quarantine); smtp.mailfrom=example@host.example.com"]
        end

        it { assert_equal :dmarc_fail, subject }
      end
    end

    describe '#dmarc_policy' do
      subject { x_rspamd_authentication_results_reader.dmarc_policy }

      describe 'when no DMARC results are present in the X-Rspamd-Authentication-Results' do
        let(:authentication_results) { ['in1.pod1.usw1.zdsys.com; dkim=pass smtp.mailfrom=example@host.example.com'] }

        it { assert_equal :dmarc_policy_missing, subject }
      end

      describe 'when the DMARC policy is to reject' do
        let(:authentication_results) do
          ["in1.pod1.usw1.zdsys.com; dmarc=fail reason=\"No valid SPF, No valid DKIM\" header.from=example.com (policy=reject); smtp.mailfrom=example@host.example.com"]
        end

        it { assert_equal :dmarc_policy_reject, subject }
      end

      describe 'when the DMARC policy is to quarantine' do
        let(:authentication_results) do
          ["in1.pod1.usw1.zdsys.com; dmarc=fail reason=\"No valid SPF, No valid DKIM\" header.from=example.com (policy=quarantine); smtp.mailfrom=example@host.example.com"]
        end

        it { assert_equal :dmarc_policy_quarantine, subject }
      end

      describe 'when the DMARC policy is none' do
        let(:authentication_results) do
          ["in1.pod1.usw1.zdsys.com; dmarc=fail reason=\"No valid SPF, No valid DKIM\" header.from=example.com (policy=none); smtp.mailfrom=example@host.example.com"]
        end

        it { assert_equal :dmarc_policy_none, subject }
      end
    end
  end
end
