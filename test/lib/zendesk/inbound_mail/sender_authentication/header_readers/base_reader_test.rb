require_relative '../../../../../support/test_helper'

SingleCov.covered! uncovered: 4

describe Zendesk::InboundMail::SenderAuthentication::HeaderReaders::BaseReader do
  include MailTestHelper

  let(:base_authentication_reader) do
    mail = Zendesk::Mail::Serializers::InboundMessageSerializer.load(json)
    mail.account = Account.new
    Zendesk::InboundMail::SenderAuthentication::HeaderReaders::BaseReader.new(mail)
  end

  let(:headers) { { } }

  let(:json) do
    {
      'body' => 'blah blah blah',
      'errors' => { },
      'headers' => headers,
      'id' => 'a',
      'parts' => []
    }.with_indifferent_access
  end

  def dkim_signature(domain)
    "v=1; a=rsa-sha256; c=relaxed/relaxed; d=#{domain}; s=selector; h=sender:mime-version:in-reply-to:references:from:date:message-id :subject:to:x-original-sender:x-original-authentication-results :precedence:mailing-list:list-id:list-post:list-help:list-archive :list-unsubscribe; bh=A0jxneG1pMwJBwGJqQ/9Tnb9xDr/RDjlFscQ+hcfnQU=; b=a3l0QePmFTlTVb8LXvUQRM/vXmOhNvTxIJjqx7vTNFH/b9idMjBzjfdNNd+LDnc3s1 pTlhq3SvWNWZ6OuY+ze8FRcEKSRwkiurbSvjC7Och7K6UQi6kEIauiuBWfPYfFM/v8hy i+edhMY4EwjIqyHmmGPXUHwxYkLm8ZLh55Mxs="
  end

  describe '#spf_alignment' do
    subject { base_authentication_reader.spf_alignment }

    describe 'when envelope-from is present' do
      before { headers['Envelope-From'] = ['jane.doe@example.com'] }

      describe 'and from is present' do
        it 'is aligned when domains match exactly' do
          headers['From'] = [address('jane.doe@example.com')]
          assert subject == :aligned
        end

        it 'is not aligned when domains mismatch' do
          headers['From'] = [address('jane.doe@another-example.com')]
          assert subject == :unaligned
        end

        it 'is not aligned for subdomains' do
          headers['From'] = [address('jane.doe@subdomain.example.com')]
          assert subject == :unaligned
        end
      end

      describe 'and from is not present' do
        it 'is aligned when reply-to domain matches envelope-from domain exactly' do
          headers['Reply-To'] = [address('jane.doe@example.com')]
          assert subject == :aligned
        end

        it 'is not aligned when reply-to domain does not match envelope-from domain' do
          headers['Reply-To'] = [address('jane.doe@another-example.com')]
          assert subject == :unaligned
        end

        it 'is not aligned for subdomains' do
          headers['Reply-To'] = [address('jane.doe@subdomain.example.com')]
          assert subject == :unaligned
        end
      end
    end

    describe 'when envelope-from is not present' do
      before { headers['Return-Path'] = ['jane.doe@example.com'] }

      describe 'and from is present' do
        it 'is aligned when from domain matches return-path domain exactly' do
          headers['From'] = [address('jane.doe@example.com')]
          assert subject == :aligned
        end

        it 'is not aligned when from does not match return-path domain' do
          headers['From'] = [address('jane.doe@another-example.com')]
          assert subject == :unaligned
        end

        it 'is not aligned for subdomains' do
          headers['From'] = [address('jane.doe@subdomain.example.com')]
          assert subject == :unaligned
        end
      end

      describe 'and from is not present' do
        it 'is aligned when reply-to domain matches return-path exactly' do
          headers['Reply-To'] = [address('jane.doe@example.com')]
          assert subject == :aligned
        end

        it 'is not aligned when reply-to domain does not match return-path domain' do
          headers['Reply-To'] = [address('jane.doe@another-example.com')]
          assert subject == :unaligned
        end

        it 'is not aligned for subdomains' do
          headers['Reply-To'] = [address('jane.doe@subdomain.example.com')]
          assert subject == :unaligned
        end
      end
    end
  end

  describe '#dkim_alignment' do
    before { headers['From'] = [address('john.doe@example.com')] }

    subject { base_authentication_reader.dkim_alignment }

    describe 'when there are no dkim signatures' do
      it 'will return none' do
        assert subject == :dkim_none
      end
    end

    describe 'when there is one dkim signature' do
      describe 'and the signature domain matches the From header domain' do
        before { headers['dkim-signature'] = [dkim_signature('example.com')] }

        it { assert subject == :aligned }
      end

      describe 'and the signature domain does not match the From header domain' do
        before { headers['dkim-signature'] = [dkim_signature('not-example.com')] }

        it { assert subject == :unaligned }
      end

      describe 'and the signature domain is a subdomain of the From header domain' do
        before { headers['dkim-signature'] = [dkim_signature('subdomain.example.com')] }

        it { assert subject == :unaligned }
      end
    end

    describe 'when there is more than one dkim signature' do
      describe 'and at least one signature domain matches the From header domain' do
        before do
          headers['dkim-signature'] = [
            dkim_signature('example.com'),
            dkim_signature('not-example.com')
          ]
        end

        it { assert subject == :aligned }
      end

      describe 'and no signature domain matches the From header domain' do
        before do
          headers['dkim-signature'] = [
            dkim_signature('subdomain.example.com'),
            dkim_signature('not-example.com')
          ]
        end

        it { assert subject == :unaligned }
      end
    end
  end

  describe '#dmarc_alignment' do
    subject { base_authentication_reader.dmarc_alignment }

    describe 'when spf_alignment is aligned' do
      before { base_authentication_reader.stubs(spf_alignment: :aligned) }

      describe 'and dkim_alignment is aligned' do
        before { base_authentication_reader.stubs(dkim_alignment: :aligned) }

        it { assert subject == :aligned }
      end

      describe 'and dkim_alignment is unaligned' do
        before { base_authentication_reader.stubs(dkim_alignment: :unaligned) }

        it { assert subject == :aligned }
      end
    end

    describe 'when spf_alignment is unaligned' do
      before { base_authentication_reader.stubs(spf_alignment: :unaligned) }

      describe 'and dkim_alignment is aligned' do
        before { base_authentication_reader.stubs(dkim_alignment: :aligned) }

        it { assert subject == :aligned }
      end

      describe 'and dkim_alignment is unaligned' do
        before { base_authentication_reader.stubs(dkim_alignment: :unaligned) }

        it { assert subject == :unaligned }
      end
    end
  end

  describe '#dmarc_fail_and_reject?' do
    subject { base_authentication_reader.dmarc_fail_and_reject? }

    it { refute subject }
  end

  describe '#dmarc' do
    subject { base_authentication_reader.dmarc }

    it { assert_equal :dmarc_missing, subject }
  end

  describe '#dmarc_policy' do
    subject { base_authentication_reader.dmarc_policy }

    it { assert_equal :dmarc_policy_missing, subject }
  end
end
