require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::InboundMail::SenderAuthentication::HeaderReaders::MissingAuthentication do
  include MailTestHelper

  let(:missing_authentication_reader) do
    mail = Zendesk::Mail::Serializers::InboundMessageSerializer.load(json)
    mail.account = Account.new
    Zendesk::InboundMail::SenderAuthentication::HeaderReaders::MissingAuthentication.new(mail)
  end

  let(:headers) do
    {
      'to': [address('support@example.zendesk.com')],
      'delivered-to': [address('support@example.com')],
      'from': [address('john.doe@example.com')]
    }
  end

  let(:json) do
    {
      'body' => 'blah blah blah',
      'errors' => { },
      'headers' => headers,
      'id' => 'a',
      'parts' => []
    }.with_indifferent_access
  end

  describe 'dkim_status' do
    it 'is always dkim_none' do
      assert missing_authentication_reader.dkim_status == :dkim_none
    end
  end

  describe 'spf_status' do
    it 'is always spf_none' do
      assert missing_authentication_reader.spf_status == :spf_none
    end
  end
end
