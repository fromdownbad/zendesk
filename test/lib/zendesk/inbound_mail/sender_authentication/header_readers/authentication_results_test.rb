require_relative '../../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::InboundMail::SenderAuthentication::HeaderReaders::AuthenticationResults do
  include MailTestHelper

  let(:authentication_results_reader) do
    mail = Zendesk::Mail::Serializers::InboundMessageSerializer.load(json)
    mail.account = Account.new
    Zendesk::InboundMail::SenderAuthentication::HeaderReaders::AuthenticationResults.new(mail)
  end

  let(:headers) { { } }

  let(:json) do
    {
      'body' => 'blah blah blah',
      'errors' => { },
      'headers' => headers,
      'id' => 'a',
      'parts' => []
    }.with_indifferent_access
  end

  describe 'dkim_status' do
    before { headers['authentication-results'] = authentication_results }
    subject { authentication_results_reader.dkim_status }

    describe 'with no dkim result, or if it\'s none' do
      [
        'mx.google.com; spf=pass smtp.mailfrom=example@host.example.com',
        'mx.google.com; dkim=none smtp.mailfrom=example@host.example.com'
      ].each do |result|
        let(:authentication_results) { [result] }
        it { assert_equal :dkim_none, subject }
      end
    end

    describe 'with passes' do
      let(:authentication_results) { ['mx.google.com; dkim=pass smtp.mailfrom=example@host.example.com'] }
      it { assert_equal :dkim_pass, subject }
    end

    describe 'with fails' do
      let(:authentication_results) { ['mx.google.com; dkim=fail smtp.mailfrom=example@host.example.com'] }
      it { assert_equal :dkim_fail, subject }
    end

    describe 'with temperrors or permerrors' do
      [
        'mx.google.com; dkim=temperror smtp.mailfrom=example@host.example.com',
        'mx.google.com; dkim=permerror smtp.mailfrom=example@host.example.com'
      ].each do |result|
        let(:authentication_results) { [result] }
        it { assert_equal :dkim_none, subject }
      end
    end

    describe 'with multiple authentication-results headers' do
      let(:authentication_results) do
        [
          'mx.google.com; dkim=fail smtp.mailfrom=example@host.example.com',
          'mx.aol.com; dkim=pass smtp.mailfrom=example@host.example.com',
          'mx.yahoo.com; dkim=none smtp.mailfrom=example@host.example.com'
        ]
      end

      it 'will only trust Google authentication headers starting with mx.google.com' do
        assert_equal :dkim_fail, subject
      end

      describe 'and none of them are added by Google' do
        let(:authentication_results) do
          [
            'mx.aol.com; dkim=pass smtp.mailfrom=example@host.example.com',
            'mx.yahoo.com; dkim=pass smtp.mailfrom=example@host.example.com'
          ]
        end

        it { assert_equal :dkim_none, subject }
      end
    end
  end

  describe 'spf_status' do
    before { headers['authentication-results'] = authentication_results }
    subject { authentication_results_reader.spf_status }

    describe 'with no spf results, or spf is none, neutral or softfail' do
      [
        'mx.google.com; smtp.mailfrom=example@host.example.com',
        'mx.google.com; spf=none smtp.mailfrom=example@host.example.com',
        'mx.google.com; spf=neutral smtp.mailfrom=example@host.example.com',
        'mx.google.com; spf=softfail smtp.mailfrom=example@host.example.com'
      ].each do |result|
        let(:authentication_results) { [result] }
        it { assert_equal subject, :spf_none }
      end
    end

    describe 'with temperrors or permerrors' do
      [
        'mx.google.com; spf=temperror smtp.mailfrom=example@host.example.com',
        'mx.google.com; spf=permerror smtp.mailfrom=example@host.example.com'
      ].each do |result|
        let(:authentication_results) { [result] }
        it { assert_equal :spf_none, subject }
      end
    end

    describe 'with passes' do
      let(:authentication_results) { ['mx.google.com; spf=pass smtp.mailfrom=example@host.example.com'] }
      it { assert_equal :spf_pass, subject }
    end

    describe 'with fails' do
      let(:authentication_results) { ['mx.google.com; spf=fail smtp.mailfrom=example@host.example.com'] }
      it { assert_equal :spf_fail, subject }
    end

    describe 'with multiple authentication-results headers' do
      let(:authentication_results) do
        [
          'mx.google.com; spf=fail smtp.mailfrom=example@host.example.com',
          'mx.aol.com; spf=pass smtp.mailfrom=example@host.example.com',
          'mx.yahoo.com; spf=none dkim=pass smtp.mailfrom=example@host.example.com'
        ]
      end

      it 'will only trust Google authentication headers starting with mx.google.com' do
        assert_equal :spf_fail, subject
      end

      describe 'and none of them are added by Google' do
        let(:authentication_results) do
          [
            'mx.aol.com; spf=pass smtp.mailfrom=example@host.example.com',
            'mx.yahoo.com; spf=pass smtp.mailfrom=example@host.example.com'
          ]
        end

        it { assert_equal :spf_none, subject }
      end
    end
  end
end
