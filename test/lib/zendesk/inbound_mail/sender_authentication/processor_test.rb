require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::SenderAuthentication::Processor do
  include MailTestHelper

  describe "#run" do
    subject { processor.run }

    let(:processor) { Zendesk::InboundMail::SenderAuthentication::Processor.new(header_reader, mail) }
    let(:header_reader) { Zendesk::InboundMail::SenderAuthentication::HeaderReaderFactory.create(mail) }

    let(:mail) do
      Zendesk::Mail::Serializers::InboundMessageSerializer.load(json).tap do |mail|
        mail.account = Account.new
      end
    end

    let(:json) do
      {
        'body' => 'hello world',
        'subject' => 'foo',
        'errors' => { },
        'headers' => {
          'to': [address('support@example.zendesk.com')],
          'delivered-to': [address('support@example.zendesk.com')],
          'from': [address('john.doe@subdomain.zendesk.com')],
          'x-rspamd-authentication-results': ['in1.pod1.usw1.zdsys.com; spf=fail smtp.mailfrom=example@zendesk.com; dkim=fail']
        }.merge(headers),
        'parts' => []
      }.with_indifferent_access
    end

    let(:headers) do
      {}
    end

    it 'returns a result' do
      assert_equal subject.class, Zendesk::InboundMail::SenderAuthentication::Result
    end

    describe 'when sender authentication can be skipped' do
      before do
        header_reader.stubs(skip?: true, spf_status: :spf_fail, dkim_status: :dkim_fail)
      end

      it 'logs' do
        mail.logger.expects(:info).with('Ignore X-Rspamd-Authentication-Results - Rspamd REPLY symbol present')
        subject
      end

      it 'changes the action to :pass' do
        assert_equal subject.action, :pass
      end

      describe_with_arturo_disabled :email_sender_auth_skip_rspamd_reply do
        it 'does not change the action' do
          assert_equal subject.action, :reject
        end
      end
    end

    describe 'when the result action is reject' do
      before do
        header_reader.stubs(spf_status: :spf_fail, dkim_status: :dkim_fail)
      end

      describe 'and the message ID is not a Zendesk message ID' do
        before do
          mail.message_id = '123e4567-e89b-12d3-a456-426655440000@example.com'
        end

        it 'does not modify the Result from the original authentication matrix ' do
          assert subject.action == :reject
        end

        it 'logs that the message ID does not match known Zendesk patterns' do
          mail.logger.expects(:info).with('Message-ID does not match known outbound Zendesk message ID patterns')
          subject
        end
      end

      describe 'from a deprecated Zendesk out host' do
        let(:headers) do
          { 'received': ['random.mx.com', 'out.pod5.iad1.zdsys.com'] }
        end

        it 'returns a Result that is modified from the original authentication matrix ' do
          assert subject.action == :pass
        end

        it 'logs that modified authentication results are returned' do
          mail.logger.expects(:info).with('SPF/DKIM failed but headers indicate Zendesk-sent email - will not reject')
          subject
        end
      end

      describe 'from a new Zendesk out host' do
        let(:headers) do
          { 'received': ['random.mx.com', 'outxyz.pod5.iad1.zdsys.com'] }
        end

        it 'detects mail was sent from zendesk as uses modified result' do
          assert subject.action == :pass
        end
      end
    end
  end
end
