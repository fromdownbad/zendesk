require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::AccountIDMessageLogger do
  fixtures :accounts

  before do
    @mail = Zendesk::Mail::InboundMessage.new
    @mail.logger = Zendesk::InboundMail::AccountIDMessageLogger.new(@mail)
  end

  describe "#format_message" do
    describe "when the mail object has an account" do
      before { @mail.account = accounts(:minimum) }

      it "adds the account_id to the beginning of log message lines" do
        log_message = @mail.logger.send(:format_message, "Chapeau!")

        assert_equal "#{@mail.message_id} account_id: #{@mail.account.id}: Chapeau!", log_message
      end
    end

    describe "when the mail object does not have an account" do
      it "does not an an account_id to the beginning of log message lines" do
        log_message = @mail.logger.send(:format_message, "Chapeau!")

        assert_equal "#{@mail.message_id} account_id: none: Chapeau!", log_message
      end
    end
  end
end
