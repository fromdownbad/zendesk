require_relative "../../../support/test_helper"
require_relative "../../../support/mail_test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::MailParsingQueue do
  let(:mail) { FixtureHelper::Mail.read('message_with_mail_parsing_queue_state.json') }

  describe "MailParsingQueue ProcessingState serialization" do
    describe "#processing_state" do
      describe "#from" do
        it { mail.processing_state.from.name.must_equal "John Doe" }
        it { mail.processing_state.from.address.must_equal "john.doe@example.com" }

        # to match existing InboundMessage#sender behavior, see inbound_mail.rb, Sender
        it { mail.processing_state.from.original_name.must_be_nil }

        describe "when 'from' not present" do
          before { mail.params.stubs(:fetch).with(:processing_state, {}).returns({}) }
          it { mail.processing_state.from.must_be_nil }
        end
      end

      describe "#from_header" do
        it { mail.processing_state.from_header.name.must_equal "John Deere" }
        it { mail.processing_state.from_header.address.must_equal "john.deere@example.com" }

        # to match existing InboundMessage#sender behavior, see inbound_mail.rb, Sender
        it { mail.processing_state.from_header.original_name.must_be_nil }

        describe "when 'from_header' not present" do
          before { mail.params.stubs(:fetch).with(:processing_state, {}).returns({}) }
          it { mail.processing_state.from_header.must_be_nil }
        end
      end

      describe "#suspension" do
        it { mail.processing_state.suspension.must_equal SuspensionType.CONTENT_PROCESSOR_TIMED_OUT }

        describe "when 'suspension' not present" do
          before { mail.params.stubs(:fetch).with(:processing_state, {}).returns({}) }
          it { mail.processing_state.suspension.must_be_nil }
        end
      end

      describe '#quoted_html_content' do
        subject { mail.processing_state.quoted_html_content }

        let(:expected_content) { "<div class=\"zendesk_quoted\"><div>boo</div></div>" }

        it { assert_equal subject, expected_content }
      end

      describe '#quoted_plain_content' do
        subject { mail.processing_state.quoted_plain_content }

        let(:expected_content) { "boo" }

        it { assert_equal subject, expected_content }
      end

      describe '#html_content' do
        subject { mail.processing_state.html_content }
        let(:expected_content) { "<div>boo</div>" }

        it { assert_equal subject, expected_content }
      end

      describe '#plain_content' do
        subject { mail.processing_state.plain_content }
        let(:expected_content) { "You'll be smitten" }

        it { assert_equal mail.processing_state.plain_content, expected_content }
      end

      describe '#full_html_content' do
        subject { mail.processing_state.full_html_content }
        let(:expected_content) { "<html><head><meta content=\"text/html; charset=ISO-8859-1\"http-equiv=\"Content-Type\"></head><body><div>boo</div></body></html>" }

        it { assert_equal mail.processing_state.full_html_content, expected_content }
      end

      describe '#full_plain_content' do
        subject { mail.processing_state.full_plain_content }
        let(:expected_content) { "You'll be smitten" }

        it { assert_equal mail.processing_state.full_plain_content, expected_content }
      end
    end
  end
end
