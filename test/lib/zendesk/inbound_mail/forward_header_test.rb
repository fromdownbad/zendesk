require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::ForwardHeader do
  fixtures :accounts

  class ForwardBuilder
    attr_accessor :comment, :upper_boundary, :headers, :lower_boundary, :original_message

    def to_s
      [comment, upper_boundary, headers, lower_boundary, original_message].join
    end
  end

  let(:mail)            { Zendesk::Mail::InboundMessage.new }
  let(:account)         { accounts(:minimum) }
  let(:original_sender) { "Original Sender [mailto:enduser@example.com]" }

  def header(message)
    @header = Zendesk::InboundMail::ForwardHeader.new(message, mail)
  end

  before do
    # TicketProcessingWorker normally would apply the account to the "mail"/InboundMessage object
    mail.account = account

    @builder = ForwardBuilder.new.tap do |forward|
      forward.comment        = "Hello"
      forward.upper_boundary = "\n\n"
      forward.headers        = "From: #{original_sender}"
      forward.lower_boundary = "\n\n"
      forward.original_message = "The original message"
    end

    assert_equal "Hello\n\nFrom: #{original_sender}\n\nThe original message", @builder.to_s
  end

  it "should detect quoted boundaries" do
    header("Some stuff\n\nBegin forwarded message:\n\n> From: Sender <sender@example.com>\n>Date: The past\n> \n> Original message\n")
    assert_equal "Begin forwarded message:\n\n> From: Sender <sender@example.com>\n>Date: The past\n> \n> ", @header.to_s
  end

  it "does not blow up when no boundary is detected" do
    header("Message\nBlah: hello@example.com")
    assert_equal '', @header.to_s
  end

  describe "beginning" do
    it "prefers the delimiter when it's available" do
      @builder.comment        = "Something\n\nwith newlines"
      @builder.upper_boundary = "\nBegin forwarded message:\n"
      header(@builder.to_s)

      assert_equal "Begin forwarded message:\nFrom: #{original_sender}\n\n", @header.to_s
    end

    describe "that is a blank line" do
      before do
        assert_equal "\n\n", @builder.upper_boundary
        header(@builder.to_s)
      end

      it "is detected" do
        assert_equal "\n\nFrom: #{original_sender}\n\n", @header.to_s
      end

      it "is detected when it contains carriage returns" do
        @builder.upper_boundary = "\r\n\r\n"
        header(@builder.to_s)

        assert_equal "\n\r\nFrom: #{original_sender}\n\n", @header.to_s
      end
    end

    describe "with common forward delimiters" do
      it "detects consecutive dashes" do
        @builder.upper_boundary = "\n ------------------------------\n"
        header(@builder.to_s)

        assert_equal " ------------------------------\nFrom: #{original_sender}\n\n", @header.to_s
      end

      it "detects dashes followed by a From marker" do
        @builder.upper_boundary = "\n----\n"
        header(@builder.to_s)

        assert_equal "----\nFrom: #{original_sender}\n\n", @header.to_s
      end

      it "detects dashes followed by a Forward marker" do
        @builder.upper_boundary = "\n------ Forwarded Message\n"
        header(@builder.to_s)

        assert_equal "------ Forwarded Message\nFrom: #{original_sender}\n\n", @header.to_s
      end

      it "detects leading and trailing dashes" do
        @builder.upper_boundary = "\n-----Ursprüngliche Nachricht----\n"
        header(@builder.to_s)
        assert_equal "-----Ursprüngliche Nachricht----\nFrom: #{original_sender}\n\n", @header.to_s
      end

      it "detects leading and trailing dashes split by a newline" do
        @builder.upper_boundary = "\n----- Forwarded by Some Person/SCO/Americas/Esselte on 12/20/2012 10:34 \nAM -----\n\n"
        header(@builder.to_s)
        assert_equal "----- Forwarded by Some Person/SCO/Americas/Esselte on 12/20/2012 10:34 \nAM -----\n\nFrom: #{original_sender}\n\n", @header.to_s
      end

      it "detects consecutive underscores" do
        @builder.upper_boundary = "\n________________________________\n"
        header(@builder.to_s)
        assert_equal "________________________________\nFrom: #{original_sender}\n\n", @header.to_s
      end

      it "detects the begin forwarded message phrase" do
        @builder.upper_boundary = "\nBegin forwarded message:\n"
        header(@builder.to_s)

        assert_equal "Begin forwarded message:\nFrom: #{original_sender}\n\n", @header.to_s
      end
    end

    describe "that cannot be found" do
      before do
        @builder.upper_boundary = "\n"
        @builder.headers = "From: #{original_sender}"
        header(@builder.to_s)
      end

      it "is the From header" do
        assert_equal "From: #{original_sender}\n\n", @header.to_s
      end
    end

    describe "with 'From'-like content in the middle of the comment author's signature" do
      before do
        @builder.comment = "Tel DE: +49 (0) 30"
        header(@builder.to_s)
      end

      it "detects the correct From header" do
        assert_equal "\n\nFrom: #{original_sender}\n\n", @header.to_s
      end
    end
  end

  describe "ending" do
    describe "that is a blank line" do
      before do
        assert_equal "\n\n", @builder.lower_boundary
        header(@builder.to_s)
      end

      it "is detected" do
        assert_equal "\n\nFrom: #{original_sender}\n\n", @header.to_s
      end
    end

    describe "that is a header group" do
      before do
        @builder.headers = "From: #{original_sender}\n>Reply-To: me\n> Subject: Hello?"
        @builder.lower_boundary = "\n"

        header(@builder.to_s)
      end

      it "is detected" do
        assert_equal "\n\nFrom: #{original_sender}\n>Reply-To: me\n> Subject: Hello?\n", @header.to_s
      end

      it "is detected when surrounded by stars" do
        @builder.headers = "*From:* #{original_sender}\n*Reply-To:* me\n*Subject:* Hello?"
        header(@builder.to_s)

        assert_equal "\n\n*From:* #{original_sender}\n*Reply-To:* me\n*Subject:* Hello?\n", @header.to_s
      end

      it "is detected amongst multiple header groups" do
        @builder.original_message = "A message\nFrom: An older forward"
        header(@builder.to_s)

        assert_equal "\n\nFrom: #{original_sender}\n>Reply-To: me\n> Subject: Hello?\n", @header.to_s
      end

      it "is detected at the end of the message" do
        @builder.lower_boundary = ""
        @builder.original_message = ""
        header(@builder.to_s)

        assert_equal "\n\nFrom: #{original_sender}\n>Reply-To: me\n> Subject: Hello?", @header.to_s
      end
    end

    describe "that cannot be found" do
      before do
        @builder.lower_boundary = "\n"
        header(@builder.to_s)
      end

      it "is the From Header" do
        assert_equal "\n\nFrom: #{original_sender}\n", @header.to_s
      end

      it "should not be the From address when it's not associated with the initial From header" do
        @builder.original_message = "The original email@example.com message\nFrom: other_sender@example.com\netc"
        header(@builder.to_s)

        assert_equal "\n\nFrom: #{original_sender}\n", @header.to_s
      end
    end
  end

  describe "when the from word is inside another word" do
    it "finds the correct from address" do
      header("Hi Adam,\n\n\n\nSee below.\n\n\n\nToodles,\n\n\n\nEd Hardy\n\n\n\nSomecompany Ltd\n\nhere's my skype: whousesskype\n\n\n\nServices offered by Somecompany include: The finest online and mobile poodle\nshaving, highly responsive poodles,and nervous people\n\n\n\n\n\n\n\nFrom: Fig [mailto:foo@bar.com]\nSent: 16 January 2015 10:48\nTo: fil@bo.com\nCc: dil@nil.com\nSubject: FW: Decaf poodle sprinkles\n\n\n\n\n")
      assert_equal "\"Fig\" <foo@bar.com>", @header.from_address
    end
  end

  describe "when the forward headers use non-breaking spaces before the semicolon" do
    before do
      message = "\r\nAgent comment.\r\n\r\n-----Message d'origine-----\r\nDe : Original Sender [mariozaizar@gmail.com]\r\n\r\nOrignal email\r\n"
      @header = header(message)
    end

    it "detects the correct From header" do
      assert_equal "-----Message d'origine-----\r\nDe : Original Sender [mariozaizar@gmail.com]\r\n\r\n", @header.to_s
    end
  end

  describe "when the forward headers use Chinese-style UTF8 'fullwidth' colons" do
    before do
      message = "Agent comment.\r\n---------- 已转发邮件 ----------\r\n发件人： Original Sender <enduser@example.com>\r\n"
      @header = header(message)
    end

    it "detects the correct From header" do
      assert_equal "---------- 已转发邮件 ----------\r\n发件人： Original Sender <enduser@example.com>\r\n", @header.to_s
    end
  end

  describe "with a from address" do
    it "provides the from address" do
      @builder.headers = "From: sender@example.com"
      header(@builder.to_s)

      assert_equal '"" <sender@example.com>', @header.from_address, @header.to_s
    end

    it "provides the from address from noisy conditions" do
      @builder.headers = "\r\n\r\nFrom:\r\nsender@example.com\r\n"
      header(@builder.to_s)

      assert_equal '"" <sender@example.com>', @header.from_address, @header.to_s
    end

    # This test does not seem to have the correct string format to trigger the desired scenario - cwippern 2017-07-26
    it "ignores addresses from CCs" do
      @builder.headers = "\n\n---------- Forwarded message ---------\n\nFrom: Sender No Email\nSubject: Halp\nDate: Jul 1 2016, at 11:44 am  \nTo: some agent <smdy@example.com>nCc: user <user@domain.com>\n\n"
      header(@builder.to_s)

      refute @header.from_address
    end

    it "ignores addresses from CCs" do
      @builder.headers = "\n\n---------- Forwarded message ---------\n\nFrom: Sender No Email\nCc: user <user@domain.com>\nSubject: Halp\nDate: Jul 1 2016, at 11:44 am  \nTo: some agent <smdy@example.com>\n\n"
      header(@builder.to_s)

      refute @header.from_address
    end

    describe_with_arturo_enabled :email_yahoo_forward_header_parsing do
      it "provides from address for Yahoo mail " do
        @builder.headers = "help this person thanks
   ----- Forwarded Message ----- From: Forwarded User <forwarded.user@z3nmail.com>To: <z3nagent@yahoo.com>Sent: Tuesday, June 23, 2020, 11:37:07 AM PDTSubject: Hey help yahoo agent
 hey help me thanks"
        header(@builder.to_s)

        assert_equal "\"Forwarded User\" <forwarded.user@z3nmail.com>", @header.from_address
      end

      it "provides from address for a mail with nested forwards" do
        # This case is an existing passing Yahoo mail
        @builder.headers = "Can you please add the unit number to this customers' address?
   ----- Forwarded Message ----- From: Forwarded User <forwarded.user@yahoo.com>To: AgentZ3n <agentz3n@z3nmail.net>Sent: Wednesday, June 3, 2020, 02:06:23 AM EDTSubject: Re: AUTHORIZATION FORM
  Thank you !  Agent, you have missed it my address unit number.Give you it again
    On Tuesday, June 2, 2020, 06:28:23 AM PDT, XXX <xxxx@xxxxx.xxx> wrote:

 Got it!  Thank you
Stay Well,
Agent

    On Monday, June 1, 2020, 09:28:02 PM EDT, Forwarded User <forwarded.user@yahoo.com> wrote:

  Hi, Agent, nice to meet you and thank you so much your help     and here is financing copy.   Thank you again and have a nice evening !yours sincerely,
    On Monday, June 1, 2020, 11:34:14 AM PDT, Agent <agent@z3nmail.com> wrote:
Stay Well,
Agent

   ----- Forwarded Message ----- From: FInancing XXX <financing@XXX.net>To:<forwarded.user@z3nmail.com>Cc: 'AgentZ3n' <agentz3n@z3nmail.com>Sent: Monday, June 1, 2020, 02:32:53 PM EDTSubject: AUTHORIZATION FORM

Thank you for shopping us! Attached is the pre-authorization receipt for your recent purchase. There will be 2 pages in the attachment, the first is your copy to keep, and the 2nd will be the one that must be returned.
"
        header(@builder.to_s)

        assert_equal "\"Forwarded User\" <forwarded.user@yahoo.com>", @header.from_address
      end

      it "provides from address for a gmail client case" do
        @builder.headers = "User needs assistance.
         Agent Z3n
---------- Forwarded message ---------
From: Forwarded User <forwarded.user@z3nmail.com>
Date: Mon, Jun 29, 2020 at 3:26 AM
Subject: Help me forward this
To: Z3nagent <agentz3n@gmail.com>


Hi Agent Z3n, Please help me."
        header(@builder.to_s)

        assert_equal "\"Forwarded User\" <forwarded.user@z3nmail.com>", @header.from_address
      end

      it "provides from address for a Microsoft Outlook client case" do
        @builder.headers = " More details here

From: Forwarded User <forwarded.user@z3nmail.com>
Sent: Monday, June 29, 2020 1:23 PM
To: Agent Z3n <agentz3n@z3nmail.com>;
Subject: Blah Blah

Hello Agent -
I have attached payroll.

Let me know if I missed anything.

Thanks
Forwaded User
"
        header(@builder.to_s)

        assert_equal "\"Forwarded User\" <forwarded.user@z3nmail.com>", @header.from_address
      end
    end

    describe_with_arturo_disabled :email_yahoo_forward_header_parsing do
      it "does not provide from address for this use case in Yahoo" do
        @builder.headers = "help this person thanks
   ----- Forwarded Message ----- From: Forwarded User <forwarded.user@test.com>To: <z3nagent@yahoo.com>Sent: Tuesday, June 23, 2020, 11:37:07 AM PDTSubject: Hey help yahoo agent
 hey help me thanks"
        header(@builder.to_s)

        refute @header.from_address
      end

      it "provides from address for a mail with nested forwards" do
        # This case is an existing passing Yahoo mail
        @builder.headers = "Can you please add the unit number to this customers' address?
   ----- Forwarded Message ----- From: Forwarded User <forwarded.user@yahoo.com>To: AgentZ3n <agentz3n@z3nmail.net>Sent: Wednesday, June 3, 2020, 02:06:23 AM EDTSubject: Re: AUTHORIZATION FORM
  Thank you !  Agent, you have missed it my address unit number.Give you it again
    On Tuesday, June 2, 2020, 06:28:23 AM PDT, XXX <xxxx@xxxxx.xxx> wrote:

 Got it!  Thank you
Stay Well,
Agent

    On Monday, June 1, 2020, 09:28:02 PM EDT, Forwarded User <forwarded.user@yahoo.com> wrote:

  Hi, Agent, nice to meet you and thank you so much your help     and here is financing copy.   Thank you again and have a nice evening !yours sincerely,
    On Monday, June 1, 2020, 11:34:14 AM PDT, Agent <agent@z3nmail.com> wrote:
Stay Well,
Agent

   ----- Forwarded Message ----- From: FInancing XXX <financing@XXX.net>To:<forwarded.user@z3nmail.com>Cc: 'AgentZ3n' <agentz3n@z3nmail.com>Sent: Monday, June 1, 2020, 02:32:53 PM EDTSubject: AUTHORIZATION FORM

Thank you for shopping us! Attached is the pre-authorization receipt for your recent purchase. There will be 2 pages in the attachment, the first is your copy to keep, and the 2nd will be the one that must be returned.
"
        header(@builder.to_s)

        assert_equal "\"Forwarded User\" <forwarded.user@yahoo.com>", @header.from_address
      end

      it "provides from address for a gmail client case" do
        @builder.headers = "User needs assistance.
         Agent Z3n
---------- Forwarded message ---------
From: Forwarded User <forwarded.user@z3nmail.com>
Date: Mon, Jun 29, 2020 at 3:26 AM
Subject: Help me forward this
To: Z3nagent <agentz3n@gmail.com>


Hi Agent Z3n, Please help me."
        header(@builder.to_s)

        assert_equal "\"Forwarded User\" <forwarded.user@z3nmail.com>", @header.from_address
      end

      it "provides from address for a Microsoft Outlook client case" do
        @builder.headers = " More details here

From: Forwarded User <forwarded.user@z3nmail.com>
Sent: Monday, June 29, 2020 1:23 PM
To: Agent Z3n <agentz3n@z3nmail.com>;
Subject: Blah Blah

Hello Agent -
I have attached payroll.

Let me know if I missed anything.

Thanks
Forwaded User
"
        header(@builder.to_s)

        assert_equal "\"Forwarded User\" <forwarded.user@z3nmail.com>", @header.from_address
      end
    end
  end

  describe "without a from address" do
    before do
      @builder.headers = "From: John\nTo: \"Jimmy\" <jimmy@example.com>"
      header(@builder.to_s)
    end

    it { refute @header.from_address }

    # zd2634763 behavior
    describe_with_arturo_disabled :email_cleanup_to_headers_from_forward_header do
      it { @header.from_address.must_equal '"John To: Jimmy" <jimmy@example.com>' }
    end
  end

  describe "catastrophic backtracking prevention" do
    before do
      # ensure v4 header pattern is active otherwise test will time out.
      header("John Agent\n\nOrders Administrator\n\n \n\n                                      \n\n \n\nT: +44 (0)330 555 1212 |  D: +44 (0)330 555 2121 | \n\nCool Company Ltd, Horizon, Honey Lane, Hurley, Berkshire SL7 7RH\n\n \n\n \n\nFrom: Johnny End-User\nSent: 10 March 2017 14:22\nTo: John Agent <john.agent@example.com>\nSubject: FW: Fixed IP Sim\n\n \n\nHi Jon\n\n \n\nNew order, existing customer.\n\n \n\nJust need a tariff created.\n\n \n\nJohnny\n\n \n\nJohnny End-User\n\nSenior Business Development Manager\n\n \n\n                     \n\n \n\nT: +44 (0)330 555 4321 |  M: +44 (0)7825551234 |  E: johnnyend-user@coolcompany.com\n\nOur new address: Cool Company Ltd, Horizon, Honey Lane, Hurley, Berkshire SL7 7RH\n\nwww.coolcompany.com (http://www.coolcompany.com/)  \n\n \n\nFrom: John Agent [mailto:john.agent@example.com]\nSent: 10 March 2017 14:11\nTo: Johnny End-User <johnnyend-user@coolcompany.com>\nSubject: RE: Fixed IP Sim\n\n \n\nHi Johnny\n\n \n\nPlease find attached\n\n \n\nMany Thanks\n\n \n\nJohn Agent | Sales Director | Cool Communications\n\n45-57 South School Street | Aberdeen | AB11 6LE | United Kingdom\n\nT: 01224 555555 | M: 07755555555  |  john.agent@example.com | www.coolcommunications.com (http://www.coolcommunications.com/)\n\n \n\n* Mobile communications (http://www.coolcommunications.com/communications/mobile/)\n\n* Phones systems (http://www.coolcommunications.com/communications/fixed-line/)\n\n* Hosted telephony (http://www.coolcommunications.com/communications/fixed-line/)\n\n* Voice and data solutions (http://www.coolcommunications.com/communications/fixed-line/)\n\n* Web Design (http://www.coolcommunications.com/design-agency-aberdeen/web-design-aberdeen/)\n\n* Brand & Print Design (http://www.coolcommunications.com/design-agency-aberdeen/web-design-aberdeen/)\n\n* Vehicle Hands-free solutions (http://www.coolcommunications.com/workshop/)\n\n* Vehicle Cameras (http://www.coolcommunications.com/workshop/)\n\n \n\n                   \n\n \n\n \n\n           \n\nThis e-mail may contain legally privileged and confidential information and is intended solely for the addressee.  If you are not the addressee, you must not copy, disclose, or take any action in reliance on it.  Further, if you are not the addressee please notify the sender by reply e-mail and delete it from your computer.  Cool Cool Communications Limited (trading as Cool Communications) accept no liability for changes made to this e-mail after it was sent or for viruses carried by it.  Any views, opinions, conclusions or other information in this message which do not relate to the business of this firm are not authorised by us.  Cool Cool Communications Limited registered office Someplace House, 52 – 54 Rose Street, Aberdeen, AB10 1HA.  Registered number ZZ012343.  A full list of the directors of the company is open to inspection at this address.  The views expressed in this e-mail are not necessarily those of Cool Cool Stuff (Site Services) Limited and unless this e-mail is sent in connection with its business.  Cool Cool Communications Limited does not accept any responsibility for the contents of this e-mail.  Any intellectual property contained within this email remains property of Cool Cool Communications Limited unless explicit permission of use is granted. Intellectual property, including graphic design and/or web design, is provided via email for review purposes only, unless explicitly stated otherwise.\n\n \n\nFrom: Johnny End-User [mailto:johnnyend-user@coolcompany.com]\nSent: 10 March 2017 13:49\nTo: John Agent <john.agent@example.com>\nSubject: RE: Fixed IP Sim\n\n \n\nHi Craig\n\n \n\nHere you go.\n\n \n\nOnce the tariff addendum has been signed, I can process the order for you.\n\n \n\nIf you have any further questions, please let me know.\n\n \n\nKind regards,\n\n \n\nJohnny\n\n \n\nJohnny End-User\n\nSenior Business Development Manager\n\n \n\n                     \n\n \n\nT: +44 (0)330 555 4321 |  M: +44 (0)7825551234 |  E: johnnyend-user@coolcompany.com\n\nOur new address: Cool Company Ltd, Horizon, Honey Lane, Hurley, Berkshire SL7 7RH\n\nwww.coolcompany.com (http://www.coolcompany.com/)  \n\n \n\nFrom: John Agent [mailto:john.agent@example.com]\nSent: 10 March 2017 13:02\nTo: Johnny End-User <johnnyend-user@coolcompany.com>\nSubject: RE: Fixed IP Sim\nImportance: High\n\n \n\nHi Johnny\n\n \n\nI have a very old email with this detail  -\n\n \n\nEE 4G\n\n \n\n8GB £39.50\n\nOverage £0.03 per MB\n\nSMS 10p\n\n12 month contract\n\n \n\nThis is to be added to our existing account\n\n \n\n \n\nMany Thanks\n\n \n\nJohn Agent | Sales Director | Cool Communications\n\n45-57 South School Street | Aberdeen | AB11 6LE | United Kingdom\n\nT: 01224 555555 | M: 07755555555  |  john.agent@example.com | www.coolcommunications.com (http://www.coolcommunications.com/)\n\n \n\n* Mobile communications (http://www.coolcommunications.com/communications/mobile/)\n\n* Phones systems (http://www.coolcommunications.com/communications/fixed-line/)\n\n* Hosted telephony (http://www.coolcommunications.com/communications/fixed-line/)\n\n* Voice and data solutions (http://www.coolcommunications.com/communications/fixed-line/)\n\n* Web Design (http://www.coolcommunications.com/design-agency-aberdeen/web-design-aberdeen/)\n\n* Brand & Print Design (http://www.coolcommunications.com/design-agency-aberdeen/web-design-aberdeen/)\n\n* Vehicle Hands-free solutions (http://www.coolcommunications.com/workshop/)\n\n* Vehicle Cameras (http://www.coolcommunications.com/workshop/)\n\n \n\n                   \n\n \n\n \n\n           \n\nThis e-mail may contain legally privileged and confidential information and is intended solely for the addressee.  If you are not the addressee, you must not copy, disclose, or take any action in reliance on it.  Further, if you are not the addressee please notify the sender by reply e-mail and delete it from your computer.  Cool Cool Communications Limited (trading as Cool Communications) accept no liability for changes made to this e-mail after it was sent or for viruses carried by it.  Any views, opinions, conclusions or other information in this message which do not relate to the business of this firm are not authorised by us.  Cool Cool Communications Limited registered office Someplace House, 52 – 54 Rose Street, Aberdeen, AB10 1HA.  Registered number ZZ012343.  A full list of the directors of the company is open to inspection at this address.  The views expressed in this e-mail are not necessarily those of Cool Cool Stuff (Site Services) Limited and unless this e-mail is sent in connection with its business.  Cool Cool Communications Limited does not accept any responsibility for the contents of this e-mail.  Any intellectual property contained within this email remains property of Cool Cool Communications Limited unless explicit permission of use is granted. Intellectual property, including graphic design and/or web design, is provided via email for review purposes only, unless explicitly stated otherwise.\n\n \n\nFrom: John Agent\nSent: 10 March 2017 12:05\nTo: Johnny End-User <johnnyend-user@coolcompany.com>\nCc: John Agent <john.agent@example.com>\nSubject: Fixed IP Sim\nImportance: High\n\n \n\nHi Johnny\n\n \n\nCan we go ahead and order an 8GB fixed IP sim on EE for 24 months ?\n\n \n\nPlease confirm price and we require the sim card on Monday AM if possible\n\n \n\n \n\nMany Thanks\n\n \n\nJohn Agent | Sales Director | Cool Communications\n\n45-57 South School Street | Aberdeen | AB11 6LE | United Kingdom\n\nT: 01224 555555 | M: 07755555555  |  john.agent@example.com | www.coolcommunications.com (http://www.coolcommunications.com/)\n\n \n\n* Mobile communications (http://www.coolcommunications.com/communications/mobile/)\n\n* Phones systems (http://www.coolcommunications.com/communications/fixed-line/)\n\n* Hosted telephony (http://www.coolcommunications.com/communications/fixed-line/)\n\n* Voice and data solutions (http://www.coolcommunications.com/communications/fixed-line/)\n\n* Web Design (http://www.coolcommunications.com/design-agency-aberdeen/web-design-aberdeen/)\n\n* Brand & Print Design (http://www.coolcommunications.com/design-agency-aberdeen/web-design-aberdeen/)\n\n* Vehicle Hands-free solutions (http://www.coolcommunications.com/workshop/)\n\n* Vehicle Cameras (http://www.coolcommunications.com/workshop/)\n\n \n\n                   \n\n \n\n \n\n           \n\nThis e-mail may contain legally privileged and confidential information and is intended solely for the addressee.  If you are not the addressee, you must not copy, disclose, or take any action in reliance on it.  Further, if you are not the addressee please notify the sender by reply e-mail and delete it from your computer.  Cool Cool Communications Limited (trading as Cool Communications) accept no liability for changes made to this e-mail after it was sent or for viruses carried by it.  Any views, opinions, conclusions or other information in this message which do not relate to the business of this firm are not authorised by us.  Cool Cool Communications Limited registered office Someplace House, 52 – 54 Rose Street, Aberdeen, AB10 1HA.  Registered number ZZ012343.  A full list of the directors of the company is open to inspection at this address.  The views expressed in this e-mail are not necessarily those of Cool Cool Stuff (Site Services) Limited and unless this e-mail is sent in connection with its business.  Cool Cool Communications Limited does not accept any responsibility for the contents of this e-mail.  Any intellectual property contained within this email remains property of Cool Cool Communications Limited unless explicit permission of use is granted. Intellectual property, including graphic design and/or web design, is provided via email for review purposes only, unless explicitly stated otherwise.\n\n \n\n \n\n-------------------------------\n\nLegal Disclaimer: Confidential and privileged information may be contained in this message and attachments and is intended for the addressee only. If you are not the addressee or not responsible for the delivery of the message to such person, you may not copy or forward this message. In such case, please destroy this message and notify the sender. COOL COMPANY LIMITED Registered in England no. 03880663 Address: Cool Company Ltd, Horizon, Honey Lane, Hurley, Berkshire SL7 7RH Web site: http://www.coolcompany.com\n\n \n\n-------------------------------\n\nLegal Disclaimer: Confidential and privileged information may be contained in this message and attachments and is intended for the addressee only. If you are not the addressee or not responsible for the delivery of the message to such person, you may not copy or forward this message. In such case, please destroy this message and notify the sender. COOL COMPANY LIMITED Registered in England no. 03880663 Address: Cool Company Ltd, Horizon, Honey Lane, Hurley, Berkshire SL7 7RH Web site: http://www.coolcompany.com\n\n\n-------------------------------\nLegal Disclaimer: Confidential and privileged information may be contained in this message and attachments and is intended for the addressee only. If you are not the addressee or not responsible for the delivery of the message to such person, you may not copy or forward this message. In such case, please destroy this message and notify the sender. COOL COMPANY LIMITED Registered in England no. 03880663 Address: Cool Company Ltd, Horizon, Honey Lane, Hurley, Berkshire SL7 7RH Web site: http://www.coolcompany.com")
    end

    it { @header.to_s.must_equal "\n\nFrom: Johnny End-User\nSent: 10 March 2017 14:22\nTo: John Agent <john.agent@example.com>\nSubject: FW: Fixed IP Sim\n\n \n\n" }
  end
end
