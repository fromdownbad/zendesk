require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::InboundMail::RateLimit do
  include MailTestHelper

  fixtures :accounts

  let(:account)               { accounts(:minimum) }
  let(:sender)                { "john@example.com" }
  let(:whitelisted_sender)    { "shiftmanager@jam.co.uk" }
  let(:recipient)             { "support@minimum.zendesk.com" }
  let(:whitelisted_recipient) { "trademark@pinterest.zendesk.com" }
  let(:rate_limit)            { Zendesk::InboundMail::RateLimit.new(account, sender, recipient) }

  describe "default loop_threshold" do
    it { account.loop_threshold.must_equal 20 }
  end

  describe "limit hierarchy" do
    describe "when using account loop_threshold" do
      it { rate_limit.send(:limit).must_equal account.loop_threshold }
    end

    describe "when using sender exception list" do
      let(:sender) { whitelisted_sender }
      it { rate_limit.send(:limit).must_equal 120 }
    end

    describe "when using recipient exception list" do
      let(:recipient) { whitelisted_recipient }

      before do
        account.stubs(subdomain: 'pinterest')
        Zendesk::MtcMpqMigration.config.stubs(:zendesk_host).returns('zendesk.com')
      end

      it { rate_limit.send(:limit).must_equal 100 }

      describe "when recipient has a Zendesk plus ID" do
        let(:recipient) { whitelisted_recipient.sub!(/@/, "+id3694841@") }
        it { rate_limit.send(:limit).must_equal 100 }
      end

      describe "when recipient has a non-Zendesk plus ID" do
        let(:recipient) { whitelisted_recipient.sub!(/@/, "+id3694841asdf12324") }
        it { rate_limit.send(:limit).must_equal account.loop_threshold }
      end
    end

    describe "when using account domain whitelist" do
      let(:sender) { "jane@example.com" }
      before { account.stubs(domain_whitelist: "jane@example.com") }

      describe_with_arturo_enabled :email_account_whitelist_multiplier do
        it { rate_limit.send(:limit).must_equal Zendesk::InboundMail::RateLimit::ACCOUNT_WHITELIST_LIMIT_MULTIPLIER * account.loop_threshold }

        describe "when account's loop threshold times the multiplier is higher than the ceiling" do
          before { account.loop_threshold = Zendesk::InboundMail::RateLimit::ACCOUNT_LOOP_THRESHOLD_CEILING }
          it { rate_limit.send(:limit).must_equal Zendesk::InboundMail::RateLimit::ACCOUNT_LOOP_THRESHOLD_CEILING }
        end
      end

      describe_with_arturo_disabled :email_account_whitelist_multiplier do
        it { rate_limit.send(:limit).must_equal Zendesk::InboundMail::RateLimit::ACCOUNT_WHITELIST_LIMIT }
      end
    end
  end

  describe "#exceeded?" do
    describe "when over the limit" do
      let(:recently_sent_email_count) { 21 }

      before { rate_limit.stubs(recently_sent_email_count: recently_sent_email_count) }

      describe 'and account sender rate limit is present' do
        before { account.inbound_mail_rate_limits.create(is_sender: true, email: sender, rate_limit: recently_sent_email_count + 1) }

        it { refute rate_limit.exceeded? }
      end

      describe 'and account recipient rate limit is present' do
        before { account.inbound_mail_rate_limits.create(is_sender: false, email: recipient, rate_limit: recently_sent_email_count + 1) }

        it { refute rate_limit.exceeded? }
      end

      describe 'and account sender and recipient rate limit are present' do
        before do
          account.inbound_mail_rate_limits.create(is_sender: true, email: sender, rate_limit: recently_sent_email_count + 1)
          account.inbound_mail_rate_limits.create(is_sender: false, email: recipient, rate_limit: recently_sent_email_count + 1)
        end

        it { refute rate_limit.exceeded? }
      end

      it { assert rate_limit.exceeded? }

      describe_with_arturo_disabled :email_inbound_mail_rate_limits do
        describe 'and account sender rate limit is present' do
          before { account.inbound_mail_rate_limits.create(is_sender: true, email: sender, rate_limit: recently_sent_email_count + 1) }

          it { assert rate_limit.exceeded? }
        end

        describe 'and account recipient rate limit is present' do
          before { account.inbound_mail_rate_limits.create(is_sender: false, email: recipient, rate_limit: recently_sent_email_count + 1) }

          it { assert rate_limit.exceeded? }
        end

        describe 'and account sender and recipient rate limit are present' do
          before do
            account.inbound_mail_rate_limits.create(is_sender: true, email: sender, rate_limit: recently_sent_email_count + 1)
            account.inbound_mail_rate_limits.create(is_sender: false, email: recipient, rate_limit: recently_sent_email_count + 1)
          end

          it { assert rate_limit.exceeded? }
        end
      end
    end

    describe "when under the limit" do
      before { rate_limit.stubs(recently_sent_email_count: 19) }
      it { refute rate_limit.exceeded? }
    end
  end

  describe "#greatly_exceeded?" do
    describe "when on the sender exception list" do
      # Whitelisted sender shiftmanager@jam.co.uk has a limit of 120
      let(:sender) { whitelisted_sender }

      describe "over 2 times the limit and on the exception list" do
        before { rate_limit.stubs(recently_sent_email_count: 241) }
        it { assert rate_limit.greatly_exceeded? }
      end

      describe "over 2 times the limit and on the exception list" do
        before { rate_limit.stubs(recently_sent_email_count: 239) }
        it { refute rate_limit.greatly_exceeded? }
      end
    end

    describe "over 2 times the normal limit" do
      before { rate_limit.stubs(recently_sent_email_count: 41) }
      it { assert rate_limit.greatly_exceeded? }
    end

    describe "under 2 times the normal limit" do
      before { rate_limit.stubs(recently_sent_email_count: 39) }
      it { refute rate_limit.greatly_exceeded? }
    end
  end

  describe "#message" do
    let(:duration) { Zendesk::InboundMail::RateLimit::DURATION }

    describe "when greatly exceeded" do
      before { rate_limit.stubs(recently_sent_email_count: 41) }
      it { rate_limit.message(true).must_equal "count 41 > threshold 40 mails in the last #{duration.inspect}." }
    end

    describe "when exceeded but not greatly exceeded" do
      before { rate_limit.stubs(recently_sent_email_count: 21) }
      it { rate_limit.message(false).must_equal "count 21 > threshold 20 mails in the last #{duration.inspect}." }
    end
  end
end
