require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 11

describe Zendesk::InboundMail::ProcessingState do
  include MailTestHelper
  fixtures :tickets, :users

  let(:account) { accounts(:minimum) }
  let(:state)  { Zendesk::InboundMail::ProcessingState.new(account) }
  let(:ticket) { tickets(:minimum_1) }

  describe "#flagged?" do
    it "is false when no flags are present" do
      state.flags.must_be_empty
      refute state.flagged?
    end

    it "is true when a flag is stored" do
      state.add_flag(EventFlagType.DEFAULT_TRUSTED)
      assert state.flagged?
    end
  end

  describe "#agent" do
    before do
      state.from = address(users(:minimum_agent).email)
    end

    it "is agent for agents" do
      assert_equal users(:minimum_agent), state.agent
    end

    it "is cached" do
      assert_equal users(:minimum_agent), state.agent
      assert_sql_queries(0) { assert_equal users(:minimum_agent), state.agent }
    end

    it "does not be agent for others" do
      state.from = address(users(:minimum_end_user).email)
      assert_equal false, state.agent
    end

    it "does not be agent for nil" do
      state.from = nil
      assert_equal false, state.agent
    end

    it "changes when from changess" do
      assert_equal users(:minimum_agent), state.agent
      state.from = address(users(:minimum_end_user).email)
      assert_equal false, state.agent
    end
  end

  describe "#from_header_whitelisted?" do
    it "returns true when from_header address is account whitelisted" do
      state.from_header_whitelisted = true
      assert state.from_header_whitelisted?
    end

    it "returns false when from_header address is not account whitelisted" do
      state.from_header_whitelisted = false
      refute state.from_header_whitelisted?
    end
  end

  describe "#content" do
    before { state.plain_content = "XXX" }

    it "is frozen" do
      assert state.plain_content.frozen?
    end

    it "unsets html when being set" do
      state.html_content  = "HTML"
      state.plain_content = "TEXT"
      state.html_content.must_be_nil
    end

    it "does not unset html when being set to the same" do
      state.html_content  = "HTML"
      state.plain_content = state.plain_content
      state.html_content.must_equal "HTML"
    end
  end

  describe "#suspended_ticket?" do
    before { state.ticket = SuspendedTicket.new }

    it "is a suspended ticket" do
      assert state.suspended_ticket?
    end
  end

  describe "#new_ticket?" do
    before { state.account = accounts(:minimum) }

    it "returns true when ticket is new and not saved" do
      state.ticket = Ticket.new
      assert state.new_ticket?
    end

    it "returns true when ticket is new and saved (just created)" do
      state.ticket = ticket
      state.mark_new_ticket
      assert state.instance_variable_get(:@new_ticket)
      assert state.new_ticket?
    end

    it "returns false when ticket is a followup" do
      state.ticket = ticket
      state.ticket.via_id = ViaType.CLOSED_TICKET
      refute state.new_ticket?
    end

    it "returns false when closed ticket is set" do
      state.closed_ticket = ticket
      refute state.new_ticket?
    end
  end

  describe "#followup_ticket?" do
    it "returns true when ticket is a followup" do
      state.ticket = ticket
      state.ticket.via_id = ViaType.CLOSED_TICKET
      assert state.followup_ticket?
    end

    it "returns true when closed ticket is set" do
      state.closed_ticket = ticket
      assert state.followup_ticket?
    end

    it "returns false when ticket is new and not saved" do
      state.ticket = Ticket.new
      refute state.followup_ticket?
    end

    it "returns false when ticket is new and saved (just created)" do
      state.ticket = ticket
      refute state.followup_ticket?
    end
  end

  describe "#comment?" do
    it "returns true when ticket already exists" do
      state.ticket = ticket
      assert state.comment?
    end

    it "returns false when ticket is a followup" do
      state.ticket = ticket
      state.ticket.via_id = ViaType.CLOSED_TICKET
      refute state.comment?
    end

    it "returns false when closed ticket is set" do
      state.closed_ticket = ticket
      refute state.comment?
    end

    it "returns false when ticket is new and not saved" do
      state.ticket = Ticket.new
      refute state.comment?
    end

    it "returns false when ticket is new and saved" do
      state.ticket = ticket
      state.mark_new_ticket
      refute state.comment?
    end
  end

  describe "#add_flag" do
    it "adds flag and options to state" do
      flag = EventFlagType.ATTACHMENT_TOO_BIG
      state.add_flag(flag, :example_options)
      assert_equal state.flags, AuditFlags.new([EventFlagType.ATTACHMENT_TOO_BIG])
      assert_equal state.flags_options, EventFlagType.ATTACHMENT_TOO_BIG => :example_options
    end
  end

  describe "#group_restricted_submitter?" do
    it "is true if submitter is group-restricted agent" do
      state.submitter = users(:with_groups_agent_groups_restricted)
      assert state.group_restricted_submitter?
    end

    it "is false if submitter is a non-restricted agent" do
      state.submitter = users(:with_groups_agent1)
      refute state.group_restricted_submitter?
    end

    it "is false if submitter is an organization-restricted agent" do
      state.submitter = users(:with_groups_agent_organization_restricted)
      refute state.group_restricted_submitter?
    end

    it "is false if submitter is an end-user" do
      state.submitter = users(:with_groups_end_user)
      refute state.group_restricted_submitter?
    end
  end

  describe "#record_deferred_stat" do
    it "stores stat" do
      state.record_deferred_stat("key", tags: "foo:bar")
      state.deferred_stats["key"].must_equal tags: "foo:bar"
    end
  end

  describe "#add_saved_new_users" do
    it "adds a single user" do
      user = users(:minimum_end_user)
      state.add_saved_new_users(user)
      state.saved_new_users.must_equal [user]
    end

    it "adds and flattens an array of users" do
      users = [users(:minimum_end_user), users(:minimum_end_user2)]
      state.add_saved_new_users(users)
      state.saved_new_users.must_equal users
    end

    it "does not add nil" do
      state.add_saved_new_users(nil)
      state.saved_new_users.must_be_empty
    end

    it "does not add nil when more than one user is supplied" do
      users = [users(:minimum_end_user), nil]
      state.add_saved_new_users(users)
      state.saved_new_users.must_equal [users(:minimum_end_user)]
    end
  end

  describe "author_is_email_cc?" do
    let (:author) { users(:minimum_author) }

    it "is false if ticket is nil" do
      refute state.author_is_email_cc?
    end

    it "is false if ticket is not nil but author is nil" do
      state.ticket = ticket
      refute state.author_is_email_cc?
    end

    it "does not memoize the return value" do
      ticket.stubs(:is_email_cc?).returns(true)

      state.ticket = ticket
      refute state.author_is_email_cc?

      state.author = author
      assert state.author_is_email_cc?
    end

    describe "when ticket and author are non-nil" do
      before do
        state.ticket = ticket
        state.author = users(:minimum_author)
      end

      it "is false if Ticket#is_email_cc? returns false for author" do
        ticket.expects(:is_email_cc?).returns(false)
        refute state.author_is_email_cc?
      end

      it "is true if Ticket#is_email_cc? returns true for author" do
        ticket.expects(:is_email_cc?).returns(true)
        assert state.author_is_email_cc?
      end

      it "memoizes the return value" do
        ticket.stubs(:is_email_cc?).returns(false)
        refute state.author_is_email_cc?

        ticket.unstub(:is_email_cc?)
        ticket.stubs(:is_email_cc?).returns(true)
        refute state.author_is_email_cc?
      end
    end
  end

  describe "author_is_follower?" do
    let (:author) { users(:minimum_author) }

    it "is false if ticket is nil" do
      refute state.author_is_follower?
    end

    it "is false if ticket is not nil but author is nil" do
      state.ticket = ticket
      refute state.author_is_follower?
    end

    it "does not memoize the return value" do
      ticket.stubs(:is_follower?).returns(true)

      state.ticket = ticket
      refute state.author_is_follower?

      state.author = author
      assert state.author_is_follower?
    end

    describe "when ticket and author are non-nil" do
      before do
        state.ticket = ticket
        state.author = users(:minimum_author)
      end

      it "is false if Ticket#is_follower? returns false for author" do
        ticket.expects(:is_follower?).returns(false)
        refute state.author_is_follower?
      end

      it "is true if Ticket#is_follower? returns true for author" do
        ticket.expects(:is_follower?).returns(true)
        assert state.author_is_follower?
      end

      it "memoizes the return value" do
        ticket.stubs(:is_follower?).returns(false)
        refute state.author_is_follower?

        ticket.unstub(:is_follower?)
        ticket.stubs(:is_follower?).returns(true)
        refute state.author_is_follower?
      end
    end
  end
end
