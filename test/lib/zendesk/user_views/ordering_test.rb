require_relative "../../../support/test_helper"
require_relative "../../../support/user_views_test_helper"
require_relative "../../../support/custom_fields_test_helper"
require 'zendesk/user_views/ordering'

SingleCov.covered! uncovered: 29

describe 'Ordering' do
  fixtures :accounts, :users
  include UserViewsTestHelper
  include CustomFieldsTestHelper
  include Api::V2::Rules::UserViewRowsHelper

  describe Zendesk::UserViews::RolesOrdering do
    let(:account) { accounts(:minimum) }
    let(:current_user) { account.owner }

    let(:permission_set_1) { account.permission_sets.create!(name: 'Permission Set 1') }
    let(:permission_set_2) { account.permission_sets.create!(name: 'Permission Set 2') }
    let(:permission_set_3) { account.permission_sets.create!(name: 'Permission Set 3') }

    let(:agent_2) do
      FactoryBot.create(:agent,
        account: account,
        name: "Agent 2",
        created_at: 1.year.ago)
    end
    let(:agent_3) do
      FactoryBot.create(:agent,
        account: account,
        name: "Agent 3",
        created_at: 2.years.ago)
    end
    let(:agent_4) do
      FactoryBot.create(:agent,
        account: account,
        name: "Agent 4",
        created_at: 3.years.ago)
    end
    let(:admin_2) do
      FactoryBot.create(:verified_admin,
        account: account,
        name: "Verified Admin 2",
        created_at: 4.years.ago)
    end

    let(:end_user_with_permission_set_1) do
      FactoryBot.create(:end_user,
        account: account,
        name: "End User With Permission Set 1",
        created_at: 5.years.ago).tap do |user|
        user.permission_set = permission_set_1
        user.save
      end
    end
    let(:admin_with_permission_set_2) do
      FactoryBot.create(:verified_admin,
        account: account,
        name: "Admin With Permission Set 2",
        created_at: 6.years.ago).tap do |user|
        user.permission_set = permission_set_2
        user.save
      end
    end

    let(:earliest_user) { @account.users.order('created_at asc').first }

    before do
      Account.any_instance.stubs(:has_user_views_negative_operators_enabled?).returns(true)
      Account.any_instance.stubs(:has_user_views?).returns(true)

      @account = account
      @admin = users(:minimum_admin)
      @agent = users(:minimum_agent)
      @end_user = users(:minimum_end_user)

      @admin.update_attribute(:created_at, 3.days.ago)
      @agent.update_attribute(:created_at, 1.days.ago)
      @end_user.update_attribute(:created_at, 2.days.ago)

      account.subscription.update_attributes!(base_agents: 30)

      agent_2.role_or_permission_set = permission_set_1
      agent_2.save!
      agent_3.role_or_permission_set = permission_set_2
      agent_3.save!
      agent_4.role_or_permission_set = permission_set_3
      agent_4.save!

      admin_2
      end_user_with_permission_set_1
      admin_with_permission_set_2
    end

    let(:user_view_executer) { Zendesk::UserViews::Executer.new(user_view, current_user, per_page: 30) }
    let(:found_users) { user_view_executer.find_users }

    ['group', 'sort'].each do |type|
      describe "ordering #{type}" do
        let(:user_view) do
          create_view setup_params("all" => [{ "field" => "created_at", "operator" => "greater_than_or_equal_to", "value" => earliest_user.created_at }],
                                   "execution" => {
              type => ordering,
              "columns" => ["id", "name", "roles"]
            })
        end

        describe "ascending" do
          let(:ordering) { {"id" => "roles", "order" => "asc"} }

          it "is in the right order" do
            assert_equal 19, found_users.length
            expected_order = [Role::END_USER, Role::ADMIN, Role::AGENT].map(&:display_name)
            actual_order = found_users.each_with_object([]) do |user, memo|
              roles = roles_field_value(user)
              memo << roles if roles != memo.last
            end
            assert_equal expected_order, actual_order
          end
        end

        describe "descending" do
          let(:ordering) { {"id" => "roles", "order" => "desc"} }

          it "is in the right order" do
            assert_equal 19, found_users.length
            expected_order = [Role::END_USER, Role::ADMIN, Role::AGENT].map(&:display_name).reverse
            actual_order = found_users.each_with_object([]) do |user, memo|
              roles = roles_field_value(user)
              memo << roles if roles != memo.last
            end
            assert_equal expected_order, actual_order
          end
        end
      end
    end

    describe "with enterprise_roles_in_user_views enabled" do
      before do
        Account.any_instance.stubs(:enterprise_roles_in_user_views_enabled?).returns(true)
        Account.any_instance.stubs(:has_permission_sets?).returns(true)
      end

      ['group', 'sort'].each do |type|
        describe "ordering #{type}" do
          let(:user_view) do
            create_view setup_params("all" => [{ "field" => "created_at", "operator" => "greater_than_or_equal_to", "value" => earliest_user.created_at }],
                                     "execution" => {
                type => ordering,
                "columns" => ["id", "name", "roles"]
              })
          end

          before do
            count = 0
            roles = [
              permission_set_1,
              permission_set_2,
              permission_set_3
            ]
            # This will round-robin assign each permission set to all 15 users.
            # Skipping every 4th, 5th, and 6th user.
            account.users.find_each do |user|
              index = count % 6
              if set = roles[index]
                user.roles = Role::AGENT.id
                user.permission_set = set
                user.save!
              end

              count += 1
            end
          end

          let(:roles_order) { ['End user', 'Administrator', 'Agent', 'Permission Set 1', 'Permission Set 2', 'Permission Set 3'] }

          describe "ascending" do
            let(:ordering) { {"id" => "roles", "order" => "asc"} }

            it "is in the right order" do
              assert_equal 19, found_users.length
              actual_order = found_users.each_with_object([]) do |user, memo|
                roles = roles_field_value(user)
                memo << roles if roles != memo.last
              end
              assert_equal roles_order, actual_order
            end
          end

          describe "descending" do
            let(:ordering) { {"id" => "roles", "order" => "desc"} }

            it "is in the right order" do
              assert_equal 19, found_users.length
              actual_order = found_users.each_with_object([]) do |user, memo|
                roles = roles_field_value(user)
                memo << roles if roles != memo.last
              end
              assert_equal roles_order.reverse, actual_order
            end
          end
        end
      end

      describe "group by roles and sort by name" do
        let(:end_user_without_permission) do
          FactoryBot.create(:end_user,
            account: @account,
            name: "End User Without Permission Set",
            created_at: 6.years.ago)
        end
        let(:z_agent_without_permission) do
          FactoryBot.create(:agent,
            account: @account,
            name: "Z Agent Without Permission Set",
            created_at: 6.years.ago)
        end

        before do
          end_user_without_permission
          z_agent_without_permission
        end

        let(:roles_order) { ['End user', 'Administrator', 'Agent', 'Permission Set 1', 'Permission Set 2', 'Permission Set 3'] }

        let(:in_order) do
          [
            ["End user", "Author Minimum"],
            ["End user", "Author Minimum Suspend"],
            ["End user", "End User With Permission Set 1"],
            ["End user", "End User Without Permission Set"],
            ["End user", "minimum_end_user"],
            ["End user", "minimum_end_user2"],
            ["End user", "minimum_search_user"],
            ["End user", "Photo User"],
            ["End user", "Ted Cassasa"],
            ["End user", "to_delete_end_user"],
            ["End user", "verify_test"],
            ["Administrator", "Admin Man"],
            ["Administrator", "Admin With Permission Set 2"],
            ["Administrator", "No Verified Admin Man"],
            ["Administrator", "Not owner man"],
            ["Administrator", "Verified Admin 2"],
            ["Agent", "Agent Minimum"],
            ["Agent", "Z Agent Without Permission Set"],
            ["Permission Set 1", "Agent 2"],
            ["Permission Set 2", "Agent 3"],
            ["Permission Set 3", "Agent 4"]
          ]
        end

        { 'asc' => 1, 'desc' => -1 }.each do |group_order, group_comp|
          { 'asc' => 1, 'desc' => -1 }.each do |sort_order, sort_comp|
            describe "when group #{group_order}, sort #{sort_order}" do
              let(:user_view) do
                create_view setup_params("all" => [{ "field" => "created_at", "operator" => "greater_than_or_equal_to", "value" => earliest_user.created_at }],
                                         "execution" => {
                    "group" => {"id" => "roles", "order" => group_order},
                    "sort" => {"id" => "name", "order" => sort_order},
                    "columns" => ["id", "name", "roles"]
                  })
              end

              it "is in order" do
                assert_equal 21, found_users.length
                expected_order = in_order.sort do |a, b|
                  cmp = group_comp * (roles_order.index(a.first) <=> roles_order.index(b.first))
                  cmp = sort_comp * (a.last.downcase <=> b.last.downcase) if cmp.zero?
                  cmp
                end

                actual_order = found_users.map do |user|
                  roles = roles_field_value(user)
                  [roles, user.name]
                end

                assert_equal expected_order, actual_order
              end
            end
          end
        end
      end
    end
  end

  describe Zendesk::UserViews::TimeZoneOrdering do
    let(:account) { accounts(:minimum) }
    let(:earliest_user) { account.users.order('created_at asc').first }
    let(:orderer) { Zendesk::UserViews::TimeZoneOrdering.new({"id" => "time_zone", "order" => "asc"}, nil, nil) }
    let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, params).query }

    before do
      @account = account
    end

    ['group', 'sort'].each do |type|
      describe "when #{type}ing in view" do
        let(:user_view) do
          create_view setup_params(
            "all" => [{ "field" => "created_at", "operator" => "greater_than_or_equal_to", "value" => earliest_user.created_at }],
            "execution" => {
              type => {"id" => "time_zone", "order" => order},
              "columns" => ["id", "name", "time_zone"]
            }
          )
        end
        let(:params) { { } }

        describe "and order is ascending" do
          let(:order) { "asc" }

          it "applies correct ordering" do
            assert_includes query, "ORDER BY IFNULL(users.time_zone, '#{@account.time_zone}') asc"
          end
        end

        describe "and order is descending" do
          let(:order) { "desc" }

          it "applies correct ordering" do
            assert_includes query, "ORDER BY IFNULL(users.time_zone, '#{@account.time_zone}') desc"
          end
        end
      end

      describe "when #{type}ing in params" do
        let(:user_view) do
          create_view setup_params(
            "all" => [{ "field" => "created_at", "operator" => "greater_than_or_equal_to", "value" => earliest_user.created_at }],
            "execution" => {
              "columns" => ["id", "name", "time_zone"]
            }
          )
        end
        let(:params) do
          {
            page: 1,
            per_page: 30,
            sort_by: "time_zone",
            sort_order: order,
            subsystem_name: nil,
            sampling: nil
          }
        end

        describe "and order is ascending" do
          let(:order) { "asc" }

          it "applies correct ordering" do
            assert_includes query, "ORDER BY IFNULL(users.time_zone, '#{@account.time_zone}') asc"
          end
        end

        describe "and order is descending" do
          let(:order) { "desc" }

          it "applies correct ordering" do
            assert_includes query, "ORDER BY IFNULL(users.time_zone, '#{@account.time_zone}') desc"
          end
        end
      end
    end

    it "is not reusable" do
      refute orderer.reusable?
    end
  end

  describe 'old ordering tests' do
    let(:account) { accounts(:minimum) }
    let(:current_user) { account.owner }

    let(:permission_set_1) { account.permission_sets.create!(name: 'Permission Set 1') }
    let(:permission_set_2) { account.permission_sets.create!(name: 'Permission Set 2') }
    let(:permission_set_3) { account.permission_sets.create!(name: 'Permission Set 3') }

    let(:agent_2) do
      FactoryBot.create(:agent,
        account: account,
        name: "Agent 2",
        created_at: 1.year.ago)
    end
    let(:agent_3) do
      FactoryBot.create(:agent,
        account: account,
        name: "Agent 3",
        created_at: 2.years.ago)
    end
    let(:agent_4) do
      FactoryBot.create(:agent,
        account: account,
        name: "Agent 4",
        created_at: 3.years.ago)
    end
    let(:admin_2) do
      FactoryBot.create(:verified_admin,
        account: account,
        name: "Verified Admin 2",
        created_at: 4.years.ago)
    end

    let(:end_user_with_permission_set_1) do
      FactoryBot.create(:end_user,
        account: account,
        name: "End User With Permission Set 1",
        created_at: 5.years.ago).tap do |user|
        user.permission_set = permission_set_1
        user.save
      end
    end
    let(:admin_with_permission_set_2) do
      FactoryBot.create(:verified_admin,
        account: account,
        name: "Admin With Permission Set 2",
        created_at: 6.years.ago).tap do |user|
        user.permission_set = permission_set_2
        user.save
      end
    end

    let(:earliest_user) { @account.users.order('created_at asc').first }

    before do
      Account.any_instance.stubs(:has_user_views_negative_operators_enabled?).returns(true)
      Account.any_instance.stubs(:has_user_views?).returns(true)

      @account = account

      @admin = users(:minimum_admin)
      @agent = users(:minimum_agent)
      @end_user = users(:minimum_end_user)

      @admin.update_attribute(:created_at, 3.days.ago)
      @agent.update_attribute(:created_at, 1.days.ago)
      @end_user.update_attribute(:created_at, 2.days.ago)

      account.subscription.update_attributes!(base_agents: 30)

      agent_2.role_or_permission_set = permission_set_1
      agent_2.save!
      agent_3.role_or_permission_set = permission_set_2
      agent_3.save!
      agent_4.role_or_permission_set = permission_set_3
      agent_4.save!

      admin_2
      end_user_with_permission_set_1
      admin_with_permission_set_2
    end

    let(:user_view_executer) { Zendesk::UserViews::Executer.new(user_view, current_user, per_page: 30) }
    let(:found_users) { user_view_executer.find_users }

    ['group', 'sort'].each do |type|
      describe "ordering #{type}" do
        let(:user_view) do
          create_view setup_params(
            "all" => [{ "field" => "created_at", "operator" => "greater_than_or_equal_to", "value" => earliest_user.created_at }],
            "execution" => {
              type => ordering,
              "columns" => ["id", "name", "roles"]
            }
          )
        end

        describe "ascending" do
          let(:ordering) { {"id" => "roles", "order" => "asc"} }

          it "is in the right order" do
            assert_equal 19, found_users.length
            expected_order = [Role::END_USER, Role::ADMIN, Role::AGENT].map(&:display_name)
            actual_order = found_users.each_with_object([]) do |user, memo|
              roles = roles_field_value(user)
              memo << roles if roles != memo.last
            end
            assert_equal expected_order, actual_order
          end
        end

        describe "descending" do
          let(:ordering) { {"id" => "roles", "order" => "desc"} }

          it "is in the right order" do
            assert_equal 19, found_users.length
            expected_order = [Role::END_USER, Role::ADMIN, Role::AGENT].map(&:display_name).reverse
            actual_order = found_users.each_with_object([]) do |user, memo|
              roles = roles_field_value(user)
              memo << roles if roles != memo.last
            end
            assert_equal expected_order, actual_order
          end
        end
      end
    end

    describe "with enterprise_roles_in_user_views enabled" do
      before do
        Account.any_instance.stubs(:enterprise_roles_in_user_views_enabled?).returns(true)
        Account.any_instance.stubs(:has_permission_sets?).returns(true)
      end

      ['group', 'sort'].each do |type|
        describe "ordering #{type}" do
          let(:user_view) do
            create_view setup_params(
              "all" => [{ "field" => "created_at", "operator" => "greater_than_or_equal_to", "value" => earliest_user.created_at }],
              "execution" => {
                type => ordering,
                "columns" => ["id", "name", "roles"]
              }
            )
          end

          before do
            count = 0
            roles = [
              permission_set_1,
              permission_set_2,
              permission_set_3
            ]
            # This will round-robin assign each permission set to all 15 users.
            # Skipping every 4th, 5th, and 6th user.
            account.users.find_each do |user|
              index = count % 6
              if set = roles[index]
                user.roles = Role::AGENT.id
                user.permission_set = set
                user.save!
              end

              count += 1
            end
          end

          let(:roles_order) { ['End user', 'Administrator', 'Agent', 'Permission Set 1', 'Permission Set 2', 'Permission Set 3'] }

          describe "ascending" do
            let(:ordering) { {"id" => "roles", "order" => "asc"} }

            it "is in the right order" do
              assert_equal 19, found_users.length
              actual_order = found_users.each_with_object([]) do |user, memo|
                roles = roles_field_value(user)
                memo << roles if roles != memo.last
              end
              assert_equal roles_order, actual_order
            end
          end

          describe "descending" do
            let(:ordering) { {"id" => "roles", "order" => "desc"} }

            it "is in the right order" do
              assert_equal 19, found_users.length
              actual_order = found_users.each_with_object([]) do |user, memo|
                roles = roles_field_value(user)
                memo << roles if roles != memo.last
              end
              assert_equal roles_order.reverse, actual_order
            end
          end
        end
      end

      describe "group by roles and sort by name" do
        let(:end_user_without_permission) do
          FactoryBot.create(:end_user,
            account: @account,
            name: "End User Without Permission Set",
            created_at: 6.years.ago)
        end
        let(:z_agent_without_permission) do
          FactoryBot.create(:agent,
            account: @account,
            name: "Z Agent Without Permission Set",
            created_at: 6.years.ago)
        end

        before do
          end_user_without_permission
          z_agent_without_permission
        end

        let(:roles_order) { ['End user', 'Administrator', 'Agent', 'Permission Set 1', 'Permission Set 2', 'Permission Set 3'] }

        let(:in_order) do
          [
            ["End user", "Author Minimum"],
            ["End user", "Author Minimum Suspend"],
            ["End user", "End User With Permission Set 1"],
            ["End user", "End User Without Permission Set"],
            ["End user", "minimum_end_user"],
            ["End user", "minimum_end_user2"],
            ["End user", "minimum_search_user"],
            ["End user", "Photo User"],
            ["End user", "Ted Cassasa"],
            ["End user", "to_delete_end_user"],
            ["End user", "verify_test"],
            ["Administrator", "Admin Man"],
            ["Administrator", "Admin With Permission Set 2"],
            ["Administrator", "No Verified Admin Man"],
            ["Administrator", "Not owner man"],
            ["Administrator", "Verified Admin 2"],
            ["Agent", "Agent Minimum"],
            ["Agent", "Z Agent Without Permission Set"],
            ["Permission Set 1", "Agent 2"],
            ["Permission Set 2", "Agent 3"],
            ["Permission Set 3", "Agent 4"]
          ]
        end

        { 'asc' => 1, 'desc' => -1 }.each do |group_order, group_comp|
          { 'asc' => 1, 'desc' => -1 }.each do |sort_order, sort_comp|
            describe "when group #{group_order}, sort #{sort_order}" do
              let(:user_view) do
                create_view setup_params(
                  "all" => [{ "field" => "created_at", "operator" => "greater_than_or_equal_to", "value" => earliest_user.created_at }],
                  "execution" => {
                    "group" => {"id" => "roles", "order" => group_order},
                    "sort" => {"id" => "name", "order" => sort_order},
                    "columns" => ["id", "name", "roles"]
                  }
                )
              end

              it "is in order" do
                assert_equal 21, found_users.length
                expected_order = in_order.sort do |a, b|
                  cmp = group_comp * (roles_order.index(a.first) <=> roles_order.index(b.first))
                  cmp = sort_comp * (a.last.downcase <=> b.last.downcase) if cmp.zero?
                  cmp
                end

                actual_order = found_users.map do |user|
                  roles = roles_field_value(user)
                  [roles, user.name]
                end

                assert_equal expected_order, actual_order
              end
            end
          end
        end
      end
    end
  end
end
