require_relative "../../../support/test_helper"
require_relative "../../../support/user_views_test_helper"
require 'zendesk/rules/user_view_initializer'
require 'zendesk/user_views/executer'

SingleCov.covered! uncovered: 5

describe 'Caster' do
  include CustomFieldsTestHelper
  fixtures :accounts, :users, :organizations, :cf_fields

  before do
    @account = accounts(:minimum)
    @caster = Zendesk::UserViews::Caster
  end

  describe "cast_value" do
    it "returns nil if the value is nil" do
      assert_nil @caster.new(@account, "Text", "is", nil, "name").cast_value
    end

    it "returns empty string for when operators are present operators" do
      ["is", "is_not", "is_present", "is_not_present"].each do |operator|
        assert_equal "", @caster.new(@account, "Text", operator, "", "name").cast_value
      end
    end

    describe "date fields" do
      before do
        @field = CustomField::Date.first
      end

      it "rescues invalid date conditions" do
        refute @caster.new(@account, "Date", "greater_than", "tomorrow", @field['key'], @field).cast_value
        refute @caster.new(@account, :datetime, "greater_than", "yesterday", "created_at").cast_value
      end

      it "allows valid date conditions for system fields" do
        assert_equal Time.parse("2001-02-01"), @caster.new(@account, :datetime, "less_than", "1/2/2001", "created_at").cast_value
        assert_equal Time.parse("2001-02-01").end_of_day, @caster.new(@account, :datetime, "greater_than", "1/2/2001", "created_at").cast_value
        assert_equal Time.parse("2001-02-01").end_of_day, @caster.new(@account, :datetime, "less_than_or_equal_to", "1/2/2001", "created_at").cast_value
      end

      it "allows valid date conditions for custom fields" do
        assert_equal "2001-02-01", @caster.new(@account, "Date", "greater_than", "1/2/2001", @field['key'], @field).cast_value
      end

      it "does not allow dates for within_the_next or within_the_previous" do
        refute @caster.new(@account, "Date", "within_next_n_days", "1/2/2001", @field['key'], @field).cast_value
        refute @caster.new(@account, :datetime, "within_previous_n_days", "1/2/2001", "created_at").cast_value
      end

      it "allows numbers for within_the_next or within_the_previous" do
        assert_equal 20, @caster.new(@account, "Date", "within_next_n_days", "20", @field['key'], @field).cast_value
        assert_equal 40, @caster.new(@account, :datetime, "within_previous_n_days", "40", "created_at").cast_value
      end

      it "does not allow numbers when operator is not within_the_next or within_the_previous" do
        refute @caster.new(@account, "Date", "is", 20, @field['key'], @field).cast_value
        refute @caster.new(@account, :datetime, "is_not", 40, "created_at").cast_value
      end

      it "does not allow dates with presence operators" do
        ["is", "is_not", "is_present", "is_not_present"].each do |operator|
          refute @caster.new(@account, "Date", operator, "", @field['key'], @field).cast_value
        end
      end

      it "does allow dates with presence operator for last_login" do
        assert @caster.new(@account, "Date", "is_present", "", "last_login", @field).cast_value
      end
    end

    describe "custom fields" do
      it "handles integer fields" do
        @field = create_user_custom_field!("integer", "Integer", "Integer")
        assert_equal Zendesk::CustomField::NumberPadding.pad(20), @caster.new(@account, "Integer", "greater_than", "20", @field['key'], @field).cast_value
        refute @caster.new(@account, "Integer", "greater_than", "abc", @field['key'], @field).cast_value
      end

      it "handles decimal fields" do
        @field = create_user_custom_field!("decimal", "Decimal", "Decimal")
        assert_equal Zendesk::CustomField::NumberPadding.pad(20.5), @caster.new(@account, "Decimal", "greater_than", "20.5", @field['key'], @field).cast_value
        refute @caster.new(@account, "Decimal", "greater_than", "abc", @field['key'], @field).cast_value
      end

      it "handles text fields" do
        @field = create_user_custom_field!("text", "Text", "Text")
        assert_equal "abc", @caster.new(@account, "Text", "is_not", "abc", @field['key'], @field).cast_value
      end

      it "handles textarea fields" do
        @field = create_user_custom_field!("textarea", "Textarea", "Textarea")
        assert_equal "abc", @caster.new(@account, "Textarea", "is", "abc", @field['key'], @field).cast_value
      end

      it "handles regexp fields" do
        @field = create_user_custom_field!("regexp", "Regexp", "Regexp")
        assert_equal "abc", @caster.new(@account, "Regexp", "is", "abc", @field['key'], @field).cast_value
      end

      it "handles checkbox field" do
        @field = create_user_custom_field!("checkbox", "Checkbox", "Checkbox")
        assert_equal "1", @caster.new(@account, "Checkbox", "is", "true", @field['key'], @field).cast_value
        refute @caster.new(@account, "Checkbox", "is", "false", @field['key'], @field).cast_value
      end

      it "does not allow blank values" do
        @field = create_user_custom_field!("integer", "Integer", "Integer")
        assert_equal Zendesk::CustomField::NumberPadding.pad(20), @caster.new(@account, "Integer", "greater_than", "20", @field['key'], @field).cast_value
        refute @caster.new(@account, "Integer", "greater_than", nil, @field['key'], @field).cast_value
      end
    end

    it "handles organizations" do
      org = @account.organizations.first
      assert_equal org.id, @caster.new(@account, "Dropdown", "is", org.id.to_s, "organization_id").cast_value
      refute @caster.new(@account, "Dropdown", "is", "abcdef", "organization_id").cast_value
    end

    it "handles locales" do
      locale = @account.available_languages.first
      assert_equal locale.id, @caster.new(@account, "Dropdown", "is", locale.id.to_s, "locale_id").cast_value
      refute @caster.new(@account, "Dropdown", "is", "abcdef", "locale_id").cast_value
    end

    it "handles roles" do
      assert_equal Role::AGENT.id, @caster.new(@account, "Dropdown", "is", "agent", "roles").cast_value
      assert_equal Role::END_USER.id, @caster.new(@account, "Dropdown", "is", "end-user", "roles").cast_value
      assert_equal Role::ADMIN.id, @caster.new(@account, "Dropdown", "is", "admin", "roles").cast_value
      assert_equal 123, @caster.new(@account, "Dropdown", "is", "permission_set_id:123", "roles").cast_value
      refute @caster.new(@account, "Dropdown", "is", "sdfsdf", "roles").cast_value
    end
  end
end
