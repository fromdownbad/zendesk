require_relative "../../../support/test_helper"
require_relative "../../../support/user_views_test_helper"
require_relative "../../../support/custom_fields_test_helper"

SingleCov.covered! uncovered: 21

describe 'QueryBuilder' do
  fixtures :accounts, :users, :organizations, :role_settings, :credit_cards
  include UserViewsTestHelper
  include CustomFieldsTestHelper

  before do
    @account = accounts(:minimum)
    Arturo.enable_feature!(:user_views_negative_operators_enabled)
    Arturo.enable_feature!(:user_views_tag_queries_without_forcing_index)
  end

  describe "suspended users" do
    before do
      @view = create_view setup_params(
        "title" => "All Users",
        "all" => [{ "field" => "name", "operator" => "is_not", "value" => nil }]
      )
      @user = @account.users.first
    end

    describe "user is not suspended" do
      it "includes user" do
        query = Zendesk::UserViews::QueryBuilder.new(@view, {}).query
        results = User.find_by_sql(query)
        assert results.include?(@user)
      end
    end

    describe "user is suspended" do
      before do
        @user.settings.suspended = true
        @user.save!
      end

      it "excludes user" do
        query = Zendesk::UserViews::QueryBuilder.new(@view, {}).query
        results = User.find_by_sql(query)
        refute results.include?(@user)
      end
    end
  end

  describe "with where condition sampling" do
    before do
      # Should return all users
      @view = create_view setup_params("title" => "All Users",
                                       "all" => [{ "field" => "name", "operator" => "is_not", "value" => nil }])
      @users = @account.users.first(11)
      @users.each_with_index { |user, i| user.update_column(:sample, 1000 * i) }
    end

    describe "when has_user_views_sampling_enabled is true" do
      before do
        Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(true)
      end

      describe "when not skipping sampling" do
        it "returns a non sampling query when the user count is less than 100_000" do
          @account.settings.user_count = UserView::SAMPLING_THRESHOLD - 1
          @account.save!
          query = Zendesk::UserViews::QueryBuilder.new(@view, {}).query
          results = User.find_by_sql(query)
          @users.each { |user| assert results.include?(user) }
        end

        it "returns a sampling query when the user count is greater than 100_000" do
          # User count: 100001, sample values with sample <= 9999
          @account.settings.user_count = UserView::SAMPLING_THRESHOLD + 1
          @account.save!
          query = Zendesk::UserViews::QueryBuilder.new(@view, {}).query
          assert query.include?("SELECT  `users`.* FROM `users` USE INDEX(`index_users_on_account_id_and_sample`)")
          results = User.find_by_sql(query)
          @users[0..9].each { |user| assert results.include?(user) }
          refute results.include?(@users.last)
        end

        describe "when skipping use index" do
          it "returns a sampling query without the use index" do
            # User count: 100001, sample values with sample <= 9999
            @account.settings.user_count = UserView::SAMPLING_THRESHOLD + 1
            @account.save!
            query = Zendesk::UserViews::QueryBuilder.new(@view, skip_use_index: true).query
            refute query.include?("USE INDEX(`index_users_on_account_id_and_sample`)")
            results = User.find_by_sql(query)
            @users[0..9].each { |user| assert results.include?(user) }
            refute results.include?(@users.last)
          end
        end

        describe "when doing tag queries" do
          before do
            Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
            # User count: 100001, sample values with sample <= 9999
            @account.settings.user_count = UserView::SAMPLING_THRESHOLD + 1
            @account.save!
            @view = create_view setup_params("title" => "Users with tag",
                                             "all" => [{ "field" => "current_tags", "operator" => "includes", "value" => ["tag"] }])
          end

          it "does not force index when querying tags" do
            query = Zendesk::UserViews::QueryBuilder.new(@view, {}).query
            refute query.include?("SELECT  `users`.* FROM `users` USE INDEX(`index_users_on_account_id_and_sample`)")
          end

          it "forces index when querying tags and Arturo is off" do
            Arturo.disable_feature!(:user_views_tag_queries_without_forcing_index)
            query = Zendesk::UserViews::QueryBuilder.new(@view, {}).query
            assert query.include?("SELECT  `users`.* FROM `users` USE INDEX(`index_users_on_account_id_and_sample`)")
          end
        end

        it "returns a sampling query with the correct sample threshold when the user count is greater than 100_000" do
          # User count: 500k, sample values with sample <= 2000
          @account.settings.user_count = UserView::SAMPLING_THRESHOLD * 5
          @account.save!
          query = Zendesk::UserViews::QueryBuilder.new(@view, {}).query
          results = User.find_by_sql(query)
          @users[0..2].each { |user| assert results.include?(user) }
          @users[3..10].each { |user| refute results.include?(user) }

          # User count: 10 mil, sample values with sample <= 100
          @account.settings.user_count = UserView::SAMPLING_THRESHOLD * 100
          @account.save!
          query = Zendesk::UserViews::QueryBuilder.new(@view, {}).query
          results = User.find_by_sql(query)
          assert results.include?(@users.first)
          @users[1..10].each { |user| refute results.include?(user) }
        end
      end

      describe "when skipping sampling" do
        it "returns a non sampling query when the user count is greater than 100_000" do
          @account.settings.user_count = UserView::SAMPLING_THRESHOLD + 1
          @account.save!

          query = Zendesk::UserViews::QueryBuilder.new(@view, sampling: false).query
          results = User.find_by_sql(query)
          @users.each { |user| assert results.include?(user) }
        end
      end
    end

    describe "when has_user_views_sampling_enabled is false" do
      before do
        Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(false)
      end

      it "returns a non sampling query when the user count is greater than 100_000" do
        @account.settings.user_count = UserView::SAMPLING_THRESHOLD + 1
        @account.save!

        query = Zendesk::UserViews::QueryBuilder.new(@view, {}).query
        results = User.find_by_sql(query)
        @users.each { |user| assert results.include?(user) }
      end
    end
  end

  describe "reusing inner joins" do
    let(:account_users_set) { Set.new(@account.users) }

    ['group', 'sort'].each do |type|
      describe "custom fields" do
        describe "filtering and #{type}ing by dropdown field" do
          let(:user_view) do
            create_view setup_params("title" => "Filtering and #{type}ing by dropdown field",
                                     "all" => [{"field" => "custom_fields.plan_type", "operator" => "is_present" }],
                                     "execution" => {
                type => {"id" => "custom_fields.plan_type", "order" => "asc"}
              })
          end
          let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, {}).query }
          let(:results) { User.find_by_sql(query) }
          let(:by_name_and_plan_type) do
            lambda do |user|
              [user.name, user.custom_field_values.value_for_key("plan_type").as_json["value"]]
            end
          end
          let(:expected_natural_ordering) do
            i = 0
            @account.users.each_with_object({}) do |o, m|
              m[o.id] = i
              i += 1
            end
          end

          before do
            dropdown_choices = [
              { name: "Gold",        value: "gold" },
              { name: "Silver",      value: "silver" },
              { name: "Bronze",      value: "bronze" }
            ]
            create_dropdown_field("plan_type", "Plan", dropdown_choices)
            @account.users.each do |user|
              dropdown_choice = dropdown_choices[user.id % 3]
              user.custom_field_values.update_from_hash("plan_type" => dropdown_choice[:value])
              user.save!
            end
          end

          it "does not include LEFT OUTER JOIN" do
            refute_includes query, "LEFT OUTER JOIN `cf_values` `#{type}_by_custom_field_value"
            refute_includes query, "LEFT OUTER JOIN `cf_dropdown_choices` `#{type}_custom_field_dropdown_choices`"

            assert_equal @account.users.length, results.length

            # We're concerned with ordering because we want to order by dropdown choice values.
            expected_users = @account.users.sort_by do |user|
              [user.custom_field_values.value_for_key("plan_type").as_json["value"], expected_natural_ordering[user.id]]
            end.map(&by_name_and_plan_type)

            actual_users = results.map(&by_name_and_plan_type)

            assert_equal expected_users, actual_users
          end
        end

        describe "filtering and #{type}ing by text field" do
          let(:field_value) { "the value" }
          let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, {}).query }
          let(:results) { User.find_by_sql(query) }

          before do
            create_user_custom_field!("text", "text", "Text")
            @account.users.each do |user|
              user.custom_field_values.update_from_hash("text" => field_value)
              user.save!
            end
          end

          describe "with positive operator" do
            let(:user_view) do
              create_view setup_params("title" => "Filtering and #{type}ing by text field",
                                       "all" => [{"field" => "custom_fields.text", "operator" => "is", "value" => field_value }],
                                       "execution" => {
                  type => {"id" => "custom_fields.text", "order" => "asc"}
                })
            end

            it "does not include LEFT OUTER JOIN" do
              refute_includes query, "LEFT OUTER JOIN `cf_values` `#{type}_by_custom_field_value"

              # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
              assert_equal account_users_set, Set.new(results)
            end
          end

          describe "with negative operator" do
            let(:user_view) do
              create_view setup_params("title" => "Filtering and #{type}ing by text field",
                                       "all" => [{"field" => "custom_fields.text", "operator" => "is_not", "value" => "other" }],
                                       "execution" => {
                  type => {"id" => "custom_fields.text", "order" => "asc"}
                })
            end

            it "includes LEFT OUTER JOIN" do
              assert_includes query, "LEFT OUTER JOIN `cf_values` `#{type}_by_custom_field_value"

              # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
              assert_equal account_users_set, Set.new(results)
            end
          end
        end

        describe "filtering and #{type}ing by textarea field" do
          let(:user_view) do
            create_view setup_params("title" => "Filtering and #{type}ing by textarea field",
                                     "all" => [{"field" => "custom_fields.textarea", "operator" => "is", "value" => field_value }],
                                     "execution" => {
                type => {"id" => "custom_fields.textarea", "order" => "asc"}
              })
          end
          let(:field_value) { "the value" }
          let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, {}).query }
          let(:results) { User.find_by_sql(query) }

          before do
            create_user_custom_field!("textarea", "textarea", "Textarea")
            @account.users.each do |user|
              user.custom_field_values.update_from_hash("textarea" => field_value)
              user.save!
            end
          end

          it "does not include LEFT OUTER JOIN" do
            refute_includes query, "LEFT OUTER JOIN `cf_values` `#{type}_by_custom_field_value"

            # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
            assert_equal account_users_set, Set.new(results)
          end
        end

        describe "filtering and #{type}ing by integer field" do
          let(:user_view) do
            create_view setup_params("title" => "Filtering and #{type}ing by integer field",
                                     "all" => [{"field" => "custom_fields.integer", "operator" => "is", "value" => field_value }],
                                     "execution" => {
                type => {"id" => "custom_fields.integer", "order" => "asc"}
              })
          end
          let(:field_value) { 10 }
          let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, {}).query }
          let(:results) { User.find_by_sql(query) }

          before do
            create_user_custom_field!("integer", "integer", "Integer")
            @account.users.each do |user|
              user.custom_field_values.update_from_hash("integer" => field_value)
              user.save!
            end
          end

          it "does not include LEFT OUTER JOIN" do
            refute_includes query, "LEFT OUTER JOIN `cf_values` `#{type}_by_custom_field_value"

            # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
            assert_equal account_users_set, Set.new(results)
          end
        end

        describe "filtering and #{type}ing by decimal field" do
          let(:user_view) do
            create_view setup_params("title" => "Filtering and #{type}ing by decimal field",
                                     "all" => [{"field" => "custom_fields.decimal", "operator" => "is", "value" => field_value }],
                                     "execution" => {
                type => {"id" => "custom_fields.decimal", "order" => "asc"}
              })
          end
          let(:field_value) { 1.23 }
          let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, {}).query }
          let(:results) { User.find_by_sql(query) }

          before do
            create_user_custom_field!("decimal", "decimal", "Decimal")
            @account.users.each do |user|
              user.custom_field_values.update_from_hash("decimal" => field_value)
              user.save!
            end
          end

          it "does not include LEFT OUTER JOIN" do
            refute_includes query, "LEFT OUTER JOIN `cf_values` `#{type}_by_custom_field_value"

            # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
            assert_equal account_users_set, Set.new(results)
          end
        end

        describe "filtering and #{type}ing by checkbox field" do
          let(:user_view) do
            create_view setup_params("title" => "Filtering and #{type}ing by checkbox field",
                                     "all" => [{"field" => "custom_fields.checkbox", "operator" => "is", "value" => field_value }],
                                     "execution" => {
                type => {"id" => "custom_fields.checkbox", "order" => "asc"}
              })
          end
          let(:field_value) { "true" }
          let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, {}).query }
          let(:results) { User.find_by_sql(query) }

          before do
            create_user_custom_field!("checkbox", "checkbox", "Checkbox")
            @account.users.each do |user|
              user.custom_field_values.update_from_hash("checkbox" => field_value)
              user.save!
            end
          end

          it "does not include LEFT OUTER JOIN" do
            refute_includes query, "LEFT OUTER JOIN `cf_values` `#{type}_by_custom_field_value"

            # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
            assert_equal account_users_set, Set.new(results)
          end
        end

        describe "filtering and #{type}ing by regexp field" do
          let(:user_view) do
            create_view setup_params("title" => "Filtering and #{type}ing by regexp field",
                                     "all" => [{"field" => "custom_fields.regexp", "operator" => "is", "value" => field_value }],
                                     "execution" => {
                type => {"id" => "custom_fields.regexp", "order" => "asc"}
              })
          end
          let(:field_value) { "1234" }
          let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, {}).query }
          let(:results) { User.find_by_sql(query) }

          before do
            create_user_custom_field!("regexp", "regexp", "Regexp", regexp_for_validation: "[0-9]+")
            @account.users.each do |user|
              user.custom_field_values.update_from_hash("regexp" => field_value)
              user.save!
            end
          end

          it "does not include LEFT OUTER JOIN" do
            refute_includes query, "LEFT OUTER JOIN `cf_values` `#{type}_by_custom_field_value"

            # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
            assert_equal account_users_set, Set.new(results)
          end
        end

        describe "filtering and #{type}ing by date field" do
          let(:user_view) do
            create_view setup_params("title" => "Filtering and #{type}ing by date field",
                                     "all" => [{"field" => "custom_fields.date", "operator" => "greater_than", "value" => "2012-01-01" }],
                                     "execution" => {
                type => {"id" => "custom_fields.date", "order" => "asc"}
              })
          end
          let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, {}).query }
          let(:results) { User.find_by_sql(query) }

          before do
            create_user_custom_field!("date", "date", "Date")
            @account.users.each do |user|
              user.custom_field_values.update_from_hash("date" => "2014-01-01")
              user.save!
            end
          end

          it "does not include LEFT OUTER JOIN" do
            refute_includes query, "LEFT OUTER JOIN `cf_values` `#{type}_by_custom_field_value"

            # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
            assert_equal account_users_set, Set.new(results)
          end
        end
      end

      describe "filtering and #{type}ing by organization" do
        let(:user_view) do
          create_view setup_params("title" => "Filtering and #{type}ing by organization",
                                   "all" => [{"field" => "organization_id", "operator" => "is_present" }],
                                   "execution" => {
              type => {"id" => "organization_id", "order" => "asc"}
            })
        end
        let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, {}).query }
        let(:results) { User.find_by_sql(query) }

        before do
          organizations = [@account.organizations.create(name: "Zendesk"), @account.organizations.create(name: "Groupon")]

          @account.users.each do |user|
            organization = organizations[user.id % 2]
            user.organization = organization
            user.save!
          end
        end

        it "does not include LEFT OUTER JOIN" do
          refute_includes query, "LEFT OUTER JOIN `organizations` `#{type}_by_organization`"

          assert_equal @account.users.length, results.length

          actual_orgs = results.each_with_object([]) do |user, memo|
            memo << user.organization.name if user.organization.name != memo.last
          end

          assert_equal %w[Groupon Zendesk], actual_orgs
        end
      end

      describe "filtering and #{type}ing by roles" do
        let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, {}).query }
        let(:results) { User.find_by_sql(query) }
        let(:permission_set) { @account.permission_sets.create!(name: 'Permission Set') }
        let(:expected_users) { @account.users - [@account.owner] }

        before do
          Account.any_instance.stubs(:enterprise_roles_in_user_views_enabled?).returns(true)
          Account.any_instance.stubs(:has_permission_sets?).returns(true)

          @account.subscription.update_attributes!(base_agents: 30)

          expected_users.each do |user|
            user.email ||= FactoryBot.generate(:email)
            user.role_or_permission_set = permission_set
            user.save!
          end
        end

        describe "with positive operator" do
          let(:user_view) do
            create_view setup_params("title" => "Filtering and #{type}ing by roles",
                                     "all" => [{"field" => "roles", "operator" => "is", "value" => "permission_set_id:#{permission_set.id}" }],
                                     "execution" => {
                type => {"id" => "roles", "order" => "asc"}
              })
          end

          it "includes LEFT OUTER JOIN" do
            assert_includes query, "LEFT OUTER JOIN `permission_sets` `#{type}_by_roles`"

            # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
            assert_equal expected_users.map(&:name).sort, results.map(&:name).sort
          end
        end

        describe "with negative operator" do
          let(:user_view) do
            create_view setup_params("title" => "Filtering and #{type}ing by roles",
                                     "all" => [{"field" => "roles", "operator" => "is_not", "value" => "admin" }],
                                     "execution" => {
                type => {"id" => "roles", "order" => "asc"}
              })
          end

          it "includes LEFT OUTER JOIN" do
            assert_includes query, "LEFT OUTER JOIN `permission_sets` `#{type}_by_roles`"

            # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
            assert_equal expected_users.map(&:name).sort, results.map(&:name).sort
          end
        end
      end
    end

    describe "filtering and grouping and sorting by text field" do
      let(:field_value) { "the value" }
      let(:query) { Zendesk::UserViews::QueryBuilder.new(user_view, {}).query }
      let(:results) { User.find_by_sql(query) }

      before do
        create_user_custom_field!("text", "text", "Text")
        @account.users.each do |user|
          user.custom_field_values.update_from_hash("text" => field_value)
          user.save!
        end
      end

      describe "with positive operator" do
        let(:user_view) do
          create_view setup_params("title" => "Filtering and grouping and sorting by text field",
                                   "all" => [{"field" => "custom_fields.text", "operator" => "is", "value" => field_value }],
                                   "execution" => {
              "group" => {"id" => "custom_fields.text", "order" => "asc"},
              "sort" => {"id" => "custom_fields.text", "order" => "asc"}
            })
        end

        it "does not include LEFT OUTER JOIN" do
          refute_includes query, "LEFT OUTER JOIN `cf_values` `sort_by_custom_field_value"
          refute_includes query, "LEFT OUTER JOIN `cf_values` `group_by_custom_field_value"
          refute_includes query, "LEFT OUTER JOIN `cf_values` `group_only_by_custom_field_value"

          # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
          assert_equal account_users_set, Set.new(results)
        end
      end

      describe "with negative operator" do
        let(:user_view) do
          create_view setup_params("title" => "Filtering and grouping and sorting by text field",
                                   "all" => [{"field" => "custom_fields.text", "operator" => "is_not", "value" => "other" }],
                                   "execution" => {
              "group" => {"id" => "custom_fields.text", "order" => "asc"},
              "sort" => {"id" => "custom_fields.text", "order" => "asc"}
            })
        end

        it "includes LEFT OUTER JOIN" do
          assert_includes query, "LEFT OUTER JOIN `cf_values` `group_only_by_custom_field_value"

          # We're more concerned that we're not filtering out users with the INNER JOIN than we are about the users being in order.
          assert_equal account_users_set, Set.new(results)
        end
      end
    end
  end

  describe "when grouping or sorting" do
    describe "by time_zone" do
      let(:user_view) do
        create_view setup_params(
          "all" => [{ "field" => "name", "operator" => "is_not", "value" => nil }],
          "execution" => {
            "sort" => {"id" => "time_zone", "order" => "asc"},
            "columns" => ["id", "name", "time_zone"]
          }
        )
      end
      let(:query_builder) { Zendesk::UserViews::QueryBuilder.new(user_view, {}) }

      it "picks the correct orderer" do
        assert_includes(
          query_builder.query,
          "ORDER BY IFNULL(users.time_zone, '#{@account.time_zone}')"
        )
      end
    end
  end

  describe "statsd_tags" do
    describe "no slave db, no group, no custom" do
      it "returns tags for sort, 'none' for group" do
        account = stub(shard_id: 123)
        sort = { id: 'not_custom', order: 'asc' }
        output = stub(sort: sort, group: nil)
        view = stub(account: account, output: output)

        builder = Zendesk::UserViews::QueryBuilder.new(view, {})
        assert_equal ["shard:123", "zendesk_version:ci-test", "order:not_custom:asc", "group:none"], builder.statsd_tags(true)
      end
    end

    describe "no slave db, group, custom sort" do
      it "returns custom tag for sort, tag for group" do
        account = stub(shard_id: 123)
        sort = { id: 'custom_fields.kustom', order: 'asc' }
        group = { id: 'not_custom', order: 'desc' }
        output = stub(sort: sort, group: group)
        view = stub(account: account, custom_fields_by_key: {'kustom' => { type: 'Integer'}}, output: output)

        builder = Zendesk::UserViews::QueryBuilder.new(view, {})
        assert_equal ["shard:123", "zendesk_version:ci-test", "order:cf_integer:asc", "group:not_custom:desc"], builder.statsd_tags(true)
      end
    end

    describe "no slave db, custom group, sort" do
      it "returns tag for sort, custom tag for group" do
        account = stub(shard_id: 123)
        sort = { id: 'not_custom', order: 'asc' }
        group = { id: 'custom_fields.kustom', order: 'desc' }
        output = stub(sort: sort, group: group)
        view = stub(account: account, custom_fields_by_key: {'kustom' => { type: 'Integer'}}, output: output)

        builder = Zendesk::UserViews::QueryBuilder.new(view, {})
        assert_equal ["shard:123", "zendesk_version:ci-test", "order:not_custom:asc", "group:cf_integer:desc"], builder.statsd_tags(true)
      end
    end

    describe "no slave db, group, custom sort, skip sort info" do
      it "returns custom tag for sort, tag for group" do
        account = stub(shard_id: 123)
        sort = { id: 'custom_fields.kustom', order: 'asc' }
        group = { id: 'not_custom', order: 'desc' }
        output = stub(sort: sort, group: group)
        view = stub(account: account, custom_fields_by_key: {'kustom' => { type: 'Integer'}}, output: output)

        builder = Zendesk::UserViews::QueryBuilder.new(view, {})
        assert_equal ["shard:123", "zendesk_version:ci-test"], builder.statsd_tags(false)
      end
    end

    describe "has slave db, no group, no custom" do
      it "returns tag  for database host" do
        class ActiveRecord::ConnectionAdapters::MysqlFlexmasterAdapter
          def current_host
            'database_host'
          end
        end
        ActiveRecord::Base.stubs(:connection).returns(ActiveRecord::ConnectionAdapters::MysqlFlexmasterAdapter.new)

        account = stub(shard_id: 123)
        sort = { id: 'not_custom', order: 'asc' }
        output = stub(sort: sort, group: nil)
        view = stub(account: account, output: output)

        builder = Zendesk::UserViews::QueryBuilder.new(view, {})
        assert_equal ["shard:123", "zendesk_version:ci-test", "order:not_custom:asc", "group:none", "db_host:database_host"], builder.statsd_tags(true)
      end
    end
  end
end
