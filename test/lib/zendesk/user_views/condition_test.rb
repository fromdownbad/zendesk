require_relative "../../../support/test_helper"
require 'zendesk/user_views/condition'

SingleCov.covered! uncovered: 41

describe 'UserAttributeCondition' do
  fixtures :accounts

  describe "#where" do
    describe "when value is nil" do
      before do
        @account = accounts(:minimum)
        @view = stub(account: @account, user_attribute_definitions: {'roles' => {'type' => 'Dropdown'}})
        @definition = stub(operator: :is, source: 'roles', value: nil)
        @subject = Zendesk::UserViews::UserAttributeCondition.new(@view, @definition)
      end

      it "does not raise an error" do
        assert @subject.where
      end
    end

    describe "filter by role" do
      before do
        @account = accounts(:minimum)
        @view = stub(account: @account, user_attribute_definitions: {'roles' => {'type' => 'Dropdown'}})
      end

      it "returns value" do
        @definition = stub(operator: :is, source: 'roles', value: 'admin')
        @subject = Zendesk::UserViews::UserAttributeCondition.new(@view, @definition)

        assert_equal @definition.source.to_sym, @subject.where.left.name
        assert_equal :==, @subject.where.operator
        assert_equal Role::ADMIN.id, @subject.where.right.val
      end

      describe "enterprise_roles_in_user_views enabled" do
        before do
          @account.stubs(:enterprise_roles_in_user_views_enabled?).returns(true)
        end

        describe "when role value is a permission set" do
          before do
            @permission_set = build_valid_permission_set(@account)
            @permission_set.save!
          end

          describe "and operator is negative" do
            before do
              @definition = stub(operator: :is_not, source: 'roles', value: "permission_set_id:#{@permission_set.id}")
              @subject = Zendesk::UserViews::UserAttributeCondition.new(@view, @definition)
            end

            it "includes constraints for permission_set is not value or role is not agent" do
              assert_equal @subject.where.to_sql,
                "((`users`.`permission_set_id` != #{@permission_set.id} OR "\
                "`users`.`permission_set_id` IS NULL) OR `users`.`roles` != #{Role::AGENT.id})"
            end
          end

          describe "and operator is positive" do
            before do
              @definition = stub(operator: :is, source: 'roles', value: "permission_set_id:#{@permission_set.id}")
              @subject = Zendesk::UserViews::UserAttributeCondition.new(@view, @definition)
            end

            it "includes constraints for permission set is value and role is agent" do
              assert_equal @subject.where.to_sql,
                "`users`.`permission_set_id` = #{@permission_set.id} AND "\
                "`users`.`roles` = #{Role::AGENT.id}"
            end
          end
        end
      end
    end

    describe "filter by permission_set" do
      before do
        @account = accounts(:minimum)
        @view = stub(account: @account, user_attribute_definitions: {'roles' => {'type' => 'Dropdown'}})
        @definition = stub(operator: :is, source: 'roles', value: 'permission_set_id:999')
        @subject = Zendesk::UserViews::UserAttributeCondition.new(@view, @definition)
      end

      it "returns value" do
        assert_equal :permission_set_id, @subject.where.left.name
        assert_equal :==, @subject.where.operator
        assert_equal 999, @subject.where.right.val
      end
    end
  end
end
