require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::UserViews::QueryTester do
  fixtures :accounts, :account_settings, :brands, :recipient_addresses, :rules

  subject { Zendesk::UserViews::QueryTester.new }

  let(:account) { accounts(:minimum) }
  let(:user_view) { account.user_views.first }

  describe ".test_account" do
    it "tests account if should work on account" do
      subject.stubs(:test_account?).with(account).returns(true)
      subject.expects(:test_account_user_views).with(account)
      subject.test_account(account)
    end

    it "does not test account if shouldn't work on account" do
      subject.stubs(:test_account?).returns(false)
      subject.expects(:test_account_user_views).with(account).never
      subject.test_account(account)
    end
  end

  describe ".test_account?" do
    describe "with user_views_query_tester and with user_views_sampling_enabled" do
      before do
        Arturo.enable_feature!(:user_views_query_tester)
        Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(true)
      end

      it "is true" do
        assert subject.test_account?(account)
      end
    end

    describe "with user_views_query_tester and without user_views_sampling_enabled" do
      before do
        Arturo.enable_feature!(:user_views_query_tester)
        Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(false)
      end

      it "is false" do
        refute subject.test_account?(account)
      end
    end

    describe "without user_views_query_tester and with user_views_sampling_enabled" do
      before do
        Arturo.disable_feature!(:user_views_query_tester)
        Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(true)
      end

      it "is false" do
        refute subject.test_account?(account)
      end
    end

    describe "without user_views_query_tester and without user_views_sampling_enabled" do
      before do
        Arturo.disable_feature!(:user_views_query_tester)
        Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(false)
      end

      it "is false" do
        refute subject.test_account?(account)
      end
    end
  end

  describe ".test_account_user_views" do
    describe "if should not test user view" do
      before do
        subject.stubs(:test_user_view?).returns(false)
      end

      it "dos nothing" do
        subject.expects(:test_user_view).never

        subject.test_account_user_views(account)
      end
    end

    describe "if should test user view" do
      before do
        subject.stubs(:test_user_view?).returns(false)
        subject.stubs(:test_user_view?).with do |uv|
          uv.id == user_view.id
        end.returns(true)
      end

      it "tests user view" do
        subject.expects(:test_user_view).once.with do |uv|
          assert_equal user_view.id, uv.id
        end

        subject.test_account_user_views(account)
      end
    end
  end

  describe ".test_user_view?" do
    describe "if user view doesn't match criteria" do
      it "is false" do
        refute subject.test_user_view?(user_view)
      end
    end

    describe "if user view matches criteria" do
      before do
        params = {
          id: user_view.id,
          user_view: {
            all: [{"field" => "created_at", "operator" => "greater_than", "value" => Time.parse("03/10/2013")}]
          }
        }
        initializer = Zendesk::Rules::UserViewInitializer.new(account, HashWithIndifferentAccess.new(params), :user_view)
        initializer.user_view.save!
        user_view.reload
      end

      it "is true" do
        assert subject.test_user_view?(user_view)
      end
    end
  end

  describe ".test_user_view" do
    before do
      Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(true)
      account.settings.user_count = UserView::SAMPLING_THRESHOLD * 10
      account.save
    end

    it "runs query with and without use index" do
      users_queries = sql_queries do
        subject.test_user_view(user_view)
      end
      users_queries.select! { |sql| sql.include?('FROM `users`') && sql.include?('user_rule:') }

      assert_equal 4, users_queries.length
      with_use_index_queries = users_queries.select { |sql| sql.include?('USE INDEX(`index_users_on_account_id_and_sample`)') }
      without_use_index_queries = users_queries.reject { |sql| sql.include?('USE INDEX(`index_users_on_account_id_and_sample`)') }
      assert_equal 2, with_use_index_queries.length
      assert_equal 2, without_use_index_queries.length
    end

    it "adds results for report" do
      subject.test_user_view(user_view)

      assert_equal 1, subject.results.length
      result = subject.results.first
      assert_equal account.subdomain, result[0]
      assert_equal account.shard_id, result[1]
      assert_equal user_view.id, result[2]
      assert_equal user_view.title, result[3]
      assert_equal ActiveRecord::Base.connection_config[:host], result[4]

      assert result[5].presence # with select time
      assert result[6].presence # with count time
      assert result[7].presence # with total entries

      assert result[8].presence # without select time
      assert result[9].presence # without count time
      assert result[10].presence # without total entries

      assert (result[11] == true || result[11] == false) # with first?

      assert result[12].presence # time
    end

    describe "execution" do
      let(:find_users_sequence) { sequence('find_users') }
      let(:with) { mock(select_time: 1, count_time: 2) }
      let(:without) { mock(select_time: 3, count_time: 4) }
      let(:collection1) { mock(total_entries: 20) }
      let(:collection2) { mock(total_entries: 30) }

      before do
        Zendesk::UserViews::Executer.expects(:new).with(user_view, User.system, {}).returns(with)
        Zendesk::UserViews::Executer.expects(:new).with(user_view, User.system, skip_use_index: true).returns(without)
      end

      describe "with use index first" do
        before do
          subject.stubs(:with_first?).returns(true)
        end

        it "runs use index first" do
          with.expects(:find_users).returns(collection1).in_sequence(find_users_sequence)
          without.expects(:find_users).returns(collection2).in_sequence(find_users_sequence)

          subject.test_user_view(user_view)

          assert_equal 1, subject.results.length
          result = subject.results.first
          assert_equal 1, result[5]
          assert_equal 2, result[6]
          assert_equal 20, result[7]
          assert_equal 3, result[8]
          assert_equal 4, result[9]
          assert_equal 30, result[10]
          assert result[11]
          assert result[12].presence
        end
      end

      describe "with use index second" do
        before do
          subject.stubs(:with_first?).returns(false)
        end

        it "runs without use index first" do
          without.expects(:find_users).returns(collection2).in_sequence(find_users_sequence)
          with.expects(:find_users).returns(collection1).in_sequence(find_users_sequence)

          subject.test_user_view(user_view)

          assert_equal 1, subject.results.length
          result = subject.results.first
          assert_equal 1, result[5]
          assert_equal 2, result[6]
          assert_equal 20, result[7]
          assert_equal 3, result[8]
          assert_equal 4, result[9]
          assert_equal 30, result[10]
          refute result[11]
          assert result[12].presence
        end
      end
    end
  end

  describe ".with_first?" do
    it "is true half the time" do
      subject.expects(:rand).returns(0)
      assert subject.with_first?
    end

    it "is false half the time" do
      subject.expects(:rand).returns(1)
      refute subject.with_first?
    end
  end

  describe ".generate_csv" do
    before do
      Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(true)
      account.settings.user_count = UserView::SAMPLING_THRESHOLD * 10
      account.save
      subject.test_user_view(user_view)
    end

    it "generates a CSV report" do
      expected_csv = <<~CSV.strip
        Account,Shard,User View,Title,DB,USE INDEX: Select (s),USE INDEX: Count (s),Count,No USE INDEX: Select (s),No USE INDEX: Count (s),Count,USE INDEX First?,Time
        #{subject.results.first.join(',')}
      CSV
      assert_equal expected_csv, subject.generate_csv.strip
    end
  end

  describe ".deliver_csv_report" do
    before do
      Account.any_instance.stubs(:has_user_views_sampling_enabled?).returns(true)
      account.settings.user_count = UserView::SAMPLING_THRESHOLD * 10
      account.save
    end

    describe "with results" do
      before do
        subject.test_user_view(user_view)
      end

      it "sends a report e-mail" do
        @csv = nil
        MonitorMailer.expects(:deliver_user_view_query_test_report).with do |a, csv|
          @csv = csv
          a.id == account.id
        end

        subject.deliver_csv_report

        assert_equal subject.generate_csv, @csv
      end
    end

    describe "without results" do
      it "dos nothing" do
        MonitorMailer.expects(:deliver_user_view_query_test_report).never

        subject.deliver_csv_report
      end
    end
  end
end
