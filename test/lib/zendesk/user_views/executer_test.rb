require_relative "../../../support/test_helper"
require_relative "../../../support/user_views_test_helper"
require 'zendesk/rules/user_view_initializer'
require 'zendesk/user_views/executer'

SingleCov.covered!

describe Zendesk::UserViews::Executer do
  include CustomFieldsTestHelper
  include UserViewsTestHelper

  fixtures :accounts, :users, :rules, :cf_fields, :organizations

  let(:account) { accounts(:minimum) }
  let(:user_view) do
    create_view setup_params("all" => [{"field" => "created_at", "operator" => "greater_than", "value" => Time.parse("03/10/2013")}])
  end
  let(:current_user) { account.owner }
  let(:params) { {} }

  subject { Zendesk::UserViews::Executer.new(user_view, current_user, params) }

  before do
    @account = account
  end

  describe ".users_for_view" do
    it "finds users" do
      executer     = mock
      actual_users = mock
      Zendesk::UserViews::Executer.expects(:new).with(user_view, current_user, params).returns(executer)
      executer.expects(:find_users).returns(actual_users)

      assert_same actual_users, Zendesk::UserViews::Executer.users_for_view(user_view, current_user, params)
    end
  end

  describe "#find_users" do
    let(:actual_users) { subject.find_users }
    let(:expected_users) { account.users.where('created_at > ?', Time.parse("03/10/2013")).to_a }

    it "returns a collection of users" do
      expected = expected_users.map(&:id).sort
      actual = actual_users.map(&:id).sort
      assert_equal expected, actual

      assert actual_users.all? { |r| r.is_a?(User) }
      assert actual_users.is_a?(WillPaginate::Collection)
    end

    it "returns the total entries" do
      assert_equal expected_users.count, actual_users.total_entries
    end

    describe "specifying an arbitrary per page limit" do
      let(:params) { { per_page: 5 } }

      it "returns the total entries" do
        assert_equal expected_users.count, actual_users.total_entries
      end

      it "returns one page of records" do
        assert_equal 1, actual_users.current_page
        assert_equal 5, actual_users.length
      end

      describe "and specifying the second page" do
        let(:params) { { per_page: 5, page: 2 } }

        it "returns the last page" do
          assert_equal 2, actual_users.current_page
          assert_equal 5, actual_users.length
        end
      end

      describe "and specifying the last page" do
        let(:params) { { per_page: 5, page: 3 } }

        it "returns the last page" do
          assert_equal 3, actual_users.current_page
          assert_equal 3, actual_users.length
        end
      end
    end

    describe "when less total entries than allowed per page" do
      before do
        User.where(id: account.users.last(5).map(&:id)).delete_all
      end

      it "returns the total entries" do
        assert_equal 8, actual_users.total_entries
      end
    end

    describe "with StatsD" do
      let(:statsd_client) { stub_for_statsd }

      before do
        ::Zendesk::StatsD::Client.stubs(:new).returns(statsd_client)
        statsd_client.stubs(:timing)
        Benchmark.stubs(:realtime).yields.returns(12.34567)
      end

      it "times the select time" do
        statsd_client.expects(:timing).with('select', 12345, tags: ["shard:#{account.shard_id}", "zendesk_version:#{GIT_HEAD_TAG}", 'order:created_at:asc', 'group:none'])
        actual_users
      end

      it "exposes the select time" do
        actual_users
        assert_equal 12.34567, subject.select_time
      end

      it "times the count time" do
        statsd_client.expects(:timing).with('count', 12345, tags: ["shard:#{account.shard_id}", "zendesk_version:#{GIT_HEAD_TAG}"])
        actual_users
      end

      it "exposes the count time" do
        actual_users
        assert_equal 12.34567, subject.count_time
      end
    end

    describe "when finding with benchmark" do
      before do
        Benchmark.stubs(:realtime).yields.returns(12.34567)
        @logs = []
        Rails.logger.stubs(:info).with do |msg|
          @logs << msg
        end
      end

      it "logs info" do
        actual_users
        assert_includes @logs, "FindUser: UserView, #{user_view.id}, #{account.id}, #{expected_users.count}, 12.34567, 1, 0"
      end
    end
  end

  describe "#logger" do
    it "is Rails.logger" do
      assert_same Rails.logger, subject.send(:logger)
    end
  end

  describe "custom fields" do
    before do
      @account = accounts(:minimum)
      @view_executer = Zendesk::UserViews::Executer
      @users = [users(:minimum_end_user), users(:minimum_search_user), users(:minimum_author)]
      @agent = users(:minimum_agent)
      update_timestamps
      create_custom_fields
      create_custom_field_values
      Arturo.enable_feature!(:user_views_negative_operators_enabled)
    end

    describe "text custom view field" do
      it "finds users when text_field is 'Moal' but is not 'Paz'" do
        view = build_view([build_condition(@text_field, :is,     "Moal"),
                           build_condition(@text_field, :is_not, "Paz")])
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 2, users.length
      end
    end

    describe "decimal fields" do
      it "finds users when a decimal field is greater than 2" do
        view = build_view(Array.wrap(build_condition(@decimal_field, :greater_than, 2)))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 1, users.length
      end

      it "finds users when a decimal custom field greater or equal 2" do
        view =  build_view(Array.wrap(build_condition(@decimal_field, :greater_than_or_equal_to, 2)))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 1, users.length
      end

      it "finds users with a decimal custom field is less than 4" do
        view =  build_view(Array.wrap(build_condition(@decimal_field, :less_than, 4)))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 1, users.length
      end

      it "finds users with a decimal custom field" do
        view = build_view(Array.wrap(build_condition(@decimal_field, :less_than_or_equal_to, 3)))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 1, users.length
      end
    end

    describe "boolean fields" do
      it "finds users with a checkbox field value checked" do
        view = build_view(Array.wrap(build_condition(@checkbox_field, :is, true)))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 1, users.length
      end

      it "finds users with a checkbox field not checked" do
        view = build_view(Array.wrap(build_condition(@checkbox_field, :is_not, true)))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 12, users.length
      end
    end

    describe "dropdowns" do
      before do
        create_dropdown_field
        create_dropdown_field_values
      end

      it "finds users when a dropdown field value is 1" do
        view = build_view(Array.wrap(build_condition(@dropdown_field, :is, '1')))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 2, users.length
      end

      it "finds users when a dropdown field is not 1" do
        view = build_view(Array.wrap(build_condition(@dropdown_field, :is_not, '1')))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 11, users.length
      end
    end

    describe "date custom fields" do
      it "finds users with a date field older than 2012-12-12" do
        view = build_view(Array.wrap(build_condition(@date_field, :less_than, "2012-12-12")))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 2, users.length
      end

      it "finds users with a date field older or on 2012-10-10" do
        view = build_view(Array.wrap(build_condition(@date_field, :less_than_or_equal_to, "2012-10-10")))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 2, users.length
      end

      it "finds users with a date field after 2012-09-09" do
        view = build_view(Array.wrap(build_condition(@date_field, :greater_than, "2012-09-09")))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 3, users.length
      end

      it "finds users with a date field after or on 2012-10-10" do
        view = build_view(Array.wrap(build_condition(@date_field, :greater_than_or_equal_to, "2012-10-10")))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 3, users.length
      end

      it "finds users with a date field on 2012-10-10" do
        view = build_view(Array.wrap(build_condition(@date_field, :is, "2012-10-10")))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 2, users.length
      end

      it "finds users with a date field not on 2012-10-10" do
        view = build_view(Array.wrap(build_condition(@date_field, :is_not, "2012-10-10")))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 11, users.length
      end
    end

    describe "users table fields" do
      describe "with all conditions" do
        it "finds users with created_at earlier than a date" do
          view = build_view(Array.wrap(build_condition("created_at", :less_than, "2012-10-09")))
          users = @view_executer.users_for_view(view, @agent)
          assert_equal 1, users.length
        end

        it "finds users with created_at between dates" do
          view = build_view([build_condition("created_at", :less_than, "2012-10-10 07:00:00 UTC"),
                             build_condition("created_at", :greater_than, "2012-06-06 07:00:00 UTC")])
          users = @view_executer.users_for_view(view, @agent)
          assert_equal 2, users.length
        end
      end

      describe "any_conditions" do
        it "finds users with created_at earlier than a date" do
          view = build_view([build_condition("name", :is_not, "")],
            [build_condition("created_at", :less_than, "2012-10-10 07:00:00 UTC"),
             build_condition("created_at", :less_than, "2012-10-10 07:00:00 UTC")])
          users = @view_executer.users_for_view(view, @agent)
          assert_equal 3, users.length
        end
      end
    end

    describe "custom field views with any_conditions" do
      it "finds users with a decimal field > 2 or a text field == Moal" do
        view = build_view([build_condition("name", :is_not, "")],
          [build_condition(@decimal_field, :greater_than, 2),
           build_condition(@text_field, :is, "Moal")])
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 3, users.length
      end

      it "finds users with a text field = Moal and a decimal field > 2 or a text field = Paz" do
        view = build_view(Array.wrap(build_condition(@text_field, :is, "Paz")),
          [build_condition(@decimal_field, :greater_than, 2),
           build_condition(@text_field, :is, "Moal")])
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 1, users.length
      end
    end

    describe "current_tags" do
      before do
        Account.any_instance.stubs(:has_user_and_organization_tags?).returns(true)
        create_user_tags
      end

      it "finds users with at least one of the following tags (ALL)" do
        view = build_view(Array.wrap(build_condition("current_tags", :includes, %w[the_owner the_partner])))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 2, users.length
      end

      it "finds users with at least one of the following tags (ANY)" do
        view = build_view([build_condition("name", :is_not, "")],
          Array.wrap(build_condition("current_tags", :includes, %w[the_owner the_partner])))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 2, users.length
      end

      it "finds users with none of the following tags (ALL)" do
        excluded_tags = ['the_owner', 'the_partner']
        view = build_view(Array.wrap(build_condition("current_tags", :not_includes, %w[the_owner the_partner])))
        users = @view_executer.users_for_view(view, @agent)
        users_without_the_tags = @account.users.reject do |user|
          (excluded_tags & user.taggings.map(&:tag_name)).any?
        end
        assert_equal users_without_the_tags.length, users.length
        assert_equal 11, users.length
      end

      it "finds users with none of the following tags (ANY)" do
        view = build_view([build_condition("name", :is_not, "")],
          Array.wrap(build_condition("current_tags", :not_includes, %w[the_owner the_partner])))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 11, users.length
      end
    end

    describe "organization_id" do
      it "finds users which organization is some (ALL)" do
        view = build_view(Array.wrap(build_condition("organization_id", :is, organizations(:minimum_organization1).id)))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 4, users.length
      end

      it "finds users which organization is not some (ALL)" do
        view = build_view(Array.wrap(build_condition("organization_id", :is_not, organizations(:minimum_organization2).id)))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 13, users.length
      end

      it "finds users which organization is not nil (ALL)" do
        view = build_view(Array.wrap(build_condition("organization_id", :is_not, nil)))
        users = @view_executer.users_for_view(view, @agent)
        assert_equal 4, users.length
      end
    end

    describe "output" do
      before do
        @all_view = build_view(Array.wrap(build_condition(@text_field, :is, "Moal")))
        @any_view = build_view(Array.wrap(build_condition("name", :is_not, "")), Array.wrap(build_condition(@text_field, :is, "Moal")))
      end

      describe "order results" do
        it "sorts by id" do
          @all_view.output = sort(:id, :asc)
          users = @view_executer.users_for_view(@all_view, @agent)
          assert_equal users.map(&:id).sort, users.map(&:id)
        end

        it "sorts by system fields" do
          @all_view.output = sort(:updated_at, :asc)
          users = @view_executer.users_for_view(@all_view, @agent)
          assert_equal users.map(&:updated_at).sort, users.map(&:updated_at)
        end

        it "sorts by custom fields" do
          @all_view.output = sort("custom_fields." + @text_field.key, :asc)
          users = @view_executer.users_for_view(@all_view, @agent)
          assert_equal 2, users.length
        end

        it "sorts by custom fields (any)" do
          @all_view.output = sort("custom_fields." + @text_field.key, :asc)
          users = @view_executer.users_for_view(@any_view, @agent)
          assert_equal 2, users.length
        end
      end
    end

    describe "#users_for_view" do
      let(:view) { build_view(Array.wrap(build_condition(@decimal_field, :greater_than, 2))) }
      describe "when finding users with `zendesk_nps` subsystem" do
        let!(:view_users) { @view_executer.users_for_view(view, @agent, subsystem_name: 'zendesk_nps') }

        it "returns the correct number of results" do
          assert_equal 1, view_users.length
        end
      end

      describe "when finding users without `zendesk_nps` subsystem" do
        let(:view_users) { @view_executer.users_for_view(view, @agent) }

        it "performs a count_by_sql query" do
          view.entity.expects(:count_by_sql)
          view_users
        end

        it "returns the correct number of results" do
          assert_equal 1, view_users.length
        end
      end
    end
  end
end
