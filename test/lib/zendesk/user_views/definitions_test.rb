require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 34

describe 'Definitions' do
  fixtures :accounts, :users, :organizations, :cf_fields

  describe "#roles_dropdown_options" do
    before do
      @user = users(:minimum_end_user)
      @subject = Zendesk::UserViews::Definitions.new(@user.account, @user)
    end

    it "returns standard values" do
      assert_equal [
        { name: Role::END_USER.display_name, value: "end-user" },
        { name: Role::AGENT.display_name,    value: "agent" },
        { name: Role::ADMIN.display_name,    value: "admin" }
      ],
        @subject.roles_dropdown_options
    end

    describe "enterprise roles in user views are enabled" do
      before do
        @user.account.expects(:enterprise_roles_in_user_views_enabled?).returns(true).at_least_once
        @permission_set = PermissionSet.new(name: 'set 1', id: 1)
        @user.account.permission_sets = [@permission_set]
      end

      it "returns permission sets" do
        assert_equal [
          { name: Role::END_USER.display_name, value: "end-user" },
          { name: Role::AGENT.display_name,    value: "agent" },
          { name: Role::ADMIN.display_name,    value: "admin" },
          { name: 'set 1',                     value: "permission_set_id:#{@permission_set.id}" }
        ],
          @subject.roles_dropdown_options
      end
    end
  end
end
