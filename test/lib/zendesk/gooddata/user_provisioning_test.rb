require_relative '../../../support/job_helper'
require_relative '../../../support/gooddata_session_helper'

SingleCov.covered! uncovered: 4

describe Zendesk::Gooddata::UserProvisioning do
  include GooddataSessionHelper

  fixtures :accounts, :users

  let(:account)           { accounts(:minimum) }
  let(:user)              { users(:minimum_agent) }
  let(:gooddata_user) do
    GooddataUser.create! do |u|
      u.account = account
      u.user = users(:minimum_agent)
      u.gooddata_project_id = 'gooddata_project_id'
      u.gooddata_user_id = 'gooddata_user_id'
    end
  end

  let(:user_provisioning) { Zendesk::Gooddata::UserProvisioning.new(account) }

  let(:gooddata_client) { Zendesk::Gooddata::Client.v2 }
  let(:login) { '1111122222@gooddata.com' }

  before { stub_gooddata_session }

  describe ".sync_for_user" do
    before do
      account.create_gooddata_integration(
        project_id: 'project_id',
        version: 2
      )
    end

    after { gooddata_user.destroy }

    describe "when a GoodData user exists for the user" do
      before { Zendesk::Gooddata::UserProvisioning.sync_for_user(user) }

      before_should "enqueues a job to sync the user with GoodData" do
        GooddataUserSyncJob.expects(:enqueue).
          with(user.account_id, gooddata_user.id)
      end
    end

    describe "when a GoodData user does not exist for the user" do
      describe "and the account has a gooddata integration" do
        before { Zendesk::Gooddata::UserProvisioning.sync_for_user(user) }

        before_should "enqueues a job to create the user with GoodData" do
          GooddataUserCreateJob.expects(:enqueue).
            with(user.account_id, user.id)
        end
      end

      describe "and the account does not have a gooddata integration" do
        before do
          account.gooddata_integration.destroy
          Zendesk::Gooddata::UserProvisioning.sync_for_user(user)
        end

        before_should "enqueues a job to create the user with GoodData" do
          GooddataUserCreateJob.expects(:enqueue).
            with(user.account_id, user.id).never
        end
      end
    end
  end

  describe ".destroy_for_account" do
    let(:gooddata_user_different_project) do
      GooddataUser.create! do |u|
        u.account = account
        u.user = users(:minimum_admin)
        u.gooddata_project_id = 'totally_different_project'
        u.gooddata_user_id = 'gooddata_user_id_other'
      end
    end

    before do
      account.create_gooddata_integration(
        project_id: 'gooddata_project_id',
        version: 2
      )
    end

    it 'deletes GooddataUsers for the account' do
      GooddataUserDestroyJob.expects(:enqueue).with(account.id, gooddata_user.id)
      GooddataUserDestroyJob.expects(:enqueue).with(account.id, gooddata_user_different_project.id)
      Zendesk::Gooddata::UserProvisioning.destroy_for_account(account)
    end
  end

  describe ".destroy_for_user" do
    describe "when a GoodData user does not exist for the user" do
      before do
        gooddata_user.destroy

        Zendesk::Gooddata::UserProvisioning.destroy_for_user(user)
      end

      before_should "not enqueue a job to sync the user with GoodData" do
        GooddataUserDestroyJob.expects(:enqueue).
          with(user.account_id, gooddata_user.id).never
      end
    end

    describe "when a GoodData user exists for the user" do
      before { Zendesk::Gooddata::UserProvisioning.destroy_for_user(user) }

      before_should "enqueue a job to destroy the user in GoodData" do
        GooddataUserDestroyJob.expects(:enqueue).
          with(user.account_id, gooddata_user.id)
      end
    end
  end

  describe ".create_for_user" do
    describe "when gooddata integration is present" do
      before do
        account.create_gooddata_integration(
          project_id: 'project_id',
          version: 2
        )
        Zendesk::Gooddata::UserProvisioning.create_for_user(user)
      end

      before_should "enqueue a job for a user" do
        GooddataUserCreateJob.expects(:enqueue).with(account.id, user.id).once
      end
    end

    describe "when gooddata integration is not present" do
      before do
        user.account.expects(:gooddata_integration).returns(nil)
        Zendesk::Gooddata::UserProvisioning.create_for_user(user)
      end

      before_should "not enqueue a job for a user" do
        GooddataUserCreateJob.expects(:enqueue).with(account.id, user.id).never
      end
    end
  end

  describe ".create_all_for_account" do
    before do
      account.create_gooddata_integration(
        project_id: 'gooddata_project_id',
        version: 2
      )
      Zendesk::Gooddata::UserProvisioning.create_all_for_account(account)
    end

    before_should "queues a create job for each agent in the account" do
      GooddataUserCreateJob.expects(:enqueue).times(account.agents.count(:all))
    end
  end

  describe ".sync_all_for_account" do
    before do
      account.create_gooddata_integration(
        project_id: 'gooddata_project_id',
        version: 2
      )
      Zendesk::Gooddata::UserProvisioning.sync_all_for_account(account)
    end

    before_should "synchronises each agent in the account" do
      Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).times(account.agents.count(:all))
    end
  end

  describe ".reprovision_all_users" do
    before do
      account.create_gooddata_integration(
        project_id: 'gooddata_project_id',
        version: 2
      )
      Zendesk::Gooddata::UserProvisioning.reprovision_all_users(account)
    end

    before_should 'reprovision all users for the account' do
      Zendesk::Gooddata::UserProvisioning.expects(:reprovision_user).times(account.agents.count(:all))
    end
  end

  describe '.reprovision_user' do
    let(:project_id) { 'gooddata_project_id' }
    let(:gooddata_integration) { stub('gooddata integration', project_id: project_id) }

    before do
      account.create_gooddata_integration(
        project_id: project_id,
        version: 2
      )

      GooddataUser.stubs(:for_user).with(user).returns(gooddata_user, nil)
      gooddata_client.stubs(:get).
        with("/gdc/account/profile/#{gooddata_user.gooddata_user_id}").
        returns(accountSetting: { login: login })

      Zendesk::Gooddata::UserProvisioning.stubs(:sync_for_user)
    end

    describe 'with deleted gooddata user' do
      let(:login) { 'deleted-1111122222@gooddata.com' }

      it 'destroys existing gooddata user' do
        Zendesk::Gooddata::UserProvisioning.expects(:destroy_for_user).with(user)

        Zendesk::Gooddata::UserProvisioning.reprovision_user(user)
      end

      it 'creates new gooddata user' do
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).with(user)

        Zendesk::Gooddata::UserProvisioning.reprovision_user(user)
      end
    end

    describe 'gooddata user has not been deleted' do
      it 'syncs the user' do
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).with(user)

        Zendesk::Gooddata::UserProvisioning.reprovision_user(user)
      end
    end

    describe "gooddata user project id does not match account's" do
      let(:gooddata_user) do
        GooddataUser.create! do |u|
          u.account = account
          u.user = user
          u.gooddata_project_id = 'blah'
          u.gooddata_user_id = 'gooddata_user_id'
        end
      end

      it 'destroys existing gooddata user' do
        Zendesk::Gooddata::UserProvisioning.expects(:destroy_for_user).with(user)

        Zendesk::Gooddata::UserProvisioning.reprovision_user(user)
      end

      it 'creates new gooddata user' do
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).with(user)

        Zendesk::Gooddata::UserProvisioning.reprovision_user(user)
      end
    end

    describe 'gooddata user not found' do
      it 'creates new gooddata user' do
        GooddataUser.stubs(:for_user).with(user).returns(nil)
        Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).with(user)

        Zendesk::Gooddata::UserProvisioning.reprovision_user(user)
      end
    end
  end

  describe "#create_gooddata_user" do
    before do
      gooddata_user.destroy

      GoodData::User.any_instance.stubs(:create).returns('user_id')
    end

    describe "when the user's account does not have an integration" do
      before do
        account.gooddata_integration.try(:destroy)
        account.reload
      end

      it "returns false" do
        refute user_provisioning.create_gooddata_user(user)
      end
    end

    describe "when the user's account has an integration" do
      before do
        account.create_gooddata_integration(
          project_id: 'project_id',
          status: 'complete'
        )

        account.gooddata_integration.editor_role_id = 'editor_role_id'
        account.gooddata_integration.save!

        stub_request(
          :post,
          %r{v2\.zendesk\.com/gdc/accounts/domains/users}
        ).to_return(
          body: {uri: '/gdc/account/profile/user_id'}.to_json
        )

        stub_request(
          :post,
          %r{v2\.zendesk\.com/gdc/projects/project_id/users}
        )

        stub_request(
          :get,
          %r{v2\.zendesk\.com/gdc/account/domains/your-organization/users}
        ).to_return(
          body: {accountSettings: {items: [] }}.to_json
        )

        GooddataUser.stubs(:gooddata_login_for_user).with(user).
          returns('login@zendesk.com')

        SecureRandom.stubs(:hex).with(50).returns('secure_password')
      end

      describe "and a gooddata user exists in Gooddata but not in Zendesk" do
        before do
          GoodData::User.any_instance.stubs(:find_by_login).returns('stranded_gooddata_user')

          user_provisioning.create_gooddata_user(user)
        end

        before_should "delete the existing user in gooddata" do
          GoodData::User.any_instance.expects(:delete)
        end
      end

      describe "and the user's role grants him some kind of access" do
        before { user_provisioning.create_gooddata_user(user) }

        before_should "create a new GoodData user through the API" do
          GoodData::User.any_instance.expects(:create).with(
            login: 'login@zendesk.com',
            password: 'secure_password',
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email
          ).returns('user_id')
        end

        before_should "add the user to the project with the correct role" do
          GoodData::Project.any_instance.expects(:add_user).with(
            user_id: 'user_id',
            role_id: 'editor_role_id'
          )
        end

        it "sets show_insights_onboarding to true" do
          assert user.settings.reload.show_insights_onboarding
        end

        it "creates a new GoodData user record" do
          assert GooddataUser.for_user(user)
        end
      end

      describe "and the user's role does not grant him any access" do
        before do
          GooddataUser.any_instance.stubs(:gooddata_role?).
            with(:none).returns(true)

          user_provisioning.create_gooddata_user(user)
        end

        before_should "does not add the user to the project" do
          GoodData::Project.any_instance.expects(:add_user).never
        end
      end
    end
  end

  describe "#sync_gooddata_user" do
    describe "when the user's account does not have an integration" do
      before do
        account.gooddata_integration.try(:destroy)
        account.reload
      end

      it "raises an exception" do
        assert_raises(RuntimeError) do
          user_provisioning.sync_gooddata_user(gooddata_user)
        end
      end
    end

    describe "when the user's account has an integration" do
      before do
        account.create_gooddata_integration(
          project_id: 'project_id',
          status: 'complete'
        )

        account.gooddata_integration.editor_role_id = 'editor_role_id'
        account.gooddata_integration.save!

        gooddata_user.stubs(:gooddata_role).returns(:editor)

        stub_request(
          :post,
          %r{v2\.zendesk\.com/gdc/projects/project_id/users}
        )
        stub_request(
          :put,
          %r{v2\.zendesk\.com/gdc/account/profile/gooddata_user_id}
        )

        gooddata_client.stubs(:get).
          with("/gdc/account/profile/#{gooddata_user.gooddata_user_id}").
          returns(accountSetting: { login: login })

        user_provisioning.sync_gooddata_user(gooddata_user)
      end

      before_should "update the user's role on the project" do
        GoodData::Project.any_instance.expects(:update_user).with(
          user_id: 'gooddata_user_id',
          role_id: 'editor_role_id'
        )
      end

      before_should "update the user's email address" do
        GoodData::User.any_instance.expects(:update).with(
          first_name: user.first_name,
          last_name: user.last_name,
          email: user.email
        )
      end

      describe "and the user is no longer an agent" do
        before do
          gooddata_user.user.stubs(:is_agent?).returns(false)

          stub_request(
            :delete,
            %r{v2\.zendesk\.com/gdc/account/profile/gooddata_user_id}
          )
        end

        it "removes the Gooddata user" do
          GoodData::User.any_instance.expects(:delete)

          user_provisioning.sync_gooddata_user(gooddata_user)
          assert_empty GooddataUser.where(id: gooddata_user.id)
        end
      end

      describe "and the user is no longer active" do
        before do
          gooddata_user.user.stubs(:is_agent?).returns(true)
          gooddata_user.user.stubs(:is_active?).returns(false)

          stub_request(
            :delete,
            %r{v2\.zendesk\.com/gdc/account/profile/gooddata_user_id}
          )
        end

        it "removes the Gooddata user" do
          GoodData::User.any_instance.expects(:delete)

          user_provisioning.sync_gooddata_user(gooddata_user)
          assert_empty GooddataUser.where(id: gooddata_user.id)
        end
      end

      describe "and the user no longer has the gooddata access role" do
        before do
          gooddata_user.user.stubs(:is_agent?).returns(true)
          gooddata_user.user.stubs(:is_active?).returns(true)
          gooddata_user.stubs(:gooddata_role?).with(:none).returns(:none)

          stub_request(
            :delete,
            %r{v2\.zendesk\.com/gdc/account/profile/gooddata_user_id}
          )
        end

        it "removes the Gooddata user" do
          GoodData::User.any_instance.expects(:delete)

          user_provisioning.sync_gooddata_user(gooddata_user)
          assert_empty GooddataUser.where(id: gooddata_user.id)
        end
      end
    end
  end

  describe "#destroy_gooddata_user" do
    before do
      gooddata_client.stubs(:get).
        with("/gdc/account/profile/#{gooddata_user.gooddata_user_id}").
        returns(accountSetting: { login: login })
    end

    describe "when active on the GoodData API" do
      before do
        stub_request(
          :delete,
          %r{v2\.zendesk\.com/gdc/account/profile/gooddata_user_id}
        )
      end

      describe "when it succeeds" do
        before { user_provisioning.destroy_gooddata_user(gooddata_user) }

        before_should "delete the GoodData user through the API" do
          GoodData::User.any_instance.expects(:delete)
        end

        it "deletes the GoodData user record" do
          refute GooddataUser.for_user(user)
        end
      end

      describe "when deleting the GoodData user record fails" do
        before { GooddataUser.any_instance.stubs(:destroy).returns(false) }

        it "raises an exception" do
          assert_raises(RuntimeError) do
            user_provisioning.destroy_gooddata_user(gooddata_user)
          end
        end
      end
    end

    # GoodData API does not allow "modifications" to deleted users, including
    # attempting to delete an already-deleted user -- the API will return 400
    describe "when deleted on the GoodData API" do
      let(:login) { 'deleted-1111122222@gooddata.com' }

      before { user_provisioning.destroy_gooddata_user(gooddata_user) }

      before_should "not delete the GoodData user through the API" do
        GoodData::User.any_instance.expects(:delete).never
      end

      it "deletes the GoodData user record" do
        refute GooddataUser.for_user(user)
      end
    end

    describe "when the GoodData API reports '404 not found' for the user" do
      before do
        gooddata_client.stubs(:get).
          with("/gdc/account/profile/#{gooddata_user.gooddata_user_id}").
          raises(GoodData::Error::NotFound)

        user_provisioning.destroy_gooddata_user(gooddata_user)
      end

      before_should "not delete the GoodData user through the API" do
        GoodData::User.any_instance.expects(:delete).never
      end

      it "deletes the GoodData user record" do
        refute GooddataUser.for_user(user)
      end
    end

    describe "when the GoodData API reports '403 forbidden' for the user" do
      before do
        gooddata_client.stubs(:get).
          with("/gdc/account/profile/#{gooddata_user.gooddata_user_id}").
          raises(GoodData::Error::Forbidden)

        user_provisioning.destroy_gooddata_user(gooddata_user)
      end

      before_should "not delete the GoodData user through the API" do
        GoodData::User.any_instance.expects(:delete).never
      end

      it "deletes the GoodData user record" do
        refute GooddataUser.for_user(user)
      end
    end
  end
end
