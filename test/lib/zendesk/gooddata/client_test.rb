require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Gooddata::Client do
  let(:configuration) { Zendesk::GooddataIntegrations::Configuration }

  describe ".v2" do
    let(:gooddata_client) { Zendesk::Gooddata::Client.v2 }

    before do
      Zendesk::Gooddata::Client.instance_variable_set(:@v2_gooddata_client, nil)
    end

    it "returns a GoodData client" do
      assert gooddata_client.is_a?(GoodData::Client)
    end

    it "configures the client for v2 integrations" do
      assert_equal(
        configuration.fetch(:api_endpoint),
        gooddata_client.send(:configuration).api_endpoint
      )
    end

    describe "when configuring from ENV" do
      before do
        ENV['GOODDATA_API_ENDPOINT'] = 'FOO'
        configuration.stubs(:use_env?).returns(true)
        configuration.setup
      end

      after do
        ENV['GOODDATA_API_ENDPOINT'] = nil
      end

      it "configures the client from the ENV if available" do
        assert_equal(
          'FOO',
          gooddata_client.send(:configuration).api_endpoint
        )
      end

      it "configures the client from the yml if not available" do
        assert_equal(
          configuration.fetch(:api_endpoint),
          gooddata_client.send(:configuration).api_endpoint
        )
      end
    end
  end
end
