require_relative '../../../support/job_helper'
require_relative '../../../support/gooddata_session_helper'

SingleCov.covered! uncovered: 7

describe Zendesk::Gooddata::IntegrationProvisioning do
  include GooddataSessionHelper

  fixtures :accounts, :users

  def self.should_not_perform_any_step_except(performed_step)
    before_should "not perform any other step" do
      steps = provisioning_steps.reject do |step|
        step == performed_step
      end
      steps.each do |step|
        integration_provisioning.expects(step).never
      end
    end
  end

  let(:account) { accounts(:minimum) }
  let(:admin)   { users(:minimum_admin) }
  let(:template_version) { 33 }

  let(:integration_provisioning) do
    Zendesk::Gooddata::IntegrationProvisioning.new(account, admin: admin)
  end

  before { stub_gooddata_session }

  describe "#gooddata_integration" do
    describe "when an integration is provided" do
      let(:provided_integration) do
        stub('provided integration', project_id: 'provided project id')
      end

      let(:integration_provisioning) do
        Zendesk::Gooddata::IntegrationProvisioning.new(account, gooddata_integration: provided_integration)
      end

      it "returns the provided integration" do
        assert_equal provided_integration, integration_provisioning.gooddata_integration
      end
    end

    describe "when an integration is not set" do
      let(:account_integration) do
        stub('gooddata integration', project_id: 'account project id')
      end

      before { account.stubs(:gooddata_integration).returns(account_integration) }

      it "returns the account integration" do
        assert_equal account_integration, integration_provisioning.gooddata_integration
      end
    end
  end

  describe "#destroy_for_account" do
    let(:gooddata_integration) { stub('gooddata_integration', id: 9) }

    describe "the integration exists" do
      before do
        account.stubs(:gooddata_integration).returns(gooddata_integration)
        Zendesk::Gooddata::UserProvisioning.stubs(:destroy_for_account)
      end

      describe "a v2 integration" do
        before do
          Zendesk::Gooddata::IntegrationProvisioning.destroy_for_account(account)
        end

        before_should "destroy users for the integration" do
          gooddata_integration.expects(:soft_delete)
          Zendesk::Gooddata::UserProvisioning.expects(:destroy_for_account).with(account)
        end

        before_should "destroy the integration" do
          gooddata_integration.expects(:soft_delete)
          GooddataIntegrationsDestroyJob.expects(:enqueue).
            with(account.id, gooddata_integration.id)
        end
      end
    end

    describe "the integration doesn't exist" do
      before do
        Zendesk::Gooddata::IntegrationProvisioning.destroy_for_account(account)
      end

      before_should "do nothing" do
        GooddataIntegrationsDestroyJob.expects(:enqueue).never
      end
    end
  end

  describe "#re_enable" do
    let(:re_enabling_steps) do
      [
        :gooddata_re_enable,
        :enable_gooddata_integration,
        :provision_users,
        :kick_off_incremental_reload,
        :gooddata_re_enable_complete
      ]
    end
    let(:gooddata_integration) { stub('gooddata_integration') }
    let(:user_provisioning) { stub('user_provisioning') }

    before do
      account.stubs(:gooddata_integration).returns(gooddata_integration)

      gooddata_integration.stubs(:project_id).returns('project_id')

      re_enabling_steps.each { |s| integration_provisioning.stubs(s) }
      Zendesk::Gooddata::UserProvisioning.stubs(:create_for_user)
      Zendesk::Gooddata::UserProvisioning.stubs(:new).returns(user_provisioning)
      user_provisioning.stubs(:create_gooddata_user)
    end

    describe "and the project is disabled" do
      before do
        stub_request(
          :get,
          %r{
              v2\.zendesk\.com/gdc/projects
              /#{account.gooddata_integration.project_id}
          }x
        ).to_return(
          body: {project: {content: {state: 'DISABLED'}}}.to_json
        )
        integration_provisioning.re_enable
      end

      before_should "enable gooddata integration via Gooddata API" do
        integration_provisioning.expects(:enable_gooddata_integration)
      end

      before_should "provision all user" do
        integration_provisioning.expects(:provision_users)
      end

      before_should "kick off incremental reload" do
        integration_provisioning.expects(:kick_off_incremental_reload)
      end

      before_should "mark reload as complete" do
        integration_provisioning.expects(:gooddata_re_enable_complete)
      end

      before_should "not sleep" do
        integration_provisioning.expects(:sleep).never
      end
    end
  end

  describe "#perform" do
    let(:provisioning_steps) do
      [
        :create_gooddata_integration,
        :create_gooddata_connector,
        :configure_gooddata_connector,
        :cache_gooddata_project_role_ids
      ]
    end

    let(:gooddata_integration) { stub('gooddata_integration') }
    let(:user_provisioning) { stub('user_provisioning') }

    before do
      account.stubs(:gooddata_integration).returns(gooddata_integration)

      gooddata_integration.stubs(:project_id).returns('project_id')

      stub_request(
        :get,
        %r{
          v2\.zendesk\.com/gdc/projects
          /#{account.gooddata_integration.project_id}
        }x
      ).to_return(
        body: {
          project: {
            content: {state: 'ENABLED'}
          }
        }.to_json
      )

      stub_request(:put, %r{v2.zendesk.com/gdc/projects/project_id/styleSettings}).
        with(
          body: {
            styleSettings: {chartPalette: integration_provisioning.send(:color_palette_json)}
          }
        ).
        to_return(
          status: 200
        )

      stub_request(:post, %r{v2.zendesk.com/gdc/md/project_id/service/timezone}).
        with(
          body: {
            service: { timezone: 'Europe/Copenhagen' }
          }
        ).
        to_return(
          status: 200
        )

      integration_provisioning.stubs(:sleep)

      provisioning_steps.each { |s| integration_provisioning.stubs(s) }
      Zendesk::Gooddata::UserProvisioning.stubs(:create_for_user)
      Zendesk::Gooddata::UserProvisioning.stubs(:new).returns(user_provisioning)
      user_provisioning.stubs(:create_gooddata_user)
    end

    describe "when the integration does not exist" do
      before do
        gooddata_integration.stubs(:present?).returns(false)
        gooddata_integration.stubs(:pending?).returns(false)
        gooddata_integration.stubs(:created?).returns(false)
        gooddata_integration.stubs(:configured?).returns(false)
        gooddata_integration.stubs(:complete?).returns(false)

        integration_provisioning.perform
      end

      before_should "create the GoodData integration" do
        integration_provisioning.expects(:create_gooddata_integration)
      end

      should_not_perform_any_step_except(:create_gooddata_integration)
    end

    describe "when the integration is in the 'pending' state" do
      before do
        gooddata_integration.stubs(:present?).returns(true)
        gooddata_integration.stubs(:pending?).returns(true)
        gooddata_integration.stubs(:created?).returns(false)
        gooddata_integration.stubs(:configured?).returns(false)
        gooddata_integration.stubs(:complete?).returns(false)
      end

      describe "and the project is enabled" do
        before do
          stub_request(
            :get,
            %r{
              v2\.zendesk\.com/gdc/projects
              /#{account.gooddata_integration.project_id}
            }x
          ).to_return(
            body: {project: {content: {state: 'ENABLED'}}}.to_json
          )

          integration_provisioning.perform
        end

        before_should "create the gooddata connector" do
          integration_provisioning.expects(:create_gooddata_connector)
        end

        before_should "not sleep" do
          integration_provisioning.expects(:sleep).never
        end

        should_not_perform_any_step_except(:create_gooddata_connector)
      end

      describe "and the project is not enabled at first" do
        before do
          stub_request(
            :get,
            %r{
              v2\.zendesk\.com/gdc/projects
              /#{account.gooddata_integration.project_id}
            }x
          ).to_return(
            body: {project: {content: {state: 'PREPARING'}}}.to_json
          ).then.to_return(
            body: {project: {content: {state: 'ENABLED'}}}.to_json
          )

          integration_provisioning.perform
        end

        before_should "eventually create the gooddata connector" do
          integration_provisioning.expects(:create_gooddata_connector)
        end

        before_should "sleep for a period of time" do
          integration_provisioning.expects(:sleep).with(2)
        end

        should_not_perform_any_step_except(:create_gooddata_connector)
      end

      describe "and the project remains not enabled" do
        before do
          stub_request(
            :get,
            %r{
              v2\.zendesk\.com/gdc/projects
              /#{account.gooddata_integration.project_id}
            }x
          ).to_return(
            body: {project: {content: {state: 'PREPARING'}}}.to_json
          )
        end

        it "raises a 'ProjectNotEnabled' exception" do
          assert_raise(Zendesk::Gooddata::IntegrationProvisioning::ProjectNotEnabled) do
            integration_provisioning.perform
          end
        end
      end
    end

    describe "when the integration is in the 'created' state" do
      before do
        gooddata_integration.stubs(:present?).returns(true)
        gooddata_integration.stubs(:pending?).returns(false)
        gooddata_integration.stubs(:created?).returns(true)
        gooddata_integration.stubs(:configured?).returns(false)
        gooddata_integration.stubs(:complete?).returns(false)

        integration_provisioning.perform
      end

      before_should "configure the GoodData connector" do
        integration_provisioning.expects(:configure_gooddata_connector)
      end

      should_not_perform_any_step_except(:configure_gooddata_connector)
    end

    describe "when the integration is in the 'configured' state" do
      before do
        gooddata_integration.stubs(:present?).returns(true)
        gooddata_integration.stubs(:pending?).returns(false)
        gooddata_integration.stubs(:created?).returns(false)
        gooddata_integration.stubs(:configured?).returns(true)
        gooddata_integration.stubs(:complete?).returns(false)

        integration_provisioning.perform
      end

      before_should "cache the project role IDs for the GoodData project" do
        integration_provisioning.expects(:cache_gooddata_project_role_ids)
      end

      should_not_perform_any_step_except(:cache_gooddata_project_role_ids)
    end

    describe "when the integration has been provisioned" do
      before do
        gooddata_integration.stubs(:present?).returns(true)
        gooddata_integration.stubs(:pending?).returns(false)
        gooddata_integration.stubs(:created?).returns(false)
        gooddata_integration.stubs(:configured?).returns(true)
        gooddata_integration.stubs(:complete?).returns(true)

        integration_provisioning.perform
      end

      before_should "not perform any provisioning steps" do
        provisioning_steps.each do |step|
          integration_provisioning.expects(step).never
        end
      end

      before_should "load color palette for the project" do
        GoodData::Project.any_instance.expects(:configure_style_settings).
          with(
            settings: JSON(File.read("#{Rails.root}/config/gooddata_color_palette.json"))
          )
      end

      before_should "set the time zone for the project" do
        time_zone = ActiveSupport::TimeZone[account.time_zone].tzinfo.identifier
        GoodData::Project.any_instance.expects(:configure_time_zone).
          with(time_zone: time_zone)
      end

      before_should "create GoodData users for the current account" do
        Zendesk::Gooddata::UserProvisioning.expects(:create_all_for_account).with(account)
      end

      before_should "provision the admin user" do
        user_provisioning.expects(:create_gooddata_user).with(admin)
      end
    end
  end

  describe "#create_gooddata_integration" do
    describe "when an integration has yet to be created" do
      before do
        stub_request(:post, %r{v2.zendesk.com/gdc/projects}).to_return(
          body: {uri: '/gdc/projects/new_project_id'}.to_json
        )
        integration_provisioning.create_gooddata_integration
      end

      before_should "create a new GoodData project" do
        GoodData::Project.any_instance.expects(:create).with(
          title: "#{account.subdomain} Insights",
          template_name: 'ZendeskAnalytics',
          template_version: template_version
        ).returns('new_project_id')
      end

      it "creates a new integration" do
        assert account.gooddata_integration.pending?
      end

      it "saves the new project ID" do
        assert_equal 'new_project_id', account.gooddata_integration.project_id
      end

      it "saves the admin ID" do
        assert_equal admin.id, account.gooddata_integration.admin_id
      end

      it "sets the integration version to '2'" do
        assert_equal 2, account.gooddata_integration.version
      end
    end

    describe "when an integration already exists" do
      before do
        account.create_gooddata_integration(project_id: 'project_id')
      end

      it "raises an exception" do
        assert_raises(RuntimeError) do
          integration_provisioning.create_gooddata_integration
        end
      end
    end
  end

  describe "#create_gooddata_connector" do
    describe "when the integration is in the 'pending' state" do
      before do
        account.create_gooddata_integration(
          project_id: 'project_id',
          status: 'pending'
        )

        stub_request(
          :post,
          %r{
            v2\.zendesk\.com/gdc/projects/project_id
            /connectors/zendesk4/integration
          }x
        )

        integration_provisioning.create_gooddata_connector
      end

      before_should "create the project integration" do
        GoodData::Integration::Zendesk4.any_instance.expects(:create).with(
          template_name: 'ZendeskAnalytics',
          template_version: template_version
        )
      end

      it "transitions the integration to the 'created' state" do
        assert account.gooddata_integration.created?
      end
    end

    describe "when the integration is not in the 'pending' state" do
      before do
        account.create_gooddata_integration(
          project_id: 'project_id',
          status: 'complete'
        )
      end

      it "raises an exception" do
        assert_raises(RuntimeError) do
          integration_provisioning.create_gooddata_connector
        end
      end
    end
  end

  describe "#configure_gooddata_connector" do
    describe "when the integration is in the 'created' state" do
      before do
        account.create_gooddata_integration(
          project_id: 'project_id',
          status: 'created'
        )

        stub_request(
          :put,
          %r{
            v2\.zendesk\.com/gdc/projects
            /#{account.gooddata_integration.project_id}
            /connectors/zendesk4/integration/settings
          }x
        )

        integration_provisioning.configure_gooddata_connector
      end

      before_should "configure the connector" do
        GoodData::Integration::Zendesk4.any_instance.expects(:configure).with(
          api_domain: "#{account.subdomain}.zendesk.com",
          zopim_api_domain: "www.zopim.com",
          zopim_subdomain: account.subdomain
        )
      end

      it "transitions the integration to the 'configured' state" do
        assert account.gooddata_integration.configured?
      end
    end

    describe "when the integration is not in the 'created' state" do
      before do
        account.create_gooddata_integration(
          project_id: 'project_id',
          status: 'complete'
        )
      end

      it "raises an exception" do
        assert_raises(RuntimeError) do
          integration_provisioning.configure_gooddata_connector
        end
      end
    end
  end

  describe "#cache_gooddata_project_role_ids" do
    describe "when the integration is in the 'configured' state" do
      let(:admin_role_id)          { '1' }
      let(:editor_role_id)         { '2' }
      let(:read_only_user_role_id) { '3' }
      let(:other_role_id)          { '4' }

      before do
        account.create_gooddata_integration(
          project_id: 'project_id',
          status: 'configured'
        )

        stub_request(
          :get,
          %r{v2\.zendesk\.com/gdc/projects/project_id/roles}
        ).to_return(
          body: {
            projectRoles: {
              roles: [
                "/role/#{admin_role_id}",
                "/role/#{editor_role_id}",
                "/role/#{read_only_user_role_id}",
                "/role/#{other_role_id}"
              ]
            }
          }.to_json
        )

        [
          [admin_role_id,          'adminRole'],
          [editor_role_id,         'zendeskEditorRole'],
          [read_only_user_role_id, 'readOnlyUserRole'],
          [other_role_id,          'otherRole']
        ].each do |role_id, role_identifier|
          stub_request(
            :get,
            %r{v2\.zendesk\.com/gdc/projects/project_id/roles/#{role_id}}
          ).to_return(
            body: {
              projectRole: {
                meta: {identifier: role_identifier}
              }
            }.to_json
          )
        end

        integration_provisioning.cache_gooddata_project_role_ids
      end

      it "saves the admin role ID" do
        assert_equal admin_role_id, account.gooddata_integration.admin_role_id
      end

      it "saves the editor role ID" do
        assert_equal(
          editor_role_id,
          account.gooddata_integration.editor_role_id
        )
      end

      it "saves the read-only user role ID" do
        assert_equal(
          read_only_user_role_id,
          account.gooddata_integration.read_only_user_role_id
        )
      end

      it "transitions the integration to the 'complete' state" do
        assert account.gooddata_integration.complete?
      end
    end

    describe "when the integration is not in the 'configured' state" do
      before do
        account.create_gooddata_integration(
          project_id: 'project_id',
          status: 'complete'
        )
      end

      it "raises an exception" do
        assert_raises(RuntimeError) do
          integration_provisioning.cache_gooddata_project_role_ids
        end
      end
    end
  end

  describe "#enable_gooddata_integration" do
    let(:gooddata_integration) { stub('gooddata_integration', project_id: 'p1234', id: 7) }
    let(:integration) { stub('integration') }
    let(:project) { stub('project', integration: integration) }

    before do
      account.stubs(:gooddata_integration).returns(gooddata_integration)
      integration_provisioning.gooddata_client.stubs(:project).returns(project)
    end

    describe "when we can get the existing GoodData project template version" do
      before do
        Zendesk::Gooddata::IntegrationProvisioning.stubs(:integration_template_version).returns(2)

        integration_provisioning.enable_gooddata_integration
      end

      before_should "should set the GoodData integration as active" do
        integration.expects(:update).with(
          active: true,
          template_name: 'ZendeskAnalytics',
          template_version: 2
        )
      end
    end
  end

  describe "#integration_template_version" do
    before do
      body = {
        integration: {
          projectTemplate: "/projectTemplates/ZendeskAnalytics/10",
          active: false
        }
      }.to_json
      stub_request(:get, %r{/gdc/projects/p1234/connectors/zendesk4/integration}).
        to_return(status: 200, body: body)
    end

    it "should return the project template version" do
      assert_equal 10, Zendesk::Gooddata::IntegrationProvisioning.integration_template_version('p1234')
    end
  end

  describe "#upgrade_gooddata_integration" do
    before do
      Zendesk::Gooddata::UserProvisioning.stubs(:sync_for_user)

      integration_provisioning.stubs(:project_role_mapping).returns(
        admin_role_id: '1',
        editor_role_id: '2',
        read_only_user_role_id: '3'
      )
    end

    describe "when the account does not have any integration" do
      before do
        integration_provisioning.upgrade_gooddata_integration("1234")
      end

      it "uses the default scheduled_at" do
        assert_equal "12am", account.gooddata_integration.scheduled_at
      end

      it "has version 2" do
        assert_equal 2, account.gooddata_integration.version
      end

      it "has a v2 project_id" do
        assert_equal "1234", account.gooddata_integration.project_id
      end

      it "has a complete status" do
        assert_equal "complete", account.gooddata_integration.status
      end
    end

    describe "when the account has an integration" do
      before do
        account.create_gooddata_integration(
          project_id: "1234",
          version: 1,
          scheduled_at: "2am"
        )
      end

      describe "there are no GooddataUsers associated with it" do
        before do
          integration_provisioning.upgrade_gooddata_integration("1234567")
        end

        it "has copied the scheduled_at" do
          assert_equal "2am", account.gooddata_integration.scheduled_at
        end

        it "has version 2" do
          assert_equal 2, account.gooddata_integration.version
        end

        it "has a v2 project_id" do
          assert_equal "1234567", account.gooddata_integration.project_id
        end

        it "has a complete status" do
          assert_equal "complete", account.gooddata_integration.status
        end

        it "has cached role ids" do
          assert_equal "1", account.gooddata_integration.admin_role_id
          assert_equal "2", account.gooddata_integration.editor_role_id
          assert_equal "3", account.gooddata_integration.read_only_user_role_id
        end
      end

      describe "there is a GooddataUser associated with it" do
        before do
          @gooddata_user = GooddataUser.create! do |u|
            u.account = account
            u.user = admin
            u.gooddata_project_id = account.gooddata_integration.project_id
            u.gooddata_user_id = 'gooddata_user_id'
          end
          integration_provisioning.upgrade_gooddata_integration("1234567")
          @gooddata_user.reload
        end

        it "sets the project ID on the user to the new project ID" do
          assert_equal "1234567", @gooddata_user.gooddata_project_id
        end

        before_should "call sync_for_user" do
          Zendesk::Gooddata::UserProvisioning.expects(:sync_for_user).with(responds_with(:id, admin.id))
        end
      end
    end

    describe "#destroy_gooddata_integration" do
      let(:gooddata_integration) { stub('gooddata_integration', project_id: 'abcprojectid', id: 7) }
      let(:integration) { stub('integration') }
      let(:project) { stub('project', integration: integration) }

      before do
        account.stubs(:gooddata_integration).returns(gooddata_integration)
        integration_provisioning.gooddata_client.stubs(:project).returns(project)
        Zendesk::Gooddata::IntegrationProvisioning.stubs(:integration_template_version).returns(18)
      end

      describe "the integration is deleted correctly" do
        describe "when the integration is enabled" do
          before do
            project.stubs(:find).returns(true)
            integration_provisioning.destroy_gooddata_integration
          end

          before_should "deactivate the GoodData integration" do
            project.expects(:delete)
            integration.expects(:update).with(
              active: false,
              template_name: 'ZendeskAnalytics',
              template_version: 18
            )
          end
        end

        describe "when the integration is not enabled" do
          before do
            project.stubs(:find).returns(false)
            integration_provisioning.destroy_gooddata_integration
          end

          before_should "not attempt to delete the project" do
            project.expects(:delete).never
            integration.expects(:update).never
          end
        end
      end
    end

    describe "#assign_role_attributes" do
      let(:admin_role_id)          { '1' }
      let(:editor_role_id)         { '2' }
      let(:read_only_user_role_id) { '3' }

      before do
        account.create_gooddata_integration(
          project_id: 'project_id',
          status: 'configured'
        )

        integration_provisioning.assign_role_attributes
      end

      it "saves the admin role ID" do
        assert_equal admin_role_id, account.gooddata_integration.admin_role_id
      end

      it "saves the editor role ID" do
        assert_equal editor_role_id, account.gooddata_integration.editor_role_id
      end

      it "saves the read-only user role ID" do
        assert_equal(
          read_only_user_role_id,
          account.gooddata_integration.read_only_user_role_id
        )
      end
    end
  end
end
