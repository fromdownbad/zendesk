require_relative "../../support/test_helper"

SingleCov.covered! uncovered: 1

describe 'Zendesk::RoutingValidations' do
  fixtures :accounts, :routes, :reserved_subdomains

  describe "#subdomain_available?" do
    it "returns true if available and valid" do
      assert Zendesk::RoutingValidations.subdomain_available?("wombatsforever")

      # should be valid with a dash
      assert Zendesk::RoutingValidations.subdomain_available?("wombats-forever")
    end

    it "returns false if not valid" do
      # too short
      refute Zendesk::RoutingValidations.subdomain_available?("wo"), "must be at least 2 characters"

      # can't start with a dash
      refute Zendesk::RoutingValidations.subdomain_available?("-foobargains"), "cannot start with a dash"

      # can't end with a dash
      refute Zendesk::RoutingValidations.subdomain_available?("foobargains-"), "cannot end with a dash"

      # can't start with a non-alphanumeric
      refute Zendesk::RoutingValidations.subdomain_available?("_subdomain"), "cannot start with a non-alphanumeric"

      # can't end with a non-alphanumeric
      refute Zendesk::RoutingValidations.subdomain_available?("subdomain_"), "cannot end with a non-alphanumeric"
    end

    it "returns false if not available" do
      assert_equal false, Zendesk::RoutingValidations.subdomain_available?("minimum")
    end

    it "returns false if reserved via regex" do
      assert_equal false, Zendesk::RoutingValidations.subdomain_available?("ns1")
    end

    it "returns false if reserved via string" do
      assert_equal false, Zendesk::RoutingValidations.subdomain_available?("hostmaster")
    end

    describe "punycode subdomain" do
      it "returns false" do
        refute Zendesk::RoutingValidations.subdomain_available?("xn--socity-6of"), "cannot be punycode"
      end

      it "returns true if internal API request" do
        assert Zendesk::RoutingValidations.subdomain_available?("xn--socity-6of", internal: true), "internal requests can be punycode"
      end
    end
  end

  # We have to test the real models because we need existing DB objects and internationalized error messages
  [[Account, :minimum], Route].each do |klass, fixture|
    describe "A #{klass.name}" do
      describe "on a new object" do
        let(:model) { klass.new }

        it "has translated message for subdomain format" do
          model.valid?
          assert_includes model.errors.full_messages, "Subdomain:   is invalid (three or more letters and numbers only, without spaces)"
        end

        it "has translated message for too long subdomain" do
          model.subdomain = "Wombatsliketoeatlotsoffoodandliveintazmaniaandareamazingtheyalsoarethecoolestanimalever"
          model.valid?
          assert_includes model.errors.full_messages, "Subdomain:  #{model.subdomain} is too long (maximum is 63 characters)"
        end

        it "has translated message for hostmapping uniqueness" do
          klass.first.update_attribute(:host_mapping, "hosthost")
          model.host_mapping = "hosthost"
          model.valid?
          assert_match /Host mapping.*is taken/, model.errors.full_messages.join(" ")
        end
      end

      describe "on an existing object" do
        let(:model) { fixture ? accounts(fixture) : klass.first }

        describe "validate_reserved_subdomain" do
          it "does not allow via string matching" do
            ["hostmaster", "www"].each do |name|
              model.subdomain = name
              refute_valid model
              assert_includes model.errors.full_messages, "Subdomain:  is reserved and cannot be registered"
            end
          end

          it "does not allow via regex matching" do
            ["ns1", "pubsub3-4", "asset0", "asset", "assets", "cdn", "cdn1", "pod1", "pod-4", "mail1"].each do |name|
              model.subdomain = name
              refute_valid model
              assert_includes model.errors.full_messages, "Subdomain:  is reserved and cannot be registered"
            end
          end

          it "allows to save existing accounts" do
            model.update_column(:subdomain, "pod1")
            assert_valid model
          end
        end

        describe "validate subdomain" do
          it "allows saving invalid subdomain on existing accounts" do
            model.update_column(:subdomain, "wo")
            assert_valid model
          end

          it "does not allow invalid character subdomain to be saved" do
            model.subdomain = "wombat!"
            refute_valid model
            assert_includes model.errors.full_messages, "Subdomain:  #{model.subdomain} is invalid (three or more letters and numbers only, without spaces)"
          end

          it "does not allow subdomain that is too long to be saved" do
            model.subdomain = "Wombatsliketoeatlotsoffoodandliveintazmaniaandareamazingtheyalsoarethecoolestanimalever"
            refute_valid model
            assert_includes model.errors.full_messages, "Subdomain:  #{model.subdomain} is too long (maximum is 63 characters)"
          end
        end

        describe "dns validations" do
          def assert_cname_error(error)
            model.valid?
            method = (error ? :assert_includes : :refute_includes)
            send method, model.errors.full_messages.join(" "), "CNAME"
          end

          before do
            Zendesk::Net::CNAMEResolver.stubs(resolve?: nil)
            model.host_mapping = "localhost"
          end

          describe "with a resolvable host" do
            before do
              Zendesk::Net::CNAMEResolver.expects(:resolve?).returns stub(name: model.default_host)
            end

            it "validates" do
              assert_cname_error false
            end

            it "is valid" do
              assert(model.cname_valid?)
            end

            it "has the right status" do
              assert_equal :pass, model.send(:cname_status)
            end
          end

          describe "with a blank host" do
            before do
              model.host_mapping = ""
            end

            it "validates" do
              assert_cname_error false
            end

            it "is invalid" do
              assert_equal false, model.cname_valid?
            end

            it "does not have a status" do
              assert_nil model.send(:cname_status)
            end
          end

          describe "with an unresolvable host in development environment" do
            before do
              model.stubs(cname_allowed_for_development?: true)
            end

            it "validates" do
              assert_cname_error false
            end

            it "is valid" do
              assert(model.cname_valid?)
            end

            it "has the right status" do
              assert_equal :pass, model.send(:cname_status)
            end
          end

          describe "with an unchanged host" do
            before do
              model.stubs(host_mapping_changed?: false)
              Zendesk::Net::CNAMEResolver.expects(:resolve?).never
            end

            it "validates" do
              assert_cname_error false
            end
          end

          describe "with an unresolvable host" do
            it "fails" do
              assert_cname_error true
            end

            it "is invalid" do
              assert_equal false, model.cname_valid?
            end

            it "has the right status" do
              assert_equal :not_cname, model.send(:cname_status)
            end
          end

          describe "with a resolvable host that points to different domain" do
            before do
              Zendesk::Net::CNAMEResolver.expects(:resolve?).returns stub(name: "xxx.zendesk.com")
            end

            it "fails" do
              assert_cname_error true
            end

            it "is invalid" do
              assert_equal false, model.cname_valid?
            end

            it "has the right status" do
              assert_equal :wrong_cname, model.send(:cname_status)
            end
          end
        end
      end
    end
  end
end
