require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Google::GAMClient do
  let(:access_token) { 'access_token' }
  let(:domain) { 'zobuzendesk.com' }
  let(:username) { 'haabaato' }
  let(:forward_to_email) { "support@#{domain.split('.').first}.zendesk.com" }
  let(:google_api_url_root) { Zendesk::Google::GAMClient::GOOGLE_API_URL_ROOT }

  def assert_request(method, url, content)
    headers = { Authorization: "Bearer #{access_token}"}
    headers['Content-Type'] = content == :json ? 'application/json' : 'application/atom+xml'
    assert_requested(method, url, times: 1, headers: headers)
  end

  describe "Email Settings API" do
    let(:gam_client)  { Zendesk::Google::GAMClient.new(access_token: access_token, domain: domain) }

    it "returns a GAM client" do
      assert gam_client.is_a?(Zendesk::Google::GAMClient)
    end

    describe "#create_email_forwarding_filter" do
      let(:url) { "https://apps-apis.google.com" + "/a/feeds/emailsettings/2.0/#{domain}/#{username}/filter" }
      let(:body) do
        <<~XML
          <?xml version='1.0' encoding='UTF-8'?>
          <entry xmlns='http://www.w3.org/2005/Atom' xmlns:apps='http://schemas.google.com/apps/2006'>
            <id>https://apps-apis.google.com/a/feeds/emailsettings/2.0/#{domain}/#{username}/filter/0</id>
            <updated>2016-04-27T22:27:53.369Z</updated>
            <link rel='self' type='application/atom+xml' href='https://apps-apis.google.com/a/feeds/emailsettings/2.0/#{domain}/#{username}/filter/0'/>
            <link rel='edit' type='application/atom+xml' href='https://apps-apis.google.com/a/feeds/emailsettings/2.0/#{domain}/#{username}/filter/0'/>
            <apps:property name='forwardTo' value='#{forward_to_email}'/>
            <apps:property name='to' value='hello@zobuzendesk.com'/>
          </entry>
        XML
      end

      it "returns the created email filter in xml format" do
        stub_request(:post, url).to_return(status: 200, body: body)
        response = gam_client.create_email_forwarding_filter(
          email_owner_username: username, forward_from_email: "support@#{domain}", forward_to_email: forward_to_email
        )
        assert_request(:post, url, :xml)
        response_hash = Hash.from_xml(body).with_indifferent_access
        assert_equal response, response_hash[:entry]
      end
    end
  end

  describe "Site Verification API" do
    let(:gam_client)  { Zendesk::Google::GAMClient.new(access_token: access_token, domain: domain) }

    it "returns a GAM client" do
      assert gam_client.is_a?(Zendesk::Google::GAMClient)
    end

    describe "#get_site_verification_token" do
      let(:url) { "#{google_api_url_root}/siteVerification/v1/token" }
      let(:body) { '{"method":"FILE","token":"google82bd8ec382b139d4.html"}' }

      it "returns site verification token in json format" do
        stub_request(:post, url).to_return(status: 200, body: body)
        response = gam_client.get_site_verification_token(domain: domain)
        assert_request(:post, url, :json)
        assert_equal response, JSON.parse(body).with_indifferent_access
      end
    end

    describe "#verify_site_token" do
      let(:url) { "#{google_api_url_root}/siteVerification/v1/webResource?verificationMethod=FILE" }
      let(:body) do
        '{
          "id":"http%3A%2F%2Fzobuzendesk.com%2F",
          "site":{"type":"SITE","identifier":"http://zobuzendesk.com/"},
          "owners":["haabaato@zobuzendesk.com"]
        }'
      end

      it "returns identified site and its owners in json format" do
        stub_request(:post, url).to_return(status: 200, body: body)
        response = gam_client.verify_site_token(domain: domain)
        assert_request(:post, url, :json)
        assert_equal response, JSON.parse(body).with_indifferent_access
      end
    end
  end

  describe "Directory API" do
    describe "#add_domain_alias" do
      let(:gam_client) do
        Zendesk::Google::GAMClient.new(access_token: access_token, domain: domain)
      end
      let(:customer_id) { "abc0123" }
      let(:url) { "#{google_api_url_root}/admin/directory/v1/customer/#{customer_id}/domainaliases" }
      let(:domain_alias) { "zobuzendesk.zendesk.com" }
      let(:body) do
        <<~JSON
          {
            "kind": "admin#directory#domainAlias",
            "etag": "iwpzoDgSq9BJw-XzORg0bILYPVc/QVrDQ30kAV59H2-njqriXagvwFc",
            "domainAliasName": "#{domain_alias}",
            "parentDomainName": "zobuzendesk.com",
            "verified": false,
            "creationTime": "1457388655589"
          }
        JSON
      end

      it "returns domain alias creation results in json format" do
        stub_request(:post, url).to_return(status: 200, body: body)
        response = gam_client.add_domain_alias(
          customer_id: customer_id, domain_alias: 'zobuzendesk.zendesk.com'
        )
        assert_request(:post, url, :json)
        assert_equal response, JSON.parse(body).with_indifferent_access
      end
    end

    describe "#get_domain_aliases" do
      let(:gam_client) do
        Zendesk::Google::GAMClient.new(access_token: access_token, domain: domain)
      end
      let(:customer_id) { "abc0123" }
      let(:domain_alias) { "zobuzendesk.zendesk.com" }
      let(:url) { "#{google_api_url_root}/admin/directory/v1/customer/#{customer_id}/domainaliases" }
      let(:body) do
        <<~JSON
          {
           "kind": "admin#directory#domainAliases",
           "etag": "XRsypGOPUmlmxokHB51cC07Vb3s/Kb_4YSKW3g-C4P8HvlQCw5eN5uQ",
           "domainAliases": [
              {
               "kind": "admin#directory#domainAlias",
               "etag": "XRsypGOPUmlmxokHB51cC07Vb3s/ArV_XJOMpYxh5-RUDJlsrh5nMNs",
               "domainAliasName": "zobuzendesk.com.test-google-a.com",
               "parentDomainName": "zobuzendesk.com",
               "verified": true,
               "creationTime": "1450723744293"
              },
              {
               "kind": "admin#directory#domainAlias",
               "etag": "XRsypGOPUmlmxokHB51cC07Vb3s/qBc16zSYIhEyutlSema_mKGcxig",
               "domainAliasName": "herbtest25.zd-staging.com",
               "parentDomainName": "zobuzendesk.com",
               "verified": true,
               "creationTime": "1464196319516"
              }
            ]
          }
        JSON
      end

      it "returns existing domain aliases in json format" do
        stub_request(:get, url).to_return(status: 200, body: body)
        response = gam_client.get_domain_aliases(customer_id: customer_id)
        assert_request(:get, url, :json)
        assert_equal response, JSON.parse(body).with_indifferent_access
      end
    end

    describe "#delete_domain_alias" do
      let(:gam_client) do
        Zendesk::Google::GAMClient.new(access_token: access_token, domain: domain)
      end
      let(:customer_id) { "abc0123" }
      let(:domain_alias) { "zobuzendesk.zendesk.com" }
      let(:url) { "#{google_api_url_root}/admin/directory/v1/customer/#{customer_id}/domainaliases/#{domain_alias}" }

      it "returns success" do
        stub_request(:delete, url).to_return(status: 200)
        gam_client.delete_domain_alias(
          customer_id: customer_id, domain_alias: 'zobuzendesk.zendesk.com'
        )
        assert_request(:delete, url, :json)
      end
    end

    describe "#add_group_member" do
      let(:gam_client) { Zendesk::Google::GAMClient.new(access_token: access_token, domain: domain) }
      let(:group_email) { "support@#{domain}" }
      let(:url) { "#{google_api_url_root}/admin/directory/v1/groups/#{group_email}/members" }
      let(:body) do
        <<~JSON
          {
            "email":"#{forward_to_email}",
            "role":"MEMBER"
          }
        JSON
      end

      it "returns success" do
        stub_request(:post, url).to_return(status: 200, body: body)
        response = gam_client.add_group_member(group_email: group_email, member_email: forward_to_email)
        assert_request(:post, url, :json)
        assert_equal response, JSON.parse(body).with_indifferent_access
      end
    end
  end

  describe "Group Settings API" do
    let(:gam_client) { Zendesk::Google::GAMClient.new(access_token: access_token, domain: domain) }

    describe "#enable_public_group_emails" do
      let(:group_email) { "support@#{domain}" }
      let(:url) { "#{google_api_url_root}/groups/v1/groups/#{group_email}" }
      let(:body) do
        '{
          "whoCanPostMessage":"ANYONE_CAN_POST"
        }'
      end

      it "returns success" do
        stub_request(:patch, url).to_return(status: 200)
        gam_client.enable_public_group_emails(group_email: group_email)
        assert_request(:patch, url, :json)
      end
    end
  end
end
