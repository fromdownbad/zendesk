require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 63

describe 'Palette' do
  let(:brand_color) { '78A300' }
  let(:settings) { Struct.new(:header_color).new(brand_color) }
  let(:palette) { Zendesk::Branding::Palette.new(settings) }
  describe "#secondary_color" do
    describe "for light brand colors" do
      let(:brand_color) { '#F5F5F5' }
      it "generates a darker secondary color" do
        assert_equal 'DCDCDC', palette.secondary_color
      end
    end

    describe "for dark brand colors" do
      let(:brand_color) { '#3B57B3' }
      it "generates a lighter secondary color" do
        assert_equal '6C83CF', palette.secondary_color
      end
    end
  end

  describe "#tab_hover_color" do
    describe "when secondary color is light" do
      it "generates a color that is 5% darker" do
        palette.stubs(:secondary_color).returns('6C83CF')
        assert_equal '5973C9', palette.tab_hover_color
      end
    end

    describe "when secondary color is dark" do
      it "generates a color that is 5% lighter" do
        palette.stubs(:secondary_color).returns('000000')
        assert_equal '0D0D0D', palette.tab_hover_color
      end
    end
  end

  describe "#contrast_color" do
    describe "for a light secondary color" do
      it "returns the font color #333333" do
        palette.stubs(:light_color?).returns(true)
        assert_equal '333333', palette.contrast_color
      end
    end

    describe "for a dark secondary color" do
      it "returns the font color #FFFFFF" do
        palette.stubs(:light_color?).returns(false)
        assert_equal 'FFFFFF', palette.contrast_color
      end
    end
  end

  describe "#light_brand_color?" do
    describe "for brand colors that have a lightness above the lightness threshold" do
      let(:brand_color) { '#FFFFFF' }
      it "returns true" do
        assert(palette.light_brand_color?)
      end
    end

    describe "for brand colors that have a lightness at or below the lightness threshold" do
      let(:brand_color) { '#000000' }
      it "returns false" do
        assert_equal false, palette.light_brand_color?
      end
    end
  end

  describe "#hex_to_hsl and #hsl_to_hex" do
    describe "are the inverse of each other" do
      let(:brand_color) { '#FF0100' }
      it "returns the same value" do
        output = palette.send(:hsl_to_hex, palette.send(:hex_to_hsl, brand_color))
        assert_equal 'FF0100', output
      end
    end
  end
end
