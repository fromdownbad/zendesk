require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportAccounts::Role::NonSupport do
  fixtures :accounts

  let(:support_role) do
    Zendesk::SupportAccounts::Role::NonSupport.new(
      account: account, permission_set_id: permission_set.id, role_name: nil
    )
  end
  let(:account) { accounts(:minimum) }
  let(:permission_set) { PermissionSet.enable_contributor!(account) }
  let(:context) { 'requestcontext' }

  describe '#sync!' do
    it 'skips the syncing' do
      Zendesk::StaffClient.any_instance.expects(:update_or_create_role!).never
      support_role.sync!(context: context)
    end
  end
end
