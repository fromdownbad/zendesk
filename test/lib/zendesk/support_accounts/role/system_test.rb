require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportAccounts::Role::System do
  fixtures :accounts

  let(:support_role) do
    Zendesk::SupportAccounts::Role::System.new(account: account, role_name: role_name, permission_set_id: permission_set_id)
  end
  let(:account) { accounts(:minimum) }
  let(:context) { 'somecontext' }
  let(:permission_set_id) { 9873 }

  describe '#sync!' do
    describe 'agent system role' do
      let(:role_name) { 'agent' }

      describe 'custom roles are enabled for account' do
        before do
          account.stubs(:has_permission_sets?).returns(true)
        end

        describe_with_arturo_enabled :ocp_agent_assignable_on_enterprise do
          it 'does not deactivate role in staff service' do
            Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
              name: role_name,
              assignable: true,
              context: context
            )
            support_role.sync!(context: context)
          end

          it 'deactivates role in staff service when explicitly told to' do
            Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
              name: role_name,
              assignable: false,
              context: context
            )
            support_role.sync!(context: context, enabled: false)
          end
        end

        describe_with_arturo_disabled :ocp_agent_assignable_on_enterprise do
          it 'deactivates role in staff service' do
            Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
              name: role_name,
              assignable: false,
              context: context
            )
            support_role.sync!(context: context)
          end
        end
      end

      describe 'custom roles are disabled for account' do
        before do
          account.stubs(:has_permission_sets?).returns(false)
        end

        it 'activates role in Pravda' do
          Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
            name: role_name,
            assignable: true,
            context: context
          )
          support_role.sync!(context: context)
        end

        describe 'agent role was not originally created in Pravda' do
          before do
            Zendesk::StaffClient.any_instance.stubs(:toggle_role!).raises Kragle::ResourceNotFound
          end

          it 'creates the role in Pravda' do
            Zendesk::StaffClient.any_instance.expects(:create_role!).with(
              title: 'txt.default.roles.support.agent.name',
              description: 'txt.default.roles.support.agent.description',
              name: 'agent',
              max_agents_countable: true,
              is_system_role: true,
              is_active: true,
              assignable: true,
              context: context
            )
            support_role.sync!(context: context)
          end
        end
      end
    end

    describe 'admin system role' do
      let(:role_name) { 'admin' }

      it 'activates role in Pravda' do
        Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
          name: role_name,
          assignable: true,
          context: context
        )
        support_role.sync!(context: context)
      end

      describe 'admin role was not originally created in Pravda' do
        before do
          Zendesk::StaffClient.any_instance.stubs(:toggle_role!).raises Kragle::ResourceNotFound
        end

        it 'creates the role in Pravda' do
          Zendesk::StaffClient.any_instance.expects(:create_role!).with(
            title: 'txt.default.roles.support.admin.name',
            description: 'txt.default.roles.support.admin.description',
            name: 'admin',
            max_agents_countable: true,
            is_system_role: true,
            is_active: true,
            assignable: true,
            context: context
          )
          support_role.sync!(context: context)
        end
      end
    end

    describe 'light agent system role' do
      let(:role_name) { 'light_agent' }

      describe 'light agents are enabled for account' do
        let(:permission_set) { PermissionSet.initialize_light_agent(account) }
        let(:permission_set_id) { permission_set.id }

        before do
          account.permission_sets.stubs(:find_by).with(id: permission_set_id).returns(permission_set)
        end

        describe_with_arturo_enabled :ocp_guide_light_agent_role_sync do
          it 'activates both support and guide light agent roles in Pravda' do
            Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
              name: role_name,
              assignable: true,
              context: context
            )
            Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
              name: role_name,
              assignable: true,
              product: 'guide',
              context: context
            )
            support_role.sync!(context: context)
          end

          describe 'guide light agent synced for the first time' do
            before do
              Zendesk::StaffClient.any_instance.stubs(:toggle_role!).with(
                name: role_name,
                assignable: true,
                context: context
              )
              Zendesk::StaffClient.any_instance.stubs(:toggle_role!).with(
                name: role_name,
                assignable: true,
                product: 'guide',
                context: context
              ).raises Kragle::ResourceNotFound
            end

            it 'creates the role in Pravda' do
              Zendesk::StaffClient.any_instance.expects(:create_role!).with(
                title: "txt.default.roles.guide.light_agent.name",
                description: "txt.default.roles.guide.light_agent.description",
                name: role_name,
                max_agents_countable: false,
                is_system_role: true,
                is_active: true,
                assignable: true,
                product: 'guide',
                context: context
              )
              support_role.sync!(context: context)
            end
          end

          describe 'light agent synced for the first time' do
            before do
              Zendesk::StaffClient.any_instance.stubs(:toggle_role!).raises Kragle::ResourceNotFound
            end

            it 'creates the role in Pravda' do
              Zendesk::StaffClient.any_instance.expects(:create_role!).with(
                title: permission_set.name,
                description: permission_set.description,
                name: role_name,
                max_agents_countable: false,
                is_system_role: true,
                is_active: true,
                assignable: true,
                context: context
              )
              Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
                name: role_name,
                assignable: true,
                product: 'guide',
                context: context
              )
              support_role.sync!(context: context)
            end

            describe 'and light agent permission set has empty description' do
              before do
                permission_set.description = nil
              end

              it 'updates permission set with translated description first' do
                permission_set.expects(:update_columns).with(description: I18n.t('txt.default.roles.light_agent.description')).once
                Zendesk::StaffClient.any_instance.expects(:create_role!).twice
                support_role.sync!(context: context)
              end
            end
          end
        end

        describe_with_arturo_disabled :ocp_guide_light_agent_role_sync do
          it 'activates both support and guide light agent roles in Pravda' do
            Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
              name: role_name,
              assignable: true,
              context: context
            )
            Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
              name: role_name,
              assignable: true,
              product: 'guide',
              context: context
            ).never
            support_role.sync!(context: context)
          end

          describe 'light agent synced for the first time' do
            before do
              Zendesk::StaffClient.any_instance.stubs(:toggle_role!).raises Kragle::ResourceNotFound
            end

            it 'creates the role in Pravda' do
              Zendesk::StaffClient.any_instance.expects(:create_role!).with(
                title: permission_set.name,
                description: permission_set.description,
                name: role_name,
                max_agents_countable: false,
                is_system_role: true,
                is_active: true,
                assignable: true,
                context: context
              )
              Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
                name: role_name,
                assignable: true,
                product: 'guide',
                context: context
              ).never
              support_role.sync!(context: context)
            end

            describe 'and light agent permission set has empty description' do
              before do
                permission_set.description = nil
              end

              it 'updates permission set with translated description first' do
                permission_set.expects(:update_columns).with(description: I18n.t('txt.default.roles.light_agent.description')).once
                Zendesk::StaffClient.any_instance.expects(:create_role!)
                support_role.sync!(context: context)
              end
            end
          end
        end
      end

      describe 'light agents are disabled for account' do
        it 'deactivates role in Pravda' do
          Zendesk::StaffClient.any_instance.expects(:toggle_role!).with(
            name: role_name,
            assignable: false,
            context: context
          )
          support_role.sync!(context: context)
        end

        describe 'light agent role has not been synced' do
          before do
            Zendesk::StaffClient.any_instance.stubs(:toggle_role!).raises Kragle::ResourceNotFound
          end

          it 'does not try and initialise the role' do
            Zendesk::StaffClient.any_instance.expects(:create_role!).never
            support_role.sync!(context: context)
          end
        end
      end
    end

    describe 'unrecognised system role' do
      let(:role_name) { 'custom_123' }

      it 'raises an error' do
        assert_raises(StandardError) do
          support_role.sync!
        end
      end
    end
  end
end
