require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportAccounts::Role::Custom do
  fixtures :accounts

  let(:support_role) do
    Zendesk::SupportAccounts::Role::Custom.new(
      account: account, permission_set_id: permission_set.id, role_name: permission_set.pravda_role_name
    )
  end
  let(:account) { accounts(:minimum) }
  let(:permission_set) { PermissionSet.create(account: account, name: role_title, description: role_description) }
  let(:role_title) { 'awesometitle' }
  let(:role_description) { 'awesomedescription' }
  let(:context) { 'requestcontext' }

  describe '#sync!' do
    describe 'custom roles are enabled for account' do
      before do
        account.stubs(:has_permission_sets?).returns(true)
      end

      describe 'when custom role has null description' do
        let(:role_description) { nil }

        it 'syncs role with name as description to Pravda' do
          Zendesk::StaffClient.any_instance.expects(:update_or_create_role!).with(
            title: role_title,
            description: role_title,
            name: "custom_#{permission_set.id}",
            max_agents_countable: true,
            assignable: true,
            context: context
          )
          support_role.sync!(context: context)
        end
      end

      describe 'when custom role has empty string description' do
        let(:role_description) { '' }

        it 'syncs role with name as description to Pravda' do
          Zendesk::StaffClient.any_instance.expects(:update_or_create_role!).with(
            title: role_title,
            description: role_title,
            name: "custom_#{permission_set.id}",
            max_agents_countable: true,
            assignable: true,
            context: context
          )
          support_role.sync!(context: context)
        end
      end

      it 'activates role in Pravda' do
        Zendesk::StaffClient.any_instance.expects(:update_or_create_role!).with(
          title: role_title,
          description: role_description,
          name: "custom_#{permission_set.id}",
          max_agents_countable: true,
          assignable: true,
          context: context
        )
        support_role.sync!(context: context)
      end

      it 'deactivates role in Pravda when expicitly told to' do
        Zendesk::StaffClient.any_instance.expects(:update_or_create_role!).with(
          title: role_title,
          description: role_description,
          name: "custom_#{permission_set.id}",
          max_agents_countable: true,
          assignable: false,
          context: context
        )
        support_role.sync!(context: context, enabled: false)
      end

      describe 'guide entitlements sync' do
        before do
          Zendesk::StaffClient.any_instance.stubs(:update_or_create_role!)
          Zendesk::SupportAccounts::Internal::GuideRoleEntitlementsSynchronization.any_instance.stubs(:perform?).returns(guide_sync)
        end

        describe 'is required' do
          let(:guide_sync) { true }

          it 'performs the sync' do
            Zendesk::SupportAccounts::Internal::GuideRoleEntitlementsSynchronization.any_instance.expects(:perform!)
            support_role.sync!(context: context)
          end
        end

        describe 'is not required' do
          let(:guide_sync) { false }

          it 'does not perform the sync' do
            Zendesk::SupportAccounts::Internal::GuideRoleEntitlementsSynchronization.any_instance.expects(:perform!).never
            support_role.sync!(context: context)
          end
        end
      end

      describe 'explore entitlements sync' do
        before do
          Zendesk::StaffClient.any_instance.stubs(:update_or_create_role!)
          Zendesk::SupportAccounts::Internal::ExploreRoleEntitlementsSynchronization.any_instance.stubs(:perform?).returns(explore_sync)
        end

        describe 'is required' do
          let(:explore_sync) { true }

          it 'performs the sync' do
            Zendesk::SupportAccounts::Internal::ExploreRoleEntitlementsSynchronization.any_instance.expects(:perform!)
            support_role.sync!(context: context)
          end
        end

        describe 'is not required' do
          let(:explore_sync) { false }

          it 'does not perform the sync' do
            Zendesk::SupportAccounts::Internal::ExploreRoleEntitlementsSynchronization.any_instance.expects(:perform!).never
            support_role.sync!(context: context)
          end
        end
      end
    end

    describe 'custom roles are disabled for account' do
      before do
        account.stubs(:has_permission_sets?).returns(false)
      end

      it 'deactivates role in Pravda' do
        Zendesk::StaffClient.any_instance.expects(:update_or_create_role!).with(
          title: role_title,
          description: role_description,
          name: "custom_#{permission_set.id}",
          max_agents_countable: true,
          assignable: false,
          context: context
        )
        support_role.sync!(context: context)
      end
    end

    describe 'role no longer exists in Support' do
      before do
        permission_set.destroy
      end

      it 'is deleted from Pravda' do
        Zendesk::StaffClient.any_instance.expects(:delete_role!).with(
          name: "custom_#{permission_set.id}",
          context: context
        )
        support_role.sync!(context: context)
      end

      describe 'role no longer exists in Pravda either' do
        before do
          Zendesk::StaffClient.any_instance.stubs(:delete_role!).raises(Kragle::ResourceNotFound)
        end

        it 'logs a warning' do
          Rails.logger.expects(:warn).at_least_once
          support_role.sync!(context: context)
        end
      end

      describe 'service is unavailable' do
        before do
          Zendesk::StaffClient.any_instance.stubs(:delete_role!).raises(Faraday::ConnectionFailed.new('no'))
        end

        it 'fails' do
          assert_raises(Faraday::ConnectionFailed) { support_role.sync!(context: context) }
        end
      end
    end
  end
end
