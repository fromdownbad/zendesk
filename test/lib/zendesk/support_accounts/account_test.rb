require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportAccounts::Account do
  let(:account_model) { accounts(:multiproduct) }
  let(:account) { Zendesk::SupportAccounts::Account.new(record: account_model) }

  describe '#id' do
    it 'returns a unique identifier' do
      assert_equal account_model.id, account.id
    end
  end

  describe '#entitlements_sync_enabled?' do
    describe 'when account has no subscription' do
      before { account_model.stubs(:subscription).returns(NilSubscription.new(account)) }

      it 'is false' do
        refute account.entitlements_sync_enabled?
      end
    end

    describe 'account is multiproduct' do
      let(:account_model) { accounts(:multiproduct) }

      describe 'and account is precreated' do
        before { account_model.stubs(:pre_created_account?).returns(true) }

        it 'is false' do
          refute account.entitlements_sync_enabled?
        end
      end

      describe 'and account is not precreated' do
        before { account_model.stubs(:pre_created_account?).returns(false) }

        it 'is true' do
          assert account.entitlements_sync_enabled?
        end
      end
    end

    describe 'account is not multiproduct' do
      let(:account_model) { accounts(:minimum) }

      describe 'and account is precreated' do
        before { account_model.stubs(:pre_created_account?).returns(true) }

        it 'is false' do
          refute account.entitlements_sync_enabled?
        end
      end

      describe 'and account is not precreated' do
        before { account_model.stubs(:pre_created_account?).returns(false) }

        it 'is true' do
          assert account.entitlements_sync_enabled?
        end
      end
    end
  end

  describe '#role_sync_enabled?' do
    describe 'is not multiproduct' do
      let(:account_model) { accounts(:minimum) }

      it 'is true' do
        assert account.role_sync_enabled?
      end

      describe 'is unconverted precreation account' do
        before do
          account_model.name = 'z3n Precreated Account'
          account_model.subdomain = 'z3nprecreated3185326966'
          account_model.save!
        end

        it 'is false' do
          refute account.role_sync_enabled?
        end
      end
    end

    describe 'is multiproduct' do
      let(:account_model) { accounts(:multiproduct) }

      it 'is true' do
        assert account.role_sync_enabled?
      end

      describe 'when account has no subscription' do
        before { account_model.stubs(:subscription).returns(NilSubscription.new(account)) }

        it 'is false' do
          refute account.role_sync_enabled?
        end
      end
    end
  end
end
