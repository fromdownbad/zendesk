require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportAccounts::Internal::ExploreRoleEntitlementsSynchronization do
  fixtures :permission_sets

  let(:synchronization) { Zendesk::SupportAccounts::Internal::ExploreRoleEntitlementsSynchronization.new(account, permission_set) }
  let(:account) { Account.new }
  let(:account_id) { 983435 }
  let(:permission_set) { PermissionSet.new }
  let(:permissions_changed) { [] }
  let(:synchronization) do
    Zendesk::SupportAccounts::Internal::ExploreRoleEntitlementsSynchronization.new(account, permission_set, permissions_changed)
  end

  before do
    account.stubs(:id).returns(account_id)
  end

  describe '#perform?' do
    describe 'with explore permission set' do
      let(:permission) { PermissionSetPermission.new(permission_set: permission_set, name: 'explore_access') }

      before do
        permission_set.stubs(:permissions).returns([permission])
      end

      describe 'explore permissions are updated' do
        let(:permissions_changed) { ['explore_access'] }

        it 'returns true' do
          assert(synchronization.perform?)
        end
      end

      describe 'no permissions for explore are updated' do
        it 'returns false' do
          assert_equal false, synchronization.perform?
        end
      end
    end

    describe 'with permission set other than explore' do
      let(:permission) { PermissionSetPermission.new(permission_set: permission_set, name: 'forum_access') }

      before do
        permission_set.stubs(:permissions).returns([permission])
      end

      describe 'permissions are updated for other product than explore' do
        let(:permissions_changed) { ['forum_access'] }

        it 'returns false' do
          assert_equal false, synchronization.perform?
        end
      end
    end

    describe 'permission set is nil' do
      let(:permission_set) { nil }

      it 'returns false' do
        assert_equal false, synchronization.perform?
      end
    end
  end

  describe '#perform!' do
    describe 'with assigned users' do
      let(:user1) { User.new }
      let(:user1_id) { 34899123 }
      let(:user2) { User.new }
      let(:user2_id) { 69495321 }
      let(:audit_headers) { {actor_id: -1, ip_address: nil} }

      before do
        user1.stubs(:id).returns(user1_id)
        user2.stubs(:id).returns(user2_id)
        permission_set.stubs(:users).returns([user1, user2])
      end

      it 'syncs explore entitlements for each user' do
        Omnichannel::ExploreEntitlementSyncJob.expects(:enqueue).with(account_id: account_id, user_id: user1_id, audit_headers: audit_headers)
        Omnichannel::ExploreEntitlementSyncJob.expects(:enqueue).with(account_id: account_id, user_id: user2_id, audit_headers: audit_headers)
        synchronization.perform!
      end
    end

    describe 'with no assigned users' do
      it 'performs no syncs' do
        Omnichannel::ExploreEntitlementSyncJob.expects(:enqueue).never
        synchronization.perform!
      end
    end
  end
end
