require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportAccounts::NilProduct do
  fixtures :accounts, :translation_locales

  let(:account) { accounts(:multiproduct) }
  let(:subscription) { account.subscription }
  let(:product) { Zendesk::SupportAccounts::NilProduct.new(account: account, subscription: subscription) }

  describe '#soft_delete!' do
    it 'calls soft_delete! on account model' do
      account.expects(:soft_delete!).with(validate: false).once
      product.soft_delete!
    end
  end

  describe '#save!' do
    it 'calls save! on subscription model' do
      subscription.expects(:save!).once
      product.save!
    end

    it 'calls save! on account model' do
      account.expects(:save!).once
      product.save!
    end

    describe 'missing subscription' do
      let(:subscription) { NilSubscription.new(account) }

      it 'does not call save! on subscription model' do
        # Cannot mock subscription because Mocha won't allow it on objects that don't exist
        # If subscription is nil and save! is called this will raise an error:
        # NoMethodError: undefined method `save!' for #<NilSubscription>
        product.save!
      end
    end
  end

  describe '#save' do
    it 'calls save on subscription model' do
      subscription.expects(:save).once
      product.save
    end

    it 'calls save on account model' do
      account.expects(:save).once
      product.save
    end

    describe 'missing subscription' do
      let(:subscription) { NilSubscription.new(account) }

      it 'does not call save on subscription model' do
        # Cannot mock subscription because Mocha won't allow it on objects that don't exist
        # If subscription is nil and save is called this will raise an error:
        # NoMethodError: undefined method `save' for #<NilSubscription>
        product.save
      end
    end
  end

  describe '#update_plan_settings!' do
    let(:model) { stub }
    it 'calls save! on model' do
      model.expects(:save!)
      product.update_plan_settings!(model)
    end
  end

  describe '#present?' do
    it 'is false' do
      assert_equal false, product.present?
    end
  end

  describe '#blank?' do
    it 'is true' do
      assert(product.blank?)
    end
  end

  describe '#nil?' do
    it 'is true' do
      assert(product.nil?)
    end
  end
end
