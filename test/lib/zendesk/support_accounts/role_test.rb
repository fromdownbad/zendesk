require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportAccounts::Role do
  fixtures :accounts

  let(:support_role) { Zendesk::SupportAccounts::Role.new(account: account) }
  let(:account_id) { 123 }
  let(:account) { Account.new }

  before do
    account.stubs(:id).returns(account_id)
    Account.stubs(:find).with(account_id).returns(account)
  end

  describe '#sync!' do
    it 'is not implemented' do
      assert_raises('Must be implemented by subclass') { support_role.sync!(context: 'controller') }
    end
  end

  describe '.for_account' do
    let(:support_roles) { Zendesk::SupportAccounts::Role.for_account(account.id) }

    describe 'no custom roles' do
      it 'returns agent and admin system roles' do
        assert_equal 2, support_roles.count
      end
    end

    describe 'with roles' do
      let(:light_agent) { PermissionSet.new(name: 'light_agent', role_type: PermissionSet::Type::LIGHT_AGENT) }

      before do
        account.permission_sets = [light_agent]
      end

      it 'returns support roles' do
        assert_equal 3, support_roles.count
        assert_equal ['light_agent', 'admin', 'agent'], support_roles.map(&:role_name)
      end
    end
  end

  describe '.default_for' do
    let(:support_roles) { Zendesk::SupportAccounts::Role.default_for(account.id) }

    it 'returns agent and admin role' do
      assert_equal 2, support_roles.count
      assert_equal ['admin', 'agent'], support_roles.map(&:role_name)
    end
  end
end
