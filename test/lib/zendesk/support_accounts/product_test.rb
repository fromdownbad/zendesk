require_relative "../../../support/test_helper"
require_relative "../../../support/multiproduct_test_helper"

SingleCov.covered!

describe Zendesk::SupportAccounts::Product do
  fixtures :accounts, :translation_locales
  include MultiproductTestHelper

  let(:account) do
    acct = accounts(:multiproduct)
    acct.feature_boost = FeatureBoost.create(account: acct, valid_until: Time.now + 30.days, boost_level: acct.subscription.plan_type)
    acct
  end
  let(:product) { Zendesk::SupportAccounts::Product.retrieve(account) }

  describe '.retrieve' do
    it 'returns the support product' do
      assert_instance_of Zendesk::SupportAccounts::Product, product
    end

    describe 'system account' do
      let(:account) { accounts(:systemaccount) }

      it 'returns the nil product' do
        assert_instance_of Zendesk::SupportAccounts::NilProduct, product
      end
    end

    describe 'pre created account' do
      before do
        account.stubs(:pre_created_account?).returns(true)
      end

      it 'returns the nil product' do
        assert_instance_of Zendesk::SupportAccounts::NilProduct, product
      end
    end

    describe 'missing subscription' do
      before do
        account.subscription = nil
      end

      it 'returns the nil product' do
        assert_instance_of Zendesk::SupportAccounts::NilProduct, product
      end
    end
  end

  describe '#save!' do
    it 'calls save! on subscription model' do
      account.subscription.expects(:save!).once
      product.save!
    end

    it 'calls save! on account model' do
      account.expects(:save!).once
      product.save!
    end

    it 'calls save! on feature boost' do
      account.feature_boost.expects(:save!).once
      product.save!
    end

    describe 'product fields are updated' do
      before do
        account.is_serviceable = false
      end

      it 'syncs the product' do
        Omnichannel::AccountProductSyncJob.expects(:enqueue)
        product.save!
      end
    end

    describe 'feature_boost fields are updated' do
      before do
        account.feature_boost.is_active = !account.feature_boost.is_active
      end

      it 'syncs the product' do
        Omnichannel::AccountProductSyncJob.expects(:enqueue)
        product.save!
      end
    end

    describe 'product fields are not updated' do
      it 'does not sync the product' do
        Omnichannel::AccountProductSyncJob.expects(:enqueue).never
        product.save!
      end
    end
  end

  describe '#save' do
    it 'calls save on subscription model with validation' do
      account.subscription.expects(:save).with(validate: true).once
      product.save
    end

    it 'calls save on feature boost with validation' do
      account.feature_boost.expects(:save).with(validate: true).once
      product.save
    end

    it 'calls save on account model with validation' do
      account.expects(:save).with(validate: true).once
      product.save
    end

    describe 'skip validation' do
      it 'calls save on subscription model without validation' do
        account.subscription.expects(:save).with(validate: false).once
        product.save(validate: false)
      end

      it 'calls save on feature boost without validation' do
        account.feature_boost.expects(:save).with(validate: false).once
        product.save(validate: false)
      end

      it 'calls save on account model without validation' do
        account.expects(:save).with(validate: false).once
        product.save(validate: false)
      end
    end

    describe 'product fields are updated' do
      before do
        account.is_serviceable = false
      end

      it 'syncs the product' do
        Omnichannel::AccountProductSyncJob.expects(:enqueue)
        product.save
      end
    end

    describe 'feature_boost fields are updated' do
      before do
        account.feature_boost.is_active = !account.feature_boost.is_active
      end

      it 'syncs the product' do
        Omnichannel::AccountProductSyncJob.expects(:enqueue)
        product.save
      end
    end

    describe 'product fields are not updated' do
      it 'does not sync the product' do
        Omnichannel::AccountProductSyncJob.expects(:enqueue).never
        product.save
      end
    end

    describe 'there is no feature boost' do
      let(:account) { accounts(:multiproduct) }

      it 'does not raise an exception' do
        Omnichannel::AccountProductSyncJob.expects(:enqueue).never
        product.save
      end
    end
  end

  describe '#soft_delete!' do
    let(:account) { accounts(:minimum) }

    before do
      stub_account_service_products
    end

    it 'calls soft_delete! on account model' do
      account.expects(:soft_delete!).with(validate: false).once
      product.soft_delete!
    end

    it 'syncs the product' do
      Omnichannel::AccountProductSyncJob.expects(:enqueue)
      product.soft_delete!
    end
  end

  describe "#restore!" do
    let(:account) { accounts(:minimum) }
    before do
      account.cancel!
      account.subscription.credit_card = CreditCard.new
      account.subscription.canceled_on = 130.days.ago
      account.subscription.save!
      account.soft_delete!
      account.deleted_at = 40.days.ago
      account.save!

      @audit = account.data_deletion_audits.first
      @audit.created_at = 40.days.ago
      @audit.save!
    end

    it "throws exception if account is partially deleted" do
      @audit.fail!(StandardError.new("test"))
      assert_raises RuntimeError do
        product.restore!
      end
    end

    it "cancels waiting data deletion jobs" do
      product.restore!
      assert @audit.reload.canceled?
    end

    it "un soft-deletes the account" do
      product.restore!
      assert_nil account.reload.deleted_at
    end

    it "resets canceled_on date" do
      Timecop.freeze Date.today
      product.restore!
      assert_equal account.subscription.canceled_on, Date.today
    end

    describe "with a finished moved audit" do
      before do
        audit = account.data_deletion_audits.create(reason: 'moved')
        audit.finish!
      end

      it "un soft-deletes the account" do
        product.restore!
        assert_nil account.reload.deleted_at
      end

      it "does not cancel the audit" do
        product.restore!
        audit = account.data_deletion_audits.find(&:for_shard_move?)
        assert_equal audit.finished?, true
      end
    end
  end

  describe '#to_h' do
    describe 'name' do
      it 'returns the product name' do
        assert_equal 'support', product.to_h[:name]
      end
    end

    describe 'state' do
      let(:actual) { product.to_h[:state] }
      let(:trial_expiry_date) { Date.parse("2017-01-01") }

      describe 'converted precreated account' do
        let(:locale) { translation_locales(:english_by_zendesk) }
        let(:account) do
          account = accounts(:support)
          account.create_pre_account_creation!(
            pod_id: 1,
            locale_id: locale.id,
            region: 'us',
            source: 'Test Account Creation',
            status: PreAccountCreation::ProvisionStatus::FINISHED
          )
          account
        end

        it 'returns product state free' do
          assert_equal :free, actual
        end
      end

      describe 'soft deleted account' do
        let(:account) do
          inactive_account = accounts(:inactive)
          inactive_account.stubs(deleted_at: DateTime.yesterday)
          inactive_account
        end

        it 'returns product state cancelled' do
          assert_equal :cancelled, actual
        end
      end

      describe 'inactive account' do
        let(:account) { accounts(:inactive) }

        it 'returns product state cancelled' do
          assert_equal :cancelled, actual
        end
      end

      describe 'sandbox account' do
        let(:account) do
          account = accounts(:minimum)
          account.stubs(is_sandbox?: true)
          account
        end

        it 'returns product state free' do
          assert_equal :free, actual
        end
      end

      describe 'spoke account' do
        let(:account) do
          account = accounts(:minimum)
          account.subscription.stubs(is_spoke?: true)
          account
        end

        it 'returns product state free' do
          assert_equal :free, actual
        end
      end

      describe 'sponsored account' do
        let(:account) do
          account = accounts(:minimum)
          account.subscription.stubs(is_sponsored?: true)
          account
        end

        it 'returns product state free' do
          assert_equal :free, actual
        end
      end

      describe 'manually paid accounts' do
        let(:account) do
          account = accounts(:minimum)
          account.subscription.stubs(payment_method_type: PaymentMethodType.Manual)
          account
        end

        it 'returns product state free' do
          assert_equal :free, actual
        end
      end

      describe 'legacy sponsered accounts' do
        let(:account) do
          account = accounts(:minimum)
          account.subscription.update_attribute(:is_trial, false)
          account.subscription.update_attribute(:payment_method_type, PaymentMethodType.Credit_Card)
          account
        end

        it 'returns product state free' do
          assert_equal :free, actual
        end
      end

      describe 'expired trial account' do
        let(:account) do
          trial_account = accounts(:trial)
          trial_account.subscription.update_attribute(:trial_expires_on, trial_expiry_date)
          trial_account.expire_trial!
          trial_account
        end

        it 'returns product state expired that has expired' do
          Timecop.travel(trial_expiry_date + 1.day) do
            assert_equal :expired, actual
          end
        end

        it 'returns product state expired that expired the same day' do
          Timecop.travel(trial_expiry_date) do
            assert_equal :expired, actual
          end
        end
      end

      describe 'expired multiproduct trial account' do
        let(:account) do
          trial_account = accounts(:trial)
          trial_account.update_column(:multiproduct, true)
          trial_account.subscription.update_attribute(:trial_expires_on, trial_expiry_date)
          trial_account.expire_trial!
          trial_account
        end

        it 'returns product state expired for a multiproduct trial account that expired on a previous day' do
          Timecop.travel(trial_expiry_date + 1.day) do
            assert_equal :expired, actual
          end
        end
      end

      describe 'account without trial expiry' do
        let(:account) do
          account = accounts(:minimum)
          account.subscription.stubs(trial_expires_on: nil)
          account
        end

        it 'returns product state expired for not serviceable account' do
          account.is_serviceable = false
          assert_equal :expired, actual
        end
      end

      describe 'trial account' do
        let(:account) { accounts(:trial) }

        it 'returns product state trial' do
          assert_equal :trial, actual
        end

        describe 'unknown account state' do
          it 'returns product state unknown' do
            account.subscription.update_attribute(:is_trial, false)
            account.subscription.update_attribute(:payment_method_type, 100)
            assert_equal :unknown, actual
          end
        end

        describe 'without any brand' do
          it 'returns product state not_started' do
            account.brands.delete_all
            assert_equal :not_started, actual
          end
        end
      end
    end

    describe 'trial_expires_at' do
      let(:actual) { product.to_h[:trial_expires_at] }

      before do
        account.subscription.trial_expires_on = Date.parse('2019-01-01')
      end

      it 'returns the products trial expiry' do
        assert_equal '2019-01-01T00:00:00+00:00', actual
      end
    end

    describe 'plan_type' do
      let(:actual) { product.to_h[:plan_settings][:plan_type] }

      describe 'normal account' do
        let(:account) { accounts(:minimum) }

        it 'returns the product plan type' do
          assert_equal SubscriptionPlanType.MEDIUM, actual
        end
      end

      describe 'big account' do
        let(:account) { accounts(:extra_large) }

        it 'returns a big product plan type' do
          assert_equal SubscriptionPlanType.ExtraLarge, actual
        end
      end
    end

    describe 'state_updated_at' do
      let(:actual) { product.to_h[:state_updated_at] }

      describe 'expired trial accounts' do
        let(:trial_expiry_date) { Date.parse("2019-01-01") }
        let(:account) do
          account = accounts(:trial)
          account.subscription.update_attribute(:trial_expires_on, trial_expiry_date)
          account.expire_trial!
          account
        end

        it 'returns the trial expiry datetime' do
          assert_equal '2019-01-01T00:00:00+00:00', actual
        end
      end

      describe 'inactive accounts' do
        let(:account) { accounts(:inactive) }
        let(:churned_on_datetime) { Date.parse("2019-01-01") }

        before do
          account.subscription.update_attribute(:churned_on, churned_on_datetime)
        end

        it 'returns the churned_on datetime if it is present' do
          assert_equal '2019-01-01T00:00:00+00:00', actual
        end

        describe 'churned date missing' do
          let(:churned_on_datetime) { nil }

          it 'is nil' do
            assert_nil actual
          end
        end
      end
    end

    describe 'max_agents' do
      let(:actual) { product.to_h[:plan_settings][:max_agents] }

      describe 'normal account' do
        let(:account) { accounts(:minimum) }

        it 'returns the maximum number of agents' do
          assert_equal 10, actual
        end
      end

      describe 'big account' do
        let(:account) { accounts(:extra_large) }

        it 'returns a lot of agents' do
          assert_equal 100, actual
        end
      end
    end

    describe 'light_agents?' do
      let(:actual) { product.to_h[:plan_settings][:light_agents] }

      before do
        account.subscription.stubs(:has_light_agents?).returns(light_agents)
      end

      describe 'with light agents' do
        let(:light_agents) { true }

        it 'is true' do
          assert(actual)
        end
      end

      describe 'without light agents' do
        let(:light_agents) { false }

        it 'is false' do
          assert_equal false, actual
        end
      end
    end

    describe 'suite' do
      let(:actual) { product.to_h[:plan_settings][:suite] }

      describe 'non suite account' do
        let(:account) { accounts(:minimum) }

        it 'is false' do
          assert_equal false, actual
        end
      end

      describe 'suite account' do
        let(:account) do
          account = accounts(:minimum)
          account.settings.stubs(suite_trial?: true)
          account
        end

        it 'is true' do
          assert(actual)
        end
      end

      describe 'zuroa non-suite account' do
        let(:account) do
          account = accounts(:minimum)
          account.subscription.stubs(zuora_managed?: true)
          account
        end

        it 'is false' do
          assert_equal false, actual
        end
      end

      describe 'zuroa suite account' do
        let(:account) do
          account = accounts(:minimum)
          account.subscription.stubs(zuora_managed?: true)
          account.settings.stubs(suite_subscription?: true)
          account
        end

        it 'is true' do
          assert(actual)
        end
      end
    end

    describe 'side_conversations' do
      let(:actual) { product.to_h[:plan_settings][:side_conversations] }

      before do
        account.subscription.stubs(:has_ticket_threads?).returns(has_ticket_threads)
      end

      describe 'with ticket threads' do
        let(:has_ticket_threads) { true }

        it 'is true' do
          assert(actual)
        end
      end

      describe 'without ticket threads' do
        let(:has_ticket_threads) { false }

        it 'is false' do
          assert_equal false, actual
        end
      end
    end

    describe 'boost info' do
      let(:plan_settings) { product.to_h[:plan_settings] }

      describe 'boost is in effect' do
        before do
          account.feature_boost.boost_level = account.subscription.plan_type + 1
        end

        it 'returns correct boosted_plan_type and boost_expires_at' do
          assert_equal account.feature_boost.boost_level, plan_settings[:boosted_plan_type]
          assert_equal account.feature_boost.valid_until, plan_settings[:boost_expires_at]
        end
      end

      describe 'boost is not active' do
        before do
          account.feature_boost.is_active = false
        end

        it 'returns nil for boosted_plan_type and boost_valid_until' do
          assert_nil plan_settings[:boosted_plan_type]
          assert_nil plan_settings[:boost_expires_at]
        end
      end

      describe 'boost is expired' do
        before do
          account.feature_boost.valid_until = Time.now - 1.week
        end

        it 'returns nil for boosted_plan_type and boost_valid_until' do
          assert_nil plan_settings[:boosted_plan_type]
          assert_nil plan_settings[:boost_expires_at]
        end
      end

      describe 'boost level is not higher than subscription plan type' do
        before do
          account.feature_boost.boost_level = account.subscription.plan_type
        end

        it 'returns nil for boosted_plan_type and boost_valid_until' do
          assert_nil plan_settings[:boosted_plan_type]
          assert_nil plan_settings[:boost_expires_at]
        end
      end

      describe 'there is no boost' do
        before do
          account.feature_boost = nil
        end

        it 'returns nil for boosted_plan_type and boost_valid_until' do
          assert_nil plan_settings[:boosted_plan_type]
          assert_nil plan_settings[:boost_expires_at]
        end
      end
    end

    describe 'with no updated at field' do
      before do
        product.stubs(:state_updated_at).returns(nil)
      end

      it 'does not include that field' do
        refute product.to_h.key?(:state_updated_at), 'Expected state_updated_at field to be removed'
      end
    end
  end

  describe '#plan_settings' do
    it 'returns plan_settings section of product payload' do
      assert_equal product.plan_settings, product.to_h[:plan_settings]
    end
  end

  describe '#update_plan_settings!' do
    let(:model) { stub(account_id: account.id) }

    it 'syncs the product plan_settings' do
      model.expects(:save!)
      Omnichannel::AccountProductPlanSettingsSyncJob.expects(:enqueue).with(account_id: account.id)
      product.update_plan_settings!(model)
    end
  end
end
