require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::SupportAccounts::RoleFactory do
  fixtures :accounts

  describe '.retrieve' do
    let(:role) do
      Zendesk::SupportAccounts::RoleFactory.retrieve(
        account: account, permission_set_id: permission_set_id, role_name: role_name
      )
    end
    let(:account) { accounts(:minimum) }

    describe 'light agent role' do
      let(:permission_set_id) { 123 }
      let(:role_name) { 'light_agent' }

      it 'returns a system role' do
        assert role.is_a?(Zendesk::SupportAccounts::Role::System), "Expected system role, instead got #{role.class}"
      end
    end

    describe 'legacy agent' do
      let(:permission_set_id) { nil }
      let(:role_name) { 'agent' }

      it 'returns a system role' do
        assert role.is_a?(Zendesk::SupportAccounts::Role::System), "Expected system role, instead got #{role.class}"
      end
    end

    describe 'custom role' do
      let(:permission_set_id) { 123 }
      let(:role_name) { 'custom_123' }

      it 'returns a system role' do
        assert role.is_a?(Zendesk::SupportAccounts::Role::Custom), "Expected custom role, instead got #{role.class}"
      end
    end

    describe 'non support role' do
      let(:permission_set_id) { 123 }
      let(:role_name) { nil }

      it 'returns the non support role' do
        assert role.is_a?(Zendesk::SupportAccounts::Role::NonSupport), "Expected contributor role, instead got #{role.class}"
      end
    end
  end
end
