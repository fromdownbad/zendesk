require_relative '../../../support/test_helper'

SingleCov.covered!

class DeprecatedController < ActionController::Base
  include Zendesk::Deprecation::ControllerSupport

  def index
    render plain: "index allowed"
  end

  def show
    render plain: "show allowed"
  end

  def create
    render plain: "creation allowed"
  end
end

class IndexDeprecatedController < DeprecatedController
  deprecate_action :index, with_feature: :deprecation_support_test
end

class MultipleDeprecatedController < DeprecatedController
  deprecate_actions [:show, :create], with_feature: :deprecation_support_test
end

class FullyDeprecatedController < DeprecatedController
  deprecate_controller with_feature: :deprecation_support_test
end

class AllDeprecatedController < DeprecatedController
  deprecate_actions [:all], with_feature: :deprecation_support_test
end

class NoArturoActionDeprecation < DeprecatedController
  deprecate_action :index
end

class NoArturoControllerDeprecation < DeprecatedController
  deprecate_controller
end

class DeprecationControllerSupportTest < ActionController::TestCase
  def self.deprecates_actions(actions)
    actions.each do |action|
      it "returns 410 status for deprecated action '#{action}'" do
        get action
        assert_response :gone
      end
    end
  end

  def self.allows_actions(actions)
    actions.each do |action|
      it "returns 200 status for non-deprecated action '#{action}'" do
        get action
        assert_response :success
      end
    end
  end

  describe "Zendesk::Deprecation::ControllerSupport" do
    fixtures :accounts

    before { Arturo.enable_feature!(:deprecation_support_test) }

    describe 'IndexDeprecatedController' do
      tests IndexDeprecatedController
      use_test_routes

      describe "with an account" do
        before { @request.account = accounts(:minimum) }

        deprecates_actions([:index])
        allows_actions([:show, :create])
      end

      describe "Single action deprecation" do
        deprecates_actions([:index])
        allows_actions([:show, :create])
      end
    end

    describe "Multiple action deprecation" do
      tests MultipleDeprecatedController
      use_test_routes

      deprecates_actions([:show, :create])
      allows_actions([:index])
    end

    describe "Fully deprecated controller" do
      tests FullyDeprecatedController
      use_test_routes

      deprecates_actions([:index, :show, :create])
    end

    describe "All actions deprecated" do
      tests AllDeprecatedController
      use_test_routes

      deprecates_actions([:index, :show, :create])
    end

    describe "Action deprecated without Arturo" do
      tests NoArturoActionDeprecation
      use_test_routes

      deprecates_actions([:index])
    end

    describe "Controller deprecation without Arturo" do
      tests NoArturoControllerDeprecation
      use_test_routes

      deprecates_actions([:index, :show, :create])
    end
  end
end
