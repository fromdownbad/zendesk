require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::CcsAndFollowers::IncompatiblityCheck do
  fixtures :accounts, :devices

  subject { Zendesk::CcsAndFollowers::IncompatiblityCheck.new(account: account) }

  let(:account) { accounts(:minimum) }

  # Note: `account.answer_bot_subscription` tries to reach answerbot API
  before { Account.any_instance.stubs(answer_bot_subscription: nil) }

  it 'creates a new IncompatiblityCheck for the given account' do
    assert_kind_of  Zendesk::CcsAndFollowers::IncompatiblityCheck, subject
    assert_equal account, subject.account
  end

  describe '#incompatible?' do
    describe 'having incompatible features' do
      before { subject.stubs(incompatible_features: [:markdown]) }
      it { assert subject.incompatible? }
    end

    describe 'NOT having any incompatible features' do
      before { subject.stubs(incompatible_features: []) }
      it { refute subject.incompatible? }
    end
  end

  describe '#incompatible_features' do
    describe 'when the account is compatible' do
      it { assert_equal [], subject.incompatible_features }
    end

    describe 'when the account is incompatible' do
      describe 'because the account uses the Mobile App' do
        before { subject.stubs(mobile_app_used?: true) }
        it { assert_includes subject.incompatible_features, :mobile_app }
      end

      describe 'because the account has the markdown editor enabled' do
        before { account.settings.stubs(markdown_ticket_comments?: true) }
        it { assert_includes subject.incompatible_features, :markdown }
      end

      describe 'because the account has voice enabled' do
        before { account.stubs(voice_enabled?: true) }
        it { assert_includes subject.incompatible_features, :voice }
      end

      describe 'because the account has answer bot enabled' do
        before { account.stubs(answer_bot_subscription: stub(max_resolutions: 0, plan_type: 1)) }
        it { assert_includes subject.incompatible_features, :answer_bot }
      end
    end
  end

  describe '#mobile_app_used?' do
    describe 'when the account has mobile app access and a recently used device' do
      before { account.settings.stubs(mobile_app_access?: true) }

      let!(:mobile_phone) do
        new_device = devices(:complete)
        new_device.type = 'MobileDevice'
        new_device.last_active_at = Date.yesterday
        new_device.account = account
        new_device.save!
        new_device
      end

      it { assert subject.mobile_app_used? }
    end

    describe 'when the account does not have mobile app access' do
      before { account.stubs(answer_bot_subscription: true) }
      it { refute subject.mobile_app_used? }
    end
  end

  describe '#summary' do
    describe 'when the account is compatible' do
      it 'says that the account is compatible' do
        summary = "Account '#{account.subdomain}' - compatible"
        assert_equal summary, subject.summary
      end
    end

    describe 'when the account is incompatible' do
      before { account.stubs(voice_enabled?: true) }

      it 'says that the account is incompatible and lists the incompatible features' do
        summary = "Account '#{account.subdomain}' - incompatible features: voice"
        assert_equal summary, subject.summary
      end
    end
  end
end
