require_relative '../../../support/test_helper'
require_relative '../../../support/rule'
require_relative '../../../support/rules_test_helper'
require 'simplediff-ruby'

SingleCov.covered!

describe Zendesk::CcsAndFollowers::RulesCompatibility do
  include TestSupport::Rule::Helper
  include TestSupport::Rule::DiffHelper

  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:compatibility) { Zendesk::CcsAndFollowers::RulesCompatibility.new(account.id) }
  let(:invalid_trigger_title) { 'trigger: old action; no relevant condition' }

  let(:old_action) do
    {
      group: :actions,
      spec: {
        field: 'notification_user',
        operator: 'is',
        value: ['requester_id', 'Email subject', 'Email body']
      }
    }
  end

  let(:new_action) do
    {
      group: :actions,
      spec: {
        field: 'notification_user',
        operator: 'is',
        value: ['requester_and_ccs', 'Email subject', 'Email body']
      }
    }
  end

  let(:nps_action) do
    {
      group: :actions,
      spec: {
        field: 'satisfaction_score',
        operator: 'is',
        value: ['1']
      }
    }
  end

  let(:nps_action_2) do
    {
      group: :actions,
      spec: {
        field: 'satisfaction_score',
        operator: 'is',
        value: [1]
      }
    }
  end

  let(:condition_to_remove_1) do
    {
      group: :conditions,
      spec: {
        field: 'requester_id',
        operator: 'is_not',
        value: 'current_user'
      }
    }
  end

  let(:conditions_to_remove) { [condition_to_remove_1] }

  let(:new_condition) do
    {
      group: :conditions,
      spec: {
        field: 'comment_is_public',
        operator: 'is',
        value: true
      }
    }
  end

  let(:other_action) do
    {
      group: :actions,
      spec: {
        field: 'notification_user',
        operator: 'is',
        value: ['assignee_id', 'Email subject', 'Email body']
      }
    }
  end

  let(:other_condition) do
    {
      group: :conditions,
      spec: {
        field: 'comment_is_public',
        operator: 'is',
        value: false
      }
    }
  end

  let(:triggers_that_should_be_updated) do
    [
      {
        description: invalid_trigger_title,
        source: [old_action, other_condition],
        expected: [
          change_object('-', build_definition_item(old_action[:spec])),
          change_object('+', build_definition_item(new_action[:spec])),
          change_object('=', build_definition_item(other_condition[:spec])),
          change_object('+', build_definition_item(new_condition[:spec]))
        ]
      },
      {
        description: 'trigger: old action; new condition',
        source: [old_action, new_condition],
        expected: [
          change_object('-', build_definition_item(old_action[:spec])),
          change_object('+', build_definition_item(new_action[:spec])),
          change_object('=', build_definition_item(new_condition[:spec]))
        ]
      },
      {
        description: 'trigger: old action; conditions to remove',
        source: [old_action] + conditions_to_remove,
        expected: [
          change_object('-', build_definition_item(old_action[:spec])),
          change_object('+', build_definition_item(new_action[:spec])),
          change_object('-', build_definition_item(condition_to_remove_1[:spec])),
          change_object('+', build_definition_item(new_condition[:spec]))
        ]
      }
    ]
  end

  let(:triggers_that_should_not_be_updated) do
    [
      {
        description: 'trigger: new action; new condition',
        source: [new_action, new_condition],
        expected: [
          change_object('=', build_definition_item(new_action[:spec])),
          change_object('=', build_definition_item(new_condition[:spec]))
        ]
      },
      {
        description: 'trigger: no relevant action; new condition',
        source: [other_action, new_condition],
        expected: [
          change_object('=', build_definition_item(other_action[:spec])),
          change_object('=', build_definition_item(new_condition[:spec]))
        ]
      },
      {
        description: 'trigger: no relevant action; no relevant condition',
        source: [other_action, other_condition],
        expected: [
          change_object('=', build_definition_item(other_action[:spec])),
          change_object('=', build_definition_item(other_action[:spec]))
        ]
      },
      {
        description: 'trigger: new action; conditions to remove',
        source: [new_action] + conditions_to_remove,
        expected: [
          change_object('=', build_definition_item(new_action[:spec])),
          change_object('-', build_definition_item(condition_to_remove_1[:spec])),
          change_object('+', build_definition_item(new_condition[:spec]))
        ]
      },
      {
        description: 'trigger: new action; no relevant condition',
        source: [new_action, other_condition],
        expected: [
          change_object('=', build_definition_item(new_action[:spec])),
          change_object('=', build_definition_item(other_condition[:spec])),
          change_object('+', build_definition_item(new_condition[:spec]))
        ]
      }
    ]
  end

  let(:all_triggers) do
    triggers_that_should_be_updated + triggers_that_should_not_be_updated
  end

  describe '#update_requester_target' do
    let(:standard_automation_conditions) do
      [
        {
          group: :conditions,
          spec: {
            field: 'status_id',
            operator: 'less_than',
            value: [StatusType.CLOSED]
          }
        },
        {
          group: :conditions,
          spec: {
            field: 'SOLVED',
            operator: 'is',
            value: ['24']
          }
        }
      ]
    end

    let(:standard_automation_expectations) do
      standard_automation_conditions.map do |condition|
        change_object('=', build_definition_item(condition[:spec]))
      end
    end

    let(:nps_automations_that_should_not_be_updated) do
      [
        {
          description: 'automation: old action; NPS action',
          source: [old_action, nps_action] + standard_automation_conditions,
          expected: [
            change_object('=', build_definition_item(old_action[:spec])),
            change_object('=', build_definition_item(nps_action[:spec]))
          ] + standard_automation_expectations
        },
        {
          description: 'automation: old action; NPS action with different format',
          source: [old_action, nps_action_2] + standard_automation_conditions,
          expected: [
            change_object('=', build_definition_item(old_action[:spec])),
            change_object('=', build_definition_item(nps_action_2[:spec]))
          ] + standard_automation_expectations
        }
      ]
    end

    let(:automations_that_should_be_updated) do
      [
        {
          description: 'automation: old action',
          source: [old_action] + standard_automation_conditions,
          expected: [
            change_object('-', build_definition_item(old_action[:spec])),
            change_object('+', build_definition_item(new_action[:spec]))
          ] + standard_automation_expectations
        }
      ]
    end

    let(:automations_that_should_not_be_updated) do
      [
        {
          description: 'automation: new action',
          source: [new_action] + standard_automation_conditions,
          expected: [
            change_object('=', build_definition_item(new_action[:spec]))
          ] + standard_automation_expectations
        },
        {
          description: 'automation: other action',
          source: [other_action] + standard_automation_conditions,
          expected: [
            change_object('=', build_definition_item(other_action[:spec]))
          ] + standard_automation_expectations
        }
      ]
    end

    let(:all_automations) do
      automations_that_should_be_updated + automations_that_should_not_be_updated
    end

    let(:all_rules) do
      all_triggers + all_automations
    end

    describe 'instrumentation' do
      it 'sends execution time to DataDog' do
        Zendesk::StatsD::Client.any_instance.expects(:time).
          with('compatibility.rules', tags: ['update:requester_target'])
        compatibility.update_requester_target
      end
    end

    describe 'when nps rules are present' do
      before do
        reset_account_rules!(automations: nps_automations_that_should_not_be_updated)
      end

      let(:args) do
        { dry_run: false }
      end

      it 'returns an empty diff' do
        assert_empty compatibility.update_requester_target(args)
      end

      it 'does not update any rules' do
        Rule.any_instance.expects(:save!).never
        compatibility.update_requester_target(args)
      end
    end

    describe 'when there are rules to update' do
      before do
        reset_account_rules!(triggers: all_triggers, automations: all_automations)
      end

      describe_with_arturo_disabled :email_ccs do
        let(:args) do
          { dry_run: false }
        end

        it 'returns an empty diff' do
          assert_empty compatibility.update_requester_target(args)
        end

        it 'does not update any rules' do
          Rule.any_instance.expects(:save!).never
          compatibility.update_requester_target(args)
        end
      end

      describe_with_arturo_enabled :email_ccs do
        let(:all_triggers) { triggers_that_should_be_updated }

        describe 'when no parameters are given' do
          it "doesn't update data by default" do
            Rule.any_instance.expects(:save!).never
            compatibility.update_requester_target
          end
        end

        describe 'when dry_run parameter is true' do
          let(:args) do
            { dry_run: true }
          end

          it 'does not update any rules' do
            Rule.any_instance.expects(:save!).never
            compatibility.update_requester_target(args)
          end

          it 'returns a diff set for rules that should be changed' do
            diff = compatibility.update_requester_target(args)
            assert_equal triggers_that_should_be_updated.size + automations_that_should_be_updated.size, diff.size

            assert diff_matches_expected?(diff, all_rules)
          end

          describe 'and a rule is invalid' do
            before do
              invalidate_rule(rule_for_title(invalid_trigger_title, account.rules.to_a))
            end

            it 'excludes the invalid rule in the diff set for rules that should be changed' do
              diff = compatibility.update_requester_target(args)
              assert_equal triggers_that_should_be_updated.size - 1 + automations_that_should_be_updated.size, diff.size
              assert rule_for_title(invalid_trigger_title, diff).nil?
            end
          end
        end

        describe 'when dry_run parameter is false' do
          let(:args) do
            { dry_run: false }
          end

          it 'returns a diff set for rules that should be changed' do
            diff = compatibility.update_requester_target(args)
            assert_equal triggers_that_should_be_updated.size + automations_that_should_be_updated.size, diff.size

            assert diff_matches_expected?(diff, all_rules)
          end

          describe 'and rules should be updated' do
            it 'updates rules' do
              Rule.any_instance.expects(:save!).times(triggers_that_should_be_updated.size + automations_that_should_be_updated.size)
              refute_empty compatibility.update_requester_target(args)
            end

            describe 'and a rule is invalid' do
              before do
                invalidate_rule(rule_for_title(invalid_trigger_title, account.rules.to_a))
              end

              it 'does not attempt to update the invalid rule' do
                Rule.any_instance.expects(:save!).times(triggers_that_should_be_updated.size - 1 + automations_that_should_be_updated.size)
                diff = compatibility.update_requester_target(args)
                assert rule_for_title(invalid_trigger_title, diff).nil?
                refute_empty diff
              end
            end
          end

          describe 'and rule contains matching conditions in both `ALL` & `ANY` conditions' do
            let(:all_triggers) do
              [
                {
                  description: 'trigger: old action; new condition',
                  source: [old_action, condition_to_remove_1, conditions_any_item],
                  expected: [
                    change_object('-', build_definition_item(old_action[:spec])),
                    change_object('+', build_definition_item(new_action[:spec])),
                    change_object('-', build_definition_item(condition_to_remove_1[:spec])),
                    change_object('+', build_definition_item(new_condition[:spec])),
                    change_object('-', build_definition_item(conditions_any_item[:spec]))
                  ]
                }
              ]
            end

            let(:conditions_any_item) do
              {
                group: :conditions_any,
                spec: {
                  field: 'requester_id',
                  operator: 'is_not',
                  value: 'current_user'
                }
              }
            end

            let(:all_automations) { [] }
            let(:diff_items) { compatibility.update_requester_target(args) }

            it 'returns a diff set for rules that should be changed' do
              assert diff_matches_expected?(diff_items, all_triggers)
            end

            it 'adds the new condition to `ALL` conditions & removes old condition' do
              assert_equal(
                diff_items.first.conditions_all,
                [
                  change_object('-', build_definition_item(condition_to_remove_1[:spec])),
                  change_object('+', build_definition_item(new_condition[:spec]))
                ]
              )
            end

            it 'does not add the condition to `ANY` conditions & removes old condition' do
              assert_equal(
                diff_items.first.conditions_any,
                [
                  change_object('-', build_definition_item(conditions_any_item[:spec]))
                ]
              )
            end
          end
        end
      end

      describe 'when there are no rules to update' do
        let(:args) do
          { dry_run: false }
        end

        describe_with_and_without_arturo_enabled :email_ccs do
          before do
            reset_account_rules!(triggers: triggers_that_should_not_be_updated,
                                 automations: automations_that_should_not_be_updated)
          end

          it 'returns an empty diff' do
            assert_empty compatibility.update_requester_target(args)
          end

          it 'does not update any rules' do
            Rule.any_instance.expects(:save!).never
            compatibility.update_requester_target(args)
          end
        end
      end
    end
  end

  private

  def reset_account_rules!(triggers: [], automations: [])
    Rule.without_arsi.delete_all

    triggers_for_account = triggers.map do |test_case|
      items = test_case[:source].group_by { |args| args[:group] }
      items[:conditions_any] ||= []

      definition = build_trigger_definition(
        conditions_all: items[:conditions].map { |args| build_definition_item(args[:spec]) },
        conditions_any: items[:conditions_any].map { |args| build_definition_item(args[:spec]) },
        actions: items[:actions].map { |args| build_definition_item(args[:spec]) }
      )
      create_trigger(account: account, title: test_case[:description], definition: definition)
    end

    automations_for_account = automations.map do |test_case|
      items = test_case[:source].group_by { |args| args[:group] }

      definition = build_automation_definition(
        conditions_all: items[:conditions].map { |args| build_definition_item(args[:spec]) },
        conditions_any: [],
        actions: items[:actions].map { |args| build_definition_item(args[:spec]) }
      )
      create_automation(owner: account, title: test_case[:description], definition: definition)
    end

    assert_equal account.rules.size, triggers_for_account.size + automations_for_account.size
  end

  def diff_matches_expected?(diff, rules)
    diff.each_index.all? do |rule_index|
      diff_item = diff[rule_index]
      actions_and_conditions = diff_item.actions +
                               diff_item.conditions_all +
                               diff_item.conditions_any
      rule_definition = rule_for_description(diff_item.title.first.content, rules: rules)
      rule_definition[:expected].each_index.all? do |definition_item_index|
        rule_definition[:expected][definition_item_index] == actions_and_conditions[definition_item_index]
      end
    end
  end

  def rule_for_description(description, rules: all_rules)
    rules.find { |rule| rule[:description] == description }
  end

  # Find the ActiveRecord with the specified title
  def rule_for_title(title, rules)
    rules.find { |rule| rule.title == title }
  end

  # Updates a rule to fail ActiveRecord validation (#valid?)
  def invalidate_rule(rule)
    rule.definition.actions.first.source = 'deleted_custom_field'
    rule.save(validate: false)
  end
end
