require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Explore::InternalApiClient do
  let(:account) { accounts(:minimum) }

  describe "#initialize" do
    it 'finds deleted accounts' do
      account.cancel!
      account.soft_delete!
      KragleConnection.expects(:build_for_explore).returns(Kragle.new('explore'))
      Zendesk::Explore::InternalApiClient.new(account.subdomain)
    end

    it 'sets the `timeout` to the default value' do
      assert_equal(
        Zendesk::Explore::InternalApiClient::REQUEST_TIMEOUT,
        Zendesk::Explore::InternalApiClient.
          new(account.subdomain).
          send(:timeout)
      )
    end

    describe 'when initialized with `timeout`' do
      it 'sets the `timeout` to the provided value' do
        assert_equal(
          20,
          Zendesk::Explore::InternalApiClient.
            new(account.subdomain, timeout: 20.seconds).
            send(:timeout)
        )
      end
    end
  end

  describe '#account_deleted' do
    it 'makes a DELETE request' do
      request = stub_request(
        :post,
        "https://proxy-nlb.pod-1.zdsystest.com/explore/zendesk/webhooks/account"
      ).with(headers: {'Host' => 'minimum.zendesk-test.com'})
      Zendesk::Explore::InternalApiClient.new(account.subdomain).account_deleted!
      assert_requested(request)
    end
  end
end
