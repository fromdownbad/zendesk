require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 11

describe Zendesk::Reports::Processor do
  fixtures :accounts, :users, :user_identities, :groups, :tickets, :integers, :memberships, :ticket_fields, :custom_field_options, :subscriptions

  before do
    @account   = Account.new
    @processor = Zendesk::Reports::Processor.new(@account, User.new, Report.new)
  end

  describe "#process" do
    describe "for an account with more than 20_000 tickets" do
      before do
        @processor.expects(:runs_in_background?).returns(true)
      end

      it "flags as enqueued when the job was enqueued" do
        ReportJob.expects(:enqueue).once
        @processor.process
        assert @processor.enqueued?
        refute @processor.throttled?
      end

      it "flags as throttled when the job was throttled" do
        ReportJob.expects(:enqueue).once.raises(Resque::ThrottledError)
        @processor.process
        refute @processor.enqueued?
        assert @processor.throttled?
      end
    end

    describe "for an account with less than 20_000 tickets" do
      before do
        @processor.expects(:runs_in_background?).returns(false)
      end

      it "flags as enqueued when the job was enqueued" do
        ReportJob.expects(:enqueue).never
        @processor.process
        refute @processor.enqueued?
        refute @processor.throttled?
      end
    end
  end

  describe "#message" do
    describe "when no job was backgrounded" do
      before do
        @processor.expects(:runs_in_background?).returns(false)
      end

      it "does not have a message" do
        assert_nil @processor.message
      end
    end

    describe "whan a job was backgrounded" do
      before do
        @processor.expects(:runs_in_background?).returns(true)
      end

      describe "for an enqueued job" do
        before { @processor.expects(:enqueued?).returns(true) }

        describe "with no previous data" do
          before { @processor.expects(:has_earlier_data?).returns(false) }

          it "gives an appropriate message" do
            assert_equal "An update is being created for this report. You will receive an email when it's ready. Check back in a few minutes.", @processor.message
          end
        end

        describe "with previous data" do
          before { @processor.expects(:has_earlier_data?).returns(true) }

          it "gives an appropriate message" do
            assert_equal "An update is being created for this report. You will receive an email when it's ready. Now presenting the previous result.", @processor.message
          end
        end
      end

      describe "for a throttled job" do
        before do
          @processor.expects(:enqueued?).returns(false)
          @processor.expects(:throttled?).returns(true)
        end

        describe "with no previous data" do
          before { @processor.expects(:has_earlier_data?).returns(false) }

          it "gives an appropriate message" do
            assert_equal "This report can only be rebuilt every 30 minutes. Check back in a few minutes.", @processor.message
          end
        end

        describe "with previous data" do
          before { @processor.expects(:has_earlier_data?).returns(true) }

          it "gives an appropriate message" do
            assert_equal "This report can only be rebuilt every 30 minutes. Now presenting the previous result.", @processor.message
          end
        end
      end
    end
  end
end
