require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Mailer::SimplifiedEmailThreading do
  fixtures :accounts, :events, :users, :tickets

  let(:account) { nil }
  let(:email_ccs_added) { false }
  let(:for_agent) { nil }

  class SimplifiedEmailThreadingTestMailer
    include Zendesk::Mailer::SimplifiedEmailThreading

    attr_reader :account, :for_agent

    def initialize(account, email_ccs_added, for_agent)
      @account = account
      @email_ccs_added = email_ccs_added
      @for_agent = for_agent
    end
  end

  subject { SimplifiedEmailThreadingTestMailer.new(account, email_ccs_added, for_agent) }

  describe "email_ccs_added?" do
    let(:account) { accounts(:minimum) }
    let(:ticket)  { tickets(:minimum_1) }
    let(:audit) { events(:create_audit_for_minimum_ticket_1) }
    let(:notification) { nil }
    let(:ccs) { [users(:minimum_end_user2), users(:minimum_author)] }
    let(:to_recipient_ids) do
      [users(:minimum_agent).id, users(:minimum_end_user).id]
    end

    describe_with_arturo_setting_enabled :follower_and_email_cc_collaborations do
      describe "when notification type is not NotificationWithCcs" do
        let(:notification) do
          Notification.new(
            subject: '{{current_user.name}} has updated {{ticket.title}}',
            body: '{{ticket.url}}',
            author: users(:minimum_agent),
            recipients: ['example@example.com']
          )
        end

        it "returns false" do
          refute subject.email_ccs_added?(notification)
        end

        describe "when notification type is NotificationWithCcs" do
          let(:notification) do
            NotificationWithCcs.new(
              account: account,
              ticket: ticket,
              subject: 'My printer is on fire',
              body: 'What should I do?',
              audit: audit,
              to_recipient_ids: to_recipient_ids,
              ccs: ccs,
              via_id: ViaType.RULE_REVISION,
              via_reference_id: 10007
            )
          end

          describe "and EmailCcChange audit event is absent" do
            it "returns false" do
              refute subject.email_ccs_added?(notification)
            end
          end

          describe "email ccs have changed" do
            before do
              event = EmailCcChange.new.tap do |change|
                change.current_email_ccs = [users(:minimum_end_user2).email]
                change.previous_email_ccs = []
              end
              notification.audit.events << event
              notification.save!
            end

            it "returns true" do
              assert subject.email_ccs_added?(notification)
            end
          end
        end
      end
    end

    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      describe_with_arturo_setting_enabled :collaboration_enabled do
        describe "when notification type is not Cc" do
          let(:notification) do
            NotificationWithCcs.new(
              account: account,
              ticket: ticket,
              subject: 'My printer is on fire',
              body: 'What should I do?',
              audit: audit,
              to_recipient_ids: to_recipient_ids,
              ccs: ccs,
              via_id: ViaType.RULE_REVISION,
              via_reference_id: 10007
            )
          end

          it "returns false" do
            refute subject.email_ccs_added?(notification)
          end
        end

        describe "when notification type is Cc" do
          let(:notification) do
            Cc.new(
              subject: '{{current_user.name}} has updated {{ticket.title}}',
              body: '{{ticket.url}}',
              author: users(:minimum_agent),
              recipients: ['example@example.com'],
              audit: audit,
              via_id: ViaType.RULE_REVISION,
              via_reference_id: 10007
            )
          end

          describe "and Change audit event with field name current_collaborators is absent" do
            it "returns false" do
              refute subject.email_ccs_added?(notification)
            end
          end

          describe "and Change audit event with field name current_collaborators is present" do
            describe "and collaborators have been added" do
              before do
                event = Change.new.tap do |change|
                  change.field_name = "current_collaborators"
                  change.value = ccs.map(&:email).compact
                  change.value_previous = ""
                end
                notification.audit.events << event
                notification.save!
              end

              it "returns true" do
                assert subject.email_ccs_added?(notification)
              end
            end

            describe "and collaborators have been removed" do
              before do
                event = Change.new.tap do |change|
                  change.field_name = "current_collaborators"
                  change.value = ""
                  change.value_previous = ccs.map(&:email).compact
                end
                notification.audit.events << event
                notification.save!
              end

              it "returns false" do
                refute subject.email_ccs_added?(notification)
              end
            end
          end
        end
      end
    end

    describe_with_arturo_setting_disabled :follower_and_email_cc_collaborations do
      describe_with_arturo_disabled :collaboration_enabled do
        it "returns false" do
          refute subject.email_ccs_added?(notification)
        end
      end
    end
  end

  describe "show_last_three_comments?" do
    let(:template_body) { "Your request was updated {{ticket.latest_comment_html}}" }

    describe "when account is nil" do
      it "returns false" do
        refute subject.show_last_three_comments?(template_body)
      end
    end

    describe "when account is present" do
      let(:account) { accounts(:minimum) }

      describe_with_arturo_disabled :email_simplified_threading do
        it "returns false" do
          refute subject.show_last_three_comments?(template_body)
        end
      end

      describe_with_arturo_enabled :email_simplified_threading do
        describe "when email ccs were not added" do
          it "returns false" do
            refute subject.show_last_three_comments?(template_body)
          end
        end

        describe "when email ccs were added" do
          let(:email_ccs_added) { true }

          describe "and for_agent is true" do
            let(:for_agent) { true }

            it "returns false" do
              refute subject.show_last_three_comments?(template_body)
            end
          end

          describe "and for_agent is false" do
            let(:for_agent) { false }

            describe "and template body contains one of the replaceable placeholders" do
              it "returns true" do
                assert subject.show_last_three_comments?(template_body)
              end
            end

            describe "and template body does not contain one of the replaceable placeholders" do
              let(:template_body) { "Your request was updated. {{ticket.quoted_comments}}" }

              it "returns false" do
                refute subject.show_last_three_comments?(template_body)
              end
            end
          end
        end
      end
    end
  end
end
