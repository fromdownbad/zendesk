require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 5

describe Zendesk::Mailer::GmailSending do
  def send_mail(from)
    m = ApplicationMailer.create_generic(account, from, 'recipient@zendesk.com', 'title', 'body')
    m['DKIM-Signature'] = "Another one ..." # simulate gmail buttons adding another dkim signature
    yield m if block_given?
    m.deliver_now
  end

  def assert_sending_via_api(body_pattern: //, fallback: false)
    get_token = stub_request(:post, "https://accounts.google.com/o/oauth2/token").
      with(body: {"client_id" => "mail_fetcher_client_id", "client_secret" => "mail_fetcher_client_secret", "grant_type" => "refresh_token", "refresh_token" => "adadadadddadadada"}).
      to_return(body: '{"access_token": "ACCESS_TOKEN", "expires_in":"123"}')
    send_email = stub_request(:post, "https://www.googleapis.com/upload/gmail/v1/users/me/messages/send?uploadType=media").
      with(body: body_pattern, headers: {'Authorization' => 'Bearer ACCESS_TOKEN', 'Content-Type' => 'message/rfc822'})

    yield

    ActionMailer::Base.deliveries.size.must_equal(fallback ? 1 : 0)
    assert_requested get_token
    assert_requested send_email
  end

  let(:account) { accounts(:minimum) }
  let(:from) { credential.username }
  let(:unencrypted_token) { 'adadadadddadadada' }
  let(:valid_attributes) do
    {
      account: account,
      username: "USERNAME@gmail.net",
      current_user: users(:minimum_agent)
    }
  end

  let!(:credential) do
    ExternalEmailCredential.new(valid_attributes).tap do |external_email_credential|
      external_email_credential.encrypted_value = unencrypted_token
      external_email_credential.save!
    end
  end

  before do
    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries.clear
    stub_request(:post, %r{accounts.google.com})
    ZendeskExceptions::Logger.expects(:record).never
  end

  describe "sending using gmail api" do
    it "works with pure email" do
      assert_sending_via_api { send_mail from }
    end

    it "works with email including name" do
      assert_sending_via_api do
        send_mail "\"Michael G (Michaels's Account #1 <foo@bar.com>)\" <#{from}>"
      end
    end

    it "sends bcc" do
      assert_sending_via_api body_pattern: /Bcc: bbc@archive.com\n/ do
        send_mail from do |m|
          m.bcc = ["bbc@archive.com"]
        end
      end
    end

    it "does not add dkim since it would be invalid" do
      assert_sending_via_api body_pattern: -> (mail) { mail.wont_include 'DKIM-Signature'; true } do
        send_mail from
      end
    end

    it "uses encrypted token to set cache key" do
      Rails.cache.stubs(:read)

      Rails.cache.
        expects(:read).
        with("#{credential.account.id}/gmail_sending.access_token.#{credential.encrypted_value}")

      assert_sending_via_api { send_mail from }
    end
  end

  describe "gmail api delivery error" do
    before do
      Rails.application.config.statsd.client.expects(:increment).never
    end

    it "sends via smtp and reports error" do
      begin
        ActionMailer::Base.test_settings[:a] = 1

        assert_sending_via_api fallback: true do
          stub_request(:post, "https://www.googleapis.com/upload/gmail/v1/users/me/messages/send?uploadType=media").
            to_return(status: 302)

          Rails.application.config.statsd.client.expects(:increment).with('gmail_sending.delivery_error', tags: ['response_code:302'])
          send_mail from
        end

        # passed all settings to delivery method ?
        notification = ActionMailer::Base.deliveries.last
        notification.delivery_method.settings[:a].must_equal 1
      ensure
        ActionMailer::Base.test_settings.delete(:a)
      end
    end

    it "does not retry the next time when api rate limit is exceeded server side and sends warning" do
      assert_sending_via_api fallback: true do
        stub_request(:post, "https://www.googleapis.com/upload/gmail/v1/users/me/messages/send?uploadType=media").
          to_return(status: 429)

        Rails.application.config.statsd.client.expects(:increment).with('gmail_sending.delivery_error', tags: ['response_code:429'])
        Rails.application.config.statsd.client.expects(:increment).with('gmail_sending.warning_sent')

        send_mail from

        notification = ActionMailer::Base.deliveries.shift
        notification.subject.must_equal "You've reached your Google sending limit for username@gmail.net"
      end
    end

    it "does not report or disable when gmail is broken" do
      ActionMailer::Base.test_settings[:a] = 1

      assert_sending_via_api fallback: true do
        stub_request(:post, "https://www.googleapis.com/upload/gmail/v1/users/me/messages/send?uploadType=media").
          to_return(status: 500, body: %({"foobar": "Backend Error"}))

        Rails.application.config.statsd.client.expects(:increment).with('gmail_sending.delivery_error', tags: ['response_code:500'])

        send_mail from

        notification = ActionMailer::Base.deliveries.last
        notification.delivery_method.settings[:a].must_equal 1
      end

      # setting was disabled to prevent further sending
      account.reload.settings.send_gmail_messages_via_gmail.must_equal true
    end

    it "does not report when gmail api is disabled" do
      assert_sending_via_api fallback: true do
        stub_request(:post, "https://www.googleapis.com/upload/gmail/v1/users/me/messages/send?uploadType=media").
          to_return(status: 400, body: %({"foobar": "Mail service not enabled"}))

        Rails.application.config.statsd.client.expects(:increment).with('gmail_sending.delivery_error', tags: ['response_code:400'])

        send_mail from

        notification = ActionMailer::Base.deliveries.shift
        notification.subject.must_equal "An issue sending email via Gmail"
      end

      # setting was disabled to prevent further sending
      account.reload.settings.send_gmail_messages_via_gmail.must_equal false
    end

    it "does not report when using bad gmail credentials" do
      assert_sending_via_api fallback: true do
        stub_request(:post, "https://www.googleapis.com/upload/gmail/v1/users/me/messages/send?uploadType=media").
          to_return(status: 401, body: %({"domain": "global","reason": "authError","message": "Invalid Credentials"}))

        Rails.application.config.statsd.client.expects(:increment).with('gmail_sending.delivery_error', tags: ['response_code:401'])

        send_mail from

        notification = ActionMailer::Base.deliveries.shift
        notification.subject.must_equal "An issue sending email via Gmail"
      end

      # setting was disabled to prevent further sending
      account.reload.settings.send_gmail_messages_via_gmail.must_equal false
    end

    it "falls back to smtp when api times out" do
      assert_sending_via_api fallback: true do
        stub_request(:post, "https://www.googleapis.com/upload/gmail/v1/users/me/messages/send?uploadType=media").and_timeout

        Rails.application.config.statsd.client.expects(:increment).with('gmail_sending.delivery_error', tags: ['response_code:-1'])
        send_mail from
      end
    end
  end

  describe "when the email has attachments" do
    class AttachmentMailer < ApplicationMailer
      def with_attachments(account, from)
        set_headers(account)
        attachments['test.txt'] = { mime_type: 'text/plain', content: 'Hello World!' }
        mail(subject: 'SUB', body: 'BODY', from: from)
      end
    end

    before do
      stub_request(:post, "https://accounts.google.com/o/oauth2/token").
        to_return(body: '{"access_token": "ACCESS_TOKEN", "expires_in":"123"}')
    end

    let(:mail) { AttachmentMailer.create_with_attachments(account, from) }

    it "should send via GMail API" do
      mail.delivery_method.must_be_instance_of Zendesk::Mailer::GmailSending::GmailDeliveryMethod
      assert_requested :post, /accounts\.google\.com/
    end

    describe_with_arturo_disabled :email_real_attachments_send_via_gmail do
      it "does not send via GMail API" do
        mail.delivery_method.wont_be_instance_of Zendesk::Mailer::GmailSending::GmailDeliveryMethod
        assert_not_requested :post, /accounts\.google\.com/
      end
    end
  end

  it "sends via smtp when using a different email" do
    send_mail account.default_recipient_address.email
    ActionMailer::Base.deliveries.size.must_equal 1
  end

  it "sends via smtp when access token request fails" do
    Zendesk::Mailer::GmailSending.expects(:report_error)
    send_mail from
    ActionMailer::Base.deliveries.size.must_equal 1
  end

  it "sends via smtp when access token request returns strange data" do
    get_token = stub_request(:post, "https://accounts.google.com/o/oauth2/token").
      with(body: {"client_id" => "mail_fetcher_client_id", "client_secret" => "mail_fetcher_client_secret", "grant_type" => "refresh_token", "refresh_token" => "adadadadddadadada"}).
      to_return(body: '{"error": "foo"}')
    Zendesk::Mailer::GmailSending.expects(:report_error)

    send_mail from
    ActionMailer::Base.deliveries.size.must_equal 1
    assert_requested(get_token)
  end

  it "sends via smtp when previous token request failed" do
    Zendesk::Mailer::GmailSending.expects(:report_error)
    send_mail from
    send_mail from
    ActionMailer::Base.deliveries.size.must_equal 2
  end

  it "sends via smtp when account has gmail sending turned off" do
    account.settings.send_gmail_messages_via_gmail = false
    account.settings.save
    send_mail from
    ActionMailer::Base.deliveries.size.must_equal 1
  end

  describe "rate limiting" do
    let(:limit) { 1500 }

    it "does not send warning when rate limit is not hit" do
      Prop.throttle(:gmail_recipients, from, increment: limit - 1)
      assert_sending_via_api { send_mail from }
      Prop.count(:gmail_recipients, from).must_equal limit
    end

    describe "when limit is breached" do
      before { Prop.throttle(:gmail_recipients, from, increment: limit) }

      it "sends via smtp when recipients per day is exceeded" do
        send_mail from
        ActionMailer::Base.deliveries.size.must_equal 2 # actual email + alert email
      end

      it "does not send alert emails on subsequent limit breaches" do
        send_mail from
        send_mail from
        ActionMailer::Base.deliveries.size.must_equal 3 # 2 x actual email + alert email
      end
    end
  end
end
