require_relative "../../../support/test_helper"
require_relative "../../../support/mailer_test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::Mailer::TicketAddress do
  include MailerTestHelper
  fixtures :all

  let(:account)          { accounts(:minimum) }
  let(:ticket)           { tickets(:minimum_1) }
  let(:author)           { users(:minimum_agent) }
  let(:recipient_address) { recipient_addresses(:not_default) }
  let(:ticket_address) do
    Zendesk::Mailer::TicketAddress.new(account: account, ticket: ticket, author: author)
  end

  before do
    recipient_addresses(:default).update_column(:name, "Default name")
    author.stubs(agent_display_name: "Alias McGee") if author
  end

  describe "#from" do
    it "is author, name and email" do
      assert_equal "\"Agent Minimum (Default name)\" <support@zdtestlocalhost.com>", ticket_address.from
    end

    it "is author and email when name is blank" do
      recipient_addresses(:default).update_column(:name, "")
      assert_equal "\"Agent Minimum\" <support@zdtestlocalhost.com>", ticket_address.from
    end

    describe "without author" do
      let(:author) { nil }

      it "is name and email" do
        assert_equal "\"Default name\" <support@zdtestlocalhost.com>", ticket_address.from
      end

      it "is email when name is blank" do
        recipient_addresses(:default).update_column(:name, "")
        assert_equal "<support@zdtestlocalhost.com>", ticket_address.from
      end
    end

    describe "when account has_agent_display_names?" do
      before do
        Account.any_instance.stubs(:has_agent_display_names?).returns(true)
      end

      it "uses alias for end-users" do
        assert_equal "\"Alias McGee (Default name)\" <support@zdtestlocalhost.com>", ticket_address.from(for_agent: false)
      end

      it "uses real name for agents" do
        assert_equal "\"Agent Minimum (Default name)\" <support@zdtestlocalhost.com>", ticket_address.from(for_agent: true)
      end

      it "does not show blank names" do
        recipient_addresses(:default).update_column(:name, "")
        assert_equal "\"Agent Minimum\" <support@zdtestlocalhost.com>", ticket_address.from(for_agent: true)
      end

      describe "without author" do
        let(:author) { nil }

        it "does not show author without author" do
          assert_equal "\"Default name\" <support@zdtestlocalhost.com>", ticket_address.from(for_agent: true)
        end
      end
    end

    it "returns matched recipient address" do
      email = recipient_addresses(:not_default).email
      ticket.original_recipient_address = email
      assert_equal "\"Agent Minimum (Not Default)\" <#{email}>", ticket_address.from
    end

    it "uses default for unmatched recipient address" do
      ticket.original_recipient_address = "something@else.com"
      assert_equal "\"Agent Minimum (Default name)\" <#{account.reply_address}>", ticket_address.from
    end

    it "uses default without recipient address" do
      refute ticket.original_recipient_address
      assert_equal "\"Agent Minimum (Default name)\" <#{account.reply_address}>", ticket_address.from
    end

    it "uses default with unverified address" do
      email = recipient_addresses(:not_default).email
      recipient_addresses(:not_default).update_column(:forwarding_verified_at, nil)
      ticket.original_recipient_address = email
      assert_equal "\"Agent Minimum (Default name)\" <support@zdtestlocalhost.com>", ticket_address.from
    end
  end

  describe "#reply_to" do
    it "returns account name and default address" do
      assert_equal "\"Default name\" <#{account.reply_address}>", ticket_address.reply_to(false)
    end

    describe "with a non-default brand" do
      let(:brand) { FactoryBot.create(:brand) }
      before { ticket.brand = brand }

      it "is brand reply address" do
        assert_equal "\"#{brand.name}\" <#{brand.reply_address.sub('@', "+id#{ticket.nice_id}@")}>", ticket_address.reply_to(false)
      end
    end

    describe "with recipient_address" do
      let(:recipient_address) { recipient_addresses(:not_default) }
      before { ticket.original_recipient_address = recipient_address.email }

      it "is recipient address" do
        assert_equal "\"Not Default\" <not_default@example.net>", ticket_address.reply_to(false)
      end

      describe "when it is unverified" do
        before do
          recipient_address.update_column(:forwarding_verified_at, nil)
        end

        it "is default address" do
          assert_equal "\"Default name\" <#{account.reply_address}>", ticket_address.reply_to(false)
        end

        it "is default address if default address is unverified (Retry on default failed)" do
          recipient_addresses(:default).update_column(:forwarding_verified_at, nil)
          assert_equal "\"Default name\" <#{account.reply_address}>", ticket_address.reply_to(false)
        end
      end
    end

    describe "with domain that allows tagging" do
      let(:domain) { account.default_host }
      before { recipient_addresses(:default).update_column(:email, "hello@#{domain}") }

      it "adds encoded id for agents" do
        assert_equal "\"Default name\" <hello+id#{ticket.encoded_id}@#{domain}>", ticket_address.reply_to(true)
      end

      it "adds id for end-users" do
        assert_equal "\"Default name\" <hello+id#{ticket.nice_id}@#{domain}>", ticket_address.reply_to(false)
      end
    end
  end

  describe "#recipient_address" do
    before { ticket.original_recipient_address = recipient_address.email }

    it "works" do
      assert_equal recipient_address, ticket_address.send(:recipient_address)
    end

    it "caches nil" do
      ticket.expects(:recipient_address).once.returns nil
      assert_nil ticket_address.send(:recipient_address)
      assert_nil ticket_address.send(:recipient_address)
    end

    it "caches values" do
      ticket.expects(:recipient_address).once.returns recipient_address
      assert_equal recipient_address, ticket_address.send(:recipient_address)
      assert_equal recipient_address, ticket_address.send(:recipient_address)
    end

    it "does not return a address that cannot send emails" do
      recipient_address.update_column(:forwarding_verified_at, nil)
      assert_nil ticket_address.send(:recipient_address)
    end
  end
end
