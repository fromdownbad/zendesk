require_relative "../../../support/test_helper"
require_relative "../../../support/mailer_test_helper"

SingleCov.covered! uncovered: 0

describe Zendesk::Mailer::AccountAddress do
  include MailerTestHelper

  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:author) { users(:minimum_agent) }

  describe "#noreply" do
    let(:address) { Zendesk::Mailer::AccountAddress.new(account: account) }

    it "is the backup recipient address with changed" do
      recipient_addresses(:default).update_column(:name, "default name")
      assert_equal '"default name" <noreply@minimum.zendesk-test.com>', address.noreply
    end

    it "consists of the account's default recipient address name and subdomain" do
      account.default_recipient_address.update_column(:name, "Rec name")
      account.default_recipient_address.update_column(:email, "nono@dontusethis.com")
      assert_equal '"Rec name" <noreply@minimum.zendesk-test.com>', address.noreply
    end
  end

  describe "#from" do
    it "uses the given brand if there's one in the options" do
      brand = FactoryBot.create(:brand, account_id: account.id, name: "Wombats", subdomain: "wombats", active: true)
      address = Zendesk::Mailer::AccountAddress.new(account: account, brand: brand)

      assert_equal '"Wombats" <support@wombats.zendesk-test.com>', address.from
    end

    it "with no brand returns noreply email address" do
      address = Zendesk::Mailer::AccountAddress.new(account: account, brand: nil)
      account.default_brand = nil

      assert_equal '"Minimum account" <noreply@minimum.zendesk-test.com>', address.from
    end

    it "escapes the name part" do
      address = Zendesk::Mailer::AccountAddress.new(account: account)

      recipient_addresses(:default).update_column(:name, 'With " Double Quote')
      assert_equal '"With  Double Quote" <support@zdtestlocalhost.com>', address.from
    end

    describe "when personalization is available" do
      let(:address) { Zendesk::Mailer::AccountAddress.new(account: account, author: author) }

      before do
        address.stubs(:personalize?).returns(true)
        assert_equal '"Agent Minimum" <support@zdtestlocalhost.com>', address.from
      end

      it "uses the agent's display name for the name part if it is set" do
        author.account.stubs(:has_agent_display_names?).returns(true)
        author.agent_display_name = 'Secret Agent'

        assert_equal '"Secret Agent" <support@zdtestlocalhost.com>', address.from
      end

      describe "and the recipient_addresses feature is enabled" do
        it "uses the agent's name followed with the default recipients address name followed in parantheses" do
          account.default_recipient_address.update_column(:name, "Rec name")
          assert_equal '"Agent Minimum (Rec name)" <support@zdtestlocalhost.com>', address.from
        end

        it "uses the agent's name without default recipients address name when blank" do
          account.default_recipient_address.update_column(:name, "")
          assert_equal '"Agent Minimum" <support@zdtestlocalhost.com>', address.from
        end

        it "uses the agent's display name for the name part if it is set" do
          author.account.stubs(:has_agent_display_names?).returns(true)
          author.agent_display_name = 'Secret Agent'

          assert_equal '"Secret Agent" <support@zdtestlocalhost.com>', address.from
        end

        it "stills use default when default is not verified (user clicked retry and it failed)" do
          recipient_addresses(:default).update_column(:forwarding_verified_at, nil)
          assert_equal "\"Agent Minimum\" <support@zdtestlocalhost.com>", address.from
        end
      end
    end
  end

  describe "#personalize?" do
    describe "with an author" do
      let(:address) { Zendesk::Mailer::AccountAddress.new(account: account, author: author) }

      it "is true when personalized replies are enabled" do
        account.is_personalized_reply_enabled = true
        assert(address.personalize?)

        account.is_personalized_reply_enabled = false
        assert_equal false, address.personalize?
      end

      it "is false when the author is the system user (e.g. via automations)" do
        account.is_personalized_reply_enabled = true
        assert(address.personalize?)

        author.id = -1
        assert author.is_system_user?
        assert_equal false, address.personalize?
      end
    end

    describe "without an author" do
      let(:address) { Zendesk::Mailer::AccountAddress.new(account: account, author: nil) }

      it "is false when personalized replies are enabled" do
        account.is_personalized_reply_enabled = true
        assert_equal false, address.personalize?
      end
    end
  end
end
