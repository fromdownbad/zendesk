require_relative "../../../support/test_helper"
require_relative "../../../support/mail_test_helper"

SingleCov.covered! uncovered: 2

describe Zendesk::Mailer::RawEmailAccess do
  include MailTestHelper

  fixtures :all

  class TestableRawEmailAccess < Zendesk::Mailer::RawEmailAccess
    attr_writer :json, :message, :html

    def blank?
      false
    end
  end

  def assert_blank(identifier)
    raw_email = Zendesk::Mailer::RawEmailAccess.new(identifier)
    assert raw_email.blank?
    refute raw_email.present?
    assert_nil raw_email.eml
    assert_nil raw_email.full_message_body_json
    assert_nil raw_email.json
  end

  def assert_persisted(remote_file)
    assert remote_file.present?
    assert remote_file.content.present?
    assert remote_file.stored?
  end

  it "is blank for an identifier with no raw email info" do
    assert_blank('')
    assert_blank(nil)
  end

  let(:email_hash) do
    {
      "headers" => {
        "subject" => [
          "This is about remote_files",
        ],
        "x-spam-score" => [
          "1.23 spam points",
        ],
        "x-other-header" => [
          "i am a header!",
          "i am another of the same header!"
        ]
      },
      "body" => "body json data from remote_files",
      "parts" => [
        {
          "headers" => { 'content-type' => ["text/plain"] },
          "body" => "part 1 json data from remote_files",
        },
        {
          "headers" => { 'content-type' => ["text/html"] },
          "body" => "<html><body>html json data from remote_files</body></html>",
        },
        {
          "headers" => { 'content-type' => ["image/png"] },
          "body" => "Attachment data remote_files",
        },
      ],
      "processing_state" => {
        "content" => {
          "html_full" => "<html><body>html json data from remote_files</body></html>",
          "html_quoted" => "<html><body>html json data from remote_files</body></html>",
          "html_reply" => "<html><body>html json data from remote_files</body></html>",
          "plain_full" => "part 1 json data from remote_files",
          "plain_quoted" => "part 1 json data from remote_files",
          "plain_reply" => "part 1 json data from remote_files",
        }
      }
    }
  end

  let(:full_message_body_hash) do
    {
      processing_state: {
        content: {
          html_quoted: "<html><body><div>html body json data from external storage remote_files</div></body></html>",
          plain_quoted: "plain body json data from external storage remote_files",
        }
      }
    }
  end

  let(:redacted_full_message_body_hash) do
    {
      processing_state: {
        content: {
          html_quoted: "<html><body><div>html body json data from external storage ▇▇▇▇▇▇▇▇▇▇▇▇</div></body></html>",
          plain_quoted: "plain body json data from external storage ▇▇▇▇▇▇▇▇▇▇▇▇",
        }
      }
    }
  end

  let(:email_hash_without_x_headers) do
    duped_email_hash = email_hash.deep_dup
    header_keys = duped_email_hash["headers"].keys
    header_keys.each do |header_key|
      if header_key.match?(Zendesk::Mailer::RawEmailAccess::X_HEADER_PREFIX)
        duped_email_hash["headers"].delete(header_key)
      end
    end

    duped_email_hash
  end

  let(:redacted_email_hash) do
    {
      "headers" => {
        "subject" => [
          "This is about ▇▇▇▇▇▇▇▇▇▇▇▇"
        ],
        "x-spam-score" => [
          "1.23 spam points" # No redaction here
        ],
        "x-other-header" => [
          "i am a header!", # No redaction here
          "i am another of the same header!" # No redaction here
        ]
      },
      "body" => "body json data from ▇▇▇▇▇▇▇▇▇▇▇▇",
      "parts" => [
        {
          "headers" => { 'content-type' => ["text/plain"] },
          "body" => "part 1 json data from ▇▇▇▇▇▇▇▇▇▇▇▇",
        },
        {
          "headers" => { 'content-type' => ["text/html"] },
          "body" => "<html><body>html json data from ▇▇▇▇▇▇▇▇▇▇▇▇</body></html>",
        },
        {
          "headers" => { 'content-type' => ["image/png"] },
          "body" => "Attachment data remote_files", # No redaction here
        },
      ],
      "processing_state" => {
        "content" => {
          "html_full" => "<html><body>html json data from ▇▇▇▇▇▇▇▇▇▇▇▇</body></html>",
          "html_quoted" => "<html><body>html json data from ▇▇▇▇▇▇▇▇▇▇▇▇</body></html>",
          "html_reply" => "<html><body>html json data from ▇▇▇▇▇▇▇▇▇▇▇▇</body></html>",
          "plain_full" => "part 1 json data from ▇▇▇▇▇▇▇▇▇▇▇▇",
          "plain_quoted" => "part 1 json data from ▇▇▇▇▇▇▇▇▇▇▇▇",
          "plain_reply" => "part 1 json data from ▇▇▇▇▇▇▇▇▇▇▇▇",
        }
      }
    }
  end

  describe "an identifier with raw email on S3" do
    let(:attachments) { nil }
    let(:eml_identifier) { "1/foo.eml" }
    let(:json_identifier) { "1/foo.json" }
    let(:eml_content) do
      "To: someone@example.com\nFrom: someone.else@example.com\nX-Spam-Score: 1.23\nX-Other-Header: i am a header!\nX-Other-Header:i am another of the same header!\n\nHello!"
    end
    let(:eml_content_without_x_headers) do
      lines = eml_content.split("\n")
      lines_without_x_headers = lines.reject do |line|
        line.match?(Zendesk::Mailer::RawEmailAccess::X_HEADER_PREFIX)
      end
      lines_without_x_headers.join("\n")
    end

    let(:raw_email) do
      RemoteFiles::File.new(
        eml_identifier,
        content_type:  "message/rfc822",
        content:       eml_content,
        configuration: Zendesk::Mail.raw_remote_files.name
      ).store_once!

      RemoteFiles::File.new(
        json_identifier,
        content_type:  "application/json",
        content:       email_hash.to_json,
        configuration: Zendesk::Mail.raw_remote_files.name
      ).store_once!

      RemoteFiles::File.new(
        json_identifier.gsub(/\.json$/, '.eml_body_content.json'),
        content_type:  "application/json",
        content:       full_message_body_hash.to_json,
        configuration: Zendesk::Mail.raw_remote_files.name
      ).store_once!

      Zendesk::Mailer::RawEmailAccess.new(
        eml_identifier,
        attachments: attachments,
        email_fix_no_delimiter_redaction: accounts(:minimum).has_email_fix_no_delimiter_redaction?,
        tiff_prevent_inline: accounts(:minimum).has_tiff_prevent_inline?
      )
    end

    let(:statsd_client) { raw_email.send(:statsd_client) }

    it "is not blank" do
      refute raw_email.blank?
      assert raw_email.present?
    end

    describe "#eml" do
      it "gets eml data from remote_files" do
        assert_equal eml_content, raw_email.eml
      end
    end

    describe "#eml_without_x_headers" do
      before { Timecop.freeze(DateTime.current) }

      it "gets eml data from remote_files with any X-* headers redacted" do
        # Compare using the mail gem's Mail::Message#== equality check, because
        # the mail gem, which #eml_without_x_headers uses, organizes the raw
        # eml's headers in its own particular order, so comparing eml strings
        # is likely to lead to false negatives and flaky tests
        assert_equal Mail.new(eml_content_without_x_headers), Mail.new(raw_email.eml_without_x_headers)
        refute_equal Mail.new(eml_content), Mail.new(raw_email.eml_without_x_headers)
      end

      describe "when it encounters an exception" do
        before { Mail.stubs(:read_from_string).raises(StandardError) }

        it "rescues the exception and returns nil" do
          assert_nil raw_email.eml_without_x_headers
        end

        it "increments a metric using StatsD" do
          statsd_client.expects(:increment).with('eml_without_x_headers_parsing_failure', tags: ["exception:#{StandardError.to_s.underscore}"]).once

          raw_email.eml_without_x_headers
        end
      end
    end

    describe "#json" do
      it "gets json data from remote_files" do
        assert_equal email_hash.to_json, raw_email.json
      end
    end

    describe "#full_message_body_json" do
      it "gets json data from remote_files" do
        assert_equal full_message_body_hash.to_json, raw_email.full_message_body_json
      end
    end

    describe '#resolve_json_cid_images!' do
      let(:attachments) { [create_attachment("file_name" => "image.png", "content_id" => "ii_k5po58cl1", "inline" => true)] }
      let(:email_hash) { JSON.parse(File.read(Rails.root.join('test/files/inbound_mailer/attachments_and_processing_state.json'))) }
      let(:mail) { FixtureHelper::Mail.read("attachments_and_processing_state.json") }

      describe 'when content id map is absent' do
        it 'does not parse the email json' do
          Zendesk::InboundMail::Support.expects(:parse_json).never
          raw_email.resolve_json_cid_images!(nil)
        end
      end

      describe 'when content id map is present' do
        describe 'and a logger is provided' do
          it 'logs with the provided logger' do
            mock_logger = mock('logger')
            Zendesk::InboundMail::Support.expects(:parse_json).twice.returns(email_hash)
            mock_logger.stubs(:info)
            Rails.logger.expects(:info).with(regexp_matches(/About to replace .json/)).never
            mock_logger.expects(:info).with(regexp_matches(/About to replace .json/)).once
            Rails.logger.expects(:info).with(regexp_matches(/Completed cid resolution for/)).never
            mock_logger.expects(:info).with(regexp_matches(/Completed cid resolution for/)).once
            account = accounts(:minimum)
            content_id_map = create_content_id_map(account, attachments)
            raw_email.resolve_json_cid_images!(content_id_map, mock_logger)
          end
        end

        it 'resolves the image sources' do
          Zendesk::StatsD::Client.any_instance.expects(:time).with('resolve_json_cid_images.time').once.yields
          Zendesk::InboundMail::Support.expects(:parse_json).twice.returns(email_hash)
          Rails.logger.stubs(:info)
          Rails.logger.expects(:info).with(regexp_matches(/About to replace .json/)).once
          Rails.logger.expects(:info).with(regexp_matches(/Completed cid resolution for/)).once
          account = accounts(:minimum)
          content_id_map = create_content_id_map(account, attachments)
          raw_email.resolve_json_cid_images!(content_id_map)
        end
      end
    end

    describe '#inline?' do
      let(:attachment) { create_attachment("file_name" => "image.png", "content_id" => "ii_k5po58cl1", "inline" => true) }

      describe_with_arturo_enabled :tiff_prevent_inline do
        describe 'when the content type is not TIFF' do
          before { attachment.stubs(:content_type).returns(Mime[:jpeg]) }

          it 'returns true' do
            assert raw_email.inline?(attachment)
          end
        end

        describe 'when the content type is TIFF' do
          before { attachment.stubs(:content_type).returns(Mime[:tiff]) }

          it 'returns false' do
            refute raw_email.inline?(attachment)
          end
        end
      end

      describe_with_arturo_disabled :tiff_prevent_inline do
        it 'returns true' do
          assert raw_email.inline?(attachment)
        end
      end
    end

    describe '#resolve_cid_images' do
      describe 'when there is a SystemStackError' do
        before { Nokogiri::HTML5.stubs(:fragment).raises(SystemStackError).once }

        it 'is rescued and cid in images are not resolved' do
          html = "<img src=\"cid:ii_k5po58cl1\" alt=\"image.png\" width=\"1\" height=\"1\">"
          actual = raw_email.resolve_cid_images(html, nil)
          assert_equal [[], html], actual
          assert_includes actual.last, 'cid:ii_k5po58cl1'
        end
      end
    end

    describe "#json_without_x_headers" do
      it "gets json data from remote_files with any X-* headers redacted" do
        assert_equal email_hash_without_x_headers.to_json, raw_email.json_without_x_headers
        refute_equal email_hash.to_json, raw_email.json_without_x_headers
      end

      describe "when it encounters an exception" do
        before { raw_email.send(:parsed_json).stubs(:deep_dup).raises(StandardError) }

        it "rescues the exception and returns nil" do
          assert_nil raw_email.json_without_x_headers
        end

        it "increments a metric using StatsD" do
          statsd_client.expects(:increment).with('json_without_x_headers_parsing_failure', tags: ["exception:#{StandardError.to_s.underscore}"]).once

          raw_email.json_without_x_headers
        end
      end
    end

    it "is persisted to a .eml file" do
      assert_persisted(raw_email.send(:remote_eml_file))
    end

    it "is persisted to a json file" do
      assert_persisted(raw_email.send(:remote_json_file))
    end

    describe "#delete_eml_file!" do
      describe "if eml_identifier is present" do
        before do
          Rails.logger.stubs(:info)
          Rails.logger.expects(:info).with(regexp_matches(/About to delete .eml file/)).once
          @original_eml_file = raw_email.send(:remote_eml_file)
          raw_email.delete_eml_file!
        end

        it "can not be retrieved" do
          new_remote_file = Zendesk::Mail.raw_remote_files.file_from_url(@original_eml_file.url)
          assert_raises RemoteFiles::NotFoundError do
            new_remote_file.retrieve!
          end
        end

        it "clears the eml content" do
          assert_nil(raw_email.eml)
        end

        it "clears the eml identifier" do
          assert_nil(raw_email.send(:eml_identifier))
        end

        it "clears the eml filename" do
          assert_nil(raw_email.eml_file_name)
        end

        it "can be deleted again without consequence" do
          raw_email.delete_eml_file!
          assert_nil(raw_email.eml)
        end
      end

      describe "if eml_identifier is blank" do
        it "does not attempt to delete the remote eml file" do
          Rails.logger.stubs(:info)
          Rails.logger.expects(:info).with(regexp_matches(/About to delete .eml file/)).never
          @original_eml_file = raw_email.send(:remote_eml_file)
          raw_email.delete_eml_file!
        end
      end
    end

    describe "#delete_json_files!" do
      describe "if json_identifier is present" do
        before do
          Rails.logger.stubs(:info)
          Rails.logger.expects(:info).with(regexp_matches(/About to delete .json file/)).once
          Rails.logger.expects(:info).with(regexp_matches(/About to delete .eml_body_content.json file/)).once
          @json_file = raw_email.send(:remote_json_file)
          @full_message_json_file = raw_email.send(:remote_full_message_json_file)
          raw_email.delete_json_files!
        end

        it "json can not be retrieved" do
          new_remote_file = Zendesk::Mail.raw_remote_files.file_from_url(@json_file.url)
          assert_raises RemoteFiles::NotFoundError do
            new_remote_file.retrieve!
          end
        end

        it "full message json can not be retrieved" do
          new_remote_file = Zendesk::Mail.raw_remote_files.file_from_url(@full_message_json_file.url)
          assert_raises RemoteFiles::NotFoundError do
            new_remote_file.retrieve!
          end
        end

        it "clears the json content" do
          assert_nil(raw_email.json)
        end

        it "clears the full message json content" do
          assert_nil(raw_email.full_message_body_json)
        end

        it "clears the json identifier" do
          assert_nil(raw_email.send(:json_identifier))
        end

        it "clears the json filename" do
          assert_nil(raw_email.json_file_name)
        end

        it "can be deleted again without consequence" do
          raw_email.delete_json_files!
          assert_nil(raw_email.json)
          assert_nil(raw_email.full_message_body_json)
        end
      end

      describe "if json_identifier is blank" do
        it "does not attempt to delete the remote json file" do
          Rails.logger.stubs(:info)
          Rails.logger.expects(:info).with(regexp_matches(/About to delete .json file/)).never
          @json_file = raw_email.send(:remote_json_file)
          raw_email.delete_json_files!
        end
      end
    end

    describe "#redact_json_files!" do
      describe "successful redaction" do
        before { raw_email.redact_json_files!("remote_files") }

        it "phrase is redacted in .json" do
          assert_equal(redacted_email_hash.to_json, raw_email.json)
        end

        it "phrase is redacted .eml_body_content.json" do
          assert_equal(redacted_full_message_body_hash.to_json, raw_email.full_message_body_json)
        end

        it "forces reload of the associated message and content" do
          # These can't be tested by inspecting whether they return redacted content because
          # methods pull from the associated `message` object (which is not touched by these tests).
          # However, by checking they have been set to null, we are assuming the redacted `message`
          # will be re-loaded next time they care used (see RawEmailAccess::message, RawEmailAccess::text &
          # RawEmailAccess::html)
          assert_nil(raw_email.instance_variable_get(:@message))
          assert_nil(raw_email.instance_variable_get(:@html))
          assert_nil(raw_email.instance_variable_get(:@text))
          assert_nil(raw_email.instance_variable_get(:@sanitized_html))
          assert_nil(raw_email.instance_variable_get(:@parsed_json))
        end

        describe "the new .json" do
          let(:new_json_file) { raw_email.send(:remote_json_file) }

          it { assert(new_json_file.stored?) }

          it "is persisted only in a single store" do
            assert(new_json_file.stored_in.size <= 1)
          end

          it "has application/json content type" do
            assert_equal "application/json", new_json_file.content_type
          end
        end

        it "persists the new .eml_body_content.json" do
          new_json_file = raw_email.send(:remote_full_message_json_file)
          assert(new_json_file.stored?)
        end

        it "can be redacted again without consequence" do
          raw_email.redact_json_files!("remote_files")
          assert_equal(redacted_email_hash.to_json, raw_email.json)
          assert_equal(redacted_full_message_body_hash.to_json, raw_email.full_message_body_json)
        end

        describe "when there are parts of the email missing" do
          let(:sparse_email_hash) do
            {
              headers: {
                subject: [],
              },
              parts: [
                {
                  headers: { 'content-type' => ["text/plain"] },
                },
                {
                  headers: { 'content-type' => ["text/html"] },
                  body: "<html><body>html json data from remote_files</body></html>",
                },
              ],
            }
          end

          let(:redacted_sparse_email_hash) do
            {
              headers: {
                subject: []
              },
              parts: [
                {
                  headers: { 'content-type' => ["text/plain"] },
                },
                {
                  headers: { 'content-type' => ["text/html"] },
                  body: "<html><body>html json data from ▇▇▇▇▇▇▇▇▇▇▇▇</body></html>",
                },
              ],
            }
          end

          let(:sparse_raw_email) do
            RemoteFiles::File.new(
              eml_identifier,
              content_type:  "message/rfc822",
              content:       "sparse",
              configuration: Zendesk::Mail.raw_remote_files.name
            ).store_once!

            RemoteFiles::File.new(
              json_identifier,
              content_type:  "application/json",
              content:       sparse_email_hash.to_json,
              configuration: Zendesk::Mail.raw_remote_files.name
            ).store_once!

            Zendesk::Mailer::RawEmailAccess.new(eml_identifier)
          end

          it "doesn't fail" do
            sparse_raw_email.redact_json_files!("remote_files")
            assert_equal(redacted_sparse_email_hash.to_json, sparse_raw_email.json)
          end
        end
      end

      describe "when a parsing error occurs" do
        it "logs a warning" do
          Zendesk::InboundMail::Support.expects(:parse_json).with(email_hash.to_json).returns(
            Yajl::Parser.parse(email_hash.to_json, check_utf8: false).with_indifferent_access
          ).once
          Zendesk::InboundMail::Support.expects(:parse_json).with(full_message_body_hash.to_json).raises(JSON::ParserError)
          Rails.logger.expects(:warn).with(regexp_matches(/^Error parsing full email message body json: JSON::ParserError/)).once
          raw_email.redact_json_files!("remote_files")
        end
      end
    end

    describe "#redact_attachment_from_json!" do
      def assert_clears_all_json_related_instance_vars(filename)
        raw_email.full_message_body_json
        raw_email.html
        raw_email.json
        raw_email.json_without_x_headers
        raw_email.message
        raw_email.sanitized_html
        raw_email.send(:parsed_json)
        raw_email.send(:remote_json_file)
        raw_email.text

        raw_email.redact_attachment_from_json!(filename)

        assert_nil raw_email.instance_variable_get(:@full_message_body_json)
        assert_nil raw_email.instance_variable_get(:@html)
        assert_nil raw_email.instance_variable_get(:@json)
        assert_nil raw_email.instance_variable_get(:@json_without_x_headers)
        assert_nil raw_email.instance_variable_get(:@message)
        assert_nil raw_email.instance_variable_get(:@parsed_json)
        assert_nil raw_email.instance_variable_get(:@remote_json_file)
        assert_nil raw_email.instance_variable_get(:@sanitized_html)
        assert_nil raw_email.instance_variable_get(:@text)
      end

      let(:filename_argument_filename) { "attachment.txt" }
      let(:other_filename) { "other_attachment.txt" }

      let(:attachment_part_with_filename_argument_filename) do
        {
          "headers" => {
            "content-type" => ["text/plain"],
            "file-name" => [filename_argument_filename]
          },
          "body" => "I am an attachment!"
        }
      end

      let(:other_attachment_part_with_filename_argument_filename) do
        {
          "headers" => {
            "content-type" => ["text/plain"],
            "file-name" => [filename_argument_filename]
          },
          "body" => "I am a totally different attachment with the same filename!"
        }
      end

      let(:other_attachment_with_different_filename) do
        {
          "headers" => {
            "content-type" => ["image/png"],
            "file-name" => [other_filename]
          },
          "body" => "I am a different attachment with a totally different filename!"
        }
      end

      let(:email_hash_with_no_attachments) do
        {
          "headers" => {
            "subject" => [
              "This is about remote_files"
            ],
          },
          "body" => "I am a body!",
          "parts" => [
            {
              "headers" => { "content-type" => ["text/plain"] },
              "body" => "I am a body!"
            },
            {
              "headers" => { "content-type" => ["text/html"] },
              "body" => "<html><body>I am a body!</body></html>"
            }
          ]
        }
      end

      describe "when there are no attachments with the filename passed in as an argument" do
        let(:email_hash) { email_hash_with_no_attachments }

        it "does not redact anything from the mail JSON" do
          raw_email.redact_attachment_from_json!(filename_argument_filename)

          assert_equal email_hash_with_no_attachments.to_json, raw_email.json
        end
      end

      describe "when there is one attachment with the filename passed in as an argument" do
        let(:email_hash) do
          email_hash = email_hash_with_no_attachments.deep_dup
          email_hash["parts"] << attachment_part_with_filename_argument_filename
          email_hash
        end

        it "redacts the attachment with that filename from the mail JSON" do
          raw_email.redact_attachment_from_json!(filename_argument_filename)

          assert_equal email_hash_with_no_attachments.to_json, raw_email.json
        end

        it { assert_clears_all_json_related_instance_vars(filename_argument_filename) }
      end

      describe "when there are multiple attachments with the filename passed in as an argument" do
        let(:email_hash) do
          attachment_parts = [
            attachment_part_with_filename_argument_filename,
            other_attachment_part_with_filename_argument_filename
          ]

          email_hash = email_hash_with_no_attachments.deep_dup
          attachment_parts.each { |attachment_part| email_hash["parts"] << attachment_part }
          email_hash
        end

        it "redacts all attachments with that filename from the mail JSON" do
          raw_email.redact_attachment_from_json!(filename_argument_filename)

          assert_equal email_hash_with_no_attachments.to_json, raw_email.json
        end

        it { assert_clears_all_json_related_instance_vars(filename_argument_filename) }
      end

      describe "when there are attachments with filenames other than the filename passed in as an argument" do
        let(:email_hash) do
          attachment_parts = [
            attachment_part_with_filename_argument_filename,
            other_attachment_part_with_filename_argument_filename,
            other_attachment_with_different_filename
          ]

          email_hash = email_hash_with_no_attachments.deep_dup
          attachment_parts.each { |attachment_part| email_hash["parts"] << attachment_part }
          email_hash
        end

        let(:email_hash_with_other_attachment_with_different_filename) do
          email_hash_with_other_attachment = email_hash_with_no_attachments.deep_dup
          email_hash_with_other_attachment["parts"] << other_attachment_with_different_filename
          email_hash_with_other_attachment
        end

        it "redacts all attachments with that filename and does not redact the attachments with the other filenames from the mail JSON" do
          raw_email.redact_attachment_from_json!(filename_argument_filename)

          assert_equal email_hash_with_other_attachment_with_different_filename.to_json, raw_email.json
        end

        it { assert_clears_all_json_related_instance_vars(filename_argument_filename) }
      end
    end
  end

  describe "an identifier with raw email stored on fallback S3" do
    let(:filename) { "1/foo" }
    let(:identifier) { "#{filename}.eml" }
    let(:fallback_store) { Zendesk::Mail.raw_remote_files.stores.select(&:read_only?).first }
    let(:raw_email) { Zendesk::Mailer::RawEmailAccess.new(identifier) }

    it "is not blank" do
      refute raw_email.blank?
      assert raw_email.present?
    end

    it "gets eml data from remote_files" do
      assert_equal fallback_store.retrieve!("#{filename}.eml").content, raw_email.eml
    end

    it "gets json data from remote_files" do
      assert_equal fallback_store.retrieve!("#{filename}.json").content, raw_email.json
    end
  end

  describe "an identifier with JSON email and no raw email stored on S3" do
    let(:json_identifier) do
      "1/foo.json"
    end

    let(:json_email) do
      RemoteFiles::File.new(
        json_identifier,
        content_type:  "application/json",
        content:       email_hash.to_json,
        configuration: Zendesk::Mail.raw_remote_files.name
      ).store_once!

      Zendesk::Mailer::RawEmailAccess.new(nil, json_identifier: json_identifier)
    end

    it "is not blank" do
      refute json_email.blank?
      assert json_email.present?
    end

    it "does not get eml data from remote_files" do
      assert_nil json_email.eml
    end

    it "gets json data from remote_files" do
      assert_equal email_hash.to_json, json_email.json
    end

    it "can be redacted without consequence" do
      json_email.redact_json_files!("remote_files")
    end
  end

  describe "#message" do
    before { @raw_email = TestableRawEmailAccess.new('xxx') }

    it "constructs a message from json" do
      @raw_email.json = '{ "headers": { "subject": [ "foo" ] } }'
      assert_equal 'foo', @raw_email.message.subject
    end
  end

  describe "#html" do
    before { @raw_email = TestableRawEmailAccess.new('xxx') }

    it "returns html part" do
      @raw_email.json = '{
        "headers": {},
        "parts": [
          { "headers": { "content-type": [ "text/html" ] }, "body": "html" }
        ]
      }'
      @raw_email.html.must_equal "html"
    end

    it "returns nil with html attachments" do
      @raw_email.json = '{
        "headers": {},
        "parts": [
          { "headers": { "content-type": [ "text/html" ], "file-name": [ "attachment.html" ] }, "body": "html" }
        ]
      }'
      refute @raw_email.html
    end

    it "returns html when the email is a pure html email" do
      @raw_email.json = '{
        "headers": { "content-type": ["text/html"] },
        "body": "html"
      }'
      @raw_email.html.must_equal "html"
    end

    it "does not fail when sanitizing invalid bytes" do
      invalid_bytes = "<html>\x8D\U+FFDA\U+FFCC\xE3\x81A\x82\xB2\x95\U+FFD4M\x82\xAD\x82\xBE\x82\xB3\x82\xA2\x81</html>"
      refute invalid_bytes.valid_encoding?
      @raw_email.html = invalid_bytes
      assert @raw_email.html

      sanitized_html = @raw_email.sanitized_html
      assert sanitized_html
    end
  end

  describe "#text" do
    before { @raw_email = TestableRawEmailAccess.new('xxx') }

    it "returns the text" do
      @raw_email.json = '{
        "headers": {},
        "parts": [
          { "headers": { "content-type": [ "text/html" ] }, "body": "html" },
          { "headers": { "content-type": [ "text/plain" ] }, "body": "text" }
        ]
      }'
      @raw_email.text.must_equal "text"
    end

    it "returns nothing where there is no text part" do
      @raw_email.json = '{
        "headers": {},
        "parts": [
          { "headers": { "content-type": [ "text/html" ] }, "body": "html" },
          { "headers": { "content-type": [ "text/oops" ] }, "body": "text" }
        ]
      }'
      @raw_email.text.must_be_nil
    end

    it "returns text when the email is a pure text email" do
      @raw_email.json = '{
        "headers": { "content-type": ["text/plain"] },
        "body": "text"
      }'
      @raw_email.text.must_equal "text"
    end
  end

  describe "#sanitized_html" do
    before { @raw_email = TestableRawEmailAccess.new('xxx') }

    it "keeps a href" do
      @raw_email.html = 'foo <a href="http://foo.com">bar</a> baz'
      assert_equal 'foo <a href="http://foo.com">bar</a> baz', @raw_email.sanitized_html
    end

    it "shows links with line breaks" do
      @raw_email.html = "foo <a href=\"\nhttps://foo.com/booking.html\n\">here</a> <a href=\"http://other.com\">x</a> baz"
      assert_equal @raw_email.html, @raw_email.sanitized_html
    end

    it "strips style tags" do
      # it would be better if we could inline the styles, but for now this will do
      @raw_email.html = 'foo <style>bar</style> baz'
      assert_equal 'foo  baz', @raw_email.sanitized_html
    end

    it "strips script tags" do
      @raw_email.html = 'foo <script>bar</script> baz'
      assert_equal 'foo  baz', @raw_email.sanitized_html
    end

    it "strips javascript hrefs" do
      @raw_email.html = 'foo <a href="javascript: alert(true);">bar</a> baz'
      assert_equal "foo bar (javascript: alert(true);) baz", @raw_email.sanitized_html
    end

    it "strips onclick attributes" do
      @raw_email.html = 'foo <a onclick="alert(true);">bar</a> baz'
      assert_equal 'foo <a>bar</a> baz', @raw_email.sanitized_html
    end

    it "returns nil if we html is not a kinf of string" do
      @raw_email.html = Zendesk::Mail::Part.new
      refute @raw_email.sanitized_html
    end

    describe "multiple image attachments" do
      let(:attachment) { attachments(:attachment_entry) }

      before do
        attachment2 = attachment.dup
        attachment2.token = nil

        # fs attachment is a piece of shit which lets you
        # save without any data being saved.
        attachment2.set_temp_data "some data"

        # Digest::MD5.hexdigest("body2") => "d6ed8ba2adae5a938c5a3757bcccf4dd"
        attachment2.stubs(:md5).returns("76af63c5bd77b2a3a2cfcbd52645fa38")

        # So we can tell the difference between the two attachment links
        attachment.stubs(:token).returns("howdy")
        attachment2.stubs(:token).returns("doody")

        attachments = [attachment, attachment2]

        @attachment_email = TestableRawEmailAccess.new('xxx', attachments: attachments)

        # We have two attachments:
        #   cid: heygabba, body: "body1"
        #   cid: gabba,    body: "body2"
        @attachment_email.json = '{"parts":[{"headers":{"content-id":["heygabba"],"file-name":["a.png"]}, "body": "body1"},{"headers":{"content-id":["gabba"],"file-name":["a.png"]}, "body": "body2"}]}'
        @attachment_email.html = 'foo <img src="cid:heygabba" alt="sup"> bar foo <img src="cid:gabba" alt="sup"> bar'
      end

      describe "with the same file name" do
        it "uses an MD5 based method to identify which attachment is which" do
          # Both attachments have the same names
          Attachment.any_instance.stubs(:filename).returns("a.png")

          # Digest::MD5.hexdigest("body1") => "d6ed8ba2adae5a938c5a3757bcccf4dd"
          attachment.stubs(:md5).returns("d6ed8ba2adae5a938c5a3757bcccf4dd")

          assert_includes @attachment_email.sanitized_html, "/token/howdy/"
          assert_includes @attachment_email.sanitized_html, "/token/doody/"
        end

        it "calls generate_md5 if no md5 is set" do
          attachment.expects(:generate_md5).twice.returns("d6ed8ba2adae5a938c5a3757bcccf4dd")
          @attachment_email.sanitized_html
        end
      end

      describe "with different filenames" do
        it "does not run md5 on attachments" do
          attachment.expects(:filename).twice.returns("b.png")
          Digest::MD5.expects(:hexdigest).never
          @attachment_email.sanitized_html
        end
      end

      describe "when the src attribute is enclosed in single quotes" do
        before do
          attachment.stubs(:md5).returns("d6ed8ba2adae5a938c5a3757bcccf4dd")
          @attachment_email.html = 'foo <img src=\'cid:heygabba\' alt="sup"> bar foo <img src=\'cid:gabba\' alt="sup"> bar'
        end

        it "correctly replaces the cid with inline attachment url" do
          assert_no_match /cid/, @attachment_email.sanitized_html
          assert_includes @attachment_email.sanitized_html, "/token/howdy/"
          assert_includes @attachment_email.sanitized_html, "/token/doody/"
        end
      end
    end
  end
end
