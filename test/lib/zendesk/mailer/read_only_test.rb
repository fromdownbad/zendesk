require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Mailer::ReadOnly do
  fixtures :accounts

  class MockMailer
    include Zendesk::Mailer::ReadOnly
  end

  describe '#read_only' do
    subject { MockMailer.new }

    it "uses ActiveRecord's replica host" do
      ActiveRecord::Base.expects(:on_slave).yields(2)
      subject.read_only { 1 + 1 }
    end
  end
end
