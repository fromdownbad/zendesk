require_relative '../../support/test_helper'
require 'zendesk/radar_factory'
require 'radar_client_rb'

SingleCov.covered! uncovered: 3

describe RadarFactory do
  let(:account) { stub(subdomain: 'support') }

  it 'can instantiate a legacy radar client' do
    client = RadarFactory.create_radar_client(account)
    assert_equal ::Radar::Client, client.class
  end
end
