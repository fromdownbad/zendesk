require_relative "../../support/test_helper"

SingleCov.covered!

class LocaleTestClass
  prepend Zendesk::LocalePresentation

  def locale
    'es' # Spanish locale
  end
end

describe Zendesk::LocalePresentation do
  fixtures :translation_locales

  let(:subject)  { LocaleTestClass.new }
  let(:japanese) { translation_locales(:japanese) }
  let(:spanish)  { translation_locales(:spanish) }

  describe '.localized_name' do
    it 'shows locale name' do
      assert_equal 'español', subject.localized_name(display_in: spanish)
    end

    it 'shows locale name in different language' do
      assert_equal 'スペイン語', subject.localized_name(display_in: japanese) # Spanish
    end

    describe 'renames politically sensitive locales to be more neutral' do
      {
        'zh-cn' => 'chino (simplificado)',
        'zh-tw' => 'chino (tradicional)'
      }.each do |locale_key, expected_translation|
        it "will return #{expected_translation} when translating #{locale_key} in Spanish" do
          locale = TranslationLocale.find_by!(locale: locale_key)
          assert_equal expected_translation, locale.localized_name(display_in: spanish)
        end
      end
    end
  end

  describe '.display_name' do
    it 'returns the native name' do
      assert_equal 'español', subject.display_name(display_in: spanish)
    end

    it 'returns concated native and translated name' do
      assert_equal 'スペイン語 - español', subject.display_name(display_in: japanese)
    end
  end
end
