require_relative "../../../support/test_helper"

SingleCov.covered!

class AlertsControllerSupportTestController < ApplicationController
  include Zendesk::Alerts::ControllerSupport
end
describe "Zendesk::Alerts::ControllerSupport in Controller" do
  tests AlertsControllerSupportTestController
  use_test_routes

  describe "#current_user_alerts" do
    before do
      @current_user = stub(is_admin?: true)
      @controller.stubs(:current_user).returns(@current_user)
    end

    describe "when the request is authenticated via remote auth" do
      before { @controller.stubs(:auth_via).returns("remote") }

      describe "and the current user is an admin" do
        it "retrieves the custom alerts" do
          Alert.expects(:for).with(@current_user, custom: [:remote_auth_retired])
          @controller.send(:current_user_alerts)
        end
      end

      describe "and the current user is not an admin" do
        before { @current_user.stubs(:is_admin?).returns(false) }

        it "does not retrieve the custom alerts" do
          Alert.expects(:for).with(@current_user, custom: [])
          @controller.send(:current_user_alerts)
        end
      end
    end

    describe "when the request is not authenticated via remote auth" do
      before { @controller.stubs(:auth_via).returns("hello") }

      describe "and the current user is an admin" do
        it "does not retrieve the custom alerts" do
          Alert.expects(:for).with(@current_user, custom: [])
          @controller.send(:current_user_alerts)
        end
      end

      describe "and the current user is not an admin" do
        before { @current_user.stubs(:is_admin?).returns(false) }

        it "does not retrieve the custom alerts" do
          Alert.expects(:for).with(@current_user, custom: [])
          @controller.send(:current_user_alerts)
        end
      end
    end
  end
end
