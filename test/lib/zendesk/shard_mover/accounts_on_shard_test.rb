require_relative "../../../support/test_helper"
require_relative "../../../support/attachment_test_helper"

SingleCov.covered!

describe Zendesk::ShardMover::AccountsOnShard do
  fixtures :accounts

  let(:feature) { 'syncattachments_enabled' }
  let(:pending_disable_value) { Zendesk::ShardMover::Accounts::PENDING_DISABLE_VALUE }

  let(:enabled_account) { accounts(:minimum) }
  let(:disabled_account) { accounts(:support) }
  let(:flipped_account) { accounts(:trial) }

  let(:helper) { Zendesk::ShardMover::DisabledAccountHelper.new(feature) }
  let(:enumerator) { Zendesk::ShardMover::AccountsOnShard.new(helper, 1, 1.second) }

  before do
    Zendesk::ShardMover::Accounts.enable_feature(feature, enabled_account)
    Zendesk::ShardMover::Accounts.ack_disable_feature(feature, disabled_account)
    Zendesk::ShardMover::Accounts.enable_feature(feature, flipped_account)
  end

  it "traverses enabled accounts on shard" do
    assert enumerator.none? { |id| id == disabled_account.id }, "should not find disabled_account"
    assert enumerator.one? { |id| id == enabled_account.id }, "should find enabled_account"
    assert enumerator.one? { |id| id == flipped_account.id }, "should find flipped_account"

    Zendesk::ShardMover::Accounts.disable_feature(feature, flipped_account)
    Timecop.travel(Time.now + 1.seconds)

    assert enumerator.none? { |id| id == disabled_account.id }, "should not find disabled_account"
    assert enumerator.one? { |id| id == enabled_account.id }, "should find enabled_account"
    assert enumerator.none? { |id| id == flipped_account.id }, "should no longer find flipped_account"
  end

  it "acknowledges pending-disabled requests" do
    assert enumerator.one? { |id| id == flipped_account.id }, "should find flipped_account"

    assert_equal pending_disable_value, Zendesk::ShardMover::Accounts.disable_feature(feature, flipped_account)
    Timecop.travel(Time.now + 1.seconds)

    assert enumerator.none? { |id| id == flipped_account.id }, "should no longer find flipped_account"

    flipped_account.reload
    assert_equal 'disabled', Zendesk::ShardMover::Accounts.disable_feature(feature, flipped_account), "flipped should be ack'd"
  end

  it "identifies disabled" do
    assert enumerator.one? { |id| id == flipped_account.id }, "should find flipped_account"

    assert enumerator.disabled? disabled_account.id
    refute enumerator.disabled? enabled_account.id
    refute enumerator.disabled? flipped_account.id

    assert_equal pending_disable_value, Zendesk::ShardMover::Accounts.disable_feature(feature, flipped_account)
    Timecop.travel(Time.now + 1.seconds)

    assert enumerator.disabled? flipped_account.id
  end

  # Ensures that the order of operations is such that, if an account that has
  # not yet been yielded becomes disabled while walking the enumeration, it
  # is never yielded
  it "identifies disabled during map" do
    # for this test, we'll start with all enabled, then disable two accounts
    # when "working with" the first:
    Zendesk::ShardMover::Accounts.enable_feature(feature, enabled_account)
    Zendesk::ShardMover::Accounts.enable_feature(feature, disabled_account)
    Zendesk::ShardMover::Accounts.enable_feature(feature, flipped_account)

    ids = []
    flipped = false
    enumerator.map do |id|
      next unless !flipped && (id == enabled_account.id || id == disabled_account.id || id == flipped_account.id)
      ids << id
      Zendesk::ShardMover::Accounts.disable_feature(feature, enabled_account)
      Zendesk::ShardMover::Accounts.disable_feature(feature, disabled_account)
      Zendesk::ShardMover::Accounts.disable_feature(feature, flipped_account)
      flipped = true
      Timecop.travel(Time.now + 1.seconds)
    end

    assert_equal 1, ids.length
  end

  it "rotates accounts by default" do
    Random.srand(1234)
    Zendesk::ShardMover::Accounts.enable_feature(feature, enabled_account)
    Zendesk::ShardMover::Accounts.enable_feature(feature, disabled_account)
    Zendesk::ShardMover::Accounts.enable_feature(feature, flipped_account)

    accounts = enumerator.to_a
    rotated = false
    1000.times do
      rotated = accounts[0] != enumerator.to_a[0]
      break if rotated
    end

    assert rotated
  end

  it "can use a different ordering for accounts" do
    ordering = lambda { |ids| ids.sort.reverse }
    Zendesk::ShardMover::Accounts.enable_feature(feature, enabled_account)
    Zendesk::ShardMover::Accounts.enable_feature(feature, disabled_account)
    Zendesk::ShardMover::Accounts.enable_feature(feature, flipped_account)

    exp = enumerator.send(:active_accounts_on_shard).sort.reverse
    assert_equal exp, Zendesk::ShardMover::AccountsOnShard.new(helper, 1, 1.second, ordering).to_a
  end

  it "doesnt crash if no accounts on shard" do
    enumerator.expects(:active_accounts_on_shard).returns([])
    assert_equal [], enumerator.to_a
  end
end
