require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::ShardMover::Accounts do
  fixtures :accounts

  subject { Zendesk::ShardMover::Accounts }

  before do
    @account = Account.last
  end

  it 'only allows updates for allowed properties' do
    result = subject.feature_capable_of_disablement?('bogus_feature_key')

    assert_equal false, result
  end

  describe 'syncattachments' do
    it 'disables syncattachments property' do
      subject.disable_feature('syncattachments_enabled', @account)
      @account.reload

      assert_equal subject::PENDING_DISABLE_VALUE, @account.settings.syncattachments_enabled
    end

    it 'multiple calls disables syncattachments property' do
      subject.disable_feature('syncattachments_enabled', @account)
      @account.reload

      results = subject.disable_feature('syncattachments_enabled', @account)
      @account.reload

      assert_equal subject::PENDING_DISABLE_VALUE, results
      assert_equal subject::PENDING_DISABLE_VALUE, @account.settings.syncattachments_enabled
    end

    it 'returns disabled when disabled' do
      subject.ack_disable_feature('syncattachments_enabled', @account)
      results = subject.disable_feature('syncattachments_enabled', @account)
      @account.reload

      assert_equal subject::DISABLED_API_VALUE, results
    end

    it 'should request disable pending and return disabled once disabled' do
      # setting should be true to begin with
      # request disable
      results = subject.disable_feature('syncattachments_enabled', @account)
      @account.reload

      assert_equal subject::PENDING_DISABLE_VALUE, results
      assert_equal subject::PENDING_DISABLE_VALUE, @account.settings.syncattachments_enabled

      # syncattachments acks the pending disable flipping it to false
      @account.settings.syncattachments_enabled = 'false'

      # should return disabled now
      results2 = subject.disable_feature('syncattachments_enabled', @account)
      @account.reload

      assert_equal subject::DISABLED_API_VALUE, results2
    end

    it 'enables write to syncattachments property' do
      subject.enable_feature('syncattachments_enabled', @account)
      @account.reload
      assert_equal subject::ENABLED_SETTING_VALUE, @account.settings.syncattachments_enabled
    end
  end

  describe 'write_to_archive_enabled' do
    it 'disables write to archive property' do
      subject.disable_feature('write_to_archive_v2_enabled', @account)
      @account.reload

      assert_equal subject::PENDING_DISABLE_VALUE, @account.settings.write_to_archive_v2_enabled
    end

    it 'multiple calls disables write to archive property' do
      subject.disable_feature('write_to_archive_v2_enabled', @account)
      @account.reload

      results = subject.disable_feature('write_to_archive_v2_enabled', @account)
      @account.reload

      assert_equal subject::PENDING_DISABLE_VALUE, results
      assert_equal subject::PENDING_DISABLE_VALUE, @account.settings.write_to_archive_v2_enabled
    end

    it 'write to archive returns disabled when disabled' do
      subject.ack_disable_feature('write_to_archive_v2_enabled', @account)
      results = subject.disable_feature('write_to_archive_v2_enabled', @account)
      @account.reload

      assert_equal subject::DISABLED_API_VALUE, results
    end

    it 'write to archive should request disable pending and return disabled once disabled' do
      # setting should be true to begin with
      # request disable
      results = subject.disable_feature('write_to_archive_v2_enabled', @account)
      @account.reload

      assert_equal subject::PENDING_DISABLE_VALUE, results
      assert_equal subject::PENDING_DISABLE_VALUE, @account.settings.write_to_archive_v2_enabled

      # syncattachments acks the pending disable flipping it to false
      @account.settings.write_to_archive_v2_enabled = 'false'

      # should return disabled now
      results2 = subject.disable_feature('write_to_archive_v2_enabled', @account)
      @account.reload

      assert_equal subject::DISABLED_API_VALUE, results2
    end

    it 'enables write to archive property' do
      subject.enable_feature('write_to_archive_v2_enabled', @account)
      @account.reload
      assert_equal subject::ENABLED_SETTING_VALUE, @account.settings.write_to_archive_v2_enabled
    end

    it 'fetches the feature status' do
      feature_status = subject.fetch_feature('write_to_archive_v2_enabled', @account)
      assert_equal 'true', feature_status
    end
  end
end
