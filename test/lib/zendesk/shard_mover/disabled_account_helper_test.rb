require_relative "../../../support/test_helper"
require_relative "../../../support/attachment_test_helper"

SingleCov.covered!

describe Zendesk::ShardMover::DisabledAccountHelper do
  fixtures :accounts

  let(:feature) { 'syncattachments_enabled' }
  let(:enabled_account) { accounts(:minimum) }
  let(:disabled_account) { accounts(:support) }
  let(:pending_account) { accounts(:multiproduct) }
  let(:nosetting_account) { accounts(:trial) }
  let(:statsd) { Object.new }
  let(:helper) { Zendesk::ShardMover::DisabledAccountHelper.new(feature, statsd) }

  before do
    Zendesk::ShardMover::Accounts.enable_feature(feature, enabled_account)
    Zendesk::ShardMover::Accounts.ack_disable_feature(feature, disabled_account)
    Zendesk::ShardMover::Accounts.disable_feature(feature, pending_account)

    nosetting_setting = AccountSetting.where(account_id: nosetting_account.id, name: feature)
    nosetting_setting.first.delete unless nosetting_setting.empty?
  end

  it "acknowledges enable" do
    statsd.expects(:increment).with('disable_feature_checks', tags: ['found_pending:true'])
    disabled = helper.disabled_accounts_on_shard

    assert_includes disabled, disabled_account.id
    assert_includes disabled, pending_account.id

    Zendesk::ShardMover::Accounts.enable_feature(feature, disabled_account)

    statsd.expects(:increment).with('disable_feature_checks', tags: ['found_pending:false'])
    disabled = helper.disabled_accounts_on_shard

    refute_includes disabled, disabled_account.id
    assert_includes disabled, pending_account.id
  end

  # unfortunately, can't really test the cross-shard nature of acknowledgments
  it "acknowledges disabled" do
    statsd.expects(:increment).with('disable_feature_checks', tags: ['found_pending:true'])
    disabled = helper.disabled_accounts_on_shard

    assert_includes disabled, disabled_account.id
    assert_includes disabled, pending_account.id

    refute_includes disabled, enabled_account.id
    refute_includes disabled, nosetting_account.id

    # Make sure pending_account moved from "pending" to "disabled"
    assert_equal Zendesk::ShardMover::Accounts::DISABLED_SETTING_VALUE,
      AccountSetting.where(account_id: pending_account.id, name: feature).pluck(:value).first
  end
end
