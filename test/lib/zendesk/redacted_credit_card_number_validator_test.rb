require_relative "../../support/test_helper"

SingleCov.covered!

describe 'Zendesk::RedactedCreditCardNumberValidator' do
  describe ".valid?" do
    it "accepts a redacted 13 digit number" do
      assert Zendesk::RedactedCreditCardNumberValidator.valid?('XXXXXXXXX1234')
    end

    it "accepts a redacted 16 digit number" do
      assert Zendesk::RedactedCreditCardNumberValidator.valid?('XXXXXXXXXXXX1234')
    end

    it "accepts a redacted 19 digit number" do
      assert Zendesk::RedactedCreditCardNumberValidator.valid?('XXXXXXXXXXXXXXX1234')
    end

    it "rejects a redacted 12 digit number" do
      refute Zendesk::RedactedCreditCardNumberValidator.valid?('XXXXXXXX1234')
    end

    it "rejects a redacted 20 digit number" do
      refute Zendesk::RedactedCreditCardNumberValidator.valid?('XXXXXXXXXXXXXXXX1234')
    end

    it "rejects a redacted number with only 3 digits" do
      refute Zendesk::RedactedCreditCardNumberValidator.valid?('XXXXXXXXXXXX123')
    end

    it "rejects a redacted number with only 5 digits" do
      refute Zendesk::RedactedCreditCardNumberValidator.valid?('XXXXXXXXXXX12345')
    end

    it "rejects a redacted number with the 4 leading digits" do
      refute Zendesk::RedactedCreditCardNumberValidator.valid?('1234XXXXXXXXXXXX')
    end

    it "rejects alpha characters" do
      refute Zendesk::RedactedCreditCardNumberValidator.valid?('XXXXXXXXXABCD')
    end

    it "rejects a hyphenated number" do
      refute Zendesk::RedactedCreditCardNumberValidator.valid?('XXXX-XXXX-XXXX-1234')
    end

    it "rejects a number with leading spaces" do
      refute Zendesk::RedactedCreditCardNumberValidator.valid?(' XXXXXXXXXXXX1234')
    end

    it "rejects a number with tailing spaces" do
      refute Zendesk::RedactedCreditCardNumberValidator.valid?('XXXXXXXXXXXX1234 ')
    end
  end

  describe "#invalid_format?" do
    it "allows a valid credit card number" do
      refute Zendesk::RedactedCreditCardNumberValidator.invalid_format?('1234-5678-1234-1234')
    end

    it "rejects an invalid credit card number" do
      assert Zendesk::RedactedCreditCardNumberValidator.invalid_format?('abcd-5678-1234-1234')
    end
  end
end
