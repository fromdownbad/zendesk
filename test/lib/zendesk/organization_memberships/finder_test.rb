require_relative "../../../support/test_helper"

SingleCov.covered!

describe Zendesk::OrganizationMemberships::Finder do
  fixtures :accounts, :users, :organizations

  let(:account) { accounts(:minimum) }
  let(:user) { users(:minimum_agent) }
  let(:organization) { organizations(:minimum_organization1) }
  let(:source_type) { Organization.name }
  let(:params) { {} }
  let(:finder) { Zendesk::OrganizationMemberships::Finder.new(account, params) }

  describe "#membership_scope" do
    it "returns the correct scope" do
      assert_equal account, finder.membership_scope
    end

    describe "with user_id" do
      let(:params) { { user_id: user.id } }

      it "returns the correct scope" do
        assert_equal account.users.find(user.id), finder.membership_scope
      end
    end

    describe "with group_id" do
      let(:params) { { organization_id: organization.id } }

      it "returns the correct scope" do
        assert_equal account.organizations.find(organization.id), finder.membership_scope
      end
    end
  end
end
