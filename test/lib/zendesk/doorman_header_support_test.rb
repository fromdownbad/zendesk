require_relative "../../support/test_helper"

SingleCov.covered!

class DoormanHeaderSupportTestController < ActionController::Base
  include Zendesk::DoormanHeaderSupport

  def doorman_account_info
    render json: parsed_doorman_header
  end
end

class DoormanHeaderSupportTest < ActionController::TestCase
  tests DoormanHeaderSupportTestController
  use_test_routes

  before do
    account = accounts(:minimum)
    @account_doorman_info = {
      'account' => {
        'id' => account.id,
        'is_active' => account.is_active,
        'shard_id' => account.shard_id,
        'subdomain' => account.subdomain,
        'multiproduct' => account.multiproduct,
        'lock_state' => account.lock_state,
        'brand_id' => account.brands.first.id,
        'route_id' => account.routes.first.id
      }
    }

    secret = 'mysecret'
    @controller.stubs(:doorman_secret).returns(secret)
    @encoded_doorman_header = JWT.encode(@account_doorman_info, secret)
  end

  describe 'with a doorman header that contains encoded account information' do
    it 'is able to decode it' do
      ZendeskExceptions::Logger.expects(:record).never

      set_header('HTTP_X_ZENDESK_DOORMAN', @encoded_doorman_header)
      get :doorman_account_info
      assert_equal response.body, @account_doorman_info.to_json
    end
  end

  describe 'with a doorman header that does not contains any information' do
    it 'do not decode it' do
      ZendeskExceptions::Logger.expects(:record).never
      JWT.expects(:decode).never

      set_header('HTTP_X_ZENDESK_DOORMAN', "")
      get :doorman_account_info
    end
  end
end
