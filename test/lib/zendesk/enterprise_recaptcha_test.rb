require_relative "../../support/test_helper"
require_relative "../../../lib/zendesk/enterprise_recaptcha"

SingleCov.covered! uncovered: 1

describe Zendesk::EnterpriseRecaptcha do
  class DummyClass
    include Zendesk::EnterpriseRecaptcha
  end

  let(:verification_url) { Zendesk::EnterpriseRecaptcha::RECAPTCHA_VERIFICATION_URL }
  let(:account) { accounts(:trial) }
  let(:api_key) { ENV.fetch('ZENDESK_RECAPTCHA_ENTERPRISE_API_KEY') }
  let(:site_key) { ENV.fetch('ZENDESK_RECAPTCHA_ENTERPRISE_SITE_KEY') }
  let(:request) { stub(host: "example.com", request_parameters: { 'g-recaptcha-response' => 'qKIbEw_8VvPDZA' }) }
  let(:subject) { DummyClass.new }
  let (:statsd) { stub_for_statsd }

  let(:valid_json_response) { '{ "name": "", "event": { "token": "qKIbEw_8VvPDZA", "siteKey": "fbY" }, "score": 0,"tokenProperties": { "valid": true, "invalidReason": "", "hostname": "", "action": "" }, "reasons": [] }' }
  let(:invalid_json_response) { '{ "name": "", "event": { "token": "qKIbEw_8VvPDZA", "siteKey": "fbY" }, "score": 0,"tokenProperties": { "valid": false, "invalidReason": "", "hostname": "", "action": "" }, "reasons": [] }' }

  before do
    subject.stubs(:current_account).returns(account)
  end

  describe_with_and_without_arturo_enabled :orca_classic_recaptcha_enterprise do
    it 'calls corresponding verification method' do
      if @arturo_enabled
        subject.expects(:verify_enterprise_recaptcha).with(request)
      else
        subject.expects(:verify_recaptcha)
      end
      subject.send(:valid_recaptcha?, request)
    end
  end

  describe "#verify_enterprise_recaptcha" do
    before do
      subject.stubs(:statsd_client).returns(statsd)
    end

    describe "when token is valid" do
      before do
        stub_request(:post, "#{verification_url}?key=#{api_key}").with(
          body: "{\"event\":{\"token\":\"qKIbEw_8VvPDZA\",\"siteKey\":\"#{site_key}\"}}",
            headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type' => 'text/json',
            'Referer' => 'example.com',
            'User-Agent' => 'Ruby'
          }
        ).to_return(status: 200, body: valid_json_response, headers: {})
      end

      it "returns true" do
        statsd.expects(:increment).with(:verification, tags: ['result:success_ent'])
        assert subject.send(:verify_enterprise_recaptcha, request)
      end
    end

    describe "when token is invalid" do
      before do
        stub_request(:post, "#{verification_url}?key=#{api_key}").with(
          body: "{\"event\":{\"token\":\"qKIbEw_8VvPDZA\",\"siteKey\":\"#{site_key}\"}}",
            headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type' => 'text/json',
            'Referer' => 'example.com',
            'User-Agent' => 'Ruby'
          }
        ).to_return(status: 200, body: invalid_json_response, headers: {})
      end

      it "returns false" do
        statsd.expects(:increment).with(:verification, tags: ['result:failed_ent_token'])
        refute subject.send(:verify_enterprise_recaptcha, request)
      end
    end

    describe "when something went wrong" do
      before do
        stub_request(:post, "#{verification_url}?key=#{api_key}").with(
          body: "{\"event\":{\"token\":\"qKIbEw_8VvPDZA\",\"siteKey\":\"#{site_key}\"}}",
            headers: {
            'Accept' => '*/*',
            'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
            'Content-Type' => 'text/json',
            'Referer' => 'example.com',
            'User-Agent' => 'Ruby'
          }
        ).to_return(status: 200, body: "{wrong}", headers: {})
      end

      it "returns false" do
        ZendeskExceptions::Logger.expects(:record).once
        statsd.expects(:increment).with(:verification, tags: ['result:failed_ent_serror'])
        refute subject.send(:verify_enterprise_recaptcha, request)
      end
    end
  end
end
