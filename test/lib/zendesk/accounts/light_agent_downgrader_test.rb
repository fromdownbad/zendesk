require_relative "../../../support/test_helper"
require_relative "../../../support/suite_test_helper"

SingleCov.covered!

describe Zendesk::Accounts::LightAgentDowngrader do
  include SuiteTestHelper
  fixtures :users, :accounts, :permission_sets

  describe '.perform' do
    let(:account) { accounts(:multiproduct) }
    let(:perform) { Zendesk::Accounts::LightAgentDowngrader.perform(account: account) }
    let(:light_agent_addon) { Zendesk::Accounts::Product.new(name: 'light_agents', state: 'subscribed', active: true, plan_settings: { max_light_agents: max_light_agents }) }
    let(:second_light_agent) { users(:multiproduct_support_agent) }
    let(:light_agent_role) { account.light_agent_permission_set }
    let(:max_light_agents) { 1 }

    before do
      account.stubs(:spp?).returns(true)
      account.stubs(:products).returns([light_agent_addon])
      second_light_agent.permission_set = light_agent_role
      second_light_agent.save!
    end

    it 'downgrades the newest light agent' do
      perform
      second_light_agent.reload
      refute second_light_agent.is_light_agent?
    end

    it 'downgrades to the new max number of light agents' do
      perform
      assert_equal max_light_agents, account.light_agents.count
    end

    describe 'account has same amount of agents as cap' do
      let(:max_light_agents) { 2 }

      it 'does not downgrade any agents' do
        old_count = account.light_agents.count
        perform
        assert_equal old_count, account.light_agents.count
      end
    end

    describe 'account less agents than cap' do
      let(:max_light_agents) { 3 }

      it 'does not downgrade any agents' do
        old_count = account.light_agents.count
        perform
        assert_equal old_count, account.light_agents.count
      end
    end

    describe 'account does not have light agent addon' do
      let(:light_agent_addon) { Zendesk::Accounts::Product.new(name: 'support_collaboration', state: 'subscribed', active: true) }

      it 'does not downgrade any agents' do
        old_count = account.light_agents.count
        perform
        assert_equal old_count, account.light_agents.count
      end
    end

    describe 'not an SP&P account' do
      before do
        account.stubs(:spp?).returns(false)
      end

      it 'skips the account' do
        old_count = account.light_agents.count
        perform
        assert_equal old_count, account.light_agents.count
      end
    end
  end
end
