require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 4

describe 'Account::OfacExporter' do
  fixtures :all

  describe "#to_csv" do
    it "exports a csv conforming to the ATTUS/Watchdog spec" do
      assert_match %r{^"","","","","support_admin","","City","","Zip","","","","1","active","",""\r\n}, Zendesk::Accounts::OfacExporter.new.to_csv
    end
  end
end
