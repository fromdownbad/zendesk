require_relative "../../../support/test_helper"
require_relative "../../../support/pre_account_creation_helper"
require 'zendesk/accounts/initializer'
require 'json'

SingleCov.covered! uncovered: 6

describe 'AccountInitializer' do
  include PreAccountCreationHelper

  fixtures :accounts, :users, :pre_account_creations, :account_settings, :role_settings

  let(:remote_authentication_params) do
    {
      type: 'saml',
      remote_login_url: 'https://saml.example.com/login',
      remote_logout_url: 'https://saml.example.com/logout',
      fingerprint: 'SAML-SAML-SAML-SAML-SAML-SAML-SAML-SAML!'
    }
  end

  describe "Standard account initialization" do
    before do
      @new_account = initialize_account
      @new_account.save!
    end

    it "returns an account" do
      assert_equal Accounts::Classic, @new_account.class
    end

    before_should "choose a shard" do
      Accounts::Classic.any_instance.expects(:choose_shard!).with(:us, nil)
      Accounts::Classic.any_instance.expects(:choose_shard!).with('us', nil)
    end

    it "sets country_id" do
      assert @new_account.address.country_id
    end

    it "sets phone number" do
      assert_equal "+1-123-456-7890", @new_account.address.phone
    end

    it "sets subdomain" do
      assert_equal @account_subdomain, @new_account.subdomain
    end

    it "sets name" do
      assert_equal @account_name, @new_account.name
    end

    it "sets owner" do
      assert_equal "Sir Wombat", @new_account.owner.name
      assert_equal "wbsnowboards@tz.com", @new_account.owner.email
    end

    it "sets ssl_required" do
      assert(@new_account.settings.ssl_required)
    end
  end

  describe 'with suite_trial parameter' do
    describe 'boolean value' do
      it 'stores true in account settings' do
        account = initialize_account(account: { suite_trial: true })

        assert(account.settings.suite_trial)
      end

      it 'stores false in account settings' do
        account = initialize_account(account: { suite_trial: false })

        assert_equal false, account.settings.suite_trial
      end
    end

    describe 'string value' do
      it 'stores true in account settings' do
        account = initialize_account(account: { suite_trial: 'true' })

        assert(account.settings.suite_trial)
      end

      it 'stores false in account settings' do
        account = initialize_account(account: { suite_trial: 'false' })

        assert_equal false, account.settings.suite_trial
      end
    end
  end

  describe 'trial_extras' do
    it 'stores all key value pairs' do
      account = initialize_account(trial_extras: { key1: 'value1' })

      assert_equal 'key1', account.trial_extras.first.key
      assert_equal 'value1', account.trial_extras.first.value
    end

    it 'without created_via_google does not enable google login' do
      account = initialize_account
      account.save!

      assert_equal false, account.role_settings[:agent_google_login]
      assert_equal false, account.role_settings[:end_user_google_login]
    end

    describe 'with a non-nil and non-empty created_via_google' do
      it 'enables google login if remote_authentication is undefined' do
        account = initialize_account(trial_extras: { created_via_google: '1' })
        account.save!

        assert_equal false, account.role_settings[:agent_zendesk_login]
        assert(account.role_settings[:agent_google_login])
        assert(account.role_settings[:end_user_google_login])
      end
    end

    it 'without created_via_office_365 does not enable office 365 login' do
      account = initialize_account
      account.save!

      assert_equal false, account.role_settings[:agent_office_365_login]
      assert_equal false, account.role_settings[:end_user_office_365_login]
    end

    describe 'with a non-nil and non-empty created_via_office_365' do
      it 'enables office 365 login if remote_authentication is undefined' do
        account = initialize_account(trial_extras: { created_via_office_365: '1' })
        account.save!

        assert_equal false, account.role_settings[:agent_zendesk_login]
        assert(account.role_settings[:agent_office_365_login])
        assert(account.role_settings[:end_user_office_365_login])
      end
    end

    it 'certain keys are copied into account settings' do
      account = initialize_account(trial_extras: {
        xsell_source: 'foo',
        product_sign_up: 'bar'
      })
      account.save!

      assert_equal 'foo', account.settings.xsell_source
      assert_equal 'bar', account.settings.product_sign_up
    end
  end

  describe "Incomplete params" do
    before do
      @params = {}
      @request_ip = "127.0.0.1"
      @account_initializer = Zendesk::Accounts::Initializer.new(@params, @request_ip, :us)
    end

    it "does not try and initialize account" do
      Zendesk::GeoLocation.expects(:locate).never
      @account_initializer.expects(:preferred_locale).never
      account = @account_initializer.account
      assert_nil account.subdomain
    end
  end

  describe 'tables involved in writing' do
    it 'checks all associations that are updated in account pre-creation include `PrecreatedAccountConversion` module or are ignored' do
      queries = sql_queries { initialize_account.save! }

      # Include `PrecreatedAccountConversion` is required for all models that
      #   1. who have association with account model and
      #   2. be updated during account creation
      # So that `after_account_convert` callback is supported
      #
      # More details: https://github.com/zendesk/zendesk/pull/14941

      # These tables are excluded because nothing is required to be updated
      # on after_account_convert callbacks
      #
      # ** Note: activities & rule_ticket_joins are added because activities
      # assignee and requester are different userr. Activity is inserted in
      # filter
      tables_to_ignore = %w[
        accounts
        sequences
        ticket_metric_sets
        ticket_metric_events
        satisfaction_ratings
        durable_queue_audits
        tokens
        cia_attribute_changes
        cia_events
        pre_account_creations
        subscriptions
        fraud_reports
        tag_scores
        activities
        rule_ticket_joins
        subscription_features
        trial_extras
        outbound_emails
        sla_ticket_policies
        esc_kafka_messages
      ]

      classes_that_include_module, classes_that_do_not_include_module = Accounts::Precreation::CLASSES_TO_BE_UPDATED.
        map { |class_string| Kernel.const_get(class_string) }.
        partition { |k| k.included_modules.include?(PrecreatedAccountConversion) }

      tables_that_should_be_updated = classes_that_include_module.map(&:table_name)

      updated_tables = queries.
        reject { |sql| sql =~ /SELECT/ }.
        each_with_object([]) do |q, arr|
        arr.concat q.scan(/INSERT INTO `([^\s]+)`|UPDATE `([^\s]+)`/)
      end
      updated_tables = updated_tables.flatten.compact.uniq

      unaccounted_updated_tables = updated_tables - tables_that_should_be_updated - tables_to_ignore
      tables_marked_for_update_but_not_updated = tables_that_should_be_updated - updated_tables

      # There are multiple errors that can be reported from this test. It is possible to break this up into multiple
      # tests however the setup is expensive (~16-17 secs) so you would need to either have multiple expensive tests or
      # share state across test cases.
      #
      # The cases that are asserted are:
      # - A class is added to CLASSES_TO_BE_UPDATED but does not have the `PrecreatedAccountConversion` module.
      # - A table is updated during account pre-creation whose class is not in CLASSES_TO_BE_UPDATED with the module
      #   included, or is not in the tables_to_ignore list above.
      # - A class is added to CLASSES_TO_BE_UPDATED but the corresponding table is not modified during account
      #   pre-creation.
      message = []
      if classes_that_do_not_include_module.any?
        message << "add 'include PrecreatedAccountConversion' to #{classes_that_do_not_include_module.map(&:name)}"
      else
        if unaccounted_updated_tables.any?
          message << "For the model(s) that map to the table(s): #{unaccounted_updated_tables} do the following: \n" \
            "\t\t* add 'include PrecreatedAccountConversion'\n" \
            "\t\t* add the class string to Accounts::Precreation::CLASSES_TO_BE_UPDATED"
          message << " OR add the tables to the `tables_to_ignore` variable defined in this test\n"
        end

        redundant_classes = tables_marked_for_update_but_not_updated + classes_that_do_not_include_module.map(&:table_name)
        if redundant_classes.any?
          message << " For the model(s) that map to the table(s): #{tables_marked_for_update_but_not_updated} do the " \
            "following: \n" \
            "\t\t* remove 'include PrecreatedAccountConversion'\n" \
            "\t\t* remove the class string from Accounts::Precreation::CLASSES_TO_BE_UPDATED"
        end
      end

      raise "Errors:\n\t- #{message.join("\n\t-")}" if message.any?
    end
  end

  describe 'account precreation process' do
    let(:minimum) do
      accounts(:minimum).tap do |acc|
        acc.owner.email = 'noreply@zendesk.com'
        acc.owner.identities.first.destroy
        acc.owner.identities.reload
        acc.owner.identities.first.make_primary!
        acc.owner.identities.first.verify!
        acc.trial_extras.destroy_all
        TrialExtra.where(account_id: acc.id).destroy_all
      end
    end

    let(:precreated_account) do
      get_precreated_account(
        name: :precreation_one,
        account_id: minimum.id,
        target_locale_id: 1
      )
    end

    let(:new_account_with_precreation) do
      precreated_account
      initialize_account
      @account_initializer.account
    end

    let(:new_account_with_no_precreation) do
      PreAccountCreation.destroy_all
      initialize_account
      @account_initializer.account
    end

    before do
      Zendesk::RedisStore.redis_client = FakeRedis::Redis.new
      @redis_client = Zendesk::RedisStore.client
      Account.any_instance.stubs(:redis_client).returns(@redis_client)
    end

    after do
      @redis_client.flushall
    end

    it 'only saves the same account instances once when there is a pre-created account' do
      new_account_with_precreation.expects(:finish_precreation_binding)

      stack_traces = []
      new_account_with_precreation.class.after_save -> {
        stack_traces << Kernel.caller.reject { |l| l =~ /gems|minitest/ }.join("\n")
      }
      new_account_with_precreation.save!

      message = stack_traces.join("\n#{"===" * 10}\n Save should not be called more than once during account creation ^^\n#{"===" * 10}\n")
      assert_equal 1, stack_traces.size, message
    end

    it 'only saves the same account instance once when there is no pre-created account' do
      new_account_with_no_precreation.expects(:finish_precreation_binding)

      stack_traces = []
      new_account_with_no_precreation.class.after_save -> {
        stack_traces << Kernel.caller.reject { |l| l =~ /gems|minitest/ }.join("\n")
      }
      new_account_with_no_precreation.save!

      message = stack_traces.join("\n#{"===" * 10}\n Save should not be called more than once during account creation ^^\n#{"===" * 10}\n")
      assert_equal 1, stack_traces.size, message
    end

    describe '.update_precreated_account' do
      it 'should not save a precreated account' do
        initialize_account

        assert_equal minimum.id, precreated_account.account.id
        updated_at = minimum.updated_at
        @account_initializer.send(:update_precreated_account, precreated_account.account)

        assert_equal updated_at, precreated_account.account.updated_at
        assert_equal updated_at, minimum.updated_at
      end

      it 'should not save trial extras' do
        initialize_account
        assert_equal minimum.id, precreated_account.account.id

        assert_equal 0, precreated_account.account.trial_extras.count
        @account_initializer.send(:update_precreated_account, precreated_account.account)

        assert_equal 0, precreated_account.account.trial_extras.count
      end

      it "removes all previous identities from the owner" do
        initialize_account
        assert_equal minimum.id, precreated_account.account.id

        precreated_account.account.owner.update_attributes!(email: 'second-identity@example.com')
        @account_initializer.send(:update_precreated_account, precreated_account.account)

        assert_equal 1, precreated_account.account.owner.identities.size
        assert_equal 'wbsnowboards@tz.com', precreated_account.account.owner.identities.first.value
      end

      it "marks account as serviceable" do
        initialize_account
        acc = precreated_account.account
        acc.is_serviceable = false

        # refute acc.is_serviceable
        @account_initializer.send(:update_precreated_account, acc)
        assert acc.is_serviceable
      end

      it "adds trial extras" do
        initialize_account
        acc = precreated_account.account
        acc.expects(:add_trial_extras)

        @account_initializer.send(:update_precreated_account, acc)
      end

      describe "#create_owner_as_verified?" do
        before do
          Arturo.enable_feature!(:create_owner_as_unverified)
        end

        describe "for a shopify account" do
          before do
            initialize_account(creation_channel: 'shopify2')
          end

          it "marks it as verified" do
            acc = precreated_account.account
            @account_initializer.send(:update_precreated_account, acc)
            assert_equal acc.owner.is_verified?, "1"
          end
        end

        describe "for a magento account" do
          before do
            initialize_account(creation_channel: 'magento2')
          end

          it "marks it as verified" do
            acc = precreated_account.account
            @account_initializer.send(:update_precreated_account, acc)
            assert_equal acc.owner.is_verified?, "1"
          end
        end

        describe "for a normal account" do
          before do
            initialize_account
          end

          it "marks it as unverified" do
            acc = precreated_account.account
            @account_initializer.send(:update_precreated_account, acc)
            assert_equal acc.owner.is_verified?, false
          end
        end
      end
    end

    describe 'Converting a precreated account to a real account' do
      it "marks the precreated account as used" do
        initialize_account
        assert_equal minimum.id, precreated_account.account.id
        base_account = @account_initializer.account
        assert_equal base_account.id, precreated_account.account.id

        assert_nil base_account.pre_account_creation.deleted_at
        assert_equal PreAccountCreation::ProvisionStatus::ONGOING, base_account.pre_account_creation.status
        base_account.save!

        refute_nil precreated_account.reload.deleted_at
        assert_equal PreAccountCreation::ProvisionStatus::FINISHED, precreated_account.status
      end

      it "saves is_fast_account_creation true to trial extras" do
        new_account_with_precreation.save!
        assert_equal "true", new_account_with_precreation.trial_extras.where(key: 'is_fast_account_creation').first.value
      end

      it "saves is_fast_account_creation false to trial extras" do
        new_account_with_no_precreation.save!
        assert_equal "false", new_account_with_no_precreation.trial_extras.where(key: 'is_fast_account_creation').first.value
      end

      it "sets is_serviceable as true" do
        new_account_with_precreation.is_serviceable = false
        new_account_with_precreation.save!
        assert new_account_with_precreation.is_serviceable
      end

      it "marks the precreated account as used even with exceptions" do
        initialize_account
        assert_equal minimum.id, precreated_account.account.id
        base_account = @account_initializer.account
        assert_equal base_account.id, precreated_account.account.id

        assert_nil base_account.pre_account_creation.deleted_at
        assert_equal PreAccountCreation::ProvisionStatus::ONGOING, base_account.pre_account_creation.status

        # simulate an error by setting acocunt name to nil
        base_account.name = nil
        assert_raises ActiveRecord::RecordInvalid do
          base_account.save!
        end

        # precreated account is marked as ONGOING, so new accounts wont use it anymore
        assert base_account.pre_account_creation.is_binding?
        assert_equal PreAccountCreation::ProvisionStatus::ONGOING, base_account.pre_account_creation.status
      end
    end

    describe 'testing conversion callbacks' do
      it 'does not call any callbacks if failed to save account' do
        pre_account_creation_callbacks.each do |cb|
          new_account_with_precreation.expects(cb).never
        end

        new_account_with_precreation.name = nil
        assert_raise ActiveRecord::RecordInvalid do
          new_account_with_precreation.save!
        end
      end

      it 'does call all callbacks if converted' do
        pre_account_creation_callbacks.each do |cb|
          new_account_with_precreation.expects(cb)
        end
        new_account_with_precreation.save!
      end
    end
  end

  describe 'with a precreated account' do
    let(:new_account_params) do
      {
        account: {
          name: 'z3nTestAccountName',
          subdomain: 'z3ntestsubdomain',
          source: 'a new source',
          help_desk_size: "10-24",
          currency: 'JPY',
          utc_offset: '+9',
          remote_authentication: remote_authentication_params,
          google_apps_domain: 'z3ntestsubdomain.com'
        },
        owner: {
          name: "Z3n test",
          email: "z3ntest@zendesk.com",
          password: new_password,
          is_verified: 1
        },
        address: {
          phone: "+1-000-000-0000",
          country_code: "US"
        },
        trial_extras: {
          key1: 'value1'
        },
        partner: {
          name: 'Mini Me',
          url: 'http://minime.example.com',
        },
        creation_channel: 'website1234'
      }
    end

    let(:minimum) do
      accounts(:minimum).tap do |acc|
        acc.owner.email = 'noreply@zendesk.com'
        acc.owner.identities.first.destroy
        acc.owner.identities.reload
        acc.owner.identities.first.make_primary!
        acc.owner.identities.first.verify!
      end
    end

    let(:precreated_account) do
      get_precreated_account(
        name: :precreation_one,
        account_id: minimum.id,
        target_locale_id: 1
      )
    end

    let(:new_password) do
      "abc%sdef" % rand(1000000)
    end

    before do
      Guide::CreateTrialJob.stubs(:enqueue)
      Account.any_instance.stubs(has_saml?: true)
      PreAccountCreation.any_instance.stubs(:need_more_accounts?).returns(false)
    end

    it 'updates a pre-created account with new params' do
      num_precreated_accounts = PreAccountCreation.count(:all)

      # simulate a precreated account a while ago
      minimum.subscription.update_attribute(:trial_expires_on, (Date.today - 10.days).to_date)
      refute_equal minimum.subscription.trial_expires_on, Date.today

      # we have a precreated account
      assert_not_nil precreated_account
      assert_equal 'noreply@zendesk.com', minimum.owner.email

      # we are using this precreated account
      account_base = initialize_account(new_account_params)
      assert_not_nil account_base.pre_account_creation
      assert_equal precreated_account.id, account_base.pre_account_creation.id

      # destroy old authentications and save new account information
      account_base.remote_authentications.destroy_all
      pre_account_creation_callbacks.each do |callback|
        account_base.expects(callback)
      end

      queries = sql_queries { account_base.save! }

      # https://zendesk.atlassian.net/wiki/pages/viewpage.action?pageId=111936598
      # currently queries size is 205
      expected_query_dist = {
        "payments" => 2,
        "accounts" => 23,
        "account_settings" => 22,
        "account_texts" => 2,
        "groups" => 1,
        "users" => 5,
        "user_settings" => 1,
        "account_property_sets" => 3,
        "url_shorteners" => 1,
        "zopim_subscriptions" => 2,
        "organizations" => 1,
        "recipient_addresses" => 6,
        "addresses" => 2,
        "brands" => 5,
        "routes" => 6,
        "gooddata_integrations" => 2,
        "remote_authentications" => 8,
        "durable_queue_audits" => 5,
        "translation_locales" => 4,
        "user_identities" => 2,
        "subscriptions" => 2,
        "feature_boosts" => 1,
        "role_settings" => 2,
        "voice_sub_accounts" => 1,
        "trial_extras" => 2,
        "pre_account_creations" => 2,
        "reserved_subdomains" => 1
      }

      ignored_tables = [
        'arturo_features' # In production, arturo features are cached and don't trigger a query for each use
      ]

      dist = queries.each_with_object(Hash.new { |h, k| h[k] = [] }) do |q, h|
        next unless table_name = q[/(FROM|UPDATE|INSERT INTO) `([a-z_]+)`/, 2]
        next if ignored_tables.include?(table_name)
        h[table_name] << q
      end

      expected_size = expected_query_dist.values.reduce(:+)
      message = "There are more queries than #{expected_size} now, it slows down fast account creation process\n"
      changes = { queries: {} }
      dist.each do |k, v|
        expected_value = expected_query_dist[k] || 0
        if v.size > expected_value
          changes[k] = "expected: #{expected_value} -> current: #{v.size}"
          changes[:queries][k] = v
        end
      end
      message += "new queries for each table: #{JSON.pretty_generate(changes)}\n"
      message += "You can print all queries for debugging `puts queries.join(\"\\n\")`\n"
      assert_empty(changes[:queries], message)

      # test all information are updated
      account = Account.find_by_subdomain('z3ntestsubdomain')
      assert_not_nil account

      # check if pre-created account is used
      assert_equal account_base.id, account.id
      assert_equal minimum.id, precreated_account.account_id
      assert_equal precreated_account.account_id, account.id

      # check subscription is up-to-date
      assert_equal Date.today + Subscription::TRIAL_DURATION, account.subscription.trial_expires_on

      # check all params are updated
      assert_equal 'a new source', account.source
      assert_equal '10-24', account.help_desk_size
      assert_equal 'z3nTestAccountName', account.name
      assert_equal 'Z3n test', account.owner.name
      assert_equal 'z3ntest@zendesk.com', account.owner.email
      assert account.owner.is_verified

      require 'bcrypt'
      assert ::BCrypt::Password.new(account.owner.crypted_password) == new_password

      assert_equal '+1-000-000-0000', account.address.phone
      assert_equal CurrencyType.find('JPY'), account.subscription.currency_type
      assert_equal 'Osaka', account.time_zone

      saml_auth = account.remote_authentications.saml.first
      assert_not_nil saml_auth
      assert_equal remote_authentication_params[:remote_login_url], saml_auth.remote_login_url
      assert_equal remote_authentication_params[:remote_logout_url], saml_auth.remote_logout_url
      assert_equal remote_authentication_params[:fingerprint], saml_auth.token

      assert_equal 2, account_base.send(:redis_trial_extras).count
      assert_includes account_base.send(:redis_trial_extras).keys, 'key1'
      assert_includes account_base.send(:redis_trial_extras).keys, 'is_fast_account_creation'
      assert_includes account_base.send(:redis_trial_extras).values, 'value1'
      assert_includes account_base.send(:redis_trial_extras).values, true

      assert_equal 'Mini Me', account.settings.partner_name
      assert_equal 'http://minime.example.com', account.settings.partner_url

      assert(account.settings.ssl_required)
      assert_equal 'website1234', account.settings.creation_channel

      assert_equal num_precreated_accounts + 1, PreAccountCreation.count(:all)
    end
  end

  def initialize_account(extra_params = {})
    @account_name = "Wombat Snowboards"
    @account_subdomain = "wombatsnowboards"
    @params = {
      account: {
        name: @account_name,
        subdomain: @account_subdomain,
        help_desk_size: "Small team"
      },
      owner: {
        name: "Sir Wombat",
        email: "wbsnowboards@tz.com",
        is_verified: "1"
      },
      address: {
        phone: "+1-123-456-7890",
        country_code: "US"
      },
      trial_extras: {
        "expected_num_seats" => "1-14",
        "Convertro_SID__c" => "EWN99GMXGZ6W",
        "Session_Landing__c" => "https://www.zendesk.com/apps/surveymonkey-create/",
        "Sub_Industry" => "",
        "Behavioral_1__c" => "",
        "Behavioral_2__c" => "",
        "Behavioral_3__c" => "",
        "DB_City__c" => "",
        "DB_State__c" => "",
        "DB_CName__c" => "",
        "DB_CCode__c" => "",
        "Country__c" => "",
        "DB_Zip__c" => "",
        "Behavioral_4__c" => "",
        "Session_Count__c" => "",
        "Session_First__c" => "",
        "Session_Last__c" => "",
        "Session_Referrer__c" => "",
        "features" => ""
      }
    }.merge(extra_params)
    @request_ip = "98.210.110.15"
    Zendesk::GeoLocation.stubs(:locate).returns(city: "San Francisco", country_code: "US")
    @account_initializer = Zendesk::Accounts::Initializer.new(@params, @request_ip, :us)
    @new_account = @account_initializer.account
  end
end
