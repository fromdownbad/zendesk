require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Accounts::CustomRolesResolver do
  fixtures :accounts

  let(:account) { accounts(:minimum) }
  let(:custom_roles_resolver) { Zendesk::Accounts::CustomRolesResolver.new(account) }

  describe '#enabled_permission_sets?' do
    subject { custom_roles_resolver.enabled_permission_sets? }

    describe 'with no permission sets' do
      it 'is false' do
        refute subject
      end
    end

    describe 'with custom roles enabled' do
      before { account.stubs(:has_permission_sets?).returns(true) }

      it 'is true' do
        assert subject
      end
    end

    describe 'with chat agents enabled' do
      before { account.stubs(:has_chat_permission_set?).returns(true) }

      it 'is true' do
        assert subject
      end
    end

    describe 'with light agents enabled' do
      before { account.stubs(:has_light_agents?).returns(true) }

      it 'is true' do
        assert subject
      end
    end

    describe 'with non support agents enabled' do
      let(:account) { accounts(:multiproduct) }

      before { PermissionSet.enable_contributor! account }

      it 'is true' do
        assert subject
      end
    end

    describe 'with billing_admin permission set present' do
      let(:account) { accounts(:multiproduct) }

      before { PermissionSet.create_billing_admin! account }

      it 'is true' do
        assert subject
      end
    end
  end

  describe '#assignable_permission_sets' do
    subject { custom_roles_resolver.assignable_permission_sets }

    describe 'with no permission sets' do
      it 'returns nothing' do
        assert_empty subject
      end
    end

    describe 'with custom roles enabled' do
      before do
        account.stubs(:has_permission_sets?).returns(true)
        permission_sets = PermissionSet.build_default_permission_sets account
        permission_sets.each(&:save!)
      end

      it 'returns the default permission sets' do
        assert_equal 3, subject.count
        assert_equal [::PermissionSet::Type::CUSTOM], subject.map(&:role_type).uniq
      end

      describe 'with existing chat agent permission set but not enabled' do
        before do
          account.stubs(:has_chat_permission_set?).returns(false)
          PermissionSet.create_chat_agent! account
        end

        it 'only returns the default permission sets' do
          assert_equal 3, subject.count
          assert_equal [::PermissionSet::Type::CUSTOM], subject.map(&:role_type).uniq
        end
      end
    end

    describe 'with chat agents enabled' do
      before do
        account.stubs(:has_chat_permission_set?).returns(true)
        PermissionSet.create_chat_agent! account
      end

      it 'returns the chat agent permission set' do
        assert_equal 1, subject.count
        assert_equal [::PermissionSet::Type::CHAT_AGENT], subject.map(&:role_type).uniq
      end
    end

    describe 'with light agents enabled' do
      before do
        account.stubs(:has_light_agents?).returns(true)
        PermissionSet.create_light_agent! account
      end

      it 'returns the light agent permission set' do
        assert_equal 1, subject.count
        assert_equal [::PermissionSet::Type::LIGHT_AGENT], subject.map(&:role_type).uniq
      end

      it 'returns nothing if light_agent_permission_set is nil' do
        account.stubs(:light_agent_permission_set).returns(nil)

        assert_empty subject
      end
    end

    describe 'with non support agents enabled' do
      before do
        account.multiproduct = true
        PermissionSet.enable_contributor! account
      end

      it 'returns the non support agent permission set' do
        assert_equal 1, subject.count
        assert_equal [::PermissionSet::Type::CONTRIBUTOR], subject.map(&:role_type).uniq
      end
    end

    describe 'with billing_admin enabled' do
      before do
        PermissionSet.create_billing_admin! account
      end

      it 'returns the billing admin permission set' do
        assert_equal 1, subject.count
        assert_equal [::PermissionSet::Type::BILLING_ADMIN], subject.map(&:role_type).uniq
      end
    end

    describe 'with everything' do
      before do
        account.multiproduct = true
        account.stubs(:has_light_agents?).returns(true)
        account.stubs(:has_chat_permission_set?).returns(true)
        account.stubs(:has_permission_sets?).returns(true)
        permission_sets = PermissionSet.build_default_permission_sets account
        permission_sets.each(&:save!)
        PermissionSet.create_chat_agent! account
        PermissionSet.create_light_agent! account
        PermissionSet.enable_contributor! account
        PermissionSet.create_billing_admin! account
      end

      it 'returns all unique permission sets' do
        assert_equal 7, subject.count
      end
    end
  end
end
