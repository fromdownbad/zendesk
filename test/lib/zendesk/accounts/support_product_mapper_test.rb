require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Accounts::SupportProductMapper do
  fixtures :accounts, :translation_locales

  let(:subject) { Zendesk::Accounts::SupportProductMapper }

  let(:account) { accounts(:minimum) }

  let(:system_account) { accounts(:systemaccount) }

  let(:locale) { translation_locales(:english_by_zendesk) }

  let(:precreated_account) do
    account = accounts(:extra_large)
    account.create_pre_account_creation!(
      pod_id: 1,
      locale_id: locale.id,
      region: 'us',
      source: 'Test Account Creation',
      status: PreAccountCreation::ProvisionStatus::INIT
    )
    account.update_column(:name, PreAccountCreation::DEFAULT_ACCOUNT_NAME)
    account.update_column(:subdomain, "z3nprecreated-1215180805")
    account
  end

  let(:converted_precreation_account) do
    account = accounts(:support)
    account.create_pre_account_creation!(
      pod_id: 1,
      locale_id: locale.id,
      region: 'us',
      source: 'Test Account Creation',
      status: PreAccountCreation::ProvisionStatus::FINISHED
    )
    account
  end

  let(:shell_account_without_support) { accounts(:shell_account_without_support) }

  let(:soft_deleted_account) do
    inactive_account.stubs(deleted_at: DateTime.yesterday)
    inactive_account
  end

  let(:inactive_account) { accounts(:inactive) }

  let(:account_without_trial_expires_on) do
    account.subscription.stubs(trial_expires_on: nil)
    account
  end

  let(:subscribed_account) do
    account.subscription.stubs(zuora_managed?: true)
    account
  end

  let(:sandbox_account) do
    account.stubs(is_sandbox?: true)
    account
  end

  let(:spoke_account) do
    account.subscription.stubs(is_spoke?: true)
    account
  end

  let(:sponsored_account) do
    account.subscription.stubs(is_sponsored?: true)
    account
  end

  let(:payment_method_manual_account) do
    account.subscription.stubs(payment_method_type: PaymentMethodType.Manual)
    account
  end

  let(:trial_expiry_date) { Date.parse("2017-01-01") }
  let(:expired_trial_account) do
    trial_account.subscription.update_attribute(:trial_expires_on, trial_expiry_date)
    trial_account.expire_trial!
    trial_account
  end

  let(:expired_trial_multiproduct_account) do
    trial_account.update_column(:multiproduct, true)
    trial_account.subscription.update_attribute(:trial_expires_on, trial_expiry_date)
    trial_account.expire_trial!
    trial_account
  end

  # non trial, non zuora, payment method CC account - see https://zendesk.atlassian.net/browse/VOLT-1174
  let(:legacy_sponsored_account) do
    account.subscription.update_attribute(:is_trial, false)
    account.subscription.update_attribute(:payment_method_type, PaymentMethodType.Credit_Card)
    account
  end

  let(:trial_account) { accounts(:trial) }

  let(:suite_trial_account) do
    trial_account.settings.stubs(suite_trial?: true)
    trial_account
  end

  let(:suite_subscribed_account) do
    subscribed_account.settings.stubs(suite_subscription?: true)
    subscribed_account
  end

  let(:side_conversations_account) do
    account.subscription.stubs(has_ticket_threads?: true)
    account
  end

  describe '.derive_product' do
    it 'returns product attributes derived from account and subscription records' do
      expected_attributes = {
        name: 'support',
        state: :trial,
        trial_expires_at: trial_account.subscription.trial_expires_on.to_time.iso8601,
        plan_settings: {
          plan_type: 3,
          max_agents: trial_account.subscription.max_agents,
          light_agents: false,
          suite: false,
          side_conversations: false,
          boost_expires_at: nil,
          boosted_plan_type: nil
        }
      }
      actual_attributes = subject.derive_product(trial_account)
      assert_equal expected_attributes, actual_attributes
    end

    it 'returns derived product for suite trial account' do
      expected_attributes = {
        name: 'support',
        state: :trial,
        trial_expires_at: suite_trial_account.subscription.trial_expires_on.to_time.iso8601,
        plan_settings: {
          plan_type: 3,
          max_agents: suite_trial_account.subscription.max_agents,
          light_agents: false,
          suite: true,
          side_conversations: false,
          boost_expires_at: nil,
          boosted_plan_type: nil
        }
      }
      actual_attributes = subject.derive_product(suite_trial_account)
      assert_equal expected_attributes, actual_attributes
    end

    it 'returns derived product for Zuora managed account' do
      expected_attributes = {
        name: 'support',
        state: :subscribed,
        trial_expires_at: subscribed_account.subscription.trial_expires_on.to_time.iso8601,
        plan_settings: {
          plan_type: 2,
          max_agents: 10,
          light_agents: false,
          suite: false,
          side_conversations: false,
          boost_expires_at: nil,
          boosted_plan_type: nil
        }
      }
      actual_attributes = subject.derive_product(subscribed_account)
      assert_equal expected_attributes, actual_attributes
    end

    it 'returns derived product for Zuora managed suite account' do
      expected_attributes = {
        name: 'support',
        state: :subscribed,
        trial_expires_at: suite_subscribed_account.subscription.trial_expires_on.to_time.iso8601,
        plan_settings: {
          plan_type: 2,
          max_agents: 10,
          light_agents: false,
          suite: true,
          side_conversations: false,
          boost_expires_at: nil,
          boosted_plan_type: nil
        }
      }
      actual_attributes = subject.derive_product(suite_subscribed_account)
      assert_equal expected_attributes, actual_attributes
    end

    it 'returns state_updated_at as the trial expiry datetime for expired trial accounts' do
      Timecop.travel(trial_expiry_date) do
        expected_trial_expires_at = expired_trial_account.subscription.trial_expires_on.to_time.iso8601
        expected_attributes = {
          name: 'support',
          state: :expired,
          trial_expires_at: expected_trial_expires_at,
          plan_settings: {
            plan_type: 3,
            max_agents: expired_trial_account.subscription.max_agents,
            light_agents: false,
            suite: false,
            side_conversations: false,
            boost_expires_at: nil,
            boosted_plan_type: nil
          },
          state_updated_at: expired_trial_account.subscription.trial_expires_on.to_time.iso8601
        }
        actual_attributes = subject.derive_product(expired_trial_account)
        assert_equal expected_attributes, actual_attributes
      end
    end

    it 'returns state_updated_at as the churned_on datetime for cancelled accounts if it is present' do
      churned_on_datetime = Time.zone.now
      inactive_account.subscription.update_attribute(:churned_on, churned_on_datetime)
      expected_attributes = {
        name: 'support',
        state: :cancelled,
        trial_expires_at: inactive_account.subscription.trial_expires_on.to_time.iso8601,
        plan_settings: {
          plan_type: 2,
          max_agents: inactive_account.subscription.max_agents,
          light_agents: false,
          suite: false,
          side_conversations: false,
          boost_expires_at: nil,
          boosted_plan_type: nil
        },
        state_updated_at: churned_on_datetime.to_datetime.iso8601
      }
      actual_attributes = subject.derive_product(inactive_account)
      assert_equal expected_attributes, actual_attributes
    end

    it 'does not return state_updated_at for cancelled accounts if churned_on is not present' do
      inactive_account.subscription.update_attribute(:churned_on, nil)
      expected_attributes = {
        name: 'support',
        state: :cancelled,
        trial_expires_at: inactive_account.subscription.trial_expires_on.to_time.iso8601,
        plan_settings: {
          plan_type: 2,
          max_agents: inactive_account.subscription.max_agents,
          light_agents: false,
          suite: false,
          side_conversations: false,
          boost_expires_at: nil,
          boosted_plan_type: nil
        }
      }
      actual_attributes = subject.derive_product(inactive_account)
      assert_equal expected_attributes, actual_attributes
    end

    it 'returns trial_expires_at as nil when subscription.trial_expires_on is nil' do
      assert_nil subject.derive_product(account_without_trial_expires_on)[:trial_expires_at]
    end

    it 'returns nil for multiproduct account that does not have support' do
      assert_nil subject.derive_product(shell_account_without_support)
    end

    it 'returns nil for precreated account' do
      assert_nil subject.derive_product(precreated_account)
    end

    it 'returns nil for system account' do
      assert_nil subject.derive_product(system_account)
    end

    it 'returns product state free for a precreated account that has been converted to a sponsored account' do
      assert_equal :free, subject.derive_product(converted_precreation_account)[:state]
    end

    it 'returns product state cancelled for a deleted account' do
      assert_equal :cancelled, subject.derive_product(soft_deleted_account)[:state]
    end

    it 'returns product state cancelled for an inactive/cancelled account' do
      assert_equal :cancelled, subject.derive_product(inactive_account)[:state]
    end

    it 'returns product state free for sandbox accounts' do
      assert_equal :free, subject.derive_product(sandbox_account)[:state]
    end

    it 'returns product state free for spoke accounts' do
      assert_equal :free, subject.derive_product(spoke_account)[:state]
    end

    it 'returns product state free for sponsored accounts' do
      assert_equal :free, subject.derive_product(sponsored_account)[:state]
    end

    it 'returns product state free for serviceable accounts that have payment method type manual' do
      assert_equal :free, subject.derive_product(payment_method_manual_account)[:state]
    end

    it 'returns product state free for serviceable accounts that are not trial, not in zuora, but have payment method CC' do
      assert_equal :free, subject.derive_product(legacy_sponsored_account)[:state]
    end

    it 'returns product state expired for an expired trial account that expired on a previous day' do
      Timecop.travel(trial_expiry_date + 1.day) do
        assert_equal :expired, subject.derive_product(expired_trial_account)[:state]
      end
    end

    it 'returns product state expired for a trial account that expired the same day' do
      Timecop.travel(trial_expiry_date) do
        assert_equal :expired, subject.derive_product(expired_trial_account)[:state]
      end
    end

    it 'returns product state expired for a multiproduct trial account that expired on a previous day' do
      Timecop.travel(trial_expiry_date + 1.day) do
        assert_equal :expired, subject.derive_product(expired_trial_multiproduct_account)[:state]
      end
    end

    it 'returns product state expired for an active but not serviceable account without trial expiry date' do
      account_without_trial_expires_on.is_serviceable = false
      assert_equal :expired, subject.derive_product(account_without_trial_expires_on)[:state]
    end

    it 'returns product state trial for a trial account' do
      assert_equal :trial, subject.derive_product(trial_account)[:state]
    end

    it 'returns product state unknown for an account that cannot be mapped to an existing state' do
      trial_account.subscription.update_attribute(:is_trial, false)
      trial_account.subscription.update_attribute(:payment_method_type, 100)
      assert_equal :unknown, subject.derive_product(trial_account)[:state]
    end

    it 'returns side_conversations plan setting for account with side conversations' do
      assert(subject.derive_product(side_conversations_account)[:plan_settings][:side_conversations])
    end
  end
end
