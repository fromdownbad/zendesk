require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Accounts::Security::AccountAssumption do
  fixtures :accounts

  let(:account) { accounts :minimum }
  let(:account_assumption) do
    Zendesk::Accounts::Security::AccountAssumption.new(account)
  end

  before { Timecop.freeze('2016-01-01') }

  describe '#assumption_duration_for_select' do
    let(:common_durations) do
      [
        ['One day', :day],
        ['One week', :week],
        ['One month', :month],
        ['One year', :year],
        ['Indefinitely', :always]
      ]
    end

    describe 'when account assumption is not enabled' do
      let(:expected) do
        common_durations << ['Select duration', :not_selected]
      end

      it 'returns the correct options for select' do
        assert_same_elements(expected, account_assumption.assumption_duration_for_select)
      end
    end

    describe 'when account assumption is enabled' do
      let(:expected) do
        common_durations << ['Change duration', :already_set]
      end

      before do
        account.settings.assumption_expiration = Time.now + 1.month
      end

      it 'returns the correct options for select' do
        assert_same_elements(expected, account_assumption.assumption_duration_for_select)
      end
    end
  end
end
