require_relative "../../../../support/test_helper"
require_relative "../../../../support/certificate_test_helper"

SingleCov.covered! uncovered: 8

describe Zendesk::Accounts::Security::SslSettingsUpdate do
  include CertificateTestHelper

  def assign_certificate_and_key
    @params[:hosted_ssl][:uploaded_certificate_data] = Rack::Test::UploadedFile.new(@new_support_cert_path)
    @params[:hosted_ssl][:uploaded_key_data] = Rack::Test::UploadedFile.new(@new_support_cert_key_path)
    @params[:hosted_ssl][:key_data_passphrase] = 'foobar'
  end

  fixtures :accounts, :certificates, :certificate_ips, :certificate_authorities

  before do
    @account = accounts(:minimum)
    @account.certificates.destroy_all
    @user = @account.owner

    @params = {account: {settings: {}}}

    @new_support_cert_path = 'test/files/certificate/support.crt'
    @new_support_cert_key_path = 'test/files/certificate/support.key'
    @new_support_cert = OpenSSL::X509::Certificate.new(File.read(@new_support_cert_path))

    @security_policy_update = begin
      Zendesk::Accounts::Security::SslSettingsUpdate.new(
        @user, @params
      )
    end

    ActionMailer::Base.perform_deliveries = true
    ActionMailer::Base.deliveries = []
  end

  describe "when hosted_ip_ssl is not enabled" do
    before do
      Arturo.disable_feature! :hosted_ip_ssl

      @params[:hosted_ssl] = {ssl_type: :sni}
      assign_certificate_and_key
      @security_policy_update.perform
      @account.reload
      @new_cert = @account.certificates.active.first
    end

    it "sets SNI enabled" do
      assert(@new_cert.sni_enabled?)
    end
  end

  describe "#perform" do
    [
      # old, new, key, expected active
      [:ip,   :ip,  true,  true],
      [:ip,   :sni, true,  true],
      [:sni,  :ip,  true,  false],
      [:none, :ip,  true,  false],
      [:none, :sni, true,  true],
      # [:ip,   :ip,  false, true], # not previously tested and failing
      [:ip,   :sni, false, true],
      [:sni,  :ip,  false, false],
      [:sni,  :sni, false, true],
      [:none, :ip,  false, false],
      [:none, :sni, false, true],
    ].each do |old_cert, new_cert, key, expected_active|
      describe "when moving from #{old_cert} to #{new_cert} #{key ? "with" : "without"} a key" do
        before do
          @params[:account][:notifications_recipient]       = 'owner'
          @params[:account][:ip_ranges]                     = '123.123.123.123'
          @params[:account][:is_ssl_enabled]                = '1'
          @params[:account][:admins_can_set_user_passwords] = '1'

          @params[:hosted_ssl] = {ssl_type: new_cert.to_s}
          Arturo.enable_feature! :hosted_ip_ssl

          case old_cert
          when :ip
            @active_cert = create_certificate!(state: :active)
            @ip_address = @active_cert.certificate_ips.first.ip
          when :sni
            @active_cert = create_certificate!(state: :active, create_ip: false)
            @active_cert.update_attribute :sni_enabled, true
          when :none then nil # nothing to do
          else raise
          end

          if key
            assign_certificate_and_key
          else
            CertificateAuthorities.append(OpenSSL::X509::Certificate.new(FixtureHelper::Certificate.read('ca.crt')), true)
            @temp_cert = @account.certificates.build.tap { |c| c.generate_temporary; c.save! }

            @new_support_cert = cert_from_csr(@temp_cert.csr_object)
            @temp_file = Tempfile.open('certificate').tap { |f| f.write(@new_support_cert); f.rewind }

            @params[:hosted_ssl][:uploaded_certificate_data] = Rack::Test::UploadedFile.new(@temp_file)
          end

          @security_policy_update.perform
          @account.reload
          @new_cert = (expected_active ? @account.certificates.active : @account.certificates.pending).first
        end

        after { @temp_file.close(true) if @temp_file }

        it "has no errors" do
          @security_policy_update.errors.must_equal []
        end

        if expected_active
          it "sends the SSL certificate approved notification" do
            assert_equal 1, ActionMailer::Base.deliveries.count
            ActionMailer::Base.deliveries[0].to_s.must_include "Your SSL certificate has been approved"
          end
        else
          it "does not send the SSL certificate approved notification" do
            assert_empty ActionMailer::Base.deliveries
          end
        end

        if expected_active
          it "activates the new certificate" do
            assert_equal @new_support_cert.serial, @account.certificates.active.first.crt_object.serial
          end
        else
          it "does not activate the new certificate" do
            assert_equal [@active_cert].compact, @account.certificates.active
            assert_equal 1, @account.certificates.pending.count(:all)
            assert_equal @new_support_cert.serial, @account.certificates.pending.first.crt_object.serial
          end
        end

        if new_cert == :sni
          it "sets the certificate as SNI" do
            assert @new_cert.sni_enabled?
          end
        else
          it "sets the certificate as IP" do
            refute @new_cert.sni_enabled?
          end
        end

        if new_cert == :sni
          it "creates a sni certificate record and a SNI certificate ip record" do
            ips = @account.certificates.reload.flat_map(&:certificate_ips)
            assert_equal 1, ips.size
            assert(ips[0].sni)
            assert_nil ips[0].ip
          end
        elsif old_cert == :none
          # no-op
        elsif !expected_active
          it "leaves the ip record on the old cert and does not add a ip record to the new cert" do
            assert(@account.certificates.active.first.certificate_ips.first.sni)
            assert_empty @account.certificates.pending.first.certificate_ips
          end
        else
          it "assigns the previous ip address to the new certificate" do
            cert = @account.certificates.reload.active.first
            assert_equal 1, cert.certificate_ips.count(:all)
            assert_equal @ip_address, cert.certificate_ips.first.ip
          end
        end
      end
    end

    describe "when an SSL certificate or SSL key is not passed in the parameters" do
      before { @params[:hosted_ssl] = {} }

      it "has no errors" do
        @security_policy_update.perform

        assert_empty(@security_policy_update.errors)
      end

      it "does not save any certificate models" do
        Certificate.any_instance.expects(:save!).never

        @security_policy_update.perform
      end
    end

    describe "when ssl is forced" do
      before do
        @account.settings.ssl_enabled = true
        @account.stubs(ssl_forced?: true)
      end

      it "does not disable ssl" do
        @params[:account][:is_ssl_enabled] = false
        @security_policy_update.perform
        assert @account.is_ssl_enabled
      end

      it "does does not disable ssl when the parameter is missing" do
        @params[:account].delete(:is_ssl_enabled)
        @security_policy_update.perform
        assert @account.is_ssl_enabled
      end
    end

    describe "#automatic_certificate_provisioning" do
      it "enables automatic_certificate_provisioning" do
        @params[:account][:automatic_certificate_provisioning] = true
        @security_policy_update.perform
        assert(@account.reload.settings.automatic_certificate_provisioning)
      end

      describe "when automatic_certificate_provisioning is enabled" do
        before do
          @account.settings.automatic_certificate_provisioning = true
          @account.settings.save!
        end

        it "does does not disable automatic_certificate_provisioning when the parameter is missing" do
          refute @params[:account].key?(:automatic_certificate_provisioning)
          @security_policy_update.perform

          assert(@account.reload.settings.automatic_certificate_provisioning)
        end

        it "disbles automatic_certificate_provisioning when the parameter is false" do
          @params[:account][:automatic_certificate_provisioning] = false
          @security_policy_update.perform

          assert_equal false, @account.reload.settings.automatic_certificate_provisioning
        end
      end
    end
  end

  describe "when the new certificate has errors" do
    before do
      @user = stub('user')
      @user.stubs(:is_account_owner?)

      @account = accounts(:minimum)
      @params  = {account: {settings: {}}}

      @user.stubs(:account).returns(@account)

      @active_cert = stub('active certificate')
      @temp_cert   = stub('temporary certificate')
      @temp_cert_secondary = stub('temporary certificate 2')

      [@active_cert, @temp_cert, @temp_cert_secondary].each do |cert|
        cert.stubs(:set_uploaded_data)
        cert.stubs(:state=)
        cert.stubs(:sni_enabled=)
        cert.stubs(:sni_enabled?).returns(false)
        cert.stubs(:autoprovisioned?).returns(false)
        cert.stubs(:save!).returns(true)
      end
      @temp_cert.stubs(:upload_certificate)
      @temp_cert_secondary.stubs(:upload_certificate)
      @temp_cert_secondary.stubs(:set_uploaded_data).returns([Certificate::ErrorCode::NEED_KEY])

      @certificates = stub(active: [@active_cert], temporary: [@temp_cert_secondary, @temp_cert])
      @account.stubs(:certificates).returns(@certificates)

      @security_policy_update = begin
        Zendesk::Accounts::Security::SslSettingsUpdate.new(
          @user, @params
        )
      end

      @params[:account] = {
        is_ssl_enabled: '1',
        set_long_hsts_header_on_host_mapping: '1'
      }

      @params[:hosted_ssl] = {
        uploaded_certificate_data: fixture_file_upload('certificate/support.crt'),
        uploaded_key_data: fixture_file_upload('certificate/support.key'),
        key_data_passphrase: 'foobar'
      }

      @revoked_cert = stub('revoked certificate')
      @revoked_cert.stubs(:save!).returns(true)
      @revoked_cert.stubs(:state=)
      @revoked_cert.stubs(sni_enabled?: false)
      @revoked_cert.stubs(:autoprovisioned?).returns(false)
      @revoked_cert.stubs(:account).returns(@account)
      @revoked_cert.stubs(:id).returns(42)
      @active_cert.stubs(:dup).returns(@revoked_cert)
    end

    describe "and the uploaded data is not compatible with the active certificate" do
      before { @active_cert.stubs(:set_uploaded_data).returns('errors') }

      describe "because we are missing the intermediate" do
        before { @temp_cert.stubs(:set_uploaded_data).returns([Certificate::ErrorCode::NEED_INTERMEDIATE]) }
        it " the replacement as in 'pending' state for ops approval" do
          @temp_cert.expects(:upload_certificate)
          @temp_cert.expects(:save!).returns(true)
          @security_policy_update.perform
        end
      end

      describe "but is comppatible with the latest temporary certificate" do
        before { @temp_cert.stubs(:set_uploaded_data).returns(nil) }

        it "updates the temporary certificate" do
          @temp_cert.expects(:upload_certificate)

          @security_policy_update.perform
        end

        it "saves the temporary certificate" do
          @temp_cert.expects(:save!).returns(true)

          @security_policy_update.perform
        end

        it "does does not return any errors" do
          @security_policy_update.perform

          assert_empty(@security_policy_update.errors)
        end
      end

      describe "but is only compatible with a previous temporary certificate" do
        before do
          @temp_cert_secondary.stubs(:set_uploaded_data).returns(nil)
          @temp_cert.stubs(:set_uploaded_data).returns([Certificate::ErrorCode::NEED_KEY])
        end

        it "updates that previous temporary certificate" do
          @temp_cert_secondary.expects(:upload_certificate).once
          @temp_cert.expects(:upload_certificate).never

          @security_policy_update.perform
        end

        it "saves the previous temporary certificate" do
          @temp_cert_secondary.expects(:save!).returns(true)
          @temp_cert.expects(:save!).never

          @security_policy_update.perform
        end

        it "does does not return any errors" do
          @security_policy_update.perform

          assert_empty(@security_policy_update.errors)
        end
      end

      describe "but is not compatible with the temporary certificate" do
        before do
          @temp_cert.stubs(:set_uploaded_data).
            returns([::Certificate::ErrorCode::NEED_KEY])
        end

        it "returns an error" do
          @security_policy_update.perform

          assert @security_policy_update.errors.count == 1
        end
      end
    end
  end
end
