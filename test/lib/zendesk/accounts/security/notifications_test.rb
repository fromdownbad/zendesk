require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Accounts::Security::Notifications do
  fixtures :all

  def persisted_settings(settings = {})
    @account.settings.set(
      security_notifications_recipient: settings[:recipient],
      security_notifications_recipient_id: settings[:id]
    )
  end

  before do
    groups = stub('groups')
    groups.stubs(:map).returns([['Group 1', '1'], ['Group 2', '2']])

    @account = accounts(:minimum)
    @account.stubs(:groups).returns(groups)
    @notifications = Zendesk::Accounts::Security::Notifications.new(@account)
  end

  describe "#recipient" do
    describe "before a recipient value is saved" do
      it "returns a default recipient" do
        assert_equal 'admins',
          @notifications.recipient
      end
    end

    describe "with a recipient-without-id" do
      before do
        persisted_settings(recipient: 'owner')
      end

      it "returns the recipient" do
        assert_equal 'owner', @notifications.recipient
      end
    end

    describe "with a recipient-with-id" do
      before do
        persisted_settings(recipient: 'group', id: 14)
      end

      describe "that references an existing record" do
        before do
          @account.groups.stubs(:exists?).returns(true)
        end

        it "returns the recipient id" do
          assert_equal 14, @notifications.recipient
        end
      end

      describe "that references an non-existent (deleted) record" do
        before do
          @account.groups.stubs(:exists?).returns(false)
        end

        it "returns the default recipient" do
          assert_equal 'admins', @notifications.recipient
        end
      end
    end
  end

  describe "#recipient_emails" do
    describe "when security notifications list is set to 'admins'" do
      before do
        persisted_settings(recipient: 'admins')
      end

      it "returns an array of appropriate emails" do
        assert_equal @account.admins.select(&:is_verified?).map(&:email), @notifications.recipient_emails
      end

      it "does not include unverified admins" do
        refute @notifications.recipient_emails.include?("non_verified_admin@zendesk.com")
      end
    end

    describe "when security notifications list is set to 'owner'" do
      before do
        persisted_settings(recipient: 'owner')
      end

      it "returns an array of appropriate emails" do
        assert_equal [@account.owner.email], @notifications.recipient_emails
      end
    end

    describe "when security notifications list is set to a specific group" do
      before do
        persisted_settings(recipient: 'group', id: 1)
        @group_users = [users(:minimum_agent), users(:minimum_admin)]
        group = stub('group', users: @group_users)
        @account.groups.stubs(:find).returns(group)
        @account.groups.stubs(:exists?).returns(true)
      end

      it "returns an array of appropriate emails" do
        assert_equal @group_users.map(&:email), @notifications.recipient_emails
      end
    end

    describe "when security notifications list is nil or incorrect" do
      before do
        persisted_settings(recipient: 'foobar')
      end

      it "returns a default array of appropriate emails" do
        assert_equal [@account.owner.email], @notifications.recipient_emails
      end
    end
  end

  describe "#possible_recipients_for_select" do
    before do
      @expected = [
        ['All Admins', 'admins'],
        ['Account Owner', 'owner'],
        ['Group 1', '1'],
        ['Group 2', '2']
      ]
    end

    it "returns an list of name/value arrays containing all possible recipients" do
      assert_equal @expected, @notifications.possible_recipients_for_select
    end
  end

  describe "#recipient_settings" do
    before do
      @expected = {
        security_notifications_recipient: 'admins',
        security_notifications_recipient_id: nil
      }
    end

    it "returns an appropriate value for a recipient-without-id" do
      @expected[:security_notifications_recipient] = 'owner'

      assert_equal @expected, @notifications.recipient_settings('owner')
    end

    describe "with a recipient-with-id that references a existing record" do
      before do
        @account.groups.stubs(:exists?).returns(true)
      end

      it "returns an appropriate value" do
        @expected[:security_notifications_recipient]    = 'group'
        @expected[:security_notifications_recipient_id] = '65'

        assert_equal @expected, @notifications.recipient_settings('65')
      end
    end

    describe "with a recipient-with-id that references a non-existent (deleted) record" do
      before do
        @account.groups.stubs(:exists?).returns(false)
      end

      it "returns an appropriate value" do
        assert_equal @expected, @notifications.recipient_settings('65')
      end
    end
  end
end
