require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Accounts::Security::EndUserSettingsUpdate do
  fixtures :all

  before do
    @user          = stub('user')
    @account       = accounts(:minimum)
    @role_settings = role_settings(:minimum)
    @params        = HashWithIndifferentAccess.new(
      account: {role_settings: {}}
    )

    @user.stubs(:account).returns(@account)

    Account.any_instance.stubs(:has_saml?).returns(true)
    RoleSettings.stubs(:where).with(account_id: @account.id).
      returns(stub(first_or_create: @role_settings))

    @native_auth = stub('native', id: 1, auth_mode: 1, priority: 2)
    @jwt_auth    = stub('jwt', id: 2, auth_mode: 3, priority: 1)
    @saml_auth   = stub('saml', id: 3, auth_mode: 2, priority: 0)

    @remote_auths = [@native_auth, @jwt_auth, @saml_auth]

    @remote_auths.each do |ra|
      ra.stubs(:update_attributes)
      ra.stubs(:update_attribute)
      ra.stubs(:is_active=)
      ra.stubs(:save!).returns(true)
    end

    @security_policy_update = begin
      Zendesk::Accounts::Security::EndUserSettingsUpdate.new(
        @user, @params, @remote_auths
      )
    end
  end

  describe "#perform" do
    before do
      @params[:account][:remote_authentications] = {
        'native' => {
          id: @native_auth.id.to_s,
          is_active: true,
          remote_logout_url: 'https://native.com'
        },
        'jwt' => {
          id: @jwt_auth.id.to_s,
          is_active: false,
          remote_logout_url: 'https://jwt.com'
        },
        'saml' => {
          id: @saml_auth.id.to_s,
          is_active: false,
          remote_logout_url: 'https://saml.com'
        }
      }

      @role_settings.update_attributes!(
        end_user_password_allowed: false
      )

      @account.ip_ranges = '1.1.1.1'
      @account.is_ssl_enabled = false
      @account.save
    end

    describe "when Zendesk is selected as the authentication type" do
      before do
        @params[:account][:role_settings] = {
          end_user_security_policy_id: '200',
          end_user_zendesk_login: true,
          end_user_google_login: true,
          end_user_twitter_login: true,
          end_user_facebook_login: false
        }

        @security_policy_update.perform
      end

      it "updates the security policy ID" do
        assert_equal 200, @role_settings.end_user_security_policy_id
      end

      it "updates the role-based login service settings" do
        assert @role_settings.end_user_zendesk_login
        assert @role_settings.end_user_google_login
        assert @role_settings.end_user_twitter_login
        refute @role_settings.end_user_facebook_login
      end
    end

    describe "when Zendesk authentication is selected" do
      before do
        @params[:account][:role_settings] = {
          end_user_security_policy_id: 200,
          end_user_zendesk_login: false,
          end_user_remote_login: true
        }

        @role_settings.update_attributes!(
          end_user_security_policy_id: 100,
          end_user_google_login: true,
          end_user_twitter_login: true,
          end_user_facebook_login: true,
          end_user_password_allowed: false
        )

        @security_policy_update.perform
      end

      before_should "update the remote authentication settings" do
        @native_auth.expects(:update_attributes).with(
          'is_active'         => true,
          'remote_logout_url' => 'https://native.com'
        )
      end

      before_should "not turn on other remote authentication methods" do
        @jwt_auth.expects(:is_active=).with(false)
        @saml_auth.expects(:is_active=).with(false)
      end

      before_should "not update other remote authentication settings" do
        @jwt_auth.expects(:update_attributes).never
        @saml_auth.expects(:update_attributes).never
      end

      before_should "update remote authentications priorities" do
        @native_auth.expects(:update_attribute).with(:priority, 0)
        @saml_auth.expects(:update_attribute).with(:priority, 1)
        @jwt_auth.expects(:update_attribute).with(:priority, 2)
      end

      it "does not update the role-based security settings" do
        assert_equal 100, @role_settings.end_user_security_policy_id
      end

      it "disables the role-based social login service settings" do
        refute @role_settings.end_user_google_login
        refute @role_settings.end_user_twitter_login
        refute @role_settings.end_user_facebook_login
      end

      it "enables passwords" do
        assert @role_settings.end_user_password_allowed
      end
    end

    describe "when remote authentication is selected" do
      let(:end_user_password_allowed) { true }

      before do
        @params[:account][:role_settings] = {
          end_user_security_policy_id: 100,
          end_user_zendesk_login: false,
          end_user_remote_login: true,
          end_user_password_allowed: end_user_password_allowed
        }

        @role_settings.update_attributes!(
          end_user_security_policy_id: 100,
          end_user_zendesk_login: true,
          end_user_remote_login: false,
          end_user_google_login: false,
          end_user_twitter_login: false,
          end_user_facebook_login: false
        )

        @security_policy_update.perform
      end

      before_should "update the remote authentication settings" do
        @native_auth.expects(:update_attributes).with(
          'is_active'         => true,
          'remote_logout_url' => 'https://native.com'
        )
      end

      before_should "not turn on other remote authentication methods" do
        @jwt_auth.expects(:is_active=).with(false)
        @saml_auth.expects(:is_active=).with(false)
      end

      before_should "not update other remote authentication settings" do
        @jwt_auth.expects(:update_attributes).never
        @saml_auth.expects(:update_attributes).never
      end

      before_should "update remote authentications priorities" do
        @native_auth.expects(:update_attribute).with(:priority, 0)
        @saml_auth.expects(:update_attribute).with(:priority, 1)
        @jwt_auth.expects(:update_attribute).with(:priority, 2)
      end

      it "updates the role-based security settings" do
        assert_equal 100, @role_settings.end_user_security_policy_id
        assert @role_settings.end_user_remote_login
      end

      it "disables other login service settings" do
        refute @role_settings.end_user_google_login
        refute @role_settings.end_user_twitter_login
        refute @role_settings.end_user_facebook_login
      end

      it "disables zendesk login but allows passwords" do
        assert_equal false, @role_settings.end_user_zendesk_login
        assert(@role_settings.end_user_password_allowed)
      end

      describe "when passwords are disabled" do
        let(:end_user_password_allowed) { false }

        it "disables zendesk login and passwords" do
          assert_equal false, @role_settings.end_user_zendesk_login
          assert_equal false, @role_settings.end_user_password_allowed
        end
      end
    end
  end
end
