require_relative '../../../../../support/test_helper'
require_relative "../../../../../support/multiproduct_test_helper"

SingleCov.covered! file: 'lib/zendesk/accounts/security/api_settings_update.rb', uncovered: 14

describe Zendesk::Accounts::Security::ApiSettingsUpdate do
  include MultiproductTestHelper
  fixtures :accounts, :role_settings, :users

  security_policy_id = {
    low: Zendesk::SecurityPolicy::Low.id,
    medium: Zendesk::SecurityPolicy::Medium.id,
    high: Zendesk::SecurityPolicy::High.id,
    custom: Zendesk::SecurityPolicy::Custom.id
  }.freeze

  [:shell_account_with_support, :minimum].each do |account_type|
    describe "with a #{account_type} account" do
      before do
        @user = users(:systemuser)

        @account = account_type == :shell_account_with_support ? setup_shell_account_with_support : accounts(account_type)

        @user.stubs(:account).returns(@account)
        Timecop.freeze('2016-01-01')
      end

      describe '#perform' do
        before do
          @params = {ip: {}, remote_authentications: {saml: {}, jwt: {}}, authentication: {agent: {password: {}}, end_user: {}}}
          @account.update_attributes!(admins_can_set_user_passwords: false)
        end

        describe 'when the account has a customized password max_sequence' do
          before do
            @params[:authentication][:agent][:security_policy_id]      = security_policy_id[:custom].to_s
            @params[:authentication][:agent][:password][:max_sequence] = '5'
            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
          end

          describe 'when changing the policy to non-customized' do
            before do
              @account.reload
              @params[:authentication][:agent] = { security_policy_id: security_policy_id[:low].to_s }
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            end

            it 'reverts max_sequence to nil' do
              @account.reload
              assert_nil @account.custom_security_policy.max_sequence
            end
          end
        end

        describe 'when the account has a customized session timeout' do
          before do
            @account.settings.session_timeout = 5
            @account.save!
          end

          describe 'when sending a non-custom agent security policy ID' do
            before do
              @params[:authentication][:agent][:security_policy_id] = security_policy_id[:medium].to_s
            end

            it 'does not change the session timeout' do
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
              @account.reload

              assert_equal 5, @account.settings.session_timeout
            end
          end
        end

        describe 'when custom_security_policy does not exist on account' do
          before { assert_nil @account.custom_security_policy }

          describe 'when sending only session_timeout' do
            before { @params[:session_timeout] = '20160' }

            it 'sets the session_timeout setting without creating a custom security_policy' do
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
              @account.reload

              assert_equal 20160, @account.settings.session_timeout
              assert_nil @account.custom_security_policy
            end
          end

          describe 'when sending medium agent security_policy_id with new session_timeout' do
            before do
              @params[:authentication][:agent][:security_policy_id] = security_policy_id[:medium].to_s
              @params[:session_timeout]                             = '20160'
            end

            it 'creates medium custom_security_policy with new session_timeout' do
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
              @account.reload

              assert_not_nil @account.custom_security_policy
              assert_equal Zendesk::SecurityPolicy::Medium.password_length, @account.custom_security_policy.password_length
              assert_equal 20160, @account.settings.session_timeout
            end
          end

          describe 'when there is a custom session_timeout' do
            before do
              @account.settings.session_timeout = 5
              @account.settings.save!

              assert_equal 5, @account.settings.session_timeout
              refute @account.settings.default(:session_timeout) == @account.settings.session_timeout
            end

            describe 'when sending a non-custom security_policy_id' do
              before do
                @params[:authentication][:agent][:security_policy_id] = security_policy_id[:medium].to_s
              end

              it 'does not modify session_timeout' do
                Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
                @account.reload

                assert_equal Zendesk::SecurityPolicy::Medium.password_length, @account.custom_security_policy.password_length
                assert_equal 5, @account.settings.session_timeout
                refute @account.settings.default(:session_timeout) == @account.settings.session_timeout
              end
            end
          end

          describe 'when sending high agent security_policy_id' do
            before { @params[:authentication][:agent][:security_policy_id] = security_policy_id[:high].to_s }

            it 'creates high custom_security_policy' do
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
              @account.reload

              assert_not_nil @account.custom_security_policy
              assert_equal Zendesk::SecurityPolicy::High.password_duration, @account.custom_security_policy.password_duration
              assert_equal Zendesk::SecurityPolicy::High.password_length, @account.custom_security_policy.password_length
            end
          end

          describe 'when sending custom agent security_policy_id and customizations' do
            before do
              @params[:authentication][:agent][:security_policy_id]         = security_policy_id[:custom].to_s
              @params[:authentication][:agent][:password][:password_length] = 7
            end

            it 'creates a customized agent custom_security_policy' do
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
              @account.reload

              assert_not_nil @account.custom_security_policy
              assert_equal 7, @account.custom_security_policy.password_length
            end
          end

          describe 'when agent password is an empty hash' do
            before { @params[:authentication][:agent][:password] = {} }

            it 'should not create a custom_security_policy' do
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
              @account.reload

              assert_nil @account.custom_security_policy
            end
          end
        end

        describe 'when a medium custom_security_policy exists,' do
          before do
            @account.role_settings.update_attribute(:agent_security_policy_id, security_policy_id[:medium].to_s)
            CustomSecurityPolicy.build_from_current_policy(@account).save

            @account.reload
            assert_equal Zendesk::SecurityPolicy::Medium.password_length, @account.custom_security_policy.password_length
          end

          describe 'sending high agent security_policy_id with new session_timeout' do
            before do
              @params[:authentication][:agent][:security_policy_id] = security_policy_id[:high].to_s
              @params[:session_timeout]                             = '20160'
            end

            it 'updates to high custom_security_policy with customized session timeout' do
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
              @account.reload

              assert_equal Zendesk::SecurityPolicy::High.password_duration, @account.custom_security_policy.password_duration
              assert_equal 20160, @account.settings.session_timeout
            end
          end

          describe 'sending low agent security_policy_id' do
            before { @params[:authentication][:agent][:security_policy_id] = security_policy_id[:low].to_s }

            it 'updates to low custom_security_policy' do
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
              @account.reload

              assert_equal Zendesk::SecurityPolicy::Low.password_length, @account.custom_security_policy.password_length
            end
          end

          describe 'sending in password parameters with with a non custom security_policy_id' do
            before do
              @params[:authentication][:agent][:password][:password_length] = 12

              @api_settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
              @api_settings_update.perform
              @account.reload
            end

            it 'sets an error' do
              refute_empty @api_settings_update.errors
            end

            it 'does not update custom password_length' do
              assert_equal Zendesk::SecurityPolicy::High.password_length, @account.custom_security_policy.password_length
            end

            it 'does not update agent_security_policy_id' do
              assert_equal security_policy_id[:medium], @account.role_settings.agent_security_policy_id
            end
          end
        end

        describe 'when a custom custom_security_policy exists' do
          before do
            @account.role_settings.update_attribute(:agent_security_policy_id, security_policy_id[:custom].to_s)
            CustomSecurityPolicy.build_from_current_policy(@account).save

            @account.reload
            # build from custom uses high values.
            assert_equal Zendesk::SecurityPolicy::High.password_length, @account.custom_security_policy.password_length
          end

          describe 'sending only password parameters' do
            before do
              @params[:authentication][:agent][:password][:password_length] = 12

              @api_settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
              @api_settings_update.perform
              @account.reload
            end

            it 'does not set an error' do
              assert_empty @api_settings_update.errors
            end

            it 'updates custom password settings' do
              assert_equal 12, @account.custom_security_policy.password_length
            end

            it 'does not change agent_security_policy_id' do
              assert_equal security_policy_id[:custom], @account.role_settings.agent_security_policy_id
            end
          end

          describe 'sending in password parameters with with a non custom security_policy_id' do
            before do
              @params[:authentication][:agent][:password][:password_length] = 12
              @params[:authentication][:agent][:security_policy_id] = security_policy_id[:medium].to_s

              @api_settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
              @api_settings_update.perform
              @account.reload
            end

            it 'sets an error' do
              refute_empty @api_settings_update.errors
            end

            it 'does not update custom password_length' do
              assert_equal Zendesk::SecurityPolicy::High.password_length, @account.custom_security_policy.password_length
            end

            it 'does not update agent_security_policy_id' do
              assert_equal security_policy_id[:custom], @account.role_settings.agent_security_policy_id
            end
          end
        end
      end
    end
  end
end
