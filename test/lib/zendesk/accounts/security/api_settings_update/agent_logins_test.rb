require_relative '../../../../../support/test_helper'
require_relative "../../../../../support/multiproduct_test_helper"

SingleCov.covered! file: 'lib/zendesk/accounts/security/api_settings_update.rb', uncovered: 29

describe Zendesk::Accounts::Security::ApiSettingsUpdate do
  include MultiproductTestHelper
  fixtures :accounts, :role_settings, :users

  [:shell_account_with_support, :minimum].each do |account_type|
    describe "with a #{account_type} account" do
      before do
        @user = users(:systemuser)

        @account = account_type == :shell_account_with_support ? setup_shell_account_with_support : accounts(account_type)

        @user.stubs(:account).returns(@account)
        Timecop.freeze('2016-01-01')
      end

      describe '#perform' do
        before do
          @params = {ip: {}, remote_authentications: {saml: {}, jwt: {}}, authentication: {agent: {password: {}}, end_user: {}}}
          @account.update_attributes!(admins_can_set_user_passwords: false)
        end

        describe 'when multiple agent logins are passed as enabled' do
          before do
            @params[:authentication][:agent][:office_365_login] = true
            @params[:authentication][:agent][:google_login]     = true
          end

          it 'does not set them as enabled' do
            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            assert_equal false, @account.role_settings.agent_office_365_login
            assert_equal false, @account.role_settings.agent_google_login
          end

          it 'sets an error' do
            api_settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
            api_settings_update.perform

            refute api_settings_update.errors.empty?
          end
        end

        describe 'when an agent login channel is enabled and we try to enable another one' do
          before do
            @params[:authentication][:agent][:office_365_login] = true
          end

          it 'does not set the new one it as enabled' do
            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            @account.role_settings.reload
            assert_equal false, @account.role_settings.agent_office_365_login
          end

          it 'does keep the old one as enabled' do
            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            @account.role_settings.reload
            assert(@account.role_settings.agent_zendesk_login)
          end

          it 'sets an error' do
            api_settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
            api_settings_update.perform

            refute api_settings_update.errors.empty?
          end
        end

        describe 'when setting all agent logins as disabled' do
          before do
            @account.role_settings.agent_zendesk_login = false
            @account.role_settings.agent_google_login = true
            @account.role_settings.save

            @params[:authentication][:agent][:google_login] = false
          end

          it 'sets zendesk_login as enabled' do
            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            @account.role_settings.reload
            assert(@account.role_settings.agent_zendesk_login)
          end
        end

        describe 'when there are valid and unvalid changes' do
          before do
            @params[:remote_authentications][:jwt][:is_active] = true
            @params[:remote_authentications][:saml][:is_active] = true
            @params[:email_agent_when_sensitive_fields_changed] = false
          end

          it 'sets errors for the unvalid changes' do
            api_settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
            api_settings_update.perform

            refute api_settings_update.errors.empty?
          end

          it 'does not save the valid changes' do
            @account.reload
            assert @account.email_agent_when_sensitive_fields_changed
          end
        end
      end
    end
  end
end
