require_relative '../../../../../support/test_helper'
require_relative "../../../../../support/multiproduct_test_helper"

SingleCov.covered! file: 'lib/zendesk/accounts/security/api_settings_update.rb', uncovered: 13

describe Zendesk::Accounts::Security::ApiSettingsUpdate do
  include MultiproductTestHelper
  fixtures :accounts, :role_settings, :users

  security_policy_id = {
    low: Zendesk::SecurityPolicy::Low.id,
    medium: Zendesk::SecurityPolicy::Medium.id,
    high: Zendesk::SecurityPolicy::High.id,
    custom: Zendesk::SecurityPolicy::Custom.id
  }.freeze

  [:shell_account_with_support, :minimum].each do |account_type|
    describe "with a #{account_type} account" do
      before do
        @user = users(:systemuser)

        @account = account_type == :shell_account_with_support ? setup_shell_account_with_support : accounts(account_type)

        @user.stubs(:account).returns(@account)
        Timecop.freeze('2016-01-01')
      end

      describe '#perform' do
        before do
          @params = {ip: {}, remote_authentications: {saml: {}, jwt: {}}, authentication: {agent: {password: {}}, end_user: {}}}
          @account.update_attributes!(admins_can_set_user_passwords: false)
        end

        describe 'when all parameters are provided' do
          before do
            Account.any_instance.stubs(:has_jwt?).returns(true)

            @params[:ip][:enable_agent_ip_restrictions]                                  = true
            @params[:ip][:ip_ranges]                                                     = '123.123.123.123'
            @params[:ip][:ip_restriction_enabled]                                        = true
            @params[:authentication][:agent][:zendesk_login]                             = false
            @params[:authentication][:agent][:google_login]                              = true
            @params[:authentication][:agent][:office_365_login]                          = false
            @params[:authentication][:agent][:remote_login]                              = false
            @params[:authentication][:agent][:remote_bypass]                             = 1
            @params[:authentication][:agent][:enforce_sso]                               = true
            @params[:authentication][:agent][:security_policy_id]                        = security_policy_id[:custom].to_s
            @params[:authentication][:agent][:password][:password_history_length]        = 6
            @params[:authentication][:agent][:password][:password_length]                = 11
            @params[:authentication][:agent][:password][:password_complexity]            = 1
            @params[:authentication][:agent][:password][:password_in_mixed_case]         = true
            @params[:authentication][:agent][:password][:password_duration]              = 180
            @params[:authentication][:agent][:password][:failed_attempts_allowed]        = 9
            @params[:authentication][:agent][:password][:max_sequence]                   = 4
            @params[:authentication][:agent][:password][:disallow_local_part_from_email] = true
            @params[:authentication][:end_user][:zendesk_login]                          = false
            @params[:authentication][:end_user][:facebook_login]                         = true
            @params[:authentication][:end_user][:google_login]                           = true
            @params[:authentication][:end_user][:office_365_login]                       = true
            @params[:authentication][:end_user][:remote_login]                           = true
            @params[:authentication][:end_user][:twitter_login]                          = true
            @params[:authentication][:end_user][:enforce_sso]                            = true
            @params[:authentication][:end_user][:security_policy_id]                     = security_policy_id[:high].to_s
            @params[:authentication][:two_factor_enforce]                                = true
            @params[:remote_authentications][:jwt][:is_active] = true
            @params[:remote_authentications][:jwt][:remote_login_url] = 'https://www.jwt-login-url.co.in'
            @params[:remote_authentications][:jwt][:remote_logout_url] = 'https://www.jwt-logout-url.co.in'
            @params[:remote_authentications][:jwt][:ip_ranges] = '122.122.122.122'
            @params[:remote_authentications][:jwt][:update_external_ids] = true
            @params[:remote_authentications][:jwt][:shared_secret] = 'uVyPVcsmNiFkTAYWEe4p14bzOiGyAxOuPwMCvVRKie6p3LPI'
            @params[:admins_can_set_user_passwords]                                      = true
            @params[:email_agent_when_sensitive_fields_changed]                          = true
            @params[:assumption_duration]                                                = 'always'
            @params[:session_timeout]                                                    = '20160'
          end

          it 'adds an error when trying to add invalid ip ranges' do
            @params[:ip][:ip_ranges] = '67.67.67.67.67 123.123.123.123 222.222.222'

            settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
            settings_update.perform

            assert_equal 2, settings_update.errors.size

            assert_equal 'Fix invalid IP range 67.67.67.67.67', settings_update.errors.first
            assert_equal 'Fix invalid IP range 222.222.222', settings_update.errors.last
          end

          it 'updates the session timeout' do
            CustomSecurityPolicy.build_from_current_policy(@account).save
            @account.reload

            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

            assert_equal 20160, @account.settings.session_timeout
          end

          it 'updates the two factor enforce setting' do
            assert_equal false, @account.settings.two_factor_enforce
            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            assert(@account.settings.two_factor_enforce)
          end

          it 'updates the remote authentication configuration' do
            assert_nil @account.remote_authentications.jwt.first
            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            @account.reload
            assert(@account.remote_authentications.jwt.first.is_active)
            assert_equal 'https://www.jwt-login-url.co.in', @account.remote_authentications.jwt.first.remote_login_url
            assert_equal 'https://www.jwt-logout-url.co.in', @account.remote_authentications.jwt.first.remote_logout_url
            assert_equal '122.122.122.122', @account.remote_authentications.jwt.first.ip_ranges
            assert(@account.remote_authentications.jwt.first.update_external_ids)
            assert_equal 'uVyPVcsmNiFkTAYWEe4p14bzOiGyAxOuPwMCvVRKie6p3LPI', @account.remote_authentications.jwt.first.token
          end

          it 'updates the password settings' do
            assert_nil @account.custom_security_policy
            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            @account.reload
            assert_equal 6, @account.custom_security_policy.password_history_length
            assert_equal 11, @account.custom_security_policy.password_length
            assert_equal 1, @account.custom_security_policy.password_complexity
            assert(@account.custom_security_policy.password_in_mixed_case)
            assert_equal 180, @account.custom_security_policy.password_duration
            assert_equal 9, @account.custom_security_policy.failed_attempts_allowed
            assert_equal 4, @account.custom_security_policy.max_sequence
            assert(@account.custom_security_policy.disallow_local_part_from_email)
          end

          it 'updates the agent authentication settings' do
            assert(@account.role_settings.agent_zendesk_login)
            assert_equal false, @account.role_settings.agent_google_login
            assert_equal false, @account.role_settings.agent_office_365_login
            assert_equal false, @account.role_settings.agent_remote_login
            assert_equal 2, @account.role_settings.agent_remote_bypass
            assert(@account.role_settings.agent_password_allowed)
            assert_equal security_policy_id[:low], @account.role_settings.agent_security_policy_id

            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

            @account.role_settings.reload

            assert_equal false, @account.role_settings.agent_zendesk_login
            assert(@account.role_settings.agent_google_login)
            assert_equal false, @account.role_settings.agent_office_365_login
            assert_equal false, @account.role_settings.agent_remote_login
            assert_equal 1, @account.role_settings.agent_remote_bypass
            assert_equal false, @account.role_settings.agent_password_allowed
            assert_equal security_policy_id[:custom], @account.role_settings.agent_security_policy_id
          end

          it 'updates the end user authentication settings' do
            assert(@account.role_settings.end_user_zendesk_login)
            assert_equal false, @account.role_settings.end_user_facebook_login
            assert_equal false, @account.role_settings.end_user_google_login
            assert_equal false, @account.role_settings.end_user_office_365_login
            assert_equal false, @account.role_settings.end_user_remote_login
            assert_equal false, @account.role_settings.end_user_twitter_login
            assert(@account.role_settings.end_user_password_allowed)
            assert_equal security_policy_id[:low], @account.role_settings.end_user_security_policy_id

            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

            @account.role_settings.reload

            assert_equal false, @account.role_settings.end_user_zendesk_login
            assert(@account.role_settings.end_user_facebook_login)
            assert(@account.role_settings.end_user_google_login)
            assert(@account.role_settings.end_user_office_365_login)
            assert(@account.role_settings.end_user_remote_login)
            assert(@account.role_settings.end_user_twitter_login)
            assert_equal false, @account.role_settings.end_user_password_allowed
            assert_equal security_policy_id[:high], @account.role_settings.end_user_security_policy_id
          end

          it 'updates the allowed IP ranges' do
            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

            assert_equal '123.123.123.123', @account.ip_ranges
          end

          it 'updates the global settings' do
            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

            assert @account.settings.ip_restriction_enabled?
            assert @account.settings.enable_agent_ip_restrictions?
            assert @account.email_agent_when_sensitive_fields_changed
            assert @account.admins_can_set_user_passwords
            assert @account.settings.enable_agent_ip_restrictions
          end
        end
      end
    end
  end
end
