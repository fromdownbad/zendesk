require_relative '../../../../../support/test_helper'
require_relative "../../../../../support/multiproduct_test_helper"

SingleCov.covered! file: 'lib/zendesk/accounts/security/api_settings_update.rb', uncovered: 20

describe Zendesk::Accounts::Security::ApiSettingsUpdate do
  include MultiproductTestHelper
  fixtures :accounts, :role_settings, :users

  [:shell_account_with_support, :minimum].each do |account_type|
    describe "with a #{account_type} account" do
      before do
        @user = users(:systemuser)

        @account = account_type == :shell_account_with_support ? setup_shell_account_with_support : accounts(account_type)

        @user.stubs(:account).returns(@account)
        Timecop.freeze('2016-01-01')
      end

      describe '#perform' do
        before do
          @params = {ip: {}, remote_authentications: {saml: {}, jwt: {}}, authentication: {agent: {password: {}}, end_user: {}}}
          @account.update_attributes!(admins_can_set_user_passwords: false)
        end

        describe 'jwt' do
          before { @account.stubs(:has_jwt?).returns(true) }

          it 'errors in jwt params' do
            @params[:remote_authentications][:jwt][:shared_secret] = 'not_a_token'
            assert_nil @account.remote_authentications.jwt.first

            api_settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
            api_settings_update.perform

            @account.remote_authentications.reload
            refute_empty api_settings_update.errors
          end

          it 'updates all values' do
            @params[:remote_authentications][:jwt][:is_active] = true
            @params[:remote_authentications][:jwt][:remote_login_url] = 'https://www.jwt-login-url.co.in'
            @params[:remote_authentications][:jwt][:remote_logout_url] = 'https://www.jwt-logout-url.co.in'
            @params[:remote_authentications][:jwt][:ip_ranges] = '122.122.122.122'
            @params[:remote_authentications][:jwt][:update_external_ids] = true
            @params[:remote_authentications][:jwt][:shared_secret] = 'uVyPVcsmNiFkTAYWEe4p14bzOiGyAxOuPwMCvVRKie6p3LPI'

            assert_nil @account.remote_authentications.jwt.first

            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

            @account.remote_authentications.reload

            assert(@account.remote_authentications.jwt.first.is_active)
            assert_equal 'https://www.jwt-login-url.co.in', @account.remote_authentications.jwt.first.remote_login_url
            assert_equal 'https://www.jwt-logout-url.co.in', @account.remote_authentications.jwt.first.remote_logout_url
            assert_equal '122.122.122.122', @account.remote_authentications.jwt.first.ip_ranges
            assert(@account.remote_authentications.jwt.first.update_external_ids)
            assert_equal 'uVyPVcsmNiFkTAYWEe4p14bzOiGyAxOuPwMCvVRKie6p3LPI', @account.remote_authentications.jwt.first.token
          end

          describe 'already existing jwt (with shared_secret)' do
            before do
              @auth = @account.remote_authentications.create!(
                next_token: SecureRandom.hex(20), # same as the shared_secret parameter that is passed, but needs next_token to pass validations
                remote_login_url: 'https://jwt-exists-login.com'
              )
              @auth.auth_mode = RemoteAuthentication::JWT
              @auth.save!
            end

            describe 'when is_active param is sent as true,' do
              before { @params[:remote_authentications][:jwt][:is_active] = true }

              describe 'no shared_secret is sent' do
                it 'updates params and does not err out' do
                  api_settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
                  api_settings_update.perform

                  @account.remote_authentications.reload

                  assert_empty api_settings_update.errors
                  assert(@account.remote_authentications.jwt.first.is_active)
                end
              end

              it 'updates params correctly' do
                @params[:remote_authentications][:jwt][:remote_login_url] = 'https://jwt-updated-login.com'
                @params[:remote_authentications][:jwt][:remote_logout_url] = 'https://jwt-new-logout.com'
                @params[:remote_authentications][:jwt][:update_external_ids] = false
                @params[:remote_authentications][:jwt][:shared_secret] = 'mNyPVcsmNiFkTAYWEe4p14bzOiGyAxOuPwMCvVRKie6p3LPI'

                Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

                @account.remote_authentications.reload

                assert(@account.remote_authentications.jwt.first.is_active)
                assert_equal 'https://jwt-updated-login.com', @account.remote_authentications.jwt.first.remote_login_url
                assert_equal 'https://jwt-new-logout.com', @account.remote_authentications.jwt.first.remote_logout_url
                assert_equal false, @account.remote_authentications.jwt.first.update_external_ids
                assert_equal 'mNyPVcsmNiFkTAYWEe4p14bzOiGyAxOuPwMCvVRKie6p3LPI', @account.remote_authentications.jwt.first.token
              end
            end

            describe 'when is_active param is false ( and both agent/end_user remote logins are false ), saml does not exist' do
              before do
                @params[:remote_authentications][:jwt][:is_active] = false
                @params[:authentication][:agent][:remote_login] = false
                @params[:authentication][:end_user][:remote_login] = false
                assert_nil @account.remote_authentications.saml.first
              end

              it 'sets is_active to false and updates with new params' do
                @params[:remote_authentications][:jwt][:remote_logout_url] = 'https://jwt-new-logout.com'

                Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

                @account.remote_authentications.reload

                assert_equal false, @account.remote_authentications.jwt.first.is_active
                assert_equal 'https://jwt-new-logout.com', @account.remote_authentications.jwt.first.remote_logout_url
                assert_equal 'https://jwt-exists-login.com', @account.remote_authentications.jwt.first.remote_login_url
              end
            end
          end
        end

        describe 'saml' do
          before { Account.any_instance.stubs(:has_saml?).returns(true) }

          describe 'only saml is sent' do
            before do
              @params = { remote_authentications: {saml: {} }}
            end

            it 'updates when only saml hash is sent' do
              @params[:remote_authentications][:saml][:is_active] = true
              @params[:remote_authentications][:saml][:fingerprint] = '77:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C'

              assert_nil @account.remote_authentications.saml.first
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

              @account.remote_authentications.reload
              assert(@account.remote_authentications.saml.first.is_active)
              assert_equal '77:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C', @account.remote_authentications.saml.first.token
            end
          end

          it 'updates all values' do
            @params[:remote_authentications][:saml][:is_active] = true
            @params[:remote_authentications][:saml][:remote_login_url] = 'https://www.saml-login-url.co.in'
            @params[:remote_authentications][:saml][:remote_logout_url] = 'https://www.saml-logout-url.co.in'
            @params[:remote_authentications][:saml][:ip_ranges] = '123.123.123.123'
            @params[:remote_authentications][:saml][:fingerprint] = '88:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C'

            assert_nil @account.remote_authentications.saml.first

            Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

            @account.remote_authentications.reload

            assert(@account.remote_authentications.saml.first.is_active)
            assert_equal 'https://www.saml-login-url.co.in', @account.remote_authentications.saml.first.remote_login_url
            assert_equal 'https://www.saml-logout-url.co.in', @account.remote_authentications.saml.first.remote_logout_url
            assert_equal '123.123.123.123', @account.remote_authentications.saml.first.ip_ranges
            assert_equal '88:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C', @account.remote_authentications.saml.first.token
          end

          describe 'already existing saml (with fingerpint)' do
            before do
              @auth = @account.remote_authentications.create!(
                fingerprint: '55:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C',
                remote_login_url: 'https://saml-exists-login.com'
              )
              @auth.auth_mode = RemoteAuthentication::SAML
              @auth.save!
            end

            describe 'is_active param is sent as true,' do
              before { @params[:remote_authentications][:saml][:is_active] = true }

              it 'updates params correctly' do
                @params[:remote_authentications][:saml][:remote_login_url] = 'https://saml-updated-login.com'
                @params[:remote_authentications][:saml][:remote_logout_url] = 'https://saml-new-logout.com'
                @params[:remote_authentications][:saml][:fingerprint] = '44:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C'

                Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

                @account.remote_authentications.reload

                assert(@account.remote_authentications.saml.first.is_active)
                assert_equal 'https://saml-updated-login.com', @account.remote_authentications.saml.first.remote_login_url
                assert_equal 'https://saml-new-logout.com', @account.remote_authentications.saml.first.remote_logout_url
                assert_equal '44:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C', @account.remote_authentications.saml.first.token
              end
            end

            describe 'when is_active param is false,' do
              before { @params[:remote_authentications][:saml][:is_active] = false }

              it 'sets is_active to false and updates with new params' do
                @params[:remote_authentications][:saml][:remote_logout_url] = 'https://saml-new-logout.com'

                Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

                @account.remote_authentications.reload

                assert_equal false, @account.remote_authentications.saml.first.is_active
                assert_equal 'https://saml-new-logout.com', @account.remote_authentications.saml.first.remote_logout_url
                assert_equal 'https://saml-exists-login.com', @account.remote_authentications.saml.first.remote_login_url
              end
            end
          end
        end

        describe 'saml and jwt combinations' do
          before do
            Account.any_instance.stubs(:has_saml?).returns(true)
            Account.any_instance.stubs(:has_jwt?).returns(true)

            @params[:remote_authentications][:saml][:remote_login_url] = 'https://www.saml-login-url.co.in'
            @params[:remote_authentications][:saml][:remote_logout_url] = 'https://www.saml-logout-url.co.in'
            @params[:remote_authentications][:saml][:ip_ranges] = '123.123.123.123'
            @params[:remote_authentications][:saml][:fingerprint] = '88:BD:4D:CC:2E:F8:EA:84:2C:2C:18:D1:81:8E:5E:BE:AA:A3:BA:9C'

            @params[:remote_authentications][:jwt][:remote_login_url] = 'https://jwt-login.com'
            @params[:remote_authentications][:jwt][:remote_logout_url] = 'https://jwt-logout.com'
            @params[:remote_authentications][:jwt][:update_external_ids] = false
            @params[:remote_authentications][:jwt][:next_token] = 'mNyPVcsmNiFkTAYWEe4p14bzOiGyAxOuPwMCvVRKie6p3LPI'
          end

          describe 'with both active' do
            before do
              @params[:remote_authentications][:saml][:is_active] = true
              @params[:remote_authentications][:jwt][:is_active] = true
            end

            describe 'with both priorities set to zero' do
              before do
                @params[:remote_authentications][:saml][:priority] = 0
                @params[:remote_authentications][:jwt][:priority] = 0
              end

              it 'returns an error' do
                api_settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
                api_settings_update.perform

                assert(api_settings_update.errors.any?)
              end
            end

            describe 'with both priorities set to one' do
              before do
                @params[:remote_authentications][:saml][:priority] = 1
                @params[:remote_authentications][:jwt][:priority] = 1
              end

              it 'returns an error' do
                api_settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
                api_settings_update.perform

                assert(api_settings_update.errors.any?)
              end
            end

            describe 'with distinct priority values (0 and 1)' do
              before do
                @params[:remote_authentications][:jwt][:priority] = 0
                @params[:remote_authentications][:saml][:priority] = 1
              end

              it 'does not cause an error' do
                api_settings_update = Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params)
                api_settings_update.perform

                assert_equal false, api_settings_update.errors.any?
              end

              it 'updates the value' do
                Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform

                @account.remote_authentications.reload
                assert_equal 0, @account.remote_authentications.jwt.first.priority
                assert_equal 1, @account.remote_authentications.saml.first.priority
              end
            end
          end
        end
      end
    end
  end
end
