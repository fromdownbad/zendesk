require_relative '../../../../../support/test_helper'
require_relative "../../../../../support/multiproduct_test_helper"

SingleCov.covered! file: 'lib/zendesk/accounts/security/api_settings_update.rb', uncovered: 36

describe Zendesk::Accounts::Security::ApiSettingsUpdate do
  include MultiproductTestHelper
  fixtures :accounts, :role_settings, :users

  [:shell_account_with_support, :minimum].each do |account_type|
    describe "with a #{account_type} account" do
      before do
        @user = users(:systemuser)

        @account = account_type == :shell_account_with_support ? setup_shell_account_with_support : accounts(account_type)

        @user.stubs(:account).returns(@account)
        Timecop.freeze('2016-01-01')
      end

      describe '#perform' do
        before do
          @params = {ip: {}, remote_authentications: {saml: {}, jwt: {}}, authentication: {agent: {password: {}}, end_user: {}}}
          @account.update_attributes!(admins_can_set_user_passwords: false)
        end

        describe 'account assumption expiration' do
          describe 'when adding a day' do
            before do
              @params[:assumption_duration] = 'day'
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            end

            it 'sets account assumption expiration 1 day into the future' do
              assert_equal 1.day.from_now, @account.settings.assumption_expiration
            end

            it "sets account settings assumption_duration to 'day'" do
              assert_equal 'day', @account.settings.assumption_duration
            end
          end

          describe 'when adding a week' do
            before do
              @params[:assumption_duration] = 'week'
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            end

            it 'sets account assumption expiration 1 week into the future' do
              assert_equal 1.week.from_now, @account.settings.assumption_expiration
            end

            it "sets account settings assumption_duration to 'week'" do
              assert_equal 'week', @account.settings.assumption_duration
            end
          end

          describe 'when adding a month' do
            before do
              @params[:assumption_duration] = 'month'
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            end

            it 'sets account assumption expiration one month in the future' do
              assert_equal 1.month.from_now, @account.settings.assumption_expiration
            end

            it "sets account settings assumption_duration to 'month'" do
              assert_equal 'month', @account.settings.assumption_duration
            end
          end

          describe 'when expiring in a year' do
            before do
              @params[:assumption_duration] = 'year'
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            end

            it 'sets account assumption expiration 1 year in the future' do
              assert_equal 1.year.from_now, @account.settings.assumption_expiration
            end

            it "sets account settings assumption_duration to 'year'" do
              assert_equal 'year', @account.settings.assumption_duration
            end
          end

          describe 'when set to always' do
            before do
              @params[:assumption_duration] = 'always'
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
            end

            it 'sets account assumption expiration 1 year in the future' do
              assert_equal '22000101'.to_datetime, @account.settings.assumption_expiration
            end

            it "sets account settings assumption_duration to 'always'" do
              assert_equal 'always', @account.settings.assumption_duration
            end
          end

          describe 'when disabling the assumption setting' do
            before do
              @params[:assumption_duration] = 'always'
              Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
              @account.reload
              assert_equal '22000101'.to_datetime, @account.settings.assumption_expiration
            end

            describe 'when sending in a fake param' do
              before do
                @params[:assumption_duration] = 'TONY'
                Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
              end

              it 'does not change already set value' do
                assert_equal '22000101'.to_datetime, @account.settings.assumption_expiration
              end
            end

            describe 'when turning off' do
              before do
                @params[:assumption_duration] = 'off'
                Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
              end

              it 'sets the value to nil' do
                assert_nil @account.settings.assumption_expiration
              end

              it "sets account settings assumption_duration to 'off'" do
                assert_equal 'off', @account.settings.assumption_duration
              end
            end

            describe 'when turning off for an account with expired "assumption_expiration"' do
              before do
                @setting = @account.settings.find { |s| s.name == "assumption_expiration" }
                time_expired = Time.now - 1.day
                @setting.update_columns(value: time_expired)
                body = {assumption_duration: "off"}
                @params.merge!(body)

                Rails.logger.expects(:warn).with("Turning off assumption on an already expired state: #{time_expired}").once
                Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
                @account.reload
              end

              it 'sets assumption_expiration to nil' do
                assert_nil @account.settings.assumption_expiration
              end

              it "sets account settings assumption_duration to 'off'" do
                assert_equal 'off', @account.settings.assumption_duration
              end
            end
          end

          describe 'when not_selected or already_set' do
            ['not_selected', 'already_set'].each do |param|
              before do
                @params[:assumption_duration] = 'day'
                Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
                @account.reload
                assert_equal 1.day.from_now, @account.settings.assumption_expiration
                assert_equal 'day', @account.settings.assumption_duration

                @params[:assumption_duration] = param
                Zendesk::Accounts::Security::ApiSettingsUpdate.new(@user, @params).perform
                @account.reload
              end

              it 'does not change the account assumption_expiration setting' do
                assert_equal 1.day.from_now, @account.settings.assumption_expiration
              end

              it 'does not change the account assumption_duration setting' do
                assert_equal 'day', @account.settings.assumption_duration
              end
            end
          end
        end
      end
    end
  end
end
