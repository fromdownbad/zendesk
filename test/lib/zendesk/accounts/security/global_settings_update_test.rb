require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Accounts::Security::GlobalSettingsUpdate do
  fixtures :accounts

  before do
    @user = stub('user')
    @user.stubs(:is_account_owner?)

    @account = accounts(:minimum)
    @params  = {account: {settings: {}}}

    @user.stubs(:account).returns(@account)

    @active_cert = stub('active certificate')
    @temp_cert   = stub('temporary certificate')
    @temp_cert_secondary = stub('temporary certificate 2')

    [@active_cert, @temp_cert, @temp_cert_secondary].each do |cert|
      cert.stubs(:set_uploaded_data)
      cert.stubs(:state=)
      cert.stubs(:sni_enabled=)
      cert.stubs(:sni_enabled?).returns(false)
      cert.stubs(:save!).returns(true)
    end
    @temp_cert.stubs(:upload_certificate)
    @temp_cert_secondary.stubs(:upload_certificate)
    @temp_cert_secondary.stubs(:set_uploaded_data).returns([Certificate::ErrorCode::NEED_KEY])

    @certificates = stub(active: [@active_cert], temporary: [@temp_cert_secondary, @temp_cert])
    @account.stubs(:certificates).returns(@certificates)

    @security_policy_update = begin
      Zendesk::Accounts::Security::GlobalSettingsUpdate.new(
        @user, @params
      )
    end
    Timecop.freeze('2016-01-01')
  end

  describe "#perform" do
    before do
      @params[:account][:settings] = {
        ip_restriction_enabled: true,
        enable_agent_ip_restrictions: true,
        enable_ip_mobile_access: false,
        credit_card_redaction: true,
        agreed_to_audioeye_tos: true,
        mobile_app_access: '1'
      }

      @account.update_attributes!(admins_can_set_user_passwords: false)
    end

    describe "when all parameters are provided" do
      before do
        @params[:account][:notifications_recipient]       = 'owner'
        @params[:account][:ip_ranges]                     = '123.123.123.123'
        @params[:account][:admins_can_set_user_passwords] = true
      end

      it "adds an error when trying to adds invalid ip ranges" do
        @params[:account][:ip_ranges] = '67.67.67.67.67 123.123.123.123 222.222.222'

        @security_policy_update.perform

        assert_equal 2, @security_policy_update.errors.size

        assert_equal 'Fix invalid IP range 67.67.67.67.67', @security_policy_update.errors.first
        assert_equal 'Fix invalid IP range 222.222.222', @security_policy_update.errors.last
      end

      it "updates the allowed IP ranges" do
        @security_policy_update.perform

        assert_equal '123.123.123.123', @account.ip_ranges
      end

      it "updates the global settings" do
        @security_policy_update.perform

        assert @account.settings.ip_restriction_enabled?
        assert @account.settings.credit_card_redaction?
        assert @account.settings.enable_agent_ip_restrictions?
        assert @account.settings.agreed_to_audioeye_tos?
        assert @account.settings.mobile_app_access?
        refute @account.settings.enable_ip_mobile_access?
        assert 'owner', @account.settings.security_notifications_recipient
        assert_nil @account.settings.security_notifications_recipient_id
      end

      describe "and the user is the account owner" do
        before { @user.stubs(:is_account_owner?).returns(true) }

        it "updates the setting" do
          @security_policy_update.perform

          assert @account.admins_can_set_user_passwords
        end
      end

      describe "and the user is not the account owner" do
        before { @user.stubs(:is_account_owner?).returns(false) }

        it "not updates the setting" do
          @security_policy_update.perform

          refute @account.admins_can_set_user_passwords
        end
      end

      describe "account assumption expiration" do
        before do
          SecurityMailer.stubs(:deliver_assumption_expiration)
        end

        describe "when adding a day" do
          before do
            @params[:account][:settings][:assumption_duration] = 'day'
            @security_policy_update.perform
          end

          it "sets account assumption expiration 1 day into the future" do
            assert_equal 1.day.from_now, @account.settings.assumption_expiration
          end

          it "sets account settings assumption_duration to 'day'" do
            assert_equal 'day', @account.settings.assumption_duration
          end
        end

        describe "when adding a week" do
          before do
            @params[:account][:settings][:assumption_duration] = 'week'
            @security_policy_update.perform
          end

          it "sets account assumption expiration 1 week into the future" do
            assert_equal 1.week.from_now, @account.settings.assumption_expiration
          end

          it "sets account settings assumption_duration to 'week'" do
            assert_equal 'week', @account.settings.assumption_duration
          end
        end

        describe "when adding a month" do
          before do
            @params[:account][:settings][:assumption_duration] = 'month'
            @security_policy_update.perform
          end

          it "sets account assumption expiration one month in the future" do
            assert_equal 1.month.from_now, @account.settings.assumption_expiration
          end

          it "sets account settings assumption_duration to 'month'" do
            assert_equal 'month', @account.settings.assumption_duration
          end
        end

        describe "when expiring in a year" do
          before do
            @params[:account][:settings][:assumption_duration] = 'year'
            @security_policy_update.perform
          end

          it "sets account assumption expiration 1 year in the future" do
            assert_equal 1.year.from_now, @account.settings.assumption_expiration
          end

          it "sets account settings assumption_duration to 'year'" do
            assert_equal 'year', @account.settings.assumption_duration
          end
        end

        describe "when set to always" do
          before do
            @params[:account][:settings][:assumption_duration] = 'always'
            @security_policy_update.perform
          end

          it "sets account assumption expiration 1 year in the future" do
            assert_equal '22000101'.to_datetime, @account.settings.assumption_expiration
          end

          it "sets account settings assumption_duration to 'always'" do
            assert_equal 'always', @account.settings.assumption_duration
          end
        end

        describe "when disabling the assumption setting" do
          before do
            @params[:account][:settings][:assumption_duration] = 'always'
            @security_policy_update.perform
            assert_equal '22000101'.to_datetime, @account.settings.assumption_expiration
          end

          describe "when sending in a fake param" do
            before do
              @params[:account][:settings][:assumption_duration] = 'TONY'
              @security_policy_update.perform
            end

            it "does not change already set value" do
              assert_equal '22000101'.to_datetime, @account.settings.assumption_expiration
            end
          end

          describe "when turning off" do
            before do
              @params[:account][:settings][:assumption_duration] = 'off'
              @security_policy_update.perform
            end

            it "sets the value to nil" do
              assert_nil @account.settings.assumption_expiration
            end

            it "sets account settings assumption_duration to 'off'" do
              assert_equal 'off', @account.settings.assumption_duration
            end
          end
        end

        describe "when unselected" do
          before do
            @params[:account][:settings][:assumption_duration] = 'day'
            @security_policy_update.perform
            assert_equal 1.day.from_now, @account.settings.assumption_expiration
            @params[:account][:settings][:assumption_duration] = 'unselected'
            @security_policy_update.perform
          end

          it "does not change the account assumption_expiration setting" do
            assert_equal 1.day.from_now, @account.settings.assumption_expiration
          end
        end
      end
    end

    describe "and the account does not have access to the IP restrictions settings" do
      before { @params[:account].delete(:settings) }

      it "does not blow up" do
        @security_policy_update.perform
      end
    end
  end
end
