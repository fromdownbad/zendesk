require_relative '../../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Accounts::Security::SettingsHelper do
  include Zendesk::Accounts::Security::SettingsHelper

  let(:account) { accounts(:minimum) }

  describe '#expiration_date' do
    it 'returns the correct value' do
      assert_equal Account::AssumptionSupport::ALWAYS_ASSUMABLE, expiration_date(:always)
    end
  end

  describe '#update_ip_ranges' do
    let(:selected_ip_ranges) { '192.161.158.18' }
    it 'updates the account ip_ranges' do
      update_ip_ranges
      assert_equal account.ip_ranges, selected_ip_ranges
    end
  end
end
