require_relative "../../../../support/test_helper"

SingleCov.covered!

describe Zendesk::Accounts::Security::SupportProductSettingsUpdate do
  fixtures :accounts

  before do
    @user = stub('user')
    @user.stubs(:is_account_owner?)

    @account = accounts(:minimum)
    @params  = {account: {settings: {}}}

    @user.stubs(:account).returns(@account)

    @security_policy_update = begin
      Zendesk::Accounts::Security::SupportProductSettingsUpdate.new(
        @user, @params
      )
    end
  end

  describe "#perform" do
    describe "with valid parameters" do
      before do
        @params[:account][:settings] = {
          agreed_to_audioeye_tos: true,
          credit_card_redaction: true,
          mobile_app_access: true,
        }

        @account.settings.set(
          agreed_to_audioeye_tos: false,
          credit_card_redaction: false,
          mobile_app_access: false
        )
        @account.save

        assert !@account.settings.credit_card_redaction?
        assert !@account.settings.agreed_to_audioeye_tos?
        assert !@account.settings.mobile_app_access?

        @security_policy_update.perform
      end

      it "updates the support product settings" do
        assert @account.settings.credit_card_redaction?
        assert @account.settings.agreed_to_audioeye_tos?
        assert @account.settings.mobile_app_access?
      end
    end

    describe "with unvalid parameters" do
      before do
        @params[:account][:settings] = {
          two_factor_enforce: false
        }
        @account.settings.two_factor_enforce = true
        @account.save

        assert @account.settings.two_factor_enforce
        @security_policy_update.perform
      end

      it "does not update other attributes" do
        assert @account.settings.two_factor_enforce
      end
    end
  end
end
