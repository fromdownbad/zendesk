require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 3

describe Zendesk::Accounts::Security::SettingsUpdate do
  fixtures :accounts, :remote_authentications

  class TestSettingsUpdate < Zendesk::Accounts::Security::SettingsUpdate
    def perform;
      save;
    end
  end

  before do
    @account      = accounts(:minimum)
    @user         = stub('user')
    @params       = {account: {settings: {}}}
    @remote_auths = []

    @user.stubs(:account).returns(@account)

    @security_policy_update = TestSettingsUpdate.new(@user, @params)
  end

  describe ".create" do
    describe "when the admins and agents settings tab is updated" do
      before { @params[:tab] = 'agents' }

      it "returns an agent settings update object" do
        @update_object = Zendesk::Accounts::Security::SettingsUpdate.create(@user, @params, @remote_auths)

        assert @update_object.is_a? Zendesk::Accounts::Security::AgentSettingsUpdate
      end
    end

    describe "when the end users settings tab is updated" do
      before { @params[:tab] = 'end_users' }

      it "returns an end-user settings update object" do
        @update_object = Zendesk::Accounts::Security::SettingsUpdate.create(@user, @params, @remote_auths)

        assert @update_object.is_a? Zendesk::Accounts::Security::EndUserSettingsUpdate
      end
    end

    describe "when the global settings tab is updated" do
      before { @params[:tab] = 'global' }

      it "returns an global settings update object" do
        @update_object = Zendesk::Accounts::Security::SettingsUpdate.create(@user, @params, @remote_auths)

        assert @update_object.is_a? Zendesk::Accounts::Security::GlobalSettingsUpdate
      end
    end

    describe "when the support_product settings tab is updated" do
      before { @params[:tab] = 'support_product' }

      it "returns an global settings update object" do
        @update_object = Zendesk::Accounts::Security::SettingsUpdate.create(@user, @params, @remote_auths)

        assert @update_object.is_a? Zendesk::Accounts::Security::SupportProductSettingsUpdate
      end
    end
  end

  describe "#success?" do
    it "raises an exception when the update has yet to be performed" do
      assert_raise(Zendesk::Accounts::Security::SettingsUpdate::NotYetPerformed) do
        @security_policy_update.success?
      end
    end

    describe "when the update has been performed" do
      before { @security_policy_update.perform }

      describe "and there are errors" do
        before { @security_policy_update.errors = ['error'] }

        it "returns false" do
          refute @security_policy_update.success?
        end
      end

      describe "and there are no errors" do
        before { @security_policy_update.errors = [] }

        it "returns true" do
          assert @security_policy_update.success?
        end
      end
    end
  end
end
