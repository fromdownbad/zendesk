require_relative "../../../../support/test_helper"

SingleCov.covered! uncovered: 1

describe Zendesk::Accounts::Security::AgentSettingsUpdate do
  fixtures :all #:accounts

  describe 'for a non-multiproduct account' do
    before do
      @account = accounts(:minimum)
      prepare_test_environment!
    end

    describe "#perform" do
      describe "when Zendesk is selected as the authentication type" do
        before do
          @params[:account][:role_settings] = {
            agent_security_policy_id: 200,
            agent_zendesk_login: true,
            agent_google_login: false,
            agent_remote_login: false
          }
        end

        it "updates the security policy ID" do
          @security_policy_update.perform

          assert_equal 200, @role_settings.agent_security_policy_id
        end

        it "makes all remote authentication methods inactive" do
          @remote_auths.each do |ra|
            ra.expects(:is_active=).with(false)
            ra.expects(:save!).returns(true)
          end

          @security_policy_update.perform
        end

        it "enables zendesk login" do
          @security_policy_update.perform

          assert(@role_settings.agent_zendesk_login)
        end

        describe "and custom security policy is selected" do
          before do
            @params[:account][:role_settings][:agent_security_policy_id] = '400'
            @params[:custom_security_policy] = {
              password_history_length: '3',
              password_length: '8',
              password_complexity: '0',
              password_in_mixed_case: false,
              password_duration: '0',
              failed_attempts_allowed: '5',
              session_timeout: '10'
            }

            [:update_attributes, :save!].each do |method|
              @custom_policy.stubs(method)
            end
          end

          it "updates the custom security policy settings" do
            @custom_policy.expects(:update_attributes).with(
              'password_history_length' => '3',
              'password_length'         => '8',
              'password_complexity'     => '0',
              'password_in_mixed_case'  => false,
              'password_duration'       => '0',
              'failed_attempts_allowed' => '5',
              'session_timeout'         => '10'
            )

            @security_policy_update.perform
          end

          it "saves the custom security policy settings changes" do
            @custom_policy.expects(:save!)

            @security_policy_update.perform
          end

          it 'assigns the session timeout as an account setting' do
            @account.settings.expects(:session_timeout=).with('10')
            @account.expects(:save!)

            @security_policy_update.perform
          end
        end
      end

      describe "when remote authentication is selected as the authentication type" do
        let(:agent_password_allowed) { true }

        before do
          @params[:account][:role_settings] = {
            agent_security_policy_id: 200,
            agent_zendesk_login: false,
            agent_google_login: false,
            agent_remote_login: true,
            agent_password_allowed: agent_password_allowed,
            agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ACCOUNT_OWNER
          }
          @security_policy_update.perform
        end

        before_should "update the remote authentication settings" do
          @native_auth.expects(:update_attributes).with(
            'is_active'         => true,
            'remote_logout_url' => 'https://native.com'
          )
        end

        before_should "not turn on other remote authentication methods" do
          @jwt_auth.expects(:is_active=).with(false)
          @saml_auth.expects(:is_active=).with(false)
        end

        before_should "not update other remote authentication settings" do
          @jwt_auth.expects(:update_attributes).never
          @saml_auth.expects(:update_attributes).never
        end

        before_should "update remote authentications priorities" do
          @native_auth.expects(:update_attribute).with(:priority, 0)
          @saml_auth.expects(:update_attribute).with(:priority, 1)
          @jwt_auth.expects(:update_attribute).with(:priority, 2)
        end

        it "disables zendesk login but allows passwords" do
          assert_equal false, @role_settings.agent_zendesk_login
          assert(@role_settings.agent_password_allowed)
        end

        describe "when passwords are disabled" do
          let(:agent_password_allowed) { false }

          it "disables zendesk login and passwords" do
            assert_equal false, @role_settings.agent_zendesk_login
            assert_equal false, @role_settings.agent_password_allowed
          end

          it "updates the agent_remote_bypass setting" do
            assert_equal RoleSettings::REMOTE_BYPASS_ACCOUNT_OWNER, @role_settings.agent_remote_bypass
          end
        end
      end

      describe "when one authentication type is selected" do
        before do
          @params[:account][:role_settings] = {
            agent_zendesk_login: false,
            agent_google_login: true,
            agent_remote_login: false,
            agent_password_allowed: false,
            agent_remote_bypass: RoleSettings::REMOTE_BYPASS_ACCOUNT_OWNER
          }

          @security_policy_update.perform
        end

        it " update" do
          assert @security_policy_update.success?
        end

        it "updates the role-based login service settings" do
          refute @role_settings.agent_zendesk_login
          assert @role_settings.agent_google_login
          refute @role_settings.agent_remote_login
          assert_equal RoleSettings::REMOTE_BYPASS_ACCOUNT_OWNER, @role_settings.agent_remote_bypass
        end
      end

      describe "when more than one authentication type is selected" do
        before do
          @params[:account][:role_settings] = {
            agent_zendesk_login: false,
            agent_google_login: true,
            agent_remote_login: true
          }
        end

        it "fails to update" do
          @security_policy_update.perform

          refute @security_policy_update.success?
        end

        it "does not attempt to save the changes" do
          ::Account.expects(:transaction).never

          @security_policy_update.perform
        end
      end

      describe "when one of the models fails to save" do
        before do
          @params[:account][:role_settings] = {
            agent_zendesk_login: true,
            agent_google_login: false,
            agent_remote_login: true
          }

          @native_auth.stubs(:save!).
            raises(ActiveRecord::RecordInvalid.new(@account))
          @security_policy_update.perform
        end

        it "fails to update" do
          refute @security_policy_update.success?
        end

        it "does not update the other models" do
          refute @role_settings.reload.agent_google_login
        end
      end

      describe "when remote authentication is enabled for both roles" do
        before do
          @role_settings.update_attributes!(
            agent_zendesk_login: false,
            agent_google_login: false,
            agent_remote_login: true,
            end_user_zendesk_login: false,
            end_user_google_login: false,
            end_user_remote_login: true
          )
        end

        describe "and a different authentication type is selected" do
          before do
            @params[:account][:role_settings] = {
              agent_zendesk_login: false,
              agent_google_login: true,
              agent_remote_login: false
            }

            @security_policy_update.perform
          end

          before_should "not save the remote authentications" do
            @remote_auths.each { |ra| ra.expects(:save!).never }
          end
        end
      end

      describe "when the account has a custom security policy" do
        before do
          CustomSecurityPolicy.build_from_current_policy(@account).save
          @account.role_settings.agent_security_policy_id = Zendesk::SecurityPolicy::Custom.id
          @account.settings.session_timeout = 5
          @account.save!
        end

        describe "when a non-custom security policy is set" do
          before do
            @params[:account][:role_settings] = {
              agent_security_policy_id: 200,
              agent_zendesk_login: true,
              agent_google_login: false,
              agent_remote_login: false
            }
          end

          it 'sets session timeout to the default' do
            @security_policy_update.perform
            assert_equal @account.settings.session_timeout, @account.settings.default(:session_timeout)
          end
        end

        describe "when a change not related with security policy or timeout is done" do
          before do
            @params[:account][:role_settings] = {
              agent_zendesk_login: false,
              agent_google_login: true,
              agent_remote_login: false
            }

            @security_policy_update.perform
          end

          it 'should not change the session timeout' do
            assert_equal @account.settings.session_timeout, 5
          end
        end
      end
    end
  end

  describe 'for a multiproduct account' do
    before do
      @account = accounts(:multiproduct)
      prepare_test_environment!
    end

    describe '#perform' do
      describe "when the account has a custom security policy" do
        before do
          CustomSecurityPolicy.build_from_current_policy(@account).save
          @account.role_settings.agent_security_policy_id = Zendesk::SecurityPolicy::Custom.id
          @account.settings.session_timeout = 5
          @account.save!
        end

        describe "when a non-custom security policy is set" do
          before do
            @params[:account][:role_settings] = {
              agent_security_policy_id: 200,
              agent_zendesk_login: true,
              agent_google_login: false,
              agent_remote_login: false
            }
          end

          it 'does not set session timeout to the default' do
            @security_policy_update.perform
            assert_not_equal @account.settings.session_timeout, @account.settings.default(:session_timeout)
          end
        end
      end
    end
  end

  def prepare_test_environment!
    @user                       = stub('user')
    @role_settings              = @account.role_settings
    @params                     = HashWithIndifferentAccess.new(
      account: {role_settings: {}}
    )

    @user.stubs(:account).returns(@account)

    Account.any_instance.stubs(:has_saml?).returns(true)
    RoleSettings.stubs(:where).with(account_id: @account.id).
      returns(stub(first_or_create: @role_settings))

    @native_auth = stub('native', id: 1, auth_mode: 1, priority: 2)
    @jwt_auth    = stub('jwt', id: 2, auth_mode: 3, priority: 1)
    @saml_auth   = stub('saml', id: 3, auth_mode: 2, priority: 0)

    @custom_policy = stub('custom security policy')

    @account.stubs(:custom_security_policy).returns(@custom_policy)

    @remote_auths = [@native_auth, @jwt_auth, @saml_auth]

    @remote_auths.each do |ra|
      ra.stubs(:update_attributes)
      ra.stubs(:update_attribute)
      ra.stubs(:is_active=)
      ra.stubs(:save!).returns(true)
    end

    @security_policy_update = begin
      Zendesk::Accounts::Security::AgentSettingsUpdate.new(
        @user, @params, @remote_auths
      )
    end

    @params[:account][:remote_authentications] = {
      'native' => {
        id: @native_auth.id,
        is_active: true,
        remote_logout_url: 'https://native.com'
      },
      'jwt' => {
        id: @jwt_auth.id,
        is_active: false,
        remote_logout_url: 'https://jwt.com'
      },
      'saml' => {
        id: @saml_auth.id,
        is_active: false,
        remote_logout_url: 'https://saml.com'
      }
    }

    @role_settings.update_attributes!(
      agent_security_policy_id: 100,
      agent_zendesk_login: false,
      agent_google_login: false,
      agent_remote_login: true
    )
  end
end
