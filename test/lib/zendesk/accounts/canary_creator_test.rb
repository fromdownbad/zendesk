require_relative "../../../support/test_helper"

SingleCov.covered! uncovered: 33

describe Zendesk::Accounts::CanaryCreator do
  before { Arturo::Feature.create!(symbol: :allow_login_on_get) }

  describe "#create_for_shard!" do
    before do
      Zendesk::Accounts::CanaryCreator.stubs(:seed)
      Zendesk::Accounts::CanaryCreator.stubs(:update_product_record)
    end

    it "creates an account" do
      account = Zendesk::Accounts::CanaryCreator.create_for_shard!(1)
      assert_equal 1, account.shard_id
      assert_equal "new-canary-shard-1", account.subdomain
      assert_equal "Z3N Canary Account for shard 1", account.name
      assert_settings(account)
      assert account.valid?
      assert Arturo::Feature.find_by_symbol(:allow_login_on_get).external_beta_subdomains.include?(account.subdomain)
    end

    it "accepts a subdomain" do
      account = Zendesk::Accounts::CanaryCreator.create_for_shard!(1, "foobartwizzle")
      assert_equal 1, account.shard_id
      assert_equal "foobartwizzle", account.subdomain
      assert_equal "Z3N Canary Account for shard 1", account.name
      assert_settings(account)
      assert account.valid?
      assert Arturo::Feature.find_by_symbol(:allow_login_on_get).external_beta_subdomains.include?(account.subdomain)
    end
  end

  describe "#create_for_cluster!" do
    before do
      Zendesk::Accounts::CanaryCreator.stubs(:seed)
      Zendesk::Accounts::CanaryCreator.stubs(:update_product_record)
    end

    it "creates an account" do
      account = Zendesk::Accounts::CanaryCreator.create_for_cluster!('2.4', 1)
      assert_equal "new-canary-cluster-2-4", account.subdomain
      assert_equal "Z3N Canary Account for cluster 2.4", account.name
      assert_equal 1, account.shard_id
      assert_settings(account)
      assert account.valid?
      assert Arturo::Feature.find_by_symbol(:allow_login_on_get).external_beta_subdomains.include?(account.subdomain)
    end

    it "accepts a subdomain" do
      account = Zendesk::Accounts::CanaryCreator.create_for_cluster!('2.4', 1, "foobartwizzle")
      assert_equal "foobartwizzle", account.subdomain
      assert_equal "Z3N Canary Account for cluster 2.4", account.name
      assert_equal 1, account.shard_id
      assert_settings(account)
      assert account.valid?
      assert Arturo::Feature.find_by_symbol(:allow_login_on_get).external_beta_subdomains.include?(account.subdomain)
    end
  end

  describe '#update_product_record' do
    before do
      Zendesk::Accounts::CanaryCreator.stubs(:seed)
      SupportProductCreationJob.stubs(:enqueue)
      stub_request(:post, /https:\/\/.+\.zendesk-test\.com\/api\/services\/accounts\/\d+\/products/)
      stub_request(:patch, /https:\/\/.+\.zendesk-test.com\/api\/services\/staff\/\d+\/entitlements/)
    end

    it 'creates product record' do
      expected_product_attributes = {
        product: {
          state: 'free',
          plan_settings: {
            plan_type: Zendesk::Accounts::CanaryCreator::PLAN_TYPE,
            max_agents: Zendesk::Accounts::CanaryCreator::NUM_MAX_AGENTS,
            light_agents: false
          }
        }
      }

      Zendesk::Accounts::Client.any_instance.expects(:update_product).with('support', expected_product_attributes, context: "canary_creator")
      Zendesk::Accounts::CanaryCreator.create_for_shard!(1)
    end
  end

  def assert_settings(account)
    assert account.settings.admins_can_set_user_passwords?
    assert account.settings.prefer_lotus?
    assert_equal 'disabled', account.settings.web_portal_state
    assert_equal 'enabled', account.settings.help_center_state
  end
end
