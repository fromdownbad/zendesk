require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Accounts::Product do
  let(:state) { 'subscribed' }
  let(:active) { true }
  let(:skus) { [] }
  let(:product_params) do
    {
      id: 1,
      account_id: 1298,
      name: 'support',
      state: state,
      active: active,
      state_updated_at: '02/10/2018 13:00:00',
      trial_expires_at: '22/10/2018 12:00:00',
      created_at: '30/09/2018 03:00:00',
      updated_at: '02/10/2018 16:00:00',
      skus: skus,
      plan_settings: {
        plan_type: 2,
        suite: false
      }
    }
  end

  let(:product) { Zendesk::Accounts::Product.new(product_params) }

  describe '#id' do
    it 'returns product id' do
      assert_equal product.id, 1
    end
  end

  describe '#account_id' do
    it 'returns account record id' do
      assert_equal product.account_id, 1298
    end
  end

  describe '#name' do
    it 'returns symbol' do
      assert_equal product.name, :support
    end
  end

  describe '#trial_expires_at' do
    it 'returns datetime' do
      assert_equal product.trial_expires_at, '22/10/2018 12:00:00'.to_datetime
    end
  end

  describe '#skus' do
    let(:skus) { ['zendesk_suite'] }

    it 'returns the list of skus' do
      assert_equal product.skus, skus
    end
  end

  describe '#zendesk_suite_sku?' do
    describe 'skus includes zendesk_suite' do
      let(:skus) { ['zendesk_suite'] }

      it 'returns true' do
        assert product.zendesk_suite_sku?
      end
    end

    describe 'skus does not include zendesk_suite' do
      let(:skus) { ['support'] }

      it 'returns false' do
        refute product.zendesk_suite_sku?
      end
    end
  end

  describe '#plan_settings' do
    let(:expected_hash) { { 'plan_type' => 2, 'suite' => false } }
    it 'returns hash of plan_settings' do
      assert_equal product.plan_settings, expected_hash
    end
  end

  describe '#state_updated_at' do
    it 'returns datetime' do
      assert_equal product.state_updated_at, '02/10/2018 13:00:00'.to_datetime
    end
  end

  describe '#updated_at' do
    it 'returns datetime' do
      assert_equal product.updated_at, '02/10/2018 16:00:00'.to_datetime
    end
  end

  describe '#created_at' do
    it 'returns datetime' do
      assert_equal product.created_at, '30/09/2018 03:00:00'.to_datetime
    end
  end

  %w[deleted cancelled not_started trial subscribed expired inactive free].each do |state|
    describe "##{state}?" do
      describe "when state from params is not #{state}" do
        let(:state) { 'something' }

        it 'returns false' do
          refute product.send "#{state}?"
        end
      end

      describe 'when state from params is deleted' do
        let(:state) { state }

        it 'returns true' do
          assert product.send "#{state}?"
        end
      end
    end
  end

  describe '#active?' do
    describe 'when active param is true' do
      let(:active) { true }

      it 'returns true' do
        assert product.active?
      end
    end

    describe 'when active param is false' do
      let(:active) { false }

      it 'returns false' do
        refute product.active?
      end
    end
  end

  describe '#started?' do
    describe "when state from params is not not_started" do
      let(:state) { 'subscribed' }

      it 'returns true' do
        assert product.started?
      end
    end

    describe "when state from params is not_started" do
      let(:state) { 'not_started' }

      it 'returns false' do
        refute product.started?
      end
    end
  end
end
