require_relative '../../../support/test_helper'

SingleCov.covered! uncovered: 3

describe Zendesk::Accounts::ProductSyncValidator do
  fixtures :accounts

  let(:subject) { Zendesk::Accounts::ProductSyncValidator }

  let(:system_account) { accounts(:systemaccount) }

  let(:sandbox_account) do
    account.stubs(is_sandbox?: true)
    account
  end

  let(:account) { accounts(:minimum) }

  let(:cancelled_account) do
    cancelled_account = accounts(:inactive)
    cancelled_account.subscription.update_attribute(:churned_on, Time.zone.now)
    cancelled_account
  end

  let(:shell_account_without_support) { accounts(:shell_account_without_support) }

  let(:precreated_account) do
    account.stubs(subdomain: "z3nprecreated-125212451525")
    account.stubs(name: PreAccountCreation::DEFAULT_ACCOUNT_NAME)
    account
  end

  let(:support_product) do
    {
      id: account.id,
      name: 'support',
      state: 'free',
      state_updated_at: DateTime.yesterday,
      created_at: DateTime.yesterday,
      updated_at: DateTime.yesterday,
      plan_settings: {
        plan_type: 1
      }
    }
  end

  let(:statsd) { stub_for_statsd }

  describe '.validate' do
    before do
      Zendesk::StatsD::Client.stubs(:new).returns(statsd)
    end

    describe 'when account service client throws a Kragle::ResponseError exception' do
      it 'skips validation, logs the error, and increments the comparison.aborted metric' do
        statsd.expects(:increment).with('comparison.aborted', tags: []).once
        statsd.expects(:increment).with('comparison.no_product_record', tags: []).never
        Rails.logger.expects(:error).once
        Zendesk::Accounts::Client.any_instance.stubs(:products).raises(Kragle::ResponseError)
        comparison_data = subject.validate(accounts(:trial))
        assert_nil comparison_data.fetch(:support_product)
        assert_equal :aborted, comparison_data.fetch(:result)
      end
    end

    describe 'when account service client throws a Faraday::Error exception' do
      it 'validates against an empty product collection and logs the error' do
        Rails.logger.expects(:error).once
        Zendesk::Accounts::Client.any_instance.stubs(:products).raises(Faraday::Error)
        assert_nil subject.validate(accounts(:trial)).fetch(:support_product)
      end
    end

    describe 'when account is system account' do
      it 'skips the account' do
        subject.any_instance.expects(:compare_product_expected_vs_actual).never
        subject.validate(system_account)
      end
    end

    describe 'when account is sandbox account' do
      it 'skips the account' do
        subject.any_instance.expects(:compare_product_expected_vs_actual).never
        subject.validate(sandbox_account)
      end
    end

    describe 'when account is inactive' do
      it 'skips the account' do
        refute cancelled_account.is_active?
        subject.any_instance.expects(:compare_product_expected_vs_actual).never
        subject.validate(cancelled_account)
      end
    end

    describe 'when account is multiproduct account' do
      it 'skips the account' do
        subject.any_instance.expects(:compare_product_expected_vs_actual).never
        subject.validate(shell_account_without_support)
      end
    end

    describe 'when account is in precreated state' do
      it 'returns a :pass result if the product record does not exist' do
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{precreated_account.id}/products?include_deleted_account=true").to_return(
          headers: { 'Content-Type' => 'application/json' },
          body: { products: [] }.to_json
        )
        assert_equal :pass, subject.validate(precreated_account).fetch(:result)
      end

      it 'returns a :fail result if the support product record exists' do
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{precreated_account.id}/products?include_deleted_account=true").to_return(
          headers: { 'Content-Type' => 'application/json' },
          body: { products: [support_product] }.to_json
        )
        assert_equal :fail, subject.validate(precreated_account).fetch(:result)
      end
    end

    describe 'when account is support' do
      let(:trial_account) { accounts(:trial) }

      describe 'if the product record does not exist' do
        before do
          stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{trial_account.id}/products?include_deleted_account=true").to_return(
            headers: { 'Content-Type' => 'application/json' },
            body: { products: [] }.to_json
          )
        end

        it 'returns a :no_product_record result' do
          assert_equal :no_product_record, subject.validate(trial_account).fetch(:result)
        end

        describe 'when instrument_results option is set to true' do
          before do
            statsd.stubs(:event)
          end

          it 'increments stat for comparison.no_product_record' do
            expected_tags = ['shard:1', 'validation_result:no_product_record', 'expected_state:trial', 'actual_state:',
                             'billing:false']
            statsd.expects(:increment).with('comparison.no_product_record', tags: expected_tags).once
            subject.validate(trial_account, instrument_results: true)
          end

          it 'sends validation events to DD' do
            statsd.expects(:event).once
            subject.validate(trial_account, instrument_results: true)
          end
        end

        describe 'when instrument_results option is set to false' do
          it 'does not increments stats' do
            statsd.expects(:increment).never
            subject.validate(trial_account, instrument_results: false)
          end

          it 'does not send validation events to DD' do
            statsd.expects(:event).never
            subject.validate(trial_account, instrument_results: false)
          end
        end
      end

      describe 'when support product record is present' do
        before do
          stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{trial_account.id}/products?include_deleted_account=true").to_return(
            headers: { 'Content-Type' => 'application/json' },
            body: { products: [support_product] }.to_json
          )

          Zendesk::StatsD::Client.stubs(:new).returns(statsd)
          statsd.stubs(:event)
        end

        it 'does not report stats to statsd by default' do
          statsd.expects(:increment).never
          subject.validate(trial_account, instrument_results: true)
        end

        describe 'trial_expires_on validation' do
          let(:support_account) { accounts(:support) }

          let(:support_product) do
            {
              id: support_account.id,
              name: 'support',
              state: 'subscribed',
              state_updated_at: DateTime.yesterday,
              trial_expires_at: support_account.subscription.trial_expires_on,
              created_at: DateTime.yesterday,
              updated_at: DateTime.yesterday,
              plan_settings: {
                plan_type: support_account.subscription.plan_type
              }
            }
          end

          before do
            stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{support_account.id}/products?include_deleted_account=true").to_return(
              headers: { 'Content-Type' => 'application/json' },
              body: { products: [support_product] }.to_json
            )
          end

          it 'returns pass if subscribed account and trial_expires_on is missing' do
            support_account.subscription.stubs(zuora_managed?: true)
            support_account.subscription.stubs(trial_expires_on: nil)
            assert_equal :pass, subject.validate(support_account).fetch(:comparison_results)[:trial_expires_at]
          end

          it 'returns pass if subscribed account and trial_expires_on does not match' do
            support_account.subscription.stubs(zuora_managed?: true)
            support_account.subscription.stubs(trial_expires_on: support_account.subscription.trial_expires_on - 1.day)
            assert_equal :pass, subject.validate(support_account).fetch(:comparison_results)[:trial_expires_at]
          end

          it 'returns pass if not subscribed account and trial_expires_on match' do
            support_account.subscription.stubs(zuora_managed?: false)
            support_account.subscription.stubs(trial_expires_on: support_account.subscription.trial_expires_on)
            assert_equal :pass, subject.validate(support_account).fetch(:comparison_results)[:trial_expires_at]
          end

          it 'returns fail if trial account and trial_expires_on does not match' do
            support_account.subscription.stubs(zuora_managed?: false)
            support_account.subscription.stubs(trial_expires_on: support_account.subscription.trial_expires_on - 1.day)
            assert_equal :fail, subject.validate(support_account).fetch(:comparison_results)[:trial_expires_at]
          end
        end

        describe 'when all compared values are equal to each other' do
          let(:support_product) do
            {
              id: account.id,
              name: 'support',
              state: 'trial',
              state_updated_at: DateTime.yesterday,
              trial_expires_at: trial_account.subscription.trial_expires_on.to_datetime,
              created_at: DateTime.yesterday,
              updated_at: DateTime.yesterday,
              plan_settings: {
                plan_type: trial_account.subscription.plan_type,
                max_agents: trial_account.subscription.max_agents,
                light_agents: trial_account.subscription.has_light_agents?
              }
            }
          end

          it 'returns a :pass result' do
            assert_equal :pass, subject.validate(trial_account).fetch(:result)
          end

          it 'returns a comparison results hash with individual attribute comparison results' do
            validation = subject.validate(trial_account)
            assert_equal :pass, validation.fetch(:comparison_results).fetch(:state)
            assert_equal :pass, validation.fetch(:comparison_results).fetch(:trial_expires_at)
            assert_equal :pass, validation.fetch(:comparison_results).fetch(:plan_type)
            assert_equal :pass, validation.fetch(:comparison_results).fetch(:max_agents)
            assert_equal :pass, validation.fetch(:comparison_results).fetch(:light_agents)
          end

          describe 'when instrument_results option is set to true' do
            it 'increments stat for comparison.pass' do
              expected_tags = ['shard:1', 'validation_result:pass', 'expected_state:trial', 'actual_state:trial',
                               'billing:false', 'state:pass', 'trial_expires_at:pass', 'plan_type:pass',
                               'max_agents:pass', 'light_agents:pass']
              statsd.expects(:increment).with('comparison.pass', tags: expected_tags).once
              subject.validate(trial_account, instrument_results: true)
            end

            it 'does not send validation events to DD' do
              statsd.expects(:event).never
              subject.validate(trial_account, instrument_results: true)
            end
          end

          describe 'when instrument_results option is set to false' do
            it 'does not increments stats' do
              statsd.expects(:increment).never
              subject.validate(trial_account, instrument_results: false)
            end

            it 'does not send validation events to DD' do
              statsd.expects(:event).never
              subject.validate(trial_account, instrument_results: false)
            end
          end
        end

        describe 'when light_agents values are not equal to each other' do
          let(:support_product) do
            {
              id: account.id,
              name: 'support',
              state: 'trial',
              state_updated_at: DateTime.yesterday,
              trial_expires_at: trial_account.subscription.trial_expires_on.to_datetime,
              created_at: DateTime.yesterday,
              updated_at: DateTime.yesterday,
              plan_settings: {
                plan_type: trial_account.subscription.plan_type,
                max_agents: trial_account.subscription.max_agents,
                light_agents: !trial_account.subscription.has_light_agents?
              }
            }
          end

          describe 'when light_agents param is nil' do
            describe 'when voltron_product_sync_validate_light_agents feature is on' do
              before do
                Arturo::Feature.create!(phase: 'on', symbol: :voltron_product_sync_validate_light_agents)
              end

              it 'returns a :fail result' do
                assert_equal :fail, subject.validate(trial_account, light_agents: nil).fetch(:result)
              end
            end

            describe 'when voltron_product_sync_validate_light_agents feature is off' do
              before do
                Arturo::Feature.create!(phase: 'off', symbol: :voltron_product_sync_validate_light_agents)
              end

              it 'returns a :pass result' do
                assert_equal :pass, subject.validate(trial_account, light_agents: nil).fetch(:result)
              end
            end
          end

          describe 'when light_agents param is true' do
            it 'returns a :fail result' do
              assert_equal :fail, subject.validate(trial_account, light_agents: true).fetch(:result)
            end
          end

          describe 'when light_agents param is false' do
            it 'returns a :pass result' do
              assert_equal :pass, subject.validate(trial_account, light_agents: false).fetch(:result)
            end
          end
        end

        describe 'when some compared values are not equal to each other' do
          let(:support_product) do
            {
              id: account.id,
              name: 'support',
              state: 'trial',
              state_updated_at: DateTime.yesterday,
              trial_expires_at: trial_account.subscription.trial_expires_on.to_datetime,
              created_at: DateTime.yesterday,
              updated_at: DateTime.yesterday,
              plan_settings: {
                plan_type: 999999,
                max_agents: 999999,
                light_agents: trial_account.subscription.has_light_agents?
              }
            }
          end

          it 'returns a :fail result' do
            assert_equal :fail, subject.validate(trial_account).fetch(:result)
          end

          it 'returns a comparison results hash with individual attribute comparison results' do
            validation = subject.validate(trial_account)
            assert_equal :pass, validation.fetch(:comparison_results).fetch(:state)
            assert_equal :pass, validation.fetch(:comparison_results).fetch(:trial_expires_at)
            assert_equal :fail, validation.fetch(:comparison_results).fetch(:plan_type)
            assert_equal :fail, validation.fetch(:comparison_results).fetch(:max_agents)
            assert_equal :pass, validation.fetch(:comparison_results).fetch(:light_agents)
          end

          describe 'when instrument_results option is set to true' do
            before do
              statsd.stubs(:event)
            end

            it 'increments stat for comparison.fail' do
              expected_tags = ['shard:1', 'validation_result:fail', 'expected_state:trial', 'actual_state:trial',
                               'billing:false', 'state:pass', 'trial_expires_at:pass', 'plan_type:fail',
                               'max_agents:fail', 'light_agents:pass']
              statsd.expects(:increment).with('comparison.fail', tags: expected_tags).once
              subject.validate(trial_account, instrument_results: true)
            end

            it 'sends validation events to DD' do
              statsd.expects(:event).once
              subject.validate(trial_account, instrument_results: true)
            end
          end

          describe 'when instrument_results option is set to false' do
            it 'does not increments stats' do
              statsd.expects(:increment).never
              subject.validate(trial_account, instrument_results: false)
            end

            it 'does not send validation events to DD' do
              statsd.expects(:event).never
              subject.validate(trial_account, instrument_results: false)
            end
          end
        end

        describe 'state_updated_at comparison' do
          let(:support_product) do
            {
              id: account.id,
              name: 'support',
              state: 'trial',
              state_updated_at: DateTime.yesterday,
              trial_expires_at: expired_trial_account.subscription.trial_expires_on.to_datetime,
              created_at: DateTime.yesterday,
              updated_at: DateTime.yesterday,
              plan_settings: {
                plan_type: expired_trial_account.subscription.plan_type,
                max_agents: expired_trial_account.subscription.max_agents,
                light_agents: expired_trial_account.subscription.has_light_agents?
              }
            }
          end

          let(:expired_trial_account) do
            trial_account.subscription.update_attribute(:trial_expires_on, DateTime.yesterday)
            trial_account.expire_trial!
            trial_account
          end

          let(:subscribed_account) do
            account = accounts(:minimum)
            account.subscription.stubs(zuora_managed?: true)
            account
          end

          before do
            stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{subscribed_account.id}/products?include_deleted_account=true").to_return(
              headers: { 'Content-Type' => 'application/json' },
              body: { products: [support_product] }.to_json
            )
          end

          describe 'when derived product state is "trial"' do
            it 'returns a comparison results hash excluding comparison for the state_updated_at attribute' do
              assert_nil subject.validate(trial_account).fetch(:comparison_results)[:state_updated_at]
            end
          end

          describe 'when derived product state is "expired"' do
            it 'returns a comparison results hash excluding comparison for the state_updated_at attribute' do
              assert_nil subject.validate(expired_trial_account).fetch(:comparison_results)[:state_updated_at]
            end
          end

          describe 'when derived product state is "subscribed"' do
            it 'returns a comparison results hash excluding comparison for the state_updated_at attribute' do
              assert_nil subject.validate(subscribed_account).fetch(:comparison_results)[:state_updated_at]
            end
          end
        end
      end
    end
  end
end
