require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Accounts::Multiproduct do
  fixtures :accounts

  class MultiproductModuleTest
    attr_accessor :current_account
    include Zendesk::Accounts::Multiproduct

    def initialize(account)
      @current_account = account
    end
  end

  let(:account) { accounts(:multiproduct) }

  describe '#multiproduct?' do
    it 'is not multiproduct' do
      account = accounts(:minimum)
      assert_equal false, MultiproductModuleTest.new(account).send(:multiproduct?)
    end

    it 'is multiproduct' do
      account = accounts(:multiproduct)
      assert(MultiproductModuleTest.new(account).send(:multiproduct?))
    end
  end

  describe '#first_active_product' do
    let(:products) { [] }

    before do
      response_body = { products: products.compact }.to_json
      stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
        to_return(body: response_body, headers: { content_type: 'application/json; charset=utf-8' })
    end

    it 'is nil for no active products' do
      assert_nil MultiproductModuleTest.new(account).send(:first_active_product)
    end

    it 'caches nils when no active products' do
      obj = MultiproductModuleTest.new(account)
      obj.expects(:active_products).once.returns([])
      assert_nil obj.send(:first_active_product)
      assert_nil obj.send(:first_active_product)
    end

    describe 'active support product' do
      let(:support_product) { FactoryBot.build(:support_trial_product) }
      let(:products) { [support_product] }

      it 'returns support when it is the only active product' do
        assert_equal :support, MultiproductModuleTest.new(account).send(:first_active_product).name
      end
    end

    describe 'active support product with acme-app' do
      let(:acme_app_product) { FactoryBot.build(:acme_app_free_product) }
      let(:support_product) { FactoryBot.build(:support_trial_product) }
      let(:products) { [acme_app_product, support_product] }

      it 'returns support' do
        assert_equal :support, MultiproductModuleTest.new(account).send(:first_active_product).name
      end
    end
  end

  describe '#support_product' do
    describe 'no support product' do
      it 'is nil' do
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products/support").
          to_return(status: 404, headers: { content_type: 'application/json; charset=utf-8' })
        assert_nil MultiproductModuleTest.new(account).send(:support_product)
      end
    end

    describe 'active support product' do
      it 'returns support when it is the only active product' do
        support_product = FactoryBot.build(:support_trial_product)
        stub_request(:get, "#{ENV['ACCOUNT_SERVICE_URL']}/api/services/accounts/#{account.id}/products").
          to_return(body: { products: [support_product] }.to_json, headers: { content_type: 'application/json; charset=utf-8' })
        assert_equal :support, MultiproductModuleTest.new(account).send(:support_product).name
      end
    end
  end
end
