require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Accounts::SettingsControllerSupport do
  fixtures :all

  let(:account) { accounts(:minimum) }
  let(:subject) { Zendesk::Accounts::SettingsControllerSupport }

  describe '#normalize_and_update_settings' do
    [
      [:active_features, :user_tagging, 'has_user_tags'],
      [:active_features, :ticket_tagging, 'ticket_tagging'],
      [:active_features, :customer_satisfaction, 'customer_satisfaction'],
      [:active_features, :business_hours, 'business_hours'],
      [:active_features, :automatic_answers, 'automatic_answers'],
      [:active_features, :allow_ccs, 'collaboration_enabled'],
      [:api, :accepted_api_agreement, 'accepted_api_agreement'],
      [:api, :api_password_access, 'api_password_access'],
      [:api, :api_token_access, 'api_token_access'],
      [:brands, :require_brand_on_new_tickets, 'require_brand_on_new_tickets'],
      [:google_apps, :has_google_apps_admin, 'has_google_apps_admin'],
      [:tickets, :list_newest_comments_first, 'events_reverse_order'],
      [:tickets, :private_attachments, 'private_attachments'],
      [:tickets, :email_attachments, 'email_attachments'],
      [:tickets, :list_empty_views, 'ticket_show_empty_views'],
      [:tickets, :tagging, 'ticket_tagging'],
      [:tickets, :markdown_ticket_comments, 'markdown_ticket_comments'],
      [:tickets, :emoji_autocompletion, 'emoji_autocompletion'],
      [:tickets, :agent_ticket_deletion, 'ticket_delete_for_agents'],
      [:rule, :macro_most_used, 'macro_most_used'],
      [:user, :tagging, 'has_user_tags'],
      [:user, :end_user_phone_number_validation, 'end_user_phone_number_validation'],
      [:user, :have_gravatars_enabled, 'have_gravatars_enabled'],
      [:cross_sell, :show_chat_tooltip, 'show_chat_tooltip']
    ].each do |feature_group, feature_id, setting_name|
      describe "when setting boolean account setting #{setting_name}" do
        [true, false].each do |setting_value|
          describe "set to #{setting_value}" do
            before do
              Apps::ChatAppInstallation.any_instance.stubs(:handle_disable_installation)
              subject.normalize_and_update_settings({ feature_group.to_s => {feature_id.to_s => setting_value}}, account)
            end

            it "sets #{setting_name} to #{setting_value}" do
              assert_equal setting_value, ActiveRecord::Type::Boolean.new.cast(account.settings.send(setting_name))
            end
          end
        end
      end
    end

    [
      [:tickets, :comments_public_by_default, 'is_comment_public_by_default'],
      [:tickets, :is_first_comment_private_enabled, 'is_first_comment_private_enabled'],
      [:user, :agent_created_welcome_emails, 'is_welcome_email_when_agent_register_enabled']
    ].each do |feature_group, feature_id, attribute_name|
      describe "when setting boolean account attribute #{attribute_name}" do
        [true, false].each do |attribute_value|
          describe "set to #{attribute_value}" do
            before do
              subject.normalize_and_update_settings({feature_group.to_s => {feature_id.to_s => attribute_value}}, account)
            end

            it "sets #{attribute_name} to #{attribute_value}" do
              assert_equal attribute_value, ActiveRecord::Type::Boolean.new.cast(account.send(attribute_name))
            end
          end
        end
      end
    end

    [:header_color, :page_background_color, :tab_background_color, :text_color].each do |feature_id|
      describe "when setting branding/#{feature_id}" do
        before do
          @test_color = 'deadbe'
          subject.normalize_and_update_settings({'branding' => {feature_id => @test_color}}, account)
        end

        it "sets #{feature_id}" do
          assert_equal @test_color, account.branding.send(feature_id)
        end
      end
    end

    describe 'when setting default brand id' do
      before do
        subject.normalize_and_update_settings({'brands' => {'default_brand_id' => 123}}, account)
      end

      it 'sets default brand id' do
        assert_equal 123, account.settings.default_brand_id
      end
    end

    describe 'when setting ticket_forms_instructions' do
      before do
        subject.normalize_and_update_settings({'ticket_form' => {'ticket_forms_instructions' => 'Please choose below wombat'}}, account)
      end

      it 'sets ticket_forms_instructions' do
        assert_equal 'Please choose below wombat', account.ticket_forms_instructions
      end
    end

    describe 'when setting macro_order' do
      before do
        subject.normalize_and_update_settings({'rule' => {'macro_order' => new_value}}, account)
      end

      %w[alphabetical position].each do |valid_value|
        describe 'when the new value is valid' do
          let(:new_value) { valid_value }

          it 'updates the setting' do
            assert_equal new_value, account.settings.macro_order
          end
        end
      end

      describe 'when the new value is invalid' do
        let(:new_value) { 'foobar' }

        it 'does not update the setting' do
          assert_equal 'alphabetical', account.settings.macro_order
        end
      end
    end

    describe 'when setting skill_based_filtered_views' do
      before do
        subject.normalize_and_update_settings({'rule' => {'skill_based_filtered_views' => new_value}}, account)
      end

      describe 'when new value is an integer' do
        let(:new_value) { 123 }

        it 'updates the setting' do
          assert_equal [new_value], account.settings.skill_based_filtered_views
        end
      end

      describe 'when new value is an array of one integer' do
        let(:new_value) { [123] }

        it 'updates the setting' do
          assert_equal new_value, account.settings.skill_based_filtered_views
        end
      end

      describe 'when new value is invalid' do
        let(:new_value) { [123, 456] }

        it 'does not update the setting' do
          assert_empty account.settings.skill_based_filtered_views
        end
      end
    end

    describe 'when setting edit_ticket_skills_permission' do
      before do
        subject.normalize_and_update_settings({'tickets' => {'edit_ticket_skills_permission' => new_value}}, account)
      end

      Zendesk::Accounts::SettingsControllerSupport::MANAGE_TICKET_SKILLS_PERMISSION.each do |_, value|
        describe 'when new value is a valid integer' do
          let(:new_value) { value }

          it 'updates the setting' do
            assert_equal new_value, account.settings.edit_ticket_skills_permission
          end
        end
      end

      describe 'when new value is invalid' do
        let(:new_value) { 4 }

        it 'update setting to default value' do
          assert_equal(
            Zendesk::Accounts::SettingsControllerSupport::MANAGE_TICKET_SKILLS_PERMISSION[:EDITABLE_BY_ADMINS],
            account.settings.edit_ticket_skills_permission
          )
        end
      end
    end

    describe 'when setting onboarding_segments' do
      before do
        subject.normalize_and_update_settings({'onboarding' => {'onboarding_segments' => new_value}}, account)
      end

      describe 'when new value is valid' do
        let(:new_value) { 'b2b' }

        it 'updates the setting' do
          assert_equal new_value, account.settings.onboarding_segments
        end
      end

      describe 'when new value is invalid' do
        let(:new_value) { 'ccc' }

        it 'does not update the setting' do
          refute account.settings.onboarding_segments.try(:include?, new_value)
        end
      end
    end

    describe 'when setting automatic_answers.threshold' do
      describe 'when new value is valid' do
        before do
          subject.normalize_and_update_settings({'automatic_answers' => {threshold: 'Balanced'}}, account)
        end

        it 'updates the setting' do
          assert_equal ::Zendesk::Types::PredictionThresholdType.BALANCED, account.settings.automatic_answers_threshold
        end
      end

      describe 'when new value is invalid' do
        before do
          @previous_setting = account.settings.automatic_answers_threshold
          subject.normalize_and_update_settings({'automatic_answers' => {threshold: 'foo'}}, account)
        end

        it 'does not update the setting' do
          assert_equal @previous_setting, account.settings.automatic_answers_threshold
        end
      end
    end

    describe 'email settings api' do
      def normalize_and_update_settings
        subject.normalize_and_update_settings({
          'email' => {
            'rich_content_in_emails' => true,
            'email_template_selection' => true,
            'send_gmail_messages_via_gmail' => true,
            'modern_email_template' => true,
            'accept_wildcard_emails' => true,
            'personalized_replies' => true,
            'gmail_actions' => true,
            'email_template_photos' => true,
            'simplified_email_threading' => true,
            'email_sender_authentication' => true,
            'custom_dkim_domain' => true,
            'html_mail_template' => 'new_html_template {{content}}',
            'text_mail_template' => 'new_text_template {{content}}',
            'mail_delimiter' => 'new_delimiter ------------',
            'no_mail_delimiter' => true
          }
        }, account)
      end

      before do
        account.settings.rich_content_in_emails = false
        account.settings.email_template_selection = false
        account.settings.send_gmail_messages_via_gmail = false
        account.settings.modern_email_template = false
        account.settings.accept_wildcard_emails = false
        account.settings.personalized_replies = false
        account.settings.gmail_actions = false
        account.settings.email_template_photos = false
        account.settings.simplified_email_threading = false
        account.settings.email_sender_authentication = false
        account.settings.custom_dkim_domain = false
        account.html_mail_template = 'old_html_template {{content}}'
        account.text_mail_template = 'old_text_template {{content}}'
        account.mail_delimiter = 'old_delimiter ------------'
        account.settings.no_mail_delimiter = false
        account.settings.save!
      end

      describe_with_arturo_enabled :email_simplified_threading_onboarding do
        it "updates the `simplified_email_threading` setting" do
          normalize_and_update_settings
          assert account.settings.simplified_email_threading
        end
      end

      describe_with_arturo_disabled :email_simplified_threading_onboarding do
        it "does not update the `simplified_email_threading` setting" do
          normalize_and_update_settings
          refute account.settings.simplified_email_threading
        end
      end

      describe_with_arturo_disabled :email_settings_api do
        it 'does not update email related settings' do
          normalize_and_update_settings

          refute account.settings.rich_content_in_emails
          refute account.settings.email_template_selection
          refute account.settings.send_gmail_messages_via_gmail
          refute account.settings.modern_email_template
          refute account.settings.accept_wildcard_emails
          refute account.settings.personalized_replies
          refute account.settings.gmail_actions
          refute account.settings.email_template_photos
          refute account.settings.simplified_email_threading
          refute account.settings.email_sender_authentication
          refute account.settings.custom_dkim_domain
          assert_equal 'old_html_template {{content}}', account.reload.html_mail_template
          assert_equal 'old_text_template {{content}}', account.reload.text_mail_template
          assert_equal 'old_delimiter ------------', account.reload.mail_delimiter
          refute account.settings.no_mail_delimiter
        end
      end

      describe_with_arturo_enabled :email_settings_api do
        it 'updates email related settings' do
          normalize_and_update_settings

          assert account.settings.rich_content_in_emails
          assert account.settings.email_template_selection
          assert account.settings.send_gmail_messages_via_gmail
          assert account.settings.modern_email_template
          assert account.settings.accept_wildcard_emails
          assert account.settings.personalized_replies
          assert account.settings.gmail_actions
          assert account.settings.email_template_photos
          assert account.settings.simplified_email_threading
          assert account.settings.email_sender_authentication
          assert account.settings.custom_dkim_domain
          assert_equal 'new_html_template {{content}}', account.reload.html_mail_template
          assert_equal 'new_text_template {{content}}', account.reload.text_mail_template
          assert_equal 'new_delimiter ------------', account.reload.mail_delimiter
          assert account.settings.no_mail_delimiter
        end

        describe 'when allow_email_template_customization is true' do
          before do
            Account.any_instance.stubs(:has_email_settings_api?).returns(true)
            Account.any_instance.stubs(:allow_email_template_customization?).returns(true)
          end

          it 'updates the templates' do
            normalize_and_update_settings

            assert_equal 'new_html_template {{content}}', account.reload.html_mail_template
            assert_equal 'new_text_template {{content}}', account.reload.text_mail_template
            assert_equal 'new_delimiter ------------', account.reload.mail_delimiter
          end
        end

        describe 'when allow_email_template_customization is false' do
          before do
            Account.any_instance.stubs(:has_email_settings_api?).returns(true)
            Account.any_instance.stubs(:allow_email_template_customization?).returns(false)
          end

          it 'does not update the templates' do
            normalize_and_update_settings

            assert_equal 'old_html_template {{content}}', account.reload.html_mail_template
            assert_equal 'old_text_template {{content}}', account.reload.text_mail_template
            assert_equal 'old_delimiter ------------', account.reload.mail_delimiter
          end
        end
      end
    end

    describe 'email ccs settings' do
      let(:auto_updated_ccs_followers_rules) { true }

      def normalize_and_update_settings
        subject.normalize_and_update_settings({
          'tickets' => {
            'accepted_new_collaboration_tos' => true,
            'agent_can_change_requester' => true,
            'agent_email_ccs_become_followers' => false,
            'auto_updated_ccs_followers_rules' => auto_updated_ccs_followers_rules,
            'comment_email_ccs_allowed' => true,
            'follower_and_email_cc_collaborations' => true,
            'light_agent_email_ccs_allowed' => true,
            'ticket_followers_allowed' => true
          }
        }, account)
      end

      before do
        account.settings.accepted_new_collaboration_tos = false
        account.settings.agent_can_change_requester = false
        account.settings.agent_email_ccs_become_followers = true
        account.settings.auto_updated_ccs_followers_rules = false
        account.settings.comment_email_ccs_allowed = false
        account.settings.follower_and_email_cc_collaborations = false
        account.settings.light_agent_email_ccs_allowed = false
        account.settings.ticket_followers_allowed = false
        account.settings.save!
      end

      describe_with_arturo_disabled :email_ccs do
        it 'does not update email ccs related settings' do
          normalize_and_update_settings

          refute account.settings.accepted_new_collaboration_tos
          refute account.settings.agent_can_change_requester
          assert account.settings.agent_email_ccs_become_followers
          refute account.settings.auto_updated_ccs_followers_rules
          refute account.settings.comment_email_ccs_allowed
          refute account.settings.follower_and_email_cc_collaborations
          refute account.settings.light_agent_email_ccs_allowed
          refute account.settings.ticket_followers_allowed
        end
      end

      describe_with_arturo_enabled :email_ccs do
        it 'updates email ccs related settings' do
          normalize_and_update_settings

          assert account.settings.accepted_new_collaboration_tos
          assert account.settings.agent_can_change_requester
          refute account.settings.agent_email_ccs_become_followers
          assert account.settings.auto_updated_ccs_followers_rules
          assert account.settings.comment_email_ccs_allowed
          assert account.settings.follower_and_email_cc_collaborations
          assert account.settings.light_agent_email_ccs_allowed
          assert account.settings.ticket_followers_allowed
        end

        describe 'when an email ccs setting is updated' do
          it 'sets the account on the setting' do
            account.settings.each { |s| s.expects(:account=).with(account) }

            normalize_and_update_settings
          end
        end

        describe 'when a non-email ccs setting is updated' do
          it 'does not set the account on the setting' do
            account.settings.each { |s| s.expects(:account=).never }

            subject.normalize_and_update_settings(
              {'ticket_form' => {'ticket_forms_instructions' => 'Please choose below wombat'}},
              account
            )
          end
        end

        describe '`auto_updated_ccs_followers_rules` account setting' do
          let(:auto_updated_ccs_followers_rules) { false }

          describe 'when setting is set to true' do
            before do
              account.settings.auto_updated_ccs_followers_rules = true
              account.settings.save!
            end

            it 'does not allow updates to the setting' do
              normalize_and_update_settings

              assert account.settings.auto_updated_ccs_followers_rules
            end
          end

          describe 'when setting is set to false' do
            let(:auto_updated_ccs_followers_rules) { true }

            before do
              account.settings.auto_updated_ccs_followers_rules = false
              account.settings.save!
            end

            it 'allows updates to the setting' do
              normalize_and_update_settings

              assert account.settings.auto_updated_ccs_followers_rules
            end
          end
        end
      end
    end

    describe 'agent_workspace setting' do
      let(:body) { { installations: [] } }

      before do
        stub_request(:get, "https://minimum.zendesk-test.com/api/v2/apps/installations.json").to_return(
          status: 200,
          body: body.to_json,
          headers: {'Content-Type' => 'application/json'}
        )

        account.settings.check_group_name_uniqueness = check_group_name_uniqueness
        account.settings.save!
        subject.normalize_and_update_settings({'agents' => {'agent_workspace' => true}}, account)
      end

      describe 'when groups & departments has been migrated' do
        let(:check_group_name_uniqueness) { true }

        it 'updates the setting' do
          assert account.settings.polaris
        end
      end

      describe 'when groups & departments has NOT been migrated' do
        let(:check_group_name_uniqueness) { false }

        it 'sets the setting to false' do
          refute account.settings.polaris
        end
      end
    end
  end

  describe 'when setting localization.locale_ids' do
    before do
      @locale_ids = [translation_locales(:brazilian_portuguese).id]
      subject.normalize_and_update_settings({'localization' => {'locale_ids' => @locale_ids}}, account)
    end

    it 'sets allowed_translation_locale_ids' do
      assert_equal @locale_ids, account.allowed_translation_locale_ids
    end
  end

  describe '#update_automatic_answers_settings' do
    describe 'when new value is valid' do
      before do
        subject.update_automatic_answers_settings({threshold: 'Balanced'}, account)
      end

      it 'updates the setting' do
        assert_equal ::Zendesk::Types::PredictionThresholdType.BALANCED, account.settings.automatic_answers_threshold
      end
    end

    describe 'when new value is invalid' do
      before do
        @previous_setting = account.settings.automatic_answers_threshold
        subject.update_automatic_answers_settings({threshold: 'foo'}, account)
      end

      it 'does not update the setting' do
        assert_equal @previous_setting, account.settings.automatic_answers_threshold
      end
    end
  end

  describe '#sanitize_onboarding_segments' do
    it 'returns string of valid segments' do
      segments = 'b2b, b2c, internal_hr, invalid'

      assert_equal 'b2b,b2c,internal_hr', subject.sanitize_onboarding_segments(segments)
    end

    it 'removes duplicate segments' do
      segments = 'b2b, b2b, b2b'

      assert_equal 'b2b', subject.sanitize_onboarding_segments(segments)
    end

    it 'returns the empty string if all segments are invalid' do
      segments = 'invalid, invalid2'

      assert_equal '', subject.sanitize_onboarding_segments(segments)
    end

    it 'returns the empty string if segments is empty string' do
      segments = 'invalid, invalid2'

      assert_equal '', subject.sanitize_onboarding_segments(segments)
    end
  end
end
