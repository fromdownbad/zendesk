require_relative '../../../support/test_helper'

SingleCov.covered!

describe Zendesk::Accounts::CreationParamsValidator do
  let(:params) do
    {
      owner: {
        email: email,
        name: owner_name
      },
      address: {
        phone: phone_number
      }
    }
  end
  let(:email)         { 'abc@zd-dev.com' }
  let(:owner_name)    { 'Supreme Leader' }
  let(:phone_number)  { '123456789' }

  let(:validator) { Zendesk::Accounts::CreationParamsValidator.new(params) }

  describe 'validates owner name' do
    describe 'when name is missing' do
      let(:owner_name) { nil }

      describe '#valid?' do
        it 'validation result is invalid' do
          refute validator.valid?
        end
      end

      describe '#errors' do
        it 'returns error message' do
          validator.valid?
          refute_empty validator.errors.messages
          assert_equal validator.errors.messages[:owner_name], ["can't be blank"]
        end
      end
    end

    describe 'when name is empty' do
      let(:owner_name) { '' }

      describe '#valid?' do
        it 'validation result is invalid' do
          refute validator.valid?
        end
      end

      describe '#errors' do
        it 'returns error message' do
          validator.valid?
          refute_empty validator.errors.messages
          assert_equal validator.errors.messages[:owner_name], ["can't be blank"]
        end
      end
    end

    describe 'when Arturo flag :ocp_limit_owner_name_cjk_characters_on_account_creation is enabled' do
      let(:statsd_client) { stub_for_statsd }
      before do
        Arturo.enable_feature!(:ocp_limit_owner_name_cjk_characters_on_account_creation)
        validator.stubs(:statsd_client).returns(statsd_client)
      end

      describe 'when name contains too many CJK characters' do
        let(:cjk_string) { 'Hi 新年好，澚門威尼斯人邀您进901526.com先领38-88元，rjze 新年好，澚門威尼斯人邀您进901526.com先领38-88元，rjze' }
        let(:owner_name) { cjk_string }

        it 'returns an error message' do
          validator.valid?
          refute_empty validator.errors.messages
          assert_equal validator.errors.messages[:owner_name], [cjk_string + ' is not properly formatted']
        end

        it 'reports incident' do
          statsd_client.expects(:increment).with('new_account_cjk_spam').once
          validator.valid?
        end
      end

      describe 'when name contains minimal CJK characters' do
        let(:owner_name) { 'Jill Smith 这是' }

        it 'the result is valid' do
          assert validator.valid?
        end
      end

      describe 'when name contains minimal CJK characters and a .com' do
        let(:cjk_string) { 'Jill Smith 这是 901526.com' }
        let(:owner_name) { cjk_string }

        it 'returns an error message' do
          validator.valid?
          refute_empty validator.errors.messages
          assert_equal validator.errors.messages[:owner_name], [cjk_string + ' is not properly formatted']
        end
      end

      describe 'when name contains minimal CJK characters and a .co.uk' do
        let(:cjk_string) { 'Jill Smith 这是 901526.co.uk' }
        let(:owner_name) { cjk_string }

        it 'returns an error message' do
          validator.valid?
          refute_empty validator.errors.messages
          assert_equal validator.errors.messages[:owner_name], [cjk_string + ' is not properly formatted']
        end
      end
    end

    describe 'when Arturo flag :ocp_limit_owner_name_cjk_characters_on_account_creation is disabled' do
      let(:statsd_client) { stub_for_statsd }
      before do
        Arturo.disable_feature!(:ocp_limit_owner_name_cjk_characters_on_account_creation)
        validator.stubs(:statsd_client).returns(statsd_client)
      end

      describe 'when name contains too many CJK characters' do
        let(:cjk_string) { 'Hi 新年好，澚門威尼斯人邀您进901526.com先领38-88元，rjze 新年好，澚門威尼斯人邀您进901526.com先领38-88元，rjze' }
        let(:owner_name) { cjk_string }

        it 'returns no error message' do
          validator.valid?
          assert_empty validator.errors.messages
        end

        it 'reports incident' do
          statsd_client.expects(:increment).with('new_account_cjk_spam').never
          validator.valid?
        end
      end

      describe 'when name contains minimal CJK characters' do
        let(:owner_name) { 'Jill Smith 这是' }

        it 'the result is valid' do
          assert validator.valid?
        end
      end

      describe 'when name contains minimal CJK characters and a .com' do
        let(:cjk_string) { 'Jill Smith 这是 901526.com' }
        let(:owner_name) { cjk_string }

        it 'returns no error message' do
          validator.valid?
          assert_empty validator.errors.messages
        end
      end

      describe 'when name contains minimal CJK characters and a .co.uk' do
        let(:cjk_string) { 'Jill Smith 这是 901526.co.uk' }
        let(:owner_name) { cjk_string }

        it 'returns no error message' do
          validator.valid?
          assert_empty validator.errors.messages
        end
      end
    end
  end

  describe 'validates owner email' do
    before do
      Zendesk::Mail::Address.stubs(:valid_address?).with(email).returns(email_validity)
    end

    describe 'when email is valid' do
      let(:email_validity) { true }
      describe '#valid?' do
        it 'validation result is valid' do
          assert validator.valid?
        end
      end

      describe '#errors' do
        it 'return empty errors' do
          validator.valid?
          assert_empty validator.errors.messages
        end
      end
    end

    describe 'when email is not valid' do
      let(:email_validity) { false }

      describe '#valid?' do
        it 'validation result is valid' do
          refute validator.valid?
        end
      end

      describe '#errors' do
        it 'return error messages' do
          validator.valid?
          refute_empty validator.errors.messages
          assert_equal validator.errors.messages[:owner_email], ["#{email} is not properly formatted"]
        end
      end
    end
  end

  describe 'validates phone number' do
    describe 'when phone number is missing' do
      let(:phone_number) { nil }

      describe '#valid?' do
        it 'validation result is invalid' do
          refute validator.valid?
        end
      end

      describe '#errors' do
        it 'returns error message' do
          validator.valid?
          refute_empty validator.errors.messages
          assert_equal validator.errors.messages[:address_phone], ["can't be blank"]
        end
      end
    end

    describe 'when phone number is empty' do
      let(:phone_number) { '' }

      describe '#valid?' do
        it 'validation result is invalid' do
          refute validator.valid?
        end
      end

      describe '#errors' do
        it 'returns error message' do
          validator.valid?
          refute_empty validator.errors.messages
          assert_equal validator.errors.messages[:address_phone], ["can't be blank"]
        end
      end
    end
  end
end
